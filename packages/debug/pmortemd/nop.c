/* Simple function used to disable optimization in building ptest.c
 */

#include <stdint.h>

int nop(uint32_t *arg)
{
	return *arg;
}
