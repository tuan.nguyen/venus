#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <getopt.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>

#include "pmortem.h"
#include "slog.h"

#if !defined(min)
#define min(a, b) ((a) < (b) ? (a) : (b))
#endif

int nop(uint32_t *arg);

/* Utility to test SIGSEGV traceback on WeMo hardware.
 */

static inline void fatal(const char *x, ...)
{
        va_list ap;

        va_start(ap, x);
        vfprintf(stderr, x, ap);
        va_end(ap);
        exit(EXIT_FAILURE);
}

static void sig_segv(int signum)
{
	uint32_t *up = (uint32_t *) &signum;

	pmortem_connect_and_send(up, 8 * 1024);
	fprintf(stderr, "SIGSEGV [%d].  Best guess fault address: %08x, ra: %08x, sig return: %p\n",
		signum, up[8], up[72], __builtin_return_address(0));

	_exit(1);
}

void level3(uint32_t arg)
{
	uint32_t value = arg + 1;

	*(uint32_t *) arg = nop(&value);
}

void level2(uint32_t arg)
{
	char buffer[56];
	uint32_t value = arg + 1;

	memset(buffer, 0, sizeof(buffer));
	nop((uint32_t *) buffer);
	level3(nop(&value));
}

void level1(uint32_t arg)
{
	uint32_t value = arg + 1;

	level2(nop(&value));
}

void level0(uint32_t arg)
{
	uint32_t value = arg + 1;

	level1(nop(&value));
}

void start_crash(uint32_t arg)
{
	uint32_t value = arg + 1;

	level0(nop(&value));
}

void clear_some_stack(void)
{
	char buffer[2048];

	memset(buffer, 0, sizeof(buffer));
	printf("stack: %p-%p\n", buffer, buffer + sizeof(buffer));
	nop((void *) buffer);
}

struct option opts[] = {
        { "help",  0, NULL, 'h' },
        { NULL,    0, NULL, 0 }
};

void usage(const char *mesg)
{
	if (mesg)
		fputs(mesg, stderr);
	fprintf(stderr, "%s [options]\n"
		"  options:\n"
		"    -h|--help             Show this usage message\n",
		*__environ);
}

int main(int argc, char *argv[])
{
	struct sigaction action;
	int c;
	SLOG_Init(SLOG_STDOUT, NULL);
    set_thread_name("pmtest");

	while ((c = getopt_long(argc, argv, "h", opts,
				NULL)) != -1) {
		switch (c) {
		case 'h':
			usage(NULL);
			exit(0);
		default:
			usage(NULL);
			exit(1);
		}
	}

	if (optind < argc)
		fprintf(stderr, "Extra arguments %s... ignored\n",
			argv[optind]);

	memset(&action, 0, sizeof(action));
	action.sa_flags = (SA_NOCLDSTOP | SA_NOCLDWAIT | SA_RESTART);
	action.sa_handler = sig_segv;
	if (sigaction(SIGSEGV, &action, NULL))
		fatal("sigaction failed: %s", strerror(errno));

	clear_some_stack();
	start_crash(0x1234);

	return 0;
}
