#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>

#include "pmortem-internal.h"
#include "pmortem.h"
#include "slog.h"

void pmortem_connect_and_send(void *stack, size_t size)
{
	int fd;
	char c = 0;
	long pagesize;
	int size_int = size;

	pagesize = sysconf(_SC_PAGESIZE);
	fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd < 0)
		LOG_ERROR("socket");
	else {
		pid_t pid = getpid();
		struct sockaddr_un address;

		memset(&address, 0, sizeof(address));
		address.sun_family = AF_UNIX;
		strncpy(address.sun_path, PMORTEMD_UDS_PATH,
			sizeof(address.sun_path));
		if (connect(fd, (struct sockaddr *) &address, sizeof(address)) < 0)
		    LOG_ERROR("connect failed");
		else {
			ssize_t n;
			int send_err = 0;

			n = send(fd, &pid, sizeof(pid), 0);
			if (n < 0)
				LOG_ERROR("send pid failed");
			n = send(fd, &stack, sizeof(stack), 0);
			if (n < 0)
				LOG_ERROR("pmortem: send stack address failed");

			// SLOGI("\nstack: %p, pagesize: %d\n", stack, pagesize);

			while ((size_int > 0) && !send_err) {
				// SLOGI("\nsending from %p, %d bytes, size: %d\n", stack, pagesize -((ptrdiff_t) stack & (pagesize - 1)), size_int);

				n = send(fd, stack,
					 pagesize -
					 ((ptrdiff_t) stack & (pagesize - 1)),
					 0);
				// SLOGI("n = %d\n", n);
				if (n < 0) {
						/* We don't log an error here,
						 * it is most likely a bad
						 * address reference.  Rather
						 * than log an error and exit,
						 * just return to let the
						 * caller clean up or abort.
						 */
					send_err = 1;
					// SLOGI("\nsend failed for %d bytes\n", pagesize -((ptrdiff_t) stack & (pagesize - 1)));
					break;
				}
				else {
					size_int -= n;
					stack += n;
					// SLOGI("\nstack: %p, size: %d, n: %d\n", stack, size_int, n);
				}

				/* Wait for acknowledgment */
				n = recv(fd, &c, sizeof(c), 0);
			}

		}
		close(fd);
	}
	return;
}
