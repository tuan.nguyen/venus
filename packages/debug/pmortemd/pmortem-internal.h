#if !defined(__PMORTEM_INTERNAL_H__)
#define __PMORTEM_INTERNAL_H__

typedef size_t ptrdiff_t;	/* The WeMo toolchain doesn't provide a
				 * ptrdiff_t, substitute size_t until building
				 * with a recent toolchain.
				 */

#define PMORTEMD_UDS_PATH  "/tmp/pmortemd.socket"
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

#define LOG_ERROR(...) log_error(__FUNCTION__, __LINE__, __VA_ARGS__)

static inline void log_error(const char *f, int line, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	fprintf(stderr, "%s:%d ", f, line);
	vfprintf(stderr, fmt, args);
	fprintf(stderr, ": %s\n", strerror(errno));
	va_end(args);
        exit(EXIT_FAILURE);
}

#endif /* __PMORTEM_INTERNAL_H__ */
