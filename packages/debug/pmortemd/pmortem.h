#if !defined(__PMORTEM_H__)
#define __PMORTEM_H__

void pmortem_connect_and_send(void *stack, size_t size);

#endif /* __PMORTEM_H__ */
