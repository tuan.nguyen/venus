#ifndef _SYNC_USERS_H_
#define _SYNC_USERS_H_

#include "VR_list.h"

#define USERS_GET_ALL_DOMAIN      "https://%s/api/devices/%s/userhomes"

typedef struct _user_list
{
	char *userId;
	char *shareId;
	char *name;
	char *enable;
	char *userType;
	char *shareTime;

    struct VR_list_head list;
}user_list_t;

void sync_users_from_cloud(void);

#endif