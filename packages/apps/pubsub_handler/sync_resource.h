#ifndef _SYNC_RESOURCE_H_
#define _SYNC_RESOURCE_H_

#include "VR_list.h"

#define RESOURCE_GET_ALL_DOMAIN      "https://%s/api/devices/%s/resources"

void sync_all_resources_with_cloud(int useEtag);
void update_sound_name(void);

typedef struct _resource_list
{
	char *deviceId;
	char *deviceName;
	char *serialId;
	char *cloudId;
	char *type;

    struct VR_list_head list;
}resource_list_t;

#endif