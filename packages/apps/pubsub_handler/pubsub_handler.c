/******************************************************************************
 * Copyright (c) 2014, VEriK Systems. All rights reserved.
 *
 * Module: Remote Service
 *
 ******************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <libubus.h>
#include <libubox/blobmsg_json.h>
#include <json-c/json.h>
#include <uci.h>
#include <curl/curl.h>
#include <mosquitto.h>

#include "pubsub_handler.h"
#include "database.h"
#include "slog.h"
#include "group_apis.h"
#include "VR_define.h" 
#include "verik_utils.h"
#include "verik_sec.h"
#include "vr_rest.h"
#include "timer.h"
#include "led.h"
#include "rule_setup.h"
#include "pmortem.h"
#include "vr_sound.h"
#include "sync_resource.h"
#include "sync_group.h"
#include "sync_users.h"
#include "report_manage.h"
#include "post_resource_manage.h"
#include "ping.h"

#define KEEP_ALIVE_TIME 60
#define CHECK_CONNECTION_TIME KEEP_ALIVE_TIME+KEEP_ALIVE_TIME/2+10

#define TURN_AP_OFF_TIMEOUT 20

#define REGISTER_CONNECT_ERROR -2
#define REGISTER_CLOUD_ERROR   -3

#define SHADOW_TOPIC_HEADER "$iot/things/"
#define VR_QOS QOS0

struct mosquitto *mosq;
static struct ubus_context *ctx;
static struct blob_buf buff;
static struct ubus_subscriber ubus_subscribe_handler;
static struct ubus_event_handler ubus_event_listener;

sqlite3 *rule_db;
sqlite3 *dev_db;
sqlite3 *support_devs_db;
sqlite3 *user_db;
new_pubsub_ubus *g_pubsub_ubus = NULL;
static int list_count = 0;

// static int g_remote_sevice_is_running = 1;
static timer_t g_timer_disconnected = 0;
static timer_t g_timer_scanwifi = 0;
static timer_t g_timer_unclaimed = 0;

static int g_inform_power_cycle = 0;
static int g_first_time_bootup = 0;
int g_is_disconnected = 1;

int g_shmid = 0;
char *g_shm = NULL;

int g_led_shmid = 0;
char *g_led_shm = NULL;

char g_mqtt_address[SIZE_1024B];
char g_mqtt_clientId[SIZE_256B];
char PUBLISH_RES_TOPIC[SIZE_256B];
char SUBSCRIBE_REQ_TOPIC[SIZE_256B];

char PUBLISH_SHADOW_UPDATE_TOPIC[SIZE_256B];
char SUBSCRIBE_CONTROL_TOPIC[SIZE_256B];
char SUBSCRIBE_SETTING_TOPIC[SIZE_256B];

// char SUBSCRIBE_SHADOW_DELTA_TOPIC[256];
// char SUBSCRIBE_SHADOW_UPDATE_REJECTED_TOPIC[256];
// char SUBSCRIBE_SHADOW_UPDATE_ACCEPTED_TOPIC[256];

pthread_mutex_t pubsub_ubus_listMutex;
pthread_t thread_mosquitto;
pthread_t thread_remove_actions_timeout;
static int remove_actions_timeout_enable = 1;
static int monitor_connection_enable = 1;

// void Send_ubus_notify(char *data);
void VR_(send_shadow_reported)(char *data);
void VR_(shadow_update_status)(const char *msgstr, int mode);
void update_shadow_status_cb(void *data);

daemon_service daemon_services[] = {
    {"zigbee_handler",      "zigbee",            0,        0, 0},
    {"zwave_handler",       "zwave",             0,        0, 0},
    {"upnp_handler",        "upnp",              0,        0, 0},
    {"venus_handler",       "venus",             0,        0, 0},
};

static const struct blobmsg_policy msg_policy[1] = {
    [0] = { .name = ST_UBUS_MESSAGE_RETURN_KEY, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy handle_ubus_notify_policy[__NOTIFY_SIZE] = {
    [NOTIFY_SERVICE] = { .name = ST_SERVICE, .type = BLOBMSG_TYPE_STRING },
    [NOTIFY_MSG] = { .name = ST_MESSAGE, .type = BLOBMSG_TYPE_STRING },
};

const char *cmd_support[] = {
    ST_NOTIFY,
    ST_SET_TIME,
    ST_GET_TIME,
    ST_AUTO_CONF,
    ST_ADD_DEVICES,
    ST_LIST_DEVICES,
    ST_REMOVE_DEVICE,
    ST_RESET,
    ST_OPEN_CLOSE_NETWORK,
    ST_GET_SUB_DEVS,
    ST_GET_BINARY,
    ST_SET_BINARY,
    ST_CHANGE_NAME,
    ST_ALEXA,
    ST_IDENTIFY,
    ST_FIRMWARE_ACTIONS,
    ST_SET_RULE,
    ST_GET_RULE,
    ST_RULE_ACTIONS,
    ST_READ_SPEC,
    ST_WRITE_SPEC,
    ST_READ_S_SPEC,
    ST_WRITE_S_SPEC,
    ST_REDISCOVER,
    ST_CREATE_GROUP,
    ST_GROUP_ACTIONS,
    ST_GROUP_CONTROL,
    ST_NETWORK,
    ST_READ_SPEC_CRC,
    ST_WRITE_SPEC_CRC,
    ST_REQUEST_ACCESS,
    ST_DENY_ACCESS,
    ST_LIST_USERS,
    ST_SHARE_ACCESS,
    ST_OB_REQUEST_ACCESS,
    ST_PUBLISH,
    ST_GET_NAME,
    ST_RULE_MANUAL,
    ST_LIST_GROUPS,
    ST_VENUS_ALARM,
    ST_SCENE_ACTIVATION,
    ST_SCENE_ACTIONS,
};

static void VR_(add_last_list)(new_pubsub_ubus *node)
{
    pthread_mutex_lock(&pubsub_ubus_listMutex);
    new_pubsub_ubus *tmp = g_pubsub_ubus;
    // while(tmp)
    // {
    //     printf("name = %s action_time = %d\n", tmp->name, tmp->action_time);
    //     tmp=tmp->next;
    // }
    // tmp = g_allubus;
    if(tmp)
    {
        while(tmp->next)
        {
            tmp=tmp->next;
        }
        tmp->next = node;
        list_count++;
    }
    else
    {
        g_pubsub_ubus = node;
        list_count++;
    }
    pthread_mutex_unlock(&pubsub_ubus_listMutex);
}

static void VR_(remove_list)(new_pubsub_ubus *node)
{
    pthread_mutex_lock(&pubsub_ubus_listMutex);
    new_pubsub_ubus *tmp = g_pubsub_ubus;
    new_pubsub_ubus *pre = g_pubsub_ubus;

    while(tmp)
    {
        if(tmp->action_time == node->action_time && tmp->req == node->req)
        {
            if(tmp == g_pubsub_ubus)
            {
                g_pubsub_ubus = tmp->next;
                free(tmp);
                list_count--;
            }
            else
            {
                pre->next = tmp->next;
                free(tmp);
                list_count--;
            }
            break;
        }
        pre=tmp;
        tmp=tmp->next;
    }
    pthread_mutex_unlock(&pubsub_ubus_listMutex);
}

static char* VR_(get_string_data)(const char* message, const char* keyname)
{
    /* convert json message to json object */
    char *result = NULL;
    json_object * jobj = NULL;
    jobj = VR_(create_json_object)((char*)message);

    if (!jobj)
    {
        return NULL;
    }

    /* scan json object */
    json_object_object_foreach(jobj, key, val)
    {
        if(!strcmp(key, keyname)) {
            result = strdup(json_object_get_string(val));
            json_object_put(jobj);
            return result;
        }
    }
    json_object_put(jobj);
    return NULL;
}

void VR_(Send_message)(const char *Topic, int qos, const char *msg, int retain)
{
    SLOGI("Topic %s msg = %s\n", Topic, msg);
    if(Topic && strlen(Topic))
    {
        int rc = mosquitto_publish(mosq, NULL, Topic, strlen(msg), (const void *)msg, qos, retain);
        //if(!rc)
        //{
            SLOGI("rc = %d\n", rc);
        //}
    }
    else
    {

        int rc = mosquitto_publish(mosq, NULL, PUBLISH_RES_TOPIC, strlen(msg), (const void *)msg, qos, retain);
        // if(!rc)
        // {
            SLOGI("rc = %d\n", rc);
        // }
    }
}

static void Broadcast_message(int qos, const char *msg, int retain)
{
    VR_(Send_message)(NULL, qos, msg, retain);
    sendMsgtoLocalService((char*)ST_SEND_NOTIFY, (char*)msg);
}

static void VR_(receive_data_cb)(struct ubus_request *req, int type, struct blob_attr *msg)
{
    const char *msgstr = "(unknown)";
    struct blob_attr *tb[__NOTIFY_MAX];
    blobmsg_parse(msg_policy, ARRAY_SIZE(msg_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
    {
        msgstr = blobmsg_data(tb[0]);
    }
    SLOGI("msgstr = %s\n", msgstr);
    new_pubsub_ubus *pubsub_ubus = ((new_pubsub_ubus *)(req->priv));

    if(pubsub_ubus)
    {
        SLOGI("req.priv = %x\n", req->priv);
        SLOGI("pubsub_ubus = %x\n", pubsub_ubus);

        SLOGI("pubsub_ubus.name = %s\n", pubsub_ubus->name);
        SLOGI("pubsub_ubus.type = %d\n", pubsub_ubus->type);
        SLOGI("pubsub_ubus.topic = %s\n", pubsub_ubus->topic);

        if(pubsub_ubus->name && strlen(pubsub_ubus->name))
        {
            //printf("cmd_support = %d\n", sizeof(cmd_support)/sizeof(char*));
            int i;
            for (i = 0; i < sizeof(cmd_support)/sizeof(char*); i++)
            {
                if(!strcmp(pubsub_ubus->name, cmd_support[i]))
                {
                    int j;
                    if(!strcmp(pubsub_ubus->name,ST_SET_BINARY))
                    {
                        VR_(set_led_state)(g_led_shm, ST_Done_secure_command);
                    }

                    for (j = 0; j < sizeof(daemon_services)/sizeof(daemon_service); j++)
                    {
                        if(pubsub_ubus->service_name && (!strcmp((char*)pubsub_ubus->service_name, daemon_services[j].name)))
                        {
                            daemon_services[j].timeout = 0;
                        }
                    }
                    
                    switch(pubsub_ubus->type)
                    {
                        case 0:
                        {
                            if(strstr(msgstr, ST_SUCCESSFUL))
                            {
                                Broadcast_message(VR_QOS, msgstr, 0);
                            }
                            else
                            {
                                VR_(Send_message)(NULL, VR_QOS, msgstr, 0);
                            }

                            if(!strstr(msgstr, "\"type\": \"upnp\""))
                            {
                                size_t length = strlen(msgstr)+1;
                                char *input_msg = (char*)malloc(length);
                                strlcpy(input_msg, msgstr, length);

                                pthread_t update_shadow_status_thread;
                                pthread_create(&update_shadow_status_thread, NULL, (void *)&update_shadow_status_cb, (void *)input_msg);
                                pthread_detach(update_shadow_status_thread);
                            }
                            break;
                        }
                        case 1:
                        {
                            VR_(Send_message)(pubsub_ubus->topic, VR_QOS, msgstr, 0);
                            break;
                        }
                        case 2:
                        {
                            
                            break;
                        }
                        default:
                            SLOGW("not support\n");
                            break;
                    }

                    break;
                }
            }
        }
        // new_allubus *tmp = g_allubus;
        // while(tmp)
        // {
        //     printf("name = %s\n", tmp->name);
        //     printf("action_time = %d\n\n", tmp->action_time);
        //     tmp = tmp->next;
        // }

        VR_(remove_list)(pubsub_ubus);
        //free(data);

        // tmp = g_allubus;
        // while(tmp)
        // {
        //     printf("name = %s\n", tmp->name);
        //     printf("action_time = %d\n\n", tmp->action_time);
        //     tmp = tmp->next;
        // }
    }
}

static int test_count=0;
static void VR_(complete_data_cb)(struct ubus_request *req, int ret)
{
    printf("free req\n");
    // if(req->priv)
    // {
    //     free(req->priv);
    // };
    if(req)
    {
        free(req);
        req = NULL;
    }
    
    test_count++;
    printf("%d\n",test_count);
    printf("%d\n",list_count);
    printf("free done\n");
}

/*############################# FOR LIST DEVICE ##############################*/
static int features_callback(void *status, int argc, char **argv, char **azColName)
{
    char *featureId = argv[0];
    char *registerId = argv[1];
    
    if(!strcmp(featureId, ST_WAKE_UP))
    {
        char *nodeId = argv[1];
        char *value = argv[2];

        char wakeUpInfo[SIZE_64B];
        snprintf(wakeUpInfo, sizeof(wakeUpInfo), "%s:%s", nodeId, value);

        json_object_object_add(status, featureId, json_object_new_string(wakeUpInfo));
    }
    else
    {
        json_object_object_add(status, featureId, json_object_new_string(registerId ? registerId : ""));
    }
    
    return 0;
}

static int capability_callback(void *data, int argc, char **argv, char **azColName)
{
    json_object *capability = (json_object *)data;

    if(!capability)
    {
        return 0;
    }

    int i;
    char *scheme = NULL;
    char *capList = NULL;
    char *endpointMem = NULL;
    for(i=0; i< argc; i++)
    {
        if(!strcmp(azColName[i], ST_SCHEME))
        {
            scheme = argv[i];
        }
        else if(!strcmp(azColName[i], ST_CAP_LIST))
        {
            capList = argv[i];
        }
        else if(!strcmp(azColName[i], ST_ENDPOINT_MEM))
        {
            endpointMem = argv[i];
        }
    }

    json_object *nonSecObj = json_object_new_object();
    JSON_ADD_STRING_SAFE(nonSecObj,ST_SCHEME,scheme);
    JSON_ADD_STRING_SAFE(nonSecObj,ST_CAP_LIST,capList);
    JSON_ADD_STRING_SAFE(nonSecObj,ST_CAP_LIST,endpointMem);
    json_object_array_add(capability, nonSecObj);

    return 0;
}

static int get_usercode_callback(void *userCode, int argc, char **argv, char **azColName)
{
    if(argc < 2)
    {
        return 0;
    }

    char *userId = argv[0];
    char *pass = argv[1];
    json_object *code = json_object_new_object();
    json_object_object_add(code, ST_USER_ID, json_object_new_string(userId ? userId : ""));
    json_object_object_add(code, ST_NAME, json_object_new_string(pass ? pass : ""));
    json_object_array_add(userCode, code);

    return 0;
}

static void get_door_lock_user_code(sqlite3 *dev_db, json_object *jobj, char *deviceId)
{
    if(!jobj || !deviceId)
    {
        return;
    }

    json_object *userCode = json_object_new_array();

    searching_database("pubsub_handler", dev_db, get_usercode_callback, (void *)userCode,
                        "SELECT %s,%s from USER_CODE where %s='%s'"
                        , ST_USER_ID, ST_PASS,
                        ST_DEVICE_ID, deviceId);

    int arraylen = json_object_array_length(userCode);
    if(arraylen > 0)
    {
        json_object_object_add(jobj, ST_USER_CODE, userCode);
    }
    else
    {
        json_object_put(userCode);
    }
}

static int get_property_callback(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 2)
    {
        SLOGE("less than 2\n");
        return 0;
    }

    json_object *property = (json_object*)data;
    if(!property)
    {
        SLOGE("property is NULL\n");
        return 0;
    }

    char *key = argv[0];
    char *value = argv[1];
    
    json_object_object_add(property, key, json_object_new_string(value ? value : ""));
    return 0;
}

static int get_scene_callback(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 3)
    {
        SLOGE("less than 3\n");
        return 0;
    }

    json_object *scene = (json_object*)data;
    if(!scene)
    {
        SLOGE("scene is NULL\n");
        return 0;
    }

    char *buttonId = argv[0];
    char *sceneId = argv[1];
    char *timeActive = argv[2];
    
    json_object_object_add(scene, ST_BUTTON_ID, json_object_new_string(buttonId ? buttonId : ""));
    json_object_object_add(scene, ST_SCENE_ID, json_object_new_string(sceneId ? sceneId : ""));
    json_object_object_add(scene, ST_TIME_ACTIVE, json_object_new_string(timeActive ? timeActive : ""));
    return 0;
}

static int get_association_callback(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 3)
    {
        return 0;
    }

    if(!data)
    {
        return 0;
    }

    status_data *tmp = (status_data*)data;

    char *deviceId = argv[0];
    char *groupId = argv[1];
    char *nodeList = argv[2];
    json_object *asso = json_object_new_object();
    json_object_object_add(asso, ST_GROUP_ID, json_object_new_string(groupId ? groupId : ""));
    json_object_object_add(asso, ST_NODE_LIST, json_object_new_string(nodeList ? nodeList : ""));

    searching_database("pubsub_handler", (sqlite3 *)tmp->db, get_property_callback, (void *)asso,
                        "SELECT key,value from MORE_PROPERTY where %s='%s' AND %s='%s'"
                        " AND %s='%s'", ST_DEVICE_ID, deviceId, ST_DEVICE_TYPE, ST_ASSOCIATION, 
                        ST_PROPERTY_ID, groupId);

    searching_database("pubsub_handler", (sqlite3 *)tmp->db, get_scene_callback, (void *)asso,
                        "SELECT buttonId,sceneId,timeActive from SCENES where %s='%s' AND %s='%s'", 
                        ST_DEVICE_ID, deviceId, ST_GROUP_ID, groupId);
    // SLOGI("asso = %s\n", json_object_to_json_string(asso));
    json_object_array_add(tmp->object, asso);

    return 0;
}

static void get_association_data(sqlite3 *dev_db, json_object *jobj, char *deviceId)
{
    if(!jobj)
    {
        return;
    }

    json_object *association = json_object_new_array();

    status_data data;
    data.object = (void*)association;
    data.db = (void*)dev_db;

    searching_database("pubsub_handler", dev_db, get_association_callback, (void *)&data,
                        "SELECT %s,%s,%s from ASSOCIATION_DATA where %s='%s'"
                        , ST_DEVICE_ID, ST_GROUP_ID, ST_NODE_LIST,
                        ST_DEVICE_ID, deviceId);

    int arraylen = json_object_array_length(association);
    if(arraylen > 0)
    {
        json_object_object_add(jobj, ST_ASSOCIATION, association);
    }
    else
    {
        json_object_put(association);
    }
}

static int get_meter_callback(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 4)
    {
        return 0;
    }

    if(!data)
    {
        return 0;
    }

    status_data *tmp = (status_data*)data;
    char *deviceId = argv[0];
    char *meterType = argv[1];
    char *meterValue = argv[2];
    char *meterUnit = argv[3];
    json_object *meter = json_object_new_object();
    json_object_object_add(meter, ST_METER_TYPE, json_object_new_string(meterType ? meterType : ""));
    json_object_object_add(meter, ST_METER_VALUE, json_object_new_string(meterValue ? meterValue : ""));
    json_object_object_add(meter, ST_METER_UNIT, json_object_new_string(meterUnit ? meterUnit : ""));

    searching_database("pubsub_handler", (sqlite3 *)tmp->db, get_property_callback, (void *)meter,
                        "SELECT key,value from MORE_PROPERTY where %s='%s' AND %s='%s'"
                        " AND %s='%s'", ST_DEVICE_ID, deviceId, ST_DEVICE_TYPE, ST_METER, 
                        ST_PROPERTY_ID, meterType);
    json_object_array_add(tmp->object, meter);

    return 0;
}

static void get_meter_data(sqlite3 *dev_db, json_object *jobj, char *deviceId)
{
    if(!jobj)
    {
        return;
    }

    json_object *meter = json_object_new_array();

    status_data data;
    data.object = (void*)meter;
    data.db = (void*)dev_db;

    searching_database("pubsub_handler", dev_db, get_meter_callback, (void *)&data,
                        "SELECT %s,%s,%s,%s from METER_DATA where %s='%s'"
                        , ST_DEVICE_ID, ST_METER_TYPE, ST_METER_VALUE, ST_METER_UNIT,
                        ST_DEVICE_ID, deviceId);

    int arraylen = json_object_array_length(meter);
    if(arraylen > 0)
    {
        json_object_object_add(jobj, ST_METER, meter);
    }
    else
    {
        json_object_put(meter);
    }
}

static int get_sensor_multilevel_callback(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 4)
    {
        return 0;
    }

    if(!data)
    {
        return 0;
    }

    status_data *tmp = (status_data*)data;

    char *deviceId = argv[0];
    char *sensorType = argv[1];
    char *sensorValue = argv[2];
    char *sensorUnit = argv[3];
    json_object *sensor = json_object_new_object();
    json_object_object_add(sensor, ST_SENSOR_TYPE, json_object_new_string(sensorType ? sensorType : ""));
    json_object_object_add(sensor, ST_VALUE, json_object_new_string(sensorValue ? sensorValue : ""));
    json_object_object_add(sensor, ST_UNIT, json_object_new_string(sensorUnit ? sensorUnit : ""));

    searching_database("pubsub_handler", (sqlite3 *)tmp->db, get_property_callback, (void *)sensor,
                        "SELECT key,value from MORE_PROPERTY where %s='%s' AND %s='%s'"
                        " AND %s='%s'", ST_DEVICE_ID, deviceId, ST_DEVICE_TYPE, ST_SENSOR_MULTILEVEL, 
                        ST_PROPERTY_ID, sensorType);
    json_object_array_add(tmp->object, sensor);

    return 0;
}

static void get_sensor_multilevel_data(sqlite3 *dev_db, json_object *jobj, char *deviceId)
{
    if(!jobj)
    {
        return;
    }

    json_object *sensorMultilevel = json_object_new_array();

    status_data data;
    data.object = (void*)sensorMultilevel;
    data.db = (void*)dev_db;

    searching_database("pubsub_handler", dev_db, get_sensor_multilevel_callback, (void *)&data,
                        "SELECT %s,%s,%s,%s from SENSOR_DATA where %s='%s'"
                        , ST_DEVICE_ID, ST_SENSOR_TYPE, ST_SENSOR_VALUE, ST_SENSOR_UNIT,
                        ST_DEVICE_ID, deviceId);

    int arraylen = json_object_array_length(sensorMultilevel);
    if(arraylen > 0)
    {
        json_object_object_add(jobj, ST_SENSOR_MULTILEVEL, sensorMultilevel);
    }
    else
    {
        json_object_put(sensorMultilevel);
    }
}

static int get_thermostat_setpoint_callback(void *setpointArray, int argc, char **argv, char **azColName)
{
    if(argc < 3)
    {
        return 0;
    }

    char *mode = argv[0];
    char *value = argv[1];
    char *unit = argv[2];
    json_object *setpoint = json_object_new_object();
    json_object_object_add(setpoint, ST_MODE, json_object_new_string(mode ? mode : ""));
    json_object_object_add(setpoint, ST_VALUE, json_object_new_string(value ? value : ""));
    json_object_object_add(setpoint, ST_UNIT, json_object_new_string(unit ? unit : ""));

    json_object_array_add(setpointArray, setpoint);

    return 0;
}

static void get_thermostat_setpoint_data(sqlite3 *dev_db, json_object *jobj, char *deviceId)
{
    if(!jobj)
    {
        return;
    }

    json_object *thermostatSetpoint = json_object_new_array();

    searching_database("pubsub_handler", dev_db, get_thermostat_setpoint_callback, (void *)thermostatSetpoint,
                        "SELECT %s,%s,%s from THERMOSTAT_SETPOINT where %s='%s'"
                        , ST_MODE, ST_VALUE, ST_UNIT,
                        ST_DEVICE_ID, deviceId);

    int arraylen = json_object_array_length(thermostatSetpoint);
    if(arraylen > 0)
    {
        json_object_object_add(jobj, ST_THERMOSTAT_SETPOINT, thermostatSetpoint);
    }
    else
    {
        json_object_put(thermostatSetpoint);
    }
}

static int get_alarm_callback(void *alarm, int argc, char **argv, char **azColName)
{
    if(argc < 1)
    {
        return 0;
    }

    char *alarmTypeFinal = argv[0];
    json_object_array_add(alarm, json_object_new_string(alarmTypeFinal ? alarmTypeFinal : ""));
    return 0;
}

static void get_alarm_data(sqlite3 *dev_db, json_object *jobj, char *deviceId)
{
    if(!jobj)
    {
        return;
    }

    json_object *alarm = json_object_new_array();
    searching_database("pubsub_handler", dev_db, get_alarm_callback, (void *)alarm,
                        "SELECT %s from ALARM where %s='%s'"
                        ,ST_ALARM_TYPE_FINAL, ST_DEVICE_ID, deviceId);
    int arraylen = json_object_array_length(alarm);

    if (arraylen > 0)
    {
        json_object_object_add(jobj, ST_ALARM_STATUS, alarm);
    }
    else
    {
        json_object_put(alarm);
    }
}

static int listdevice_callback(void *data, int argc, char **argv, char **azColName)
{
    if(!data)
    {
        return 0;
    }

    int i;
    json_object *jobj = json_object_new_object();
    char deviceId[SIZE_64B];
    char deviceType[SIZE_64B];

    status_data *tmp = (status_data*)data;

    for(i=0; i<argc; i++)
    {
        if(!strcmp(azColName[i], ST_ID) || !strcmp(azColName[i], ST_UDN))
        {
            SAFE_STRCPY(deviceId, argv[i]);
        }

        if(!strcmp(azColName[i], ST_TYPE))
        {
            SAFE_STRCPY(deviceType, argv[i]);
        }

        if(!strcmp(azColName[i], ST_PARENT_ID))
        {
            json_object_object_add(jobj, azColName[i], json_object_new_string(argv[i] ? argv[i] : "0"));
        }
        else
        {
            json_object_object_add(jobj, azColName[i], json_object_new_string(argv[i] ? argv[i] : ""));
        }
    }

    SEARCH_DATA_INIT_VAR(resourceId);
    searching_database("pubsub_handler", (sqlite3 *)tmp->db, get_last_data_cb, &resourceId,
                    "SELECT owner from DEVICES where deviceId='%s'", 
                    deviceId);
    if(resourceId.len)
    {
        json_object_object_add(jobj, ST_CLOUD_RESOURCE, json_object_new_string(resourceId.value));
    }

    FREE_SEARCH_DATA_VAR(resourceId);

    json_object *capabilityObj = json_object_new_array();

    searching_database("pubsub_handler", (sqlite3 *)tmp->db, capability_callback, (void *)capabilityObj,
                    "SELECT %s,%s,%s from CAPABILITY where deviceId='%s'", 
                    ST_SCHEME, ST_CAP_LIST, ST_ENDPOINT_MEM, deviceId);

    json_object_object_add(jobj, ST_CAPABILITY, capabilityObj);
    json_object *status = json_object_new_object();

    searching_database("pubsub_handler", (sqlite3 *)tmp->db, features_callback, (void *)status,
                        "SELECT %s,%s from FEATURES where %s='%s'"
                        , ST_FEATURE_ID, ST_REGISTER,
                        ST_DEVICE_ID, deviceId);

    get_alarm_data((sqlite3 *)tmp->db, status, deviceId);

    int association=0, meter=0, sensorMultilevel=0, userCode=0, thermostatSetpoint=0;
    get_feature_device_support(capabilityObj, deviceType, &association,
                      &meter, &sensorMultilevel, &userCode, &thermostatSetpoint);

    if(association)
    {
        get_association_data((sqlite3 *)tmp->db, status, deviceId);
    }

    if(meter)
    {
        get_meter_data((sqlite3 *)tmp->db, status, deviceId);
    }

    if(sensorMultilevel)
    {
        get_sensor_multilevel_data((sqlite3 *)tmp->db, status, deviceId);
    }

    if(userCode)
    {
        get_door_lock_user_code((sqlite3 *)tmp->db, status, deviceId);
    }

    if(thermostatSetpoint)
    {
        get_thermostat_setpoint_data((sqlite3 *)tmp->db, status, deviceId);
    }

    json_object_object_add(jobj, ST_STATUS, status);
    json_object_array_add(tmp->object, jobj);

    return 0;
}

static void VR_(list_device)(const char *service_type, const char *msg)
{

    SLOGI("The command %s is called \n ", __func__);

    char *topic = VR_(get_string_data)(msg, ST_TOPIC);

    if(!strcmp(service_type, ST_ALL))
    {
        char timestamp[SIZE_32B];
        unsigned int current_time = (unsigned)time(NULL);
        sprintf(timestamp, "%u", current_time);

        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ALL));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_LIST_DEVICES_R));
        json_object_object_add(jobj, ST_CLOUD_TIME_STAMP, json_object_new_string(timestamp));
        json_object *devicelist = json_object_new_array();

        char cmd[SIZE_256B];
        char database_file[SIZE_256B];
        snprintf(database_file, sizeof(database_file), TEMP_DEVICES_DATABASE, current_time);
        snprintf(cmd, sizeof(cmd), "cp %s %s", DEVICES_DATABASE, database_file);
        VR_(execute_system)(cmd);

        sqlite3 *tmp_dev_db;

        int rc = VR_(get_file_infor)(database_file);

        if(rc == -1)
        {
            tmp_dev_db = dev_db;
        }
        else
        {
            rc = open_database(database_file, &tmp_dev_db);
            if(rc == -1)
            {
                tmp_dev_db = dev_db;
            }
        }

        status_data data;
        data.object = (void*)devicelist;
        data.db = (void*)tmp_dev_db;

        DISABLE_DATABASE_LOG();

        searching_database("pubsub_handler", dev_db, listdevice_callback, (void *)&data,
                        "SELECT * from CONTROL_DEVS where type='%s'", ST_UPNP);

        searching_database("pubsub_handler", dev_db, listdevice_callback, (char *)&data,
                    "SELECT %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s from SUB_DEVICES ",
                    ST_OWNER, ST_SERIAL_ID, ST_DEVICE_TYPE, ST_TYPE_BASIC, ST_TYPE_SPECIFIC, ST_DEVICE_MODE,
                    ST_FRIENDLY_NAME, ST_ID, ST_ENDPOINT_NUM, ST_PARENT_ID, ST_IS_ZWAVE_PLUS,
                    ST_ROLE_TYPE, ST_NODE_TYPE, ST_TYPE, ST_OWNER);

        ENABLE_DATABASE_LOG();

        if(rc != -1)
        {
            close_database(&tmp_dev_db);
        }

        sprintf(cmd, "rm %s", database_file);
        VR_(execute_system)(cmd);

        json_object_object_add(jobj, ST_DEVICES_LIST, devicelist);

        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
    }
    else
    {
        new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
        memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));
        SAFE_STRCPY(pubsub_ubus->name, ST_LIST_DEVICES);
        SLOGI("topic = %s\n", topic);
        if(topic)
        {
            SAFE_STRCPY(pubsub_ubus->topic, topic);
        }

        VR_(add_last_list)(pubsub_ubus);

        int i;
        pubsub_ubus->type = 1;
        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            SLOGI("daemon_services[%d].name = %s id = %08X\n", i, daemon_services[i].name, daemon_services[i].id);
            if(!strcmp(service_type, daemon_services[i].name))
            {
                pubsub_ubus->service_name = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {
                    pubsub_ubus->action_time = (unsigned)time(NULL);

                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    pubsub_ubus->req=req;
                    
                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_LIST_DEVICES, NULL, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                    break;
                }
            }
        }
    }

    SAFE_FREE(topic);
}

static void _set_binary(char *service, char *id, char *cmd, 
                char *value, char *subdevId, char *topic)
{
    if(!service || !id)
    {
        return;
    }

    if(!strcmp(service, ST_VENUS_SERVICE))
    {   
        if(!strcmp(id, ST_LOCATION))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SET_BINARY_R));
            json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_VENUS_SERVICE));

            char *latitude = VR_(read_option)(NULL, UCI_LOCATION_LATITUDE);
            char *longitude = VR_(read_option)(NULL, UCI_LOCATION_LONGITUDE);
            if(!latitude && !longitude)
            {
                uci_do_add("security", ST_LOCATION);
                VR_(write_option)(NULL, UCI_LOCATION_LATITUDE"=%s", value);
                VR_(write_option)(NULL, UCI_LOCATION_LONGITUDE"=%s", subdevId);
            }
            else
            {
                VR_(write_option)(NULL, UCI_LOCATION_LATITUDE"=%s", value);
                VR_(write_option)(NULL, UCI_LOCATION_LONGITUDE"=%s", subdevId);
                free(latitude);
                free(longitude);
            }

            json_object_object_add(jobj, ST_LATITUDE, json_object_new_string(value));
            json_object_object_add(jobj, ST_LONGTITUDE, json_object_new_string(subdevId));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

            Broadcast_message(VR_QOS, json_object_to_json_string(jobj), 0);
            json_object_put(jobj);

            return;
        }
    }

    if(!cmd || !value)
    {
        return;
    }

    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));
    SAFE_STRCPY(pubsub_ubus->name, ST_SET_BINARY);
    if(topic)
    {
        SAFE_STRCPY(pubsub_ubus->topic, topic);
    }

    VR_(add_last_list)(pubsub_ubus);

    int i;
    pubsub_ubus->type = 0;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        SLOGI("daemon_services[%d].name = %s id = %08X\n", i, daemon_services[i].name, daemon_services[i].id);
        if(!strcmp(service, daemon_services[i].name))
        {
            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                blobmsg_add_string(&buff, ST_CMD, cmd);
                blobmsg_add_string(&buff, ST_VALUE, value);
                if(subdevId && strlen(subdevId)) 
                {
                    if(!strcmp(service, ST_UPNP))
                    {
                        blobmsg_add_string(&buff, ST_SUBDEVID, subdevId);
                    }
                }

                pubsub_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_SET_BINARY, buff.head, req,
                                    VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                break;
            }
        }
    }
}

static void VR_(set_binary)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    char* id = VR_(get_string_data)(msg, ST_ID);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);
    char* value = VR_(get_string_data)(msg, ST_VALUE);
    char* cmd = VR_(get_string_data)(msg, ST_COMMAND);
    char* subdevID = VR_(get_string_data)(msg, ST_SUBDEVID);

    _set_binary((char*)service_type, id, cmd, value, subdevID, topic);

    SAFE_FREE(id);
    SAFE_FREE(cmd);
    SAFE_FREE(topic);
    SAFE_FREE(value);
    SAFE_FREE(subdevID);
}

static void VR_(get_binary)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    char* id = VR_(get_string_data)(msg, ST_ID);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);
    char* subdevID = VR_(get_string_data)(msg, ST_SUBDEVID);

    if(service_type && !strcasecmp(service_type, ST_VENUS_SERVICE))
    {
        if(!strcasecmp(id, ST_LOCATION))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_BINARY_R));
            json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_VENUS_SERVICE));
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ST_LOCATION));
            
            char *latitude = VR_(read_option)(NULL, UCI_LOCATION_LATITUDE);
            char *longitude = VR_(read_option)(NULL, UCI_LOCATION_LONGITUDE);

            if(latitude && longitude)
            {
                json_object_object_add(jobj, ST_LATITUDE, json_object_new_string(latitude));
                json_object_object_add(jobj, ST_LONGTITUDE, json_object_new_string(longitude));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }
            else
            {
                if(latitude)
                    free(latitude);
                if(longitude)
                    free(longitude);
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            }
            VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
            return;
        }
    }

    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));
    SAFE_STRCPY(pubsub_ubus->name, ST_GET_BINARY);
    SLOGI("topic = %s\n", topic);
    if(topic)
    {
        SAFE_STRCPY(pubsub_ubus->topic, topic);
    }

    VR_(add_last_list)(pubsub_ubus);

    int i;
    pubsub_ubus->type = 1;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        SLOGI("daemon_services[%d].name = %s id = %08X\n", i, daemon_services[i].name, daemon_services[i].id);
        if(!strcmp(service_type, daemon_services[i].name))
        {
            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                if(subdevID && strlen(subdevID)) 
                {
                    if(!strcmp(service_type, ST_UPNP))
                    {
                        blobmsg_add_string(&buff, ST_SUBDEVID, subdevID);
                    }
                }

                pubsub_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_GET_BINARY, buff.head, req,
                                    VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                break;
            }
        }
    }

    SAFE_FREE(id);
    SAFE_FREE(topic);
    SAFE_FREE(subdevID);
}

static void VR_(change_name)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    char* id = VR_(get_string_data)(msg, ST_ID);
    char* value = VR_(get_string_data)(msg, ST_VALUE);
    char* subdevID = VR_(get_string_data)(msg, ST_SUBDEVID);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));
    SAFE_STRCPY(pubsub_ubus->name, ST_CHANGE_NAME);
    SLOGI("topic = %s\n", topic);
    if(topic)
    {
        SAFE_STRCPY(pubsub_ubus->topic, topic);
    }

    VR_(add_last_list)(pubsub_ubus);

    int i;
    pubsub_ubus->type = 0;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        SLOGI("daemon_services[%d].name = %s id = %08X\n", i, daemon_services[i].name, daemon_services[i].id);
        if(!strcmp(service_type, daemon_services[i].name))
        {
            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                if(value && strlen(value)) 
                {
                    blobmsg_add_string(&buff, ST_VALUE, value);
                }
                
                if(subdevID && strlen(subdevID)) 
                {
                    if(!strcmp(service_type, ST_UPNP))
                    {
                        blobmsg_add_string(&buff, ST_SUBDEVID, subdevID);
                    }
                }

                pubsub_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_CHANGE_NAME, buff.head, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);

                break;
            }
        }
    }

    SAFE_FREE(id);
    SAFE_FREE(value);
    SAFE_FREE(subdevID);
    SAFE_FREE(topic);
}

static void VR_(get_name)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    char* id = VR_(get_string_data)(msg, ST_ID);
    char* subdevID = VR_(get_string_data)(msg, ST_SUBDEVID);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));
    SAFE_STRCPY(pubsub_ubus->name, ST_GET_NAME);
    SLOGI("topic = %s\n", topic);
    if(topic)
    {
        SAFE_STRCPY(pubsub_ubus->topic, topic);
    }

    VR_(add_last_list)(pubsub_ubus);

    int i;
    pubsub_ubus->type = 0;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        SLOGI("daemon_services[%d].name = %s id = %08X\n", i, daemon_services[i].name, daemon_services[i].id);
        if(!strcmp(ST_UPNP, daemon_services[i].name))
        {
            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                
                if(subdevID && strlen(subdevID)) 
                {
                    if(!strcmp(service_type, ST_UPNP))
                    {
                        blobmsg_add_string(&buff, ST_SUBDEVID, subdevID);
                    }
                }

                pubsub_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_GET_NAME, buff.head, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                break;
            }
        }
    }

    SAFE_FREE(id);
    SAFE_FREE(subdevID);
    SAFE_FREE(topic);
}

static void VR_(reset)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    char* adminId = VR_(get_string_data)(msg, ST_ADMIN_ID);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    if(!strcmp(service_type, ST_UPNP))
    {
        new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
        memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));

        SAFE_STRCPY(pubsub_ubus->name, ST_RESET);
        if(topic)
        {
            SAFE_STRCPY(pubsub_ubus->topic, topic);
        }
        pubsub_ubus->type = 1;

        VR_(add_last_list)(pubsub_ubus);
        int i;
        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(service_type, daemon_services[i].name))
            {
                pubsub_ubus->service_name = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {
                    pubsub_ubus->action_time = (unsigned)time(NULL);

                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    pubsub_ubus->req=req;

                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_RESET, NULL, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                    break;
                }
            }
        }
    }
    else
    {
        SEARCH_DATA_INIT_VAR(usertype);
        searching_database("pubsub_handler", user_db, get_last_data_cb, &usertype, 
                    "SELECT userType from USERS where userId='%s'", adminId);

        if(!strcmp(usertype.value, ST_ADMIN))
        {
            if (!strcmp(service_type, ST_HARD))
            {
                SLOGI("factory reset\n");
                system("rm -rf /overlay/*");
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RESET_R));
                json_object_object_add(jobj, ST_RESET_TYPE, json_object_new_string(service_type));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
                json_object_put(jobj);
                system("reboot");
            }
            else if(!strcmp(service_type, ST_REBOOT))
            {
                SLOGI("reboot\n");
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RESET_R));
                json_object_object_add(jobj, ST_RESET_TYPE, json_object_new_string(service_type));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
                json_object_put(jobj);
                VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
                system("reboot");
            }
            else if(!strcmp(service_type, ST_WIFI_SETTINGS))
            {
                SLOGI("wifi_settings\n");
                VR_(execute_system)("rm -rf /overlay/etc/config/alljoyn-onboarding");
                VR_(execute_system)("rm -rf /overlay/etc/config/wireless");
                VR_(execute_system)("rm -rf /overlay/etc/alljoyn-onboarding");
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RESET_R));
                json_object_object_add(jobj, ST_RESET_TYPE, json_object_new_string(service_type));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
                json_object_put(jobj);
                VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
                system("reboot");
            }
        }
        else
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RESET_R));
            json_object_object_add(jobj, ST_RESET_TYPE, json_object_new_string(service_type));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("you are not allowed to do this"));
            VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
        }
        FREE_SEARCH_DATA_VAR(usertype);
    }

    SAFE_FREE(adminId);
    SAFE_FREE(topic);
}

static void VR_(read_spec)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    char* id = VR_(get_string_data)(msg, ST_ID);
    char* class = VR_(get_string_data)(msg, ST_CLASS);
    char* command = VR_(get_string_data)(msg, ST_COMMAND);
    char* data0 = VR_(get_string_data)(msg, ST_DATA0);
    char* data1 = VR_(get_string_data)(msg, ST_DATA1);
    char* data2 = VR_(get_string_data)(msg, ST_DATA2);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));

    SAFE_STRCPY(pubsub_ubus->name, ST_READ_SPEC);
    if(topic)
    {
        SAFE_STRCPY(pubsub_ubus->topic, topic);
    }
    pubsub_ubus->type = 1;

    VR_(add_last_list)(pubsub_ubus);
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(service_type, daemon_services[i].name))
        {
            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                blobmsg_add_string(&buff, ST_CLASS, class);
                blobmsg_add_string(&buff, ST_CMD, command);
                if(data0 && strlen(data0)) blobmsg_add_string(&buff, ST_DATA0, data0);
                if(data1 && strlen(data1)) blobmsg_add_string(&buff, ST_DATA1, data1);
                if(data2 && strlen(data2)) blobmsg_add_string(&buff, ST_DATA2, data2);

                pubsub_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_READ_SPEC, buff.head, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                break;
            }
        }
    }

    SAFE_FREE(id);
    SAFE_FREE(class);
    SAFE_FREE(command);
    SAFE_FREE(data0);
    SAFE_FREE(data1);
    SAFE_FREE(data2);
    SAFE_FREE(topic);
}

static void _write_spec(char *service, char *id, char *class, char *command, 
                    char *data0, char *data1, char *data2, char *topic)
{
    if(!service || !id || !class || !command)
    {
        return;
    }

    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));

    SAFE_STRCPY(pubsub_ubus->name, ST_WRITE_SPEC);
    if(topic)
    {
        SAFE_STRCPY(pubsub_ubus->topic, topic);
    }
    pubsub_ubus->type = 0;

    VR_(add_last_list)(pubsub_ubus);
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(service, daemon_services[i].name))
        {
            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                blobmsg_add_string(&buff, ST_CLASS, class);
                blobmsg_add_string(&buff, ST_CMD, command);
                if(data0 && strlen(data0)) blobmsg_add_string(&buff, ST_DATA0, data0);
                if(data1 && strlen(data1)) blobmsg_add_string(&buff, ST_DATA1, data1);
                if(data2 && strlen(data2)) blobmsg_add_string(&buff, ST_DATA2, data2);

                pubsub_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_WRITE_SPEC, buff.head, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                break;
            }
        }
    }
}

static void VR_(write_spec)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    char* id = VR_(get_string_data)(msg, ST_ID);
    char* class = VR_(get_string_data)(msg, ST_CLASS);
    char* command = VR_(get_string_data)(msg, ST_COMMAND);
    char* data0 = VR_(get_string_data)(msg, ST_DATA0);
    char* data1 = VR_(get_string_data)(msg, ST_DATA1);
    char* data2 = VR_(get_string_data)(msg, ST_DATA2);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    _write_spec((char*)service_type, id, class, command, data0, data1, data2, topic);

    SAFE_FREE(id);
    SAFE_FREE(class);
    SAFE_FREE(command);
    SAFE_FREE(data0);
    SAFE_FREE(data1);
    SAFE_FREE(data2);
    SAFE_FREE(topic);
}

static void _write_spec_crc(char *service, char *id, char *class, char *command, 
                        char *data0, char *data1, char *data2, char *srcEndPoint,
                        char *desEndPoint, char *isCRC, char *topic)
{
    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));

    SAFE_STRCPY(pubsub_ubus->name, ST_WRITE_SPEC_CRC);
    if(topic)
    {
        SAFE_STRCPY(pubsub_ubus->topic, topic);
    }
    pubsub_ubus->type = 0;

    VR_(add_last_list)(pubsub_ubus);
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(service, daemon_services[i].name))
        {
            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                blobmsg_add_string(&buff, ST_CLASS, class);
                blobmsg_add_string(&buff, ST_CMD, command);
                if(data0 && strlen(data0)) blobmsg_add_string(&buff, ST_DATA0, data0);
                if(data1 && strlen(data1)) blobmsg_add_string(&buff, ST_DATA1, data1);
                if(data2 && strlen(data2)) blobmsg_add_string(&buff, ST_DATA2, data2);

                if(srcEndPoint && strlen(srcEndPoint)) 
                    blobmsg_add_string(&buff, ST_SRC_ENDPOINT, srcEndPoint);
                if(desEndPoint && strlen(desEndPoint)) 
                    blobmsg_add_string(&buff, ST_DES_ENDPOINT, desEndPoint);
                if(isCRC && strlen(isCRC)) blobmsg_add_string(&buff, ST_IS_CRC, isCRC);

                pubsub_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_WRITE_SPEC_CRC, buff.head, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                break;
            }
        }
    }
}

static void VR_(write_spec_crc)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    char* id = VR_(get_string_data)(msg, ST_ID);
    char* class = VR_(get_string_data)(msg, ST_CLASS);
    char* command = VR_(get_string_data)(msg, ST_COMMAND);
    char* data0 = VR_(get_string_data)(msg, ST_DATA0);
    char* data1 = VR_(get_string_data)(msg, ST_DATA1);
    char* data2 = VR_(get_string_data)(msg, ST_DATA2);
    char* srcEndPoint = VR_(get_string_data)(msg, ST_DATA3);
    char* desEndPoint = VR_(get_string_data)(msg, ST_DATA4);
    char* isCRC = VR_(get_string_data)(msg, ST_DATA5);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    _write_spec_crc((char*)service_type, id, class, command, data0, data1, data2, 
                    srcEndPoint, desEndPoint, isCRC, topic);

    SAFE_FREE(id);
    SAFE_FREE(class);
    SAFE_FREE(command);
    SAFE_FREE(data0);
    SAFE_FREE(data1);
    SAFE_FREE(data2);
    SAFE_FREE(srcEndPoint);
    SAFE_FREE(desEndPoint);
    SAFE_FREE(isCRC);
    SAFE_FREE(topic);
}

static void VR_(network)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    if(!service_type)
    {
        return;
    }
    char* id = VR_(get_string_data)(msg, ST_ID);
    char* class = VR_(get_string_data)(msg, ST_CLASS);
    char* command = VR_(get_string_data)(msg, ST_COMMAND);
    char* data0 = VR_(get_string_data)(msg, ST_DATA0);
    char* data1 = VR_(get_string_data)(msg, ST_DATA1);
    char* data2 = VR_(get_string_data)(msg, ST_DATA2);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);
    
    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));

    SAFE_STRCPY(pubsub_ubus->name, ST_NETWORK);
    if(topic)
    {
        SAFE_STRCPY(pubsub_ubus->topic, topic);
    }
    pubsub_ubus->type = 1;

    VR_(add_last_list)(pubsub_ubus);
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(service_type, daemon_services[i].name))
        {
            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                blobmsg_add_string(&buff, ST_CLASS, class);
                blobmsg_add_string(&buff, ST_CMD, command);
                if(data0 && strlen(data0)) blobmsg_add_string(&buff, ST_DATA0, data0);
                if(data1 && strlen(data1)) blobmsg_add_string(&buff, ST_DATA1, data1);
                if(data2 && strlen(data2)) blobmsg_add_string(&buff, ST_DATA2, data2);

                pubsub_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_NETWORK, buff.head, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                break;
            }
        }
    }

    SAFE_FREE(id);
    SAFE_FREE(class);
    SAFE_FREE(command);
    SAFE_FREE(data0);
    SAFE_FREE(data1);
    SAFE_FREE(data2);
    SAFE_FREE(topic);
}

static void VR_(read_s_spec)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    char* id = VR_(get_string_data)(msg, ST_ID);
    char* class = VR_(get_string_data)(msg, ST_CLASS);
    char* command = VR_(get_string_data)(msg, ST_COMMAND);
    char* data0 = VR_(get_string_data)(msg, ST_DATA0);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));

    SAFE_STRCPY(pubsub_ubus->name, ST_READ_S_SPEC);
    if(topic)
    {
        SAFE_STRCPY(pubsub_ubus->topic, topic);
    }
    pubsub_ubus->type = 1;

    VR_(add_last_list)(pubsub_ubus);
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(service_type, daemon_services[i].name))
        {
            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                blobmsg_add_string(&buff, ST_CLASS, class);
                blobmsg_add_string(&buff, ST_CMD, command);
                if(data0 && strlen(data0)) blobmsg_add_string(&buff, ST_DATA0, data0);

                pubsub_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_READ_S_SPEC, buff.head, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                break;
            }
        }
    }

    SAFE_FREE(id);
    SAFE_FREE(class);
    SAFE_FREE(command);
    SAFE_FREE(data0);
    SAFE_FREE(topic);
}

static void VR_(write_s_spec)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    char* id = VR_(get_string_data)(msg, ST_ID);
    char* class = VR_(get_string_data)(msg, ST_CLASS);
    char* command = VR_(get_string_data)(msg, ST_COMMAND);
    char* data0 = VR_(get_string_data)(msg, ST_DATA0);
    char* data1 = VR_(get_string_data)(msg, ST_DATA1);
    char* data2 = VR_(get_string_data)(msg, ST_DATA2);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));

    SAFE_STRCPY(pubsub_ubus->name, ST_WRITE_S_SPEC);
    if(topic)
    {
        SAFE_STRCPY(pubsub_ubus->topic, topic);
    }
    pubsub_ubus->type = 0;

    VR_(add_last_list)(pubsub_ubus);
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(service_type, daemon_services[i].name))
        {
            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                blobmsg_add_string(&buff, ST_CLASS, class);
                blobmsg_add_string(&buff, ST_CMD, command);
                if(data0 && strlen(data0)) blobmsg_add_string(&buff, ST_DATA0, data0);
                if(data1 && strlen(data1)) blobmsg_add_string(&buff, ST_DATA1, data1);
                if(data2 && strlen(data2)) blobmsg_add_string(&buff, ST_DATA2, data2);

                pubsub_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_WRITE_S_SPEC, buff.head, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                break;
            }
        }
    }

    SAFE_FREE(id);
    SAFE_FREE(class);
    SAFE_FREE(command);
    SAFE_FREE(data0);
    SAFE_FREE(data1);
    SAFE_FREE(data2);
    SAFE_FREE(topic);
}

static void VR_(identify)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    char* id = VR_(get_string_data)(msg, ST_ID);
    char* value = VR_(get_string_data)(msg, ST_VALUE);
    char* subdevID = VR_(get_string_data)(msg, ST_SUBDEVID);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));
    SAFE_STRCPY(pubsub_ubus->name, ST_IDENTIFY);
    SLOGI("topic = %s\n", topic);
    if(topic)
    {
        SAFE_STRCPY(pubsub_ubus->topic, topic);
    }

    VR_(add_last_list)(pubsub_ubus);

    int i;
    pubsub_ubus->type = 0;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        SLOGI("daemon_services[%d].name = %s id = %08X\n", i, daemon_services[i].name, daemon_services[i].id);
        if(!strcmp(service_type, daemon_services[i].name))
        {
            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                if(value && strlen(value)) 
                {
                    blobmsg_add_string(&buff, ST_VALUE, value);
                }
                
                if(subdevID && strlen(subdevID)) 
                {
                    if(!strcmp(service_type, ST_UPNP))
                    {
                        blobmsg_add_string(&buff, ST_SUBDEVID, subdevID);
                    }
                }

                pubsub_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_IDENTIFY, buff.head, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                break;
            }
        }
    }

    SAFE_FREE(id);
    SAFE_FREE(value);
    SAFE_FREE(subdevID);
    SAFE_FREE(topic);
}

struct myprogress {
    const char *topic;
    const char *action;
};

static int download_value = 100;

static int older_progress(void *p,
                          double dltotal, double dlnow,
                          double ultotal, double ulnow)
{
    int current_value = 0;
    struct myprogress *myp = (struct myprogress *)p;

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
    json_object_object_add(jobj, ST_ACTION, json_object_new_string(myp->action));
    json_object_object_add(jobj, ST_STATE, json_object_new_string(ST_DOWNLOADING));

    if(dltotal)
    {
        current_value = (int)(100*dlnow/dltotal);

        char value[8];
        snprintf(value, sizeof(value),  "%d", current_value);

        json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
    }
    else
    {
        json_object_object_add(jobj, ST_VALUE, json_object_new_string("0"));
        current_value = 0;
    }
    if(download_value != current_value)
    {
        download_value = current_value;
        VR_(Send_message)(myp->topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
    }
    json_object_put(jobj);
    return 0;
}

void updatefirmware(void *data)
{
    update_firmware_d *upfirm = (update_firmware_d *)data;
    SLOGI("upfirm->URL = %s\n", (char *)upfirm->URL);
    SLOGI("upfirm->topic = %s\n", (char *)upfirm->topic);

    struct myprogress myp;
    myp.topic = upfirm->topic;
    myp.action = ST_UPDATE;

    const char *filename = FIRMWARE_NAME;
    int res = VR_(download_file)((char *)upfirm->URL, filename, NULL, older_progress, (void *)&myp);

    if(CURLE_OK != res)
    {
        // VR_(led_show)();
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
        json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        if(CURLE_COULDNT_RESOLVE_HOST == res)
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_FIRMWARE_FAILED_DOWNLOAD));
        }
        VR_(Send_message)(upfirm->topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
        free(upfirm);
        VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
        system("reboot");
    }
    else
    {
        char md5sum[SIZE_256B];
        char command[SIZE_256B];
        snprintf(command, sizeof(command), "md5sum %s | cut -d' ' -f1", filename);

        VR_(run_command)(command, md5sum, sizeof(md5sum));

        char *fw_md5sum = (char*)VR_(read_option)(NULL, UCI_FIRMWARE_CHECKSUM);
        if(fw_md5sum)
        {
            SLOGI("fw_md5sum = %s\n", fw_md5sum);
            SLOGI("md5sum = %s\n", md5sum);
            if(strcmp(md5sum, fw_md5sum))
            {
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
                json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_FIRMWARE_WRONG_CHECKSUM));
                VR_(Send_message)(upfirm->topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
                json_object_put(jobj);
                free(upfirm);
                VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
                system("reboot");
            }
        }
        else
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_FIRMWARE_MISSING_CHECKSUM));
            VR_(Send_message)(upfirm->topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
            free(upfirm);
            VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
            system("reboot");
        }

        char *current_version = (char*)VR_(read_option)(NULL, UCI_FIRMWARE_CURRENT_VER);
        if(!current_version || !strlen(current_version))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_FIRMWARE_MISSING_VERSION));
            VR_(Send_message)(upfirm->topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
            free(upfirm);
            VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
            system("reboot");
        }
        
        snprintf(command, sizeof(command), "firmware_update.sh %s", filename);

        SLOGI("command = %s\n", command);
        FILE *fp;
        fp = popen(command,"r");
        char buff[255];
        while(fgets(buff, 255, fp)!=NULL)
        {
            printf("buff = %s\n", buff);
            usleep(10000);
        }
        pclose(fp);

        if(!strncasecmp(buff,ST_UPDATE_SUCCESS, strlen(ST_UPDATE_SUCCESS)))
        {
            VR_(execute_system)("ubus send alljoyn '{\"state\":\"firmware_updated\"}' &");
            sleep(5);
            
            json_object *jobj = json_object_new_object();
            VR_(write_option)(NULL, UCI_FIRMWARE_PRE_VER"=%s", current_version);
            free(current_version);

            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            json_object_object_add(jobj, ST_STATE, json_object_new_string("update done, restarting system"));

            VR_(Send_message)(upfirm->topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
            free(upfirm);
            system("reboot");
        }
        else if(!strncasecmp(buff,ST_WRONG_URL, strlen(ST_WRONG_URL)))
        {
            // VR_(led_show)();
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_WRONG_URL));
            VR_(Send_message)(upfirm->topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
            free(upfirm);
            VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
            system("reboot");
        }
        else if(!strncasecmp(buff,ST_UNTAR_FAILED, strlen(ST_UNTAR_FAILED)))
        {
            // VR_(led_show)();
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_UNTAR_FAILED));
            VR_(Send_message)(upfirm->topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
            free(upfirm);
            VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
            system("reboot");
        }
    }   
}

void update_supported_database(void *data)
{
    CURLcode res;
    char *topic = (char*)(data);

    const char *filename = "/tmp/databases.tar.gz";

    char *link_supported_database = (char*)VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_DOWNLOAD_URL);

    SLOGI("link_supported_database: %s\n", link_supported_database);

    struct myprogress myp;
    myp.topic = topic;
    myp.action = ST_UPDATE_SUPPORT_DATABASE;

    res = VR_(download_file)(link_supported_database, filename, NULL,
                            older_progress, (void *)&myp);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
    json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE_SUPPORT_DATABASE));

    if(CURLE_OK != res)
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_int(res));
    }
    else
    {
        char md5sum[SIZE_256B];
        char command[SIZE_256B];
        snprintf(command, sizeof(command), "md5sum %s | cut -d' ' -f1", filename);

        VR_(run_command)(command, md5sum, sizeof(md5sum));

        char *fw_md5sum = (char*)VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_CHECKSUM);
        if(fw_md5sum)
        {
            SLOGI("fw_md5sum = %s\n", fw_md5sum);
            SLOGI("md5sum = %s\n", md5sum);
            if(strcmp(md5sum, fw_md5sum))
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("wrong checksum"));
            }
            else
            {
                VR_(execute_system)("tar -xvzf /tmp/databases.tar.gz -C /tmp/");
                VR_(execute_system)("dd if=/tmp/supported_devices.db of=/etc/supported_devices.db");
                VR_(execute_system)("rm -rf /etc/sound; mv /tmp/sound/ /etc/");
                char *latest_version = VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_LATEST_VER);
                if(latest_version && strlen(latest_version))
                {
                    VR_(execute_system)("ubus send %s '{\"version\":\"%s\"}'", ST_UPDATE_SUPPORTED_DB, latest_version);
                    json_object_object_add(jobj, ST_VERSION, json_object_new_string(latest_version));

                    VR_(write_option)(NULL, UCI_FIRMWARE_DATABASE_CURRENT_VER"=%s", latest_version);
                    free(latest_version);
                }
                else
                {
                    VR_(execute_system)("ubus send %s '{\"version\":\"%s\"}'", ST_UPDATE_SUPPORTED_DB, ST_UNKNOWN);
                }
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                json_object_object_add(jobj, ST_STATE, json_object_new_string("update done"));
            }
        }
        else
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_int(res));
        }      
    }
    VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
    json_object_put(jobj);
    SAFE_FREE(topic);
    SAFE_FREE(link_supported_database);
}

void read_more_version_info(char *filename, json_object *version_info)
{
    if(!filename || !version_info)
    {
        return;
    }

    int i = 0;
    FILE *fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    fp = fopen(filename, "r");
    if(!fp)
    {
        return;
    }
    while ((read = getline(&line, &len, fp)) != -1)
    {
        char *pos;
        if ((pos=strchr(line, '\n')) != NULL)
            *pos = '\0';

        switch(i)
        {
            case 0:
                json_object_object_add(version_info, ST_VERSION, json_object_new_string(line));
                break;

            case 1:
                json_object_object_add(version_info, ST_BRANCH, json_object_new_string(line));
                break;

            case 2:
                json_object_object_add(version_info, ST_BUILD_DATE, json_object_new_string(line));
                break;

            case 3:
                json_object_object_add(version_info, ST_UPDATE_DATE, json_object_new_string(line));
                break;
        }
        i++;
    }
    if(line)
        free(line);
    fclose(fp);
}

void VR_(firmware_actions)(const char *service_type, const char *msg) //no need to use service_type
{
    SLOGI("The command %s is called \n ", __func__);
    char* action = VR_(get_string_data)(msg, ST_ACTION);
    char* version = VR_(get_string_data)(msg, ST_VERSION);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    SLOGI("action = %s", action);
    SLOGI("version = %s", version);
    SLOGI("topic = %s", topic);

    if(action && strlen(action))
    {
        if(!strcasecmp(action, ST_UPDATE))
        {
            VR_(execute_system)("ubus send alljoyn '{\"state\":\"firmware_updating\"}' &");

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            json_object_object_add(jobj, ST_STATE, json_object_new_string(ST_UPDATING));

            VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);

            update_firmware_d *data = (update_firmware_d *)malloc(sizeof(update_firmware_d));
            memset(data, 0x00, sizeof(update_firmware_d));

            char *url = (char*)VR_(read_option)(NULL, UCI_FIRMWARE_DOWNLOAD_URL);
            if(url)
            {
                SAFE_STRCPY(data->URL, url);
                free(url);
            }

            strncpy(data->topic, topic, 255);

            pthread_t upfirmware_thread;
            pthread_create(&upfirmware_thread, NULL, (void *)&updatefirmware, data);
            pthread_detach(upfirmware_thread);
        }
        else if(!strcasecmp(action, ST_GET_LATEST))
        {
            int res;
            char address[SIZE_512B], headerIn[SIZE_512B]={0};
            
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

            char *firmware_address = (char*)VR_(read_option)(NULL,UCI_CLOUD_ADDRESS);
            char *firmware_current_version = VR_(read_option)(NULL, UCI_FIRMWARE_CURRENT_VER);
            char *hubUUID = (char*)VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
            char firmware_version[SIZE_256B];
            firmware_info_t firmware_data;
            // char device_name[SIZE_32B];
            // char device_rev[SIZE_32B];

            // VR_(run_command)(GET_BOARD_NAME_COMMAND, device_name, sizeof(device_name));
            // VR_(run_command)(GET_BOARD_REV_COMMAND, device_rev, sizeof(device_rev));

            get_data_firmware(PHD_BOARD, HW_REV_E, &firmware_data);

            if(firmware_current_version)
            {
                if(strstr(firmware_current_version, "Beta"))
                {
                    SAFE_STRCPY(firmware_version, "0.0.0");
                }
                else
                {
                    SAFE_STRCPY(firmware_version, firmware_current_version);
                }
            }
            else
            {
                SAFE_STRCPY(firmware_version, "0.0.0");
            }

            if(!firmware_address || 
                (firmware_address && !strcmp(firmware_address, PRODUCT_FIRMWARE_ADDR))
                )
            {
                snprintf(address, sizeof(address), "%s"IS_UPDATE_LINK, FIRMWARE_ADDRESS, 
                                                      firmware_data.product_name, 
                                                      firmware_version,
                                                      hubUUID);
                snprintf(headerIn, sizeof(headerIn), "authorization: %s", firmware_data.token_product);
            }
            else
            {
                snprintf(address, sizeof(address), "%s"IS_UPDATE_LINK, FIRMWARE_ADDRESS, 
                                                      firmware_data.dev_name, 
                                                      firmware_version,
                                                      hubUUID);
                snprintf(headerIn, sizeof(headerIn), "authorization: %s", firmware_data.token_dev);
            }

            char *result = NULL;
            int is_update = 0;
            res = VR_(get_request)(address, headerIn, NULL, NULL, &result, DEFAULT_TIMEOUT);
            if(!res)
            {
                SLOGI("result = %s\n", result);
                json_object *check_update_result = VR_(create_json_object)(result);
                if(check_update_result)
                {
                    json_object *error = json_object_object_get(check_update_result, "error");
                    if(error)
                    {
                        const char *message = json_object_get_string(json_object_object_get(error, "message"));
                        SLOGE("Checking update Error: %s\n", message);
                        json_object_object_add(jobj, ST_LATEST_VER, json_object_new_string(firmware_current_version));
                        json_object_object_add(jobj, ST_UPDATE, json_object_new_boolean(is_update));
                    }
                    else
                    {
                        is_update = json_object_get_boolean(json_object_object_get(check_update_result, "fwUpdate"));
                        const char *latest_version = json_object_get_string(json_object_object_get(check_update_result, "version"));
                        json_object_object_add(jobj, ST_LATEST_VER, json_object_new_string(latest_version));
                        json_object_object_add(jobj, ST_UPDATE, json_object_new_boolean(is_update));
                        if(is_update)
                        {
                            const char *fw_checksum = json_object_get_string(json_object_object_get(check_update_result, "fwChecksum"));
                            const char *fw_url = json_object_get_string(json_object_object_get(check_update_result, "fwUrl"));
                            VR_(write_option)(NULL, UCI_FIRMWARE_LATEST_VER"=%s", latest_version);
                            VR_(write_option)(NULL, UCI_FIRMWARE_CHECKSUM"=%s", fw_checksum);
                            VR_(write_option)(NULL, UCI_FIRMWARE_DOWNLOAD_URL"=%s", fw_url);
                        }
                    }
                    json_object_put(check_update_result);
                }
                else
                {
                    SLOGI("failed to parser check update json format\n");
                    json_object_object_add(jobj, ST_LATEST_VER, json_object_new_string(firmware_current_version));
                    json_object_object_add(jobj, ST_UPDATE, json_object_new_boolean(is_update));
                }
            }
            else
            {
                json_object_object_add(jobj, ST_LATEST_VER, json_object_new_string(firmware_current_version));
                json_object_object_add(jobj, ST_UPDATE, json_object_new_boolean(is_update));
            }
            free(result);

            VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);

            SAFE_FREE(firmware_current_version);
            SAFE_FREE(firmware_address);
            SAFE_FREE(hubUUID);
        }
        else if(!strcasecmp(action, ST_GET_CURRENT))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
            
            char *ap_disabled = VR_(read_option)(NULL, UCI_WIFI_AP_MODE_DISABLED);
            char *firmware_current_version = VR_(read_option)(NULL, UCI_FIRMWARE_CURRENT_VER);
            char *firmware_newest_version = VR_(read_option)(NULL, UCI_FIRMWARE_LATEST_VER);
            char *supported_current_version = VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_CURRENT_VER);

            if(firmware_current_version && firmware_newest_version)
            {
                int res = VR_(version_cmp)(firmware_current_version, firmware_newest_version);
                if(res < 0)
                {
                    json_object_object_add(jobj, ST_UPDATE, json_object_new_boolean(true));
                }
                else
                {
                    json_object_object_add(jobj, ST_UPDATE, json_object_new_boolean(false));
                }
            }
            else
            {
                json_object_object_add(jobj, ST_UPDATE, json_object_new_boolean(false));
            }

            JSON_RETURN(jobj, ST_CURRENT_VER, firmware_current_version);
            JSON_RETURN(jobj, ST_LATEST_VER, firmware_newest_version);
            JSON_RETURN(jobj, ST_SP_CURRENT_VER, supported_current_version);

            json_object *apmode = json_object_new_object();

            char *apssid = VR_(read_option)(NULL, UCI_HUB_DEFAULT_NAME);
            char *apencryption = VR_(read_option)(NULL,UCI_ALLJOYN_ONBOARDING_APENCRYPTION);
            JSON_RETURN(apmode, ST_SSID, apssid);
            JSON_RETURN(apmode, ST_ENCRYPTION, apencryption);

            if(ap_disabled)
            {
                if(!strcmp(ap_disabled, "0"))
                {
                    json_object_object_add(apmode, ST_STATE, json_object_new_string("active"));
                }
                else if(!strcmp(ap_disabled, "1"))
                {
                    json_object_object_add(apmode, ST_STATE, json_object_new_string("inactive"));
                }
            }
            else
            {
                json_object_object_add(apmode, ST_STATE, json_object_new_string("unknown"));
            }

            json_object *stamode = json_object_new_object();

            char *ssid = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_SSID);
            char *encryption = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_ENCRYPTION);
            JSON_RETURN(stamode, ST_SSID, ssid);
            JSON_RETURN(stamode, ST_ENCRYPTION, encryption);

            if(ap_disabled)
            {
                if(!strcmp(ap_disabled, "0"))
                {
                    char *state = VR_(read_option)(NULL, UCI_LOCAL_STATE);
                    char *sta_disabled = VR_(read_option)(NULL, UCI_WIFI_STA_DISABLED);
                    if(!state || !sta_disabled)
                    {
                        json_object_object_add(stamode, ST_STATE, json_object_new_string("inactive"));
                    }
                    else if(!strcmp(state, "available")) /*builder mode*/
                    {
                        if(!strcmp(sta_disabled, "0"))/*sta mode enable*/
                        {
                            char sta_result[SIZE_64B];
                            VR_(run_command)(GET_STA_STATUS_COMMAND, sta_result, sizeof(sta_result));
                            if(strcmp(sta_result, "COMPLETED"))
                            {
                                json_object_object_add(stamode, ST_STATE, json_object_new_string("inactive"));
                            }
                            else
                            {
                                json_object_object_add(stamode, ST_STATE, json_object_new_string("active"));
                                json_object_object_add(apmode, ST_STATE, json_object_new_string("inactive"));
                            }
                        }
                        else
                        {
                            json_object_object_add(stamode, ST_STATE, json_object_new_string("inactive"));
                        }
                    }
                    else
                    {
                        json_object_object_add(stamode, ST_STATE, json_object_new_string("inactive"));
                    }

                    SAFE_FREE(state);
                    SAFE_FREE(sta_disabled);
                }
                else if(!strcmp(ap_disabled, "1"))
                {
                    json_object_object_add(stamode, ST_STATE, json_object_new_string("active"));
                }
            }
            else
            {
                json_object_object_add(stamode, ST_STATE, json_object_new_string("unknown"));
            }

            json_object_object_add(jobj, ST_AP_MODE, apmode);
            json_object_object_add(jobj, ST_STA_MODE, stamode);

            json_object *version_info = json_object_new_object();
            read_more_version_info("/etc/venus_version", version_info);
            json_object_object_add(jobj, ST_VER_INFO, version_info);

            SAFE_FREE(ap_disabled);

            char temperature[SIZE_32B];
            VR_(run_command)(GET_TEMP_COMMAND, temperature, sizeof(temperature));
            json_object_object_add(jobj, ST_TEMP, json_object_new_string(temperature));

            char ipAddress[SIZE_32B];
            VR_(run_command)(GET_IP_ADDRESS_COMMAND, ipAddress, sizeof(ipAddress));
            json_object_object_add(jobj, ST_IP_ADDRESS, json_object_new_string(ipAddress));

            char macAddress[SIZE_32B];
            VR_(run_command)(GET_MAC_ADDRESS_COMMAND, macAddress, sizeof(macAddress));
            json_object_object_add(jobj, ST_MAC_ADDRESS, json_object_new_string(macAddress));

            char *bssid = VR_(read_option)(NULL, UCI_WIFI_AP_MAC_ADDRESS);
            JSON_RETURN(jobj, ST_BSSID, bssid);

            char *timezone = VR_(read_option)(NULL, UCI_SYSTEM_TIMEZONE);
            JSON_RETURN(jobj, ST_TIME_ZONE, timezone);

            int current_volume = VR_(get_volume)();
            json_object_object_add(jobj, ST_VOLUME, json_object_new_int(current_volume));

            unsigned int led_current = get_led_current();
            json_object_object_add(jobj, ST_LED_CURRENT, json_object_new_int(led_current));

            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
        }
        else if(!strcasecmp(action, ST_UPDATE_SUPPORT_DATABASE))
        {
            int res;
            char address[SIZE_512B], headerIn[SIZE_512B]={0};

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));

            char *firmware_address = (char*)VR_(read_option)(NULL,UCI_CLOUD_ADDRESS);
            char *firmware_current_version = VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_CURRENT_VER);
            char *hubUUID = (char*)VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
            char firmware_version[SIZE_256B];

            if(firmware_current_version)
            {
                if(strlen(firmware_current_version) < 5)
                {
                    SAFE_STRCPY(firmware_version, "0.0.0");
                }
                else
                {
                    SAFE_STRCPY(firmware_version, firmware_current_version);
                }
            }
            else
            {
                SAFE_STRCPY(firmware_version, "0.0.0");
            }

            if(!firmware_address || 
                (firmware_address && !strcmp(firmware_address, PRODUCT_FIRMWARE_ADDR))
                )
            {
                char *requestId = VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_ID);
                char *token = VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_TOKEN);
                if(!requestId)
                {
                    requestId = strdup(DEFAULT_DATABASE_ID);
                }

                if(!token)
                {
                    token = strdup(DEFAULT_DATABASE_TOKEN);
                }

                snprintf(address, sizeof(address), "%s"IS_UPDATE_LINK, FIRMWARE_ADDRESS, 
                                                      requestId,
                                                      firmware_version,
                                                      hubUUID);
                snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);

                free(requestId);
                free(token);
            }
            else
            {
                char *requestId = VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_ID);
                char *token = VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_TOKEN);
                if(!requestId)
                {
                    requestId = strdup(DEFAULT_DATABASE_ID);
                }

                if(!token)
                {
                    token = strdup(DEFAULT_DATABASE_TOKEN);
                }

                snprintf(address, sizeof(address), "%s"IS_UPDATE_LINK, FIRMWARE_ADDRESS, 
                                                      requestId,
                                                      firmware_version,
                                                      hubUUID);
                snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);
                free(requestId);
                free(token);
            }

            char *result = NULL;
            res = VR_(get_request)(address, headerIn, NULL, NULL, &result, DEFAULT_TIMEOUT);
            if(!res)
            {
                SLOGI("result = %s\n", result);
                json_object *check_update_result = VR_(create_json_object)(result);
                if(check_update_result)
                {
                    json_object *error = json_object_object_get(check_update_result, "error");
                    if(error)
                    {
                        const char *message = json_object_get_string(json_object_object_get(error, "message"));
                        SLOGE("Checking update Error: %s\n", message);
                        json_object_object_add(jobj, ST_STATE, json_object_new_string("update done"));
                    }
                    else
                    {
                        int is_update = json_object_get_boolean(json_object_object_get(check_update_result, "fwUpdate"));
                        const char *latest_version = json_object_get_string(json_object_object_get(check_update_result, "version"));

                        if(is_update)
                        {
                            json_object_object_add(jobj, ST_STATE, json_object_new_string(ST_UPDATING));

                            const char *fw_checksum = json_object_get_string(json_object_object_get(check_update_result, "fwChecksum"));
                            const char *fw_url = json_object_get_string(json_object_object_get(check_update_result, "fwUrl"));
                            VR_(write_option)(NULL, UCI_FIRMWARE_DATABASE_LATEST_VER"=%s", latest_version);
                            VR_(write_option)(NULL, UCI_FIRMWARE_DATABASE_CHECKSUM"=%s", fw_checksum);
                            VR_(write_option)(NULL, UCI_FIRMWARE_DATABASE_DOWNLOAD_URL"=%s", fw_url);

                            char *topic_pt = NULL;
                            if(topic)
                            {
                                size_t length = strlen(topic)+1;
                                topic_pt = (char *)malloc(length);
                                strlcpy(topic_pt, topic, length);
                            }
                            pthread_t update_supported_database_thread;
                            pthread_create(&update_supported_database_thread, NULL, (void *)&update_supported_database, topic_pt);
                            pthread_detach(update_supported_database_thread);
                        }
                        else
                        {
                            json_object_object_add(jobj, ST_STATE, json_object_new_string("update done"));
                        }
                    }
                    json_object_put(check_update_result);
                }
                else
                {
                    SLOGI("failed to parser check update json format\n");
                    json_object_object_add(jobj, ST_STATE, json_object_new_string("update done"));
                }
            }
            else
            {
                json_object_object_add(jobj, ST_STATE, json_object_new_string("update done"));
            }
            SAFE_FREE(result);

            SAFE_FREE(firmware_current_version);
            SAFE_FREE(firmware_address);
            SAFE_FREE(hubUUID);

            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
        }
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("missing argument"));
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
    }

    SAFE_FREE(action);
    SAFE_FREE(version);
    SAFE_FREE(topic);
}

void VR_(open_close_network)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    int i = 0;
    char* action = VR_(get_string_data)(msg, ST_ACTION);
    char* device_id = VR_(get_string_data)(msg, ST_DEVICE_ID);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    SLOGI("action = %s\n", action);
    SLOGI("device_id = %s\n", device_id);
    SLOGI("topic = %s\n", topic);

    if(!strcmp(service_type, ST_ALL))
    {
        // if(!strcasecmp(action,"open"))
        // {
        //     VR_(execute_system)("ubus send pubsub '{\"state\":\"open_network\"}' &");
        // }
        // else if(!strcasecmp(action,"close"))
        // {
        //     VR_(execute_system)("ubus send pubsub '{\"state\":\"close_network\"}' &");
        // }

        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if((!strcmp(ST_ZIGBEE, daemon_services[i].name) || !strcmp(ST_ZWAVE, daemon_services[i].name)) 
                && daemon_services[i].id != 0)
            {
                new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
                memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));

                SAFE_STRCPY(pubsub_ubus->name, ST_OPEN_CLOSE_NETWORK);
                if(topic)
                {
                    SAFE_STRCPY(pubsub_ubus->topic, topic);
                }
                pubsub_ubus->type = 1;

                pubsub_ubus->service_name = (void*)daemon_services[i].name;
                pubsub_ubus->action_time = (unsigned)time(NULL);
                VR_(add_last_list)(pubsub_ubus);
                blob_buf_init(&buff, 0);

                struct ubus_request *req_s = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req_s;

                if(!strcasecmp(action,ST_OPEN))
                {
                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_OPEN_NETWORK, buff.head, req_s,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                }
                else if(!strcasecmp(action,ST_CLOSE))
                {
                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_CLOSE_NETWORK, buff.head, req_s,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                }
            }
        }
    }
    else
    {
        SLOGI("service_type = %s\n", service_type);
        new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
        memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));
        SAFE_STRCPY(pubsub_ubus->name, ST_OPEN_CLOSE_NETWORK);

        if(topic)
        {
            SAFE_STRCPY(pubsub_ubus->topic, topic);
        }
        pubsub_ubus->type = 1;
        VR_(add_last_list)(pubsub_ubus);

        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(service_type, daemon_services[i].name))
            {
                pubsub_ubus->service_name = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {
                    pubsub_ubus->action_time = (unsigned)time(NULL);

                    blob_buf_init(&buff, 0);
                    if(!strcmp(service_type, ST_UPNP) && strlen(device_id))
                    { 
                        blobmsg_add_string(&buff, ST_ID, device_id);
                    }

                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    pubsub_ubus->req=req;
                    
                    if(!strcasecmp(action,ST_OPEN))
                    {
                        VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_OPEN_NETWORK, buff.head, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                    }
                    else if(!strcasecmp(action,ST_CLOSE))
                    {
                        VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_CLOSE_NETWORK, buff.head, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                    }
                    
                    break;
                }
            }
        }
    }

    SAFE_FREE(action);
    SAFE_FREE(device_id);
    SAFE_FREE(topic);
}

void VR_(add_devices)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    int i = 0;
    char* device_id = VR_(get_string_data)(msg, ST_DEVICE_ID);
    char* subdevID = VR_(get_string_data)(msg, ST_SUBDEVID);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    if(strcmp(service_type, ST_UPNP))
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(service_type));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_ADD_DEVICES_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("add devices just support only upnp subdevices"));
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
    }
    else
    {
        new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
        memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));
        SAFE_STRCPY(pubsub_ubus->name, ST_ADD_DEVICES);

        if(topic)
        {
            SAFE_STRCPY(pubsub_ubus->topic, topic);
        }

        pubsub_ubus->type = 1;
        VR_(add_last_list)(pubsub_ubus);

        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(ST_UPNP, daemon_services[i].name))
            {
                pubsub_ubus->service_name = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0 )
                {
                    pubsub_ubus->action_time = (unsigned)time(NULL);

                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, ST_ID, device_id);
                    blobmsg_add_string(&buff, ST_SUBDEVID, subdevID);

                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    pubsub_ubus->req=req;

                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_ADD_DEVICES, buff.head, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                    break;
                }
            }
        }
    }

    SAFE_FREE(device_id);
    SAFE_FREE(subdevID);
    SAFE_FREE(topic);
}

void VR_(remove_device)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    int i = 0;
    char* device_id = VR_(get_string_data)(msg, ST_DEVICE_ID);
    char* subdevID = VR_(get_string_data)(msg, ST_SUBDEVID);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));
    SAFE_STRCPY(pubsub_ubus->name, ST_REMOVE_DEVICE);

    if(topic)
    {
        SAFE_STRCPY(pubsub_ubus->topic, topic);
    }

    pubsub_ubus->type = 1;
    VR_(add_last_list)(pubsub_ubus);

    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(service_type, daemon_services[i].name))
        {
            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                if(device_id && strlen(device_id))
                {
                    blobmsg_add_string(&buff, ST_ID, device_id);
                }

                if(subdevID && strlen(subdevID))
                {
                    blobmsg_add_string(&buff, ST_SUBDEVID, subdevID);
                }

                pubsub_ubus->action_time = (unsigned)time(NULL);
                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_REMOVE_DEVICE, buff.head, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                break;
            }
        }
    }

    SAFE_FREE(device_id);
    SAFE_FREE(subdevID);
    SAFE_FREE(topic);
}

static void cloud_create_group_thread(void *data)
{
    group_info *group_data = (group_info *)data;

    if(!group_data)
    {
        return;
    }

    char *userId = group_data->userId;
    char *appId = group_data->appId;
    char *groupId = group_data->groupId;
    char *groupName = group_data->groupName;
    char *groupDev = group_data->groupDev;

    int res = database_actions("pubsub_handler", dev_db, 
                         "INSERT INTO GROUPS (userId,groupId,groupName,device) "
                         "VALUES('%s','%s','%s','%s')",
                         userId, groupId, groupName, groupDev);
    if(res)
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CREATE_GROUP_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
        json_object_object_add(jobj, ST_APP_ID, json_object_new_string(appId));
        json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
        json_object_object_add(jobj, ST_GROUP_DEV, json_object_new_string(groupDev));

        VR_(Send_message)(NULL, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
        goto done;
    }

    VR_(update_group_Etag)();

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CREATE_GROUP_R));
    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
    json_object_object_add(jobj, ST_APP_ID, json_object_new_string(appId));
    json_object_object_add(jobj, ST_GROUP_ID, json_object_new_string(groupId));
    json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
    json_object_object_add(jobj, ST_GROUP_DEV, json_object_new_string(groupDev));

    char group_cloud_id[SIZE_64B];
    memset(group_cloud_id, 0x00, sizeof(group_cloud_id));
    res = VR_(cloud_create_group)(g_shm, groupName, groupDev, 
                                      group_cloud_id, sizeof(group_cloud_id));
    if(!res)
    {
        if(strlen(group_cloud_id))
        {
            database_actions("pubsub_handler", dev_db, 
                            "INSERT INTO DEVICES(deviceId,deviceType,owner) "
                            "VALUES('%s','%s','%s')", groupId, ST_GROUP, group_cloud_id);
            json_object_object_add(jobj, ST_CLOUD_RESOURCE, json_object_new_string(group_cloud_id));
            shm_update_data(g_shm, group_cloud_id, group_data->groupId, ST_GROUP, SHM_ADD);
        }
    }

    Broadcast_message(VR_QOS, (char *)json_object_to_json_string(jobj), 0);
    json_object_put(jobj);

done:    
    SAFE_FREE(groupId);
    SAFE_FREE(groupName);
    SAFE_FREE(groupDev);
    SAFE_FREE(userId);
    SAFE_FREE(appId);
    SAFE_FREE(group_data);
} 

void VR_(create_group)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    char* userId = VR_(get_string_data)(msg, ST_USER_ID);
    char* appId = VR_(get_string_data)(msg, ST_APP_ID);
    char* groupName = VR_(get_string_data)(msg, ST_GROUP_NAME);
    char* groupDev = VR_(get_string_data)(msg, ST_GROUP_DEV);

    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    if( userId && strlen(userId) 
        && appId && strlen(appId) 
        && groupName && strlen(groupName) 
        && groupDev && strlen(groupDev)
       )
    {
        char groupId[SIZE_32B];
        snprintf(groupId, sizeof(groupId), "%08X", VR_(generate_32bit)());

        SEARCH_DATA_INIT_VAR(name);

        searching_database("pubsub_handler", dev_db, get_last_data_cb, &name, 
                            "SELECT groupName from GROUPS where groupId='%s'",
                            groupId);
        if(name.len)
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CREATE_GROUP_R));
            json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
            json_object_object_add(jobj, ST_APP_ID, json_object_new_string(appId));
            json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("group already has existed"));
            VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
            goto done;
        }

        searching_database("pubsub_handler", dev_db, get_last_data_cb, &name, 
                            "SELECT groupName from GROUPS where groupName='%s'",
                            groupName);
        if(name.len)
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CREATE_GROUP_R));
            json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
            json_object_object_add(jobj, ST_APP_ID, json_object_new_string(appId));
            json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("group name already has existed"));
            VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
            goto done;
        }
        FREE_SEARCH_DATA_VAR(name);

        if(!VR_(check_group_dev_valid)(groupDev))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CREATE_GROUP_R));
            json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
            json_object_object_add(jobj, ST_APP_ID, json_object_new_string(appId));
            json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
            json_object_object_add(jobj, ST_GROUP_DEV, json_object_new_string(groupDev));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("group dev is invalid"));
            VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
            return;
        }

        group_info *group_data = (group_info *)malloc(sizeof(group_info));
        memset(group_data, 0, sizeof(group_info));

        group_data->groupId = strdup(groupId);
        group_data->groupName = strdup(groupName);
        group_data->groupDev = strdup(groupDev);
        group_data->userId = strdup(userId);
        group_data->appId = strdup(appId);

        pthread_t cloud_create_group_thread_t;
        pthread_create(&cloud_create_group_thread_t, NULL, (void *)&cloud_create_group_thread, (void *)group_data);
        pthread_detach(cloud_create_group_thread_t);  
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CREATE_GROUP_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("missing argument"));
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
    }

done:    
    SAFE_FREE(userId);
    SAFE_FREE(appId);
    SAFE_FREE(groupName);
    SAFE_FREE(groupDev);
    SAFE_FREE(topic);
}

void VR_(group_actions)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    char* action = VR_(get_string_data)(msg, ST_ACTION);
    char* userId = VR_(get_string_data)(msg, ST_USER_ID);
    char* groupId = VR_(get_string_data)(msg, ST_GROUP_ID);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    if( action && strlen(action)
        && userId && strlen(userId)
        && groupId && strlen(groupId)
        )
    {
        if(!strcmp(action, ST_MODIFY))
        {
            char* groupName = VR_(get_string_data)(msg, ST_GROUP_NAME);
            char* groupDev = VR_(get_string_data)(msg, ST_GROUP_DEV);

            if(!groupName || !strlen(groupName)
                || !groupDev || !strlen(groupDev)
                )
            {
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GROUP_ACTIONS_R));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("missing argument"));
                json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
                json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
                json_object_object_add(jobj, ST_GROUP_ID, json_object_new_string(groupId));
                VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
                json_object_put(jobj);

                SAFE_FREE(groupName);
                SAFE_FREE(groupDev);
                goto done;
            }

            SEARCH_DATA_INIT_VAR(database_group_id);
            searching_database("pubsub_handler", dev_db, get_last_data_cb, &database_group_id, 
                                "SELECT groupId from GROUPS where groupName='%s' and groupId!='%s'",
                                groupName, groupId);
            if(database_group_id.len)
            {
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GROUP_ACTIONS_R));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("group name has existed"));
                json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
                json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
                json_object_object_add(jobj, ST_GROUP_ID, json_object_new_string(groupId));
                json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
                json_object_object_add(jobj, ST_NEW_NAME, json_object_new_string(groupName));
                VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
                json_object_put(jobj);
                
                SAFE_FREE(groupName);
                SAFE_FREE(groupDev);
            }
            else
            {
                if(!VR_(check_group_dev_valid)(groupDev))
                {
                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GROUP_ACTIONS_R));
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
                    json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
                    json_object_object_add(jobj, ST_GROUP_ID, json_object_new_string(groupId));
                    json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
                    json_object_object_add(jobj, ST_GROUP_DEV, json_object_new_string(groupDev));
                    json_object_object_add(jobj, ST_REASON, json_object_new_string("group dev is invalid"));

                    VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
                    json_object_put(jobj);
                    SAFE_FREE(groupName);
                    SAFE_FREE(groupDev);
                    FREE_SEARCH_DATA_VAR(database_group_id);
                    goto done;
                }

                database_actions("pubsub_handler", dev_db, 
                             "UPDATE GROUPS set %s='%s',%s='%s' where %s='%s'",
                             ST_GROUP_NAME, groupName, ST_DEVICE, groupDev, 
                             ST_GROUP_ID, groupId);
                VR_(update_group_Etag)();
                VR_(cloud_modify_group)(g_shm, groupId, groupName, groupDev);

                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GROUP_ACTIONS_R));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
                json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
                json_object_object_add(jobj, ST_GROUP_ID, json_object_new_string(groupId));
                json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
                json_object_object_add(jobj, ST_GROUP_DEV, json_object_new_string(groupDev));

                Broadcast_message(VR_QOS, (char *)json_object_to_json_string(jobj), 0);
                json_object_put(jobj);
            }
            SAFE_FREE(groupDev);
            SAFE_FREE(groupName);
            FREE_SEARCH_DATA_VAR(database_group_id);
        }
        else
        {
            database_actions("pubsub_handler", dev_db,
                    "DELETE from GROUPS where groupId='%s'", groupId);
            database_actions("pubsub_handler", dev_db, "DELETE FROM DEVICES where deviceId='%s'", groupId);
            VR_(update_group_Etag)();
            VR_(cloud_delete_group)(g_shm, groupId);

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GROUP_ACTIONS_R));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
            json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
            json_object_object_add(jobj, ST_GROUP_ID, json_object_new_string(groupId));
            VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
        }
    }
done:
    SAFE_FREE(action);
    SAFE_FREE(userId);
    SAFE_FREE(groupId);
    SAFE_FREE(topic);
}

int group_action_cb(void *data, char *devicetype, void *devicelist, int length)
{
    printf("devicetype = %s\n", devicetype);
    int i = 0;
    group_action_data *data_cb = (group_action_data *)data;
    printf("data_cb->command = %s\n", data_cb->command);
    printf("data_cb->value = %s\n", data_cb->value);

    printf("devicelist = %s\n", (char *)devicelist);
    
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(devicetype, daemon_services[i].name))
        {
            if(daemon_services[i].id != 0)
            {
                char *save_tok;
                char *ch = strtok_r(devicelist, ",", &save_tok);
                while(ch != NULL)
                {
                    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
                    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));
                    SAFE_STRCPY(pubsub_ubus->name, ST_NOTIFY);

                    pubsub_ubus->type = 0;
                    pubsub_ubus->service_name = (void*)daemon_services[i].name;
                    pubsub_ubus->action_time = (unsigned)time(NULL);

                    VR_(add_last_list)(pubsub_ubus);

                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, ST_ID, ch);
                    blobmsg_add_string(&buff, ST_CMD, data_cb->command);
                    blobmsg_add_string(&buff, ST_VALUE, data_cb->value);

                    struct ubus_request *req_s = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    pubsub_ubus->req=req_s;

                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_SET_BINARY, buff.head, req_s,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                      
                    ch = strtok_r(NULL, ",", &save_tok);
                }
                break;
            }
        }
    }
    return 0;
}

void VR_(group_control)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    char* userid = VR_(get_string_data)(msg, ST_USER_ID);
    char* groupid = VR_(get_string_data)(msg, ST_GROUP_ID);
    char* command = VR_(get_string_data)(msg, ST_COMMAND);
    char* value = VR_(get_string_data)(msg, ST_VALUE);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    if( userid && strlen(userid) 
        && groupid && strlen(groupid) 
        && command && strlen(command)
        && value && strlen(value)
       )
    {
        SEARCH_DATA_INIT_VAR(device);
        searching_database("pubsub_handler", dev_db, get_last_data_cb, &device, 
                            "SELECT device from GROUPS where groupId='%s'",
                            groupid);
        if(device.len)
        {
            json_object * group_dev = VR_(create_json_object)(device.value);
            if(group_dev)
            {
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GROUP_CONTROL_R));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userid));
                json_object_object_add(jobj, ST_GROUP_ID, json_object_new_string(groupid));
                json_object_object_add(jobj, ST_COMMAND, json_object_new_string(command));
                json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
                const char * msgstr = (char *)json_object_to_json_string(jobj);

                Broadcast_message(VR_QOS, msgstr, 0);

                size_t length = strlen(msgstr)+1;
                char *input_msg = (char*)malloc(length);
                strlcpy(input_msg, msgstr, length);
                pthread_t update_shadow_status_thread;
                pthread_create(&update_shadow_status_thread, NULL, (void *)&update_shadow_status_cb, (void *)input_msg);
                pthread_detach(update_shadow_status_thread);
                json_object_put(jobj);

                group_action_data data;
                data.command = (char *)command;
                data.value = (char *)value;
                group_control_process(group_dev, (void*)&data, group_action_cb);
                json_object_put(group_dev);
            }
            else
            {
                SLOGE("Failed to parse json\n");
            }
        }
        else
        {

        }
        FREE_SEARCH_DATA_VAR(device);
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GROUP_CONTROL_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("missing argument"));
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
    }
    SAFE_FREE(userid);
    SAFE_FREE(groupid);
    SAFE_FREE(command);
    SAFE_FREE(value);
    SAFE_FREE(topic);
}

void VR_(share_access)(const char *service_type, const char *msg)
{
    SLOGI("share_access\n");

    char* adminId = VR_(get_string_data)(msg, ST_ADMIN_ID);
    char* userId = VR_(get_string_data)(msg, ST_USER_ID);
    char* shareId = VR_(get_string_data)(msg, ST_SHARE_ID);
    char* username = VR_(get_string_data)(msg, ST_USER_NAME);
    char* expiration = VR_(get_string_data)(msg, ST_EXPIRATION);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);
    
    SLOGI("adminId = %s\n", adminId);
    SLOGI("userId = %s\n", userId);
    SLOGI("shareId = %s\n", shareId);
    SLOGI("username = %s\n", username);
    SLOGI("expiration = %s\n", expiration);

    if(adminId && strlen(adminId)
        && userId && strlen(userId)
        && shareId && strlen(shareId)
        && username && strlen(username)
        )
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SHARE_ACCESS_R));
        json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
        json_object_object_add(jobj, ST_SHARE_ID, json_object_new_string(shareId));
        json_object_object_add(jobj, ST_USER_NAME, json_object_new_string(username));
        SEARCH_DATA_INIT_VAR(usertype);
        searching_database("pubsub_handler", user_db, get_last_data_cb, &usertype, 
                    "SELECT userType from USERS where userId='%s'", adminId);

        if(!strcmp(usertype.value, ST_ADMIN))
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

            VR_(remove_deny_timer)(userId);

            if(expiration && strlen(expiration) && strcmp(expiration, "0"))
            {
                time_t expiration_t = strtoul(expiration, NULL, 0);
                VR_(deny_with_timer)(userId, expiration_t);
                json_object_object_add(jobj, ST_EXPIRATION, json_object_new_string(expiration));
            }

            SEARCH_DATA_INIT_VAR(enable);
            searching_database("pubsub_handler", user_db, get_last_data_cb, &enable, 
                        "SELECT enable from USERS where userId='%s'", userId);
            if(enable.len)
            {
                if(!strcmp(enable.value, ST_DENY))
                {
                    database_actions("pubsub_handler", user_db, 
                        "UPDATE USERS set enable='%s' where userId='%s'", ST_ENABLE, userId);
                }
            }
            else
            {
                database_actions("pubsub_handler", user_db, 
                        "INSERT INTO USERS (userId,shareId,name,enable,userType) "\
                        "VALUES ('%s','%s','%s','%s','%s')", userId, shareId, username, ST_ENABLE, ST_USER);
            }
            FREE_SEARCH_DATA_VAR(enable);

            if(expiration && strlen(expiration) && strcmp(expiration, "0"))
            {
                database_actions("pubsub_handler", user_db, 
                        "UPDATE USERS set shareTime='%s',options='%u' where userId='%s'", 
                        expiration, (unsigned)time(NULL), userId);
            }
            // else
            // {
            //     database_actions("pubsub_handler", user_db, 
            //             "UPDATE USERS set shareTime=null,options=null where userId='%s'", 
            //             userId);
            // }
        }
        else
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("you are not allowed to do this"));
        }
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
        FREE_SEARCH_DATA_VAR(usertype);

        json_object *publish_jobj = json_object_new_object();
        json_object_object_add(publish_jobj, ST_METHOD, json_object_new_string(ST_PUBLISH));
        json_object_object_add(publish_jobj, ST_MESSAGE, json_object_new_string(ST_ACCESS_CHANGED));
        sendMsgtoLocalService((char*)ST_SEND_PUBLISH, (char *)json_object_to_json_string(publish_jobj));
        json_object_put(publish_jobj);
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SHARE_ACCESS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
    }

    SAFE_FREE(adminId);
    SAFE_FREE(userId);
    SAFE_FREE(shareId);
    SAFE_FREE(username);
    SAFE_FREE(expiration);
    SAFE_FREE(topic);
}

void VR_(deny_access)(const char *service_type, const char *msg)
{
    SLOGI("deny_access\n");

    char* userId1 = VR_(get_string_data)(msg, ST_ADMIN_ID);
    char* userId2 = VR_(get_string_data)(msg, ST_USER_ID);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    SLOGI("userId1 = %s\n", userId1);
    SLOGI("userId2 = %s\n", userId2);

    if(userId1 && strlen(userId1) && userId2 && strlen(userId2))
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_DENY_ACCESS_R));
        SEARCH_DATA_INIT_VAR(usertype);
        searching_database("pubsub_handler", user_db, get_last_data_cb, &usertype, 
                    "SELECT userType from USERS where userId='%s'", userId1);

        if(!strcmp(usertype.value, ST_ADMIN))
        {
            searching_database("pubsub_handler", user_db, get_last_data_cb, &usertype, 
                    "SELECT userType from USERS where userId='%s'", userId2);

            if(!strcmp(usertype.value, ST_USER))
            {
                VR_(remove_deny_timer)(userId2);

                database_actions("pubsub_handler", user_db, 
                        "DELETE from USERS where userId='%s'", userId2);

                json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId2));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

                char psk[SIZE_256B];
                VR_(generate_128bit)(psk);
                VR_(write_option)(NULL, UCI_LOCAL_PSK"=%s", psk);
                VR_(execute_system)("ubus send pubsub '{\"state\":\"%s\"}' &", ST_PSK_CHANGED);
            }
            else
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("Could not remove admin"));
            }
        }
        else if(!strcmp(usertype.value, ST_USER))
        {
            searching_database("pubsub_handler", user_db, get_last_data_cb, &usertype, 
                    "SELECT userType from USERS where userId='%s'", userId2);

            if(!strcmp(usertype.value, ST_USER) && !strcmp(userId1, userId2))
            {
                VR_(remove_deny_timer)(userId2);

                database_actions("pubsub_handler", user_db, 
                        "DELETE from USERS where userId='%s'", userId2);

                json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId2));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

                char psk[SIZE_256B];
                VR_(generate_128bit)(psk);
                VR_(write_option)(NULL, UCI_LOCAL_PSK"=%s", psk);
                VR_(execute_system)("ubus send pubsub '{\"state\":\"%s\"}' &", ST_PSK_CHANGED);
            }
            else
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("you are not allowed to do this"));
            }
        }
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
        FREE_SEARCH_DATA_VAR(usertype);

        json_object *notify_jobj = json_object_new_object();
        json_object_object_add(notify_jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
        json_object_object_add(notify_jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_PSK_CHANGED));
        sendMsgtoLocalService((char*)ST_SEND_NOTIFY, (char *)json_object_to_json_string(notify_jobj));
        json_object_put(notify_jobj);
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_DENY_ACCESS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
    }

    SAFE_FREE(userId1);
    SAFE_FREE(userId2);
    SAFE_FREE(topic);
}

static int listuser_callback(void *userlist, int argc, char **argv, char **azColName)
{
   //  int i;
   // for(i=0; i<argc; i++){

   //    printf("%s = %s | ", azColName[i], argv[i] ? argv[i] : "NULL");
   //    //if(data != NULL) strcpy(data, argv[i]);
   // }
   // printf("\n");
    int i;
    json_object * jobj = json_object_new_object();
    for(i=0; i<argc; i++)
    {
        json_object_object_add(jobj, azColName[i], json_object_new_string(argv[i] ? argv[i] : "NULL"));
    }

    json_object_array_add(userlist, jobj);
    return 0;
}

void VR_(list_users)(const char *service_type, const char *msg)
{
    SLOGI("list_users\n");

    char* userId = VR_(get_string_data)(msg, ST_ADMIN_ID);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    SLOGI("userId = %s\n", userId);

    if(userId && strlen(userId))
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_LIST_USERS_R));
        SEARCH_DATA_INIT_VAR(usertype);
        searching_database("pubsub_handler", user_db, get_last_data_cb, &usertype, 
                    "SELECT userType from USERS where userId='%s'", userId);

        if(!strcmp(usertype.value, ST_ADMIN))
        {
            json_object *userlist = json_object_new_array();
            searching_database("pubsub_handler", user_db, listuser_callback, (char *)userlist, 
                    "SELECT userId,shareId,name,enable,shareTime,options from USERS where userType='%s'", ST_USER);
            json_object_object_add(jobj, ST_USER_LIST, userlist);
        }
        else if(!strcmp(usertype.value, ST_USER))
        {
            json_object *userlist = json_object_new_array();
            searching_database("pubsub_handler", user_db, listuser_callback, (char *)userlist, 
                    "SELECT userId,shareId,name,enable,shareTime,options from USERS where userId='%s'", userId);
            json_object_object_add(jobj, ST_USER_LIST, userlist);
        }
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
        FREE_SEARCH_DATA_VAR(usertype);
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_LIST_USERS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
    }

    SAFE_FREE(userId);
    SAFE_FREE(topic);
}

static void upload_log_thread(void *data)
{
    size_t log_length = get_file_length(DEFAULT_LOG_LOCATION);
    if(log_length > LOG_SIZE_ROTATE)
    {
        clean_file_content(DEFAULT_LOG_LOCATION);
        SLOGI("file log to large size %d, shoule be remove\n", log_length);
        return;
    }

    int rc = VR_(httpUploadFile)(DEFAULT_LOG_LOCATION);
    SLOGI("############ auto_upload_cb rc = %d ###########\n", rc);

    if(rc)
    {
        char *log_count = VR_(read_option)(NULL,"security.@remote-access[0].log_count");
        if(log_count)
        {
            int tmp = strtol(log_count,NULL,0);
            if(tmp == 10)
            {
                clean_file_content(DEFAULT_LOG_LOCATION);
            }
            else
            {
                tmp++;
                VR_(write_option)(NULL,"security.@remote-access[0].log_count=%d", tmp);
            }
            free(log_count);
        }
        else
        {
            clean_file_content(DEFAULT_LOG_LOCATION);
            VR_(write_option)(NULL,"security.@remote-access[0].log_count=0");
        }
    }
    else
    {
        clean_file_content(DEFAULT_LOG_LOCATION);
        VR_(write_option)(NULL,"security.@remote-access[0].log_count=0");
    }
}

void VR_(send_report)(const char *service_type, const char *msg)
{
    SLOGI("send_report");
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SEND_REPORT_R));

    char cmd[SIZE_64B];
    snprintf(cmd, sizeof(cmd), "ps -w >> %s", DEFAULT_LOG_LOCATION);
    VR_(execute_system)(cmd);
    snprintf(cmd, sizeof(cmd), "cat /proc/meminfo >> %s", DEFAULT_LOG_LOCATION);
    VR_(execute_system)(cmd);
    snprintf(cmd, sizeof(cmd), "df >> %s", DEFAULT_LOG_LOCATION);
    VR_(execute_system)(cmd);

    pthread_t send_report_t;
    pthread_create(&send_report_t, NULL, (void *)&upload_log_thread, NULL);
    pthread_detach(send_report_t);

    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

    VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
    json_object_put(jobj);

    SAFE_FREE(topic);
}

void timer_scanwifi(void *data)
{
    SLOGI("start rescan wifi\n");
    VR_(execute_system)("/usr/sbin/wifi_scan");
    g_timer_scanwifi=0;
    SLOGI("end rescan wifi\n");
}

void VR_(wifi_settings)(const char *service_type, const char *msg)
{
    SLOGI("wifi_settings\n");
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);
    char* userid = VR_(get_string_data)(msg, ST_USER_ID);
    char* mode = VR_(get_string_data)(msg, ST_MODE);
    char* ssid = VR_(get_string_data)(msg, ST_SSID);
    char* password = VR_(get_string_data)(msg, ST_PASSWORD);
    char* encryption = VR_(get_string_data)(msg, ST_ENCRYPTION);
    char* channel = VR_(get_string_data)(msg, ST_CHANNEL);

    SLOGI("userid = %s\n", userid);
    SLOGI("mode = %s\n", mode);

#ifdef SHARE_USER_LIMIT
    char *local_state = VR_(read_option)(NULL, UCI_LOCAL_STATE);
    if(!local_state)
    {
        goto wifi_setfailed;
    }


    if(!strcmp(local_state, ST_CONNECTED))
    {
        if(userid && strlen(userid))
        {
            SEARCH_DATA_INIT_VAR(usertype);
            searching_database("alljoyn_handler", user_db, get_last_data_cb, &usertype,
                        "SELECT userType from USERS where userId='%s'", userid);
            SLOGI("usertype.value = %s\n", usertype.value);
            if(strcmp(usertype.value, ST_ADMIN))
            {
                if(!strcmp(mode, ST_STA) || !strcmp(mode, ST_AP))
                {
                    free(local_state);
                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    json_object_object_add(jobj, ST_REASON, json_object_new_string("you are not allow to do this"));
                    VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
                    json_object_put(jobj);
                    FREE_SEARCH_DATA_VAR(usertype);
                    goto wifi_setfailed;
                }
            }
            FREE_SEARCH_DATA_VAR(usertype);
        }
        else
        {
            free(local_state);
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
            VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
            goto wifi_setfailed;
        }
        free(local_state);
    }
#endif

    if( mode && strlen(mode))
    {
        if(!strcmp(mode, ST_SCAN))
        {
            if(!g_timer_scanwifi)
            {
                timerStart(&g_timer_scanwifi, timer_scanwifi, NULL, 3000, TIMER_ONETIME);
            }

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
            json_object_object_add(jobj, ST_MODE, json_object_new_string(mode));
            
            json_object *scanlist = json_object_new_array();
            FILE *fp;
            char * line = NULL;
            size_t len = 0;
            ssize_t read;
            fp = fopen("/tmp/wifi_scan_results", "r");
            if(!fp)
            {
                return;
            }
            while ((read = getline(&line, &len, fp)) != -1)
            {
                char *pos;
                if ((pos=strchr(line, '\n')) != NULL)
                    *pos = '\0';
                char *save_tok;
                char* ssid_f = strtok_r((char*)line, "\t", &save_tok);
                char* auth_f = strtok_r(NULL, "\t", &save_tok);
                if (!ssid_f) 
                {
                    continue;
                }

                char encryption_s[SIZE_64B];
                char* authType = strtok_r(auth_f, "-", &save_tok);
                char* firstCipher = strtok_r(NULL, "-", &save_tok);
                char* secondCipher = strtok_r(NULL, "-", &save_tok);

                if (!authType) 
                {
                    continue;
                }

                if (!strcmp(authType, "WEP")) 
                {
                    SAFE_STRCPY(encryption_s, "wep");
                } 
                else if (!strcmp(authType, "Open")) 
                {
                    SAFE_STRCPY(encryption_s, "open");
                } 
                else if (!strcmp(authType, "WPA2")) 
                {
                    SAFE_STRCPY(encryption_s, "psk2");
                } 
                else if (!strcmp(authType, "WPA")) 
                {
                    SAFE_STRCPY(encryption_s, "psk");
                }
                if(!strcmp(encryption_s,"psk") || !strcmp(encryption_s,"psk2"))
                {
                    if(!firstCipher || !strcmp(firstCipher, "PSK"))
                    {
                        continue;
                    }
                    else if (!strcmp(firstCipher, "TKIP"))
                    {
                        strcat(encryption_s, "+tkip");
                        if(secondCipher && !strcmp(secondCipher, "CCMP"))
                        {
                            strcat(encryption_s, "+ccmp");
                        }
                    }
                    else if (!strcmp(firstCipher, "CCMP"))
                    {
                        if(secondCipher && !strcmp(secondCipher, "TKIP"))
                        {
                            strcat(encryption_s, "+tkip+ccmp");
                        }
                        else
                        {
                            strcat(encryption_s, "+ccmp");
                        }
                    }
                }

                json_object *ssid_obj = json_object_new_object();
                json_object_object_add(ssid_obj, ST_SSID, json_object_new_string(ssid_f));
                json_object_object_add(ssid_obj, ST_AUTH, json_object_new_string(encryption_s));
                json_object_array_add(scanlist, ssid_obj);
            }
            if(line)
                free(line);
            fclose(fp);

            json_object_object_add(jobj, ST_SCAN_LIST, scanlist);
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
        }
        else if(!strcmp(mode, ST_GET))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
            json_object_object_add(jobj, ST_MODE, json_object_new_string(mode));
            json_object *apmode = json_object_new_object();
            char *wifi_mode = VR_(read_option)(NULL, UCI_WIFI_STA_MODE);

            char *apssid = VR_(read_option)(NULL, UCI_HUB_DEFAULT_NAME);
            if(apssid)
            {
                json_object_object_add(apmode, ST_SSID, json_object_new_string(apssid));
                free(apssid);
            }

            char *apencryption = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_APENCRYPTION);
            if(apencryption)
            {
                json_object_object_add(apmode, ST_ENCRYPTION, json_object_new_string(apencryption));
                free(apencryption);
            }

            char *apkey = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_APKEY);
            if(apkey)
            {
                char b64_encode[SIZE_256B];
                psk_encrypt(userid, apkey, b64_encode);
                json_object_object_add(apmode, ST_KEY, json_object_new_string(b64_encode));
                free(apkey);
            }

            if(wifi_mode)
            {
                if(!strcmp(wifi_mode, ST_AP))
                {
                    json_object_object_add(apmode, ST_STATE, json_object_new_string("active"));
                }
                else if(!strcmp(wifi_mode, ST_STA))
                {
                    json_object_object_add(apmode, ST_STATE, json_object_new_string("inactive"));
                }
            }
            else
            {
                json_object_object_add(apmode, ST_STATE, json_object_new_string("unknown"));
            }

            json_object *stamode = json_object_new_object();
            char *ssid = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_SSID);
            if(ssid)
            {
                json_object_object_add(stamode, ST_SSID, json_object_new_string(ssid));
                free(ssid);
            }
            char *encryption = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_ENCRYPTION);
            if(encryption)
            {
                json_object_object_add(stamode, ST_ENCRYPTION, json_object_new_string(encryption));
                free(encryption);
            }
            char *key = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_KEY);
            if(key)
            {
                char b64_encode[SIZE_256B];
                psk_encrypt(userid, key, b64_encode);
                json_object_object_add(stamode, ST_KEY, json_object_new_string(b64_encode));
                free(key);
            }
            if(wifi_mode)
            {
                if(!strcmp(wifi_mode, ST_AP))
                {
                    json_object_object_add(stamode, ST_STATE, json_object_new_string("inactive"));
                }
                else if(!strcmp(wifi_mode, ST_STA))
                {
                    json_object_object_add(stamode, ST_STATE, json_object_new_string("active"));
                }
            }
            else
            {
                json_object_object_add(stamode, ST_STATE, json_object_new_string("unknown"));
            }

            if(wifi_mode)
            {
                free(wifi_mode);
            }

            json_object_object_add(jobj, ST_AP_MODE, apmode);
            json_object_object_add(jobj, ST_STA_MODE, stamode);
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
            json_object_put(jobj);
        }
        else
        {
            if( ssid && strlen(ssid) &&
                password && strlen(password) &&
                encryption && strlen(encryption) &&
                channel && strlen(channel)
                )
            {
                if(!strcmp(mode, ST_AP))
                {
                    VR_(write_option)(NULL, UCI_HUB_NAME"=%s", ssid);
                    VR_(write_option)(NULL, UCI_HUB_ENCRYPTION"=%s", encryption);
                    VR_(write_option)(NULL, UCI_HUB_KEY"=%s", password);
                    VR_(write_option)(NULL, UCI_HUB_CHANNEL"=%s", channel);

                    if(strlen(ssid) > 32)
                    {
                        char tmp[SIZE_64B];
                        strncpy(tmp, ssid, SIZE_32B);
                        tmp[SIZE_32B] = '\0';
                        VR_(write_option)(NULL, UCI_HUB_DEFAULT_NAME"=%s", tmp);
                    }
                    else
                    {
                        VR_(write_option)(NULL, UCI_HUB_DEFAULT_NAME"=%s", ssid);
                    }

                    VR_(write_option)(NULL, UCI_ALLJOYN_ONBOARDING_APENCRYPTION"=%s", encryption);
                    VR_(write_option)(NULL, UCI_ALLJOYN_ONBOARDING_APKEY"=%s", password);
                }
                else if(!strcmp(mode, ST_STA))
                {
                    // VR_(update_hub_infor)(NULL, ssid);
                    VR_(write_option)(NULL, UCI_ALLJOYN_ONBOARDING_SSID"=%s", ssid);
                    VR_(write_option)(NULL, UCI_ALLJOYN_ONBOARDING_ENCRYPTION"=%s", encryption);
                    VR_(write_option)(NULL, UCI_ALLJOYN_ONBOARDING_KEY"=%s", password);
                    VR_(write_option)("/etc/alljoyn-onboarding", UCI_ALLJOYN_ONBOARDING_STATE"=1");
                    VR_(write_option)(NULL, UCI_CLOUD_UPDATE_SSID"=%s", ST_FALSE);
                    VR_(execute_system)("ubus send onboarding '{\"state\":\"configured\"}' &");
                }
                else
                {
                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    json_object_object_add(jobj, ST_REASON, json_object_new_string("wrong mode"));
                    VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
                    json_object_put(jobj);
                }

                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
                json_object_object_add(jobj, ST_MODE, json_object_new_string(mode));
                json_object_object_add(jobj, ST_SSID, json_object_new_string(ssid));
                json_object_object_add(jobj, ST_PASSWORD, json_object_new_string(password));
                json_object_object_add(jobj, ST_ENCRYPTION, json_object_new_string(encryption));
                json_object_object_add(jobj, ST_CHANNEL, json_object_new_string(channel));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
                sleep(3);
                if(!strcmp(mode, "sta"))
                {
                    VR_(execute_system)("/etc/init.d/alljoyn-onboarding restart");
                }
                json_object_put(jobj);
            }
            else
            {
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
                VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
                json_object_put(jobj);
            }
        }     
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
    }

// wifi_setfailed:
    SAFE_FREE(topic);
    SAFE_FREE(userid);
    SAFE_FREE(mode);
    SAFE_FREE(ssid);
    SAFE_FREE(password);
    SAFE_FREE(encryption);
    SAFE_FREE(channel);
}

void VR_(rule_manual)(const char *service_type, const char *msg)
{
    char* actions = VR_(get_string_data)(msg, ST_ACTION);
    char* rule_id = VR_(get_string_data)(msg, ST_RULE_ID);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    SLOGI("rule_id = %s\n", rule_id);
    SLOGI("actions = %s\n", actions);

    if(rule_id && strlen(rule_id))
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RULE_MANUAL_R));
        json_object_object_add(jobj, ST_RULE_ID, json_object_new_string(rule_id));

        SEARCH_DATA_INIT_VAR(dbActions);
        searching_database("pubsub_handler", rule_db, get_last_data_cb, 
                        &dbActions, "SELECT actions from RULES where id='%s'", rule_id);
        if(dbActions.len)
        {
            rule_report_timeline(rule_id);
            _trigger_actions_(rule_id, dbActions.value, TRIGGER_MODE);
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
        }
        else
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("rule not found"));
        }

        FREE_SEARCH_DATA_VAR(dbActions);

        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
    }
    else if(actions && strlen(actions))
    {
        size_t action_length = strlen(actions);
        char *command = (char *)malloc(action_length+SIZE_512B);
        memset(command, 0x00, action_length+SIZE_512B);
        sprintf(command, "%s", "/etc/rule_control.sh \"dontcare\" \"dontcare\" ");

        char *ch, *save_tok;
        ch = strtok_r(actions, "&&", &save_tok);
        while(ch != NULL)
        {
            printf("ch = %s\n", ch);
            sprintf(command+strlen(command), "%s%s%s", "\"",ch,";\" ");
            ch = strtok_r(NULL, "&&", &save_tok);
        }
        sprintf(command+strlen(command), "%s", " &");
        SLOGI("command = %s\n", command);
        VR_(execute_system)(command);
        free(command);

        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RULE_MANUAL_R));
        json_object_object_add(jobj, ST_ACTIONS, json_object_new_string(actions));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RULE_MANUAL_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing argument"));
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
    }

    SAFE_FREE(actions);
    SAFE_FREE(rule_id);
    SAFE_FREE(topic);
}

static int listgroup_callback(void *grouplist, int argc, char **argv, char **azColName)
{
    int i;
    char *groupId = NULL;
    json_object * jobj = json_object_new_object();
    for(i=0; i<argc; i++)
    {
        if(!strcmp(azColName[i], ST_GROUP_ID))
        {
            groupId = argv[i];
        }

        if(!strcmp(azColName[i], ST_DEVICE))
        {
            if(argv[i])
            {
                json_object *devices = VR_(create_json_object)(argv[i]);
                if(devices)
                {
                    json_object_object_add(jobj, azColName[i], devices);
                }
            }
        }
        else
        {
            json_object_object_add(jobj, azColName[i], json_object_new_string(argv[i] ? argv[i] : "NULL"));
        }
    }

    if(groupId)
    {
        SEARCH_DATA_INIT_VAR(resourceId);
        searching_database("pubsub_handler", dev_db, get_last_data_cb, &resourceId,
                        "SELECT owner from DEVICES where deviceId='%s'", 
                        groupId);
        if(resourceId.len)
        {
            json_object_object_add(jobj, ST_CLOUD_RESOURCE, json_object_new_string(resourceId.value));
        }
        FREE_SEARCH_DATA_VAR(resourceId);
    }

    json_object_array_add(grouplist, jobj);
    
    return 0;
}

void VR_(list_groups)(const char *service_type, const char *msg)
{
    SLOGI("list_groups\n");
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);

    SLOGI("topic = %s\n", topic);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_LIST_GROUPS_R));
    json_object *grouplist = json_object_new_array();

    searching_database("pubsub_handler", dev_db, listgroup_callback, (void *)grouplist,
                            "SELECT * from GROUPS");

    json_object_object_add(jobj, ST_GROUP_LIST, grouplist);
    VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
    json_object_put(jobj);
    SAFE_FREE(topic);
}

void VR_(venus_alarm)(const char *service_type, const char *msg)
{
    SLOGI("venus_alarm\n");

    char* topic = VR_(get_string_data)(msg, ST_TOPIC);
    char* userId = VR_(get_string_data)(msg, ST_USER_ID);
    char* devId = VR_(get_string_data)(msg, ST_DEVICE_ID);
    char* value = VR_(get_string_data)(msg, ST_VALUE);

    SLOGI("userId = %s\n", userId);
    SLOGI("devId = %s\n", devId);
    SLOGI("value = %s\n", value);
    if(!userId || !strlen(userId) ||
        !devId || !strlen(devId) ||
        !value || !strlen(value)
        )
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_VENUS_ALARM_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
        goto set_alarm_done;
    }

#ifdef SHARE_USER_LIMIT
    char *local_state = (char *)VR_(read_option)(NULL, UCI_LOCAL_STATE);
    if(local_state)
    {
        if(!strcmp(local_state, ST_CONNECTED))
        {
            SEARCH_DATA_INIT_VAR(usertype);
            searching_database("pubsub_handler", user_db, get_last_data_cb, &usertype,
                        "SELECT userType from USERS where userId='%s'", userId);

            if(usertype.len && strcmp(usertype.value, ST_ADMIN))
            {
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_VENUS_ALARM_R));
                json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
                json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(devId));
                json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("you are not allow to do this"));
                VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
                json_object_put(jobj);
                FREE_SEARCH_DATA_VAR(usertype);
                return;
            }
            FREE_SEARCH_DATA_VAR(usertype);
        }

        free(local_state);
    }
#endif

    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));
    SAFE_STRCPY(pubsub_ubus->name, ST_VENUS_ALARM);
    SLOGI("topic = %s\n", topic);
    if(topic)
    {
        SAFE_STRCPY(pubsub_ubus->topic, topic);
    }

    VR_(add_last_list)(pubsub_ubus);

    int i;
    pubsub_ubus->type = 0;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        SLOGI("daemon_services[%d].name = %s id = %08X\n", i, daemon_services[i].name, daemon_services[i].id);
        if(!strcmp(service_type, daemon_services[i].name))
        {
            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, devId);
                blobmsg_add_string(&buff, ST_VALUE, value);

                pubsub_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_VENUS_ALARM, buff.head, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                break;
            }
        }
    }
set_alarm_done:
    SAFE_FREE(topic);
    SAFE_FREE(userId);
    SAFE_FREE(devId);
    SAFE_FREE(value);
}

void VR_(venus_alarm_v2)(const char *service_type, const char *msg)
{
    SLOGI("venus_alarm version 2\n");

    char* topic = VR_(get_string_data)(msg, ST_TOPIC);
    char* userId = VR_(get_string_data)(msg, ST_USER_ID);
    char* devId = VR_(get_string_data)(msg, ST_DEVICE_ID);
    char* value = VR_(get_string_data)(msg, ST_VALUE);
    char* temporaryTime = VR_(get_string_data)(msg, ST_TEMPORARY_TIME);
    SLOGI("userId = %s\n", userId);
    SLOGI("devId = %s\n", devId);
    SLOGI("value = %s\n", value);
    SLOGI("temporaryTime = %s\n", temporaryTime);
    if(!userId || !strlen(userId) ||
        !devId || !strlen(devId) ||
        !value || !strlen(value)
        )
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_VENUS_ALARM_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
        goto set_alarm_done;
    }

#ifdef SHARE_USER_LIMIT
    char *local_state = (char *)VR_(read_option)(NULL, UCI_LOCAL_STATE);
    if(local_state)
    {
        if(!strcmp(local_state, ST_CONNECTED))
        {
            SEARCH_DATA_INIT_VAR(usertype);
            searching_database("pubsub_handler", user_db, get_last_data_cb, &usertype,
                        "SELECT userType from USERS where userId='%s'", userId);

            if(usertype.len && strcmp(usertype.value, ST_ADMIN))
            {
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_VENUS_ALARM_R));
                json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
                json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(devId));
                json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
                if(temporaryTime)
                {
                    json_object_object_add(jobj, ST_TEMPORARY_TIME, json_object_new_string(temporaryTime));
                }
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("you are not allow to do this"));
                VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
                json_object_put(jobj);
                FREE_SEARCH_DATA_VAR(usertype);
                return;
            }
            FREE_SEARCH_DATA_VAR(usertype);
        }

        free(local_state);
    }
#endif

    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));
    SAFE_STRCPY(pubsub_ubus->name, ST_VENUS_ALARM_V2);
    SLOGI("topic = %s\n", topic);
    if(topic)
    {
        SAFE_STRCPY(pubsub_ubus->topic, topic);
    }

    VR_(add_last_list)(pubsub_ubus);

    int i;
    pubsub_ubus->type = 0;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        SLOGI("daemon_services[%d].name = %s id = %08X\n", i, daemon_services[i].name, daemon_services[i].id);
        if(!strcmp(service_type, daemon_services[i].name))
        {
            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, devId);
                blobmsg_add_string(&buff, ST_VALUE, value);
                if(temporaryTime)
                {
                    blobmsg_add_string(&buff, ST_TEMPORARY_TIME, temporaryTime);
                }

                pubsub_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_VENUS_ALARM_V2, buff.head, req,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                break;
            }
        }
    }
set_alarm_done:
    SAFE_FREE(topic);
    SAFE_FREE(userId);
    SAFE_FREE(devId);
    SAFE_FREE(value);
    SAFE_FREE(temporaryTime);
}

// static void active_scene_thread(void *data)
// {
//     if(!data)
//     {
//         return;
//     }

//     scene_infor *scene_info = (scene_infor*)data;
//     VR_(scene_trigger)(scene_info->deviceId, scene_info->buttonId);
//     free(scene_info);
// }

static void VR_(scene_activation)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    char* id = VR_(get_string_data)(msg, ST_DEVICE_ID);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);
    char* userId = VR_(get_string_data)(msg, ST_USER_ID);
    char* buttonId = VR_(get_string_data)(msg, ST_BUTTON_ID);

    if(!id || !userId || !buttonId)
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SCENE_ACTIVATION_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
        goto done;
    }

    new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
    memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));
    SAFE_STRCPY(pubsub_ubus->name, ST_SCENE_ACTIVATION);
    if(topic)
    {
        SAFE_STRCPY(pubsub_ubus->topic, topic);
    }

    VR_(add_last_list)(pubsub_ubus);

    int i;
    pubsub_ubus->type = 0;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        SLOGI("daemon_services[%d].name = %s id = %08X\n", i, daemon_services[i].name, daemon_services[i].id);
        if(!strcmp(service_type, daemon_services[i].name))
        {
            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                blobmsg_add_string(&buff, ST_BUTTON_ID, buttonId);

                pubsub_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                pubsub_ubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_SCENE_ACTIVATION, buff.head, req,
                                    VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                break;
            }
        }
    }

    // scene_infor *scene_info = (scene_infor*)malloc(sizeof(scene_infor));
    // snprintf(scene_info->deviceId, sizeof(scene_info->deviceId), "%s", id);
    // snprintf(scene_info->buttonId, sizeof(scene_info->buttonId), "%s", buttonId);

    // pthread_t active_scene_t;
    // pthread_create(&active_scene_t, NULL, (void *)&active_scene_thread, (void *)scene_info);
    // pthread_detach(active_scene_t);
done:
    SAFE_FREE(id);
    SAFE_FREE(topic);
    SAFE_FREE(userId);
    SAFE_FREE(buttonId);
}

static void VR_(scene_actions)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    char* id = VR_(get_string_data)(msg, ST_DEVICE_ID);
    char* action = VR_(get_string_data)(msg, ST_ACTION);
    char* topic = VR_(get_string_data)(msg, ST_TOPIC);
    char* sceneId = VR_(get_string_data)(msg, ST_SCENE_ID);
    char* buttonId = VR_(get_string_data)(msg, ST_BUTTON_ID);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SCENE_ACTIONS_R));

    if(!id || !action || !sceneId || !buttonId)
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        goto done;
    }

    if(!strcmp(action, ST_PAIR))
    {
        update_senceId_database("pubsub_handler", dev_db, id, buttonId, sceneId);
    }
    else if(!strcmp(action, ST_UNPAIR))
    {
        update_senceId_database("pubsub_handler", dev_db, id, buttonId, "");
    }

    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(id));
    json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
    json_object_object_add(jobj, ST_BUTTON_ID, json_object_new_string(buttonId));
    json_object_object_add(jobj, ST_SCENE_ID, json_object_new_string(sceneId));
    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

done:
    Broadcast_message(VR_QOS, (char *)json_object_to_json_string(jobj), 0);
    json_object_put(jobj);

    SAFE_FREE(id);
    SAFE_FREE(topic);
    SAFE_FREE(action);
    SAFE_FREE(sceneId);
    SAFE_FREE(buttonId);
}

static int list_status_callback(void *data, int argc, char **argv, char **azColName)
{
    if(!data)
    {
        return 0;
    }

    int i;
    char deviceId[SIZE_64B];
    char deviceType[SIZE_64B];

    status_data *tmp = (status_data*)data;

    for(i=0; i<argc; i++)
    {
        if(!strcmp(azColName[i], ST_ID) || !strcmp(azColName[i], ST_UDN))
        {
            strcpy(deviceId, argv[i]);
        }

        if(!strcmp(azColName[i], ST_TYPE))
        {
            strcpy(deviceType, argv[i]);
        }
    }

    json_object *jobj = json_object_new_object();
    json_object *status = json_object_new_object();

    json_object *capabilityObj = json_object_new_array();
    searching_database("pubsub_handler", (sqlite3 *)tmp->db, capability_callback, (void *)capabilityObj,
                    "SELECT %s,%s,%s from CAPABILITY where deviceId='%s'",
                    ST_SCHEME, ST_CAP_LIST, ST_ENDPOINT_MEM, deviceId);

    searching_database("pubsub_handler", (sqlite3*)tmp->db, features_callback, (void *)status,
                        "SELECT %s,%s from FEATURES where %s='%s'"
                        , ST_FEATURE_ID, ST_REGISTER,
                        ST_DEVICE_ID, deviceId);

    get_alarm_data((sqlite3 *)tmp->db, status, deviceId);
    int association=0, meter=0, sensorMultilevel=0, userCode=0, thermostatSetpoint=0;
    get_feature_device_support(capabilityObj, deviceType, &association,
                      &meter, &sensorMultilevel, &userCode, &thermostatSetpoint);
    if(association)
    {
        get_association_data((sqlite3 *)tmp->db, status, deviceId);
    }

    if(meter)
    {
        get_meter_data((sqlite3 *)tmp->db, status, deviceId);
    }

    if(sensorMultilevel)
    {
        get_sensor_multilevel_data((sqlite3 *)tmp->db, status, deviceId);
    }

    if(userCode)
    {
        get_door_lock_user_code((sqlite3 *)tmp->db, status, deviceId);
    }

    if(thermostatSetpoint)
    {
        get_thermostat_setpoint_data((sqlite3 *)tmp->db, status, deviceId);
    }

    json_object_object_add(jobj, ST_ID, json_object_new_string(deviceId));
    json_object_object_add(jobj, ST_STATUS, status);
    json_object_array_add((json_object*)tmp->object, jobj);
    json_object_put(capabilityObj);
    return 0;
}

static void VR_(get_status)(const char *service_type, const char *msg)
{

    SLOGI("The command %s is called \n ", __func__);

    char *topic = VR_(get_string_data)(msg, ST_TOPIC);

    char timestamp[SIZE_32B];
    unsigned int current_time = (unsigned)time(NULL);
    sprintf(timestamp, "%u", current_time);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(service_type));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_STATUS_R));
    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    json_object_object_add(jobj, ST_CLOUD_TIME_STAMP, json_object_new_string(timestamp));
    json_object *devicelist = json_object_new_array();

    char cmd[SIZE_256B];
    char database_file[SIZE_256B];
    snprintf(database_file, sizeof(database_file), TEMP_DEVICES_DATABASE, current_time);
    snprintf(cmd, sizeof(cmd), "cp %s %s", DEVICES_DATABASE, database_file);
    VR_(execute_system)(cmd);

    sqlite3 *tmp_dev_db;

    int rc = VR_(get_file_infor)(database_file);

    if(rc == -1)
    {
        tmp_dev_db = dev_db;
    }
    else
    {
        rc = open_database(database_file, &tmp_dev_db);
        if(rc == -1)
        {
            tmp_dev_db = dev_db;
        }
    }

    status_data data;
    data.object = (void*)devicelist;
    data.db = (void*)tmp_dev_db;

    DISABLE_DATABASE_LOG();

    if(!strcmp(service_type, ST_ALL))
    {
        searching_database("pubsub_handler", tmp_dev_db, list_status_callback, (void *)&data,
                        "SELECT udn from CONTROL_DEVS where type='%s'", ST_UPNP);

        searching_database("pubsub_handler", tmp_dev_db, list_status_callback, (char *)&data,
                    "SELECT %s,%s from SUB_DEVICES ", ST_ID, ST_TYPE);
    }
    else
    {
        //upnp has onOff status only.
        searching_database("pubsub_handler", tmp_dev_db, list_status_callback, (void *)&data,
                    "SELECT udn from CONTROL_DEVS where type='%s'", ST_UPNP);

        searching_database("pubsub_handler", tmp_dev_db, list_status_callback, (char *)&data,
                    "SELECT id,type from SUB_DEVICES where id in"
                    "(select distinct deviceId from FEATURES where featureId!='lastUpdate'"
                    "and deviceId not in (select distinct deviceId from FEATURES where featureId='lastUpdate'))");

        searching_database("pubsub_handler", tmp_dev_db, list_status_callback, (char *)&data,
                    "SELECT id,type from SUB_DEVICES where id in"
                    "(select distinct deviceId from FEATURES where featureId='lastUpdate'"
                    "and register>%s)", service_type);
    }

    ENABLE_DATABASE_LOG();

    if(rc != -1)
    {
        close_database(&tmp_dev_db);
    }

    sprintf(cmd, "rm %s", database_file);
    VR_(execute_system)(cmd);

    json_object_object_add(jobj, ST_DEVICES_LIST, devicelist);
    VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
    json_object_put(jobj);

    SAFE_FREE(topic);
}

static void VR_(setup)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    char *topic = VR_(get_string_data)(msg, ST_TOPIC);
    char *hubInput = VR_(get_string_data)(msg, ST_HUBS_LIST);

    json_object *hubsList = VR_(create_json_object)(hubInput);
    if(!hubsList)
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SETUP_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("missing input"));
        VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
        return;
    }

    json_object *tmp = json_object_new_object();
    json_object_object_add(tmp, ST_HUBS_LIST, hubsList);
    VR_(execute_system)("ubus send pubsub '%s'", json_object_to_json_string(tmp));

    json_object_put(tmp);
    SAFE_FREE(topic);
}

command_handler_t command_handler[] = {
    { .name = ST_CHANGE_NAME,            .handler = VR_(change_name)},
    { .name = ST_GET_NAME,               .handler = VR_(get_name)},
    { .name = ST_LIST_DEVICES,           .handler = VR_(list_device)},
    { .name = ST_OPEN_CLOSE_NETWORK,     .handler = VR_(open_close_network)},
    { .name = ST_SET_BINARY,             .handler = VR_(set_binary)},
    { .name = ST_GET_BINARY,             .handler = VR_(get_binary)},
    { .name = ST_RESET,                  .handler = VR_(reset)},
    { .name = ST_READ_SPEC,              .handler = VR_(read_spec)},
    { .name = ST_WRITE_SPEC,             .handler = VR_(write_spec)},
    { .name = ST_READ_S_SPEC,            .handler = VR_(read_s_spec)},
    { .name = ST_WRITE_S_SPEC,           .handler = VR_(write_s_spec)},
    { .name = ST_ADD_DEVICES,            .handler = VR_(add_devices)},
    { .name = ST_REMOVE_DEVICE,          .handler = VR_(remove_device)},
    { .name = ST_FIRMWARE_ACTIONS,       .handler = VR_(firmware_actions)},
    { .name = ST_IDENTIFY,               .handler = VR_(identify)},
    { .name = ST_CREATE_GROUP,           .handler = VR_(create_group)},
    { .name = ST_GROUP_ACTIONS,          .handler = VR_(group_actions)},
    { .name = ST_GROUP_CONTROL,          .handler = VR_(group_control)},
    // { .name = ST_SET_RULE,               .handler = VR_(set_rule)},
    // { .name = ST_GET_RULE,               .handler = VR_(get_rule)},
    { .name = ST_RULE_MANUAL,            .handler = VR_(rule_manual)},
    { .name = ST_SCENE_ACTIVATION,       .handler = VR_(scene_activation)},
    { .name = ST_SCENE_ACTIONS,          .handler = VR_(scene_actions)},
    // { .name = ST_RULE_ACTIONS,           .handler = VR_(rule_actions)},
    { .name = ST_SHARE_ACCESS,           .handler = VR_(share_access)},
    { .name = ST_DENY_ACCESS,            .handler = VR_(deny_access)},
    { .name = ST_LIST_USERS,             .handler = VR_(list_users)},
    { .name = ST_SEND_REPORT,            .handler = VR_(send_report)},
    { .name = ST_WIFI_SETTINGS,          .handler = VR_(wifi_settings)},
    { .name = ST_LIST_GROUPS,            .handler = VR_(list_groups)},
    { .name = ST_VENUS_ALARM,            .handler = VR_(venus_alarm)},
    { .name = ST_VENUS_ALARM_V2,         .handler = VR_(venus_alarm_v2)},
    { .name = ST_WRITE_SPEC_CRC,         .handler = VR_(write_spec_crc)},
    { .name = ST_NETWORK,                .handler = VR_(network)},
    { .name = ST_GET_STATUS,             .handler = VR_(get_status)},
    { .name = ST_SETUP,                  .handler = VR_(setup)},
};

void VR_(shadow_addDevice)(const char *action, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    int i = 0;

    SLOGI("action = %s\n", action);
    SLOGI("msg = %s\n", msg);

    if(!action || !msg)
    {
        return;
    }
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if((!strcmp(ST_ZIGBEE, daemon_services[i].name) || !strcmp(ST_ZWAVE, daemon_services[i].name)) 
            && daemon_services[i].id != 0)
        {
            new_pubsub_ubus *pubsub_ubus = (new_pubsub_ubus*)malloc(sizeof(new_pubsub_ubus));
            memset(pubsub_ubus, 0x00, sizeof(new_pubsub_ubus));

            SAFE_STRCPY(pubsub_ubus->name, ST_OPEN_CLOSE_NETWORK);
            pubsub_ubus->type = 1;

            pubsub_ubus->service_name = (void*)daemon_services[i].name;
            pubsub_ubus->action_time = (unsigned)time(NULL);
            VR_(add_last_list)(pubsub_ubus);
            blob_buf_init(&buff, 0);

            struct ubus_request *req_s = (struct ubus_request *)malloc(sizeof(struct ubus_request));
            pubsub_ubus->req=req_s;

            if(!strcasecmp(msg,ST_TRUE))
            {
                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_OPEN_NETWORK, buff.head, req_s,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                // VR_(execute_system)("ubus send pubsub '{\"state\":\"open_network\"}' &");
            }
            else if(!strcasecmp(msg,ST_FALSE))
            {
                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_CLOSE_NETWORK, buff.head, req_s,
                                        VR_(receive_data_cb), VR_(complete_data_cb), (void *) pubsub_ubus);
                // VR_(execute_system)("ubus send pubsub '{\"state\":\"close_network\"}' &");
            }
        }
    }

    json_object *jobj = json_object_new_object();
    json_object *state = json_object_new_object();
    json_object_object_add(state, action, json_object_new_string(msg));
    json_object_object_add(jobj, g_mqtt_clientId, state);
    VR_(send_shadow_reported)((char *)json_object_to_json_string(jobj));

    json_object_put(jobj);
}

thermostat_mode_convert_t thermostat_mode_convert[]=
{
    {ST_HEAT,                   ST_HEATING},
    {ST_COOL,                   ST_COOLING},
    {ST_FURNACE,                ST_FURNACE},
    {ST_DRY_AIR,                ST_DRY_AIR},
    {ST_MOIST_AIR,              ST_MOIST_AIR},
    {ST_AUTO_CHANGE_OVER,       ST_AUTO_CHANGE_OVER},
    {ST_ENERGY_SAVE_HEAT,       ST_ENERGY_SAVE_HEATING},
    {ST_ENERGY_SAVE_COOL,       ST_ENERGY_SAVE_COOLING},
    {ST_AWAY,                   ST_AWAY},
    {ST_FULL_POWER,             ST_FULL_POWER},
};

static const char *get_thermostat_mode_from_setpoint(const char *setPointMode)
{
    if(!setPointMode)
    {
        return NULL;
    }

    int i;
    for(i=0; i < sizeof(thermostat_mode_convert)/sizeof(thermostat_mode_convert_t); i++)
    {
        if(!strcmp(thermostat_mode_convert[i].setPointMode, setPointMode))
        {
            return thermostat_mode_convert[i].mode;
        }
    }

    return NULL;
}

static const char *get_thermostat_setpoint_from_mode(const char *mode)
{
    if(!mode)
    {
        return NULL;
    }

    int i;
    for(i=0; i < sizeof(thermostat_mode_convert)/sizeof(thermostat_mode_convert_t); i++)
    {
        if(!strcmp(thermostat_mode_convert[i].mode, mode))
        {
            return thermostat_mode_convert[i].setPointMode;
        }
    }

    return ST_COOLING;
}

void VR_(shadow_device_actions)(const char *devid, json_object *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    // int i = 0;

    SLOGI("devid = %s\n", devid);
    SLOGI("msg = %s\n", json_object_get_string(msg));

    if(!devid || !msg)
    {
        return;
    }

    char service_type[SIZE_32B];
    memset(service_type, 0x00, sizeof(service_type));

    char *local_id = VR_(get_localid_from_cloudid)(g_shm, (char*)devid, service_type);

    if(!local_id)
    {
        return;
    }

    SLOGI("local_id = %s\n", local_id);
    SLOGI("service_type = %s\n", service_type);

    json_object_object_foreach(msg, cmd, value_obj)
    {
        if(!strcmp(cmd,ST_CLOUD_ON_OFF))
        {
            if(!strcmp(service_type, ST_GROUP))
            {
                json_object *message = json_object_new_object();
                json_object_object_add(message, ST_USER_ID, json_object_new_string(ST_ALEXA));
                json_object_object_add(message, ST_GROUP_ID, json_object_new_string(local_id));
                json_object_object_add(message, ST_COMMAND, json_object_new_string(ST_ON_OFF));
                json_object_object_add(message, ST_VALUE, json_object_new_string(json_object_get_string(value_obj)));
                // json_object_object_add(message, ST_TOPIC, json_object_new_string(ST_SHADOW));
                VR_(group_control)(service_type, json_object_to_json_string(message));
                json_object_put(message);
            }
            else
            {
                json_object *message = json_object_new_object();
                json_object_object_add(message, ST_ID, json_object_new_string(local_id));
                json_object_object_add(message, ST_VALUE, json_object_new_string(json_object_get_string(value_obj)));
                json_object_object_add(message, ST_COMMAND, json_object_new_string(ST_ON_OFF));
                // json_object_object_add(message, ST_TOPIC, json_object_new_string(ST_SHADOW));
                VR_(set_binary)(service_type, json_object_to_json_string(message));
                json_object_put(message);
            }
        }
        else if(!strcmp(cmd,ST_CLOUD_DIM))
        {
            if(!strcmp(service_type, ST_GROUP))
            {
                json_object *message = json_object_new_object();
                json_object_object_add(message, ST_USER_ID, json_object_new_string(ST_ALEXA));
                json_object_object_add(message, ST_GROUP_ID, json_object_new_string(local_id));
                json_object_object_add(message, ST_COMMAND, json_object_new_string(ST_DIM));
                json_object_object_add(message, ST_VALUE, json_object_new_string(json_object_get_string(value_obj)));
                // json_object_object_add(message, ST_TOPIC, json_object_new_string(ST_SHADOW));
                VR_(group_control)(service_type, json_object_to_json_string(message));
                json_object_put(message);
            }
            else
            {
                json_object *message = json_object_new_object();
                json_object_object_add(message, ST_ID, json_object_new_string(local_id));
                json_object_object_add(message, ST_VALUE, json_object_new_string(json_object_get_string(value_obj)));
                json_object_object_add(message, ST_COMMAND, json_object_new_string(ST_DIM));
                // json_object_object_add(message, ST_TOPIC, json_object_new_string(ST_SHADOW));
                VR_(set_binary)(service_type, json_object_to_json_string(message));
                json_object_put(message);
            }
        }
        else if(!strcmp(cmd,ST_CLOUD_SET_TEMP))
        {
            json_object *value = NULL;
            json_object *unit = NULL;
            if(!json_object_object_get_ex(value_obj, ST_VALUE, &value))
            {
                continue;
            }

            if(!value)
            {
                continue;
            }

            if(!json_object_object_get_ex(value_obj, ST_UNIT, &unit))
            {
                continue;
            }

            if(!unit)
            {
                continue;
            }

            const char *setTempValue = json_object_get_string(value);
            const char *setTempUnit = json_object_get_string(unit);
            const char *setPointMode = NULL;
            char *therMode = NULL;

            json_object *message = json_object_new_object();
            json_object_object_add(message, ST_ID, json_object_new_string(local_id));
            json_object_object_add(message, ST_CLASS, json_object_new_string(ST_THERMOSTAT_SETPOINT));
            json_object_object_add(message, ST_COMMAND, json_object_new_string(ST_SET));
            
            SEARCH_DATA_INIT_VAR(thermostat_mode);
            SEARCH_DATA_INIT_VAR(thermostat_previous_mode);

            searching_database("pubsub_handler", dev_db, get_last_data_cb, &thermostat_mode, 
                                "SELECT register from FEATURES where deviceId='%s' AND featureId='%s'", 
                                local_id, ST_THERMOSTAT_MODE);

            searching_database("pubsub_handler", dev_db, get_last_data_cb, &thermostat_previous_mode,
                                "SELECT register from FEATURES where deviceId='%s' AND featureId='%s'",
                                local_id, ST_THERMOSTAT_PREVIOUS_MODE);

            if(thermostat_mode.len)
            {
                if(!strcmp(thermostat_mode.value, ST_OFF))
                {
                    if(thermostat_previous_mode.len && strcmp(thermostat_previous_mode.value, ST_OFF))
                    {
                        therMode = strdup(thermostat_previous_mode.value);
                        setPointMode = get_thermostat_setpoint_from_mode(thermostat_previous_mode.value);
                        if(setPointMode)
                        {
                            json_object_object_add(message, ST_DATA0, json_object_new_string(setPointMode));
                        }
                    }
                    else
                    {
                        therMode = strdup(ST_COOL);
                        json_object_object_add(message, ST_DATA0, json_object_new_string(ST_COOLING));
                    }
                }
                else
                {
                    setPointMode = get_thermostat_setpoint_from_mode(thermostat_mode.value);
                    if(setPointMode)
                    {
                        json_object_object_add(message, ST_DATA0, json_object_new_string(setPointMode));
                    }
                }
            }
            else
            {
                if(thermostat_previous_mode.len && strcmp(thermostat_previous_mode.value, ST_OFF))
                {
                    therMode = strdup(thermostat_previous_mode.value);
                    setPointMode = get_thermostat_setpoint_from_mode(thermostat_mode.value);
                    if(setPointMode)
                    {
                        json_object_object_add(message, ST_DATA0, json_object_new_string(setPointMode));
                    }
                }
                else
                {
                    therMode = strdup(ST_COOL);
                    json_object_object_add(message, ST_DATA0, json_object_new_string(ST_COOLING));
                }
            }
            FREE_SEARCH_DATA_VAR(thermostat_mode);
            FREE_SEARCH_DATA_VAR(thermostat_previous_mode);

            json_object_object_add(message, ST_DATA1, json_object_new_string(setTempValue));
            json_object_object_add(message, ST_DATA2, json_object_new_string(setTempUnit));
            // json_object_object_add(message, ST_TOPIC, json_object_new_string(ST_SHADOW));

            if(therMode)
            {
                json_object *message_mode = json_object_new_object();
                json_object_object_add(message_mode, ST_ID, json_object_new_string(local_id));
                json_object_object_add(message_mode, ST_CLASS, json_object_new_string(ST_THERMOSTAT_MODE));
                json_object_object_add(message_mode, ST_COMMAND, json_object_new_string(ST_SET));
                json_object_object_add(message_mode, ST_DATA0, json_object_new_string(therMode));
                json_object_object_add(message_mode, ST_DATA2, json_object_new_string("1"));
                VR_(write_spec)(service_type, json_object_to_json_string(message_mode));
                json_object_put(message_mode);
                SAFE_FREE(therMode);
            }

            VR_(write_spec)(service_type, json_object_to_json_string(message));
            json_object_put(message);
        }
        else if(!strcmp(cmd, ST_ACTIVE))
        {
            json_object *message = json_object_new_object();
            json_object_object_add(message, ST_ID, json_object_new_string(local_id));
            json_object_object_add(message, ST_CLASS, json_object_new_string(ST_THERMOSTAT_MODE));
            json_object_object_add(message, ST_COMMAND, json_object_new_string(ST_SET));

            const char *mode = json_object_get_string(value_obj);
            // if(!strcmp(mode, ST_ON))
            // {
            //     json_object_object_add(message, ST_DATA0, json_object_new_string(ST_ON));
            // }
            // else
            // {
                json_object_object_add(message, ST_DATA0, json_object_new_string(mode));
            // }
           
            json_object_object_add(message, ST_DATA2, json_object_new_string("1"));
            VR_(write_spec)(service_type, json_object_to_json_string(message));
            json_object_put(message);
        }
        else if(!strcmp(cmd, ST_FAN_MODE))
        {
            json_object *message = json_object_new_object();
            json_object_object_add(message, ST_ID, json_object_new_string(local_id));
            json_object_object_add(message, ST_CLASS, json_object_new_string(ST_THERMOSTAT_FAN_MODE));
            json_object_object_add(message, ST_COMMAND, json_object_new_string(ST_SET));

            const char *mode = json_object_get_string(value_obj);
            json_object_object_add(message, ST_DATA0, json_object_new_string(mode));
            json_object_object_add(message, ST_DATA2, json_object_new_string("1"));
            VR_(write_spec)(service_type, json_object_to_json_string(message));
            json_object_put(message);
        }
        else if(!strcmp(cmd, ST_COLOR))
        {
            json_object *message = json_object_new_object();

            json_object_object_add(message, ST_ID, json_object_new_string(local_id));
            json_object_object_add(message, ST_CLASS, json_object_new_string(ST_SWITCH_COLOR));
            json_object_object_add(message, ST_COMMAND, json_object_new_string(ST_SET));
            json_object_object_add(message, ST_DATA0, value_obj);

            VR_(write_spec)(service_type, json_object_to_json_string(message));
            json_object_object_del(message, ST_DATA0);
            json_object_put(message);
        }
    }

    SAFE_FREE(local_id);
}

command_handler_t shadow_command_handler[] = {
    // { .name = "change_name",            .handler = VR_(change_name)},
    // { .name = "list_devices",           .handler = VR_(list_device)},
    { .name = ST_CLOUD_ADD_DEVICE,     .handler = VR_(shadow_addDevice)},
    // { .name = "onoff",         .handler = VR_(shadow_onoff)},
    // { .name = "get_binary",             .handler = VR_(get_binary)},
    // { .name = "reset",                  .handler = VR_(reset)},
    // { .name = "read_spec",              .handler = VR_(read_spec)},
    // { .name = "write_spec",             .handler = VR_(write_spec)},
    // { .name = "read_s_spec",            .handler = VR_(read_s_spec)},
    // { .name = "write_s_spec",           .handler = VR_(write_s_spec)},
    // { .name = "add_devices",            .handler = VR_(add_devices)},
    // { .name = "remove_device",          .handler = VR_(remove_device)},
    // { .name = "firmware_actions",       .handler = VR_(firmware_actions)},
    // { .name = "identify",               .handler = VR_(identify)},
    // { .name = "create_group",           .handler = VR_(create_group)},
    // { .name = "group_actions",          .handler = VR_(group_actions)},
    // { .name = "group_control",          .handler = VR_(group_control)},
    // { .name = "set_rule",               .handler = VR_(set_rule)},
    // { .name = "get_rule",               .handler = VR_(get_rule)},
    // { .name = "rule_actions",           .handler = VR_(rule_actions)},
    // { .name = "share_access",           .handler = VR_(share_access)},
    // { .name = "deny_access",            .handler = VR_(deny_access)},
    // { .name = "list_users",             .handler = VR_(list_users)},
    // { .name = "send_report",            .handler = VR_(send_report)},

};

static bool VR_(is_shadow_topic)(char *topic)
{
    if(!strncmp(topic, SHADOW_TOPIC_HEADER, strlen(SHADOW_TOPIC_HEADER)))
    {
        return true;
    }
    else
    {
        return false;
    }
}

static bool VR_(is_notify_command)(const char *msg)
{
    //parsing message
    char *commandType = VR_(get_string_data)(msg, ST_TYPE);
    if (commandType == NULL)
    {
        return false;
    }
    else
    {
        if (!strcmp(commandType, ST_NOTIFY))
        {
            SAFE_FREE(commandType);
            return true;
        }
        else
        {
            SAFE_FREE(commandType);
            return false;
        }
    }
}

static command_handler_f VR_(command_handler_search_by_name)(const char *name)
{
    int i = 0;
    for (; i < sizeof(command_handler)/sizeof (command_handler_t); i++)
    {
        if (!strcmp(name, command_handler[i].name))
        {
            return command_handler[i].handler;
        }
    }

    return NULL;
}

static command_handler_f VR_(shadow_handler_search_by_name)(const char *name)
{
    int i = 0;
    for (; i < sizeof(shadow_command_handler)/sizeof (command_handler_t); i++)
    {
        printf("shadow_command_handler[i].name = %s\n", shadow_command_handler[i].name);
        if (!strcmp(name, shadow_command_handler[i].name))
        {
            return shadow_command_handler[i].handler;
        }
    }

    return NULL;
}

static void VR_(remote_send_bye_msg)()
{
    SLOGI("The system command %s is called \n ", __func__);
    //MosquittoPublishMsg("BYEBYE");  
}

static void auto_upload_cb(struct uloop_timeout *timeout)
{
    char cmd[SIZE_64B];
    snprintf(cmd, sizeof(cmd), "ps -w >> %s", DEFAULT_LOG_LOCATION);
    VR_(execute_system)(cmd);
    snprintf(cmd, sizeof(cmd), "cat /proc/meminfo >> %s", DEFAULT_LOG_LOCATION);
    VR_(execute_system)(cmd);
    snprintf(cmd, sizeof(cmd), "df >> %s", DEFAULT_LOG_LOCATION);
    VR_(execute_system)(cmd);
    
    pthread_t send_report_t;
    pthread_create(&send_report_t, NULL, (void *)&upload_log_thread, NULL);
    pthread_detach(send_report_t);
    
    uloop_timeout_set(timeout, 1*60*60*1000);
}

static void sync_ssid_to_cloud(void)
{
    char *update = VR_(read_option)(NULL, UCI_CLOUD_UPDATE_SSID);
    if(!update)
    {
        SLOGI("state update ssid not found\n");
        return;
    }

    if(!strcmp(update, ST_TRUE))
    {
        char ssid[SIZE_32B];
        get_hub_ssid(ssid, sizeof(ssid));
        if(!VR_(update_hub_infor)(NULL, ssid))
        {
            VR_(write_option)(NULL, UCI_CLOUD_UPDATE_SSID"=%s", ST_TRUE);
        }
    }
    free(update);
}

static struct uloop_timeout auto_upload = {
    .cb = auto_upload_cb,
};

void VR_(my_log_callback)(struct mosquitto *mosq, void *obj, int level, const char *str)
{
    SLOGI("%s\n", str);
}

void timer_diconnected_announce(void *data)
{
    if(g_is_disconnected)
    {
        VR_(execute_system)("ubus send pubsub '{\"state\":\"disconnected\"}' &");
    }
}

void VR_(mosquitto_disconnect_callback)(struct mosquitto *mosq, void *obj, int rc)
{
    SLOGI ("[DEBUG] %s, %d  the connection has been lost rc = %d \n", __func__, __LINE__, rc);
    if(!g_is_disconnected)
    {
        timerStart(&g_timer_disconnected, timer_diconnected_announce, NULL, 30000, TIMER_ONETIME);
        g_is_disconnected = 1;
    }
    
    // if(rc)
    // {
    //     int ret = mosquitto_reconnect(mosq);
    //     while(ret)
    //     {
    //         SLOGI("mosquitto_reconnect error return = %d\n", ret);
    //         sleep(10);
    //         ret = mosquitto_reconnect(mosq);
    //     }
    // }
    // else
    // {
    //     //g_remote_sevice_is_running = 0;
    //     //pthread_cancel(thread_mosquitto);
    //     mosquitto_loop_stop(mosq, 1);
    //     uloop_end();
    // }
}

void sync_all_from_cloud()
{
    /*should sync resources first*/
    sync_users_from_cloud();
    sync_all_resources_with_cloud(USE_ETAG);
    sync_rule_from_cloud(USE_ETAG);
    sync_all_groups_with_cloud(USE_ETAG);
    sync_ssid_to_cloud();
    /*should remove after 2.2.3*/
    update_sound_name();
}

void VR_(mosquitto_connect_callback)(struct mosquitto *mosq, void *obj, int result)
{
    if (!result)
    {
        g_is_disconnected = 0;
        
        if(g_timer_disconnected)
        {
            timerCancel(&g_timer_disconnected);
            g_timer_disconnected = 0;
        }

        pthread_t sync_with_cloud;
        pthread_create(&sync_with_cloud, NULL, (void *)&sync_all_from_cloud, NULL);
        pthread_detach(sync_with_cloud);

        mosquitto_subscribe(mosq, NULL, SUBSCRIBE_REQ_TOPIC, QOS1);
        mosquitto_subscribe(mosq, NULL, SUBSCRIBE_CONTROL_TOPIC, QOS1);
        mosquitto_subscribe(mosq, NULL, SUBSCRIBE_SETTING_TOPIC, QOS1);

        // mosquitto_subscribe(mosq, NULL, SUBSCRIBE_SHADOW_DELTA_TOPIC, 0);
        // mosquitto_subscribe(mosq, NULL, SUBSCRIBE_SHADOW_UPDATE_REJECTED_TOPIC, QOS1);//for debug
        // mosquitto_subscribe(mosq, NULL, SUBSCRIBE_SHADOW_UPDATE_ACCEPTED_TOPIC, QOS1);//for debug

        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_PUBSUB));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
        json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CONNECTED));
        VR_(Send_message)(PUBLISH_RES_TOPIC, VR_QOS, (char *)json_object_to_json_string(jobj), 0);

        json_object_put(jobj);

        if(!g_first_time_bootup)
        {
            g_first_time_bootup = 1;
        }

        VR_(execute_system)("ubus send pubsub '{\"state\":\"connected\"}' &");
    }
    else
    {
        SLOGI("[INFO] Connect to server with return code: %d \n", result);      
    }
}

// struct rule_accept_info{
//     char *ruleId;
//     int64_t timeStamp;
//     int state;
// };

// void rule_accept_thread(void *data)
// {
//     if(!data)
//     {
//         return;
//     }
    
//     struct rule_accept_info *ruleInfo = (struct rule_accept_info *)data;
//     if(RULE_SUCCESS == ruleInfo->state || RULE_EXIST == ruleInfo->state)
//     {
//         VR_(rule_accept)(ruleInfo->ruleId, ruleInfo->timeStamp, TRUE);
//     }
//     else
//     {
//         VR_(rule_accept)(ruleInfo->ruleId, ruleInfo->timeStamp, FALSE);
//     }

//     free(ruleInfo->ruleId);
//     free(ruleInfo);
// }

void send_rule_accept_response(char *ruleId, int64_t timeStamp, int state)
{
    if(ruleId)
    {
        if(RULE_SUCCESS == state || RULE_EXIST == state)
        {
            VR_(rule_accept)(ruleId, timeStamp, TRUE);
        }
        else
        {
            VR_(rule_accept)(ruleId, timeStamp, FALSE);
        }
        // struct rule_accept_info *ruleInfo = (struct rule_accept_info *)malloc(sizeof(struct rule_accept_info));
        // // ruleInfo->ruleId = strdup(ruleId);
        // // ruleInfo->timeStamp = timeStamp;
        // // ruleInfo->state = state;

        // pthread_t rule_accept_t;
        // pthread_create(&rule_accept_t, NULL, (void *)&rule_accept_thread, ruleInfo);
        // pthread_detach(rule_accept_t);
    }
}

void handle_rule_message(char *message)
{
    char *state = NULL;
    char *ruleId = NULL;
    char *ruleName = NULL;
    char *ruleType = NULL;
    char *condition = NULL;
    char *actions = NULL;
    int64_t timeStamp = 0;
    char *enabled = NULL;
    int res = 0;
    res = VR_(get_rule_infor)(g_shm, message, &state, &ruleId, &ruleName, 
                        &ruleType, &condition, &actions, &timeStamp,
                        &enabled);

    SLOGI("state = %s\n", state);
    SLOGI("ruleId = %s\n", ruleId);
    SLOGI("ruleName = %s\n", ruleName);
    SLOGI("condition = %s\n", condition);
    SLOGI("actions = %s\n", actions);
    SLOGI("timeStamp = %lld\n", timeStamp);
    SLOGI("enabled = %s\n", enabled);

    if(res)
    {
        send_rule_accept_response(ruleId, timeStamp, res);
        goto rule_done;
    }

    if(!state)
    {
        goto rule_done;
    }

    if(!strcmp(state, ST_CREATE))
    {
        res = set_rule(ruleType, ruleId, ruleName, condition, actions, enabled);
        send_rule_accept_response(ruleId, timeStamp, res);
    }
    else
    {
        res = rule_actions(state, ruleType, ruleId, ruleName, condition, actions, enabled);
        send_rule_accept_response(ruleId, timeStamp, res);
    }

rule_done:
    SAFE_FREE(state);
    SAFE_FREE(ruleId);
    SAFE_FREE(ruleName);
    SAFE_FREE(ruleType);
    SAFE_FREE(condition);
    SAFE_FREE(actions);
    SAFE_FREE(enabled);
}

void handle_resource_update(json_object *data)
{
    if(!data)
    {
        SLOGE("data is null\n");
        return;
    }

    CHECK_JSON_OBJECT_EXIST(cloudIdObj, data, ST_ID, resource_update_done);
    CHECK_JSON_OBJECT_EXIST(localIdObj, data, ST_CLOUD_LOCAL_ID, resource_update_done);
    CHECK_JSON_OBJECT_EXIST(serialIdObj, data, ST_CLOUD_SERIAL_ID, resource_update_done);
    CHECK_JSON_OBJECT_EXIST(nameObj, data, ST_NAME, resource_update_done);

    const char *cloudId = json_object_get_string(cloudIdObj);
    const char *localId = json_object_get_string(localIdObj);
    const char *name = json_object_get_string(nameObj);

    database_actions("pubsub_handler", dev_db, 
                    "UPDATE SUB_DEVICES set %s='%s' where %s='%s'", 
                    ST_FRIENDLY_NAME, name, ST_ID, localId);

    char service_type[SIZE_32B];
    memset(service_type, 0x00, sizeof(service_type));

    char *tmp = VR_(get_localid_from_cloudid)(g_shm, (char*)cloudId, service_type);

    if(!tmp)
    {
        return;
    }

    if(strcmp(tmp, localId))
    {
        SAFE_FREE(tmp);
        return;
    }

    create_all_sound_files((char *)localId, service_type, (char *)name, GET_SOUND_ASYNCHONOUS);
    SAFE_FREE(tmp);

resource_update_done:
    return;
}

static void handle_user_update(const char *action, json_object *data)
{
    if(!action || !data)
    {
        return;
    }

    if(!strcmp(action, ST_CREATE))
    {
        const char *name = ST_UNKNOWN;
        json_object *nameObj = NULL;
        if(json_object_object_get_ex(data, ST_NAME, &nameObj))
        {
            name = json_object_get_string(nameObj);
        }

        CHECK_JSON_OBJECT_EXIST(userIdObj, data, ST_USER_ID, handle_user_done);
        CHECK_JSON_OBJECT_EXIST(shareIdObj, data, ST_ID, handle_user_done);
        CHECK_JSON_OBJECT_EXIST(userTypeObj, data, ST_ROLE, handle_user_done);

        const char *userId = json_object_get_string(userIdObj);
        const char *shareId = json_object_get_string(shareIdObj);
        const char *userType = json_object_get_string(userTypeObj);

        database_actions("pubsub_handler", user_db, 
                        "INSERT INTO USERS (userId,shareId,name,enable,userType) "\
                        "VALUES ('%s','%s','%s','%s','%s')", userId, shareId, name, ST_ENABLE, userType);

        json_object *publish_jobj = json_object_new_object();
        json_object_object_add(publish_jobj, ST_METHOD, json_object_new_string(ST_PUBLISH));
        json_object_object_add(publish_jobj, ST_MESSAGE, json_object_new_string(ST_ACCESS_CHANGED));
        sendMsgtoLocalService((char*)ST_SEND_PUBLISH, (char *)json_object_to_json_string(publish_jobj));
        json_object_put(publish_jobj);
    }
    else if(!strcmp(action, ST_DELETE))
    {
        CHECK_JSON_OBJECT_EXIST(userIdObj, data, ST_USER_ID, handle_user_done);
        const char *userId = json_object_get_string(userIdObj);

        database_actions("pubsub_handler", user_db, 
                        "DELETE from USERS where userId='%s'", userId);

        char psk[SIZE_256B];
        VR_(generate_128bit)(psk);
        VR_(write_option)(NULL, UCI_LOCAL_PSK"=%s", psk);
        VR_(execute_system)("ubus send pubsub '{\"state\":\"%s\"}' &", ST_PSK_CHANGED);

        json_object *notify_jobj = json_object_new_object();
        json_object_object_add(notify_jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
        json_object_object_add(notify_jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_PSK_CHANGED));
        sendMsgtoLocalService((char*)ST_SEND_NOTIFY, (char *)json_object_to_json_string(notify_jobj));
        json_object_put(notify_jobj);
    }

handle_user_done:
    return;
}

void handle_setting_message(char *message)
{
    if(!message)
    {
        SLOGE("message is null\n");
        return;
    }

    json_object *settingMessageObj = VR_(create_json_object)(message);
    if(!settingMessageObj)
    {
        SLOGE("message is not json format\n");
        return;
    }

    CHECK_JSON_OBJECT_EXIST(modelObj, settingMessageObj, ST_CLOUD_MODEL, setting_message_done);
    CHECK_JSON_OBJECT_EXIST(verbObj, settingMessageObj, ST_CLOUD_VERB, setting_message_done);
    CHECK_JSON_OBJECT_EXIST(dataObj, settingMessageObj, ST_CLOUD_DATA, setting_message_done);
    const char *model = json_object_get_string(modelObj);
    if(!strcmp(model, ST_RULE))
    {
        handle_rule_message((char *)json_object_to_json_string(dataObj));
    }
    else if(!strcmp(model, ST_CLOUD_RESOURCE))
    {
        const char *verb = json_object_get_string(verbObj);
        if(!strcmp(verb, ST_UPDATE))
        {
            handle_resource_update(dataObj);
        }
    }
    else if(!strcmp(model, ST_CLOUD_USER_HOME))
    {
        const char *verb = json_object_get_string(verbObj);
        handle_user_update(verb, dataObj);
    }

setting_message_done:
    json_object_put(settingMessageObj);  
    return;
}

void handle_control_message(char *message)
{
    if(!message)
    {
        SLOGE("message is null\n");
        return;
    }

    json_object *controlMessageObj = VR_(create_json_object)(message);
    if(!controlMessageObj)
    {
        SLOGE("message is not json format\n");
        return;
    }

    CHECK_JSON_OBJECT_EXIST(stateObj, controlMessageObj, ST_STATE, control_message_done);

    json_object_object_foreach(stateObj, uuid, value)
    {   
        CHECK_JSON_OBJECT_EXIST(docObj, value, ST_STATE, control_message_done);
        CHECK_JSON_OBJECT_EXIST(typeObj, value, ST_TYPE, control_message_done);

        const char *type = json_object_get_string(typeObj);

        if(!strcmp(type, ST_RULE))
        {    
            VR_(rule_be_triggered)(uuid, (char*)json_object_to_json_string(docObj));
        }    
        else if(!strcmp(type, ST_CLOUD_RESOURCE) || !strcmp(type, ST_CLOUD_RESOURCE_GROUP))
        {
            VR_(shadow_device_actions)(uuid, docObj);
        }
        else if (!strcmp(type, ST_DEVICE))
        {
            json_object_object_foreach(docObj, hub_action, act_obj)
            {
                command_handler_f func_handler = VR_(shadow_handler_search_by_name)(hub_action);
                if (func_handler != NULL)
                {
                    func_handler(hub_action, json_object_get_string(act_obj));
                }
            }
        }    
    }

control_message_done:
    json_object_put(controlMessageObj);  
    return;
}

void VR_(mosquitto_message_callback)(struct mosquitto *mosq_obj, void *obj, const struct mosquitto_message *message)
{
    SLOGI("Subscribe callback");
    SLOGI("%s\t%s", message->topic, message->payload);

    if(!strcmp(message->topic, SUBSCRIBE_CONTROL_TOPIC))
    {
        handle_control_message(message->payload);
        return;
    }

    if(!strcmp(message->topic, SUBSCRIBE_SETTING_TOPIC))
    {
        handle_setting_message(message->payload);
        return;
    }

    if(VR_(is_notify_command)(message->payload))
    {
        SLOGI("The notify command\n");
        return; 
    }

    char *type = VR_(get_string_data)(message->payload, ST_TYPE);
    char *method = VR_(get_string_data)(message->payload, ST_METHOD);
    SLOGI("method = %s\n", method);
    if(method)
    {
        command_handler_f func_handler = VR_(command_handler_search_by_name)(method);

        if (func_handler != NULL)
        {
            func_handler(type, message->payload);
        }
        else
        {
            SLOGI("Currently not support (type: method) :%s %s\n", type, method);
        }  
    }

    SAFE_FREE(type);
    SAFE_FREE(method);
}

// This function is called after subscribing to MQTT broker 
void VR_(mosquitto_subscribe_callback)(struct mosquitto *mosq, void *obj, int mid, int qos_count, const int *granted_qos)
{   
    SLOGI("Subscribed (mid: %d): %d\n", mid, granted_qos[0]);
    //g_remote_sevice.is_connected = 2; 
    if (mosq)
    {       
        // // Send alive message 
        // RemoteSendAliveMsg();
        // // For backward-compatible with previous function 
        // RemotePing(NULL);
    }
}

static int handle_ubus_notify_method(struct ubus_context *ctx, struct ubus_object *obj,
                            struct ubus_request_data *req, const char* method,
                            struct blob_attr *msg)
{
    char *msgstr = blobmsg_format_json(msg, true);
    json_object *jobj = VR_(create_json_object)(msgstr);
    if(!jobj)
    {
        SAFE_FREE(msgstr);
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_FAILED);
        ubus_send_reply(ctx, req, buff.head);
        return 0;
    }

    CHECK_JSON_OBJECT_EXIST(serviceObj, jobj, ST_SERVICE, notify_done);
    CHECK_JSON_OBJECT_EXIST(messageObj, jobj, ST_MESSAGE, notify_done);

    const char *service = json_object_get_string(serviceObj);
    const char *message = json_object_get_string(messageObj);

    if(!strcmp(service, ST_ALLJOYN))
    {
        VR_(Send_message)(NULL, VR_QOS, message, 0);
    }

notify_done:

    SAFE_FREE(msgstr);
    json_object_put(jobj);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
    ubus_send_reply(ctx, req, buff.head);

    //FIXME: check if 0 or -1 
    return 0;
}

static const struct blobmsg_policy write_spec_policy[]={
    [0] = { .name = ST_SERVICE, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_DEVICE_ID, .type = BLOBMSG_TYPE_STRING },
    [2] = { .name = ST_CLASS, .type = BLOBMSG_TYPE_STRING },
    [3] = { .name = ST_CMD, .type = BLOBMSG_TYPE_STRING },
    [4] = { .name = ST_DATA0, .type = BLOBMSG_TYPE_STRING },
    [5] = { .name = ST_DATA1, .type = BLOBMSG_TYPE_STRING },
    [6] = { .name = ST_DATA2, .type = BLOBMSG_TYPE_STRING },
};

static int ubus_write_spec(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    const char *serviceName = "(unknown)";
    const char *id = "(unknown)";
    const char *class = "(unknown)";
    const char *cmd = "(unknown)";
    const char *data0 = NULL;
    const char *data1 = NULL;
    const char *data2 = NULL;

    struct blob_attr *data[7];
    blobmsg_parse(write_spec_policy, ARRAY_SIZE(write_spec_policy), data, blob_data(msg), blob_len(msg));
    if(data[0] && data[1] && data[2] && data[3])
    {
        serviceName = blobmsg_data(data[0]);
        id = blobmsg_data(data[1]);
        class = blobmsg_data(data[2]);
        cmd = blobmsg_data(data[3]);
        SLOGI("serviceName = %s\n", serviceName);
        SLOGI("id = %s\n", id);
        SLOGI("class = %s\n", class);
        SLOGI("cmd = %s\n", cmd);
        if(data[4])
        {
            data0 = blobmsg_data(data[4]);
        }
        if(data[5])
        {
            data1 = blobmsg_data(data[5]);
        }
        if(data[6])
        {
            data2 = blobmsg_data(data[6]);
        }

        _write_spec((char*)serviceName, (char*)id, (char*)class, (char*)cmd, 
                    (char*)data0, (char*)data1, (char*)data2, NULL);

        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
        ubus_send_reply(ctx, req, buff.head);
    }
    else
    {
        SLOGW("Invalid argument");
    }
    return 0;
}

static const struct blobmsg_policy set_binary_policy[5] = {
    [0] = { .name = ST_SERVICE, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_DEVICE_ID, .type = BLOBMSG_TYPE_STRING },
    [2] = { .name = ST_CMD, .type = BLOBMSG_TYPE_STRING },
    [3] = { .name = ST_VALUE, .type = BLOBMSG_TYPE_STRING },
    [4] = { .name = ST_SUBDEVID, .type = BLOBMSG_TYPE_STRING },
};

static int set_binary(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    const char *serviceName = "(unknown)";
    const char *id = "(unknown)";
    const char *cmd = "(unknown)";
    const char *value = "(unknown)";
    const char *subdevId = NULL;

    struct blob_attr *data[5];
    blobmsg_parse(set_binary_policy, ARRAY_SIZE(set_binary_policy), data, blob_data(msg), blob_len(msg));
    if(data[0] && data[1] && data[2] && data[3])
    {
        serviceName = blobmsg_data(data[0]);
        id = blobmsg_data(data[1]);
        cmd = blobmsg_data(data[2]);
        value = blobmsg_data(data[3]);
        SLOGI("serviceName = %s\n", serviceName);
        SLOGI("id = %s\n", id);
        SLOGI("value = %s\n", value);
        if(data[4])
        {
            subdevId = blobmsg_data(data[4]);
        }

        _set_binary((char*)serviceName, (char*)id, (char*)cmd, (char*)value, (char*)subdevId, NULL);

        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
        ubus_send_reply(ctx, req, buff.head);
    }
    else
    {
        SLOGW("Invalid argument");
    }
    return 0;
}

static const struct blobmsg_policy set_hub_policy[2] = {
    [0] = { .name = ST_HUB_UUID, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
};

static void _send_trigger_another_hub(char *hubUUID, char *ruleId)
{
    if(!hubUUID || !ruleId)
    {
        return;
    }

    json_object *jobj = json_object_new_object();
    json_object *state = json_object_new_object();
    json_object *ruleObj = json_object_new_object();
    json_object *command = json_object_new_object();

    json_object_object_add(command, ST_TRIGGER, json_object_new_boolean(true));
    json_object_object_add(ruleObj, ST_STATE, command);
    json_object_object_add(ruleObj, ST_TYPE, json_object_new_string(ST_RULE));
    json_object_object_add(state, ruleId, ruleObj);
    json_object_object_add(jobj, ST_STATE, state);

    char *topic = (char*)malloc(strlen(hubUUID) + SIZE_64B);
    sprintf(topic, "%s/control", hubUUID);

    VR_(Send_message)(topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
    json_object_put(jobj);
    SAFE_FREE(topic);
}

static int send_trigger(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    const char *hubUUID = "(unknown)";
    const char *id = "(unknown)";

    struct blob_attr *data[2];
    blobmsg_parse(set_hub_policy, ARRAY_SIZE(set_hub_policy), data, blob_data(msg), blob_len(msg));
    if(data[0] && data[1] 
        )
    {
        hubUUID = blobmsg_data(data[0]);
        id = blobmsg_data(data[1]);
        SLOGI("hubUUID = %s\n", hubUUID);
        SLOGI("id = %s\n", id);

        _send_trigger_another_hub((char *)hubUUID, (char *)id);

        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
        ubus_send_reply(ctx, req, buff.head);
    }
    else
    {
        SLOGW("Invalid argument");
    }
    return 0;
}

static const struct blobmsg_policy trigger_policy[2] = {
    [0] = { .name = ST_RULE_ID, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_ACTIONS, .type = BLOBMSG_TYPE_STRING },
};

/*
    we have 2 modes: EXECUTE and TRIGGER
    EXECUTE: just do actions relate currentHub
    TRIGGER: do actions relate currentHub and trigger another hub.
*/
void _trigger_actions_(char *ruleId, char *actions, int trigger_mode)
{
    if(!ruleId || !actions)
    {
        return;
    }

    char *currentHubUUID = VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
    if(!currentHubUUID)
    {
        SLOGI("could not get hubUUID\n");
        return;
    }

    char *save_tok_actions;
    char *tok_action = strtok_r((char*)actions, "&&", &save_tok_actions);
    while(tok_action)
    {
        char *hubUUID = NULL;
        char *service = NULL;
        char *devId = NULL;
        char *cmd = NULL;
        char *mode = NULL;/*for thermostat only*/
        char *value = NULL;
        char *unit = NULL;/*for thermostat only*/

        char *save_tok_act;
        char *tok = strtok_r(tok_action, ";", &save_tok_act);
        GET_DATA_FROM_TOKEN_STRING(tok, hubUUID, ";", save_tok_act);
        if(strcmp(hubUUID, currentHubUUID))
        {
            if(trigger_mode == TRIGGER_MODE)
            {
                /*send trigger to pubsub*/
                _send_trigger_another_hub(hubUUID, (char *)ruleId);
            }

            goto next_action;
        }
        
        GET_DATA_FROM_TOKEN_STRING(tok, service, ";", save_tok_act);
        GET_DATA_FROM_TOKEN_STRING(tok, devId, ";", save_tok_act);
        GET_DATA_FROM_TOKEN_STRING(tok, cmd, ";", save_tok_act);
        if(cmd && !strcmp(ST_SET_TEMP, cmd))
        {
            GET_DATA_FROM_TOKEN_STRING(tok, mode, ";", save_tok_act);
        }
        GET_DATA_FROM_TOKEN_STRING(tok, value, ";", save_tok_act);

        if(cmd && !strcmp(ST_SET_TEMP, cmd))
        {
            GET_DATA_FROM_TOKEN_STRING(tok, unit, ";", save_tok_act);
            _write_spec(service, devId, (char*)ST_THERMOSTAT_SETPOINT, (char*)ST_SET,
                        mode, value, unit, NULL);
        }
        else if(cmd && !strcmp(ST_SET_MODE, cmd))
        {
            _write_spec(service, devId, (char*)ST_THERMOSTAT_MODE, (char*)ST_SET, value, NULL, NULL, NULL);
        }
        else
        {
            _set_binary(service, devId, cmd, value, NULL, NULL);
        }

next_action:
        tok_action = strtok_r(NULL, "&&", &save_tok_actions);

        SAFE_FREE(hubUUID);
        SAFE_FREE(service);
        SAFE_FREE(devId);
        SAFE_FREE(cmd);
        SAFE_FREE(mode);
        SAFE_FREE(value);
        SAFE_FREE(unit);
    }

    SAFE_FREE(currentHubUUID);
}

/*using by timer rule(crontab script) and d2d rule (rule handler)*/
static int trigger_handler(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    SLOGI("trigger_handler\n");
    struct blob_attr *tb[2];
    blobmsg_parse(trigger_policy, ARRAY_SIZE(trigger_policy), tb, blob_data(msg), blob_len(msg));
    const char *ruleId = "(unknown)";
    const char *actions = "(unknown)";

    if(!tb[0]  || !tb[1])
    {
        SLOGI("missing infor\n");
        goto done;
    }

    ruleId = blobmsg_data(tb[0]);
    actions = blobmsg_data(tb[1]);

    rule_report_timeline((char *)ruleId);
    _trigger_actions_((char *)ruleId, (char *)actions, TRIGGER_MODE);
done:
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
    ubus_send_reply(ctx, req, buff.head);
    
    return 0;
}

static const struct ubus_method pubsub_methods[] = 
{
    UBUS_METHOD(ST_NOTIFY, handle_ubus_notify_method, handle_ubus_notify_policy),
    UBUS_METHOD(ST_WRITE_SPEC, ubus_write_spec, write_spec_policy),
    UBUS_METHOD(ST_SET_BINARY, set_binary, set_binary_policy),
    UBUS_METHOD(ST_SEND_TRIGGER, send_trigger, set_hub_policy),
    UBUS_METHOD(ST_TRIGGER, trigger_handler, set_hub_policy),
};

static struct ubus_object_type pubsub_object_type =
UBUS_OBJECT_TYPE(ST_PUBSUB, pubsub_methods);

static struct ubus_object pubsub_object = {
    .name = ST_PUBSUB,
    .type = &pubsub_object_type,
    .methods = pubsub_methods,
    .n_methods = ARRAY_SIZE(pubsub_methods),
};

// void Send_ubus_notify(char *data)
// {
//     SLOGI("ubus notify data = %s\n", data);
//     blob_buf_init(&buff, 0);
//     blobmsg_add_string(&buff, "msg", data);
//     if(ctx)
//     {
//         SLOGI("start send ubus notify\n");
//         int rc = ubus_notify(ctx, &pubsub_object, "pubsub", buff.head, 0);
//         SLOGI("end send ubus notify rc = %d\n", rc);
//     }
// }

static void VR_(handle_ubus_subscriber_remove)(struct ubus_context *ctx, struct ubus_subscriber *s, uint32_t id)
{
    SLOGI("Object %08x went away\n", id);
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++) 
    {
        if (daemon_services[i].id == id)
        {
            daemon_services[i].id = 0;
            break;
        }
    }
}

void VR_(send_shadow_reported)(char *data)
{
    SLOGI("data = %s\n", data);
    if(!data || !strlen(data))
    {
        return;
    }

    char *state = (char*)malloc(strlen(data) + SIZE_128B);
    sprintf(state, "{\"reported\":%s}", data);

    VR_(Send_message)(PUBLISH_SHADOW_UPDATE_TOPIC, VR_QOS, (char *)(state), 0);   

    free(state);
}

void VR_(send_shadow_reported_with_desired)(char *data)
{
    SLOGI("data = %s\n", data);
    if(!data || !strlen(data))
    {
        return;
    }

    char *state = (char*)malloc(strlen(data)*2 + SIZE_128B);
    sprintf(state, "{\"state\":{\"reported\":%s, \"desired\":%s}}", data, data);

    VR_(Send_message)(PUBLISH_SHADOW_UPDATE_TOPIC, VR_QOS, (char *)(state), 0);   

    free(state);
}

void init_json_object(json_object **jobj)
{
    if(*jobj == NULL)
    {
        *jobj = json_object_new_object();
    }
}

// static void get_thermostat_mode(const char *deviceType, const char *local_id)
// {
//     json_object *message = json_object_new_object();
//     json_object_object_add(message, ST_ID, json_object_new_string(local_id));
//     json_object_object_add(message, ST_CLASS, json_object_new_string(ST_THERMOSTAT_MODE));
//     json_object_object_add(message, ST_COMMAND, json_object_new_string(ST_GET));
//     json_object_object_add(message, ST_DATA2, json_object_new_string("1"));

//     VR_(read_spec)(deviceType, json_object_to_json_string(message));

//     json_object_put(message);
// }
//mode: 0 alexa, 1 local

//Mode: 0: Low threshold, 1: High threshold
static void get_temperature_threshold(char *nodeId, int mode, char *unit, char *value, int len)
{
    if(!value)
    {
        SLOGI("Input Invalid\n");
        return;
    }

    char featureId[SIZE_32B];
    if(mode)
    {
        strncpy(featureId, ST_TEMPERATURE_HIGH_THRESHOLD, sizeof(featureId)-1);
    }
    else
    {
        strncpy(featureId, ST_TEMPERATURE_LOW_THRESHOLD, sizeof(featureId)-1);
    }
    featureId[sizeof(featureId)-1]='\0';

    SEARCH_DATA_INIT_VAR(thresholdTemp);
    searching_database("pubsub_handler", dev_db, get_last_data_cb, &thresholdTemp,
                        "SELECT register from FEATURES where deviceId='%s' AND featureId='%s'",
                        nodeId, featureId);
    if(thresholdTemp.len)
    {
        strncpy(value, thresholdTemp.value, len);
        FREE_SEARCH_DATA_VAR(thresholdTemp);
        return;
    }

    SEARCH_DATA_INIT_VAR(serialId);
    searching_database("pubsub_handler", dev_db, get_last_data_cb, &serialId,
                        "SELECT serialId from SUB_DEVICES where id='%s'", nodeId);
    if(!serialId.len)
    {
        SLOGE("Failed to get serialId of device %s from database\n", nodeId);
        FREE_SEARCH_DATA_VAR(thresholdTemp);
        FREE_SEARCH_DATA_VAR(serialId);
        return;
    }

    SEARCH_DATA_INIT_VAR(deviceSpecific);
    searching_database("pubsub_handler", support_devs_db, get_last_data_cb, &deviceSpecific,
                            "SELECT DeviceSpecific from SUPPORT_DEVS where SerialID='%s'",
                            serialId.value);
    if(!deviceSpecific.len)
    {
        SLOGE("Failed to get deviceSpecific of device %s from database\n", serialId.value);
        FREE_SEARCH_DATA_VAR(serialId);
        FREE_SEARCH_DATA_VAR(thresholdTemp);
        FREE_SEARCH_DATA_VAR(deviceSpecific);
        return;
    }

    json_object *deviceSpecificObj = VR_(create_json_object)(deviceSpecific.value);
    if(!deviceSpecificObj)
    {
        SLOGE("Failed to create json object %s\n", deviceSpecific.value);
        FREE_SEARCH_DATA_VAR(serialId);
        FREE_SEARCH_DATA_VAR(thresholdTemp);
        FREE_SEARCH_DATA_VAR(deviceSpecific);
        return;
    }

    CHECK_JSON_OBJECT_EXIST(threshHoldTempObj, deviceSpecificObj, ST_TEMPERATURE_THRESH_HOLD, getDone);
    char unitVal[SIZE_32B];
    if(!strcmp(unit, ST_CELSIUS))
    {
        if(mode)
        {
            strncpy(unitVal, ST_MAX_CELSIUS, sizeof(unitVal)-1);
        }
        else
        {
            strncpy(unitVal, ST_MIN_CELSIUS, sizeof(unitVal)-1);
        }
    }
    else
    {
        if(mode)
        {
            strncpy(unitVal, ST_MAX_FAHRENHEIT, sizeof(unitVal)-1);
        }
        else
        {
            strncpy(unitVal, ST_MIN_FAHRENHEIT, sizeof(unitVal)-1);
        }
    }
    unitVal[sizeof(unitVal)-1]='\0';

    CHECK_JSON_OBJECT_EXIST(valObj, threshHoldTempObj, unitVal, getDone);
    strncpy(value, json_object_get_string(valObj), len);

getDone:
    json_object_put(deviceSpecificObj);
    FREE_SEARCH_DATA_VAR(serialId);
    FREE_SEARCH_DATA_VAR(deviceSpecific);
    FREE_SEARCH_DATA_VAR(thresholdTemp);
}

void VR_(shadow_update_status)(const char *input, int mode)
{
    SLOGI("input = %s\n", input);
    SLOGI("mode = %d\n", mode);
    if(!input || !strlen(input))
    {
        return;
    }

    char *cloudid = NULL;
    json_object *jobj = VR_(create_json_object)((char *)input);
    if(!jobj)
    {
        SLOGE("failed to create json object\n");
        return;
    }
    json_object *shadow_status = json_object_new_object();
    json_object *dev_status = NULL;
    const char *local_id;

    // CHECK_JSON_OBJECT_EXIST(type_obj, jobj, ST_TYPE, done);
    CHECK_JSON_OBJECT_EXIST(method_obj, jobj, ST_METHOD, done);

    json_object *groupId_obj = NULL;
    json_object *udnId_obj = NULL;
    json_object_object_get_ex(jobj, ST_GROUP_ID, &groupId_obj);
    json_object_object_get_ex(jobj, ST_UDN, &udnId_obj);
    if(groupId_obj)
    {
        local_id = json_object_get_string(groupId_obj);
    }
    else if(udnId_obj)
    {
        local_id = json_object_get_string(udnId_obj);
    }
    else
    {
        CHECK_JSON_OBJECT_EXIST(deviceid_obj, jobj, ST_DEVICE_ID, done);
        local_id = json_object_get_string(deviceid_obj);
    }

    // const char *type = json_object_get_string(type_obj);
    const char *method = json_object_get_string(method_obj);
    if(!strcmp(method, ST_SET_BINARY_R))
    {
        CHECK_JSON_OBJECT_EXIST(status_obj, jobj, ST_STATUS, done);
        const char *status = json_object_get_string(status_obj);
        if(!strcmp(status, ST_RECEIVED) || !strcmp(status, ST_SUCCESSFUL)) //need check user id to know local or alexa control, duplicate update.
        {                                                                   //SUCCESSFULL for updating when local control.
            CHECK_JSON_OBJECT_EXIST(command_obj, jobj, ST_COMMAND, done);
            CHECK_JSON_OBJECT_EXIST(value_obj, jobj, ST_VALUE, done);
            const char *command = json_object_get_string(command_obj);
            const char *value = json_object_get_string(value_obj);
            if(value && strlen(value))
            {
                init_json_object(&dev_status);

                if(!strcmp(command, ST_ON_OFF))
                {
                    json_object_object_add(dev_status, ST_CLOUD_ON_OFF, json_object_new_string(value));
                }
                else if(!strcmp(command, ST_DIM))
                {
                    json_object_object_add(dev_status, ST_CLOUD_ON_OFF, json_object_new_string(ST_ON_VALUE));
                    json_object_object_add(dev_status, ST_CLOUD_DIM, json_object_new_string(value));
                }
                // else if(!strcmp(command, ST_SET_TEMP))
                // {
                //     SEARCH_DATA_INIT_VAR(thermostat_mode_db);
                //     searching_database("pubsub_handler", dev_db, get_last_data_cb, &thermostat_mode_db,
                //             "SELECT register from FEATURES where deviceId='%s' AND featureId='%s'", 
                //                     local_id, ST_THERMOSTAT_MODE);

                //     if(thermostat_mode_db.len)
                //     {
                //         json_object *setpointReportObj = json_object_new_object();
                //         json_object *tempReportObj = json_object_new_object();
                //         json_object_object_add(tempReportObj, ST_VALUE, json_object_new_string(value));
                //         json_object_object_add(tempReportObj, ST_UNIT, json_object_new_string(ST_MULTILEVEL_SENSOR_LABEL_CELCIUS));
                //         json_object_object_add(setpointReportObj, thermostat_mode_db.value, tempReportObj);
                //         json_object_object_add(dev_status, ST_CLOUD_SET_TEMP, setpointReportObj);
                //     }
                //     FREE_SEARCH_DATA_VAR(thermostat_mode_db);
                // }
            }
            else
            {
                SLOGE("cannot get value\n");
            }
        }
        else if(!strcmp(status, ST_FAILED))
        {
            CHECK_JSON_OBJECT_EXIST(command_obj, jobj, ST_COMMAND, done);
            const char *command = json_object_get_string(command_obj);
            SEARCH_DATA_INIT_VAR(value);
            SEARCH_DATA_INIT_VAR(thermostat_mode_db);
            if(!strcmp(command, ST_SET_TEMP))
            {
                // searching_database("pubsub_handler", dev_db, get_last_data_cb, &thermostat_mode_db,
                //             "SELECT register from FEATURES where deviceId='%s' AND featureId='%s'", 
                //                     local_id, ST_THERMOSTAT_MODE);
                // char thermostatMode[SIZE_64B];
                // strcpy(thermostatMode, thermostat_mode_db.value);
                // *(thermostatMode) = toupper(*(thermostatMode));
                // char setpointKey[SIZE_64B];
                // sprintf(setpointKey, "%s%s", ST_THERMOSTAT_SETPOINT, thermostatMode);

                // searching_database("pubsub_handler", dev_db, get_last_data_cb, &value,
                //         "SELECT register from FEATURES where deviceId='%s' AND featureId='%s'", 
                //                 local_id, setpointKey);
            }
            else
            {
                searching_database("pubsub_handler", dev_db, get_last_data_cb, &value,
                        "SELECT register from FEATURES where deviceId='%s' AND featureId='%s'", 
                                local_id, command);
            }
            
            if(value.len)
            {
                init_json_object(&dev_status);

                if(!strcmp(command, ST_ON_OFF))
                {
                    json_object_object_add(dev_status, ST_CLOUD_ON_OFF, json_object_new_string(value.value));
                }
                else if(!strcmp(command, ST_DIM))
                {
                    SEARCH_DATA_INIT_VAR(onoff_value);
                    searching_database("pubsub_handler", dev_db, get_last_data_cb, &onoff_value,
                        "SELECT register from FEATURES where deviceId='%s' AND featureId='%s'", 
                                local_id, ST_ON_OFF);

                    if(onoff_value.len)
                    {
                        json_object_object_add(dev_status, ST_CLOUD_ON_OFF, json_object_new_string(onoff_value.value));
                    }
                    json_object_object_add(dev_status, ST_CLOUD_DIM, json_object_new_string(value.value));
                    FREE_SEARCH_DATA_VAR(onoff_value);
                }
                else if(!strcmp(command, ST_SET_TEMP))
                {
                    if(strstr(value.value, ":"))
                    {
                        char tempValue[SIZE_32B], unit[SIZE_32B];
                        char *save_tok;
                        char *ch = strtok_r(value.value, ":", &save_tok);
                        if(ch != NULL)
                        {
                            strncpy(tempValue, ch, sizeof(tempValue));
                            ch = strtok_r(NULL, ":", &save_tok);
                        }
                        if(ch != NULL)
                        {
                            strncpy(unit, ch, sizeof(unit));
                        }
                        json_object *setpointReportObj = json_object_new_object();
                        json_object *tempReportObj = json_object_new_object();
                        json_object_object_add(tempReportObj, ST_VALUE, json_object_new_string(tempValue));
                        json_object_object_add(tempReportObj, ST_UNIT, json_object_new_string(unit));
                        json_object_object_add(setpointReportObj, thermostat_mode_db.value, tempReportObj);
                        json_object_object_add(dev_status, ST_CLOUD_SET_TEMP, setpointReportObj);
                    }
                    
                }
                FREE_SEARCH_DATA_VAR(value);
                FREE_SEARCH_DATA_VAR(thermostat_mode_db);
            }
            else
            {
                SLOGE("value not found\n");
                FREE_SEARCH_DATA_VAR(value);
                goto done;
            }
        }
    }
    else if(!strcmp(method, ST_NOTIFY))
    {
        CHECK_JSON_OBJECT_EXIST(notifytype_obj, jobj, ST_NOTIFY_TYPE, done);
        const char *notifytype = json_object_get_string(notifytype_obj);
        
        if(!strcmp(notifytype, ST_CMD_CLASS_ZPC))
        {
            CHECK_JSON_OBJECT_EXIST(cmdClassObj, jobj, ST_CMD_CLASS, done);
            CHECK_JSON_OBJECT_EXIST(cmdObj, jobj, ST_CMD, done);
            const char *cmdClass = json_object_get_string(cmdClassObj);
            const char *cmd = json_object_get_string(cmdObj);
            if(!strcmp(cmdClass, ST_SCENE_ACTIVATION))
            {
                CHECK_JSON_OBJECT_EXIST(buttonIdObj, jobj, ST_BUTTON_ID, done);
                const char *buttonId = json_object_get_string(buttonIdObj);
                VR_(scene_trigger)((char*)local_id, (char*)buttonId);
            }
            else if(!strcmp(cmd, ST_REPORT))
            {
                if(!strcmp(cmdClass, ST_BATTERY))
                {
                    CHECK_JSON_OBJECT_EXIST(batteryLevelObj, jobj, ST_BATTERY_LEVEL, done);
                    char batteryLevel[SIZE_32B];
                    sprintf(batteryLevel, "%d", json_object_get_int(batteryLevelObj));
                    init_json_object(&dev_status);
                    json_object_object_add(dev_status, ST_BATTERY, json_object_new_string(batteryLevel));
                }
                else if(!strcmp(cmdClass, ST_DOOR_LOCK))
                {
                    CHECK_JSON_OBJECT_EXIST(lockStatusObj, jobj, ST_LOCK_STATUS, done);
                    char lockStatus[SIZE_32B];
                    sprintf(lockStatus, "%d", json_object_get_int(lockStatusObj));
                    init_json_object(&dev_status);
                    json_object_object_add(dev_status, ST_CLOUD_ON_OFF, json_object_new_string(lockStatus));
                }
                else if(!strcmp(cmdClass, ST_ALARM) || 
                        !strcmp(cmdClass, ST_SENSOR_ALARM) || 
                        !strcmp(cmdClass, ST_SENSOR_BINARY) ||
                        !strcmp(cmdClass, ST_NOTIFICATION))
                {
                    CHECK_JSON_OBJECT_EXIST(alarmtype_obj, jobj, ST_ALARM_TYPE_FINAL, done);
                    init_json_object(&dev_status);
                    const char *alarmtype = json_object_get_string(alarmtype_obj);
                    // if(!strcmp(alarmtype, ST_TAMPER))
                    // {
                    //     json_object_object_add(dev_status, "tamper", json_object_new_string(ST_TRUE));
                    // }
                    // else 

                    int res = get_door_status(alarmtype);
                    if(!strcmp(alarmtype, ST_DOOR_CLOSED)
                        || (res == DOOR_RF_LOCK_VALUE)
                        )
                    {
                        json_object_object_add(dev_status, ST_CLOUD_ON_OFF, json_object_new_string(ST_CLOSE_VALUE));
                    }
                    else if(!strcmp(alarmtype, ST_DOOR_OPEN)
                            || (res == DOOR_RF_UNLOCK_VALUE)
                            )
                    {
                        json_object_object_add(dev_status, ST_CLOUD_ON_OFF, json_object_new_string(ST_OPEN_VALUE));
                    }
                    else if(!strcmp(alarmtype, ST_WRONG_PIN_3TIMES))
                    {
                        json_object_object_add(dev_status, ST_ALARM, json_object_new_string(ST_WRONG_PIN_3TIMES));
                    }
                    else if(!strcmp(alarmtype, ST_STUCKINLOCK))
                    {
                        json_object_object_add(dev_status, ST_ALARM, json_object_new_string(ST_STUCKINLOCK));
                    }
                    else if(!strcmp(alarmtype, ST_TEMPERATURE_HIGH))
                    {
                        char valThreshold[SIZE_32B] ={0};
                        get_temperature_threshold((char *)local_id, 1, ST_CELSIUS, valThreshold, sizeof(valThreshold)-1);
                        if(strlen(valThreshold))
                        {
                            json_object *tempReportObj = json_object_new_object();
                            json_object_object_add(tempReportObj, ST_VALUE, json_object_new_string(valThreshold));
                            json_object_object_add(tempReportObj, ST_UNIT, json_object_new_string(ST_CELSIUS));
                            json_object_object_add(dev_status, ST_ALARM, json_object_new_string(ST_OVERHEAT));
                            json_object_object_add(dev_status, ST_TEMPERATURE_THRESH_HOLD, tempReportObj);
                        }
                    }
                    else if(!strcmp(alarmtype, ST_TEMPERATURE_LOW))
                    {
                        char valThreshold[SIZE_32B] ={0};
                        get_temperature_threshold((char *)local_id, 0, ST_CELSIUS, valThreshold, sizeof(valThreshold)-1);
                        if(strlen(valThreshold))
                        {
                            json_object *tempReportObj = json_object_new_object();
                            json_object_object_add(tempReportObj, ST_VALUE, json_object_new_string(valThreshold));
                            json_object_object_add(tempReportObj, ST_UNIT, json_object_new_string(ST_CELSIUS));
                            json_object_object_add(dev_status, ST_ALARM, json_object_new_string(ST_UNDERHEAT));
                            json_object_object_add(dev_status, ST_TEMPERATURE_THRESH_HOLD, tempReportObj);
                        }
                    }
                    else if(!strcmp(alarmtype, ST_TEMPERATURE_OK))
                    {
                        json_object_object_add(dev_status, ST_ALARM, json_object_new_string(ST_TEMPERATURE_OK));
                    }
                    else if(!strcmp(alarmtype, ST_SMOKE_CLEARED))
                    {
                        json_object_object_add(dev_status, ST_CLOUD_SMOKE, json_object_new_string(ST_FALSE));
                    }
                    else if(!strcmp(alarmtype, ST_SMOKE_DETECTED))
                    {
                        json_object_object_add(dev_status, ST_CLOUD_SMOKE, json_object_new_string(ST_TRUE));
                    }
                    else if(!strcmp(alarmtype, ST_CO_CLEARED))
                    {
                        json_object_object_add(dev_status, ST_CLOUD_CO, json_object_new_string(ST_FALSE));
                    }
                    else if(!strcmp(alarmtype, ST_CO_DETECTED))
                    {
                        json_object_object_add(dev_status, ST_CLOUD_CO, json_object_new_string(ST_TRUE));
                    }
                    else if(!strcmp(alarmtype, ST_WATER_CLEAR))
                    {
                        json_object_object_add(dev_status, ST_WATER, json_object_new_string(ST_FALSE));
                    }
                    else if(!strcmp(alarmtype, ST_WATER_LEAKS))
                    {
                        json_object_object_add(dev_status, ST_WATER, json_object_new_string(ST_TRUE));
                    }
                    else if(!strcmp(alarmtype, ST_WATER_FLOW_IDLE))
                    {
                        json_object_object_add(dev_status, ST_WATER_FLOW, json_object_new_string(ST_IDLE));
                    }
                    else if(!strcmp(alarmtype, ST_WATER_FLOW_LOW))
                    {
                        json_object_object_add(dev_status, ST_WATER_FLOW, json_object_new_string(ST_LOW));
                    }
                    else if(!strcmp(alarmtype, ST_WATER_FLOW_HIGH))
                    {
                        json_object_object_add(dev_status, ST_WATER_FLOW, json_object_new_string(ST_HIGH));
                    }
                    // else if(!strcmp(alarmtype, "MotionCleared"))
                    // {
                    //     json_object_object_add(dev_status, "motion", json_object_new_string(ST_FALSE));
                    // }
                    // else if(!strcmp(alarmtype, "MotionDetected"))
                    // {
                    //     json_object_object_add(dev_status, "motion", json_object_new_string(ST_TRUE));
                    // }
                    else
                    {
                        SLOGE("alarm type not match\n");
                    }
                }
                else if(!strcmp(cmdClass, ST_BARRIER_OPERATOR))
                {
                    CHECK_JSON_OBJECT_EXIST(barrierStatusObj, jobj, ST_BARRIER_STATUS, done);
                    char barrierStatus[SIZE_32B];
                    int tmp = json_object_get_int(barrierStatusObj);
                    sprintf(barrierStatus, "%d", tmp);
                    init_json_object(&dev_status);
                    switch(tmp)
                    {
                        case GARAGE_MISSING:
                            json_object_object_add(dev_status, ST_GARAGE_SENSOR, json_object_new_string(ST_MISSING));
                            break;
                        case GARAGE_LOW_BATTERY:
                            json_object_object_add(dev_status, ST_GARAGE_SENSOR, json_object_new_string(ST_LOW_BATTERY));
                            break;
                        default:
                            json_object_object_add(dev_status, ST_CLOUD_ON_OFF, json_object_new_string(barrierStatus));
                            break;
                    }
                }
                else if(!strcmp(cmdClass, ST_BASIC) ||                        
                        !strcmp(cmdClass, ST_SWITCH_BINARY) ||                         
                        !strcmp(cmdClass, ST_SWITCH_MULTILEVEL))                
                {
                    CHECK_JSON_OBJECT_EXIST(valueObj, jobj, ST_VALUE, done);
                    init_json_object(&dev_status);
                    uint8_t value = json_object_get_int(valueObj);
                    if(value == 0)
                    {
                        json_object_object_add(dev_status, ST_CLOUD_ON_OFF, json_object_new_string(ST_OFF_VALUE));
                    }
                    else if(value == 255)
                    {
                        json_object_object_add(dev_status, ST_CLOUD_ON_OFF, json_object_new_string(ST_ON_VALUE));
                    }
                    else
                    {
                        char dim[SIZE_32B];
                        sprintf(dim, "%d", value);
                        json_object_object_add(dev_status, ST_CLOUD_ON_OFF, json_object_new_string(ST_ON_VALUE));
                        json_object_object_add(dev_status, ST_CLOUD_DIM, json_object_new_string(dim));
                    }
                }
                else if(!strcmp(cmdClass, ST_SENSOR_MULTILEVEL))
                {
                    CHECK_JSON_OBJECT_EXIST(sensorPatternObj, jobj, ST_SENSOR_PATTERN, done);
                    CHECK_JSON_OBJECT_EXIST(unitObj, jobj, ST_UNIT, done);
                    CHECK_JSON_OBJECT_EXIST(valueObj, jobj, ST_VALUE, done);
                    init_json_object(&dev_status);
                    const char *sensorPattern = json_object_get_string(sensorPatternObj);
                    const char *unit = json_object_get_string(unitObj);
                    float value = json_object_get_double(valueObj);
                    if(!strcmp(sensorPattern, ST_MULTILEVEL_SENSOR_AIR_TEMPERATURE))
                    {
                        char valueStr[SIZE_32B];
                        sprintf(valueStr, "%.2f", value);
                        json_object *tempReportObj = json_object_new_object();

                        json_object_object_add(tempReportObj, ST_VALUE, json_object_new_string(valueStr));
                        json_object_object_add(tempReportObj, ST_UNIT, json_object_new_string(unit));
                        json_object_object_add(dev_status, ST_CLOUD_CURRENT_TEMP, tempReportObj);
                    }
                }
                else if(!strcmp(cmdClass, ST_THERMOSTAT_MODE))
                {
                    CHECK_JSON_OBJECT_EXIST(thermostatModeObj, jobj, ST_THERMOSTAT_MODE, done);
                    const char *thermostatMode = json_object_get_string(thermostatModeObj);

                    init_json_object(&dev_status);
                    if(!strcmp(thermostatMode, ST_RESUME))
                    {
                        json_object_object_add(dev_status, ST_ACTIVE, json_object_new_string(ST_ON));
                    }
                    else
                    {
                        json_object_object_add(dev_status, ST_ACTIVE, json_object_new_string(thermostatMode));
                    }
                }
                else if(!strcmp(cmdClass, ST_THERMOSTAT_SETPOINT))
                {

                    CHECK_JSON_OBJECT_EXIST(setpointPatternObj, jobj, ST_SET_POINT_PATTERN, done);
                    CHECK_JSON_OBJECT_EXIST(unitObj, jobj, ST_UNIT, done);
                    CHECK_JSON_OBJECT_EXIST(setpointValueObj, jobj, ST_SET_POINT_VALUE, done);
                    init_json_object(&dev_status);
                    const char *setpointPattern = json_object_get_string(setpointPatternObj);

                    const char *thermostatMode = get_thermostat_mode_from_setpoint(setpointPattern);
                    if(!thermostatMode)
                    {
                        goto done;
                    }

                    const char *unit = json_object_get_string(unitObj);
                    float setpointValue = json_object_get_double(setpointValueObj);
                    char valueStr[SIZE_32B];
                    sprintf(valueStr, "%.2f", setpointValue);
                    json_object *setpointReportObj = json_object_new_object();
                    json_object *tempReportObj = json_object_new_object();

                    json_object_object_add(tempReportObj, ST_VALUE, json_object_new_string(valueStr));
                    json_object_object_add(tempReportObj, ST_UNIT, json_object_new_string(unit));
                    json_object_object_add(setpointReportObj, thermostatMode, tempReportObj);
                    json_object_object_add(dev_status, ST_CLOUD_SET_TEMP, setpointReportObj);
                }
                else if(!strcmp(cmdClass, ST_THERMOSTAT_FAN_MODE))
                {
                    CHECK_JSON_OBJECT_EXIST(thermostatFanModeObj, jobj, ST_THERMOSTAT_FAN_MODE, done);
                    const char *thermostatFanMode = json_object_get_string(thermostatFanModeObj);
                    init_json_object(&dev_status);
                    json_object_object_add(dev_status, ST_FAN_MODE, json_object_new_string(thermostatFanMode));
                }
            }
        }
        else if(!strcmp(notifytype, ST_BINARY_STATE_RESPONSE))
        {
            CHECK_JSON_OBJECT_EXIST(value_obj, jobj, ST_VALUE, done);
            const char *value = json_object_get_string(value_obj);
            init_json_object(&dev_status);

            char *save_tok, *tok;
            char *tmp = strdup(value);

            tok = strtok_r(tmp, ",", &save_tok);
            if(tok)
            {
                json_object_object_add(dev_status, ST_CLOUD_ON_OFF, json_object_new_string(tok));
                tok = strtok_r(NULL, ",", &save_tok);
            }

            if(tok)
            {
                json_object_object_add(dev_status, ST_CLOUD_DIM, json_object_new_string(tok));
            }

            free(tmp);
        }
    }
    else if(!strcmp(method, ST_WRITE_SPEC_R))
    {
        CHECK_JSON_OBJECT_EXIST(status_obj, jobj, ST_STATUS, done);

        const char *status = json_object_get_string(status_obj);
        if(!strcmp(status, ST_SUCCESSFUL))
        {
            CHECK_JSON_OBJECT_EXIST(command_info_bj, jobj, ST_COMMAND_INFO, done);
            CHECK_JSON_OBJECT_EXIST(class_obj, command_info_bj, ST_CLASS, done);
            CHECK_JSON_OBJECT_EXIST(command_obj, command_info_bj, ST_COMMAND, done);
            CHECK_JSON_OBJECT_EXIST(data0_obj, command_info_bj, ST_DATA0, done);

            const char *class = json_object_get_string(class_obj);
            const char *command = json_object_get_string(command_obj);

            if(strcmp(command, ST_SET))
            {
                goto done;
            }

            if(!strcmp(class, ST_THERMOSTAT_MODE))
            {
                init_json_object(&dev_status);
                const char *ther_mode = json_object_get_string(data0_obj);

                if(!strcmp(ther_mode, ST_RESUME))
                {
                    json_object_object_add(dev_status, ST_ACTIVE, json_object_new_string(ST_ON));
                    // get_thermostat_mode(type, local_id);
                }
                else
                {
                    json_object_object_add(dev_status, ST_ACTIVE, json_object_new_string(ther_mode));
                }
            }
            else if(!strcmp(class, ST_THERMOSTAT_FAN_MODE))
            {
                init_json_object(&dev_status);
                const char *ther_fan_mode = json_object_get_string(data0_obj);
                json_object_object_add(dev_status, ST_FAN_MODE, json_object_new_string(ther_fan_mode));
            }
            else if(!strcmp(class, ST_THERMOSTAT_SETPOINT))
            {
                const char *ther_mode = json_object_get_string(data0_obj);
                const char *thermostatMode = get_thermostat_mode_from_setpoint(ther_mode);
                if(!thermostatMode)
                {
                    goto done;
                }

                CHECK_JSON_OBJECT_EXIST(data1_obj, command_info_bj, ST_DATA1, done);
                CHECK_JSON_OBJECT_EXIST(data2_obj, command_info_bj, ST_DATA2, done);

                init_json_object(&dev_status);
                const char *data1 = json_object_get_string(data1_obj);
                const char *data2 = json_object_get_string(data2_obj);
                float set_point_temp = strtod(data1, NULL);
                char temp[SIZE_32B];
                sprintf(temp, "%.2f", (double)set_point_temp);
                json_object *setpointReportObj = json_object_new_object();
                json_object *tempReportObj = json_object_new_object();

                json_object_object_add(tempReportObj, ST_VALUE, json_object_new_string(temp));
                JSON_ADD_STRING_SAFE(tempReportObj, ST_UNIT, data2);
                json_object_object_add(setpointReportObj, thermostatMode, tempReportObj);
                json_object_object_add(dev_status, ST_CLOUD_SET_TEMP, setpointReportObj);
            }
            else if(!strcmp(class, ST_SWITCH_COLOR))
            {
                const char *data0 = json_object_get_string(data0_obj);

                json_object *colorObj = VR_(create_json_object)((char*)data0);
                if(colorObj)
                {
                    init_json_object(&dev_status);
                    json_object_object_add(dev_status, ST_COLOR, colorObj);
                }
            }
        }
    }
    else if(!strcmp(method, ST_GROUP_CONTROL_R))
    {
        CHECK_JSON_OBJECT_EXIST(status_obj, jobj, ST_STATUS, done);
        const char *status = json_object_get_string(status_obj);
        if(!strcmp(status, ST_RECEIVED) || !strcmp(status, ST_SUCCESSFUL)) //need check user id to know local or alexa control, duplicate update.
        {                                                                   //SUCCESSFULL for updating when local control.
            CHECK_JSON_OBJECT_EXIST(command_obj, jobj, ST_COMMAND, done);
            CHECK_JSON_OBJECT_EXIST(value_obj, jobj, ST_VALUE, done);
            const char *command = json_object_get_string(command_obj);
            const char *value = json_object_get_string(value_obj);
            if(value && strlen(value))
            {
                init_json_object(&dev_status);

                if(!strcmp(command, ST_ON_OFF))
                {
                    json_object_object_add(dev_status, ST_CLOUD_ON_OFF, json_object_new_string(value));
                }
                else if(!strcmp(command, ST_DIM))
                {
                    json_object_object_add(dev_status, ST_CLOUD_ON_OFF, json_object_new_string(ST_ON_VALUE));
                    json_object_object_add(dev_status, ST_CLOUD_DIM, json_object_new_string(value));
                }
            }
            else
            {
                SLOGE("cannot get value\n");
            }
        }
    }
    else if(!strcmp(method, ST_ACTION_COMPLETE))/*upnp devices*/
    {
        CHECK_JSON_OBJECT_EXIST(status_obj, jobj, ST_STATUS, done);
        const char *status = json_object_get_string(status_obj);
        if(strcmp(status, ST_SUCCESSFUL))
        {
            goto done;
        }

        CHECK_JSON_OBJECT_EXIST(actionType_obj, jobj, ST_ACTION_TYPE, done);
        const char *actionType = json_object_get_string(actionType_obj);
        if(strcmp(actionType, ST_SET_BINARY) && strcmp(actionType, ST_GET_BINARY))
        {
            goto done;
        }

        CHECK_JSON_OBJECT_EXIST(value_obj, jobj, ST_VALUE, done);
        const char *value = json_object_get_string(value_obj);

        init_json_object(&dev_status);

        char *save_tok, *tok;
        char *tmp = strdup(value);

        tok = strtok_r(tmp, ",", &save_tok);
        if(tok)
        {
            json_object_object_add(dev_status, ST_CLOUD_ON_OFF, json_object_new_string(tok));
            tok = strtok_r(NULL, ",", &save_tok);
        }

        if(tok)
        {
            json_object_object_add(dev_status, ST_CLOUD_DIM, json_object_new_string(tok));
        }

        free(tmp);
    }

    if(!dev_status)
    {
        SLOGE("no status report\n");
        goto done;
    }

    int status_length = json_object_object_length(dev_status);
    SLOGI("status_length = %d\n", status_length);
    if(status_length <= 0)
    {
        json_object_put(dev_status);
        goto done;
    }

    if(!is_report((char*)local_id, 
                 (char*)json_object_to_json_string(dev_status), 
                 (unsigned int)time(NULL))
        )
    {
        SLOGE("Same status,not report\n");
        json_object_put(dev_status);
        goto done;
    }

    cloudid = VR_(get_cloudid_from_localid)(g_shm, (char *)local_id);
    if(!cloudid)
    {
        SLOGE("cloudid not found\n");
        json_object_put(dev_status);
        goto done;
    }
    json_object_object_add(shadow_status, cloudid, dev_status);

    if(!mode)//alexa
    {
        VR_(send_shadow_reported)((char *)json_object_to_json_string(shadow_status));
    }
    else //local
    {
        VR_(send_shadow_reported_with_desired)((char *)json_object_to_json_string(shadow_status));
    }

done:
    SAFE_FREE(cloudid);
    json_object_put(jobj);
    json_object_put(shadow_status);
    return;
}

void update_shadow_status_cb(void *data)
{
    char *message = (char *)data;
    if(message)
    {
        //// update status for shadow devices ////
        VR_(shadow_update_status)(message, 0);
        free(message);
    }
}

static int VR_(handle_ubus_notification)(struct ubus_context *ctx, struct ubus_object *obj,
                                    struct ubus_request_data *req, const char *method,
                                    struct blob_attr *msg)
{
    SLOGI("PUBSUB recive_notify_cb from %s\n", method);
    const char *msgstr = "(unknown)";
    struct blob_attr *tb[1];
    blobmsg_parse(msg_policy, ARRAY_SIZE(msg_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
    {
        msgstr = blobmsg_data(tb[0]);
        SLOGI("msgstr = %s\n", msgstr);
        VR_(Send_message)(NULL, VR_QOS, msgstr, 0);
        //Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, "notify", (char *)msgstr, 1);
        size_t length = strlen(msgstr)+1;
        char *input_msg = (char*)malloc(length);
        strlcpy(input_msg, msgstr, length);

        pthread_t update_shadow_status_thread;
        pthread_create(&update_shadow_status_thread, NULL, (void *)&update_shadow_status_cb, (void *)input_msg);
        pthread_detach(update_shadow_status_thread);
        
    }   
    return 0;
}

static int _post_power_cycle_event()
{
    char *token = VR_(read_option)(NULL, UCI_CLOUD_TOKEN);
    if(!token)
    {
        SLOGI("Token not found\n");
        return -1;
    }

    if(!strcmp(token, "default"))
    {
        SLOGI("Token invalid\n");
        free(token);
        return -1;
    }

    char *hubUUID = VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
    if(!hubUUID)
    {
        SLOGI("hubUUID not found\n");
        free(token);
        return -1;
    }

    int res = 0;
    char *result = NULL;
    char data[SIZE_256B];
    char address[SIZE_1024B], headerIn[SIZE_1024B]={0};
    snprintf(headerIn, sizeof(headerIn),"authorization: %s", token);

    char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    if(!cloud_address)
    {
        snprintf(address, sizeof(address), POWER_CYCLE_EVENT_DOMAIN, CLOUD_DOMAIN, hubUUID);
    }
    else
    {
        snprintf(address, sizeof(address), POWER_CYCLE_EVENT_DOMAIN, cloud_address, hubUUID);
    }

    char *power_time = VR_(read_option)(NULL, UCI_FIRMWARE_POWER_CYCLE_TIME);
    if(power_time)
    {
        sprintf(data, "{\"timestamp\":%s}", power_time);
        free(power_time);
    }
    else
    {
        sprintf(data, "{\"timestamp\":%u}", (unsigned int)time(NULL) - CONNECT_DELAY_TIME);
    }

    res = VR_(post_request)(address, headerIn, data, NULL, &result, DEFAULT_TIMEOUT);
    if(res)
    {
        SLOGE("failed to post power cycle event\n");
        res = -1;
        goto postPowerCycleDone;
    }
    SLOGI("result = %s\n", result);

postPowerCycleDone:
    SAFE_FREE(result);
    SAFE_FREE(token);
    SAFE_FREE(hubUUID);
    SAFE_FREE(cloud_address);
    return res;
}

void power_event_thread_cb(void)
{
    while(g_is_disconnected)
    {
        sleep(3);
    }

    int res = _post_power_cycle_event();
    int retry = 3;
    while(res != 0 && retry > 0)
    {
        sleep(3);
        res = _post_power_cycle_event();
        retry --;
    }
}

static const struct blobmsg_policy ubus_event_policy[2] = {
    [0] = { .name = "id", .type = BLOBMSG_TYPE_INT32 },
    [1] = { .name = "path", .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy update_supported_event[1] = {
    [0] = { .name = "version", .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy ubus_event_resource_policy[3] = {
    [0] = { .name = "deviceId", .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = "deviceType", .type = BLOBMSG_TYPE_STRING },
    [2] = { .name = "data", .type = BLOBMSG_TYPE_STRING },
};

static void VR_(handle_ubus_event)(struct ubus_context *ctx, struct ubus_event_handler *ev,
              const char *type, struct blob_attr *msg)
{
    if(!strcmp(type, "ubus.object.add"))
    {
        struct blob_attr *tb[2];
        blobmsg_parse(ubus_event_policy, ARRAY_SIZE(ubus_event_policy), 
                        tb, blob_data(msg), blob_len(msg));
        uint32_t id;
        int i, ret;
        const char *path = "unknown";

        if(tb[0] && tb[1])
        {
            id = blobmsg_get_u32(tb[0]);
            path = blobmsg_data(tb[1]);

            for(i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
            {
                if(!strcmp(daemon_services[i].name, path))
                {
                    if(daemon_services[i].id != 0)
                    {
                        ret = ubus_unsubscribe(ctx, &ubus_subscribe_handler, daemon_services[i].id);
                        SLOGW("UNsubscribe object %s with id %08X: %s\n", 
                            daemon_services[i].name, daemon_services[i].id, ubus_strerror(ret));
                    }
                    
                    daemon_services[i].id = id;
                    if(daemon_services[i].timer_restart)
                    {
                        timerCancel(&(daemon_services[i].timer_restart));
                    }
                    daemon_services[i].restart_time = (unsigned int)time(NULL);
                    daemon_services[i].timer_restart = 0;

                    ubus_subscribe_handler.remove_cb = VR_(handle_ubus_subscriber_remove);
                    ubus_subscribe_handler.cb = VR_(handle_ubus_notification);
                    ret = ubus_subscribe(ctx, &ubus_subscribe_handler, daemon_services[i].id);
                    if (ret)
                    {
                        SLOGW("Could not subscribe %s with id %08X. Reason:  %s\n", 
                            daemon_services[i].name, daemon_services[i].id, ubus_strerror(ret));
                    }
                    else
                    {
                        SLOGI("[INFO] Has successfully subscribed ubus object (name: id) = (%s, %08X) \n",
                         daemon_services[i].name, daemon_services[i].id);
                    }
                }    
            }
        }
    }
    else if(!strcmp(type, ST_UPDATE_SUPPORTED_DB))
    {
        SLOGI("pubsub re-open supported database\n");
        if(support_devs_db)
        {
            sqlite3_close(support_devs_db);
        }

        open_database(SUPPORTED_DEVS_DATABASE, &support_devs_db);

        struct blob_attr *tb[1];
        blobmsg_parse(update_supported_event, ARRAY_SIZE(update_supported_event), tb, blob_data(msg), blob_len(msg));

        const char *version = "unknown";
        if(tb[0])
        {
            version = blobmsg_data(tb[0]);
        }
        
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_PUBSUB));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
        json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_UPDATE_SUPPORTED_DB));
        json_object_object_add(jobj, ST_VERSION, json_object_new_string(version));
        VR_(Send_message)(NULL, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
        json_object_put(jobj);
    }
    else if(!strcmp(type, ST_POWER_CYCLE))
    {
        if(!g_inform_power_cycle)
        {
            SLOGI("PUBSUB INFORM POWER CYCLE\n");
            pthread_t power_event_thread;
            pthread_create(&power_event_thread, NULL, (void *)&power_event_thread_cb, NULL);
            pthread_detach(power_event_thread);
            g_inform_power_cycle = 1;
        }
    }
    // else if(!strcmp(type, ST_ALLJOYN))
    // {
    //     char *msgstr = blobmsg_format_json(msg, true);
    //     VR_(Send_message)(NULL, VR_QOS, msgstr, 0);
    //     free(msgstr);
    // }
    else if(!strcmp(type, ST_RESOURCE))
    {
        struct blob_attr *tb[3];
        blobmsg_parse(ubus_event_resource_policy, ARRAY_SIZE(ubus_event_resource_policy),
                        tb, blob_data(msg), blob_len(msg));

        if(!tb[0] || !tb[1] || !tb[2])
        {
            return;
        }

        const char *deviceId = blobmsg_data(tb[0]);
        const char *deviceType = blobmsg_data(tb[1]);
        const char *data = blobmsg_data(tb[2]);

        adding_failed_post_resouce_to_list((char*)deviceId, (char*)deviceType, (char*)data);
    }
}

void restart_service(void *data)
{
    printf("in restart_service data = %s\n", (char *)data);
    if(!data)
    {
        return;
    }
    int i;
    for (i = 0; i< sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp((char*)data, daemon_services[i].service_name))
        {
            daemon_services[i].timer_restart = 0;
        }
    }
}

static void remove_action_timeout_cb(void *data)
{
    //printf("remove_action_timeout_cb\n");
    while(1)
    {
        new_pubsub_ubus *tmp = g_pubsub_ubus;
        if(tmp)
        {
            unsigned int time_now = (unsigned)time(NULL);
            //printf("tmp->action_time = %d\n", tmp->action_time);
            if((time_now - tmp->action_time) > 40)
            {
                //printf("ACTION %s TIME %d WAS TIMEOUT\n", tmp->name, tmp->action_time);
                if(tmp->name && strlen(tmp->name))
                {
                    int i;
                    for (i = 0; i < sizeof(cmd_support)/sizeof(char*); i++)
                    {
                        //printf("cmd_support[i] = %s\n", cmd_support[i]);
                        if(!strcmp(tmp->name, cmd_support[i]) && tmp->service_name)
                        {
                            char method[256];
                            snprintf(method, sizeof(method), "%sR", tmp->name);

                            json_object *jobj = json_object_new_object();
                            json_object_object_add(jobj, ST_TYPE, json_object_new_string((char*)tmp->service_name));
                            json_object_object_add(jobj, ST_METHOD, json_object_new_string(method));
                            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                            json_object_object_add(jobj, ST_REASON, json_object_new_string("Action timeout"));
                            VR_(Send_message)(tmp->topic, VR_QOS, (char *)json_object_to_json_string(jobj), 0);
                            json_object_put(jobj);

                            int j;
                            for (j = 0; j< sizeof(daemon_services)/sizeof(daemon_service); j++)
                            {
                                if(!strcmp((char*)tmp->service_name, daemon_services[j].name))
                                {
                                    daemon_services[j].timeout++;
                                    printf("daemon_services[j].timeout = %d\n", daemon_services[j].timeout);
                                    printf("daemon_services[j].timer_restart = %lu\n", (long)daemon_services[j].timer_restart);
                                    if(daemon_services[j].timeout>2)
                                    {
                                        if(!daemon_services[j].timer_restart)
                                        {
                                            if((daemon_services[j].restart_time > tmp->action_time) && (tmp->action_time != 0))
                                            {
                                                daemon_services[j].timeout = 0;
                                                break;
                                            }
                                            printf("##### ACTION timeout %d, restart service %s #########\n", 
                                                daemon_services[j].timeout, daemon_services[j].name);
                                            daemon_services[j].timeout = 0;
                                            daemon_services[j].restart_time = (unsigned int)time(NULL);
                                            timerStart(&(daemon_services[j].timer_restart), restart_service, 
                                                                            (void*)daemon_services[j].service_name, 
                                                                            10000, TIMER_ONETIME);
                                            char cmd[256],log_location[SIZE_128B]={0};
                                            get_log_location(log_location, sizeof(log_location));

                                            snprintf(cmd, sizeof(cmd), "/etc/start_services.sh start_service %s >> %s",
                                                                    daemon_services[j].service_name, log_location);
                                            system((const char*)cmd);
                                            break;
                                        }
                                        else
                                        {
                                            daemon_services[j].timeout = 0;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                VR_(remove_list)(tmp);
            }
        }
        sleep(1);
    }
    
    // uloop_timeout_set(timeout, 1000);
}

static int VR_(ubus_service_init)()
{
    int ret, i;
    const char *ubus_socket = NULL;

    uloop_init();
    ctx = ubus_connect(ubus_socket);
    if (!ctx) {
        SLOGE("Failed to connect to ubus\n");
        return -1;
    }

    ubus_add_uloop(ctx);

    // ubus initialization 
    ret = ubus_add_object(ctx, &pubsub_object);
    if (ret) 
    {
        SLOGE("Failed to add_object object: %s\n", ubus_strerror(ret));
        return -1;
    }

    // ubus subscriber's initialization 
    ubus_subscribe_handler.remove_cb = VR_(handle_ubus_subscriber_remove);
    ubus_subscribe_handler.cb = VR_(handle_ubus_notification);            
    ret = ubus_register_subscriber(ctx, &ubus_subscribe_handler);

    for(i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(ubus_lookup_id(ctx, daemon_services[i].name, &daemon_services[i].id))
        {
            // SLOGI("The ubus service %s is invisible \n", daemon_services[i].name);
        }
        else
        {
            ret = ubus_subscribe(ctx, &ubus_subscribe_handler, daemon_services[i].id);
            SLOGW("Watching object %s with id %08X: %s\n", daemon_services[i].name, 
                daemon_services[i].id, ubus_strerror(ret));
        }
    }

    // ubus event's initialization 
    memset(&ubus_event_listener, 0, sizeof(ubus_event_listener));
    ubus_event_listener.cb = VR_(handle_ubus_event);
    ret = ubus_register_event_handler(ctx, &ubus_event_listener, "ubus.object.add");
    if (ret)
    {
        SLOGE("Failed to register event handler: %s\n", ubus_strerror(ret));
        return -1;
    }

    ret = ubus_register_event_handler(ctx, &ubus_event_listener, ST_UPDATE_SUPPORTED_DB);
    if (ret)
    {
        SLOGE("Failed to register event handler %s: %s\n", ST_UPDATE_SUPPORTED_DB, ubus_strerror(ret));
        return -1;
    }

    ret = ubus_register_event_handler(ctx, &ubus_event_listener, ST_POWER_CYCLE);
    if (ret)
    {
        SLOGE("Failed to register event handler %s: %s\n", ST_POWER_CYCLE, ubus_strerror(ret));
        return -1;
    }

    ret = ubus_register_event_handler(ctx, &ubus_event_listener, ST_RESOURCE);
    if (ret)
    {
        SLOGE("Failed to register event handler %s: %s\n", ST_RESOURCE, ubus_strerror(ret));
        return -1;
    }

    uloop_timeout_set(&auto_upload, 60*1000);
    uloop_run();
    return 0;
}

static void VR_(free_ubus_service)(void)
{
    if(ctx)
    {
        ubus_free(ctx);
    }

    uloop_done();

    if(buff.buf)
    {
        free(buff.buf);
    }

    if(rule_db)
    {
        sqlite3_close(rule_db);
    }

    if(dev_db)
    {
        sqlite3_close(dev_db);
    }

    if(user_db)
    {
        sqlite3_close(user_db);
    }

    if(support_devs_db)
        sqlite3_close(support_devs_db);
}

/***********************************************************************
Return Value:
    0: OK
************************************************************************/

static void VR_(mosquitto_close)()
{
    // Send byebye message 
    VR_(remote_send_bye_msg)();
    //clean up
    mosquitto_destroy(mosq);
    mosquitto_lib_cleanup();
}

void VR_(handle_signal)(int signo)
{
    SLOGI("########### GOT SIGNAL %d\n #############", signo);
    if(signo == SIGSEGV)
    {
        uint32_t *up = (uint32_t *) &signo;
        pmortem_connect_and_send(up, 8 * 1024);
        fprintf(stderr, "SIGSEGV [%d].  Best guess fault address: %08x, ra: %08x, sig return: %p\n",
            signo, up[8], up[72], __builtin_return_address(0));
    }

    exit(1);
    //g_remote_sevice_is_running = 0;
    if(mosq)
    {
        mosquitto_loop_stop(mosq, 1);
    }
    // pthread_cancel(thread_mosquitto);
    // if(thread_remove_actions_timeout)
    // {
    //     printf("start cancel\n");
    //     pthread_cancel(thread_remove_actions_timeout);
    // }

    remove_actions_timeout_enable = 0;
    uloop_end();
    exit(1);
}

// /***********************************************************************
// Description:
// monitor mosquitor connection
// ************************************************************************/
void VR_(monitor_connection)(void)
{
    int retry_connect = 0;
    while(monitor_connection_enable)
    {
        if(g_is_disconnected)
        {
            if(!retry_connect)
            {
                retry_connect = 1;
                sleep(CHECK_CONNECTION_TIME);
                continue;
            }

            SLOGI("disconnect mosquitto\n");
            int rc = mosquitto_disconnect(mosq);
            if(MOSQ_ERR_SUCCESS == rc || MOSQ_ERR_NO_CONN == rc)
            {
                mosquitto_loop_stop(mosq, true);
                sleep(1);
                SLOGI("mosquitto connect again\n");
                mosquitto_connect(mosq, g_mqtt_address, AWS_IOT_MQTT_PORT, KEEP_ALIVE_TIME);
                mosquitto_loop_start(mosq);
            }

            sleep(CHECK_CONNECTION_TIME);
        }
        else
        {
            retry_connect = 0;
            sleep(CHECK_CONNECTION_TIME);
        }
    }
};

// /***********************************************************************
// Description:
// Main thread of mosquitto 
// ************************************************************************/
void VR_(mosquitto_main_thread)(void)
{
    SLOGI("mosquitto_main_thread\n");
    while(!VR_(is_wireless_up)(ST_STA_MODE)) //not STA mode, wait here
    {
        usleep(100000);
    }
    printf("g_mqtt_address = %s\n", g_mqtt_address);

    // int rc = mosquitto_connect(mosq, g_mqtt_address, AWS_IOT_MQTT_PORT, KEEP_ALIVE_TIME);
    mosquitto_connect(mosq, g_mqtt_address, AWS_IOT_MQTT_PORT, KEEP_ALIVE_TIME);
    mosquitto_loop_start(mosq);
    // while(g_remote_sevice_is_running)
    // {
    //     rc = mosquitto_loop(mosq, -1, 1);
    //     // if(g_remote_sevice_is_running && rc)
    //     // {
    //     //     printf("rc = %d\n", rc);
    //     //     sleep(1);
    //     //     int test = mosquitto_reconnect(mosq);
    //     //     printf("test = %d\n", test);
    //     // }
    // }
    
    SLOGI("end mosquitto_main_thread\n");
};

int VR_(overwrite_certs)(char *filename,const char *data)
{
    int i, k=0, blackslash_appear=0;
    char *data_convert = (char *)malloc(strlen(data)+1);

    for(i=0; i < strlen(data); i++) 
    {
        if(data[i] == '\\')
        {
            blackslash_appear = 1;
        }
        else
        {
            if(blackslash_appear)
            {
                if(data[i] == 'n')
                {
                    data_convert[k++] = '\n';
                }
                else
                {
                    data_convert[k++] = data[i];
                }
            }
            else
            {
                data_convert[k++] = data[i];
            }
            blackslash_appear = 0;
        }
    }
    data_convert[k] = '\0';
    SLOGI("data_convert = %s\n", data_convert);

    FILE *fp;
    fp = fopen(filename, "w");
    if(fp)
    {
        fprintf(fp, "%s", data_convert);
        fclose(fp);
        return 1;
    }
    
    return 0;
}

static int _check_claimed()
{
    char *token = VR_(read_option)(NULL, UCI_CLOUD_TOKEN);
    if(!token)
    {
        SLOGI("Token not found\n");
        return -1;
    }

    if(!strcmp(token, "default"))
    {
        SLOGI("Token invalid\n");
        free(token);
        return -1;
    }

    char *hubUUID = VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
    if(!hubUUID)
    {
        SLOGI("hubUUID not found\n");
        free(token);
        return -1;
    }

    int res = 0;
    char *result = NULL;
    char address[SIZE_1024B], headerIn[SIZE_1024B];
    snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);

    char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    if(!cloud_address)
    {
        snprintf(address, sizeof(address), CHECK_CLAIMED_DOMAIN, CLOUD_DOMAIN, hubUUID);
    }
    else
    {
        snprintf(address, sizeof(address), CHECK_CLAIMED_DOMAIN, cloud_address, hubUUID);
    }

    res = VR_(get_request)(address, headerIn, NULL, NULL, &result, DEFAULT_TIMEOUT);
    if(res)
    {
        SLOGE("failed to get claimed\n");
        res = -1;
        goto claimed_done;
    }
    SLOGI("result = %s\n", result);
    json_object *claimedObj = VR_(create_json_object)(result);
    if(!claimedObj)
    {
        SLOGE("data return is not json object\n");
    }

    CHECK_JSON_OBJECT_EXIST(isClaimedObj, claimedObj, ST_CLOUD_ISCLAIMED, claimed_done);

    res = json_object_get_int(isClaimedObj);

claimed_done:
    SAFE_FREE(result);
    SAFE_FREE(token);
    SAFE_FREE(hubUUID);
    SAFE_FREE(cloud_address);
    return res;
}

static void inform_unclaimed(void *data)
{
    VR_(execute_system)("ubus send pubsub '{\"state\":\"unclaimed\"}' &");
    timerStart(&g_timer_unclaimed, inform_unclaimed, NULL, 120000, TIMER_ONETIME);
}

static void check_claimed()
{
    // int check_claim_retry = 180;
    int rc = _check_claimed();

    if(rc != 1 && !g_timer_unclaimed)
    {
        timerStart(&g_timer_unclaimed, inform_unclaimed, NULL, 30000, TIMER_ONETIME);
    }

    while(rc != 1)
    {
        sleep(1);   
        rc = _check_claimed();
    }

    timerCancel(&g_timer_unclaimed);

    VR_(write_option)(NULL, UCI_CLOUD_STATE"=claimed");

    // char bssid[SIZE_32B], ssid[SIZE_32B];
    // get_hub_bssid(bssid, sizeof(bssid));
    // get_hub_ssid(ssid, sizeof(ssid));
    // VR_(update_hub_infor)(bssid, ssid);
    
    sleep(TURN_AP_OFF_TIMEOUT);/*too make sure app get result from cloud*/

    VR_(execute_system)("ubus send pubsub '{\"state\":\"claimed\"}' &");
    VR_(write_option)(NULL, UCI_WIFI_AP_MODE_DISABLED"=1");
    VR_(execute_system)("wifi");
    VR_(execute_system)("/etc/init.d/ble restart &");
    
    sleep(TURN_AP_OFF_TIMEOUT+5);/*too make sure ap was turn down before change state*/
}

int VR_(register_device)(void)
{
    int res = -1;
    char cloud_addr[SIZE_256B];   
    if(!VR_(is_wireless_up)(ST_STA_MODE)) //not STA mode, wait here
    {
        //could not connect to router, check your router power 
        //and try to move your venus to close your router.
        return res;
    }

    while(router_connect_status("wlan0"))
    {
        usleep(100000);
    }

    char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    if(!cloud_address)
    {
        snprintf(cloud_addr, sizeof(cloud_addr), CLOUD_ADDRESS, CLOUD_DOMAIN);
    }
    else
    {
        snprintf(cloud_addr, sizeof(cloud_addr), CLOUD_ADDRESS, cloud_address);
        free(cloud_address);
    }

    int retry = 4;
    int ping_result = ping_command("8.8.8.8", 3);
    while(ping_result && retry>0)
    {
        ping_result = ping_command("8.8.8.8", 3);
        retry--;
    }

    if(ping_result)
    {
        VR_(execute_system)("ubus send pubsub '{\"state\":\"connect_failed\"}' &");
        return REGISTER_CONNECT_ERROR;
    }

    int isConnect = VR_(check_internet_connection)(cloud_addr);
    retry = 3;
    while(isConnect && retry > 0)
    {
        //could not connect to server, check your internet connection.
        retry--;
        sleep(2);
        isConnect = VR_(check_internet_connection)(cloud_addr);
    }

    if(isConnect)
    {
        VR_(execute_system)("ubus send pubsub '{\"state\":\"connect_failed\"}' &");
        return REGISTER_CONNECT_ERROR;
    }

    char *homeId = NULL;
    homeId = (char *)VR_(read_option)(NULL, UCI_CLOUD_HOME_ID);

    char *claimToken = NULL;
    claimToken = (char *)VR_(read_option)(NULL, UCI_LOCAL_CLAIM_TOKEN);

    if(homeId && claimToken)
    {
        int retry=3;

        char hubID[SIZE_128B], hubName[SIZE_128B], request_data[SIZE_512B], bssid[SIZE_32B];

        get_hub_id(hubID, sizeof(hubID));
        get_hub_bssid(bssid, sizeof(bssid));
        get_hub_name(hubName, sizeof(hubName));

        snprintf(request_data, sizeof(request_data),
                "{\"homeId\":\"%s\","
                  "\"localId\":\"%s\","
                  "\"claimToken\":\"%s\","
                  "\"name\":\"%s\","
                  "\"bssid\":\"%s\""
                  "}", 
                homeId, hubID, claimToken, hubName, bssid);
        SLOGI("request_data = %s\n", request_data);
        while(res && (retry>0))
        {
            char cloud_reg_addr[SIZE_1024B];
            char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
            if(!cloud_address)
            {
                snprintf(cloud_reg_addr, sizeof(cloud_reg_addr), CLOUD_REGISTER_ADDRESS, CLOUD_DOMAIN);
            }
            else
            {
                snprintf(cloud_reg_addr, sizeof(cloud_reg_addr), CLOUD_REGISTER_ADDRESS, cloud_address);
                free(cloud_address);
            }
            char *result = NULL;
            res = VR_(post_request)(cloud_reg_addr, NULL, request_data, NULL, &result, DEFAULT_TIMEOUT);
            if(!res)
            {
                SLOGI("######## RESPONSE = %s\n", result);
                json_object *jobj = VR_(create_json_object)(result);
                if(jobj)
                {
                    json_object *error = json_object_object_get(jobj, "error");
                    if(error)
                    {
                        printf("##### ERROR MESSAGE #### = %s\n", 
                                json_object_to_json_string(json_object_object_get(error, "message")));
                        retry--;
                        res = REGISTER_CLOUD_ERROR;
                        sleep(1);
                        json_object_put(jobj);
                    }
                    else
                    {
                        const char *hubUUID = json_object_get_string(json_object_object_get(jobj, "id"));
                        SLOGI("hubUUID = %s\n", hubUUID)

                        const char *access_token = json_object_get_string(json_object_object_get(jobj, "access_token"));
                        SLOGI("access_token = %s\n", access_token)

                        json_object *mqttAuth = json_object_object_get(jobj, "mqttauth");
                        const char *endpointAddress = json_object_get_string(json_object_object_get(mqttAuth, "endpointAddress"));
                        SLOGI("endpointAddress = %s\n", endpointAddress);

                        if(hubUUID && strcmp(hubUUID, "null") &&
                           access_token && strcmp(access_token, "null") &&
                           endpointAddress && strcmp(endpointAddress, "null")
                           )
                        {
                            VR_(write_option)(NULL, UCI_CLOUD_HUB_UUID"=%s", hubUUID);
                            VR_(write_option)(NULL, UCI_CLOUD_MQTT_ADDRESS"=%s", endpointAddress);
                            VR_(write_option)(NULL, UCI_CLOUD_TOKEN"=%s", access_token);
                            VR_(write_option)(NULL, UCI_CLOUD_STATE"=registered");
                            VR_(write_option)(NULL, UCI_FIREWALL_MASQ"=1");
                            VR_(execute_system)("/etc/init.d/firewall restart");
                            json_object_put(jobj);

                            VR_(execute_system)("ubus send pubsub '{\"state\":\"registered\"}' &");
                        }
                        else
                        {
                            retry--;
                            res = REGISTER_CLOUD_ERROR;
                            sleep(1);
                            json_object_put(jobj);
                        }
                    }
                }
                else
                {
                    SLOGE("Failed to parse json\n");
                    retry--;
                    res = REGISTER_CLOUD_ERROR;
                    sleep(1);
                }
            }
            else
            {
                retry--;
                SLOGE("####### FAILED TO POST REQUEST TO SERVER\n");
            }
            SAFE_FREE(result);
            sleep(1);
        }
    }

    if(homeId)
    {
        free(homeId);
    }

    if(claimToken)
    {
        free(claimToken);
    }

    return res;
}
// /***********************************************************************
// Return Value:
//     0: OK
//     -1: Mosquitto initialization
// ************************************************************************/

static int VR_(mosquitto_init)(void)
{
    SLOGI("start to init\n");
    int res = -1, retry_change_state = 3;
    char *remote_state = NULL;
    remote_state = (char *)VR_(read_option)(NULL, UCI_CLOUD_STATE);

    if(remote_state && !strcmp(remote_state, "available"))
    {
        free(remote_state);
        return res;
    }

    VR_(set_led_state)(g_led_shm, ST_Default);//make sure change default led

    if(remote_state && 
        (!strcmp(remote_state, "configured") 
        || !strcmp(remote_state, "register_failed"))
        )
    {
        //may be venus has not internet or server got problem.
        int rc = VR_(register_device)();
        while(rc)
        {
            if(retry_change_state > 0)
            {
                retry_change_state--;
            }
            else
            {
                VR_(write_option)(NULL, UCI_CLOUD_STATE"=register_failed");
                if(rc == REGISTER_CONNECT_ERROR)
                {
                    VR_(execute_system)("ubus send pubsub '{\"state\":\"register_failed\", \"reason\":\"connectError\"}' &");
                }
                else if(rc == REGISTER_CLOUD_ERROR)
                {
                    VR_(execute_system)("ubus send pubsub '{\"state\":\"register_failed\", \"reason\":\"cloudError\"}' &");
                }
                sleep(180);
            }
            
            SLOGI("FAILED to register venus device, retry again\n");
            rc = VR_(register_device)();
        }

        /*update remote state*/
        SAFE_FREE(remote_state);
        remote_state = (char *)VR_(read_option)(NULL, UCI_CLOUD_STATE);
    }

    if(remote_state && !strcmp(remote_state, "registered"))
    {
        check_claimed();
        /*update remote state*/
        SAFE_FREE(remote_state);
        remote_state = (char *)VR_(read_option)(NULL, UCI_CLOUD_STATE);  
    }

    if(remote_state && !strcmp(remote_state, "claimed"))
    {
        VR_(write_option)(NULL, UCI_CLOUD_STATE"=connected");
    }

    SAFE_FREE(remote_state);

    char *mqtt_address = VR_(read_option)(NULL, UCI_CLOUD_MQTT_ADDRESS);
    char *mqtt_clientId = VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
    char *cloud_token = VR_(read_option)(NULL, UCI_CLOUD_TOKEN);

    if(!mqtt_address
        || !mqtt_clientId
        || !cloud_token
        )
    {
        SLOGI("Failed to get mqtt_address or mqtt_clientId\n");
        return res;
    }

    SAFE_STRCPY(g_mqtt_address, mqtt_address);
    SAFE_STRCPY(g_mqtt_clientId, mqtt_clientId);
    snprintf(PUBLISH_RES_TOPIC, sizeof(PUBLISH_RES_TOPIC), "%s/res", g_mqtt_clientId);
    snprintf(SUBSCRIBE_REQ_TOPIC, sizeof(SUBSCRIBE_REQ_TOPIC), "%s/req", g_mqtt_clientId);

    snprintf(PUBLISH_SHADOW_UPDATE_TOPIC, sizeof(PUBLISH_SHADOW_UPDATE_TOPIC), "$iot/things/%s/shadow/update", g_mqtt_clientId);
    snprintf(SUBSCRIBE_CONTROL_TOPIC, sizeof(SUBSCRIBE_CONTROL_TOPIC), "%s/control", g_mqtt_clientId);
    snprintf(SUBSCRIBE_SETTING_TOPIC, sizeof(SUBSCRIBE_SETTING_TOPIC), "%s/setting", g_mqtt_clientId);
    // sprintf(SUBSCRIBE_SHADOW_DELTA_TOPIC, "$aws/things/%s/shadow/update/delta", g_mqtt_clientId);
    // sprintf(SUBSCRIBE_SHADOW_UPDATE_REJECTED_TOPIC, "$aws/things/%s/shadow/update/rejected", g_mqtt_clientId);
    // sprintf(SUBSCRIBE_SHADOW_UPDATE_ACCEPTED_TOPIC, "$aws/things/%s/shadow/update/accepted", g_mqtt_clientId);

    mosquitto_lib_init();
    
    printf("g_mqtt_clientId = %s\n", g_mqtt_clientId);

    mosq = mosquitto_new(g_mqtt_clientId, true, NULL);

    if(!mosq)
    {
        SLOGE("FAILED TO CREATE MOSQUITTO\n");
        goto init_done;
    }

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string("pubsub"));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_DISCONNECTED));

    // mosquitto_log_callback_set(mosq, VR_(my_log_callback));
    mosquitto_will_set(mosq, PUBLISH_RES_TOPIC, strlen((char*)json_object_to_json_string(jobj)),
                        (char*)json_object_to_json_string(jobj), VR_QOS, false);

    json_object_put(jobj);

    mosquitto_connect_callback_set(mosq, VR_(mosquitto_connect_callback));
    mosquitto_message_callback_set(mosq, VR_(mosquitto_message_callback));
    mosquitto_disconnect_callback_set(mosq, VR_(mosquitto_disconnect_callback));
    mosquitto_subscribe_callback_set(mosq, VR_(mosquitto_subscribe_callback));

    mosquitto_username_pw_set(mosq, mqtt_clientId, cloud_token);
    mosquitto_tls_set(mosq, 
                        // NULL,
                        AWS_IOT_ROOT_CA_FILENAME,
                        AWS_IOT_ROOT_CA,
                        NULL,
                        // AWS_IOT_CERTIFICATE_FILENAME,
                        NULL,
                        // AWS_IOT_PRIVATE_KEY_FILENAME,
                        NULL);
    mosquitto_tls_opts_set(mosq, 0, "tlv1.2", NULL);
    // mosquitto_opts_set(mosq, MOSQ_OPT_PROTOCOL_VERSION, &protocol_version);

    mosquitto_message_retry_set(mosq, 3);
    mosquitto_reconnect_delay_set(mosq, 1, 5, false);

    int rc = pthread_create(&thread_mosquitto, NULL, (void *) &VR_(mosquitto_main_thread), NULL);
    if (rc != 0)
    {
        SLOGI("Can not create thread");
    }
    pthread_detach(thread_mosquitto);

    pthread_t thread_monitor_connection;
    pthread_create(&thread_monitor_connection, NULL, (void *) &VR_(monitor_connection), NULL);
    pthread_detach(thread_monitor_connection);

    rc = pthread_create(&thread_remove_actions_timeout, NULL, (void *) &remove_action_timeout_cb, NULL);
    if (rc != 0)
    {
        SLOGI("Can not create thread_remove_actions_timeout");
    }
    pthread_detach(thread_remove_actions_timeout);
    res = 0;

init_done:
    SAFE_FREE(mqtt_address);
    SAFE_FREE(mqtt_clientId);
    SAFE_FREE(cloud_token);
    return res;
}

int main(int argc, char** argv) 
{
    SLOG_Init(SLOG_STDOUT, NULL);
    set_thread_name("pubsub_handler");
    struct sigaction action;

    char cmd[256];
    memset(cmd, 0x00, sizeof(cmd));
    sprintf(cmd, "%s", "pgrep pubsub_handler | head -n -1 | cut -c-5 | xargs kill -9 &>/dev/null");
    system(cmd);

    timerInit();
    tls_init();
    init_report_filter_list();
    init_curl_request_list();

    pthread_mutex_init(&pubsub_ubus_listMutex, 0);
    open_database(USERS_DATABASE, &user_db);
    open_database(DEVICES_DATABASE, &dev_db);
    open_database(RULE_DATABASE, &rule_db);
    open_database(SUPPORTED_DEVS_DATABASE, &support_devs_db);

    memset(&action, 0, sizeof(action));
    action.sa_flags = (SA_NOCLDSTOP | SA_NOCLDWAIT | SA_RESTART);
    action.sa_handler = VR_(handle_signal);
    sigaction(SIGSEGV, &action, NULL);
    sigaction(SIGINT, &action, NULL);
    sigaction(SIGTERM, &action, NULL);
    sigaction(SIGKILL, &action, NULL);
    //signal(SIGUSR1, VR_(Usr1Handler));

    if(shm_init(&g_shm, &g_shmid))
    {
        SLOGI("Failed to init share memory\n");
        return 0;
    }

    if(led_shm_init(&g_led_shm, &g_led_shmid, "/etc/led_control.sh"))
    {
        SLOGI("Failed to init led share memory\n");
        return 0;
    }

    pthread_t curl_request_thread;
    pthread_create(&curl_request_thread, NULL, (void *) &request_thread, NULL);
    pthread_detach(curl_request_thread);

    pthread_t handle_post_resource_failed_thread;
    pthread_create(&handle_post_resource_failed_thread, NULL, (void *) &handle_post_resource_failed, NULL);
    pthread_detach(handle_post_resource_failed_thread);

    if (0 == VR_(mosquitto_init)())
    {
        VR_(ubus_service_init)();
        VR_(free_ubus_service)();
        VR_(mosquitto_close)();
    }

    stop_request_thread();
    stop_post_failed_resource();
    tls_cleanup();
    sleep(1);
    SLOGI("END REMOTE\n");
    return 0;
};
