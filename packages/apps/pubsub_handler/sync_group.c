#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "vr_rest.h"
#include "database.h"
#include "verik_utils.h"
#include "slog.h"
#include "sync_group.h"
#include "group_apis.h"
/*
    - how to detect name duplicate
    - junk duplicat name in cloud.
*/
static group_list_t g_group_list;
// pthread_mutex_t g_group_list_queueMutex;/*do we need this??*/

extern sqlite3 *dev_db;
extern char *g_shm;
extern char g_mqtt_clientId[256];
int g_retry = 0;

#define ETAG_PARTERN "ETag: "

static void update_Etag(char *header)
{
    if(!header)
    {
        return;
    }

    char *ch = strstr(header, ETAG_PARTERN);
    if(ch)
    {
        char *endLine = strstr(ch, "\r\n");
        if(!endLine)
        {
            return;
        }

        size_t ETagNum = endLine - ch - strlen(ETAG_PARTERN);
        char newETag[SIZE_256B];
        if(ETagNum > (sizeof(newETag)-1))
        {
            return;
        }

        strncpy(newETag, ch+strlen(ETAG_PARTERN), ETagNum);
        newETag[ETagNum] = '\0';

        VR_(write_option)(NULL, GROUP_ETAG_ADDR"=%s", newETag);
    }
}

static void init_group_list()
{
    VR_INIT_LIST_HEAD(&g_group_list.list);
    // pthread_mutex_init(&g_group_list_queueMutex, 0);
}

static int add_new_group_to_list(void *node_add)
{
    int res = 0;
    // pthread_mutex_lock(&g_group_list_queueMutex);
    group_list_t *input = (group_list_t *)node_add;
    group_list_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &(g_group_list.list), list)
    {
        if(!strcmp(tmp->groupId, input->groupId))
        {
            res = 1;
            break;
        }
    }

    if(!res)
    {
        VR_(list_add_tail)(&(input->list), &(g_group_list.list));
    }

    // pthread_mutex_unlock(&g_group_list_queueMutex);
    return res;
}

// static void remove_group_from_list(char *groupId)
// {
//     if(!groupId)
//     {
//         return;
//     }

//     pthread_mutex_lock(&g_group_list_queueMutex);
//     group_list_t *tmp = NULL;

//     struct VR_list_head *pos, *q;
//     VR_(list_for_each_safe)(pos, q, &g_group_list.list)
//     {
//         tmp = VR_(list_entry)(pos, group_list_t, list);
//         if(tmp->groupId && !strcmp(tmp->groupId, groupId))
//         {
//             VR_(list_del)(pos);
//             free_group_list(tmp);
//         }
//     }
//     pthread_mutex_unlock(&g_group_list_queueMutex);
// }

// static void printf_group_list(group_list_t *groupList)
// {
//     int i=0;
//     group_list_t *tmp = NULL;
    
//     VR_(list_for_each_entry)(tmp, &groupList->list, list)
//     {
//         printf("#### dev %d ######\n", i);
//         printf("userId = %s\n", tmp->userId);
//         printf("groupId = %s\n", tmp->groupId);
//         printf("groupName = %s\n", tmp->groupName);
//         printf("groupDev = %s\n", tmp->groupDev);
//         printf("cloudId = %s\n", tmp->cloudId);
//         i++;
//     }
// }

static void free_group_list(group_list_t *tmp)
{
    if(!tmp)
    {
        return;
    }

    SAFE_FREE(tmp->userId);
    SAFE_FREE(tmp->groupId);
    SAFE_FREE(tmp->groupName);
    SAFE_FREE(tmp->groupDev);
    SAFE_FREE(tmp->cloudId);
    SAFE_FREE(tmp->cloudName);
    SAFE_FREE(tmp);
}

static void _update_cloud_group(char *cloudId, char *groupName)
{
    int found = 0, res = 0;
    // pthread_mutex_lock(&g_group_list_queueMutex);
    group_list_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_group_list.list)
    {
        tmp = VR_(list_entry)(pos, group_list_t, list);
        if(tmp->cloudId && !strcmp(tmp->cloudId, cloudId))
        {
            found = 1;
            break;
        }
    }
    

    if(found)
    {
        res = VR_(cloud_modify_group)(g_shm, tmp->groupId, tmp->groupName, tmp->groupDev);
        /*if error is dupplicate name then passing it
            will be post later.
        */
        if(GROUP_NAME_DUPPLICATE != res)
        {
            VR_(list_del)(pos);
            free_group_list(tmp);
        }
        else
        {
            size_t newLength = strlen(tmp->groupName) + strlen(groupName);
            char *newName = (char*)malloc(newLength+SIZE_256B);
            sprintf(newName, "%s(old is %s %u)", tmp->groupName, groupName, (unsigned int)time(NULL));
            VR_(cloud_modify_group)(g_shm, tmp->groupId, newName, tmp->groupDev);

            tmp->cloudName = newName;
        }
    }
    else
    {
        /*if cloud groupt id does not exist in local,
        remove it
        */
        cloud_request_delete_group((char*)cloudId);
        shm_update_data(g_shm, (char*)cloudId, NULL, NULL, SHM_DELETE);
    }

    // pthread_mutex_unlock(&g_group_list_queueMutex);
}

static int is_list_resource_changed(char *newGroupDev, char *groupDev)
{
    int res = 0;
    if(!newGroupDev || !groupDev)
    {
        return res;
    }

    json_object *newGroupObj = VR_(create_json_object)(newGroupDev);
    if(!newGroupObj)
    {
        return res;
    }

    json_object *groupObj = VR_(create_json_object)(groupDev);
    if(!groupObj)
    {
        json_object_put(newGroupObj);
        return res;
    }

    enum json_type type = json_object_get_type(newGroupObj);
    if(json_type_array != type)
    {
        goto done;
    }

    type = json_object_get_type(groupObj);
    if(json_type_array != type)
    {
        goto done;
    }
    
    int arrayNewGroup = json_object_array_length(newGroupObj);
    int arrayGroup = json_object_array_length(groupObj);
    if(arrayNewGroup == arrayGroup)
    {
        int i,j,found;
        for(i=0; i<arrayNewGroup; i++)
        {
            found = 0;
            json_object *jNewValue = json_object_array_get_idx(newGroupObj, i);
            if(!jNewValue)
            {
                res = 1;
                goto done;
            }

            for(j=0; j<arrayGroup; j++)
            {
                json_object *jvalue = json_object_array_get_idx(groupObj, j);
                if(!jvalue)
                {
                    res = 1;
                    goto done;
                }
                if(!strcmp(json_object_get_string(jvalue), json_object_get_string(jNewValue)))
                {
                    found = 1;
                    break;
                }
            }

            if(!found)
            {
                res = 1;
                goto done;
            }
        }
    }
    else
    {
        res = 1;
    }

done:
    json_object_put(newGroupObj);
    json_object_put(groupObj);
    return res;
}

static void check_group_was_change(char *cloudId, char *groupName, char *groupDev)
{
    // pthread_mutex_lock(&g_group_list_queueMutex);
    int found_group = 0;
    int found_changed = 0;
    group_list_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_group_list.list)
    {
        tmp = VR_(list_entry)(pos, group_list_t, list);
        if(!tmp->cloudId)
        {
            continue;
        }

        if(strcmp(tmp->cloudId, cloudId))
        {
            continue;
        }

        if(!tmp->groupName)
        {
            continue;
        }

        found_group = 1;

        if(strcmp(tmp->groupName, groupName))
        {
            SLOGI("group %s has name changed from %s to %s\n", 
                    tmp->groupId, groupName, tmp->groupName);
            found_changed = 1;
            break;
        }

        if(is_list_resource_changed(tmp->groupDev, groupDev))
        {
            SLOGI("group %s has resources list changed from %s to %s\n", 
                    tmp->groupId, groupDev, tmp->groupDev);
            found_changed = 1;
        }
        break;
    }

    if(found_changed)
    {
        int res = VR_(cloud_modify_group)(g_shm, tmp->groupId, tmp->groupName, tmp->groupDev);
        /*if error is dupplicate name then passing it
            will be post later.
        */
        if(GROUP_NAME_DUPPLICATE != res)
        {
            VR_(list_del)(pos);
            free_group_list(tmp);
        }
        else
        {
            size_t newLength = strlen(tmp->groupName) + strlen(groupName);
            char *newName = (char*)malloc(newLength+SIZE_256B);
            sprintf(newName, "%s(old is %s)", tmp->groupName, groupName);

            VR_(cloud_modify_group)(g_shm, tmp->groupId, newName, tmp->groupDev);
            tmp->cloudName = newName;
        }
    }
    else if(found_group) /*nothing changed, should remove*/
    {
        VR_(list_del)(pos);
        free_group_list(tmp);
    }
    else
    {
        /*if cloud groupt id does not exist in local,
        remove it
        */
        cloud_request_delete_group((char*)cloudId);
        shm_update_data(g_shm, (char*)cloudId, NULL, NULL, SHM_DELETE);
    }

    // pthread_mutex_unlock(&g_group_list_queueMutex);

    return;
}

static void update_group(json_object *fromCloud)
{
    if(!fromCloud)
    {
        return;
    }

    CHECK_JSON_OBJECT_EXIST(cloudIdObj, fromCloud, ST_ID, update_group_done);
    CHECK_JSON_OBJECT_EXIST(groupNameObj, fromCloud, ST_NAME, update_group_done);
    CHECK_JSON_OBJECT_EXIST(hubUUIDObj, fromCloud, ST_DEVICE_ID, update_group_done);
    CHECK_JSON_OBJECT_EXIST(groupDev, fromCloud, ST_RESOURCE_GROUP_RESOURCE, update_group_done);
    const char *cloudId = json_object_get_string(cloudIdObj);
    const char *groupName = json_object_get_string(groupNameObj);
    const char *hubUUID = json_object_get_string(hubUUIDObj);

    if(strcmp(hubUUID, g_mqtt_clientId))
    {
        return;
    }

    enum json_type type = json_object_get_type(groupDev);
    if(json_type_array != type)
    {
        SLOGI("this is not json array format\n");
        _update_cloud_group((char*)cloudId, (char*)groupName);
        return;
    }

    ///*convert list resource from cloud to cloud new local format*/
    int i;
    int arrayLen = json_object_array_length(groupDev);
    json_object *resourceUUIDs = json_object_new_array();
    for(i=0; i<arrayLen; i++)
    {
        json_object *jvalue = json_object_array_get_idx(groupDev, i);
        CHECK_JSON_OBJECT_EXIST(resourceUUIDObj, jvalue, ST_RESOURCE_UUID, _next_resource_);
        char *localId = VR_(get_localid_from_cloudid)(g_shm, (char*)json_object_get_string(resourceUUIDObj), NULL);
        if(localId)
        {
            json_object_array_add(resourceUUIDs, json_object_new_string(localId));
            free(localId);
        }
_next_resource_:
        continue;
    }

    check_group_was_change((char*)cloudId, (char*)groupName, 
                            (char*)json_object_to_json_string(resourceUUIDs));

    json_object_put(resourceUUIDs);

update_group_done:
    return;
}

static void _post_dupplicate_name()
{
    // pthread_mutex_lock(&g_group_list_queueMutex);
    group_list_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_group_list.list)
    {
        tmp = VR_(list_entry)(pos, group_list_t, list);

        if(!tmp->cloudId)/*it is new group*/
        {
            char group_cloud_id[SIZE_128B];
            memset(group_cloud_id, 0x00, sizeof(group_cloud_id));
            int needFree = 0;
            int res = VR_(cloud_create_group)(g_shm, tmp->groupName, tmp->groupDev, 
                                          group_cloud_id, sizeof(group_cloud_id));

            if(GROUP_NAME_DUPPLICATE == res)
            {
                size_t newLength = strlen(tmp->groupName);
                char *newName = (char*)malloc(newLength+SIZE_256B);
                sprintf(newName, "%s(%u)", tmp->groupName, (unsigned)time(NULL));

                res = VR_(cloud_create_group)(g_shm, newName, tmp->groupDev, 
                                            group_cloud_id, sizeof(group_cloud_id));
                tmp->cloudName = newName;
            }
            else
            {
                needFree = 1;
            }

            if(!res && strlen(group_cloud_id))
            {
                tmp->cloudId = strdup(group_cloud_id);
                database_actions("pubsub_handler", dev_db, 
                                "INSERT INTO DEVICES(deviceId,deviceType,owner) "
                                "VALUES('%s','%s','%s')", tmp->groupId, ST_GROUP, group_cloud_id);
                shm_update_data(g_shm, group_cloud_id, tmp->groupId, ST_GROUP, SHM_ADD);
            }

            if(needFree)
            {
                VR_(list_del)(pos);
                free_group_list(tmp);
            }
        }
        else
        {
            int res = VR_(cloud_modify_group)(g_shm, tmp->groupId, tmp->groupName, tmp->groupDev);
            if(GROUP_NAME_DUPPLICATE != res)
            {
                VR_(list_del)(pos);
                free_group_list(tmp);
            }
            else
            {
                g_retry = 1;
            }
        }
    }
    // pthread_mutex_unlock(&g_group_list_queueMutex);
}

static int create_group_list_cb(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 5)
    {
        return 0;
    }

    int i;
    char *userId = NULL;
    char *groupId = NULL;
    char *groupName = NULL;
    char *groupDev = NULL;
    char *cloudId = NULL;

    for(i=0; i<argc; i++)
    {
        if(!strcmp(azColName[i], ST_USER_ID))
        {
            userId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_GROUP_ID))
        {
            groupId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_GROUP_NAME))
        {
            groupName = argv[i];
        }
        else if(!strcmp(azColName[i], ST_DEVICE))
        {
            groupDev = argv[i];
        }
        else if(!strcmp(azColName[i], ST_OWNER))
        {
            cloudId = argv[i];
        }
    }

    if(!userId || !groupId || !groupName || !groupDev)
    {
        SLOGI("missing info\n");
        return 0;
    }

    group_list_t *new_group = (group_list_t*)malloc(sizeof(group_list_t));
    memset(new_group, 0x00, sizeof(group_list_t));
    new_group->userId = strdup(userId);
    new_group->groupId = strdup(groupId);
    new_group->groupName = strdup(groupName);
    new_group->groupDev = strdup(groupDev);

    /*testing with create local group*/

    if(cloudId)
    {
        new_group->cloudId = strdup(cloudId);
    }

    if(add_new_group_to_list(new_group))
    {
        free_group_list(new_group);
    }
    return 0;
}

static void init_group_from_database()
{
    searching_database("pubsub_handler", dev_db, create_group_list_cb, NULL,
                        "SELECT %s,%s,%s,%s,%s from GROUPS LEFT JOIN DEVICES on groupId=deviceId",
                        ST_USER_ID, ST_GROUP_ID, ST_GROUP_NAME, ST_DEVICE, ST_OWNER);

    // printf_group_list(&g_group_list);
}

static void _sync_all_groups(char* shm, char *cloud_address, char *hubUUID, char *token, int useEtag)
{
    if(!shm || !hubUUID || !token)
    {
        return;
    }

    int ret = 0;
    char address[SIZE_1024B];
    if(!cloud_address)
    {
        snprintf(address, sizeof(address), GROUP_GET_ALL_DOMAIN, CLOUD_DOMAIN, hubUUID);
    }
    else
    {
        snprintf(address, sizeof(address), GROUP_GET_ALL_DOMAIN, cloud_address, hubUUID);
    }

    char *groupAllETag = NULL;
    char headerIn[SIZE_1024B] = {0};
    snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);
    if(useEtag)
    {
        groupAllETag = VR_(read_option)(NULL, GROUP_ETAG_ADDR);
        snprintf(headerIn+strlen(headerIn), sizeof(headerIn)-strlen(headerIn),
                    "\nIf-None-Match: %s", groupAllETag?"12345":"");
    }

    char *header = NULL;
    char *response = NULL;
    ret = VR_(get_request)(address, headerIn, NULL, &header, &response, DEFAULT_TIMEOUT);
    if(ret)
    {
        goto sync_all_done;
    }

    // SLOGI("header = %s\n", header);
    // SLOGI("response = %s\n", response);

    if(!header)
    {
        goto sync_all_done;
    }

    if(strstr(header, "304 Not Modified"))
    {
        SLOGI("ALL GROUP IS NOT CHANGE\n");
        goto sync_all_done;
    }
    
    update_Etag(header);

    if(!response || !strlen(response))
    {
        goto sync_all_done;
    }

    json_object *groupArray = VR_(create_json_object)(response);
    if(!groupArray)
    {
        SLOGI("%s is not json format\n");
        goto sync_all_done;
    }

    enum json_type type = json_object_get_type(groupArray);
    if(json_type_array != type)
    {
        SLOGI("this is not json array format\n");
        json_object_put(groupArray);
        goto sync_all_done;
    }

    init_group_list();
    init_group_from_database();

    int i;
    int arrayLen = json_object_array_length(groupArray);
    for(i=0; i<arrayLen; i++)
    {
        json_object *jvalue = json_object_array_get_idx(groupArray, i);
        update_group(jvalue);
    }

    while(!VR_list_empty(&g_group_list.list))
    {
        _post_dupplicate_name();
        sleep(1);
    }

    json_object_put(groupArray);
sync_all_done:
    SAFE_FREE(header);
    SAFE_FREE(response);
    SAFE_FREE(groupAllETag);
    return;
}

static void sync_all_groups(int useEtag)
{
    char *token = VR_(read_option)(NULL, UCI_CLOUD_TOKEN);
    if(!token)
    {
        SLOGI("Token not found\n");
        return;
    }

    if(!strcmp(token, "default"))
    {
        SLOGI("Token invalid\n");
        free(token);
        return;
    }

    char *hubUUID = VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
    if(!hubUUID)
    {
        SLOGI("hubUUID not found\n");
        free(token);
        return;
    }

    char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    _sync_all_groups(g_shm, cloud_address, hubUUID, token, useEtag);

    SAFE_FREE(cloud_address);
    free(hubUUID);
    free(token);
    return;
}

void sync_all_groups_with_cloud(int useEtag)
{
    sync_all_groups(useEtag);
    
    if(g_retry)
    {
        sync_all_groups(useEtag);
    }
}