#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <json-c/json.h>

#include "database.h"
#include "VR_define.h"
#include "rule_setup.h"
#include "verik_utils.h"
#include "vr_rest.h"
#include "slog.h"
#include "pubsub_handler.h"

extern sqlite3 *rule_db;
extern sqlite3 *dev_db;
extern char *g_shm;
extern char g_mqtt_clientId[256];

static rule_list_t g_rule_list;
pthread_mutex_t g_rule_list_queueMutex;

void handle_rule_message(char *message);

static void init_rule_list()
{
    VR_INIT_LIST_HEAD(&g_rule_list.list);
    pthread_mutex_init(&g_rule_list_queueMutex, 0);
}

static void free_rule_list(rule_list_t *tmp)
{
    if(!tmp)
    {
        return;
    }

    SAFE_FREE(tmp->type);
    SAFE_FREE(tmp->ruleId);
    SAFE_FREE(tmp->name);
    SAFE_FREE(tmp->conditions);
    SAFE_FREE(tmp->actions);
    SAFE_FREE(tmp->enabled);
    SAFE_FREE(tmp);
}

static int add_new_rule_to_list(void *node_add)
{
    int res = 0;
    pthread_mutex_lock(&g_rule_list_queueMutex);
    rule_list_t *input = (rule_list_t *)node_add;
    rule_list_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &(g_rule_list.list), list)
    {
        if(!strcmp(tmp->ruleId, input->ruleId))
        {
            res = 1;
            break;
        }
    }

    if(!res)
    {
        VR_(list_add_tail)(&(input->list), &(g_rule_list.list));
    }

    pthread_mutex_unlock(&g_rule_list_queueMutex);
    return res;
}

static void remove_rule_from_list(char *ruleId)
{
    if(!ruleId)
    {
        return;
    }

    pthread_mutex_lock(&g_rule_list_queueMutex);
    rule_list_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_rule_list.list)
    {
        tmp = VR_(list_entry)(pos, rule_list_t, list);
        if(tmp->ruleId && !strcmp(tmp->ruleId, ruleId))
        {
            VR_(list_del)(pos);
            free_rule_list(tmp);
        }
    }
    pthread_mutex_unlock(&g_rule_list_queueMutex);
}

// void printf_rule_list(rule_list_t *ruleList)
// {
//     int i=0;
//     rule_list_t *tmp = NULL;
    
//     VR_(list_for_each_entry)(tmp, &ruleList->list, list)
//     {
//         printf("#### dev %d ######\n", i);
//         printf("type = %s\n", tmp->type);
//         printf("ruleId = %s\n", tmp->ruleId);
//         printf("name = %s\n", tmp->name);
//         printf("conditions = %s\n", tmp->conditions);
//         printf("actions = %s\n", tmp->actions);
//         printf("enabled = %s\n", tmp->enabled);
//         i++;
//     }
// }

static int create_rule_list_cb(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 6)
    {
        return 0;
    }

    int i;
    char *type = NULL;
    char *ruleId = NULL;
    char *name = NULL;
    char *conditions = NULL;
    char *actions = NULL;
    char *enabled = NULL;

    for(i=0; i<argc; i++)
    {
        if(!strcmp(azColName[i], ST_TYPE))
        {
            type = argv[i];
        }
        else if(!strcmp(azColName[i], ST_ID))
        {
            ruleId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_NAME))
        {
            name = argv[i];
        }
        else if(!strcmp(azColName[i], ST_CONDITIONS))
        {
            conditions = argv[i];
        }
        else if(!strcmp(azColName[i], ST_ACTIONS))
        {
            actions = argv[i];
        }
        else if(!strcmp(azColName[i], ST_STATUS))
        {
            enabled = argv[i];
        }
    }

    if(!type || !ruleId || !name || !conditions || !actions || !enabled)
    {
        SLOGI("missing info\n");
        return 0;
    }

    rule_list_t *new_rule = (rule_list_t*)malloc(sizeof(rule_list_t));
    memset(new_rule, 0, sizeof(rule_list_t));
    new_rule->type = strdup(type);
    new_rule->ruleId = strdup(ruleId);
    new_rule->name = strdup(name);
    new_rule->conditions = strdup(conditions);
    new_rule->actions = strdup(actions);
    new_rule->enabled = strdup(enabled);

    if(add_new_rule_to_list(new_rule))
    {
        free_rule_list(new_rule);
    }
    return 0;
}

void create_rule_list_from_database()
{
    searching_database("pubsub_handler", rule_db, create_rule_list_cb, NULL,
                        "SELECT * from RULES");

    // printf_rule_list(&g_rule_list);
}

static int create_previous_status_cb(void *data, int argc, char **argv, char **azColName)
{
    int i;
    char *deviceId = NULL;
    char *featureId = NULL;
    char *value = NULL;
    for(i=0; i<argc; i++)
    {
        if(!strcmp(azColName[i], ST_DEVICE_ID))
        {
            deviceId = argv[i];
        }
        if(!strcmp(azColName[i], ST_FEATURE_ID))
        {
            featureId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_REGISTER))
        {
            value = argv[i];
        }
    }

    database_actions("pubsub_handler", rule_db, "INSERT INTO PREVIOUS_STATUS (%s,%s,%s) "
                            "VALUES('%s','%s','%s')", 
                            ST_DEVICE_ID, ST_FEATURE_ID, ST_VALUE,
                            deviceId, featureId, value);
    return 0;
}

static int _set_rule_time(char *ruleType, char *ruleId, char *ruleName, 
                            char *condition, char *actions, char *enabled)
{
    if(!ruleType || !ruleId || !condition || 
        !actions || !ruleName || !enabled)
    {
        return RULE_MISSING_INFO;
    }

    if(!database_actions("pubsub_handler", rule_db, 
                            "INSERT INTO RULES (%s,%s,%s,%s,%s,%s,%s) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                            ST_TYPE, ST_ID, ST_NAME, ST_CONDITIONS, ST_ACTIONS, ST_STATUS, ST_RUN,
                            ruleType, ruleId, ruleName, condition, actions, enabled, ST_RUNNING))
    {
        if(!strcmp(enabled, ST_ACTIVE))
        {
            VR_(install_rule)(ruleId, condition, actions);
        }
        return RULE_SUCCESS;
    }
    else
    {
        SLOGE("failed to insert rule into database\n");
        return RULE_DATABASE_FAILED;
    }
}

static int _set_rule_d2d(char *ruleType, char *ruleId, char *ruleName, 
                            char *condition, char *actions, char *enabled)
{
    if(!ruleType || !ruleId || !condition || 
        !actions || !ruleName || !enabled)
    {
        return RULE_MISSING_INFO;
    }

    if(!database_actions("pubsub_handler", rule_db, 
                            "INSERT INTO RULES (%s,%s,%s,%s,%s,%s,%s) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                            ST_TYPE, ST_ID, ST_NAME, ST_CONDITIONS, ST_ACTIONS, ST_STATUS, ST_RUN,
                            ruleType, ruleId, ruleName, condition, actions, enabled, ST_RUNNING))
    {
        char device_id[SIZE_64B];
        char *save_tok;
        char *tok = strtok_r(condition,";", &save_tok);
        if(tok)
        {
            strncpy(device_id, tok, sizeof(device_id)-1);
            database_actions("pubsub_handler", rule_db, "DELETE from PREVIOUS_STATUS where %s='%s'", 
                                    ST_DEVICE_ID, device_id);

            searching_database("pubsub_handler", dev_db, create_previous_status_cb, NULL,
                                "SELECT %s,%s,%s from FEATURES where %s='%s' and %s IN ('%s')",
                                ST_DEVICE_ID, ST_FEATURE_ID, ST_REGISTER, ST_DEVICE_ID, device_id,
                                ST_FEATURE_ID, ST_ON_OFF);  
        }
        return RULE_SUCCESS;
    }
    else
    {
        SLOGE("failed to insert rule into database\n");
        return RULE_DATABASE_FAILED;
    }
}

static int _set_rule_location(char *ruleType, char *ruleId, char *ruleName, 
                            char *condition, char *actions, char *enabled)
{
    if(!ruleType || !ruleId || !condition || 
        !actions || !ruleName || !enabled)
    {
        return RULE_MISSING_INFO;
    }

    if(!database_actions("pubsub_handler", rule_db, 
                            "INSERT INTO RULES (%s,%s,%s,%s,%s,%s,%s) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                            ST_TYPE, ST_ID, ST_NAME, ST_CONDITIONS, ST_ACTIONS, ST_STATUS, ST_RUN,
                            ruleType, ruleId, ruleName, condition, actions, enabled, ST_RUNNING))
    {
        return RULE_SUCCESS;
    }
    else
    {
        SLOGE("failed to insert rule into database\n");
        return RULE_DATABASE_FAILED;
    }
}

static int _set_rule_auto_off(char *ruleType, char *ruleId, char *ruleName, 
                            char *condition, char *actions, char *enabled)
{
    return 0;
#if 0
    if(!ruleType || !ruleId || !condition || 
        !actions || !ruleName || !enabled)
    {
        return RULE_MISSING_INFO;
    }

    if(!database_actions("pubsub_handler", rule_db, 
                            "INSERT INTO RULES (%s,%s,%s,%s,%s,%s,%s) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                            ST_TYPE, ST_ID, ST_NAME, ST_CONDITIONS, ST_ACTIONS, ST_STATUS, ST_RUN,
                            ruleType, ruleId, ruleName, condition, actions, enabled, ST_WAITTING))
    {
        char tmp[256];
        memset(tmp, 0x00, sizeof(tmp));
        char device_type[32], device_id[32];
        strncpy(tmp, actions, 255);

        char *save_tok;
        char *pch = strtok_r(tmp, ";", &save_tok);
        if(pch != NULL)
        {
            strcpy(device_type, pch);
            pch = strtok_r(NULL, ";", &save_tok);
        }
        if(pch != NULL)
        {
            strcpy(device_id, pch);
        }

        SEARCH_DATA_INIT_VAR(result);
        searching_database("pubsub_handler", dev_db, get_last_data_cb, &result,
                            "SELECT %s from FEATURES where %s='%s' AND %s='%s';",
                            ST_REGISTER, ST_DEVICE_ID, device_id, ST_FEATURE_ID, ST_ON_OFF);
        if(result.len && atoi(result.value)>0)
        {
            char crontabs_timer[64];
            time_t epoch_time = time(NULL);
            struct tm *tm_struct = localtime(&epoch_time);
            int hour = tm_struct->tm_hour;
            int minute = tm_struct->tm_min;

            strncpy(tmp, condition, 255);
            char hour_in[32], min_in[32];
            char *pch = strtok_r(tmp, ":", &save_tok);
            if(pch != NULL)
            {
                strcpy(hour_in, pch);
                pch = strtok_r(NULL, ":", &save_tok);
            }
            if(pch != NULL)
            {
                strcpy(min_in, pch);
            }

            int min_set = minute + atoi(min_in);

            if(min_set >= 60)
            {
                min_set = min_set - 60;
                hour = hour + 1;
            }

            int hour_set = hour + atoi(hour_in);
            if(hour_set >=24)
            {
                hour_set = hour_set - 24;
            }

            sprintf(crontabs_timer, "%d:%d;%s", hour_set, min_set, ST_AUTO_OFF);
            VR_(install_rule)(ruleId, crontabs_timer, actions);
            database_actions("pubsub_handler", rule_db, "UPDATE RULES set %s='%d' where %s='%s'", 
                                ST_RUN, (unsigned)time(NULL), ST_ID, ruleId);
        }
        FREE_SEARCH_DATA_VAR(result);
        return RULE_SUCCESS;
    }
    else
    {
        SLOGE("failed to insert rule into database\n");
        return RULE_DATABASE_FAILED;
    }
#endif
}

static int _set_rule_custom(char *ruleType, char *ruleId, char *ruleName, 
                            char *condition, char *actions, char *enabled)
{
    return 0;
#if 0 
    if(!ruleType || !ruleId || !condition || 
        !actions || !ruleName || !enabled)
    {
        return RULE_MISSING_INFO;
    }

    if(!database_actions("pubsub_handler", rule_db, 
                            "INSERT INTO RULES (%s,%s,%s,%s,%s,%s,%s) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                            ST_TYPE, ST_ID, ST_NAME, ST_CONDITIONS, ST_ACTIONS, ST_STATUS, ST_RUN,
                            ruleType, ruleId, ruleName, condition, actions, enabled, ST_RUNNING))
    {
        char tmp[256];
        memset(tmp, 0x00, sizeof(tmp));
        strncpy(tmp, condition, 255);

        char crontabs_timer[64];
        time_t epoch_time = time(NULL);
        struct tm *tm_struct = localtime(&epoch_time);
        int hour = tm_struct->tm_hour;
        int minute = tm_struct->tm_min;

        char hour_in[32], min_in[32];
        char *save_tok;
        char *pch = strtok_r(tmp, ":", &save_tok);
        if(pch != NULL)
        {
            strcpy(hour_in, pch);
            pch = strtok_r(NULL, ":", &save_tok);
        }
        if(pch != NULL)
        {
            strcpy(min_in, pch);
        }

        int min_set = minute + atoi(min_in);

        if(min_set >= 60)
        {
            min_set = min_set - 60;
            hour = hour + 1;
        }

        int hour_set = hour + atoi(hour_in);
        if(hour_set >=24)
        {
            hour_set = hour_set - 24;
        }

        sprintf(crontabs_timer, "%d:%d;%s", hour_set, min_set, ST_NO_LOOP);
        VR_(install_rule)(ruleId, crontabs_timer, actions);
        database_actions("pubsub_handler", rule_db, "UPDATE RULES set %s='%d' where id='%s'", 
                                ST_RUN, (unsigned)time(NULL), ST_ID, ruleId);

        return RULE_SUCCESS;
    }
    else
    {
        SLOGE("failed to insert rule into database\n");
        return RULE_DATABASE_FAILED;
    }
#endif
}

static int _set_rule_manual(char *ruleType, char *ruleId, char *ruleName, 
                            char *condition, char *actions, char *enabled)
{
    if(!ruleType || !ruleId || !condition || 
        !actions || !ruleName || !enabled)
    {
        return RULE_MISSING_INFO;
    }

    if(!database_actions("pubsub_handler", rule_db, 
                        "INSERT INTO RULES (%s,%s,%s,%s,%s,%s,%s) VALUES('%s','%s','%s','%s','%s','%s','%s')",
                        ST_TYPE, ST_ID, ST_NAME, ST_CONDITIONS, ST_ACTIONS, ST_STATUS, ST_RUN,
                        ruleType, ruleId, ruleName, condition, actions, enabled, ST_RUNNING))
    {
        return RULE_SUCCESS;
    }
    else
    {
        SLOGE("failed to insert rule into database\n");
        return RULE_DATABASE_FAILED;
    }
}

int set_rule(char *ruleType, char*ruleId, char *ruleName, char *condition, char *actions, char *enabled)
{
    if(!ruleType || !ruleId || !condition || 
        !actions || !ruleName || !enabled)
    {
        SLOGE("missing info\n");
        return RULE_MISSING_INFO;
    }
    SLOGI("set rule type: %s, %s, %s, %s, %s, %s\n", ruleType, ruleId, ruleName,
                                    condition, actions, enabled);

    if(strlen(ruleName) > SIZE_512B)
    {
        SLOGE("rule name too long\n");
        return RULE_NAME_TOO_LONG;
    }

    int res = RULE_SUCCESS;

    SEARCH_DATA_INIT_VAR(name);
    searching_database("pubsub_handler", rule_db, get_last_data_cb, &name, 
                        "SELECT %s from RULES where %s='%s'", ST_NAME, ST_ID,
                        ruleId);
    if(name.len)
    {
        SLOGE("rule exist\n");
        res = database_actions("pubsub_handler", rule_db, "DELETE from RULES where %s='%s'", ST_ID, ruleId);
        if(res)
        {
            FREE_SEARCH_DATA_VAR(name);
            return RULE_EXIST;
        }
    }
    FREE_SEARCH_DATA_VAR(name);

    if(!strcmp(ruleType, ST_TIME))
    {
        res = _set_rule_time(ruleType, ruleId, ruleName, condition, actions, enabled);
    }
    else if(!strcmp(ruleType, ST_DEV_TO_DEV))
    {
        res = _set_rule_d2d(ruleType, ruleId, ruleName, condition, actions, enabled);
    }
    else if(!strcmp(ruleType, ST_LOCATION))
    {
        res = _set_rule_location(ruleType, ruleId, ruleName, condition, actions, enabled);
    }
    else if(!strcmp(ruleType, ST_AUTO_OFF))
    {
        res = _set_rule_auto_off(ruleType, ruleId, ruleName, condition, actions, enabled);
    }
    else if(!strcmp(ruleType, ST_CUSTOM))
    {
        res = _set_rule_custom(ruleType, ruleId, ruleName, condition, actions, enabled);
    }
    else if(!strcmp(ruleType, ST_MANUAL))
    {
        res = _set_rule_manual(ruleType, ruleId, ruleName, condition, actions, enabled);
    }

    return res;
}

static int _rule_actions_delete(char *ruleType, char*ruleId)
{
    if(!ruleType || !ruleId)
    {
        SLOGI("mising info\n");
        return RULE_MISSING_INFO;
    }
    SLOGI("ruleId = %s\n", ruleId);
    SLOGI("ruleType = %s\n", ruleType);
    
    if(!strcmp(ruleType, ST_TIME) || !strcmp(ruleType, ST_AUTO_OFF) || !strcmp(ruleType, ST_CUSTOM))
    {
        char command[SIZE_256B];
        snprintf(command, sizeof(command), "%s%s%s", 
                        "sed -e \'/\"", ruleId, "\"/d\' -i /etc/crontabs/root");
        VR_(execute_system)(command);
    }

    if(!strcmp(ruleType, ST_DEV_TO_DEV))
    {
        SEARCH_DATA_INIT_VAR(current_condition);
        searching_database("pubsub_handler", rule_db, get_last_data_cb, &current_condition,
                             "SELECT %s from RULES where %s='%s'", ST_CONDITIONS, ST_ID, ruleId);
        if(current_condition.len)
        {
            char *tok = strtok(current_condition.value, ";");
            if(tok)
            {
                char *deviceId = tok;
                database_actions("pubsub_handler", rule_db, 
                                "DELETE from PREVIOUS_STATUS where %s='%s'", ST_DEVICE_ID, deviceId);
            }
        }
        FREE_SEARCH_DATA_VAR(current_condition);
    }

    int res = RULE_SUCCESS;
    
    res = database_actions("pubsub_handler", rule_db, "DELETE from RULES where %s='%s'", ST_ID, ruleId);

    return res;
}

static int _rule_actions_active(char *ruleType, char*ruleId)
{
    if(!ruleType || !ruleId)
    {
        return RULE_MISSING_INFO;
    }

    if(!strcmp(ruleType, ST_TIME) || !strcmp(ruleType, ST_AUTO_OFF) || !strcmp(ruleType, ST_CUSTOM))
    {
        char command[SIZE_256B];
        snprintf(command, sizeof(command), "%s%s%s", "sed -e \'/\"", ruleId, "\"/ s/#//\' -i /etc/crontabs/root");
        VR_(execute_system)(command);
    }
    
    SEARCH_DATA_INIT_VAR(status);
    searching_database("pubsub_handler", rule_db, get_last_data_cb, &status, 
                        "SELECT %s from RULES where %s='%s'", ST_STATUS, ST_ID, ruleId);
    if(status.len && !strcmp(status.value, ST_INACTIVE))
    {
        database_actions("pubsub_handler", rule_db, "UPDATE RULES set %s='%s' where %s='%s'", 
                        ST_STATUS, ST_ACTIVE, ST_ID, ruleId);
    }
    FREE_SEARCH_DATA_VAR(status);

    return RULE_SUCCESS;
}

static int _rule_actions_deactive(char *ruleType, char *ruleId)
{
    if(!ruleType || !ruleId)
    {
        return RULE_MISSING_INFO;
    }

    if(!strcmp(ruleType,ST_TIME) || !strcmp(ruleType,ST_AUTO_OFF) || !strcmp(ruleType,ST_CUSTOM))
    {
        char command[SIZE_256B];
        snprintf(command, sizeof(command), "%s%s%s", "sed -e \'/\"", ruleId, "\"/ s/#//\' -i /etc/crontabs/root");
        VR_(execute_system)(command);
    }

    SEARCH_DATA_INIT_VAR(status);
    searching_database("pubsub_handler", rule_db, get_last_data_cb, 
                        &status, "SELECT %s from RULES where %s='%s'", 
                        ST_STATUS, ST_ID, ruleId);

    if(status.len && !strcmp(status.value, ST_ACTIVE))
    {
        database_actions("pubsub_handler", rule_db, "UPDATE RULES set %s='%s' where %s='%s'", 
                            ST_STATUS, ST_INACTIVE, ST_ID, ruleId);
    }

    FREE_SEARCH_DATA_VAR(status);
    return RULE_SUCCESS;
}

static int _rule_actions_update(char *ruleType, char*ruleId, char *ruleNewName, 
                                char *newCondition, char *newActions, char *enabled)
{
    SLOGI("ruleNewName = %s\n", ruleNewName);
    SLOGI("newCondition = %s\n", newCondition);
    SLOGI("newActions = %s\n", newActions);

    if(!ruleNewName || !newCondition || !newActions || !enabled || !ruleType)
    {
        return RULE_MISSING_INFO;
    }

    int res = RULE_SUCCESS;
    SEARCH_DATA_INIT_VAR(current_condition);
    searching_database("pubsub_handler", rule_db, get_last_data_cb, &current_condition, 
                                    "SELECT %s from RULES where %s='%s'",
                                    ST_CONDITIONS, ST_ID, ruleId);
    if(current_condition.len)
    {
        /*remove curent status of d2d rule*/
        if(!strcmp(ruleType, ST_DEV_TO_DEV))
        {
            size_t mallocLength = strlen(newCondition)+1;
            char *tmp_new_conditions = (char*)(malloc(mallocLength));
            strlcpy(tmp_new_conditions, newCondition, mallocLength);

            /*delete current status*/
            char *tok = strtok(current_condition.value, ";");
            if(tok)
            {
                char *deviceId = tok;
                database_actions("pubsub_handler", rule_db, 
                                "DELETE from PREVIOUS_STATUS where %s='%s'", 
                                ST_DEVICE_ID, deviceId);
            }

            /*insert new status*/
            tok = strtok(tmp_new_conditions, ";");
            if(tok)
            {
                char *deviceId = tok;
                database_actions("pubsub_handler", rule_db, 
                                "DELETE from PREVIOUS_STATUS where %s='%s'",
                                ST_DEVICE_ID, deviceId);

                searching_database("pubsub_handler", dev_db, create_previous_status_cb, NULL,
                                "SELECT %s,%s,%s from FEATURES where %s='%s' and %s IN ('%s')",
                                ST_DEVICE_ID, ST_FEATURE_ID, ST_REGISTER, ST_DEVICE_ID,
                                deviceId, ST_FEATURE_ID, ST_ON_OFF);
            }

            free(tmp_new_conditions);
        }

        res = database_actions("pubsub_handler", rule_db, 
                        "UPDATE RULES set %s='%s',%s='%s', %s='%s', %s='%s' where %s='%s'",
                        ST_NAME, ruleNewName, ST_CONDITIONS, newCondition, ST_ACTIONS, newActions,
                        ST_STATUS, enabled, ST_ID, ruleId);
        if(res)
        {
            res = RULE_DATABASE_FAILED;
        }
        else
        {
            if(!strcmp(ruleType, ST_TIME) || !strcmp(ruleType, ST_AUTO_OFF))
            {
                char command[SIZE_256B];
                snprintf(command, sizeof(command), "%s%s%s", "sed -e \'/\"", ruleId, "\"/d\' -i /etc/crontabs/root");
                VR_(execute_system)(command);

                if(!strcmp(enabled, ST_ACTIVE))
                {
                    VR_(install_rule)(ruleId, newCondition, newActions);
                }
            }
        }
    }
    else /*rule does not exist*/
    {
        SLOGI("rule does not exist\n");
        if(!strcmp(ruleType, ST_TIME))
        {
            res = _set_rule_time(ruleType, ruleId, ruleNewName, newCondition, newActions, enabled);
        }
        else if(!strcmp(ruleType, ST_DEV_TO_DEV))
        {
            res = _set_rule_d2d(ruleType, ruleId, ruleNewName, newCondition, newActions, enabled);
        }
        else if(!strcmp(ruleType, ST_LOCATION))
        {
            res = _set_rule_location(ruleType, ruleId, ruleNewName, newCondition, newActions, enabled);
        }
        else if(!strcmp(ruleType, ST_AUTO_OFF))
        {
            res = _set_rule_auto_off(ruleType, ruleId, ruleNewName, newCondition, newActions, enabled);
        }
        else if(!strcmp(ruleType, ST_CUSTOM))
        {
            res = _set_rule_custom(ruleType, ruleId, ruleNewName, newCondition, newActions, enabled);
        }
        else if(!strcmp(ruleType, ST_MANUAL))
        {
            res = _set_rule_manual(ruleType, ruleId, ruleNewName, newCondition, newActions, enabled);
        }
    }

    FREE_SEARCH_DATA_VAR(current_condition);
    return res;

}

int rule_actions(char *ruleAction, char *ruleType, char*ruleId, 
                 char *ruleName, char *condition, char *actions,
                 char *enabled)
{
    if(!ruleAction || !ruleId)
    {
        SLOGE("missing info\n");
        return RULE_MISSING_INFO;
    }

    int res = RULE_SUCCESS;
    SLOGI("ruleAction = %s\n", ruleAction);
    if(!strcmp(ruleAction,ST_DELETE))
    {
        res = _rule_actions_delete(ruleType, ruleId);
    }
    else if(!strcmp(ruleAction,ST_ACTIVATE))
    {
        res = _rule_actions_active(ruleType, ruleId);
    }
    else if(!strcmp(ruleAction,ST_DEACTIVATE))
    {
        res = _rule_actions_deactive(ruleType, ruleId);
    }
    else if(!strcmp(ruleAction, ST_UPDATE))
    {
        res = _rule_actions_update(ruleType, ruleId, ruleName, condition, actions, enabled);
    }
    return res;
}

int VR_(rule_accept)(char *ruleId, int64_t timeStamp, json_bool isAccept)
{
    if(!ruleId)
    {
        SLOGE("missing RULE ID or TIMESTAMP \n");
        return -1;
    }

    int res = -1;
    char address[SIZE_1024B], headerIn[SIZE_1024B]={0};
    // int64_t timestamp = strtoull(timeStamp, NULL, 10);

    json_object *ruleAccept = json_object_new_object();
    json_object_object_add(ruleAccept, ST_CLOUD_ACCEPTED, json_object_new_boolean(isAccept));
    json_object_object_add(ruleAccept, ST_CLOUD_TIME_STAMP, json_object_new_int64(timeStamp));

    char *token = VR_(read_option)(NULL, UCI_CLOUD_TOKEN);
    if(!token)
    {
        SLOGI("Token not found\n");
        return -1;
    }

    if(!strcmp(token, "default"))
    {
        SLOGI("Token invalid\n");
        free(token);
        return -1;
    }

    snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);
    char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    if(!cloud_address)
    {
        snprintf(address, sizeof(address), RULE_ACCEPT_DOMAIN, CLOUD_DOMAIN, ruleId);
    }
    else
    {
        snprintf(address, sizeof(address), RULE_ACCEPT_DOMAIN, cloud_address, ruleId);
        free(cloud_address);
    }
    // char *result = NULL;
    // res = VR_(post_request)(address, NULL, (char*)json_object_to_json_string(ruleAccept), NULL, &result);
    adding_curl_request_list(HTTP_POST_REQUEST, address, headerIn, (char*)json_object_to_json_string(ruleAccept), NULL);

    // SLOGI("######## RESPONSE = %s\n", result);
    json_object_put(ruleAccept);
    // SAFE_FREE(result);
    SAFE_FREE(token);
    return res;
}

void sync_unaccepted_rule(char *cloud_address, char *hubUUID, char *token)
{
    if(!hubUUID || !token)
    {
        return;
    }

    int ret = 0;
    char address[SIZE_1024B], headerIn[SIZE_1024B];
    snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);
    if(!cloud_address)
    {
        snprintf(address, sizeof(address), RULE_GET_UNACCEPT_DOMAIN, CLOUD_DOMAIN, hubUUID);
    }
    else
    {
        snprintf(address, sizeof(address), RULE_GET_UNACCEPT_DOMAIN, cloud_address, hubUUID);
    }

    char *result = NULL;
    ret = VR_(get_request)(address, headerIn, NULL, NULL, &result, DEFAULT_TIMEOUT);
    if(ret)
    {
        SAFE_FREE(result);
        return;
    }

    SLOGI("######## RESPONSE = %s\n", result);
    json_object *ruleArray = VR_(create_json_object)(result);
    if(!ruleArray)
    {
        SAFE_FREE(result);
        return;
    }

    enum json_type type = json_object_get_type(ruleArray);
    if(json_type_array != type)
    {
        json_object_put(ruleArray);
        SAFE_FREE(result);
        return;
    }

    int i;
    int arrayLen = json_object_array_length(ruleArray);
    for(i=0; i<arrayLen; i++)
    {
        json_object *jvalue = json_object_array_get_idx(ruleArray, i);
        handle_rule_message((char*)json_object_to_json_string(jvalue));
    }

    json_object_put(ruleArray);
    SAFE_FREE(result);
    return;
}

#define ETAG_PARTERN "ETag: "

static void update_Etag(char *header)
{
    if(!header)
    {
        return;
    }

    char *ch = strstr(header, ETAG_PARTERN);
    if(ch)
    {
        char *endLine = strstr(ch, "\r\n");
        if(!endLine)
        {
            return;
        }

        size_t ETagNum = endLine - ch -strlen(ETAG_PARTERN);
        SLOGI("ETagNum = %d\n", ETagNum);
        char newETag[SIZE_256B];
        if(ETagNum > (sizeof(newETag)-1))
        {
            return;
        }

        strncpy(newETag, ch+strlen(ETAG_PARTERN), ETagNum);
        newETag[ETagNum] = '\0';
        SLOGI("newETag = %s\n", newETag);

        VR_(write_option)(NULL, UCI_RULE_ETAG"=%s", newETag);
    }
}

static int check_rule_is_up_to_date(rule_list_t *rule, char *ruleName, char *conditions, 
                                    char *actions, char *enabled)
{
    if(!rule)
    {
        return 1;
    }

    if(rule->name && strcmp(rule->name, ruleName))
    {
        return 1;
    }

    if(rule->conditions && strcmp(rule->conditions, conditions))
    {
        return 1;
    }

    if(rule->actions && strcmp(rule->actions, actions))
    {
        return 1;
    }

    if(rule->enabled && strcmp(rule->enabled, enabled))
    {
        return 1;
    }

    return 0;
}

static void _remove_wrong_rules()
{
    pthread_mutex_lock(&g_rule_list_queueMutex);
    rule_list_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_rule_list.list)
    {
        tmp = VR_(list_entry)(pos, rule_list_t, list);
        VR_(list_del)(pos);

        database_actions("pubsub_handler", rule_db, 
                        "DELETE from RULES where %s='%s'", ST_ID, tmp->ruleId);
        free_rule_list(tmp);
    }
    pthread_mutex_unlock(&g_rule_list_queueMutex);
}

static void _sync_all_rule(char *message)
{
    if(!message)
    {
        return;
    }

    char *state = NULL;
    char *ruleId = NULL;
    char *ruleName = NULL;
    char *ruleType = NULL;
    char *condition = NULL;
    char *actions = NULL;
    char *enabled = NULL;
    int64_t timeStamp = 0;
    int res = VR_(get_rule_infor)(g_shm, message, &state, &ruleId, &ruleName, 
                        &ruleType, &condition, &actions, &timeStamp, &enabled);

    int found = 0;
    rule_list_t *tmp = NULL;

    if(!state)
    {
        goto done;
    }

    if(res)
    {
        goto done;
    }

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_rule_list.list)
    {
        tmp = VR_(list_entry)(pos, rule_list_t, list);
        if(tmp->ruleId && !strcmp(tmp->ruleId, ruleId))
        {
            found = 1;
            break;
        }
    }

    if(!found) //not found, insert to new rule
    {
        set_rule(ruleType, ruleId, ruleName, condition, actions, enabled);
    }
    else
    {
        if(!strcmp(state, ST_DELETE))/*if delete, just delete it*/
        {
            rule_actions(state, ruleType, ruleId, ruleName, condition, actions, enabled);
        }
        else /*check udpate*/
        {
            res = check_rule_is_up_to_date(tmp, ruleName, condition, actions, enabled);
            if(res)
            {
                if(!strcmp(state, ST_CREATE))
                {
                    rule_actions(ST_UPDATE, ruleType, ruleId, ruleName, condition, actions, enabled);
                }
                else /*may be active/deactived*/
                {
                    rule_actions(state, ruleType, ruleId, ruleName, condition, actions, enabled);
                }
            }
            else
            {
                SLOGI("rule is up to date\n");
            }
        }

        remove_rule_from_list(ruleId);
    }
done:
    SAFE_FREE(state);
    SAFE_FREE(ruleId);
    SAFE_FREE(ruleName);
    SAFE_FREE(ruleType);
    SAFE_FREE(condition);
    SAFE_FREE(actions);
    SAFE_FREE(enabled);
}

void sync_all_rule(char *cloud_address, char *hubUUID, char *token, int useEtag)
{
    if(!hubUUID || !token)
    {
        return;
    }

    int ret = 0;
    char address[SIZE_1024B];
    if(!cloud_address)
    {
        snprintf(address, sizeof(address), RULE_GET_ALL_DOMAIN, CLOUD_DOMAIN, hubUUID);
    }
    else
    {
        snprintf(address, sizeof(address), RULE_GET_ALL_DOMAIN, cloud_address, hubUUID);
    }

    char *ruleAllETag = NULL;
    char headerIn[SIZE_1024B]={0};
    snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);

    if(useEtag)
    {
        ruleAllETag = VR_(read_option)(NULL, UCI_RULE_ETAG);
        snprintf(headerIn+strlen(headerIn), sizeof(headerIn)-strlen(headerIn),
                    "\nIf-None-Match: %s", ruleAllETag?ruleAllETag:"");
    }

    char *header = NULL;
    char *response = NULL;
    ret = VR_(get_request)(address, headerIn, NULL, &header, &response, DEFAULT_TIMEOUT);
    if(ret)
    {
        goto sync_all_done;
    }

    if(!header)
    {
        goto sync_all_done;
    }

    if(strstr(header, "304 Not Modified"))
    {
        SLOGI("ALL RULE IS NOT CHANGE\n");
        goto sync_all_done;
    }

    //update ETag
    update_Etag(header);

    if(!response || !strlen(response))
    {
        goto sync_all_done;
    }

    json_object *ruleArray = VR_(create_json_object)(response);
    if(!ruleArray)
    {
        SLOGI("this is not json format\n");
        goto sync_all_done;
    }

    enum json_type type = json_object_get_type(ruleArray);
    if(json_type_array != type)
    {
        SLOGI("this is not json array format\n");
        json_object_put(ruleArray);
        goto sync_all_done;
    }

    int i;
    init_rule_list();
    create_rule_list_from_database();

    int arrayLen = json_object_array_length(ruleArray);
    for(i=0; i<arrayLen; i++)
    {
        json_object *jvalue = json_object_array_get_idx(ruleArray, i);
        _sync_all_rule((char *)json_object_to_json_string(jvalue));
    }
    /*delete database with current value in rule list*/
    _remove_wrong_rules();

    json_object_put(ruleArray);
sync_all_done:
    SAFE_FREE(header);
    SAFE_FREE(response);
    SAFE_FREE(ruleAllETag);
    return;
}

void sync_rule_from_cloud(int useEtag)
{
    char *token = VR_(read_option)(NULL, UCI_CLOUD_TOKEN);
    if(!token)
    {
        SLOGI("Token not found\n");
        return;
    }

    if(!strcmp(token, "default"))
    {
        SLOGI("Token invalid\n");
        free(token);
        return;
    }

    char *hubUUID = VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
    if(!hubUUID)
    {
        SLOGI("hubUUID not found\n");
        free(token);
        return;
    }

    char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    sync_unaccepted_rule(cloud_address, hubUUID, token);
    sync_all_rule(cloud_address, hubUUID, token, useEtag);

    SAFE_FREE(cloud_address);
    free(hubUUID);
    free(token);
    return;
}

void VR_(rule_be_triggered)(char *ruleId, char *data)
{
    if(!data || !ruleId)
    {
        SLOGE("data or ruleId is null\n");
        return;
    }

    json_object *ruleDocObj = VR_(create_json_object)(data);
    if(!ruleDocObj)
    {
        SLOGE("data is not json format\n");
        return;
    }

    CHECK_JSON_OBJECT_EXIST(triggerObj, ruleDocObj, ST_TRIGGER, triggerDone);

    json_bool trigger = json_object_get_boolean(triggerObj);
    if(!trigger)
    {
        goto triggerDone;
    }
    
    SEARCH_DATA_INIT_VAR(dbActions);
    searching_database("pubsub_handler", rule_db, get_last_data_cb, 
                    &dbActions, "SELECT actions from RULES where id='%s'", ruleId);

    if(dbActions.len)
    {
        rule_report_timeline(ruleId);
        _trigger_actions_(ruleId, dbActions.value, EXECUTE_MODE);
    }
    FREE_SEARCH_DATA_VAR(dbActions);

triggerDone:
    json_object_put(ruleDocObj);
}

void VR_(scene_trigger)(char *deviceId, char *buttonId)
{
    if(!buttonId || !deviceId)
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(sceneId);
    searching_database("pubsub_handler", dev_db, get_last_data_cb, &sceneId, 
                        "SELECT %s from SCENES where %s='%s' AND %s='%s'",
                        ST_SCENE_ID, ST_DEVICE_ID, deviceId, ST_BUTTON_ID,
                        buttonId);

    if(sceneId.len)
    {
        SEARCH_DATA_INIT_VAR(dbActions);
        searching_database("pubsub_handler", rule_db, get_last_data_cb, 
                        &dbActions, "SELECT actions from RULES where id='%s'", sceneId.value);
        if(dbActions.len)
        {
            rule_report_timeline(sceneId.value);
            _trigger_actions_(sceneId.value, dbActions.value, EXECUTE_MODE);
        }
        FREE_SEARCH_DATA_VAR(dbActions);
    }
    FREE_SEARCH_DATA_VAR(sceneId);
}