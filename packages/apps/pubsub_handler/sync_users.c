#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "vr_rest.h"
#include "database.h"
#include "verik_utils.h"
#include "slog.h"
#include "sync_users.h"

extern sqlite3 *user_db;
static user_list_t g_user_list;
pthread_mutex_t g_user_list_queueMutex;

static void init_user_list()
{
    VR_INIT_LIST_HEAD(&g_user_list.list);
    pthread_mutex_init(&g_user_list_queueMutex, 0);
}

static void free_user_list(user_list_t *tmp)
{
    if(!tmp)
    {
        return;
    }

    SAFE_FREE(tmp->userId);
    SAFE_FREE(tmp->shareId);
    SAFE_FREE(tmp->name);
    SAFE_FREE(tmp->enable);
    SAFE_FREE(tmp->userType);
    SAFE_FREE(tmp->shareTime);
    SAFE_FREE(tmp);
}

static int add_new_user_to_list(void *node_add)
{
    int res = 0;
    pthread_mutex_lock(&g_user_list_queueMutex);
    user_list_t *input = (user_list_t *)node_add;
    user_list_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &(g_user_list.list), list)
    {
        if(!strcmp(tmp->userId, input->userId))
        {
            res = 1;
            break;
        }
    }

    if(!res)
    {
        VR_(list_add_tail)(&(input->list), &(g_user_list.list));
    }

    pthread_mutex_unlock(&g_user_list_queueMutex);
    return res;
}

static int create_user_list_cb(void *data, int argc, char **argv, char **azColName)
{
    int i;
    char *userId = NULL;
    char *shareId = NULL;
    char *name = NULL;
    char *enable = NULL;
    char *userType = NULL;
    char *shareTime = NULL;

    for(i=0; i<argc; i++)
    {
        if(!strcmp(azColName[i], ST_USER_ID))
        {
            userId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_SHARE_ID))
        {
            shareId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_NAME))
        {
            name = argv[i];
        }
        else if(!strcmp(azColName[i], ST_ENABLE))
        {
            enable = argv[i];
        }
        else if(!strcmp(azColName[i], ST_USER_TYPE))
        {
            userType = argv[i];
        }
        else if(!strcmp(azColName[i], ST_SHARE_TIME))
        {
            shareTime = argv[i];
        }
    }

    if(!userId || !shareId || !name || !enable || !userType)
    {
        SLOGI("missing info\n");
        return 0;
    }

    user_list_t *new_user = (user_list_t*)malloc(sizeof(user_list_t));
    memset(new_user, 0, sizeof(user_list_t));
    new_user->userId = strdup(userId);
    new_user->shareId = strdup(shareId);
    new_user->name = strdup(name);
    new_user->enable = strdup(enable);
    new_user->userType = strdup(userType);

    if(shareTime)
    {
        new_user->shareTime = strdup(shareTime);
    }

    add_new_user_to_list(new_user);
    return 0;
}

void printf_user_list(user_list_t *userList)
{
    int i=0;
    user_list_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &userList->list, list)
    {
        printf("#### dev %d ######\n", i);
        printf("userId = %s\n", tmp->userId);
        printf("shareId = %s\n", tmp->shareId);
        printf("name = %s\n", tmp->name);
        printf("enable = %s\n", tmp->enable);
        printf("userType = %s\n", tmp->userType);
        printf("shareTime = %s\n", tmp->shareTime);
        i++;
    }
}

static void create_user_list_from_database()
{
    init_user_list();
    searching_database("pubsub_handler", user_db, create_user_list_cb, NULL,
                        "SELECT * from USERS");

    // printf_user_list(&g_user_list);
}

static void remove_user_from_list(char *userId)
{
    if(!userId)
    {
        return;
    }

    pthread_mutex_lock(&g_user_list_queueMutex);
    user_list_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_user_list.list)
    {
        tmp = VR_(list_entry)(pos, user_list_t, list);
        if(tmp->userId && !strcmp(tmp->userId, userId))
        {
            VR_(list_del)(pos);
            free_user_list(tmp);
        }
    }
    pthread_mutex_unlock(&g_user_list_queueMutex);
}

/*return 1 if has user was removed*/
static int _remove_wrong_users()
{
    int ret = 0;
    pthread_mutex_lock(&g_user_list_queueMutex);
    user_list_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_user_list.list)
    {
        tmp = VR_(list_entry)(pos, user_list_t, list);
        VR_(list_del)(pos);

        database_actions("pubsub_handler", user_db, 
                        "DELETE from USERS where %s='%s'", ST_USER_ID, tmp->userId);
        free_user_list(tmp);
        ret = 1;
    }
    pthread_mutex_unlock(&g_user_list_queueMutex);
    return ret;
}

/*return 1 if has new user was shared*/
static int _sync_all_user(json_object *fromCloud)
{
    int ret = 0;

    if(!fromCloud)
    {
        return ret;
    }

    CHECK_JSON_OBJECT_EXIST(userIdObj, fromCloud, ST_USER_ID, sync_done);
    const char *userId = json_object_get_string(userIdObj);

    int found = 0;
    struct VR_list_head *pos, *q;
    user_list_t *tmp = NULL;

    VR_(list_for_each_safe)(pos, q, &g_user_list.list)
    {
        tmp = VR_(list_entry)(pos, user_list_t, list);
        if(!strcmp(tmp->userId, userId))
        {
            found = 1;
            break;
        }
    }

    if(found)
    {
        /*maybe need update shareTime here*/
        remove_user_from_list((char *)userId);
    }
    else
    {
        const char *name = ST_UNKNOWN;
        json_object *nameObj = NULL;
        if(json_object_object_get_ex(fromCloud, ST_NAME, &nameObj))
        {
            name = json_object_get_string(nameObj);
        }

        CHECK_JSON_OBJECT_EXIST(shareIdObj, fromCloud, ST_ID, sync_done);
        CHECK_JSON_OBJECT_EXIST(userTypeObj, fromCloud, ST_ROLE, sync_done);
        const char *shareId = json_object_get_string(shareIdObj);
        const char *userType = json_object_get_string(userTypeObj);

        database_actions("pubsub_handler", user_db, 
                        "INSERT INTO USERS (userId,shareId,name,enable,userType) "\
                        "VALUES ('%s','%s','%s','%s','%s')", userId, shareId, name, ST_ENABLE, userType);
        ret = 1;
    }

sync_done:
    return ret;
}

static void sync_all_users(char *cloud_address, char *hubUUID, char *token)
{
    if(!hubUUID || !token)
    {
        return;
    }

    int ret = 0;
    char address[SIZE_1024B];
    if(!cloud_address)
    {
        snprintf(address, sizeof(address), USERS_GET_ALL_DOMAIN, CLOUD_DOMAIN, hubUUID);
    }
    else
    {
        snprintf(address, sizeof(address), USERS_GET_ALL_DOMAIN, cloud_address, hubUUID);
    }

    char headerIn[SIZE_1024B];
    snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);
    // char *ruleAllETag = VR_(read_option)(NULL, UCI_RULE_ETAG);

    // printf("ruleAllETag = %s\n", ruleAllETag);
    // snprintf(headerIn, sizeof(headerIn), "If-None-Match: %s", ruleAllETag?ruleAllETag:"");

    char *header = NULL;
    char *response = NULL;
    ret = VR_(get_request)(address, headerIn, NULL, &header, &response, DEFAULT_TIMEOUT);
    if(ret)
    {
        goto sync_all_done;
    }

    if(!header)
    {
        goto sync_all_done;
    }

    // if(strstr(header, "304 Not Modified"))
    // {
    //     SLOGI("ALL RULE IS NOT CHANGE\n");
    //     goto sync_all_done;
    // }

    //update ETag
    // update_Etag(header);

    if(!response || !strlen(response))
    {
        goto sync_all_done;
    }

    SLOGI("response = %s\n", response);

    json_object *userArray = VR_(create_json_object)(response);
    if(!userArray)
    {
        SLOGI("this is not json format\n");
        goto sync_all_done;
    }

    enum json_type type = json_object_get_type(userArray);
    if(json_type_array != type)
    {
        SLOGI("this is not json array format\n");
        json_object_put(userArray);
        goto sync_all_done;
    }

    int i, access_change = 0, psk_change = 0;
    create_user_list_from_database();

    int arrayLen = json_object_array_length(userArray);
    for(i=0; i<arrayLen; i++)
    {
        json_object *jvalue = json_object_array_get_idx(userArray, i);
        access_change |= _sync_all_user(jvalue);
    }

    psk_change |= _remove_wrong_users();

    if(psk_change)
    {
        json_object *notify_jobj = json_object_new_object();
        json_object_object_add(notify_jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
        json_object_object_add(notify_jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_PSK_CHANGED));
        sendMsgtoLocalService((char*)ST_SEND_NOTIFY, (char *)json_object_to_json_string(notify_jobj));
        json_object_put(notify_jobj);
    }
    else if(access_change)
    {
        json_object *publish_jobj = json_object_new_object();
        json_object_object_add(publish_jobj, ST_METHOD, json_object_new_string(ST_PUBLISH));
        json_object_object_add(publish_jobj, ST_MESSAGE, json_object_new_string(ST_ACCESS_CHANGED));
        sendMsgtoLocalService((char*)ST_SEND_PUBLISH, (char *)json_object_to_json_string(publish_jobj));
        json_object_put(publish_jobj);
    }

    json_object_put(userArray);

sync_all_done:
    SAFE_FREE(header);
    SAFE_FREE(response);
    // SAFE_FREE(ruleAllETag);
    return;
}

void sync_users_from_cloud(void)
{
    char *token = VR_(read_option)(NULL, UCI_CLOUD_TOKEN);
    if(!token)
    {
        SLOGI("Token not found\n");
        return;
    }

    if(!strcmp(token, "default"))
    {
        SLOGI("Token invalid\n");
        free(token);
        return;
    }

    char *hubUUID = VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
    if(!hubUUID)
    {
        SLOGI("hubUUID not found\n");
        free(token);
        return;
    }

    char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    sync_all_users(cloud_address, hubUUID, token);

    SAFE_FREE(cloud_address);
    free(hubUUID);
    free(token);
    return;
}