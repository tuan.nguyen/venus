#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "vr_rest.h"
#include "database.h"
#include "verik_utils.h"
#include "slog.h"
#include "sync_resource.h"
#include "vr_sound.h"

// extern sqlite3 *rule_db;
extern sqlite3 *dev_db;
extern sqlite3 *support_devs_db;
extern char *g_shm;

static resource_list_t g_resource_list;

#define ETAG_PARTERN "ETag: "

static void update_Etag(char *header)
{
    if(!header)
    {
        return;
    }

    char *ch = strstr(header, ETAG_PARTERN);
    if(ch)
    {
        char *endLine = strstr(ch, "\r\n");
        if(!endLine)
        {
            return;
        }

        size_t ETagNum = endLine - ch - strlen(ETAG_PARTERN);
        char newETag[SIZE_256B];
        if(ETagNum > (sizeof(newETag)-1))
        {
            return;
        }

        strncpy(newETag, ch+strlen(ETAG_PARTERN), ETagNum);
        newETag[ETagNum] = '\0';
        SLOGI("newETag = %s\n", newETag);

        VR_(write_option)(NULL, UCI_RESOURCE_ETAG"=%s", newETag);
    }
}

static void _detele_resource(char *resource_id)
{
    if(resource_id)
    {
        VR_(delete_resources)(resource_id, DEFAULT_TIMEOUT);
        database_actions("pubsub_handler", dev_db, 
                    "DELETE from DEVICES where owner='%s'", resource_id);
        shm_update_data(g_shm, resource_id, NULL, NULL, SHM_DELETE);
    }
}

static void free_resource_list(resource_list_t *tmp)
{
    if(!tmp)
    {
        return;
    }

    SAFE_FREE(tmp->deviceId);
    SAFE_FREE(tmp->deviceName);
    SAFE_FREE(tmp->serialId);
    SAFE_FREE(tmp->type);
    SAFE_FREE(tmp->cloudId);
    SAFE_FREE(tmp);
}


static int add_new_resource_to_list(void *node_add)
{
    int res = 0;
    resource_list_t *input = (resource_list_t *)node_add;
    resource_list_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &(g_resource_list.list), list)
    {
        if(!strcmp(tmp->deviceId, input->deviceId))
        {
            res = 1;
            break;
        }
    }

    if(!res)
    {
        VR_(list_add_tail)(&(input->list), &(g_resource_list.list));
    }

    return res;
}

static void _post_new_devices()
{
    resource_list_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_resource_list.list)
    {
        tmp = VR_(list_entry)(pos, resource_list_t, list);

        json_object *resource = json_object_new_object();

        JSON_ADD_STRING_SAFE(resource, ST_ID, tmp->cloudId);
        json_object_object_add(resource, ST_CLOUD_LOCAL_ID, json_object_new_string(tmp->deviceId));
        json_object_object_add(resource, ST_CLOUD_SERIAL_ID, json_object_new_string(tmp->serialId));
        json_object_object_add(resource, ST_NAME, json_object_new_string(tmp->deviceName));

        char resource_id[SIZE_256B];
        memset(resource_id, 0x00, sizeof(resource_id));
        if(!VR_(post_resources)((char *)json_object_to_json_string(resource), resource_id,
                                sizeof(resource_id), DEFAULT_TIMEOUT));
        {
            if(strlen(resource_id))
            {
                if(!tmp->cloudId)
                {
                    database_actions("pubsub_handler", dev_db, 
                                "INSERT INTO DEVICES(deviceId,deviceType,owner)"
                                "VALUES('%s','%s','%s')", tmp->deviceId, tmp->type, resource_id);
                    shm_update_data(g_shm, resource_id, tmp->deviceId, tmp->type, SHM_ADD);
                }
                else if(strcmp(tmp->cloudId, resource_id))
                {
                    shm_update_data(g_shm, tmp->cloudId, NULL, NULL, SHM_DELETE);
                    database_actions("pubsub_handler", dev_db, 
                            "UPDATE DEVICES set owner='%s', deviceType='%s' where deviceId='%s'",
                            resource_id, tmp->type, tmp->deviceId);
                    shm_update_data(g_shm, resource_id, tmp->deviceId, tmp->type, SHM_ADD);
                }
            }
        }
        json_object_put(resource);

        VR_(list_del)(pos);
        free_resource_list(tmp);
    }
}

static void update_resource(json_object *fromCloud)
{
    if(!fromCloud)
    {
        return;
    }

    CHECK_JSON_OBJECT_EXIST(idObj, fromCloud, ST_ID, update_resource_done);
    CHECK_JSON_OBJECT_EXIST(localIdObj, fromCloud, ST_CLOUD_LOCAL_ID, update_resource_done);
    CHECK_JSON_OBJECT_EXIST(nameObj, fromCloud, ST_NAME, update_resource_done);
    CHECK_JSON_OBJECT_EXIST(serialIdObj, fromCloud, ST_CLOUD_SERIAL_ID, update_resource_done);
    const char *resourceId = json_object_get_string(idObj);
    const char *localId = json_object_get_string(localIdObj);

    int found = 0;
    struct VR_list_head *pos, *q;
    resource_list_t *tmp = NULL;

    VR_(list_for_each_safe)(pos, q, &g_resource_list.list)
    {
        tmp = VR_(list_entry)(pos, resource_list_t, list);
        if(!strcmp(tmp->deviceId, localId))
        {
            found = 1;
            break;
        }
    }

    if(found)/*found in local, check update name, resourceId if it changes*/
    {
        if(!tmp->cloudId)
        {
            database_actions("pubsub_handler", dev_db, 
                            "INSERT INTO DEVICES(deviceId,deviceType,owner)"
                            "VALUES('%s','%s','%s')", tmp->deviceId, tmp->type, resourceId);
            shm_update_data(g_shm, (char*)resourceId, tmp->deviceId, tmp->type, SHM_ADD);
        }
        else if(strcmp(tmp->cloudId, resourceId))
        {
            database_actions("pubsub_handler", dev_db, 
                            "UPDATE DEVICES set %s='%s' where %s='%s'", 
                            ST_OWNER, resourceId, ST_DEVICE_ID, localId);
            shm_update_data(g_shm, tmp->cloudId, NULL, NULL, SHM_DELETE);
            shm_update_data(g_shm, (char*)resourceId, tmp->deviceId, tmp->type, SHM_ADD);
        }

        const char *name = json_object_get_string(nameObj);
        if(strcmp(tmp->deviceName, name))
        {
            database_actions("pubsub_handler", dev_db, 
                            "UPDATE SUB_DEVICES set %s='%s' where %s='%s'", 
                            ST_FRIENDLY_NAME, name, ST_ID, localId);
            create_all_sound_files((char *)localId, tmp->type, (char *)name, GET_SOUND_SYNCHONOUS);
        }
        /*remove from list, remain will be posted to cloud as new devices*/
        VR_(list_del)(pos);
        free_resource_list(tmp);
    }
    else /*not found, should be removed.*/
    {
        _detele_resource((char*)resourceId);
    }

update_resource_done:
    return;
}

// static void printf_resource_list(resource_list_t *resourceList)
// {
//     int i=0;
//     resource_list_t *tmp = NULL;
    
//     VR_(list_for_each_entry)(tmp, &resourceList->list, list)
//     {
//         printf("#### dev %d ######\n", i);
//         printf("deviceId = %s\n", tmp->deviceId);
//         printf("deviceName = %s\n", tmp->deviceName);
//         printf("serialId = %s\n", tmp->serialId);
//         printf("cloudId = %s\n", tmp->cloudId);
//         printf("type = %s\n", tmp->type);
//         i++;
//     }
// }

static int create_resource_list_cb(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 5)
    {
        return 0;
    }

    int i;
    char *deviceId = NULL;
    char *deviceName = NULL;
    char *serialId = NULL;
    char *cloudId = NULL;
    char *type = NULL;

    for(i=0; i<argc; i++)
    {
        if(!strcmp(azColName[i], ST_ID) || !strcmp(azColName[i], ST_UDN))
        {
            deviceId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_FRIENDLY_NAME))
        {
            deviceName = argv[i];
        }
        else if(!strcmp(azColName[i], ST_SERIAL_ID) || !strcmp(azColName[i], ST_REAL_NAME))
        {
            serialId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_OWNER))
        {
            cloudId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_TYPE))
        {
            type = argv[i];
        }
    }

    if(!deviceId || !deviceName || !serialId || !type)
    {
        SLOGI("missing info\n");
        return 0;
    }

    resource_list_t *new_resource = (resource_list_t*)malloc(sizeof(resource_list_t));
    memset(new_resource, 0x00, sizeof(resource_list_t));
    new_resource->deviceId = strdup(deviceId);
    new_resource->deviceName = strdup(deviceName);
    new_resource->serialId = strdup(serialId);
    new_resource->type = strdup(type);

    if(cloudId)
    {
        new_resource->cloudId = strdup(cloudId);
    }

    if(add_new_resource_to_list(new_resource))
    {
        free_resource_list(new_resource);
    }
    return 0;
}

static void init_resource_from_database()
{
    VR_INIT_LIST_HEAD(&g_resource_list.list);
    searching_database("pubsub_handler", dev_db, create_resource_list_cb, NULL,
                        "SELECT %s,%s,%s,DEVICES.%s,SUB_DEVICES.%s from SUB_DEVICES LEFT JOIN DEVICES on id=deviceId",
                        ST_SERIAL_ID, ST_ID, ST_FRIENDLY_NAME, ST_OWNER, ST_TYPE);

    searching_database("pubsub_handler", dev_db, create_resource_list_cb, NULL,
                        "SELECT %s,%s,%s,DEVICES.%s,CONTROL_DEVS.%s from CONTROL_DEVS LEFT JOIN DEVICES on udn=deviceId where type='upnp'",
                        ST_REAL_NAME, ST_UDN, ST_FRIENDLY_NAME, ST_OWNER, ST_TYPE);

    // printf_resource_list(&g_resource_list);
}

static int db_name_cb(void *data, int argc, char **argv, char **azColName)
{
    if(!data)
    {
        return 0;
    }

    if(argc != 3)
    {
        return 0;
    }

    char *id = argv[0];
    char *name = argv[1];
    char *type = argv[2];

    json_object *devInfo = json_object_new_object();
    json_object_object_add(devInfo, ST_ID, json_object_new_string(id));
    json_object_object_add(devInfo, ST_FRIENDLY_NAME, json_object_new_string(name));
    json_object_object_add(devInfo, ST_TYPE, json_object_new_string(type));

    json_object_array_add(data, devInfo);
    return 0;
}

#define UPDATE_SOUND_NAME_VER "2.3.1"
void update_sound_name(void)
{
    char *currentVer = VR_(read_option)(NULL, UCI_FIRMWARE_CURRENT_VER);
    char *preVer = VR_(read_option)(NULL, UCI_FIRMWARE_PRE_VER);
    if(!currentVer || !preVer)
    {
        SAFE_FREE(preVer);
        SAFE_FREE(currentVer);
        return;
    }

    if((VR_(version_cmp)(currentVer, UPDATE_SOUND_NAME_VER)>=0)
        && (VR_(version_cmp)(preVer, UPDATE_SOUND_NAME_VER)<0)
        )
    {
        json_object *nameArray = json_object_new_array();
        searching_database("pubsub_handler", dev_db, db_name_cb, nameArray,
                        "SELECT %s,%s,%s from SUB_DEVICES", ST_ID, ST_FRIENDLY_NAME, ST_TYPE);

        int i;
        int arrayLen = json_object_array_length(nameArray);
        for(i = 0; i< arrayLen; i++)
        {
            json_object *jvalue = json_object_array_get_idx(nameArray, i);
            CHECK_JSON_OBJECT_EXIST(idObj, jvalue, ST_ID, _next_);
            CHECK_JSON_OBJECT_EXIST(nameObj, jvalue, ST_FRIENDLY_NAME, _next_);
            CHECK_JSON_OBJECT_EXIST(typeObj, jvalue, ST_TYPE, _next_);

            const char *id = json_object_get_string(idObj);
            const char *name = json_object_get_string(nameObj);
            const char *type = json_object_get_string(typeObj);
            create_all_sound_files((char *)id, (char*)type, (char *)name, GET_SOUND_SYNCHONOUS);
    _next_:
            continue;
        }

        json_object_put(nameArray);
        VR_(write_option)(NULL, UCI_FIRMWARE_PRE_VER"=%s", UPDATE_SOUND_NAME_VER);
    }

    SAFE_FREE(preVer);
    SAFE_FREE(currentVer);
}

static void _sync_all_resources(char *cloud_address, char *hubUUID, char *token, int useEtag)
{
    if(!hubUUID || !token)
    {
        return;
    }

    int ret = 0;
    char address[SIZE_1024B];
    if(!cloud_address)
    {
        snprintf(address, sizeof(address), RESOURCE_GET_ALL_DOMAIN, CLOUD_DOMAIN, hubUUID);
    }
    else
    {
        snprintf(address, sizeof(address), RESOURCE_GET_ALL_DOMAIN, cloud_address, hubUUID);
    }

    char *resourceAllETag = NULL;
    char headerIn[SIZE_1024B]={0};
    snprintf(headerIn, sizeof(headerIn),"authorization: %s", token);

    if(useEtag)
    {
        resourceAllETag = VR_(read_option)(NULL, UCI_RESOURCE_ETAG);
        snprintf(headerIn+strlen(headerIn), sizeof(headerIn)-strlen(headerIn),
                    "\nIf-None-Match: %s", resourceAllETag?resourceAllETag:"");
    }

    char *header = NULL;
    char *response = NULL;
    ret = VR_(get_request)(address, headerIn, NULL, &header, &response, DEFAULT_TIMEOUT);
    if(ret)
    {
        goto sync_all_done;
    }

    // SLOGI("header = %s\n", header);
    // SLOGI("response = %s\n", response);

    if(!header)
    {
        goto sync_all_done;
    }

    if(strstr(header, "304 Not Modified"))
    {
        SLOGI("ALL RESOURCES IS NOT CHANGE\n");
        goto sync_all_done;
    }
    
    update_Etag(header);

    if(!response || !strlen(response))
    {
        goto sync_all_done;
    }

    json_object *resourceArray = VR_(create_json_object)(response);
    if(!resourceArray)
    {
        SLOGI("this is not json format\n");
        goto sync_all_done;
    }

    enum json_type type = json_object_get_type(resourceArray);
    if(json_type_array != type)
    {
        SLOGI("this is not json array format\n");
        json_object_put(resourceArray);
        goto sync_all_done;
    }

    init_resource_from_database();

    int i;
    int arrayLen = json_object_array_length(resourceArray);
    for(i=0; i<arrayLen; i++)
    {
        json_object *jvalue = json_object_array_get_idx(resourceArray, i);
        update_resource(jvalue);
    }

    // printf_resource_list(&g_resource_list);
    while(!VR_list_empty(&g_resource_list.list))
    {
        _post_new_devices();
        sleep(1);
    }

    // json_object_put(resourceListObj);
    json_object_put(resourceArray);
sync_all_done:
    SAFE_FREE(header);
    SAFE_FREE(response);
    SAFE_FREE(resourceAllETag);
    return;
}

static void sync_all_resources(int useEtag)
{
    char *token = VR_(read_option)(NULL, UCI_CLOUD_TOKEN);
    if(!token)
    {
        SLOGI("Token not found\n");
        return;
    }

    if(!strcmp(token, "default"))
    {
        SLOGI("Token invalid\n");
        free(token);
        return;
    }

    char *hubUUID = VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
    if(!hubUUID)
    {
        SLOGI("hubUUID not found\n");
        free(token);
        return;
    }

    char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    _sync_all_resources(cloud_address, hubUUID, token, useEtag);

    SAFE_FREE(cloud_address);
    free(hubUUID);
    free(token);
    return;
}

void sync_all_resources_with_cloud(int useEtag)
{
    sync_all_resources(useEtag);
}