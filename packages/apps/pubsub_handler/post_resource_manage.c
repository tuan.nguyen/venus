#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "post_resource_manage.h"
#include "VR_define.h"
#include "vr_rest.h"
#include "database.h"
#include "verik_utils.h"
#include "sync_group.h"
#include "rule_setup.h"
#include "ping.h"

extern int g_is_disconnected;
extern sqlite3 *dev_db;
extern char *g_shm;

static resource_list g_post_resource_faied;
static int handle_post_resouce_failed_enable = 1;

static void init_list()
{
    VR_INIT_LIST_HEAD(&g_post_resource_faied.list);
}

static int add_new_to_list(void *node_add)
{
    resource_list *input = (resource_list *)node_add;
    VR_(list_add_tail)(&(input->list), &(g_post_resource_faied.list));
    return 0;
}

static void free_element_in_list(resource_list *tmp)
{
    if(!tmp)
    {
        return;
    }

    SAFE_FREE(tmp->deviceId);
    SAFE_FREE(tmp->deviceType);
    SAFE_FREE(tmp->data);
    SAFE_FREE(tmp);
}

static void remove_all(void)
{
    if(VR_list_empty(&g_post_resource_faied.list))
    {
        return;
    }

    struct VR_list_head *pos, *q;
    resource_list *tmp = NULL;
    VR_(list_for_each_safe)(pos, q, &g_post_resource_faied.list)
    {
        tmp = VR_(list_entry)(pos, resource_list, list);
        VR_(list_del)(pos);
        free_element_in_list(tmp);
    }
}

void adding_failed_post_resouce_to_list(char *deviceId, char *deviceType, char *data)
{
    if(!deviceId || !deviceType || !data)
    {
        return;
    }

    if(g_is_disconnected)
    {
        return;
    }

    resource_list *new = (resource_list*)malloc(sizeof(resource_list));
    memset(new, 0x00, sizeof(resource_list));

    new->deviceId = strdup(deviceId);
    new->deviceType = strdup(deviceType);
    new->data = strdup(data);
    add_new_to_list((void*)new);
}

void handle_post_resource_failed(void *data)
{
    int ret = 0, number_update = 0;
    init_list();

    while(handle_post_resouce_failed_enable)
    {
        if(g_is_disconnected)
        {
            /*already disconnect, delete all list
            pubsub will sync all resources after connect.*/
            remove_all();
            number_update = 0;
            usleep(10000);
            continue;
        }

        if(VR_list_empty(&g_post_resource_faied.list))
        {
            usleep(10000);
            continue;
        }

        struct VR_list_head *pos, *q;
        resource_list *tmp = NULL;
        VR_(list_for_each_safe)(pos, q, &g_post_resource_faied.list)
        {
            tmp = VR_(list_entry)(pos, resource_list, list);

            if(g_is_disconnected)
            {
                /*if disconnect is detected, stop post*/
                break;
            }

            char resourceId[SIZE_256B];
            memset(resourceId, 0x00, sizeof(resourceId));

            ret = VR_(post_resources)(tmp->data, resourceId, sizeof(resourceId), DEFAULT_TIMEOUT);
            if(ret)
            {
                int sleep_time = 10;
                int res = ping_command("8.8.8.8", 3);
                if(res)
                {
                    while(!g_is_disconnected && sleep_time > 0)
                    {
                        sleep_time --;
                        sleep(1);
                    }
                }
                continue;
            }

            if(strlen(resourceId))
            {
                SEARCH_DATA_INIT_VAR(resource);
                searching_database("pubsub_handler", dev_db,  get_last_data_cb, &resource, 
                                    "SELECT owner from DEVICES where deviceId='%s'", tmp->deviceId);
                if(!resource.len)
                {
                    database_actions("pubsub_handler", dev_db, 
                            "INSERT INTO DEVICES(deviceId,deviceType,owner)"
                            "VALUES('%s','%s','%s')", tmp->deviceId, tmp->deviceType, resourceId);
                    shm_update_data(g_shm, resourceId, tmp->deviceId, tmp->deviceType, SHM_ADD);
                }
                else if(strcmp(resourceId, resource.value))
                {
                    shm_update_data(g_shm, resource.value, NULL, NULL, SHM_DELETE);
                    database_actions("pubsub_handler", dev_db, 
                            "UPDATE DEVICES set owner='%s', deviceType='%s' where deviceId='%s'",
                            resourceId, tmp->deviceType, tmp->deviceId);
                    shm_update_data(g_shm, resourceId, tmp->deviceId, tmp->deviceType, SHM_ADD);
                }

                FREE_SEARCH_DATA_VAR(resource);
            }

            // number_update ++;
            VR_(list_del)(pos);
            free_element_in_list(tmp);
        }

        if(!g_is_disconnected)
        {
            if(number_update)
            {
                sync_rule_from_cloud(NOT_USE_ETAG);
                sync_all_groups_with_cloud(NOT_USE_ETAG);
                number_update = 0;
            }
        }
        else
        {
            remove_all();
            number_update = 0;
            usleep(10000);
        }

        usleep(2000);
    }
}

void stop_post_failed_resource(void)
{
    handle_post_resouce_failed_enable = 0;
}