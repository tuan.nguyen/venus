#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "report_manage.h"
#include "VR_define.h"
#include "slog.h"

#define TIME_BETWEEN_REPORT 30

static shadow_report_t g_last_shadow_status;

static int is_time_valid(unsigned int currentTime, unsigned int lastReportTime)
{
    if((currentTime - lastReportTime) > TIME_BETWEEN_REPORT)
    {
        return 1;
    }

    return 0;
}

static int is_new_status(char *newStatus, char *lastStatus)
{
    if(strcmp(newStatus, lastStatus))
    {
        return 1; 
    }

    return 0;
}

static void add_to_new_list(char *deviceId, char *newStatus, unsigned int reportTime)
{
    shadow_report_t *newReport = (shadow_report_t*)malloc(sizeof(shadow_report_t));
    newReport->deviceId = strdup(deviceId);
    newReport->lastStatus = strdup(newStatus);
    newReport->reportTime = reportTime;

    VR_(list_add_tail)(&(newReport->list), &(g_last_shadow_status.list));
}

static void update_new_status(shadow_report_t *node, char *newStatus, unsigned int reportTime)
{
    SAFE_FREE(node->lastStatus);
    node->lastStatus = strdup(newStatus);
    node->reportTime = reportTime;
}

void init_report_filter_list()
{
    VR_INIT_LIST_HEAD(&g_last_shadow_status.list);
}

int is_report(char *deviceId, char *newStatus, unsigned int reportTime)
{
    int found = 0;
    int res = 0;
    shadow_report_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_last_shadow_status.list)
    {
        tmp = VR_(list_entry)(pos, shadow_report_t, list);
        if(tmp->deviceId && !strcmp(tmp->deviceId, deviceId))
        {
            found = 1;
            break;
        }
    }

    if(found)
    {
        res = is_time_valid(reportTime, tmp->reportTime);
        if(!res)
        {
            res = is_new_status(newStatus, tmp->lastStatus);
            if(res)
            {
                update_new_status(tmp, newStatus, reportTime);
            }
        }
        else
        {
            update_new_status(tmp, newStatus, reportTime);
        }
    }
    else
    {
        add_to_new_list(deviceId, newStatus, reportTime);
        res = 1;
    }

    return res;
}