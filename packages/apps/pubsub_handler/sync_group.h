#ifndef _SYNC_GROUP_H_
#define _SYNC_GROUP_H_

#include "VR_list.h"

#define GROUP_GET_ALL_DOMAIN      "https://%s/api/devices/%s/resourcegroups?filter[include]=resourcegroupresource"

typedef struct _group_list
{
	char *userId;
	char *groupId;
	char *groupName;
	char *groupDev;
	char *cloudId;
	char *cloudName;
    struct VR_list_head list;
}group_list_t;

void sync_all_groups_with_cloud(int useEtag);

#endif