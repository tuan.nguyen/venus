#ifndef _PUBSUB_HANDLER_H_
#define _PUBSUB_HANDLER_H_
/******************************************************************************
 * Copyright (c) 2014, VEriK Systems. All rights reserved.
 *
 * Module: Remote ACCESS
 *
 ******************************************************************************/

typedef struct _daemon_service 
{
    const char   *service_name;
    const char   *name;
    uint32_t     id;
    int          timeout;
    timer_t      timer_restart;
    unsigned int restart_time;
} daemon_service;

typedef struct _new_pubsub_ubus 
{
    char name[256];
    int type; // 0 :send all, 1: send with seassion id, 2: dont send.
    char topic[512];;
    void *service_name;

    unsigned int action_time;
    struct ubus_request *req;
    struct _new_pubsub_ubus *next;
} new_pubsub_ubus;

typedef struct _update_firmware 
{
    char URL[512];
    char topic[512];
} update_firmware_d;

typedef struct _group_action
{
    char *command;
    char *value;
} group_action_data;

enum {
    NOTIFY_ID,
    SERVICE_NAME,
    __NOTIFY_MAX
};

enum {
	NOTIFY_SERVICE,
	NOTIFY_MSG,
	__NOTIFY_SIZE
};

typedef void (* command_handler_f)(const char *service_type, const char *msg);

typedef struct command_handler_s
{
	const char *name;
	command_handler_f handler; 
} command_handler_t;

typedef struct _thermostat_mode_convert_
{
    const char *mode;
    const char *setPointMode;
} thermostat_mode_convert_t;

#ifdef AWS_IOT_MQTT_CLIENT_ID
#undef AWS_IOT_MQTT_CLIENT_ID
#endif

#ifdef AWS_IOT_ROOT_CA_FILENAME
#undef AWS_IOT_ROOT_CA_FILENAME
#endif

#ifdef AWS_IOT_CERTIFICATE_FILENAME
#undef AWS_IOT_CERTIFICATE_FILENAME
#endif

#ifdef AWS_IOT_PRIVATE_KEY_FILENAME
#undef AWS_IOT_PRIVATE_KEY_FILENAME
#endif

//#define AWS_IOT_MQTT_HOST              "AHHQRB8CY8LJA.iot.us-east-1.amazonaws.com" ///< Customer specific MQTT HOST. The same will be used for Thing Shadow
#define AWS_IOT_MQTT_PORT			   8883
//#define AWS_IOT_MQTT_CLIENT_ID		   "10000000000010"
#define AWS_IOT_ROOT_CA       		   CERTIFICATE_PATH ///< Root CA
#define AWS_IOT_ROOT_CA_FILENAME       AWS_IOT_ROOT_CA"root-CA.crt" ///< Root CA file name
#define AWS_IOT_CERTIFICATE_FILENAME   AWS_IOT_ROOT_CA"certificate.pem.crt" ///< device signed certificate file name
#define AWS_IOT_PRIVATE_KEY_FILENAME   AWS_IOT_ROOT_CA"private.pem.key" ///< Device private key filename

#define QOS0  0
#define QOS1  1

#define CONNECT_DELAY_TIME  25

#define CHECK_CLAIMED_DOMAIN     "https://%s/api/devices/%s/isclaimed"
#define POWER_CYCLE_EVENT_DOMAIN "https://%s/api/devices/%s/powerCycleEvent"

#define EXECUTE_MODE 1 
#define TRIGGER_MODE 2

void _trigger_actions_(char *ruleId, char *actions, int mode);

#endif 