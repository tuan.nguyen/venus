#ifndef _REPORT_MANAGE_H_
#define _REPORT_MANAGE_H_

#include "VR_list.h"

typedef struct _shadow_report_
{
    char *deviceId;
    char *lastStatus;
    unsigned int reportTime;

    struct VR_list_head list;
} shadow_report_t;

void init_report_filter_list(void);
int is_report(char *deviceId, char *newStatus, unsigned int reportTime);
#endif