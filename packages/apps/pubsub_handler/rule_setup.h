#ifndef _RULE_SETUP_H_
#define _RULE_SETUP_H_

#define RULE_MISSING_INFO -1
#define RULE_NAME_TOO_LONG -2
#define RULE_EXIST -3
#define RULE_DATABASE_FAILED -4
#define RULE_SUCCESS 0

#define RULE_ACCEPT_DOMAIN      		"https://%s/api/rules/%s/accepted"
#define RULE_GET_UNACCEPT_DOMAIN      	"https://%s/api/devices/%s/rules?accepted=null"
#define RULE_GET_ALL_DOMAIN      		"https://%s/api/devices/%s/rules"

#include "VR_list.h"
#include "rule.h"

typedef struct _rule_list
{
	char *type;
	char *ruleId;
	char *name;
	char *conditions;
	char *actions;
	char *enabled;  
    struct VR_list_head list;
}rule_list_t;

int set_rule(char *ruleType, char*ruleId, char *ruleName, char *condition, char *actions, char *enabled);
int rule_actions(char *ruleAction, char *ruleType, char*ruleId, 
                 char *ruleName, char *condition, char *actions,
                 char *enabled);
int VR_(rule_accept)(char *ruleId, int64_t timeStamp, json_bool isAccept);
void VR_(scene_trigger)(char *deviceId, char *buttonId);
void VR_(rule_be_triggered)(char *ruleId, char *data);
void sync_rule_from_cloud(int useEtag);
#endif