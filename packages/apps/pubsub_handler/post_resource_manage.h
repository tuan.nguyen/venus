#ifndef _POST_RESOUCE_MANAGE_H_
#define _POST_RESOUCE_MANAGE_H_

#include "VR_list.h"

typedef struct _resource_list_
{
	char *deviceId;
	char *deviceType;
    char *data;

    struct VR_list_head list;
} resource_list;

void adding_failed_post_resouce_to_list(char *deviceId, char *deviceType, char *data);
void handle_post_resource_failed(void *data);
void stop_post_failed_resource(void);
#endif