#ifndef _RADIO_MON_H
#define _RADIO_MON_H


//#define USE_PULSEAUDIO 		1
#define USE_ALSA 		1
//#define USE_SNDFILE 		1

#define N_CHANNELS 		1

#define BFSK_DATA_RATE 		50.0
#define BFSK_MARK_FREQ 		20000
#define BFSK_SPACE_FREQ 	18500

#define APP_DEBUG
#ifdef APP_DEBUG
# define dbg_log(format, args...)  fprintf(stderr, format, ## args)
#else
# define dbg_log(format, args...)
#endif

typedef struct command_t 
{
	const char *name;
	int cmdnum;
}command;

typedef struct wifiinfo_t 
{
	char length_expect;
	char length_received;
	char ssid_received[100];
	unsigned short crc_received;

	int crc_valid;
	int ssid_valid;
	char crc_ssid[100];
	char ssid_ssid[100];
}wifiinfo;

enum cmdname {
  BSS = 0,
  capability,
  signal_t,
  SSID,
  WPA,
  RSN,
  Pairwise_ciphers,
  Authentication_suites,
};

typedef int (*callback)(char *str, int len);

int radio_receive(char *program_name, callback ptr_func);
int execute_system(const char*cmd);

#endif /* _RADIO_MON */
