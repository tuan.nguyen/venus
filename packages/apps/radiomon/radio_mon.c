#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sqlite3.h> 

#include "radio_mon.h"
#include "VR_define.h"

#define MIN_ARGS	5
#define MAX_ARGS	10

#define MAX_STR		64
#define START_SYMBOL	0x2 /* Start of Text in ASCII */
#define STOP_SYMBOL	0x3 /* End of Text in ASCII */
#define FIELD_SEPARATOR 0x1e /* Record Separator in ASCII */

#define ULTRA_SOUND_BUFFER_SIZE 100

sqlite3 *wifiInfor_db;

enum key_t {
	SSID_KEY = 0,
	PASS_KEY,
	MODE_KEY,
	MAX_KEY
};

#define MAX_BUFFER_SIZE 4096

struct wifi_info {
	unsigned char field_is_valid[MAX_KEY];
	char field_value[MAX_KEY][MAX_STR];
};

enum state_t {
	IDLE = 0,
	START_RECEIVED,
	STOP_RECEIVED,
	MAX_STATE
};

struct data_receive_sm_t {
	unsigned char *buf;
	int idx;
	enum state_t state;
};

struct wifi_info wifi;
struct data_receive_sm_t data_receive_sm;

void reset_sm()
{
	data_receive_sm.idx = 0;
	data_receive_sm.state = IDLE;
	memset(data_receive_sm.buf, 0x00, sizeof(char)*MAX_BUFFER_SIZE);
}

int crc16(char *buffer, int length)
{
	unsigned short crc = 0xFFFF;
	int j = 0;

	for (j = 0; j < length ; j++) {
		crc = ((crc  >> 8) | (crc  << 8) )& 0xffff;
		crc ^= (buffer[j] & 0xff);//byte to int, trunc sign
		crc ^= ((crc & 0xff) >> 4);
		crc ^= (crc << 12) & 0xffff;
		crc ^= ((crc & 0xFF) << 5) & 0xffff;
	}
	crc &= 0xffff;
	return crc;
}

int execute_system(const char*cmd)
{
    if (!cmd) {
        return -1;
    }
    printf("Executing %s...", cmd);
    int result = system(cmd);
    if (result==0){
    	printf("OK\n");
    }
    else{
    	printf("System result=%d\n", result);
    }
    if (-1 == result) {
        printf("Error executing system: %s\n", cmd);
    }
    return result;
}

static int check_wifi_connection(int time_repeat)
{
	int tmp = time_repeat;
	FILE *fp;
	char * line = NULL;
	size_t len = 0;
    ssize_t read;
	while(tmp > 0)
	{
		sleep(7*(time_repeat-tmp));
		fp=popen("wpa_cli -i wlan0 status | grep wpa_state | cut -d= -f2","r"); //wlan0 is the only obwifi interface
		while ((read = getline(&line, &len, fp)) != -1)
		{
			char *pos;
			if ((pos=strchr(line, '\n')) != NULL)
			    *pos = '\0';
			printf("++++++++++++++ line = %s\n", line);
			if(!strcmp(line, "COMPLETED"))
			{
				free(line);
				pclose(fp);
				return 1;
			}	
		}
		tmp--;
		if(fp)
			pclose(fp);
		fp=NULL;
		if(line)
			free(line);
	}
	return 0;
}

static int open_database(char *Database_Location)
{
    int  rc;
    /* Open database */
    rc = sqlite3_open(Database_Location, &wifiInfor_db);
    if( rc )
    {
        printf("Can't open database: %s\n", sqlite3_errmsg(wifiInfor_db));
        return -1;
    }
    else
    {
        printf("Opened database successfully\n");
    }

    return rc; 
}

static int db_callback(void *data, int argc, char **argv, char **azColName){
   int i;
   for(i=0; i<argc; i++){
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
      if(data != NULL) strcpy(data, argv[i]);
   }
   printf("\n");
   return 0;
}

static void searching_database(char* result, const char* format, ...)
{
    char *zErrMsg = 0;
    char sql[512];
    memset(sql, 0x00, sizeof(sql));

    va_list argptr;
    va_start(argptr, format);
    vsprintf(sql, format, argptr);
    va_end(argptr);

    int rc = sqlite3_exec(wifiInfor_db, sql, db_callback, (void*)result, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        printf("SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        printf("%s SUCCESS\n", sql);
    }
}

static void database_actions(const char* format, ...)
{
    char sql[512];
    memset(sql, 0x00, sizeof(sql));
    char *zErrMsg = 0;

    va_list argptr;
    va_start(argptr, format);
    vsprintf(sql, format, argptr);
    va_end(argptr);

    int rc = sqlite3_exec(wifiInfor_db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        fprintf(stdout, "%s SUCCESS\n", sql);
    }
}

static int interface_exist(const char* if_name)
{
	FILE *fp;
	char * line = NULL;
	size_t len = 0;
    ssize_t read;
	fp=popen("iw dev | grep \"Interface\" | cut -f 2 -s -d\" \"", "r");
	while ((read = getline(&line, &len, fp)) != -1)
	{
		char *pos;
		if ((pos=strchr(line, '\n')) != NULL)
		    *pos = '\0';
		printf("line = %s\n", line);
		if(!strcmp(line, if_name))
		{
			free(line);
			pclose(fp);
			return 1;
		}
			
	}
	if(line)
		free(line);
	pclose(fp);
	return 0;
}

command commands[] = {
	{"BSS ",						BSS},
	{"capability:",     			capability},
	{"signal:",        				signal_t},
	{"SSID:", 						SSID},
    {"WPA:",            			WPA},
    {"RSN:",       					RSN},
    {"* Pairwise ciphers:",     	Pairwise_ciphers},
    {"* Authentication suites:",    Authentication_suites},
};

static int scan_wifi()
{
	database_actions("DELETE from WIFI_INFO;");
	FILE *fp;
	int i;
	char *line = NULL;
	size_t len = 0;
    ssize_t read;

    char quality[32], ssid[128], encryption[128];
	char enc[128], ccmp[128], tkip[128], psk[128];

	memset(quality, 0x00, sizeof(quality));
	memset(ssid, 0x00, sizeof(ssid));
	memset(encryption, 0x00, sizeof(encryption));
	memset(enc, 0x00, sizeof(enc));
	memset(ccmp, 0x00, sizeof(ccmp));
	memset(tkip, 0x00, sizeof(tkip));
	memset(psk, 0x00, sizeof(psk));

	if(!interface_exist("wlan1"))
	{
		execute_system("iw phy0 interface add wlan1 type managed");
		execute_system("ifconfig wlan1 up");
	}

	fp=popen("iw wlan1 scan ap-force | "
				"grep -e \"^BSS\" "
					" -e \"signal:\" "
					" -e \"SSID:\" "
					" -e \"capability:\" "
					" -e \"RSN:\" "
					" -e \"WPA:\" "
					" -e \"Pairwise ciphers\" "
					" -e \"Authentication suites\"", "r");
	while ((read = getline(&line, &len, fp)) != -1) 
	{
        for(i = 0; i<sizeof(commands)/sizeof(command); i++)
        {
        	if(strstr(line, commands[i].name))
        	{
        		switch(commands[i].cmdnum)
				{
					case BSS:
					{
						if(strstr(line, "(on wlan1)"))
						{
							unsigned char tmp[256];
							memset(tmp ,0x00, sizeof(tmp));
							tmp[0]=0x04+strlen(ssid); //package length
							tmp[1]=0x00; //field id: ssid
							for(i=0; i< strlen(ssid); i++)
							{
								tmp[i+2] = ssid[i];
							}

							if(!strcmp(enc, "OPEN") || !strcmp(enc, "WEP"))
							{
								database_actions("INSERT INTO %s VALUES('%s','%s','%s','%s','%02X','%04X')", 
												"WIFI_INFO (Quality,SSID,Encryption,Mode,Length,CRC)", 
												quality, ssid, encryption, enc, tmp[0], crc16(tmp, tmp[0]-2));
								//printf("%s\t%s\t%s\t%s\n", quality, enc, ssid, encryption);
							}
							else if(strlen(ssid))
							{
								database_actions("INSERT INTO %s VALUES('%s','%s','%s','%s-%s-%s-%s','%02X','%04X')", 
												"WIFI_INFO (Quality,SSID,Encryption,Mode,Length,CRC)", 
												quality, ssid, encryption, enc, ccmp, tkip, psk, tmp[0], crc16(tmp, tmp[0]-2));
								//printf("%s\t%s-%s-%s-%s\t%s\t%s\n", quality, enc, ccmp, tkip, psk, ssid, encryption);
							}

							memset(quality, 0x00, sizeof(quality));
							memset(ssid, 0x00, sizeof(ssid));
							memset(encryption, 0x00, sizeof(encryption));

							memset(enc, 0x00, sizeof(encryption));
							memset(ccmp, 0x00, sizeof(encryption));
							memset(tkip, 0x00, sizeof(encryption));
							memset(psk, 0x00, sizeof(encryption));

							strcpy(encryption, "open");
							strcpy(enc, "OPEN");
						}
						
						break;
					}

					case capability:
					{
						if(strstr(line, "Privacy"))
						{
							strcpy(encryption, "wep");
							strcpy(enc, "WEP");
						}
						break;
					}

					case signal_t:
					{
						strncpy(quality, line+strlen(commands[i].name)+2, sizeof(quality));
						char *pos;
						if ((pos=strchr(quality, '\n')) != NULL)
						    *pos = '\0';
						break;
					}

					case SSID:
					{
						strncpy(ssid, line+strlen(commands[i].name)+2, sizeof(ssid));
						char *pos;
						if ((pos=strchr(ssid, '\n')) != NULL)
						    *pos = '\0';
						break;
					}

					case WPA:
					{
						if(strstr(encryption, "psk2"))
						{
							strcpy(encryption, "mixed-psk");
						}
						else
						{
							strcpy(encryption, "psk");
						}
						
						if(!strcmp(enc, "WPA2"))
						{
							sprintf(enc, "%s/%s", enc, "WPA");
						}
						else
						{
							strcpy(enc, "WPA");
						}
						break;
					}

					case RSN:
					{
						if(strstr(encryption, "psk"))
						{
							strcpy(encryption, "mixed-psk");
						}
						else
						{
							strcpy(encryption, "psk2");
						}

						if(!strcmp(enc, "WPA"))
						{
							sprintf(enc, "%s/%s", enc, "WPA2");
						}
						else
						{
							strcpy(enc, "WPA2");
						}
						break;
					}

					case Pairwise_ciphers:
					{
						if(strstr(line, "CCMP"))
						{
							strcpy(ccmp, "CCMP");
							sprintf(encryption, "%s+%s", encryption, "ccmp");
						}
						if(strstr(line, "TKIP"))
						{
							strcpy(tkip, "TKIP");
							sprintf(encryption, "%s+%s", encryption, "tkip");
						}

						break;
					}

					case Authentication_suites:
					{
						if(strstr(line, "PSK"))
						{
							strcpy(psk, "PSK");
						}
						break;
					}
				}
			break;
        	}
        }
    }
    if(line)
    {
    	free(line);
    }
	pclose(fp);
	return 0;
}

static int distance (const char * word1,int len1,
                     const char * word2,int len2)
{
    int matrix[len1 + 1][len2 + 1];
    int i;
    for (i = 0; i <= len1; i++) {
        matrix[i][0] = i;
    }
    for (i = 0; i <= len2; i++) {
        matrix[0][i] = i;
    }
    for (i = 1; i <= len1; i++) {
        int j;
        char c1;

        c1 = word1[i-1];
        for (j = 1; j <= len2; j++) {
            char c2;

            c2 = word2[j-1];
            if (c1 == c2) {
                matrix[i][j] = matrix[i-1][j-1];
            }
            else {
                int delete;
                int insert;
                int substitute;
                int minimum;

                delete = matrix[i-1][j] + 1;
                insert = matrix[i][j-1] + 1;
                substitute = matrix[i-1][j-1] + 1;
                minimum = delete;
                if (insert < minimum) {
                    minimum = insert;
                }
                if (substitute < minimum) {
                    minimum = substitute;
                }
                matrix[i][j] = minimum;
            }
        }
    }
    return matrix[len1][len2];
}



static int guess_db_callback(void *data, int argc, char **argv, char **azColName){
    int i;
    wifiinfo *wifi_info = (wifiinfo*)data;
    // printf("wifi_info->length_expect = %02X\n", wifi_info->length_expect);
    // printf("wifi_info->length_received = %02X\n", wifi_info->length_received);
    // for(i = 0; i < wifi_info->length_received - 2; i++)
    // {
    // 	printf("wifi_info->ssid_received[%d] = %02X\n", i, wifi_info->ssid_received[i]);
    // }
    // printf("wifi_info->crc_received = %02X\n", wifi_info->crc_received);

    char ssid[100];

    for(i=0; i<argc; i++){
    	printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    	if(!strcmp(azColName[i], "SSID"))
    	{
    		int rc = distance(argv[i], strlen(argv[i]), wifi_info->ssid_received, wifi_info->length_received - 2);
    		rc = rc*100/(int)wifi_info->length_expect;
    		printf("exact percent = %d\n", rc);
    		if(rc <= 25)
    		{
    			if(!*wifi_info->ssid_ssid && !wifi_info->ssid_valid)
    			{
    				wifi_info->ssid_valid = 1;
    				strcpy(wifi_info->ssid_ssid, argv[i]);
    			}
    			else
    			{
    				wifi_info->ssid_valid = 0;
    			}
    		}

    		if(argv[i]) 
    			strcpy(ssid, argv[i]);
    	}
    	else if(!strcmp(azColName[i], "CRC"))
    	{
    		char tmp[8];
    		sprintf(tmp, "%02X", wifi_info->crc_received);
    		if(!strcmp(tmp, argv[i]))
    		{
    			if(!*wifi_info->crc_ssid && !wifi_info->crc_valid)
    			{
    				wifi_info->crc_valid = 1;
    				strcpy(wifi_info->crc_ssid, ssid);
    			}
    			else
    			{
    				wifi_info->crc_valid = 0;
    			}
    		}
    	}
    }
    // printf("wifi_info->crc_valid = %d\n", wifi_info->crc_valid);
    // printf("wifi_info->crc_ssid = %s\n", wifi_info->crc_ssid);

    // printf("wifi_info->ssid_valid = %d\n", wifi_info->ssid_valid);
    // printf("wifi_info->ssid_ssid = %s\n", wifi_info->ssid_ssid);

    // printf("\n");
    return 0;
}

int guess_ssid(char length_expect, char length_received)
{
	char *zErrMsg = 0;
    char sql[512];
    sprintf(sql, "SELECT SSID,CRC from WIFI_INFO where Length='%02X'", length_expect);
    wifiinfo wifi_info;
    wifi_info.crc_valid = 0;
    wifi_info.ssid_valid = 0;
    wifi_info.length_expect = length_expect;
    wifi_info.length_received = length_received;
    memset(wifi_info.crc_ssid, 0x00, sizeof(wifi_info.crc_ssid));
    memset(wifi_info.ssid_ssid, 0x00, sizeof(wifi_info.ssid_ssid));
    memcpy(&wifi_info.ssid_received, data_receive_sm.buf + 2, length_received - 2);
    wifi_info.crc_received = data_receive_sm.buf[length_received - 1] | (data_receive_sm.buf[length_received - 2] << 8);

    int rc = sqlite3_exec(wifiInfor_db, sql, guess_db_callback, (void*)&wifi_info, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        printf("SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        printf("%s SUCCESS\n", sql);
    }

    // printf("wifi_info.crc_valid = %d\n", wifi_info.crc_valid);
    // printf("wifi_info.crc_ssid = %s\n", wifi_info.crc_ssid);

    // printf("wifi_info.ssid_valid = %d\n", wifi_info.ssid_valid);
    // printf("wifi_info.ssid_ssid = %s\n", wifi_info.ssid_ssid);

    if(wifi_info.ssid_valid)
    {
    	data_receive_sm.idx = length_expect;
    	strncpy(data_receive_sm.buf + 2, wifi_info.ssid_ssid, strlen(wifi_info.ssid_ssid));
    }

    if(wifi_info.crc_valid)
    {
    	data_receive_sm.idx = length_expect;
    	strncpy(data_receive_sm.buf + 2, wifi_info.crc_ssid, strlen(wifi_info.crc_ssid));
    	return 1;
    }
	return 0;
}

int receive_validation()
{
	/* data format
	 * 1 byte: length of the packet, included this 1 byte
	 * 1 byte field_id: 0-SSID, 1-PASSWD, 2-MODE 
	 * variable field data bytes
	 * 2 byte CRC-16: checksum is calculated from the first 
	 *    byte to the byte immediately before CRC, stored in BE
	 */
	char pkt_len = data_receive_sm.buf[0];
	unsigned char field_id = data_receive_sm.buf[1];
	unsigned short expected_crc = 0;
	unsigned short pkt_crc = 0;
	int i = 0;
	unsigned char all_field_valid = 1;
	/* for wifi conneting
	*/
	int check_crc = 0;
	// printf("pkt_len = %d\n", pkt_len);
	// printf("field_id = %d\n", field_id);
	// for(i = 0; i < data_receive_sm.idx; i++)
	// {
	// 	printf("%02X\n", data_receive_sm.buf[i]);
	// }

	if (pkt_len != data_receive_sm.idx) {
		fprintf(stderr, "%s: invalid len %d expected %d\n",
				__FUNCTION__, data_receive_sm.idx, pkt_len);
		if(!field_id && data_receive_sm.idx > 2)
		{
			if(guess_ssid(pkt_len, data_receive_sm.idx))
			{
				check_crc = 1;
			}
			else
			{
				return 1;
			}
		}	
	}

	if(!check_crc)
	{
		pkt_crc = data_receive_sm.buf[pkt_len - 1]
			| (data_receive_sm.buf[pkt_len - 2] << 8);
		expected_crc = crc16((char *)data_receive_sm.buf, pkt_len - 2);
		if (pkt_crc != expected_crc) {
			fprintf(stderr, "%s: invalid crc 0x%04x expected 0x%04x\n",
					__FUNCTION__, pkt_crc, expected_crc);
			return 1;
		}
	}

// printf("#############################\n");
// 	for(i = 0; i < data_receive_sm.idx; i++)
// 	{
// 		printf("%02X\n", data_receive_sm.buf[i]);
// 	}

	if (field_id < 0 || field_id >= MAX_KEY) {
		fprintf(stderr, "%s: invalid field id 0x%02x\n",
				__FUNCTION__, field_id);
		return 1;
	}

	if (pkt_len - 1 > MAX_STR) {
		fprintf(stderr, "%s: field %d exceed MAX_STR\n",
				__FUNCTION__, field_id);
		return 1;
	}

	/* we got a valid field */
	execute_system("/etc/led_control.sh STA_done");
	wifi.field_is_valid[field_id] = 1;
	memcpy(&wifi.field_value[field_id][0],
		data_receive_sm.buf + 2, pkt_len - 4); /* exclue len + field_id
							  + CRC */
	wifi.field_value[field_id][pkt_len - 3] = '\0';
	fprintf(stdout, "FIELD %d is valid len %d %s\n", field_id, pkt_len - 3, wifi.field_value[field_id]);

	/* if all fields are valid, just stop */
	for (i = 0; i < MAX_KEY; i++) {
		all_field_valid &= wifi.field_is_valid[i];
		if (all_field_valid  == 0)
			break;
	}
	if (all_field_valid != 0) {
		for (i = 0; i < MAX_KEY; i++) {
			fprintf(stdout, "field[%d]=%s\n",
					i, wifi.field_value[i]);
		}

		printf("Connecting to the new access point ...");

		execute_system("cp /etc/config/wireless /etc/config/wireless_old");

		char cmd[3*ULTRA_SOUND_BUFFER_SIZE], encryption[ULTRA_SOUND_BUFFER_SIZE];
		memset(cmd, 0x00, sizeof(char)*3*ULTRA_SOUND_BUFFER_SIZE);
		memset(encryption, 0x00, sizeof(char)*ULTRA_SOUND_BUFFER_SIZE);

		sprintf(cmd, "uci set security.@local-access[0].key=\"%s\"", wifi.field_value[2]); execute_system(cmd);
		execute_system("uci commit security");

		sprintf(cmd, "uci set wireless.@wifi-iface[0].mode=sta"); execute_system(cmd);
		sprintf(cmd, "uci set wireless.@wifi-iface[0].network=cereswifi"); execute_system(cmd);
		sprintf(cmd, "uci set wireless.@wifi-iface[0].ssid=\"%s\"", wifi.field_value[0]); execute_system(cmd);

		if(strcmp(wifi.field_value[1], "open"))
		{
			sprintf(cmd, "uci set wireless.@wifi-iface[0].key=%s",wifi.field_value[1]); execute_system(cmd);
		}
		
		searching_database(encryption, "SELECT Encryption from WIFI_INFO where SSID='%s'", wifi.field_value[0]);

		if(!*encryption)
		{
			int repeat = 3;
			while(repeat)
			{
				sleep(3);
				scan_wifi();
				searching_database(encryption, "SELECT Encryption from WIFI_INFO where SSID='%s'", wifi.field_value[0]);
				if(*encryption)
				{
					break;
				}
				repeat = repeat - 1;
			}

			if(!repeat)
			{
				memset(&wifi, 0, sizeof(wifi));
				return 1;
			}
		}

		sprintf(cmd, "uci set wireless.@wifi-iface[0].encryption=%s", encryption); execute_system(cmd);
		execute_system("uci commit wireless");
		execute_system("wifi");

		// execute_system("/etc/led_control.sh STA_done");
		// execute_system("echo 0 > /sys/class/leds/BL_led/brightness");
		// execute_system("/etc/led_control.sh STA_connecting");
		sleep(10);

		if (!check_wifi_connection(3))
		{
			execute_system("cp /etc/config/wireless_old /etc/config/wireless");
			execute_system("wifi");
			if(!check_wifi_connection(3))
			{
				execute_system("wifi detect > /etc/config/wireless");
				execute_system("wifi");
				// execute_system("/etc/led_control.sh AP_mode");
			}
			else
			{
				// execute_system("/etc/led_control.sh STA_connected");
			}
		}
		else
		{
			// execute_system("/etc/init.d/alljoyn restart");
			// execute_system("/etc/init.d/start_services restart");
		}

		memset(&wifi, 0, sizeof(wifi));
	}
	return all_field_valid == 0 ? 1 : 0;
}

int receive_sm(char *str, int len)
{
	int i = 0;
	int finish = 0;

#if 0
	dbg_log("%s: feed %d byte 0x%02x state %d idx %d\n",
			__func__, len, str[0],
			data_receive_sm.state, data_receive_sm.idx);
#endif

	for (i = 0; i < len; i++) {
		switch(data_receive_sm.state) {
		case IDLE:
			/* check for START_SYMBOL */
			if (str[i] == START_SYMBOL) {
				data_receive_sm.idx = 0;
				memset(data_receive_sm.buf, 0x00, sizeof(char)*MAX_BUFFER_SIZE);
				data_receive_sm.state = START_RECEIVED;
			} else {
				reset_sm();
			}
			break;
		case START_RECEIVED:
			/* store data to internal buffer
			 * if it is not STOP SYMBOL */
			if (str[i] == STOP_SYMBOL) {
				data_receive_sm.state = STOP_RECEIVED;
			} else {
				if (data_receive_sm.idx > MAX_BUFFER_SIZE) {
					fprintf(stderr, "receive buffer overflow \
							reset state machine\n");
					reset_sm();
					break;
				}
				// printf(" received data %02X\n", str[i]);
				data_receive_sm.buf[data_receive_sm.idx++] = str[i];
				break;
			}
			/* Fall through for STOP_RECEIVED */
		case STOP_RECEIVED:
			if (receive_validation() == 0) {
				finish = 1;
			} else {
				reset_sm();
			}
			break;
		default:
			reset_sm();
			break;
		}
		if (finish)
			return 0;
	}
	return 1;
}

int main(int argc, char** argv)
{
	execute_system("/etc/led_control.sh radiomon");
	open_database(WIFI_INFO_DATABASE);
	scan_wifi();
	// FILE *fp;char buff[32];
	// fp=popen("iw wlan0 info | grep \"type\" | cut -d' ' -f2", "r");
	// fgets(buff, 32, fp);
	// printf("buff = %s\n", buff);
	// if(strstr(buff, "AP"))
	// {
	// 	execute_system("/etc/led_control.sh AP_mode");
	// }
	// if(strstr(buff, "managed"))
	// {
	// 	execute_system("/etc/led_control.sh STA_connecting");
	// 	if(!check_wifi_connection(3))
	// 	{
	// 		execute_system("wifi detect > /etc/config/wireless");
	// 		execute_system("wifi");
	// 		execute_system("/etc/led_control.sh AP_mode");
	// 	}
	// 	else
	// 	{
	// 		execute_system("/etc/led_control.sh STA_connected");
	// 	}
	// }
	// fclose(fp);
	data_receive_sm.idx = 0;
	data_receive_sm.buf = malloc(sizeof(char)*MAX_BUFFER_SIZE);
	data_receive_sm.state = IDLE;
	memset(&wifi, 0, sizeof(wifi));
	if (!data_receive_sm.buf) {
		fprintf(stderr, "Cannot allocate memory\n");
		exit(1);
	}

	// data_receive_sm.idx = 0;
	// memset(data_receive_sm.buf, 0x00, sizeof(char)*MAX_BUFFER_SIZE);
	// data_receive_sm.buf[data_receive_sm.idx++] = 0x14;
	// receive_validation();
	radio_receive("demo", receive_sm);
	free(data_receive_sm.buf);
	execute_system("/etc/led_control.sh radiomon_off");
	return 0;
}

