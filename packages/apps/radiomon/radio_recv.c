
#include <getopt.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <float.h>
#include <assert.h>
#include <signal.h>
#include <sys/time.h>

#include "radio_mon.h"
#include "fsk.h"
#include "simpleaudio.h"
#include "databits.h"

#define N_CHANNELS 	1
#define SAMPLE_RATE 	44100
#define NOISE_FACTOR 	0.0

int tx_leader_bits_len = 2;

static int rx_stop = 0;

void rx_stop_sighandler(int sig)
{
    rx_stop = 1;
}

static void report_no_carrier( fsk_plan *fskp,
	unsigned int sample_rate,
	float bfsk_data_rate,
	float frame_n_bits,
	unsigned int nframes_decoded,
	size_t carrier_nsamples,
	float confidence_total,
	float amplitude_total )
{
	execute_system("/etc/led_control.sh STA_done");
	float nbits_decoded = nframes_decoded * frame_n_bits;
	float throughput_rate =
		nbits_decoded * sample_rate / (float)carrier_nsamples;

	debug_log1("nframes_decoded=%u\n", nframes_decoded);
	debug_log1("nbits_decoded=%f\n", nbits_decoded);
	debug_log1("carrier_nsamples=%lu\n", carrier_nsamples);
	fprintf(stderr, "\n### NOCARRIER ndata=%u confidence=%.3f ampl=%.3f bps=%.2f",
			nframes_decoded,
			confidence_total / nframes_decoded,
			amplitude_total / nframes_decoded,
			throughput_rate);
	debug_log(" bits*sr=%llu rate*nsamp=%llu",
			(unsigned long long)(nbits_decoded * sample_rate + 0.5),
			(unsigned long long)(bfsk_data_rate * carrier_nsamples) );

	if ( (unsigned long long)(nbits_decoded * sample_rate + 0.5) == (unsigned long long)(bfsk_data_rate * carrier_nsamples) ) {
		fprintf(stderr, " (rate perfect) ###\n");
	} else {
		float throughput_skew = (throughput_rate - bfsk_data_rate)
			/ bfsk_data_rate;
		fprintf(stderr, " (%.1f%% %s) ###\n",
				fabs(throughput_skew) * 100.0,
				signbit(throughput_skew) ? "slow" : "fast"
		       );
	}
}

int radio_receive(char *program_name, callback ptr_func)
{
	char *stream_name = "input audio";
	int quiet_mode = 0;
	int output_print_filter = 0;
	float band_width = 0;
	unsigned int bfsk_mark_f = 0;
	unsigned int bfsk_space_f = 0;
	unsigned int bfsk_inverted_freqs = 0;
	int bfsk_nstartbits = -1;
	float bfsk_nstopbits = -1;
	unsigned int bfsk_do_rx_sync = 0;
	unsigned int bfsk_sync_byte = -1;
	unsigned int bfsk_n_data_bits = 0;
	int autodetect_shift;

	float	carrier_autodetect_threshold = 0.0;

	// fsk_confidence_threshold : signal-to-noise squelch control
	//
	// The minimum SNR-ish confidence level seen as "a signal".
	float fsk_confidence_threshold = 1.5;

	// fsk_confidence_search_limit : performance vs. quality
	//
	// If we find a frame with confidence > confidence_search_limit,
	// quit searching for a better frame.  confidence_search_limit has a
	// dramatic effect on peformance (high value yields low performance, but
	// higher decode quality, for noisy or hard-to-discern signals (Bell 103,
	// or skewed rates).
	float fsk_confidence_search_limit = 2.3f;
	// float fsk_confidence_search_limit = INFINITY;  /* for test */

	simpleaudio *sa;
	sa_backend_t sa_backend = SA_BACKEND_SYSDEFAULT;
	char *sa_backend_device = NULL; 
	sa_format_t sample_format = SA_SAMPLE_FORMAT_S16;
	unsigned int sample_rate = SAMPLE_RATE;
	unsigned int nchannels = N_CHANNELS; // FIXME: only works with one channel

	unsigned int rx_one = 0;
	float rxnoise_factor = 0.0;

	int output_mode_binary = 0;

	float	bfsk_data_rate = 0.0;
	databits_decoder	*bfsk_databits_decode;

	bfsk_databits_decode = databits_decode_ascii8;

	/* validate the default system audio mechanism */
#if !(USE_PULSEAUDIO || USE_ALSA)
# define _MINIMODEM_NO_SYSTEM_AUDIO
# if !USE_SNDFILE
#  error At least one of {USE_PULSEAUDIO,USE_ALSA,USE_SNDFILE} must be enabled!
# endif
#endif

	bfsk_mark_f = BFSK_MARK_FREQ;
	assert(bfsk_mark_f > 0);
	bfsk_space_f = BFSK_SPACE_FREQ;
	assert(bfsk_space_f > 0);

	bfsk_data_rate = BFSK_DATA_RATE;
	fprintf(stdout, "bfsk_data_rate = %f, bfsk_mark_f = %d, bfsk_space_f = %d\n",
			bfsk_data_rate, bfsk_mark_f, bfsk_space_f);
	/* The receive code requires floating point samples to feed to the FFT */
	sample_format = SA_SAMPLE_FORMAT_FLOAT;


	if ( bfsk_n_data_bits == 0 )
		bfsk_n_data_bits = 8;


	if ( output_mode_binary )
		bfsk_databits_decode = databits_decode_binary;

	if ( bfsk_data_rate >= 400 ) {
		/*
		 * Bell 202:     baud=1200 mark=1200 space=2200
		 */
		autodetect_shift = - ( bfsk_data_rate * 5 / 6 );
		if ( bfsk_mark_f == 0 )
			bfsk_mark_f  = bfsk_data_rate / 2 + 600;
		if ( bfsk_space_f == 0 )
			bfsk_space_f = bfsk_mark_f - autodetect_shift;
		if ( band_width == 0 )
			band_width = 200;
	} else if ( bfsk_data_rate >= 100 ) {
		/*
		 * Bell 103:     baud=300 mark=1270 space=1070
		 * ITU-T V.21:   baud=300 mark=1280 space=1080
		 */
		autodetect_shift = 200;
		if ( bfsk_mark_f == 0 )
			bfsk_mark_f  = 1270;
		if ( bfsk_space_f == 0 )
			bfsk_space_f = bfsk_mark_f - autodetect_shift;
		if ( band_width == 0 )
			band_width = 50;	// close enough
	} else {
		/*
		 * RTTY:     baud=45.45 mark/space=variable shift=-170
		 */
		autodetect_shift = 170;
		if ( bfsk_mark_f == 0 )
			bfsk_mark_f  = 1585;
		if ( bfsk_space_f == 0 )
			bfsk_space_f = bfsk_mark_f - autodetect_shift;
		if ( band_width == 0 ) {
			band_width = 10;	// FIXME chosen arbitrarily
		}
	}

	// defaults: 1 start bit, 1 stop bit
	if ( bfsk_nstartbits < 0 )
		bfsk_nstartbits = 1;
	if ( bfsk_nstopbits < 0 )
		bfsk_nstopbits = 1.0;

	// do not transmit any leader tone if no start bits
	if ( bfsk_nstartbits == 0 )
		tx_leader_bits_len = 0;

	if (bfsk_inverted_freqs) {
		float t = bfsk_mark_f;
		bfsk_mark_f = bfsk_space_f;
		bfsk_space_f = t;
	}

	/* restrict band_width to <= data rate (FIXME?) */
	if ( band_width > bfsk_data_rate )
		band_width = bfsk_data_rate;

	// sanitize confidence search limit
	if ( fsk_confidence_search_limit < fsk_confidence_threshold )
		fsk_confidence_search_limit = fsk_confidence_threshold;



	fprintf(stdout, "bfsk_n_data_bits = %d\n", bfsk_n_data_bits);
	/*
	 * Open the input audio stream
	 */
	sa = simpleaudio_open_stream(sa_backend, sa_backend_device,
			SA_STREAM_RECORD,
			sample_format, sample_rate, nchannels,
			program_name, stream_name);
	if ( ! sa )
		return 1;

	sample_rate = simpleaudio_get_rate(sa);

	if ( rxnoise_factor != 0.0 )
		simpleaudio_set_rxnoise(sa, rxnoise_factor);

	/*
	 * Prepare the input sample chunk rate
	 */
	float nsamples_per_bit = sample_rate / bfsk_data_rate;

	debug_log1("nsamples_per_bit = %f, \n", nsamples_per_bit);


	/*
	 * Prepare the fsk plan
	 */

	fsk_plan *fskp;
	fskp = fsk_plan_new(sample_rate, bfsk_mark_f, bfsk_space_f, band_width);
	if ( !fskp ) {
		fprintf(stderr, "fsk_plan_new() failed\n");
		return 1;
	}

	/*
	 * Prepare the input sample buffer.  For 8-bit frames with prev/start/stop
	 * we need 11 data-bits worth of samples, and we will scan through one bits
	 * worth at a time, hence we need a minimum total input buffer size of 12
	 * data-bits.  */
	unsigned int nbits = 0;
	nbits += 1;			// prev stop bit (last whole stop bit)
	nbits += bfsk_nstartbits;	// start bits
	nbits += bfsk_n_data_bits;
	nbits += 1;			// stop bit (first whole stop bit)
	debug_log1("nbits (should be 11 for 8 bits) = %d\n", nbits);

	// FIXME EXPLAIN +1 goes with extra bit when scanning
	size_t	samplebuf_size = ceilf(nsamples_per_bit) * (nbits+1);
	samplebuf_size *= 2; // account for the half-buf filling method
#define SAMPLE_BUF_DIVISOR 12
#ifdef SAMPLE_BUF_DIVISOR
	// For performance, use a larger samplebuf_size than necessary
	if ( samplebuf_size < sample_rate / SAMPLE_BUF_DIVISOR )
		samplebuf_size = sample_rate / SAMPLE_BUF_DIVISOR;
#endif
	float	*samplebuf = malloc(samplebuf_size * sizeof(float));
	size_t	samples_nvalid = 0;
	debug_log1("samplebuf_size=%zu\n", samplebuf_size);

	/*
	 * Run the main loop
	 */

	int			ret = 0;

	int			carrier = 0;
	float		confidence_total = 0;
	float		amplitude_total = 0;
	unsigned int	nframes_decoded = 0;
	size_t		carrier_nsamples = 0;

	unsigned int	noconfidence = 0;
	unsigned int	advance = 0;

	// Fraction of nsamples_per_bit that we will "overscan"; range (0.0 .. 1.0)
	float fsk_frame_overscan = 0.5;
	//   should be != 0.0 (only the nyquist edge cases actually require this?)
	// for handling of slightly faster-than-us rates:
	//   should be >> 0.0 to allow us to lag back for faster-than-us rates
	//   should be << 1.0 or we may lag backwards over whole bits
	// for optimal analysis:
	//   should be >= 0.5 (half a bit width) or we may not find the optimal bit
	//   should be <  1.0 (a full bit width) or we may skip over whole bits
	// for encodings without start/stop bits:
	//     MUST be <= 0.5 or we may accidentally skip a bit
	//
	assert( fsk_frame_overscan >= 0.0 && fsk_frame_overscan < 1.0 );

	// ensure that we overscan at least a single sample
	unsigned int nsamples_overscan
		= nsamples_per_bit * fsk_frame_overscan + 0.5;
	if ( fsk_frame_overscan > 0.0 && nsamples_overscan == 0 )
		nsamples_overscan = 1;
	debug_log1("fsk_frame_overscan=%f nsamples_overscan=%u\n",
			fsk_frame_overscan, nsamples_overscan);

	// n databits plus bfsk_startbit start bits plus bfsk_nstopbit stop bits:
	float frame_n_bits = bfsk_n_data_bits + bfsk_nstartbits + bfsk_nstopbits;
	unsigned int frame_nsamples = nsamples_per_bit * frame_n_bits + 0.5;

	float track_amplitude = 0.0;
	float peak_confidence = 0.0;
	char str[4096];
	unsigned int idx_str = 0;

	signal(SIGINT, rx_stop_sighandler);
	printf("----------------------------\n\n");

	while ( 1 ) {

		if ( rx_stop )
			break;

		debug_log1("advance=%u, samplebuf_size = %u\n", advance, samplebuf_size);

		/* Shift the samples in samplebuf by 'advance' samples */
		assert( advance <= samplebuf_size );
		if ( advance == samplebuf_size ) {
			samples_nvalid = 0;
			advance = 0;
		}
		if ( advance ) {
			if ( advance > samples_nvalid )
				break;
			memmove(samplebuf, samplebuf+advance,
					(samplebuf_size-advance)*sizeof(float));
			samples_nvalid -= advance;
		}

		if ( samples_nvalid < samplebuf_size/2 ) {
			float	*samples_readptr = samplebuf + samples_nvalid;
			size_t	read_nsamples = samplebuf_size/2;
			/* Read more samples into samplebuf (fill it) */
			assert ( read_nsamples > 0 );
			assert ( samples_nvalid + read_nsamples <= samplebuf_size );
			ssize_t r;
			r = simpleaudio_read(sa, samples_readptr, read_nsamples);
			debug_log1("simpleaudio_read(samplebuf+%zd, n=%zu) returns %zd\n",
					samples_readptr - samplebuf, read_nsamples, r);
			if ( r < 0 ) {
				fprintf(stderr, "simpleaudio_read: error\n");
				ret = -1;
				break;
			}
			samples_nvalid += r;
		}

		if ( samples_nvalid == 0 )
			break;

		/* Auto-detect carrier frequency */
		static int carrier_band = -1;
		if ( carrier_autodetect_threshold > 0.0 && carrier_band < 0 ) {
			unsigned int i;
			float nsamples_per_scan = nsamples_per_bit;
			if ( nsamples_per_scan > fskp->fftsize )
				nsamples_per_scan = fskp->fftsize;
			for ( i=0; i+nsamples_per_scan<=samples_nvalid;
					i+=nsamples_per_scan ) {
				carrier_band = fsk_detect_carrier(fskp,
						samplebuf+i, nsamples_per_scan,
						carrier_autodetect_threshold);
				if ( carrier_band >= 0 )
					break;
			}
			advance = i + nsamples_per_scan;
			if ( advance > samples_nvalid )
				advance = samples_nvalid;
			if ( carrier_band < 0 ) {
				debug_log("autodetected carrier band not found\n");
				continue;
			}

			// default negative shift -- reasonable?
			int b_shift = - (float)(autodetect_shift + fskp->band_width/2.0)
				/ fskp->band_width;
			if ( bfsk_inverted_freqs )
				b_shift *= -1;
			/* only accept a carrier as b_mark if it will not result
			 * in a b_space band which is "too low". */
			int b_space = carrier_band + b_shift;
			if ( b_space < 1 || b_space >= fskp->nbands ) {
				debug_log("autodetected space band out of range\n" );
				carrier_band = -1;
				continue;
			}

			debug_log("### TONE freq=%.1f ###\n",
					carrier_band * fskp->band_width);

			fsk_set_tones_by_bandshift(fskp, /*b_mark*/carrier_band, b_shift);
		}

		/*
		 * The main processing algorithm: scan samplesbuf for FSK frames,
		 * looking at an entire frame at once.
		 */

		// example expect_bits_string
		//	  0123456789A
		//	  isddddddddp	i == idle bit (a.k.a. prev_stop bit)
		//			s == start bit  d == data bits  p == stop bit
		// ebs = "10dddddddd1"  <-- expected mark/space framing pattern
		//
		// NOTE! expect_n_bits ends up being (frame_n_bits+1), because
		// we expect the prev_stop bit in addition to this frame's own
		// (start + n_data_bits + stop) bits.  But for each decoded frame,
		// we will advance just frame_n_bits worth of samples, leaving us
		// pointing at our stop bit -- it becomes the next frame's prev_stop.
		//
		//                  prev_stop--v
		//                       start--v        v--stop
		// char *expect_bits_string = "10dddddddd1";
		//
		char expect_bits_string[32];
		int j = 0;
		if ( bfsk_nstopbits != 0.0 )
			expect_bits_string[j++] = '1';
		int i;
		// Nb. only integer number of start bits works (for rx)
		for ( i=0; i<bfsk_nstartbits; i++ )
			expect_bits_string[j++] = '0';
		for ( i=0; i<bfsk_n_data_bits; i++,j++ ) {
			if ( ! carrier && bfsk_do_rx_sync )
				expect_bits_string[j] = ( (bfsk_sync_byte>>i)&1 ) + '0';
			else
				expect_bits_string[j] = 'd';
		}
		if ( bfsk_nstopbits != 0.0 )
			expect_bits_string[j++] = '1';
		expect_bits_string[j++] = 0;


		unsigned int expect_n_bits  = strlen(expect_bits_string);
		unsigned int expect_nsamples = nsamples_per_bit * expect_n_bits;

		// fprintf(stderr, "ebs = '%s' (%lu)  ;  expect_nsamples=%u samples_nvalid=%lu\n", expect_bits_string, strlen(expect_bits_string), expect_nsamples, samples_nvalid);

		if ( samples_nvalid < expect_nsamples )
			break;

		// try_max_nsamples
		// serves two purposes
		// 1. avoids finding a non-optimal first frame
		// 2. allows us to track slightly slow signals
		unsigned int try_max_nsamples;
		if ( carrier )
			try_max_nsamples = nsamples_per_bit * 0.75 + 0.5;
		else
			try_max_nsamples = nsamples_per_bit;
		try_max_nsamples += nsamples_overscan;

		// FSK_ANALYZE_NSTEPS Try 3 frame positions across the try_max_nsamples
		// range.  Using a larger nsteps allows for more accurate tracking of
		// fast/slow signals (at decreased performance).  Note also
		// FSK_ANALYZE_NSTEPS_FINE below, which refines the frame
		// position upon first acquiring carrier, or if confidence falls.
#define FSK_ANALYZE_NSTEPS		3
		unsigned int try_step_nsamples = try_max_nsamples / FSK_ANALYZE_NSTEPS;
		if ( try_step_nsamples == 0 )
			try_step_nsamples = 1;

		float confidence, amplitude;
		unsigned int bits = 0;
		/* Note: frame_start_sample is actually the sample where the
		 * prev_stop bit begins (since the "frame" includes the prev_stop). */
		unsigned int frame_start_sample = 0;

		unsigned int try_first_sample;
		float try_confidence_search_limit;

		try_confidence_search_limit = fsk_confidence_search_limit;
		try_first_sample = carrier ? nsamples_overscan : 0;

		confidence = fsk_find_frame(fskp, samplebuf, expect_nsamples,
				try_first_sample,
				try_max_nsamples,
				try_step_nsamples,
				try_confidence_search_limit,
				expect_bits_string,
				&bits,
				&amplitude,
				&frame_start_sample
				);

		int do_refine_frame = 0;

		if ( confidence < peak_confidence * 0.75 ) {
			do_refine_frame = 1;
			debug_log1(" ... do_refine_frame rescan (confidence %.3f << %.3f peak)\n", confidence, peak_confidence);
			peak_confidence = 0;
		}

		// no-confidence if amplitude drops abruptly to < 25% of the
		// track_amplitude, which follows amplitude with hysteresis
		if ( amplitude < track_amplitude * 0.25 ) {
			confidence = 0;
		}

#define FSK_MAX_NOCONFIDENCE_BITS	20

		if ( confidence <= fsk_confidence_threshold ) {

			// FIXME: explain
			if ( ++noconfidence > FSK_MAX_NOCONFIDENCE_BITS )
			{
				carrier_band = -1;
				if ( carrier ) {
					if (!quiet_mode)
						report_no_carrier(fskp, sample_rate, bfsk_data_rate,
								frame_n_bits, nframes_decoded,
								carrier_nsamples, confidence_total, amplitude_total);
					carrier = 0;
					carrier_nsamples = 0;
					confidence_total = 0;
					amplitude_total = 0;
					nframes_decoded = 0;
					track_amplitude = 0.0;

					char dataoutbuf[1];
					dataoutbuf[0] = 0x03;
					ptr_func(dataoutbuf, 1);

					if ( rx_one )
						break;
				}
			}

			/* Advance the sample stream forward by try_max_nsamples so the
			 * next time around the loop we continue searching from where
			 * we left off this time.		*/
			advance = try_max_nsamples;
			debug_log1("@ NOCONFIDENCE=%u advance=%u\n", noconfidence, advance);
			continue;
		}

		// Add a frame's worth of samples to the sample count
		carrier_nsamples += frame_nsamples;

		if ( carrier ) {
			system("/etc/led_control.sh STA_receive");
			// If we already had carrier, adjust sample count +start -overscan
			carrier_nsamples += frame_start_sample;
			carrier_nsamples -= nsamples_overscan;

		} else {

			// We just acquired carrier.

			if ( !quiet_mode ) {
				if ( bfsk_data_rate >= 100 )
					fprintf(stderr, "### CARRIER %u @ %.1f Hz ",
							(unsigned int)(bfsk_data_rate + 0.5),
							fskp->b_mark * fskp->band_width);
				else
					fprintf(stderr, "### CARRIER %.2f @ %.1f Hz ",
							bfsk_data_rate,
							fskp->b_mark * fskp->band_width);
			}

			if ( !quiet_mode )
				fprintf(stderr, "###\n");

			carrier = 1;
			bfsk_databits_decode(0, 0, 0, 0); // reset the frame processor

			do_refine_frame = 1;
			debug_log1(" ... do_refine_frame rescan (acquired carrier)\n");
		}

		if (do_refine_frame)
		{
			if ( confidence < INFINITY && try_step_nsamples > 1 ) {
				// FSK_ANALYZE_NSTEPS_FINE:
				// Scan again, but try harder to find the best frame.
				// Since we found a valid confidence frame in the "sloppy"
				// fsk_find_frame() call already, we're sure to find one at
				// least as good this time.
#define FSK_ANALYZE_NSTEPS_FINE		8
				try_step_nsamples = try_max_nsamples / FSK_ANALYZE_NSTEPS_FINE;
				if ( try_step_nsamples == 0 )
					try_step_nsamples = 1;
				try_confidence_search_limit = INFINITY;
				float confidence2, amplitude2;
				unsigned int bits2;
				unsigned int frame_start_sample2;
				confidence2 = fsk_find_frame(fskp, samplebuf, expect_nsamples,
						try_first_sample,
						try_max_nsamples,
						try_step_nsamples,
						try_confidence_search_limit,
						expect_bits_string,
						&bits2,
						&amplitude2,
						&frame_start_sample2
						);
				if ( confidence2 > confidence ) {
					bits = bits2;
					amplitude = amplitude2;
					frame_start_sample = frame_start_sample2;
				}
			}
		}

		track_amplitude = ( track_amplitude + amplitude ) / 2;
		if ( peak_confidence < confidence )
			peak_confidence = confidence;
		debug_log1("@ confidence=%.3f peak_conf=%.3f amplitude=%.3f track_amplitude=%.3f\n",
				confidence, peak_confidence, amplitude, track_amplitude );

		confidence_total += confidence;
		amplitude_total += amplitude;
		nframes_decoded++;
		noconfidence = 0;

		// Advance the sample stream forward past the junk before the
		// frame starts (frame_start_sample), and then past decoded frame
		// (see also NOTE about frame_n_bits and expect_n_bits)...
		// But actually advance just a bit less than that to allow
		// for tracking slightly fast signals, hence - nsamples_overscan.
		advance = frame_start_sample + frame_nsamples - nsamples_overscan;

		debug_log1("@ nsamples_per_bit=%.3f n_data_bits=%u "
				" frame_start=%u advance=%u\n",
				nsamples_per_bit, bfsk_n_data_bits,
				frame_start_sample, advance);

		// chop off the prev_stop bit
		if ( bfsk_nstopbits != 0.0 )
			bits = bits >> 1;


		/*
		 * Send the raw data frame bits to the backend frame processor
		 * for final conversion to output data bytes.
		 */

		// chop off framing bits
		unsigned int frame_bits_shift = bfsk_nstartbits;
		unsigned int frame_bits_mask = (int)(1 << bfsk_n_data_bits) - 1;
		bits = ( bits >> frame_bits_shift ) & frame_bits_mask;

		unsigned int dataout_size = 4096;
		char dataoutbuf[4096];
		unsigned int dataout_nbytes = 0;

		// suppress printing of bfsk_sync_byte bytes
		if ( bfsk_do_rx_sync ) {
			if ( dataout_nbytes == 0 && bits == bfsk_sync_byte )
				continue;
		}

		dataout_nbytes += bfsk_databits_decode(dataoutbuf + dataout_nbytes,
				dataout_size - dataout_nbytes,
				bits, (int)bfsk_n_data_bits);

		if ( dataout_nbytes == 0 )
			continue;

		/*
		 * Print the output buffer to stdout
		 */
		if ( output_print_filter == 0 ) {
			if (dataout_nbytes > 0) {
				debug_log1("dataoutbuf[0] = %d (0x%x), %c\n", dataoutbuf[0], dataoutbuf[0], dataoutbuf[0]);
				if (ptr_func(dataoutbuf, dataout_nbytes) == 0) {
					rx_stop = 1;
				}
			}
			if ( write(1, dataoutbuf, dataout_nbytes) < 0 )
				perror("write");
		} else {
			char *p = dataoutbuf;
			for ( ; dataout_nbytes; p++, dataout_nbytes-- ) {
				char printable_char = isprint(*p)||isspace(*p) ? *p : '.';
				if ( write(1, &printable_char, 1) < 0 )
					perror("write");
			}
		}

	} /* end of the main loop */

	signal(SIGINT, SIG_DFL);

	if ( carrier ) {
		if ( !quiet_mode )
			report_no_carrier(fskp, sample_rate, bfsk_data_rate,
					frame_n_bits, nframes_decoded,
					carrier_nsamples, confidence_total, amplitude_total);
	}

	simpleaudio_close(sa);

	fsk_plan_destroy(fskp);

	return ret;
}
