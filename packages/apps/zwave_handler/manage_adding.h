#ifndef _MANAGE_ADDING_H_
#define _MANAGE_ADDING_H_

#include "zw_app_utils.h"

typedef struct _adding_actions_list
{
    zwave_command_response_t response;
    struct VR_list_head list;
}adding_actions_list_t;

typedef struct _zwave_adding_process_queue
{
    char deviceId[SIZE_32B];
    adding_actions_list_t actionsList;
    pthread_mutex_t actionsListMutex;
    
    struct VR_list_head list;
}zwave_adding_process_queue_t;

typedef struct _adding_process_info_
{
	char *deviceId;
	char *serialId;
}adding_process_info;

void init_dev_adding_actions_list();
void adding_actions_devices_prepare(adding_process_info *info);
void remove_dev_adding_actions(char *deviceId, json_object *jobj);
void actions_process(char *deviceId, int sync, json_object *addingObj);

#endif