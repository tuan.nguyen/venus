#ifndef _MANU_PRODUCT_H_
#define _MANU_PRODUCT_H_

#include "zw_serialapi/zw_classcmd.h"
#include "zw_api.h"
#include "VR_define.h"

typedef struct _class_command
{
    char cmd_name[128];
    uint8_t cmd_id;
} class_command;

typedef struct _struct_point
{
    char struct_name[128];
    uint8_t class_id;
    int size;
    class_command *class_cmd;
} struct_point;


class_command BASIC[]=
{
    {ST_SET,                             BASIC_SET},
    {ST_GET,                             BASIC_GET},
};

class_command BATTERY[]=
{
    {ST_VER,                             BATTERY_VERSION},
    {ST_GET,                             BATTERY_GET},
    {ST_REPORT,                          BATTERY_REPORT},
};

class_command SENSOR_MULTILEVEL[]=
{
    {ST_VER,                             SENSOR_MULTILEVEL_VERSION},
    {ST_GET,                             SENSOR_MULTILEVEL_GET},
    {ST_REPORT,                          SENSOR_MULTILEVEL_REPORT},
    {ST_SUPPORTED_GET_SENSOR,            SENSOR_MULTILEVEL_SUPPORTED_GET_SENSOR_V5},
    {ST_SUPPORTED_GET_SCALE,             SENSOR_MULTILEVEL_SUPPORTED_GET_SCALE_V5},
};

class_command SENSOR_ALARM[]=
{
    {ST_GET,                             SENSOR_ALARM_GET},
    {ST_SUPPORTED_GET,                   SENSOR_ALARM_SUPPORTED_GET},
};

class_command SENSOR_BINARY[]=
{
    {ST_GET,                             SENSOR_BINARY_GET},
    {ST_SUPPORTED_GET_SENSOR,            SENSOR_BINARY_SUPPORTED_GET_SENSOR_V2},
};

class_command SWITCH_ALL[]=
{
    {ST_SET,                             SWITCH_ALL_SET},
    {ST_ON,                              SWITCH_ALL_ON},
    {ST_OFF,                             SWITCH_ALL_OFF},
    {ST_GET,                             SWITCH_ALL_GET},
};

class_command SIMPLE_AV_CONTROL[]=
{
    {ST_SET,                             SIMPLE_AV_CONTROL_SET},
    {ST_CHANGE_CHANNEL,                  SIMPLE_AV_CONTROL_SET},
    {ST_GET,                             SIMPLE_AV_CONTROL_GET},
    {ST_SUPPORTED_GET,                   SIMPLE_AV_CONTROL_SUPPORTED_GET},
};

class_command SWITCH_BINARY[]=
{
    {ST_GET,                             SWITCH_BINARY_GET},
    {ST_SET,                             SWITCH_BINARY_SET},

};

class_command SWITCH_MULTILEVEL[]=
{
    {ST_SET,                             SWITCH_MULTILEVEL_SET},
    {ST_GET,                             SWITCH_MULTILEVEL_GET},
    {ST_SUPPORTED_GET,                   SWITCH_MULTILEVEL_SUPPORTED_GET_V3},
    {ST_STOP_LEVEL_CHANGE,               SWITCH_MULTILEVEL_STOP_LEVEL_CHANGE},
    {ST_START_LEVEL_CHANGE,              SWITCH_MULTILEVEL_START_LEVEL_CHANGE},

};


class_command SWITCH_COLOR[]=
{
    {ST_SUPPORTED_GET,                   SWITCH_COLOR_SUPPORTED_GET},
    {ST_GET,                             SWITCH_COLOR_GET},
    {ST_SET,                             SWITCH_COLOR_SET},
    {ST_START_LEVEL_CHANGE,              SWITCH_COLOR_START_LEVEL_CHANGE},
    {ST_STOP_LEVEL_CHANGE,               SWITCH_COLOR_STOP_LEVEL_CHANGE},

};

class_command MULTI_CHANNEL[]=
{
    {ST_END_POINT_FIND,                  MULTI_CHANNEL_END_POINT_FIND_V2},
    {ST_END_POINT_GET,                   MULTI_CHANNEL_END_POINT_GET_V2},
    {ST_CAPABILITY_GET,                  MULTI_CHANNEL_CAPABILITY_GET_V2},
    {ST_AGGREGATED_MEMBERS_GET,          MULTI_CHANNEL_AGGREGATED_MEMBERS_GET_V4},
};

class_command MULTI_CHANNEL_ASSOCIATION[]=
{
    {ST_SET,                             MULTI_CHANNEL_ASSOCIATION_SET_V2},
    {ST_REMOVE,                          MULTI_CHANNEL_ASSOCIATION_REMOVE_V2},
    {ST_GET,                             MULTI_CHANNEL_ASSOCIATION_GET_V2},
    {ST_GROUPINGS_GET,                   MULTI_CHANNEL_ASSOCIATION_GROUPINGS_GET_V2},
    
};

class_command METER[]=
{
    {ST_VER,                             METER_VERSION},
    {ST_GET,                             METER_GET},
    {ST_REPORT,                          METER_REPORT},
    {ST_SUPPORTED_GET,                   METER_SUPPORTED_GET_V2},
    {ST_RESET,                           METER_RESET_V2},
};


class_command MANUFACTURER_SPECIFIC[]=
{
    {ST_DEVICE_SPECIFIC_GET,             DEVICE_SPECIFIC_GET_V2},
    {ST_GET,                             MANUFACTURER_SPECIFIC_GET},
};


class_command ASSOCIATION[]={
    {ST_VER,                             ASSOCIATION_VERSION},
    {ST_GET,                             ASSOCIATION_GET},
    {ST_GROUPINGS_GET,                   ASSOCIATION_GROUPINGS_GET},
    {ST_SPECIFIC_GROUP_GET,              ASSOCIATION_SPECIFIC_GROUP_GET_V2},
    {ST_GROUPINGS_REPORT,                ASSOCIATION_GROUPINGS_REPORT},
    {ST_REMOVE,                          ASSOCIATION_REMOVE},
    {ST_SET,                             ASSOCIATION_SET},
    {ST_REPORT,                          ASSOCIATION_REPORT},
};

class_command ASSOCIATION_GRP_INFO[]={
    {ST_GROUP_NAME_GET,                  ASSOCIATION_GROUP_NAME_GET},
    {ST_GROUP_INFO_GET,                  ASSOCIATION_GROUP_INFO_GET},
    {ST_GROUP_COMMAND_LIST_GET,          ASSOCIATION_GROUP_COMMAND_LIST_GET},

};

class_command ZWAVEPLUS_INFO[]={
    {ST_GET,                             ZWAVEPLUS_INFO_GET},

};

class_command CONFIGURATION[]=
{
    {ST_VER,                             CONFIGURATION_VERSION},
    {ST_GET,                             CONFIGURATION_GET},
    {ST_SET,                             CONFIGURATION_SET},
    {ST_REPORT,                          CONFIGURATION_REPORT},
    {ST_BULK_SET,                        CONFIGURATION_BULK_SET_V2},
    {ST_BULK_GET,                        CONFIGURATION_BULK_GET_V2},
    {ST_NAME_GET,                        CONFIGURATION_NAME_GET_V4},
    {ST_INFO_GET,                        CONFIGURATION_INFO_GET_V4},
    {ST_PROPERTIES_GET,                  CONFIGURATION_PROPERTIES_GET_V4},
};

class_command DOOR_LOCK[]=
{
    {ST_VER,                             DOOR_LOCK_VERSION},
    {ST_CONFIGURATION_GET,               DOOR_LOCK_CONFIGURATION_GET},
    {ST_CONFIGURATION_SET,               DOOR_LOCK_CONFIGURATION_SET},
    {ST_GET,                             DOOR_LOCK_OPERATION_GET},
    {ST_SET,                             DOOR_LOCK_OPERATION_SET},
};

class_command USER_CODE[]=
{
    {ST_VER,                             USER_CODE_VERSION},
    {ST_GET,                             USER_CODE_GET},
    {ST_SET,                             USER_CODE_SET},
    {ST_USERS_NUMBER_GET,                USERS_NUMBER_GET},
};


class_command THERMOSTAT_SETPOINT[]=
{
    {ST_GET,                             THERMOSTAT_SETPOINT_GET},
    {ST_SET,                             THERMOSTAT_SETPOINT_SET},
    {ST_CAPABILITIES_GET,                THERMOSTAT_SETPOINT_CAPABILITIES_GET_V3},
    {ST_SUPPORTED_GET,                   THERMOSTAT_SETPOINT_SUPPORTED_GET},
};
class_command WAKE_UP[]=
{
    {ST_INTERVAL_GET,                    WAKE_UP_INTERVAL_GET},
    {ST_INTERVAL_SET,                    WAKE_UP_INTERVAL_SET},
    {ST_INTERVAL_CAPABILITIES_GET,       WAKE_UP_INTERVAL_CAPABILITIES_GET_V2},
    {ST_NO_MORE_INFORMATION,             WAKE_UP_NO_MORE_INFORMATION},
};

class_command THERMOSTAT_MODE[]=
{
    {ST_GET,                             THERMOSTAT_MODE_GET},
    {ST_SET,                             THERMOSTAT_MODE_SET},
    {ST_SUPPORTED_GET,                   THERMOSTAT_MODE_SUPPORTED_GET},
};

class_command THERMOSTAT_OPERATING_STATE[]=
{
    {ST_LOGGING_GET,                     THERMOSTAT_OPERATING_STATE_LOGGING_GET_V2},
    {ST_GET,                             THERMOSTAT_OPERATING_STATE_GET},
    {ST_LOGGING_SUPPORTED_GET,           THERMOSTAT_OPERATING_STATE_LOGGING_SUPPORTED_GET_V2},
};

class_command THERMOSTAT_FAN_STATE[]=
{
    {ST_GET,                             THERMOSTAT_FAN_STATE_GET},
};

class_command THERMOSTAT_FAN_MODE[]=
{
    {ST_GET,                             THERMOSTAT_FAN_MODE_GET},
    {ST_SET,                             THERMOSTAT_FAN_MODE_SET},
    {ST_SUPPORTED_GET,                   THERMOSTAT_FAN_MODE_SUPPORTED_GET},

};

class_command BARRIER_OPERATOR[]=
{
    {ST_GET,                             BARRIER_OPERATOR_GET},
    {ST_SET,                             BARRIER_OPERATOR_SET},
    {ST_SIGNAL_SUPPORTED_GET,            BARRIER_OPERATOR_SIGNAL_SUPPORTED_GET},
    {ST_SIGNAL_SET,                      BARRIER_OPERATOR_SIGNAL_SET},
    {ST_SIGNAL_GET,                      BARRIER_OPERATOR_SIGNAL_GET},

};

class_command IRRIGATION[]=
{
    {ST_SYSTEM_CONFIG_SET,               IRRIGATION_SYSTEM_CONFIG_SET},
    {ST_SYSTEM_CONFIG_GET,               IRRIGATION_SYSTEM_CONFIG_GET},
    {ST_SYSTEM_INFO_GET,                 IRRIGATION_SYSTEM_INFO_GET},
    {ST_SYSTEM_STATUS_GET,               IRRIGATION_SYSTEM_STATUS_GET},
    {ST_VALVE_INFO_GET,                  IRRIGATION_VALVE_INFO_GET},
    {ST_VALVE_CONFIG_GET,                IRRIGATION_VALVE_CONFIG_GET},
    {ST_VALVE_CONFIG_SET,                IRRIGATION_VALVE_CONFIG_SET},
    {ST_VALVE_RUN,                       IRRIGATION_VALVE_RUN},
    {ST_VALVE_TABLE_SET,                 IRRIGATION_VALVE_TABLE_SET},
    {ST_VALVE_TABLE_GET,                 IRRIGATION_VALVE_TABLE_GET},
    {ST_VALVE_TABLE_RUN,                 IRRIGATION_VALVE_TABLE_RUN},
    {ST_SYSTEM_SHUTOFF,                  IRRIGATION_SYSTEM_SHUTOFF},

};

class_command WINDOW_COVERING[]=
{
    {ST_SUPPORTED_GET,                   WINDOW_COVERING_SUPPORTED_GET},
    {ST_GET,                             WINDOW_COVERING_GET},
    {ST_STOP_LEVEL_CHANGE,               WINDOW_COVERING_STOP_LEVEL_CHANGE},
    {ST_SET,                             WINDOW_COVERING_SET},
    {ST_START_LEVEL_CHANGE,              WINDOW_COVERING_START_LEVEL_CHANGE},

};

class_command VERSION[]=
{
    {ST_GET,                             VERSION_GET},
    {ST_COMMAND_CLASS_GET,               VERSION_COMMAND_CLASS_GET},
};

class_command APPLICATION_STATUS[]=
{
    {ST_BUSY,                            APPLICATION_BUSY},
    {ST_REJECT_REQUEST,                  APPLICATION_REJECTED_REQUEST},
};

class_command ALARM[]=
{
    {ST_GET,                             ALARM_GET},
    {ST_SET,                             ALARM_SET_V2},
    {ST_SUPPORTED_GET,                   ALARM_TYPE_SUPPORTED_GET_V2},
};

class_command NOTIFICATION[]=
{
    {ST_GET,                             NOTIFICATION_GET_V3},
    {ST_SET,                             NOTIFICATION_SET_V3},
    {ST_SUPPORTED_GET,                   NOTIFICATION_SUPPORTED_GET_V3},
    {ST_EVENT_SUPPORTED_GET,             EVENT_SUPPORTED_GET_V3},
};


class_command POWERLEVEL[]=
{
    {ST_GET,                             POWERLEVEL_GET},
    {ST_SET,                             POWERLEVEL_SET},
    {ST_TEST_NODE_GET,                   POWERLEVEL_TEST_NODE_GET},
    {ST_TEST_NODE_SET,                   POWERLEVEL_TEST_NODE_SET},
};

class_command PROTECTION[]=
{
    {ST_SET,                             PROTECTION_SET},
    {ST_GET,                             PROTECTION_GET},
    {ST_SUPPORTED_GET,                   PROTECTION_SUPPORTED_GET_V2},
    {ST_EC_SET,                          PROTECTION_EC_SET_V2},
    {ST_EC_GET,                          PROTECTION_EC_GET_V2},
    {ST_TIMEOUT_SET,                     PROTECTION_TIMEOUT_SET_V2},
    {ST_TIMEOUT_GET,                     PROTECTION_TIMEOUT_GET_V2},
};

class_command SCHEDULE_ENTRY_LOCK[]=
{
    {ST_ENABLE_SET,                      SCHEDULE_ENTRY_LOCK_ENABLE_SET},
    {ST_ENABLE_ALL_SET,                  SCHEDULE_ENTRY_LOCK_ENABLE_ALL_SET},
    {ST_TYPE_SUPPORTED_GET,              SCHEDULE_ENTRY_TYPE_SUPPORTED_GET},
    {ST_WEEK_DAY_SET,                    SCHEDULE_ENTRY_LOCK_WEEK_DAY_SET},
    {ST_WEEK_DAY_GET,                    SCHEDULE_ENTRY_LOCK_WEEK_DAY_GET},
    {ST_YEAR_DAY_SET,                    SCHEDULE_ENTRY_LOCK_YEAR_DAY_SET},
    {ST_YEAR_DAY_GET,                    SCHEDULE_ENTRY_LOCK_YEAR_DAY_GET},
    {ST_TIME_OFFSET_GET,                 SCHEDULE_ENTRY_LOCK_TIME_OFFSET_GET_V2},
    {ST_DAILY_REPEATING_GET,             SCHEDULE_ENTRY_LOCK_DAILY_REPEATING_GET_V3},
    {ST_TIME_OFFSET_SET,                 SCHEDULE_ENTRY_LOCK_TIME_OFFSET_SET_V2},
    {ST_DAILY_REPEATING_SET,             SCHEDULE_ENTRY_LOCK_DAILY_REPEATING_SET_V3},
};

class_command TIME_PARAMETERS[]=
{
    {ST_GET,                             TIME_PARAMETERS_GET},
    {ST_SET,                             TIME_PARAMETERS_SET},
};

class_command TIME[]=
{
    {ST_OFFSET_SET,                      TIME_OFFSET_SET_V2},
    {ST_GET,                             TIME_GET},
    {ST_DATE_GET,                        DATE_GET},
    {ST_OFFSET_GET,                      TIME_OFFSET_GET_V2},
};

class_command THERMOSTAT_SETBACK[]=
{
    {ST_GET,                             THERMOSTAT_SETBACK_GET},
    {ST_SET,                             THERMOSTAT_SETBACK_SET},
};

class_command ANTITHEFT[]={
    {ST_GET,                             ANTITHEFT_GET},
    {ST_SET,                             ANTITHEFT_SET},
};

class_command CENTRAL_SCENE[]={
    {ST_SUPPORTED_GET,                  CENTRAL_SCENE_SUPPORTED_GET},
    {ST_NOTIFICATION,                   CENTRAL_SCENE_NOTIFICATION},
};
class_command DOOR_LOCK_LOGGING[]=
{
    {ST_RECORDS_SUPPORTED_GET,           DOOR_LOCK_LOGGING_RECORDS_SUPPORTED_GET},
    {ST_RECORD_GET,                      RECORD_GET},
};

class_command MULTI_CMD[]=
{
    {ST_ENCAP,                           MULTI_CMD_ENCAP},
};

class_command LANGUAGE[]=
{
    {ST_GET,                             LANGUAGE_GET},
    {ST_SET,                             LANGUAGE_SET},
};

class_command CLOCK_V1[]=
{
    {ST_GET,                             CLOCK_GET},
    {ST_SET,                             CLOCK_SET},
};

class_command SCENE_ACTIVATION[]=
{
    {ST_SET,                             SCENE_ACTIVATION_SET},
};

class_command SCENE_ACTUATOR_CONF[]=
{
    {ST_GET,                             SCENE_ACTUATOR_CONF_GET},
    {ST_SET,                             SCENE_ACTUATOR_CONF_SET},
};

class_command INDICATOR[]=
{
    {ST_SET,                             INDICATOR_SET},
    {ST_GET,                             INDICATOR_GET},
    {ST_SUPPORTED_GET,                   INDICATOR_SUPPORTED_GET_V2},

};

class_command NODE_NAMING[]=
{
    {ST_NODE_NAME_SET,                    NODE_NAMING_NODE_NAME_SET},
    {ST_NODE_NAME_GET,                    NODE_NAMING_NODE_NAME_GET},
    {ST_NODE_LOCATION_SET,                NODE_NAMING_NODE_LOCATION_SET},
    {ST_NODE_LOCATION_GET,                NODE_NAMING_NODE_LOCATION_GET},
};

class_command MANUFACTURER_PROPRIETARY[]=
{
};

class_command FIRMWARE_UPDATE_MD[]=
{
    {ST_FIRMWARE_MD_GET,                    FIRMWARE_MD_GET},
    {ST_FIRMWARE_UPDATE_MD_REQUEST_GET,     FIRMWARE_UPDATE_MD_REQUEST_GET},
    {ST_FIRMWARE_UPDATE_MD_GET,             FIRMWARE_UPDATE_MD_GET},
    {ST_FIRMWARE_UPDATE_MD_REPORT,          FIRMWARE_UPDATE_MD_REPORT},
};

class_command SUPERVISION[]=
{
    {ST_GET,                                SUPERVISION_GET},
};

class_command SCENE_CONTROLLER_CONF[]=
{
    {ST_GET,                                SCENE_CONTROLLER_CONF_GET},
    {ST_SET,                                SCENE_CONTROLLER_CONF_SET},
};

class_command CC_REMOTE_ASSOCIATION_ACTIVATE[]=
{
    {ST_ACTIVATE,                           REMOTE_ASSOCIATION_ACTIVATE},
};

class_command REMOTE_ASSOCIATION[]=
{
    {ST_CONFIGURATION_GET,                  REMOTE_ASSOCIATION_CONFIGURATION_GET},
    {ST_CONFIGURATION_SET,                  REMOTE_ASSOCIATION_CONFIGURATION_SET},
};

class_command SENSOR_CONFIGURATION[]=
{
    {ST_TRIGGER_LEVEL_GET,                  SENSOR_TRIGGER_LEVEL_GET},
    {ST_TRIGGER_LEVEL_SET,                  SENSOR_TRIGGER_LEVEL_SET},
};
struct_point pointer_str[]=
{
    {ST_BASIC,                          COMMAND_CLASS_BASIC,                        sizeof(BASIC)/sizeof(class_command),                        BASIC},
    {ST_BATTERY,                        COMMAND_CLASS_BATTERY,                      sizeof(BATTERY)/sizeof(class_command),                      BATTERY},
    {ST_SENSOR_BINARY,                  COMMAND_CLASS_SENSOR_BINARY,                sizeof(SENSOR_BINARY)/sizeof(class_command),                SENSOR_BINARY},
    {ST_SENSOR_MULTILEVEL,              COMMAND_CLASS_SENSOR_MULTILEVEL,            sizeof(SENSOR_MULTILEVEL)/sizeof(class_command),            SENSOR_MULTILEVEL},
    {ST_METER,                          COMMAND_CLASS_METER,                        sizeof(METER)/sizeof(class_command),                        METER},
    {ST_ASSOCIATION,                    COMMAND_CLASS_ASSOCIATION,                  sizeof(ASSOCIATION)/sizeof(class_command),                  ASSOCIATION},
    {ST_CONFIGURATION,                  COMMAND_CLASS_CONFIGURATION,                sizeof(CONFIGURATION)/sizeof(class_command),                CONFIGURATION},
    {ST_DOOR_LOCK,                      COMMAND_CLASS_DOOR_LOCK,                    sizeof(DOOR_LOCK)/sizeof(class_command),                    DOOR_LOCK},
    {ST_USER_CODE,                      COMMAND_CLASS_USER_CODE,                    sizeof(USER_CODE)/sizeof(class_command),                    USER_CODE},
    {ST_SWITCH_COLOR,                   COMMAND_CLASS_SWITCH_COLOR,                 sizeof(SWITCH_COLOR)/sizeof(class_command),                 SWITCH_COLOR},
    {ST_THERMOSTAT_SETPOINT,            COMMAND_CLASS_THERMOSTAT_SETPOINT,          sizeof(THERMOSTAT_SETPOINT)/sizeof(class_command),          THERMOSTAT_SETPOINT},
    {ST_THERMOSTAT_MODE,                COMMAND_CLASS_THERMOSTAT_MODE,              sizeof(THERMOSTAT_MODE)/sizeof(class_command),              THERMOSTAT_MODE},
    {ST_THERMOSTAT_FAN_MODE,            COMMAND_CLASS_THERMOSTAT_FAN_MODE,          sizeof(THERMOSTAT_FAN_MODE)/sizeof(class_command),          THERMOSTAT_FAN_MODE},
    {ST_THERMOSTAT_OPERATING_STATE,     COMMAND_CLASS_THERMOSTAT_OPERATING_STATE,   sizeof(THERMOSTAT_OPERATING_STATE)/sizeof(class_command),   THERMOSTAT_OPERATING_STATE},
    {ST_THERMOSTAT_FAN_STATE,           COMMAND_CLASS_THERMOSTAT_FAN_STATE,         sizeof(THERMOSTAT_FAN_STATE)/sizeof(class_command),         THERMOSTAT_FAN_STATE},
    {ST_WAKE_UP,                        COMMAND_CLASS_WAKE_UP,                      sizeof(WAKE_UP)/sizeof(class_command),                      WAKE_UP},
    {ST_BARRIER_OPERATOR,               COMMAND_CLASS_BARRIER_OPERATOR,             sizeof(BARRIER_OPERATOR)/sizeof(class_command),             BARRIER_OPERATOR},
    {ST_VERSION,                        COMMAND_CLASS_VERSION,                      sizeof(VERSION)/sizeof(class_command),                      VERSION},
    {ST_APPLICATION_STATUS,             COMMAND_CLASS_APPLICATION_STATUS ,          sizeof(APPLICATION_STATUS)/sizeof(class_command),           APPLICATION_STATUS},
    {ST_ALARM,                          COMMAND_CLASS_ALARM,                        sizeof(ALARM)/sizeof(class_command),                        ALARM},
    {ST_POWER_LEVEL,                    COMMAND_CLASS_POWERLEVEL,                   sizeof(POWERLEVEL)/sizeof(class_command),                   POWERLEVEL},
    {ST_SCHEDULE_ENTRY_LOCK,            COMMAND_CLASS_SCHEDULE_ENTRY_LOCK,          sizeof(SCHEDULE_ENTRY_LOCK)/sizeof(class_command),          SCHEDULE_ENTRY_LOCK},
    {ST_TIME_PARAMETERS,                COMMAND_CLASS_TIME_PARAMETERS,              sizeof(TIME_PARAMETERS)/sizeof(class_command),              TIME_PARAMETERS},
    {ST_THERMOSTAT_SETBACK,             COMMAND_CLASS_THERMOSTAT_SETBACK,           sizeof(THERMOSTAT_SETBACK)/sizeof(class_command),           THERMOSTAT_SETBACK},
    {ST_SENSOR_ALARM,                   COMMAND_CLASS_SENSOR_ALARM,                 sizeof(SENSOR_ALARM)/sizeof(class_command),                 SENSOR_ALARM},
    {ST_SWITCH_BINARY,                  COMMAND_CLASS_SWITCH_BINARY,                sizeof(SWITCH_BINARY)/sizeof(class_command),                SWITCH_BINARY},
    {ST_ASSOCIATION_GRP_INFO,           COMMAND_CLASS_ASSOCIATION_GRP_INFO,         sizeof(ASSOCIATION_GRP_INFO)/sizeof(class_command),         ASSOCIATION_GRP_INFO},
    {ST_IRRIGATION,                     COMMAND_CLASS_IRRIGATION,                   sizeof(IRRIGATION)/sizeof(class_command),                   IRRIGATION},
    {ST_MANUFACTURER_SPECIFIC,          COMMAND_CLASS_MANUFACTURER_SPECIFIC,        sizeof(MANUFACTURER_SPECIFIC)/sizeof(class_command),        MANUFACTURER_SPECIFIC},
    {ST_MULTI_CHANNEL,                  COMMAND_CLASS_MULTI_CHANNEL_V2,             sizeof(MULTI_CHANNEL)/sizeof(class_command),                MULTI_CHANNEL},
    {ST_MULTI_CHANNEL_ASSOCIATION,      COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION_V2, sizeof(MULTI_CHANNEL_ASSOCIATION)/sizeof(class_command),    MULTI_CHANNEL_ASSOCIATION},
    {ST_SWITCH_MULTILEVEL,              COMMAND_CLASS_SWITCH_MULTILEVEL,            sizeof(SWITCH_MULTILEVEL)/sizeof(class_command),            SWITCH_MULTILEVEL},
    {ST_PROTECTION,                     COMMAND_CLASS_PROTECTION,                   sizeof(PROTECTION)/sizeof(class_command),                   PROTECTION},
    {ST_SIMPLE_AV_CONTROL,              COMMAND_CLASS_SIMPLE_AV_CONTROL,            sizeof(SIMPLE_AV_CONTROL)/sizeof(class_command),            SIMPLE_AV_CONTROL},
    {ST_TIME,                           COMMAND_CLASS_TIME,                         sizeof(TIME)/sizeof(class_command),                         TIME},
    {ST_ZWAVE_PLUS_INFO,                COMMAND_CLASS_ZWAVEPLUS_INFO,               sizeof(ZWAVEPLUS_INFO)/sizeof(class_command),               ZWAVEPLUS_INFO},
    {ST_WINDOW_COVERING,                COMMAND_CLASS_WINDOW_COVERING,              sizeof(WINDOW_COVERING)/sizeof(class_command),              WINDOW_COVERING},
    {ST_NOTIFICATION,                   COMMAND_CLASS_NOTIFICATION_V3,              sizeof(NOTIFICATION)/sizeof(class_command),                 NOTIFICATION},
    {ST_SWITCH_ALL,                     COMMAND_CLASS_SWITCH_ALL,                   sizeof(SWITCH_ALL)/sizeof(class_command),                   SWITCH_ALL},
    {ST_ANTITHEFT,                      COMMAND_CLASS_ANTITHEFT,                    sizeof(ANTITHEFT)/sizeof(class_command),                    ANTITHEFT},
    {ST_CENTRAL_SCENE,                  COMMAND_CLASS_CENTRAL_SCENE,                sizeof(CENTRAL_SCENE)/sizeof(class_command),                CENTRAL_SCENE},
    {ST_DOOR_LOCK_LOGGING,              COMMAND_CLASS_DOOR_LOCK_LOGGING,            sizeof(DOOR_LOCK_LOGGING)/sizeof(class_command),            DOOR_LOCK_LOGGING},
    {ST_MULTI_CMD,                      COMMAND_CLASS_MULTI_CMD,                    sizeof(MULTI_CMD)/sizeof(class_command),                    MULTI_CMD},
    {ST_LANGUAGE,                       COMMAND_CLASS_LANGUAGE,                     sizeof(LANGUAGE)/sizeof(class_command),                     LANGUAGE},
    {ST_CLOCK,                          COMMAND_CLASS_CLOCK,                        sizeof(CLOCK_V1)/sizeof(class_command),                     CLOCK_V1},
    {ST_SCENE_ACTIVATION,               COMMAND_CLASS_SCENE_ACTIVATION,             sizeof(SCENE_ACTIVATION)/sizeof(class_command),             SCENE_ACTIVATION},
    {ST_SCENE_ACTUATOR_CONF,            COMMAND_CLASS_SCENE_ACTUATOR_CONF,          sizeof(SCENE_ACTUATOR_CONF)/sizeof(class_command),          SCENE_ACTUATOR_CONF},
    {ST_INDICATOR,                      COMMAND_CLASS_INDICATOR,                    sizeof(INDICATOR)/sizeof(class_command),                    INDICATOR},
    {ST_NODE_NAMING,                    COMMAND_CLASS_NODE_NAMING,                  sizeof(NODE_NAMING)/sizeof(class_command),                  NODE_NAMING},
    {ST_MANUFACTURER_PROPRIETARY,       COMMAND_CLASS_MANUFACTURER_PROPRIETARY,     sizeof(MANUFACTURER_PROPRIETARY)/sizeof(class_command),     MANUFACTURER_PROPRIETARY},
    {ST_FIRMWARE_UPDATE_MD,             COMMAND_CLASS_FIRMWARE_UPDATE_MD,           sizeof(FIRMWARE_UPDATE_MD)/sizeof(class_command),           FIRMWARE_UPDATE_MD},
    {ST_SUPERVISION,                    COMMAND_CLASS_SUPERVISION,                  sizeof(SUPERVISION)/sizeof(class_command),                  SUPERVISION},
    {ST_SCENE_CONTROLLER_CONF,          COMMAND_CLASS_SCENE_CONTROLLER_CONF,        sizeof(SCENE_CONTROLLER_CONF)/sizeof(class_command),        SCENE_CONTROLLER_CONF},
    {ST_REMOTE_ASSOCIATION_ACTIVATE,    COMMAND_CLASS_REMOTE_ASSOCIATION_ACTIVATE,  sizeof(CC_REMOTE_ASSOCIATION_ACTIVATE)/sizeof(class_command),CC_REMOTE_ASSOCIATION_ACTIVATE},
    {ST_REMOTE_ASSOCIATION,             COMMAND_CLASS_REMOTE_ASSOCIATION,           sizeof(REMOTE_ASSOCIATION)/sizeof(class_command),           REMOTE_ASSOCIATION},
    {ST_SENSOR_CONFIGURATION,           COMMAND_CLASS_SENSOR_CONFIGURATION,         sizeof(SENSOR_CONFIGURATION)/sizeof(class_command),         SENSOR_CONFIGURATION},

};


#endif 
