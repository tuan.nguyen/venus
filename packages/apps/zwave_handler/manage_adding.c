#include <stdio.h>
#include <string.h>
#include "manage_adding.h"
#include "slog.h"
#include "verik_utils.h"
#include "timing.h"
#include "database.h"

pthread_mutex_t zwave_dev_adding_actions_queueMutex;
zwave_adding_process_queue_t g_dev_adding_actions;
extern int g_open_network;
extern sqlite3 *support_devs_db;
extern sqlite3 *zwave_db;
sTiming             txTiming;

void init_dev_adding_actions_list()
{
    VR_INIT_LIST_HEAD(&g_dev_adding_actions.list);
    pthread_mutex_init(&zwave_dev_adding_actions_queueMutex, 0);
}

void printf_dev_adding_actions_list(adding_actions_list_t *actionsList)
{
    int i=0;
    adding_actions_list_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &actionsList->list, list)
    {
        printf("#### dev %d ######\n", i);
        printf("method = %s\n", tmp->response.method);
        printf("nodeid = %s\n", tmp->response.nodeid);
        printf("command_class = %s\n", tmp->response.command_class);
        printf("command = %s\n", tmp->response.command);
        i++;
    }
}


void printf_dev_list()
{
    int i=0;
    zwave_adding_process_queue_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &g_dev_adding_actions.list, list)
    {
        printf("#### dev %d ######\n", i);
        printf("deviceid = %s\n", tmp->deviceId);
        i++;
    }
}

int add_new_dev_to_list(void *node_add)
{
    int res = 0;
    pthread_mutex_lock(&zwave_dev_adding_actions_queueMutex);
    zwave_adding_process_queue_t *input = (zwave_adding_process_queue_t *)node_add;
    zwave_adding_process_queue_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &(g_dev_adding_actions.list), list)
    {
        if(!strcmp(tmp->deviceId, input->deviceId))
        {
            res = 1;
            break;
        }
    }

    if(!res)
    {
        VR_(list_add_tail)(&(input->list), &(g_dev_adding_actions.list));
    }

    pthread_mutex_unlock(&zwave_dev_adding_actions_queueMutex);
    return res;
}

static void remove_all_actions(adding_actions_list_t *actionsList, pthread_mutex_t *actionsListMutex)
{
    adding_actions_list_t *tmp = NULL;
    struct VR_list_head *pos, *q;

    pthread_mutex_lock(actionsListMutex);
    VR_(list_for_each_safe)(pos, q, &actionsList->list)
    {
        tmp = VR_(list_entry)(pos, adding_actions_list_t, list);

        VR_(list_del)(pos);
        free(tmp);
    }
    pthread_mutex_unlock(actionsListMutex);
}

void remove_dev_from_list(char *deviceId)
{
    pthread_mutex_lock(&zwave_dev_adding_actions_queueMutex);
    zwave_adding_process_queue_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_dev_adding_actions.list)
    {
        tmp = VR_(list_entry)(pos, zwave_adding_process_queue_t, list);
        if(!strcmp(tmp->deviceId, deviceId))
        {
            VR_(list_del)(pos);
            remove_all_actions(&tmp->actionsList, &tmp->actionsListMutex);
            free(tmp);
        }
    }
    pthread_mutex_unlock(&zwave_dev_adding_actions_queueMutex);
}

void remove_action_from_list(adding_actions_list_t *actionsList, pthread_mutex_t *actionsListMutex, zwave_command_response_t response)
{
    adding_actions_list_t *tmp = NULL;
    struct VR_list_head *pos, *q;

    pthread_mutex_lock(actionsListMutex);
    VR_(list_for_each_safe)(pos, q, &actionsList->list)
    {
        tmp = VR_(list_entry)(pos, adding_actions_list_t, list);
        if(!strcmp(tmp->response.hash, response.hash))
        {
            SLOGI("tmp->nodeid =%s\n", tmp->response.nodeid);
            SLOGI("tmp->command_class =%s\n", tmp->response.command_class);
            SLOGI("tmp->command =%s\n", tmp->response.command);

            VR_(list_del)(pos);
            free(tmp);
        }
    }
    pthread_mutex_unlock(actionsListMutex);
}

void add_new_action_to_list(adding_actions_list_t *actionsList, pthread_mutex_t *actionsListMutex, zwave_command_response_t response)
{
    // int added = 0;
 //    adding_actions_list_t *tmp = NULL;
 //    struct VR_list_head *pos, *q;
 //    VR_(list_for_each_safe)(pos, q, &actionsList->list)
 //    {
 //        tmp = VR_(list_entry)(pos, adding_actions_list_t, list);
 //        if(!strcmp(tmp->response.hash, response.hash))
 //        {
 //            added = 1;
 //            break;
 //        }
 //    }

    // if(!added)
    // {
        adding_actions_list_t *actionNode = (adding_actions_list_t *)(malloc)(sizeof(adding_actions_list_t));
        memset(actionNode, 0x00, sizeof(adding_actions_list_t));

        memcpy(&actionNode->response, (void*)&response, sizeof(zwave_command_response_t));

        pthread_mutex_lock(actionsListMutex);
        VR_(list_add_tail)(&(actionNode->list), &(actionsList->list));
        pthread_mutex_unlock(actionsListMutex);
    // }
}

static int wait_dev_response(zwave_adding_process_queue_t *addingNode, zwave_command_response_t response, int timeAction)
{
    SLOGI("wait_dev_response device %s action %s-%s\n", response.nodeid, response.method, response.command_class);
    int res = 0;
    if(timeAction > 0)
    {
        sleep(timeAction);
    }
    else
    {
        string_to_hash((unsigned char *)&response, sizeof(zwave_command_response_t), response.hash, sizeof(response.hash));
        add_new_action_to_list(&addingNode->actionsList, &addingNode->actionsListMutex, response);
        int timercount = 4000;//4s
        while(
                (!VR_list_empty(&addingNode->actionsList.list))
                && (timercount > 0)
                )
        {
            VR_(usleep)(1000);
            timercount --;
        }

        if(timercount == 0)
        {
            SLOGI("device %s action %s-%s timeout\n", response.nodeid, response.method, response.command_class);
            res = 1;
            remove_action_from_list(&addingNode->actionsList, &addingNode->actionsListMutex, response);
        }
    }
    return res;
}

static void get_children_status(char *devId, zwave_command_response_t response)
{
    SLOGI("devId = %s\n", devId);
    if(!devId)
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(children);

    searching_database(_VR_CB_(zwave), &children, 
                    "SELECT childrenId from SUB_DEVICES where id='%s'", 
                    devId);
    if(children.len)
    {
        char *tok = NULL, *save_tok = NULL;
        tok = strtok_r(children.value, ",", &save_tok);
        while(tok)
        {
            strcpy(response.method, ST_GET_BINARY_R);
            strcpy(response.nodeid, tok);

            _get_binary_prepare(response);
            tok = strtok_r(NULL, ",", &save_tok);
        }
    }

    FREE_SEARCH_DATA_VAR(children);
}

#define IGNORE_ADDING_ACTION_TIMEOUT 2
static void adding_actions_process(char *deviceId, json_object *addingObj, int sync, json_object *actionObj)
{
    if(!addingObj || !deviceId)
    {
        return;
    }

    enum json_type type = json_object_get_type(addingObj);
    if(type != json_type_array)
    {
        return;
    }

    int i=0, res=0, ignore_timeout=IGNORE_ADDING_ACTION_TIMEOUT;
    int arraylen = json_object_array_length(addingObj);
    zwave_adding_process_queue_t *addingNode = NULL;

    if(sync) //create a dev
    {
        addingNode = (zwave_adding_process_queue_t *)(malloc)(sizeof(zwave_adding_process_queue_t));
        memset(addingNode, 0x00, sizeof(zwave_adding_process_queue_t));

        strncpy(addingNode->deviceId, deviceId, sizeof(addingNode->deviceId));
        VR_INIT_LIST_HEAD(&addingNode->actionsList.list);
        pthread_mutex_init(&addingNode->actionsListMutex, 0);

        add_new_dev_to_list(addingNode);
    }

    for (i=0; i< arraylen; i++)
    {
        int action, timeAction=0;
        json_object * jvalue;
        
        zwave_command_response_t response;
        memset(&response, 0x00, sizeof(zwave_command_response_t));

        jvalue = json_object_array_get_idx(addingObj, i);
        action = json_object_get_int(jvalue);
        if(actionObj)
        {
            char actionToDevice[SIZE_16B];
            sprintf(actionToDevice, "%04d", action);
            CHECK_JSON_OBJECT_EXIST(addingDelayObj, actionObj, actionToDevice, nextAdding);
            CHECK_JSON_OBJECT_EXIST(delayTime, addingDelayObj, ST_DELAY, nextAdding);
            int timeDelay = json_object_get_int(delayTime);
            //sleep to command affect to device.
            int delayTimeMs = 1000 * timeDelay;
            timingGetClockSystem(&txTiming);
            while(1)
            {
                if (timingGetElapsedMSec(&txTiming) > delayTimeMs)
                {
                    timingGetClockSystemStop(&txTiming);
                    SLOGI("delayTimeMs: %d ms for action: %s is timeout!", delayTimeMs, actionToDevice);
                    break;
                }
                usleep(1000);
            }
        }
nextAdding:
        switch(action)
        {
            case GET_BINARY:
                strcpy(response.method, ST_GET_BINARY_R);
                strcpy(response.nodeid, deviceId);

                _get_binary_prepare(response);
                break;

            case GET_STATUS_DIM_OFF:
                strcpy(response.method, ST_GET_BINARY_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command_class, ST_SWITCH_BINARY);

                _get_binary_prepare(response);
                break;

            case GET_CHILDREN_STATUS:
                get_children_status(deviceId, response);
                break;

            case SET_BINARY:
                strcpy(response.method, ST_SET_BINARY_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command, ST_ON_OFF);
                strcpy(response.value, ST_ON_VALUE);

                _set_binary_prepare(response);
                timeAction = 1;
                break;

            case GET_BATTERY:
                strcpy(response.method, ST_READ_SPEC_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command_class, ST_BATTERY);
                strcpy(response.command, ST_GET);

                _get_specification_prepare(response);
                break;

            case GET_TEMP:
                strcpy(response.method, ST_READ_SPEC_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command_class, ST_SENSOR_MULTILEVEL);
                strcpy(response.command, ST_GET);

                response.data0 = (char *)malloc(strlen(ST_MULTILEVEL_SENSOR_AIR_TEMPERATURE)+1);
                strcpy(response.data0, ST_MULTILEVEL_SENSOR_AIR_TEMPERATURE);

                _get_specification_prepare(response);
                break;

            case GET_HUMIDITY:
                strcpy(response.method, ST_READ_SPEC_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command_class, ST_SENSOR_MULTILEVEL);
                strcpy(response.command, ST_GET);

                response.data0 = (char *)malloc(strlen(ST_MULTILEVEL_SENSOR_HUMIDITY)+1);
                strcpy(response.data0, ST_MULTILEVEL_SENSOR_HUMIDITY);

                _get_specification_prepare(response);
                break;

            case GET_LUMINANCE:
                strcpy(response.method, ST_READ_SPEC_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command_class, ST_SENSOR_MULTILEVEL);
                strcpy(response.command, ST_GET);

                response.data0 = (char *)malloc(strlen(ST_MULTILEVEL_SENSOR_LUMINANCE)+1);
                strcpy(response.data0, ST_MULTILEVEL_SENSOR_LUMINANCE);

                _get_specification_prepare(response);
                break;

            case GET_UV:
                strcpy(response.method, ST_READ_SPEC_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command_class, ST_SENSOR_MULTILEVEL);
                strcpy(response.command, ST_GET);

                response.data0 = (char *)malloc(strlen(ST_MULTILEVEL_SENSOR_ULTRAVIOLET)+1);
                strcpy(response.data0, ST_MULTILEVEL_SENSOR_ULTRAVIOLET);

                _get_specification_prepare(response);
                break;

            case GET_METER:
                strcpy(response.method, ST_READ_SPEC_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command_class, ST_METER);
                strcpy(response.command, ST_GET);

                _get_specification_prepare(response);
                break;

            case GET_THERMOSTAT_MODE:
                strcpy(response.method, ST_READ_SPEC_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command_class, ST_THERMOSTAT_MODE);
                strcpy(response.command, ST_GET);

                _get_specification_prepare(response);
                break;

            case GET_THERMOSTAT_SET_POINT:
                strcpy(response.method, ST_READ_SPEC_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command_class, ST_THERMOSTAT_SETPOINT);
                strcpy(response.command, ST_GET);

                response.data0 = (char *)malloc(strlen(ST_COOLING)+1);
                strcpy(response.data0, ST_COOLING);

                _get_specification_prepare(response);

                response.data0 = (char *)malloc(strlen(ST_COOLING)+1);
                strcpy(response.data0, ST_HEATING);
                _get_specification_prepare(response);
                break;

            case GET_THERMOSTAT_OPERATING:
                strcpy(response.method, ST_READ_SPEC_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command_class, ST_THERMOSTAT_OPERATING_STATE);
                strcpy(response.command, ST_GET);

                _get_specification_prepare(response);
                break;

            case GET_THERMOSTAT_FAN_MODE:
                strcpy(response.method, ST_READ_SPEC_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command_class, ST_THERMOSTAT_FAN_MODE);
                strcpy(response.command, ST_GET);

                _get_specification_prepare(response);
                break;

            case GET_THERMOSTAT_FAN_OPERATING:
                strcpy(response.method, ST_READ_SPEC_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command_class, ST_THERMOSTAT_FAN_STATE);
                strcpy(response.command, ST_GET);

                _get_specification_prepare(response);
                break;

            case CLEAN_USERCODE:
                strcpy(response.method, ST_WRITE_S_SPEC_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command_class, ST_USER_CODE);
                strcpy(response.command, ST_SET);

                response.data0 = strdup(ST_OFF_VALUE);
                response.data1 = strdup(ST_OFF_VALUE);
                _set_specification_prepare(response);

                timeAction=1;
                break;                
            default:
                break;
        }

        if(sync)
        {
            res = wait_dev_response(addingNode, response, timeAction); //insert action to dev and remove it if timeout
            if(res) //timeout
            {
                /*some device does not respone by
                notify with set command so we need give a retry*/
                if(!ignore_timeout)
                {
                    break;
                }
                else
                {
                    ignore_timeout--;
                }
            }
            else
            {
                ignore_timeout = IGNORE_ADDING_ACTION_TIMEOUT;
            }
        }
    }

    if(sync)
    {
        remove_dev_from_list(addingNode->deviceId);
    }
}

void _remove_adding_actions(adding_actions_list_t *actionsList, pthread_mutex_t *actionsListMutex, json_object *jobj)
{
    if(!jobj)
    {
        return;
    }

    json_object *commandObj = NULL;
    json_object *commandClassObj = NULL;
    json_object_object_get_ex(jobj, ST_CMD_CLASS, &commandClassObj);
    json_object_object_get_ex(jobj, ST_CMD, &commandObj);

    if(!commandClassObj)
    {
        return;
    }
    // char method[SIZE_32B];
    char cmd[SIZE_32B];
    const char *command = json_object_get_string(commandObj);
    const char *commandClass = json_object_get_string(commandClassObj);

    if(!strcmp(command, ST_REPORT))
    {
        strncpy(cmd, ST_GET, sizeof(cmd)-1);
    }

    adding_actions_list_t *tmp = NULL;
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &actionsList->list)
    {
        tmp = VR_(list_entry)(pos, adding_actions_list_t, list);
        if(!strcmp(tmp->response.method, ST_GET_BINARY_R)
            || !strcmp(tmp->response.method, ST_SET_BINARY_R)
            )
        {
            if(!strcmp(commandClass, ST_BASIC)
                || !strcmp(commandClass, ST_SWITCH_ALL)
                || !strcmp(commandClass, ST_SWITCH_BINARY)
                || !strcmp(commandClass, ST_SWITCH_MULTILEVEL)
                || !strcmp(commandClass, ST_DOOR_LOCK)
                || !strcmp(commandClass, ST_BARRIER_OPERATOR)
                )
            {
                if(!strcmp(command, ST_REPORT))
                {
                    remove_action_from_list(actionsList, actionsListMutex, tmp->response);
                }
            }
        }
        else
        {
            if(!strcmp(tmp->response.command_class, commandClass)
                || !strcmp(tmp->response.command, cmd)
                )
            {
                remove_action_from_list(actionsList, actionsListMutex, tmp->response);
            }
        }
    }
}

void remove_dev_adding_actions(char *deviceId, json_object *jobj)
{
    // if(!g_open_network) //not in adding mode
    // {
    //     return;
    // }
    // SLOGI("start print dev\n");
    // printf_dev_list();
    // SLOGI("end print dev\n");

    zwave_adding_process_queue_t *tmp = NULL;
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_dev_adding_actions.list)
    {
        tmp = VR_(list_entry)(pos, zwave_adding_process_queue_t, list);
        if(!strcmp(tmp->deviceId, deviceId))
        {
            SLOGI("start _remove_adding_actions deviceId=%s\n", tmp->deviceId);
            _remove_adding_actions(&tmp->actionsList, &tmp->actionsListMutex, jobj);
        }
    }
}

void adding_actions_devices_prepare(adding_process_info *info)
{
    if(!info)
    {
        return;
    }

    if(!info->deviceId || !info->serialId)
    {
        return;
    }

    SLOGI("adding_actions_devices_prepare %s\n", info->deviceId);

    SEARCH_DATA_INIT_VAR(actions);

    searching_database("zwave_handler", support_devs_db, zwave_cb, &actions,
                    "SELECT Actions from SUPPORT_DEVS where SerialID='%s'", 
                    info->serialId);
    

    if(!actions.len)
    {
        goto addingDone;
    }

    json_object *actionsObject = VR_(create_json_object)(actions.value);
    if(!actionsObject)
    {
        goto addingDone;
    }

    json_object *addingObj = NULL;
    json_object *syncActionObj = NULL;
    json_object_object_get_ex(actionsObject, ST_ADDING, &addingObj);
    json_object_object_get_ex(actionsObject, ST_SYNC_ACTION, &syncActionObj);

    if(!addingObj)
    {
        json_object_put(actionsObject);
        goto addingDone;
    }

    if(syncActionObj)
    {
        const char *syncAction = json_object_get_string(syncActionObj);
        if(!strcmp(syncAction, ST_TRUE))
        {
            adding_actions_process(info->deviceId, addingObj, 1, actionsObject);
        }
        else
        {
            adding_actions_process(info->deviceId, addingObj, 0, actionsObject);
        }
    }
    else
    {
        adding_actions_process(info->deviceId, addingObj, 0, actionsObject);
    }

    json_object_put(actionsObject);

addingDone:
    SAFE_FREE(info->deviceId);
    SAFE_FREE(info->serialId);
    SAFE_FREE(info);
    FREE_SEARCH_DATA_VAR(actions);
}

void actions_process(char *deviceId, int sync, json_object *addingObj)
{
   adding_actions_process(deviceId, addingObj, sync, NULL);
}