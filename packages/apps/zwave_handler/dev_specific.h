#ifndef _DEVICE_SPECIFIC_H_
#define _DEVICE_SPECIFIC_H_

#include "VR_list.h"

#define ST_INVERT_STATUS "invertStatus"
#define ST_SINGLE_VALUE  "singleValue"

#define ST_USER_CODE_AUTO_SET  "userCodeAutoSet"

typedef struct _device_specific
{
	char 	*deviceId;
    char 	*serialId;
    char 	*deviceSpecific;
    timer_t timerPolling;
    struct VR_list_head list;
}device_specific_t;

void init_dev_specific_list();
void insert_specific_dev_list(char *deviceId, char *serialId);
void dev_specific_polling_actions(char *deviceId, char *pattern);
void dev_specific_replace_byte(json_object *out, char *devId, char *input, size_t input_size);
void dev_specific_temperature_color(char *devId, int kelvinValue, int *ww, int *cw);
void get_temperature_threshold(char *nodeId, int mode, char *unit, char *value, int len);
void get_specific_action_color_mode(char *devId);

uint8_t invert_status(char *devId, uint8_t value);
/*init zwave polling list for libzwave polling device*/
void dev_specific_zwave_polling_interval(char *devId, int *interval, int* cmd);
uint8_t get_maximum_user_code(char *devId);
#endif