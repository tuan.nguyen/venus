#ifndef _MANAGE_USERCODE_H_
#define _MANAGE_USERCODE_H_

#include "zw_app_utils.h"

#define REMOVE_USER_CODE_CMD 	0
#define ADD_USER_CODE_CMD 		1
#define USER_CODE_CMD_TIMEOUT 	20

#define USER_CODE_ID_DUPPLICATE 	255

#define USER_CODE_SAME_NAME 		1
#define USER_CODE_SAME_USER_ID 		2

typedef struct _user_code_info
{
    uint8_t userId;
	char *deviceId;
    char *name;

}user_code_info;

typedef struct _user_code_pending
{
	int cmd; //0:remove,1:add
	unsigned int timestampt;
    user_code_info info;

    struct VR_list_head list;
}user_code_pending_t;

typedef struct _user_code_
{
    user_code_info info;

    struct VR_list_head list;
}user_code_t;

void init_user_code_list();

// for pending usercode
int add_user_code_cmd(int cmd, char *deviceId, uint8_t userId, char *name);
char* remove_user_code_cmd(char *deviceId, uint8_t userId);
void manage_user_code_pending_thread(void *data);
void stop_user_code_manage_pending(void);

// for current usercode
void add_user_code(char *deviceId, char *userId, char *name);
void remove_user_code(char *deviceId, uint8_t userId);
void remove_all_user_code(char *deviceId);
int check_user_code_info(char *deviceId, uint8_t userId, const char *name);
uint8_t get_user_id_valid(char *deviceId, uint8_t maxUserCode);
#endif