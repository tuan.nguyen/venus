#ifndef _UNSUPPORTED_H_
#define _UNSUPPORTED_H_

#include "zw_serialapi/zw_classcmd.h"
// #include "zw_api.h"

typedef struct _type_specific
{
    uint8_t typeId;
    const char *defaultName;
}type_specific_t;

typedef struct _unSupported_device_type
{
    uint8_t deviceType;
    uint8_t specificSize;
    type_specific_t *specific;
} unSupported_device_type;

type_specific_t AV_CONTROL_POINT_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                             "AV Control Point"},
    {SPECIFIC_TYPE_DOORBELL,                             "Door Bell"},
    {SPECIFIC_TYPE_SATELLITE_RECEIVER,                   "Satellite Receiver"},
    {SPECIFIC_TYPE_SATELLITE_RECEIVER_V2,                "Satellite Receiver Version 2"},
};

type_specific_t DISPLAY_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                             "Display"},
    {SPECIFIC_TYPE_SIMPLE_DISPLAY,                       "Simple Display"},
};

type_specific_t ENTRY_CONTROL_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                             "Entry Control"},
    {SPECIFIC_TYPE_DOOR_LOCK,                            "Door Lock"},
    {SPECIFIC_TYPE_ADVANCED_DOOR_LOCK,                   "Advanced Door Lock"},
    {SPECIFIC_TYPE_SECURE_KEYPAD_DOOR_LOCK,              "Secure Keypad Door Lock"},
    {SPECIFIC_TYPE_SECURE_KEYPAD_DOOR_LOCK_DEADBOLT,     "Secure Keypad Door Lock DeadBolt"},
    {SPECIFIC_TYPE_SECURE_DOOR,                          "Secure Door"},
    {SPECIFIC_TYPE_SECURE_GATE,                          "Secure Gate"},
    {SPECIFIC_TYPE_SECURE_BARRIER_ADDON,                 "Secure Barrier AddOn"},
    {SPECIFIC_TYPE_SECURE_BARRIER_OPEN_ONLY,             "Secure Barrier Open Only"},
    {SPECIFIC_TYPE_SECURE_BARRIER_CLOSE_ONLY,            "Secure Barrier Close Only"},
    {SPECIFIC_TYPE_SECURE_LOCKBOX,                       "Secure LockBox"},
    {SPECIFIC_TYPE_SECURE_KEYPAD,                        "Secure KeyPad"},
};

type_specific_t GENERIC_CONTROLLER_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                             "Generic Controller"},
    {SPECIFIC_TYPE_PORTABLE_REMOTE_CONTROLLER,           "Portable Remote Controller"},
    {SPECIFIC_TYPE_PORTABLE_SCENE_CONTROLLER,            "Scene Controller"},
    {SPECIFIC_TYPE_PORTABLE_INSTALLER_TOOL,              "Installer Tool"},
    {SPECIFIC_TYPE_REMOTE_CONTROL_AV,                    "Remote Control AV"},
    {SPECIFIC_TYPE_REMOTE_CONTROL_SIMPLE,                "Remote Control Simple"},
};

type_specific_t METER_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                             "Meter"},
    {SPECIFIC_TYPE_SIMPLE_METER,                         "Simple Meter"},
    {SPECIFIC_TYPE_ADV_ENERGY_CONTROL,                   "Advanced Energy Control"},
    {SPECIFIC_TYPE_WHOLE_HOME_METER_SIMPLE,              "Whole Home Meter Simple"},
};

type_specific_t METER_PULSE_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                             "Meter Pulse"},
};

type_specific_t NON_INTEROPERABLE_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                             "Non Interoperable"},
};

type_specific_t REPEATER_SLAVE_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                             "Repeater Slave"},
    {SPECIFIC_TYPE_REPEATER_SLAVE,                       "Repeater Slave"},
    {SPECIFIC_TYPE_VIRTUAL_NODE,                         "Virtual Node"},
};

type_specific_t SECURITY_PANEL_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                             "Security Panel"},
    {SPECIFIC_TYPE_ZONED_SECURITY_PANEL,                 "Zoned Security Panel"},
};

type_specific_t SEMI_INTEROPERABLE_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                             "Semi Interoperable"},
    {SPECIFIC_TYPE_ENERGY_PRODUCTION,                 "Energy Production"},
};

type_specific_t SENSOR_ALARM_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                    "Sensor Alarm"},
    {SPECIFIC_TYPE_ADV_ZENSOR_NET_ALARM_SENSOR,                     "Advanded Zensor Network Alarm Sensor"},
    {SPECIFIC_TYPE_ADV_ZENSOR_NET_SMOKE_SENSOR,                     "Advanded Zensor Network Smoke Sensor"},
    {SPECIFIC_TYPE_BASIC_ROUTING_ALARM_SENSOR,                      "Basic Routing Alarm Sensor"},
    {SPECIFIC_TYPE_BASIC_ROUTING_SMOKE_SENSOR,                      "Basic Routing Smoke Sensor"},
    {SPECIFIC_TYPE_BASIC_ZENSOR_NET_ALARM_SENSOR,                   "Basic Zensor Network Alarm Sensor"},
    {SPECIFIC_TYPE_BASIC_ZENSOR_NET_SMOKE_SENSOR,                   "Basic Zensor Network Smoke Sensor"},
    {SPECIFIC_TYPE_ROUTING_ALARM_SENSOR,                    "Routing Alarm Sensor"},
    {SPECIFIC_TYPE_ROUTING_SMOKE_SENSOR,                    "Routing Smoke Sensor"},
    {SPECIFIC_TYPE_ZENSOR_NET_ALARM_SENSOR,                     "Zensor Network Alarm Sensor"},
    {SPECIFIC_TYPE_ZENSOR_NET_SMOKE_SENSOR,                     "Zensor Network Smoke Sensor"},
    {SPECIFIC_TYPE_ALARM_SENSOR,                    "Alarm Sensor"},
};

type_specific_t SENSOR_BINARY_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                    "Sensor Binary"},
    {SPECIFIC_TYPE_ROUTING_SENSOR_BINARY,       "Routing Sensor Binary"},
};

type_specific_t SENSOR_MULTILEVEL_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                 "Sensor Multilevel"},
    {SPECIFIC_TYPE_ROUTING_SENSOR_MULTILEVEL,                "Routing Sensor Multilevel"},
    {SPECIFIC_TYPE_CHIMNEY_FAN,                  "Chimney Fan"},
};

type_specific_t STATIC_CONTROLLER_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                "Static Controller"},
    {SPECIFIC_TYPE_PC_CONTROLLER,                "Controller"},
    {SPECIFIC_TYPE_SCENE_CONTROLLER,                "Scene Controller"},
    {SPECIFIC_TYPE_STATIC_INSTALLER_TOOL,               "Static Installer Tool"},
    {SPECIFIC_TYPE_SET_TOP_BOX,                 "Set Top Bix"},
    {SPECIFIC_TYPE_SUB_SYSTEM_CONTROLLER,               "Sub System Controller"},
    {SPECIFIC_TYPE_TV,                  "Television"},
    {SPECIFIC_TYPE_GATEWAY,                 "Gateway"},
};

type_specific_t SWITCH_BINARY_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,          "Switch Binary"},
    {SPECIFIC_TYPE_POWER_SWITCH_BINARY,        "Power Switch Binary"},
    {SPECIFIC_TYPE_SCENE_SWITCH_BINARY,        "Scene Switch Binary"},
    {SPECIFIC_TYPE_POWER_STRIP,        "Power Strip"},
    {SPECIFIC_TYPE_SIREN,               "Siren"},
    {SPECIFIC_TYPE_VALVE_OPEN_CLOSE,          "Valve Open Close"},
    {SPECIFIC_TYPE_COLOR_TUNABLE_BINARY,          "Color Tunable Binary"},
    {SPECIFIC_TYPE_IRRIGATION_CONTROLLER,        "Irrigation Controller"},
};

type_specific_t SWITCH_MULTILEVEL_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                "Switch Multilevel"},
    {SPECIFIC_TYPE_CLASS_A_MOTOR_CONTROL, "Motor Control Class A"},
    {SPECIFIC_TYPE_CLASS_B_MOTOR_CONTROL, "Motor Control Class B"},
    {SPECIFIC_TYPE_CLASS_C_MOTOR_CONTROL, "Motor Control Class C"},
    {SPECIFIC_TYPE_MOTOR_MULTIPOSITION,   "Motor Multiposition"},
    {SPECIFIC_TYPE_POWER_SWITCH_MULTILEVEL,   "Power Switch Multilevel"},
    {SPECIFIC_TYPE_SCENE_SWITCH_MULTILEVEL,   "Scene Switch Multilevel"},
    {SPECIFIC_TYPE_FAN_SWITCH,                  "Fan Switch"},
    {SPECIFIC_TYPE_COLOR_TUNABLE_MULTILEVEL,  "Color Tunable Multilevel"},
};

type_specific_t SWITCH_REMOTE_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                "Switch Remote"},
    {SPECIFIC_TYPE_SWITCH_REMOTE_BINARY,                "Switch Remote Binary"},
    {SPECIFIC_TYPE_SWITCH_REMOTE_MULTILEVEL,                "Switch Remote Multilevel"},
    {SPECIFIC_TYPE_SWITCH_REMOTE_TOGGLE_BINARY,                 "Switch Remote Toggle Binary"},
    {SPECIFIC_TYPE_SWITCH_REMOTE_TOGGLE_MULTILEVEL,         "Switch Remote Toggle Multilevel"},
};


type_specific_t SWITCH_TOGGLE_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                    "Switch Toggle"},
    {SPECIFIC_TYPE_SWITCH_TOGGLE_BINARY,                    "Switch Toggle Binary"},
    {SPECIFIC_TYPE_SWITCH_TOGGLE_MULTILEVEL,                    "Switch Toggle Multilevel"},
};

type_specific_t THERMOSTAT_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,                "Thermostat"},
    {SPECIFIC_TYPE_SETBACK_SCHEDULE_THERMOSTAT,                 "Setback Schedule Thermostat"},
    {SPECIFIC_TYPE_SETBACK_THERMOSTAT,                  "Setback Thermostat"},
    {SPECIFIC_TYPE_SETPOINT_THERMOSTAT,                 "Setpoint Thermostat"},
    {SPECIFIC_TYPE_THERMOSTAT_GENERAL,                  "Thermostat General"},
    {SPECIFIC_TYPE_THERMOSTAT_GENERAL_V2,               "Thermostat General Version 2"},
    {SPECIFIC_TYPE_THERMOSTAT_HEATING,                  "Thermostat Heating"},
};

type_specific_t VENTILATION_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,            "Ventilation"},
    {SPECIFIC_TYPE_RESIDENTIAL_HRV,     "Residential HRV"},
};

type_specific_t WINDOW_COVERING_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,            "Window Covering"},
    {SPECIFIC_TYPE_SIMPLE_WINDOW_COVERING,     "Simple Window Covering"},
};

type_specific_t ZIP_NODE_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,            "ZIP Node"},
    {SPECIFIC_TYPE_ZIP_ADV_NODE,     "ZIP Advanded Node"},
    {SPECIFIC_TYPE_ZIP_TUN_NODE,     "ZIP TUN Node"},
};

type_specific_t WALL_CONTROLLER_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,            "Wall Controller"},
    {SPECIFIC_TYPE_BASIC_WALL_CONTROLLER,     "Basic Wall Controller"},
};

type_specific_t NETWORK_EXTENDER_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,            "Network Extender"},
    {SPECIFIC_TYPE_SECURE_EXTENDER,     "Secure Extender"},
};

type_specific_t APPLIANCE_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,             "Appliance"},
    {SPECIFIC_TYPE_GENERAL_APPLIANCE,           "General Appliance"},
    {SPECIFIC_TYPE_KITCHEN_APPLIANCE,           "Kitchen Appiance"},
    {SPECIFIC_TYPE_LAUNDRY_APPLIANCE,           "Laundry Appliance"},
};

type_specific_t SENSOR_NOTIFICATION_DEVICE_NAME[]=
{
    {SPECIFIC_TYPE_NOT_USED,             "Notification Sensor"},
    {SPECIFIC_TYPE_NOTIFICATION_SENSOR,           "Notification Sensor"},
};

unSupported_device_type unSupportedDevName[]=
{
    {GENERIC_TYPE_AV_CONTROL_POINT,     sizeof(AV_CONTROL_POINT_DEVICE_NAME)/sizeof(type_specific_t) , AV_CONTROL_POINT_DEVICE_NAME},
    {GENERIC_TYPE_DISPLAY,              sizeof(DISPLAY_DEVICE_NAME)/sizeof(type_specific_t) , DISPLAY_DEVICE_NAME},
    {GENERIC_TYPE_ENTRY_CONTROL,        sizeof(ENTRY_CONTROL_DEVICE_NAME)/sizeof(type_specific_t) , ENTRY_CONTROL_DEVICE_NAME},
    {GENERIC_TYPE_GENERIC_CONTROLLER,   sizeof(GENERIC_CONTROLLER_DEVICE_NAME)/sizeof(type_specific_t) , GENERIC_CONTROLLER_DEVICE_NAME},
    {GENERIC_TYPE_METER,                sizeof(METER_DEVICE_NAME)/sizeof(type_specific_t) , METER_DEVICE_NAME},
    {GENERIC_TYPE_METER_PULSE,          sizeof(METER_PULSE_DEVICE_NAME)/sizeof(type_specific_t) , METER_PULSE_DEVICE_NAME},
    {GENERIC_TYPE_NON_INTEROPERABLE,    sizeof(NON_INTEROPERABLE_DEVICE_NAME)/sizeof(type_specific_t) , NON_INTEROPERABLE_DEVICE_NAME},
    {GENERIC_TYPE_REPEATER_SLAVE,       sizeof(REPEATER_SLAVE_DEVICE_NAME)/sizeof(type_specific_t) , REPEATER_SLAVE_DEVICE_NAME},
    {GENERIC_TYPE_SECURITY_PANEL,       sizeof(SECURITY_PANEL_DEVICE_NAME)/sizeof(type_specific_t) , SECURITY_PANEL_DEVICE_NAME},
    {GENERIC_TYPE_SEMI_INTEROPERABLE,   sizeof(SEMI_INTEROPERABLE_DEVICE_NAME)/sizeof(type_specific_t) , SEMI_INTEROPERABLE_DEVICE_NAME},
    {GENERIC_TYPE_SENSOR_ALARM,         sizeof(SENSOR_ALARM_DEVICE_NAME)/sizeof(type_specific_t) , SENSOR_ALARM_DEVICE_NAME},
    {GENERIC_TYPE_SENSOR_BINARY,        sizeof(SENSOR_BINARY_DEVICE_NAME)/sizeof(type_specific_t) , SENSOR_BINARY_DEVICE_NAME},
    {GENERIC_TYPE_SENSOR_MULTILEVEL,    sizeof(SENSOR_MULTILEVEL_DEVICE_NAME)/sizeof(type_specific_t) , SENSOR_MULTILEVEL_DEVICE_NAME},
    {GENERIC_TYPE_STATIC_CONTROLLER,    sizeof(STATIC_CONTROLLER_DEVICE_NAME)/sizeof(type_specific_t) , STATIC_CONTROLLER_DEVICE_NAME},            
    {GENERIC_TYPE_SWITCH_BINARY,        sizeof(SWITCH_BINARY_DEVICE_NAME)/sizeof(type_specific_t) , SWITCH_BINARY_DEVICE_NAME},        
    {GENERIC_TYPE_SWITCH_MULTILEVEL,    sizeof(SWITCH_MULTILEVEL_DEVICE_NAME)/sizeof(type_specific_t) , SWITCH_MULTILEVEL_DEVICE_NAME},            
    {GENERIC_TYPE_SWITCH_REMOTE,        sizeof(SWITCH_REMOTE_DEVICE_NAME)/sizeof(type_specific_t) , SWITCH_REMOTE_DEVICE_NAME},        
    {GENERIC_TYPE_SWITCH_TOGGLE,        sizeof(SWITCH_TOGGLE_DEVICE_NAME)/sizeof(type_specific_t) , SWITCH_TOGGLE_DEVICE_NAME},        
    {GENERIC_TYPE_THERMOSTAT,           sizeof(THERMOSTAT_DEVICE_NAME)/sizeof(type_specific_t) , THERMOSTAT_DEVICE_NAME},    
    {GENERIC_TYPE_VENTILATION,          sizeof(VENTILATION_DEVICE_NAME)/sizeof(type_specific_t) , VENTILATION_DEVICE_NAME},    
    {GENERIC_TYPE_WINDOW_COVERING,      sizeof(WINDOW_COVERING_DEVICE_NAME)/sizeof(type_specific_t) , WINDOW_COVERING_DEVICE_NAME},        
    {GENERIC_TYPE_ZIP_NODE,             sizeof(ZIP_NODE_DEVICE_NAME)/sizeof(type_specific_t) , ZIP_NODE_DEVICE_NAME},    
    {GENERIC_TYPE_WALL_CONTROLLER,      sizeof(WALL_CONTROLLER_DEVICE_NAME)/sizeof(type_specific_t) , WALL_CONTROLLER_DEVICE_NAME},        
    {GENERIC_TYPE_NETWORK_EXTENDER,     sizeof(NETWORK_EXTENDER_DEVICE_NAME)/sizeof(type_specific_t) , NETWORK_EXTENDER_DEVICE_NAME},            
    {GENERIC_TYPE_APPLIANCE,            sizeof(APPLIANCE_DEVICE_NAME)/sizeof(type_specific_t) , APPLIANCE_DEVICE_NAME},    
    {GENERIC_TYPE_SENSOR_NOTIFICATION,  sizeof(SENSOR_NOTIFICATION_DEVICE_NAME)/sizeof(type_specific_t) , SENSOR_NOTIFICATION_DEVICE_NAME},            
};
#endif 
