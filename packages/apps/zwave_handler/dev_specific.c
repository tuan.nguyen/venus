#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "timer.h"
#include "VR_define.h"
#include "slog.h"
#include "verik_utils.h"
#include "database.h"
#include "dev_specific.h"
#include "manage_adding.h"

extern sqlite3 *support_devs_db;
extern sqlite3 *zwave_db;
pthread_mutex_t g_dev_specific_info_listMutex;
device_specific_t g_dev_specific_list;

void init_dev_specific_list()
{
    VR_INIT_LIST_HEAD(&g_dev_specific_list.list);
    pthread_mutex_init(&g_dev_specific_info_listMutex, 0);
}

static int add_new_dev_specific_to_list(void *node_add)
{
    int res = 0;
    pthread_mutex_lock(&g_dev_specific_info_listMutex);
    device_specific_t *input = (device_specific_t *)node_add;
    device_specific_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &(g_dev_specific_list.list), list)
    {
        if(!strcmp(tmp->deviceId, input->deviceId))
        {
            res = 1;
            break;
        }
    }

    if(!res)
    {
        VR_(list_add_tail)(&(input->list), &(g_dev_specific_list.list));
    }

    pthread_mutex_unlock(&g_dev_specific_info_listMutex);
    return res;
}

// static void printf_dev_specific_list()
// {
//     int i=0;
//     device_specific_t *tmp = NULL;
    
//     VR_(list_for_each_entry)(tmp, &g_dev_specific_list.list, list)
//     {
//         SLOGI("#### dev %d ######\n", i);
//         SLOGI("deviceId = %s\n", tmp->deviceId);
//         SLOGI("serialId = %s\n", tmp->serialId);
//         SLOGI("typeSpecific = %s\n", tmp->deviceSpecific);
//         i++;
//     }
// }

static device_specific_t *get_dev_specifiec_from_id(char *id)
{
    if(!id)
    {
        return NULL;
    }

    device_specific_t *tmp = NULL;
    VR_(list_for_each_entry)(tmp, &g_dev_specific_list.list, list)
    {
        if(!strcmp(tmp->deviceId, id))
        {
            return tmp;
        }
    }
    return NULL;
}

void dev_specific_replace_byte(json_object *out, char *devId, char *input, size_t input_size)
{
    if(!devId || !input)
    {
        return;
    }

    device_specific_t *dev = get_dev_specifiec_from_id(devId);
    if(!dev)
    {
        return;
    }

    if(!dev->deviceSpecific)
    {
        return;
    }

    json_object *specificObject = VR_(create_json_object)(dev->deviceSpecific);
    if(!specificObject)
    {
        return;
    }

    CHECK_JSON_OBJECT_EXIST(byteReplaceObj, specificObject, ST_BYTE_REPLACE_MULTI, replace_done);

    enum json_type type = json_object_get_type(byteReplaceObj);
    if(json_type_array != type)
    {
        return;
    }

    int i, found = 0;
    int arraylen = json_object_array_length(byteReplaceObj);
    const char *pattern = NULL;
    json_object *jvalue = byteReplaceObj;
    for (i=0; i< arraylen; i++)
    {
        jvalue = json_object_array_get_idx(byteReplaceObj, i);
        CHECK_JSON_OBJECT_EXIST(patternObj, jvalue, ST_PATTERN, _next);
        pattern = json_object_get_string(patternObj);
        if(!strncmp(input, pattern, strlen(pattern)))
        {
            found = 1;
            break;
        }
_next:
        continue;
    }

    if(!found)
    {
        goto replace_done;
    }

    if(!pattern)
    {
        goto replace_done;
    }

    CHECK_JSON_OBJECT_EXIST(replaceToObj, jvalue, ST_REPLACE_TO, replace_done);
    CHECK_JSON_OBJECT_EXIST(meaningObj, jvalue, ST_MEANING, replace_done);

    const char *replaceTo = json_object_get_string(replaceToObj);
    const char *meaning = json_object_get_string(meaningObj);

    size_t inputLen = strlen(input);
    size_t patternLen = strlen(pattern);
    size_t replaceLen = strlen(replaceTo);

    if((replaceLen + patternLen) > inputLen)
    {
        SLOGE("replace %s from %s invalid\n", replaceTo, input);
        goto replace_done;
    }

    char *new = (char*)malloc(inputLen+1);
    char *old = (char*)malloc(replaceLen+1);
    strncpy(old, input+patternLen, replaceLen);

    strcpy(new, pattern);
    strcat(new, replaceTo);
    strcat(new, input+replaceLen+patternLen);

    strncpy(input, new, input_size-1);
    unsigned int value = strtoul(old, NULL, 16);

    if(out)
    {
        json_object_object_add(out, meaning, json_object_new_int(value));
    }

    free(new);
    free(old);

replace_done:
    json_object_put(specificObject);
}

static void free_dev_specific(device_specific_t *addingNode)
{
    if(!addingNode)
    {
        return;
    }

    SAFE_FREE(addingNode->deviceId);
    SAFE_FREE(addingNode->serialId);
    SAFE_FREE(addingNode->deviceSpecific);
    SAFE_FREE(addingNode);
}

void insert_specific_dev_list(char *deviceId, char *serialId)
{
    if(!deviceId || !serialId)
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(device_specific);
    searching_database("zwave_handler", support_devs_db, zwave_cb, &device_specific,
                        "SELECT DeviceSpecific from SUPPORT_DEVS where SerialID='%s'",
                        serialId);
    if(!device_specific.len)
    {
        goto addingDone;
    }

    device_specific_t *addingNode = (device_specific_t *)(malloc)(sizeof(device_specific_t));
    memset(addingNode, 0x00, sizeof(device_specific_t));

    addingNode->deviceSpecific = (char *)malloc(device_specific.len +1);
    strcpy(addingNode->deviceSpecific, device_specific.value);

    addingNode->deviceId = (char *)malloc(strlen(deviceId) + 1);
    strcpy(addingNode->deviceId, deviceId);
    
    addingNode->serialId = (char *)malloc(strlen(serialId) + 1);
    strcpy(addingNode->serialId, serialId);
    
    if(add_new_dev_specific_to_list(addingNode))
    {
        free_dev_specific(addingNode);
    }

addingDone:
    FREE_SEARCH_DATA_VAR(device_specific);
}

static void polling_action_handler(void *data)
{
    device_specific_t *dev = (device_specific_t *)data;
    if(!dev)
    {
        return;
    }

    json_object *specificObject = VR_(create_json_object)(dev->deviceSpecific);
    if(!specificObject)
    {
        return;
    }
    CHECK_JSON_OBJECT_EXIST(pollingObj, specificObject, ST_POOLING, pollingDone);
    CHECK_JSON_OBJECT_EXIST(actionObj, pollingObj, ST_ACTION, pollingDone);

    actions_process(dev->deviceId, SYNC_FALSE, actionObj);

    dev->timerPolling = 0;

pollingDone:
    json_object_put(specificObject);
    return;
}

void dev_specific_polling_actions(char *deviceId, char *pattern)
{
    if(!deviceId)
    {
        return;
    }

    device_specific_t *dev = get_dev_specifiec_from_id(deviceId);
    if(!dev)
    {   
        return;
    }

    if(!dev->deviceSpecific)
    {
        return;
    }

    json_object *specificObject = VR_(create_json_object)(dev->deviceSpecific);
    if(!specificObject)
    {
        SLOGE("%s is not json object\n", dev->deviceSpecific);
        return;
    }

    CHECK_JSON_OBJECT_EXIST(pollingObj, specificObject, ST_POOLING, pollingDone);

    if(pattern)
    {
        CHECK_JSON_OBJECT_EXIST(signalObj, pollingObj, ST_SIGNAL, pollingDone);
        const char *signalIn = json_object_get_string(signalObj);

        if(strcmp(pattern, signalIn))
        {
            goto pollingDone;
        }
    }

    CHECK_JSON_OBJECT_EXIST(intervalObj, pollingObj, ST_INTERVAL_TIME, pollingDone);
    
    int interval = json_object_get_int(intervalObj)*1000;
    if(interval == 0)
    {
        interval = 500;
    }

    if(dev->timerPolling)
    {
        timerCancel(&(dev->timerPolling));
        dev->timerPolling = 0;
    }

    timerStart(&(dev->timerPolling), polling_action_handler, dev, interval, TIMER_ONETIME);

pollingDone:
    json_object_put(specificObject);
    return;
}

/*
    Zwave have 2 register to set temperature color.
    We will convert temp color in kelvin to 2 byte with fomula is
         0% Kelvil        ------------      50% Kelvin       ------------     100% Kelvil
    |WarmWhite|CoolWhile| ------------ |WarmWhite|CoolWhite| ------------ |WarmWhite|CoolWhite|
        255        0                       127       127                       0        255
*/
void dev_specific_temperature_color(char *devId, int kelvinValue, int *ww, int *cw)
{
    *ww = 127;
    *cw = 127;

    if(!devId)
    {
        return;
    }

    device_specific_t *dev = get_dev_specifiec_from_id(devId);
    if(!dev)
    {
        return;
    }

    if(!dev->deviceSpecific)
    {
        return;
    }

    json_object *specificObject = VR_(create_json_object)(dev->deviceSpecific);
    if(!specificObject)
    {
        return;
    }

    CHECK_JSON_OBJECT_EXIST(tempColorObj, specificObject, ST_TEMPERATURE_COLOR, done);
    CHECK_JSON_OBJECT_EXIST(minObj, tempColorObj, ST_MIN, done);
    CHECK_JSON_OBJECT_EXIST(maxObj, tempColorObj, ST_MAX, done);

    int min = json_object_get_int(minObj);
    int max = json_object_get_int(maxObj);

    float percent = (float)(kelvinValue - min)/(max-min);
    if(percent <= 0)
    {
        *ww = 255;
        *cw = 0;
    }
    else if(percent >= 1)
    {
        *ww = 0;
        *cw = 255;
    }
    else
    {
        *cw = 255*percent;
        *ww = 255 - *cw;
    }

done:
    json_object_put(specificObject);
    return;
}

uint8_t invert_status(char *devId, uint8_t value)
{
    uint8_t res = value;
    if(!devId)
    {
        return res;
    }

    device_specific_t *dev = get_dev_specifiec_from_id(devId);
    if(!dev)
    {
        return res;
    }

    if(!dev->deviceSpecific)
    {
        return res;
    }

    json_object *specificObject = VR_(create_json_object)(dev->deviceSpecific);
    if(!specificObject)
    {
        SLOGI("%s does not json format\n", dev->deviceSpecific);
        return res;
    }

    char strValue[SIZE_32B];
    sprintf(strValue, "0x%02X", value); /*hex format*/
    CHECK_JSON_OBJECT_EXIST(invertObj, specificObject, ST_INVERT_STATUS, done);
    CHECK_JSON_OBJECT_EXIST(singleValueObj, invertObj, ST_SINGLE_VALUE, done);
    CHECK_JSON_OBJECT_EXIST(newValueObj, singleValueObj, strValue, done);

    const char *newValue = json_object_get_string(newValueObj);
    res = strtol(newValue, NULL, 16);
done:
    json_object_put(specificObject);
    return res;
}

/*init zwave polling list for libzwave polling device*/
void dev_specific_zwave_polling_interval(char *devId, int *interval, int* cmd)
{
    *interval = 0;
    *cmd = 0;
    if(!devId)
    {
        return;
    }

    device_specific_t *dev = get_dev_specifiec_from_id(devId);
    if(!dev)
    {
        return;
    }

    if(!dev->deviceSpecific)
    {
        return;
    }

    json_object *specificObject = VR_(create_json_object)(dev->deviceSpecific);
    if(!specificObject)
    {
        return;
    }

    CHECK_JSON_OBJECT_EXIST(pollingObj, specificObject, ST_UPDATE_STATUS, isPollingDone);
    CHECK_JSON_OBJECT_EXIST(intervalObj, pollingObj, ST_INTERVAL, isPollingDone);
    CHECK_JSON_OBJECT_EXIST(cmdObj, pollingObj, ST_CMD, isPollingDone);

    *interval = json_object_get_int(intervalObj);
    *cmd = htoi(json_object_get_string(cmdObj));

isPollingDone:
    return;
}

uint8_t get_maximum_user_code(char *devId)
{
    int maxUser = MAXIMUM_USER_CODE_ID_DEFAULT;
    if(!devId)
    {
        return maxUser;
    }

    device_specific_t *dev = get_dev_specifiec_from_id(devId);
    if(!dev)
    {
        return maxUser;
    }

    if(!dev->deviceSpecific)
    {
        return maxUser;
    }

    json_object *specificObject = VR_(create_json_object)(dev->deviceSpecific);
    if(!specificObject)
    {
        return maxUser;
    }

    CHECK_JSON_OBJECT_EXIST(userCodeObj, specificObject, ST_USER_CODE_AUTO_SET, getUserDone);
    CHECK_JSON_OBJECT_EXIST(maxUserObj, userCodeObj, ST_SUPPORTED_USERS, getUserDone);
    maxUser = json_object_get_int(maxUserObj);

getUserDone:
    return maxUser;
}

//Mode: 0: Low threshold, 1: High threshold
void get_temperature_threshold(char *devId, int mode, char *unit, char *value, int len)
{
    if(!devId || !value || !unit)
    {
        SLOGI("Input Invalid\n");
        return;
    }

    char featureId[SIZE_32B];
    if(mode)
    {
        strncpy(featureId, ST_TEMPERATURE_HIGH_THRESHOLD, sizeof(featureId)-1);
    }
    else
    {
        strncpy(featureId, ST_TEMPERATURE_LOW_THRESHOLD, sizeof(featureId)-1);
    }
    featureId[sizeof(featureId)-1]='\0';

    SEARCH_DATA_INIT_VAR(thresholdTemp);
    searching_database(_VR_CB_(zwave), &thresholdTemp,
                        "SELECT register from FEATURES where deviceId='%s' AND featureId='%s'",
                        devId, featureId);
    if(thresholdTemp.len)
    {
        strncpy(value, thresholdTemp.value, len);
        FREE_SEARCH_DATA_VAR(thresholdTemp);
        return;
    }

    device_specific_t *dev = get_dev_specifiec_from_id(devId);
    if(!dev)
    {
        return;
    }

    if(!dev->deviceSpecific)
    {
        return;
    }

    json_object *specificObject = VR_(create_json_object)(dev->deviceSpecific);
    if(!specificObject)
    {
        SLOGI("%s does not json format\n", dev->deviceSpecific);
        FREE_SEARCH_DATA_VAR(thresholdTemp);
        return;
    }

    CHECK_JSON_OBJECT_EXIST(threshHoldTempObj, specificObject, ST_TEMPERATURE_THRESH_HOLD, getDone);
    char unitVal[SIZE_32B];
    if(!strcmp(unit, ST_CELSIUS))
    {
        if(mode)
        {
            strncpy(unitVal, ST_MAX_CELSIUS, sizeof(unitVal)-1);
        }
        else
        {
            strncpy(unitVal, ST_MIN_CELSIUS, sizeof(unitVal)-1);
        }
    }
    else
    {
        if(mode)
        {
            strncpy(unitVal, ST_MAX_FAHRENHEIT, sizeof(unitVal)-1);
        }
        else
        {
            strncpy(unitVal, ST_MIN_FAHRENHEIT, sizeof(unitVal)-1);
        }
    }
    unitVal[sizeof(unitVal)-1]='\0';

    CHECK_JSON_OBJECT_EXIST(valObj, threshHoldTempObj, unitVal, getDone);
    strncpy(value, json_object_get_string(valObj), len);

getDone:
    json_object_put(specificObject);
    FREE_SEARCH_DATA_VAR(thresholdTemp);
}

void get_specific_action_color_mode(char *devId)
{
    if(!devId)
    {
        return;
    }

    device_specific_t *dev = get_dev_specifiec_from_id(devId);
    if(!dev)
    {
        return;
    }

    if(!dev->deviceSpecific)
    {
        return;
    }

    json_object *specificObject = VR_(create_json_object)(dev->deviceSpecific);
    if(!specificObject)
    {
        SLOGI("%s does not json format\n", dev->deviceSpecific);
        return;
    }

    CHECK_JSON_OBJECT_EXIST(colorModeObj, specificObject, ST_COLORFUL_MODE, getDone);
    CHECK_JSON_OBJECT_EXIST(updateStatusObj, colorModeObj, ST_UPDATE_STATUS, getDone);
    CHECK_JSON_OBJECT_EXIST(actionsObj, updateStatusObj, ST_ACTION, getDone);
    CHECK_JSON_OBJECT_EXIST(syncActionObj, updateStatusObj, ST_SYNC_ACTION, getDone);

    if(syncActionObj)
    {
        const char *syncAction = json_object_get_string(syncActionObj);
        if(!strcmp(syncAction, ST_TRUE))
        {
            actions_process(devId, SYNC_TRUE, actionsObj);
        }
        else
        {
            actions_process(devId, SYNC_FALSE, actionsObj);
        }
    }
    else
    {
        actions_process(devId, SYNC_FALSE, actionsObj);
    }

getDone:
    json_object_put(specificObject);
}
