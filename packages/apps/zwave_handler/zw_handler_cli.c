#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <getopt.h>
#include <unistd.h>
#include <math.h>

#include <libubus.h>
#include <libubox/blobmsg_json.h>
#include <json-c/json.h>
#include <sys/stat.h>//check file exist ?

#include "utils.h"
#include "manu_product.h"
#include "zw_app_utils.h"
#include "gpio_intf.h"
#include "slog.h"
#include "timer.h"
#include "timing.h"

#include "database.h"
#include "group_apis.h"
#include "verik_utils.h"
#include "manage_adding.h"
#include "manage_usercode.h"
#include "dev_specific.h"
#include "action_control.h"
#include "vr_sound.h"
#include "vr_rest.h"
#include "dev_state.h"
#include "pmortem.h"

#ifdef TEST
#include "test_case/test_notification.h"
#endif

#define COMMAND_FAILED                   -1
#define GET_CAP_FAILED                   -2
#define GET_DEV_TYPE_FAILED              -3
#define DEV_NOT_FOUND                    -4
#define DEV_UNACTIVATE                   -5
#define GET_SCHEME_FAILED                -6
#define CLASS_NOT_SUPPORT_CMD            -7
#define DEV_NOT_SUPPORT_CLASS            -8
#define CONFIG_ID_NOT_FOUND              -9
#define COMMAND_NOT_SUPPORT              -10
#define USER_CODE_NAME_INVALID           -11
#define USER_CODE_USERID_INVALID         -12

#define ZWAVE_CONTROLLER_FIRMWARE_ZM5304 "/lib/firmware/zwave/serialapi_controller_static_ZM5304_US_%s.hex"
#define ZWAVE_CONTROLLER_FIRMWARE_ZM5101 "/lib/firmware/zwave/serialapi_controller_static_ZM5101_US_%s.hex"
#define ZWAVE_CONTROLLER_VERSION TOSTRING(LASTEST_FIRMWARE_0_VERSION)"."TOSTRING(LASTEST_FIRMWARE_0_SUB_VERSION)

#define ZWAVE_CONSOLE_PORT_DEFAULT "/dev/ttyO2"
#define GPIO_PIN_CTRL_NUM 49 
#define GPIO_PIN_RST_NUM  46
#define SIZE_MISC_DATA    255
#define SIZE_DATA_IN_QUEUE    1024
#define SIZE_HOME_ID      16
#define ADD_DEV_NOTIFICATION      1
#define REPLACE_FAILED_NODE_NOTIFICATION      0

#define PARENT_ENDPOINT_NUM      "0"
#define PARENT_ID_NUM            "0"
#define MAXIMUM_DEVICE_TIME_LIFE (3600*24 + 5*60) //24h+5mins

#define NORMAL_DEVICE_DELAY                  2000 /*us*/
#define SECURE_DEVICE_DELAY_INTERNAL         30000 /*us*/
#define SECURE_DEVICE_DELAY_EXTERNAL         200000 /*us*/
#define BURST_TIME_DELAY                     3000 /*ms*/

#define SLEEP_TIME 500 //micro second

const int OPEN=1;
const int CLOSE=0;

#define DOORLOCK_OPEN_VALUE 0
#define DOORLOCK_CLOSE_VALUE 255

#define BARRIER_OPEN_VALUE 255
#define BARRIER_CLOSE_VALUE 0
#define GET_HIGH_BYTE 8

static struct ubus_context *ctx;
static struct blob_buf buff;
static struct ubus_event_handler ubus_event_listener;

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

//static uint8_t size_passcode = 4; //default doorlock pass size

sqlite3 *zwave_db;
sqlite3 *support_devs_db;
notify_queue_t g_notify;

int g_shmid = 0;
char *g_shm = NULL;

static int already_have_devices = 0;

ZW_OwnerID zw_OwnerID;
static timer_t timerSpeakAdding;
static int g_sound_alarm_enable = 1;
int g_open_network = 0; //block control when active
uint8_t sequenceNumberSimpleAV = 0;
static pthread_mutex_t CriticalMutexTransmitQueue=PTHREAD_MUTEX_INITIALIZER;

sTiming             txTiming;
pthread_t zwave_command_process_thread;
static int zwave_command_process_enable = 1;
static int g_inform_power_cycle = 0;

pthread_mutex_t zwave_dev_info_listMutex;
zwave_dev_info_t g_zwave_dev_list;

zwave_security_scheme_t zwave_scheme_table[]=
{
    {"nonS" , NON_SEC_SCHEME,       0},
    {"S0" ,   SEC_0_SCHEME,         1},
    {"S2" ,   SEC_2_1_SCHEME,       2},
    {"S2" ,   SEC_2_2_SCHEME,       3},
    {"S2" ,   SEC_2_3_SCHEME,       4},
};

void remove_dev_in_cloud(char *ID);
void Send_ubus_notify(char *data);
void remove_dev(char *id);
void remove_device_thread(void *data);

zwave_dev_info_t *_create_zwave_dev_(char *serialId, char *deviceType, char *deviceMode, char *id, char *endpointNum,
                                    char *parentId, char *childrenId, char *active, char *cloudId, int isAdding);
void printf_dev_info_list();
void remove_zwave_dev_from_id(char *id);
void remove_zwave_dev_lists();
void update_zwave_status(char* service_name, sqlite3 *db, db_callback callback, 
                        const char *ID, const char* feature, const char* value,
                        const char* mode, int rotate_num);

static void cancel_adding_device_cb(struct uloop_timeout *timeout);
static void update_zwave_controll_info();
static void alarm_voice_inform(struct ubus_context *ctx, char *ID, char *alarmTypeFinal);

void init_zwave_dev_list()
{
    VR_INIT_LIST_HEAD(&g_zwave_dev_list.list);
    pthread_mutex_init(&zwave_dev_info_listMutex, 0);
}

int add_last_list(void *node_add)
{
    int res = 0;
    pthread_mutex_lock(&zwave_dev_info_listMutex);
    zwave_dev_info_t *input = (zwave_dev_info_t *)node_add;
    zwave_dev_info_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &(g_zwave_dev_list.list), list)
    {
        if(tmp->localId == input->localId)
        {
            res = 1;
            break;
        }
    }

    if(!res)
    {
        VR_(list_add_tail)(&(input->list), &(g_zwave_dev_list.list));
    }

    pthread_mutex_unlock(&zwave_dev_info_listMutex);
    return res;
}

void remove_list(char *id)
{
    pthread_mutex_lock(&zwave_dev_info_listMutex);
    zwave_dev_info_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_zwave_dev_list.list)
    {
        tmp = VR_(list_entry)(pos, zwave_dev_info_t, list);
        if(tmp->localId == htoi(id))
        {
            VR_(list_del)(pos);
            if(tmp->json_capId)
            {
                json_object_put(tmp->json_capId);
            }
            free(tmp);
        }
    }
    pthread_mutex_unlock(&zwave_dev_info_listMutex);
}

// static char buf[SIZE_256B];
// static char* GetBarrierStateStr(uint8_t state)
// {
//     switch (state)
//     {
//         case BARRIER_OPERATOR_REPORT_CLOSED:
//             snprintf(buf, SIZE_256B, "closed");
//             break;
//         case BARRIER_OPERATOR_REPORT_CLOSING:
//             snprintf(buf, SIZE_256B, "closing");
//             break;
//         case BARRIER_OPERATOR_REPORT_STOPPED:
//             snprintf(buf, SIZE_256B, "stopped");
//             break;
//         case BARRIER_OPERATOR_REPORT_OPENING:
//             snprintf(buf, SIZE_256B, "opening");
//             break;
//         case BARRIER_OPERATOR_REPORT_OPEN:
//             snprintf(buf, SIZE_256B, "open");
//             break;
//         default:
//             snprintf(buf, SIZE_256B, "unknown");
//             break;
//     }
//     return buf;
// }

int controller_enable()
{
    /*
     * Enable GPIO pins
     */
    if (-1 == GPIOExport(GPIO_PIN_CTRL_NUM) || -1 == GPIOExport(GPIO_PIN_RST_NUM))
    {
        SLOGE("FAILED to export control and reset pin\n");
        return -1;
    }

    /*
     * Set GPIO directions
     */
    if (-1 == GPIODirection(GPIO_PIN_CTRL_NUM, OUT) || -1 == GPIODirection(GPIO_PIN_RST_NUM, OUT))
    {
        SLOGE("FAILED to set direction control and reset pin\n");
        return -1;
    }

    /* 
     * Write data
     */
    if (-1 == GPIOWrite(GPIO_PIN_CTRL_NUM, 0))
    {
        SLOGW("Can't write to CTRL PIN:%d\n", GPIO_PIN_CTRL_NUM);
        return -1;
    }
    if (-1 == GPIOWrite(GPIO_PIN_RST_NUM, 1))
    {
        SLOGW("Can't write to RST PIN:%d\n", GPIO_PIN_RST_NUM);
        return -1;
    }

    return 0;
}

void controller_reset()
{
    /*
     * Disable GPIO pins
     */
    if (-1 == GPIOUnexport(GPIO_PIN_CTRL_NUM) || -1 == GPIOUnexport(GPIO_PIN_RST_NUM))
    {
        SLOGW("Can't write to RST PIN:%d\n", GPIO_PIN_RST_NUM);
    }
    return;
}

static int features_callback(void *status, int argc, char **argv, char **azColName)
{

    json_object_object_add(status, argv[0], json_object_new_string(argv[1] ? argv[1] : "NULL"));
    return 0;
}

static int listdevice_callback(void *devicelist, int argc, char **argv, char **azColName)
{
    int i;
    json_object * jobj = json_object_new_object();
    char ID[16];
    for(i=0; i<argc; i++)
    {
        if(!strcmp(azColName[i], "ID"))
        {
            strcpy(ID, argv[i]);
        }
        json_object_object_add(jobj, azColName[i], json_object_new_string(argv[i] ? argv[i] : "NULL"));
    }

    json_object * status = json_object_new_object();

    searching_database(_VR_(zwave), features_callback, (void *)status,
                        "SELECT featureId,register from FEATURES where deviceId='%s'", ID);

    json_object_object_add(jobj, ST_STATUS, status);

    json_object_array_add(devicelist, jobj);
    return 0;
}

////////////////////////// For Ubus //////////////////////////////
enum
{
    NODEID,
    VALUE,
    __MAX
};
static const struct blobmsg_policy null_policy[0]={};

static const struct blobmsg_policy set_policy[__MAX]={
    [NODEID] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
    [VALUE] = { .name = ST_VALUE, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy setbinary_policy[3]={
    [0] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_CMD, .type = BLOBMSG_TYPE_STRING },
    [2] = { .name = ST_VALUE, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy get_policy[1]={
    [NODEID] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy get_specification[6]={
    [0] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_CLASS, .type = BLOBMSG_TYPE_STRING },
    [2] = { .name = ST_CMD, .type = BLOBMSG_TYPE_STRING },
    [3] = { .name = ST_DATA0, .type = BLOBMSG_TYPE_STRING },
    [4] = { .name = ST_DATA1, .type = BLOBMSG_TYPE_STRING },
    [5] = { .name = ST_DATA2, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy specification_CRC[9]={
    [0] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_CLASS, .type = BLOBMSG_TYPE_STRING },
    [2] = { .name = ST_CMD, .type = BLOBMSG_TYPE_STRING },
    [3] = { .name = ST_DATA0, .type = BLOBMSG_TYPE_STRING },
    [4] = { .name = ST_DATA1, .type = BLOBMSG_TYPE_STRING },
    [5] = { .name = ST_DATA2, .type = BLOBMSG_TYPE_STRING },
    [6] = { .name = ST_SRC_ENDPOINT, .type = BLOBMSG_TYPE_STRING },
    [7] = { .name = ST_DES_ENDPOINT, .type = BLOBMSG_TYPE_STRING },
    [8] = { .name = ST_IS_CRC, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy set_specification[6]={
    [0] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_CLASS, .type = BLOBMSG_TYPE_STRING },
    [2] = { .name = ST_CMD, .type = BLOBMSG_TYPE_STRING },
    [3] = { .name = ST_DATA0, .type = BLOBMSG_TYPE_STRING },
    [4] = { .name = ST_DATA1, .type = BLOBMSG_TYPE_STRING },
    [5] = { .name = ST_DATA2, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy get_secure_specification[4]={
    [0] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_CLASS, .type = BLOBMSG_TYPE_STRING },
    [2] = { .name = ST_CMD, .type = BLOBMSG_TYPE_STRING },
    [3] = { .name = ST_DATA0, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy alarm_policy[]={
    [0] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_VALUE, .type = BLOBMSG_TYPE_STRING },
    [2] = { .name = ST_TEMPORARY_TIME, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy scene_policy[2]={
    [0] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_BUTTON_ID, .type = BLOBMSG_TYPE_STRING },
};


static int list_device(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));
    memset(pzwParam, 0x00, sizeof(TZWParam));

    SLOGI("in list_device \n");
    json_object *jobj = json_object_new_object();
    
    SLOGI("zwave list_devices\n");

    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_LIST_DEVICES_R));
    json_object *devicelist = json_object_new_array();

    searching_database(_VR_(zwave), listdevice_callback, (char *)devicelist,
                        "SELECT * from SUB_DEVICES where owner='%08X%02X'",
                        zw_OwnerID.HomeID, zw_OwnerID.MyNodeID);

    printf("strlen(json_object_to_json_string(devicelist)) = %d\n", strlen(json_object_to_json_string(devicelist)));
    if(strlen(json_object_to_json_string(devicelist)) == 3) // [ ]
    {
        if(already_have_devices)
        {
            already_have_devices = 0;
            system("/etc/start_services.sh start_service zwave_handler >> /system.log");
        }
    }
    else
    {
        already_have_devices = 1;
    }

    json_object_object_add(jobj, ST_DEVICES_LIST, devicelist);
    // VR_(usleep)(500);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    if(pzwParam)
    {
        free(pzwParam);
    }

    return 0;
}

static int reset_remove_cloud_cb(void *data, int argc, char **argv, char **azColName)
{
    if(argc == 0)
    {
        return 0;
    }

    char *resource_id = argv[0];
    if(resource_id && strlen(resource_id))
    {
        if(!VR_(delete_resources)(resource_id, RESOURCES_TIMEOUT))
        {
            database_actions(_VR_(zwave), "DELETE from DEVICES where owner='%s'", resource_id);
            shm_update_data(g_shm, resource_id, NULL, NULL, SHM_DELETE);
        }
    }

    return 0;
}

void reset_remove_cloud()
{
    searching_database("zwave_handler", zwave_db, reset_remove_cloud_cb, NULL,  
                        "SELECT owner from DEVICES where deviceType='%s'", ST_ZWAVE);
}

void reset_zwave_thread()
{
    remove_zwave_dev_lists();
    // printf_dev_info_list();
    reset_remove_cloud();

    char zwaveControllerId[SIZE_64B];
    sprintf(zwaveControllerId, "%08X%02X", zw_OwnerID.HomeID, zw_OwnerID.MyNodeID);

    database_actions(_VR_(zwave), "DELETE from CONTROL_DEVS where udn='%s'", zwaveControllerId);
    database_actions(_VR_(zwave), "DELETE from SUB_DEVICES where type='zwave'");
    database_actions(_VR_(zwave), "DELETE from FEATURES where type='zwave_handler'");
    database_actions(_VR_(zwave), "DELETE from METER_DATA where type='zwave_handler'");
    database_actions(_VR_(zwave), "DELETE from ASSOCIATION_DATA where type='zwave_handler'");
    database_actions(_VR_(zwave), "DELETE from USER_CODE where type='zwave_handler'");
    database_actions(_VR_(zwave), "DELETE from ALARM where type='zwave_handler'");
    database_actions(_VR_(zwave), "DELETE from SENSOR_DATA where type='zwave_handler'");
    database_actions(_VR_(zwave), "DELETE from THERMOSTAT_SETPOINT where type='zwave_handler'");
    database_actions(_VR_(zwave), "DELETE from MORE_PROPERTY where type='zwave_handler'");
    database_actions(_VR_(zwave), "DELETE from CAPABILITY where type='zwave'");
    database_actions(_VR_(zwave), "DELETE from SCENES where type='zwave'");

    update_zwave_controll_info();
}

static int reset_device(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));
    memset(pzwParam, 0x00, sizeof(TZWParam));

    SLOGI("in reset_device \n");
    json_object *jobj = json_object_new_object();

    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("ZWAVE RESET\n");
    system("echo \"ZWAVE RESET\" > /zwave.reset");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    
    SLOGI("START Send Zwave Command\n");
    pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_SET_DEFAULT;
    zwaveSendCommand(pzwParam);
    SLOGI("END Zwave Command\n");
    if (pzwParam->ret==0)
    {
        pthread_t reset_zwave_thread_t;
        pthread_create(&reset_zwave_thread_t, NULL, (void *)&reset_zwave_thread, NULL);
        pthread_detach(reset_zwave_thread_t);

        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RESET_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    }
    else if (pzwParam->ret==-1)
    {
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RESET_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("Action timeout (try again)"));
    }
    
    // VR_(usleep)(500);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    if(pzwParam)
    {
        free(pzwParam);
    }

    return 0;
}

static void remove_junk_devices()
{
    int i, found = 0;
    zwave_dev_info_t *zwave_dev = NULL;
    struct VR_list_head *pos, *q;

    TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));
    memset(pzwParam, 0x00, sizeof(TZWParam));
    pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_GET_LIST;
    zwaveSendCommand(pzwParam);

    if(pzwParam->ret != 0)
    {
        free(pzwParam);
        return;
    }

    for ( i=0; i<pzwParam->param1; i++)
    {
        found = 0;
        VR_(list_for_each_safe)(pos, q, &g_zwave_dev_list.list)
        {
            zwave_dev = VR_(list_entry)(pos, zwave_dev_info_t, list);
            if(pzwParam->multi_node[i].nodeId == zwave_dev->localId)
            {
                found = 1;
                break;
            }
        }

        if(!found)
        {
            TZWParam *removeParam = (TZWParam*)malloc(sizeof(TZWParam));
            memset(removeParam, 0x00, sizeof(TZWParam));
            removeParam->command=COMMAND_CLASS_SPECIFIC_NODE_REMOVE_FORCE;
            removeParam->param1=pzwParam->multi_node[i].nodeId;

            zwave_command_response_t response;
            memset(&response, 0x00, sizeof(zwave_command_response_t));
            strcpy(response.method, ST_REMOVE_DEVICE_R);
            strcpy(response.nodeid, "FF");
            strcpy(response.command, ST_REMOVE);
            sprintf(response.value, "%02X", pzwParam->multi_node[i].nodeId);

            insert_to_a_queue(DEAD_PRIORITY, response, removeParam);
            free(removeParam);
        }
    }
    free(pzwParam);
}

void update_network()
{
    remove_junk_devices();

    // SLOGI("start update update_network\n");
    // TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));
    // memset(pzwParam, 0x00, sizeof(TZWParam));
    // pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_REQUEST_UPDATE;
    // zwaveSendCommand(pzwParam);
    // free(pzwParam);
    // return ;
}

// 0 successful, another failed.
int update_route(uint8_t id)
{
    // SLOGI("start update route id %02X\n", id);
    int ret = 0;
    // TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));
    // memset(pzwParam, 0x00, sizeof(TZWParam));
    // pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_UPDATE_RETURN_ROUTE;
    // pzwParam->param1=id;
    // ret = zwaveSendCommand(pzwParam);
    // free(pzwParam);
    return ret;
}

int check_dev_is_support_cap(json_object *json_capId, int capId, int *scheme)
{
    if(!json_capId)
    {
        return 0;
    }

    printf("json_object_to_json_string(json_capId) %s\n", json_object_to_json_string(json_capId));
    printf("capId = %d\n", capId);
    int tmp_scheme = NON_SEC_SCHEME;
    int tmp_priority = 0;
    int res = 0;
    char capId_str[SIZE_32B];
    sprintf(capId_str, "%d", capId);
    *scheme = tmp_scheme;
    json_object_object_foreach(json_capId, list_capId, sheme_obj)
    {
        if(strstr(list_capId, capId_str))
        {
            tmp_scheme = strtol(json_object_get_string(sheme_obj), NULL, 0);
            int i;
            for(i=0; i< sizeof(zwave_scheme_table)/sizeof(zwave_security_scheme_t); i++)
            {
                if(zwave_scheme_table[i].scheme == tmp_scheme)
                {
                    if(zwave_scheme_table[i].priority > tmp_priority)
                    {
                        *scheme = tmp_scheme;
                        tmp_priority = zwave_scheme_table[i].priority;
                    }
                    break;
                }
            }
            res = 1;
        }
    }

/*still not found, if capId is BASIC, return true*/
    if(!res && capId == COMMAND_CLASS_BASIC)
    {
        res = 1;
    }

    return res;
}

int get_cap_with_priority(json_object *json_capId, int array_capId[], int array_length, int *scheme)
{
    if(!json_capId)
    {
        return -1;
    }

    printf("json_object_to_json_string(json_capId) %s\n", json_object_to_json_string(json_capId));
    int i;
    for(i=0; i< array_length; i++)
    {
        printf("capId[%d] = %d\n", i, array_capId[i]);
    }

    int tmp_scheme = NON_SEC_SCHEME;
    int tmp_priority = 0;
    int res = array_capId[array_length-1];/*allway choose BASIC for default*/
    char capId_str[SIZE_32B];
    // sprintf(capId_str, "%d", capId);
    *scheme = tmp_scheme;
    json_object_object_foreach(json_capId, list_capId, sheme_obj)
    {
        for(i=0; i< array_length; i++)
        {
            sprintf(capId_str, "%d", array_capId[i]);
            if(strstr(list_capId, capId_str))
            {
                tmp_scheme = strtol(json_object_get_string(sheme_obj), NULL, 0);
                int j;
                for(j=0; j< sizeof(zwave_scheme_table)/sizeof(zwave_security_scheme_t); j++)
                {
                    if(zwave_scheme_table[j].scheme == tmp_scheme)
                    {
                        if(zwave_scheme_table[j].priority >= tmp_priority)
                        {
                            *scheme = tmp_scheme;
                            res = array_capId[i];
                            tmp_priority = zwave_scheme_table[j].priority;
                        }
                        break;
                    }
                }
                break;
            }
        }
    }

    return res;
}

static int get_capId_and_scheme_dev_cb(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 2)
    {
        return 0;
    }

    char *capId = argv[0];
    char *sec = argv[1];
    if(!capId || !sec || !data)
    {
        return 0;
    }

    json_object_object_add(data, capId, json_object_new_string(sec));

    return 0;
}

void get_capId_and_scheme_dev(char *nodeid, json_object **json_capId)
{
    if(!nodeid)
    {
        return;
    }
    *json_capId = json_object_new_object();
    searching_database("zwave_handler", zwave_db, get_capId_and_scheme_dev_cb, (void*)*json_capId,  
                        "SELECT %s,%s from CAPABILITY where %s='%s'",
                        ST_CAP_LIST, ST_SCHEME, ST_DEVICE_ID, nodeid);

    return;
}

void get_float_format(char *SerialId, char *nodeId, char *format_key,
                        int *precision, int *scale, int *size, const char *unit)
{
    if(!strcmp(unit, ST_MULTILEVEL_SENSOR_LABEL_CELCIUS))
    {
        /*default Celsius*/
        *precision = 0;
        *scale = 0;
        *size = 1;
    }
    else if(!strcmp(unit, ST_MULTILEVEL_SENSOR_LABEL_FAHRENHEIT))
    {
        /*default Farhenrit*/
        *precision = 0;
        *scale = 1;
        *size = 1;
    }

    if(!SerialId)
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(FloatFormat);
    searching_database("zwave_handler", support_devs_db, zwave_cb, &FloatFormat, 
            "SELECT FloatFormat from SUPPORT_DEVS where SerialID='%s'", 
            SerialId);

    if(!FloatFormat.len)
    {
        searching_database(_VR_CB_(zwave), &FloatFormat, 
            "SELECT registe from FEATURES where deviceId='%s' and featureId='%s'", 
            nodeId, format_key);
    }

    if(!FloatFormat.len)
    {
        goto done;
    }

    json_object *jobj = VR_(create_json_object)(FloatFormat.value);
    if(!jobj)
    {
        SLOGI("floatformat is not json format\n");
        goto done;
    }

    CHECK_JSON_OBJECT_EXIST(unitObj, jobj, unit, done);
    const char *tmp = json_object_get_string(unitObj);
    char *unitFormat = (char*)malloc(strlen(tmp)+1);
    strcpy(unitFormat, tmp);
    
    char *tok, *save_tok;
    tok = strtok_r(unitFormat, ",", &save_tok);
    if(tok != NULL)
    {
        *precision = strtol(tok, NULL, 0);
        tok = strtok_r(NULL, ",", &save_tok);
    }
    if(tok != NULL)
    {
        *scale = strtol(tok, NULL, 0);
        tok = strtok_r(NULL, ",", &save_tok);
    }
    if(tok != NULL)
    {
        *size = strtol(tok, NULL, 0);
    }

    json_object_put(jobj);

done:
    FREE_SEARCH_DATA_VAR(FloatFormat);
}

void return_json_message(int res, json_object **jobj, char *command, char *commandClass)
{
    if(!jobj || !*jobj)
    {
        return;
    }

    char tmp[SIZE_512B];
    switch(res)
    {
        case DEV_NOT_FOUND:
            json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(*jobj, ST_REASON, json_object_new_string("device not found"));
            break;

        case GET_DEV_TYPE_FAILED:
            json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(*jobj, ST_REASON, json_object_new_string("failed to get DeviceType in database"));
            break;

        case GET_CAP_FAILED:
            json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(*jobj, ST_REASON, json_object_new_string("failed to get Capability in database"));
            break;

        case GET_SCHEME_FAILED:
            json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(*jobj, ST_REASON, json_object_new_string("failed to get Scheme in database"));
            break;

        case CLASS_NOT_SUPPORT_CMD:
            sprintf(tmp,  "this class does not support %s method, please check it again", command?command:"NULL");

            json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(*jobj, ST_REASON, json_object_new_string(tmp));
            break;

        case DEV_NOT_SUPPORT_CLASS:
            sprintf(tmp, "this device does not support %s class, please list_devices command to check class support", commandClass?commandClass:"NULL");

            json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(*jobj, ST_REASON, json_object_new_string(tmp));
            break;

        case CONFIG_ID_NOT_FOUND:
            json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(*jobj, ST_REASON, json_object_new_string("not found config id"));
            break;

        case DEV_UNACTIVATE:
            json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(*jobj, ST_REASON, json_object_new_string("device unactivate"));
            break;

        case USER_CODE_NAME_INVALID:
            json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(*jobj, ST_REASON, json_object_new_string("name invalid"));
            break;

        case USER_CODE_USERID_INVALID:
            json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(*jobj, ST_REASON, json_object_new_string("userId limit"));
            break;

        case COMMAND_FAILED:
            json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            break;

        default:
            json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_RECEIVED));
            break;
    }

}

static void free_response(zwave_command_response_t response)
{
    SAFE_FREE(response.data0);
    SAFE_FREE(response.data1);
    SAFE_FREE(response.data2);
    SAFE_FREE(response.compliantJson);
}


static void _init_set_onoff(int deviceType, TZWParam *pzwParam, json_object *json_capId, int input_value)
{
    if(!pzwParam)
    {
        return;
    }
    int scheme;
    int array_capId[] = { COMMAND_CLASS_SWITCH_BINARY, 
                          COMMAND_CLASS_SWITCH_MULTILEVEL,
                          COMMAND_CLASS_SWITCH_ALL,
                          COMMAND_CLASS_BASIC};
    int cmd_class = 0;                    
    int class = get_cap_with_priority(json_capId, array_capId, 
                                    sizeof(array_capId)/sizeof(array_capId[0]), &scheme);
    if((deviceType == GENERIC_TYPE_SWITCH_MULTILEVEL) && 
        (class == COMMAND_CLASS_SWITCH_BINARY))
    {
        int scheme_tmp;
        if(check_dev_is_support_cap(json_capId, COMMAND_CLASS_SWITCH_ALL, &scheme_tmp))
        {
            class = COMMAND_CLASS_SWITCH_ALL;
            scheme = scheme_tmp;   
        }
    }

    if(class == COMMAND_CLASS_BASIC)
    {
        cmd_class = BASIC_SET;
    }
    else if(class == COMMAND_CLASS_SWITCH_BINARY)
    {
        cmd_class = SWITCH_BINARY_SET;
    }
    else if(class == COMMAND_CLASS_SWITCH_ALL)
    {
        if(input_value > 0)
        {
            cmd_class = SWITCH_ALL_ON;
        }
        else
        {
            cmd_class = SWITCH_ALL_OFF;
        }
    }
    else if(class == COMMAND_CLASS_SWITCH_MULTILEVEL)
    {
        cmd_class = SWITCH_MULTILEVEL_SET;
    }

    pzwParam->cmd_set.cmd[0]=class;
    pzwParam->cmd_set.cmd[1]=cmd_class;

    pzwParam->scheme=scheme;

    if(class == COMMAND_CLASS_SWITCH_ALL)
    {
        pzwParam->cmd_set.cmd_length=2;
    }
    else
    {
        if(input_value > 0)
        {
            pzwParam->cmd_set.cmd[2] = 0xFF;
        }
        else
        {
            pzwParam->cmd_set.cmd[2] = 0x00;
        }
        pzwParam->cmd_set.cmd_length=3;
    }
    pzwParam->param3=1;
}

static void _init_set_dim(TZWParam *pzwParam, json_object *json_capId, int input_value)
{
    if(!pzwParam)
    {
        return;
    }

    int scheme; 
    int array_capId[] = {COMMAND_CLASS_SWITCH_MULTILEVEL, COMMAND_CLASS_BASIC};
    int class = get_cap_with_priority(json_capId, array_capId, sizeof(array_capId)/sizeof(array_capId[0]), &scheme);
    int cmd_class = 0;                    
    if(class == COMMAND_CLASS_BASIC)
    {
        cmd_class = BASIC_SET;
    }
    else if(class == COMMAND_CLASS_SWITCH_MULTILEVEL)
    {
        cmd_class = SWITCH_MULTILEVEL_SET;
    }

    pzwParam->cmd_set.cmd[0]=class;
    pzwParam->cmd_set.cmd[1]=cmd_class;

    pzwParam->scheme=scheme;
    pzwParam->cmd_set.cmd[2] = input_value;
    pzwParam->cmd_set.cmd_length=3;
    pzwParam->param3=1;

    if(pzwParam->cmd_set.cmd[2] > 99)
    {
        pzwParam->cmd_set.cmd[2] = 99;
    }
}

static int _init_set_thermostat(TZWParam *pzwParam, json_object *json_capId,
                                zwave_command_response_t *response, char* nodeId, char* value)
{
    if(!pzwParam || !nodeId || !value)
    {
        return -1;
    }

    if(!strcmp(response->command, ST_DIM))
    {
        return COMMAND_NOT_SUPPORT;
    }

    int scheme;
    if(check_dev_is_support_cap(json_capId, COMMAND_CLASS_THERMOSTAT_MODE, &scheme))
    {
        pzwParam->scheme=scheme;
    }
    else
    {
        pzwParam->scheme=NON_SEC_SCHEME;
    }

    int modeId = -1;
    strcpy(response->method, ST_WRITE_SPEC_R);
    strcpy(response->command_class, ST_THERMOSTAT_MODE);
    strcpy(response->command, ST_SET);

    pzwParam->cmd_set.cmd[0] = COMMAND_CLASS_THERMOSTAT_MODE;
    pzwParam->cmd_set.cmd[1] = THERMOSTAT_MODE_SET;
    if(!strcmp(value, ST_ON_VALUE))
    {
        SEARCH_DATA_INIT_VAR(thermostat_mode);
        SEARCH_DATA_INIT_VAR(thermostat_previous_mode);

        searching_database(_VR_CB_(zwave), &thermostat_mode,
                            "SELECT register from FEATURES where deviceId='%s' AND featureId='%s'",
                            nodeId, ST_THERMOSTAT_MODE);

        searching_database(_VR_CB_(zwave), &thermostat_previous_mode,
                            "SELECT register from FEATURES where deviceId='%s' AND featureId='%s'",
                            nodeId, ST_THERMOSTAT_PREVIOUS_MODE);
        if(thermostat_mode.len)
        {
            if(!strcmp(thermostat_mode.value, ST_OFF))
            {
                if(thermostat_previous_mode.len && strcmp(thermostat_previous_mode.value, ST_OFF))
                {
                    response->data0 = strdup(thermostat_previous_mode.value);
                }
                else
                {
                    response->data0 = strdup(ST_COOL);
                }
            }
            else
            {
                response->data0 = strdup(thermostat_mode.value);
            }
        }
        else
        {
            if(thermostat_previous_mode.len && strcmp(thermostat_previous_mode.value, ST_OFF))
            {
                response->data0 = strdup(thermostat_mode.value);
            }
            else
            {
                response->data0 = strdup(ST_COOL);
            }
        }
        FREE_SEARCH_DATA_VAR(thermostat_mode);
        FREE_SEARCH_DATA_VAR(thermostat_previous_mode);
    }
    else
    {
        response->data0 = strdup(ST_OFF);
    }

    modeId = get_thermostat_mode_id(response->data0);
    if(modeId == -1)
    {
        SLOGE("does not support this mode %s\n", response->data0);
        return -1;
    }

    pzwParam->cmd_set.cmd[2] = modeId;
    pzwParam->cmd_set.cmd_length=3;
    pzwParam->param3=1;
    pzwParam->ret=-1;

    return 0;
}

int _set_binary_prepare(zwave_command_response_t response)
{
    int res = 0;
    char *nodeid = &response.nodeid[0];
    char *command = &response.command[0];
    char *value = &response.value[0];

    zwave_dev_info_t *dev = get_zwave_dev_from_id(nodeid);
    if(!dev)
    {
        dev = create_zwave_dev_from_id(nodeid);
        if(!dev)
        {
            return DEV_NOT_FOUND;
        }
    }
    
    if(!dev->active)
    {
        return DEV_UNACTIVATE;
    }

    if(!strlen(dev->serialId))
    {
        SEARCH_DATA_INIT_VAR(serialId);
        searching_database(_VR_CB_(zwave), &serialId, 
                "SELECT serialId from SUB_DEVICES where id='%s'", nodeid);
        strncpy(dev->serialId, serialId.value, sizeof(dev->serialId)-1);
        FREE_SEARCH_DATA_VAR(serialId);
    }

    json_object *json_capId = dev->json_capId;
    if(!json_capId)
    {
        get_capId_and_scheme_dev((char*)nodeid, &json_capId);
        if(!json_capId)
        {
            return GET_CAP_FAILED;
        }
        dev->json_capId = json_capId;
    }

    TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));
    memset(pzwParam, 0x00, sizeof(TZWParam));

    pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_REQUEST_SPECIFICATION_ZPC;
    pzwParam->param2=dev->localId;
    pzwParam->src_endpoint=0;
    pzwParam->dest_endpoint=dev->endpointNum;

    if(dev->deviceType == GENERIC_TYPE_THERMOSTAT)
    {
        res = _init_set_thermostat(pzwParam, json_capId, &response, nodeid, value);
        if(res)
        {
            goto done;
        }
    }
    else if(dev->deviceType == GENERIC_TYPE_ENTRY_CONTROL) //Door lock or gara
    {
        int scheme;
        if(check_dev_is_support_cap(json_capId, COMMAND_CLASS_DOOR_LOCK, &scheme))
        {
            pzwParam->scheme=scheme;
            pzwParam->cmd_set.cmd[0] = COMMAND_CLASS_DOOR_LOCK;
            pzwParam->cmd_set.cmd[1] = DOOR_LOCK_OPERATION_SET;
            
            if(strtol(value,NULL,0) == OPEN)
            {
                pzwParam->cmd_set.cmd[2] = DOOR_LOCK_OPERATION_SET_DOOR_UNSECURED;
            }
            else
            {
                pzwParam->cmd_set.cmd[2] = DOOR_LOCK_OPERATION_SET_DOOR_SECURED;
            }

            pzwParam->cmd_set.cmd_length=3;
            pzwParam->param3=1;
            pzwParam->ret=-1;
        }
        else if(check_dev_is_support_cap(json_capId, COMMAND_CLASS_BARRIER_OPERATOR, &scheme))
        {
            pzwParam->scheme=scheme;
            pzwParam->cmd_set.cmd[0] = COMMAND_CLASS_BARRIER_OPERATOR; 
            pzwParam->cmd_set.cmd[1] = BARRIER_OPERATOR_SET;
            if(strtol(value,NULL,0) == OPEN)
            {
                pzwParam->cmd_set.cmd[2] = BARRIER_OPERATOR_SET_OPEN;
            }
            else
            {
                pzwParam->cmd_set.cmd[2] = BARRIER_OPERATOR_SET_CLOSE;
            }
            pzwParam->cmd_set.cmd_length=3;
            pzwParam->param3=1;

            pzwParam->ret=-1;
        }
        else
        {
            res = -1;
            goto done;
        }
    }
    else
    {
        uint8_t input_value = (uint8_t)strtol(value,NULL,0);
        if(!strcmp(command, ST_ON_OFF))
        {
            /*set dim fisrt after power cycle*/
            if(dev->status.dimFirst)
            {
                dev->status.dimFirst = false;
                if(dev->status.dim)
                {
                    uint8_t input_value = dev->status.dim;
                    _init_set_dim(pzwParam, json_capId, input_value);
                    response.cmdType = COMMAND_TYPE_SET_DIM;
                    response.cmdValue = input_value;
                }
                else
                {
                    _init_set_onoff(dev->deviceType, pzwParam, json_capId, input_value);
                    response.cmdType = COMMAND_TYPE_SET_ONOFF;
                    response.cmdValue = input_value?1:0;
                }
            }
            else
            {
                _init_set_onoff(dev->deviceType, pzwParam, json_capId, input_value);
                response.cmdType = COMMAND_TYPE_SET_ONOFF;
                response.cmdValue = input_value?1:0;
            }
        }
        else if(!strcmp(command, ST_DIM))
        {
            if(dev->status.dimFirst)
            {
                dev->status.dimFirst = false;
            }
            _init_set_dim(pzwParam, json_capId, input_value);
            response.cmdType = COMMAND_TYPE_SET_DIM;
            response.cmdValue = input_value;
        }

        pzwParam->ret=-1;
    }

    if(pzwParam->cmd_set.cmd[0] == COMMAND_CLASS_SWITCH_BINARY
        && pzwParam->cmd_set.cmd[1] == SWITCH_BINARY_SET
        )
    {
        pzwParam->cmd_set.cmd[2] = invert_status(nodeid, pzwParam->cmd_set.cmd[2]);
    }

    insert_to_queue(response, pzwParam, dev);
done:
    SAFE_FREE(pzwParam);
    return res;
}

static int setbinary(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    int res = 0;
    struct blob_attr *tb[3];

    const char *nodeid = ST_UNKNOWN;
    const char *command = ST_UNKNOWN;
    const char *value = ST_UNKNOWN;

    blobmsg_parse(setbinary_policy, ARRAY_SIZE(setbinary_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0] && tb[1] && tb[2])
    {
        nodeid = blobmsg_data(tb[0]);
        command = blobmsg_data(tb[1]);
        value = blobmsg_data(tb[2]);
    }
    else
    {
        return 0;
    }

    SLOGI("nodeid = %s\n", nodeid);
    SLOGI("command = %s\n", command);
    SLOGI("value = %s\n", value);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SET_BINARY_R));
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));
    json_object_object_add(jobj, ST_COMMAND, json_object_new_string(command));
    json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
    
    zwave_command_response_t response;
    memset(&response, 0x00, sizeof(zwave_command_response_t));
    strcpy(response.method, ST_SET_BINARY_R);
    strcpy(response.nodeid, nodeid);
    strcpy(response.command, command);
    strcpy(response.value, value);

    res = _set_binary_prepare(response);
    if(res)
    {
        free_response(response);
    }
    return_json_message(res, &jobj, (char*)command, NULL);

    // VR_(usleep)(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

int _get_binary_prepare(zwave_command_response_t response)
{
    int res = 0;
    char *nodeid = &response.nodeid[0];
    zwave_dev_info_t *dev = get_zwave_dev_from_id(nodeid);
    if(!dev)
    {
        dev = create_zwave_dev_from_id(nodeid);
        if(!dev)
        {
            return DEV_NOT_FOUND;
        }
    }

    if(!dev->active)
    {
        return DEV_UNACTIVATE;
    }

    json_object *json_capId = dev->json_capId;
    if(!json_capId)
    {
        get_capId_and_scheme_dev((char*)nodeid, &json_capId);
        if(!json_capId)
        {
            return GET_CAP_FAILED;
        }
        dev->json_capId = json_capId;
    }

    int scheme=NON_SEC_SCHEME;
    TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));
    memset(pzwParam, 0x00, sizeof(TZWParam));

    pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_REQUEST_SPECIFICATION_ZPC;
    pzwParam->param2=htoi(nodeid);
    pzwParam->src_endpoint=0;
    pzwParam->dest_endpoint=dev->endpointNum;

    if(dev->deviceType == GENERIC_TYPE_ENTRY_CONTROL)
    {
        if(check_dev_is_support_cap(json_capId, COMMAND_CLASS_DOOR_LOCK, &scheme))
        {
            pzwParam->scheme=scheme;
            pzwParam->cmd_set.cmd[0] = COMMAND_CLASS_DOOR_LOCK; 
            pzwParam->cmd_set.cmd[1] = DOOR_LOCK_OPERATION_GET;
            pzwParam->cmd_set.cmd_length=2;
            pzwParam->param3=1;
            pzwParam->ret=-1;
        }
        else if(check_dev_is_support_cap(json_capId, COMMAND_CLASS_BARRIER_OPERATOR, &scheme))
        {
            pzwParam->scheme=scheme;
            pzwParam->cmd_set.cmd[0] = COMMAND_CLASS_BARRIER_OPERATOR; 
            pzwParam->cmd_set.cmd[1] = BARRIER_OPERATOR_GET;
            pzwParam->cmd_set.cmd_length=2;
            pzwParam->param3=1;
            pzwParam->ret=-1;
        }
        else
        {
            res = -1;
            goto done;
        }
    }
    else
    {
        // get highest scheme
        if(response.command_class && !strcmp(response.command_class, ST_SWITCH_BINARY))
        {
            pzwParam->cmd_set.cmd[0] = COMMAND_CLASS_SWITCH_BINARY;
            pzwParam->cmd_set.cmd[1] = SWITCH_BINARY_GET;
        }
        else
        {
            int array_capId[] = {COMMAND_CLASS_SWITCH_BINARY, COMMAND_CLASS_SWITCH_MULTILEVEL, COMMAND_CLASS_BASIC};
            int class = get_cap_with_priority(json_capId, array_capId, sizeof(array_capId)/sizeof(array_capId[0]), &scheme);
            int cmd_class = 0;
            if(class == COMMAND_CLASS_BASIC)
            {
                cmd_class = BASIC_GET;
            }
            else if(class == COMMAND_CLASS_SWITCH_BINARY)
            {
                cmd_class = SWITCH_BINARY_GET;
            }
            else if(class == COMMAND_CLASS_SWITCH_MULTILEVEL)
            {
                cmd_class = SWITCH_MULTILEVEL_GET;
            }
            pzwParam->cmd_set.cmd[0]=class;
            pzwParam->cmd_set.cmd[1]=cmd_class;
        }
        pzwParam->scheme=scheme;
        pzwParam->cmd_set.cmd_length=2;
        pzwParam->param3=1;
        pzwParam->ret=-1;
    }
    
    insert_to_queue(response, pzwParam, dev);
done:
    SAFE_FREE(pzwParam);
    return res;
}

static int getbinary(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    int res = 0;
    struct blob_attr *tb[1];
    const char *nodeid = ST_UNKNOWN;
    blobmsg_parse(get_policy, ARRAY_SIZE(get_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
    {
        nodeid = blobmsg_data(tb[0]);
    }
    else
    {
        return 0;
    }

    SLOGI("nodeid = %s\n", nodeid);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_BINARY_R));
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));

    zwave_command_response_t response;
    memset(&response, 0x00, sizeof(zwave_command_response_t));
    strcpy(response.method, ST_GET_BINARY_R);
    strcpy(response.nodeid, nodeid);

    res = _get_binary_prepare(response);
    return_json_message(res, &jobj, NULL, NULL);

    // VR_(usleep)(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

int get_configuration_data_from_name(char *nodeid, char *name, char *configId, char *configLength)
{
    if(!name || !configId)
    {
        return -1;
    }

    SEARCH_DATA_INIT_VAR(serialId);
    searching_database(_VR_CB_(zwave), &serialId, 
                        "SELECT serialId from SUB_DEVICES where id='%s'", nodeid);
    if(!serialId.len)
    {
        SLOGE("Failed to get serialId of device %s from database\n", nodeid);
        FREE_SEARCH_DATA_VAR(serialId);
        return -1;
    }

    SEARCH_DATA_INIT_VAR(initConfiguration);
    searching_database("zwave_handler", support_devs_db, zwave_cb, &initConfiguration, 
                            "SELECT InitConfiguration from SUPPORT_DEVS where SerialID='%s'", 
                            serialId.value);
    if(!initConfiguration.len)
    {
        SLOGE("Failed to get initConfiguration of device %s from database\n", serialId.value);
        FREE_SEARCH_DATA_VAR(serialId);
        FREE_SEARCH_DATA_VAR(initConfiguration);
        return -1;
    }

    json_object *initConfigObj = VR_(create_json_object)(initConfiguration.value);
    if(!initConfigObj)
    {
        SLOGE("Failed to create json object %s\n", initConfiguration.value);
        FREE_SEARCH_DATA_VAR(serialId);
        FREE_SEARCH_DATA_VAR(initConfiguration);
        return -1;
    }

    enum json_type type = json_object_get_type(initConfigObj);
    if(type != json_type_array)
    {
        SLOGE("initConfigObj is not array\n");
        json_object_put(initConfigObj);
        FREE_SEARCH_DATA_VAR(serialId);
        FREE_SEARCH_DATA_VAR(initConfiguration);
        return -1;
    }

    int arrayLength = json_object_array_length(initConfigObj);
    int i, res=-1;
    for(i=0; i<arrayLength; i++)
    {
        json_object *jvalue = json_object_array_get_idx(initConfigObj, i);
        if(jvalue)
        {
            json_object *configNameObj = json_object_object_get(jvalue, ST_CONFIG_NAME);
            if(!configNameObj)
            {
                continue;
            }

            const char *configName = json_object_get_string(configNameObj);
            if(strcmp(name, configName))
            {
                continue;
            }

            json_object *configIdObj = json_object_object_get(jvalue, ST_CONFIG_ID);
            json_object *configLengthObj = json_object_object_get(jvalue, ST_CONFIG_LENGTH);
            if(!configIdObj || !configLengthObj)
            {
                continue;
            }

            strcpy(configId, json_object_get_string(configIdObj));
            if(configLength)
            {
                strcpy(configLength, json_object_get_string(configLengthObj));
            }
            res = 0;
            break;
        }
        else
        {
            SLOGE("jvalue null\n");
        }
    }

    json_object_put(initConfigObj);
    FREE_SEARCH_DATA_VAR(serialId);
    FREE_SEARCH_DATA_VAR(initConfiguration);
    return res;
}

int get_configuration_data_from_id(char *nodeid, uint8_t id, char *configName)
{
    if(!configName)
    {
        return -1;
    }

    SEARCH_DATA_INIT_VAR(serialId);
    searching_database(_VR_CB_(zwave), &serialId, 
                        "SELECT serialId from SUB_DEVICES where id='%s'", nodeid);
    if(!serialId.len)
    {
        SLOGE("Failed to get serialId of device %s from database\n", nodeid);
        FREE_SEARCH_DATA_VAR(serialId);
        return -1;
    }
    SEARCH_DATA_INIT_VAR(initConfiguration);
    searching_database("zwave_handler", support_devs_db, zwave_cb, &initConfiguration, 
                            "SELECT InitConfiguration from SUPPORT_DEVS where SerialID='%s'", 
                            serialId.value);
    if(!initConfiguration.len)
    {
        SLOGE("Failed to get initConfiguration of device %s from database\n", serialId.value);
        FREE_SEARCH_DATA_VAR(serialId);
        FREE_SEARCH_DATA_VAR(initConfiguration);
        return -1;
    }

    json_object *initConfigObj = VR_(create_json_object)(initConfiguration.value);
    if(!initConfigObj)
    {
        SLOGE("Failed to create json object %s\n", initConfiguration.value);
        FREE_SEARCH_DATA_VAR(serialId);
        FREE_SEARCH_DATA_VAR(initConfiguration);
        return -1;
    }

    enum json_type type = json_object_get_type(initConfigObj);
    if(type != json_type_array)
    {
        SLOGE("initConfigObj is not array\n");
        json_object_put(initConfigObj);
        FREE_SEARCH_DATA_VAR(serialId);
        FREE_SEARCH_DATA_VAR(initConfiguration);
        return -1;
    }

    int arrayLength = json_object_array_length(initConfigObj);
    int i,res=-1;
    for(i=0; i<arrayLength; i++)
    {
        json_object *jvalue = json_object_array_get_idx(initConfigObj, i);
        if(jvalue)
        {
            json_object *configIdObj = json_object_object_get(jvalue, ST_CONFIG_ID);
            if(!configIdObj)
            {
                continue;
            }

            uint8_t configId = strtol(json_object_get_string(configIdObj), NULL, 0);
            if(configId != id)
            {
                continue;
            }

            json_object *configNameObj = json_object_object_get(jvalue, ST_CONFIG_NAME);
            if(!configNameObj)
            {
                continue;
            }

            strcpy(configName, json_object_get_string(configNameObj));
            res=0;
            break;
        }
    }
    json_object_put(initConfigObj);
    FREE_SEARCH_DATA_VAR(serialId);
    FREE_SEARCH_DATA_VAR(initConfiguration);
    return res;
}

int _get_specification_prepare(zwave_command_response_t response)
{
    int res = 0, i,j;
    class_command *class_cmd = NULL;

    char *nodeid = &response.nodeid[0];
    char *CommandClass = &response.command_class[0];
    char *Command = &response.command[0];
    char *data0 = response.data0;
    char *data1 = response.data1;
    //char *data2 = response.data2;

    zwave_dev_info_t *dev = get_zwave_dev_from_id(nodeid);
    if(!dev)
    {
        dev = create_zwave_dev_from_id(nodeid);
        if(!dev)
        {
            return DEV_NOT_FOUND;
        }
    }

    if(!dev->active)
    {
        return DEV_UNACTIVATE;
    }

    json_object *json_capId = dev->json_capId;
    if(!json_capId)
    {
        get_capId_and_scheme_dev((char*)nodeid, &json_capId);
        if(!json_capId)
        {
            return GET_CAP_FAILED;
        }
        dev->json_capId = json_capId;
    }

    int scheme = NON_SEC_SCHEME;
    TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));
    memset(pzwParam, 0x00, sizeof(TZWParam));

    pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_REQUEST_SPECIFICATION_ZPC;
    pzwParam->param2=dev->localId;
    pzwParam->src_endpoint=0;
    pzwParam->dest_endpoint=dev->endpointNum;
    pzwParam->param3=0x01;

    // if(data2)
    // {
    //     if (strtol(data2,NULL,0)==1)
    //     {
    //         pzwParam->param3=0x01;
    //     }
    // }

    for(i =0; i < sizeof(pointer_str)/sizeof(struct_point); i++)
    {
        if(!strcmp(pointer_str[i].struct_name, CommandClass))
        {
            class_cmd = pointer_str[i].class_cmd;
            pzwParam->cmd_set.cmd[0]= pointer_str[i].class_id;
            for(j=0; j < pointer_str[i].size; j++)
            {
                if(!strcmp(class_cmd[j].cmd_name, Command))
                {
                    pzwParam->cmd_set.cmd[1]= class_cmd[j].cmd_id;
                    if(!check_dev_is_support_cap(json_capId, pointer_str[i].class_id, &scheme))
                    {
                        res = GET_SCHEME_FAILED;
                        goto getSpecPreDone;
                    }
                    pzwParam->scheme=scheme;
                    break;
                }
            }
            if(j >= pointer_str[i].size)
            {

                res = CLASS_NOT_SUPPORT_CMD;
                goto getSpecPreDone;
            }
            break;
        }
    }

    if(!class_cmd)
    {
        res = DEV_NOT_SUPPORT_CLASS;
        goto getSpecPreDone;
    }

    i=2;
    if(!strcmp(CommandClass, ST_SENSOR_MULTILEVEL))
    {
        int sensorId = get_sensor_id(data0, NULL, NULL);
        if(sensorId == -1)
        {
            SLOGE("SENSOR_MULTILEVEL does not support this mode %s\n", data0);
            res = COMMAND_FAILED;
            goto getSpecPreDone;
        }
        pzwParam->cmd_set.cmd[i++] = sensorId;
    }
    else if(!strcmp(CommandClass, ST_METER))
    {
        if(strcmp(Command, ST_SUPPORTED_GET))
        {
            /*data0 is meter type, data1 is unit*/
            int meterUnit = -1;
            get_meter_id(data0, data1, &meterUnit, NULL);
            // int meterUnit = get_meter_unit_id(data0);
            if(meterUnit == -1)
            {
                SLOGE("METER does not support meter type %s with unit %s\n", data0, data1);
                res = COMMAND_FAILED;
                goto getSpecPreDone;
            }

            SLOGI("meterUnit = %d\n", meterUnit);
            if(meterUnit > SCALE_BIT_MASK)
            {
                pzwParam->cmd_set.cmd[i++] = (uint8_t)((SCALE_BIT_MASK) << SCALE_POSITION);
                pzwParam->cmd_set.cmd[i++] = (uint8_t)(meterUnit);
            }
            else
            {
                pzwParam->cmd_set.cmd[i++] = (uint8_t)((meterUnit & SCALE_BIT_MASK) << SCALE_POSITION);
            }
        }
    }
    else if(!strcmp(CommandClass, ST_THERMOSTAT_SETPOINT))
    {
        /*only command get need data0 is thermostat setpoint mode
        supportedGet doesnot need*/
        if(!strcmp(Command, ST_GET))
        {
            int modeId = get_thermostat_setpoint_id(data0);
            if(modeId == -1)
            {
                SLOGE("THERMOSTAT_SETPOINT does not support this mode %s\n", data0);
                res = COMMAND_FAILED;
                goto getSpecPreDone;
            }

            pzwParam->cmd_set.cmd[i++] = modeId;
        }
    }  
    else if(!strcmp(CommandClass, ST_CONFIGURATION))
    {
        // pzwParam->cmd_set.cmd[i++]= (uint8_t)strtol(data0, NULL, 0);
        char configId[SIZE_32B]={0};
        if(!get_configuration_data_from_name((char*)nodeid, (char*)data0, configId, NULL))
        {
            if(*configId)
            {
                pzwParam->cmd_set.cmd[i++]=strtol(configId, NULL, 0);
            }
            else
            {
                res = CONFIG_ID_NOT_FOUND;
                goto getSpecPreDone;
            }
        }
        else
        {
            res = CONFIG_ID_NOT_FOUND;
            goto getSpecPreDone;
        }
    }
    else if(!strcmp(CommandClass, ST_ASSOCIATION))
    {
        /*groupingsGet does not need data0,
        only get node list of group id need data0(groupId)*/
        if(!strcmp(Command, ST_GET))
        {
            if(data0)
            {
                pzwParam->cmd_set.cmd[i++] = (uint8_t)strtol(data0, NULL, 0);
            }
        }
    }
    else if(!strcmp(CommandClass, ST_BARRIER_OPERATOR))
    {
        if(!strcmp(Command, ST_SIGNAL_GET))
        {
            if(data0)
            {
                char *signalName = data0;
                int signalId = get_barrier_signal_id(signalName);
                if(signalId == -1)
                {
                    SLOGE("BARRIER_OPERATOR does not support subsystem type %s\n", signalName);
                    res = COMMAND_FAILED;
                    goto getSpecPreDone;
                }
                pzwParam->cmd_set.cmd[i++] = (uint8_t)signalId;
            }
        }
    }
    else if(!strcmp(CommandClass, ST_ASSOCIATION_GRP_INFO))
    {
        if(!strcmp(Command, ST_GROUP_NAME_GET))
        {
            if(data0)
            {
                char *groupId = data0;
                pzwParam->cmd_set.cmd[i++] = (uint8_t)strtol(groupId, NULL, 0);
            }
        }
        else if(!strcmp(Command, ST_GROUP_COMMAND_LIST_GET))
        {
            if(data0 && data1)
            {
                char *allowCache = data0;
                char *groupId = data1;
                pzwParam->cmd_set.cmd[i++] = ((strtol(allowCache, NULL, 0) & 0x01) << 7);
                pzwParam->cmd_set.cmd[i++] = strtol(groupId, NULL, 0);
            }
        }
    }
    else
    {
        if(data0)
        {
            pzwParam->cmd_set.cmd[i++] = (uint8_t)strtol(data0, NULL, 0);
        }
    }

    // if (strcmp(CommandClass, ST_THERMOSTAT_SETPOINT) && strcmp(data1, ST_UNKNOWN))
    // {
    //     pzwParam->cmd_set.cmd[i++]= (uint8_t)strtol(data1, NULL, 0);
    // }
    
    pzwParam->cmd_set.cmd_length=i;
    
    insert_to_queue(response, pzwParam, dev);

getSpecPreDone:
    SAFE_FREE(pzwParam);
    return res;
}

static int getspecification(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    int res = 0;
    struct blob_attr *tb[6];
    const char *nodeid = ST_UNKNOWN;
    const char *CommandClass = ST_UNKNOWN;
    const char *Command = ST_UNKNOWN;
    const char *data0 = ST_UNKNOWN;
    const char *data1 = ST_UNKNOWN;

    blobmsg_parse(get_specification, ARRAY_SIZE(get_specification), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_READ_SPEC_R));
    
    if (tb[0] && tb[1] && tb[2])
    {
        nodeid = blobmsg_data(tb[0]);
        CommandClass = blobmsg_data(tb[1]);
        Command = blobmsg_data(tb[2]);
        if(tb[3])
        {
            data0 = blobmsg_data(tb[3]);
        }
        if(tb[4])
        {
            data1 = blobmsg_data(tb[4]);            
        }
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        if(!tb[0]) 
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: nodeid"));
        }
        else if(!tb[1])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: CommandClass"));
        }
        else if(!tb[2])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: Command"));
        }
        else if(!tb[3])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: data0"));
        }

        // VR_(usleep)(SLEEP_TIME);
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
        ubus_send_reply(ctx, req, buff.head);
        return 0;
    }

    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));

    json_object *commandinfo = json_object_new_object();
    json_object_object_add(commandinfo, ST_CLASS, json_object_new_string(CommandClass));
    json_object_object_add(commandinfo, ST_COMMAND, json_object_new_string(Command));
    if(tb[3])
    {
        json_object_object_add(commandinfo, ST_DATA0, json_object_new_string(data0));
    }
    if(tb[4])
    {
        json_object_object_add(commandinfo, ST_DATA1, json_object_new_string(data1));
    }
    if(tb[5])
    {
        json_object_object_add(commandinfo, ST_DATA2, json_object_new_string(blobmsg_data(tb[5])));
    }
    json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

    SLOGI("nodeid = %s\n", nodeid);
    SLOGI("CommandClass = %s\n", CommandClass);
    SLOGI("Command = %s\n", Command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);

    zwave_command_response_t response;
    memset(&response, 0x00, sizeof(zwave_command_response_t));
    strcpy(response.method, ST_READ_SPEC_R);
    strcpy(response.nodeid, nodeid);
    strcpy(response.command_class, CommandClass);
    strcpy(response.command, Command);
    if(tb[3])
    {
        response.data0 = (char *)malloc(strlen(data0)+1);
        strcpy(response.data0, data0);
    }
    if(tb[4])
    {
        response.data1 = (char *)malloc(strlen(data1)+1);
        strcpy(response.data1, data1);
    }
    if(tb[5])
    {
        response.data2 = (char *)malloc(strlen(blobmsg_data(tb[5]))+1);
        strcpy(response.data2, blobmsg_data(tb[5]));
    }

    res = _get_specification_prepare(response);
    if(res)
    {
        free_response(response);
    }
    return_json_message(res, &jobj, (char*)Command, (char*)CommandClass);

    // VR_(usleep)(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

static int getspecification_CRC(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    return 0;
}

int _set_specification_prepare(zwave_command_response_t response)
{
    int res = COMMAND_FAILED, i,j;
    class_command *class_cmd = NULL;

    char *nodeid = &response.nodeid[0];
    char *CommandClass = &response.command_class[0];
    char *Command = &response.command[0];
    char *data0 = response.data0;
    char *data1 = response.data1;
    char *data2 = response.data2;

    zwave_dev_info_t *dev = get_zwave_dev_from_id(nodeid);
    if(!dev)
    {
        dev = create_zwave_dev_from_id(nodeid);
        if(!dev)
        {
            return DEV_NOT_FOUND;
        }
    }

    if(!dev->active)
    {
        return DEV_UNACTIVATE;
    }

    if(!strlen(dev->serialId))
    {
        SEARCH_DATA_INIT_VAR(serialId);
        searching_database(_VR_CB_(zwave), &serialId, 
                "SELECT serialId from SUB_DEVICES where id='%s'", nodeid);
        strncpy(dev->serialId, serialId.value, sizeof(dev->serialId)-1);
        FREE_SEARCH_DATA_VAR(serialId);
    }

    json_object *json_capId = dev->json_capId;
    if(!json_capId)
    {
        get_capId_and_scheme_dev((char*)nodeid, &json_capId);
        if(!json_capId)
        {
            return GET_CAP_FAILED;
        }
        dev->json_capId = json_capId;
    }

    int scheme = NON_SEC_SCHEME;
    TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));
    memset(pzwParam, 0x00, sizeof(TZWParam));

    pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_REQUEST_SPECIFICATION_ZPC;
    pzwParam->param2=dev->localId;
    pzwParam->src_endpoint=0;
    pzwParam->dest_endpoint=dev->endpointNum;
    pzwParam->param3=0x01;
    
    // if(strcmp(CommandClass, ST_SWITCH_COLOR))
    // {
    //     if(data2)
    //     {
    //         if(strtol(data2,NULL,0)==1)
    //         {
    //             pzwParam->param3=0x01;
    //         }
    //     }
    // }
    
    for(i =0; i < sizeof(pointer_str)/sizeof(struct_point); i++)
    {
        if(!strcmp(pointer_str[i].struct_name, CommandClass))
        {
            class_cmd = pointer_str[i].class_cmd;
            pzwParam->cmd_set.cmd[0]= pointer_str[i].class_id;
            for(j=0; j < pointer_str[i].size; j++)
            {
                if(!strcmp(class_cmd[j].cmd_name, Command))
                {
                    pzwParam->cmd_set.cmd[1]= class_cmd[j].cmd_id;
                    if(!check_dev_is_support_cap(json_capId, pointer_str[i].class_id, &scheme))
                    {
                        res = GET_SCHEME_FAILED;
                        goto setSpecPreDone;
                    }
                    pzwParam->scheme=scheme;
                    break;
                }
            }

            if(j == pointer_str[i].size)
            {
                res = CLASS_NOT_SUPPORT_CMD;
                goto setSpecPreDone;
            }
            break;
        }
    }

    if(!class_cmd)
    {
        res = DEV_NOT_SUPPORT_CLASS;
        goto setSpecPreDone;
    }

    i=2;

    if(!strcmp(CommandClass, ST_SWITCH_COLOR))
    {
        json_object *color = VR_(create_json_object)(data0);
        if(!color)
        {
            SLOGI("color %s is not json format\n", data0);
            goto setSpecPreDone;
        }

        i=i+1+MAX_NUMBER_COMPONENT*2;
        pzwParam->cmd_set.cmd[NUMBER_COMPONENT_INDEX]=MAX_NUMBER_COMPONENT;
        json_object_object_foreach(color, key, value)
        {
            if(!strcmp(key, RGB_COLOR))
            {
                int color = json_object_get_int(value);
                pzwParam->cmd_set.cmd[RED_INDEX]=RED;
                pzwParam->cmd_set.cmd[RED_INDEX+1]=(color>>16)&0xFF;
                pzwParam->cmd_set.cmd[GREEN_INDEX]=GREEN;
                pzwParam->cmd_set.cmd[GREEN_INDEX+1]=(color>>8)&0xFF;
                pzwParam->cmd_set.cmd[BLUE_INDEX]=BLUE;
                pzwParam->cmd_set.cmd[BLUE_INDEX+1]=color;

                pzwParam->cmd_set.cmd[WARM_WHITE_INDEX]=WARM_WHITE;
                pzwParam->cmd_set.cmd[WARM_WHITE_INDEX+1]=0;
                pzwParam->cmd_set.cmd[COOL_WHITE_INDEX]=COOL_WHITE;
                pzwParam->cmd_set.cmd[COOL_WHITE_INDEX+1]=0;
            }
            else if(!strcmp(key, TEMP_COLOR))
            {
                int cw,ww;
                dev_specific_temperature_color(nodeid, json_object_get_int(value), &ww, &cw);

                pzwParam->cmd_set.cmd[WARM_WHITE_INDEX]=WARM_WHITE;
                pzwParam->cmd_set.cmd[WARM_WHITE_INDEX+1]=ww;
                pzwParam->cmd_set.cmd[COOL_WHITE_INDEX]=COOL_WHITE;
                pzwParam->cmd_set.cmd[COOL_WHITE_INDEX+1]=cw;

                pzwParam->cmd_set.cmd[RED_INDEX]=RED;
                pzwParam->cmd_set.cmd[RED_INDEX+1]=0;
                pzwParam->cmd_set.cmd[GREEN_INDEX]=GREEN;
                pzwParam->cmd_set.cmd[GREEN_INDEX+1]=0;
                pzwParam->cmd_set.cmd[BLUE_INDEX]=BLUE;
                pzwParam->cmd_set.cmd[BLUE_INDEX+1]=0;
            }
        }

        json_object_put(color);
    }
    else if(!strcmp(CommandClass,ST_THERMOSTAT_SETPOINT))
    {
        float temp_input = 0;
        int precision, scale, size;
        const char *unit = ST_MULTILEVEL_SENSOR_LABEL_CELCIUS;

        if(data1)
        {
            temp_input = strtod(data1,NULL);
        }

        if(data2)
        {
            unit = data2;
        }

        get_float_format(dev->serialId, nodeid, ST_THERMOSTAT_SETPOINT_FORMAT, 
                        &precision, &scale, &size, unit);

        int set_point_temp = temp_input*pow(10, precision);
        printf("temp_input = %f\n", temp_input);
        printf("set_point_temp = %d\n", set_point_temp);

        int modeId = get_thermostat_setpoint_id(data0);
        if(modeId == -1)
        {
            SLOGE("THERMOSTAT_SETPOINT does not support this mode %s\n", data0);
            res = COMMAND_FAILED;
            goto setSpecPreDone;
        }

        pzwParam->cmd_set.cmd[i++] = modeId;

        pzwParam->cmd_set.cmd[i++]=((precision&0x07)<<5) + ((scale&0x03)<<3)+ ((size&0x07)<<0);

        for (j=size-1;j>=0;j--)
        {
            pzwParam->cmd_set.cmd[i++]=(uint8_t)(set_point_temp>>(8*j));
        }
    }
    else if(!strcmp(CommandClass,ST_WAKE_UP))
    {
        if(!data0 || !data1)
        {
            goto setSpecPreDone;
        }

        int wake_up_interval=(int)strtol(data0, NULL, 0);

        pzwParam->cmd_set.cmd[i++]=(wake_up_interval>>16)&0xFF;
        pzwParam->cmd_set.cmd[i++]=(wake_up_interval>>8)&0xFF;
        pzwParam->cmd_set.cmd[i++]=(wake_up_interval>>0)&0xFF;

        int report_node_id=(int)strtol(data1, NULL, 16);//nodeId in hex
        pzwParam->cmd_set.cmd[i++]=(uint8_t)report_node_id;
    }
    else if(!strcmp(CommandClass,ST_THERMOSTAT_FAN_MODE))
    {
        int modeId = get_thermostat_fan_mode_id(data0);
        if(modeId == -1)
        {
            SLOGE("does not support this mode %s\n", data0);
            goto setSpecPreDone;
        }

        pzwParam->cmd_set.cmd[i++] = modeId;
    }
    else if(!strcmp(CommandClass,ST_THERMOSTAT_MODE))
    {
        if(!data0)
        {
            goto setSpecPreDone;
        }

        int modeId = -1;
        if(!strcmp(data0, ST_ON))
        {
            SEARCH_DATA_INIT_VAR(thermostat_mode);
            SEARCH_DATA_INIT_VAR(thermostat_previous_mode);

            searching_database(_VR_CB_(zwave), &thermostat_mode, 
                                "SELECT register from FEATURES where deviceId='%s' AND featureId='%s'", 
                                nodeid, ST_THERMOSTAT_MODE);

            searching_database(_VR_CB_(zwave), &thermostat_previous_mode, 
                                "SELECT register from FEATURES where deviceId='%s' AND featureId='%s'", 
                                nodeid, ST_THERMOSTAT_PREVIOUS_MODE);
            if(thermostat_mode.len)
            {
                if(!strcmp(thermostat_mode.value, ST_OFF))
                {
                    if(thermostat_previous_mode.len && strcmp(thermostat_previous_mode.value, ST_OFF))
                    {
                        strcpy(data0, thermostat_previous_mode.value);
                    }
                    else
                    {
                        strcpy(data0, ST_COOL);
                    }
                }
                else
                {
                    strcpy(data0, thermostat_mode.value);
                }
            }
            else
            {
                if(thermostat_previous_mode.len && strcmp(thermostat_previous_mode.value, ST_OFF))
                {
                    strcpy(data0, thermostat_previous_mode.value);
                }
                else
                {
                    strcpy(data0, ST_COOL);
                }
            }
            FREE_SEARCH_DATA_VAR(thermostat_mode);
            FREE_SEARCH_DATA_VAR(thermostat_previous_mode);
        }

        modeId = get_thermostat_mode_id(data0);

        if(modeId == -1)
        {
            SLOGE("does not support this mode %s\n", data0);
            goto setSpecPreDone;
        }
        pzwParam->cmd_set.cmd[i++] = modeId;
    }
    else if(!strcmp(CommandClass,ST_POWER_LEVEL))
    {
        if(!data0)
        {
            goto setSpecPreDone;
        }

        if (!strcmp(Command, ST_SET))
        {
            if(!strcmp(data0, ST_NORMAL_POWER))
            {
                pzwParam->cmd_set.cmd[i++]= POWERLEVEL_REPORT_NORMALPOWER;
            }else if(!strcmp(data0, "minus1dBm"))
            {
                pzwParam->cmd_set.cmd[i++]= POWERLEVEL_REPORT_MINUS1DBM;
            }else if(!strcmp(data0, "minus2dBm"))
            {
                pzwParam->cmd_set.cmd[i++]= POWERLEVEL_REPORT_MINUS2DBM;
            }else if(!strcmp(data0, "minus3dBm"))
            {
                pzwParam->cmd_set.cmd[i++]= POWERLEVEL_REPORT_MINUS3DBM;
            }else if(!strcmp(data0, "minus4dBm"))
            {
                pzwParam->cmd_set.cmd[i++]= POWERLEVEL_REPORT_MINUS4DBM;
            }
            else if(!strcmp(data0, "minus5dBm"))
            {
                pzwParam->cmd_set.cmd[i++]= POWERLEVEL_REPORT_MINUS5DBM;
            }
            else if(!strcmp(data0, "minus6dBm"))
            {
                pzwParam->cmd_set.cmd[i++]= POWERLEVEL_REPORT_MINUS6DBM;
            }
            else if(!strcmp(data0, "minus7dBm"))
            {
                pzwParam->cmd_set.cmd[i++]= POWERLEVEL_REPORT_MINUS7DBM;
            }
            else if(!strcmp(data0, "minus8dBm"))
            {
                pzwParam->cmd_set.cmd[i++]= POWERLEVEL_REPORT_MINUS8DBM;
            }
            else if(!strcmp(data0, "minus9dBm"))
            {
                pzwParam->cmd_set.cmd[i++]= POWERLEVEL_REPORT_MINUS9DBM;
            }
            int powerlevel_timeout=(int)strtol(data1, NULL, 0);
            pzwParam->cmd_set.cmd[i++]=(uint8_t)powerlevel_timeout;
        }
    }
    else if(!strcmp(CommandClass, ST_CONFIGURATION))
    {
        if(!data0 || !data1)
        {
            goto setSpecPreDone;
        }

        char configId[SIZE_32B]={0}, configLength[SIZE_32B]={0};
        if(!get_configuration_data_from_name((char*)nodeid, (char*)data0, configId, configLength))
        {
            if(!(*configId) || !(*configLength))
            {
                res = CONFIG_ID_NOT_FOUND;
                goto setSpecPreDone;
            }

            pzwParam->cmd_set.cmd[i++]= (uint8_t)strtol(configId, NULL, 0);//paramNumber
            int paramLength = strtol(configLength, NULL, 0);
            int paramValue = strtol(data1, NULL, 0);//value
            pzwParam->cmd_set.cmd[i++]= (uint8_t)paramLength;

            int j;
            for (j=paramLength-1;j>=0;j--)
            {
                pzwParam->cmd_set.cmd[i++]=(uint8_t)(paramValue>>(8*j));
            }
        }
        else
        {
            res = CONFIG_ID_NOT_FOUND;
            goto setSpecPreDone;
        }
    }
    else if(!strcmp(Command,ST_SET) && !strcmp(CommandClass,ST_DOOR_LOCK))
    {
        if(!data0)
        {
            goto setSpecPreDone;
        }

        if(!strcmp(data0, ST_OPEN))
        {
            pzwParam->cmd_set.cmd[i++] = DOOR_LOCK_OPERATION_SET_DOOR_UNSECURED;
        }
        else if(!strcmp(data0, ST_CLOSE))
        {
            pzwParam->cmd_set.cmd[i++] = DOOR_LOCK_OPERATION_SET_DOOR_SECURED;
        }
    }
    else if(!strcmp(CommandClass,ST_BARRIER_OPERATOR))
    {
        if(!data0)
        {
            goto setSpecPreDone;
        }

        if(!strcmp(Command,ST_SET))
        {
            if(!strcmp(data0, ST_OPEN))
            {
                pzwParam->cmd_set.cmd[i++] = BARRIER_OPERATOR_SET_OPEN;
            }
            else if(!strcmp(data0, ST_CLOSE))
            {
                pzwParam->cmd_set.cmd[i++] = BARRIER_OPERATOR_SET_CLOSE;
            }
        }
        else if(!strcmp(Command,ST_SIGNAL_SET))
        {
            if(!data1)
            {
                goto setSpecPreDone;
            }

            char *signalName = data0;
            char *signalState = data1;
            int signalId = get_barrier_signal_id(signalName);
            if(signalId == -1)
            {
                SLOGE("BARRIER_OPERATOR does not support subsystem type %s\n", signalName);
                res = COMMAND_FAILED;
                goto setSpecPreDone;
            }
            SLOGI("signalId= %d\n", signalId);
            pzwParam->cmd_set.cmd[i++] = (uint8_t)signalId;
            pzwParam->cmd_set.cmd[i++] = (uint8_t)strtol(signalState, NULL, 0);
        }
    }
    else if(!strcmp(CommandClass, ST_ASSOCIATION))
    {
        if(!data0 || !data1)
        {
            SLOGE("missing data when set association\n");
            goto setSpecPreDone;
        }

        pzwParam->cmd_set.cmd[i++]= (uint8_t)strtol(data0, NULL, 0); //groupId
        char *nodeList = strdup(data1);
        char *save_tok;
        char *ch = strtok_r(nodeList, ",", &save_tok);
        while(ch != NULL)
        {
            pzwParam->cmd_set.cmd[i++]= (uint8_t)strtol(ch, NULL, 16); //nodeId hex value
            ch = strtok_r(NULL, ",", &save_tok);
        }
        SAFE_FREE(nodeList);
    }
    else if(!strcmp(CommandClass, ST_SCENE_CONTROLLER_CONF))
    {
        if(!data0 || !data1)
        {
            SLOGE("missing data when set scene config\n");
            goto setSpecPreDone;
        }

        pzwParam->cmd_set.cmd[i++]= (uint8_t)strtol(data0, NULL, 0); //groupId
        char *buttonId = data1;
        // char *sceneId = data2;

        pzwParam->cmd_set.cmd[i++]= (uint8_t)strtol(buttonId, NULL, 0);
        pzwParam->cmd_set.cmd[i++]= 0;//dimming_duration alway is 0
    }
    else if(!strcmp(CommandClass, ST_USER_CODE))
    {
        if(!data0 || !data1)
        {
            SLOGE("missing data when set usercode\n");
            goto setSpecPreDone;
        }

        char *userId = data0;
        char *actionStr = data1;

        int action = strtol(actionStr, NULL, 0);

        if(action == DOORLOCK_USERCODE_ADD)
        {
            if(!data2)
            {
                goto setSpecPreDone;
            }

            json_object *userCodeObj = VR_(create_json_object)(data2);
            if(!userCodeObj)
            {
                goto setSpecPreDone;
            }

            CHECK_JSON_OBJECT_EXIST(passObj, userCodeObj, ST_PASS, setSpecPreDone);
            CHECK_JSON_OBJECT_EXIST(nameObj, userCodeObj, ST_NAME, setSpecPreDone);
            const char *pass = json_object_get_string(passObj);
            const char *name = json_object_get_string(nameObj);

            uint8_t userIdValue = (uint8_t)strtol(userId, NULL, 0);
            uint8_t maxUsers = get_maximum_user_code(nodeid);

            if(userIdValue > maxUsers)
            {
                res = USER_CODE_USERID_INVALID;
                goto setSpecPreDone;
            }

            int state = check_user_code_info(nodeid, userIdValue, name);
            if(USER_CODE_SAME_NAME == state)
            {
                res = USER_CODE_NAME_INVALID;
                goto setSpecPreDone;
            }

            if(USER_CODE_SAME_USER_ID == state)
            {
                userIdValue = get_user_id_valid(nodeid, maxUsers);
                if(userIdValue > maxUsers)
                {
                    res = USER_CODE_USERID_INVALID;
                    goto setSpecPreDone;
                }
            }

            pzwParam->cmd_set.cmd[i++] = (uint8_t)userIdValue;
            pzwParam->cmd_set.cmd[i++] = (uint8_t)action;

            for(j=0;j<strlen(pass);j++)
            {
                pzwParam->cmd_set.cmd[i++] = pass[j];
            }
            json_object_put(userCodeObj);
        }
        else
        {
            pzwParam->cmd_set.cmd[i++] = (uint8_t)strtol(userId, NULL, 0);
            pzwParam->cmd_set.cmd[i++] = (uint8_t)action;
        }
    }
    else if(!strcmp(CommandClass, ST_SIMPLE_AV_CONTROL))
    {
        //ubus call zwave setBinary '{"id":"05","cmd":"onOff","value":"1"}'
        //ubus call zwave writeSpec '{"id":"46", "class":"simpleAvControl","cmd":"set", "data0":"01", "data1":"02", "data2":"00"}'
        if(!data0)
        {
            //data0: button Command (decimal)
            SLOGE("missing data when set simple av control\n");
            goto setSpecPreDone;
        }
        if(!strcmp(Command,ST_SET))
        {
            pzwParam->cmd_set.cmd[i++] = sequenceNumberSimpleAV++; //Sequence Number
            pzwParam->cmd_set.cmd[i++] = (uint8_t)KEY_DOWN_ATTRIBUTE;//Key Attributes, Key Down – Sent when a new key is pressed.
            pzwParam->cmd_set.cmd[i++] = 0;//Item ID MSB
            pzwParam->cmd_set.cmd[i++] = 0;//Item ID LSB
            uint16_t buttonCommand = (uint16_t)strtol(data0, NULL, 10);
            pzwParam->cmd_set.cmd[i++] = (uint8_t)(buttonCommand >> 8);//Command MSB,1
            pzwParam->cmd_set.cmd[i++] = (uint8_t)buttonCommand;//Command LSB,1
        }
        else if(!strcmp(Command,ST_CHANGE_CHANNEL))
        {
            pzwParam->param1 = sequenceNumberSimpleAV;
            uint8_t numOfDigits = strlen(data0);
            sequenceNumberSimpleAV += numOfDigits;
            memcpy(&pzwParam->cmd_set.cmd[i], data0, numOfDigits);
            i += numOfDigits;
            pzwParam->command = COMMAND_CLASS_SPECIFIC_CHANGE_CHANNEL_TV;
        }
    }
    else
    {
        if(data0)
        {
            pzwParam->cmd_set.cmd[i++] = (uint8_t)strtol(data0, NULL, 0);
        }

        if(data1)
        {
            pzwParam->cmd_set.cmd[i++] = (uint8_t)strtol(data1, NULL, 0);
        }

        if(data2)
        {
            pzwParam->cmd_set.cmd[i++] = (uint8_t)strtol(data2, NULL, 0);
        }
    } 
    pzwParam->cmd_set.cmd_length = i;
    insert_to_queue(response, pzwParam, dev);
    res = 0;
setSpecPreDone:
    SAFE_FREE(pzwParam);
    return res;
}

static int setspecification(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    int res = 0;

    struct blob_attr *tb[6];
    const char *nodeid = ST_UNKNOWN;
    const char *CommandClass = ST_UNKNOWN;
    const char *Command = ST_UNKNOWN;
    const char *data0 = ST_UNKNOWN;
    const char *data1 = ST_UNKNOWN;
    const char *data2 = ST_UNKNOWN;

    blobmsg_parse(set_specification, ARRAY_SIZE(set_specification), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WRITE_SPEC_R));

    if (tb[0] && tb[1] && tb[2])
    {
        nodeid = blobmsg_data(tb[0]);
        CommandClass = blobmsg_data(tb[1]);
        Command = blobmsg_data(tb[2]);
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        if(!tb[0]) 
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: nodeid"));
        }
        else if(!tb[1])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: CommandClass"));
        }
        else if(!tb[2])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: Command"));
        }

        // VR_(usleep)(SLEEP_TIME);
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
        ubus_send_reply(ctx, req, buff.head);
        return 0;
    }

    if(tb[3])
    {
        data0 = blobmsg_data(tb[3]);
    }

    if(tb[4])
    {
        data1 = blobmsg_data(tb[4]);
    }

    if(tb[5])
    {
        data2 = blobmsg_data(tb[5]);
    }

    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));

    json_object *commandinfo = json_object_new_object();
    json_object_object_add(commandinfo, ST_CLASS, json_object_new_string(CommandClass));
    json_object_object_add(commandinfo, ST_COMMAND, json_object_new_string(Command));
    if(tb[3])
    {
        json_object_object_add(commandinfo, ST_DATA0, json_object_new_string(data0));
    }
    if(tb[4])
    {
        json_object_object_add(commandinfo, ST_DATA1, json_object_new_string(data1));
    }
    if(tb[5])
    {
        json_object_object_add(commandinfo, ST_DATA2, json_object_new_string(data2));
    }
    json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

    SLOGI("nodeid = %s\n", nodeid);
    SLOGI("CommandClass = %s\n", CommandClass);
    SLOGI("Command = %s\n", Command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);
    SLOGI("data2 = %s\n", data2);

    zwave_command_response_t response;
    memset(&response, 0x00, sizeof(zwave_command_response_t));
    strcpy(response.method, ST_WRITE_SPEC_R);
    strcpy(response.nodeid, nodeid);
    strcpy(response.command_class, CommandClass);
    strcpy(response.command, Command);
    if(tb[3])
    {
        response.data0 = (char *)malloc(strlen(data0)+1);
        strcpy(response.data0, data0);
    }
    if(tb[4])
    {
        response.data1 = (char *)malloc(strlen(data1)+1);
        strcpy(response.data1, data1);
    }
    if(tb[5])
    {
        response.data2 = (char *)malloc(strlen(data2)+1);
        strcpy(response.data2, data2);
    }

    res = _set_specification_prepare(response);
    if(res)
    {
        free_response(response);
    }
    return_json_message(res, &jobj, (char*)Command, (char*)CommandClass);

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

static int _set_specification_CRC_prepare(zwave_command_response_t response)
{
    int res = 0, i,j;
    class_command *class_cmd = NULL;
    char *commandinfo_buffer = NULL;/*stored infor for command in queue*/
    
    char *nodeid = &response.nodeid[0];
    char *CommandClass = &response.command_class[0];
    char *Command = &response.command[0];
    char *data0 = response.compliantJson;
    char *data1 = response.data1;
    char *data2 = response.data2;
    char *data3 = &response.data3[0];
    char *data4 = &response.data4[0];
    char *data5 = &response.data5[0];

    zwave_dev_info_t *dev = get_zwave_dev_from_id(nodeid);
    if(!dev)
    {
        dev = create_zwave_dev_from_id(nodeid);
        if(!dev)
        {
            return DEV_NOT_FOUND;
        }
    }

    json_object *commandinfo = json_object_new_object();
    json_object_object_add(commandinfo, ST_CMD_CLASS, json_object_new_string(CommandClass));
    json_object_object_add(commandinfo, ST_CMD, json_object_new_string(Command));
    JSON_ADD_STRING_SAFE(commandinfo, ST_DATA0, data0);
    JSON_ADD_STRING_SAFE(commandinfo, ST_DATA1, data1);
    JSON_ADD_STRING_SAFE(commandinfo, ST_DATA2, data2);
    JSON_ADD_STRING_SAFE(commandinfo, ST_DATA3, data3);
    JSON_ADD_STRING_SAFE(commandinfo, ST_DATA4, data4);
    JSON_ADD_STRING_SAFE(commandinfo, ST_DATA5, data5);

    TZWParam *pzwParam = (TZWParam *)malloc(sizeof(TZWParam));
    memset(pzwParam, 0x00, sizeof(TZWParam));

    pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_REQUEST_SPECIFICATION_ZPC;
    pzwParam->param2 = htoi(nodeid);
    pzwParam->src_endpoint = htoi(data3);
    pzwParam->dest_endpoint = htoi(data4);
    pzwParam->scheme = htoi(data5);

    if(data2)
    {
        /*forceImnediately = flag & 0x01;
        flag & 0x02 --> CmdClassSupervisionEncapsule*/
        pzwParam->param3 = htoi(data2);
    }

    for (i = 0; i < sizeof(pointer_str) / sizeof(struct_point); i++)
    {
        if (!strcmp(pointer_str[i].struct_name, CommandClass))
        {
            class_cmd = pointer_str[i].class_cmd;
            pzwParam->cmd_set.cmd[0] = pointer_str[i].class_id;
            for (j = 0; j < pointer_str[i].size; j++)
            {
                if (!strcmp(class_cmd[j].cmd_name, Command))
                {
                    pzwParam->cmd_set.cmd[1] = class_cmd[j].cmd_id;
                    break;
                }
            }
            if (j == pointer_str[i].size)
            {
                res = CLASS_NOT_SUPPORT_CMD;
                goto setSpecCRCPreDone;
            }
            break;
        }
    }

    i = 2;
    uint8_t temp_length;
    if (data0)
    {
        parsing(pzwParam->cmd_set.cmd[0], pzwParam->cmd_set.cmd[1], data0, &pzwParam->cmd_set.cmd[2], &temp_length);
    }
    SLOGI("temp_length: %u", temp_length);
    i += temp_length;

    pzwParam->cmd_set.cmd_length = i;

    const char *commandInfoStr = json_object_to_json_string(commandinfo);
    size_t commandInfoLength = strlen(commandInfoStr);
    commandinfo_buffer = (char*)malloc(commandInfoLength+1);
    strncpy(commandinfo_buffer, commandInfoStr, commandInfoLength);
    commandinfo_buffer[commandInfoLength] = '\0';

    pzwParam->misc_data = (void *)commandinfo_buffer;
    
    insert_to_queue(response, pzwParam, dev);

setSpecCRCPreDone:
    SAFE_FREE(pzwParam);
    json_object_put(commandinfo);
    return res;
}

static int setspecification_CRC(struct ubus_context *ctx, struct ubus_object *obj,
                                struct ubus_request_data *req, const char *method,
                                struct blob_attr *msg)
{
    int res = 0;
    //config_associate_command *config_associate_cmd = NULL;

    struct blob_attr *tb[9];
    const char *nodeid = ST_UNKNOWN;
    const char *CommandClass = ST_UNKNOWN;
    const char *Command = ST_UNKNOWN;
    const char *data0 = ST_UNKNOWN;
    const char *data1 = ST_UNKNOWN;
    const char *data2 = ST_UNKNOWN;
    const char *data3 = ST_UNKNOWN;
    const char *data4 = ST_UNKNOWN;
    const char *data5 = ST_UNKNOWN;

    blobmsg_parse(specification_CRC, ARRAY_SIZE(specification_CRC), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WRITE_SPEC_CRC_R));

    if (tb[0] && tb[1] && tb[2] && tb[3] && tb[6] && tb[7] && tb[8])
    {
        nodeid = blobmsg_data(tb[0]);
        CommandClass = blobmsg_data(tb[1]);
        Command = blobmsg_data(tb[2]);
        data0 = blobmsg_data(tb[3]);
        if(tb[4])
        {
            data2 = blobmsg_data(tb[4]);
        }
        data3 = blobmsg_data(tb[6]);
        data4 = blobmsg_data(tb[7]);
        data5 = blobmsg_data(tb[8]);

        SLOGI("nodeid = %s\n", nodeid);
        SLOGI("CommandClass = %s\n", CommandClass);
        SLOGI("Command = %s\n", Command);
        SLOGI("data0 = %s\n", data0);//value
        SLOGI("data3 = %s\n", data3);//source_endpoint
        SLOGI("data4 = %s\n", data4);//destination_enpoint
        SLOGI("data5 = %s\n", data5);//is_CRC
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        if (!tb[0])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: NodeID"));
        }
        else if (!tb[1])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: CommandClass"));
        }
        else if (!tb[2])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: Command"));
        }
        else if (!tb[6])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: data3"));
        }
        else if (!tb[7])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: data4"));
        }
        else if (!tb[8])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: data5"));
        }

        // VR_(usleep)(SLEEP_TIME);
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
        ubus_send_reply(ctx, req, buff.head);
        return 0;
    }

    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));

    json_object *commandinfo = json_object_new_object();
    json_object_object_add(commandinfo, ST_CMD_CLASS, json_object_new_string(CommandClass));
    json_object_object_add(commandinfo, ST_CMD, json_object_new_string(Command));
    if (tb[3])
    {
        json_object_object_add(commandinfo, ST_DATA0, json_object_new_string(data0));
    }
    if (tb[4])
    {
        json_object_object_add(commandinfo, ST_DATA1, json_object_new_string(data1));
    }
    if (tb[5])
    {
        json_object_object_add(commandinfo, ST_DATA2, json_object_new_string(blobmsg_data(tb[5])));
    }
    if (tb[6])
    {
        json_object_object_add(commandinfo, "data3", json_object_new_string(blobmsg_data(tb[6])));
    }
    if (tb[7])
    {
        json_object_object_add(commandinfo, "data4", json_object_new_string(blobmsg_data(tb[7])));
    }
    if (tb[8])
    {
        json_object_object_add(commandinfo, "data5", json_object_new_string(blobmsg_data(tb[8])));
    }
    json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

    zwave_command_response_t response;
    memset(&response, 0x00, sizeof(zwave_command_response_t));
    strcpy(response.method, ST_WRITE_SPEC_CRC_R);
    strcpy(response.nodeid, nodeid);
    strcpy(response.command_class, CommandClass);
    strcpy(response.command, Command);
    if(tb[3])
    {
        //data0 is json format
        response.compliantJson = (char*)malloc(strlen(data0)+1);
        strcpy(response.compliantJson, data0);
    }
    if(tb[4])
    {
        response.data1 = (char *)malloc(strlen(data1)+1);
        strcpy(response.data1, data1);
    }
    if(tb[5])
    {
        response.data2 = (char *)malloc(strlen(data2)+1);
        strcpy(response.data2, data2);
    }
    if(tb[6])
    {
        strcpy(response.data3, data3);
    }
    if(tb[7])
    {
        strcpy(response.data4, data4);
    }
    if(tb[8])
    {
        strcpy(response.data5, data5);
    }

    res = _set_specification_CRC_prepare(response);
    if(res)
    {
        free_response(response);
    }
    return_json_message(res, &jobj, (char*)Command, (char*)CommandClass);

    // SLOGI("START Send Zwave Command\n");
    // zwaveSendCommand(pzwParam);
    // SLOGI("END Zwave Command\n");

    // if (pzwParam->ret == 0)
    // {

    //     json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    // }
    // else if (pzwParam->ret == 2)
    // {
    //     json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    //     json_object_object_add(jobj, ST_REASON, json_object_new_string("Please press button on device to make device wakeup"));
    // }
    // else
    // {
    //     json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
    //     json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
    // }

    // VR_(usleep)(SLEEP_TIME*2);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

static int setsecurespecification(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    int res = 0;

    struct blob_attr *tb[6];
    const char *nodeid = ST_UNKNOWN;
    const char *CommandClass = ST_UNKNOWN;
    const char *Command = ST_UNKNOWN;
    const char *data0 = ST_UNKNOWN;
    const char *data1 = ST_UNKNOWN;
    const char *data2 = ST_UNKNOWN;

    blobmsg_parse(set_specification, ARRAY_SIZE(set_specification), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WRITE_S_SPEC_R));

    if (tb[0] && tb[1] && tb[2])
    {
        nodeid = blobmsg_data(tb[0]);
        CommandClass = blobmsg_data(tb[1]);
        Command = blobmsg_data(tb[2]);
        if(tb[3])
        {
            data0 = blobmsg_data(tb[3]);
        }
        if(tb[4])
        {
            data1 = blobmsg_data(tb[4]);
        } 
        if(tb[5])
        {
            data2 = blobmsg_data(tb[5]);
        }
    }
    else
    {
        // VR_(usleep)(SLEEP_TIME);
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_MISSING_ARGUMENT);
        ubus_send_reply(ctx, req, buff.head);
        return 0;
    }

    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));

    json_object *commandinfo = json_object_new_object();
    json_object_object_add(commandinfo, ST_CLASS, json_object_new_string(CommandClass));
    json_object_object_add(commandinfo, ST_COMMAND, json_object_new_string(Command));
    if(tb[3])
    {
        json_object_object_add(commandinfo, ST_DATA0, json_object_new_string(data0));
    }
    if(tb[4])
    {
        json_object_object_add(commandinfo, ST_DATA1, json_object_new_string(data1));
    }
    if(tb[5])
    {
        json_object_object_add(commandinfo, ST_DATA2, json_object_new_string(blobmsg_data(tb[5])));
    }
    json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

    SLOGI("nodeid = %s\n", nodeid);
    SLOGI("CommandClass = %s\n", CommandClass);
    SLOGI("Command = %s\n", Command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);
    SLOGI("data2 = %s\n", data2);

    zwave_command_response_t response;
    memset(&response, 0x00, sizeof(zwave_command_response_t));
    strcpy(response.method, ST_WRITE_S_SPEC_R);
    strcpy(response.nodeid, nodeid);
    strcpy(response.command_class, CommandClass);
    strcpy(response.command, Command);
    if(tb[3])
    {
        response.data0 = (char *)malloc(strlen(data0)+1);
        strcpy(response.data0, data0);
    }
    if(tb[4])
    {
        response.data1 = (char *)malloc(strlen(data1)+1);
        strcpy(response.data1, data1);
    }
    if(tb[5])
    {
        response.data2 = (char *)malloc(strlen(data2)+1);
        strcpy(response.data2, data2);
    }

    res = _set_specification_prepare(response);
    if(res)
    {
        free_response(response);
    }
    return_json_message(res, &jobj, (char*)Command, (char*)CommandClass);

    // VR_(usleep)(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);
    
    json_object_put(jobj);
    return 0;
}

static int getsecurespecification(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    int res = 0;
    struct blob_attr *tb[4];
    const char *nodeid = ST_UNKNOWN;
    const char *CommandClass = ST_UNKNOWN;
    const char *Command = ST_UNKNOWN;
    const char *data0 = ST_UNKNOWN;

    blobmsg_parse(get_secure_specification, ARRAY_SIZE(get_secure_specification), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_READ_S_SPEC_R));

    if (tb[0] && tb[1] && tb[2])
    {
        nodeid = blobmsg_data(tb[0]);
        CommandClass = blobmsg_data(tb[1]);
        Command = blobmsg_data(tb[2]);
        if(tb[3])
        {
            data0 = blobmsg_data(tb[3]);
        }
    }
    else
    {
        // VR_(usleep)(SLEEP_TIME);
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_MISSING_ARGUMENT);
        ubus_send_reply(ctx, req, buff.head);
        return 0;
    }

    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));

    json_object *commandinfo = json_object_new_object();
    json_object_object_add(commandinfo, ST_CLASS, json_object_new_string(CommandClass));
    json_object_object_add(commandinfo, ST_COMMAND, json_object_new_string(Command));
    if(tb[3])
    {
        json_object_object_add(commandinfo, ST_DATA0, json_object_new_string(data0));
    }
    json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

    SLOGI("nodeid = %s\n", nodeid);
    SLOGI("CommandClass = %s\n", CommandClass);
    SLOGI("Command = %s\n", Command);
    SLOGI("data0 = %s\n", data0);

    zwave_command_response_t response;
    memset(&response, 0x00, sizeof(zwave_command_response_t));
    strcpy(response.method, ST_READ_S_SPEC_R);
    strcpy(response.nodeid, nodeid);
    strcpy(response.command_class, CommandClass);
    strcpy(response.command, Command);
    if(tb[3])
    {
        response.data0 = (char *)malloc(strlen(data0)+1);
        strcpy(response.data0, data0);
    }

    res = _get_specification_prepare(response);
    if(res)
    {
        free_response(response);
    }
    return_json_message(res, &jobj, (char*)Command, (char*)CommandClass);

    // VR_(usleep)(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);
    return 0;
}

static int identify(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[2];
    const char *nodeid = ST_UNKNOWN;
    const char *value = ST_UNKNOWN;
    SLOGI( "in identify\n");

    blobmsg_parse(set_policy, ARRAY_SIZE(set_policy), tb, blob_data(msg), blob_len(msg));
    if (tb[0])
    {
        TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));
        memset(pzwParam, 0x00, sizeof(TZWParam));

        nodeid = blobmsg_data(tb[0]);
        if(tb[1]) 
            value = blobmsg_data(tb[1]);

        char tmp[256];
        memset(tmp, 0x00, sizeof(tmp));

        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_IDENTIFY_R));
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));
        if(tb[1]) 
        json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
        
        //pzwParam->command=COMMAND_CLASS_SPECIFIC_IDENTIFY_FUNCTION;
        pzwParam->param1=htoi(nodeid);
        
        if(tb[1])
        {
            pzwParam->param2 = (uint8_t)strtol(value, NULL, 0);
        }
        else
        {
            pzwParam->param2 = 3;
        }
        
        // zwaveSendCommand(pzwParam);
        zwave_command_response_t response;
        memset(&response, 0x00, sizeof(zwave_command_response_t));
        strcpy(response.method, ST_IDENTIFY_R);
        strcpy(response.nodeid, nodeid);
        if(tb[1]) 
            strcpy(response.value, value);
        insert_to_queue(response, pzwParam, NULL);
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_RECEIVED));

        // if (pzwParam->ret==0) 
        // {
        //     json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
        // }
        // else if (pzwParam->ret==2)
        // {
        //     json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        //     json_object_object_add(jobj, ST_REASON, json_object_new_string("does not support identify function"));
        // }
        // else
        // {
        //     json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        //     json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
        // }

        // VR_(usleep)(SLEEP_TIME);
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
        ubus_send_reply(ctx, req, buff.head);

        json_object_put(jobj);
        
        if(pzwParam)
        {
            free(pzwParam);
        }

        return 0;
    }
    else
    {
        return -1;
    }
};

static int changename(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[2];
    const char *id = ST_UNKNOWN;
    const char *value = ST_UNKNOWN;
    SLOGI( "in changename\n");

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CHANGE_NAME_R));
    
    blobmsg_parse(set_policy, ARRAY_SIZE(set_policy), tb, blob_data(msg), blob_len(msg));
    if (tb[0] && tb[1])
    {
        id = blobmsg_data(tb[0]);
        value = blobmsg_data(tb[1]);
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(id)); 
        json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
        
        database_actions(_VR_(zwave), "UPDATE SUB_DEVICES SET friendlyName='%s' "
                                                "WHERE id='%s'", 
                                                value, id);

        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_MISSING_ARGUMENT));
    }

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;  
}

static int alexa(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[2];
    const char *id = ST_UNKNOWN;
    const char *value = ST_UNKNOWN;
    SLOGI( "in alexa\n");

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string("alexaR"));
    
    blobmsg_parse(set_policy, ARRAY_SIZE(set_policy), tb, blob_data(msg), blob_len(msg));
    if (tb[0] && tb[1])
    {
        id = blobmsg_data(tb[0]);
        value = blobmsg_data(tb[1]);
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(id));
        json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
        
        if(!strncasecmp(value, ST_ENABLE, 6))
        {
            SEARCH_DATA_INIT_VAR(tmp);
            searching_database(_VR_CB_(zwave), &tmp,  
                                "SELECT alexa from SUB_DEVICES where id='%s'", id);
            if(tmp.len && !strncasecmp(tmp.value, "NO", 2))
            {
                database_actions(_VR_(zwave), "UPDATE SUB_DEVICES SET alexa='%s' "
                                                "WHERE id='%s'", 
                                                "0,0", id);
            }
            FREE_SEARCH_DATA_VAR(tmp);
        }
        else if(!strncasecmp(value, ST_DISABLE, 7))
        {
            database_actions(_VR_(zwave), "UPDATE SUB_DEVICES SET alexa='%s' "
                                                "WHERE id='%s'", 
                                                "NO", id);
        }

        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_MISSING_ARGUMENT));
    }

    // VR_(usleep)(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;  
}

static struct uloop_timeout cancel_adding_device = {
    .cb = cancel_adding_device_cb,
};

static int open_closenetwork(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    
    TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));
    pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_REMOVE_NODE_ZPC_AUTO;

    if(!strcmp(method, ST_OPEN_NETWORK))
    {
        VR_(execute_system)("ubus send zwave '{\"state\":\"open_network\"}' &");

        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_OPEN_NETWORK_R));
        pzwParam->param1=OPEN; 

        uloop_timeout_set(&cancel_adding_device, ADDING_DEVICE_TIMEOUT*1000);//5 mins
    }
    else if(!strcmp(method, ST_CLOSE_NETWORK))
    {
        VR_(execute_system)("ubus send zwave '{\"state\":\"close_network\"}' &");

        uloop_timeout_cancel(&cancel_adding_device);
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CLOSE_NETWORK_R));
        pzwParam->param1=CLOSE;
    }

    zwaveSendCommand(pzwParam);

    if (pzwParam->ret==0)
    {
        if(!strcmp(method, ST_CLOSE_NETWORK))
        {
            g_open_network = 0;
        }
        else
        {
            g_open_network = 1;
        }
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
    }

    // VR_(usleep)(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    if(pzwParam)
    {
        free(pzwParam);
    }
    return 0;  
}

static int remove_device(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[1];
    TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));
    blobmsg_parse(get_policy, ARRAY_SIZE(get_policy), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();

    if(tb[0])
    {
        pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_REMOVE_FORCE;
        pzwParam->param1=htoi(blobmsg_data(tb[0]));
    }    
    else
    {
        pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_REMOVE;
        pzwParam->param1=OPEN;
        // uloop_timeout_set(&cancel_adding_device, 5*60*1000);//5 mins
    }

    zwaveSendCommand(pzwParam);

    if(tb[0])
    {
        char *nodeId = (char *)malloc(SIZE_32B);
        sprintf(nodeId, "%s", (char*)blobmsg_data(tb[0]));
        pthread_t remove_device_thread_t;
        pthread_create(&remove_device_thread_t, NULL, (void *)&remove_device_thread, (void *)nodeId);
        pthread_detach(remove_device_thread_t);

        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_REMOVE_DEVICE_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeId));
    }
    else
    {
        if(pzwParam->ret == 0)
        {
            json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_REMOVE_DEVICE_R));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_WAIT_NOTIFY));
        }
    }

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    if(pzwParam)
    {
        free(pzwParam);
    }
    return 0;
}

static int checking_devices_state(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    pthread_mutex_lock(&zwave_dev_info_listMutex);
    zwave_dev_info_t *zwave_dev = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_zwave_dev_list.list)
    {
        zwave_dev = VR_(list_entry)(pos, zwave_dev_info_t, list);
        if(zwave_dev->deviceMode == MODE_NONLISTENING 
            && zwave_dev->status.state == 1
            && zwave_dev->endpointNum == 0)
        {
            char deviceId[SIZE_32B];
            snprintf(deviceId, sizeof(deviceId)-1, "%02X", zwave_dev->localId);

            if(zwave_dev->status.wakeUp == 0)
            {
                char timestampt[SIZE_32B];
                sprintf(timestampt, "%u", (unsigned)time(NULL));
                update_zwave_status(_VR_CB_(zwave), deviceId, ST_WAKE_UP_NOTIFICATION, timestampt, ST_REPLACE, 0);
                // tmp->status.wakeUp = (unsigned)time(NULL);
                continue;
            }

            unsigned int time_now = (unsigned)time(NULL);
            unsigned int tmp = time_now - zwave_dev->status.wakeUp;
            printf("time_now = %u\n", time_now);
            printf("tmp = %u\n", tmp);
            if(tmp > MAXIMUM_DEVICE_TIME_LIFE)
            {
                tmp = time_now - zwave_dev->status.lastUpdate;
                if(tmp > MAXIMUM_DEVICE_TIME_LIFE)
                {
                    update_zwave_status(_VR_CB_(zwave), deviceId, ST_STATE, ST_DEAD, ST_REPLACE, 0);
                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
                    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_DEVICE_STATE));
                    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceId));
                    json_object_object_add(jobj, ST_STATE, json_object_new_string(ST_DEAD));

                    char last_update[SIZE_256B];
                    snprintf(last_update, sizeof(last_update)-1, "%u", zwave_dev->status.lastUpdate);
                    json_object_object_add(jobj, ST_LAST_UPDATE, json_object_new_string(last_update));
                    Send_ubus_notify((char*)json_object_to_json_string(jobj));
                    json_object_put(jobj);
                }
            }
        }
    }
    pthread_mutex_unlock(&zwave_dev_info_listMutex);
    
    // VR_(usleep)(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL); 
    ubus_send_reply(ctx, req, buff.head);

    return 0;
}

static int network(struct ubus_context *ctx, struct ubus_object *obj,
                   struct ubus_request_data *req, const char *method,
                   struct blob_attr *msg)
{
    int res = 0;
    struct blob_attr *tb[6];
    const char *nodeid = ST_UNKNOWN;
    const char *CommandClass = ST_UNKNOWN;
    const char *Command = ST_UNKNOWN;
    const char *data0 = ST_UNKNOWN;
    const char *data1 = ST_UNKNOWN;
    const char *data2 = ST_UNKNOWN;
    const char *ip_address = ST_UNKNOWN;/*only for firmware update, get link from ftp server*/
    const char *path_file = ST_UNKNOWN;

    TZWParam *pzwParam = (TZWParam *)malloc(sizeof(TZWParam));
    memset(pzwParam, 0x00, sizeof(TZWParam));

    blobmsg_parse(set_specification, ARRAY_SIZE(set_specification), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NETWORK_R));

    if (tb[1] && tb[2] && tb[3])
    {
        CommandClass = blobmsg_data(tb[1]);
        Command = blobmsg_data(tb[2]);

        if (!strcmp(CommandClass, ST_SET_SUC_MODE))
        {
            nodeid = blobmsg_data(tb[0]);
            pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_SET_SUC_SIS_MODE;
            pzwParam->param1 = htoi(nodeid);
            if (!strcmp(Command, ST_ENABLE))
            {
                pzwParam->param2 = 0x01;
            }
        }
        else if (!strcmp(CommandClass, ST_FIRMWARE_UPDATE_MD))
        {
            nodeid = blobmsg_data(tb[0]);
            pzwParam->command = COMMAND_CLASS_SPECIFIC_FIRMWARE_UPDATE_MD;
            pzwParam->param1 = htoi(nodeid);
            data0 = blobmsg_data(tb[3]);//path ftp
            /*"data0": "{\"ip_address\":\"ftp://192.168.0.159/\", 
            "\"path_file"\": \"SensorPIR_Battery_slave_enhanced_232_OTA_ZW050x_US.ota\"}",*/
            json_object *jobj = json_tokener_parse(data0);
            ip_address = json_object_get_string(json_object_object_get(jobj, "ip_address"));
            path_file = json_object_get_string(json_object_object_get(jobj, "path_file"));
            data1 = blobmsg_data(tb[4]);
            data2 = blobmsg_data(tb[5]);
            /*check path save file exist*/
            struct stat st = {0};
            if (stat("/etc/backup_restore_zwave_controller/firmware_update_md/", &st) == -1)
            {
                mkdir("/etc/backup_restore_zwave_controller/firmware_update_md/", 0700);
            }

            char *str_path_file = malloc(SIZE_1024B);

            strcpy(str_path_file, "rm -rf /etc/backup_restore_zwave_controller/firmware_update_md/* \ncurl -u venus_firmware:v3nu5_f!rmw@r3 '");
            strcat(str_path_file, ip_address);
            strcat(str_path_file, "/");            
            strcat(str_path_file, path_file);
            
            strcat(str_path_file, "' -o /etc/backup_restore_zwave_controller/firmware_update_md/firmware_update_file.ota\n");
            system(str_path_file);
            strcpy(str_path_file, "/etc/backup_restore_zwave_controller/firmware_update_md/firmware_update_file.ota");
            pzwParam->param2 = (uint8_t)htoi(data2);
            pzwParam->scheme = htoi(data1);
            pzwParam->misc_data = (void *)str_path_file;//local path of file
            json_object_put(jobj);
        }
        //backup/restore nvm, and auto update firmware for device
        else if (!strcmp(CommandClass, ST_ZWAVE_CONTROLLER_BACKUP_RESTORE))
        {
            path_file = blobmsg_data(tb[3]);

            size_t length = strlen(path_file);
            char *str_path_file = malloc(length+1);
            strncpy(str_path_file, path_file, length);

            pzwParam->misc_data = (void *)str_path_file;
            if (!strcmp(Command, ST_BACKUP))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_CONTROLLER_BACKUP;
            }
            else if (!strcmp(Command, ST_RESTORE))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_CONTROLLER_RESTORE;
            }
        }
        else if (!strcmp(CommandClass, ST_UPDATE_FIRMWARE_PROGRAMMER))//get s0 key, s2 key, save .txt file
        {
            /*ubus call zwave network '{"id":"FF", "class":"updateFirmwareProgrammer", "cmd":"update",
            "data0":"/lib/firmware/zwave/serialapi_controller_static_ZM5304_US_4_61.hex"}'*/
            path_file = blobmsg_data(tb[3]);

            size_t length = strlen(path_file);
            char *str_path_file = malloc(length+1);
            strncpy(str_path_file, path_file, length);
            
            pzwParam->misc_data = (void *)str_path_file;
            pzwParam->command = COMMAND_CLASS_SPECIFIC_UPDATE_FIRMWARE;
        }
        else if (!strcmp(CommandClass, ST_S2_BOOTSTRAPPING_KEX_REPORT))
        {
            pzwParam->command = COMMAND_CLASS_SPECIFIC_S2_BOOTSTRAPPING_KEX_REPORT;
            data0 = blobmsg_data(tb[3]);
            data1 = blobmsg_data(tb[4]);
            data2 = blobmsg_data(tb[5]);
            pzwParam->param1 = htoi(data0);//accept 0 or 1
            pzwParam->param2 = (uint8_t)htoi(data1);//request key report S0 or S2.0, S2.1, S2.2
            pzwParam->param3 = htoi(data2);//CSA
        }
        else if (!strcmp(CommandClass, ST_S2_BOOTSTRAPPING_CHALLENGE_RESPONSE))
        {
            pzwParam->command = COMMAND_CLASS_SPECIFIC_S2_BOOTSTRAPPING_CHALLENGE_RESPONSE;
            data0 = blobmsg_data(tb[3]);
            data1 = blobmsg_data(tb[4]);
            if(tb[5])
                data2 = blobmsg_data(tb[5]);
            pzwParam->param1 = htoi(data0);//accept 0 or 1
            pzwParam->param2 = htoi(data1);//16 bit: param2 = 1 or 32 bit: param2 = 0 
            pzwParam->param4 = atoi(data2);//DSK
        }
        else if (!strcmp(CommandClass, ST_PRIORITY_ROUTE))
        {
            nodeid = blobmsg_data(tb[0]);
            pzwParam->command = COMMAND_CLASS_SPECIFIC_PRIORITY_ROUTE;
            pzwParam->param2 = htoi(nodeid);
            if (!strcmp(Command, ST_SET))
            {
                pzwParam->param1 = 0;
                data0 = blobmsg_data(tb[3]);
                json_object *jobj = json_tokener_parse(data0);
                
                json_object* jroutes = json_object_object_get(jobj, ST_ROUTES);
                int arraylen = json_object_array_length(jroutes);
                int j;
                json_object *jvalue;
                pzwParam->cmd_set.cmd_length = arraylen;
                for (j = 0; j < arraylen; j++)
                {
                    jvalue = json_object_array_get_idx(jroutes, j);
                    pzwParam->cmd_set.cmd[j] = strtol(json_object_get_string(jvalue), NULL, 16);
                }
                
                json_object* jrouteConfig = json_object_object_get(jobj, ST_ROUTE_CONFIG);
                const char* routeConfig = json_object_get_string(jrouteConfig);
                if (routeConfig)
                {
                    if (!strcmp(routeConfig, ST_AUTO))
                    {
                        pzwParam->param3 = ZW_LAST_WORKING_ROUTE_SPEED_AUTO;
                    }
                    else if (!strcmp(routeConfig, ST_100K))
                    {
                        pzwParam->param3 = ZW_LAST_WORKING_ROUTE_SPEED_100K;   
                    }
                    else if (!strcmp(routeConfig, ST_40K))
                    {
                        pzwParam->param3 = ZW_LAST_WORKING_ROUTE_SPEED_40K;   
                    }
                    else if (!strcmp(routeConfig, ST_9600))
                    {
                        pzwParam->param3 = ZW_LAST_WORKING_ROUTE_SPEED_9600;   
                    }
                }

                json_object_put(jobj);
            }
            else if (!strcmp(Command, ST_GET))
            {
                pzwParam->param1 = 1;
            }

        }
    }
    else if (tb[1] && tb[2])
    {
        nodeid = blobmsg_data(tb[0]);
        CommandClass = blobmsg_data(tb[1]);
        Command = blobmsg_data(tb[2]);
        if (!strcmp(CommandClass, ST_CONTROLLER_CHANGE))
        {
            if (!strcmp(Command, ST_OPEN))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_CONTROLLER_CHANGE;
                pzwParam->param1 = OPEN;
            }
            if (!strcmp(Command, ST_CLOSE))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_CONTROLLER_CHANGE;
                pzwParam->param1 = CLOSE;
            }
        }
        else if (!strcmp(CommandClass, ST_ADD_DEV))
        {
            if (!strcmp(Command, ST_OPEN))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_ADD_NODE_ZPC;
                pzwParam->param1 = OPEN;
            }
            else if (!strcmp(Command, ST_CLOSE))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_ADD_NODE_ZPC;
                pzwParam->param1 = CLOSE;
            }
        }
        else if (!strcmp(CommandClass, ST_SET_SUC_MODE))
        {
            if (!strcmp(Command, ST_DISABLE))
            {
                nodeid = blobmsg_data(tb[0]);
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_SET_SUC_SIS_MODE;
                pzwParam->param1 = htoi(nodeid);
                pzwParam->param2 = 0x00;
            }
        }
        else if (!strcmp(CommandClass, ST_REMOVE_DEV))
        {

            if (!strcmp(Command, ST_OPEN))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_REMOVE_NODE_ZPC;
                pzwParam->param1 = OPEN;
            }
            else if (!strcmp(Command, ST_CLOSE))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_REMOVE_NODE_ZPC;
                pzwParam->param1 = CLOSE;
            }
        }
        else if (!strcmp(CommandClass, ST_REMOVE_AUTO_NODE_ZPC))
        {
            if (!strcmp(Command, ST_OPEN))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_REMOVE_NODE_ZPC_AUTO;
                pzwParam->param1 = OPEN;
            }
            else if (!strcmp(Command, ST_CLOSE))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_REMOVE_NODE_ZPC_AUTO;
                pzwParam->param1 = CLOSE;
            }
        }
        else if (!strcmp(CommandClass, ST_RESET))
        {
            if (!strcmp(Command, ST_RUN))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_SET_DEFAULT;
            }
        }
        else if (!strcmp(CommandClass, ST_REQ_NET_UPDATE))
        {
            pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_REQUEST_UPDATE;
        }
        else if (!strcmp(CommandClass, ST_NODE_LIST))
        {
            pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_GET_LIST;
        }
        else if (!strcmp(CommandClass, ST_REPLACE_FAILED_NODE))
        {
            if (!strcmp(Command, ST_OPEN))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_REPLACE_FAILED_NODE;
                pzwParam->param1 = htoi(nodeid);
                pzwParam->param2 = OPEN;
            }
            else if (!strcmp(Command, ST_CLOSE))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_REPLACE_FAILED_NODE;
                pzwParam->param1 = htoi(nodeid);
                pzwParam->param2 = CLOSE;
            }
        }
        else if (!strcmp(CommandClass, ST_NETWORK_TEST))
        {
            pzwParam->command = COMMAND_CLASS_SPECIFIC_NETWORK_TEST_CONNECTION;
            pzwParam->param1 = htoi(nodeid);
            if (!strcmp(Command, ST_HEALTH_TEST_START))
            {
                pzwParam->param2 = COMMAND_HEALTH_TEST;
                pzwParam->param3 = false;
            }
            else if (!strcmp(Command, ST_HEALTH_TEST_STOP))
            {
                pzwParam->param2 = COMMAND_HEALTH_TEST;
                pzwParam->param3 = true;
            }
            else if (!strcmp(Command, ST_MAINTENANCE_START))
            {
                pzwParam->param2 = COMMAND_MAINTENANCE;
                pzwParam->param3 = false;

            }
            else if (!strcmp(Command, ST_MAINTENANCE_STOP))
            {
                pzwParam->param2 = COMMAND_MAINTENANCE;
                pzwParam->param3 = true;
            }
            else if (!strcmp(Command, ST_REDISCOVERY_START))
            {
                pzwParam->param2 = COMMAND_REDISCOVERY;
                pzwParam->param3 = false;
            }
            else if (!strcmp(Command, ST_REDISCOVERY_STOP))
            {
                pzwParam->param2 = COMMAND_REDISCOVERY;
                pzwParam->param3 = true;
            }
            else if (!strcmp(Command, ST_DUMP_NEIGHBORS))
            {
                pzwParam->param2 = COMMAND_DUMP_NEIGHBORS;
            }
            else if (!strcmp(Command, ST_PING_ALL_NODE_START))
            {
                pzwParam->param2 = COMMAND_PING_ALL_NODE;
                pzwParam->param3 = false;

            }
            else if (!strcmp(Command, ST_PING_ALL_NODE_STOP))
            {
                pzwParam->param2 = COMMAND_PING_ALL_NODE;
                pzwParam->param3 = true;
            }
            else if (!strcmp(Command, ST_RSSI_MAP))
            {
                pzwParam->param2 = COMMAND_RSSI_MAP;
            }
        }
        else if (!strcmp(CommandClass, ST_IS_FAILED_NODE))
        {
            if (!strcmp(Command, ST_GET))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_IS_FAILED_NODE;
                pzwParam->param1 = htoi(nodeid);
            }
        }
        else if (!strcmp(CommandClass, ST_CONTROLLER_INFO))
        {
            if (!strcmp(Command, ST_GET))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_GET_CONTROLLER_INFO_ZPC;
            }
        }
        else if (!strcmp(CommandClass, ST_DUMP_NEIGHBORS))
        {
            if (!strcmp(Command, ST_GET))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NETWORK_DUMP_NEIGHBORS;
            }
        }
        else if (!strcmp(CommandClass, ST_REMOVE_FAILED_NODE))
        {
            if (!strcmp(Command, ST_REMOVE))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_REMOVE_FORCE;
                pzwParam->param1 = htoi(nodeid);
            }
        }
        else if (!strcmp(CommandClass, ST_LEARNING_MODE))
        {
            if (!strcmp(Command, ST_OPEN))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_SET_LEARN_MODE;
                pzwParam->param1 = OPEN;
            }
            else if (!strcmp(Command, ST_CLOSE))
            {
                pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_SET_LEARN_MODE;
                pzwParam->param1 = ZW_SET_LEARN_MODE_DISABLE;
            }
        }
        else if (!strcmp(CommandClass, ST_SEND_MY_NIF))
        {
            pzwParam->command = COMMAND_CLASS_SPECIFIC_SEND_MY_NIF;
            pzwParam->param1 = htoi(nodeid);
        }
        else if (!strcmp(CommandClass, ST_GET_SECURE_KEY))//get s0 key, s2 key, save .txt file
        {
            pzwParam->misc_data = malloc(SIZE_HOME_ID);            
            pzwParam->command = COMMAND_CLASS_SPECIFIC_GET_SECURE_KEY;
        }
        else if (!strcmp(CommandClass, ST_GET_FIRMWARE_PROGRAMMER_VERSION))
        {
            pzwParam->misc_data = malloc(SIZE_MISC_DATA);
            //pzwParam->misc_data is version which libzwave return
            /*ubus call zwave network '{"id":"FF", "class":"getFirmwareProgrammerVersion","cmd":"get"}'*/
            pzwParam->command = COMMAND_CLASS_SPECIFIC_GET_VERSION_FIRMWARE;
        }
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_MISSING_ARGUMENT));

        // VR_(usleep)(SLEEP_TIME);
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
        ubus_send_reply(ctx, req, buff.head);
        return 0;
    }

    SLOGI("nodeid = %s\n", nodeid);
    SLOGI("CommandClass = %s\n", CommandClass);
    SLOGI("Command = %s\n", Command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);
    SLOGI("data2 = %s\n", data2);

    zwave_command_response_t response;
    memset(&response, 0x00, sizeof(zwave_command_response_t));
    strcpy(response.method, ST_NETWORK_R);
    strcpy(response.nodeid, nodeid);
    strcpy(response.command_class, CommandClass);
    strcpy(response.command, Command);
    if(tb[3])
    {
        response.data0 = (char *)malloc(strlen(data0)+1);
        strcpy(response.data0, data0);
    }
    if(tb[4])
    {
        response.data1 = (char *)malloc(strlen(data1)+1);
        strcpy(response.data1, data1);
    }
    if(tb[5])
    {
        response.data2 = (char *)malloc(strlen(data2)+1);
        strcpy(response.data2, data2);
    }

    json_object *commandinfo = json_object_new_object();
    json_object_object_add(commandinfo, ST_CLASS, json_object_new_string(CommandClass));
    json_object_object_add(commandinfo, ST_COMMAND, json_object_new_string(Command));
    if (strcmp(data0, ST_UNKNOWN))
        json_object_object_add(commandinfo, ST_DATA0, json_object_new_string(data0));
    if (strcmp(data1, ST_UNKNOWN))
        json_object_object_add(commandinfo, ST_DATA1, json_object_new_string(data1));
    if (strcmp(data2, ST_UNKNOWN))
        json_object_object_add(commandinfo, ST_DATA2, json_object_new_string(data2));
    json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

    insert_to_queue(response, pzwParam, NULL);
    return_json_message(res, &jobj, (char*)Command, (char*)CommandClass);

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    if (pzwParam)
    {
        free(pzwParam);
    }
    return 0;
}

static int venus_alarm(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[3];
    blobmsg_parse(alarm_policy, ARRAY_SIZE(alarm_policy), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_VENUS_ALARM_R));

    if(tb[0] && tb[1])
    {
        const char *id = blobmsg_data(tb[0]);
        const char *value = blobmsg_data(tb[1]);

        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(id));
        json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
        if(strcmp(id, ST_ALL))
        {
            zwave_dev_info_t *dev = get_zwave_dev_from_id((char*)id);
            if(!strcmp(value, ST_ON_VALUE))
            {
                g_sound_alarm_enable = 1;
                set_register_database(_VR_CB_(zwave), id, ST_SOUND_ALARM_ENABLE, ST_ON_VALUE, ST_REPLACE, 0);
                if(dev)
                {
                    dev->speakerAlarm = ALARM_VIA_SPEAKER_ENABLE;
                }
            }
            else if(!strcmp(value, ST_OFF_VALUE))
            {
                set_register_database(_VR_CB_(zwave), id, ST_SOUND_ALARM_ENABLE, ST_OFF_VALUE, ST_REPLACE, 0);
                VR_(cancel_alarm)((void *)ctx, (char*)id);
                if(dev)
                {
                    dev->speakerAlarm = ALARM_VIA_SPEAKER_DISABLE;
                }
            }
            else if(!strcmp(value, ST_TEMP_VALUE))
            {
                if(tb[2])
                {
                    const char *duration_temporary_time = blobmsg_data(tb[2]);
                    char timeSet[SIZE_32B];
                    unsigned int time_temporary = (unsigned)time(NULL) + strtoul(duration_temporary_time, NULL, 0);
                    sprintf(timeSet, "%u", time_temporary);

                    set_register_database(_VR_CB_(zwave), id, ST_TEMPORARY_ALARM_TIME, timeSet, ST_REPLACE, 0);
                    if(dev)
                    {
                        dev->temporary_disable_alarm_time = time_temporary;
                    }
                }
                else
                {
                    SLOGE("Input temporary time missing\n");
                }

                VR_(temp_disable_alarm)((void *)ctx, (char*)id);
            }
            else if(!strcmp(value, ST_DISABLE_THIS_TIME))
            {
                VR_(cancel_alarm)((void *)ctx, (char*)id);
                if(dev)
                {
                    dev->speakerAlarm = ALARM_VIA_SPEAKER_ENABLE;
                }
            }
        }
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    }    
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_MISSING_ARGUMENT));

    }
    // VR_(usleep)(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

static int ubus_update_network(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    SLOGI("in ubus_update_network \n");
    json_object *jobj = json_object_new_object();

    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_UPDATE_NETWORK_R));
    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

    update_network();

    // VR_(usleep)(500);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);
    return 0;
}

static int reset_stop_add_timer(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    SLOGI("in reset_stop_add_timer \n");
    uloop_timeout_set(&cancel_adding_device, ADDING_DEVICE_TIMEOUT*1000);//5 mins

    // VR_(usleep)(500);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
    ubus_send_reply(ctx, req, buff.head);

    return 0;
}

/*force last status device after power cycle detected*/
static void force_status(void)
{
    pthread_mutex_lock(&zwave_dev_info_listMutex);
    zwave_dev_info_t *zwave_dev = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_zwave_dev_list.list)
    {
        zwave_dev = VR_(list_entry)(pos, zwave_dev_info_t, list);
        if((GENERIC_TYPE_SWITCH_BINARY == zwave_dev->deviceType || 
            GENERIC_TYPE_SWITCH_MULTILEVEL == zwave_dev->deviceType)
            && MODE_ALWAYSLISTENING == zwave_dev->deviceMode
            && !strcmp(zwave_dev->VRDeviceType, ST_DIMMABLE_LIGHT))
        {
            SLOGI("deviveMode = %s\n", zwave_dev->VRDeviceType);
            char deviceId[SIZE_32B];
            if(!zwave_dev->endpointNum)
            {
                snprintf(deviceId, sizeof(deviceId), "%02X", zwave_dev->localId);
            }
            else
            {
                snprintf(deviceId, sizeof(deviceId), "%04X", zwave_dev->localId);
            }
            SLOGI("start set previous status of device %s onOff %d dim %d\n", 
                    deviceId, zwave_dev->status.onOff, zwave_dev->status.dim);

            zwave_command_response_t response;
            memset(&response, 0x00, sizeof(zwave_command_response_t));

            if(zwave_dev->status.onOff)
            {
                if(zwave_dev->status.dim)
                {
                    strcpy(response.method, ST_SET_BINARY_R);
                    strcpy(response.nodeid, deviceId);
                    strcpy(response.command, ST_DIM);
                    snprintf(response.value, sizeof(response.value), "%d", zwave_dev->status.dim);
                    _set_binary_prepare(response);
                }
                else
                {
                    strcpy(response.method, ST_SET_BINARY_R);
                    strcpy(response.nodeid, deviceId);
                    strcpy(response.command, ST_ON_OFF);
                    snprintf(response.value, sizeof(response.value), "%d", zwave_dev->status.onOff);

                    _set_binary_prepare(response);
                }
            }
            else
            {
                strcpy(response.method, ST_SET_BINARY_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command, ST_ON_OFF);
                snprintf(response.value, sizeof(response.value), "%d", zwave_dev->status.onOff);

                _set_binary_prepare(response);

                /*make sure the next control is set dim*/
                if(zwave_dev->status.dim)
                {
                    zwave_dev->status.dimFirst = true;
                }
            }
            
        }
    }
    pthread_mutex_unlock(&zwave_dev_info_listMutex);
}

static int force_last_status(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    SLOGI("in force_last_status \n");
    /*get all light and switch (GENERIC_TYPE_SWITCH_BINARY, GENERIC_TYPE_SWITCH_MULTILEVEL)*/

    force_status();
    // VR_(usleep)(500);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
    ubus_send_reply(ctx, req, buff.head);

    return 0;
}

static void scene_activation_thread(void *data)
{
    if(!data)
    {
        return;
    }

    scene_infor *scene_info = (scene_infor*)data;
    zwave_command_response_t response;
    memset(&response, 0x00, sizeof(zwave_command_response_t));

    SEARCH_DATA_INIT_VAR(groupId)
    searching_database(_VR_CB_(zwave), &groupId,
                        "SELECT groupId from SCENES where deviceId='%s' AND buttonId='%s'",
                        scene_info->deviceId, scene_info->buttonId);
    if(groupId.len)
    {           
        strcpy(response.method, ST_SCENE_ACTIVATION_R);
        strcpy(response.nodeid, scene_info->deviceId);
        strcpy(response.command_class, ST_REMOTE_ASSOCIATION_ACTIVATE);
        strcpy(response.command, ST_ACTIVATE);

        response.data0 = (char *)malloc(strlen(groupId.value)+1);
        strcpy(response.data0, groupId.value);
        strncpy(response.data3, scene_info->buttonId, sizeof(response.data3));//not use in _set_specification_prepare
        
        _set_specification_prepare(response);
    }
    else
    {
        strcpy(response.method, ST_SCENE_ACTIVATION_R);
        strcpy(response.nodeid, scene_info->deviceId);
        strcpy(response.command_class, ST_REMOTE_ASSOCIATION_ACTIVATE);
        strcpy(response.command, ST_ACTIVATE);

        response.data0 = (char *)malloc(strlen(scene_info->buttonId)+1);
        strcpy(response.data0, scene_info->buttonId);
        strncpy(response.data3, scene_info->buttonId, sizeof(response.data3));

        _set_specification_prepare(response);
    }
    FREE_SEARCH_DATA_VAR(groupId);
    
    free(scene_info);
}

static int scene_activation(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    SLOGI("in force_last_status \n");

    struct blob_attr *tb[2];
    blobmsg_parse(scene_policy, ARRAY_SIZE(scene_policy), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SCENE_ACTIVATION_R));

    if(!tb[0] || !tb[1])
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_MISSING_ARGUMENT));
    }

    const char *id = blobmsg_data(tb[0]);
    const char *buttonId = blobmsg_data(tb[1]);

    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(id));
    json_object_object_add(jobj, ST_BUTTON_ID, json_object_new_string(buttonId));

    scene_infor *scene_info = (scene_infor*)malloc(sizeof(scene_infor));
    snprintf(scene_info->deviceId, sizeof(scene_info->deviceId), "%s", id);
    snprintf(scene_info->buttonId, sizeof(scene_info->buttonId), "%s", buttonId);
    pthread_t scene_activation_thread_t;
    pthread_create(&scene_activation_thread_t, NULL, (void *)&scene_activation_thread, scene_info);
    pthread_detach(scene_activation_thread_t);

    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_RECEIVED));

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

static const struct ubus_method zwave_methods[] = {
    UBUS_METHOD(ST_LIST_DEVICES, list_device, null_policy),
    UBUS_METHOD(ST_OPEN_NETWORK, open_closenetwork, null_policy),
    UBUS_METHOD(ST_CLOSE_NETWORK, open_closenetwork, null_policy),
    UBUS_METHOD(ST_REMOVE_DEVICE, remove_device, get_policy),
    UBUS_METHOD(ST_SET_BINARY, setbinary, setbinary_policy),
    UBUS_METHOD(ST_GET_BINARY, getbinary, get_policy),
    UBUS_METHOD(ST_READ_SPEC, getspecification, get_specification),
    UBUS_METHOD(ST_WRITE_SPEC, setspecification, set_specification),
    UBUS_METHOD(ST_WRITE_S_SPEC, setsecurespecification, set_specification),
    UBUS_METHOD(ST_READ_S_SPEC, getsecurespecification, get_secure_specification),
    UBUS_METHOD(ST_RESET, reset_device, null_policy),
    UBUS_METHOD(ST_IDENTIFY, identify, set_policy),
    UBUS_METHOD(ST_CHANGE_NAME, changename, set_policy),
    UBUS_METHOD(ST_ALEXA, alexa, set_policy),
    UBUS_METHOD(ST_CHECK_DEV_STATE, checking_devices_state, null_policy),
    UBUS_METHOD(ST_UPDATE_NETWORK, ubus_update_network, null_policy),
    UBUS_METHOD(ST_NETWORK, network, set_specification),
    UBUS_METHOD(ST_READ_SPEC_CRC, getspecification_CRC, specification_CRC),
    UBUS_METHOD(ST_WRITE_SPEC_CRC, setspecification_CRC, specification_CRC),
    UBUS_METHOD(ST_VENUS_ALARM, venus_alarm, alarm_policy),
    UBUS_METHOD(ST_RESET_STOP_ADD_TIMER, reset_stop_add_timer, null_policy),
    UBUS_METHOD(ST_FORCE_LAST_STATUS, force_last_status, null_policy),
    UBUS_METHOD(ST_SCENE_ACTIVATION, scene_activation, scene_policy),
#ifdef TEST
    UBUS_METHOD("test_notification", test_notification, null_policy),
#endif
};

static struct ubus_object_type zwave_object_type =
    UBUS_OBJECT_TYPE(ST_ZWAVE, zwave_methods);

static struct ubus_object zwave_object = {
    .name = ST_ZWAVE,
    .type = &zwave_object_type,
    .methods = zwave_methods,
    .n_methods = ARRAY_SIZE(zwave_methods),
};

static int init_ubus_service()
{
    const char *ubus_socket = NULL;

    uloop_init();

    ctx = ubus_connect(ubus_socket);
    if (!ctx)
    {
        fprintf(stderr, "Failed to connect to ubus\n");
        return -1;
    }

    ubus_add_uloop(ctx);

    return 0;
}

static void free_ubus_service()
{
    if(ctx)
    {
        ubus_free(ctx);
    }
    uloop_done();

    if(buff.buf)
    {
        free(buff.buf);
    }

    if(zwave_db)
    {
        sqlite3_close(zwave_db);
    }

    if(support_devs_db)
    {
        sqlite3_close(support_devs_db);
    }
}

void sig_handler(int signo)
{
    SLOGI("signo = %d\n", signo);
    if (signo == SIGINT || signo == SIGTERM || signo == SIGSEGV)
    {
        if(signo == SIGSEGV)
        {
            uint32_t *up = (uint32_t *) &signo;

            pmortem_connect_and_send(up, 8 * 1024);
            fprintf(stderr, "SIGSEGV [%d].  Best guess fault address: %08x, ra: %08x, sig return: %p\n",
                signo, up[8], up[72], __builtin_return_address(0));
        }

        zwave_command_process_enable = 0;
        zwaveDestruct();
        free_ubus_service();
        controller_reset();
        uloop_end();
        exit(1);
    }
}

void Send_ubus_notify(char *data)
{
    SLOGI("ubus notify data = %s\n", data);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, data);
    
    if(ctx)
    {
        ubus_notify(ctx, &zwave_object, ST_ZWAVE, buff.head, -1);
    }
    SLOGI("end send ubus notify\n");
}

static void cancel_adding_device_cb(struct uloop_timeout *timeout)
{
    TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));
    memset(pzwParam, 0x00, sizeof(TZWParam));
    pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_REMOVE_NODE_ZPC_AUTO;        
    pzwParam->param1=CLOSE;
    zwaveSendCommand(pzwParam);
    free(pzwParam);

    VR_(execute_system)("ubus send zwave '{\"state\":\"close_network\"}' &");

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CLOSE_NETWORK_R));
    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

    Send_ubus_notify((char*)json_object_to_json_string(jobj));
    // char *notify_message = (char*)json_object_to_json_string(jobj);
    // PushNotificationToHandler(ZWAVE_COMMAND_PROCESS_NOTIFY, 
    //                         (uint8_t*)notify_message, 
    //                         strlen(notify_message)+1);
    json_object_put(jobj);
}

void timer_Speak_Adding_handler(void *data)
{
    VR_(inform_adding_device)(NULL);
}

int VR_(set_association)(uint8_t device_id, char *association_data, uint8_t scheme)
{
    SLOGI("device %02X set association with data %s for home control id %02X", 
            device_id, association_data, zw_OwnerID.MyNodeID);

    int rc;
    char *tok, *save_tok, *pch, *stok;
    TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));

    tok = strtok_r(association_data, ";", &save_tok);
    while(tok != NULL)
    {
        SLOGI("tok = %s\n", tok);
        char association[SIZE_512B], groupId[SIZE_32B], endPoint[SIZE_32B]={"0"}, delayTime[SIZE_32B];
        strncpy(association, tok, sizeof(association));
        pch = strtok_r(association, ",", &stok);
        if(pch != NULL)
        {
            strcpy(groupId, pch);
            pch = strtok_r(NULL, ",", &stok);
        }
        if(pch != NULL)
        {
            strcpy(endPoint, pch);
            pch = strtok_r(NULL, ",", &stok);
        }
        if(pch != NULL)
        {
            strcpy(delayTime, pch);
            pch = strtok_r(NULL, ",", &stok);
        }
        //groupId, endpoint, delay time
        SLOGI("groupId = %s\n", groupId);
        SLOGI("endPoint = %s\n", endPoint);
        SLOGI("delayTime = %s\n", delayTime);

        memset(pzwParam, 0x00, sizeof(TZWParam));
        pzwParam->ret = -1;
        pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_REQUEST_SPECIFICATION_ZPC; 
        pzwParam->src_endpoint=0;
        pzwParam->dest_endpoint=htoi(endPoint);
        pzwParam->scheme = scheme;
        pzwParam->param2 = device_id;
        pzwParam->param3 = 1; //always force

        pzwParam->cmd_set.cmd[0] = COMMAND_CLASS_ASSOCIATION;
        pzwParam->cmd_set.cmd[1] = ASSOCIATION_SET;
        pzwParam->cmd_set.cmd[2] = htoi(groupId);
        pzwParam->cmd_set.cmd[3] = zw_OwnerID.MyNodeID; //node id, that is received notification
        pzwParam->cmd_set.cmd_length=4;
        //sleep to command affect to device.
        int delayTimeMs = 1000 * htoi(delayTime);
        timingGetClockSystem(&txTiming);
        while(1)
        {
            if (timingGetElapsedMSec(&txTiming) > delayTimeMs)
            {
                timingGetClockSystemStop(&txTiming);
                SLOGI("delayTimeMs: %d is timeout!", delayTimeMs);
                break;
            }
            usleep(1000);
        }
        zwaveSendCommand(pzwParam);
        rc = pzwParam->ret;
        if(rc)
        {
            SAFE_FREE(pzwParam);
            return rc;
        }
        tok = strtok_r(NULL, ";", &save_tok);
    }

    SAFE_FREE(pzwParam);
    return rc;
}

int VR_(set_configuration)(uint8_t device_id, char *configuration_data, uint8_t scheme, int wakeup_only)
{
    SLOGI("device %02X set configuration with data %s\n", 
            device_id, configuration_data);
    int rc;
    char *tok, *save_tok, *pch, *stok;
    TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));

    tok = strtok_r(configuration_data, ";", &save_tok);
    while(tok != NULL)
    {
        SLOGI("tok = %s\n", tok);
        char configuration[SIZE_512B], configId[SIZE_32B], length[SIZE_32B], value[SIZE_32B];
        char endPoint[SIZE_32B] = {"0"}; 
        strncpy(configuration, tok, sizeof(configuration));
        pch = strtok_r(configuration, ",", &stok);
        if(pch != NULL)
        {
            strcpy(configId, pch);
            pch = strtok_r(NULL, ",", &stok);
        }
        if(pch != NULL)
        {
            strcpy(length, pch);
            pch = strtok_r(NULL, ",", &stok);
        }
        if(pch != NULL)
        {
            strcpy(value, pch);
            pch = strtok_r(NULL, ",", &stok);
        }
        if(pch != NULL)
        {
            strcpy(endPoint, pch);
            pch = strtok_r(NULL, ",", &stok);
        }

        SLOGI("configId = %s\n", configId);
        SLOGI("length = %s\n", length);
        SLOGI("value = %s\n", value);
        SLOGI("endPoint = %s\n", endPoint);
        SLOGI("scheme = %d\n", scheme);

        int size = htoi(length);
        memset(pzwParam, 0x00, sizeof(TZWParam));
        pzwParam->ret = -1;
        pzwParam->command = COMMAND_CLASS_SPECIFIC_NODE_REQUEST_SPECIFICATION_ZPC; 
        pzwParam->src_endpoint=0;
        pzwParam->dest_endpoint=htoi(endPoint);
        pzwParam->scheme = scheme;
        pzwParam->param2 = device_id;
        pzwParam->param3 = 1; //always force

        if(wakeup_only)
        {
            if(!strcmp(configId,"WK"))
            {
                int wake_up_interval=(int)strtol(value, NULL, 16);

                pzwParam->cmd_set.cmd[0] = COMMAND_CLASS_WAKE_UP;
                pzwParam->cmd_set.cmd[1] = WAKE_UP_INTERVAL_SET;
                pzwParam->cmd_set.cmd[2]=(wake_up_interval>>16)&0xFF;
                pzwParam->cmd_set.cmd[3]=(wake_up_interval>>8)&0xFF;
                pzwParam->cmd_set.cmd[4]=(wake_up_interval>>0)&0xFF;
                pzwParam->cmd_set.cmd[5]=(uint8_t)zw_OwnerID.MyNodeID;
                pzwParam->cmd_set.cmd_length = 6;

                zwaveSendCommand(pzwParam);
                rc = pzwParam->ret;
                if(rc)
                {
                    free(pzwParam);
                    return rc;
                }
            }
        }
        else
        {   
            if(strcmp(configId,"WK"))
            {
                pzwParam->cmd_set.cmd[0] = COMMAND_CLASS_CONFIGURATION;
                pzwParam->cmd_set.cmd[1] = CONFIGURATION_SET;
                pzwParam->cmd_set.cmd[2] = htoi(configId);
                pzwParam->cmd_set.cmd[3] = size;

                int config_value = (int)strtol(value, NULL, 16);
                if(size == 1)
                {
                    pzwParam->cmd_set.cmd[4] = config_value;
                }
                else if(size == 2 )
                {
                    uint16_t temp_data = (uint16_t)config_value;
                    SWAP(temp_data);
                    memcpy(&pzwParam->cmd_set.cmd[4], (uint8_t*)&temp_data, sizeof(temp_data));
                }
                else if(size == 3 )
                {
                    uint32_t temp_data = (uint32_t)config_value;
                    temp_data = temp_data<<8;
                    SWAP(temp_data);
                    memcpy(&pzwParam->cmd_set.cmd[4], (uint8_t*)&temp_data, 3);
                }
                else if(size == 4)
                {
                    uint32_t temp_data = (uint32_t)config_value;
                    SWAP(temp_data);
                    memcpy(&pzwParam->cmd_set.cmd[4], (uint8_t*)&temp_data, sizeof(temp_data));
                }

                pzwParam->cmd_set.cmd_length = 4+size;
                zwaveSendCommand(pzwParam);
                rc = pzwParam->ret;
                if(rc)
                {
                    free(pzwParam);
                    return rc;
                }
            }
        }

        tok = strtok_r(NULL, ";", &save_tok);
    }

    free(pzwParam);
    return rc;
}

static void update_association_node_list(char *nodeId, char *json_string, int mode)
{
    if(!json_string || !nodeId)
    {
        SLOGE("input missing\n");
        return;
    }

    json_object *dataObj = VR_(create_json_object)(json_string);
    if(!dataObj)
    {
        SLOGE("input is not json format\n");
        return;
    }

    CHECK_JSON_OBJECT_EXIST(groupIdObj, dataObj, ST_ZW_GROUP_ID, done);
    CHECK_JSON_OBJECT_EXIST(nodeListObj, dataObj, ST_ZW_NODE_LIST, done);
    enum json_type type = json_object_get_type(nodeListObj);
    if(json_type_array != type)
    {
        goto done;
    }

    int i;
    char nodeList[SIZE_256B];
    char groupId[SIZE_256B];
    int arraylen = json_object_array_length(nodeListObj);
    const char *groupIdTmp = json_object_get_string(groupIdObj);
    if(!groupIdTmp)
    {
         goto done;
    }
    
    if(strlen(groupIdTmp)<2)
    {
        snprintf(groupId, sizeof(groupId)-1, "0%s", groupIdTmp);
    }
    else
    {
        snprintf(groupId, sizeof(groupId)-1, "%s", groupIdTmp);
    }

    for(i = 0; i<arraylen; i++)
    {
        json_object *jvalue = json_object_array_get_idx(nodeListObj, i);
        if(jvalue)
        {
            uint8_t nodeValue = (uint8_t)strtol(json_object_get_string(jvalue), NULL, 0);
            if(!nodeValue)
            {
                continue;
            }

            if(!i)
            {
                sprintf(nodeList, "%02X", nodeValue);
            }
            else
            {
                sprintf(nodeList+strlen(nodeList), ",%02X", nodeValue);
            }
        }
    }
    SLOGI("nodeList = %s\n", nodeList);
    if(mode)
    {
        remove_ass_node_list_database(_VR_CB_(zwave), nodeId, groupId, nodeList);
    }
    else
    {
        set_ass_node_list_database(_VR_CB_(zwave), nodeId, groupId, nodeList, ST_UPDATE, 10);
    }
done:
    json_object_put(dataObj);
    return;
}

static void return_node_list(json_object *jobj, TZWParam *pzwParam)
{
    if(!jobj || !pzwParam)
    {
        return;
    }
    uint8_t i;
    json_object *jarray_node_list = json_object_new_array();
    for (i=0; i < pzwParam->param1; i++)
    {
        json_object * jobj_node_list=json_object_new_object();
        json_object_object_add(jobj_node_list, ST_NODE_ID,json_object_new_int(pzwParam->multi_node[i].nodeId));
        json_object_object_add(jobj_node_list, ST_NODE_MODE,json_object_new_int(pzwParam->multi_node[i].mode));

        json_object_array_add(jarray_node_list,jobj_node_list);
    }
    json_object_object_add(jobj, ST_NODE_LIST, jarray_node_list);

}

static void return_controller_infor(json_object *jobj, TZWParam *pzwParam)
{
    if(!jobj || !pzwParam)
    {
        return;
    }

    uint8_t i, j;
    json_object *jarray_capability = json_object_new_array();
    char homeID[9];
    sprintf(homeID, "%08X", pzwParam->param4);
    char nodeID[3];
    sprintf(nodeID, "%02X", pzwParam->node.node_id);
    char Type[3];
    sprintf(Type, "%02X", pzwParam->node.node_type);
    json_object_object_add(jobj, ST_HOMEID, json_object_new_string(homeID));
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeID));
    json_object_object_add(jobj, ST_CONTROLLER_CAPABILITY, json_object_new_int(pzwParam->param2));
    json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(Type));
    json_object_object_add(jobj, ST_NODE_MODE, json_object_new_int(pzwParam->node.node_info.mode));
    json_object_object_add(jobj, ST_NODE_FLAGS, json_object_new_int(pzwParam->node.node_flags));
    json_object_object_add(jobj, ST_TYPE_BASIC, json_object_new_int(pzwParam->node.node_info.nodeType.basic));
    json_object_object_add(jobj, ST_TYPE_GENERIC, json_object_new_int(pzwParam->node.node_info.nodeType.generic));
    json_object_object_add(jobj, ST_TYPE_SPECIFIC, json_object_new_int(pzwParam->node.node_info.nodeType.specific));

    for (i = 0; i < pzwParam->node.node_capability.noCapability; i++)
    {
        json_object_array_add(jarray_capability, json_object_new_int(pzwParam->node.node_capability.aCapability[i]));
    }
    json_object_object_add(jobj, ST_NON_SECURE_CAPABILITY, jarray_capability);
    json_object_object_add(jobj, ST_NODE_SECURE_SCHEME, json_object_new_int(pzwParam->node.node_secure_scheme));
    json_object *jarray_secure_capability = json_object_new_array();

    for (i = 0; i < pzwParam->node.node_capability_secureV0.noCapability; i++)
    {
        json_object_array_add(jarray_secure_capability, json_object_new_int(pzwParam->node.node_capability_secureV0.aCapability[i]));
    }
    json_object_object_add(jobj, ST_SECURE_CAPABILITY, jarray_secure_capability);

    json_object *jarray_secure_2_capability = json_object_new_array();
    
    for (i = 0; i < pzwParam->node.node_capability_secureV2.noCapability; i++)
    {
        json_object_array_add(jarray_secure_2_capability, json_object_new_int(pzwParam->node.node_capability_secureV2.aCapability[i]));
    }
    json_object_object_add(jobj, ST_SECURE_2_CAPABILITY, jarray_secure_2_capability);
    

    json_object_object_add(jobj, ST_NO_ENDPOINT, json_object_new_int(pzwParam->node.no_endpoint));

    json_object *jarray_multichanel_capability = json_object_new_array();

    for (i = 0; i < pzwParam->node.no_endpoint; i++)
    {
        json_object *jobj_multichannel_capability = json_object_new_object();
        json_object_object_add(jobj_multichannel_capability, ST_ENDPOINT, json_object_new_int(pzwParam->node.multi_channel_capability[i].endpoint));
        json_object_object_add(jobj_multichannel_capability, ST_GENERIC_DEVICE_CLASS, json_object_new_int(pzwParam->node.multi_channel_capability[i].generic_device_class));
        json_object_object_add(jobj_multichannel_capability, ST_SPECIFIC_DEVICE_CLASS, json_object_new_int(pzwParam->node.multi_channel_capability[i].specific_device_class));
        json_object *jarray_endpoint_capability = json_object_new_array();
        for (j = 0; j < pzwParam->node.multi_channel_capability[i].endpoint_capability.noCapability; j++)
        {
            json_object_array_add(jarray_endpoint_capability, json_object_new_int(pzwParam->node.multi_channel_capability[i].endpoint_capability.aCapability[j]));
        }
        json_object_object_add(jobj_multichannel_capability, ST_CAPABILITY, jarray_endpoint_capability);

        json_object_array_add(jarray_multichanel_capability, jobj_multichannel_capability);
    }
    json_object_object_add(jobj, ST_MULTICHANNEL_CAPABILITY, jarray_multichanel_capability);
    json_object_object_add(jobj, ST_IS_ZWAVE_PLUS, json_object_new_int(pzwParam->node.is_zp_node));
    if (pzwParam->node.is_zp_node > 0)
    {
        json_object_object_add(jobj, ST_ROLE_TYPE, json_object_new_int(pzwParam->node.zp_info.role_type));
        json_object_object_add(jobj, ST_NODE_TYPE, json_object_new_int(pzwParam->node.zp_info.node_type));
    }
}

static void return_neighbor_dump(json_object *jobj, TZWParam *pzwParam)
{
    if(!jobj || !pzwParam)
    {
        return;
    }

    uint8_t i, j;
    int index = 0;
    char id[3] = {'\0'};
    char tempId[3] = {'\0'};

    json_object *jarray_node_list = json_object_new_array();

    for (i = 0; i < pzwParam->param1 ; i++)
    {
        sprintf(id, "%02X", pzwParam->raw_data[index]);
        json_object *jobj_node = json_object_new_object();

        json_object *jarray_neighbor = json_object_new_array();
        uint8_t neighbors = pzwParam->raw_data[index + 1];        
        for (j = index + 2; j < index + 2+ neighbors ; j++)
        {
            sprintf(tempId, "%02X", pzwParam->raw_data[j]);
            json_object_array_add(jarray_neighbor, json_object_new_string(tempId));
        }
        json_object_object_add(jobj_node, id, jarray_neighbor);
        json_object_array_add(jarray_node_list, jobj_node);
        index = index + 2 + neighbors;
    }

    json_object_object_add(jobj, ST_DUMP_NEIGHBORS, jarray_node_list);

}

static void return_priority_route(json_object *jobj, TZWParam *pzwParam)
{
    if(!jobj || !pzwParam)
    {
        return;
    }

    uint8_t i;
    char tempId[3] = {'\0'};

    json_object *jarray_node_list = json_object_new_array();

    for (i = 0; i < pzwParam->data_out.cmd_length - 1 ; i++)
    {
        if (!pzwParam->data_out.cmd[i]) break;
        sprintf(tempId, "%02X", pzwParam->data_out.cmd[i]);
        json_object_array_add(jarray_node_list, json_object_new_string(tempId));
    }

    switch (pzwParam->data_out.cmd[pzwParam->data_out.cmd_length - 1])
    {
        case ZW_LAST_WORKING_ROUTE_SPEED_AUTO:
            json_object_object_add(jobj, ST_ROUTE_CONFIG, json_object_new_string(ST_AUTO));
            break;

        case ZW_LAST_WORKING_ROUTE_SPEED_100K:
            json_object_object_add(jobj, ST_ROUTE_CONFIG, json_object_new_string(ST_100K));
            break;

        case ZW_LAST_WORKING_ROUTE_SPEED_40K:
            json_object_object_add(jobj, ST_ROUTE_CONFIG, json_object_new_string(ST_40K));
            break;

        case ZW_LAST_WORKING_ROUTE_SPEED_9600:
            json_object_object_add(jobj, ST_ROUTE_CONFIG, json_object_new_string(ST_9600));
            break;
    }
    json_object_object_add(jobj, ST_PRIORITY_ROUTE, jarray_node_list);

}


static uint8_t g_last_node_id = 0;
static uint8_t g_last_node_is_S0 = 0;
/*A S0 device should sleep SECURE_DEVICE_DELAY_INTERNAL between 2 commands in the row.*/
static int wait_time_valid_to_execute(int queueId, int totalCmdInQueue,
                                      zwave_command_queue_t *commandInQ,
                                      zwave_dev_info_t *zwave_dev)
{
    int res = 1;
    if(!commandInQ || !commandInQ->pzwParam)
    {
        return res;
    }

    TZWParam *pzwParam = commandInQ->pzwParam;

    /*if two commands in the sequence of one device, slow it down.*/
    if(pzwParam->scheme != SEC_0_SCHEME)
    {
        if(pzwParam->param2 == g_last_node_id)
        {
            usleep(50000);
        }
        else
        {
            g_last_node_id = pzwParam->param2;
        }

        g_last_node_is_S0 = 0;
        return res;
    }

    zwave_dev_info_t *parrent_dev = zwave_dev; /*propose this is farther*/
    if(pzwParam->dest_endpoint) /*this is child device*/
    {
        char parentId[SIZE_32B];
        snprintf(parentId, sizeof(parentId), "%02X", pzwParam->param2);
        parrent_dev = get_zwave_dev_from_id(parentId);
    }

    if(!parrent_dev)
    {
        return res;
    }

    uint64_t currentTime = get_timestamp_in_microseconds();
    uint64_t wait_time = currentTime-parrent_dev->lastExecuteTime;
    if(wait_time > SECURE_DEVICE_DELAY_INTERNAL)
    {
        parrent_dev->lastExecuteTime = currentTime;
    }
    else
    {
        uint32_t sleepMore = SECURE_DEVICE_DELAY_INTERNAL-wait_time;
        SLOGI("control device 0x%02X too fast need sleep %d (us)\n", pzwParam->param2, sleepMore);
        if(totalCmdInQueue && (DEAD_PRIORITY != queueId))
        {
            if(!re_insert_to_a_queue(DEAD_PRIORITY, commandInQ->response, commandInQ->pzwParam))
            {
                /*after get_and_delete_first_element_in_queue, totalCmd shoud increase if re-insert*/
                zwave_dev->totalCmd++;
                commandInQ->response.cmdId = zwave_dev->totalCmd;
                return 0;
            }
        }

        if(sleepMore > 5000000)
        {
            parrent_dev->lastExecuteTime = currentTime;
        }
        else
        {
            if(sleepMore > 1000000)
            {
                sleep(sleepMore/1000000);
            }
            else
            {
                usleep(sleepMore);
            }

            parrent_dev->lastExecuteTime = currentTime + sleepMore;
        }
    }

    if(!g_last_node_id)
    {
        g_last_node_id = pzwParam->param2;
    }
    else if(pzwParam->param2 != g_last_node_id)
    {
        if(g_last_node_is_S0)
        {
            SLOGI("LAST NODE IS SECURITY DELAY %d(us)\n", SECURE_DEVICE_DELAY_EXTERNAL);
            usleep(SECURE_DEVICE_DELAY_EXTERNAL);
        }
        g_last_node_id = pzwParam->param2;
    }

    g_last_node_is_S0 = 1;
    return res;
}

void zwave_command_process(void *data)
{
    SLOGI("Start zwave command process\n");
    while(zwave_command_process_enable)
    {
        int re_insert_to_queue = 0;
        int queueId = NO_COMMAND;
        int send_zwave_command = 1;
        char str_value[8];
        queueId = decide_queue_to_execute();
        
        if(queueId == NO_COMMAND)
        {
            g_last_node_is_S0 = 0;
            g_last_node_id = 0;
            usleep(10000);
            continue;
        }

        zwave_queue_priority_t *queueInfo = get_queue_from_queueId(queueId);
        if(!queueInfo)
        {
            SLOGW("not found queue id %d\n", queueId);
            usleep(1000);
            continue;
        }

        if(queueInfo->queue.command_index <= 0)
        {
            SLOGW("no command in queue %d, command_index = %d\n", queueId, queueInfo->queue.command_index);
            usleep(1000);
            continue;
        }

        if(check_command_in_queue_is_valid_to_execute(queueInfo))
        {
            SLOGI("ignore queue %d because command in queue is not valid to run\n", queueId);
            usleep(1000);
            continue;
        }

        zwave_command_queue_t *commandInQ = get_and_delete_first_element_in_queue(&queueInfo->queue, &queueInfo->queueMutex);
        if(!commandInQ)
        {
            SLOGW("commandInQ not found\n");
            usleep(1000);
            continue;
        }

        SLOGI("execute queue with Id = %d\n", queueId);

        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));

        int totalCmdInQueue = total_command_in_queue();
        zwave_dev_info_t *zwave_dev = get_zwave_dev_from_id(commandInQ->response.nodeid);
        if(!wait_time_valid_to_execute(queueId, totalCmdInQueue, commandInQ, zwave_dev))
        {
            re_insert_to_queue = 1;
            goto done;
        }

        TZWParam *pzwParam = commandInQ->pzwParam;
        char *command = commandInQ->response.command;
        char *value = commandInQ->response.value;
        char *nodeid = commandInQ->response.nodeid;
        char *method = commandInQ->response.method;

        if((DEAD_PRIORITY != queueId) && (totalCmdInQueue > 3))
        {
            /*burst system by set timeout 3s*/
            if(pzwParam->scheme != SEC_0_SCHEME)
            {
                pzwParam->param4 = BURST_TIME_DELAY;
            }
            else
            {
                pzwParam->param4 = BURST_TIME_DELAY*2;
            }

            if(zwave_dev)
            {
                if(zwave_dev->deviceMode == MODE_FREQUENTLYLISTENING
                    || zwave_dev->deviceMode == MODE_NONLISTENING)
                {
                    pzwParam->param4 = 0;
                }
            }
        }

        // SLOGI("####### START CONTROL DEVICE %s \n\n", nodeid)
        // SLOGI("pzwParam->ret = %02X\n", pzwParam->ret);
        // SLOGI("pzwParam->src_endpoint = %02X\n", pzwParam->src_endpoint);
        // SLOGI("pzwParam->dest_endpoint = %02X\n", pzwParam->dest_endpoint);
        // SLOGI("pzwParam->scheme = %02X\n", pzwParam->scheme);
        // SLOGI("pzwParam->command = %02X\n", pzwParam->command);
        // SLOGI("pzwParam->param2 = %02X\n", pzwParam->param2);
        // SLOGI("pzwParam->param3 = %02X\n", pzwParam->param3);
        // SLOGI("pzwParam->param4 = %d\n", pzwParam->param4);
        // int j;
        // for(j=0; j<pzwParam->cmd_set.cmd_length; j++)
        // {
        //     SLOGI("pzwParam->cmd_set.cmd[%d] = %02X\n", j, pzwParam->cmd_set.cmd[j]);
        // }

        if(send_zwave_command)
        {
            // SLOGI("############# start send zwave command\n");
            zwaveSendCommand(pzwParam);
            // SLOGI("############# end send zwave command\n");
        }

        if(zwave_dev && MODE_ALWAYSLISTENING == zwave_dev->deviceMode)
        {
            if(DEAD_PRIORITY != queueId && 
                pzwParam->ret == -1)
            {
                /*reset timeout value*/
                pzwParam->param4 = BURST_TIME_DELAY*3;
                if(!re_insert_to_a_queue(DEAD_PRIORITY, commandInQ->response, commandInQ->pzwParam))
                {
                    /*after get_and_delete_first_element_in_queue, totalCmd shoud increase if re-insert*/
                    zwave_dev->totalCmd++;
                    commandInQ->response.cmdId = zwave_dev->totalCmd;
                    re_insert_to_queue = 1;
                    goto done;
                }
            }
        }

        JSON_ADD_STRING_SAFE(jobj, ST_METHOD, method);
        JSON_ADD_STRING_SAFE(jobj, ST_DEVICE_ID, nodeid);

        SLOGI("method = %s\n", method);

        if(nodeid && strcmp(nodeid, "FF"))
        {
            update_dev_priority(nodeid, pzwParam);

            /*control shoud return timestampt for all cases*/
            char timestampt[SIZE_32B];
            sprintf(timestampt, "%u", (unsigned)time(NULL));
            if (pzwParam->ret == 0)
            {
                update_zwave_status(_VR_CB_(zwave), nodeid, ST_LAST_UPDATE, timestampt, ST_REPLACE, 0);
                update_zwave_status(_VR_CB_(zwave), nodeid, ST_STATE, ST_ALIVE, ST_REPLACE, 0);
                json_object_object_add(jobj, ST_CLOUD_TIME_STAMP, json_object_new_string(timestampt));
            }
            else if (pzwParam->ret==-1)
            {
                if(!g_open_network)
                {
                    update_zwave_status(_VR_CB_(zwave), nodeid, ST_STATE, ST_DEAD, ST_REPLACE, 0);
                    json_object_object_add(jobj, ST_CLOUD_TIME_STAMP, json_object_new_string(timestampt));
                }
            }
        }

        if(!strcmp(method, ST_SET_BINARY_R))
        {
            JSON_ADD_STRING_SAFE(jobj, ST_COMMAND, command);
            JSON_ADD_STRING_SAFE(jobj, ST_VALUE, value);

            if (pzwParam->ret==0)
            {
                if(pzwParam->cmd_set.cmd[0] == COMMAND_CLASS_DOOR_LOCK)
                {
                    set_register_database(_VR_CB_(zwave), nodeid, ST_ON_OFF, value, ST_REPLACE, 0);

                    /*work around for making dev->status.onOff is difference with notify*/
                    if(!strcmp(value, ST_OPEN_VALUE))
                    {
                        update_zwave_dev_from_id((char*) nodeid, ST_ON_OFF, ST_OPENING_VALUE);
                    }
                    else if(!strcmp(value, ST_CLOSE_VALUE))
                    {
                        update_zwave_dev_from_id((char*) nodeid, ST_ON_OFF, ST_CLOSING_VALUE);
                    }
                }
                else if(pzwParam->cmd_set.cmd[0] == COMMAND_CLASS_BARRIER_OPERATOR)
                {
                    /*inform deviceType with garage door for telling rule handle should ignore this message*/
                    JSON_ADD_STRING_SAFE(jobj, ST_DEVICE_TYPE, ST_GARAGE_DOOR_CONTROLLER);
                    if(!strcmp(value, ST_OPEN_VALUE) || !strcmp(value, ST_OPEN))
                    {
                        update_zwave_status(_VR_CB_(zwave), nodeid, ST_ON_OFF, ST_OPEN_VALUE, ST_REPLACE, 0);
                    }
                    else if(!strcmp(value, ST_CLOSE_VALUE) || !strcmp(value, ST_CLOSE))
                    {
                        update_zwave_status(_VR_CB_(zwave), nodeid, ST_ON_OFF, ST_CLOSE_VALUE, ST_REPLACE, 0);
                    }
                }
                else if(pzwParam->cmd_set.cmd[0] == COMMAND_CLASS_THERMOSTAT_MODE)
                {
                    update_zwave_status(_VR_CB_(zwave), nodeid, ST_ON_OFF, value, ST_REPLACE, 0);
                }
                else if(pzwParam->cmd_set.cmd[0] == COMMAND_CLASS_THERMOSTAT_SETPOINT)
                {
                    // char *mode = commandInQ->response.data0;
                    // set_thermostat_setpoint_value_database(_VR_CB_(zwave), ID, mode, value, ST_MULTILEVEL_SENSOR_LABEL_CELCIUS);
                }
                else
                {
                    if(!strcasecmp(command, ST_ON_OFF))
                    {
                        if(zwave_dev)
                        {
                            if(!strcmp(zwave_dev->VRDeviceType, ST_WATER_VALVE))
                            {
                                const char *alarm_type = ST_WATER_VALVE_CLOSED;
                                int status = 0;
                                if(!strcmp(value, ST_OPEN_VALUE))
                                {
                                    alarm_type = ST_WATER_VALVE_OPEN;
                                    status = 1;
                                }

                                if(zwave_dev->status.onOff != status)
                                {
                                    alarm_voice_inform(NULL, nodeid, (char*)alarm_type);
                                }
                            }
                        }
                        update_zwave_status(_VR_CB_(zwave), nodeid, ST_ON_OFF, value, ST_REPLACE, 0);
                    }
                    else if(!strcasecmp(command, ST_DIM))
                    {
                        update_zwave_status(_VR_CB_(zwave), nodeid, ST_ON_OFF, ST_OPEN_VALUE, ST_REPLACE, 0);
                        update_zwave_status(_VR_CB_(zwave), nodeid, ST_DIM, value, ST_REPLACE, 0);
                    }
                }
                JSON_ADD_STRING_SAFE(jobj, ST_STATUS, ST_SUCCESSFUL);
            }
            else if (pzwParam->ret==-1)
            {
                if(!g_open_network)
                {
                    JSON_ADD_STRING_SAFE(jobj, ST_STATUS, ST_FAILED);
                }
                else
                {
                    JSON_ADD_STRING_SAFE(jobj, ST_STATUS, ST_NETWORK_STILL_OPEN);
                }
                JSON_ADD_STRING_SAFE(jobj, ST_REASON, ST_TIMEOUT);
            }
            else
            {
                JSON_ADD_STRING_SAFE(jobj, ST_STATUS, ST_FAILED);
                JSON_ADD_STRING_SAFE(jobj, ST_REASON, "node is not in network");
            }

            char *notify_message = (char*)json_object_to_json_string(jobj);
            PushNotificationToHandler(ZWAVE_COMMAND_PROCESS_NOTIFY, 
                                    (uint8_t*)notify_message, 
                                    strlen(notify_message)+1);
        }
        else if(!strcmp(method, ST_GET_BINARY_R))
        {
            if (pzwParam->ret==0) 
            {

            }
            else if (pzwParam->ret==-1 || pzwParam->ret==2)
            {
                if(!g_open_network)
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                }
                else
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NETWORK_STILL_OPEN));
                }
                
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));

                char *notify_message = (char*)json_object_to_json_string(jobj);
                PushNotificationToHandler(ZWAVE_COMMAND_PROCESS_NOTIFY, 
                                        (uint8_t*)notify_message, 
                                        strlen(notify_message)+1);
            }
        }
        else if(!strcmp(method, ST_READ_SPEC_R))
        {
            char *CommandClass = commandInQ->response.command_class;
            char *data0 = commandInQ->response.data0;
            char *data1 = commandInQ->response.data1;
            char *data2 = commandInQ->response.data2;

            json_object *commandinfo = json_object_new_object();
            JSON_ADD_STRING_SAFE(commandinfo, ST_CLASS, CommandClass);
            JSON_ADD_STRING_SAFE(commandinfo, ST_COMMAND, command);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA0, data0);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA1, data1);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA2, data2);
            json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

            if (pzwParam->ret==0) 
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }
            else if (pzwParam->ret==-1) 
            {
                if(!g_open_network)
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                }
                else
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NETWORK_STILL_OPEN));
                }
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }
            else if (pzwParam->ret==2) 
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("Please press button on device to make device wakeup"));
            }

            char *notify_message = (char*)json_object_to_json_string(jobj);
            PushNotificationToHandler(ZWAVE_COMMAND_PROCESS_NOTIFY, 
                                    (uint8_t*)notify_message, 
                                    strlen(notify_message)+1);
        }
        else if(!strcmp(method, ST_WRITE_SPEC_R))
        {
            char *CommandClass = commandInQ->response.command_class;
            char *data0 = commandInQ->response.data0;
            char *data1 = commandInQ->response.data1;
            char *data2 = commandInQ->response.data2;

            json_object *commandinfo = json_object_new_object();
            JSON_ADD_STRING_SAFE(commandinfo, ST_CLASS, CommandClass);
            JSON_ADD_STRING_SAFE(commandinfo, ST_COMMAND, command);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA0, data0);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA1, data1);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA2, data2);
            json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

            if (pzwParam->ret==0)
            {
                if(!strcmp(CommandClass,ST_BARRIER_OPERATOR))
                {
                    if(!data0 || !data1)
                    {
                        goto done;
                    }

                    if(!strcmp(command, ST_SIGNAL_SET))
                    {
                        char *signalName = data0;
                        char *signalState = data1;
                        set_register_database(_VR_CB_(zwave), nodeid, signalName, signalState, ST_REPLACE, 0);
                    }
                }
                else if(!strcmp(CommandClass,ST_ASSOCIATION))
                {
                    if(!data0 || !data1)
                    {
                        goto done;
                    }

                    if(!strcasecmp(command,ST_SET))
                    {
                        char groupId[SIZE_32B];
                        char *nodeList = data1;
                        if(strlen(data0) < 2)
                        {
                            snprintf(groupId, SIZE_32B-1,"0%s", data0);
                        }
                        else
                        {
                            snprintf(groupId, SIZE_32B-1,"%s", data0);
                        }
                        set_ass_node_list_database(_VR_CB_(zwave), nodeid, groupId, nodeList, ST_UPDATE, 10);
                    }
                    else if(!strcasecmp(command,ST_REMOVE))
                    {
                        char groupId[SIZE_32B];
                        char *nodeList = data1;

                        if(strlen(data0) < 2)
                        {
                            snprintf(groupId, SIZE_32B-1,"0%s", data0);
                        }
                        else
                        {
                            snprintf(groupId, SIZE_32B-1,"%s", data0);
                        }

                        if(!strcmp(nodeList,"00") 
                            || !strcmp(nodeList,"0")
                            || !strcmp(nodeList,"00,")
                            || !strcmp(nodeList,"0,")
                            )
                        {
                            remove_all_ass_node_database(_VR_(zwave), nodeid, groupId);
                        }
                        else
                        {
                            remove_ass_node_list_database(_VR_CB_(zwave), nodeid, groupId, nodeList);
                        }
                    }
                }
                else if(!strcmp(CommandClass,ST_CONFIGURATION))
                {
                    if(!data0 || !data1)
                    {
                        goto done;
                    }

                    char *paramName = data0;
                    char *paramValue = data1;
                    set_register_database(_VR_CB_(zwave), nodeid, paramName, paramValue, ST_REPLACE, 0);

                    if(!strcmp(paramName, ST_TEMPERATURE_LOW_THRESHOLD)
                        || !strcmp(paramName, ST_TEMPERATURE_HIGH_THRESHOLD))
                    {
                        get_sound_temperature_threshold(paramValue);
                    }
                    else if(!strcmp(paramName, ST_COLORFUL_MODE))
                    {
                        get_specific_action_color_mode(nodeid);
                    }
                }
                else if(!strcmp(CommandClass,ST_WAKE_UP))
                {
                    // set_data_database(_VR_CB_(zwave), nodeid, ST_WAKE_UP, data1, data0, ST_REPLACE, 0);
                }
                else if(!strcmp(CommandClass,ST_SWITCH_COLOR))
                {
                    if(!data0)
                    {
                        goto done;
                    }

                    set_register_database(_VR_CB_(zwave), nodeid, ST_SWITCH_COLOR, data0, ST_REPLACE, 0);
                }
                else if(!strcmp(CommandClass,ST_THERMOSTAT_MODE))
                {
                    if(!data0)
                    {
                        goto done;
                    }

                    char *mode = data0;
                    if(!strcmp(mode, ST_OFF))
                    {
                        SEARCH_DATA_INIT_VAR(thermostat_mode);
                        searching_database(_VR_CB_(zwave), &thermostat_mode, 
                                            "SELECT register from FEATURES where deviceId='%s' AND featureId='%s'", 
                                            nodeid, ST_THERMOSTAT_MODE);
                        if(thermostat_mode.len)
                        {
                            if(strcmp(thermostat_mode.value, ST_OFF))
                            {
                                set_register_database(_VR_CB_(zwave), nodeid, ST_THERMOSTAT_PREVIOUS_MODE, 
                                                        thermostat_mode.value, ST_REPLACE, 0);
                            }
                        }
                        else
                        {
                            set_register_database(_VR_CB_(zwave), nodeid, ST_THERMOSTAT_PREVIOUS_MODE, 
                                                    ST_COOL, ST_REPLACE, 0);
                        }
                        FREE_SEARCH_DATA_VAR(thermostat_mode);
                    }

                    if(!strcmp(mode, ST_ON))
                    {
                        //set_register_database(_VR_CB_(zwave), nodeid, ST_THERMOSTAT_MODE, mode, ST_REPLACE, 0);
                    }
                    else
                    {
                        set_register_database(_VR_CB_(zwave), nodeid, ST_THERMOSTAT_MODE, mode, ST_REPLACE, 0);
                    }
                }
                else if(!strcmp(CommandClass,ST_THERMOSTAT_FAN_MODE))
                {
                    if(!data0)
                    {
                        goto done;
                    }

                    char *mode = data0;
                    set_register_database(_VR_CB_(zwave), nodeid, ST_THERMOSTAT_FAN_MODE, mode, ST_REPLACE, 0);
                }
                else if(!strcmp(CommandClass,ST_POWER_LEVEL))
                {
                    if(!data0)
                    {
                        goto done;
                    }

                    char *level = data0;
                    set_register_database(_VR_CB_(zwave), nodeid, ST_POWER_LEVEL, level, ST_REPLACE, 0);
                }
                else if(!strcmp(CommandClass,ST_THERMOSTAT_SETPOINT))
                {
                    if(!data0 || !data1 || !data2)
                    {
                        goto done;
                    }

                    char *setPointMode = data0;
                    char *value = data1;
                    char *unit = data2;
                    set_thermostat_setpoint_value_database(_VR_CB_(zwave), nodeid, setPointMode, value, unit);
                }
                else if(!strcmp(CommandClass, ST_METER) && !strcasecmp(command,ST_RESET))
                {
                    // char *meterType = data0;
                    // char *meterUnit = data1;
                    reset_all_meter_value_database(_VR_(zwave), nodeid);
                }
                else if(!strcmp(CommandClass, ST_SCENE_CONTROLLER_CONF))
                {
                    if(!data0 || !data1)
                    {
                        goto done;
                    }

                    char groupId[SIZE_32B];
                    // char *groupId = data0;
                    char *buttonId = data1;

                    if(strlen(data0) < 2)
                    {
                        snprintf(groupId, sizeof(groupId), "0%s", data0);
                    }
                    else
                    {
                        snprintf(groupId, sizeof(groupId), "%s", data0);
                    }
                    // char *sceneId = data2;
                    set_scene_button_database(_VR_CB_(zwave), nodeid, groupId, buttonId, NULL);
                }
                else 
                {
                    set_register_database(_VR_CB_(zwave), nodeid, CommandClass, data0, ST_REPLACE, 0);
                }
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }
            else if (pzwParam->ret==2)
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("Please press button on device to make device wakeup"));
            }
            else
            {
                if(!g_open_network)
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                }
                else
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NETWORK_STILL_OPEN));
                }
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }

            char *notify_message = (char*)json_object_to_json_string(jobj);
            PushNotificationToHandler(ZWAVE_COMMAND_PROCESS_NOTIFY, 
                                    (uint8_t*)notify_message, 
                                    strlen(notify_message)+1);
        }
        else if(!strcmp(method, ST_WRITE_S_SPEC_R))
        {
            char *CommandClass = commandInQ->response.command_class;
            char *data0 = commandInQ->response.data0;
            char *data1 = commandInQ->response.data1;
            char *data2 = commandInQ->response.data2;

            json_object *commandinfo = json_object_new_object();
            JSON_ADD_STRING_SAFE(commandinfo, ST_CLASS, CommandClass);
            JSON_ADD_STRING_SAFE(commandinfo, ST_COMMAND, command);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA0, data0);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA1, data1);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA2, data2);
            json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

            if (pzwParam->ret==0)
            {
                if(!strcmp(CommandClass,ST_BARRIER_OPERATOR))
                {
                    if(!data0 || !data1)
                    {
                        goto done;
                    }

                    if(!strcmp(command, ST_SIGNAL_SET))
                    {
                        char *signalName = data0;
                        char *signalState = data1;
                        set_register_database(_VR_CB_(zwave), nodeid, signalName, signalState, ST_REPLACE, 0);
                    }
                }
                else if(!strcmp(CommandClass,ST_USER_CODE))
                {
                    // data0: codeId, data1: remove/add, data2: {"pass":"","name":"name"}
                    if(!data0 || !data1)
                    {
                        goto done;
                    }

                    char *userIdStr = data0;
                    int userCodeCmd = (uint8_t)strtol(data1, NULL, 0);
                    int userId = (uint8_t)strtol(userIdStr, NULL, 0);
                    int sendUserId = (uint8_t)pzwParam->cmd_set.cmd[2];

                    if(userCodeCmd == REMOVE_USER_CODE_CMD)
                    {
                        if(userId == 0)
                        {
                            remove_all_user_code(nodeid);
                        }
                    }
                    else
                    {
                        if(!data2)
                        {
                            goto done;
                        }

                        json_object *userCodeObj = VR_(create_json_object)(data2);
                        if(!userCodeObj)
                        {
                            goto done;
                        }

                        json_object *nameObj = NULL;
                        if(!json_object_object_get_ex(userCodeObj, ST_NAME, &nameObj))
                        {
                            json_object_put(userCodeObj);
                            goto done;
                        }

                        if(userId != sendUserId)
                        {
                            userId = sendUserId;
                            char validUserId[SIZE_32B];
                            snprintf(validUserId, sizeof(validUserId), "%d", userId);
                            json_object_object_add(jobj, ST_VALID_USER_ID, json_object_new_string(validUserId));
                        }

                        char *name = (char*)json_object_get_string(nameObj);
                        /*for look up name*/
                        add_user_code_cmd(ADD_USER_CODE_CMD, nodeid, userId, name);
                    }
                }
                else if(!strcmp(CommandClass,ST_CONFIGURATION))
                {
                    if(!data0 || !data1)
                    {
                        goto done;
                    }

                    char *paramName = data0;
                    char *paramValue = data1;
                    set_register_database(_VR_CB_(zwave), nodeid, paramName, paramValue, ST_REPLACE, 0);

                    if(!strcmp(paramName, ST_PIN_LENGTH))
                    {
                        /*remove all user code here*/
                        remove_all_user_code(nodeid);
                    }
                }

                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }
            else
            {
                if(!g_open_network)
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                }
                else
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NETWORK_STILL_OPEN));
                }
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }

            char *notify_message = (char*)json_object_to_json_string(jobj);
            PushNotificationToHandler(ZWAVE_COMMAND_PROCESS_NOTIFY, 
                                    (uint8_t*)notify_message, 
                                    strlen(notify_message)+1);
        }
        else if(!strcmp(method, ST_READ_S_SPEC_R))
        {
            char *CommandClass = commandInQ->response.command_class;
            char *data0 = commandInQ->response.data0;
            char *data1 = commandInQ->response.data1;
            char *data2 = commandInQ->response.data2;

            json_object *commandinfo = json_object_new_object();
            JSON_ADD_STRING_SAFE(commandinfo, ST_CLASS, CommandClass);
            JSON_ADD_STRING_SAFE(commandinfo, ST_COMMAND, command);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA0, data0);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA1, data1);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA2, data2);
            json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);
            if (pzwParam->ret==0)
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }
            else
            {
                if(!g_open_network)
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                }
                else
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NETWORK_STILL_OPEN));
                }
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }
            char *notify_message = (char*)json_object_to_json_string(jobj);
            PushNotificationToHandler(ZWAVE_COMMAND_PROCESS_NOTIFY, 
                                    (uint8_t*)notify_message, 
                                    strlen(notify_message)+1);
        }
        else if(!strcmp(method, ST_IDENTIFY_R))
        {
            JSON_ADD_STRING_SAFE(jobj, ST_VALUE, value);
            if (pzwParam->ret==0) 
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }
            else if (pzwParam->ret==2)
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("does not support identify function"));
            }
            else
            {
                if(!g_open_network)
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                }
                else
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NETWORK_STILL_OPEN));
                }
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }
            char *notify_message = (char*)json_object_to_json_string(jobj);
            PushNotificationToHandler(ZWAVE_COMMAND_PROCESS_NOTIFY, 
                                    (uint8_t*)notify_message, 
                                    strlen(notify_message)+1);
        }
        else if(!strcmp(method, ST_WRITE_SPEC_CRC_R))
        {
            char *CommandClass = commandInQ->response.command_class;
            char *data0 = commandInQ->response.compliantJson;
            char *data1 = commandInQ->response.data1;
            char *data2 = commandInQ->response.data2;
            char *data3 = commandInQ->response.data3;
            char *data4 = commandInQ->response.data4;
            char *data5 = commandInQ->response.data5;

            json_object *commandinfo = json_object_new_object();
            JSON_ADD_STRING_SAFE(commandinfo, ST_CLASS, CommandClass);
            JSON_ADD_STRING_SAFE(commandinfo, ST_COMMAND, command);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA0, data0);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA1, data1);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA2, data2);
            JSON_ADD_STRING_SAFE(commandinfo, "data3", data3);
            JSON_ADD_STRING_SAFE(commandinfo, "data4", data4);
            JSON_ADD_STRING_SAFE(commandinfo, "data5", data5);
            json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

            if (pzwParam->ret == 0)
            {
                if(!data0)
                {
                    return;
                }
                
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                if(!strcasecmp(CommandClass,ST_ASSOCIATION))
                {
                    if(!strcasecmp(command,ST_SET))
                    {
                        update_association_node_list(nodeid, data0, 0);
                    }
                    else if(!strcasecmp(command,ST_REMOVE))
                    {
                        update_association_node_list(nodeid, data0, 1);
                    }
                }
            }
            else if (pzwParam->ret == 2)
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("queued"));
            }
            else
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }
            char *notify_message = (char*)json_object_to_json_string(jobj);
            PushNotificationToHandler(ZWAVE_COMMAND_PROCESS_NOTIFY, 
                                    (uint8_t*)notify_message, 
                                    strlen(notify_message)+1);
        }
        else if(!strcmp(method, ST_NETWORK_R))
        {
            char *CommandClass = commandInQ->response.command_class;
            char *Command = commandInQ->response.command;
            char *data0 = commandInQ->response.data0;
            char *data1 = commandInQ->response.data1;
            char *data2 = commandInQ->response.data2;

            json_object *commandinfo = json_object_new_object();
            JSON_ADD_STRING_SAFE(commandinfo, ST_CLASS, CommandClass);
            JSON_ADD_STRING_SAFE(commandinfo, ST_COMMAND, command);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA0, data0);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA1, data1);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA2, data2);
            json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

            if (pzwParam->ret == 0)
            {
                if (!strcmp(CommandClass, ST_RESET))
                {
                    pthread_t reset_zwave_thread_t;
                    pthread_create(&reset_zwave_thread_t, NULL, (void *)&reset_zwave_thread, NULL);
                    pthread_detach(reset_zwave_thread_t);

                    return_controller_infor(jobj, pzwParam);
                }
                else if (!strcmp(CommandClass, ST_IS_FAILED_NODE))
                {
                    sprintf(str_value, "%d", pzwParam->param2);
                    json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));
                }
                else if (!strcmp(CommandClass, ST_SEND_MY_NIF))
                {
                    json_object_object_add(jobj, ST_RETURN, json_object_new_int(pzwParam->ret));
                }
                else if (!strcmp(CommandClass, ST_GET_SECURE_KEY))//get key and save key success
                {
                    json_object_object_add(jobj, ST_RETURN, json_object_new_int(pzwParam->ret));
                    json_object_object_add(jobj, ST_FILE_NAME, json_object_new_string((char *)pzwParam->misc_data)); 
                }
                else if (!strcmp(CommandClass, ST_CONTROLLER_INFO))
                {
                    return_controller_infor(jobj, pzwParam);
                }
                else if (!strcmp(CommandClass, ST_DUMP_NEIGHBORS))
                {
                    return_neighbor_dump(jobj, pzwParam);
                }
                else if (!strcmp(CommandClass, ST_PRIORITY_ROUTE))
                {
                    if (!strcmp(Command, ST_GET)) 
                    {
                        return_priority_route(jobj, pzwParam);
                    }
                    
                }
                else if (!strcmp(CommandClass, ST_NODE_LIST))
                {
                    return_node_list(jobj, pzwParam);
                }
                else if (!strcmp(CommandClass, ST_REMOVE_FAILED_NODE))
                {
                    char nodeID[3];
                    sprintf(nodeID, "%02X", pzwParam->node.node_id);
                    json_object_object_add(jobj, ST_REMOVED_NODE_ID, json_object_new_string(nodeID));
                }
                else if (!strcmp(CommandClass, ST_GET_FIRMWARE_PROGRAMMER_VERSION))
                {
                    json_object_object_add(jobj, ST_FIRMWARE_VERSION, json_object_new_string((char *)pzwParam->misc_data));                    
                }
                else if (!strcmp(CommandClass, ST_SET_SUC_MODE))
                {
                    json_object_object_add(jobj, ST_CONTROLLER_CAPABILITY, json_object_new_int(pzwParam->param3));
                }
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }
            else if (pzwParam->ret == -1)
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }
            else
            {
                if (!strcmp(CommandClass, ST_REMOVE_FAILED_NODE))
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_NOT_REMOVED));
                }
                else if (!strcmp(CommandClass, ST_REPLACE_FAILED_NODE))
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_NOT_STARTED));
                }
                else if (!strcmp(CommandClass, ST_SET_SUC_MODE))
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_NOT_STARTED));
                }
                else if (!strcmp(CommandClass, ST_NETWORK_TEST))
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_NOT_STARTED));
                }
                else if (!strcmp(CommandClass, ST_PRIORITY_ROUTE))
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_NO_VALID_ROUTE));
                }
            }

            char *notify_message = (char*)json_object_to_json_string(jobj);
            PushNotificationToHandler(ZWAVE_COMMAND_PROCESS_NOTIFY, 
                                    (uint8_t*)notify_message, 
                                    strlen(notify_message)+1);
        }
        else if(!strcmp(method,ST_SCENE_ACTIVATION_R))
        {
            // char *groupId = commandInQ->response.data0;
            char *buttonId = commandInQ->response.data3;

            JSON_ADD_STRING_SAFE(jobj, ST_BUTTON_ID, buttonId);

            if (pzwParam->ret == 0)
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }
            else
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            }

            char *notify_message = (char*)json_object_to_json_string(jobj);
            PushNotificationToHandler(ZWAVE_COMMAND_PROCESS_NOTIFY, 
                                    (uint8_t*)notify_message, 
                                    strlen(notify_message)+1);

            update_sence_activate_database(_VR_(zwave), nodeid, buttonId, (unsigned)time(NULL));
        }

done:
        if(re_insert_to_queue)/*if it re-insert, just free queue only*/
        {
            SAFE_FREE(commandInQ->pzwParam);
            SAFE_FREE(commandInQ);
        }
        else
        {
            free_queue_element(commandInQ);
        }
        json_object_put(jobj);

        usleep(NORMAL_DEVICE_DELAY);
    }
}

void remove_dev_in_database_and_group(char *ID, zwave_dev_info_t *zwave_dev)
{
    if(!ID)
    {
        return;
    }
    /*must call before remove FEATURES*/
    remove_and_update_device_in_group(_VR_CB_(zwave), g_shm, ID, ST_ZWAVE, NULL);

    int association=0, meter=0, sensorMultilevel=0, userCode=0, thermostatSetpoint=0;
    json_object *json_capId = NULL;

    if(zwave_dev)
    {
        json_capId = zwave_dev->json_capId;
        get_feature_device_support(json_capId, ST_ZWAVE, &association,
                      &meter, &sensorMultilevel, &userCode, &thermostatSetpoint);
    }
    else
    {
        get_capId_and_scheme_dev((char*)ID, &json_capId);
        get_feature_device_support(json_capId, ST_ZWAVE, &association,
                      &meter, &sensorMultilevel, &userCode, &thermostatSetpoint);
        json_object_put(json_capId);
    }

    if(association)
    {
        database_actions(_VR_(zwave), "DELETE from ASSOCIATION_DATA where deviceId='%s'", ID);
        database_actions(_VR_(zwave), "DELETE from MORE_PROPERTY where deviceId='%s' AND deviceType='%s'", 
                                ID, ST_ASSOCIATION);
    }

    if(meter)
    {
        database_actions(_VR_(zwave), "DELETE from METER_DATA where deviceId='%s'", ID);
        database_actions(_VR_(zwave), "DELETE from MORE_PROPERTY where deviceId='%s' AND deviceType='%s'", 
                                ID, ST_METER);
    }

    if(sensorMultilevel)
    {
        database_actions(_VR_(zwave), "DELETE from SENSOR_DATA where deviceId='%s'", ID);
        database_actions(_VR_(zwave), "DELETE from MORE_PROPERTY where deviceId='%s' AND deviceType='%s'", 
                                ID, ST_SENSOR_MULTILEVEL);
    }

    if(userCode)
    {
        database_actions(_VR_(zwave), "DELETE from USER_CODE where deviceId='%s'", ID);
        database_actions(_VR_(zwave), "DELETE from MORE_PROPERTY where deviceId='%s' AND deviceType='%s'", 
                                ID, ST_USER_CODE);
    }

    if(thermostatSetpoint)
    {
        database_actions(_VR_(zwave), "DELETE from THERMOSTAT_SETPOINT where deviceId='%s'", ID);
        database_actions(_VR_(zwave), "DELETE from MORE_PROPERTY where deviceId='%s' AND deviceType='%s'", 
                                ID, ST_THERMOSTAT_SETPOINT);
    }

    database_actions(_VR_(zwave), "DELETE from SCENES where deviceId='%s'", ID);
    database_actions(_VR_(zwave), "DELETE from FEATURES where deviceId='%s'", ID);
    database_actions(_VR_(zwave), "DELETE from CAPABILITY where deviceId='%s'", ID);
    database_actions(_VR_(zwave), "DELETE from ALARM where deviceId='%s'", ID);
    database_actions(_VR_(zwave), "DELETE from SUB_DEVICES where id='%s'", ID);
}

void remove_dev_in_cloud(char *ID)
{
    if(!ID)
    {
        return;
    }
    int ret = 0;

    SEARCH_DATA_INIT_VAR(resource_id);
    searching_database(_VR_CB_(zwave), &resource_id, 
                    "SELECT owner from DEVICES where deviceId='%s'", 
                    ID);
    if(resource_id.len)
    {
        ret = VR_(delete_resources)(resource_id.value, RESOURCES_TIMEOUT);
        if(!ret)
        {
            database_actions(_VR_(zwave), "DELETE from DEVICES where deviceId='%s'", ID);
            shm_update_data(g_shm, resource_id.value, NULL, NULL, SHM_DELETE);
        }
    }
    FREE_SEARCH_DATA_VAR(resource_id);
}

void remove_dev(char *ID)
{
    if(!ID)
    {
        return;
    }

    zwave_dev_info_t *zwave_dev = get_zwave_dev_from_id(ID);
    if(!zwave_dev)
    {
        zwave_dev = create_zwave_dev_from_id(ID);
    }
    removeDeviceFromPollingList((uint8_t)htoi(ID));
    SEARCH_DATA_INIT_VAR(childrendId);
    searching_database(_VR_CB_(zwave), &childrendId, 
                    "SELECT childrenId from SUB_DEVICES where id='%s'", 
                    ID);
    if(childrendId.len) //device has children
    {
        char *tok, *save_tok;
        tok = strtok_r(childrendId.value, ",", &save_tok);
        while(tok != NULL)
        {
            char childId[SIZE_32B];
            strcpy(childId, tok);
            remove_dev_in_database_and_group(childId, zwave_dev);
            remove_dev_in_cloud(childId);
            remove_zwave_dev_from_id(childId); //remove in link list
            VR_(cancel_alarm)(NULL, childId);
            remove_voice_inform_from_id(ST_ZWAVE, childId);

            tok = strtok_r(NULL, ",", &save_tok);
        }

        //remove parent
        remove_dev_in_database_and_group(ID, zwave_dev);
        remove_dev_in_cloud(ID);
        remove_zwave_dev_from_id(ID); //remove in link list
        VR_(cancel_alarm)(NULL, ID);
        remove_voice_inform_from_id(ST_ZWAVE, ID);
    }
    else
    {
        /*dont have children, maybe is parrent(without child) or children*/
        SEARCH_DATA_INIT_VAR(parentIdData);
        searching_database(_VR_CB_(zwave), &parentIdData, 
                        "SELECT parentId from SUB_DEVICES where id='%s'", 
                        ID);
        if(parentIdData.len)
        {
            if(!strcmp(parentIdData.value, PARENT_ID_NUM))// this is parrent
            {
                remove_dev_in_database_and_group(ID, zwave_dev);
                remove_dev_in_cloud(ID);
                remove_zwave_dev_from_id(ID); //remove in link list
                // printf_dev_info_list(NULL);
                VR_(cancel_alarm)(NULL, ID);
                remove_voice_inform_from_id(ST_ZWAVE, ID);
            }
            else /*we decided to remove all devices if remove a child*/
            {
                /*in case parentDev is not exist*/
                remove_dev_in_database_and_group(ID, zwave_dev);
                remove_dev_in_cloud(ID);
                remove_zwave_dev_from_id(ID); //remove in link list
                // printf_dev_info_list(NULL);
                VR_(cancel_alarm)(NULL, ID);
                remove_voice_inform_from_id(ST_ZWAVE, ID);

                remove_dev(parentIdData.value);
            }
        }
        else // delete parent
        {
            remove_dev_in_database_and_group(ID, zwave_dev);
            remove_dev_in_cloud(ID);
            remove_zwave_dev_from_id(ID); //remove in link list
            // printf_dev_info_list(NULL);
            VR_(cancel_alarm)(NULL, ID);
            remove_voice_inform_from_id(ST_ZWAVE, ID);
        }
        FREE_SEARCH_DATA_VAR(parentIdData);
    }
    FREE_SEARCH_DATA_VAR(childrendId);
}

//libzwave auto set association with group1
//0 is success, -1 is failed
int adding_process_set_association(uint64_t manu_product_id, uint8_t device_id, uint8_t scheme)
{
    int res = 0;
    SEARCH_DATA_INIT_VAR(association_data);
    searching_database("zwave_handler", support_devs_db, zwave_cb, &association_data, 
                "SELECT Association from SUPPORT_DEVS where SerialID='%012llX'", 
                manu_product_id);

    if(association_data.len)
    {
        if(VR_(set_association)(device_id, association_data.value, scheme))
        {
            timerCancel(&timerSpeakAdding);
            // VR_(inform_retry_adding)();
            res = -1;
        }
    }
    FREE_SEARCH_DATA_VAR(association_data);
    return res;
}
// set device configurations base on supported database
// format: configId,length,value,endpoint 
int adding_process_set_configuration(uint64_t manu_product_id, uint8_t device_id, uint8_t scheme)
{
    int res = 0;
    SEARCH_DATA_INIT_VAR(configuration_data);
    searching_database("zwave_handler", support_devs_db, zwave_cb, &configuration_data, 
                "SELECT Reconfiguration from SUPPORT_DEVS where SerialID='%012llX'", 
                manu_product_id);
    if(configuration_data.len)
    {
        if(VR_(set_configuration)(device_id, configuration_data.value, scheme, 0))
        {
            timerCancel(&timerSpeakAdding);
            // VR_(inform_retry_adding)();
            res = -1;
        }
    }
    FREE_SEARCH_DATA_VAR(configuration_data);
    return res;
}

/*need free cloudId after using*/
static void post_resources_to_cloud(char *Owner, char *Serial, char *SerialID, char *DeviceType,
                                    char *FriendlyName, char *ID, char *capabilityJsonStr, 
                                    char **cloudId)
{
    /*auto gen uuid*/
    char resource_id[SIZE_256B];
    uuid_make(resource_id, sizeof(resource_id));

    json_object *json_post_resources = json_object_new_object();
    json_object *resources = json_object_new_object();
    json_object *CapabilityObj = VR_(create_json_object)(capabilityJsonStr);
    JSON_ADD_STRING_SAFE(resources, ST_OWNER, Owner);
    JSON_ADD_STRING_SAFE(resources, ST_SERIAL, Serial);
    JSON_ADD_STRING_SAFE(resources, ST_SERIAL_ID, SerialID);
    JSON_ADD_STRING_SAFE(resources, ST_DEVICE_TYPE, DeviceType);
    JSON_ADD_STRING_SAFE(resources, ST_FRIENDLY_NAME, FriendlyName);
    JSON_ADD_STRING_SAFE(resources, ST_ID, ID);
    if(CapabilityObj)
    {
        json_object_object_add(resources, ST_CAPABILITY, CapabilityObj);
    }
    JSON_ADD_STRING_SAFE(resources, ST_TYPE, ST_ZWAVE);

    json_object_object_add(json_post_resources, ST_CLOUD_RESOURCE, resources);
    json_object_object_add(json_post_resources, ST_ID, json_object_new_string(resource_id));

    JSON_ADD_STRING_SAFE(json_post_resources, ST_CLOUD_LOCAL_ID, ID);
    JSON_ADD_STRING_SAFE(json_post_resources, ST_CLOUD_SERIAL_ID, SerialID);
    JSON_ADD_STRING_SAFE(json_post_resources, ST_NAME, FriendlyName);

    const char *data = json_object_to_json_string(json_post_resources);

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_DEVICE_ID, ID);
    blobmsg_add_string(&buff, ST_DEVICE_TYPE, "zwave");
    blobmsg_add_string(&buff, ST_DATA, data);
    ubus_send_event(ctx, ST_RESOURCE, buff.head);

    update_resource_id(_VR_CB_(zwave), ID, resource_id);
    shm_update_data(g_shm, resource_id, ID, "zwave", SHM_ADD);

    if(cloudId)
    {
        *cloudId = strdup(resource_id);
    }

    json_object_put(json_post_resources);
}

void store_capability(char *deviceId, char *capability, int scheme, char *endpointMem)
{
    if(!capability || !strlen(capability))
    {
        SLOGE("missing capability\n");
        return;
    }

    if(endpointMem && strlen(endpointMem))
    {
        database_actions(_VR_(zwave), "INSERT INTO CAPABILITY"
                    "(%s,%s,%s,%s,%s,%s)"
                    "VALUES('%s', '%s', '%d', '%s', '%s', '%s')",
                    ST_DEVICE_ID, ST_CAP_LIST, ST_SCHEME, ST_ENDPOINT_MEM, ST_IN_OUT, ST_TYPE,
                    deviceId, capability, scheme, endpointMem, "in", ST_ZWAVE);
    }
    else
    {
        database_actions(_VR_(zwave), "INSERT INTO CAPABILITY"
                    "(%s,%s,%s,%s,%s)"
                    "VALUES('%s', '%s', '%d', '%s', '%s')",
                    ST_DEVICE_ID, ST_CAP_LIST, ST_SCHEME, ST_IN_OUT, ST_TYPE,
                    deviceId, capability, scheme, "in", ST_ZWAVE);
    }
}

void convert_cap_to_json(json_object *capability, char *capList, int scheme)
{
    if(!capList || !strlen(capList))
    {
        SLOGE("missing cap List\n");
        return;
    }

    char schemeStr[8];
    sprintf(schemeStr, "%d", scheme);
    json_object *nonSecObj = json_object_new_object();
    json_object_object_add(nonSecObj, ST_SCHEME, json_object_new_string(schemeStr));
    json_object_object_add(nonSecObj, ST_CAP_LIST, json_object_new_string(capList));
    json_object_array_add(capability, nonSecObj);
}

static void _adding_device(json_object *jobj, char *capabilityJsonStr, char *serial,
                            char *serialId, char *deviceId, char *deviceType, 
                            char *typeSpecific, char *endpointNum, char *parentId, 
                            zwave_adding_infor zwaveAddInfo, char *endpointMem)
{
    if(!zwaveAddInfo.owner || !serial || !serialId || 
        !deviceId  || !zwaveAddInfo.isZwavePlus)
    {
        SLOGE("missing infor\n");
        return;
    }

    int res = 0;
    int zwavePlus = strtol(zwaveAddInfo.isZwavePlus, NULL, 0);
    if (zwavePlus > 0)
    {
        res = database_actions(_VR_(zwave), "INSERT INTO SUB_DEVICES"
                            "('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')"
                            "VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                            ST_OWNER, ST_SERIAL, ST_SERIAL_ID, ST_DEVICE_TYPE, ST_TYPE_BASIC, ST_TYPE_SPECIFIC, 
                            ST_DEVICE_MODE, ST_FRIENDLY_NAME, ST_ID, ST_ENDPOINT_NUM, ST_PARENT_ID, 
                            ST_ACTIVE, ST_ALEXA, ST_IS_ZWAVE_PLUS, ST_ROLE_TYPE, ST_NODE_TYPE, ST_TYPE,
                            zwaveAddInfo.owner, serial, serialId, deviceType, 
                            zwaveAddInfo.typeBasic, typeSpecific, zwaveAddInfo.deviceMode, zwaveAddInfo.deviceName,
                            deviceId, endpointNum, parentId, zwaveAddInfo.active, zwaveAddInfo.alexa, 
                            zwaveAddInfo.isZwavePlus, zwaveAddInfo.roleType, zwaveAddInfo.nodeType, ST_ZWAVE);
    }
    else
    {
        res = database_actions(_VR_(zwave), "INSERT INTO SUB_DEVICES"
                            "('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')"
                            "VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                            ST_OWNER, ST_SERIAL, ST_SERIAL_ID, ST_DEVICE_TYPE, ST_TYPE_BASIC, ST_TYPE_SPECIFIC, 
                            ST_DEVICE_MODE, ST_FRIENDLY_NAME, ST_ID, ST_ENDPOINT_NUM, ST_PARENT_ID, 
                            ST_ACTIVE, ST_ALEXA, ST_IS_ZWAVE_PLUS, ST_TYPE,
                            zwaveAddInfo.owner, serial, serialId, deviceType, 
                            zwaveAddInfo.typeBasic, typeSpecific, zwaveAddInfo.deviceMode, zwaveAddInfo.deviceName, 
                            deviceId, endpointNum, parentId, zwaveAddInfo.active, zwaveAddInfo.alexa, 
                            zwaveAddInfo.isZwavePlus, ST_ZWAVE);
    }

    if(res)
    {
        SLOGE("failed to insert database\n");
        return;
    }

    json_object *capabilityObj = VR_(create_json_object)(capabilityJsonStr);

    json_object *deviceinfo = json_object_new_object();
    json_object_object_add(deviceinfo, ST_OWNER, json_object_new_string(zwaveAddInfo.owner));
    json_object_object_add(deviceinfo, ST_SERIAL, json_object_new_string(serial));
    json_object_object_add(deviceinfo, ST_SERIAL_ID, json_object_new_string(serialId));
    json_object_object_add(deviceinfo, ST_DEVICE_TYPE, json_object_new_string(deviceType));
    json_object_object_add(deviceinfo, ST_TYPE_BASIC, json_object_new_string(zwaveAddInfo.typeBasic));
    json_object_object_add(deviceinfo, ST_TYPE_SPECIFIC, json_object_new_string(typeSpecific));
    json_object_object_add(deviceinfo, ST_DEVICE_MODE, json_object_new_string(zwaveAddInfo.deviceMode));
    json_object_object_add(deviceinfo, ST_FRIENDLY_NAME, json_object_new_string(zwaveAddInfo.deviceName));
    json_object_object_add(deviceinfo, ST_ID, json_object_new_string(deviceId));
    if(capabilityObj)
    {
        json_object_object_add(deviceinfo, ST_CAPABILITY, capabilityObj);
    }
    json_object_object_add(deviceinfo, ST_ENDPOINT_NUM, json_object_new_string(endpointNum));
    json_object_object_add(deviceinfo, ST_PARENT_ID, json_object_new_string(parentId));
    if(endpointMem && strlen(endpointMem))
    {
        json_object_object_add(deviceinfo, ST_ENDPOINT_MEM, json_object_new_string(endpointMem));
    }
    json_object_object_add(deviceinfo, ST_IS_ZWAVE_PLUS, json_object_new_string(zwaveAddInfo.isZwavePlus));
    if(zwavePlus > 0)
    {
        json_object_object_add(deviceinfo, ST_ROLE_TYPE, json_object_new_string(zwaveAddInfo.roleType));
        json_object_object_add(deviceinfo, ST_NODE_TYPE, json_object_new_string(zwaveAddInfo.nodeType));
    }
    json_object_object_add(deviceinfo, ST_TYPE, json_object_new_string(ST_ZWAVE));
    
    char *cloudId = NULL;
    post_resources_to_cloud(zwaveAddInfo.owner, serial, serialId, deviceType, 
                            zwaveAddInfo.deviceName, deviceId, capabilityJsonStr,
                            &cloudId);
    _create_zwave_dev_(serialId, deviceType, zwaveAddInfo.deviceMode, deviceId, 
                        endpointNum, parentId, NULL, zwaveAddInfo.active, cloudId, 1);

    adding_process_info *info = (adding_process_info*)malloc(sizeof(adding_process_info));
    info->deviceId = strdup(deviceId);
    info->serialId = strdup(serialId);

    pthread_t adding_actions_thread_t;
    pthread_create(&adding_actions_thread_t, NULL, (void *)&adding_actions_devices_prepare, info);
    pthread_detach(adding_actions_thread_t);
    //add device to polling list
    int zwave_polling_interval, cmdPolling;
    dev_specific_zwave_polling_interval(deviceId, &zwave_polling_interval, &cmdPolling);
    if(zwave_polling_interval)
    {
        addDeviceToPollingList((uint8_t)htoi(deviceId), htoi(endpointNum), zwave_polling_interval, cmdPolling);
    }
    JSON_ADD_STRING_SAFE(deviceinfo, ST_CLOUD_RESOURCE, cloudId);
    json_object_object_add(jobj, ST_DEVICE_INFO, deviceinfo);
    Send_ubus_notify((char*)json_object_to_json_string(jobj));
    json_object_object_del(jobj, ST_DEVICE_INFO);
    json_object_put(deviceinfo);
    SAFE_FREE(cloudId);
}

void _adding_children_devices(json_object *jobj, char *parentSerial, char *parentSerialId, char *parentId, 
                              zwave_adding_infor zwaveAddInfo, int numEndpoint, MULTI_CHANNEL_CAPABILITY channel[], 
                              int scheme, char *childrenId)
{
    if(!zwaveAddInfo.owner || !parentSerial || !parentId || 
        !parentSerialId || !zwaveAddInfo.deviceName || !childrenId)
    {
        SLOGE("missing infor\n");
        return;
    }

    int i;
    for(i=0; i<numEndpoint; i++)
    {
        char childId[8];
        char childCapability[SIZE_256B] = {0};
        char childSerial[SIZE_128B] = {0};
        char childSerialID[SIZE_128B] = {0};
        char endpointNum[SIZE_128B] = {0};
        char childType[8] = {0};
        char childTypeSpecific[8] = {0};
        char endpointMem[SIZE_128B] = {0};

        convert_capability_to_string(channel[i].endpoint_capability.aCapability,
                         channel[i].endpoint_capability.noCapability, 
                         scheme, NULL, NULL, NULL, childCapability, sizeof(childCapability));

        convert_endpoint_to_string(channel[i].ep_member, channel[i].no_memberof, endpointMem);

        json_object *childCapobj = json_object_new_array();/*need free*/
        convert_cap_to_json(childCapobj, childCapability, scheme);

        sprintf(endpointNum, "%02X", channel[i].endpoint);
        sprintf(childSerialID, "%s%s", parentSerialId, endpointNum);
        sprintf(childId, "%02X%s",  channel[i].endpoint, parentId);
        sprintf(childSerial, "%02X%s", channel[i].endpoint, parentSerial);
        sprintf(childType, "%02X", channel[i].generic_device_class);
        sprintf(childTypeSpecific, "%02X", channel[i].specific_device_class);

        if(strlen(childCapability))
        {
            store_capability(childId, childCapability, scheme, endpointMem);
        }

        SEARCH_DATA_INIT_VAR(FriendlyName);
        searching_database("zwave_handler", support_devs_db, zwave_cb, &FriendlyName,
                            "SELECT FriendlyName from SUPPORT_DEVS where SerialID='%s'",
                            childSerialID);
        if(FriendlyName.len)
        {
            size_t maxNameLength = sizeof(zwaveAddInfo.deviceName)-1;
            strncpy(zwaveAddInfo.deviceName, FriendlyName.value, maxNameLength);
            zwaveAddInfo.deviceName[maxNameLength] = '\0';
        }

        FREE_SEARCH_DATA_VAR(FriendlyName);

        _adding_device(jobj, (char*)json_object_to_json_string(childCapobj), childSerial, 
                    childSerialID, childId, childType, childTypeSpecific, endpointNum, parentId,
                    zwaveAddInfo, endpointMem);
        
        json_object_put(childCapobj);

        size_t childrenLeng = strlen(childrenId);
        if(childrenLeng == 0)
        {
            sprintf(childrenId, "%s", childId);
        }
        else
        {
            sprintf(childrenId+childrenLeng, ",%s", childId);
        }
    }
}

void zwave_adding_handler(struct ubus_context *ctx, bool is_add_dev_notification,
                         json_object *jobj, NOTIFY_TX_BUFFER_T pTxNotify, int *sending_notify)
{
    if(!jobj)
    {
        SLOGE("missing jobj\n");
        return;
    }
    
    int res = 0;
    int unexpected = 0;
    char ID[8],deviceType[8], deviceMode[8], typeSpecific[8], typeBasic[8];
    if(is_add_dev_notification)
        json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_ADD_DEV));
    else
        json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_REPLACE_DEV));
    switch (pTxNotify.AddNodeZPCNotify.bStatus)
    {
        case ADD_NODE_STATUS_LEARN_READY:
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_READY));
            *sending_notify = 0;
            break;
        case ADD_NODE_STATUS_NODE_FOUND:
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NODE_FOUND));
            break;
        case ADD_NODE_STATUS_ADDING_SLAVE:
            sprintf(ID, "%02X", pTxNotify.AddNodeZPCNotify.zpc_node.node_id);
            sprintf(deviceType, "%02X", pTxNotify.AddNodeZPCNotify.zpc_node.node_type);

            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_ADDING));
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(deviceType));

            timerStart(&timerSpeakAdding, timer_Speak_Adding_handler, NULL, 2000, TIMER_ONETIME);
            break;
        case ADD_NODE_STATUS_ADDING_CONTROLLER:
            sprintf(ID, "%02X", pTxNotify.AddNodeZPCNotify.zpc_node.node_id);
            sprintf(deviceType, "%02X", pTxNotify.AddNodeZPCNotify.zpc_node.node_type);

            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_ADDING));
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(deviceType));

            timerStart(&timerSpeakAdding, timer_Speak_Adding_handler, NULL, 2000, TIMER_ONETIME);
            break; 
        case ADD_NODE_STATUS_FAILED:
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_ADD_FAILED));
            timerCancel(&timerSpeakAdding);
            VR_(inform_retry_adding)((void *)ctx);
            break;
        case ADD_REMOVE_NODE_TIMEOUT:
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_TIMEOUT));

            timerCancel(&timerSpeakAdding);
            VR_(inform_retry_adding)((void *)ctx);
            break;
        case ADD_NODE_STATUS_INTERVIEW_DEV_FAILED:
        {
            unexpected = 1;
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_UNEXPECTED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_FAILED_TO_INTERVIEW));
            goto add_device_done;            
        }
        case ADD_NODE_STATUS_ADD_SECURE_FAILED:
        {
            unexpected = 1;
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_UNEXPECTED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_ADD_SECURE_FAILED));
        }
        add_device_done:
        case ADD_NODE_STATUS_DONE:
        {
            int supported_device = 1;
            zwave_adding_infor zwaveAddInfo;
            char *defaultName = ST_UNKNOWN;
            int multichannel_sec = NON_SEC_SCHEME, configuration_sec = NON_SEC_SCHEME, association_sec = NON_SEC_SCHEME;
            char childrenId[SIZE_1024B] = {0};
            char owner[SIZE_64B], serial[SIZE_64B], serialId[SIZE_32B];
            char nonSecureCap[SIZE_256B] = {0}, secCap[SIZE_256B]={0}, sec2Cap[SIZE_256B]={0};
            char isZwavePlus[8], roleType[8], nodeType[8], alexa[8], active[8];

            size_t maxNameLength = sizeof(zwaveAddInfo.deviceName)-1;
            strncpy(zwaveAddInfo.deviceName, defaultName, maxNameLength);
            zwaveAddInfo.deviceName[maxNameLength] = '\0';

            zwaveAddInfo.owner = &owner[0];
            zwaveAddInfo.deviceMode = &deviceMode[0];
            zwaveAddInfo.typeBasic = &typeBasic[0];
            zwaveAddInfo.isZwavePlus = &isZwavePlus[0];
            zwaveAddInfo.roleType = &roleType[0];
            zwaveAddInfo.nodeType = &nodeType[0];
            zwaveAddInfo.active = &active[0];
            zwaveAddInfo.alexa = &alexa[0];

            strcpy(active, ST_YES);
            strcpy(alexa, ST_NO);
            sprintf(ID, "%02X", pTxNotify.AddNodeZPCNotify.zpc_node.node_id);
            sprintf(deviceType, "%02X", pTxNotify.AddNodeZPCNotify.zpc_node.node_info.nodeType.generic);
            sprintf(typeBasic, "%02X", pTxNotify.AddNodeZPCNotify.zpc_node.node_info.nodeType.basic);
            sprintf(typeSpecific, "%02X", pTxNotify.AddNodeZPCNotify.zpc_node.node_info.nodeType.specific);
            sprintf(deviceMode, "%d", pTxNotify.AddNodeZPCNotify.zpc_node.node_info.mode);
            sprintf(isZwavePlus, "%d", pTxNotify.AddNodeZPCNotify.zpc_node.is_zp_node);

            if (pTxNotify.AddNodeZPCNotify.zpc_node.is_zp_node>0)
            {
                sprintf(roleType, "%d", pTxNotify.AddNodeZPCNotify.zpc_node.zp_info.role_type);
                sprintf(nodeType, "%d", pTxNotify.AddNodeZPCNotify.zpc_node.zp_info.node_type);
            }

            uint16_t manufacturerID_temp  = (pTxNotify.AddNodeZPCNotify.zpc_node.node_manufacture.manufacturerID[0] << 8)
                                        | pTxNotify.AddNodeZPCNotify.zpc_node.node_manufacture.manufacturerID[1];
            uint32_t type_product_temp = (uint32_t)( (uint32_t)(pTxNotify.AddNodeZPCNotify.zpc_node.node_manufacture.productTypeID[0] << 24 )
                                      | (uint32_t)(pTxNotify.AddNodeZPCNotify.zpc_node.node_manufacture.productTypeID[1] << 16 )
                                      | (uint32_t)(pTxNotify.AddNodeZPCNotify.zpc_node.node_manufacture.productID[0] << 8 )
                                      | (uint32_t)(pTxNotify.AddNodeZPCNotify.zpc_node.node_manufacture.productID[1] << 0 )
                                      );
            uint64_t manu_product_id = (uint64_t)((((uint64_t)(manufacturerID_temp) << 32)) | type_product_temp);

            sprintf(owner, "%08X%02X", zw_OwnerID.HomeID, zw_OwnerID.MyNodeID);
            sprintf(serial, "%02X%02X%04X%08X", pTxNotify.AddNodeZPCNotify.zpc_node.node_id, 
                pTxNotify.AddNodeZPCNotify.zpc_node.node_type, manufacturerID_temp, type_product_temp);
            sprintf(serialId, "%012llX", manu_product_id);

            get_capability(pTxNotify, nonSecureCap, secCap, sec2Cap, &multichannel_sec, &configuration_sec, &association_sec, sizeof(nonSecureCap));

            json_object *capabilityObj = json_object_new_array();/*need free*/
            if(strlen(nonSecureCap))
            {
                convert_cap_to_json(capabilityObj, nonSecureCap, NON_SEC_SCHEME);
            }
            if(strlen(secCap))
            {
                convert_cap_to_json(capabilityObj, secCap, SEC_0_SCHEME);
            }
            if(strlen(sec2Cap))
            {
                convert_cap_to_json(capabilityObj, sec2Cap, pTxNotify.AddNodeZPCNotify.zpc_node.node_secure_scheme);
            }
            json_object_object_add(jobj, ST_CONTROLLER_CAPABILITY, json_object_new_int(pTxNotify.AddNodeZPCNotify.my_capability));

            SEARCH_DATA_INIT_VAR(deviceExist);
            searching_database(_VR_CB_(zwave), &deviceExist, 
                                "SELECT FriendlyName from SUB_DEVICES where id='%s'", 
                                ID);
            if(deviceExist.len)
            {
                /*in this case, we not guarantee status sleepy device after adding will be correct.*/
                remove_dev(ID);
            }
            FREE_SEARCH_DATA_VAR(deviceExist);

            SEARCH_DATA_INIT_VAR(FriendlyName);
            searching_database("zwave_handler", support_devs_db, zwave_cb, &FriendlyName, 
                                "SELECT FriendlyName from SUPPORT_DEVS where SerialID='%012llX'", 
                                manu_product_id);
            
            if(!FriendlyName.len) /*not supported device*/
            {
                const char *name_unsupported = ST_UNKNOWN;
                name_unsupported = get_name_unsupported(pTxNotify.AddNodeZPCNotify.zpc_node.node_info.nodeType.generic, 
                                                        pTxNotify.AddNodeZPCNotify.zpc_node.node_info.nodeType.specific);
                supported_device = 0;
                if(!unexpected)
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NOT_SUPPORT));
                }
                strncpy(zwaveAddInfo.deviceName, name_unsupported, maxNameLength);
                zwaveAddInfo.deviceName[maxNameLength] = '\0';
            }
            else
            {
                if(!unexpected)
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NEW_DEVICE));
                }

                strncpy(zwaveAddInfo.deviceName, FriendlyName.value, maxNameLength);
                zwaveAddInfo.deviceName[maxNameLength] = '\0';
                /*set association*/
                res = adding_process_set_association(manu_product_id, pTxNotify.AddNodeZPCNotify.zpc_node.node_id, association_sec);
                if(res)
                {
                    unexpected=1;
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_UNEXPECTED));
                    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
                    json_object_object_add(jobj, ST_REASON, json_object_new_string("failed to set association"));
                }
                /*set configuration*/
                res = adding_process_set_configuration(manu_product_id, pTxNotify.AddNodeZPCNotify.zpc_node.node_id, configuration_sec);
                if(res)
                {
                    unexpected=1;
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_UNEXPECTED));
                    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
                    json_object_object_add(jobj, ST_REASON, json_object_new_string("failed to set configuration"));
                }
            }
            FREE_SEARCH_DATA_VAR(FriendlyName);

            if(strlen(nonSecureCap))
            {
                store_capability(ID, nonSecureCap, NON_SEC_SCHEME, NULL);
            }
            
            if(strlen(secCap))
            {
                store_capability(ID, secCap, SEC_0_SCHEME, NULL);
            }

            if(strlen(sec2Cap))
            {
                store_capability(ID, sec2Cap, pTxNotify.AddNodeZPCNotify.zpc_node.node_secure_scheme, NULL);
            }

            if(supported_device)
            {
                SEARCH_DATA_INIT_VAR(Alexa);
                searching_database("zwave_handler", support_devs_db, zwave_cb, &Alexa, 
                                "SELECT alexa from SUPPORT_DEVS where serialId='%012llX'", 
                                manu_product_id);
                if(Alexa.len)
                {
                    strncpy(alexa, Alexa.value, sizeof(alexa)-1);
                }
                FREE_SEARCH_DATA_VAR(Alexa);
            }

            _adding_device(jobj, (char*)json_object_to_json_string(capabilityObj), serial, 
                                serialId, ID, deviceType, typeSpecific, PARENT_ENDPOINT_NUM, 
                                PARENT_ID_NUM, zwaveAddInfo, NULL);

            _adding_children_devices(jobj, serial, serialId, ID, zwaveAddInfo, 
                                    pTxNotify.AddNodeZPCNotify.zpc_node.no_endpoint, 
                                    pTxNotify.AddNodeZPCNotify.zpc_node.multi_channel_capability, 
                                    multichannel_sec, childrenId);

            if(pTxNotify.AddNodeZPCNotify.zpc_node.no_endpoint > 0)
            {
                _adding_children_devices(jobj, serial, serialId, ID, zwaveAddInfo, 
                                        pTxNotify.AddNodeZPCNotify.zpc_node.no_agg_endpoint,
                                        pTxNotify.AddNodeZPCNotify.zpc_node.multi_channel_agg_capability, 
                                        multichannel_sec, childrenId);
            }

            if(strlen(childrenId))
            {
                database_actions(_VR_(zwave), "UPDATE SUB_DEVICES set childrenId='%s' where id='%s'",
                                    childrenId, ID);
                update_zwave_dev_from_id(ID, ST_CHILDREN_ID, childrenId);
            }

            timerCancel(&timerSpeakAdding);

            if(supported_device && !unexpected)
            {
                SEARCH_DATA_INIT_VAR(sound_file);
                searching_database("zwave_handler", support_devs_db, zwave_cb, &sound_file, 
                                "SELECT Sound from SUPPORT_DEVS where SerialID='%012llX'", 
                                manu_product_id);

                if(sound_file.len)
                {
                    VR_(inform_device_adding_success)((void *)ctx, sound_file.value);
                }
                else
                {
                    VR_(inform_device_adding_success)((void *)ctx, ST_DEFAULT_SOUND_NAME);
                }
                FREE_SEARCH_DATA_VAR(sound_file);
            }
            else if(!supported_device && !unexpected)
            {
                VR_(inform_device_not_support)((void *)ctx);
            }

            json_object_object_del(jobj, ST_CONTROLLER_CAPABILITY);
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_ADD_DONE));//inform finish adding

            json_object_put(capabilityObj);
            break;
        }
    }
}

static void update_network_adding_device(json_object *jobj, NOTIFY_TX_BUFFER_T pTxNotify)
{
    if(!jobj)
    {
        return;
    }

    int i;
    json_object *jarray_update_network = json_object_new_array();
    for (i = 0; i < pTxNotify.UpdateNetworkNotify.no_node; i++)
    {
        const char *deviceName = ST_UNKNOWN;
        char owner[SIZE_64B], serial[SIZE_64B], serialId[SIZE_32B];
        char isZwavePlus[8], alexa[8], active[8];
        char deviceId[8], deviceType[8], deviceMode[8], typeSpecific[8], typeBasic[8];

        strcpy(active, ST_YES);
        strcpy(alexa, ST_NO);
        sprintf(deviceId, "%02X", pTxNotify.UpdateNetworkNotify.TranferNetworkNotify[i].node_id);
        sprintf(deviceType, "%02X", pTxNotify.UpdateNetworkNotify.TranferNetworkNotify[i].generic_type);
        sprintf(typeBasic, "%02X", pTxNotify.UpdateNetworkNotify.TranferNetworkNotify[i].basic_type);
        sprintf(typeSpecific, "%02X", pTxNotify.UpdateNetworkNotify.TranferNetworkNotify[i].specific_type);
        sprintf(deviceMode, "%d", pTxNotify.UpdateNetworkNotify.TranferNetworkNotify[i].node_mode);
        sprintf(isZwavePlus, "%d", 0);

        sprintf(owner, "%08X%02X", zw_OwnerID.HomeID, zw_OwnerID.MyNodeID);
        sprintf(serial, "FFFFFFFFFFFFFF%s", deviceId);
        sprintf(serialId, "FFFFFFFFFF%s", deviceId);

        deviceName = get_name_unsupported(pTxNotify.UpdateNetworkNotify.TranferNetworkNotify[i].generic_type, 
                                                pTxNotify.UpdateNetworkNotify.TranferNetworkNotify[i].specific_type);

        json_object *capabilityObj = json_object_new_array();/*need free*/
        database_actions(_VR_(zwave), "INSERT INTO SUB_DEVICES"
                            "('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')"
                            "VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                            ST_OWNER, ST_SERIAL, ST_SERIAL_ID, ST_DEVICE_TYPE, ST_TYPE_BASIC, ST_TYPE_SPECIFIC, 
                            ST_DEVICE_MODE, ST_FRIENDLY_NAME, ST_ID, ST_ENDPOINT_NUM, ST_PARENT_ID, 
                            ST_ACTIVE, ST_ALEXA, ST_IS_ZWAVE_PLUS, ST_TYPE,
                            owner, serial, serialId, deviceType, 
                            typeBasic, typeSpecific, deviceMode, deviceName, 
                            deviceId, PARENT_ENDPOINT_NUM, PARENT_ID_NUM, active, alexa, 
                            isZwavePlus, ST_ZWAVE);
        

        json_object *deviceinfo = json_object_new_object();
        json_object_object_add(deviceinfo, ST_OWNER, json_object_new_string(owner));
        // json_object_object_add(deviceinfo, ST_SERIAL, json_object_new_string(serial));
        json_object_object_add(deviceinfo, ST_SERIAL_ID, json_object_new_string(serialId));
        json_object_object_add(deviceinfo, ST_DEVICE_TYPE, json_object_new_string(deviceType));
        json_object_object_add(deviceinfo, ST_TYPE_BASIC, json_object_new_string(typeBasic));
        json_object_object_add(deviceinfo, ST_TYPE_SPECIFIC, json_object_new_string(typeSpecific));
        json_object_object_add(deviceinfo, ST_DEVICE_MODE, json_object_new_string(deviceMode));
        json_object_object_add(deviceinfo, ST_FRIENDLY_NAME, json_object_new_string(deviceName));
        json_object_object_add(deviceinfo, ST_ID, json_object_new_string(deviceId));
        json_object_object_add(deviceinfo, ST_CAPABILITY, capabilityObj);
        json_object_object_add(deviceinfo, ST_ENDPOINT_NUM, json_object_new_string(PARENT_ENDPOINT_NUM));
        json_object_object_add(deviceinfo, ST_PARENT_ID, json_object_new_string(PARENT_ID_NUM));
        json_object_object_add(deviceinfo, ST_IS_ZWAVE_PLUS, json_object_new_string(isZwavePlus));
        json_object_object_add(deviceinfo, ST_TYPE, json_object_new_string(ST_ZWAVE));
        
        char *cloudId = NULL;
        post_resources_to_cloud(owner, serial, serialId, deviceType, 
                                (char*)deviceName, deviceId, (char*)"[]", &cloudId);
        _create_zwave_dev_(serialId, deviceType, deviceMode, deviceId, 
                            PARENT_ENDPOINT_NUM, PARENT_ID_NUM, NULL, active, cloudId, 1);

        JSON_ADD_STRING_SAFE(deviceinfo, ST_CLOUD_RESOURCE, cloudId);
        json_object_array_add(jarray_update_network, deviceinfo);
        SAFE_FREE(cloudId);
        // json_object_put(capabilityObj);
    }

    json_object_object_add(jobj, ST_NETWORK_INFO, jarray_update_network);
}

void remove_device_thread(void *data)
{
    if(!data)
    {
        return;
    }

    char *nodeId = (char *)data;
    remove_dev(nodeId);
    SAFE_FREE(nodeId);
}

void zwave_removing_handler(struct ubus_context *ctx, json_object *jobj, NOTIFY_TX_BUFFER_T pTxNotify,
                            int *sending_notify, uint8_t status)
{
    if(!jobj)
    {
        SLOGE("missing jobj\n");
        return;
    }

    char ID[8],Type[8];
    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_REMOVE_DEV));
    switch (pTxNotify.RemoveNodeZPCNotify.bStatus)
    {
        case REMOVE_NODE_STATUS_LEARN_READY:
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_READY));
            break;
        case REMOVE_NODE_STATUS_REMOVING_SLAVE:
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_REMOVING));
            sprintf(ID, "%02X", pTxNotify.RemoveNodeZPCNotify.zpc_node.node_id);
            sprintf(Type, "%02X", pTxNotify.RemoveNodeZPCNotify.zpc_node.node_type);
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(Type));

            break;
        case REMOVE_NODE_STATUS_REMOVING_CONTROLLER:
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_REMOVING));
            sprintf(ID, "%02X", pTxNotify.RemoveNodeZPCNotify.zpc_node.node_id);
            sprintf(Type, "%02X", pTxNotify.RemoveNodeZPCNotify.zpc_node.node_type);
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(Type));

            break;
        case REMOVE_NODE_STATUS_DONE:
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_REMOVE_DONE));
            sprintf(ID, "%02X", pTxNotify.RemoveNodeZPCNotify.zpc_node.node_id);
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));

            if (status == REMOVE_NODE_ZPC_NOTIFY_AUTO)
            {
                uloop_timeout_set(&cancel_adding_device, ADDING_DEVICE_TIMEOUT * 1000);//5 mins
                VR_(execute_system)("ubus call zigbee %s &", ST_RESET_STOP_ADD_TIMER);
            }

            VR_(inform_found_device)((void *)ctx);

            if(pTxNotify.RemoveNodeZPCNotify.zpc_node.node_id)
            {
                char *nodeId = (char *)malloc(SIZE_32B);
                sprintf(nodeId, "%02X", pTxNotify.RemoveNodeZPCNotify.zpc_node.node_id);
                pthread_t remove_device_thread_t;
                pthread_create(&remove_device_thread_t, NULL, (void *)&remove_device_thread, (void *)nodeId);
                pthread_detach(remove_device_thread_t);
            }

            break;
        case REMOVE_NODE_STATUS_FAILED:
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_REMOVE_FAILED));
            // json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            timerCancel(&timerSpeakAdding);
            // VR_(inform_retry_adding)();
            break;
        case ADD_REMOVE_NODE_TIMEOUT:
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_TIMEOUT));
            // json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            timerCancel(&timerSpeakAdding);
            // VR_(inform_retry_adding)();
            break;
    }
}

void try_to_parser_alarm_info(json_object *jobj, cmd_class_notify_t *data)
{
    char alarmTypeFinal[SIZE_1024B];
    char alarmMeaning[SIZE_1024B];
    alarmTypeFinal[SIZE_1024B-1] = '\0';
    alarmMeaning[SIZE_1024B-1] = '\0';
    strcpy(alarmTypeFinal, ST_UNKNOWN);
    strcpy(alarmMeaning, ST_UNKNOWN);

    if(!data || !jobj)
    {
        SLOGI("cmd_class_notify_t data not found\n");
        goto report_done;
    }

    int isParam = 0;
    uint8_t zwave_alarm_type=0;
    uint8_t zwave_alarm_event=0;
    uint8_t no_event_params=0;
    uint8_t *event_param = NULL;
    const char *notificationName = NULL;
    const char *paramMeaning = NULL;
    const char *notificationTypeName = NULL;

    SLOGI("data->version = %d\n", data->version);
    if (data->version==2)
    {
        zwave_alarm_type = data->alarm_report_v2.zwave_alarm_type;
        zwave_alarm_event = data->alarm_report_v2.zwave_alarm_event;
        no_event_params = data->alarm_report_v2.no_event_params;
        event_param = &data->alarm_report_v2.event_param[0];
    }
    else if (data->version==3)
    {
        zwave_alarm_type = data->notification_report.zwave_alarm_type;
        zwave_alarm_event = data->notification_report.zwave_alarm_event;
        no_event_params = data->notification_report.no_event_params;
        event_param = &data->notification_report.event_param[0];
    }
    SLOGI("zwave_alarm_type = %d\n", notificationTypeName);
    SLOGI("zwave_alarm_event = %d\n", zwave_alarm_event);
    SLOGI("no_event_params = %d\n", no_event_params);
    notificationTypeName = get_notification_type_name(zwave_alarm_type, 
                                                        zwave_alarm_event, 
                                                        &notificationName, &isParam);
    SLOGI("notificationTypeName = %s\n", notificationTypeName);
    SLOGI("notificationName = %s\n", notificationName);
    SLOGI("isParam = %d\n", isParam);

    if(!notificationName)
    {
        goto report_done;
    }
        
    strncpy(alarmTypeFinal, notificationName, SIZE_1024B-1);

    if(!no_event_params || isParam<1 || !event_param)
    {
        strncpy(alarmMeaning, get_alarm_meaning(alarmTypeFinal), sizeof(alarmMeaning)-1);
        goto report_done;
    }

    /*get param meaning*/
    paramMeaning = get_param_info(zwave_alarm_type, zwave_alarm_event, 
                                    event_param[0]);

    SLOGI("paramMeaning = %s\n", paramMeaning);
    if(paramMeaning)
    {
        strncat(alarmTypeFinal, paramMeaning, sizeof(alarmTypeFinal)-strlen(alarmTypeFinal));
        strncpy(alarmMeaning, get_alarm_meaning(alarmTypeFinal), sizeof(alarmMeaning)-1);
    }
    else
    {
        SLOGI("alarmTypeFinal = %s\n", alarmTypeFinal);
        strncpy(alarmMeaning, get_alarm_meaning(alarmTypeFinal), sizeof(alarmMeaning)-1);
        SLOGI("alarmMeaning = %s\n", alarmMeaning);
        if(ALARM_GENERIC_ACCESS_CONTROL == zwave_alarm_type)
        {
            if(ALARM_AC_BARRIER_MOTOR_EXCEEDED == zwave_alarm_event)
            {
                if(event_param[0] < 255)
                {
                    if (event_param[0] < 128)
                    {
                        snprintf(alarmMeaning+strlen(alarmMeaning), sizeof(alarmMeaning)-strlen(alarmMeaning), " %d seconds", event_param[0]);
                    }
                    else
                    {
                        snprintf(alarmMeaning+strlen(alarmMeaning), sizeof(alarmMeaning)-strlen(alarmMeaning)," %d minutes", event_param[0]-127);
                    }
                }
            }
            else if((ALARM_AC_BARRIER_SENSOR_NOT_DETECTED == zwave_alarm_event) ||
                    (ALARM_AC_BARRIER_SENSOR_LOW_BATTERY_WARNING == zwave_alarm_event))
            {
                if(event_param[0] != 0)
                {
                    snprintf(alarmMeaning+strlen(alarmMeaning), sizeof(alarmMeaning)-strlen(alarmMeaning), " with sensorId: %d", event_param[0]);
                }
            }
        }
        else if(ALARM_GENERIC_CLOCK == zwave_alarm_type)
        {
            if(ALARM_CLOCK_TIME_REMAINING == zwave_alarm_event)
            {
                snprintf(alarmMeaning+strlen(alarmMeaning), sizeof(alarmMeaning)-strlen(alarmMeaning), " %d hour %d minutes %d seconds", 
                        event_param[0], event_param[1], event_param[2]);
            }
        }
        else /*location*/
        {
            SLOGI("location\n");
            strncat(alarmMeaning, " at location ", sizeof(alarmMeaning)-strlen(alarmMeaning));
            snprintf(alarmMeaning+strlen(alarmMeaning), no_event_params+1, "%s", event_param);
        }
    }

report_done:
    json_object_object_add(jobj, ST_ALARM_TYPE_FINAL, json_object_new_string(alarmTypeFinal));
    json_object_object_add(jobj, ST_ALARM_MEANING, json_object_new_string(alarmMeaning));
}

static void alarm_voice_inform(struct ubus_context *ctx, char *ID, char *alarmTypeFinal)
{
    if(!ID || !alarmTypeFinal)
    {
        return;
    }

    if(g_sound_alarm_enable)
    {
        /*checking device inform status via venus speaker or not*/
        int alarm = 0;
        char *defaultSound = ST_DEFAULT_SOUND_NAME;

        zwave_dev_info_t *dev = get_zwave_dev_from_id(ID);
        if(!dev || dev->speakerAlarm == ALARM_VIA_SPEAKER_NO_DATA)
        {
            if(dev)
            {
                defaultSound = dev->defaultSound;
            }

            SEARCH_DATA_INIT_VAR(sound_alarm_enable);
            searching_database(_VR_CB_(zwave), &sound_alarm_enable, 
                                "SELECT register from FEATURES where deviceId='%s' and featureId='%s'", 
                                ID, ST_SOUND_ALARM_ENABLE);
            if(!sound_alarm_enable.len)
            {
                set_register_database(_VR_CB_(zwave), ID, ST_SOUND_ALARM_ENABLE, ST_ON_VALUE, ST_REPLACE, 0);
                if(dev)
                {
                    dev->speakerAlarm = ALARM_VIA_SPEAKER_ENABLE;
                }

                alarm = 1;
            }
            else if(!strcmp(sound_alarm_enable.value, ST_ON_VALUE))
            {
                alarm = 1;
                if(dev)
                {
                    dev->speakerAlarm = ALARM_VIA_SPEAKER_ENABLE;
                }
            }
            else
            {
                if(dev)
                {
                    dev->speakerAlarm = ALARM_VIA_SPEAKER_DISABLE;
                }
            }

            FREE_SEARCH_DATA_VAR(sound_alarm_enable);
        }
        else if(dev->speakerAlarm == ALARM_VIA_SPEAKER_ENABLE)
        {
            unsigned int currentTime = (unsigned)time(NULL);
            if(dev->temporary_disable_alarm_time < currentTime)
            {
                alarm = 1;
            }
            defaultSound = dev->defaultSound;
        }

        if (!strcmp(alarmTypeFinal, ST_TEMPERATURE_HIGH) || !strcmp(alarmTypeFinal, ST_TEMPERATURE_LOW))
        {
            int mode = 0;
            if (!strcmp(alarmTypeFinal, ST_TEMPERATURE_HIGH))
            {
                mode = 1;
            }
            json_object *tempReportObj = json_object_new_object();
            char valThreshold[SIZE_32B] ={0};
            get_temperature_threshold(ID, mode, ST_CELSIUS, valThreshold, sizeof(valThreshold)-1);
            if(strlen(valThreshold))
            {
                json_object_object_add(tempReportObj, ST_TEMPERATURE_THRESH_HOLD, json_object_new_string(valThreshold));
                json_object_object_add(tempReportObj, ST_UNIT, json_object_new_string(ST_CELSIUS));
                VR_(alarm)((void *)ctx, alarm, (char *)alarmTypeFinal, ID, ST_ZWAVE, defaultSound, tempReportObj);
            }
            else
            {
                VR_(alarm)((void *)ctx, alarm, (char *)alarmTypeFinal, ID, ST_ZWAVE, defaultSound, NULL);
            }
            json_object_put(tempReportObj);
        }
        else
        {
             VR_(alarm)((void *)ctx, alarm, (char *)alarmTypeFinal, ID, ST_ZWAVE, defaultSound, NULL);
        }
    }
    else
    {
        SLOGI("disable all alarms\n");
    }

    return;
}

static void alarm_update_database(char *ID, const char *alarmTypeFinal)
{
    int i,j;
    group_alarm_type_t alarm_info;
    alarm_info = get_group_alarm(alarmTypeFinal);
    if (!alarm_info.groupAlarm)
    {
        return;
    }
    /* write database*/
    set_alarm_value_database(_VR_CB_(zwave), ID, alarm_info.groupAlarm, alarm_info.alarmTypeFinal);

    /* Check active*/
    for (i = 0; i < alarm_info.alarm_array_active_len; i++)
    {
        char *alarmTypeFinal_DB = get_alarm_value_database(_VR_CB_(zwave), ID, alarm_info.alarmActive[i].groupAlarm);
        if (!alarmTypeFinal_DB)
        {
            continue;
        }

        for (j = 0; j < alarm_info.alarmActive[i].number_alarm; j++)
        {
            if (!strcmp(alarm_info.alarmActive[i].alarmTypeFinal[j], alarmTypeFinal_DB))
            {
                remove_alarm_database(_VR_(zwave), ID, alarm_info.alarmActive[i].groupAlarm);
            }
        }
        free(alarmTypeFinal_DB);
    }
}

static void alarm_update_database_and_voice_inform(struct ubus_context *ctx, json_object *jobj, char *ID)
{
    if(!jobj || !ID)
    {
        return;
    }

    CHECK_JSON_OBJECT_EXIST(alarmTypeFinalObj, jobj, ST_ALARM_TYPE_FINAL, done);
    const char *alarmTypeFinal = json_object_get_string(alarmTypeFinalObj);

    if(!strcmp(alarmTypeFinal, ST_UNKNOWN))
    {
        return;
    }

    int res = get_door_status(alarmTypeFinal);
    if(res == DOOR_RF_LOCK_VALUE)
    {
        update_zwave_status(_VR_CB_(zwave), ID, ST_ON_OFF, ST_CLOSE_VALUE, ST_REPLACE, 0);
    }
    else if(res == DOOR_RF_UNLOCK_VALUE)
    {
        update_zwave_status(_VR_CB_(zwave), ID, ST_ON_OFF, ST_OPEN_VALUE, ST_REPLACE, 0);
    }
    else if(strcmp(alarmTypeFinal, ST_TAMPER)) //dont record tamper
    {
        if(!strcmp(alarmTypeFinal, ST_DELETE_ALL_USERS))
        {
            remove_all_user_code(ID);
        }
        // else if(!strcmp(alarmTypeFinal, ST_DUPLICATE_USER_CODE))
        // {
        //     char *name = remove_user_code_cmd(ID, userId);
        //     if(name)
        //     {
        //         char userIdStr[SIZE_32B];
        //         snprintf(userIdStr, sizeof(userIdStr), "%d", USER_CODE_ID_DUPPLICATE);
        //         add_user_code(ID, userIdStr, name);
        //         free(name);
        //     }
        // }
        else if(!strcmp(alarmTypeFinal, ST_ADD_NEW_USER))
        {
            json_object *userIdObj = NULL;
            if(json_object_object_get_ex(jobj, ST_USER_ID, &userIdObj))
            {
                int userId = json_object_get_int(userIdObj);
                char *name = remove_user_code_cmd(ID, userId);
                if(!name)
                {
                    name = strdup(ST_MANUAL);
                }
                
                char userIdStr[SIZE_32B];
                snprintf(userIdStr, sizeof(userIdStr), "%d", userId);
                add_user_code(ID, userIdStr, name);
                free(name);
            }
        }
        else if(!strcmp(alarmTypeFinal, ST_REMOVE_SINGLE_USER))
        {
            json_object *userIdObj = NULL;
            if(json_object_object_get_ex(jobj, ST_USER_ID, &userIdObj))
            {
                int userId = json_object_get_int(userIdObj);
                remove_user_code(ID, userId);
            }
        }

        // set_register_database(_VR_CB_(zwave), ID, ST_ALARM, alarmTypeFinal, ST_REPLACE, 0);
    }

    alarm_update_database(ID, alarmTypeFinal);
    alarm_voice_inform(ctx, ID, (char*)alarmTypeFinal);
done:
    return;
}

void alarm_report_final(struct ubus_context *ctx, json_object *jobj, uint8_t nodeId, int deviceType,
                        char *alarm_pattern, cmd_class_notify_t *data)
{
    if(!jobj || !alarm_pattern)
    {
        return;
    }

    char ID[8];
    snprintf(ID, sizeof(ID)-1, "%02X", nodeId);

    SEARCH_DATA_INIT_VAR(serialId);
    searching_database(_VR_CB_(zwave), &serialId, 
                    "SELECT serialId from SUB_DEVICES where id='%02X'", 
                    nodeId);
    if(serialId.len)
    {
        SEARCH_DATA_INIT_VAR(alarm_data);
        searching_database("zwave_handler", support_devs_db, zwave_cb, &alarm_data, 
                            "SELECT AlarmData from SUPPORT_DEVS where SerialID='%s'", 
                            serialId.value);
        if(alarm_data.len)
        {
            json_object *jobj_alarm = json_tokener_parse(alarm_data.value);
            if(jobj_alarm)
            {
                const char *alarmTypeFinal=json_object_get_string(
                                                json_object_object_get(jobj_alarm, alarm_pattern));
                if(alarmTypeFinal && strlen(alarmTypeFinal))
                {
                    json_object_object_add(jobj, ST_ALARM_TYPE_FINAL, json_object_new_string(alarmTypeFinal));         
                }
                else
                {
                    SLOGI("not found alarm pattern in json data\n");
                    try_to_parser_alarm_info(jobj, data);
                }
                json_object_put(jobj_alarm);
            }
            else
            {
                SLOGI("failed to paser alarm data\n");
                try_to_parser_alarm_info(jobj, data);
            }
        }
        else
        {
            try_to_parser_alarm_info(jobj, data);
        }
        FREE_SEARCH_DATA_VAR(alarm_data);
    }
    else
    {
        SLOGI("not found serialId\n");
    }
    FREE_SEARCH_DATA_VAR(serialId);

    alarm_update_database_and_voice_inform(ctx, jobj, ID);
}

void command_in_queue_handle_result(char *nodeId, json_object *jobj)
{
    if(!jobj)
    {
        SLOGE("jobj is NULL\n");
        return;
    }

    CHECK_JSON_OBJECT_EXIST(cmdClassObj, jobj, ST_CMD_CLASS, done);
    CHECK_JSON_OBJECT_EXIST(cmdObj, jobj, ST_CMD, done);
    CHECK_JSON_OBJECT_EXIST(data0Obj, jobj, ST_DATA0, done);

    const char *cmdClass = json_object_get_string(cmdClassObj);
    const char *cmd = json_object_get_string(cmdObj);
    const char *data0 = json_object_get_string(data0Obj);

    if(!strcmp(cmdClass, ST_ASSOCIATION))
    {
        if(!strcmp(cmd, ST_SET))
        {
            update_association_node_list(nodeId, (char*)data0, 0);
        }
        else if(!strcmp(cmd, ST_REMOVE))
        {
            update_association_node_list(nodeId, (char*)data0, 1);
        }
    }
done:
    return;
}

/*
check and update timestampt in dev
0 is not same status
1 is same status*/
static int check_dev_same_status(zwave_dev_info_t *dev, int value, unsigned int timestampt)
{
    int res = 0;

    if(!dev)
    {
        return res;
    }

    /*should update if device status is dead*/
    if(!dev->status.state)
    {
        return res;
    }
    if(
        ((value == 0) && (dev->status.onOff == 0)) ||
        ((value == 0xFF) && (dev->status.onOff == 1)) ||
        (dev->status.dim && (value == dev->status.dim) && dev->status.onOff == 1)
        )
    {
        dev->status.lastPoll = timestampt;
        res = 1;
    }

    return res;
}

static void notify_active_cb(struct uloop_timeout *timeout)
{
    //printf("notify_active_cb\n");
    NOTIFY_TX_BUFFER_T pTxNotify;
    int sending_notify = 1;
    int deviceWakeUp = 0;
    char str_value[SIZE_32B];
    if (g_notify.notify_index>0)
    {
        char ID[SIZE_64B], tmp[SIZE_256B];
        memset(ID, 0x00, sizeof(ID));
        memset(tmp, 0x00, sizeof(tmp));

        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
         
        if (g_notify.notify[0].notify_status==ADD_NODE_ZPC_NOTIFY ||
            g_notify.notify[0].notify_status==ADD_NODE_ZPC_NOTIFY_AUTO)
        {
            memcpy((uint8_t*)&pTxNotify.AddNodeZPCNotify,g_notify.notify[0].notify_message,sizeof(ADD_NODE_ZPC_NOTIFY_T));

            zwave_adding_handler(ctx, ADD_DEV_NOTIFICATION, jobj, pTxNotify, &sending_notify);

            if(sending_notify)
            {
                Send_ubus_notify((char*)json_object_to_json_string(jobj));
            }
        }
        else if (g_notify.notify[0].notify_status == S2_KEY_GRANT_NOTIFY)//grant key Secure S2 --> waiting user select keys
        {
            memcpy((uint8_t *)&pTxNotify.S2EventNotify, g_notify.notify[0].notify_message, S2_EVT_MAX_BUFFER_SIZE);
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_S2_KEY_GRANT));
            json_object_object_add(jobj, ST_SECURITY_KEYS, json_object_new_int(pTxNotify.S2EventNotify.kex_report.security_keys));
            json_object_object_add(jobj, ST_REQUEST_CSA, json_object_new_int(pTxNotify.S2EventNotify.kex_report.csa));
            //waiting Input Request key and CSA from user, App create CheckListBox to user choose keys
            Send_ubus_notify((char*)json_object_to_json_string(jobj));
        }
        else if (g_notify.notify[0].notify_status == S2_PUBLIC_KEY_CHALLENGE_NOTIFY)
        {
            memcpy((uint8_t *)&pTxNotify.S2EventNotify, g_notify.notify[0].notify_message, S2_EVT_MAX_BUFFER_SIZE);
            //check DSK, if 2 first byte of public key #0, send DSK to libzwave
            //else push notify to app, request DSK from user
            uint32_t dsk = 0;
            uint16_t byteDskIdentifyDev = 0;
            uint8_t * public_key = pTxNotify.S2EventNotify.challenge_req.public_key;
            dsk = *(uint8_t *)(public_key++);
            dsk = (dsk << 8) + *(uint8_t *)(public_key++);
            byteDskIdentifyDev = *(uint8_t *)(public_key++);
            byteDskIdentifyDev = (byteDskIdentifyDev << 8) + *(uint8_t *)(public_key);
            if (dsk)//add S0, S2.0
            {
                SLOGE("add S0, S2.0");
            }
            else
            {
                //push 4 first byte to app, 2 first byte is 0x00, 0x00,
                //2 next byte to suggest user choose correct device 
                json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_S2_PUBLIC_KEY_CHALLENGE));
                json_object_object_add(jobj, ST_S2_2_BYTE_SUGGEST, json_object_new_int(byteDskIdentifyDev));
                //waiting Input DSK from user. App create textbox to user enter DSK
                Send_ubus_notify((char*)json_object_to_json_string(jobj));
            }  

        }
        else if (g_notify.notify[0].notify_status == S2_PUBLIC_KEY_SHOW_UP_NOTIFY)//show my public key in add device CSA
        //show my public key in learning mode
        {
            memcpy((uint8_t *)&pTxNotify.S2EventNotify, g_notify.notify[0].notify_message, S2_EVT_MAX_BUFFER_SIZE);
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_S2_PUBLIC_KEY_SHOW_UP));
            uint16_t lowDSK = ((pTxNotify.S2EventNotify.key_show_up.my_dsk[0] << GET_HIGH_BYTE) + 
                    pTxNotify.S2EventNotify.key_show_up.my_dsk[1]);
            uint16_t highDSK = 0;
            if(pTxNotify.S2EventNotify.key_show_up.length_show_up_dsk == LENGTH_DSK_CSA)
            {
                highDSK = ((pTxNotify.S2EventNotify.key_show_up.my_dsk[0] << GET_HIGH_BYTE) + 
                    pTxNotify.S2EventNotify.key_show_up.my_dsk[1]);
                lowDSK = ((pTxNotify.S2EventNotify.key_show_up.my_dsk[2] << GET_HIGH_BYTE) + 
                    pTxNotify.S2EventNotify.key_show_up.my_dsk[3]);                
            }
            json_object_object_add(jobj, ST_S2_HIGH_MY_DSK, json_object_new_int(highDSK));
            json_object_object_add(jobj, ST_S2_LOW_MY_DSK, json_object_new_int(lowDSK));
            char  myDSK[NUMBER_DIGITS_MY_DSK], index;
            memset(myDSK, 0x00, sizeof(myDSK));
            for(index = 0; index < NUMBER_INT_MY_DSK; index++)
            {
                //2 Byte --> 5 digits
                sprintf(&myDSK[index * 5],"%05d", 
                    ((pTxNotify.S2EventNotify.key_show_up.my_dsk[index * 2] << GET_HIGH_BYTE) + pTxNotify.S2EventNotify.key_show_up.my_dsk[index * 2 + 1]));
            }
            json_object_object_add(jobj, ST_S2_MY_DSK, json_object_new_string(myDSK));
            if (pTxNotify.S2EventNotify.key_show_up.length_show_up_dsk == LENGTH_DSK_SSA)
            {
                json_object_object_add(jobj, ST_S2_MY_DSK_HIGHLIGHT_LEN, json_object_new_int(LENGTH_DSK_SSA_IN_DECIMAL_DIGIT));
            }
            else if (pTxNotify.S2EventNotify.key_show_up.length_show_up_dsk == LENGTH_DSK_CSA)
            {
                json_object_object_add(jobj, ST_S2_MY_DSK_HIGHLIGHT_LEN, json_object_new_int(LENGTH_DSK_CSA_IN_DECIMAL_DIGIT));
            }
            
            //App create textbox show my DSK
            Send_ubus_notify((char*)json_object_to_json_string(jobj));
        }       
        else if (g_notify.notify[0].notify_status==REMOVE_NODE_ZPC_NOTIFY ||
                 g_notify.notify[0].notify_status==REMOVE_NODE_ZPC_NOTIFY_AUTO)
        {
            memcpy((uint8_t*)&pTxNotify.RemoveNodeZPCNotify,g_notify.notify[0].notify_message,sizeof(ADD_NODE_ZPC_NOTIFY_T));
            zwave_removing_handler(ctx, jobj, pTxNotify, &sending_notify, g_notify.notify[0].notify_status);
            if(sending_notify)
            {
                Send_ubus_notify((char*)json_object_to_json_string(jobj));
            }
        }
        else if (g_notify.notify[0].notify_status==UPDATE_SUC_NOTIFY)
        {
            char ID[8];

            memcpy((uint8_t*)&pTxNotify.UpdateSUCNotify,g_notify.notify[0].notify_message,sizeof(UPDATE_SUC_NOTIFY_T));

            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_UPDATE_SUC_ID));
            json_object_object_add(jobj, ST_CONTROLLER_CAPABILITY, json_object_new_int(pTxNotify.UpdateSUCNotify.my_capability));
            sprintf(ID, "%02X", pTxNotify.UpdateSUCNotify.suc_id);
            json_object_object_add(jobj, ST_SUC_ID, json_object_new_string(ID));
            Send_ubus_notify((char*)json_object_to_json_string(jobj));
        }
        else if (g_notify.notify[0].notify_status==CONTROLLER_CHANGE_ZPC_NOTIFY)
        {
            char ID[8],Type[8];

            memcpy((uint8_t*)&pTxNotify.ControlerChangeZPCNotify,g_notify.notify[0].notify_message,sizeof(ADD_NODE_ZPC_NOTIFY_T));

            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CONTROLLER_CHANGE_ZPC));
            switch (pTxNotify.ControlerChangeZPCNotify.bStatus)
            {
                case ADD_NODE_STATUS_LEARN_READY:
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_READY));
                    break;
                case ADD_NODE_STATUS_NODE_FOUND:
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NODE_FOUND));
                    break;
                case ADD_REMOVE_NODE_TIMEOUT:
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_TIMEOUT));
                    break;
                case ADD_NODE_STATUS_INTERVIEW_DEV_FAILED:
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED_TO_INTERVIEW));
                    break;
                case ADD_NODE_STATUS_FAILED:
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    break;
                case ADD_NODE_STATUS_ADDING_CONTROLLER:
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_CHANGING_CONTROLLER));
                    sprintf(ID, "%02X", pTxNotify.ControlerChangeZPCNotify.zpc_node.node_id);
                    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
                    sprintf(Type, "%02X", pTxNotify.ControlerChangeZPCNotify.zpc_node.node_type);
                    json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(Type));
                    break; 
                case ADD_NODE_STATUS_DONE:
                {
                    uint8_t i,j;
                    json_object *jarray_capability = json_object_new_array();
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_CHANGE_DONE));
                    json_object_object_add(jobj, ST_CONTROLLER_CAPABILITY, json_object_new_int(pTxNotify.AddNodeZPCNotify.my_capability));
                    sprintf(ID, "%02X", pTxNotify.ControlerChangeZPCNotify.zpc_node.node_id);
                    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
                    sprintf(Type, "%02X", pTxNotify.ControlerChangeZPCNotify.zpc_node.node_type);
                    json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(Type));
                    json_object_object_add(jobj, ST_NODE_MODE, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.node_info.mode));
                    json_object_object_add(jobj, ST_TYPE_BASIC, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.node_info.nodeType.basic));
                    json_object_object_add(jobj, ST_TYPE_GENERIC, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.node_info.nodeType.generic));
                    json_object_object_add(jobj, ST_TYPE_SPECIFIC, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.node_info.nodeType.specific));


                    for (i=0;i<pTxNotify.ControlerChangeZPCNotify.zpc_node.node_capability.noCapability;i++)
                    {
                        json_object_array_add(jarray_capability, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.node_capability.aCapability[i]));
                    }
                    json_object_object_add(jobj, ST_NON_SECURE_CAPABILITY,jarray_capability);
                    json_object_object_add(jobj, ST_NODE_SECURE_SCHEME, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.node_secure_scheme));

                    /*add S0 capability */
                    json_object *jarray_secure_capability = json_object_new_array();
                    for (i = 0; i < pTxNotify.AddNodeZPCNotify.zpc_node.node_capability_secureV0.noCapability; i++)
                    {
                        json_object_array_add(jarray_secure_capability, json_object_new_int(pTxNotify.AddNodeZPCNotify.zpc_node.node_capability_secureV0.aCapability[i]));
                    }
                    json_object_object_add(jobj, ST_SECURE_CAPABILITY, jarray_secure_capability);

                    /*add S2 capability */
                    json_object *jarray_secure2_capability = json_object_new_array();
                    for (i = 0; i < pTxNotify.AddNodeZPCNotify.zpc_node.node_capability_secureV2.noCapability; i++)
                    {
                        json_object_array_add(jarray_secure2_capability, json_object_new_int(pTxNotify.AddNodeZPCNotify.zpc_node.node_capability_secureV2.aCapability[i]));
                    }
                    json_object_object_add(jobj, ST_SECURE_2_CAPABILITY, jarray_secure2_capability);

                    json_object_object_add(jobj, ST_NO_ENDPOINT, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.no_endpoint));

                    json_object *jarray_multichanel_capability = json_object_new_array();


                    for (i=0;i<pTxNotify.ControlerChangeZPCNotify.zpc_node.no_endpoint;i++)
                    {
                        json_object * jobj_multichannel_capability=json_object_new_object();
                        json_object_object_add(jobj_multichannel_capability, ST_ENDPOINT,json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.multi_channel_capability[i].endpoint));
                        json_object_object_add(jobj_multichannel_capability, ST_GENERIC_DEVICE_CLASS,json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.multi_channel_capability[i].generic_device_class));
                        json_object_object_add(jobj_multichannel_capability, ST_SPECIFIC_DEVICE_CLASS,json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.multi_channel_capability[i].specific_device_class));
                        json_object * jarray_endpoint_capability = json_object_new_array();
                        for (j=0;j<pTxNotify.ControlerChangeZPCNotify.zpc_node.multi_channel_capability[i].endpoint_capability.noCapability;j++)
                        {
                            json_object_array_add(jarray_endpoint_capability, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.multi_channel_capability[i].endpoint_capability.aCapability[j]));
                        }
                        json_object_object_add(jobj_multichannel_capability, ST_CAPABILITY,jarray_endpoint_capability);

                        json_object_array_add(jarray_multichanel_capability,jobj_multichannel_capability);
                    }
                    json_object_object_add(jobj, ST_MULTICHANNEL_CAPABILITY, jarray_multichanel_capability);
                    json_object_object_add(jobj, ST_IS_ZWAVE_PLUS, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.is_zp_node));

                    json_object_object_add(jobj, ST_NO_AGG_ENDPOINT, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.no_agg_endpoint));
                    json_object *jarray_multichanel_agg_capability = json_object_new_array();
                    for (i = 0; i < pTxNotify.ControlerChangeZPCNotify.zpc_node.no_agg_endpoint; i++)
                    {
                        json_object *jobj_multichannel_agg_capability = json_object_new_object();
                        json_object_object_add(jobj_multichannel_agg_capability, ST_ENDPOINT, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.multi_channel_agg_capability[i].endpoint));
                        json_object_object_add(jobj_multichannel_agg_capability, ST_TYPE_GENERIC, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.multi_channel_agg_capability[i].generic_device_class));
                        json_object_object_add(jobj_multichannel_agg_capability, ST_TYPE_SPECIFIC, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.multi_channel_agg_capability[i].specific_device_class));
                        json_object *jarray_endpoint_agg_capability = json_object_new_array();
                        for (j = 0; j < pTxNotify.ControlerChangeZPCNotify.zpc_node.multi_channel_agg_capability[i].endpoint_capability.noCapability; j++)
                        {
                            json_object_array_add(jarray_endpoint_agg_capability, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.multi_channel_agg_capability[i].endpoint_capability.aCapability[j]));
                        }
                        json_object_object_add(jobj_multichannel_agg_capability, ST_CAPABILITY, jarray_endpoint_agg_capability);

                        json_object *jarray_endpoint_agg_ep_member = json_object_new_array();
                        for (j = 0; j < pTxNotify.ControlerChangeZPCNotify.zpc_node.multi_channel_agg_capability[i].no_memberof; j++)
                        {
                            json_object_array_add(jarray_endpoint_agg_ep_member, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.multi_channel_agg_capability[i].ep_member[j]));
                        }
                        json_object_object_add(jobj_multichannel_agg_capability, ST_EP_MEMBERS, jarray_endpoint_agg_ep_member);

                        json_object_array_add(jarray_multichanel_agg_capability, jobj_multichannel_agg_capability);
                    }
                    json_object_object_add(jobj, ST_MULTICHANNEL_AGG_CAPABILITY, jarray_multichanel_agg_capability);

                    if (pTxNotify.ControlerChangeZPCNotify.zpc_node.is_zp_node>0)
                    {
                        json_object_object_add(jobj, ST_ROLE_TYPE, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.zp_info.role_type));
                        json_object_object_add(jobj, ST_NODE_TYPE, json_object_new_int(pTxNotify.ControlerChangeZPCNotify.zpc_node.zp_info.node_type));
                    }
                }
                break;
            }
            Send_ubus_notify((char*)json_object_to_json_string(jobj));
        }
        else if (g_notify.notify[0].notify_status==CMD_REQ_IN_QUEUE_ZPC_NOTIFY)
        {
            memcpy((uint8_t*)&pTxNotify.CmdReqInQueueNotify,g_notify.notify[0].notify_message,sizeof(cmd_req_in_queue_notify_t));

            if(pTxNotify.CmdClassNotify.source_endpoint!=0)
            {
                sprintf(ID, "%02X%02X", pTxNotify.CmdClassNotify.source_endpoint, 
                                            pTxNotify.CmdClassNotify.source_nodeId);
            }
            else
            {
                sprintf(ID, "%02X", pTxNotify.CmdClassNotify.source_nodeId);
            }

            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            sprintf(tmp, "%02X", pTxNotify.CmdReqInQueueNotify.source_endpoint);
            json_object_object_add(jobj, ST_DEVICE_ENDPOINT, json_object_new_string(tmp));

            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_SET_SPECIFICATION_IN_QUEUE));
            switch (pTxNotify.CmdReqInQueueNotify.status)
            {
                case TRANSMIT_COMPLETE_OK:
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                    break;
                case TRANSMIT_COMPLETE_NO_ACK:
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_TIMEOUT));
                    break;
                case TRANSMIT_COMPLETE_FAIL:
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    break;
                
            }

            json_object *jobj_command_info = json_tokener_parse(pTxNotify.CmdReqInQueueNotify.str_data);
            command_in_queue_handle_result(ID, jobj_command_info);
            json_object_object_add(jobj, ST_COMMAND_INFO, jobj_command_info);

            Send_ubus_notify((char*)json_object_to_json_string(jobj));
            json_object_put(jobj_command_info);
        }
        else if (g_notify.notify[0].notify_status==REPLACE_FAILED_NODE_ZPC_NOTIFY)
        {
            memcpy((uint8_t*)&pTxNotify.AddNodeZPCNotify,g_notify.notify[0].notify_message,sizeof(ADD_NODE_ZPC_NOTIFY_T));
            if(pTxNotify.AddNodeZPCNotify.bStatus != ADD_REMOVE_NODE_TIMEOUT)
            {
                if(pTxNotify.AddNodeZPCNotify.zpc_node.node_id)
                {
                    /*do not using thread in here*/
                    sprintf(ID, "%02X", pTxNotify.AddNodeZPCNotify.zpc_node.node_id);
                    remove_dev(ID);
                }
            }
            zwave_adding_handler(ctx, REPLACE_FAILED_NODE_NOTIFICATION, jobj, pTxNotify, &sending_notify);
            if(sending_notify)
            {
                Send_ubus_notify((char*)json_object_to_json_string(jobj));
            }
        }
        else if (g_notify.notify[0].notify_status==FIRMWARE_UPDATE_NOTIFY)
        {
            memcpy((uint8_t*)&pTxNotify.FirmwareUpdateNotify,g_notify.notify[0].notify_message,sizeof(FIRMWARE_UPDATE_NOTIFY_T));

            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_FIRMWARE_UPDATE_MD));
            switch (pTxNotify.FirmwareUpdateNotify.status)
            {
                
                case  FIRMWARE_UPDATE_TIMOUT_STATE:
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FIRMWARE_UPDATE_TIMOUT_STATE));
                    break;
                case  FIRMWARE_UPDATE_START:
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FIRMWARE_UPDATE_START));
                    break;
                case  FIRMWARE_UPDATE_STOP_BY_USER:
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FIRMWARE_UPDATE_STOP_BY_USER));
                    break;
                case  FIRMWARE_UPDATE_SUCCESS:
                    {
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FIRMWARE_UPDATE_SUCCESS));
                        json_object_object_add(jobj, ST_WAIT_TIME, json_object_new_int(pTxNotify.FirmwareUpdateNotify.wait_time));
                    }
                    break;
                case  FIRMWARE_UPDATE_FAIL:
                    {
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FIRMWARE_UPDATE_FAIL));
                        json_object_object_add(jobj, ST_WAIT_TIME, json_object_new_int(pTxNotify.FirmwareUpdateNotify.wait_time));                        
                    }
                    break;
                case  FIRMWARE_UPDATE_FAIL_TO_OPEN_FILE:
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FIRMWARE_UPDATE_FAIL_TO_OPEN_FILE));
                    break;
                case  FIRMWARE_UPDATE_DEVICE_NOT_RESPONE:
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FIRMWARE_UPDATE_DEVICE_NOT_RESPONE));
                    break;
                case  WAITING_FIRMWARE_UPDATE:
                    {
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_WAITING_FIRMWARE_UPDATE));
                        json_object_object_add(jobj, ST_PERCENTAGE, json_object_new_int(pTxNotify.FirmwareUpdateNotify.percent));                        
                        break;
                    }

            }
            sprintf(ID, "%02X", pTxNotify.FirmwareUpdateNotify.deviceId);
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            Send_ubus_notify((char*)json_object_to_json_string(jobj));
        }          
        else if (g_notify.notify[0].notify_status==CMD_CLASS_ZPC_NOTIFY)
        {
            memcpy((uint8_t*)&pTxNotify.CmdClassNotify,g_notify.notify[0].notify_message,sizeof(cmd_class_notify_t));
            
            /*children id combine by source_endpoint and source_nodeID*/
            if(pTxNotify.CmdClassNotify.source_endpoint!=0)
            {
                sprintf(ID, "%02X%02X", pTxNotify.CmdClassNotify.source_endpoint, 
                                            pTxNotify.CmdClassNotify.source_nodeId);
            }
            else
            {
                sprintf(ID, "%02X", pTxNotify.CmdClassNotify.source_nodeId);
            }
            
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            sprintf(tmp, "%02X", pTxNotify.CmdClassNotify.source_endpoint);
            json_object_object_add(jobj, ST_DEVICE_ENDPOINT, json_object_new_string(tmp));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CMD_CLASS_ZPC));

            switch (pTxNotify.CmdClassNotify.cmd_class)
            {
                case COMMAND_CLASS_ALARM:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case ALARM_REPORT:
                        {
                            printf("alarm_pattern:%s\n",pTxNotify.CmdClassNotify.meta_data.alarm_pattern);
                            cmd_class_notify_t *data = NULL;

                            dev_specific_replace_byte(jobj, ID, pTxNotify.CmdClassNotify.meta_data.alarm_pattern,
                                                        sizeof(pTxNotify.CmdClassNotify.meta_data.alarm_pattern));

                            printf("alarm_pattern:%s\n",pTxNotify.CmdClassNotify.meta_data.alarm_pattern);
                            dev_specific_polling_actions(ID, pTxNotify.CmdClassNotify.meta_data.alarm_pattern);
                            
                            if (pTxNotify.CmdClassNotify.version==1)
                            {
                                json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ALARM));
                                json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                                json_object_object_add(jobj, ST_ALARM_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.alarm_report_v1.alarm_type));
                                json_object_object_add(jobj, ST_ALARM_LEVEL, json_object_new_int(pTxNotify.CmdClassNotify.alarm_report_v1.alarm_level));
                            }
                            else if (pTxNotify.CmdClassNotify.version==2)
                            {
                                json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ALARM));
                                json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                                json_object_object_add(jobj, ST_ALARM_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.alarm_report_v2.alarm_type));
                                json_object_object_add(jobj, ST_ALARM_LEVEL, json_object_new_int(pTxNotify.CmdClassNotify.alarm_report_v2.alarm_level));
                                json_object_object_add(jobj, ST_SENSOR_SRC_ID, json_object_new_int(pTxNotify.CmdClassNotify.alarm_report_v2.zensor_src_id));
                                json_object_object_add(jobj, ST_ZWAVE_ALARM_STATUS, json_object_new_int(pTxNotify.CmdClassNotify.alarm_report_v2.zwave_alarm_status));
                                json_object_object_add(jobj, ST_ZWAVE_ALARM_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.alarm_report_v2.zwave_alarm_type));
                                json_object_object_add(jobj, ST_ZWAVE_ALARM_EVENT, json_object_new_int(pTxNotify.CmdClassNotify.alarm_report_v2.zwave_alarm_event));
                                json_object_object_add(jobj, ST_NO_EVENT_PARAMS, json_object_new_int(pTxNotify.CmdClassNotify.alarm_report_v2.no_event_params));
                                int i;
                                json_object *jarray=json_object_new_array();
                                for (i=0;i<pTxNotify.CmdClassNotify.alarm_report_v2.no_event_params;i++)
                                {
                                    json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.alarm_report_v2.event_param[i]));
                                }
                                json_object_object_add(jobj, ST_EVENT_PARAM,jarray);

                                data = &pTxNotify.CmdClassNotify;/*becareful with alarm_report_v2*/
                            }
                            else if (pTxNotify.CmdClassNotify.version==3)
                            {
                                json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_NOTIFICATION));
                                json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                                json_object_object_add(jobj, ST_ALARM_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.notification_report.alarm_type));
                                json_object_object_add(jobj, ST_ALARM_LEVEL, json_object_new_int(pTxNotify.CmdClassNotify.notification_report.alarm_level));
                                json_object_object_add(jobj, ST_ZWAVE_ALARM_STATUS, json_object_new_int(pTxNotify.CmdClassNotify.notification_report.zwave_alarm_status));
                                json_object_object_add(jobj, ST_ZWAVE_ALARM_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.notification_report.zwave_alarm_type));
                                json_object_object_add(jobj, ST_ZWAVE_ALARM_EVENT, json_object_new_int(pTxNotify.CmdClassNotify.notification_report.zwave_alarm_event));
                                json_object_object_add(jobj, ST_SEQUENCE, json_object_new_int(pTxNotify.CmdClassNotify.notification_report.sequence));
                                json_object_object_add(jobj, ST_NO_EVENT_PARAMS, json_object_new_int(pTxNotify.CmdClassNotify.notification_report.no_event_params));
                                int i;
                                json_object *jarray=json_object_new_array();
                                for (i=0;i<pTxNotify.CmdClassNotify.notification_report.no_event_params;i++)
                                {
                                    json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.notification_report.event_param[i]));
                                }
                                json_object_object_add(jobj, ST_EVENT_PARAM,jarray);
                                json_object_object_add(jobj, ST_SEQUENCE_NUMBER, json_object_new_int(pTxNotify.CmdClassNotify.notification_report.sequence_number));

                                data = &pTxNotify.CmdClassNotify;
                            }
                            alarm_report_final(ctx, jobj, pTxNotify.CmdClassNotify.source_nodeId,
                                                    pTxNotify.CmdClassNotify.node_type,
                                                    pTxNotify.CmdClassNotify.meta_data.alarm_pattern,
                                                    data);
                        }
                        break;

                        case ALARM_TYPE_SUPPORTED_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ALARM));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));
                            json_object_object_add(jobj, ST_V1_ALARM, json_object_new_int(pTxNotify.CmdClassNotify.alarm_supported_report.v1_alarm));
                            json_object_object_add(jobj, ST_NO_BIT_MASKS, json_object_new_int(pTxNotify.CmdClassNotify.alarm_supported_report.no_bit_masks));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.alarm_supported_report.no_bit_masks;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.alarm_supported_report.bit_mask[i]));
                            }
                            json_object_object_add(jobj, ST_BIT_MASK,jarray);
                        }
                        break;

                        case EVENT_SUPPORTED_REPORT_V3:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_NOTIFICATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_EVENT_SUPPORTED_REPORT));
                            json_object_object_add(jobj, ST_NOTIFICATION_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.event_supported_report.notification_type));
                            json_object_object_add(jobj, ST_NO_BIT_MASKS, json_object_new_int(pTxNotify.CmdClassNotify.event_supported_report.no_bit_masks));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.event_supported_report.no_bit_masks;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.event_supported_report.bit_mask[i]));
                            }
                            json_object_object_add(jobj, ST_BIT_MASK,jarray);
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_SENSOR_ALARM:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case SENSOR_ALARM_REPORT:
                        {
                            printf("alarm_pattern:%s\n",pTxNotify.CmdClassNotify.meta_data.alarm_pattern);
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SENSOR_ALARM));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));

                            json_object_object_add(jobj, ST_SRC_ID, json_object_new_int(pTxNotify.CmdClassNotify.sensor_alarm_report.src_id));
                            json_object_object_add(jobj, ST_SENSOR_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.sensor_alarm_report.sensor_type));
                            json_object_object_add(jobj, ST_SENSOR_STATE, json_object_new_int(pTxNotify.CmdClassNotify.sensor_alarm_report.sensor_state));
                            json_object_object_add(jobj, ST_SECONDS, json_object_new_int(pTxNotify.CmdClassNotify.sensor_alarm_report.seconds));
                            alarm_report_final(ctx, jobj, pTxNotify.CmdClassNotify.source_nodeId,
                                                pTxNotify.CmdClassNotify.node_type,
                                                pTxNotify.CmdClassNotify.meta_data.alarm_pattern,
                                                NULL);
                        }
                        break;

                        case SENSOR_ALARM_SUPPORTED_REPORT:
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SENSOR_ALARM));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));
                            json_object_object_add(jobj, ST_NO_BIT_MASKS, json_object_new_int(pTxNotify.CmdClassNotify.sensor_alarm_supported_report.no_bit_masks));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.sensor_alarm_supported_report.no_bit_masks;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.sensor_alarm_supported_report.bit_mask[i]));
                            }
                            json_object_object_add(jobj, ST_BIT_MASK,jarray);

                        break;
                    }
                }
                break;

                case COMMAND_CLASS_SENSOR_BINARY:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case SENSOR_BINARY_REPORT:
                        {
                            printf("alarm_pattern:%s\n",pTxNotify.CmdClassNotify.meta_data.alarm_pattern);
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SENSOR_BINARY));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            if (pTxNotify.CmdClassNotify.version==1)
                            {
                                json_object_object_add(jobj, ST_SENSOR_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.sensor_binary_report_v1.sensor_value));
                            }else if (pTxNotify.CmdClassNotify.version==2)
                            {
                                json_object_object_add(jobj, ST_SENSOR_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.sensor_binary_report_v2.sensor_value));
                                json_object_object_add(jobj, ST_SENSOR_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.sensor_binary_report_v2.sensor_type));
                            }
                            alarm_report_final(ctx, jobj, pTxNotify.CmdClassNotify.source_nodeId,
                                                    pTxNotify.CmdClassNotify.node_type,
                                                    pTxNotify.CmdClassNotify.meta_data.alarm_pattern,
                                                    NULL);
                        }
                        break;

                        case SENSOR_BINARY_SUPPORTED_SENSOR_REPORT_V2:
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SENSOR_BINARY));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));
                            json_object_object_add(jobj, ST_NO_BIT_MASKS, json_object_new_int(pTxNotify.CmdClassNotify.sensor_binary_supported_report.no_bit_masks));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.sensor_binary_supported_report.no_bit_masks;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.sensor_binary_supported_report.bit_mask[i]));
                            }
                            json_object_object_add(jobj, ST_BIT_MASK,jarray);

                        break;
                    }
                }
                break;

                case COMMAND_CLASS_SWITCH_ALL:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case SWITCH_ALL_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SWITCH_ALL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            json_object_object_add(jobj, ST_MODE, json_object_new_int(pTxNotify.CmdClassNotify.switch_all_report.mode));
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_DEVICE_RESET_LOCALLY:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case DEVICE_RESET_LOCALLY_NOTIFICATION:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_DEVICE_RESET_LOCALLY));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_NOTIFICATION));

                            remove_dev(ID);
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_ZWAVEPLUS_INFO:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case ZWAVEPLUS_INFO_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ZWAVE_PLUS_INFO));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));

                            json_object_object_add(jobj, ST_ZWAVE_PLUS_VERSION, json_object_new_int(pTxNotify.CmdClassNotify.zwaveplus_info_report.zwaveplus_version));
                            json_object_object_add(jobj, ST_ROLE_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.zwaveplus_info_report.role_type));
                            json_object_object_add(jobj, ST_NODE_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.zwaveplus_info_report.node_type));
                            json_object_object_add(jobj, ST_INSTALLER_ICON_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.zwaveplus_info_report.installer_icon_type));
                            json_object_object_add(jobj, ST_USER_ICON_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.zwaveplus_info_report.user_icon_type));
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_ASSOCIATION:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case ASSOCIATION_REPORT:
                        {
                            char nodeList[SIZE_256B] = {0};
                            char groupId[SIZE_32B] = {0};
                            char maxNode[SIZE_32B] = {0};
                            sprintf(groupId, "%02X", pTxNotify.CmdClassNotify.association_report.groud_id);
                            sprintf(maxNode, "%d", pTxNotify.CmdClassNotify.association_report.max_nodes_supported);

                            set_property_database(_VR_CB_(zwave), ID, ST_ASSOCIATION, groupId, ST_MAX_NODE_SUPPORT, maxNode);

                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ASSOCIATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));

                            // json_object_object_add(jobj, ST_GROUP_ID, json_object_new_int(pTxNotify.CmdClassNotify.association_report.groud_id));
                            json_object_object_add(jobj, ST_MAX_NODES_SUPPORTED, json_object_new_int(pTxNotify.CmdClassNotify.association_report.max_nodes_supported));
                            json_object_object_add(jobj, ST_REPORT_TO_FOLLOW, json_object_new_int(pTxNotify.CmdClassNotify.association_report.report_to_follow));
                            json_object_object_add(jobj, ST_NO_NODES, json_object_new_int(pTxNotify.CmdClassNotify.association_report.no_nodes));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.association_report.no_nodes;i++)
                            {
                                if(!i)
                                {
                                    sprintf(nodeList,"%02X", pTxNotify.CmdClassNotify.association_report.node_list[i]);
                                }
                                else
                                {
                                    sprintf(nodeList+strlen(nodeList), ",%02X", pTxNotify.CmdClassNotify.association_report.node_list[i]);
                                }
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.association_report.node_list[i]));
                            }
                            json_object_object_add(jobj, ST_NODE_LIST, jarray);

                            if(strlen(nodeList))
                            {
                                set_ass_node_list_database(_VR_CB_(zwave), ID, groupId, nodeList, ST_REPLACE, 0);
                            }
                            json_object_object_add(jobj, ST_GROUP_ID, json_object_new_string(groupId));
                            json_object_object_add(jobj, ST_MAX_NODE_SUPPORT, json_object_new_string(maxNode));
                            json_object_object_add(jobj, ST_NODE_LIST, json_object_new_string(nodeList));
                        }
                        break;

                        case ASSOCIATION_SPECIFIC_GROUP_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ASSOCIATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SPECIFIC_GROUP_REPORT));

                            json_object_object_add(jobj, ST_GROUP, json_object_new_int(pTxNotify.CmdClassNotify.association_specific_group_report.group));
                        }
                        break;

                        case ASSOCIATION_GROUPINGS_REPORT:
                        {
                            char maxGroup[SIZE_32B] = {0};
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ASSOCIATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_GROUPINGS_REPORT));

                            json_object_object_add(jobj, ST_SUPPORTED_GROUPS, json_object_new_int(pTxNotify.CmdClassNotify.association_groupings_report.supported_groups));

                            sprintf(maxGroup, "%d", pTxNotify.CmdClassNotify.association_groupings_report.supported_groups);

                            json_object_object_add(jobj, ST_ASSOCIATION_MAX_GROUP_SUPPORT, json_object_new_string(maxGroup));

                            set_register_database(_VR_CB_(zwave), ID, ST_ASSOCIATION_MAX_GROUP_SUPPORT, maxGroup, ST_REPLACE, 0);
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION_V2:
                {
                    switch (pTxNotify.CmdClassNotify.cmd)
                    {
                        case MULTI_CHANNEL_ASSOCIATION_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_MULTI_CHANNEL_ASSOCIATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));

                            json_object_object_add(jobj, ST_GROUP_ID, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_association_report.groud_id));
                            json_object_object_add(jobj, ST_MAX_NODES_SUPPORTED, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_association_report.max_nodes_supported));
                            json_object_object_add(jobj, ST_REPORT_TO_FOLLOW, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_association_report.report_to_follow));
                            json_object_object_add(jobj, ST_NO_NODES, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_association_report.no_nodes));
                            int i;
                            json_object *jarray = json_object_new_array();
                            for (i = 0; i < pTxNotify.CmdClassNotify.multi_channel_association_report.no_nodes; i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_association_report.node_list[i]));
                            }
                            json_object_object_add(jobj, ST_NODE_LIST, jarray);
                            json_object *jarray_multi_endpoint = json_object_new_array();
                            json_object_object_add(jobj, ST_NO_MULTI_CHANNEL_NODE, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_association_report.no_multi_channel_node));
                            for (i = 0; i < pTxNotify.CmdClassNotify.multi_channel_association_report.no_multi_channel_node; i++)
                            {
                                json_object *jlist = json_object_new_object();
                                json_object_object_add(jlist, ST_MULTI_CHANNEL_NODE, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_association_report.multi_channel_node_list[i]));
                                json_object_object_add(jlist, ST_BIT_ADDRESS, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_association_report.bit_address_list[i]));
                                json_object_object_add(jlist, ST_ENDPOINT, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_association_report.endpoint_list[i]));
                                json_object_array_add(jarray_multi_endpoint, jlist);
                            }
                            json_object_object_add(jobj, ST_USAGE_LIST, jarray_multi_endpoint);
                        }
                        break;

                        case ASSOCIATION_SPECIFIC_GROUP_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ASSOCIATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SPECIFIC_GROUP_REPORT));

                            json_object_object_add(jobj, ST_GROUP, json_object_new_int(pTxNotify.CmdClassNotify.association_specific_group_report.group));
                        }
                        break;

                        case MULTI_CHANNEL_ASSOCIATION_GROUPINGS_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_MULTI_CHANNEL_ASSOCIATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_GROUPINGS_REPORT));

                            json_object_object_add(jobj, ST_SUPPORTED_GROUPS,
                                                   json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_association_groupings_report.supported_groups));
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_ASSOCIATION_GRP_INFO:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case ASSOCIATION_GROUP_NAME_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ASSOCIATION_GRP_INFO));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_GROUP_NAME_REPORT));
                            
                            json_object_object_add(jobj, ST_GROUP_ID, json_object_new_int(pTxNotify.CmdClassNotify.association_group_name_report.group_id));
                            json_object_object_add(jobj, ST_NAME_LEN, json_object_new_int(pTxNotify.CmdClassNotify.association_group_name_report.name_len));
                            json_object_object_add(jobj, ST_NAME, json_object_new_string(pTxNotify.CmdClassNotify.association_group_name_report.name));

                        }
                        break;

                        case ASSOCIATION_GROUP_COMMAND_LIST_REPORT:
                        {
                            size_t length = 0;
                            char groupId[SIZE_32B] = {0};
                            char classSupport[SIZE_512B] = {0};
                            sprintf(groupId, "%02X", pTxNotify.CmdClassNotify.association_group_cmd_list_report.group_id);

                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ASSOCIATION_GRP_INFO));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_COMMAND_LIST_REPORT));
                            
                            json_object_object_add(jobj, ST_GROUP_ID, json_object_new_int(pTxNotify.CmdClassNotify.association_group_cmd_list_report.group_id));
                            json_object_object_add(jobj, ST_LIST_LEN, json_object_new_int(pTxNotify.CmdClassNotify.association_group_cmd_list_report.list_len));
                            int i;
                            json_object *jarray=json_object_new_array();

                            for (i=0;i<pTxNotify.CmdClassNotify.association_group_cmd_list_report.list_len/2;i++)
                            {
                                if(!i)
                                {
                                    sprintf(classSupport,"%d", pTxNotify.CmdClassNotify.association_group_cmd_list_report.cmd_list[i].cmd_class);
                                }
                                else
                                {
                                    length = strlen(classSupport);
                                    snprintf(classSupport+length, sizeof(classSupport)-length-1, ",%d", 
                                            pTxNotify.CmdClassNotify.association_group_cmd_list_report.cmd_list[i].cmd_class);
                                }
                                json_object *jcmd=json_object_new_object();
                                json_object_object_add(jcmd, ST_CMD_CLASS, json_object_new_int(pTxNotify.CmdClassNotify.association_group_cmd_list_report.cmd_list[i].cmd_class));
                                json_object_object_add(jcmd, ST_CMD, json_object_new_int(pTxNotify.CmdClassNotify.association_group_cmd_list_report.cmd_list[i].cmd));
                                json_object_array_add(jarray,jcmd);
                            }
                            json_object_object_add(jobj, ST_CMD_LIST,jarray);
                            json_object_object_add(jobj, ST_CLASS_SUPPORT, json_object_new_string(classSupport));

                            set_property_database(_VR_CB_(zwave), ID, ST_ASSOCIATION, groupId, ST_CLASS_SUPPORT, classSupport);
                        }
                        break;

                        case ASSOCIATION_GROUP_INFO_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ASSOCIATION_GRP_INFO));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_GROUP_INFO_REPORT));
                            
                            json_object_object_add(jobj, ST_LIST_MODE, json_object_new_int(pTxNotify.CmdClassNotify.association_group_info_report.list_mode));
                            json_object_object_add(jobj, ST_DYNAMIC_INFO, json_object_new_int(pTxNotify.CmdClassNotify.association_group_info_report.dynamic_info));
                            json_object_object_add(jobj, ST_GROUP_COUNT, json_object_new_int(pTxNotify.CmdClassNotify.association_group_info_report.group_count));
                            int i;
                            json_object *jarray=json_object_new_array();

                            for (i=0;i<pTxNotify.CmdClassNotify.association_group_info_report.group_count;i++)
                            {
                                json_object *jgrp=json_object_new_object();
                                json_object_object_add(jgrp, ST_GROUP_ID, json_object_new_int(pTxNotify.CmdClassNotify.association_group_info_report.group_info[i].group_id));
                                json_object_object_add(jgrp, ST_MODE, json_object_new_int(pTxNotify.CmdClassNotify.association_group_info_report.group_info[i].mode));
                                json_object_object_add(jgrp, ST_PROFILE, json_object_new_int(pTxNotify.CmdClassNotify.association_group_info_report.group_info[i].profile));
                                json_object_object_add(jgrp, ST_EVENT_CODE, json_object_new_int(pTxNotify.CmdClassNotify.association_group_info_report.group_info[i].event_code));

                                json_object_array_add(jarray,jgrp);
                            }
                            json_object_object_add(jobj, ST_GROUP_INFO,jarray);

                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_APPLICATION_STATUS:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case APPLICATION_BUSY:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_APPLICATION_STATUS));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_BUSY));
                            
                            json_object_object_add(jobj, ST_STATUS, json_object_new_int(pTxNotify.CmdClassNotify.application_busy.status));
                            json_object_object_add(jobj, ST_WAIT_TIME, json_object_new_int(pTxNotify.CmdClassNotify.application_busy.wait_time));
                            
                        }
                        break;

                        case APPLICATION_REJECTED_REQUEST:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_APPLICATION_STATUS));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REJECTED_REQUEST));
                            
                            json_object_object_add(jobj, ST_STATUS, json_object_new_int(pTxNotify.CmdClassNotify.application_reject_request.status));
                            
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_SENSOR_MULTILEVEL:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case SENSOR_MULTILEVEL_SUPPORTED_SCALE_REPORT_V5:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SENSOR_MULTILEVEL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_SCALE_REPORT));
                            
                            json_object_object_add(jobj, ST_SENSOR_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.sensor_multilevel_supported_scale_report.sensor_type));
                            json_object_object_add(jobj, ST_SCALE_BIT_MASK, json_object_new_int(pTxNotify.CmdClassNotify.sensor_multilevel_supported_scale_report.scale_bit_mask));
                            
                            const char *sensorName = NULL;
                            const char *sensorUnitSupport = NULL;

                            sensorName = get_sensor_name(pTxNotify.CmdClassNotify.sensor_multilevel_supported_scale_report.sensor_type, 
                                                        pTxNotify.CmdClassNotify.sensor_multilevel_supported_scale_report.scale_bit_mask, 
                                                        &sensorUnitSupport);
                            JSON_ADD_STRING_SAFE(jobj, ST_SENSOR_PATTERN, sensorName);
                            JSON_ADD_STRING_SAFE(jobj, ST_SENSOR_UNIT_SUPPORT, sensorUnitSupport);

                            if(sensorName && sensorUnitSupport)
                            {
                                set_property_database(_VR_CB_(zwave), ID, ST_SENSOR_MULTILEVEL, (char *)sensorName,
                                                        ST_SENSOR_UNIT_SUPPORT, (char *)sensorUnitSupport);
                            }
                        }
                        break;

                        case SENSOR_MULTILEVEL_SUPPORTED_SENSOR_REPORT_V5:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SENSOR_MULTILEVEL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_SENSOR_REPORT));
                            
                            json_object_object_add(jobj, ST_NO_BIT_MASKS, json_object_new_int(pTxNotify.CmdClassNotify.sensor_multilevel_supported_report.no_bit_masks));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.sensor_multilevel_supported_report.no_bit_masks;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.sensor_multilevel_supported_report.bit_mask[i]));
                            }
                            json_object_object_add(jobj, ST_BIT_MASK,jarray);

                            char *sensorSupport = NULL;
                            get_sensor_multilevel_from_bit_mask(pTxNotify.CmdClassNotify.sensor_multilevel_supported_report.bit_mask,
                                                            pTxNotify.CmdClassNotify.sensor_multilevel_supported_report.no_bit_masks,
                                                            &sensorSupport);
                            JSON_ADD_STRING_SAFE(jobj, ST_SENSOR_SUPPORT, sensorSupport);

                            set_register_database(_VR_CB_(zwave), ID, ST_SENSOR_SUPPORT, sensorSupport, ST_REPLACE, 0);

                            SAFE_FREE(sensorSupport);
                        }
                        break;

                        case SENSOR_MULTILEVEL_REPORT:
                        {
                            float value=0;
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SENSOR_MULTILEVEL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            json_object_object_add(jobj, ST_SENSOR_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.sensor_multilevel_report.sensor_type));
                            json_object_object_add(jobj, ST_PRECISION, json_object_new_int(pTxNotify.CmdClassNotify.sensor_multilevel_report.precision));
                            json_object_object_add(jobj, ST_SCALE, json_object_new_int(pTxNotify.CmdClassNotify.sensor_multilevel_report.scale));
                            json_object_object_add(jobj, ST_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.sensor_multilevel_report.size));
                            json_object_object_add(jobj, ST_SENSOR_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.sensor_multilevel_report.sensor_value));
                            value = pTxNotify.CmdClassNotify.sensor_multilevel_report.sensor_value/
                                                        pow(10,pTxNotify.CmdClassNotify.sensor_multilevel_report.precision);
                            
                            const char *sensorUnitName = ST_UNKNOWN;
                            const char *sensorName = get_sensor_name(pTxNotify.CmdClassNotify.sensor_multilevel_report.sensor_type, 
                                                                     pTxNotify.CmdClassNotify.sensor_multilevel_report.scale, 
                                                                     &sensorUnitName);

                            json_object_object_add(jobj, ST_SENSOR_PATTERN, json_object_new_string(sensorName));
                            json_object_object_add(jobj, ST_UNIT, json_object_new_string(sensorUnitName));
                            sprintf(str_value, "%f", value);
                            json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));

                            set_sensor_value_database(_VR_CB_(zwave), ID, sensorName, str_value, sensorUnitName);
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_THERMOSTAT_SETPOINT:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case THERMOSTAT_SETPOINT_CAPABILITIES_REPORT_V3:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_THERMOSTAT_SETPOINT));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_CAPABILITIES_REPORT));
                            
                            json_object_object_add(jobj, ST_SET_POINT_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.setpoint_type));
                            json_object_object_add(jobj, ST_MIN_PRECISION, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.min_precision));
                            json_object_object_add(jobj, ST_MIN_SCALE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.min_scale));
                            json_object_object_add(jobj, ST_MIN_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.min_size));
                            json_object_object_add(jobj, ST_MIN_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.min_value));
                            json_object_object_add(jobj, ST_MAX_PRECISION, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.max_precision));
                            json_object_object_add(jobj, ST_MAX_SCALE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.max_scale));
                            json_object_object_add(jobj, ST_MAX_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.max_size));
                            json_object_object_add(jobj, ST_MAX_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.max_value));

                            char floatFormat[SIZE_32B];
                            sprintf(floatFormat, "%d,%d,%d",
                                    pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.min_precision,
                                    pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.min_scale,
                                    pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.min_size);

                            if(pTxNotify.CmdClassNotify.thermostat_setpoint_report.scale == MULTILEVEL_SENSOR_LABEL_CELCIUS)
                            {
                                saving_thermostat_floatformat_database(_VR_CB_(zwave), ID, ST_MULTILEVEL_SENSOR_LABEL_CELCIUS, floatFormat);
                            }
                            else
                            {
                                saving_thermostat_floatformat_database(_VR_CB_(zwave), ID, ST_MULTILEVEL_SENSOR_LABEL_FAHRENHEIT, floatFormat);
                            }

                        }
                        break;

                        case THERMOSTAT_SETPOINT_SUPPORTED_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_THERMOSTAT_SETPOINT));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));
                            
                            json_object_object_add(jobj, ST_NO_BIT_MASKS, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_operating_logging_supported_report.no_bit_masks));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.thermostat_operating_logging_supported_report.no_bit_masks;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_operating_logging_supported_report.bit_mask[i]));
                            }
                            json_object_object_add(jobj, ST_BIT_MASK,jarray);

                            char *thermostatSetpointSupport = NULL;
                            get_thermostat_setpoint_from_bit_mask(pTxNotify.CmdClassNotify.thermostat_operating_logging_supported_report.bit_mask,
                                                            pTxNotify.CmdClassNotify.thermostat_operating_logging_supported_report.no_bit_masks,
                                                            &thermostatSetpointSupport);
                            JSON_ADD_STRING_SAFE(jobj, ST_THERMOSTAT_SETPOINT_SUPPORT, thermostatSetpointSupport);

                            set_register_database(_VR_CB_(zwave), ID, ST_THERMOSTAT_SETPOINT_SUPPORT, thermostatSetpointSupport, ST_REPLACE, 0);

                            SAFE_FREE(thermostatSetpointSupport);
                        }
                        break;

                        case THERMOSTAT_SETPOINT_REPORT:
                        {
                            const char *setpointPattern = get_thermostat_setpoint_name(pTxNotify.CmdClassNotify.thermostat_setpoint_report.setpoint_type);
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_THERMOSTAT_SETPOINT));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            json_object_object_add(jobj, ST_SET_POINT_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_setpoint_report.setpoint_type));
                            json_object_object_add(jobj, ST_PRECISION, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_setpoint_report.precision));
                            json_object_object_add(jobj, ST_SCALE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_setpoint_report.scale));
                            json_object_object_add(jobj, ST_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_setpoint_report.size));                            
                            sprintf(str_value, "%d", pTxNotify.CmdClassNotify.thermostat_setpoint_report.value);
                            json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));
                            json_object_object_add(jobj, ST_SET_POINT_PATTERN, json_object_new_string(setpointPattern));
                            
                            float temp =  pTxNotify.CmdClassNotify.thermostat_setpoint_report.value/
                                                        pow(10,pTxNotify.CmdClassNotify.thermostat_setpoint_report.precision);
                            sprintf(str_value, "%.3f", temp);

                            char floatFormat[SIZE_32B];
                            sprintf(floatFormat, "%d,%d,%d",
                                    pTxNotify.CmdClassNotify.thermostat_setpoint_report.precision,
                                    pTxNotify.CmdClassNotify.thermostat_setpoint_report.scale,
                                    pTxNotify.CmdClassNotify.thermostat_setpoint_report.size);

                            if(pTxNotify.CmdClassNotify.thermostat_setpoint_report.scale == MULTILEVEL_SENSOR_LABEL_CELCIUS)
                            {
                                json_object_object_add(jobj, ST_UNIT, json_object_new_string(ST_MULTILEVEL_SENSOR_LABEL_CELCIUS));
                                set_thermostat_setpoint_value_database(_VR_CB_(zwave), ID, setpointPattern, str_value, ST_MULTILEVEL_SENSOR_LABEL_CELCIUS);
                                saving_thermostat_floatformat_database(_VR_CB_(zwave), ID, ST_MULTILEVEL_SENSOR_LABEL_CELCIUS, floatFormat);
                            }
                            else
                            {
                                json_object_object_add(jobj, ST_UNIT, json_object_new_string(ST_MULTILEVEL_SENSOR_LABEL_FAHRENHEIT));
                                set_thermostat_setpoint_value_database(_VR_CB_(zwave), ID, setpointPattern, str_value, ST_MULTILEVEL_SENSOR_LABEL_FAHRENHEIT);
                                saving_thermostat_floatformat_database(_VR_CB_(zwave), ID, ST_MULTILEVEL_SENSOR_LABEL_FAHRENHEIT, floatFormat);
                            }
                            json_object_object_add(jobj, ST_SET_POINT_VALUE, json_object_new_string(str_value));
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_THERMOSTAT_SETBACK:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case THERMOSTAT_SETBACK_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_THERMOSTAT_SETBACK));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            json_object_object_add(jobj, ST_SET_BACK_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_setback_report.setback_type));
                            json_object_object_add(jobj, ST_SET_BACK_STATE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_setback_report.setback_state));
                        }
                        break;
                    }
                        
                }
                break;

                case COMMAND_CLASS_THERMOSTAT_OPERATING_STATE:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case THERMOSTAT_OPERATING_STATE_LOGGING_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_THERMOSTAT_OPERATING_STATE));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_LOGGING_REPORT));
                            
                            json_object_object_add(jobj, ST_REPORT_TO_FOLLOW, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_operating_logging_report.report_to_follow));
                            json_object_object_add(jobj, ST_NO_USAGES, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_operating_logging_report.no_usages));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.thermostat_operating_logging_report.no_usages;i++)
                            {
                                json_object *jlist=json_object_new_object();
                                json_object_object_add(jlist, ST_OPERATING_STATE_LOG_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_operating_logging_report.usage_list[i].operating_state_log_type));
                                json_object_object_add(jlist, ST_USAGE_TODAY_HOURS, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_operating_logging_report.usage_list[i].usage_today_hours));
                                json_object_object_add(jlist, ST_USAGE_TODAY_MINUTES, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_operating_logging_report.usage_list[i].usage_today_minutes));
                                json_object_object_add(jlist, ST_USAGE_YESTERDAY_HOURS, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_operating_logging_report.usage_list[i].usage_yesterday_hours));
                                json_object_object_add(jlist, ST_USAGE_YESTERDAY_MINUTES, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_operating_logging_report.usage_list[i].usage_yesterday_minutes));

                                json_object_array_add(jarray,jlist);
                            }
                            json_object_object_add(jobj, ST_USAGE_LIST, jarray);
                        }
                        break;

                        case THERMOSTAT_OPERATING_STATE_REPORT:
                        {
                            const char *thermostat_operating_mode = get_thermostat_operating_name(pTxNotify.CmdClassNotify.thermostat_operating_state_report.operating_state);
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_THERMOSTAT_OPERATING_STATE));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            json_object_object_add(jobj, ST_OPERATING_STATE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_operating_state_report.operating_state));
                            json_object_object_add(jobj, ST_THERMOSTAT_OPERATING_STATE, json_object_new_string(thermostat_operating_mode));

                            set_register_database(_VR_CB_(zwave), ID, ST_THERMOSTAT_OPERATING_STATE, thermostat_operating_mode, ST_REPLACE, 0);
                        }
                        break;

                        case THERMOSTAT_OPERATING_LOGGING_SUPPORTED_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_THERMOSTAT_OPERATING_STATE));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));
                            
                            json_object_object_add(jobj, ST_NO_BIT_MASKS, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_operating_logging_supported_report.no_bit_masks));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.thermostat_operating_logging_supported_report.no_bit_masks;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_operating_logging_supported_report.bit_mask[i]));
                            }
                            json_object_object_add(jobj, ST_BIT_MASK,jarray);
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_THERMOSTAT_MODE:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case THERMOSTAT_MODE_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_THERMOSTAT_MODE));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            const char *mode = ST_UNKNOWN;
                            if (pTxNotify.CmdClassNotify.version==1)
                            {
                                json_object_object_add(jobj, ST_MODE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_mode_report_v1.mode));
                                mode = get_thermostat_mode_name(pTxNotify.CmdClassNotify.thermostat_mode_report_v1.mode);
                                json_object_object_add(jobj, ST_THERMOSTAT_MODE, json_object_new_string(mode));
                            }
                            else if (pTxNotify.CmdClassNotify.version==3)
                            {
                                json_object_object_add(jobj, ST_MODE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_mode_report_v3.mode));
                                json_object_object_add(jobj, ST_NO_MANUFACTURER_DATA, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_mode_report_v3.no_manufacturer_data));
                                int i;
                                json_object *jarray=json_object_new_array();
                                for (i=0;i<pTxNotify.CmdClassNotify.thermostat_mode_report_v3.no_manufacturer_data;i++)
                                {
                                    json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_mode_report_v3.manufacturer_data[i]));
                                }
                                json_object_object_add(jobj, ST_MANUFACTURER_DATA,jarray);

                                mode = get_thermostat_mode_name(pTxNotify.CmdClassNotify.thermostat_mode_report_v1.mode);
                                json_object_object_add(jobj, ST_THERMOSTAT_MODE, json_object_new_string(mode));
                            }

                            if(!strcmp(mode, ST_OFF))
                            {
                                SEARCH_DATA_INIT_VAR(thermostat_mode);
                                searching_database(_VR_CB_(zwave), &thermostat_mode, 
                                                    "SELECT register from FEATURES where deviceId='%s' AND featureId='%s'", 
                                                    ID, ST_THERMOSTAT_MODE);
                                if(thermostat_mode.len)
                                {
                                    if(strcmp(thermostat_mode.value, ST_OFF))
                                    {
                                        set_register_database(_VR_CB_(zwave), ID, ST_THERMOSTAT_PREVIOUS_MODE, 
                                                                thermostat_mode.value, ST_REPLACE, 0);
                                    }
                                }
                                else
                                {
                                    set_register_database(_VR_CB_(zwave), ID, ST_THERMOSTAT_PREVIOUS_MODE, 
                                                            ST_COOL, ST_REPLACE, 0);
                                }
                                FREE_SEARCH_DATA_VAR(thermostat_mode);
                            }

                            set_register_database(_VR_CB_(zwave), ID, ST_THERMOSTAT_MODE, mode, ST_REPLACE, 0);
                        }
                        break;

                        case THERMOSTAT_MODE_SUPPORTED_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_THERMOSTAT_MODE));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));
                            
                            json_object_object_add(jobj, ST_NO_BIT_MASKS, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_mode_supported_report.no_bit_masks));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.thermostat_mode_supported_report.no_bit_masks;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_mode_supported_report.bit_mask[i]));
                            }
                            json_object_object_add(jobj, ST_BIT_MASK,jarray);

                            char *thermostatModeSupport = NULL;
                            get_thermostat_mode_from_bit_mask(pTxNotify.CmdClassNotify.thermostat_mode_supported_report.bit_mask,
                                                            pTxNotify.CmdClassNotify.thermostat_mode_supported_report.no_bit_masks,
                                                            &thermostatModeSupport);
                            JSON_ADD_STRING_SAFE(jobj, ST_THERMOSTAT_MODE_SUPPORT, thermostatModeSupport);

                            set_register_database(_VR_CB_(zwave), ID, ST_THERMOSTAT_MODE_SUPPORT, thermostatModeSupport, ST_REPLACE, 0);
                            SAFE_FREE(thermostatModeSupport);
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_THERMOSTAT_FAN_MODE:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case THERMOSTAT_FAN_MODE_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_THERMOSTAT_FAN_MODE));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            json_object_object_add(jobj, ST_OFF, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_fan_mode_report_v2.off));
                            json_object_object_add(jobj, ST_FAN_MODE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_fan_mode_report_v2.fan_mode));

                            if(pTxNotify.CmdClassNotify.thermostat_fan_mode_report_v2.off)
                            {
                                json_object_object_add(jobj, ST_THERMOSTAT_FAN_MODE, json_object_new_string(ST_OFF));

                                set_register_database(_VR_CB_(zwave), ID, ST_THERMOSTAT_FAN_MODE, ST_IDLE, ST_REPLACE, 0);
                            }
                            else
                            {
                                const char *fanMode = get_thermostat_fan_mode_name(pTxNotify.CmdClassNotify.thermostat_fan_mode_report_v2.fan_mode);
                                json_object_object_add(jobj, ST_THERMOSTAT_FAN_MODE, json_object_new_string(fanMode));

                                set_register_database(_VR_CB_(zwave), ID, ST_THERMOSTAT_FAN_MODE, fanMode, ST_REPLACE, 0);
                            }
                            
                        }
                        break;

                        case THERMOSTAT_FAN_MODE_SUPPORTED_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_THERMOSTAT_FAN_MODE));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));
                            
                            json_object_object_add(jobj, ST_NO_BIT_MASKS, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_fan_mode_supported_report.no_bit_masks));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.thermostat_fan_mode_supported_report.no_bit_masks;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_fan_mode_supported_report.bit_mask[i]));
                            }
                            json_object_object_add(jobj, ST_BIT_MASK,jarray);
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_THERMOSTAT_FAN_STATE:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case THERMOSTAT_FAN_STATE_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_THERMOSTAT_FAN_STATE));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            json_object_object_add(jobj, ST_FAN_OPERATING_STATE, json_object_new_int(pTxNotify.CmdClassNotify.thermostat_fan_state_report.fan_operating_state));
                            const char *fanState = get_thermostat_fan_state_name(pTxNotify.CmdClassNotify.thermostat_fan_state_report.fan_operating_state);
                            json_object_object_add(jobj, ST_THERMOSTAT_FAN_STATE, json_object_new_string(fanState));

                            set_register_database(_VR_CB_(zwave), ID, ST_THERMOSTAT_FAN_STATE, fanState, ST_REPLACE, 0);
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_BASIC:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case BASIC_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_BASIC));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            uint8_t value = 0;
                            uint8_t isPoll = pTxNotify.CmdClassNotify.pollingReport;

                            if (pTxNotify.CmdClassNotify.version==1)
                            {
                                sprintf(str_value, "%d", pTxNotify.CmdClassNotify.basic_report_v1.value);
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));

                                value = pTxNotify.CmdClassNotify.basic_report_v1.value;
                            }
                            else if (pTxNotify.CmdClassNotify.version==2)
                            {
                                sprintf(str_value, "%d", pTxNotify.CmdClassNotify.basic_report_v2.value);
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));
                                json_object_object_add(jobj, ST_TARGET_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.basic_report_v2.target_value));
                                json_object_object_add(jobj, ST_DURATION, json_object_new_int(pTxNotify.CmdClassNotify.basic_report_v2.duration));

                                value = pTxNotify.CmdClassNotify.basic_report_v1.value;
                            }

                            unsigned int timestampt = (unsigned int)time(NULL);

                            zwave_dev_info_t *dev = get_zwave_dev_from_id(ID);
                            if(isPoll)
                            {
                                if(check_dev_same_status(dev, value, timestampt))
                                {
                                    //SLOGI("ignore poll %s status with same status %d\n", ID, value!=255?value:1);
                                    sending_notify = 0;
                                    break;
                                }
                            }

                            if(dev)
                            {
                                if((timestampt - dev->status.lastPoll) < 4)
                                {
                                    if(check_dev_same_status(dev, value, timestampt))
                                    {
                                        //SLOGI("ignore poll %s status with same status in 3s %d\n", ID, value!=255?value:1);
                                        sending_notify = 0;
                                        break;
                                    }
                                }

                                dev->status.lastPoll = timestampt; //update for next check
                            }

                            //SLOGI("isPoll %d accept poll %s status with status %d, sending_notify = %d\n", isPoll, ID, value!=255?value:1, sending_notify);

                            if((pTxNotify.CmdClassNotify.version==1) ||
                                (pTxNotify.CmdClassNotify.version==2)
                                )
                            {
                                if(value == 0)
                                {
                                    update_zwave_status(_VR_CB_(zwave), ID, ST_ON_OFF, ST_OFF_VALUE, ST_REPLACE, 0);
                                }
                                else if(value == 0xFF)
                                {
                                    update_zwave_status(_VR_CB_(zwave), ID, ST_ON_OFF, ST_ON_VALUE, ST_REPLACE, 0);
                                }
                                else
                                {
                                    update_zwave_status(_VR_CB_(zwave), ID, ST_ON_OFF, ST_ON_VALUE, ST_REPLACE, 0);
                                    update_zwave_status(_VR_CB_(zwave), ID, ST_DIM, str_value, ST_REPLACE, 0);
                                }
                            }

                            /* Not check signal*/
                            dev_specific_polling_actions(ID, NULL);
                        }
               
                        break;
                        case BASIC_SET:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_BASIC));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SET));
                            sprintf(str_value, "%d", pTxNotify.CmdClassNotify.basic_set.value);
                            json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));
                        }

                        break;
                    }
                }
                break;

                case COMMAND_CLASS_BATTERY:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case BATTERY_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_BATTERY));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            json_object_object_add(jobj, ST_BATTERY_LEVEL, json_object_new_int(pTxNotify.CmdClassNotify.battery_report.battery_level));

                            sprintf(tmp, "%d", pTxNotify.CmdClassNotify.battery_report.battery_level);

                            set_register_database(_VR_CB_(zwave), ID, ST_BATTERY_LEVEL, tmp, ST_REPLACE, 0);
                        }
                        break;
                    }    
                }
                break;

                case COMMAND_CLASS_SWITCH_BINARY:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case SWITCH_BINARY_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SWITCH_BINARY));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));

                            uint8_t tmp_value = 0;
                            if (pTxNotify.CmdClassNotify.version==1)
                            {
                                tmp_value = pTxNotify.CmdClassNotify.switch_binary_report_v1.value;
                            }
                            else if (pTxNotify.CmdClassNotify.version==2)
                            {
                                tmp_value = pTxNotify.CmdClassNotify.switch_binary_report_v2.value;
                                json_object_object_add(jobj, ST_TARGET_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.switch_binary_report_v2.target_value));
                                json_object_object_add(jobj, ST_DURATION, json_object_new_int(pTxNotify.CmdClassNotify.switch_binary_report_v2.duration));
                            }
                            else
                            {
                                break;
                            }

                            tmp_value = invert_status(ID, tmp_value);
                            zwave_dev_info_t *dev = get_zwave_dev_from_id(ID);
                            sprintf(str_value, "%d", tmp_value);
                            json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));

                            if(tmp_value == 0)
                            {
                                if(dev && !strcmp(dev->VRDeviceType, ST_WATER_VALVE))
                                {
                                    if(dev->status.onOff == 1)
                                    {
                                        alarm_voice_inform(NULL, ID, ST_WATER_VALVE_CLOSED);
                                    }
                                }
                                update_zwave_status(_VR_CB_(zwave), ID, ST_ON_OFF, ST_OFF_VALUE, ST_REPLACE, 0);
                            }
                            else if(tmp_value == 0xFF)
                            {
                                if(dev && !strcmp(dev->VRDeviceType, ST_WATER_VALVE))
                                {
                                    if(dev->status.onOff == 0)
                                    {
                                        alarm_voice_inform(NULL, ID, ST_WATER_VALVE_OPEN);
                                    }
                                }
                                update_zwave_status(_VR_CB_(zwave), ID, ST_ON_OFF, ST_ON_VALUE, ST_REPLACE, 0);
                            }
                            else
                            {
                                if(dev && !strcmp(dev->VRDeviceType, ST_WATER_VALVE))
                                {
                                    if(dev->status.onOff == 0)
                                    {
                                        alarm_voice_inform(NULL, ID, ST_WATER_VALVE_OPEN);
                                    }
                                }
                                update_zwave_status(_VR_CB_(zwave), ID, ST_ON_OFF, ST_ON_VALUE, ST_REPLACE, 0);
                                update_zwave_status(_VR_CB_(zwave), ID, ST_DIM, str_value, ST_REPLACE, 0);
                            }

                            //dev_specific_pooling_actions(ID, NULL);
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_TIME_PARAMETERS:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case TIME_PARAMETERS_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_TIME_PARAMETERS));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            json_object_object_add(jobj, ST_YEAR, json_object_new_int(pTxNotify.CmdClassNotify.time_parameters_report.year));
                            json_object_object_add(jobj, ST_MONTH, json_object_new_int(pTxNotify.CmdClassNotify.time_parameters_report.month));
                            json_object_object_add(jobj, ST_DAY, json_object_new_int(pTxNotify.CmdClassNotify.time_parameters_report.day));
                            json_object_object_add(jobj, ST_HOUR_UTC, json_object_new_int(pTxNotify.CmdClassNotify.time_parameters_report.hour_UTC));
                            json_object_object_add(jobj, ST_MINUTE_UTC, json_object_new_int(pTxNotify.CmdClassNotify.time_parameters_report.minute_UTC));
                            json_object_object_add(jobj, ST_SECOND_UTC, json_object_new_int(pTxNotify.CmdClassNotify.time_parameters_report.second_UTC));
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_TIME:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case TIME_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_TIME));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            json_object_object_add(jobj, ST_RTC_FAILURE, json_object_new_int(pTxNotify.CmdClassNotify.time_report.RTC_failure));
                            json_object_object_add(jobj, ST_HOUR_LOCAL_TIME, json_object_new_int(pTxNotify.CmdClassNotify.time_report.hour_local_time));
                            json_object_object_add(jobj, ST_MINUTE_LOCAL_TIME, json_object_new_int(pTxNotify.CmdClassNotify.time_report.minute_local_time));
                            json_object_object_add(jobj, ST_SECOND_LOCAL_TIME, json_object_new_int(pTxNotify.CmdClassNotify.time_report.second_local_time));
                        }
                        break;

                        case DATE_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_TIME));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_DATE_REPORT));
                            
                            json_object_object_add(jobj, ST_YEAR, json_object_new_int(pTxNotify.CmdClassNotify.time_date_report.year));
                            json_object_object_add(jobj, ST_MONTH, json_object_new_int(pTxNotify.CmdClassNotify.time_date_report.month));
                            json_object_object_add(jobj, ST_DAY, json_object_new_int(pTxNotify.CmdClassNotify.time_date_report.day));
                        }
                        break;

                        case TIME_OFFSET_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_TIME));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_OFFSET_REPORT));
                            
                            json_object_object_add(jobj, ST_SIGN_TZO, json_object_new_int(pTxNotify.CmdClassNotify.time_offset_report.sign_TZO));
                            json_object_object_add(jobj, ST_HOUR_TZO, json_object_new_int(pTxNotify.CmdClassNotify.time_offset_report.hour_TZO));
                            json_object_object_add(jobj, ST_MINUTE_TZO, json_object_new_int(pTxNotify.CmdClassNotify.time_offset_report.minute_TZO));
                            json_object_object_add(jobj, ST_SIGN_OFFSET_DST, json_object_new_int(pTxNotify.CmdClassNotify.time_offset_report.sign_offset_DST));
                            json_object_object_add(jobj, ST_MINUTE_OFFSET_DST, json_object_new_int(pTxNotify.CmdClassNotify.time_offset_report.minute_offset_DST));
                            json_object_object_add(jobj, ST_MONTH_START_DST, json_object_new_int(pTxNotify.CmdClassNotify.time_offset_report.month_start_DST));
                            json_object_object_add(jobj, ST_DAY_START_DST, json_object_new_int(pTxNotify.CmdClassNotify.time_offset_report.day_start_DST));
                            json_object_object_add(jobj, ST_HOUR_START_DST, json_object_new_int(pTxNotify.CmdClassNotify.time_offset_report.hour_start_DST));
                            json_object_object_add(jobj, ST_MONTH_END_DST, json_object_new_int(pTxNotify.CmdClassNotify.time_offset_report.month_end_DST));
                            json_object_object_add(jobj, ST_DAY_END_DST, json_object_new_int(pTxNotify.CmdClassNotify.time_offset_report.day_end_DST));
                            json_object_object_add(jobj, ST_HOUR_END_DST, json_object_new_int(pTxNotify.CmdClassNotify.time_offset_report.hour_end_DST));
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_SWITCH_COLOR:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case SWITCH_COLOR_SUPPORTED_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SWITCH_COLOR));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));
                            
                            json_object_object_add(jobj, ST_COLOR_COMPONENT_MASK, json_object_new_int(pTxNotify.CmdClassNotify.color_control_supported_report.color_component_mask));
                            
                        }
                        break;

                        case SWITCH_COLOR_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SWITCH_COLOR));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            if (pTxNotify.CmdClassNotify.version==1)
                            {
                                json_object_object_add(jobj, ST_COLOR_COMPONENT_ID, json_object_new_int(pTxNotify.CmdClassNotify.color_control_report_v1.color_component_id));
                                sprintf(str_value, "%d", pTxNotify.CmdClassNotify.color_control_report_v1.value);
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));            
                            }
                            else if (pTxNotify.CmdClassNotify.version==3)
                            {
                                json_object_object_add(jobj, ST_COLOR_COMPONENT_ID, json_object_new_int(pTxNotify.CmdClassNotify.color_control_report_v3.color_component_id));
                                sprintf(str_value, "%d", pTxNotify.CmdClassNotify.color_control_report_v3.value);
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));
                                
                                json_object_object_add(jobj, ST_TARGET_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.color_control_report_v3.target_value));
                                json_object_object_add(jobj, ST_DURATION, json_object_new_int(pTxNotify.CmdClassNotify.color_control_report_v3.duration));
                            }
                        }
                        break;
                    }
                        
                }
                break;

                case COMMAND_CLASS_SWITCH_MULTILEVEL:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case SWITCH_MULTILEVEL_SUPPORTED_REPORT_V3:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SWITCH_MULTILEVEL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));
                            
                            json_object_object_add(jobj, ST_PRIMARY_SWITCH_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.switch_multilevel_supported_report.primary_switch_type));
                            json_object_object_add(jobj, ST_SECONDARY_SWITCH_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.switch_multilevel_supported_report.secondary_switch_type));
                        }
                        break;

                        case SWITCH_MULTILEVEL_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SWITCH_MULTILEVEL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            uint8_t dimValue = 0;
                            if (pTxNotify.CmdClassNotify.version==1)
                            {
                                sprintf(str_value, "%d", pTxNotify.CmdClassNotify.switch_multilevel_report_v1.value);
                                dimValue = pTxNotify.CmdClassNotify.switch_multilevel_report_v1.value;
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));
                                 
                            }
                            // else if (pTxNotify.CmdClassNotify.version==2)
                            // {
                            //     json_object_object_add(jobj, ST_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.switch_multilevel_report_v2.value));
                            //     json_object_object_add(jobj, ST_DURATION, json_object_new_int(pTxNotify.CmdClassNotify.switch_multilevel_report_v2.duration));

                            // }
                            else if (pTxNotify.CmdClassNotify.version==4)
                            {
                                sprintf(str_value, "%d", pTxNotify.CmdClassNotify.switch_multilevel_report_v3.value);
                                dimValue = pTxNotify.CmdClassNotify.switch_multilevel_report_v3.value;
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));
                                
                                json_object_object_add(jobj, ST_TARGET_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.switch_multilevel_report_v3.target_value));
                                json_object_object_add(jobj, ST_DURATION, json_object_new_int(pTxNotify.CmdClassNotify.switch_multilevel_report_v3.duration));
                            }

                            if(dimValue)
                            {
                                update_zwave_status(_VR_CB_(zwave), ID, ST_ON_OFF, ST_ON_VALUE, ST_REPLACE, 0);
                                update_zwave_status(_VR_CB_(zwave), ID, ST_DIM, str_value, ST_REPLACE, 0);
                            }
                            else
                            {
                                update_zwave_status(_VR_CB_(zwave), ID, ST_ON_OFF, ST_OFF_VALUE, ST_REPLACE, 0);
                            }

                        }
                        break;
                        case SWITCH_MULTILEVEL_SET:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SWITCH_MULTILEVEL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SET));
                            
                            if (pTxNotify.CmdClassNotify.version==1)
                            {
                                sprintf(str_value, "%d", pTxNotify.CmdClassNotify.switch_multilevel_set_v1.value);
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));
                                
                            }else if (pTxNotify.CmdClassNotify.version==2)
                            {
                                sprintf(str_value, "%d", pTxNotify.CmdClassNotify.switch_multilevel_set_v2.value);
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));
                                json_object_object_add(jobj, ST_DURATION, json_object_new_int(pTxNotify.CmdClassNotify.switch_multilevel_set_v2.duration));

                            }
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_METER:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case METER_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_METER));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            if (pTxNotify.CmdClassNotify.version==1)
                            {
                                json_object_object_add(jobj, ST_METER_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v1.meter_type));
                                json_object_object_add(jobj, ST_PRECISION, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v1.precision));
                                json_object_object_add(jobj, ST_SCALE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v1.scale));
                                json_object_object_add(jobj, ST_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v1.size));
                                json_object_object_add(jobj, ST_METER_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v1.meter_value));

                                float electricValue = pTxNotify.CmdClassNotify.meter_report_v1.meter_value/pow(10,pTxNotify.CmdClassNotify.meter_report_v1.precision);
                                int accumulated = 0;
                                const char *meterUnitName = ST_UNKNOWN;
                                const char *meterName = get_meter_name( pTxNotify.CmdClassNotify.meter_report_v1.meter_type, 
                                                                        pTxNotify.CmdClassNotify.meter_report_v1.scale, 
                                                                        &meterUnitName, &accumulated);
                                if(accumulated == 1)
                                {
                                    char *meterLastValue = get_meter_value_database(_VR_CB_(zwave), ID, meterName, meterUnitName);
                                    if(meterLastValue)
                                    {
                                        float lastValue = strtod(meterLastValue, NULL);
                                        electricValue += lastValue;
                                        free(meterLastValue);
                                    }
                                }
                                
                                sprintf(str_value, "%f", electricValue);
                                json_object_object_add(jobj, ST_METER_PATTERN, json_object_new_string(meterName));
                                json_object_object_add(jobj, ST_UNIT, json_object_new_string(meterUnitName));
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));

                                set_meter_value_database(_VR_CB_(zwave), ID, meterName, str_value, meterUnitName);
                            }
                            else if (pTxNotify.CmdClassNotify.version==2)
                            {
                                json_object_object_add(jobj, ST_RATE_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v2.rate_type));
                                json_object_object_add(jobj, ST_METER_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v2.meter_type));
                                json_object_object_add(jobj, ST_PRECISION, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v2.precision));
                                json_object_object_add(jobj, ST_SCALE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v2.scale));
                                json_object_object_add(jobj, ST_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v2.size));
                                json_object_object_add(jobj, ST_METER_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v2.meter_value));
                                json_object_object_add(jobj, ST_DELTA_TIME, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v2.delta_time));
                                json_object_object_add(jobj, ST_PREVIOUS_METER_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v2.previous_meter_value));

                                float electricValue = pTxNotify.CmdClassNotify.meter_report_v2.meter_value/pow(10,pTxNotify.CmdClassNotify.meter_report_v2.precision);
                                float preElectricValue = pTxNotify.CmdClassNotify.meter_report_v2.previous_meter_value/pow(10,pTxNotify.CmdClassNotify.meter_report_v2.precision);

                                int accumulated = 0;
                                const char *meterUnitName = ST_UNKNOWN;
                                const char *meterName = get_meter_name( pTxNotify.CmdClassNotify.meter_report_v2.meter_type, 
                                                                        pTxNotify.CmdClassNotify.meter_report_v2.scale, 
                                                                        &meterUnitName, &accumulated);
                                if(accumulated == 1)
                                {
                                    char *meterLastValue = get_meter_value_database(_VR_CB_(zwave), ID, meterName, meterUnitName);
                                    if(meterLastValue)
                                    {
                                        float lastValue = strtod(meterLastValue, NULL);
                                        electricValue -= preElectricValue;
                                        electricValue += lastValue;
                                        free(meterLastValue);
                                    }
                                }
                                
                                sprintf(str_value, "%f", electricValue);
                                json_object_object_add(jobj, ST_METER_PATTERN, json_object_new_string(meterName));
                                json_object_object_add(jobj, ST_UNIT, json_object_new_string(meterUnitName));
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));

                                set_meter_value_database(_VR_CB_(zwave), ID, meterName, str_value, meterUnitName);
                            }
                            else if (pTxNotify.CmdClassNotify.version==4)
                            {
                                json_object_object_add(jobj, ST_RATE_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v4.rate_type));
                                json_object_object_add(jobj, ST_METER_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v4.meter_type));
                                json_object_object_add(jobj, ST_PRECISION, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v4.precision));
                                json_object_object_add(jobj, ST_SCALE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v4.scale));
                                json_object_object_add(jobj, ST_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v4.size));
                                json_object_object_add(jobj, ST_METER_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v4.meter_value));
                                json_object_object_add(jobj, ST_DELTA_TIME, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v4.delta_time));
                                json_object_object_add(jobj, ST_PREVIOUS_METER_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v4.previous_meter_value));

                                int accumulated = 0;
                                uint8_t scale = pTxNotify.CmdClassNotify.meter_report_v4.scale;
                                if(scale == 0x07)
                                {
                                    scale = pTxNotify.CmdClassNotify.meter_report_v4.scale2;
                                }
                                float electricValue = pTxNotify.CmdClassNotify.meter_report_v4.meter_value/pow(10,pTxNotify.CmdClassNotify.meter_report_v4.precision);
                                float preElectricValue = pTxNotify.CmdClassNotify.meter_report_v4.previous_meter_value/pow(10,pTxNotify.CmdClassNotify.meter_report_v4.precision);

                                const char *meterUnitName = ST_UNKNOWN;
                                const char *meterName = get_meter_name( pTxNotify.CmdClassNotify.meter_report_v4.meter_type, 
                                                                        scale, &meterUnitName, &accumulated);
                                if(accumulated == 1)
                                {
                                    char *meterLastValue = get_meter_value_database(_VR_CB_(zwave), ID, meterName, meterUnitName);
                                    if(meterLastValue)
                                    {
                                        float lastValue = strtod(meterLastValue, NULL);
                                        electricValue -= preElectricValue;
                                        electricValue += lastValue;
                                        free(meterLastValue);
                                    }
                                }
                                
                                sprintf(str_value, "%f", electricValue);
                                json_object_object_add(jobj, ST_METER_PATTERN, json_object_new_string(meterName));
                                json_object_object_add(jobj, ST_UNIT, json_object_new_string(meterUnitName));
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));
                                json_object_object_add(jobj, ST_SCALE_2, json_object_new_int(pTxNotify.CmdClassNotify.meter_report_v4.scale2));

                                set_meter_value_database(_VR_CB_(zwave), ID, meterName, str_value, meterUnitName);
                            }
                        }
                        break;

                        case METER_SUPPORTED_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_METER));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));

                            const char *meterName = NULL;
                            char *meterSupport = NULL;

                            if (pTxNotify.CmdClassNotify.version==2)
                            {
                                json_object_object_add(jobj, ST_METER_RESET, json_object_new_int(pTxNotify.CmdClassNotify.meter_supported_report_v2.meter_reset));
                                json_object_object_add(jobj, ST_METER_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.meter_supported_report_v2.meter_type));
                                json_object_object_add(jobj, ST_SCALE_SUPPORTED, json_object_new_int(pTxNotify.CmdClassNotify.meter_supported_report_v2.scale_supported));
                                meterName = get_meter_scale_from_bit_mask(pTxNotify.CmdClassNotify.meter_supported_report_v2.meter_type, 
                                                           &pTxNotify.CmdClassNotify.meter_supported_report_v2.scale_supported,
                                                           1, &meterSupport);
                                JSON_ADD_STRING_SAFE(jobj, ST_METER_PATTERN, meterName);
                                JSON_ADD_STRING_SAFE(jobj, ST_METER_SUPPORT, meterSupport);
                                if(meterName && meterSupport)
                                {
                                    set_property_database(_VR_CB_(zwave), ID, ST_METER, (char *)meterName,
                                                        ST_METER_SUPPORT, meterSupport);
                                }
                                SAFE_FREE(meterSupport);
                            }
                            else if (pTxNotify.CmdClassNotify.version==4)
                            {
                                json_object_object_add(jobj, ST_METER_RESET, json_object_new_int(pTxNotify.CmdClassNotify.meter_supported_report_v4.meter_reset));
                                json_object_object_add(jobj, ST_RATE_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.meter_supported_report_v4.rate_type));
                                json_object_object_add(jobj, ST_METER_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.meter_supported_report_v4.meter_type));
                                json_object_object_add(jobj, ST_MORE_SCALE_TYPES, json_object_new_int(pTxNotify.CmdClassNotify.meter_supported_report_v4.more_scale_types));
                                int i;
                                json_object *jarray=json_object_new_array();
                                json_object_object_add(jobj, ST_NO_BIT_MASKS, json_object_new_int(pTxNotify.CmdClassNotify.meter_supported_report_v4.no_bit_masks));
                                for (i=0;i<pTxNotify.CmdClassNotify.meter_supported_report_v4.no_bit_masks;i++)
                                {
                                    json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.meter_supported_report_v4.bit_mask[i]));
                                }
                                json_object_object_add(jobj, ST_BIT_MASK,jarray);

                                meterName = get_meter_scale_from_bit_mask(pTxNotify.CmdClassNotify.meter_supported_report_v4.meter_type, 
                                                                   pTxNotify.CmdClassNotify.meter_supported_report_v4.bit_mask,
                                                                   pTxNotify.CmdClassNotify.meter_supported_report_v4.no_bit_masks,
                                                                   &meterSupport);
                                JSON_ADD_STRING_SAFE(jobj, ST_METER_PATTERN, meterName);
                                JSON_ADD_STRING_SAFE(jobj, ST_METER_SUPPORT, meterSupport);
                                if(meterName && meterSupport)
                                {
                                    set_property_database(_VR_CB_(zwave), ID, ST_METER, (char *)meterName,
                                                        ST_METER_SUPPORT, meterSupport);
                                }
                                SAFE_FREE(meterSupport);
                            }
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_MANUFACTURER_SPECIFIC:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case MANUFACTURER_SPECIFIC_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_MANUFACTURER_SPECIFIC));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            json_object_object_add(jobj, ST_MANUFACTURER_ID, json_object_new_int(pTxNotify.CmdClassNotify.manufacturer_specific_report.manufacturer_id));
                            json_object_object_add(jobj, ST_PRODUCT_TYPE_ID, json_object_new_int(pTxNotify.CmdClassNotify.manufacturer_specific_report.product_type_id));
                            json_object_object_add(jobj, ST_PRODUCT_ID, json_object_new_int(pTxNotify.CmdClassNotify.manufacturer_specific_report.product_id));
                        }
                        break;

                        case DEVICE_SPECIFIC_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_MANUFACTURER_SPECIFIC));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_DEVICE_SPECIFIC_REPORT));
                            
                            json_object_object_add(jobj, ST_DEVICE_ID_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.device_specific_report.device_id_type));
                            json_object_object_add(jobj, ST_DEVICE_ID_DATA_FORMAT, json_object_new_int(pTxNotify.CmdClassNotify.device_specific_report.device_id_data_format));
                            json_object_object_add(jobj, ST_DEVICE_ID_DATA_LENGTH, json_object_new_int(pTxNotify.CmdClassNotify.device_specific_report.device_id_data_length));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.device_specific_report.device_id_data_length;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.device_specific_report.device_id_data[i]));
                            }
                            json_object_object_add(jobj, ST_DEVICE_ID_DATA,jarray);
                            
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_MULTI_CHANNEL_V3:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case MULTI_CHANNEL_END_POINT_REPORT_V3:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_MULTI_CHANNEL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_END_POINT_REPORT));
                            
                            if (pTxNotify.CmdClassNotify.version == 3)
                            {
                                json_object_object_add(jobj, ST_DYNAMIC, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_report.dynamic));
                                json_object_object_add(jobj, ST_IDENTICAL, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_report.identical));
                                json_object_object_add(jobj, ST_NO_ENDPOINT, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_report.no_endpoint));
                            }
                            else if (pTxNotify.CmdClassNotify.version == 4)
                            {
                                json_object_object_add(jobj, ST_DYNAMIC, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_endpoint_report_v4.dynamic));
                                json_object_object_add(jobj, ST_IDENTICAL,
                                                       json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_endpoint_report_v4.identical));
                                json_object_object_add(jobj, ST_INDIVIDUAL_END_POINTS,
                                                       json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_endpoint_report_v4.individual_end_points));
                                json_object_object_add(jobj, ST_AGGREGATED_END_POINTS,
                                                       json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_endpoint_report_v4.aggregated_end_points));
                            }
                        }
                        break;

                        case MULTI_CHANNEL_CAPABILITY_REPORT_V3:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_MULTI_CHANNEL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_CAPABILITY_REPORT));
                            
                            json_object_object_add(jobj, ST_DYNAMIC, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_capability_report.dynamic));
                            json_object_object_add(jobj, ST_ENDPOINT, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_capability_report.endpoint));
                            json_object_object_add(jobj, ST_GENERIC_DEVICE_CLASS, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_capability_report.generic_device_class));
                            json_object_object_add(jobj, ST_SPECIFIC_DEVICE_CLASS, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_capability_report.specific_device_class));
                            json_object_object_add(jobj, ST_NO_CMD_CLASS, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_capability_report.no_cmd_class));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.multi_channel_capability_report.no_cmd_class;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_capability_report.cmd_class_list[i]));
                            }
                            json_object_object_add(jobj, ST_CMD_CLASS_LIST,jarray);
                            
                        }
                        break;

                        case MULTI_CHANNEL_END_POINT_FIND_REPORT_V3:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_MULTI_CHANNEL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_END_POINT_FIND_REPORT));
                            
                            json_object_object_add(jobj, ST_REPORT_TO_FOLLOW, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_endpoint_find_report.report_to_follow));
                            json_object_object_add(jobj, ST_GENERIC_DEVICE_CLASS, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_endpoint_find_report.generic_device_class));
                            json_object_object_add(jobj, ST_SPECIFIC_DEVICE_CLASS, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_endpoint_find_report.specific_device_class));
                            json_object_object_add(jobj, ST_NO_ENDPOINT, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_endpoint_find_report.no_endpoint));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.multi_channel_endpoint_find_report.no_endpoint;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_endpoint_find_report.endpoint_list[i]));
                            }
                            json_object_object_add(jobj, ST_ENDPOINT_LIST,jarray);
                        }
                        break;

                        case MULTI_CHANNEL_AGGREGATED_MEMBERS_REPORT_V4:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_MULTI_CHANNEL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_AGGREGATED_MEMBERS_REPORT));

                            json_object_object_add(jobj, ST_AGGREGATED_END_POINT,
                                                   json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_aggregated_members_report_v4.aggregated_end_point));
                            json_object_object_add(jobj, ST_NO_BIT_MASKS,
                                                   json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_aggregated_members_report_v4.no_bit_masks));
                            int i;
                            json_object *jarray = json_object_new_array();
                            for (i = 0; i < pTxNotify.CmdClassNotify.multi_channel_aggregated_members_report_v4.no_bit_masks; i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.multi_channel_aggregated_members_report_v4.bit_mask[i]));
                            }
                            json_object_object_add(jobj, ST_BIT_MASK, jarray);
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_POWERLEVEL:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case POWERLEVEL_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_POWER_LEVEL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            json_object_object_add(jobj, ST_POWER_LEVEL, json_object_new_int(pTxNotify.CmdClassNotify.powerlevel_report.powerlevel));
                            json_object_object_add(jobj, ST_TIMEOUT, json_object_new_int(pTxNotify.CmdClassNotify.powerlevel_report.timeout));
                        }
                        break;

                        case POWERLEVEL_TEST_NODE_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_POWER_LEVEL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_TEST_NODE_REPORT));
                            
                            json_object_object_add(jobj, ST_TEST_NODE_ID, json_object_new_int(pTxNotify.CmdClassNotify.powerlevel_test_node_report.test_node_id));
                            json_object_object_add(jobj, ST_STATUS_OF_OPERATION, json_object_new_int(pTxNotify.CmdClassNotify.powerlevel_test_node_report.status_of_operation));
                            json_object_object_add(jobj, ST_TEST_FRAME_ACK_COUNT, json_object_new_int(pTxNotify.CmdClassNotify.powerlevel_test_node_report.test_frame_ack_count));
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_VERSION:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case VERSION_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_VERSION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            if (pTxNotify.CmdClassNotify.version==1)
                            {
                                json_object_object_add(jobj, ST_ZW_LIB_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.version_report_v1.zw_lib_type));
                                json_object_object_add(jobj, ST_ZW_PROTOCOL_VERSION, json_object_new_int(pTxNotify.CmdClassNotify.version_report_v1.zw_protocol_version));
                                json_object_object_add(jobj, ST_ZW_PROTOCOL_SUB_VERSION, json_object_new_int(pTxNotify.CmdClassNotify.version_report_v1.zw_protocol_sub_version));
                                json_object_object_add(jobj, ST_APP_VERSION, json_object_new_int(pTxNotify.CmdClassNotify.version_report_v1.app_version));
                                json_object_object_add(jobj, ST_APP_SUB_VERSION, json_object_new_int(pTxNotify.CmdClassNotify.version_report_v1.app_sub_version));
                                
                            }else if (pTxNotify.CmdClassNotify.version==2)
                            {
                                json_object_object_add(jobj, ST_ZW_LIB_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.version_report_v2.zw_lib_type));
                                json_object_object_add(jobj, ST_ZW_PROTOCOL_VERSION, json_object_new_int(pTxNotify.CmdClassNotify.version_report_v2.zw_protocol_version));
                                json_object_object_add(jobj, ST_ZW_PROTOCOL_SUB_VERSION, json_object_new_int(pTxNotify.CmdClassNotify.version_report_v2.zw_protocol_sub_version));
                                json_object_object_add(jobj, ST_FIRMWARE_0_VERSION, json_object_new_int(pTxNotify.CmdClassNotify.version_report_v2.firmware0_version));
                                json_object_object_add(jobj, ST_FIRMWARE0_SUB_VERSION, json_object_new_int(pTxNotify.CmdClassNotify.version_report_v2.firmware0_sub_version));
                                json_object_object_add(jobj, ST_HARDWARE_VERSION, json_object_new_int(pTxNotify.CmdClassNotify.version_report_v2.hardware_version));
                                json_object_object_add(jobj, ST_NO_FIRMWARE_TARGETS, json_object_new_int(pTxNotify.CmdClassNotify.version_report_v2.no_firmware_targets));
                                int i;
                                json_object *jarray=json_object_new_array();
                                for (i=0;i<pTxNotify.CmdClassNotify.version_report_v2.no_firmware_targets;i++)
                                {
                                    json_object *jlist=json_object_new_object();
                                    json_object_object_add(jlist, ST_FIRMWARE_VERSION, json_object_new_int(pTxNotify.CmdClassNotify.version_report_v2.fw_target_list[i].firmware_version));
                                    json_object_object_add(jlist, ST_FIRMWARE_SUB_VERSION, json_object_new_int(pTxNotify.CmdClassNotify.version_report_v2.fw_target_list[i].firmware_sub_version));

                                    json_object_array_add(jarray,jlist);
                                }
                                json_object_object_add(jobj, ST_FW_TARGET_LIST,jarray);
                            }
                        }
                        break;

                        case VERSION_COMMAND_CLASS_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_VERSION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_COMMAND_CLASS_REPORT));
                            
                            json_object_object_add(jobj, ST_REQUEST_CMD_CLASS, json_object_new_int(pTxNotify.CmdClassNotify.version_cmd_class_report.request_cmd_class));
                            json_object_object_add(jobj, ST_CMD_CLASS_VERSION, json_object_new_int(pTxNotify.CmdClassNotify.version_cmd_class_report.cmd_class_version));
                            
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_PROTECTION:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case PROTECTION_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_PROTECTION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            if (pTxNotify.CmdClassNotify.version==1)
                            {
                                json_object_object_add(jobj, ST_PROTECTION_STATE, json_object_new_int(pTxNotify.CmdClassNotify.protection_report_v1.protection_state));
                                
                            }else if (pTxNotify.CmdClassNotify.version==2)
                            {
                                json_object_object_add(jobj, ST_LOCAL_PROTECTION_STATE, json_object_new_int(pTxNotify.CmdClassNotify.protection_report_v2.local_protection_state));
                                json_object_object_add(jobj, ST_RF_PROTECTION_STATE, json_object_new_int(pTxNotify.CmdClassNotify.protection_report_v2.rf_protection_state));
                            }
                        }
                        break;

                        case PROTECTION_SUPPORTED_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_PROTECTION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));
                            
                            json_object_object_add(jobj, ST_EXCLUSIVE_CONTROL, json_object_new_int(pTxNotify.CmdClassNotify.protection_supported_report.exclusive_control));
                            json_object_object_add(jobj, ST_TIMEOUT, json_object_new_int(pTxNotify.CmdClassNotify.protection_supported_report.timeout));
                            json_object_object_add(jobj, ST_LOCAL_PROTECTION_STATE, json_object_new_int(pTxNotify.CmdClassNotify.protection_supported_report.local_protection_state));
                            json_object_object_add(jobj, ST_RF_PROTECTION_STATE, json_object_new_int(pTxNotify.CmdClassNotify.protection_supported_report.rf_protection_state));
                        }
                        break;

                        case PROTECTION_EC_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_PROTECTION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_EC_REPORT));
                            
                            json_object_object_add(jobj, ST_NODE_ID, json_object_new_int(pTxNotify.CmdClassNotify.protection_ec_report.node_id));
                        }
                        break;

                        case PROTECTION_TIMEOUT_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_PROTECTION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_TIMEOUT_REPORT));
                            
                            json_object_object_add(jobj, ST_TIMEOUT, json_object_new_int(pTxNotify.CmdClassNotify.protection_timeout_report.timeout));
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_CONFIGURATION:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case CONFIGURATION_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_CONFIGURATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            json_object_object_add(jobj, ST_PARAMETER_NUMBER, json_object_new_int(pTxNotify.CmdClassNotify.configuration_report.parameter_number));
                            json_object_object_add(jobj, ST_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.configuration_report.size));
                            json_object_object_add(jobj, ST_CONFIGURATION_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.configuration_report.configuration_value));

                            char paramName[SIZE_32B] ={0};
                            if(!get_configuration_data_from_id(ID, pTxNotify.CmdClassNotify.configuration_report.parameter_number, paramName))
                            {
                                if(strlen(paramName))
                                {
                                    char paramValue[SIZE_32B] ={0};
                                    sprintf(paramValue, "%d", pTxNotify.CmdClassNotify.configuration_report.configuration_value);
                                    json_object_object_add(jobj, ST_CONFIG_NAME, json_object_new_string(paramName));

                                    set_register_database(_VR_CB_(zwave), ID, paramName, paramValue, ST_REPLACE, 0);
                                }
                            }
                        }
                        break;

                        case CONFIGURATION_BULK_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_CONFIGURATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_BULK_REPORT));
                            
                            json_object_object_add(jobj, ST_PARAMETER_OFFSET, json_object_new_int(pTxNotify.CmdClassNotify.configuration_bulk_report.parameter_offset));
                            json_object_object_add(jobj, ST_NO_PARAMS, json_object_new_int(pTxNotify.CmdClassNotify.configuration_bulk_report.no_params));
                            json_object_object_add(jobj, ST_REPORT_TO_FOLLOW, json_object_new_int(pTxNotify.CmdClassNotify.configuration_bulk_report.report_to_follow));
                            json_object_object_add(jobj, ST_DEFAULT, json_object_new_int(pTxNotify.CmdClassNotify.configuration_bulk_report.default_value));
                            json_object_object_add(jobj, ST_HANDSHAKE, json_object_new_int(pTxNotify.CmdClassNotify.configuration_bulk_report.handshake));
                            json_object_object_add(jobj, ST_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.configuration_bulk_report.size));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.configuration_bulk_report.no_params;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.configuration_bulk_report.params_list[i]));
                            }
                            json_object_object_add(jobj, ST_PARAMS_LIST,jarray);
                        }
                        break;

                        case CONFIGURATION_NAME_REPORT_V3:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_CONFIGURATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_NAME_REPORT));
                            
                            json_object_object_add(jobj, ST_PARAMETER_NUMBER, json_object_new_int(pTxNotify.CmdClassNotify.configuration_name_report.parameter_number));
                            json_object_object_add(jobj, ST_REPORT_TO_FOLLOW, json_object_new_int(pTxNotify.CmdClassNotify.configuration_name_report.report_to_follow));
                            json_object_object_add(jobj, ST_NAME_LEN, json_object_new_int(pTxNotify.CmdClassNotify.configuration_name_report.name_len));
                            json_object_object_add(jobj, ST_NAME, json_object_new_string(pTxNotify.CmdClassNotify.configuration_name_report.name));
                        }
                        break;

                        case CONFIGURATION_INFO_REPORT_V3:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_CONFIGURATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_INFO_REPORT));
                            
                            json_object_object_add(jobj, ST_PARAMETER_NUMBER, json_object_new_int(pTxNotify.CmdClassNotify.configuration_info_report.parameter_number));
                            json_object_object_add(jobj, ST_REPORT_TO_FOLLOW, json_object_new_int(pTxNotify.CmdClassNotify.configuration_info_report.report_to_follow));
                            json_object_object_add(jobj, ST_INFO_LEN, json_object_new_int(pTxNotify.CmdClassNotify.configuration_info_report.info_len));
                            json_object_object_add(jobj, ST_INFO, json_object_new_string(pTxNotify.CmdClassNotify.configuration_info_report.info));
                        }
                        break;

                        case CONFIGURATION_PROPERTIES_REPORT_V3:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_CONFIGURATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_PROPERTIES_REPORT));
                            
                            json_object_object_add(jobj, ST_PARAMETER_NUMBER, json_object_new_int(pTxNotify.CmdClassNotify.configuration_properties_report.parameter_number));
                            json_object_object_add(jobj, ST_FORMAT, json_object_new_int(pTxNotify.CmdClassNotify.configuration_properties_report.format));
                            json_object_object_add(jobj, ST_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.configuration_properties_report.size));
                            json_object_object_add(jobj, ST_MIN_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.configuration_properties_report.min_value));
                            json_object_object_add(jobj, ST_MAX_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.configuration_properties_report.max_value));
                            json_object_object_add(jobj, ST_DEFAULT_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.configuration_properties_report.default_value));
                            json_object_object_add(jobj, ST_NEXT_PARAMETER_NUMBER, json_object_new_int(pTxNotify.CmdClassNotify.configuration_properties_report.next_parameter_number));
                        }
                        break;
                    }
                        
                }
                break;

                case COMMAND_CLASS_DOOR_LOCK:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case DOOR_LOCK_OPERATION_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_DOOR_LOCK));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            uint8_t lockStatus = 0;
                            const char *alarm_type = ST_UNKNOWN;

                            if (pTxNotify.CmdClassNotify.version==1)
                            {
                                json_object_object_add(jobj, ST_DOOR_LOCK_MODE, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_operation_report_v1.door_lock_mode));
                                json_object_object_add(jobj, ST_OUTSIDE_DOOR_HANDLES_MODE, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_operation_report_v1.outside_door_handles_mode));
                                json_object_object_add(jobj, ST_INSIDE_DOOR_HANDLES_MODE, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_operation_report_v1.inside_door_handles_mode));
                                json_object_object_add(jobj, ST_DOOR_CONDITION, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_operation_report_v1.door_condition));
                                json_object_object_add(jobj, ST_LOCK_TIMEOUT_MINUTES, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_operation_report_v1.lock_timeout_minutes));
                                json_object_object_add(jobj, ST_LOCK_TIMEOUT_SECONDS, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_operation_report_v1.lock_timeout_seconds));
                                lockStatus = pTxNotify.CmdClassNotify.door_lock_operation_report_v1.door_lock_mode;
                            }
                            else if (pTxNotify.CmdClassNotify.version==3)
                            {
                                json_object_object_add(jobj, ST_DOOR_LOCK_MODE, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_operation_report_v3.door_lock_mode));
                                json_object_object_add(jobj, ST_OUTSIDE_DOOR_HANDLES_MODE, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_operation_report_v3.outside_door_handles_mode));
                                json_object_object_add(jobj, ST_INSIDE_DOOR_HANDLES_MODE, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_operation_report_v3.inside_door_handles_mode));
                                json_object_object_add(jobj, ST_DOOR_CONDITION, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_operation_report_v3.door_condition));
                                json_object_object_add(jobj, ST_LOCK_TIMEOUT_MINUTES, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_operation_report_v3.lock_timeout_minutes));
                                json_object_object_add(jobj, ST_LOCK_TIMEOUT_SECONDS, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_operation_report_v3.lock_timeout_seconds));
                                json_object_object_add(jobj, ST_TARGET_DOOR_LOCK_MODE, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_operation_report_v3.target_door_lock_mode));
                                json_object_object_add(jobj, ST_DURATION, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_operation_report_v3.duration));
                                lockStatus = pTxNotify.CmdClassNotify.door_lock_operation_report_v3.door_lock_mode;

                            }

                            if((pTxNotify.CmdClassNotify.version==1) || (pTxNotify.CmdClassNotify.version==3))
                            {
                                if(lockStatus == DOORLOCK_OPEN_VALUE)
                                {
                                    alarm_type = ST_DOOR_RF_UNLOCKED;
                                    lockStatus = OPEN;
                                }
                                else
                                {
                                    alarm_type = ST_DOOR_RF_LOCKED;
                                    lockStatus = CLOSE;
                                }

                                char stValue[SIZE_32B];
                                sprintf(stValue, "%d", lockStatus);
                                zwave_dev_info_t *dev = get_zwave_dev_from_id(ID);
                                if(dev)
                                {
                                    if(dev->status.onOff != lockStatus)
                                    {
                                        alarm_voice_inform(ctx, ID, (char*)alarm_type);
                                    }
                                }
                                else
                                {
                                    alarm_voice_inform(ctx, ID, (char*)alarm_type);
                                }

                                json_object_object_add(jobj, ST_LOCK_STATUS, json_object_new_int(lockStatus));
                                update_zwave_status(_VR_CB_(zwave), ID, ST_ON_OFF, stValue, ST_REPLACE, 0);
                            }
                        }
                        break;

                        case DOOR_LOCK_CONFIGURATION_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_DOOR_LOCK));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_CONFIGURATION_REPORT));
                            
                            json_object_object_add(jobj, ST_OPERATION_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_configuration_report.operation_type));
                            json_object_object_add(jobj, ST_OUTSIDE_DOOR_HANDLES_MODE, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_configuration_report.outside_door_handles_mode));
                            json_object_object_add(jobj, ST_INSIDE_DOOR_HANDLES_MODE, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_configuration_report.inside_door_handles_mode));
                            json_object_object_add(jobj, ST_LOCK_TIMEOUT_MINUTES, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_configuration_report.lock_timeout_minutes));
                            json_object_object_add(jobj, ST_LOCK_TIMEOUT_SECONDS, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_configuration_report.lock_timeout_seconds));
                            
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_DOOR_LOCK_LOGGING:
                {
                    switch (pTxNotify.CmdClassNotify.cmd)
                    {
                        case DOOR_LOCK_LOGGING_RECORDS_SUPPORTED_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_DOOR_LOCK_LOGGING));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_RECORDS_SUPPORTED_REPORT));
                            json_object_object_add(jobj, ST_MAX_RECORDS_STORED,
                                                   json_object_new_int(pTxNotify.CmdClassNotify.door_lock_logging_records_supported_report.max_records_stored));
                        }
                        break;
                        case RECORD_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_DOOR_LOCK_LOGGING));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_RECORD_REPORT));
                            json_object_object_add(jobj, ST_RECORD_NUMBER,
                                                   json_object_new_int(pTxNotify.CmdClassNotify.door_lock_logging_record_report.record_number));
                            json_object_object_add(jobj, ST_TIMESTAMP_YEAR,
                                                   json_object_new_int(pTxNotify.CmdClassNotify.door_lock_logging_record_report.timestamp_year));
                            json_object_object_add(jobj, ST_TIMESTAMP_MONTH,
                                                   json_object_new_int(pTxNotify.CmdClassNotify.door_lock_logging_record_report.timestamp_month));
                            json_object_object_add(jobj, ST_TIMESTAMP_DAY,
                                                   json_object_new_int(pTxNotify.CmdClassNotify.door_lock_logging_record_report.timestamp_day));
                            json_object_object_add(jobj, ST_RECORD_STATUS,
                                                   json_object_new_int(pTxNotify.CmdClassNotify.door_lock_logging_record_report.record_status));
                            json_object_object_add(jobj, ST_TIMESTAMP_HOUR_LOCAL_TIME,
                                                   json_object_new_int(pTxNotify.CmdClassNotify.door_lock_logging_record_report.timestamp_hour_local_time));
                            json_object_object_add(jobj, ST_TIMESTAMP_MINUTE_LOCAL_TIME,
                                                   json_object_new_int(pTxNotify.CmdClassNotify.door_lock_logging_record_report.timestamp_minute_local_time));
                            json_object_object_add(jobj, ST_TIMESTAMP_SECOND_LOCAL_TIME,
                                                   json_object_new_int(pTxNotify.CmdClassNotify.door_lock_logging_record_report.timestamp_second_local_time));
                            json_object_object_add(jobj, ST_EVENT_TYPE,
                                                   json_object_new_int(pTxNotify.CmdClassNotify.door_lock_logging_record_report.event_type));
                            json_object_object_add(jobj, ST_USER_IDENTIFIER,
                                                   json_object_new_int(pTxNotify.CmdClassNotify.door_lock_logging_record_report.user_identifier));
                            json_object_object_add(jobj, ST_USER_CODE_LENGTH,
                                                   json_object_new_int(pTxNotify.CmdClassNotify.door_lock_logging_record_report.user_code_length));
                            int i;
                            json_object *jarray = json_object_new_array();
                            for (i = 0; i < pTxNotify.CmdClassNotify.door_lock_logging_record_report.user_code_length; i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.door_lock_logging_record_report.user_code[i]));
                            }
                            json_object_object_add(jobj, ST_USER_CODE, jarray);
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_SCHEDULE_ENTRY_LOCK:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case SCHEDULE_ENTRY_TYPE_SUPPORTED_REPORT :                       {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SCHEDULE_ENTRY_LOCK));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_TYPE_SUPPORTED_REPORT));

                            if (pTxNotify.CmdClassNotify.version==1)
                            {
                                json_object_object_add(jobj, ST_NO_SLOTS_WEEK_DAY, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_type_supported_report_v1.no_slots_week_day));
                                json_object_object_add(jobj, ST_NO_SLOTS_YEAR_DAY, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_type_supported_report_v1.no_slots_year_day));
                                
                            }else if (pTxNotify.CmdClassNotify.version==3)
                            {
                                json_object_object_add(jobj, ST_NO_SLOTS_WEEK_DAY, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_type_supported_report_v3.no_slots_week_day));
                                json_object_object_add(jobj, ST_NO_SLOTS_YEAR_DAY, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_type_supported_report_v3.no_slots_year_day));
                                json_object_object_add(jobj, ST_NO_SLOTS_DAILY_REPEATING, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_type_supported_report_v3.no_slots_daily_repeating));
                            }
                        }
                        break;

                        case SCHEDULE_ENTRY_LOCK_WEEK_DAY_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SCHEDULE_ENTRY_LOCK));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_WEEK_DAY_REPORT));
                            
                            json_object_object_add(jobj, ST_USER_ID, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_week_day_report.user_id));
                            json_object_object_add(jobj, ST_SCHEDULE_SLOT_ID, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_week_day_report.schedule_slot_id));
                            json_object_object_add(jobj, ST_DAY_OF_WEEK, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_week_day_report.day_of_week));
                            json_object_object_add(jobj, ST_START_HOUR, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_week_day_report.start_hour));
                            json_object_object_add(jobj, ST_START_MINUTE, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_week_day_report.start_minute));
                            json_object_object_add(jobj, ST_STOP_HOUR, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_week_day_report.stop_hour));
                            json_object_object_add(jobj, ST_STOP_MINUTE, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_week_day_report.stop_minute));
                        }
                        break;

                        case SCHEDULE_ENTRY_LOCK_YEAR_DAY_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SCHEDULE_ENTRY_LOCK));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_YEAR_DAY_REPORT));
                            
                            json_object_object_add(jobj, ST_USER_ID, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.user_id));
                            json_object_object_add(jobj, ST_SCHEDULE_SLOT_ID, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.schedule_slot_id));
                            json_object_object_add(jobj, ST_START_YEAR, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.start_year));
                            json_object_object_add(jobj, ST_START_MONTH, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.start_month));
                            json_object_object_add(jobj, ST_START_DAY, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.start_day));
                            json_object_object_add(jobj, ST_START_HOUR, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.start_hour));
                            json_object_object_add(jobj, ST_START_MINUTE, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.start_minute));
                            json_object_object_add(jobj, ST_STOP_YEAR, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.stop_year));
                            json_object_object_add(jobj, ST_STOP_MONTH, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.stop_month));
                            json_object_object_add(jobj, ST_STOP_DAY, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.stop_day));
                            json_object_object_add(jobj, ST_STOP_HOUR, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.stop_hour));
                            json_object_object_add(jobj, ST_STOP_MINUTE, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.stop_minute));
                        }
                        break;

                        case SCHEDULE_ENTRY_LOCK_TIME_OFFSET_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SCHEDULE_ENTRY_LOCK));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_TIME_OFFSET_REPORT));
                            
                            json_object_object_add(jobj, ST_SIGN_TZO, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_time_offset_report.sign_TZO));
                            json_object_object_add(jobj, ST_HOUR_TZO, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_time_offset_report.hour_TZO));
                            json_object_object_add(jobj, ST_MINUTE_TZO, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_time_offset_report.minute_TZO));
                            json_object_object_add(jobj, ST_SIGN_OFFSET_DST, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_time_offset_report.sign_offset_DST));
                            json_object_object_add(jobj, ST_MINUTE_OFFSET_DST, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_time_offset_report.minute_offset_DST));
                        }
                        break;

                        case SCHEDULE_ENTRY_LOCK_DAILY_REPEATING_REPORT_V3:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SCHEDULE_ENTRY_LOCK));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_DAILY_REPEATING_REPORT));
                            
                            json_object_object_add(jobj, ST_USER_ID, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_daily_repeating_report.user_id));
                            json_object_object_add(jobj, ST_SCHEDULE_SLOT_ID, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_daily_repeating_report.schedule_slot_id));
                            json_object_object_add(jobj, ST_WEEK_DAY_BIT_MASK, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_daily_repeating_report.week_day_bitmask));
                            json_object_object_add(jobj, ST_START_HOUR, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_daily_repeating_report.start_hour));
                            json_object_object_add(jobj, ST_START_MINUTE, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_daily_repeating_report.start_minute));
                            json_object_object_add(jobj, ST_DURATION_HOUR, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_daily_repeating_report.duration_hour));
                            json_object_object_add(jobj, ST_DURATION_MINUTE, json_object_new_int(pTxNotify.CmdClassNotify.schedule_entry_lock_daily_repeating_report.duration_minute));
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_BARRIER_OPERATOR:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case BARRIER_OPERATOR_REPORT:
                        {
                            const char *alarm_type = ST_UNKNOWN;

                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_BARRIER_OPERATOR));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            json_object_object_add(jobj, ST_STATE, json_object_new_int(pTxNotify.CmdClassNotify.barrier_operator_report.state));
                            int barrierStatus = pTxNotify.CmdClassNotify.barrier_operator_report.state;

                            switch(barrierStatus)
                            {
                                case 0xFF:
                                    barrierStatus = 1;
                                    alarm_type = ST_GARA_OPEN;
                                    break;
                                case 254:
                                    alarm_type = ST_GARA_OPENING;
                                    break;
                                case 252:
                                    alarm_type = ST_GARA_CLOSING;
                                    break;
                                case 0:
                                    alarm_type = ST_GARA_CLOSED;
                                    break;
                            }

                            zwave_dev_info_t *dev = get_zwave_dev_from_id(ID);
                            if(dev)
                            {
                                if(dev->status.onOff != barrierStatus)
                                {
                                    alarm_voice_inform(ctx, ID, (char*)alarm_type);
                                }
                            }
                            else
                            {
                                alarm_voice_inform(ctx, ID, (char*)alarm_type);
                            }

                            char stValue[SIZE_32B];
                            sprintf(stValue, "%d", barrierStatus);
                            json_object_object_add(jobj, ST_BARRIER_STATUS, json_object_new_int(barrierStatus));

                            update_zwave_status(_VR_CB_(zwave), ID, ST_ON_OFF, stValue, ST_REPLACE, 0);
                        }
                        break;

                        case BARRIER_OPERATOR_SIGNAL_SUPPORTED_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_BARRIER_OPERATOR));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SIGNAL_SUPPORTED_REPORT));
                            int i;
                            json_object *jarray=json_object_new_array();
                            json_object_object_add(jobj, ST_NO_BIT_MASKS, json_object_new_int(pTxNotify.CmdClassNotify.barrier_operator_signal_supported_report.no_bit_masks));
                            for (i=0;i<pTxNotify.CmdClassNotify.barrier_operator_signal_supported_report.no_bit_masks;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.barrier_operator_signal_supported_report.bit_mask[i]));
                            }
                            json_object_object_add(jobj, ST_BIT_MASK,jarray);

                            char *barrierSignalSupport = NULL;
                            get_barrier_signal_support_from_bit_mask(pTxNotify.CmdClassNotify.barrier_operator_signal_supported_report.bit_mask,
                                                                    pTxNotify.CmdClassNotify.barrier_operator_signal_supported_report.no_bit_masks,
                                                                    &barrierSignalSupport);

                            JSON_ADD_STRING_SAFE(jobj, ST_SIGNAL_SUPPORTED, barrierSignalSupport);
                            if(barrierSignalSupport)
                            {
                                set_register_database(_VR_CB_(zwave), ID, ST_SIGNAL_SUPPORTED, barrierSignalSupport, ST_REPLACE, 0);
                                free(barrierSignalSupport);
                            }

                        }
                        break;

                        case BARRIER_OPERATOR_SIGNAL_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_BARRIER_OPERATOR));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SIGNAL_REPORT));
                            
                            const char *signalName = get_barrier_signal_name(pTxNotify.CmdClassNotify.barrier_operator_signal_report.subsystem_type);
                            json_object_object_add(jobj, ST_SUBSYSTEM_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.barrier_operator_signal_report.subsystem_type));
                            json_object_object_add(jobj, ST_SUBSYSTEM_NAME, json_object_new_string(signalName));
                            json_object_object_add(jobj, ST_SUBSYSTEM_STATE, json_object_new_int(pTxNotify.CmdClassNotify.barrier_operator_signal_report.subsystem_state));

                            sprintf(str_value, "%d", pTxNotify.CmdClassNotify.barrier_operator_signal_report.subsystem_state);
                            set_register_database(_VR_CB_(zwave), ID, signalName, str_value, ST_REPLACE, 0);
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_IRRIGATION:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case IRRIGATION_SYSTEM_INFO_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_IRRIGATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SYSTEM_INFO_REPORT));
                            
                            json_object_object_add(jobj, ST_MASTER_VALVE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_info_report.master_valve));
                            json_object_object_add(jobj, ST_TOTAL_NO_VALVES, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_info_report.total_no_valves));
                            json_object_object_add(jobj, ST_TOTAL_NO_VALVE_TABLES, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_info_report.total_no_valve_tables));
                            json_object_object_add(jobj, ST_VALVE_TABLE_MAX_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_info_report.valve_table_max_size));
                        }
                        break;

                        case IRRIGATION_SYSTEM_STATUS_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_IRRIGATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SYSTEM_STATUS_REPORT));
                            
                            json_object_object_add(jobj, ST_SYSTEM_VOLTAGE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_status_report.system_voltage));
                            json_object_object_add(jobj, ST_SENSOR_STATUS, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_status_report.sensor_status));
                            json_object_object_add(jobj, ST_FLOW_PRECISION, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_status_report.flow_precision));
                            json_object_object_add(jobj, ST_FLOW_SCALE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_status_report.flow_scale));
                            json_object_object_add(jobj, ST_FLOW_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_status_report.flow_size));
                            json_object_object_add(jobj, ST_FLOW_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_status_report.flow_value));
                            json_object_object_add(jobj, ST_PRESSURE_PRECISION, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_status_report.pressure_precision));
                            json_object_object_add(jobj, ST_PRESSURE_SCALE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_status_report.pressure_scale));
                            json_object_object_add(jobj, ST_PRESSURE_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_status_report.pressure_size));
                            json_object_object_add(jobj, ST_PRESSURE_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_status_report.pressure_value));
                            json_object_object_add(jobj, ST_SHUTOFF_DURATION, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_status_report.shutoff_duration));
                            json_object_object_add(jobj, ST_SYSTEM_ERROR_STATUS, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_status_report.system_error_status));
                            json_object_object_add(jobj, ST_MASTER_VALVE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_status_report.master_valve));
                            json_object_object_add(jobj, ST_VALVE_ID, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_status_report.valve_id));
                            
                        }
                        break;

                        case IRRIGATION_SYSTEM_CONFIG_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_IRRIGATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SYSTEM_CONFIG_REPORT));
                            
                            json_object_object_add(jobj, ST_MASTER_VALVE_DELAY, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_config_report.master_valve_delay));
                            json_object_object_add(jobj, ST_HIGH_PRESSURE_THRESHOLD_PRECISION, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_config_report.HPT_precision));
                            json_object_object_add(jobj, ST_HIGH_PRESSURE_THRESHOLD_SCALE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_config_report.HPT_scale));
                            json_object_object_add(jobj, ST_HIGH_PRESSURE_THRESHOLD_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_config_report.HPT_size));
                            json_object_object_add(jobj, ST_HIGH_PRESSURE_THRESHOLD_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_config_report.HPT_value));
                            json_object_object_add(jobj, ST_LOW_PRESSURE_THRESHOLD_PRECISION, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_config_report.LPT_precision));
                            json_object_object_add(jobj, ST_LOW_PRESSURE_THRESHOLD_SCALE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_config_report.LPT_scale));
                            json_object_object_add(jobj, ST_LOW_PRESSURE_THRESHOLD_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_config_report.LPT_size));
                            json_object_object_add(jobj, ST_LOW_PRESSURE_THRESHOLD_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_config_report.LPT_value));
                            json_object_object_add(jobj, ST_SENSOR_POLARITY, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_system_config_report.sensor_polarity));
                        }
                        break;

                        case IRRIGATION_VALVE_INFO_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_IRRIGATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_VALVE_INFO_REPORT));
                            
                            json_object_object_add(jobj, ST_CONNECTED, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_info_report.connected));
                            json_object_object_add(jobj, ST_MASTER_VALVE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_info_report.master_valve));
                            json_object_object_add(jobj, ST_VALVE_ID, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_info_report.valve_id));
                            json_object_object_add(jobj, ST_NOMINAL_CURRENT, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_info_report.nominal_current));
                            json_object_object_add(jobj, ST_VALVE_ERROR_STATUS, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_info_report.valve_error_status));
                        }
                        break;

                        case IRRIGATION_VALVE_CONFIG_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_IRRIGATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_VALVE_CONFIG_REPORT));
                            
                            json_object_object_add(jobj, ST_MASTER_VALVE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.master_valve));
                            json_object_object_add(jobj, ST_VALVE_ID, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.valve_id));
                            json_object_object_add(jobj, ST_NOMINAL_CURRENT_HIGH_THRESHOLD, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.nominal_current_high_threshold));
                            json_object_object_add(jobj, ST_NOMINAL_CURRENT_LOW_THRESHOLD, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.nominal_current_low_threshold));
                            json_object_object_add(jobj, ST_MAXIMUM_FLOW_PRECISION, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.maximum_flow_precision));
                            json_object_object_add(jobj, ST_MAXIMUM_FLOW_SCALE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.maximum_flow_scale));
                            json_object_object_add(jobj, ST_MAXIMUM_FLOW_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.maximum_flow_size));
                            json_object_object_add(jobj, ST_MAXIMUM_FLOW_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.maximum_flow_value));
                            json_object_object_add(jobj, ST_FLOW_HIGH_THRESHOLD_PRECISION, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.FHT_precision));
                            json_object_object_add(jobj, ST_FLOW_HIGH_THRESHOLD_SCALE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.FHT_scale));
                            json_object_object_add(jobj, ST_FLOW_HIGH_THRESHOLD_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.FHT_size));
                            json_object_object_add(jobj, ST_FLOW_HIGH_THRESHOLD_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.FHT_value));
                            json_object_object_add(jobj, ST_FLOW_LOW_THRESHOLD_PRECISION, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.FLT_precision));
                            json_object_object_add(jobj, ST_FLOW_LOW_THRESHOLD_SCALE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.FLT_scale));
                            json_object_object_add(jobj, ST_FLOW_LOW_THRESHOLD_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.FLT_size));
                            json_object_object_add(jobj, ST_FLOW_LOW_THRESHOLD_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.FLT_value));
                            json_object_object_add(jobj, ST_SENSOR_USAGE, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_config_report.sensor_usage));
                        }
                        break;

                        case IRRIGATION_VALVE_TABLE_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_IRRIGATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_VALVE_TABLE_REPORT));
                            
                            json_object_object_add(jobj, ST_VALVE_TABLE_ID, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_table_report.valve_table_id));
                            json_object_object_add(jobj, ST_NO_VALVES, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_table_report.no_valves));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.irrigation_valve_table_report.no_valves;i++)
                            {
                                json_object *jlist=json_object_new_object();
                                json_object_object_add(jlist, ST_VALVE_ID, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_table_report.valve_list[i].valve_id));
                                json_object_object_add(jlist, ST_DURATION, json_object_new_int(pTxNotify.CmdClassNotify.irrigation_valve_table_report.valve_list[i].duration));

                                json_object_array_add(jarray,jlist);
                            }
                            json_object_object_add(jobj, ST_VALVE_LIST, jarray);
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_LANGUAGE:
                {
                    switch (pTxNotify.CmdClassNotify.cmd)
                    {
                        case LANGUAGE_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_LANGUAGE));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_LANGUAGE_REPORT));

                            json_object_object_add(jobj, ST_LANGUAGE_1, json_object_new_int(pTxNotify.CmdClassNotify.language_report.language_1));
                            json_object_object_add(jobj, ST_LANGUAGE_2, json_object_new_int(pTxNotify.CmdClassNotify.language_report.language_2));
                            json_object_object_add(jobj, ST_LANGUAGE_3, json_object_new_int(pTxNotify.CmdClassNotify.language_report.language_3));
                            json_object_object_add(jobj, ST_COUNTRY, json_object_new_int(pTxNotify.CmdClassNotify.language_report.country));
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_SENSOR_CONFIGURATION:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case SENSOR_TRIGGER_LEVEL_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SENSOR_CONFIGURATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            json_object_object_add(jobj, ST_SENSOR_TYPE, json_object_new_int(pTxNotify.CmdClassNotify.sensor_trigger_level_report.sensor_type));
                            json_object_object_add(jobj, ST_PRECISION, json_object_new_int(pTxNotify.CmdClassNotify.sensor_trigger_level_report.precision));
                            json_object_object_add(jobj, ST_SCALE, json_object_new_int(pTxNotify.CmdClassNotify.sensor_trigger_level_report.scale));
                            json_object_object_add(jobj, ST_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.sensor_trigger_level_report.size));
                            json_object_object_add(jobj, ST_TRIGGER_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.sensor_trigger_level_report.trigger_value));
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_SIMPLE_AV_CONTROL:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case SIMPLE_AV_CONTROL_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SIMPLE_AV_CONTROL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            json_object_object_add(jobj, ST_NO_REPORTS, json_object_new_int(pTxNotify.CmdClassNotify.simple_av_control_report.no_reports));
                        }
                        break;

                        case SIMPLE_AV_CONTROL_SUPPORTED_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SIMPLE_AV_CONTROL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));

                            json_object_object_add(jobj, ST_REPORT_NO, json_object_new_int(pTxNotify.CmdClassNotify.simple_av_control_supported_report.report_no));
                            json_object_object_add(jobj, ST_NO_BIT_MASKS, json_object_new_int(pTxNotify.CmdClassNotify.simple_av_control_supported_report.no_bit_masks));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.simple_av_control_supported_report.no_bit_masks;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.simple_av_control_supported_report.bit_mask[i]));
                            }
                            json_object_object_add(jobj, ST_BIT_MASK,jarray);
                              
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_USER_CODE:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case USERS_NUMBER_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_USER_CODE));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_USERS_NUMBER_REPORT));
                            
                            json_object_object_add(jobj, ST_SUPPORTED_USERS, json_object_new_int(pTxNotify.CmdClassNotify.users_number_report.supported_users));
                        }
                        break;

                        case USER_CODE_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_USER_CODE));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));

                            json_object_object_add(jobj, ST_USER_ID, json_object_new_int(pTxNotify.CmdClassNotify.user_code_report.user_id));
                            json_object_object_add(jobj, ST_USER_ID_STATUS, json_object_new_int(pTxNotify.CmdClassNotify.user_code_report.user_id_status));
                            json_object_object_add(jobj, ST_NO_USER_CODE, json_object_new_int(pTxNotify.CmdClassNotify.user_code_report.no_user_code));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.user_code_report.no_user_code;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.user_code_report.user_code[i]));
                            }
                            json_object_object_add(jobj, ST_USER_CODE,jarray);

                            /*clean all usercodes*/
                            if((pTxNotify.CmdClassNotify.user_code_report.user_id == 0) 
                                && (pTxNotify.CmdClassNotify.user_code_report.user_id_status == USER_CODE_AVAILABLE)
                                )
                            {
                                remove_all_user_code(ID);
                                break;
                            }

                            char userIdStr[SIZE_64B];
                            sprintf(userIdStr, "%d", pTxNotify.CmdClassNotify.user_code_report.user_id);

                            if(pTxNotify.CmdClassNotify.user_code_report.user_id_status == USER_CODE_AVAILABLE)
                            {
                                remove_user_code(ID, pTxNotify.CmdClassNotify.user_code_report.user_id);
                            }
                            else if(pTxNotify.CmdClassNotify.user_code_report.user_id_status == USER_CODE_SUCCESS)
                            {
                                char *name = remove_user_code_cmd(ID, pTxNotify.CmdClassNotify.user_code_report.user_id);
                                if(!name)
                                {
                                    name=strdup(ST_MANUAL);
                                }

                                add_user_code(ID, userIdStr, name);
                                free(name);
                            }
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_WAKE_UP:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case WAKE_UP_INTERVAL_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_WAKE_UP));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_INTERVAL_REPORT));
                            
                            json_object_object_add(jobj, ST_SECONDS, json_object_new_int(pTxNotify.CmdClassNotify.wake_up_interval_report.seconds));
                            json_object_object_add(jobj, ST_NODE_ID, json_object_new_int(pTxNotify.CmdClassNotify.wake_up_interval_report.node_id));
                        }
                        break;

                        case WAKE_UP_INTERVAL_CAPABILITIES_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_WAKE_UP));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_INTERVAL_CAPABILITIES_REPORT));
                            
                            json_object_object_add(jobj, ST_MIN_WK_INTERVAL_SECONDS, json_object_new_int(pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.min_wk_interval_seconds));
                            json_object_object_add(jobj, ST_MAX_WK_INTERVAL_SECONDS, json_object_new_int(pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.max_wk_interval_seconds));
                            json_object_object_add(jobj, ST_DEFAULT_WK_INTERVAL_SECONDS, json_object_new_int(pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.default_wk_interval_seconds));
                            json_object_object_add(jobj, ST_WK_INTERVAL_STEP_SECONDS, json_object_new_int(pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.wk_interval_step_seconds));
                        }
                        break;

                        case WAKE_UP_NOTIFICATION:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_WAKE_UP));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_WAKE_UP_NOTIFICATION));
                            if(pTxNotify.CmdClassNotify.node_is_listening == MODE_NONLISTENING)
                            {
                                deviceWakeUp = 1;
                            }
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_WINDOW_COVERING:
                {
                    switch  (pTxNotify.CmdClassNotify.cmd)
                    {
                        case WINDOW_COVERING_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_WINDOW_COVERING));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            
                            json_object_object_add(jobj, ST_PARAM_ID, json_object_new_int(pTxNotify.CmdClassNotify.window_covering_report.param_id));
                            sprintf(str_value, "%d", pTxNotify.CmdClassNotify.window_covering_report.value);
                            json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));
                            json_object_object_add(jobj, ST_TARGET_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.window_covering_report.target_value));
                            json_object_object_add(jobj, ST_DURATION, json_object_new_int(pTxNotify.CmdClassNotify.window_covering_report.duration));
                        }
                        break;

                        case WINDOW_COVERING_SUPPORTED_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_WINDOW_COVERING));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));
                            
                            json_object_object_add(jobj, ST_NO_PARAM_MASKS, json_object_new_int(pTxNotify.CmdClassNotify.window_covering_supported_report.no_param_masks));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.window_covering_supported_report.no_param_masks;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.window_covering_supported_report.param_mask[i]));
                            }
                            json_object_object_add(jobj, ST_PARAM_MASK,jarray);
                        }
                        break;
                    }
                }
                break;
                case COMMAND_CLASS_CLOCK:
                {
                    switch (pTxNotify.CmdClassNotify.cmd)
                    {
                        case CLOCK_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_CLOCK));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));

                            json_object_object_add(jobj, ST_WEEKDAY, json_object_new_int(pTxNotify.CmdClassNotify.clock_report.weekday));
                            json_object_object_add(jobj, ST_HOUR, json_object_new_int(pTxNotify.CmdClassNotify.clock_report.hour));
                            json_object_object_add(jobj, ST_MINUTE, json_object_new_int(pTxNotify.CmdClassNotify.clock_report.minute));
                        }
                        break;
                    }
                }
                break;
                case COMMAND_CLASS_ANTITHEFT:
                {
                    switch (pTxNotify.CmdClassNotify.cmd)
                    {
                        case ANTITHEFT_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ANTITHEFT));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));

                            json_object_object_add(jobj, ST_PROTECTION_STATUS, json_object_new_int(pTxNotify.CmdClassNotify.antitheft_report.protection_status));
                            json_object_object_add(jobj, ST_MANUFACTURER_ID, json_object_new_int(pTxNotify.CmdClassNotify.antitheft_report.manufacturer_id));
                            json_object_object_add(jobj, ST_NO_ANTI_THEFT_HINT, json_object_new_int(pTxNotify.CmdClassNotify.antitheft_report.no_anti_theft_hint));
                            int i;
                            json_object *jarray = json_object_new_array();
                            for (i = 0; i < pTxNotify.CmdClassNotify.antitheft_report.no_anti_theft_hint; i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.antitheft_report.hint_list[i]));
                            }
                            json_object_object_add(jobj, ST_HINT_LIST, jarray);
                        }
                        break;
                    }
                }
                break;
                case COMMAND_CLASS_SCENE_ACTUATOR_CONF:
                {
                    switch (pTxNotify.CmdClassNotify.cmd)
                    {
                        case SCENE_ACTUATOR_CONF_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SCENE_ACTUATOR_CONF));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));

                            json_object_object_add(jobj, ST_SCENE_ID, json_object_new_int(pTxNotify.CmdClassNotify.scene_actuator_conf_report.scene_id));
                            json_object_object_add(jobj, ST_LEVEL, json_object_new_int(pTxNotify.CmdClassNotify.scene_actuator_conf_report.level));
                            json_object_object_add(jobj, ST_DIMMING_DURATION, json_object_new_int(pTxNotify.CmdClassNotify.scene_actuator_conf_report.dimming_duration));
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_SCENE_CONTROLLER_CONF:
                {
                    switch (pTxNotify.CmdClassNotify.cmd)
                    {
                        case SCENE_CONTROLLER_CONF_REPORT:
                        {
                            char buttonId[SIZE_32B];
                            char groupId[SIZE_32B];
                            snprintf(buttonId, sizeof(buttonId), "%d", pTxNotify.CmdClassNotify.scene_controller_conf_report.scene_id);
                            snprintf(groupId, sizeof(groupId), "%d", pTxNotify.CmdClassNotify.scene_controller_conf_report.group_id);

                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SCENE_CONTROLLER_CONF));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));

                            json_object_object_add(jobj, ST_GROUP_ID, json_object_new_string(groupId));
                            json_object_object_add(jobj, ST_SCENE_ID, json_object_new_int(pTxNotify.CmdClassNotify.scene_controller_conf_report.scene_id));
                            json_object_object_add(jobj, ST_BUTTON_ID, json_object_new_string(buttonId));
                            json_object_object_add(jobj, ST_DIMMING_DURATION, json_object_new_int(pTxNotify.CmdClassNotify.scene_controller_conf_report.dimming_duration));

                            set_scene_button_database(_VR_CB_(zwave), ID, groupId, buttonId, NULL);
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_CENTRAL_SCENE:
                {
                    switch (pTxNotify.CmdClassNotify.cmd)
                    {
                        case CENTRAL_SCENE_SUPPORTED_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_CENTRAL_SCENE));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));
                            if (pTxNotify.CmdClassNotify.version==1)
                            {
                                json_object_object_add(jobj, ST_SUPPORTED_SCENES, json_object_new_int(pTxNotify.CmdClassNotify.central_scene_supported_report.supported_scenes));
                            }
                            else
                            {
                                json_object_object_add(jobj, ST_SUPPORTED_SCENES, json_object_new_int(pTxNotify.CmdClassNotify.central_scene_supported_report_v2.supported_scenes));
                                json_object_object_add(jobj, ST_NO_BIT_MASKS, json_object_new_int(pTxNotify.CmdClassNotify.central_scene_supported_report_v2.no_bit_masks));
                                json_object_object_add(jobj, ST_IDENTICAL, json_object_new_int(pTxNotify.CmdClassNotify.central_scene_supported_report_v2.identical));
                                json_object_object_add(jobj, ST_SLOW_REFRESH, json_object_new_int(pTxNotify.CmdClassNotify.central_scene_supported_report_v2.slow_refresh));
                                int i, j;
                                json_object *jarray_supported_scenes = json_object_new_array();

                                for (i = 0; i < pTxNotify.CmdClassNotify.central_scene_supported_report_v2.supported_scenes; i++)
                                {
                                    json_object *jobj_temp = json_object_new_object();
                                    json_object *jarray = json_object_new_array();
                                    for (j = 0; j < pTxNotify.CmdClassNotify.central_scene_supported_report_v2.no_bit_masks; j++)
                                    {
                                        json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.central_scene_supported_report_v2.supported_key_attributes[i][j]));
                                    }
                                    json_object_object_add(jobj_temp, ST_SUPPORTED_KEY_ATTRIBUTES_FOR_SCENE, jarray);
                                    char *centralSceneSupported = NULL;
                                    get_supported_key_attributes_from_bit_mask((uint8_t*)&pTxNotify.CmdClassNotify.central_scene_supported_report_v2.supported_key_attributes[i][0],
                                                                    pTxNotify.CmdClassNotify.central_scene_supported_report_v2.no_bit_masks,
                                                                    &centralSceneSupported);
                                    JSON_ADD_STRING_SAFE(jobj_temp, ST_KEY_ATTRIBUTES_SUPPORTED, centralSceneSupported);

                                    json_object_array_add(jarray_supported_scenes, jobj_temp);

                                }

                                json_object_object_add(jobj, ST_SUPPORTED_KEY_ATTRIBUTES, jarray_supported_scenes);
                            }

                        }
                        break;
                        case CENTRAL_SCENE_NOTIFICATION:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_CENTRAL_SCENE));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            json_object_object_add(jobj, ST_SEQUENCE_NUMBER, json_object_new_int(pTxNotify.CmdClassNotify.central_scene_notification.sequence_number));
                            json_object_object_add(jobj, ST_KEY_ATTRIBUTES, json_object_new_int(pTxNotify.CmdClassNotify.central_scene_notification.key_attributes));
                            json_object_object_add(jobj, ST_SLOW_REFRESH, json_object_new_int(pTxNotify.CmdClassNotify.central_scene_notification.slow_refresh));
                            json_object_object_add(jobj, ST_SCENE_NUMBER, json_object_new_int(pTxNotify.CmdClassNotify.central_scene_notification.scene_number));

                        }
                        break;
                        case CENTRAL_SCENE_CONFIGURATION_REPORT_V3:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_CENTRAL_SCENE));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_CONFIGURATION_REPORT));
                            json_object_object_add(jobj, ST_SLOW_REFRESH, json_object_new_int(pTxNotify.CmdClassNotify.central_scene_configuration_report.slow_refresh));

                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_SCENE_ACTIVATION:
                {
                    switch (pTxNotify.CmdClassNotify.cmd)
                    {
                        case SCENE_ACTIVATION_SET:
                        {
                            char buttonId[SIZE_32B];
                            snprintf(buttonId, sizeof(buttonId), "%d", pTxNotify.CmdClassNotify.scene_activation_set.scene_id);

                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SCENE_ACTIVATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SET));

                            json_object_object_add(jobj, ST_SCENE_ID, json_object_new_int(pTxNotify.CmdClassNotify.scene_activation_set.scene_id));
                            json_object_object_add(jobj, ST_BUTTON_ID, json_object_new_string(buttonId));
                            json_object_object_add(jobj, ST_DIMMING_DURATION, json_object_new_int(pTxNotify.CmdClassNotify.scene_activation_set.dimming_duration));

                            update_sence_activate_database(_VR_(zwave), ID, buttonId, (unsigned)time(NULL));
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_REMOTE_ASSOCIATION:
                {
                    switch (pTxNotify.CmdClassNotify.cmd)
                    {
                        case REMOTE_ASSOCIATION_CONFIGURATION_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_REMOTE_ASSOCIATION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_CONFIGURATION_REPORT));

                            json_object_object_add(jobj, ST_LOCAL_GROUPING_ID, json_object_new_int(pTxNotify.CmdClassNotify.remote_association_configuration_report.local_grouping_id));
                            json_object_object_add(jobj, ST_REMOTE_NODE_ID, json_object_new_int(pTxNotify.CmdClassNotify.remote_association_configuration_report.remote_node_id));
                            json_object_object_add(jobj, ST_REMOTE_GROUPING_ID, json_object_new_int(pTxNotify.CmdClassNotify.remote_association_configuration_report.remote_grouping_id));
                        }
                        break;
                    }
                }
                break;
                case COMMAND_CLASS_HAIL:
                {
                    switch (pTxNotify.CmdClassNotify.cmd)
                    {
                        case HAIL:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_HAIL));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_HAIL));
                        }
                        break;
                    }
                }
                break;
                case COMMAND_CLASS_INDICATOR:
                {
                    switch (pTxNotify.CmdClassNotify.cmd)
                    {
                        case INDICATOR_REPORT:
                        {
                            
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_INDICATOR));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                            if (pTxNotify.CmdClassNotify.version == 1)
                            {
                                sprintf(str_value, "%d", pTxNotify.CmdClassNotify.indicator_report.value);
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string(str_value));
                            }
                            else{
                                json_object_object_add(jobj, ST_INDICATOR_0_VALUE, json_object_new_int(pTxNotify.CmdClassNotify.indicator_report_v2.indicator_0_value));
                                json_object_object_add(jobj, ST_INDICATOR_OBJECT_COUNT, json_object_new_int(pTxNotify.CmdClassNotify.indicator_report_v2.indicator_object_count));
                                int i;
                                json_object *jarray=json_object_new_array();

                                for (i=0;i<pTxNotify.CmdClassNotify.indicator_report_v2.indicator_object_count;i++)
                                {
                                    json_object *jgrp=json_object_new_object();
                                    json_object_object_add(jgrp, ST_INDICATOR_ID, json_object_new_int(pTxNotify.CmdClassNotify.indicator_report_v2.indicator_object[i].indicator_id));
                                    json_object_object_add(jgrp, ST_PROPERTY_ID, json_object_new_int(pTxNotify.CmdClassNotify.indicator_report_v2.indicator_object[i].property_id));
                                    sprintf(str_value, "%d", pTxNotify.CmdClassNotify.indicator_report_v2.indicator_object[i].value);
                                    json_object_object_add(jgrp, ST_VALUE, json_object_new_string(str_value));
                                    json_object_array_add(jarray,jgrp);
                                }
                                json_object_object_add(jobj, ST_INDICATOR_OBJECT_LIST,jarray);
                            }
                        }
                        break;
                        case INDICATOR_SUPPORTED_REPORT_V2:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_INDICATOR));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));
                            json_object_object_add(jobj, ST_INDICATOR_ID, json_object_new_int(pTxNotify.CmdClassNotify.indicator_support_report.indicator_id));
                            json_object_object_add(jobj, ST_NEXT_INDICATOR_ID, json_object_new_int(pTxNotify.CmdClassNotify.indicator_support_report.next_indicator_id));
                            json_object_object_add(jobj, ST_PROPERTY_SUPPORTED_BIT_MASK_LENGTH, json_object_new_int(pTxNotify.CmdClassNotify.indicator_support_report.property_supported_bit_mask_length));
                            int i;
                            json_object *jarray=json_object_new_array();
                            for (i=0;i<pTxNotify.CmdClassNotify.indicator_support_report.property_supported_bit_mask_length;i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.indicator_support_report.property_supported_bit_mask_list[i]));
                            }
                            json_object_object_add(jobj, ST_PROPERTY_SUPPORTED_BIT_MASK_LIST,jarray);
                        }
                        break;
                    }
                }
                break;
                case COMMAND_CLASS_NODE_NAMING:
                {
                    switch (pTxNotify.CmdClassNotify.cmd)
                    {
                        case NODE_NAMING_NODE_NAME_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_NODE_NAMING));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_NODE_NAME_REPORT));

                            json_object_object_add(jobj, ST_CHAR_PRESENTATION, json_object_new_int(pTxNotify.CmdClassNotify.node_name_report.char_presentation));
                            json_object_object_add(jobj, ST_NO_NODE_NAME, json_object_new_int(pTxNotify.CmdClassNotify.node_name_report.no_node_name));
                            int i;
                            json_object *jarray = json_object_new_array();
                            for (i = 0; i < pTxNotify.CmdClassNotify.node_name_report.no_node_name; i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.node_name_report.node_name_list[i]));
                            }
                            json_object_object_add(jobj, ST_NODE_NAME_LIST, jarray);
                        }
                        break;
                        case NODE_NAMING_NODE_LOCATION_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_NODE_NAMING));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_NODE_LOCATION_REPORT));

                            json_object_object_add(jobj, ST_CHAR_PRESENTATION, json_object_new_int(pTxNotify.CmdClassNotify.node_location_report.char_presentation));
                            json_object_object_add(jobj, ST_NO_NODE_LOCATION, json_object_new_int(pTxNotify.CmdClassNotify.node_location_report.no_node_location));
                            int i;
                            json_object *jarray = json_object_new_array();
                            for (i = 0; i < pTxNotify.CmdClassNotify.node_location_report.no_node_location; i++)
                            {
                                json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.node_location_report.node_location_list[i]));
                            }
                            json_object_object_add(jobj, ST_NODE_LOCATION_LIST, jarray);
                        }
                        break;
                    }
                }
                break;
                case COMMAND_CLASS_FIRMWARE_UPDATE_MD:
                {
                    switch (pTxNotify.CmdClassNotify.cmd)
                    {
                        case FIRMWARE_MD_REPORT:
                        {
                            if (pTxNotify.CmdClassNotify.version == 1)
                            {
                                json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_FIRMWARE_UPDATE_MD));
                                json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_FIRMWARE_MD_REPORT));
                                json_object_object_add(jobj, ST_MANUFACTURER_ID, json_object_new_int(pTxNotify.CmdClassNotify.firmware_md_report.manufacturer_id));
                                json_object_object_add(jobj, ST_FIRMWARE_ID, json_object_new_int(pTxNotify.CmdClassNotify.firmware_md_report.firmware_id));
                                json_object_object_add(jobj, ST_CHECKSUM, json_object_new_int(pTxNotify.CmdClassNotify.firmware_md_report.checksum));
                            }
                            else if (pTxNotify.CmdClassNotify.version == 3)
                            {
                                json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_FIRMWARE_UPDATE_MD));
                                json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_FIRMWARE_MD_REPORT));
                                json_object_object_add(jobj, ST_MANUFACTURER_ID, json_object_new_int(pTxNotify.CmdClassNotify.firmware_md_report_v3.manufacturer_id));
                                json_object_object_add(jobj, ST_FIRMWARE_0_CHECKSUM, json_object_new_int(pTxNotify.CmdClassNotify.firmware_md_report_v3.firmware0_id));
                                json_object_object_add(jobj, ST_FIRMWARE_0_CHECKSUM, json_object_new_int(pTxNotify.CmdClassNotify.firmware_md_report_v3.firmware0_checksum));
                                json_object_object_add(jobj, ST_FIRMWARE_UPGRADABLE, json_object_new_int(pTxNotify.CmdClassNotify.firmware_md_report_v3.firmware_upgradable));
                                json_object_object_add(jobj, ST_NO_FIRMWARE_TARGETS, json_object_new_int(pTxNotify.CmdClassNotify.firmware_md_report_v3.no_firmware_targets));
                                json_object_object_add(jobj, ST_MAX_FRAGMENT_SIZE, json_object_new_int(pTxNotify.CmdClassNotify.firmware_md_report_v3.max_fragment_size));

                                int i;
                                json_object *jarray = json_object_new_array();
                                for (i = 0; i < pTxNotify.CmdClassNotify.firmware_md_report_v3.no_firmware_targets; i++)
                                {
                                    json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.firmware_md_report_v3.firmware_id_list[i]));
                                }
                                json_object_object_add(jobj, ST_FIRMWARE_ID_LIST, jarray);
                            }
                        }
                        break;
                        case FIRMWARE_UPDATE_MD_REQUEST_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_FIRMWARE_UPDATE_MD));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_FIRMWARE_UPDATE_MD_REQUEST_REPORT));
                            json_object_object_add(jobj, ST_STATUS, json_object_new_int(pTxNotify.CmdClassNotify.firmware_update_md_request_report.status));
                        }
                        break;
                        case FIRMWARE_UPDATE_MD_GET:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_FIRMWARE_UPDATE_MD));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_FIRMWARE_UPDATE_MD_GET));
                            json_object_object_add(jobj, ST_NO_REPORTS, json_object_new_int(pTxNotify.CmdClassNotify.firmware_update_md_get.no_reports));
                            json_object_object_add(jobj, ST_REPORT_NUMBER, json_object_new_int(pTxNotify.CmdClassNotify.firmware_update_md_get.report_number));
                        }
                        break;
                        case FIRMWARE_UPDATE_MD_REPORT:
                        {
                            if (pTxNotify.CmdClassNotify.version == 1)
                            {
                                json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_FIRMWARE_UPDATE_MD));
                                json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_FIRMWARE_UPDATE_MD_REPORT));
                                json_object_object_add(jobj, ST_LAST, json_object_new_int(pTxNotify.CmdClassNotify.firmware_update_md_report.last));
                                json_object_object_add(jobj, ST_REPORT_NUMBER, json_object_new_int(pTxNotify.CmdClassNotify.firmware_update_md_report.report_number));
                                json_object_object_add(jobj, ST_NO_DATA, json_object_new_int(pTxNotify.CmdClassNotify.firmware_update_md_report.no_data));
                                int i;
                                json_object *jarray = json_object_new_array();
                                for (i = 0; i < pTxNotify.CmdClassNotify.firmware_update_md_report.no_data; i++)
                                {
                                    json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.firmware_update_md_report.data_list[i]));
                                }
                                json_object_object_add(jobj, ST_DATA_LIST, jarray);
                            }
                            else if (pTxNotify.CmdClassNotify.version == 2)
                            {
                                json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_FIRMWARE_UPDATE_MD));
                                json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_FIRMWARE_UPDATE_MD_REPORT));
                                json_object_object_add(jobj, ST_LAST, json_object_new_int(pTxNotify.CmdClassNotify.firmware_update_md_report_v2.last));
                                json_object_object_add(jobj, ST_REPORT_NUMBER, json_object_new_int(pTxNotify.CmdClassNotify.firmware_update_md_report_v2.report_number));
                                json_object_object_add(jobj, ST_NO_DATA, json_object_new_int(pTxNotify.CmdClassNotify.firmware_update_md_report_v2.no_data));
                                int i;
                                json_object *jarray = json_object_new_array();
                                for (i = 0; i < pTxNotify.CmdClassNotify.firmware_update_md_report_v2.no_data; i++)
                                {
                                    json_object_array_add(jarray, json_object_new_int(pTxNotify.CmdClassNotify.firmware_update_md_report_v2.data_list[i]));
                                }
                                json_object_object_add(jobj, ST_DATA_LIST, jarray);
                                json_object_object_add(jobj, ST_CHECKSUM, json_object_new_int(pTxNotify.CmdClassNotify.firmware_update_md_report_v2.checksum));
                            }
                        }
                        break;
                        case FIRMWARE_UPDATE_MD_STATUS_REPORT:
                        {
                            if (pTxNotify.CmdClassNotify.version == 1)
                            {
                                json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_FIRMWARE_UPDATE_MD));
                                json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_FIRMWARE_UPDATE_MD_STATUS_REPORT));
                                json_object_object_add(jobj, ST_STATUS, json_object_new_int(pTxNotify.CmdClassNotify.firmware_update_md_status_report.status));
                            }
                            else if (pTxNotify.CmdClassNotify.version == 3)
                            {
                                json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_FIRMWARE_UPDATE_MD));
                                json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_FIRMWARE_UPDATE_MD_STATUS_REPORT));
                                json_object_object_add(jobj, ST_STATUS, json_object_new_int(pTxNotify.CmdClassNotify.firmware_update_md_status_report_v3.status));
                                json_object_object_add(jobj, ST_WAIT_TIME, json_object_new_int(pTxNotify.CmdClassNotify.firmware_update_md_status_report_v3.wait_time));
                            }
                        }
                        break;
                    }
                }
                break;

                case COMMAND_CLASS_SUPERVISION:
                {
                    switch (pTxNotify.CmdClassNotify.cmd)
                    {
                        case SUPERVISION_REPORT:
                        {
                            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SUPERVISION));
                            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPERVISION_REPORT));

                            json_object_object_add(jobj, ST_CMD_CLASS_ENCAP, json_object_new_int(pTxNotify.CmdClassNotify.supervision_report.class));
                            json_object_object_add(jobj, ST_CMD_ENCAP, json_object_new_int(pTxNotify.CmdClassNotify.supervision_report.cmd));
                            json_object_object_add(jobj, ST_MORE_STATUS_UPDATES, json_object_new_int(pTxNotify.CmdClassNotify.supervision_report.more_status_updates));
                            json_object_object_add(jobj, ST_STATUS, json_object_new_int(pTxNotify.CmdClassNotify.supervision_report.status));
                            json_object_object_add(jobj, ST_DURATION, json_object_new_int(pTxNotify.CmdClassNotify.supervision_report.duration));

                        }
                        break;
                    }
                }
                break;
                //-------------------------------------------------------------------------
            }

            /*for manage adding synchronous*/
            remove_dev_adding_actions(ID, jobj);
            //----------------------send data to app-----------------------------------
            if(sending_notify)
            {
                char timestampt[SIZE_32B];
                sprintf(timestampt, "%u", (unsigned)time(NULL));

                if(deviceWakeUp)
                {
                    update_zwave_status(_VR_CB_(zwave), ID, ST_WAKE_UP_NOTIFICATION, timestampt, ST_REPLACE, 0);
                }

                update_zwave_status(_VR_CB_(zwave), ID, ST_LAST_UPDATE, timestampt, ST_REPLACE, 0);
                update_zwave_status(_VR_CB_(zwave), ID, ST_STATE, ST_ALIVE, ST_REPLACE, 0);
                json_object_object_add(jobj, ST_CLOUD_TIME_STAMP, json_object_new_string(timestampt));
                Send_ubus_notify((char*)json_object_to_json_string(jobj));
            }
        }
        else if (g_notify.notify[0].notify_status==LEARN_MODE_NOTIFY)
        {
            memcpy((uint8_t*)&pTxNotify.LearnModeNotify,g_notify.notify[0].notify_message,sizeof(LEARN_MODE_NOTIFY_T));
            memset(tmp, 0x00, sizeof(tmp));
            sprintf(tmp, "%02X", pTxNotify.LearnModeNotify.including_node_id);
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(tmp));
            sprintf(ID, "%02X", pTxNotify.LearnModeNotify.my_node_id);
            json_object_object_add(jobj, ST_MY_ID, json_object_new_string(ID));

            if (pTxNotify.LearnModeNotify.status==APP_SET_LEARN_MODE_START)
            {    
                
                json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_LEARN_MODE));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_STARTED));

            }else if (pTxNotify.LearnModeNotify.status==APP_SET_LEARN_MODE_REMOVED)
            {    
                char  homeID[9];
                sprintf(homeID,"%08X",pTxNotify.LearnModeNotify.my_home_id);
                json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_LEARN_MODE));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_REMOVED));
                json_object_object_add(jobj, ST_HOMEID, json_object_new_string(homeID));
                json_object_object_add(jobj, ST_MY_NODE_FLAGS, json_object_new_int(pTxNotify.LearnModeNotify.my_node_flags));
                json_object_object_add(jobj, ST_CONTROLLER_CAPABILITY, json_object_new_int(pTxNotify.LearnModeNotify.my_capability));

                pthread_t reset_zwave_thread_t;
                pthread_create(&reset_zwave_thread_t, NULL, (void *)&reset_zwave_thread, NULL);
                pthread_detach(reset_zwave_thread_t);

            }else if (pTxNotify.LearnModeNotify.status==APP_SET_LEARN_MODE_TIMEOUT)
            {    
                
                json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_LEARN_MODE));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_TIMEOUT));

            }else if (pTxNotify.LearnModeNotify.status==APP_SET_LEARN_MODE_FAILED)
            {    
                
                json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_LEARN_MODE));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_LEARN_FAILED));

            }else if (pTxNotify.LearnModeNotify.status==APP_SET_LEARN_MODE_INCLUDED_NON_SECURE)
            {   
                char  type[8];
                char  homeID[9];
                sprintf(homeID,"%08X",pTxNotify.LearnModeNotify.my_home_id);
                sprintf(type, "%02X", pTxNotify.LearnModeNotify.including_node_type);
                json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_LEARN_MODE));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_ADD_NON_SECURE_DONE));
                json_object_object_add(jobj, ST_INCLUDING_SECURE_SCHEME, json_object_new_int(pTxNotify.LearnModeNotify.including_secure_scheme));
                json_object_object_add(jobj, ST_MY_NODE_FLAGS, json_object_new_int(pTxNotify.LearnModeNotify.my_node_flags));
                json_object_object_add(jobj, ST_HOMEID, json_object_new_string(homeID));
                json_object_object_add(jobj, ST_CONTROLLER_CAPABILITY, json_object_new_int(pTxNotify.LearnModeNotify.my_capability));
                json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(type));
                json_object_object_add(jobj, ST_NODE_MODE, json_object_new_int(pTxNotify.LearnModeNotify.including_node_info.mode));
                json_object_object_add(jobj, ST_TYPE_BASIC, json_object_new_int(pTxNotify.LearnModeNotify.including_node_info.nodeType.basic));
                json_object_object_add(jobj, ST_TYPE_GENERIC, json_object_new_int(pTxNotify.LearnModeNotify.including_node_info.nodeType.generic));
                json_object_object_add(jobj, ST_TYPE_SPECIFIC, json_object_new_int(pTxNotify.LearnModeNotify.including_node_info.nodeType.specific));

            }
            else if (pTxNotify.LearnModeNotify.status==APP_SET_LEARN_MODE_INCLUDED_SECURE_READY)
            {    
                
                json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_LEARN_MODE));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_ADD_SECURE_STARTED));

            }else if (pTxNotify.LearnModeNotify.status==APP_SET_LEARN_MODE_INCLUDED_SECURE_DONE)
            {    
                char  type[8];
                char  homeID[9];
                sprintf(homeID,"%08X",pTxNotify.LearnModeNotify.my_home_id);
                sprintf(type, "%02X", pTxNotify.LearnModeNotify.including_node_type);
                json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_LEARN_MODE));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_ADD_SECURE_DONE));
                json_object_object_add(jobj, ST_INCLUDING_SECURE_SCHEME, json_object_new_int(pTxNotify.LearnModeNotify.including_secure_scheme));
                json_object_object_add(jobj, ST_MY_NODE_FLAGS, json_object_new_int(pTxNotify.LearnModeNotify.my_node_flags));
                json_object_object_add(jobj, ST_HOMEID, json_object_new_string(homeID));
                json_object_object_add(jobj, ST_CONTROLLER_CAPABILITY, json_object_new_int(pTxNotify.LearnModeNotify.my_capability));
                json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(type));
                json_object_object_add(jobj, ST_NODE_MODE, json_object_new_int(pTxNotify.LearnModeNotify.including_node_info.mode));
                json_object_object_add(jobj, ST_TYPE_BASIC, json_object_new_int(pTxNotify.LearnModeNotify.including_node_info.nodeType.basic));
                json_object_object_add(jobj, ST_TYPE_GENERIC, json_object_new_int(pTxNotify.LearnModeNotify.including_node_info.nodeType.generic));
                json_object_object_add(jobj, ST_TYPE_SPECIFIC, json_object_new_int(pTxNotify.LearnModeNotify.including_node_info.nodeType.specific));


            }else if (pTxNotify.LearnModeNotify.status==APP_SET_LEARN_MODE_INCLUDED_SECURE_FAILED)
            {    
                
                json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_LEARN_MODE));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_ADD_SECURE_FAILED));

            }
            Send_ubus_notify((char*)json_object_to_json_string(jobj));
        }
        else if (g_notify.notify[0].notify_status==UPDATE_NETWORK_NOTIFY)
        {
            memcpy((uint8_t*)&pTxNotify.UpdateNetworkNotify,g_notify.notify[0].notify_message,sizeof(UPDATE_NETWORK_NOTIFY_T));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_UPDATE_NETWORK));
            update_network_adding_device(jobj, pTxNotify);
            Send_ubus_notify((char*)json_object_to_json_string(jobj));
        }
        else if (g_notify.notify[0].notify_status==PING_FAILED_NODE_NOTIFY)
        {
            memcpy((uint8_t*)&pTxNotify.PingFailedNodeNotify,g_notify.notify[0].notify_message,sizeof(PING_FAILED_NODE_NOTIFY_T));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_PING_ALL_NODE));
            uint8_t i;
            char tempId[3] = {'\0'};
            json_object *jarray_node_list = json_object_new_array();

            for (i=0; i < pTxNotify.PingFailedNodeNotify.no_node; i++)
            {
                sprintf(tempId, "%02X", pTxNotify.PingFailedNodeNotify.nodes[i]);
                json_object_array_add(jarray_node_list, json_object_new_string(tempId));
            }
            json_object_object_add(jobj, ST_FAILED_NODE_LIST, jarray_node_list);
            Send_ubus_notify((char*)json_object_to_json_string(jobj));
        }
        else if (g_notify.notify[0].notify_status==NETWORK_HEALTH_NOTIFY)
        {
            uint8_t i;
            char tempId[3] = {'\0'};

            memcpy((uint8_t*)&pTxNotify.NetworkHealthNotify,g_notify.notify[0].notify_message,sizeof(NETWORK_HEALTH_NOTIFY_T));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_NETWORK_HEALTH));
            sprintf(tempId, "%02X", pTxNotify.NetworkHealthNotify.node_id);
            json_object_object_add(jobj, ST_ID, json_object_new_string(tempId));
            

            json_object *jarray_node_list = json_object_new_array();

            for (i=0; i < pTxNotify.NetworkHealthNotify.neigbors_count; i++)
            {
                sprintf(tempId, "%02X", pTxNotify.NetworkHealthNotify.neighbors[i]);
                json_object_array_add(jarray_node_list, json_object_new_string(tempId));
            }
            json_object_object_add(jobj, ST_NEIGHBORS, jarray_node_list);
            json_object_object_add(jobj, ST_TEST_COUNT, json_object_new_int(pTxNotify.NetworkHealthNotify.test_count));
            json_object_object_add(jobj, ST_PER, json_object_new_int(pTxNotify.NetworkHealthNotify.PER));
            json_object_object_add(jobj, ST_ROUTE_CHANGE, json_object_new_int(pTxNotify.NetworkHealthNotify.RC));
            json_object_object_add(jobj, ST_ROUTE_COUNT, json_object_new_int(pTxNotify.NetworkHealthNotify.route_count));
            json_object_object_add(jobj, ST_NETWORK_HEALTH_VALUE, json_object_new_int(pTxNotify.NetworkHealthNotify.NH));
            json_object_object_add(jobj, ST_NETWORK_HEALTH_SYMBOL, json_object_new_string(pTxNotify.NetworkHealthNotify.NHS));

            Send_ubus_notify((char*)json_object_to_json_string(jobj));
        }
        else if (g_notify.notify[0].notify_status==INCLUSION_CONTROLLER_NOTIFY)
        {
            memcpy((uint8_t*)&pTxNotify.InclusionControllerNotify,g_notify.notify[0].notify_message,sizeof(INCLUSION_CONTROLLER_T));
            if (pTxNotify.InclusionControllerNotify.status==INCLUSION_CONTROLLER_START)
            {
                json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_INCLUSION_CONTROLLER));
                json_object_object_add(jobj, ST_STATUS, json_object_new_int(pTxNotify.InclusionControllerNotify.status));
                Send_ubus_notify((char*)json_object_to_json_string(jobj));
            }else if (pTxNotify.InclusionControllerNotify.status==INCLUSION_CONTROLLER_TIMEOUT)
            {

            }
        }
        else if (g_notify.notify[0].notify_status==ZWAVE_COMMAND_PROCESS_NOTIFY)
        {
            Send_ubus_notify((char*)g_notify.notify[0].notify_message);
        }

        pthread_mutex_lock(&CriticalMutexTransmitQueue);
        g_notify.notify_index--;
        memcpy((uint8_t*) &g_notify.notify[0],(uint8_t*)&g_notify.notify[1],g_notify.notify_index*sizeof(notify_t));
        pthread_mutex_unlock(&CriticalMutexTransmitQueue);
        json_object_put(jobj);
    }
    
    uloop_timeout_set(timeout, 100);
}

static struct uloop_timeout polling_notify = {
    .cb = notify_active_cb,
};

static void handle_ubus_event(struct ubus_context *ctx, struct ubus_event_handler *ev,
              const char *type, struct blob_attr *msg)
{
    if(!strcmp(type, ST_UPDATE_SUPPORTED_DB))
    {
        SLOGI("zwave re-open supported database\n");
        if(support_devs_db)
        {
            sqlite3_close(support_devs_db);
        }

        open_database(SUPPORTED_DEVS_DATABASE, &support_devs_db);
    }
    else if(!strcmp(type, ST_POWER_CYCLE))
    {
        if(!g_inform_power_cycle)
        {
            SLOGI("ZWAVE POWER CYCLE\n");
            g_inform_power_cycle = 1;
            force_status();
        }
    }
}

static void zwave_ubus_service()
{
    int ret;

    ret = ubus_add_object(ctx, &zwave_object);
    if (ret)
        fprintf(stderr, "Failed to add object: %s\n", ubus_strerror(ret));

    memset(&ubus_event_listener, 0, sizeof(ubus_event_listener));
    ubus_event_listener.cb = handle_ubus_event;

    ret = ubus_register_event_handler(ctx, &ubus_event_listener, ST_UPDATE_SUPPORTED_DB);
    if (ret)
    {
        SLOGE("Failed to register event handler %s: %s\n", ST_UPDATE_SUPPORTED_DB, ubus_strerror(ret));
    }

    ret = ubus_register_event_handler(ctx, &ubus_event_listener, ST_POWER_CYCLE);
    if (ret)
    {
        SLOGE("Failed to register event handler %s: %s\n", ST_POWER_CYCLE, ubus_strerror(ret));
    }

    uloop_timeout_set(&polling_notify, 1000);

    uloop_run();
}

static int get_dev_status_callback(void *data, int argc, char **argv, char **azColName)
{    
    if(argc < 1)
    {
        return 0;
    }

    char *feature = argv[0];
    char *value = argv[1];
    zwave_dev_status_t *tmp = (zwave_dev_status_t *)data;

    if(!tmp)
    {
        return 0;
    }

    if(!strcmp(feature, ST_ON_OFF))
    {
        tmp->onOff = strtol(value, NULL, 0);
    }
    else if(!strcmp(feature, ST_DIM))
    {
        tmp->dim = strtol(value, NULL, 0);
    }
    else if(!strcmp(feature, ST_STATE))
    {
        if(!strcmp(value, ST_ALIVE))
        {
            tmp->state = 1;
        }
        else
        {
            tmp->state = 0;
        }
    }
    else if(!strcmp(feature, ST_LAST_UPDATE))
    {
        tmp->lastUpdate = strtoul(value, NULL, 0);
        tmp->lastPoll = tmp->lastUpdate;
    }
    else if(!strcmp(feature, ST_WAKE_UP_NOTIFICATION))
    {
        tmp->wakeUp = strtoul(value, NULL, 0);
    }
    else if(!strcmp(feature, ST_PRIORITY))
    {
        tmp->priority = strtoul(value, NULL, 0);
    }

    return 0;
}

static int get_origin_info(void *data, int argc, char **argv, char **azColName)
{
    if(!data)
    {
        return 0;
    }

    if(argc < 2)
    {
        return 0;
    }

    int i;
    char *sound = NULL;
    char *deviceType = NULL;

    for(i=0; i<argc;i++)
    {
        if(!strcmp(azColName[i], "Sound"))
        {
            sound = argv[i];
        }
        else if(!strcmp(azColName[i], "DeviceType"))
        {
            deviceType = argv[i];
        }
    }

    if(!sound || !deviceType)
    {
        return 0;
    }

    zwave_dev_info_t *zwave_dev = (zwave_dev_info_t *)data;
    strncpy(zwave_dev->VRDeviceType, deviceType, sizeof(zwave_dev->VRDeviceType)-1);
    strncpy(zwave_dev->defaultSound, sound, sizeof(zwave_dev->defaultSound)-1);

    return 0;
}

zwave_dev_info_t *_create_zwave_dev_(char *serialId, char *deviceType, char *deviceMode, char *id, char *endpointNum,
                                    char *parentId, char *childrenId, char *active, char *cloudId, int isAdding)
{
    if(!id)
    {
        return NULL;
    }

    zwave_dev_info_t *zwave_dev = (zwave_dev_info_t *)(malloc)(sizeof(zwave_dev_info_t));
    memset(zwave_dev, 0x00, sizeof(zwave_dev_info_t));
    zwave_dev->localId = htoi(id);
    zwave_dev->deviceType = htoi(deviceType);
    zwave_dev->deviceMode = htoi(deviceMode);
    
    zwave_dev->endpointNum = htoi(endpointNum);
    zwave_dev->parentId = htoi(parentId);

    if(serialId)
    {
        strncpy(zwave_dev->serialId, serialId, sizeof(zwave_dev->serialId)-1);
        searching_database("zwave_handler", support_devs_db, get_origin_info, zwave_dev, 
                            "SELECT Sound,DeviceType from SUPPORT_DEVS where SerialID='%s'", 
                            serialId);
    }

    if(childrenId)
    {
        strncpy(zwave_dev->childrenId, childrenId, sizeof(zwave_dev->childrenId)-1);
    }

    if(!strcmp(active, ST_YES)) 
    {
        zwave_dev->active = 1;
    }
    else
    {
        zwave_dev->active = 0;
    }

    if(cloudId)
    {
        strncpy(zwave_dev->cloudId, cloudId, sizeof(zwave_dev->cloudId)-1);
    }
    else
    {
        SEARCH_DATA_INIT_VAR(cloudId_db);
        searching_database(_VR_CB_(zwave), &cloudId_db,  
                        "SELECT owner "
                        "from DEVICES where deviceId='%s' and deviceType='%s'",
                        id, ST_ZWAVE);
        if(cloudId_db.len)
        {
            strncpy(zwave_dev->cloudId, cloudId_db.value, sizeof(zwave_dev->cloudId)-1);
        }
        FREE_SEARCH_DATA_VAR(cloudId_db);
    }

    json_object *json_capId = NULL;
    get_capId_and_scheme_dev(id, &json_capId);
    if(json_capId)
    {
        zwave_dev->json_capId = json_capId;
    }

    zwave_dev->status.priority = INIT_PRIORITY_NUM;

    if(!isAdding)
    {
        searching_database(_VR_(zwave), get_dev_status_callback, &(zwave_dev->status),
                    "SELECT featureId,register from FEATURES where deviceId='%s'"
                    "AND featureId IN ('%s', '%s', '%s', '%s', '%s', '%s')", id, ST_STATE,
                    ST_WAKE_UP_NOTIFICATION, ST_LAST_UPDATE, ST_ON_OFF, ST_DIM, ST_PRIORITY);
    }
    
    int res = add_last_list((void *)zwave_dev);
    if(res)
    {
        if(zwave_dev->json_capId)
        {
            json_object_put(zwave_dev->json_capId);
        }
        SAFE_FREE(zwave_dev);
        zwave_dev = NULL;
    }

    insert_specific_dev_list(id, serialId);
    return zwave_dev;
}

static int init_dev_info_db_callback(void *data, int argc, char **argv, char **azColName)
{
    int i;
    char *serialId = NULL;
    char *deviceType = NULL;
    char *deviceMode = NULL;
    char *id = NULL;
    char *endpointNum = NULL;
    char *parentId = NULL;
    char *childrenId = NULL;
    char *active = NULL;

    for(i=0; i<argc;i++)
    {
        // printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
        if(!strcmp(azColName[i], ST_SERIAL_ID))
        {
            serialId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_DEVICE_TYPE))
        {
            deviceType = argv[i];
        }
        else if(!strcmp(azColName[i], ST_DEVICE_MODE))
        {
            deviceMode = argv[i];
        }
        else if(!strcmp(azColName[i], ST_ID))
        {
            id = argv[i];
        }
        else if(!strcmp(azColName[i], ST_ENDPOINT_NUM))
        {
            endpointNum = argv[i];
        }
        else if(!strcmp(azColName[i], ST_PARENT_ID))
        {
            parentId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_CHILDREN_ID))
        {
            childrenId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_ACTIVE))
        {
            active = argv[i];
        }
    }

    if(id)
    {
        zwave_dev_info_t *zwave_dev = _create_zwave_dev_(serialId, deviceType, deviceMode, id, endpointNum,
                                                        parentId, childrenId, active, NULL, 0);
        int zwave_polling_interval, cmdPolling;
        dev_specific_zwave_polling_interval(id, &zwave_polling_interval, &cmdPolling);
        if(zwave_polling_interval)
        {
            addDeviceToPollingList((uint8_t)htoi(id), htoi(endpointNum), zwave_polling_interval, cmdPolling);
        }

        if(zwave_dev)
        {
            zwave_dev->speakerAlarm = ALARM_VIA_SPEAKER_NO_DATA;
        }

        SEARCH_DATA_INIT_VAR(sound_alarm_enable);
        searching_database(_VR_CB_(zwave), &sound_alarm_enable,
                            "SELECT register from FEATURES where deviceId='%s' and featureId='%s'",
                            id, ST_SOUND_ALARM_ENABLE);
        if(sound_alarm_enable.len)
        {
            if(zwave_dev)
            {
                if(!strcmp(sound_alarm_enable.value, ST_OFF_VALUE))
                {
                    zwave_dev->speakerAlarm = ALARM_VIA_SPEAKER_DISABLE;
                }
                else
                {
                    zwave_dev->speakerAlarm = ALARM_VIA_SPEAKER_ENABLE;
                }
            } 
        }
        FREE_SEARCH_DATA_VAR(sound_alarm_enable);

        SEARCH_DATA_INIT_VAR(temporary_disable_time);
        searching_database(_VR_CB_(zwave), &temporary_disable_time,
                            "SELECT register from FEATURES where deviceId='%s' and featureId='%s'",
                            id, ST_TEMPORARY_ALARM_TIME);
        if(temporary_disable_time.len)
        {
            if(zwave_dev)
            {
                zwave_dev->temporary_disable_alarm_time = strtoul(temporary_disable_time.value, NULL, 0);
            }
        }
        FREE_SEARCH_DATA_VAR(temporary_disable_time);

        if(data)
        {
            data = (void *)&zwave_dev;
        }
    }
    return 0;
}

static void get_dev_info_from_database()
{
    DISABLE_DATABASE_LOG();
    searching_database(_VR_(zwave), init_dev_info_db_callback, NULL,
                        "SELECT serialId,deviceType,deviceMode,id,endpointNum,parentId,childrenId,active "
                        "from SUB_DEVICES where owner='%08X%02X'",
                        zw_OwnerID.HomeID, zw_OwnerID.MyNodeID);
    ENABLE_DATABASE_LOG();
}

zwave_dev_info_t *get_zwave_dev_from_id(char *id)
{
    if(!id)
    {
        return NULL;
    }

    zwave_dev_info_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &(g_zwave_dev_list.list), list)
    {
        if(tmp->localId == htoi(id))
        {
            return tmp;
        }
    }

    return NULL;
}

zwave_dev_info_t *create_zwave_dev_from_id(char *id)
{
    zwave_dev_info_t *zwave_dev = NULL;
    searching_database(_VR_(zwave), init_dev_info_db_callback, &zwave_dev,
                        "SELECT serialId,deviceType,id,endpointNum,parentId,childrenId,active "
                        "from SUB_DEVICES where owner='%08X%02X' AND id='%s'",
                        zw_OwnerID.HomeID, zw_OwnerID.MyNodeID, id);
    return zwave_dev;
}

void remove_zwave_dev_from_id(char *id)
{
    remove_list(id);
}

void remove_zwave_dev_lists()
{
    pthread_mutex_lock(&zwave_dev_info_listMutex);
    zwave_dev_info_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_zwave_dev_list.list)
    {
        tmp = VR_(list_entry)(pos, zwave_dev_info_t, list);
        
        VR_(list_del)(pos);
        if(tmp->json_capId)
        {
            json_object_put(tmp->json_capId);
        }
        free(tmp);
        
    }
    pthread_mutex_unlock(&zwave_dev_info_listMutex);
}

void update_zwave_dev_from_id(char *id, char *input, char *value)
{
    if(!input || !value)
    {
        return;
    }

    zwave_dev_info_t *tmp = get_zwave_dev_from_id(id);
    if(!tmp)
    {
        return;
    }

    if(!strcmp(input, ST_ON_OFF))
    {
        tmp->status.onOff = strtol(value, NULL, 0);
    }
    else if(!strcmp(input, ST_DIM))
    {
        tmp->status.dim = strtol(value, NULL, 0);
    }
    else if(!strcmp(input, ST_STATE))
    {
        if(!strcmp(value, ST_ALIVE))
        {
            tmp->status.state = 1;
        }
        else
        {
            tmp->status.state = 0;
        }

    }
    else if(!strcmp(input, ST_LAST_UPDATE))
    {
        tmp->status.lastUpdate = strtoul(value, NULL, 0);
    }
    else if(!strcmp(input, ST_WAKE_UP_NOTIFICATION))
    {
        tmp->status.wakeUp = strtoul(value, NULL, 0);
    }
    else if(!strcmp(input, ST_THERMOSTAT_MODE))
    {
        // tmp->status.onOff = strtol(value, NULL, 0);
        // SLOGI("%s status.onOff = %d\n", id, tmp->status.onOff);
        if(!strcmp(input, ST_OFF))
        {
            tmp->status.onOff = 0;
        }
        else
        {
            tmp->status.onOff = 1;
        }
    }
    else if(!strcmp(input, ST_CHILDREN_ID))
    {
        strncpy(tmp->childrenId, value, sizeof(tmp->childrenId)-1);
    }
    else if(!strcmp(input, ST_PRIORITY))
    {
        tmp->status.priority = strtol(value, NULL, 0);
    }
}

/*update database and link list*/
void update_zwave_status(char* service_name, sqlite3 *db, db_callback callback, 
                        const char *ID, const char* feature, const char* value,
                        const char* mode, int rotate_num)
{
    set_register_database(service_name, db, callback, ID, feature, value, mode, rotate_num);
    update_zwave_dev_from_id((char*) ID, (char*) feature, (char*) value);
}

// void printf_dev_info_list()
// {
//     int i=0;
//     zwave_dev_info_t *tmp = NULL;
    
//     VR_(list_for_each_entry)(tmp, &g_zwave_dev_list.list, list)
//     {
//         if(!tmp->totalCmd && !tmp->cmdComplete)
//         {
//             continue;
//         }

//         printf("#### dev %d ######\n", i);
//         printf("localId = %04X\n", tmp->localId);
//         printf("deviceType = %02X\n", tmp->deviceType);
//         printf("active = %d\n", tmp->active);
//         printf("endpointNum = %02X\n", tmp->endpointNum);
//         printf("parentId = %02X\n", tmp->parentId);
//         printf("serialId = %s\n", tmp->serialId);
//         printf("cloudId = %s\n", tmp->cloudId);
//         printf("childrenId = %s\n", tmp->childrenId?tmp->childrenId: "NULL");
//         printf("json_capId = %s\n", tmp->json_capId?json_object_to_json_string(tmp->json_capId):"NULL");
//         printf("status.onoff = %d\n", tmp->status.onOff);
//         printf("status.dim = %d\n", tmp->status.dim);
//         printf("status.state = %d\n", tmp->status.state);
//         printf("status.lastUpdate = %d\n", tmp->status.lastUpdate);
//         printf("status.wakeUp = %d\n", tmp->status.wakeUp);
//         printf("totalCmd = %d\n", tmp->totalCmd);
//         printf("cmdComplete = %d\n", tmp->cmdComplete);
//         i++;
//     }
// }

// >= 0 no need update.
int is_zwave_controller_updated()
{
    int ret = 1;
    char *version = get_zwave_controller_version();
    if(!version)
    {
        SLOGE("cant get version from zwave controller\n");
        return ret;
    }

    SLOGI("zwave controller version %s\n", version);
    SLOGI("newest version %s\n", ZWAVE_CONTROLLER_VERSION);

    ret = VR_(version_cmp)(version, ZWAVE_CONTROLLER_VERSION);
    free(version);
    return ret;
}

static void update_zwave_controll_info()
{
    zwaveControllerInfo(&zw_OwnerID);

    SEARCH_DATA_INIT_VAR(zwave_controller_id);
    searching_database(_VR_CB_(zwave), &zwave_controller_id,  
                        "SELECT udn from CONTROL_DEVS where udn='%08X%02X'", 
                        zw_OwnerID.HomeID, zw_OwnerID.MyNodeID);
    if(!zwave_controller_id.len)
    {
        database_actions(_VR_(zwave), "INSERT INTO %s VALUES('%s','%s','%08X%02X', '%s')",
                    "CONTROL_DEVS (realName,friendlyName,udn,type)",
                    "zwave_controller", "zwave_venus", 
                    zw_OwnerID.HomeID, zw_OwnerID.MyNodeID, ST_ZWAVE);
    }
    else
    {
        char zwaveControllerId[SIZE_64B];
        sprintf(zwaveControllerId, "%08X%02X", zw_OwnerID.HomeID, zw_OwnerID.MyNodeID);
        if(strcmp(zwaveControllerId, zwave_controller_id.value))
        {
            database_actions(_VR_(zwave), "UPDATE CONTROL_DEVS SET udn='%s' where realName='%s'",
                                        zwaveControllerId, "zwave_controller");
        }
    }
    FREE_SEARCH_DATA_VAR(zwave_controller_id);
}

int get_firmware(void)
{
    int ret;
    SLOGI("start update zwave controller firmware %s\n", ZWAVE_CONTROLLER_VERSION);
    char *firmwarePath;
    char device_ver[SIZE_32B];

    VR_(run_command)(GET_BOARD_VER_COMMAND, device_ver, sizeof(device_ver));
    SLOGE("Device rev : %s ", device_ver);
    int version = strtol(device_ver, NULL, 10);

    if (version > HW_REV_E)
    {
        firmwarePath = (char*)malloc(strlen(ZWAVE_CONTROLLER_FIRMWARE_ZM5101)+SIZE_32B);
        sprintf(firmwarePath, ZWAVE_CONTROLLER_FIRMWARE_ZM5101, ZWAVE_CONTROLLER_VERSION);
        SLOGE("Go to Device rev F %s ", firmwarePath);
    }
    else
    {
        firmwarePath = (char*)malloc(strlen(ZWAVE_CONTROLLER_FIRMWARE_ZM5304)+SIZE_32B);
        sprintf(firmwarePath, ZWAVE_CONTROLLER_FIRMWARE_ZM5304, ZWAVE_CONTROLLER_VERSION);
        SLOGE("Go to Device rev #F %s ", firmwarePath);
    }


    ret = zwave_update_firmwre(firmwarePath);
    if(0 != ret)
    {
        VR_(execute_system)("ubus send zwave '{\"state\":\"uart_failed\"}' &");
        free(firmwarePath);
        return -1;
    }
    free(firmwarePath);

    return 0;

    
}

int main(int argc, char *argv[])
{
    int c;
    char *pdevID = NULL;
    gLogLevel=2;
    struct sigaction action;

    SLOG_Init(SLOG_STDOUT, NULL);
    set_thread_name("zwave-service");
    
    char cmd[256];
    memset(cmd, 0x00, sizeof(cmd));
    sprintf(cmd, "%s", "pgrep zwave_handler | head -n -1 | cut -c-5 | xargs kill -9 &>/dev/null");
    system(cmd);

    // unsigned char buf[SIZE_512B];
    // read_eeprom(0x0001, buf, 10);

    // printf("buf = %s\n", buf);

    while(1) {
        static struct option long_options[] =
            {
              /* These options don’t set a flag.
                 We distinguish them by their indices. */
              {"help",  no_argument,       0, 'h'},
              {"port",  required_argument, 0, 'p'},
              {"loglevel",  required_argument, 0, 'l'},
              {"logPolling",  required_argument, 0, 'z'},
              {0, 0, 0, 0}
            };
            /* getopt_long stores the option index here. */
            int option_index = 0;

             c = getopt_long (argc, argv, "hp:lz",
                            long_options, &option_index);

            /* Detect the end of the options. */
            if (c == -1)
                break;

            switch(c) {
            case 'h':
                printf("Usage: zwavehandler-cli [options]\n");
                printf("Options:\n");
                printf("--help(-h): print help information\n");
                printf("--port(-p): specify the dedicated port to control zwave controller.\n");
                printf("--log(-l): print log debug.\n");
                printf("--log(-z): print log polling\n");
                printf("Without options, the default port is used to connect to zwave controller\n");
                exit(EXIT_SUCCESS);
            case 'p':
                pdevID =(char *)malloc(strlen(optarg));
                strncpy(pdevID, optarg, strlen(optarg));
                break;
            case 'l':
                gLogLevel = logDebug;
                break;
            case 'z':
                gLogLevel = logPolling;
                break;
            default:
                printf("Invalid option, please use --help or -h to know how to use\n");
                exit(EXIT_SUCCESS);
                break;
        }
    }

    if (pdevID == NULL) 
    {
        pdevID = (char *)malloc(strlen(ZWAVE_CONSOLE_PORT_DEFAULT));
        strncpy(pdevID, ZWAVE_CONSOLE_PORT_DEFAULT, strlen(ZWAVE_CONSOLE_PORT_DEFAULT)); 
    }
    if(-1 == controller_enable())
    {
        controller_reset();
        return -1;
    }

    sleep(2);

    memset(&action, 0, sizeof(action));
    action.sa_flags = (SA_NOCLDSTOP | SA_NOCLDWAIT | SA_RESTART);
    action.sa_handler = sig_handler;
    sigaction(SIGSEGV, &action, NULL);
    sigaction(SIGTERM, &action, NULL);
    sigaction(SIGINT, &action, NULL);

    timerInit(); // sleep function will not work after init timer.
    int ret = zwaveInitialize(pdevID, &g_notify);
    if(-1 == ret)//uart failed
    {
        SLOGE("FAILED TO INITIALIZE UART\n");
        VR_(execute_system)("ubus send zwave '{\"state\":\"uart_failed\"}' &");
        return -1;
    }
    else if(1 == ret) //firmware failed
    {
        if ((ret = get_firmware()) !=0) return ret; 
    }

    if(is_zwave_controller_updated() < 0)
    {
        if ((ret = get_firmware()) !=0) return ret; 
    }

    open_database(DEVICES_DATABASE, &zwave_db);
    open_database(SUPPORTED_DEVS_DATABASE, &support_devs_db);

    update_zwave_controll_info();

    init_zwave_dev_list();
    init_dev_adding_actions_list();
    init_dev_specific_list();
    init_queue_table();
    init_user_code_list();
    // test_insert_queue();
    // test_decice_insert();

    pthread_create(&zwave_command_process_thread, NULL, (void *)&zwave_command_process, NULL);
    pthread_detach(zwave_command_process_thread);

    pthread_attr_t tattr;
    pthread_attr_init(&tattr);
    pthread_attr_setstacksize(&tattr, 17000);
    pthread_t manage_user_code_thread_t;
    pthread_create(&manage_user_code_thread_t, &tattr, (void *)&manage_user_code_pending_thread, NULL);
    pthread_detach(manage_user_code_thread_t);

    if(shm_init(&g_shm, &g_shmid))
    {
        SLOGI("Failed to init share memory\n");
        return 0;
    }

    // send close when restart service
    VR_(execute_system)("ubus send zwave '{\"state\":\"close_network\"}' &");

    get_dev_info_from_database();

    init_ubus_service();
    zwave_ubus_service();
    free_ubus_service();

    controller_reset();

    if(pdevID)
    {
        free(pdevID);
    }

    printf("################## END ZWAVE ####################\n");
    return 0;
}