#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "dev_state.h"
#include "alarm_meaning.h"
#include "VR_define.h"
#include "slog.h"
#include "unsupport_name.h"

/*#####################################################################################*/
/*###########################        SENSOR MULTILEVEL        #########################*/
/*#####################################################################################*/
source_select_t temperature_value[]=
{
    {MULTILEVEL_SENSOR_LABEL_CELCIUS,                      ST_MULTILEVEL_SENSOR_LABEL_CELCIUS}, 
    {MULTILEVEL_SENSOR_LABEL_FAHRENHEIT,                   ST_MULTILEVEL_SENSOR_LABEL_FAHRENHEIT},
};

source_select_t general_purpose[]=
{
    {MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE,             ST_MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE}, 
    {MULTILEVEL_SENSOR_LABEL_DIMENSIONLESS_VALUE,          ST_MULTILEVEL_SENSOR_LABEL_DIMENSIONLESS_VALUE},
};

source_select_t luminance[]=
{
    {MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE,             ST_MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE}, 
    {MULTILEVEL_SENSOR_LABEL_LUX,                          ST_MULTILEVEL_SENSOR_LABEL_LUX},
};

source_select_t power[]=
{
    {MULTILEVEL_SENSOR_LABEL_WATT,                         ST_MULTILEVEL_SENSOR_LABEL_WATT}, 
    {MULTILEVEL_SENSOR_LABEL_BTU,                          ST_MULTILEVEL_SENSOR_LABEL_BTU},
};

source_select_t humidity[]=
{
    {MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE,             ST_MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE}, 
    {MULTILEVEL_SENSOR_LABEL_ABSOLUTE,                     ST_MULTILEVEL_SENSOR_LABEL_ABSOLUTE},
};

source_select_t velocity[]=
{
    {MULTILEVEL_SENSOR_LABEL_MS,                           ST_MULTILEVEL_SENSOR_LABEL_MS}, 
    {MULTILEVEL_SENSOR_LABEL_MPH,                          ST_MULTILEVEL_SENSOR_LABEL_MPH},
};
/*0 = no win,90 = east, 180 = south, 270 = west, 360 = north*/
source_select_t direction[]=
{
    {MULTILEVEL_SENSOR_LABEL_DEGREES,                      ST_MULTILEVEL_SENSOR_LABEL_DEGREES},
};

source_select_t pressure_value[]=
{
    {MULTILEVEL_SENSOR_LABEL_KILOPASCAL,                   ST_MULTILEVEL_SENSOR_LABEL_KILOPASCAL}, 
    {MULTILEVEL_SENSOR_LABEL_INCHES_OF_MERCURY,            ST_MULTILEVEL_SENSOR_LABEL_INCHES_OF_MERCURY},
};

source_select_t solar_radiation[]=
{
    {MULTILEVEL_SENSOR_LABEL_WATT_PER_SQUARE,              ST_MULTILEVEL_SENSOR_LABEL_WATT_PER_SQUARE}, 
};

source_select_t rain_rate[]=
{
    {MULTILEVEL_SENSOR_LABEL_MILLIMETER,                   ST_MULTILEVEL_SENSOR_LABEL_MILLIMETER}, 
    {MULTILEVEL_SENSOR_LABEL_INCHES_PER_HOUR,              ST_MULTILEVEL_SENSOR_LABEL_INCHES_PER_HOUR},
};

source_select_t tide_level[]=
{
    {MULTILEVEL_SENSOR_LABEL_METER,                        ST_MULTILEVEL_SENSOR_LABEL_METER}, 
    {MULTILEVEL_SENSOR_LABEL_FEET,                         ST_MULTILEVEL_SENSOR_LABEL_FEET},
};

source_select_t weight[]=
{
    {MULTILEVEL_SENSOR_LABEL_KILOGRAM,                     ST_MULTILEVEL_SENSOR_LABEL_KILOGRAM}, 
    {MULTILEVEL_SENSOR_LABEL_POUNDS,                       ST_MULTILEVEL_SENSOR_LABEL_POUNDS},
};

source_select_t voltage[]=
{
    {MULTILEVEL_SENSOR_LABEL_VOLT,                         ST_MULTILEVEL_SENSOR_LABEL_VOLT}, 
    {MULTILEVEL_SENSOR_LABEL_MILLIVOLT,                    ST_MULTILEVEL_SENSOR_LABEL_MILLIVOLT},
};

source_select_t current[]=
{
    {MULTILEVEL_SENSOR_LABEL_AMPERE,                       ST_MULTILEVEL_SENSOR_LABEL_AMPERE}, 
    {MULTILEVEL_SENSOR_LABEL_MILLIAMPERE,                  ST_MULTILEVEL_SENSOR_LABEL_MILLIAMPERE},
};

source_select_t carbon_dioxide[]=
{
    {MULTILEVEL_SENSOR_LABEL_PARTS_MILLION,                ST_MULTILEVEL_SENSOR_LABEL_PARTS_MILLION}, 
};

source_select_t air_flow[]=
{
    {MULTILEVEL_SENSOR_LABEL_CUBIC_METER_PER_HOUR,         ST_MULTILEVEL_SENSOR_LABEL_CUBIC_METER_PER_HOUR}, 
    {MULTILEVEL_SENSOR_LABEL_CUBIC_FEET_PER_MINUTE,        ST_MULTILEVEL_SENSOR_LABEL_CUBIC_FEET_PER_MINUTE},
};

source_select_t tank_capacity[]=
{
    {MULTILEVEL_SENSOR_LABEL_LITER,                         ST_MULTILEVEL_SENSOR_LABEL_LITER}, 
    {MULTILEVEL_SENSOR_LABEL_CUBIC_METER,                   ST_MULTILEVEL_SENSOR_LABEL_CUBIC_METER},
    {MULTILEVEL_SENSOR_LABEL_GALLONS,                       ST_MULTILEVEL_SENSOR_LABEL_GALLONS},
};

source_select_t distance[]=
{
    {MULTILEVEL_SENSOR_LABEL_METER,                         ST_MULTILEVEL_SENSOR_LABEL_METER}, 
    {MULTILEVEL_SENSOR_LABEL_CENTIMETER,                    ST_MULTILEVEL_SENSOR_LABEL_CENTIMETER},
    {MULTILEVEL_SENSOR_LABEL_DISTANCE_FEET,                 ST_MULTILEVEL_SENSOR_LABEL_FEET},
};

source_select_t angle_position[]=
{
    {MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE,              ST_MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE}, 
    {MULTILEVEL_SENSOR_LABEL_DEGREES_RELATIVE_NORTH_1,      ST_MULTILEVEL_SENSOR_LABEL_DEGREES_RELATIVE_NORTH},
    {MULTILEVEL_SENSOR_LABEL_DEGREES_RELATIVE_NORTH_2,      ST_MULTILEVEL_SENSOR_LABEL_DEGREES_RELATIVE_NORTH},
};

source_select_t rotation[]=
{
    {MULTILEVEL_SENSOR_LABEL_REVOLUTIONS_PER_MINUTE,        ST_MULTILEVEL_SENSOR_LABEL_REVOLUTIONS_PER_MINUTE}, 
    {MULTILEVEL_SENSOR_LABEL_HERTZ,                         ST_MULTILEVEL_SENSOR_LABEL_HERTZ},
};

source_select_t seismic_intensity[]=
{
    {MULTILEVEL_SENSOR_LABEL_MERCALLI,                      ST_MULTILEVEL_SENSOR_LABEL_MERCALLI}, 
    {MULTILEVEL_SENSOR_LABEL_EUROPEAN_MACROSEISMIC,         ST_MULTILEVEL_SENSOR_LABEL_EUROPEAN_MACROSEISMIC},
    {MULTILEVEL_SENSOR_LABEL_LIEDU,                         ST_MULTILEVEL_SENSOR_LABEL_LIEDU}, 
    {MULTILEVEL_SENSOR_LABEL_SHINDO,                        ST_MULTILEVEL_SENSOR_LABEL_SHINDO},
};

source_select_t seismic_magnitude[]=
{
    {MULTILEVEL_SENSOR_LABEL_LOCAL,                         ST_MULTILEVEL_SENSOR_LABEL_LOCAL}, 
    {MULTILEVEL_SENSOR_LABEL_MOMENT,                        ST_MULTILEVEL_SENSOR_LABEL_MOMENT},
    {MULTILEVEL_SENSOR_LABEL_SURFACE_WAVE,                  ST_MULTILEVEL_SENSOR_LABEL_SURFACE_WAVE}, 
    {MULTILEVEL_SENSOR_LABEL_BODY_WAVE,                     ST_MULTILEVEL_SENSOR_LABEL_BODY_WAVE},
};

source_select_t ultraviolet[]=
{
    {MULTILEVEL_SENSOR_LABEL_UV_INDEX,                      ST_MULTILEVEL_SENSOR_LABEL_UV_INDEX}, 
};

source_select_t electrical_resistivity[]=
{
    {MULTILEVEL_SENSOR_LABEL_OHM_METER,                     ST_MULTILEVEL_SENSOR_LABEL_OHM_METER}, 
};

source_select_t electrical_conductivity[]=
{
    {MULTILEVEL_SENSOR_LABEL_SIEMENS_PER_METER,             ST_MULTILEVEL_SENSOR_LABEL_SIEMENS_PER_METER}, 
};

source_select_t loudness[]=
{
    {MULTILEVEL_SENSOR_LABEL_DECIBEL,                       ST_MULTILEVEL_SENSOR_LABEL_DECIBEL}, 
    {MULTILEVEL_SENSOR_LABEL_WEIGHTED_DECIBEL,              ST_MULTILEVEL_SENSOR_LABEL_WEIGHTED_DECIBEL},
};

source_select_t moisture[]=
{
    {MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE,              ST_MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE}, 
    {MULTILEVEL_SENSOR_LABEL_VOLUME_WATER_CONTENT,          ST_MULTILEVEL_SENSOR_LABEL_VOLUME_WATER_CONTENT},
    {MULTILEVEL_SENSOR_LABEL_IMPEDANCE,                     ST_MULTILEVEL_SENSOR_LABEL_IMPEDANCE},
    {MULTILEVEL_SENSOR_LABEL_WATER_ACTIVITY,                ST_MULTILEVEL_SENSOR_LABEL_WATER_ACTIVITY},
};

source_select_t frequency[]=
{
    {MULTILEVEL_SENSOR_LABEL_FREQUENCY_HERTZ,               ST_MULTILEVEL_SENSOR_LABEL_HERTZ}, 
    {MULTILEVEL_SENSOR_LABEL_KILOHERTZ,                     ST_MULTILEVEL_SENSOR_LABEL_KILOHERTZ},
};

source_select_t label_time[]=
{
    {MULTILEVEL_SENSOR_LABEL_SECOND,                        ST_MULTILEVEL_SENSOR_LABEL_SECOND}, 
};

source_select_t particulate_matter_25[]=
{
    {MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER,          ST_MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER}, 
    {MULTILEVEL_SENSOR_LABEL_MICROGRAM_PER_CUBIC_METER,     ST_MULTILEVEL_SENSOR_LABEL_MICROGRAM_PER_CUBIC_METER},
};

source_select_t formaldehyde_CH2O[]=
{
    {MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER,          ST_MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER}, 
};

source_select_t radon_concentration[]=
{
    {MULTILEVEL_SENSOR_LABEL_BECQUEREL_PER_CUBIC_METER,     ST_MULTILEVEL_SENSOR_LABEL_BECQUEREL_PER_CUBIC_METER}, 
    {MULTILEVEL_SENSOR_LABEL_PICOCURIES_PER_LITER,          ST_MULTILEVEL_SENSOR_LABEL_PICOCURIES_PER_LITER},
};

source_select_t methane_density[]=
{
    {MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER,          ST_MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER},
};

source_select_t volatile_organic_compound_level[]=
{
    {MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER,          ST_MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER}, 
    {MULTILEVEL_SENSOR_LABEL_ORGANIC_PARTS_MILLION,         ST_MULTILEVEL_SENSOR_LABEL_PARTS_MILLION},
};

source_select_t carbon_monoxide_CO[]=
{
    {MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER,          ST_MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER}, 
    {MULTILEVEL_SENSOR_LABEL_ORGANIC_PARTS_MILLION,         ST_MULTILEVEL_SENSOR_LABEL_PARTS_MILLION},
};

source_select_t soil_humidity[]=
{
    {MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE,              ST_MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE}, 
};

source_select_t soil_reactivity[]=
{
    {MULTILEVEL_SENSOR_LABEL_ACIDITY,                       ST_MULTILEVEL_SENSOR_LABEL_ACIDITY}, 
};

source_select_t soil_sanlinity[]=
{
    {MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER,          ST_MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER}, 
};

source_select_t heart_rate[]=
{
    {MULTILEVEL_SENSOR_LABEL_BEATS_PER_MINUTE,              ST_MULTILEVEL_SENSOR_LABEL_BEATS_PER_MINUTE}, 
};

source_select_t blood_pressure[]=
{
    {MULTILEVEL_SENSOR_LABEL_SYSTOLIC,                      ST_MULTILEVEL_SENSOR_LABEL_SYSTOLIC}, 
    {MULTILEVEL_SENSOR_LABEL_DIASTOLIC,                     ST_MULTILEVEL_SENSOR_LABEL_DIASTOLIC},
};

source_select_t mass[]=
{
    {MULTILEVEL_SENSOR_LABEL_KILOGRAM,                      ST_MULTILEVEL_SENSOR_LABEL_KILOGRAM}, 
};

source_select_t basis_metabolic[]=
{
    {MULTILEVEL_SENSOR_LABEL_JOULE,                         ST_MULTILEVEL_SENSOR_LABEL_JOULE}, 
};

source_select_t body_mass[]=
{
    {MULTILEVEL_SENSOR_LABEL_BMI_INDEX,                     ST_MULTILEVEL_SENSOR_LABEL_BMI_INDEX}, 
};

source_select_t acceleration[]=
{
    {MULTILEVEL_SENSOR_LABEL_METER_PER_SQUARE_SECOND,       ST_MULTILEVEL_SENSOR_LABEL_METER_PER_SQUARE_SECOND}, 
};

source_select_t smoke_density[]=
{
    {MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE,              ST_MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE}, 
};

source_select_t water_flow[]=
{
    {MULTILEVEL_SENSOR_LABEL_LITER_PER_HOUR,                ST_MULTILEVEL_SENSOR_LABEL_LITER_PER_HOUR}, 
};

source_select_t water_pressure[]=
{
    {MULTILEVEL_SENSOR_LABEL_KILOPASCAL,                    ST_MULTILEVEL_SENSOR_LABEL_KILOPASCAL}, 
};

source_select_t rf_signal_strength[]=
{
    {MULTILEVEL_SENSOR_LABEL_RSSI,                          ST_MULTILEVEL_SENSOR_LABEL_RSSI}, 
    {MULTILEVEL_SENSOR_LABEL_DBM,                           ST_MULTILEVEL_SENSOR_LABEL_DBM},
};

source_select_t particulate_matter_10[]=
{
    {MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER,         ST_MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER}, 
    {MULTILEVEL_SENSOR_LABEL_MICROGRAM_PER_CUBIC_METER,    ST_MULTILEVEL_SENSOR_LABEL_MICROGRAM_PER_CUBIC_METER},
};

source_select_t respiratory_rate[]=
{
    {MULTILEVEL_SENSOR_LABEL_BREATHS_PER_MINUTE,            ST_MULTILEVEL_SENSOR_LABEL_BREATHS_PER_MINUTE}, 
};

source_select_t relative_modulation[]=
{
    {MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE,              ST_MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE}, 
};

class_infor_t sensorMultilevelTable[]=
{
    {MULTILEVEL_SENSOR_AIR_TEMPERATURE,                 ST_MULTILEVEL_SENSOR_AIR_TEMPERATURE,           sizeof(temperature_value)/sizeof(source_select_t),  temperature_value},
    {MULTILEVEL_SENSOR_GENERAL_PURPOSE,                 ST_MULTILEVEL_SENSOR_GENERAL_PURPOSE,           sizeof(general_purpose)/sizeof(source_select_t),    general_purpose},
    {MULTILEVEL_SENSOR_LUMINANCE,                       ST_MULTILEVEL_SENSOR_LUMINANCE,                 sizeof(luminance)/sizeof(source_select_t),          luminance},
    {MULTILEVEL_SENSOR_POWER,                           ST_MULTILEVEL_SENSOR_POWER,                     sizeof(power)/sizeof(source_select_t),              power},
    {MULTILEVEL_SENSOR_HUMIDITY,                        ST_MULTILEVEL_SENSOR_HUMIDITY,                  sizeof(humidity)/sizeof(source_select_t),           humidity},
    {MULTILEVEL_SENSOR_VELOCITY,                        ST_MULTILEVEL_SENSOR_VELOCITY,                  sizeof(velocity)/sizeof(source_select_t),           velocity},
    {MULTILEVEL_SENSOR_DIRECTION,                       ST_MULTILEVEL_SENSOR_DIRECTION,                 sizeof(direction)/sizeof(source_select_t),          direction},
    {MULTILEVEL_SENSOR_ATMOSPHERIC_PRESSURE,            ST_MULTILEVEL_SENSOR_ATMOSPHERIC_PRESSURE,      sizeof(pressure_value)/sizeof(source_select_t),     pressure_value},
    {MULTILEVEL_SENSOR_BAROMETRIC_PRESSURE,             ST_MULTILEVEL_SENSOR_BAROMETRIC_PRESSURE,       sizeof(pressure_value)/sizeof(source_select_t),     pressure_value},
    {MULTILEVEL_SENSOR_SOLAR_RADIATION,                 ST_MULTILEVEL_SENSOR_SOLAR_RADIATION,           sizeof(solar_radiation)/sizeof(source_select_t),    solar_radiation},
    {MULTILEVEL_SENSOR_DEW_POINT,                       ST_MULTILEVEL_SENSOR_DEW_POINT,                 sizeof(temperature_value)/sizeof(source_select_t),  temperature_value},
    {MULTILEVEL_SENSOR_RAIN_RATE,                       ST_MULTILEVEL_SENSOR_RAIN_RATE,                 sizeof(rain_rate)/sizeof(source_select_t),          rain_rate},
    {MULTILEVEL_SENSOR_TIDE_LEVEL,                      ST_MULTILEVEL_SENSOR_TIDE_LEVEL,                sizeof(tide_level)/sizeof(source_select_t),         tide_level},
    {MULTILEVEL_SENSOR_WEIGHT,                          ST_MULTILEVEL_SENSOR_WEIGHT,                    sizeof(weight)/sizeof(source_select_t),             weight},
    {MULTILEVEL_SENSOR_VOLTAGE,                         ST_MULTILEVEL_SENSOR_VOLTAGE,                   sizeof(voltage)/sizeof(source_select_t),            voltage},
    {MULTILEVEL_SENSOR_CURRENT,                         ST_MULTILEVEL_SENSOR_CURRENT,                   sizeof(current)/sizeof(source_select_t),            current},
    {MULTILEVEL_SENSOR_CARBON_DIOXIDE_CO2_LEVEL,        ST_MULTILEVEL_SENSOR_CARBON_DIOXIDE_CO2_LEVEL,  sizeof(carbon_dioxide)/sizeof(source_select_t),     carbon_dioxide},
    {MULTILEVEL_SENSOR_AIR_FLOW,                        ST_MULTILEVEL_SENSOR_AIR_FLOW,                  sizeof(air_flow)/sizeof(source_select_t),           air_flow},
    {MULTILEVEL_SENSOR_TANK_CAPACITY,                   ST_MULTILEVEL_SENSOR_TANK_CAPACITY,             sizeof(tank_capacity)/sizeof(source_select_t),      tank_capacity},
    {MULTILEVEL_SENSOR_DISTANCE,                        ST_MULTILEVEL_SENSOR_DISTANCE,                  sizeof(distance)/sizeof(source_select_t),           distance},
    {MULTILEVEL_SENSOR_ANGLE_POSITION,                  ST_MULTILEVEL_SENSOR_ANGLE_POSITION,            sizeof(angle_position)/sizeof(source_select_t),     angle_position},
    {MULTILEVEL_SENSOR_ROTATION,                        ST_MULTILEVEL_SENSOR_ROTATION,                  sizeof(rotation)/sizeof(source_select_t),           rotation},
    {MULTILEVEL_SENSOR_WATER_TEMPERATURE,               ST_MULTILEVEL_SENSOR_WATER_TEMPERATURE,         sizeof(temperature_value)/sizeof(source_select_t),  temperature_value},
    {MULTILEVEL_SENSOR_SOIL_TEMPERATURE,                ST_MULTILEVEL_SENSOR_SOIL_TEMPERATURE,          sizeof(temperature_value)/sizeof(source_select_t),  temperature_value},
    {MULTILEVEL_SENSOR_SEISMIC_INTENSITY,               ST_MULTILEVEL_SENSOR_SEISMIC_INTENSITY,         sizeof(seismic_intensity)/sizeof(source_select_t),  seismic_intensity},
    {MULTILEVEL_SENSOR_SEISMIC_MAGNITUDE,               ST_MULTILEVEL_SENSOR_SEISMIC_MAGNITUDE,         sizeof(seismic_magnitude)/sizeof(source_select_t),  seismic_magnitude},
    {MULTILEVEL_SENSOR_ULTRAVIOLET,                     ST_MULTILEVEL_SENSOR_ULTRAVIOLET,               sizeof(ultraviolet)/sizeof(source_select_t),        ultraviolet},
    {MULTILEVEL_SENSOR_ELECTRICAL_RESISTIVITY,          ST_MULTILEVEL_SENSOR_ELECTRICAL_RESISTIVITY,    sizeof(electrical_resistivity)/sizeof(source_select_t),   electrical_resistivity},
    {MULTILEVEL_SENSOR_ELECTRICAL_CONDUCTIVITY,         ST_MULTILEVEL_SENSOR_ELECTRICAL_CONDUCTIVITY,   sizeof(electrical_conductivity)/sizeof(source_select_t),  electrical_conductivity},
    {MULTILEVEL_SENSOR_LOUNDNESS,                       ST_MULTILEVEL_SENSOR_LOUNDNESS,                 sizeof(loudness)/sizeof(source_select_t),           loudness},
    {MULTILEVEL_SENSOR_MOISTURE,                        ST_MULTILEVEL_SENSOR_MOISTURE,                  sizeof(moisture)/sizeof(source_select_t),           moisture},
    {MULTILEVEL_SENSOR_FREQUENCY,                       ST_MULTILEVEL_SENSOR_FREQUENCY,                 sizeof(frequency)/sizeof(source_select_t),          frequency},
    {MULTILEVEL_SENSOR_TIME,                            ST_MULTILEVEL_SENSOR_TIME,                      sizeof(label_time)/sizeof(source_select_t),         label_time},
    {MULTILEVEL_SENSOR_TARGET_TEMPERATURE,              ST_MULTILEVEL_SENSOR_TARGET_TEMPERATURE,        sizeof(temperature_value)/sizeof(source_select_t),  temperature_value},
    {MULTILEVEL_SENSOR_PARTICULATE_MATTER_25,           ST_MULTILEVEL_SENSOR_PARTICULATE_MATTER_25,     sizeof(particulate_matter_25)/sizeof(source_select_t),    particulate_matter_25},
    {MULTILEVEL_SENSOR_FORMALDEHYDE_CH2O_LEVEL,         ST_MULTILEVEL_SENSOR_FORMALDEHYDE_CH2O_LEVEL,   sizeof(formaldehyde_CH2O)/sizeof(source_select_t),  formaldehyde_CH2O},
    {MULTILEVEL_SENSOR_RADON_CONCERNTRATION,            ST_MULTILEVEL_SENSOR_RADON_CONCERNTRATION,      sizeof(radon_concentration)/sizeof(source_select_t),radon_concentration},
    {MULTILEVEL_SENSOR_METHANE_DENSITY,                 ST_MULTILEVEL_SENSOR_METHANE_DENSITY,           sizeof(methane_density)/sizeof(source_select_t),    methane_density},
    {MULTILEVEL_SENSOR_VOLATILE_ORGANIC_COMPOUND_LEVEL, ST_MULTILEVEL_SENSOR_VOLATILE_ORGANIC_COMPOUND_LEVEL,  sizeof(volatile_organic_compound_level)/sizeof(source_select_t),volatile_organic_compound_level},
    {MULTILEVEL_SENSOR_CARBON_MONOXIDE_LEVEL,           ST_MULTILEVEL_SENSOR_CARBON_MONOXIDE_LEVEL,     sizeof(carbon_monoxide_CO)/sizeof(source_select_t), carbon_monoxide_CO},
    {MULTILEVEL_SENSOR_SOIL_HUMIDITY,                   ST_MULTILEVEL_SENSOR_SOIL_HUMIDITY,             sizeof(soil_humidity)/sizeof(source_select_t),      soil_humidity},
    {MULTILEVEL_SENSOR_SOIL_REACTIVITY,                 ST_MULTILEVEL_SENSOR_SOIL_REACTIVITY,           sizeof(soil_reactivity)/sizeof(source_select_t),    soil_reactivity},
    {MULTILEVEL_SENSOR_SOIL_SALINITY,                   ST_MULTILEVEL_SENSOR_SOIL_SALINITY,             sizeof(soil_sanlinity)/sizeof(source_select_t),     soil_sanlinity},
    {MULTILEVEL_SENSOR_HEART_RATE,                      ST_MULTILEVEL_SENSOR_HEART_RATE,                sizeof(heart_rate)/sizeof(source_select_t),         heart_rate},
    {MULTILEVEL_SENSOR_BLOOD_PRESSURE,                  ST_MULTILEVEL_SENSOR_BLOOD_PRESSURE,            sizeof(blood_pressure)/sizeof(source_select_t),     blood_pressure},
    {MULTILEVEL_SENSOR_MUSCLE_MASS,                     ST_MULTILEVEL_SENSOR_MUSCLE_MASS,               sizeof(mass)/sizeof(source_select_t),               mass},
    {MULTILEVEL_SENSOR_FAT_MASS,                        ST_MULTILEVEL_SENSOR_FAT_MASS,                  sizeof(mass)/sizeof(source_select_t),               mass},
    {MULTILEVEL_SENSOR_BONE_MASS,                       ST_MULTILEVEL_SENSOR_BONE_MASS,                 sizeof(mass)/sizeof(source_select_t),               mass},
    {MULTILEVEL_SENSOR_TOTAL_BODY_WATER,                ST_MULTILEVEL_SENSOR_TOTAL_BODY_WATER,          sizeof(mass)/sizeof(source_select_t),               mass},
    {MULTILEVEL_SENSOR_BASIS_METABOLIC_RATE,            ST_MULTILEVEL_SENSOR_BASIS_METABOLIC_RATE,      sizeof(basis_metabolic)/sizeof(source_select_t),    basis_metabolic},
    {MULTILEVEL_SENSOR_BODY_MASS,                       ST_MULTILEVEL_SENSOR_BODY_MASS,                 sizeof(body_mass)/sizeof(source_select_t),          body_mass},
    {MULTILEVEL_SENSOR_ACCELERATION_X_AXIS,             ST_MULTILEVEL_SENSOR_ACCELERATION_X_AXIS,       sizeof(acceleration)/sizeof(source_select_t),       acceleration},
    {MULTILEVEL_SENSOR_ACCELERATION_Y_AXIS,             ST_MULTILEVEL_SENSOR_ACCELERATION_Y_AXIS,       sizeof(acceleration)/sizeof(source_select_t),       acceleration},
    {MULTILEVEL_SENSOR_ACCELERATION_Z_AXIS,             ST_MULTILEVEL_SENSOR_ACCELERATION_Z_AXIS,       sizeof(acceleration)/sizeof(source_select_t),       acceleration},
    {MULTILEVEL_SENSOR_SMOKE_DENSITY,                   ST_MULTILEVEL_SENSOR_SMOKE_DENSITY,             sizeof(smoke_density)/sizeof(source_select_t),      smoke_density},
    {MULTILEVEL_SENSOR_WATER_FLOW,                      ST_MULTILEVEL_SENSOR_WATER_FLOW,                sizeof(water_flow)/sizeof(source_select_t),         water_flow},
    {MULTILEVEL_SENSOR_WATER_PRESSURE,                  ST_MULTILEVEL_SENSOR_WATER_PRESSURE,            sizeof(water_pressure)/sizeof(source_select_t),     water_pressure},
    {MULTILEVEL_SENSOR_RF_SIGNAL_STRENGTH,              ST_MULTILEVEL_SENSOR_RF_SIGNAL_STRENGTH,        sizeof(rf_signal_strength)/sizeof(source_select_t),     rf_signal_strength},
    {MULTILEVEL_SENSOR_PARTICULATE_MATTER_10,           ST_MULTILEVEL_SENSOR_PARTICULATE_MATTER_10,     sizeof(particulate_matter_10)/sizeof(source_select_t),  particulate_matter_10},
    {MULTILEVEL_SENSOR_RESPIRATORY_RATE,                ST_MULTILEVEL_SENSOR_RESPIRATORY_RATE,          sizeof(respiratory_rate)/sizeof(source_select_t),       respiratory_rate},
    {MULTILEVEL_SENSOR_RELATIVE_MODULATION_LEVEL,       ST_MULTILEVEL_SENSOR_RELATIVE_MODULATION_LEVEL, sizeof(relative_modulation)/sizeof(source_select_t),    relative_modulation},
    {MULTILEVEL_SENSOR_BOILER_WATER_TEMPERATURE,        ST_MULTILEVEL_SENSOR_BOILER_WATER_TEMPERATURE,  sizeof(temperature_value)/sizeof(source_select_t),      temperature_value},
    {MULTILEVEL_SENSOR_DOMESTIC_HOT_WATER,              ST_MULTILEVEL_SENSOR_DOMESTIC_HOT_WATER,        sizeof(temperature_value)/sizeof(source_select_t),      temperature_value},
    {MULTILEVEL_SENSOR_OUTSIDE_TEMPERATURE,             ST_MULTILEVEL_SENSOR_OUTSIDE_TEMPERATURE,       sizeof(temperature_value)/sizeof(source_select_t),      temperature_value},
    {MULTILEVEL_SENSOR_EXHAUST_TEMPERATURE,             ST_MULTILEVEL_SENSOR_EXHAUST_TEMPERATURE,       sizeof(temperature_value)/sizeof(source_select_t),      temperature_value},
};

/*#####################################################################################*/
/*###########################           THERMOSTAT           #########################*/
/*#####################################################################################*/
source_select_t thermostatModeTable[]=
{
    {THERMOSTAT_MODE_OFF_VALUE,                                  ST_OFF},
    {THERMOSTAT_MODE_HEAT_VALUE,                                 ST_HEAT},
    {THERMOSTAT_MODE_COOL_VALUE,                                 ST_COOL},    
    {THERMOSTAT_MODE_AUTO_VALUE,                                 ST_AUTO},
    {THERMOSTAT_MODE_AUXILIARY_HEAT_VALUE,                  ST_AUXILIARY},
    {THERMOSTAT_MODE_RESUME_VALUE,                             ST_RESUME},
    {THERMOSTAT_MODE_FAN_ONLY_SETMODE_VALUE,                 ST_FAN_ONLY},
    {THERMOSTAT_MODE_FURNACE_VALUE,                           ST_FURNACE},
    {THERMOSTAT_MODE_DRY_AIR_VALUE,                           ST_DRY_AIR},
    {THERMOSTAT_MODE_MOIST_VALUE,                           ST_MOIST_AIR},
    {THERMOSTAT_MODE_AUTO_CHANGE_OVER_VALUE,         ST_AUTO_CHANGE_OVER},
    {THERMOSTAT_MODE_ENERGY_SAVE_HEAT_VALUE,         ST_ENERGY_SAVE_HEAT},
    {THERMOSTAT_MODE_ENERGY_SAVE_COOL_VALUE,         ST_ENERGY_SAVE_COOL},
    {THERMOSTAT_MODE_AWAY_VALUE,                                 ST_AWAY},
    {THERMOSTAT_MODE_RESERVED_VALUE,                         ST_RESERVED},
    {THERMOSTAT_MODE_FULL_POWER_VALUE,                    ST_FULL_POWER },
    {THERMOSTAT_MODE_MANUFACTURER_SPECIFIC_VALUE,   ST_MANUFACTURER_SPECIFIC},
};

source_select_t thermostatSetPointTable[]=
{
    {THERMOSTAT_SETPOINT_HEATING_VALUE,                             ST_HEATING},
    {THERMOSTAT_SETPOINT_COOLING_VALUE,                             ST_COOLING},    
    {THERMOSTAT_SETPOINT_FUMANCE_VALUE,                             ST_FURNACE},
    {THERMOSTAT_SETPOINT_DRY_AIR_VALUE,                             ST_DRY_AIR},
    {THERMOSTAT_SETPOINT_MOIST_AIR_VALUE,                           ST_MOIST_AIR},
    {THERMOSTAT_SETPOINT_AUTO_CHANGE_OVER_VALUE,                    ST_AUTO_CHANGE_OVER},
    {THERMOSTAT_SETPOINT_ENERGY_SAVE_HEATING_VALUE,                 ST_ENERGY_SAVE_HEATING},
    {THERMOSTAT_SETPOINT_ENERGY_SAVE_COOLING_VALUE,                 ST_ENERGY_SAVE_COOLING},
    {THERMOSTAT_SETPOINT_ALWAY_HEATING_VALUE,                       ST_AWAY_HEATING},
    {THERMOSTAT_SETPOINT_ALWAY_COOLING_VALUE,                       ST_AWAY_COOLING},
    {THERMOSTAT_SETPOINT_FULL_POWER_VALUE,                          ST_FULL_POWER},
};

source_select_t thermostatOperatingTable[]=
{
    {THERMOSTAT_OPERATING_IDLE_VALUE,                              ST_IDLE},
    {THERMOSTAT_OPERATING_HEATING_VALUE,                           ST_HEATING},    
    {THERMOSTAT_OPERATING_COOLING_VALUE,                           ST_COOLING},
    {THERMOSTAT_OPERATING_FAN_ONLY_OPERATE_VALUE,                  ST_FAN_ONLY},
    {THERMOSTAT_OPERATING_PENDING_HEAT_VALUE,                      ST_PENDING_HEAT},
    {THERMOSTAT_OPERATING_PENDING_COOL_VALUE,                      ST_PENDING_COOL},
    {THERMOSTAT_OPERATING_VENT_ECONOMIZER_VALUE,                   ST_VENT_ECONOMIZER},
    {THERMOSTAT_OPERATING_AUX_HEATING,                             ST_AUX_HEATING},
    {THERMOSTAT_OPERATING_2ND_STAGE_HEATING,                       ST_2ND_STAGE_HEATING},
    {THERMOSTAT_OPERATING_2ND_STAGE_COOLING,                       ST_2ND_STAGE_COOLING},
    {THERMOSTAT_OPERATING_2ND_STAGE_AUX_HEAT,                      ST_2ND_STAGE_AUX_HEAT},
    {THERMOSTAT_OPERATING_3RD_STAGE_AUX_HEAT,                      ST_3RD_STAGE_AUX_HEAT},
};

source_select_t thermostatFanStateTable[]=
{
    {THERMOSTAT_FAN_STATE_IDLE_VALUE,                          ST_IDLE},
    {THERMOSTAT_FAN_STATE_RUNNING_LOW_VALUE,                   ST_RUNNING_LOW},    
    {THERMOSTAT_FAN_STATE_RUNNING_HIGH_VALUE,                  ST_RUNNING_HIGH},
    {THERMOSTAT_FAN_STATE_RUNNING_MEDIUM_VALUE,                ST_RUNNING_MEDIUM},
    {THERMOSTAT_FAN_STATE_CIRCULATION_STATE_VALUE,             ST_CIRCULATION},
    {THERMOSTAT_FAN_STATE_HUMI_CIRCULATION_STATE_VALUE,        ST_HUMI_CIRCULATION},
    {THERMOSTAT_FAN_STATE_LEFT_RIGHT_STATE_VALUE,              ST_LEFT_RIGHT},
    {THERMOSTAT_FAN_STATE_UP_DOWN_STATE_VALUE,                 ST_UP_DOWN},
    {THERMOSTAT_FAN_STATE_QUIET_STATE_VALUE,                   ST_QUIET},
};

source_select_t thermostatFanModeTable[]=
{
    {THERMOSTAT_FAN_MODE_AUTO_LOW_VALUE             ,ST_AUTO_LOW},
    {THERMOSTAT_FAN_MODE_LOW_VALUE                  ,ST_LOW},
    {THERMOSTAT_FAN_MODE_AUTO_HIGH_VALUE            ,ST_AUTO_HIGH},
    {THERMOSTAT_FAN_MODE_HIGH_VALUE                 ,ST_HIGH},
    {THERMOSTAT_FAN_MODE_AUTO_MEDIUM_VALUE          ,ST_AUTO_MEDIUM},
    {THERMOSTAT_FAN_MODE_MEDIUM_VALUE               ,ST_MEDIUM},
    {THERMOSTAT_FAN_MODE_CIRCULATION_VALUE          ,ST_CIRCULATION},
    {THERMOSTAT_FAN_MODE_HUMI_CIRCULATION_VALUE     ,ST_HUMI_CIRCULATION},
    {THERMOSTAT_FAN_MODE_LEFT_RIGHT_VALUE           ,ST_LEFT_RIGHT},
    {THERMOSTAT_FAN_MODE_UP_DOWN_VALUE              ,ST_UP_DOWN},
    {THERMOSTAT_FAN_MODE_QUIET_VALUE                ,ST_QUIET},
};

/*#####################################################################################*/
/*###########################               METER             #########################*/
/*#####################################################################################*/
source_select_t electric_meter[]=
{
    {METER_KWH,                 ST_METER_KWH,       ACCUMULATED},
    {METER_KVAH,                ST_METER_KVAH,      ACCUMULATED},
    {METER_W,                   ST_METER_W,         INSTANT},
    {METER_PULSE_COUNT,         ST_METER_PULSE_COUNT,   ACCUMULATED},
    {METER_V,                   ST_METER_V,             INSTANT},
    {METER_A,                   ST_METER_A,             INSTANT},
    {METER_POWER_FACTOR,        ST_METER_POWER_FACTOR,  INSTANT},
    {METER_KVAR,                ST_METER_KVAR,          INSTANT},
    {METER_KVARH,               ST_METER_KVARH,         ACCUMULATED},
};

source_select_t gas_meter[]=
{
    {METER_CUBIC_METERS,                 ST_METER_CUBIC_METERS, ACCUMULATED},
    {METER_CUBIC_FEET,                   ST_METER_CUBIC_FEET,   ACCUMULATED},
    {METER_PULSE_COUNT,                  ST_METER_PULSE_COUNT,  ACCUMULATED},
};

source_select_t water_meter[]=
{
    {METER_CUBIC_METERS,                ST_METER_CUBIC_METERS,  ACCUMULATED},
    {METER_CUBIC_FEET,                  ST_METER_CUBIC_FEET,    ACCUMULATED},
    {METER_US_GALLONS,                  ST_METER_US_GALLONS,    ACCUMULATED},
    {METER_PULSE_COUNT,                 ST_METER_PULSE_COUNT,   ACCUMULATED},
};

source_select_t heating_meter[]=
{
    {METER_KWH,                 ST_METER_KWH,           ACCUMULATED},
};

source_select_t cooling_meter[]=
{
    {METER_KWH,                 ST_METER_KWH,           ACCUMULATED},
};

class_infor_t meterTable[]=
{
    {ELECTRIC_METER,            ST_ELECTRIC_METER,           sizeof(electric_meter)/sizeof(source_select_t),    electric_meter},
    {GAS_METER,                 ST_GAS_METER,                sizeof(gas_meter)/sizeof(source_select_t),         gas_meter},
    {WATER_METER,               ST_WATER_METER,              sizeof(water_meter)/sizeof(source_select_t),       water_meter},
    {HEATING_METER,             ST_HEATING_METER,            sizeof(heating_meter)/sizeof(source_select_t),     heating_meter},
    {COOLING_METER,             ST_COOLING_METER,            sizeof(cooling_meter)/sizeof(source_select_t),     cooling_meter},
};

/*#####################################################################################*/
/*###########################        NOTIFICATION        #########################*/
/*#####################################################################################*/
source_select_t carbon_test[]=
{
    {0x01, ST_ALARM_PARAM_OK},
    {0x02, ST_ALARM_PARAM_FAILED},
};

source_select_t commonAlarm[]=
{
    {0x01, ST_ALARM_PARAM_NO_DATA},
    {0x02, ST_ALARM_PARAM_BELOW_LOW_THRESHOLD},
    {0x03, ST_ALARM_PARAM_ABOVE_HIGH_THRESHOLD},
    {0x04, ST_ALARM_PARAM_MAX},
};

source_select_t barrierInitProcess[]=
{
    {0x00, ST_ALARM_PARAM_COMPLETE},
    {0xFF, ST_ALARM_PARAM_PERFORMING},
};

source_select_t barrierVacationMode[]=
{
    {0x00, ST_ALARM_PARAM_DISABLE},
    {0xFF, ST_ALARM_PARAM_ENABLE},
};

source_select_t barrierSafetyBeamObstacle[]=
{
    {0x00, ST_ALARM_PARAM_NO_OBSTRUCTION},
    {0xFF, ST_ALARM_PARAM_OBSTRUCTION},
};

source_select_t olatileOrganicCompound[]=
{
    {0x01, ST_ALARM_PARAM_CLEAN},
    {0x02, ST_ALARM_PARAM_SLIGHTLY_POLLUTED},
    {0x03, ST_ALARM_PARAM_MODERATELY_POLLUTED},
    {0x04, ST_ALARM_PARAM_HIGHLY_POLLUTED},
};

source_select_t waterValveOperation[]=
{
    {0x00, ST_ALARM_PARAM_OFF},
    {0x01, ST_ALARM_PARAM_ON},
};

notification_param_t notificationParam[]=
{
    {ALARM_GENERIC_SMOKE_ALARM,             ALARM_SMOKE_DETECTED_LOCATION,           0, NULL},
    {ALARM_GENERIC_CO_ALARM,                ALARM_CO_DETECTED_LOCATION, 0, NULL},
    {ALARM_GENERIC_CO_ALARM,                ALARM_CO_TEST,                  sizeof(carbon_test)/sizeof(source_select_t), carbon_test},
    {ALARM_GENERIC_CO2_ALARM,               ALARM_CO2_DETECTED_LOCATION,  0, NULL},
    {ALARM_GENERIC_CO2_ALARM,               ALARM_CO2_TEST,                 sizeof(carbon_test)/sizeof(source_select_t), carbon_test},
    {ALARM_GENERIC_HEAT_ALARM,              ALARM_HEAT_OVERHEAT_DETECTED_LOCATION,        0, NULL},
    {ALARM_GENERIC_HEAT_ALARM,              ALARM_HEAT_UNDER_HEAT_DETECTED_LOCATION,      0, NULL},
    {ALARM_GENERIC_HEAT_ALARM,              ALARM_HEAT_RAPID_TEMPERATURE_RISE_LOCATION,   0, NULL},
    {ALARM_GENERIC_HEAT_ALARM,              ALARM_HEAT_RAPID_TEMPERATURE_FALL_LOCATION,   0, NULL},
    {ALARM_GENERIC_WATER_ALARM,             ALARM_WATER_LEAK_DETECTED_LOCATION,     0, NULL},
    {ALARM_GENERIC_WATER_ALARM,             ALARM_WATER_LEVEL_DROPPED_LOCATION,     0, NULL},
    {ALARM_GENERIC_WATER_ALARM,             ALARM_WATER_FLOW_ALARM,           sizeof(commonAlarm)/sizeof(source_select_t), commonAlarm},
    {ALARM_GENERIC_WATER_ALARM,             ALARM_WATER_PRESSURE_ALARM,       sizeof(commonAlarm)/sizeof(source_select_t), commonAlarm},
    {ALARM_GENERIC_WATER_ALARM,             ALARM_WATER_TEMPERATURE_ALARM,    sizeof(commonAlarm)/sizeof(source_select_t), commonAlarm},
    {ALARM_GENERIC_WATER_ALARM,             ALARM_WATER_LEVEL_ALARM,          sizeof(commonAlarm)/sizeof(source_select_t), commonAlarm},
    {ALARM_GENERIC_ACCESS_CONTROL,          ALARM_AC_BARRIER_PERFORMING_INIT_PROCESS,  sizeof(barrierInitProcess)/sizeof(source_select_t), barrierInitProcess},
    {ALARM_GENERIC_ACCESS_CONTROL,          ALARM_AC_BARRIER_MOTOR_EXCEEDED,               0, NULL},/*need handle by process 0x00 -> 0x7F:seconds, another minutes*/
    {ALARM_GENERIC_ACCESS_CONTROL,          ALARM_AC_BARRIER_VACATION_MODE,       sizeof(barrierVacationMode)/sizeof(source_select_t), barrierVacationMode},
    {ALARM_GENERIC_ACCESS_CONTROL,          ALARM_AC_BARRIER_SAFETY_BEAM_OBSTACLE,              sizeof(barrierSafetyBeamObstacle)/sizeof(source_select_t), barrierSafetyBeamObstacle},
    {ALARM_GENERIC_ACCESS_CONTROL,          ALARM_AC_BARRIER_SENSOR_NOT_DETECTED, 0, NULL},/*need handle by process 00 sensor not define, another is sensor ID*/
    {ALARM_GENERIC_ACCESS_CONTROL,          ALARM_AC_BARRIER_SENSOR_LOW_BATTERY_WARNING, 0, NULL},/*need handle by process 00 sensor not define, another is sensor ID*/
    {ALARM_GENERIC_HOME_SECURITY,           ALARM_HOME_INTRUSION_LOCATION,           0, NULL},
    {ALARM_GENERIC_HOME_SECURITY,           ALARM_HOME_GLASS_BREAKAGE_LOCATION,      0, NULL},
    {ALARM_GENERIC_HOME_SECURITY,           ALARM_HOME_MOTION_DETECTION_LOCATION,    0, NULL},
    {ALARM_GENERIC_CLOCK,                   ALARM_CLOCK_TIME_REMAINING,                0, NULL},/*need handle byte1 hour, byte2 minutes, byte3 seconds*/
    {ALARM_GENERIC_HOME_HEALTH,             ALARM_HOME_HEALTH_VOLATILE_ORGANIC_LEVEL,        sizeof(olatileOrganicCompound)/sizeof(source_select_t), olatileOrganicCompound},
    {ALARM_GENERIC_WATER_VALVE,             ALARM_WATER_VALVE_OPERATION,         sizeof(waterValveOperation)/sizeof(source_select_t), waterValveOperation},
    {ALARM_GENERIC_WATER_VALVE,             ALARM_WATER_MASTER_VALVE_OPERATION,  sizeof(waterValveOperation)/sizeof(source_select_t), waterValveOperation},
    {ALARM_GENERIC_WATER_VALVE,             ALARM_WATER_VALVE_CURRENT_ALARM,     sizeof(commonAlarm)/sizeof(source_select_t), commonAlarm},
    {ALARM_GENERIC_WATER_VALVE,             ALARM_WATER_MASTER_VALVE_CURRENT_ALARM, sizeof(commonAlarm)/sizeof(source_select_t), commonAlarm},
    {ALARM_GENERIC_GAS_ALARM,               ALARM_GAS_COMBUSTIBLE_GAS_DETECTED_LOCATION,      0, NULL},
    {ALARM_GENERIC_GAS_ALARM,               ALARM_GAS_TOXIC_GAS_DETECTED_LOCATION,     0, NULL},
    {ALARM_GENERIC_PEST_CONTROL,            ALARM_PEST_TRAP_ARMED_LOCATION,           0, NULL},
    {ALARM_GENERIC_PEST_CONTROL,            ALARM_PEST_TRAP_REARMED_REQUIRED_LOCATION,    0, NULL},
    {ALARM_GENERIC_PEST_CONTROL,            ALARM_PEST_DETECTED_LOCATION,         0, NULL},
    {ALARM_GENERIC_PEST_CONTROL,            ALARM_PEST_EXTERMINATED_LOCATION,     0, NULL},
};

source_select_t smoke_alarm[]=
{
    {ALARM_SMOKE_STATE_IDLE,                   ST_ALARM_SMOKE_STATE_IDLE,                     NO_PARAM},
    {ALARM_SMOKE_DETECTED_LOCATION,            ST_ALARM_SMOKE_DETECTED_LOCATION,              HAS_PARAM},
    {ALARM_SMOKE_DETECTED,                     ST_ALARM_SMOKE_DETECTED,                       NO_PARAM},
    {ALARM_SMOKE_ALARM_TEST,                   ST_ALARM_SMOKE_ALARM_TEST,                     NO_PARAM},
    {ALARM_SMOKE_REPLACEMENT_REQUIRED,         ST_ALARM_SMOKE_REPLACEMENT_REQUIRED,           NO_PARAM},
    {ALARM_SMOKE_REPLACEMENT_REQUIRED_EOL,     ST_ALARM_SMOKE_REPLACEMENT_REQUIRED_EOL,       NO_PARAM},
    {ALARM_SMOKE_ALARM_SILENCED,               ST_ALARM_SMOKE_ALARM_SILENCED,                 NO_PARAM},
    {ALARM_SMOKE_MAINTENANCE_REQUIRED_PPI,     ST_ALARM_SMOKE_MAINTENANCE_REQUIRED_PPI,       NO_PARAM},
    {ALARM_SMOKE_MAINTENANCE_REQUIRED_DID,     ST_ALARM_SMOKE_MAINTENANCE_REQUIRED_DID,       NO_PARAM},
    {ALARM_SMOKE_UNKHOWN_EVENT_STATE,          ST_ALARM_SMOKE_UNKHOWN_EVENT_STATE,            NO_PARAM},                        
};

source_select_t co_alarm[]=
{
    {ALARM_CO_STATE_IDLE,                           ST_ALARM_CO_STATE_IDLE,                      NO_PARAM},
    {ALARM_CO_DETECTED_LOCATION,                    ST_ALARM_CO_DETECTED_LOCATION,               HAS_PARAM},
    {ALARM_CO_DETECTED,                             ST_ALARM_CO_DETECTED,                        NO_PARAM},
    {ALARM_CO_TEST,                                 ST_ALARM_CO_TEST,                            HAS_PARAM},
    {ALARM_CO_REPLACEMENT_REQUIRED,                 ST_ALARM_CO_REPLACEMENT_REQUIRED,            NO_PARAM},
    {ALARM_CO_REPLACEMENT_REQUIRED_EOL,             ST_ALARM_CO_REPLACEMENT_REQUIRED_EOL,        NO_PARAM},
    {ALARM_CO_ALARM_SILENCED,                       ST_ALARM_CO_ALARM_SILENCED,                  NO_PARAM},
    {ALARM_CO_MAINTENANCE_REQUIRED_PPI,             ST_ALARM_CO_MAINTENANCE_REQUIRED_PPI,        NO_PARAM},
    {ALARM_CO_UNKHOWN_EVENT_STATE,                  ST_ALARM_CO_UNKHOWN_EVENT_STATE,             NO_PARAM},
};

source_select_t co2_alarm[]=
{
    {ALARM_CO2_STATE_IDLE,                      ST_ALARM_CO2_STATE_IDLE,                        NO_PARAM},
    {ALARM_CO2_DETECTED_LOCATION,               ST_ALARM_CO2_DETECTED_LOCATION,             HAS_PARAM},
    {ALARM_CO2_DETECTED,                        ST_ALARM_CO2_DETECTED,                      NO_PARAM},
    {ALARM_CO2_TEST,                            ST_ALARM_CO2_TEST,                          HAS_PARAM},
    {ALARM_CO2_REPLACEMENT_REQUIRED,            ST_ALARM_CO2_REPLACEMENT_REQUIRED,          NO_PARAM},
    {ALARM_CO2_REPLACEMENT_REQUIRED_EOL,        ST_ALARM_CO2_REPLACEMENT_REQUIRED_EOL,      NO_PARAM},
    {ALARM_CO2_ALARM_SILENCED,                  ST_ALARM_CO2_ALARM_SILENCED,                NO_PARAM},
    {ALARM_CO2_MAINTENANCE_REQUIRED_PPI,        ST_ALARM_CO2_MAINTENANCE_REQUIRED_PPI,      NO_PARAM},
    {ALARM_CO2_UNKHOWN_EVENT_STATE,             ST_ALARM_CO2_UNKHOWN_EVENT_STATE,               NO_PARAM},
};

source_select_t heat_alarm[]=
{
    {ALARM_HEAT_STATE_IDLE,                         ST_ALARM_HEAT_STATE_IDLE,                            NO_PARAM},
    {ALARM_HEAT_OVERHEAT_DETECTED_LOCATION,         ST_ALARM_HEAT_OVERHEAT_DETECTED_LOCATION,       HAS_PARAM},
    {ALARM_HEAT_OVERHEAT_DETECTED,                  ST_ALARM_HEAT_OVERHEAT_DETECTED,                NO_PARAM},
    {ALARM_HEAT_UNDER_HEAT_DETECTED_LOCATION,       ST_ALARM_HEAT_UNDER_HEAT_DETECTED_LOCATION,     HAS_PARAM},
    {ALARM_HEAT_UNDER_HEAT_DETECTED,                ST_ALARM_HEAT_UNDER_HEAT_DETECTED,              NO_PARAM},
    {ALARM_HEAT_RAPID_TEMPERATURE_RISE_LOCATION,    ST_ALARM_HEAT_RAPID_TEMPERATURE_RISE_LOCATION,  HAS_PARAM},
    {ALARM_HEAT_RAPID_TEMPERATURE_RISE,             ST_ALARM_HEAT_RAPID_TEMPERATURE_RISE,           NO_PARAM},
    {ALARM_HEAT_RAPID_TEMPERATURE_FALL_LOCATION,    ST_ALARM_HEAT_RAPID_TEMPERATURE_FALL_LOCATION,  HAS_PARAM},
    {ALARM_HEAT_RAPID_TEMPERATURE_FALL,             ST_ALARM_HEAT_RAPID_TEMPERATURE_FALL,           NO_PARAM},
    {ALARM_HEAT_ALARM_TEST,                         ST_ALARM_HEAT_ALARM_TEST,                       NO_PARAM},
    {ALARM_HEAT_ALARM_SILENCED,                     ST_ALARM_HEAT_ALARM_SILENCED,                   NO_PARAM},
    {ALARM_HEAT_REPLACEMENT_HEAT_REQUIRED_EOL,      ST_ALARM_HEAT_REPLACEMENT_REQUIRED_EOL,         NO_PARAM},
    {ALARM_HEAT_MAINTENANCE_HEAT_REQUIRED_DID,      ST_ALARM_HEAT_MAINTENANCE_REQUIRED_DID,         NO_PARAM},
    {ALARM_HEAT_MAINTENANCE_HEAT_REQUIRED_PPI,      ST_ALARM_HEAT_MAINTENANCE_REQUIRED_PPI,         NO_PARAM},
    {ALARM_HEAT_UNKHOWN_EVENT_STATE,                ST_ALARM_HEAT_UNKHOWN_EVENT_STATE,                   NO_PARAM},
};

source_select_t water_alarm[]=
{
    {ALARM_WATER_STATE_IDLE,                        ST_ALARM_WATER_STATE_IDLE,                           NO_PARAM},
    {ALARM_WATER_LEAK_DETECTED_LOCATION,            ST_ALARM_WATER_LEAK_DETECTED_LOCATION,         HAS_PARAM},
    {ALARM_WATER_LEAK_DETECTED,                     ST_ALARM_WATER_LEAK_DETECTED,                  NO_PARAM},
    {ALARM_WATER_LEVEL_DROPPED_LOCATION,            ST_ALARM_WATER_LEVEL_DROPPED_LOCATION,         HAS_PARAM},
    {ALARM_WATER_LEVEL_DROPPED,                     ST_ALARM_WATER_LEVEL_DROPPED,                  NO_PARAM},
    {ALARM_WATER_REPLACE_WATER_FILTER,              ST_ALARM_WATER_REPLACE_WATER_FILTER,           NO_PARAM},
    {ALARM_WATER_FLOW_ALARM,                        ST_ALARM_WATER_FLOW_ALARM,                     HAS_PARAM},
    {ALARM_WATER_PRESSURE_ALARM,                    ST_ALARM_WATER_PRESSURE_ALARM,                 HAS_PARAM},
    {ALARM_WATER_TEMPERATURE_ALARM,                 ST_ALARM_WATER_TEMPERATURE_ALARM,              HAS_PARAM},
    {ALARM_WATER_LEVEL_ALARM,                       ST_ALARM_WATER_LEVEL_ALARM,                    HAS_PARAM},
    {ALARM_WATER_SUMP_PUMP_ACTIVE,                  ST_ALARM_WATER_SUMP_PUMP_ACTIVE,               NO_PARAM},
    {ALARM_WATER_SUMP_PUMP_FAILURE,                 ST_ALARM_WATER_SUMP_PUMP_FAILURE,              NO_PARAM},
    {ALARM_WATER_UNKHOWN_EVENT_STATE,               ST_ALARM_WATER_UNKHOWN_EVENT_STATE,                  NO_PARAM},
};

source_select_t access_control[]=
{
    {ALARM_AC_STATE_IDLE,                               ST_ALARM_AC_STATE_IDLE,                             NO_PARAM},
    {ALARM_AC_MANUAL_LOCK_OPERATION,                    ST_ALARM_AC_MANUAL_LOCK_OPERATION,                  NO_PARAM},
    {ALARM_AC_MANUAL_UNLOCK_OPERATION,                  ST_ALARM_AC_MANUAL_UNLOCK_OPERATION,                NO_PARAM},
    {ALARM_AC_RF_LOCK_OPERATION,                        ST_ALARM_AC_RF_LOCK_OPERATION,                      NO_PARAM},
    {ALARM_AC_RF_UNLOCK_OPERATION,                      ST_ALARM_AC_RF_UNLOCK_OPERATION,                    NO_PARAM},
    {ALARM_AC_KEYPAD_LOCK_OPERATION,                    ST_ALARM_AC_KEYPAD_LOCK_OPERATION,                  NO_PARAM},
    {ALARM_AC_KEYPAD_UNLOCK_OPERATION,                  ST_ALARM_AC_KEYPAD_UNLOCK_OPERATION,                NO_PARAM},
    {ALARM_AC_MANUAL_NOT_FULLY_LOCKED_OPERATION,        ST_ALARM_AC_MANUAL_NOT_FULLY_LOCKED_OPERATION ,     NO_PARAM},
    {ALARM_AC_RF_NOT_FULLY_LOCKED_OPERATION,            ST_ALARM_AC_RF_NOT_FULLY_LOCKED_OPERATION,          NO_PARAM},
    {ALARM_AC_AUTO_LOCK_LOCKED_OPERATION,               ST_ALARM_AC_AUTO_LOCK_LOCKED_OPERATION,             NO_PARAM},
    {ALARM_AC_AUTO_LOCK_NOT_FULLY_LOCKED_OPERATION,     ST_ALARM_AC_AUTO_LOCK_NOT_FULLY_LOCKED_OPERATION,   NO_PARAM},
    {ALARM_AC_LOCK_JAMMED,                              ST_ALARM_AC_LOCK_JAMMED,                            NO_PARAM},
    {ALARM_AC_ALL_USER_CODE_DELETED,                    ST_ALARM_AC_ALL_USER_CODE_DELETED,                  NO_PARAM},
    {ALARM_AC_SINGLE_USER_CODE_DELETED,                 ST_ALARM_AC_SINGLE_USER_CODE_DELETED,               NO_PARAM},
    {ALARM_AC_NEW_USER_CODE_ADDED,                      ST_ALARM_AC_NEW_USER_CODE_ADDED,                    NO_PARAM},
    {ALARM_AC_NEW_USER_CODE_NOT_ADDED,                  ST_ALARM_AC_NEW_USER_CODE_NOT_ADDED,                NO_PARAM},
    {ALARM_AC_KEYPAD_TEMPORARY_DISABLED,                ST_ALARM_AC_KEYPAD_TEMPORARY_DISABLED,              NO_PARAM},
    {ALARM_AC_KEYPAD_BUSY,                              ST_ALARM_AC_KEYPAD_BUSY,                            NO_PARAM},
    {ALARM_AC_NEW_PROGRAM_CODE_ENTERED,                 ST_ALARM_AC_NEW_PROGRAM_CODE_ENTERED,               NO_PARAM},
    {ALARM_AC_MANUALY_ENTER_USER_ACCESS,                ST_ALARM_AC_MANUALY_ENTER_USER_ACCESS_CODE_EXCEEDED,NO_PARAM},
    {ALARM_AC_UNLOCK_RF_INVALID_USER_CODE,              ST_ALARM_AC_UNLOCK_RF_INVALID_USER_CODE,            NO_PARAM},
    {ALARM_AC_LOCKED_RF_INVALID_USER_CODE,              ST_ALARM_AC_LOCKED_RF_INVALID_USER_CODE,            NO_PARAM},
    {ALARM_AC_WINDOW_DOOR_OPEN,                         ST_ALARM_AC_WINDOW_DOOR_OPEN,                       NO_PARAM},
    {ALARM_AC_WINDOW_DOOR_CLOSED,                       ST_ALARM_AC_WINDOW_DOOR_CLOSED,                     NO_PARAM},
    {ALARM_AC_WINDOW_DOOR_HANDLE_OPEN,                  ST_ALARM_AC_WINDOW_DOOR_HANDLE_OPEN,                NO_PARAM},
    {ALARM_AC_WINDOW_DOOR_HANDLE_CLOSED,                ST_ALARM_AC_WINDOW_DOOR_HANDLE_CLOSED,              NO_PARAM},
    {ALARM_AC_BARRIER_PERFORMING_INIT_PROCESS,          ST_ALARM_AC_BARRIER_PERFORMING_INIT_PROCESS,        HAS_PARAM},
    {ALARM_AC_BARRIER_OPERATION_FORCE_EXCEEDED,         ST_ALARM_AC_BARRIER_OPERATION_FORCE_EXCEEDED,       NO_PARAM},
    {ALARM_AC_BARRIER_MOTOR_EXCEEDED,                   ST_ALARM_AC_BARRIER_MOTOR_EXCEEDED,                 HAS_PARAM},
    {ALARM_AC_BARRIER_OPERATION_PHYSICAL_EXCEEDED,      ST_ALARM_AC_BARRIER_OPERATION_PHYSICAL_EXCEEDED,    NO_PARAM},
    {ALARM_AC_BARRIER_UNABLE_PERFORM_REQUEST,           ST_ALARM_AC_BARRIER_UNABLE_PERFORM_REQUEST,         NO_PARAM},
    {ALARM_AC_BARRIER_UNATTENDED_DISABLED,              ST_ALARM_AC_BARRIER_UNATTENDED_DISABLED,            NO_PARAM},
    {ALARM_AC_BARRIER_MALFUNCTION,                      ST_ALARM_AC_BARRIER_MALFUNCTION,                    NO_PARAM},
    {ALARM_AC_BARRIER_VACATION_MODE,                    ST_ALARM_AC_BARRIER_VACATION_MODE,                  HAS_PARAM},
    {ALARM_AC_BARRIER_SAFETY_BEAM_OBSTACLE,             ST_ALARM_AC_BARRIER_SAFETY_BEAM_OBSTACLE,           HAS_PARAM},
    {ALARM_AC_BARRIER_SENSOR_NOT_DETECTED,              ST_ALARM_AC_BARRIER_SENSOR_NOT_DETECTED,            HAS_PARAM},
    {ALARM_AC_BARRIER_SENSOR_LOW_BATTERY_WARNING,       ST_ALARM_AC_BARRIER_SENSOR_LOW_BATTERY_WARNING,     HAS_PARAM},
    {ALARM_AC_BARRIER_DETECTED_SHORT,                   ST_ALARM_AC_BARRIER_DETECTED_SHORT,                 NO_PARAM},
    {ALARM_AC_BARRIER_ASSOCIATED_WITH_NON_ZWAVE_REMOTE, ST_ALARM_AC_BARRIER_ASSOCIATED_WITH_NON_ZWAVE_REMOTE,NO_PARAM},
    {ALARM_AC_UNKHOWN_EVENT_STATE,                      ST_ALARM_AC_UNKHOWN_EVENT_STATE,                    NO_PARAM},
};

source_select_t home_security[]=
{
    {ALARM_HOME_STATE_IDLE,                     ST_ALARM_HOME_STATE_IDLE,                   NO_PARAM},
    {ALARM_HOME_INTRUSION_LOCATION,             ST_ALARM_HOME_INTRUSION_LOCATION,           HAS_PARAM},
    {ALARM_HOME_INTRUSION,                      ST_ALARM_HOME_INTRUSION,                    NO_PARAM},
    {ALARM_HOME_TEMPERATING_PRODUCT_REMOVE,     ST_ALARM_HOME_TEMPERATING_PRODUCT_REMOVE,   NO_PARAM},
    {ALARM_HOME_TEMPERATING_INVALID_CODE,       ST_ALARM_HOME_TEMPERATING_INVALID_CODE,     NO_PARAM},
    {ALARM_HOME_GLASS_BREAKAGE_LOCATION,        ST_ALARM_HOME_GLASS_BREAKAGE_LOCATION,      HAS_PARAM},
    {ALARM_HOME_GLASS_BREAKAGE,                 ST_ALARM_HOME_GLASS_BREAKAGE,               NO_PARAM},
    {ALARM_HOME_MOTION_DETECTION_LOCATION,      ST_ALARM_HOME_MOTION_DETECTION_LOCATION,    HAS_PARAM},
    {ALARM_HOME_MOTION_DETECTION,               ST_ALARM_HOME_MOTION_DETECTION,             NO_PARAM},
    {ALARM_HOME_TEMPERATING_MOVED,              ST_ALARM_HOME_TEMPERATING_MOVED,            NO_PARAM},
    {ALARM_HOME_UNKHOWN_EVENT_STATE,            ST_ALARM_HOME_UNKHOWN_EVENT_STATE,          NO_PARAM},
};

source_select_t power_management[]=
{
    {ALARM_PM_STATE_IDLE,                   ST_ALARM_PM_STATE_IDLE,                 NO_PARAM},
    {ALARM_PM_POWER_APPLIED,                ST_ALARM_PM_POWER_APPLIED,              NO_PARAM},
    {ALARM_PM_AC_DISCONNECTED,              ST_ALARM_PM_AC_DISCONNECTED,            NO_PARAM},
    {ALARM_PM_AC_REDISCONNECT,              ST_ALARM_PM_AC_REDISCONNECT,            NO_PARAM},
    {ALARM_PM_SURGE_DETECTED,               ST_ALARM_PM_SURGE_DETECTED,             NO_PARAM},
    {ALARM_PM_VOTAGE_DROP_DRIFT,            ST_ALARM_PM_VOTAGE_DROP_DRIFT,          NO_PARAM},
    {ALARM_PM_OVERCURRENT_DETECTED,         ST_ALARM_PM_OVERCURRENT_DETECTED,       NO_PARAM},
    {ALARM_PM_OVERVOTAGE_DETECTED,          ST_ALARM_PM_OVERVOTAGE_DETECTED,        NO_PARAM},
    {ALARM_PM_OVERLOAD_DETECTED,            ST_ALARM_PM_OVERLOAD_DETECTED,          NO_PARAM},
    {ALARM_PM_LOAD_ERROR,                   ST_ALARM_PM_LOAD_ERROR,                 NO_PARAM},
    {ALARM_PM_REPLACE_BATTERY_SOON,         ST_ALARM_PM_REPLACE_BATTERY_SOON,       NO_PARAM},
    {ALARM_PM_REPLACE_BATTERY_NOW,          ST_ALARM_PM_REPLACE_BATTERY_NOW,        NO_PARAM},
    {ALARM_PM_BATTERY_FLUID_LOW,            ST_ALARM_PM_BATTERY_FLUID_LOW,          NO_PARAM},
    {ALARM_PM_BATTERY_CHARGING,             ST_ALARM_PM_BATTERY_CHARGING,           NO_PARAM},
    {ALARM_PM_BATTERY_FULLY_CHARGED,        ST_ALARM_PM_BATTERY_FULLY_CHARGED,      NO_PARAM},
    {ALARM_PM_CHARGE_BATTERY_SOON,          ST_ALARM_PM_CHARGE_BATTERY_SOON,        NO_PARAM},
    {ALARM_PM_CHARGE_BATTERY_NOW,           ST_ALARM_PM_CHARGE_BATTERY_NOW,         NO_PARAM},
    {ALARM_PM_BACKUP_BATTERY_LOW,           ST_ALARM_PM_BACKUP_BATTERY_LOW,         NO_PARAM},
    {ALARM_PM_UNKHOWN_EVENT_STATE,          ST_ALARM_PM_UNKHOWN_EVENT_STATE,        NO_PARAM},
};

source_select_t system_notification[]=
{
    {ALARM_SYSTEM_STATE_IDLE,                       ST_ALARM_SYSTEM_STATE_IDLE,                         NO_PARAM},
    {ALARM_SYSTEM_HARDWARE_FAILLURE,                ST_ALARM_SYSTEM_HARDWARE_FAILLURE,                  NO_PARAM},
    {ALARM_SYSTEM_HARDWARE_FAILLURE_MANUFACTUER,    ST_ALARM_SYSTEM_HARDWARE_FAILLURE_MANUFACTUER,      NO_PARAM},
    {ALARM_SYSTEM_SOFTWARE_FAILURE,                 ST_ALARM_SYSTEM_SOFTWARE_FAILURE,                   NO_PARAM},
    {ALARM_SYSTEM_SOFTWARE_FAILURE_MANUFACTURE,     ST_ALARM_SYSTEM_SOFTWARE_FAILURE_MANUFACTURE,       NO_PARAM},
    {ALARM_SYSTEM_HEART_BEAT,                       ST_ALARM_SYSTEM_HEART_BEAT,                         NO_PARAM},
    {ALARM_SYSTEM_TEMPERATING_PRODUCT_REMOVE,       ST_ALARM_SYSTEM_TEMPERATING_PRODUCT_REMOVE,         NO_PARAM},
    {ALARM_SYSTEM_EMERGENCY_SHUTOFF,                ST_ALARM_SYSTEM_EMERGENCY_SHUTOFF,                  NO_PARAM},
    {ALARM_SYSTEM_UNKHOWN_EVENT_STATE,              ST_ALARM_SYSTEM_UNKHOWN_EVENT_STATE,                NO_PARAM},
};

source_select_t emergency_alarm[]=
{
    {ALARM_EMERGENCY_STATE_IDLE,                ST_ALARM_EMERGENCY_STATE_IDLE,                      NO_PARAM},
    {ALARM_EMERGENCY_CONTACT_POLICE,            ST_ALARM_EMERGENCY_CONTACT_POLICE,                  NO_PARAM},
    {ALARM_EMERGENCY_CONTACT_FIRE_SERVICE,      ST_ALARM_EMERGENCY_CONTACT_FIRE_SERVICE,            NO_PARAM},
    {ALARM_EMERGENCY_CONTACT_MEDICAL_SERVICE,   ST_ALARM_EMERGENCY_CONTACT_MEDICAL_SERVICE,         NO_PARAM},
    {ALARM_EMERGENCY_UNKHOWN_EVENT_STATE,       ST_ALARM_EMERGENCY_UNKHOWN_EVENT_STATE,             NO_PARAM}, 
};

source_select_t clock_notification[]=
{
    {ALARM_CLOCK_STATE_IDLE,             ST_ALARM_CLOCK_STATE_IDLE,             NO_PARAM},
    {ALARM_CLOCK_WAKE_UP_ALERT,          ST_ALARM_CLOCK_WAKE_UP_ALERT,          NO_PARAM},
    {ALARM_CLOCK_TIMER_ENDED,            ST_ALARM_CLOCK_TIMER_ENDED,            NO_PARAM},
    {ALARM_CLOCK_TIME_REMAINING,         ST_ALARM_CLOCK_TIME_REMAINING,         HAS_PARAM},
    {ALARM_CLOCK_UNKHOWN_EVENT_STATE,    ST_ALARM_CLOCK_UNKHOWN_EVENT_STATE,    NO_PARAM},
};

source_select_t appliance[]=
{
    {ALARM_APP_STATE_IDLE,                          ST_ALARM_APP_STATE_IDLE,                    NO_PARAM},
    {ALARM_APP_PROGRAM_STARTED,                     ST_ALARM_APP_PROGRAM_STARTED,               NO_PARAM},
    {ALARM_APP_PROGRAM_PROGRESS,                    ST_ALARM_APP_PROGRAM_PROGRESS,              NO_PARAM},
    {ALARM_APP_PROGRAM_COMPLETED,                   ST_ALARM_APP_PROGRAM_COMPLETED,             NO_PARAM},
    {ALARM_APP_REPLACE_MAIN_FILTER,                 ST_ALARM_APP_REPLACE_MAIN_FILTER,           NO_PARAM},
    {ALARM_APP_SUPPLYING_WATER,                     ST_ALARM_APP_SUPPLYING_WATER,               NO_PARAM},
    {ALARM_APP_BOILLING,                            ST_ALARM_APP_BOILLING,                      NO_PARAM},
    {ALARM_APP_WASHING,                             ST_ALARM_APP_WASHING,                       NO_PARAM},
    {ALARM_APP_RINSING,                             ST_ALARM_APP_RINSING,                       NO_PARAM},
    {ALARM_APP_DRAINING,                            ST_ALARM_APP_DRAINING,                      NO_PARAM},
    {ALARM_APP_SPINNING,                            ST_ALARM_APP_SPINNING,                      NO_PARAM},
    {ALARM_APP_DRYING,                              ST_ALARM_APP_DRYING,                        NO_PARAM},
    {ALARM_APP_FAILURE_SET_TARGET_TEMPERATURE,      ST_ALARM_APP_FAILURE_SET_TARGET_TEMPERATURE,NO_PARAM},
    {ALARM_APP_WATTER_SUPPLY_FAILURE,               ST_ALARM_APP_WATTER_SUPPLY_FAILURE,         NO_PARAM},
    {ALARM_APP_BOILLING_FAILURE,                    ST_ALARM_APP_BOILLING_FAILURE,              NO_PARAM},
    {ALARM_APP_WASHING_FAILURE,                     ST_ALARM_APP_WASHING_FAILURE,               NO_PARAM},
    {ALARM_APP_RINSING_FAILURE,                     ST_ALARM_APP_RINSING_FAILURE,               NO_PARAM},
    {ALARM_APP_DRAINING_FAILURE,                    ST_ALARM_APP_DRAINING_FAILURE,              NO_PARAM},
    {ALARM_APP_SPINNING_FAILURE,                    ST_ALARM_APP_SPINNING_FAILURE,              NO_PARAM},
    {ALARM_APP_DRYING_FAILURE,                      ST_ALARM_APP_DRYING_FAILURE,                NO_PARAM},
    {ALARM_APP_FAN_FAILURE,                         ST_ALARM_APP_FAN_FAILURE,                   NO_PARAM},
    {ALARM_APP_COMPRESSOR_FAILURE,                  ST_ALARM_APP_COMPRESSOR_FAILURE,            NO_PARAM},
    {ALARM_APP_UNKHOWN_EVENT_STATE,                 ST_ALARM_APP_UNKHOWN_EVENT_STATE,           NO_PARAM},
};

source_select_t home_health[]=
{
    {ALARM_HOME_HEALTH_STATE_IDLE,              ST_ALARM_HOME_HEALTH_STATE_IDLE,            NO_PARAM},
    {ALARM_HOME_HEALTH_LEAVING_BED,             ST_ALARM_HOME_HEALTH_LEAVING_BED,           NO_PARAM},
    {ALARM_HOME_HEALTH_SITTING_ON_BED,          ST_ALARM_HOME_HEALTH_SITTING_ON_BED,        NO_PARAM},
    {ALARM_HOME_HEALTH_LYING_ON_BED,            ST_ALARM_HOME_HEALTH_LYING_ON_BED,          NO_PARAM},
    {ALARM_HOME_HEALTH_SITTING_ON_BED_ADGE,     ST_ALARM_HOME_HEALTH_SITTING_ON_BED_ADGE,   NO_PARAM},
    {ALARM_HOME_HEALTH_POSTURE_CHANGED,         ST_ALARM_HOME_HEALTH_POSTURE_CHANGED,       NO_PARAM},
    {ALARM_HOME_HEALTH_VOLATILE_ORGANIC_LEVEL,  ST_ALARM_HOME_HEALTH_VOLATILE_ORGANIC,      HAS_PARAM},
    {ALARM_HOME_HEALTH_UNKHOWN_EVENT_STATE,     ST_ALARM_HOME_HEALTH_UNKHOWN_EVENT_STATE,   NO_PARAM},
};

source_select_t siren[]=
{
    {ALARM_SIREN_STATE_IDLE,             ST_ALARM_SIREN_STATE_IDLE,                 NO_PARAM},
    {ALARM_SIREN_ACTIVE,                 ST_ALARM_SIREN_ACTIVE,                     NO_PARAM},
    {ALARM_SIREN_UNKHOWN_EVENT_STATE,    ST_ALARM_SIREN_UNKHOWN_EVENT_STATE,        NO_PARAM},
};

source_select_t water_valve[]=
{
    {ALARM_WATER_VALVE_STATE_IDLE,              ST_ALARM_WATER_VALVE_STATE_IDLE,                      NO_PARAM},
    {ALARM_WATER_VALVE_OPERATION,               ST_ALARM_WATER_VALVE_OPERATION,                       HAS_PARAM},
    {ALARM_WATER_MASTER_VALVE_OPERATION,        ST_ALARM_WATER_MASTER_VALVE_OPERATION,                HAS_PARAM},
    {ALARM_WATER_VALVE_SHORT_CIRCUIT,           ST_ALARM_WATER_VALVE_SHORT_CIRCUIT,                   NO_PARAM},
    {ALARM_WATER_MASTER_VALVE_SHORT_CIRCUIT,    ST_ALARM_WATER_MASTER_VALVE_SHORT_CIRCUIT,            NO_PARAM},
    {ALARM_WATER_VALVE_CURRENT_ALARM,           ST_ALARM_WATER_VALVE_CURRENT_ALARM,                   HAS_PARAM},
    {ALARM_WATER_MASTER_VALVE_CURRENT_ALARM,    ST_ALARM_WATER_MASTER_VALVE_CURRENT_ALARM,            HAS_PARAM},
    {ALARM_WATER_UNKHOWN_EVENT_STATE,           ST_ALARM_WATER_VALVE_UNKHOWN_EVENT_STATE,             NO_PARAM},
};

source_select_t weather_alarm[]=
{
    {ALARM_WEATHER_STATE_IDLE,                      ST_ALARM_WEATHER_STATE_IDLE,                    NO_PARAM},
    {ALARM_WEATHER_RAIN_ALARM,                      ST_ALARM_WEATHER_RAIN_ALARM,                    NO_PARAM},
    {ALARM_WEATHER_MOISTURE_ALARM,                  ST_ALARM_WEATHER_MOISTURE_ALARM,                NO_PARAM},
    {ALARM_WEATHER_FREEZE_ALARM,                    ST_ALARM_WEATHER_FREEZE_ALARM,                  NO_PARAM},
    {ALARM_WEATHER_UNKHOWN_EVENT_STATE,             ST_ALARM_WEATHER_UNKHOWN_EVENT_STATE,           NO_PARAM},
};

source_select_t irrigation[]=
{
    {ALARM_IRRIGATION_STATE_IDLE,                               ST_ALARM_IRRIGATION_STATE_IDLE,                            NO_PARAM},
    {ALARM_IRRIGATION_SCHEDULE_STARTED,                         ST_ALARM_IRRIGATION_SCHEDULE_STARTED,                      NO_PARAM},
    {ALARM_IRRIGATION_SCHEDULE_FINISHED,                        ST_ALARM_IRRIGATION_SCHEDULE_FINISHED,                     NO_PARAM},
    {ALARM_IRRIGATION_VALVE_TABLE_RUN_STARTED,                  ST_ALARM_IRRIGATION_VALVE_TABLE_RUN_STARTED,               NO_PARAM},
    {ALARM_IRRIGATION_VALVE_TABLE_RUN_FINISHED,                 ST_ALARM_IRRIGATION_VALVE_TABLE_RUN_FINISHED,              NO_PARAM},
    {ALARM_IRRIGATION_DEVICE_NOT_CONFIGURED,                    ST_ALARM_IRRIGATION_DEVICE_NOT_CONFIGURED,                 NO_PARAM},
    {ALARM_IRRIGATION_UNKHOWN_EVENT_STATE,                      ST_ALARM_IRRIGATION_UNKHOWN_EVENT_STATE,                   NO_PARAM},
};

source_select_t gas_alarm[]=
{
    {ALARM_GAS_STATE_IDLE,                              ST_ALARM_GAS_STATE_IDLE,                                NO_PARAM},
    {ALARM_GAS_COMBUSTIBLE_GAS_DETECTED_LOCATION,       ST_ALARM_GAS_COMBUSTIBLE_GAS_DETECTED_LOCATION,         HAS_PARAM},
    {ALARM_GAS_COMBUSTIBLE_GAS_DETECTED,                ST_ALARM_GAS_COMBUSTIBLE_GAS_DETECTED,                  NO_PARAM},
    {ALARM_GAS_TOXIC_GAS_DETECTED_LOCATION,             ST_ALARM_GAS_TOXIC_GAS_DETECTED_LOCATION,               HAS_PARAM},
    {ALARM_GAS_TOXIC_GAS_DETECTED,                      ST_ALARM_GAS_TOXIC_GAS_DETECTED,                        NO_PARAM},
    {ALARM_GAS_ALARM_TEST,                              ST_ALARM_GAS_ALARM_TEST,                                NO_PARAM},
    {ALARM_GAS_REPLACEMENT_REQUIRED,                    ST_ALARM_GAS_REPLACEMENT_REQUIRED,                      NO_PARAM},
    {ALARM_GAS_UNKHOWN_EVENT_STATE,                     ST_ALARM_GAS_UNKHOWN_EVENT_STATE,                       NO_PARAM},
};

source_select_t pest_control[]=
{
    {ALARM_PEST_STATE_IDLE,                              ST_ALARM_PEST_STATE_IDLE,                           NO_PARAM},
    {ALARM_PEST_TRAP_ARMED_LOCATION,                     ST_ALARM_PEST_TRAP_ARMED_LOCATION,                  HAS_PARAM},
    {ALARM_PEST_TRAP_ARMED,                              ST_ALARM_PEST_TRAP_ARMED,                           NO_PARAM},
    {ALARM_PEST_TRAP_REARMED_REQUIRED_LOCATION,          ST_ALARM_PEST_TRAP_REARMED_REQUIRED_LOCATION,       HAS_PARAM},
    {ALARM_PEST_TRAP_REARMED_REQUIRED,                   ST_ALARM_PEST_TRAP_REARMED_REQUIRED,                NO_PARAM},
    {ALARM_PEST_DETECTED_LOCATION,                       ST_ALARM_PEST_DETECTED_LOCATION,                    HAS_PARAM},
    {ALARM_PEST_DETECTED,                                ST_ALARM_PEST_DETECTED,                             NO_PARAM},
    {ALARM_PEST_EXTERMINATED_LOCATION,                   ST_ALARM_PEST_EXTERMINATED_LOCATION,                HAS_PARAM},
    {ALARM_PEST_EXTERMINATED,                            ST_ALARM_PEST_EXTERMINATED,                         NO_PARAM},
    {ALARM_PEST_UNKHOWN_EVENT_STATE,                     ST_ALARM_PEST_UNKHOWN_EVENT_STATE,                  NO_PARAM},
};

class_infor_t notificationTable[]=
{
    {ALARM_GENERIC_SMOKE_ALARM,   ST_ALARM_GENERIC_SMOKE_ALARM,   sizeof(smoke_alarm)/sizeof(source_select_t),    smoke_alarm},
    {ALARM_GENERIC_CO_ALARM,      ST_ALARM_GENERIC_CO_ALARM,      sizeof(co_alarm)/sizeof(source_select_t),       co_alarm},
    {ALARM_GENERIC_CO2_ALARM,     ST_ALARM_GENERIC_CO2_ALARM,     sizeof(co2_alarm)/sizeof(source_select_t),      co2_alarm},
    {ALARM_GENERIC_HEAT_ALARM,    ST_ALARM_GENERIC_HEAT_ALARM,    sizeof(heat_alarm)/sizeof(source_select_t),     heat_alarm},
    {ALARM_GENERIC_WATER_ALARM,   ST_ALARM_GENERIC_WATER_ALARM,   sizeof(water_alarm)/sizeof(source_select_t),    water_alarm},
    {ALARM_GENERIC_ACCESS_CONTROL,   ST_ALARM_GENERIC_ACCESS_CONTROL,   sizeof(access_control)/sizeof(source_select_t),   access_control},
    {ALARM_GENERIC_HOME_SECURITY,    ST_ALARM_GENERIC_HOME_SECURITY,    sizeof(home_security)/sizeof(source_select_t),    home_security},
    {ALARM_GENERIC_POWER_MANAGEMENT, ST_ALARM_GENERIC_POWER_MANAGEMENT, sizeof(power_management)/sizeof(source_select_t), power_management},
    {ALARM_GENERIC_SYSTEM,        ST_ALARM_GENERIC_SYSTEM,        sizeof(system_notification)/sizeof(source_select_t),    system_notification},
    {ALARM_GENERIC_EMERGENCY_ALARM,  ST_ALARM_GENERIC_EMERGENCY_ALARM,  sizeof(emergency_alarm)/sizeof(source_select_t),  emergency_alarm},
    {ALARM_GENERIC_CLOCK,         ST_ALARM_GENERIC_CLOCK,         sizeof(clock_notification)/sizeof(source_select_t),     clock_notification},
    {ALARM_GENERIC_APPLIANCE,     ST_ALARM_GENERIC_APPLIANCE,     sizeof(appliance)/sizeof(source_select_t),       appliance},
    {ALARM_GENERIC_HOME_HEALTH,   ST_ALARM_GENERIC_HOME_HEALTH,   sizeof(home_health)/sizeof(source_select_t),     home_health},
    {ALARM_GENERIC_SIREN,         ST_ALARM_GENERIC_SIREN,         sizeof(siren)/sizeof(source_select_t),           siren},
    {ALARM_GENERIC_WATER_VALVE,   ST_ALARM_GENERIC_WATER_VALVE,   sizeof(water_valve)/sizeof(source_select_t),     water_valve},
    {ALARM_GENERIC_WEATHER_ALARM, ST_ALARM_GENERIC_WEATHER_ALARM, sizeof(weather_alarm)/sizeof(source_select_t),   weather_alarm},
    {ALARM_GENERIC_IRRIGATION,    ST_ALARM_GENERIC_IRRIGATION,    sizeof(irrigation)/sizeof(source_select_t),      irrigation},
    {ALARM_GENERIC_GAS_ALARM,     ST_ALARM_GENERIC_GAS_ALARM,     sizeof(gas_alarm)/sizeof(source_select_t),       gas_alarm},
    {ALARM_GENERIC_PEST_CONTROL,  ST_ALARM_GENERIC_PEST_CONTROL,  sizeof(pest_control)/sizeof(source_select_t),    pest_control},
};

alarm_meaning_t alarmMeaning[]=
{
    {ST_ALARM_SMOKE_STATE_IDLE                               ,ST_ALARM_MEANING_SMOKE_STATE_IDLE},
    {ST_ALARM_SMOKE_DETECTED_LOCATION                        ,ST_ALARM_MEANING_SMOKE_DETECTED},
    {ST_ALARM_SMOKE_DETECTED                                 ,ST_ALARM_MEANING_SMOKE_DETECTED},
    {ST_ALARM_SMOKE_ALARM_TEST                               ,ST_ALARM_MEANING_SMOKE_ALARM_TEST},
    {ST_ALARM_SMOKE_REPLACEMENT_REQUIRED                     ,ST_ALARM_MEANING_SMOKE_REPLACEMENT_REQUIRED},
    {ST_ALARM_SMOKE_REPLACEMENT_REQUIRED_EOL                 ,ST_ALARM_MEANING_SMOKE_REPLACEMENT_REQUIRED_EOL},
    {ST_ALARM_SMOKE_ALARM_SILENCED                           ,ST_ALARM_MEANING_SMOKE_ALARM_SILENCED},
    {ST_ALARM_SMOKE_MAINTENANCE_REQUIRED_PPI                 ,ST_ALARM_MEANING_SMOKE_MAINTENANCE_REQUIRED_PPI},
    {ST_ALARM_SMOKE_MAINTENANCE_REQUIRED_DID                 ,ST_ALARM_MEANING_SMOKE_MAINTENANCE_REQUIRED_DID},
    {ST_ALARM_SMOKE_UNKHOWN_EVENT_STATE                      ,ST_ALARM_MEANING_SMOKE_UNKHOWN_EVENT_STATE},

    {ST_ALARM_CO_STATE_IDLE                                  ,ST_ALARM_MEANING_CO_STATE_IDLE},
    {ST_ALARM_CO_DETECTED_LOCATION                           ,ST_ALARM_MEANING_CO_DETECTED},
    {ST_ALARM_CO_DETECTED                                    ,ST_ALARM_MEANING_CO_DETECTED},
    {ST_ALARM_CO_TEST                                        ,ST_ALARM_MEANING_CO_TEST},
    {ST_ALARM_CO_TEST_OK                                     ,ST_ALARM_MEANING_CO_TEST_OK},
    {ST_ALARM_CO_TEST_FAILED                                 ,ST_ALARM_MEANING_CO_TEST_FAILED},
    {ST_ALARM_CO_REPLACEMENT_REQUIRED                        ,ST_ALARM_MEANING_CO_REPLACEMENT_REQUIRED},
    {ST_ALARM_CO_REPLACEMENT_REQUIRED_EOL                    ,ST_ALARM_MEANING_CO_REPLACEMENT_REQUIRED_EOL},
    {ST_ALARM_CO_ALARM_SILENCED                              ,ST_ALARM_MEANING_CO_ALARM_SILENCED},
    {ST_ALARM_CO_MAINTENANCE_REQUIRED_PPI                    ,ST_ALARM_MEANING_CO_MAINTENANCE_REQUIRED_PPI},
    {ST_ALARM_CO_UNKHOWN_EVENT_STATE                         ,ST_ALARM_MEANING_CO_UNKHOWN_EVENT_STATE},

    {ST_ALARM_CO2_STATE_IDLE                                 ,ST_ALARM_MEANING_CO2_STATE_IDLE},
    {ST_ALARM_CO2_DETECTED_LOCATION                          ,ST_ALARM_MEANING_CO2_DETECTED},
    {ST_ALARM_CO2_DETECTED                                   ,ST_ALARM_MEANING_CO2_DETECTED},
    {ST_ALARM_CO2_TEST                                       ,ST_ALARM_MEANING_CO2_TEST},
    {ST_ALARM_CO2_TEST_OK                                    ,ST_ALARM_MEANING_CO2_TEST_OK},
    {ST_ALARM_CO2_TEST_FAILED                                ,ST_ALARM_MEANING_CO2_TEST_FAILED},
    {ST_ALARM_CO2_REPLACEMENT_REQUIRED                       ,ST_ALARM_MEANING_CO2_REPLACEMENT_REQUIRED},
    {ST_ALARM_CO2_REPLACEMENT_REQUIRED_EOL                   ,ST_ALARM_MEANING_CO2_REPLACEMENT_REQUIRED_EOL},
    {ST_ALARM_CO2_ALARM_SILENCED                             ,ST_ALARM_MEANING_CO2_ALARM_SILENCED},
    {ST_ALARM_CO2_MAINTENANCE_REQUIRED_PPI                   ,ST_ALARM_MEANING_CO2_MAINTENANCE_REQUIRED_PPI},
    {ST_ALARM_CO2_UNKHOWN_EVENT_STATE                        ,ST_ALARM_MEANING_CO2_UNKHOWN_EVENT_STATE},

    {ST_ALARM_HEAT_STATE_IDLE                                ,ST_ALARM_MEANING_HEAT_STATE_IDLE},
    {ST_ALARM_HEAT_OVERHEAT_DETECTED_LOCATION                ,ST_ALARM_MEANING_HEAT_OVERHEAT_DETECTED},
    {ST_ALARM_HEAT_OVERHEAT_DETECTED                         ,ST_ALARM_MEANING_HEAT_OVERHEAT_DETECTED},
    {ST_ALARM_HEAT_UNDER_HEAT_DETECTED_LOCATION              ,ST_ALARM_MEANING_HEAT_UNDER_HEAT_DETECTED},
    {ST_ALARM_HEAT_UNDER_HEAT_DETECTED                       ,ST_ALARM_MEANING_HEAT_UNDER_HEAT_DETECTED},
    {ST_ALARM_HEAT_RAPID_TEMPERATURE_RISE_LOCATION           ,ST_ALARM_MEANING_HEAT_RAPID_TEMPERATURE_RISE},
    {ST_ALARM_HEAT_RAPID_TEMPERATURE_RISE                    ,ST_ALARM_MEANING_HEAT_RAPID_TEMPERATURE_RISE},
    {ST_ALARM_HEAT_RAPID_TEMPERATURE_FALL_LOCATION           ,ST_ALARM_MEANING_HEAT_RAPID_TEMPERATURE_FALL},
    {ST_ALARM_HEAT_RAPID_TEMPERATURE_FALL                    ,ST_ALARM_MEANING_HEAT_RAPID_TEMPERATURE_FALL},
    {ST_ALARM_HEAT_ALARM_TEST                                ,ST_ALARM_MEANING_HEAT_ALARM_TEST},
    {ST_ALARM_HEAT_REPLACEMENT_REQUIRED                      ,ST_ALARM_MEANING_HEAT_REPLACEMENT_REQUIRED},
    {ST_ALARM_HEAT_REPLACEMENT_REQUIRED_EOL                  ,ST_ALARM_MEANING_HEAT_REPLACEMENT_REQUIRED_EOL},
    {ST_ALARM_HEAT_ALARM_SILENCED                            ,ST_ALARM_MEANING_HEAT_ALARM_SILENCED},
    {ST_ALARM_HEAT_MAINTENANCE_REQUIRED_PPI                  ,ST_ALARM_MEANING_HEAT_MAINTENANCE_REQUIRED_PPI},
    {ST_ALARM_HEAT_MAINTENANCE_REQUIRED_DID                  ,ST_ALARM_MEANING_HEAT_MAINTENANCE_REQUIRED_DID},
    {ST_ALARM_HEAT_UNKHOWN_EVENT_STATE                       ,ST_ALARM_MEANING_HEAT_UNKHOWN_EVENT_STATE},

    {ST_ALARM_WATER_STATE_IDLE                               ,ST_ALARM_MEANING_WATER_STATE_IDLE},
    {ST_ALARM_WATER_LEAK_DETECTED_LOCATION                   ,ST_ALARM_MEANING_WATER_LEAK_DETECTED_LOCATION},
    {ST_ALARM_WATER_LEAK_DETECTED                            ,ST_ALARM_MEANING_WATER_LEAK_DETECTED},
    {ST_ALARM_WATER_LEVEL_DROPPED_LOCATION                   ,ST_ALARM_MEANING_WATER_LEVEL_DROPPED_LOCATION},
    {ST_ALARM_WATER_LEVEL_DROPPED                            ,ST_ALARM_MEANING_WATER_LEVEL_DROPPED},
    {ST_ALARM_WATER_REPLACE_WATER_FILTER                     ,ST_ALARM_MEANING_WATER_REPLACE_WATER_FILTER},
    {ST_ALARM_WATER_FLOW_ALARM                               ,ST_ALARM_MEANING_WATER_FLOW_ALARM},
    {ST_ALARM_WATER_FLOW_ALARM_NODATA                        ,ST_ALARM_MEANING_WATER_FLOW_ALARM_NODATA},
    {ST_ALARM_WATER_FLOW_ALARM_LOW_THRESHOLD                 ,ST_ALARM_MEANING_WATER_FLOW_ALARM_LOW_THRESHOLD},
    {ST_ALARM_WATER_FLOW_ALARM_HIGH_THRESHOLD                ,ST_ALARM_MEANING_WATER_FLOW_ALARM_HIGH_THRESHOLD},
    {ST_ALARM_WATER_FLOW_ALARM_MAX                           ,ST_ALARM_MEANING_WATER_FLOW_ALARM_MAX},
    {ST_ALARM_WATER_PRESSURE_ALARM                           ,ST_ALARM_MEANING_WATER_PRESSURE_ALARM},
    {ST_ALARM_WATER_PRESSURE_ALARM_NODATA                    ,ST_ALARM_MEANING_WATER_PRESSURE_ALARM_NODATA},
    {ST_ALARM_WATER_PRESSURE_ALARM_LOW_THRESHOLD             ,ST_ALARM_MEANING_WATER_PRESSURE_ALARM_LOW_THRESHOLD},
    {ST_ALARM_WATER_PRESSURE_ALARM_HIGH_THRESHOLD            ,ST_ALARM_MEANING_WATER_PRESSURE_ALARM_HIGH_THRESHOLD},
    {ST_ALARM_WATER_PRESSURE_ALARM_MAX                       ,ST_ALARM_MEANING_WATER_PRESSURE_ALARM_MAX},
    {ST_ALARM_WATER_TEMPERATURE_ALARM                        ,ST_ALARM_MEANING_WATER_TEMPERATURE_ALARM},
    {ST_ALARM_WATER_TEMPERATURE_ALARM_NODATA                 ,ST_ALARM_MEANING_WATER_TEMPERATURE_ALARM_NODATA},
    {ST_ALARM_WATER_TEMPERATURE_ALARM_LOW_THRESHOLD          ,ST_ALARM_MEANING_WATER_TEMPERATURE_ALARM_LOW_THRESHOLD},
    {ST_ALARM_WATER_TEMPERATURE_ALARM_HIGH_THRESHOLD         ,ST_ALARM_MEANING_WATER_TEMPERATURE_ALARM_HIGH_THRESHOLD},
    {ST_ALARM_WATER_LEVEL_ALARM                              ,ST_ALARM_MEANING_WATER_LEVEL_ALARM},
    {ST_ALARM_WATER_LEVEL_ALARM_NODATA                       ,ST_ALARM_MEANING_WATER_LEVEL_ALARM_NODATA},
    {ST_ALARM_WATER_LEVEL_ALARM_LOW_THRESHOLD                ,ST_ALARM_MEANING_WATER_LEVEL_ALARM_LOW_THRESHOLD},
    {ST_ALARM_WATER_LEVEL_ALARM_HIGH_THRESHOLD               ,ST_ALARM_MEANING_WATER_LEVEL_ALARM_HIGH_THRESHOLD},
    {ST_ALARM_WATER_SUMP_PUMP_ACTIVE                         ,ST_ALARM_MEANING_WATER_SUMP_PUMP_ACTIVE},
    {ST_ALARM_WATER_SUMP_PUMP_FAILURE                        ,ST_ALARM_MEANING_WATER_SUMP_PUMP_FAILURE},
    {ST_ALARM_WATER_UNKHOWN_EVENT_STATE                      ,ST_ALARM_MEANING_WATER_UNKHOWN_EVENT_STATE},

    {ST_ALARM_AC_STATE_IDLE                                  ,ST_ALARM_MEANING_AC_STATE_IDLE},
    {ST_ALARM_AC_MANUAL_LOCK_OPERATION                       ,ST_ALARM_MEANING_AC_MANUAL_LOCK_OPERATION},
    {ST_ALARM_AC_MANUAL_UNLOCK_OPERATION                     ,ST_ALARM_MEANING_AC_MANUAL_UNLOCK_OPERATION},
    {ST_ALARM_AC_RF_LOCK_OPERATION                           ,ST_ALARM_MEANING_AC_RF_LOCK_OPERATION},
    {ST_ALARM_AC_RF_UNLOCK_OPERATION                         ,ST_ALARM_MEANING_AC_RF_UNLOCK_OPERATION},
    {ST_ALARM_AC_KEYPAD_LOCK_OPERATION                       ,ST_ALARM_MEANING_AC_KEYPAD_LOCK_OPERATION},
    {ST_ALARM_AC_KEYPAD_UNLOCK_OPERATION                     ,ST_ALARM_MEANING_AC_KEYPAD_UNLOCK_OPERATION},
    {ST_ALARM_AC_MANUAL_NOT_FULLY_LOCKED_OPERATION           ,ST_ALARM_MEANING_AC_MANUAL_NOT_FULLY_LOCKED_OPERATION},
    {ST_ALARM_AC_RF_NOT_FULLY_LOCKED_OPERATION               ,ST_ALARM_MEANING_AC_RF_NOT_FULLY_LOCKED_OPERATION},
    {ST_ALARM_AC_AUTO_LOCK_LOCKED_OPERATION                  ,ST_ALARM_MEANING_AC_AUTO_LOCK_LOCKED_OPERATION},
    {ST_ALARM_AC_AUTO_LOCK_NOT_FULLY_LOCKED_OPERATION        ,ST_ALARM_MEANING_AC_AUTO_LOCK_NOT_FULLY_LOCKED_OPERATION},
    {ST_ALARM_AC_LOCK_JAMMED                                 ,ST_ALARM_MEANING_AC_LOCK_JAMMED},
    {ST_ALARM_AC_ALL_USER_CODE_DELETED                       ,ST_ALARM_MEANING_AC_ALL_USER_CODE_DELETED},
    {ST_ALARM_AC_SINGLE_USER_CODE_DELETED                    ,ST_ALARM_MEANING_AC_SINGLE_USER_CODE_DELETED},
    {ST_ALARM_AC_NEW_USER_CODE_ADDED                         ,ST_ALARM_MEANING_AC_NEW_USER_CODE_ADDED},
    {ST_ALARM_AC_NEW_USER_CODE_NOT_ADDED                     ,ST_ALARM_MEANING_AC_NEW_USER_CODE_NOT_ADDED},
    {ST_ALARM_AC_KEYPAD_TEMPORARY_DISABLED                   ,ST_ALARM_MEANING_AC_KEYPAD_TEMPORARY_DISABLED},
    {ST_ALARM_AC_KEYPAD_BUSY                                 ,ST_ALARM_MEANING_AC_KEYPAD_BUSY},
    {ST_ALARM_AC_NEW_PROGRAM_CODE_ENTERED                    ,ST_ALARM_MEANING_AC_NEW_PROGRAM_CODE_ENTERED},
    {ST_ALARM_AC_MANUALY_ENTER_USER_ACCESS_CODE_EXCEEDED     ,ST_ALARM_MEANING_AC_MANUALY_ENTER_USER_ACCESS_CODE_EXCEEDED},
    {ST_ALARM_AC_UNLOCK_RF_INVALID_USER_CODE                 ,ST_ALARM_MEANING_AC_UNLOCK_RF_INVALID_USER_CODE},
    {ST_ALARM_AC_LOCKED_RF_INVALID_USER_CODE                 ,ST_ALARM_MEANING_AC_LOCKED_RF_INVALID_USER_CODE},
    {ST_ALARM_AC_WINDOW_DOOR_OPEN                            ,ST_ALARM_MEANING_AC_WINDOW_DOOR_OPEN},
    {ST_ALARM_AC_WINDOW_DOOR_CLOSED                          ,ST_ALARM_MEANING_AC_WINDOW_DOOR_CLOSED},
    {ST_ALARM_AC_WINDOW_DOOR_HANDLE_OPEN                     ,ST_ALARM_MEANING_AC_WINDOW_DOOR_HANDLE_OPEN},
    {ST_ALARM_AC_WINDOW_DOOR_HANDLE_CLOSED                   ,ST_ALARM_MEANING_AC_WINDOW_DOOR_HANDLE_CLOSED},
    {ST_ALARM_AC_BARRIER_PERFORMING_INIT_PROCESS_PERFORMING  ,ST_ALARM_MEANING_AC_BARRIER_PERFORMING_INIT_PROCESS_PERFORMING},
    {ST_ALARM_AC_BARRIER_PERFORMING_INIT_PROCESS_COMPLETED   ,ST_ALARM_MEANING_AC_BARRIER_PERFORMING_INIT_PROCESS_COMPLETED},
    {ST_ALARM_AC_BARRIER_OPERATION_FORCE_EXCEEDED            ,ST_ALARM_MEANING_AC_BARRIER_OPERATION_FORCE_EXCEEDED},
    {ST_ALARM_AC_BARRIER_MOTOR_EXCEEDED                      ,ST_ALARM_MEANING_AC_BARRIER_MOTOR_EXCEEDED},
    {ST_ALARM_AC_BARRIER_OPERATION_PHYSICAL_EXCEEDED         ,ST_ALARM_MEANING_AC_BARRIER_OPERATION_PHYSICAL_EXCEEDED},
    {ST_ALARM_AC_BARRIER_UNABLE_PERFORM_REQUEST              ,ST_ALARM_MEANING_AC_BARRIER_UNABLE_PERFORM_REQUEST},
    {ST_ALARM_AC_BARRIER_UNATTENDED_DISABLED                 ,ST_ALARM_MEANING_AC_BARRIER_UNATTENDED_DISABLED},
    {ST_ALARM_AC_BARRIER_MALFUNCTION                         ,ST_ALARM_MEANING_AC_BARRIER_MALFUNCTION},
    {ST_ALARM_AC_BARRIER_VACATION_MODE_DISABLE               ,ST_ALARM_MEANING_AC_BARRIER_VACATION_MODE_DISABLE},
    {ST_ALARM_AC_BARRIER_VACATION_MODE_ENABLE                ,ST_ALARM_MEANING_AC_BARRIER_VACATION_MODE_ENABLE},
    {ST_ALARM_AC_BARRIER_SAFETY_BEAM_OBSTACLE_NO_OBSTRUCTION ,ST_ALARM_MEANING_AC_BARRIER_SAFETY_BEAM_OBSTACLE_NO_OBSTRUCTION},
    {ST_ALARM_AC_BARRIER_SAFETY_BEAM_OBSTACLE_OBSTRUCTION    ,ST_ALARM_MEANING_AC_BARRIER_SAFETY_BEAM_OBSTACLE_OBSTRUCTION},
    {ST_ALARM_AC_BARRIER_SENSOR_NOT_DETECTED                 ,ST_ALARM_MEANING_AC_BARRIER_SENSOR_NOT_DETECTED},
    {ST_ALARM_AC_BARRIER_SENSOR_LOW_BATTERY_WARNING          ,ST_ALARM_MEANING_AC_BARRIER_SENSOR_LOW_BATTERY_WARNING},
    {ST_ALARM_AC_BARRIER_DETECTED_SHORT                      ,ST_ALARM_MEANING_AC_BARRIER_DETECTED_SHORT},
    {ST_ALARM_AC_BARRIER_ASSOCIATED_WITH_NON_ZWAVE_REMOTE    ,ST_ALARM_MEANING_AC_BARRIER_ASSOCIATED_WITH_NON_ZWAVE_REMOTE},
    {ST_ALARM_AC_UNKHOWN_EVENT_STATE                         ,ST_ALARM_MEANING_AC_UNKHOWN_EVENT_STATE},

    {ST_ALARM_HOME_STATE_IDLE                                ,ST_ALARM_MEANING_HOME_STATE_IDLE},
    {ST_ALARM_HOME_INTRUSION_LOCATION                        ,ST_ALARM_MEANING_HOME_INTRUSION_LOCATION},
    {ST_ALARM_HOME_INTRUSION                                 ,ST_ALARM_MEANING_HOME_INTRUSION},
    {ST_ALARM_HOME_TEMPERATING_PRODUCT_REMOVE                ,ST_ALARM_MEANING_HOME_TEMPERATING_PRODUCT_REMOVE},
    {ST_ALARM_HOME_TEMPERATING_INVALID_CODE                  ,ST_ALARM_MEANING_HOME_TEMPERATING_INVALID_CODE},
    {ST_ALARM_HOME_GLASS_BREAKAGE_LOCATION                   ,ST_ALARM_MEANING_HOME_GLASS_BREAKAGE_LOCATION},
    {ST_ALARM_HOME_GLASS_BREAKAGE                            ,ST_ALARM_MEANING_HOME_GLASS_BREAKAGE},
    {ST_ALARM_HOME_MOTION_DETECTION_LOCATION                 ,ST_ALARM_MEANING_HOME_MOTION_DETECTION_LOCATION},
    {ST_ALARM_HOME_MOTION_DETECTION                          ,ST_ALARM_MEANING_HOME_MOTION_DETECTION},
    {ST_ALARM_HOME_TEMPERATING_MOVED                         ,ST_ALARM_MEANING_HOME_TEMPERATING_MOVED},
    {ST_ALARM_HOME_UNKHOWN_EVENT_STATE                       ,ST_ALARM_MEANING_HOME_UNKHOWN_EVENT_STATE},

    {ST_ALARM_PM_STATE_IDLE                                  ,ST_ALARM_MEANING_PM_STATE_IDLE},
    {ST_ALARM_PM_POWER_APPLIED                               ,ST_ALARM_MEANING_PM_POWER_APPLIED},
    {ST_ALARM_PM_AC_DISCONNECTED                             ,ST_ALARM_MEANING_PM_AC_DISCONNECTED},
    {ST_ALARM_PM_AC_REDISCONNECT                             ,ST_ALARM_MEANING_PM_AC_REDISCONNECT},
    {ST_ALARM_PM_SURGE_DETECTED                              ,ST_ALARM_MEANING_PM_SURGE_DETECTED},
    {ST_ALARM_PM_VOTAGE_DROP_DRIFT                           ,ST_ALARM_MEANING_PM_VOTAGE_DROP_DRIFT},
    {ST_ALARM_PM_OVERCURRENT_DETECTED                        ,ST_ALARM_MEANING_PM_OVERCURRENT_DETECTED},
    {ST_ALARM_PM_OVERVOTAGE_DETECTED                         ,ST_ALARM_MEANING_PM_OVERVOTAGE_DETECTED},
    {ST_ALARM_PM_OVERLOAD_DETECTED                           ,ST_ALARM_MEANING_PM_OVERLOAD_DETECTED},
    {ST_ALARM_PM_LOAD_ERROR                                  ,ST_ALARM_MEANING_PM_LOAD_ERROR},
    {ST_ALARM_PM_REPLACE_BATTERY_SOON                        ,ST_ALARM_MEANING_PM_REPLACE_BATTERY_SOON},
    {ST_ALARM_PM_REPLACE_BATTERY_NOW                         ,ST_ALARM_MEANING_PM_REPLACE_BATTERY_NOW},
    {ST_ALARM_PM_BATTERY_FLUID_LOW                           ,ST_ALARM_MEANING_PM_BATTERY_FLUID_LOW},
    {ST_ALARM_PM_BATTERY_CHARGING                            ,ST_ALARM_MEANING_PM_BATTERY_CHARGING},
    {ST_ALARM_PM_BATTERY_FULLY_CHARGED                       ,ST_ALARM_MEANING_PM_BATTERY_FULLY_CHARGED},
    {ST_ALARM_PM_CHARGE_BATTERY_SOON                         ,ST_ALARM_MEANING_PM_CHARGE_BATTERY_SOON},
    {ST_ALARM_PM_CHARGE_BATTERY_NOW                          ,ST_ALARM_MEANING_PM_CHARGE_BATTERY_NOW},
    {ST_ALARM_PM_BACKUP_BATTERY_LOW                          ,ST_ALARM_MEANING_PM_BACKUP_BATTERY_LOW},
    {ST_ALARM_PM_UNKHOWN_EVENT_STATE                         ,ST_ALARM_MEANING_PM_UNKHOWN_EVENT_STATE},

    {ST_ALARM_SYSTEM_STATE_IDLE                              ,ST_ALARM_MEANING_SYSTEM_STATE_IDLE},
    {ST_ALARM_SYSTEM_HARDWARE_FAILLURE                       ,ST_ALARM_MEANING_SYSTEM_HARDWARE_FAILLURE},
    {ST_ALARM_SYSTEM_HARDWARE_FAILLURE_MANUFACTUER           ,ST_ALARM_MEANING_SYSTEM_HARDWARE_FAILLURE_MANUFACTUER},
    {ST_ALARM_SYSTEM_SOFTWARE_FAILURE                        ,ST_ALARM_MEANING_SYSTEM_SOFTWARE_FAILURE},
    {ST_ALARM_SYSTEM_SOFTWARE_FAILURE_MANUFACTURE            ,ST_ALARM_MEANING_SYSTEM_SOFTWARE_FAILURE_MANUFACTURE},
    {ST_ALARM_SYSTEM_HEART_BEAT                              ,ST_ALARM_MEANING_SYSTEM_HEART_BEAT},
    {ST_ALARM_SYSTEM_TEMPERATING_PRODUCT_REMOVE              ,ST_ALARM_MEANING_SYSTEM_TEMPERATING_PRODUCT_REMOVE},
    {ST_ALARM_SYSTEM_EMERGENCY_SHUTOFF                       ,ST_ALARM_MEANING_SYSTEM_EMERGENCY_SHUTOFF},
    {ST_ALARM_SYSTEM_UNKHOWN_EVENT_STATE                     ,ST_ALARM_MEANING_SYSTEM_UNKHOWN_EVENT_STATE},

    {ST_ALARM_EMERGENCY_STATE_IDLE                           ,ST_ALARM_MEANING_EMERGENCY_STATE_IDLE},
    {ST_ALARM_EMERGENCY_CONTACT_POLICE                       ,ST_ALARM_MEANING_EMERGENCY_CONTACT_POLICE},
    {ST_ALARM_EMERGENCY_CONTACT_FIRE_SERVICE                 ,ST_ALARM_MEANING_EMERGENCY_CONTACT_FIRE_SERVICE},
    {ST_ALARM_EMERGENCY_CONTACT_MEDICAL_SERVICE              ,ST_ALARM_MEANING_EMERGENCY_CONTACT_MEDICAL_SERVICE},
    {ST_ALARM_EMERGENCY_UNKHOWN_EVENT_STATE                  ,ST_ALARM_MEANING_EMERGENCY_UNKHOWN_EVENT_STATE},

    {ST_ALARM_CLOCK_STATE_IDLE                               ,ST_ALARM_MEANING_CLOCK_STATE_IDLE},
    {ST_ALARM_CLOCK_WAKE_UP_ALERT                            ,ST_ALARM_MEANING_CLOCK_WAKE_UP_ALERT},
    {ST_ALARM_CLOCK_TIMER_ENDED                              ,ST_ALARM_MEANING_CLOCK_TIMER_ENDED},
    {ST_ALARM_CLOCK_TIME_REMAINING                           ,ST_ALARM_MEANING_CLOCK_TIME_REMAINING},
    {ST_ALARM_CLOCK_UNKHOWN_EVENT_STATE                      ,ST_ALARM_MEANING_CLOCK_UNKHOWN_EVENT_STATE},

    {ST_ALARM_APP_STATE_IDLE                                 ,ST_ALARM_MEANING_APP_STATE_IDLE},
    {ST_ALARM_APP_PROGRAM_STARTED                            ,ST_ALARM_MEANING_APP_PROGRAM_STARTED},
    {ST_ALARM_APP_PROGRAM_PROGRESS                           ,ST_ALARM_MEANING_APP_PROGRAM_PROGRESS},
    {ST_ALARM_APP_PROGRAM_COMPLETED                          ,ST_ALARM_MEANING_APP_PROGRAM_COMPLETED},
    {ST_ALARM_APP_REPLACE_MAIN_FILTER                        ,ST_ALARM_MEANING_APP_REPLACE_MAIN_FILTER},
    {ST_ALARM_APP_SUPPLYING_WATER                            ,ST_ALARM_MEANING_APP_SUPPLYING_WATER},
    {ST_ALARM_APP_BOILLING                                   ,ST_ALARM_MEANING_APP_BOILLING},
    {ST_ALARM_APP_WASHING                                    ,ST_ALARM_MEANING_APP_WASHING},
    {ST_ALARM_APP_RINSING                                    ,ST_ALARM_MEANING_APP_RINSING},
    {ST_ALARM_APP_RINSING                                    ,ST_ALARM_MEANING_APP_RINSING},
    {ST_ALARM_APP_DRAINING                                   ,ST_ALARM_MEANING_APP_DRAINING},
    {ST_ALARM_APP_SPINNING                                   ,ST_ALARM_MEANING_APP_SPINNING},
    {ST_ALARM_APP_DRYING                                     ,ST_ALARM_MEANING_APP_DRYING},
    {ST_ALARM_APP_FAILURE_SET_TARGET_TEMPERATURE             ,ST_ALARM_MEANING_APP_FAILURE_SET_TARGET_TEMPERATURE},
    {ST_ALARM_APP_WATTER_SUPPLY_FAILURE                      ,ST_ALARM_MEANING_APP_WATTER_SUPPLY_FAILURE},
    {ST_ALARM_APP_BOILLING_FAILURE                           ,ST_ALARM_MEANING_APP_BOILLING_FAILURE},
    {ST_ALARM_APP_WASHING_FAILURE                            ,ST_ALARM_MEANING_APP_WASHING_FAILURE},
    {ST_ALARM_APP_RINSING_FAILURE                            ,ST_ALARM_MEANING_APP_RINSING_FAILURE},
    {ST_ALARM_APP_DRAINING_FAILURE                           ,ST_ALARM_MEANING_APP_DRAINING_FAILURE},
    {ST_ALARM_APP_SPINNING_FAILURE                           ,ST_ALARM_MEANING_APP_SPINNING_FAILURE},
    {ST_ALARM_APP_DRYING_FAILURE                             ,ST_ALARM_MEANING_APP_DRYING_FAILURE},
    {ST_ALARM_APP_FAN_FAILURE                                ,ST_ALARM_MEANING_APP_FAN_FAILURE},
    {ST_ALARM_APP_COMPRESSOR_FAILURE                         ,ST_ALARM_MEANING_APP_COMPRESSOR_FAILURE},
    {ST_ALARM_APP_UNKHOWN_EVENT_STATE                        ,ST_ALARM_MEANING_APP_UNKHOWN_EVENT_STATE},

    {ST_ALARM_HOME_HEALTH_STATE_IDLE                         ,ST_ALARM_MEANING_HOME_HEALTH_STATE_IDLE},
    {ST_ALARM_HOME_HEALTH_LEAVING_BED                        ,ST_ALARM_MEANING_HOME_HEALTH_LEAVING_BED},
    {ST_ALARM_HOME_HEALTH_SITTING_ON_BED                     ,ST_ALARM_MEANING_HOME_HEALTH_SITTING_ON_BED},
    {ST_ALARM_HOME_HEALTH_LYING_ON_BED                       ,ST_ALARM_MEANING_HOME_HEALTH_LYING_ON_BED},
    {ST_ALARM_HOME_HEALTH_SITTING_ON_BED_ADGE                ,ST_ALARM_MEANING_HOME_HEALTH_SITTING_ON_BED_ADGE},
    {ST_ALARM_HOME_HEALTH_POSTURE_CHANGED                    ,ST_ALARM_MEANING_HOME_HEALTH_POSTURE_CHANGED},
    {ST_ALARM_HOME_HEALTH_VOLATILE_ORGANIC_CLEAN             ,ST_ALARM_MEANING_HOME_HEALTH_VOLATILE_ORGANIC_CLEAN},
    {ST_ALARM_HOME_HEALTH_VOLATILE_ORGANIC_SLIGHTLY          ,ST_ALARM_MEANING_HOME_HEALTH_VOLATILE_ORGANIC_SLIGHTLY},
    {ST_ALARM_HOME_HEALTH_VOLATILE_ORGANIC_MODERATELY        ,ST_ALARM_MEANING_HOME_HEALTH_VOLATILE_ORGANIC_MODERATELY},
    {ST_ALARM_HOME_HEALTH_VOLATILE_ORGANIC_HIGHLY            ,ST_ALARM_MEANING_HOME_HEALTH_VOLATILE_ORGANIC_HIGHLY},
    {ST_ALARM_HOME_HEALTH_UNKHOWN_EVENT_STATE                ,ST_ALARM_MEANING_HOME_HEALTH_UNKHOWN_EVENT_STATE},

    {ST_ALARM_SIREN_STATE_IDLE                               ,ST_ALARM_MEANING_SIREN_STATE_IDLE},
    {ST_ALARM_SIREN_ACTIVE                                   ,ST_ALARM_MEANING_SIREN_ACTIVE},
    {ST_ALARM_SIREN_UNKHOWN_EVENT_STATE                      ,ST_ALARM_MEANING_SIREN_UNKHOWN_EVENT_STATE},

    {ST_ALARM_WATER_VALVE_STATE_IDLE                         ,ST_ALARM_MEANING_WATER_VALVE_STATE_IDLE},
    {ST_ALARM_WATER_VALVE_OPERATION_ON                       ,ST_ALARM_MEANING_WATER_VALVE_OPERATION_ON},
    {ST_ALARM_WATER_VALVE_OPERATION_OFF                      ,ST_ALARM_MEANING_WATER_VALVE_OPERATION_OFF},
    {ST_ALARM_WATER_MASTER_VALVE_OPERATION_ON                ,ST_ALARM_MEANING_WATER_MASTER_VALVE_OPERATION_ON},
    {ST_ALARM_WATER_MASTER_VALVE_OPERATION_OFF               ,ST_ALARM_MEANING_WATER_MASTER_VALVE_OPERATION_OFF},
    {ST_ALARM_WATER_VALVE_SHORT_CIRCUIT                      ,ST_ALARM_MEANING_WATER_VALVE_SHORT_CIRCUIT},
    {ST_ALARM_WATER_MASTER_VALVE_SHORT_CIRCUIT               ,ST_ALARM_MEANING_WATER_MASTER_VALVE_SHORT_CIRCUIT},
    {ST_ALARM_WATER_VALVE_CURRENT_ALARM                      ,ST_ALARM_MEANING_WATER_VALVE_CURRENT_ALARM},
    {ST_ALARM_WATER_VALVE_CURRENT_ALARM_NODATA               ,ST_ALARM_MEANING_WATER_VALVE_CURRENT_ALARM_NODATA},
    {ST_ALARM_WATER_VALVE_CURRENT_ALARM_LOW_THRESHOLD        ,ST_ALARM_MEANING_WATER_VALVE_CURRENT_ALARM_LOW_THRESHOLD},
    {ST_ALARM_WATER_VALVE_CURRENT_ALARM_HIGH_THRESHOLD       ,ST_ALARM_MEANING_WATER_VALVE_CURRENT_ALARM_HIGH_THRESHOLD},
    {ST_ALARM_WATER_VALVE_CURRENT_ALARM_MAX                  ,ST_ALARM_MEANING_WATER_VALVE_CURRENT_ALARM_MAX},
    {ST_ALARM_WATER_MASTER_VALVE_CURRENT_ALARM               ,ST_ALARM_MEANING_WATER_MASTER_VALVE_CURRENT_ALARM},
    {ST_ALARM_WATER_MASTER_VALVE_CURRENT_ALARM_NODATA        ,ST_ALARM_MEANING_WATER_MASTER_VALVE_CURRENT_ALARM_NODATA},
    {ST_ALARM_WATER_MASTER_VALVE_CURRENT_ALARM_LOW_THRESHOLD ,ST_ALARM_MEANING_WATER_MASTER_VALVE_CURRENT_ALARM_LOW_THRESHOLD},
    {ST_ALARM_WATER_MASTER_VALVE_CURRENT_ALARM_HIGH_THRESHOLD,ST_ALARM_MEANING_WATER_MASTER_VALVE_CURRENT_ALARM_HIGH_THRESHOLD},
    {ST_ALARM_WATER_MASTER_VALVE_CURRENT_ALARM_MAX           ,ST_ALARM_MEANING_WATER_MASTER_VALVE_CURRENT_ALARM_MAX},
    {ST_ALARM_WATER_VALVE_UNKHOWN_EVENT_STATE                ,ST_ALARM_MEANING_WATER_VALVE_UNKHOWN_EVENT_STATE},

    {ST_ALARM_WEATHER_STATE_IDLE                             ,ST_ALARM_MEANING_WEATHER_STATE_IDLE},
    {ST_ALARM_WEATHER_RAIN_ALARM                             ,ST_ALARM_MEANING_WEATHER_RAIN_ALARM},
    {ST_ALARM_WEATHER_MOISTURE_ALARM                         ,ST_ALARM_MEANING_WEATHER_MOISTURE_ALARM},
    {ST_ALARM_WEATHER_FREEZE_ALARM                           ,ST_ALARM_MEANING_WEATHER_FREEZE_ALARM},
    {ST_ALARM_WEATHER_UNKHOWN_EVENT_STATE                    ,ST_ALARM_MEANING_WEATHER_UNKHOWN_EVENT_STATE},

    {ST_ALARM_IRRIGATION_STATE_IDLE                          ,ST_ALARM_MEANING_IRRIGATION_STATE_IDLE},
    {ST_ALARM_IRRIGATION_SCHEDULE_STARTED                    ,ST_ALARM_MEANING_IRRIGATION_SCHEDULE_STARTED},
    {ST_ALARM_IRRIGATION_SCHEDULE_FINISHED                   ,ST_ALARM_MEANING_IRRIGATION_SCHEDULE_FINISHED},
    {ST_ALARM_IRRIGATION_VALVE_TABLE_RUN_STARTED             ,ST_ALARM_MEANING_IRRIGATION_VALVE_TABLE_RUN_STARTED},
    {ST_ALARM_IRRIGATION_VALVE_TABLE_RUN_FINISHED            ,ST_ALARM_MEANING_IRRIGATION_VALVE_TABLE_RUN_FINISHED},
    {ST_ALARM_IRRIGATION_DEVICE_NOT_CONFIGURED               ,ST_ALARM_MEANING_IRRIGATION_DEVICE_NOT_CONFIGURED},
    {ST_ALARM_IRRIGATION_UNKHOWN_EVENT_STATE                 ,ST_ALARM_MEANING_IRRIGATION_UNKHOWN_EVENT_STATE},

    {ST_ALARM_GAS_STATE_IDLE                                 ,ST_ALARM_MEANING_GAS_STATE_IDLE},
    {ST_ALARM_GAS_COMBUSTIBLE_GAS_DETECTED_LOCATION          ,ST_ALARM_MEANING_GAS_COMBUSTIBLE_GAS_DETECTED_LOCATION},
    {ST_ALARM_GAS_COMBUSTIBLE_GAS_DETECTED                   ,ST_ALARM_MEANING_GAS_COMBUSTIBLE_GAS_DETECTED},
    {ST_ALARM_GAS_TOXIC_GAS_DETECTED_LOCATION                ,ST_ALARM_MEANING_GAS_TOXIC_GAS_DETECTED_LOCATION},
    {ST_ALARM_GAS_TOXIC_GAS_DETECTED                         ,ST_ALARM_MEANING_GAS_TOXIC_GAS_DETECTED},
    {ST_ALARM_GAS_ALARM_TEST                                 ,ST_ALARM_MEANING_GAS_ALARM_TEST},
    {ST_ALARM_GAS_REPLACEMENT_REQUIRED                       ,ST_ALARM_MEANING_GAS_REPLACEMENT_REQUIRED},
    {ST_ALARM_GAS_UNKHOWN_EVENT_STATE                        ,ST_ALARM_MEANING_GAS_UNKHOWN_EVENT_STATE},

    {ST_ALARM_PEST_STATE_IDLE                                ,ST_ALARM_MEANING_PEST_STATE_IDLE},
    {ST_ALARM_PEST_TRAP_ARMED_LOCATION                       ,ST_ALARM_MEANING_PEST_TRAP_ARMED_LOCATION},
    {ST_ALARM_PEST_TRAP_ARMED                                ,ST_ALARM_MEANING_PEST_TRAP_ARMED},
    {ST_ALARM_PEST_TRAP_REARMED_REQUIRED_LOCATION            ,ST_ALARM_MEANING_PEST_TRAP_REARMED_REQUIRED_LOCATION},
    {ST_ALARM_PEST_TRAP_REARMED_REQUIRED                     ,ST_ALARM_MEANING_PEST_TRAP_REARMED_REQUIRED},
    {ST_ALARM_PEST_DETECTED_LOCATION                         ,ST_ALARM_MEANING_PEST_DETECTED_LOCATION},
    {ST_ALARM_PEST_DETECTED                                  ,ST_ALARM_MEANING_PEST_DETECTED},
    {ST_ALARM_PEST_EXTERMINATED_LOCATION                     ,ST_ALARM_MEANING_PEST_EXTERMINATED_LOCATION},
    {ST_ALARM_PEST_EXTERMINATED                              ,ST_ALARM_MEANING_PEST_EXTERMINATED},
    {ST_ALARM_PEST_UNKHOWN_EVENT_STATE                       ,ST_ALARM_MEANING_PEST_UNKHOWN_EVENT_STATE},
};

/*#####################################################################################*/
/*###########################           Central Scene           #########################*/
/*#####################################################################################*/
source_select_t centralSceneSupportedTable[]=
{
    {SUPPORTED_KEY_ATTRIBUTES_PRESS_1_TIME,                 ST_KEY_PRESSED_1_TIME},
    {SUPPORTED_KEY_ATTRIBUTES_RELEASED,                     ST_KEY_RELEASED},
    {SUPPORTED_KEY_ATTRIBUTES_HELD_DOWN,                    ST_KEY_HELD_DOWN},
    {SUPPORTED_KEY_ATTRIBUTES_PRESS_2_TIME,                 ST_KEY_PRESSED_2_TIME},
    {SUPPORTED_KEY_ATTRIBUTES_PRESS_3_TIME,                 ST_KEY_PRESSED_3_TIME},
    {SUPPORTED_KEY_ATTRIBUTES_PRESS_4_TIME,                 ST_KEY_PRESSED_4_TIME},
    {SUPPORTED_KEY_ATTRIBUTES_PRESS_5_TIME,                 ST_KEY_PRESSED_5_TIME},
};
/*#####################################################################################*/
/*###########################        BARRIER        #########################*/
/*#####################################################################################*/

source_select_t barrier_signal[]=
{
    {BARRIER_SIGNAL_AUDIBLE_NOTIFICATION,                     ST_BARRIER_SIGNAL_AUDIBLE_NOTIFICATION                  },
    {BARRIER_SIGNAL_VISAUL_NOTIFICATION,                      ST_BARRIER_SIGNAL_VISAUL_NOTIFICATION                   },
};

static const char* get_name_from_id(uint8_t id, source_select_t table[], int table_len)
{
    int i;

    for(i = 0; i < table_len; i++)
    {
        if(table[i].id == id)
        {
            return table[i].name;
        }
    }

    return ST_UNKNOWN;
}

static const char* get_class_from_id(uint8_t id, class_infor_t table[], int table_len)
{
    int i;

    for(i = 0; i < table_len; i++)
    {
        if(table[i].id == id)
        {
            return table[i].name;
        }
    }

    return ST_UNKNOWN;
}

static int get_option_from_id(uint8_t id, source_select_t table[], int table_len)
{
    int i;
    for(i =0; i < table_len; i++)
    {
        if(table[i].id == id)
        {
            return table[i].option;
        }
    }

    return -1;
}

static int get_id_from_name(char *name, source_select_t table[], int table_len)
{
    int i;

    if(!name)
    {
        return -1;
    }

    for(i =0; i < table_len; i++)
    {
        if(!strcmp(name, table[i].name))
        {
            return table[i].id;
        }
    }

    return -1;
}

static int get_option_from_name(char *name, source_select_t table[], int table_len)
{
    int i;

    if(!name)
    {
        return -1;
    }
    
    for(i =0; i < table_len; i++)
    {
        if(!strcmp(name, table[i].name))
        {
            return table[i].option;
        }
    }

    return -1;
}
//need free output after using
static void get_value_bit_mode(uint8_t mask[], uint8_t mask_len, 
                      source_select_t table[], uint8_t table_len,
                      char **output)
{
    int i, j;

    char *result = (char *)malloc(1);
    result[0] = '\0';

    for(i = 0;i < mask_len; i++)
    {
        for (j = 0; j < 8; j++) 
        {
            if (mask[i]&(1<<j)) 
            {
                uint8_t id = j+8*i;
                SLOGI("id  = %d\n", id);
                const char *mode = get_name_from_id(id, table, table_len);
                SLOGI("mode  = %s\n", mode);
                char *new_result = (char*)realloc(result, strlen(result)+strlen(mode)+32);
                if(!new_result)
                {
                    SLOGE("Error (re)allocating memory\n");
                    *output = result;
                    return;
                }

                result = new_result;
                size_t result_len = strlen(result);
                if(!result_len)
                {
                    sprintf(result, "%s", mode);
                }
                else
                {
                    sprintf(result+result_len, ",%s", mode);
                }
            }
        }
    }

    *output = result;
}

//need free output after using
static void get_class_bit_mode(uint8_t mask[], uint8_t mask_len, 
                      class_infor_t table[], uint8_t table_len,
                      char **output)
{
    int i, j;

    char *result = (char *)malloc(1);
    result[0] = '\0';

    for(i = 0;i < mask_len; i++)
    {
        for (j = 0; j < 8; j++) 
        {
            if (mask[i]&(1<<j)) 
            {
                uint8_t id = j+8*i;
                const char *mode = get_class_from_id(id, table, table_len);
                char *new_result = (char*)realloc(result, strlen(result)+strlen(mode)+32);
                if(!new_result)
                {
                    SLOGE("Error (re)allocating memory\n");
                    *output = result;
                    return;
                }

                result = new_result;
                size_t result_len = strlen(result);
                if(!result_len)
                {
                    sprintf(result, "%s", mode);
                }
                else
                {
                    sprintf(result+result_len, ",%s", mode);
                }
            }
        }
    }

    *output = result;
}

/*return sensor name, sensor Unit name if sensor Unit is valid*/
const char* get_sensor_infor_from_id(int sensorId, int sensorUnit, const char **sensorUnitName, 
                                    int *option, class_infor_t multisensorTable[], uint8_t tableLen)
{
    if(sensorId < 0)
    {
        return ST_UNKNOWN;
    }

    int i;
    for(i =0; i < tableLen; i++)
    {
        if(sensorId == multisensorTable[i].id)
        {
            if(sensorUnit<0)
            {
                return multisensorTable[i].name;
            }

            if(sensorUnitName)
            {
                *sensorUnitName = get_name_from_id(sensorUnit, multisensorTable[i].unitTable, multisensorTable[i].unitTableLen);
            }

            if(option)
            {
                *option = get_option_from_id(sensorUnit, multisensorTable[i].unitTable, multisensorTable[i].unitTableLen);
            }

            return multisensorTable[i].name;
        }
    }

    return ST_UNKNOWN;
}

/*return sensor id, sensor Unit if sensor Unit Name is valid*/
static int get_sensor_id_from_name(char *sensorName, char *sensorUnitName, int *sensorUnit,
                                   uint8_t *option, class_infor_t multisensorTable[], uint8_t tableLen)
{
    if(!sensorName)
    {
        return -1;
    }

    int i;
    for(i =0; i < tableLen; i++)
    {
        if(!strcmp(sensorName, multisensorTable[i].name))
        {
            if(!sensorUnitName)
            {
                return multisensorTable[i].id;
            }

            if(sensorUnit)
            {
                *sensorUnit = get_id_from_name(sensorUnitName, multisensorTable[i].unitTable, multisensorTable[i].unitTableLen);
            }

            if(option)
            {
                *option = get_option_from_name(sensorUnitName, multisensorTable[i].unitTable, multisensorTable[i].unitTableLen);
            }

            return multisensorTable[i].id;
        }
    }

    return -1;;
}

/*return sensor name, all unit supported*/
const char* get_class_scale_bit_mode(int meterType, uint8_t mask[], uint8_t mask_len,
                                          class_infor_t meterTable[], uint8_t tableLen, char **output)
{
    if(meterType < 0)
    {
        return ST_UNKNOWN;
    }

    int i;
    for(i =0; i < tableLen; i++)
    {
        if(meterType == meterTable[i].id)
        {
            get_value_bit_mode(mask, mask_len, meterTable[i].unitTable, meterTable[i].unitTableLen, output);
            return meterTable[i].name;
        }
    }

    return ST_UNKNOWN;
}

const char *get_param_info(uint8_t notificationType, uint8_t notificationId, uint8_t paramIn)
{
    int i;
    for(i = 0; i <  sizeof(notificationParam)/sizeof(notification_param_t); i++)
    {
        if((notificationType == notificationParam[i].notificationType) &&
            (notificationId == notificationParam[i].notificationId))
        {
            if(!notificationParam[i].tableLen)
            {
                return NULL;
            }

            return get_name_from_id(paramIn, notificationParam[i].table, notificationParam[i].tableLen);
        }
    }

    return ST_UNKNOWN;
}

static const char *get_name_unsupported_from_id(uint8_t id, uint8_t specific_type, unSupported_device_type table[], int table_len)
{
    int i;
    int j;
    for(i = 0; i < table_len; i++)
    {
        if(table[i].deviceType == id)
        {
            for (j = 0; j < table[i].specificSize; j++)
             {
                if(table[i].specific[j].typeId == specific_type)
                {
                    return table[i].specific[j].defaultName;
                }
             } 
        }
    }
    return ST_UNKNOWN;
}

///////////// for thermostat //////////////////////
int get_thermostat_setpoint_id(char *name)
{
    return get_id_from_name(name, thermostatSetPointTable, sizeof(thermostatSetPointTable)/sizeof(source_select_t));
}

int get_thermostat_mode_id(char *name)
{
    return get_id_from_name(name, thermostatModeTable, sizeof(thermostatModeTable)/sizeof(source_select_t));
}

int get_thermostat_fan_mode_id(char *name)
{
    return get_id_from_name(name, thermostatFanModeTable, sizeof(thermostatFanModeTable)/sizeof(source_select_t));
}

const char *get_thermostat_mode_name(uint8_t id)
{
    return get_name_from_id(id, thermostatModeTable, sizeof(thermostatModeTable)/sizeof(source_select_t));
}

const char *get_thermostat_setpoint_name(uint8_t id)
{
    return get_name_from_id(id, thermostatSetPointTable, sizeof(thermostatSetPointTable)/sizeof(source_select_t));
}

const char *get_thermostat_operating_name(uint8_t id)
{
    return get_name_from_id(id, thermostatOperatingTable, sizeof(thermostatOperatingTable)/sizeof(source_select_t));
}

const char *get_thermostat_fan_mode_name(uint8_t id)
{
    return get_name_from_id(id, thermostatFanModeTable, sizeof(thermostatFanModeTable)/sizeof(source_select_t));
}

const char *get_thermostat_fan_state_name(uint8_t id)
{
    return get_name_from_id(id, thermostatFanStateTable, sizeof(thermostatFanStateTable)/sizeof(source_select_t));
}

void get_thermostat_mode_from_bit_mask(uint8_t mask[], uint8_t mask_len, char **output)
{
    get_value_bit_mode(mask, mask_len, thermostatModeTable, sizeof(thermostatModeTable)/sizeof(source_select_t), output);
}

void get_thermostat_setpoint_from_bit_mask(uint8_t mask[], uint8_t mask_len, char **output)
{
    get_value_bit_mode(mask, mask_len, thermostatSetPointTable, sizeof(thermostatSetPointTable)/sizeof(source_select_t), output);
}

/////////////for sensor multilevel //////////////////////
/*return sensor name and sensor unit name from sensorID and sensorUnit*/
const char *get_sensor_name(int sensorId, int sensorUnit, const char **sensorUnitName)
{
    return get_sensor_infor_from_id(sensorId, sensorUnit, sensorUnitName, 
                                    NULL,sensorMultilevelTable, 
                                    sizeof(sensorMultilevelTable)/sizeof(class_infor_t));
}
/*return sensor id and sensor unit from sensor name and sensor Unit name*/
int get_sensor_id(char *sensorName, char *sensorUnitName, int *sensorUnit)
{
    return get_sensor_id_from_name(sensorName, sensorUnitName, sensorUnit, NULL,
                                    sensorMultilevelTable, 
                                    sizeof(sensorMultilevelTable)/sizeof(class_infor_t));
}

/*return output is list sensor support from bitmask*/
void get_sensor_multilevel_from_bit_mask(uint8_t mask[], uint8_t mask_len, char **output)
{
    get_class_bit_mode(mask, mask_len, sensorMultilevelTable, sizeof(sensorMultilevelTable)/sizeof(class_infor_t), output);
}

/*return sensor name parse from sensor ID
output is list of unit in character of sensor ID*/
const char *get_sensor_scale_from_bit_mask(int sensorId, uint8_t mask[], uint8_t mask_len, char **output)
{
    return get_class_scale_bit_mode(sensorId, mask, mask_len, sensorMultilevelTable, sizeof(sensorMultilevelTable)/sizeof(class_infor_t), output);
}

///////////////// for meter ///////////////
/*return meter name from meter id
meter unit name if meterUnit is valid
option return if variable valid*/
const char *get_meter_name(int meterId, int meterUnit, const char **meterUnitName, int *option)
{
    return get_sensor_infor_from_id(meterId, meterUnit, meterUnitName, option, meterTable, 
                                    sizeof(meterTable)/sizeof(class_infor_t));
}
/*return meter id with coresponding meter name
meter unit will be return if meterUnitName exist
the same with option.*/
int get_meter_id(char *meterName, char *meterUnitName, int *meterUnit, uint8_t *option)
{
    return get_sensor_id_from_name(meterName, meterUnitName, meterUnit, option, meterTable, 
                                    sizeof(meterTable)/sizeof(class_infor_t));
}

/*return meter name parse from meter ID
output is list of unit in character of meter ID*/
const char *get_meter_scale_from_bit_mask(int meterType, uint8_t mask[], uint8_t mask_len, char **output)
{
    return get_class_scale_bit_mode(meterType, mask, mask_len, meterTable, sizeof(meterTable)/sizeof(class_infor_t), output);
}

/////////////// for alarm///////////////////////
/*return notification type and notificationName and isParam*/
const char *get_notification_type_name(int notificationType, int notificationId, const char **notificationName, int *isParam)
{
    return get_sensor_infor_from_id(notificationType, notificationId, notificationName, 
                                    isParam, notificationTable, sizeof(notificationTable)/sizeof(class_infor_t));
}

const char *get_alarm_meaning(char *alarmPartern)
{
    if(!alarmPartern)
    {
        return ST_UNKNOWN;
    }

    int i;
    for(i=0; i<sizeof(alarmMeaning)/sizeof(alarm_meaning_t); i++)
    {
        if(!strcmp(alarmMeaning[i].alarmPartern,alarmPartern))
        {
            return alarmMeaning[i].alarmMeaning;
        }
    }
    return ST_UNKNOWN;
}

/////////////// for barrier ///////////////////////
/*because bit 0 will get id 1 so we couldn't reuse get_value_bit_mode function*/
void get_barrier_signal_support_from_bit_mask(uint8_t mask[], uint8_t mask_len, char **output)
{
    // get_value_bit_mode(mask, mask_len, barrier_signal, sizeof(barrier_signal)/sizeof(source_select_t), output);
    int i, j;

    char *result = (char *)malloc(1);
    result[0] = '\0';

    for(i = 0;i < mask_len; i++)
    {
        for (j = 0; j < 8; j++) 
        {
            if (mask[i]&(1<<j)) 
            {
                uint8_t id = j+ 8*i + 1;/*plus 1 is only difference point with get_value_bit_mode*/
                SLOGI("id  = %d\n", id);
                const char *mode = get_name_from_id(id, barrier_signal, sizeof(barrier_signal)/sizeof(source_select_t));
                SLOGI("mode  = %s\n", mode);
                char *new_result = (char*)realloc(result, strlen(result)+strlen(mode)+32);
                if(!new_result)
                {
                    SLOGE("Error (re)allocating memory\n");
                    *output = result;
                    return;
                }

                result = new_result;
                size_t result_len = strlen(result);
                if(!result_len) 
                {
                    sprintf(result, "%s", mode);
                }
                else
                {
                    sprintf(result+result_len, ",%s", mode);
                }
            }
        }
    }

    *output = result;
}

const char *get_barrier_signal_name(int signalId)
{
    return get_name_from_id(signalId, barrier_signal, sizeof(barrier_signal)/sizeof(source_select_t));
}

int get_barrier_signal_id(char *signalName)
{
    return get_id_from_name(signalName, barrier_signal, sizeof(barrier_signal)/sizeof(source_select_t));
}

/////////////// unsupported_name ///////////////////////
const char *get_name_unsupported(uint8_t device_type, uint8_t specific_value)
{

    return get_name_unsupported_from_id(device_type, specific_value, unSupportedDevName, sizeof(unSupportedDevName)/sizeof(unSupported_device_type));
}

void get_supported_key_attributes_from_bit_mask(uint8_t mask[], uint8_t mask_len, char **output)
{
    get_value_bit_mode(mask, mask_len, centralSceneSupportedTable, sizeof(centralSceneSupportedTable)/sizeof(source_select_t), output);
}
