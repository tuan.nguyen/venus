#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "manage_usercode.h"
#include "database.h"
#include "slog.h"

pthread_mutex_t userCodePendingqueueMutex;
user_code_pending_t g_user_code_pending_list;

pthread_mutex_t userCodequeueMutex;
user_code_t g_user_code_list;

static int g_manage_user_code_thread = 1;
extern sqlite3 *zwave_db;

static void init_user_code_pending_list()
{
    VR_INIT_LIST_HEAD(&g_user_code_pending_list.list);
    pthread_mutex_init(&userCodePendingqueueMutex, 0);
}

int check_user_code_info(char *deviceId, uint8_t userId, const char *name)
{
    int res = 0;

    if(!deviceId || !name)
    {
        return res;
    }

    user_code_t *tmp = NULL;
    struct VR_list_head *pos, *q;
    
    VR_(list_for_each_safe)(pos, q, &g_user_code_list.list)
    {
        tmp = VR_(list_entry)(pos, user_code_t, list);
        if(strcmp(tmp->info.deviceId, deviceId))
        {
            continue;
        }

        if(!strcmp(tmp->info.name, name))
        {
            res = USER_CODE_SAME_NAME;
            break;
        }

        if(tmp->info.userId == userId)
        {
            res = USER_CODE_SAME_USER_ID;
            // break;name priority higher than userId
        }
    }

    return res;
}

uint8_t get_user_id_valid(char *deviceId, uint8_t maxUserCode)
{
    int i;
    for(i = 1; i <= maxUserCode; i++)
    {
        int res = 0;
        user_code_t *tmp = NULL;
        struct VR_list_head *pos, *q;

        VR_(list_for_each_safe)(pos, q, &g_user_code_list.list)
        {
            tmp = VR_(list_entry)(pos, user_code_t, list);
            if(strcmp(tmp->info.deviceId, deviceId))
            {
                continue;
            }

            if(tmp->info.userId == i)
            {
                res = USER_CODE_SAME_USER_ID;
                break;
            }
        }

        if(!res)
        {
            return i;
        }
    }

    return maxUserCode+1;
}

static int _add_new_user_code(void *node_add)
{
    int res = 0;
    pthread_mutex_lock(&userCodequeueMutex);
    struct VR_list_head *pos, *q;
    user_code_t *input = (user_code_t *)node_add;
    user_code_t *tmp = NULL;

    VR_(list_for_each_safe)(pos, q, &g_user_code_list.list)
    {
        tmp = VR_(list_entry)(pos, user_code_t, list);
        if(!strcmp(tmp->info.deviceId, input->info.deviceId) &&
           (tmp->info.userId == input->info.userId)
           )
        {
            res = -1;
            break;
        }
    }

    if(!res)
    {
        VR_(list_add_tail)(&(input->list), &(g_user_code_list.list));
    }

    pthread_mutex_unlock(&userCodequeueMutex);
    return res;
}

static int _add_user_code(char *deviceId, char *userId, char *name)
{
    if(!deviceId || !name || !userId)
    {
        return -1;
    }

    user_code_t *user = (user_code_t *)malloc(sizeof(user_code_t));
    memset(user, 0x00, sizeof(user_code_t));

    user->info.deviceId = strdup(deviceId);
    user->info.userId = strtol(userId, NULL, 10);
    user->info.name = strdup(name);

    int res = _add_new_user_code((void *)user);
    if(res)
    {
        free(user->info.deviceId);
        SAFE_FREE(user->info.name);
        free(user);
    }

    return res;
}

void add_user_code(char *deviceId, char *userId, char *name)
{
    _add_user_code(deviceId, userId, name);
    set_door_user_code_database(_VR_CB_(zwave), deviceId, userId, name);
}

void remove_user_code(char *deviceId, uint8_t userId)
{
    if(!deviceId)
    {
        return;
    }

    pthread_mutex_lock(&userCodequeueMutex);
    user_code_t *tmp = NULL;
    struct VR_list_head *pos, *q;

    VR_(list_for_each_safe)(pos, q, &g_user_code_list.list)
    {
        tmp = VR_(list_entry)(pos, user_code_t, list);
        if(!strcmp(tmp->info.deviceId, deviceId) &&
           (tmp->info.userId == userId)
           )
        {
            VR_(list_del)(pos);
            free(tmp->info.deviceId);
            SAFE_FREE(tmp->info.name);
            free(tmp);
            break;
        }
    }
    pthread_mutex_unlock(&userCodequeueMutex);

    char userIdStr[SIZE_32B];
    snprintf(userIdStr, sizeof(userIdStr), "%d", userId);
    remove_door_user_code_database(_VR_(zwave), deviceId, userIdStr);
    return;
}

void remove_all_user_code(char *deviceId)
{
    if(!deviceId)
    {
        return;
    }

    pthread_mutex_lock(&userCodequeueMutex);
    user_code_t *tmp = NULL;
    struct VR_list_head *pos, *q;

    VR_(list_for_each_safe)(pos, q, &g_user_code_list.list)
    {
        tmp = VR_(list_entry)(pos, user_code_t, list);
        if(!strcmp(tmp->info.deviceId, deviceId))
        {
            VR_(list_del)(pos);
            free(tmp->info.deviceId);
            SAFE_FREE(tmp->info.name);
            free(tmp);
        }
    }
    pthread_mutex_unlock(&userCodequeueMutex);

    remove_all_user_code_database(_VR_(zwave), deviceId);
    return;
}

static int get_usercode_callback(void *userCode, int argc, char **argv, char **azColName)
{
    if(argc < 3)
    {
        return 0;
    }

    char *deviceId = argv[0];
    char *userId = argv[1];
    char *name = argv[2];

    json_object *code = json_object_new_object();
    json_object_object_add(code, ST_DEVICE_ID, json_object_new_string(deviceId ? deviceId : ""));
    json_object_object_add(code, ST_USER_ID, json_object_new_string(userId ? userId : ""));
    json_object_object_add(code, ST_NAME, json_object_new_string(name ? name : ""));
    json_object_array_add(userCode, code);

    return 0;
}

void init_user_code_list()
{
    init_user_code_pending_list();

    VR_INIT_LIST_HEAD(&g_user_code_list.list);
    pthread_mutex_init(&userCodequeueMutex, 0);

    json_object *userCode = json_object_new_array();
    searching_database(_VR_(zwave), get_usercode_callback, userCode,
                        "SELECT %s,%s,%s from USER_CODE where type='zwave_handler'",
                        ST_DEVICE_ID, ST_USER_ID, ST_PASS);

    int arraylen = json_object_array_length(userCode);
    int i;
    for(i = 0; i < arraylen; i++)
    {
        json_object *array = json_object_array_get_idx(userCode, i);
        CHECK_JSON_OBJECT_EXIST(deviceIdObj, array, ST_DEVICE_ID, _next_item);
        CHECK_JSON_OBJECT_EXIST(userIdObj, array, ST_USER_ID, _next_item);
        CHECK_JSON_OBJECT_EXIST(nameObj, array, ST_NAME, _next_item);
        const char *deviceId = json_object_get_string(deviceIdObj);
        const char *userId = json_object_get_string(userIdObj);
        const char *name = json_object_get_string(nameObj);

        _add_user_code((char *)deviceId, (char *)userId, (char *)name);

_next_item:
        continue;
    }

    json_object_put(userCode);
}

void printf_user_code_list()
{
    int i=0;
    user_code_pending_t *tmp = NULL;

    VR_(list_for_each_entry)(tmp, &g_user_code_pending_list.list, list)
    {
        printf("#### userCode %d ######\n", i);
        printf("cmd = %d\n", tmp->cmd);
        printf("deviceId = %s\n", tmp->info.deviceId);
        printf("userId = %d\n", tmp->info.userId);
        if(tmp->info.name)
        {
            printf("name = %s\n", tmp->info.name);
        }
        i++;
    }
}

char* remove_user_code_cmd(char *deviceId, uint8_t userId)
{
    char *name = NULL;
    pthread_mutex_lock(&userCodePendingqueueMutex);
    user_code_pending_t *tmp = NULL;
    struct VR_list_head *pos, *q;
    
    VR_(list_for_each_safe)(pos, q, &g_user_code_pending_list.list)
    {
        tmp = VR_(list_entry)(pos, user_code_pending_t, list);
        if(!strcmp(tmp->info.deviceId, deviceId) &&
           (tmp->info.userId == userId)
           )
        {
            VR_(list_del)(pos);
            free(tmp->info.deviceId);
            name = tmp->info.name;
            free(tmp);
            break;
        }
    }

    pthread_mutex_unlock(&userCodePendingqueueMutex);
    return name;
}

static int _add_new_user_code_cmd(void *node_add)
{
    int res = 0;
    pthread_mutex_lock(&userCodePendingqueueMutex);
    struct VR_list_head *pos, *q;
    user_code_pending_t *input = (user_code_pending_t *)node_add;
    user_code_pending_t *tmp = NULL;

    VR_(list_for_each_safe)(pos, q, &g_user_code_pending_list.list)
    {
        tmp = VR_(list_entry)(pos, user_code_pending_t, list);
        if((tmp->cmd == input->cmd) &&
           !strcmp(tmp->info.deviceId, input->info.deviceId) &&
           (tmp->info.userId == input->info.userId)
           )
        {
            res = -1;
            break;
        }
    }

    if(!res)
    {
        VR_(list_add_tail)(&(input->list), &(g_user_code_pending_list.list));
    }

    pthread_mutex_unlock(&userCodePendingqueueMutex);
    return res;
}

int add_user_code_cmd(int cmd, char *deviceId, uint8_t userId, char *name)
{
    if(!deviceId || !name)
    {
        return -1;
    }

    user_code_pending_t *user = (user_code_pending_t *)malloc(sizeof(user_code_pending_t));
    memset(user, 0x00, sizeof(user_code_pending_t));

    user->cmd = cmd;
    user->timestampt = (unsigned)time(NULL);
    user->info.deviceId = strdup(deviceId);
    user->info.userId = userId;

    if(cmd == ADD_USER_CODE_CMD)
    {
        user->info.name = strdup(name);
    }

    int res = _add_new_user_code_cmd((void *)user);
    if(res)
    {
        free(user->info.deviceId);
        SAFE_FREE(user->info.name);
        free(user);
    }
    
    return res;
}

void manage_user_code_pending_thread(void *data)
{
    while(g_manage_user_code_thread)
    {
        if(VR_list_empty(&g_user_code_pending_list.list))
        {
            sleep(1);
            continue;
        }

        user_code_pending_t *command = VR_list_entry(g_user_code_pending_list.list.next, user_code_pending_t, list);
        if(command)
        {
            unsigned int lastTime = command->timestampt;
            unsigned int current = (unsigned)time(NULL);
            if((current - lastTime) > USER_CODE_CMD_TIMEOUT)
            {
                printf("set user code %s, %02X, %s timeout\n", command->info.deviceId,
                                command->info.userId, command->info.name);
                char *name = remove_user_code_cmd(command->info.deviceId, command->info.userId);
                SAFE_FREE(name);
            }
        }

        sleep(1);
    }
}

void stop_user_code_manage_pending(void)
{
    g_manage_user_code_thread = 0;
}