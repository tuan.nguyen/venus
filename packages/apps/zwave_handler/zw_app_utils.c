#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <getopt.h>
#include <unistd.h>
#include <json-c/json.h>
#include "zw_app_utils.h"
#include "slog.h"

#define SET_VALUE(_A,_B) \
    do { \
        if (_A) { \
            *_A=_B; \
        } \
    } while (0)

#define RATE_TYPE_BIT_MASK      0x03 //BIT1 + BIT2
#define SCALE_BIT_MASK          0x07 //BIT1 + BIT2 + BIT3
#define RATE_TYPE_POSITION      0x06 //<< 6 OR >> 6
#define SCALE_POSITION          0x03 //<< 3 OR >> 3

void parsing(uint8_t cmdClass, uint8_t cmd, const char *string, uint8_t *pData, uint8_t *pDataLength)
{
    uint8_t i;
    *pDataLength = 0;
    json_object *jobj = json_tokener_parse(string);
    //free memory after parsing complete
    switch (cmdClass)
    {
    case COMMAND_CLASS_ALARM:
        switch (cmd)
        {
        case ALARM_GET:
        {
            i = 0;

            json_object *jv1_alarm_type = json_object_object_get(jobj, "v1_alarm_type");
            if (jv1_alarm_type != NULL)
            {
                int v1_alarm_type = json_object_get_int(jv1_alarm_type);
                pData[i++] = (uint8_t)v1_alarm_type;
                int notification_type = json_object_get_int(json_object_object_get(jobj, "notification_type"));
                pData[i++] = (uint8_t)notification_type;
                int event = json_object_get_int(json_object_object_get(jobj, "event"));
                pData[i++] = (uint8_t)event;
                *pDataLength = i;
                break;
            }

            int alarm_type = json_object_get_int(json_object_object_get(jobj, "alarm_type"));
            pData[i++] = (uint8_t)alarm_type;

            json_object *jzwave_alarm_type = json_object_object_get(jobj, "zwave_alarm_type");
            if (jzwave_alarm_type != NULL)
            {
                int zwave_alarm_type = json_object_get_int(jzwave_alarm_type);
                pData[i++] = (uint8_t)zwave_alarm_type;
            }
            *pDataLength = i;
        }
        break;

        case ALARM_SET_V2:
        {
            i = 0;

            json_object *jnotification_type = json_object_object_get(jobj, "notification_type");
            if (jnotification_type != NULL)
            {
                int notification_type = json_object_get_int(jnotification_type);
                pData[i++] = (uint8_t)notification_type;
                int notification_status = json_object_get_int(json_object_object_get(jobj, "notification_status"));
                pData[i++] = (uint8_t)notification_status;

                *pDataLength = i;
                break;
            }

            int zwave_alarm_type = json_object_get_int(json_object_object_get(jobj, "zwave_alarm_type"));
            pData[i++] = (uint8_t)zwave_alarm_type;
            int zwave_alarm_status = json_object_get_int(json_object_object_get(jobj, "zwave_alarm_status"));
            pData[i++] = (uint8_t)zwave_alarm_status;
            *pDataLength = i;
        }
        break;

        case ALARM_TYPE_SUPPORTED_GET_V2:
        {
            i = 0;

            *pDataLength = i;
        }
        break;

        case EVENT_SUPPORTED_GET_V3:
        {
            i = 0;

            int notification_type = json_object_get_int(json_object_object_get(jobj, "notification_type"));
            pData[i++] = (uint8_t)notification_type;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_SENSOR_ALARM:
        switch (cmd)
        {
        case SENSOR_ALARM_GET:
        {
            i = 0;
            int sensor_type = json_object_get_int(json_object_object_get(jobj, "sensor_type"));
            pData[i++] = (uint8_t)sensor_type;
            *pDataLength = i;
        }
        break;

        case SENSOR_ALARM_SUPPORTED_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;
    case COMMAND_CLASS_SWITCH_ALL:
        switch (cmd)
        {
        case SWITCH_ALL_ON:
        case SWITCH_ALL_OFF:
        case SWITCH_ALL_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;

        case SWITCH_ALL_SET:
        {
            i = 0;
            int mode = json_object_get_int(json_object_object_get(jobj, "mode"));
            pData[i++] = (uint8_t)mode;
            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_APPLICATION_STATUS:
        switch (cmd)
        {
        case APPLICATION_BUSY:
        {
            i = 0;
            int status = json_object_get_int(json_object_object_get(jobj, "status"));
            pData[i++] = (uint8_t)status;
            int wait_time = json_object_get_int(json_object_object_get(jobj, "wait_time"));
            pData[i++] = (uint8_t)wait_time;
            *pDataLength = i;
        }
        break;

        case APPLICATION_REJECTED_REQUEST:
        {
            i = 0;
            int status = json_object_get_int(json_object_object_get(jobj, "status"));
            pData[i++] = (uint8_t)status;
            *pDataLength = i;
        }
        break;
        }
        break;
    case COMMAND_CLASS_ASSOCIATION:
        switch (cmd)
        {
        case ASSOCIATION_SET:
        case ASSOCIATION_REMOVE:
        {
            i = 0;
            int group_id = json_object_get_int(json_object_object_get(jobj, "group_id"));
            pData[i++] = (uint8_t)group_id;
            jobj = json_object_object_get(jobj, "node_list");
            int arraylen = json_object_array_length(jobj);
            int j;
            json_object *jvalue;
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(jobj, j);
                pData[i++] = (uint8_t)json_object_get_int(jvalue);
            }

            *pDataLength = i;
        }
        break;

        case ASSOCIATION_GET:
        {
            i = 0;
            int group_id = json_object_get_int(json_object_object_get(jobj, "group_id"));
            pData[i++] = (uint8_t)group_id;
            *pDataLength = i;
        }
        break;

        case ASSOCIATION_GROUPINGS_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_ASSOCIATION_GRP_INFO:
        switch (cmd)
        {
        case ASSOCIATION_GROUP_NAME_GET:
        {
            i = 0;
            int group_id = json_object_get_int(json_object_object_get(jobj, "group_id"));
            pData[i++] = (uint8_t)group_id;
            *pDataLength = i;
        }
        break;

        case ASSOCIATION_GROUP_INFO_GET:
        {
            i = 0;
            int refresh_cache = json_object_get_int(json_object_object_get(jobj, "refresh_cache"));
            int list_mode = json_object_get_int(json_object_object_get(jobj, "list_mode"));
            pData[i++] = ((refresh_cache & 0x01) << 7) + ((list_mode & 0x01) << 6);
            int group_id = json_object_get_int(json_object_object_get(jobj, "group_id"));
            pData[i++] = (uint8_t)group_id;
            *pDataLength = i;
        }
        break;

        case ASSOCIATION_GROUP_COMMAND_LIST_GET:
        {
            i = 0;
            int allow_cache = json_object_get_int(json_object_object_get(jobj, "allow_cache"));
            pData[i++] = ((allow_cache & 0x01) << 7);
            int group_id = json_object_get_int(json_object_object_get(jobj, "group_id"));
            pData[i++] = (uint8_t)group_id;
            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_BARRIER_OPERATOR:
        switch (cmd)
        {
        case BARRIER_OPERATOR_SET:
        {
            i = 0;
            int target_value = json_object_get_int(json_object_object_get(jobj, "target_value"));
            pData[i++] = (uint8_t)target_value;

            *pDataLength = i;
        }
        break;

        case BARRIER_OPERATOR_GET:
        case BARRIER_OPERATOR_SIGNAL_SUPPORTED_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;

        case BARRIER_OPERATOR_SIGNAL_SET:
        {
            i = 0;
            int subsystem_type = json_object_get_int(json_object_object_get(jobj, "subsystem_type"));
            pData[i++] = (uint8_t)subsystem_type;
            int subsystem_state = json_object_get_int(json_object_object_get(jobj, "subsystem_state"));
            pData[i++] = (uint8_t)subsystem_state;

            *pDataLength = i;
        }
        break;

        case BARRIER_OPERATOR_SIGNAL_GET:
        {
            i = 0;
            int subsystem_type = json_object_get_int(json_object_object_get(jobj, "subsystem_type"));
            pData[i++] = (uint8_t)subsystem_type;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_BASIC:
        switch (cmd)
        {
        case BASIC_SET:
        {
            i = 0;
            int value = json_object_get_int(json_object_object_get(jobj, "value"));
            pData[i++] = (uint8_t)value;
            *pDataLength = i;
        }
        break;

        case BASIC_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_BATTERY:
        switch (cmd)
        {
        case BATTERY_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_SENSOR_BINARY:
        switch (cmd)
        {
        case SENSOR_BINARY_GET:
        {
            i = 0;
            json_object *jsensor_type = json_object_object_get(jobj, "sensor_type");
            if (jsensor_type != NULL)
            {
                int sensor_type = json_object_get_int(jsensor_type);
                pData[i++] = (uint8_t)sensor_type;
            }
            *pDataLength = i;
        }
        break;

        case SENSOR_BINARY_SUPPORTED_GET_SENSOR_V2:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_SWITCH_BINARY:
        switch (cmd)
        {
        case SWITCH_BINARY_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;

        case SWITCH_BINARY_SET:
        {
            i = 0;

            int value = json_object_get_int(json_object_object_get(jobj, "value"));
            pData[i++] = (uint8_t)value;

            json_object *jduration = json_object_object_get(jobj, "duration");
            if (jduration != NULL)
            {
                int duration = json_object_get_int(jduration);
                pData[i++] = (uint8_t)duration;
            }

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_SWITCH_COLOR:
        switch (cmd)
        {
        case SWITCH_COLOR_SUPPORTED_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;

        case SWITCH_COLOR_GET:
        {
            i = 0;

            int color_component_id = json_object_get_int(json_object_object_get(jobj, "color_component_id"));
            pData[i++] = (uint8_t)color_component_id;

            *pDataLength = i;
        }
        break;

        case SWITCH_COLOR_SET:
        {
            i = 0;
            int color_component_count = json_object_get_int(json_object_object_get(jobj, "color_component_count"));
            pData[i++] = (uint8_t)(color_component_count & 0x1F);

            json_object *jcomponent_list = json_object_object_get(jobj, "component_list");
            int arraylen = json_object_array_length(jcomponent_list);
            int j;
            json_object *jvalue;
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(jcomponent_list, j);
                int color_component_id = json_object_get_int(json_object_object_get(jvalue, "color_component_id"));
                pData[i++] = (uint8_t)color_component_id;
                int value = json_object_get_int(json_object_object_get(jvalue, "value"));
                pData[i++] = (uint8_t)value;
            }

            json_object *jduration = json_object_object_get(jobj, "duration");
            if (jduration != NULL)
            {
                int duration = json_object_get_int(jduration);
                pData[i++] = (uint8_t)duration;
            }

            *pDataLength = i;
        }
        break;

        case SWITCH_COLOR_START_LEVEL_CHANGE:
        {
            i = 0;
            int up_down = json_object_get_int(json_object_object_get(jobj, "up_down"));
            int ignore_start_level = json_object_get_int(json_object_object_get(jobj, "ignore_start_level"));
            pData[i++] = ((up_down & 0x01) << 6) + ((ignore_start_level & 0x01) << 5);
            int color_component_id = json_object_get_int(json_object_object_get(jobj, "color_component_id"));
            pData[i++] = (uint8_t)color_component_id;
            int start_level = json_object_get_int(json_object_object_get(jobj, "start_level"));
            pData[i++] = (uint8_t)start_level;

            json_object *jduration = json_object_object_get(jobj, "duration");
            if (jduration != NULL)
            {
                int duration = json_object_get_int(jduration);
                pData[i++] = (uint8_t)duration;
            }

            *pDataLength = i;
        }
        break;

        case SWITCH_COLOR_STOP_LEVEL_CHANGE:
        {
            i = 0;
            int color_component_id = json_object_get_int(json_object_object_get(jobj, "color_component_id"));
            pData[i++] = (uint8_t)color_component_id;
            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_CONFIGURATION:
        switch (cmd)
        {
        case CONFIGURATION_SET:
        {
            i = 0;
            int parameter_number = json_object_get_int(json_object_object_get(jobj, "parameter_number"));
            pData[i++] = (uint8_t)parameter_number;
            int default_value = json_object_get_int(json_object_object_get(jobj, "default"));
            int size = json_object_get_int(json_object_object_get(jobj, "size"));
            pData[i++] = ((default_value & 0x01) << 7) + ((size & 0x07) << 0);
            int configuration_value = json_object_get_int(json_object_object_get(jobj, "configuration_value"));
            int j;
            for (j = size - 1; j >= 0; j--)
            {
                pData[i++] = (uint8_t)(configuration_value >> (8 * j));
            }

            *pDataLength = i;
        }
        break;

        case CONFIGURATION_GET:
        {
            i = 0;
            int parameter_number = json_object_get_int(json_object_object_get(jobj, "parameter_number"));
            pData[i++] = (uint8_t)parameter_number;
            *pDataLength = i;
        }
        break;

        case CONFIGURATION_BULK_SET_V2:
        {
            i = 0;
            int parameter_offset = json_object_get_int(json_object_object_get(jobj, "parameter_offset"));
            pData[i++] = (uint8_t)(parameter_offset >> 8);
            pData[i++] = (uint8_t)(parameter_offset >> 0);
            int no_params = json_object_get_int(json_object_object_get(jobj, "no_params"));
            pData[i++] = (uint8_t)no_params;
            int default_value = json_object_get_int(json_object_object_get(jobj, "default"));
            int handshake = json_object_get_int(json_object_object_get(jobj, "handshake"));
            int size = json_object_get_int(json_object_object_get(jobj, "size"));
            pData[i++] = ((default_value & 0x01) << 7) + ((handshake & 0x01) << 6) + (size & 0x07);

            jobj = json_object_object_get(jobj, "params_list");
            int arraylen = json_object_array_length(jobj);
            int j;
            json_object *jvalue;
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(jobj, j);
                int configuration_value = json_object_get_int(jvalue);
                int index;
                for (index = size - 1; index >= 0; index--)
                {
                    pData[i++] = (uint8_t)(configuration_value >> (8 * index));
                }
            }

            *pDataLength = i;
        }
        break;

        case CONFIGURATION_BULK_GET_V2:
        {
            i = 0;
            int parameter_offset = json_object_get_int(json_object_object_get(jobj, "parameter_offset"));
            pData[i++] = (uint8_t)(parameter_offset >> 8);
            pData[i++] = (uint8_t)(parameter_offset >> 0);
            int no_params = json_object_get_int(json_object_object_get(jobj, "no_params"));
            pData[i++] = (uint8_t)no_params;

            *pDataLength = i;
        }
        break;

        case CONFIGURATION_NAME_GET_V4:
        case CONFIGURATION_INFO_GET_V4:
        case CONFIGURATION_PROPERTIES_GET_V4:
        {
            i = 0;
            int parameter_number = json_object_get_int(json_object_object_get(jobj, "parameter_number"));
            pData[i++] = (uint8_t)(parameter_number >> 8);
            pData[i++] = (uint8_t)(parameter_number >> 0);

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_DOOR_LOCK:
        switch (cmd)
        {
        case DOOR_LOCK_OPERATION_SET:
        {
            i = 0;
            int door_lock_mode = json_object_get_int(json_object_object_get(jobj, "door_lock_mode"));
            pData[i++] = (uint8_t)door_lock_mode;
            *pDataLength = i;
        }
        break;

        case DOOR_LOCK_OPERATION_GET:
        case DOOR_LOCK_CONFIGURATION_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;

        case DOOR_LOCK_CONFIGURATION_SET:
        {
            i = 0;
            int operation_type = json_object_get_int(json_object_object_get(jobj, "operation_type"));
            pData[i++] = (uint8_t)operation_type;
            int outside_door_handles_mode = json_object_get_int(json_object_object_get(jobj, "outside_door_handles_mode"));
            int inside_door_handles_mode = json_object_get_int(json_object_object_get(jobj, "inside_door_handles_mode"));
            pData[i++] = ((outside_door_handles_mode & 0x0F) << 4) + ((inside_door_handles_mode & 0x0F) << 0);
            int lock_timeout_minutes = json_object_get_int(json_object_object_get(jobj, "lock_timeout_minutes"));
            pData[i++] = (uint8_t)lock_timeout_minutes;
            int lock_timeout_seconds = json_object_get_int(json_object_object_get(jobj, "lock_timeout_seconds"));
            pData[i++] = (uint8_t)lock_timeout_seconds;
            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_IRRIGATION:
        switch (cmd)
        {
        case IRRIGATION_SYSTEM_CONFIG_SET:
        {
            i = 0;
            int master_valve_delay = json_object_get_int(json_object_object_get(jobj, "master_valve_delay"));
            pData[i++] = (uint8_t)(master_valve_delay);
            int precision = json_object_get_int(json_object_object_get(jobj, "high_pressure_threshold_precision"));
            int scale = json_object_get_int(json_object_object_get(jobj, "high_pressure_threshold_scale"));
            int size = json_object_get_int(json_object_object_get(jobj, "high_pressure_threshold_size"));
            pData[i++] = ((precision & 0x07) << 5) + ((scale & 0x03) << 3) + ((size & 0x07) << 0);
            int value = json_object_get_int(json_object_object_get(jobj, "high_pressure_threshold_value"));
            int j;
            for (j = size - 1; j >= 0; j--)
            {
                pData[i++] = (uint8_t)(value >> (8 * j));
            }

            precision = json_object_get_int(json_object_object_get(jobj, "low_pressure_threshold_precision"));
            scale = json_object_get_int(json_object_object_get(jobj, "low_pressure_threshold_scale"));
            size = json_object_get_int(json_object_object_get(jobj, "low_pressure_threshold_size"));
            pData[i++] = ((precision & 0x07) << 5) + ((scale & 0x03) << 3) + ((size & 0x07) << 0);
            value = json_object_get_int(json_object_object_get(jobj, "low_pressure_threshold_value"));
            for (j = size - 1; j >= 0; j--)
            {
                pData[i++] = (uint8_t)(value >> (8 * j));
            }
            int sensor_polarity = json_object_get_int(json_object_object_get(jobj, "sensor_polarity"));
            pData[i++] = (uint8_t)(sensor_polarity);
            *pDataLength = i;
        }
        break;

        case IRRIGATION_SYSTEM_CONFIG_GET:
        case IRRIGATION_SYSTEM_INFO_GET:
        case IRRIGATION_SYSTEM_STATUS_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;

        case IRRIGATION_VALVE_INFO_GET:
        case IRRIGATION_VALVE_CONFIG_GET:
        {
            i = 0;

            int master_valve = json_object_get_int(json_object_object_get(jobj, "master_valve"));
            pData[i++] = (uint8_t)(master_valve & 0x01);
            int valve_id = json_object_get_int(json_object_object_get(jobj, "valve_id"));
            pData[i++] = (uint8_t)(valve_id);

            *pDataLength = i;
        }
        break;

        case IRRIGATION_VALVE_CONFIG_SET:
        {
            i = 0;
            int master_valve = json_object_get_int(json_object_object_get(jobj, "master_valve"));
            pData[i++] = (uint8_t)(master_valve & 0x01);
            int valve_id = json_object_get_int(json_object_object_get(jobj, "valve_id"));
            pData[i++] = (uint8_t)(valve_id);
            int nominal_current_high_threshold = json_object_get_int(json_object_object_get(jobj, "nominal_current_high_threshold"));
            pData[i++] = (uint8_t)(nominal_current_high_threshold);
            int nominal_current_low_threshold = json_object_get_int(json_object_object_get(jobj, "nominal_current_low_threshold"));
            pData[i++] = (uint8_t)(nominal_current_low_threshold);

            int precision = json_object_get_int(json_object_object_get(jobj, "maximum_flow_precision"));
            int scale = json_object_get_int(json_object_object_get(jobj, "maximum_flow_scale"));
            int size = json_object_get_int(json_object_object_get(jobj, "maximum_flow_size"));
            pData[i++] = ((precision & 0x07) << 5) + ((scale & 0x03) << 3) + ((size & 0x07) << 0);
            int value = json_object_get_int(json_object_object_get(jobj, "maximum_flow_value"));
            int j;
            for (j = size - 1; j >= 0; j--)
            {
                pData[i++] = (uint8_t)(value >> (8 * j));
            }

            precision = json_object_get_int(json_object_object_get(jobj, "flow_high_threshold_precision"));
            scale = json_object_get_int(json_object_object_get(jobj, "flow_high_threshold_scale"));
            size = json_object_get_int(json_object_object_get(jobj, "flow_high_threshold_size"));
            pData[i++] = ((precision & 0x07) << 5) + ((scale & 0x03) << 3) + ((size & 0x07) << 0);
            value = json_object_get_int(json_object_object_get(jobj, "flow_high_threshold_value"));
            for (j = size - 1; j >= 0; j--)
            {
                pData[i++] = (uint8_t)(value >> (8 * j));
            }
            precision = json_object_get_int(json_object_object_get(jobj, "flow_low_threshold_precision"));
            scale = json_object_get_int(json_object_object_get(jobj, "flow_low_threshold_scale"));
            size = json_object_get_int(json_object_object_get(jobj, "flow_low_threshold_size"));
            pData[i++] = ((precision & 0x07) << 5) + ((scale & 0x03) << 3) + ((size & 0x07) << 0);
            value = json_object_get_int(json_object_object_get(jobj, "flow_low_threshold_value"));
            for (j = size - 1; j >= 0; j--)
            {
                pData[i++] = (uint8_t)(value >> (8 * j));
            }

            int sensor_usage = json_object_get_int(json_object_object_get(jobj, "sensor_usage"));
            pData[i++] = (uint8_t)(sensor_usage);
            *pDataLength = i;
        }
        break;

        case IRRIGATION_VALVE_RUN:
        {
            i = 0;

            int master_valve = json_object_get_int(json_object_object_get(jobj, "master_valve"));
            pData[i++] = (uint8_t)(master_valve & 0x01);
            int valve_id = json_object_get_int(json_object_object_get(jobj, "valve_id"));
            pData[i++] = (uint8_t)(valve_id);
            int duration = json_object_get_int(json_object_object_get(jobj, "duration"));
            pData[i++] = (uint8_t)(duration >> 8);
            pData[i++] = (uint8_t)(duration >> 0);

            *pDataLength = i;
        }
        break;

        case IRRIGATION_VALVE_TABLE_SET:
        {
            i = 0;

            int valve_table_id = json_object_get_int(json_object_object_get(jobj, "valve_table_id"));
            pData[i++] = (uint8_t)(valve_table_id);
            jobj = json_object_object_get(jobj, "valve_list");
            int arraylen = json_object_array_length(jobj);
            int j;
            json_object *jvalue;
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(jobj, j);
                int valve_id = json_object_get_int(json_object_object_get(jvalue, "valve_id"));
                pData[i++] = (uint8_t)(valve_id);
                int duration = json_object_get_int(json_object_object_get(jvalue, "duration"));
                pData[i++] = (uint8_t)(duration >> 8);
                pData[i++] = (uint8_t)(duration >> 0);
            }

            *pDataLength = i;
        }
        break;

        case IRRIGATION_VALVE_TABLE_GET:
        {
            i = 0;

            int valve_table_id = json_object_get_int(json_object_object_get(jobj, "valve_table_id"));
            pData[i++] = (uint8_t)(valve_table_id);

            *pDataLength = i;
        }
        break;

        case IRRIGATION_VALVE_TABLE_RUN:
        {
            i = 0;

            jobj = json_object_object_get(jobj, "valve_table_list");
            int arraylen = json_object_array_length(jobj);
            int j;
            json_object *jvalue;
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(jobj, j);
                pData[i++] = (uint8_t)json_object_get_int(jvalue);
            }

            *pDataLength = i;
        }
        break;

        case IRRIGATION_SYSTEM_SHUTOFF:
        {
            i = 0;

            int duration = json_object_get_int(json_object_object_get(jobj, "duration"));
            pData[i++] = (uint8_t)(duration);

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_MANUFACTURER_SPECIFIC:
        switch (cmd)
        {
        case DEVICE_SPECIFIC_GET_V2:
        {
            i = 0;

            int device_id_type = json_object_get_int(json_object_object_get(jobj, "device_id_type"));
            pData[i++] = (uint8_t)(device_id_type & 0x07);

            *pDataLength = i;
        }
        break;

        case MANUFACTURER_SPECIFIC_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_METER:
        switch (cmd)
        {
        case METER_GET:
        {
            i = 0;
            json_object *jscale = json_object_object_get(jobj, "scale");
            if (jscale != NULL)
            {
                int scale = json_object_get_int(jscale);
                /*Commoand Class Meter --> Command = Meter_Get with para
                Rate_Type (Bit7-->Bit6), Scale (Bit5-->Bit3)*/
                pData[i++] = (uint8_t)((scale & SCALE_BIT_MASK) << SCALE_POSITION);
            }
            json_object *jrate_type = json_object_object_get(jobj, "rate_type");
            if (jrate_type != NULL)
            {
                /*Bit        | 7   |  6  |  5  |  4  |  3  |  2  |  1  |  0  | 
                 *Parameter  | Rate Type |   Scale         | Reserved        |  (byte 2)
                 *Parameter  |               Scale2                          |  (byte 3)
                */
                int rate_type = json_object_get_int(jrate_type);
                int scale = json_object_get_int(jscale);
                pData[0] =  (uint8_t)((scale & SCALE_BIT_MASK) << SCALE_POSITION) + (uint8_t)((rate_type & RATE_TYPE_BIT_MASK) << RATE_TYPE_POSITION);
            }
            json_object *jscale2 = json_object_object_get(jobj, "scale2");
            if (jscale2 != NULL)
            {
                int scale2 = json_object_get_int(jscale2);
                pData[i++] = (uint8_t)scale2;
            }
            *pDataLength = i;
        }
        break;

        case METER_SUPPORTED_GET_V2:
        case METER_RESET_V2:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_MULTI_CHANNEL_V3:
        switch (cmd)
        {
        case MULTI_CHANNEL_END_POINT_FIND_V3:
        {
            i = 0;
            int generic_device_class = json_object_get_int(json_object_object_get(jobj, "generic_device_class"));
            pData[i++] = (uint8_t)generic_device_class;
            int specific_device_class = json_object_get_int(json_object_object_get(jobj, "specific_device_class"));
            pData[i++] = (uint8_t)specific_device_class;
            json_object *jreserved = json_object_object_get(jobj, "reserved");
            if (jreserved != NULL)
            {
                int reserved = json_object_get_int(jreserved);
                pData[i++] = (uint8_t)reserved;
            }
            *pDataLength = i;
        }
        break;

        case MULTI_CHANNEL_END_POINT_GET_V3:
        {
            i = 0;

            *pDataLength = i;
        }
        break;

        case MULTI_CHANNEL_AGGREGATED_MEMBERS_GET_V4:
        {
            i = 0;
            int aggregated_end_point = json_object_get_int(json_object_object_get(jobj, "aggregated_end_point"));
            pData[i++] = (uint8_t)aggregated_end_point;
            *pDataLength = i;
        }
        break;
        case MULTI_CHANNEL_CAPABILITY_GET_V3:
        {
            i = 0;
            int end_point = json_object_get_int(json_object_object_get(jobj, "end_point"));
            pData[i++] = (uint8_t)end_point;
            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION_V2:
        switch (cmd)
        {
        case MULTI_CHANNEL_ASSOCIATION_SET_V2:
        case MULTI_CHANNEL_ASSOCIATION_REMOVE_V2:
        {
            i = 0;
            int group_id = json_object_get_int(json_object_object_get(jobj, "group_id"));
            pData[i++] = (uint8_t)group_id;

            json_object *jnode_list = json_object_object_get(jobj, "node_list");
            int arraylen = json_object_array_length(jnode_list);
            int j;
            json_object *jvalue;
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(jnode_list, j);
                pData[i++] = (uint8_t)json_object_get_int(jvalue);
            }
            pData[i++] = MULTI_CHANNEL_ASSOCIATION_SET_MARKER_V2;
            json_object *jmulti_channel_node_list = json_object_object_get(jobj, "multi_channel_node_list");
            arraylen = json_object_array_length(jmulti_channel_node_list);

            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(jmulti_channel_node_list, j);
                int multi_channel_node_id = json_object_get_int(json_object_object_get(jvalue, "multi_channel_node_id"));
                pData[i++] = (uint8_t)multi_channel_node_id;
                int bit_address = json_object_get_int(json_object_object_get(jvalue, "bit_address"));
                int end_point = json_object_get_int(json_object_object_get(jvalue, "end_point"));
                pData[i++] = ((bit_address & 0x01) << 7) + ((end_point & 0x7F) << 0);
            }

            *pDataLength = i;
        }
        break;

        case MULTI_CHANNEL_ASSOCIATION_GET_V2:
        {
            i = 0;
            int group_id = json_object_get_int(json_object_object_get(jobj, "group_id"));
            pData[i++] = (uint8_t)group_id;
            *pDataLength = i;
        }
        break;

        case MULTI_CHANNEL_ASSOCIATION_GROUPINGS_GET_V2:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_SENSOR_MULTILEVEL:
        switch (cmd)
        {
        case SENSOR_MULTILEVEL_GET:
        {
            i = 0;

            json_object *jsensor_type = json_object_object_get(jobj, "sensor_type");
            if (jsensor_type != NULL)
            {
                int sensor_type = json_object_get_int(jsensor_type);
                pData[i++] = (uint8_t)sensor_type;
                int scale = json_object_get_int(json_object_object_get(jobj, "scale"));
                pData[i++] = (uint8_t)((scale & 0x03) << 3);
            }

            *pDataLength = i;
        }
        break;

        case SENSOR_MULTILEVEL_SUPPORTED_GET_SENSOR_V5:
        {
            i = 0;

            *pDataLength = i;
        }
        break;

        case SENSOR_MULTILEVEL_SUPPORTED_GET_SCALE_V5:
        {
            i = 0;
            int sensor_type = json_object_get_int(json_object_object_get(jobj, "sensor_type"));
            pData[i++] = (uint8_t)sensor_type;
            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_SWITCH_MULTILEVEL:
        switch (cmd)
        {
        case SWITCH_MULTILEVEL_SET:
        {
            i = 0;
            int value = json_object_get_int(json_object_object_get(jobj, "value"));
            pData[i++] = (uint8_t)value;
            json_object *jduration = json_object_object_get(jobj, "duration");
            if (jduration != NULL)
            {
                int duration = json_object_get_int(jduration);
                pData[i++] = (uint8_t)duration;
            }
            *pDataLength = i;
        }
        break;

        case SWITCH_MULTILEVEL_STOP_LEVEL_CHANGE:
        case SWITCH_MULTILEVEL_SUPPORTED_GET_V3:
        case SWITCH_MULTILEVEL_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;

        case SWITCH_MULTILEVEL_START_LEVEL_CHANGE:
        {
            i = 0;
            json_object *jprimary_up_down = json_object_object_get(jobj, "primary_up_down");
            if (jprimary_up_down != NULL)
            {
                int primary_up_down = json_object_get_int(jprimary_up_down);
                int ignore_start_level = json_object_get_int(json_object_object_get(jobj, "ignore_start_level"));
                int secondary_switch_inc_dec = json_object_get_int(json_object_object_get(jobj, "secondary_switch_inc_dec"));
                pData[i++] = ((primary_up_down & 0x03) << 6) + ((ignore_start_level & 0x01) << 5) + ((secondary_switch_inc_dec & 0x03) << 3);
                int primary_start_level = json_object_get_int(json_object_object_get(jobj, "primary_start_level"));
                pData[i++] = (uint8_t)primary_start_level;
                int duration = json_object_get_int(json_object_object_get(jobj, "duration"));
                pData[i++] = (uint8_t)duration;
                int secondary_switch_step_size = json_object_get_int(json_object_object_get(jobj, "secondary_switch_step_size"));
                pData[i++] = (uint8_t)secondary_switch_step_size;
                *pDataLength = i;
                break;
            }
            int up_down = json_object_get_int(json_object_object_get(jobj, "up_down"));
            int ignore_start_level = json_object_get_int(json_object_object_get(jobj, "ignore_start_level"));
            pData[i++] = ((up_down & 0x01) << 6) + ((ignore_start_level & 0x01) << 5);
            int start_level = json_object_get_int(json_object_object_get(jobj, "start_level"));
            pData[i++] = (uint8_t)start_level;

            json_object *jduration = json_object_object_get(jobj, "duration");
            if (jduration != NULL)
            {
                int duration = json_object_get_int(jduration);
                pData[i++] = (uint8_t)duration;
            }

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_POWERLEVEL:
        switch (cmd)
        {
        case POWERLEVEL_SET:
        {
            i = 0;
            int power_level = json_object_get_int(json_object_object_get(jobj, "power_level"));
            pData[i++] = (uint8_t)power_level;
            int timeout = json_object_get_int(json_object_object_get(jobj, "timeout"));
            pData[i++] = (uint8_t)timeout;
            *pDataLength = i;
        }
        break;

        case POWERLEVEL_TEST_NODE_GET:
        case POWERLEVEL_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;

        case POWERLEVEL_TEST_NODE_SET:
        {
            i = 0;
            int test_node_id = json_object_get_int(json_object_object_get(jobj, "test_node_id"));
            pData[i++] = (uint8_t)test_node_id;
            int power_level = json_object_get_int(json_object_object_get(jobj, "power_level"));
            pData[i++] = (uint8_t)power_level;
            int test_frame_count = json_object_get_int(json_object_object_get(jobj, "test_frame_count"));
            pData[i++] = (uint8_t)(test_frame_count >> 8);
            pData[i++] = (uint8_t)(test_frame_count >> 0);
            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_PROTECTION:
        switch (cmd)
        {
        case PROTECTION_SET:
        {
            i = 0;

            json_object *jlocal_protection_state = json_object_object_get(jobj, "local_protection_state");
            if (jlocal_protection_state != NULL)
            {
                int local_protection_state = json_object_get_int(jlocal_protection_state);
                pData[i++] = (uint8_t)local_protection_state;
                int rf_protection_state = json_object_get_int(json_object_object_get(jobj, "rf_protection_state"));
                pData[i++] = (uint8_t)rf_protection_state;
                *pDataLength = i;
                break;
            }
            int protection_state = json_object_get_int(json_object_object_get(jobj, "protection_state"));
            pData[i++] = (uint8_t)protection_state;
            *pDataLength = i;
        }
        break;

        case PROTECTION_GET:
        case PROTECTION_SUPPORTED_GET_V2:
        case PROTECTION_EC_GET_V2:
        case PROTECTION_TIMEOUT_GET_V2:
        {
            i = 0;

            *pDataLength = i;
        }
        break;

        case PROTECTION_EC_SET_V2:
        {
            i = 0;

            int node_id = json_object_get_int(json_object_object_get(jobj, "node_id"));
            pData[i++] = (uint8_t)node_id;

            *pDataLength = i;
        }
        break;

        case PROTECTION_TIMEOUT_SET_V2:
        {
            i = 0;

            int timeout = json_object_get_int(json_object_object_get(jobj, "timeout"));
            pData[i++] = (uint8_t)timeout;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_SCHEDULE_ENTRY_LOCK:
        switch (cmd)
        {
        case SCHEDULE_ENTRY_LOCK_TIME_OFFSET_GET_V2:
        case SCHEDULE_ENTRY_TYPE_SUPPORTED_GET_V2:
        {
            i = 0;

            *pDataLength = i;
        }
        break;

        case SCHEDULE_ENTRY_LOCK_ENABLE_SET:
        {
            i = 0;
            int user_id = json_object_get_int(json_object_object_get(jobj, "user_id"));
            pData[i++] = (uint8_t)user_id;
            int enabled = json_object_get_int(json_object_object_get(jobj, "enabled"));
            pData[i++] = (uint8_t)enabled;

            *pDataLength = i;
        }
        break;

        case SCHEDULE_ENTRY_LOCK_ENABLE_ALL_SET:
        {
            i = 0;
            int enabled = json_object_get_int(json_object_object_get(jobj, "enabled"));
            pData[i++] = (uint8_t)enabled;

            *pDataLength = i;
        }
        break;

        case SCHEDULE_ENTRY_LOCK_WEEK_DAY_SET:
        {
            i = 0;
            int set_action = json_object_get_int(json_object_object_get(jobj, "set_action"));
            pData[i++] = (uint8_t)set_action;
            int user_id = json_object_get_int(json_object_object_get(jobj, "user_id"));
            pData[i++] = (uint8_t)user_id;
            int schedule_slot_id = json_object_get_int(json_object_object_get(jobj, "schedule_slot_id"));
            pData[i++] = (uint8_t)schedule_slot_id;
            int day_of_week = json_object_get_int(json_object_object_get(jobj, "day_of_week"));
            pData[i++] = (uint8_t)day_of_week;
            int start_hour = json_object_get_int(json_object_object_get(jobj, "start_hour"));
            pData[i++] = (uint8_t)start_hour;
            int start_minute = json_object_get_int(json_object_object_get(jobj, "start_minute"));
            pData[i++] = (uint8_t)start_minute;
            int stop_hour = json_object_get_int(json_object_object_get(jobj, "stop_hour"));
            pData[i++] = (uint8_t)stop_hour;
            int stop_minute = json_object_get_int(json_object_object_get(jobj, "stop_minute"));
            pData[i++] = (uint8_t)stop_minute;

            *pDataLength = i;
        }
        break;

        case SCHEDULE_ENTRY_LOCK_WEEK_DAY_GET:
        case SCHEDULE_ENTRY_LOCK_YEAR_DAY_GET:
        case SCHEDULE_ENTRY_LOCK_DAILY_REPEATING_GET_V3:
        {
            i = 0;
            int user_id = json_object_get_int(json_object_object_get(jobj, "user_id"));
            pData[i++] = (uint8_t)user_id;
            int schedule_slot_id = json_object_get_int(json_object_object_get(jobj, "schedule_slot_id"));
            pData[i++] = (uint8_t)schedule_slot_id;

            *pDataLength = i;
        }
        break;

        case SCHEDULE_ENTRY_LOCK_YEAR_DAY_SET:
        {
            i = 0;
            int set_action = json_object_get_int(json_object_object_get(jobj, "set_action"));
            pData[i++] = (uint8_t)set_action;
            int user_id = json_object_get_int(json_object_object_get(jobj, "user_id"));
            pData[i++] = (uint8_t)user_id;
            int schedule_slot_id = json_object_get_int(json_object_object_get(jobj, "schedule_slot_id"));
            pData[i++] = (uint8_t)schedule_slot_id;
            int start_year = json_object_get_int(json_object_object_get(jobj, "start_year"));
            pData[i++] = (uint8_t)start_year;
            int start_month = json_object_get_int(json_object_object_get(jobj, "start_month"));
            pData[i++] = (uint8_t)start_month;
            int start_day = json_object_get_int(json_object_object_get(jobj, "start_day"));
            pData[i++] = (uint8_t)start_day;
            int start_hour = json_object_get_int(json_object_object_get(jobj, "start_hour"));
            pData[i++] = (uint8_t)start_hour;
            int start_minute = json_object_get_int(json_object_object_get(jobj, "start_minute"));
            pData[i++] = (uint8_t)start_minute;
            int stop_year = json_object_get_int(json_object_object_get(jobj, "stop_year"));
            pData[i++] = (uint8_t)stop_year;
            int stop_month = json_object_get_int(json_object_object_get(jobj, "stop_month"));
            pData[i++] = (uint8_t)stop_month;
            int stop_day = json_object_get_int(json_object_object_get(jobj, "stop_day"));
            pData[i++] = (uint8_t)stop_day;
            int stop_hour = json_object_get_int(json_object_object_get(jobj, "stop_hour"));
            pData[i++] = (uint8_t)stop_hour;
            int stop_minute = json_object_get_int(json_object_object_get(jobj, "stop_minute"));
            pData[i++] = (uint8_t)stop_minute;

            *pDataLength = i;
        }
        break;

        case SCHEDULE_ENTRY_LOCK_TIME_OFFSET_SET_V2:
        {
            i = 0;
            int sign_TZO = json_object_get_int(json_object_object_get(jobj, "sign_TZO"));
            int hour_TZO = json_object_get_int(json_object_object_get(jobj, "hour_TZO"));
            pData[i++] = ((sign_TZO & 0x01) << 7) + ((hour_TZO & 0x7F) << 0);
            int minute_TZO = json_object_get_int(json_object_object_get(jobj, "minute_TZO"));
            pData[i++] = (uint8_t)minute_TZO;
            int sign_offset_DST = json_object_get_int(json_object_object_get(jobj, "sign_offset_DST"));
            int minute_offset_DST = json_object_get_int(json_object_object_get(jobj, "minute_offset_DST"));
            pData[i++] = ((sign_offset_DST & 0x01) << 7) + ((minute_offset_DST & 0x7F) << 0);

            *pDataLength = i;
        }
        break;

        case SCHEDULE_ENTRY_LOCK_DAILY_REPEATING_SET_V3:
        {
            i = 0;
            int set_action = json_object_get_int(json_object_object_get(jobj, "set_action"));
            pData[i++] = (uint8_t)set_action;
            int user_id = json_object_get_int(json_object_object_get(jobj, "user_id"));
            pData[i++] = (uint8_t)user_id;
            int schedule_slot_id = json_object_get_int(json_object_object_get(jobj, "schedule_slot_id"));
            pData[i++] = (uint8_t)schedule_slot_id;
            int week_day_bitmask = json_object_get_int(json_object_object_get(jobj, "week_day_bitmask"));
            pData[i++] = (uint8_t)week_day_bitmask;
            int start_hour = json_object_get_int(json_object_object_get(jobj, "start_hour"));
            pData[i++] = (uint8_t)start_hour;
            int start_minute = json_object_get_int(json_object_object_get(jobj, "start_minute"));
            pData[i++] = (uint8_t)start_minute;
            int duration_hour = json_object_get_int(json_object_object_get(jobj, "duration_hour"));
            pData[i++] = (uint8_t)duration_hour;
            int duration_minute = json_object_get_int(json_object_object_get(jobj, "duration_minute"));
            pData[i++] = (uint8_t)duration_minute;
            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_SENSOR_CONFIGURATION:
        switch (cmd)
        {
        case SENSOR_TRIGGER_LEVEL_SET:
        {
            i = 0;
            int default_value = json_object_get_int(json_object_object_get(jobj, "default"));
            int current = json_object_get_int(json_object_object_get(jobj, "current"));
            pData[i++] = ((default_value & 0x01) << 7) + ((current & 0x01) << 6);
            int sensor_type = json_object_get_int(json_object_object_get(jobj, "sensor_type"));
            pData[i++] = (uint8_t)sensor_type;
            int precision = json_object_get_int(json_object_object_get(jobj, "precision"));
            int scale = json_object_get_int(json_object_object_get(jobj, "scale"));
            int size = json_object_get_int(json_object_object_get(jobj, "size"));
            pData[i++] = ((precision & 0x07) << 5) + ((scale & 0x03) << 3) + ((size & 0x07) << 0);
            int trigger_value = json_object_get_int(json_object_object_get(jobj, "trigger_value"));
            int j;
            for (j = size - 1; j >= 0; j--)
            {
                pData[i++] = (uint8_t)(trigger_value >> (8 * j));
            }
            *pDataLength = i;
        }
        break;

        case SENSOR_TRIGGER_LEVEL_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_SIMPLE_AV_CONTROL:
        switch (cmd)
        {
        case SIMPLE_AV_CONTROL_SET:
        {
            i = 0;
            int sequence_number = json_object_get_int(json_object_object_get(jobj, "sequence_number"));
            pData[i++] = (uint8_t)sequence_number;
            int key_attributes = json_object_get_int(json_object_object_get(jobj, "key_attributes"));
            pData[i++] = (uint8_t)(key_attributes & 0x0F);
            int item = json_object_get_int(json_object_object_get(jobj, "key_attributes"));
            pData[i++] = (uint8_t)(item >> 8);
            pData[i++] = (uint8_t)(item >> 0);

            jobj = json_object_object_get(jobj, "cmd_list");
            int arraylen = json_object_array_length(jobj);
            int j;
            json_object *jvalue;
            for (j = 0; j < arraylen; j++)
            {
                int value;
                jvalue = json_object_array_get_idx(jobj, j);
                value = json_object_get_int(jvalue);
                pData[i++] = (uint8_t)(value >> 8);
                pData[i++] = (uint8_t)(value >> 0);
            }

            *pDataLength = i;
        }
        break;

        case SIMPLE_AV_CONTROL_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;

        case SIMPLE_AV_CONTROL_SUPPORTED_GET:
        {
            i = 0;
            int report_no = json_object_get_int(json_object_object_get(jobj, "report_no"));
            pData[i++] = (uint8_t)report_no;
            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_THERMOSTAT_FAN_MODE:
        switch (cmd)
        {
        case THERMOSTAT_FAN_MODE_SET:
        {
            i = 0;
            int mode = json_object_get_int(json_object_object_get(jobj, "fan_mode"));
            pData[i++] = (uint8_t)mode;

            json_object *joff = json_object_object_get(jobj, "off");
            if (joff != NULL)
            {
                int off = json_object_get_int(joff);
                pData[i - 1] = ((off & 0x01) << 7) + (mode & 0x7F);
            }
            *pDataLength = i;
        }
        break;

        case THERMOSTAT_FAN_MODE_GET:
        case THERMOSTAT_FAN_MODE_SUPPORTED_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_THERMOSTAT_FAN_STATE:
        switch (cmd)
        {
        case THERMOSTAT_FAN_STATE_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_THERMOSTAT_MODE:
        switch (cmd)
        {
        case THERMOSTAT_MODE_SET:
        {
            i = 0;
            int mode = json_object_get_int(json_object_object_get(jobj, "mode"));
            pData[i++] = (uint8_t)(mode & 0x1F);
            json_object *jno_jmanufacturer_specific_data = json_object_object_get(jobj, "no_manufacturer_specific_data");
            if (jno_jmanufacturer_specific_data != NULL)
            {
                int no_manufacturer_specific_data = json_object_get_int(jno_jmanufacturer_specific_data);
                pData[i - 1] = ((mode & 0x1F) + ((no_manufacturer_specific_data & 0x07) << 5)); //i = 1; i - 1 = 0 -->pData[0] =
                jobj = json_object_object_get(jobj, "manufacturer_specific_data");
                int arraylen = json_object_array_length(jobj);
                int j;
                json_object *jvalue;
                for (j = 0; j < arraylen; j++)
                {
                    jvalue = json_object_array_get_idx(jobj, j);
                    pData[i++] = (uint8_t)json_object_get_int(jvalue);
                }
            }

            *pDataLength = i;
        }
        break;

        case THERMOSTAT_MODE_GET:
        case THERMOSTAT_MODE_SUPPORTED_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_THERMOSTAT_OPERATING_STATE:
        switch (cmd)
        {

        case THERMOSTAT_OPERATING_STATE_LOGGING_GET_V2:
        {
            i = 0;
            jobj = json_object_object_get(jobj, "bit_mask");
            int arraylen = json_object_array_length(jobj);
            int j;
            json_object *jvalue;
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(jobj, j);
                pData[i++] = (uint8_t)json_object_get_int(jvalue);
            }

            *pDataLength = i;
        }
        break;

        case THERMOSTAT_OPERATING_STATE_GET:
        case THERMOSTAT_OPERATING_STATE_LOGGING_SUPPORTED_GET_V2:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_THERMOSTAT_SETBACK:
        switch (cmd)
        {
        case THERMOSTAT_SETBACK_SET:
        {
            i = 0;
            int setback_type = json_object_get_int(json_object_object_get(jobj, "setback_type"));
            pData[i++] = (uint8_t)setback_type;
            int setback_state = json_object_get_int(json_object_object_get(jobj, "setback_state"));
            pData[i++] = (uint8_t)setback_state;
            *pDataLength = i;
        }
        break;

        case THERMOSTAT_SETBACK_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_THERMOSTAT_SETPOINT:
        switch (cmd)
        {
        case THERMOSTAT_SETPOINT_SET:
        {
            i = 0;
            int setpoint_type = json_object_get_int(json_object_object_get(jobj, "setpoint_type"));
            pData[i++] = (uint8_t)setpoint_type;
            int precision = json_object_get_int(json_object_object_get(jobj, "precision"));
            int scale = json_object_get_int(json_object_object_get(jobj, "scale"));
            int size = json_object_get_int(json_object_object_get(jobj, "size"));
            pData[i++] = ((precision & 0x07) << 5) + ((scale & 0x03) << 3) + ((size & 0x07) << 0);
            int value = json_object_get_int(json_object_object_get(jobj, "value"));
            int j;
            for (j = size - 1; j >= 0; j--)
            {
                pData[i++] = (uint8_t)(value >> (8 * j));
            }
            *pDataLength = i;
        }
        break;

        case THERMOSTAT_SETPOINT_GET:
        case THERMOSTAT_SETPOINT_CAPABILITIES_GET_V3:
        {
            i = 0;

            int setpoint_type = json_object_get_int(json_object_object_get(jobj, "setpoint_type"));
            pData[i++] = (uint8_t)setpoint_type;

            *pDataLength = i;
        }
        break;

        case THERMOSTAT_SETPOINT_SUPPORTED_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        }
        break;

    case COMMAND_CLASS_TIME:
        switch (cmd)
        {
        case TIME_OFFSET_SET_V2:
        {
            i = 0;
            int sign_TZO = json_object_get_int(json_object_object_get(jobj, "sign_TZO"));
            int hour_TZO = json_object_get_int(json_object_object_get(jobj, "hour_TZO"));
            pData[i++] = ((sign_TZO & 0x01) << 7) + ((hour_TZO & 0x7F) << 0);
            int minute_TZO = json_object_get_int(json_object_object_get(jobj, "minute_TZO"));
            pData[i++] = (uint8_t)minute_TZO;
            int sign_offset_DST = json_object_get_int(json_object_object_get(jobj, "sign_offset_DST"));
            int minute_offset_DST = json_object_get_int(json_object_object_get(jobj, "minute_offset_DST"));
            pData[i++] = ((sign_offset_DST & 0x01) << 7) + ((minute_offset_DST & 0x7F) << 0);
            int month_start_DST = json_object_get_int(json_object_object_get(jobj, "month_start_DST"));
            pData[i++] = (uint8_t)month_start_DST;
            int day_start_DST = json_object_get_int(json_object_object_get(jobj, "day_start_DST"));
            pData[i++] = (uint8_t)day_start_DST;
            int hour_start_DST = json_object_get_int(json_object_object_get(jobj, "hour_start_DST"));
            pData[i++] = (uint8_t)hour_start_DST;
            int month_end_DST = json_object_get_int(json_object_object_get(jobj, "month_end_DST"));
            pData[i++] = (uint8_t)month_end_DST;
            int day_end_DST = json_object_get_int(json_object_object_get(jobj, "day_end_DST"));
            pData[i++] = (uint8_t)day_end_DST;
            int hour_end_DST = json_object_get_int(json_object_object_get(jobj, "hour_end_DST"));
            pData[i++] = (uint8_t)hour_end_DST;
            *pDataLength = i;
        }
        break;

        case TIME_GET:
        case DATE_GET:
        case TIME_OFFSET_GET_V2:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_TIME_PARAMETERS:
        switch (cmd)
        {
        case TIME_PARAMETERS_SET:
        {
            i = 0;
            int year = json_object_get_int(json_object_object_get(jobj, "year"));
            pData[i++] = (uint8_t)(year >> 8);
            pData[i++] = (uint8_t)(year >> 0);
            int month = json_object_get_int(json_object_object_get(jobj, "month"));
            pData[i++] = (uint8_t)month;
            int day = json_object_get_int(json_object_object_get(jobj, "day"));
            pData[i++] = (uint8_t)day;
            int hour_utc = json_object_get_int(json_object_object_get(jobj, "hour_utc"));
            pData[i++] = (uint8_t)hour_utc;
            int minute_utc = json_object_get_int(json_object_object_get(jobj, "minute_utc"));
            pData[i++] = (uint8_t)minute_utc;
            int second_utc = json_object_get_int(json_object_object_get(jobj, "second_utc"));
            pData[i++] = (uint8_t)second_utc;
            *pDataLength = i;
        }
        break;

        case TIME_PARAMETERS_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_USER_CODE:
        switch (cmd)
        {
        case USER_CODE_SET:
        {
            i = 0;
            int user_id = json_object_get_int(json_object_object_get(jobj, "user_id"));
            pData[i++] = (uint8_t)user_id;
            int user_id_status = json_object_get_int(json_object_object_get(jobj, "user_id_status"));
            pData[i++] = (uint8_t)user_id_status;

            jobj = json_object_object_get(jobj, "user_code_list");
            int arraylen = json_object_array_length(jobj);
            int j;
            json_object *jvalue;
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(jobj, j);
                pData[i++] = (uint8_t)json_object_get_int(jvalue);
            }
            *pDataLength = i;
        }
        break;

        case USER_CODE_GET:
        {
            i = 0;

            int user_id = json_object_get_int(json_object_object_get(jobj, "user_id"));
            pData[i++] = (uint8_t)user_id;

            *pDataLength = i;
        }
        break;
        case USERS_NUMBER_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_VERSION:
        switch (cmd)
        {
        case VERSION_COMMAND_CLASS_GET:
        {
            i = 0;
            int request_cmd_class = json_object_get_int(json_object_object_get(jobj, "request_cmd_class"));
            pData[i++] = (uint8_t)request_cmd_class;
            *pDataLength = i;
        }
        break;

        case VERSION_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_WAKE_UP:
        switch (cmd)
        {
        case WAKE_UP_INTERVAL_SET:
        {
            i = 0;
            int seconds = json_object_get_int(json_object_object_get(jobj, "seconds"));
            pData[i++] = (uint8_t)(seconds >> 16);
            pData[i++] = (uint8_t)(seconds >> 8);
            pData[i++] = (uint8_t)(seconds >> 0);
            int node_id = json_object_get_int(json_object_object_get(jobj, "node_id"));
            pData[i++] = (uint8_t)node_id;
            *pDataLength = i;
        }
        break;

        case WAKE_UP_INTERVAL_GET:
        case WAKE_UP_INTERVAL_CAPABILITIES_GET_V2:
        case WAKE_UP_NO_MORE_INFORMATION:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_WINDOW_COVERING:
        switch (cmd)
        {
        case WINDOW_COVERING_SUPPORTED_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;

        case WINDOW_COVERING_GET:
        case WINDOW_COVERING_STOP_LEVEL_CHANGE:
        {
            i = 0;

            int param_id = json_object_get_int(json_object_object_get(jobj, "param_id"));
            pData[i++] = (uint8_t)param_id;

            *pDataLength = i;
        }
        break;

        case WINDOW_COVERING_SET:
        {
            i = 0;

            int param_count = json_object_get_int(json_object_object_get(jobj, "param_count"));
            pData[i++] = (uint8_t)(param_count & 0x1F);
            json_object *jparams_list = json_object_object_get(jobj, "params_list");
            int arraylen = json_object_array_length(jparams_list);
            int j;
            json_object *jvalue;
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(jparams_list, j);
                int param_id = json_object_get_int(json_object_object_get(jvalue, "param_id"));
                pData[i++] = (uint8_t)param_id;
                int value = json_object_get_int(json_object_object_get(jvalue, "value"));
                pData[i++] = (uint8_t)value;
            }

            int duration = json_object_get_int(json_object_object_get(jobj, "duration"));
            pData[i++] = (uint8_t)duration;

            *pDataLength = i;
        }
        break;

        case WINDOW_COVERING_START_LEVEL_CHANGE:
        {
            i = 0;
            int up_down = json_object_get_int(json_object_object_get(jobj, "up_down"));
            pData[i++] = (up_down & 0x01) << 6;
            int param_id = json_object_get_int(json_object_object_get(jobj, "param_id"));
            pData[i++] = (uint8_t)param_id;
            int duration = json_object_get_int(json_object_object_get(jobj, "duration"));
            pData[i++] = (uint8_t)duration;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_ZWAVEPLUS_INFO:
        switch (cmd)
        {
        case ZWAVEPLUS_INFO_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;
    case COMMAND_CLASS_ANTITHEFT:
        switch (cmd)
        {
        case ANTITHEFT_SET:
        {
            i = 0;
            int enable = json_object_get_int(json_object_object_get(jobj, "enable"));
            int no_magic_code = json_object_get_int(json_object_object_get(jobj, "no_magic_code"));
            pData[i++] = ((enable & 0x01) << 7) + (no_magic_code & 0x7f);
            json_object *jmagic_code = json_object_object_get(jobj, "magic_code");
            int arraylen = json_object_array_length(jmagic_code);
            int j;
            json_object *jvalue;
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(jmagic_code, j);
                pData[i++] = (uint8_t)json_object_get_int(jvalue);
            }
            int manufacturer_id = json_object_get_int(json_object_object_get(jobj, "manufacturer_id"));
            pData[i++] = (uint8_t)(manufacturer_id >> 8);
            pData[i++] = (uint8_t)(manufacturer_id >> 0);
            int no_anti_theft_hint = json_object_get_int(json_object_object_get(jobj, "no_anti_theft_hint"));
            pData[i++] = (uint8_t)no_anti_theft_hint;
            json_object *janti_theft_hint = json_object_object_get(jobj, "anti_theft_hint");
            arraylen = json_object_array_length(janti_theft_hint);
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(janti_theft_hint, j);
                pData[i++] = (uint8_t)json_object_get_int(jvalue);
            }
            *pDataLength = i;
        }
        break;

        case ANTITHEFT_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }

        break;

    case COMMAND_CLASS_DOOR_LOCK_LOGGING:
        switch (cmd)
        {
        case RECORD_GET:
        {
            i = 0;
            int value = json_object_get_int(json_object_object_get(jobj, "record_number"));
            pData[i++] = (uint8_t)value;
            *pDataLength = i;
        }
        break;
        case DOOR_LOCK_LOGGING_RECORDS_SUPPORTED_GET:
        {
            i = 0;
            *pDataLength = i;
        }
        break;
        }
        break;
    case COMMAND_CLASS_MULTI_CMD:
        switch (cmd)
        {
        case MULTI_CMD_ENCAP:
        {
            i = 0;
            int number_of_commands = json_object_get_int(json_object_object_get(jobj, "number_of_commands"));
            pData[i++] = (uint8_t)number_of_commands;
            mainlog(logDebug, "MULTI_CMD_ENCAP, number_of_commands = %u", number_of_commands);

            json_object *commands_list = json_object_object_get(jobj, "commands_list");
            int arraylen = json_object_array_length(commands_list);
            int j, k;
            json_object *jvalue, *jdata;
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(commands_list, j);
                int command_length = json_object_get_int(json_object_object_get(jvalue, "command_length"));
                pData[i++] = (uint8_t)command_length;
                int command_class = json_object_get_int(json_object_object_get(jvalue, "command_class"));
                pData[i++] = (uint8_t)command_class;
                int command = json_object_get_int(json_object_object_get(jvalue, "command"));
                pData[i++] = (uint8_t)command;
                json_object *jdata_list = json_object_object_get(jvalue, "data_list");
                int datalen = json_object_array_length(jdata_list);
                for (k = 0; k < datalen; k++)
                {
                    jdata = json_object_array_get_idx(jdata_list, k);
                    pData[i++] = (uint8_t)json_object_get_int(jdata);
                }
            }
            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_LANGUAGE:
        switch (cmd)
        {
        case LANGUAGE_SET:
        {
            i = 0;
            int language_1 = json_object_get_int(json_object_object_get(jobj, "language_1"));
            pData[i++] = (uint8_t)language_1;
            int language_2 = json_object_get_int(json_object_object_get(jobj, "language_2"));
            pData[i++] = (uint8_t)language_2;
            int language_3 = json_object_get_int(json_object_object_get(jobj, "language_3"));
            pData[i++] = (uint8_t)language_3;
            int country = json_object_get_int(json_object_object_get(jobj, "country"));
            pData[i++] = (uint8_t)(country >> 8);
            pData[i++] = (uint8_t)(country >> 0);

            *pDataLength = i;
        }
        break;
        case LANGUAGE_GET:
        {
            i = 0;
            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_CLOCK:
        switch (cmd)
        {
        case CLOCK_SET:
        {
            i = 0;
            int weekday = json_object_get_int(json_object_object_get(jobj, "weekday"));
            int hour = json_object_get_int(json_object_object_get(jobj, "hour"));
            pData[i++] = ((weekday & 0x07) << 5) + ((hour & 0x1F) << 0);
            int minute = json_object_get_int(json_object_object_get(jobj, "minute"));
            pData[i++] = (uint8_t)minute;
            *pDataLength = i;
        }
        break;
        case CLOCK_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_SCENE_ACTIVATION:
        switch (cmd)
        {
        case SCENE_ACTIVATION_SET:
        {
            i = 0;
            int scene_id = json_object_get_int(json_object_object_get(jobj, "scene_id"));
            pData[i++] = (uint8_t)scene_id;
            int dimming_duration = json_object_get_int(json_object_object_get(jobj, "dimming_duration"));
            pData[i++] = (uint8_t)dimming_duration;
            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_SCENE_ACTUATOR_CONF:
        switch (cmd)
        {
        case SCENE_ACTUATOR_CONF_GET:
        {
            i = 0;
            int scene_id = json_object_get_int(json_object_object_get(jobj, "scene_id"));
            pData[i++] = (uint8_t)scene_id;
            *pDataLength = i;
        }
        break;
        case SCENE_ACTUATOR_CONF_SET:
        {
            i = 0;
            int scene_id = json_object_get_int(json_object_object_get(jobj, "scene_id"));
            pData[i++] = (uint8_t)scene_id;
            int dimming_duration = json_object_get_int(json_object_object_get(jobj, "dimming_duration"));
            pData[i++] = (uint8_t)dimming_duration;
            int override = json_object_get_int(json_object_object_get(jobj, "override"));
            pData[i++] = ((override & 0x01) << 7);
            int level = json_object_get_int(json_object_object_get(jobj, "level"));
            pData[i++] = (uint8_t)level;
            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_INDICATOR:
        switch (cmd)
        {
        case INDICATOR_SET:
        {
            i = 0;
            json_object *jvalue = json_object_object_get(jobj, "value");
            if (jvalue != NULL)
            {
                int value = json_object_get_int(jvalue);
                pData[i++] = (uint8_t)value;
            }
            json_object *jindicator_0_value = json_object_object_get(jobj, "indicator_0_value");
            if (jindicator_0_value != NULL)
            {
                int indicator_0_value = json_object_get_int(jindicator_0_value);
                pData[i++] = (uint8_t)indicator_0_value;
                int indicator_object_count = json_object_get_int(json_object_object_get(jobj, "indicator_object_count"));
                pData[i++] = (uint8_t)(indicator_object_count & 0x1F);
                json_object *jindicator_object_list = json_object_object_get(jobj, "indicator_object_list");
                int arraylen = json_object_array_length(jindicator_object_list);
                int j;
                for (j = 0; j < arraylen; j++)
                {
                    jvalue = json_object_array_get_idx(jindicator_object_list, j);
                    int indicator_id = json_object_get_int(json_object_object_get(jvalue, "indicator_id"));
                    pData[i++] = (uint8_t)indicator_id;
                    int property_id = json_object_get_int(json_object_object_get(jvalue, "property_id"));
                    pData[i++] = (uint8_t)property_id;
                    int value = json_object_get_int(json_object_object_get(jvalue, "value"));
                    pData[i++] = (uint8_t)value;
                }
            }
            *pDataLength = i;
        }
        break;

        case INDICATOR_GET:
        {
            i = 0;
            json_object *jindicator_id = json_object_object_get(jobj, "indicator_id");
            if (jindicator_id != NULL)
            {/*version 2*/
                int indicator_id = json_object_get_int(jindicator_id);
                pData[i++] = (uint8_t)indicator_id;
            }
            *pDataLength = i;
        }
        break;

        case INDICATOR_SUPPORTED_GET_V2:
        {
            i = 0;
            int indicator_id = json_object_get_int(json_object_object_get(jobj, "indicator_id"));
            pData[i++] = (uint8_t)indicator_id;
            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_NODE_NAMING:
        switch (cmd)
        {
        case NODE_NAMING_NODE_NAME_SET:
        {
            i = 0;
            int char_presentation = json_object_get_int(json_object_object_get(jobj, "char_presentation"));
            pData[i++] = (char_presentation & 0x07);
            jobj = json_object_object_get(jobj, "node_name_list");
            int arraylen = json_object_array_length(jobj);
            int j;
            json_object *jvalue;
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(jobj, j);
                pData[i++] = (uint8_t)json_object_get_int(jvalue);
            }
            *pDataLength = i;
        }
        break;
        case NODE_NAMING_NODE_LOCATION_SET:
        {
            i = 0;
            int char_presentation = json_object_get_int(json_object_object_get(jobj, "char_presentation"));
            pData[i++] = (char_presentation & 0x07);
            jobj = json_object_object_get(jobj, "node_location_list");
            int arraylen = json_object_array_length(jobj);
            int j;
            json_object *jvalue;
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(jobj, j);
                pData[i++] = (uint8_t)json_object_get_int(jvalue);
            }
            *pDataLength = i;
        }
        break;
        case NODE_NAMING_NODE_NAME_GET:
        case NODE_NAMING_NODE_LOCATION_GET:
        {
            i = 0;

            *pDataLength = i;
        }
        break;
        }
        break;
    case COMMAND_CLASS_MANUFACTURER_PROPRIETARY:
    {
        i = 0;
        int manufacturer_id = json_object_get_int(json_object_object_get(jobj, "manufacturer_id"));
        pData[i++] = (uint8_t)(manufacturer_id >> 8);
        pData[i++] = (uint8_t)(manufacturer_id >> 0);
        jobj = json_object_object_get(jobj, "data_list");
        int arraylen = json_object_array_length(jobj);
        int j;
        json_object *jvalue;
        for (j = 0; j < arraylen; j++)
        {
            jvalue = json_object_array_get_idx(jobj, j);
            pData[i++] = (uint8_t)json_object_get_int(jvalue);
        }
        *pDataLength = i;
    }
    break;

    case COMMAND_CLASS_FIRMWARE_UPDATE_MD:
        switch (cmd)
        {
        case FIRMWARE_UPDATE_MD_REQUEST_GET:
        {
            i = 0;
            int manufacturer_id = json_object_get_int(json_object_object_get(jobj, "manufacturer_id"));
            pData[i++] = (uint8_t)(manufacturer_id >> 8);
            pData[i++] = (uint8_t)(manufacturer_id >> 0);
            int firmware_id = json_object_get_int(json_object_object_get(jobj, "firmware_id"));
            pData[i++] = (uint8_t)(firmware_id >> 8);
            pData[i++] = (uint8_t)(firmware_id >> 0);
            int checksum = json_object_get_int(json_object_object_get(jobj, "checksum"));
            pData[i++] = (uint8_t)(checksum >> 8);
            pData[i++] = (uint8_t)(checksum >> 0);
            json_object *jv3_firmware_target = json_object_object_get(jobj, "firmware_target");
            if (jv3_firmware_target != NULL)
            {
                int v3_firmware_target = json_object_get_int(jv3_firmware_target);
                pData[i++] = (uint8_t)v3_firmware_target;
                int fragment_size = json_object_get_int(json_object_object_get(jobj, "fragment_size"));
                pData[i++] = (uint8_t)(fragment_size >> 8);
                pData[i++] = (uint8_t)(fragment_size >> 0);
            }
            *pDataLength = i;
        }
        break;
        case FIRMWARE_UPDATE_MD_GET:
        {
            i = 0;
            int number_of_reports = json_object_get_int(json_object_object_get(jobj, "number_of_reports"));
            pData[i++] = (uint8_t)number_of_reports;
            int report_number = json_object_get_int(json_object_object_get(jobj, "report_number"));
            pData[i++] = ((uint8_t)(report_number >> 8) && 0x7F);
            pData[i++] = (uint8_t)(report_number >> 0);
            *pDataLength = i;
        }
        break;

        case FIRMWARE_UPDATE_MD_REPORT:
        {
            i = 0;
            int last = json_object_get_int(json_object_object_get(jobj, "last"));
            int report_number = json_object_get_int(json_object_object_get(jobj, "report_number"));
            pData[i++] = ((uint8_t)(report_number >> 8) && 0x7F) + ((last & 0x01) << 7);
            pData[i++] = (uint8_t)(report_number >> 0);
            json_object *jdata_list = json_object_object_get(jobj, "data_list");
            int arraylen = json_object_array_length(jdata_list);
            int j;
            json_object *jvalue;
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(jdata_list, j);
                pData[i++] = (uint8_t)json_object_get_int(jvalue);
            }
            json_object *jv2_checksum = json_object_object_get(jobj, "checksum");
            if (jv2_checksum != NULL)
            {
                int checksum = json_object_get_int(jv2_checksum);
                pData[i++] = (uint8_t)(checksum >> 8);
                pData[i++] = (uint8_t)(checksum >> 0);
            }
            *pDataLength = i;
        }
        break;

        case FIRMWARE_MD_GET:
        {
            i = 0;
            *pDataLength = i;
        }
        break;
        }
        break;

    case COMMAND_CLASS_SUPERVISION:
        switch (cmd)
        {
        case SUPERVISION_GET:
        {
            i = 0;
            int status_updates = json_object_get_int(json_object_object_get(jobj, "status_updates"));
            int session_id = json_object_get_int(json_object_object_get(jobj, "session_id"));
            pData[i++] = (uint8_t)(session_id & 0x3F) | ((status_updates & 0x01) << 7);
            int no_encapsulated_cmds = json_object_get_int(json_object_object_get(jobj, "no_encapsulated_cmds"));
            pData[i++] = (uint8_t)(no_encapsulated_cmds & 0x1F);

            jobj = json_object_object_get(jobj, "encapsulated_cmd_list");
            int arraylen = json_object_array_length(jobj);
            int j;
            json_object *jvalue;
            for (j = 0; j < arraylen; j++)
            {
                jvalue = json_object_array_get_idx(jobj, j);
                pData[i++] = (uint8_t)json_object_get_int(jvalue);
            }

            *pDataLength = i;
        }
        break;
        }
        break;
    case COMMAND_CLASS_SCENE_CONTROLLER_CONF:
        switch (cmd)
        {
        case SCENE_CONTROLLER_CONF_GET:
        {
            i = 0;
            int group_id = json_object_get_int(json_object_object_get(jobj, "group_id"));
            pData[i++] = (uint8_t)group_id;
            *pDataLength = i;
        }
        break;
        case SCENE_CONTROLLER_CONF_SET:
        {
            i = 0;
            int group_id = json_object_get_int(json_object_object_get(jobj, "group_id"));
            pData[i++] = (uint8_t)group_id;
            int scene_id = json_object_get_int(json_object_object_get(jobj, "scene_id"));
            pData[i++] = (uint8_t)scene_id;
            int dimming_duration = json_object_get_int(json_object_object_get(jobj, "dimming_duration"));
            pData[i++] = (uint8_t)dimming_duration;
            *pDataLength = i;
        }
        break;
        }
        break;
    case COMMAND_CLASS_REMOTE_ASSOCIATION_ACTIVATE:
        switch (cmd)
        {
        case REMOTE_ASSOCIATION_ACTIVATE:
        {
            i = 0;
            int grouping_id = json_object_get_int(json_object_object_get(jobj, "grouping_id"));
            pData[i++] = (uint8_t)grouping_id;
            *pDataLength = i;
        }
        break;
        }
        break;
    case COMMAND_CLASS_REMOTE_ASSOCIATION:
        switch (cmd)
        {
        case REMOTE_ASSOCIATION_CONFIGURATION_GET:
        {
            i = 0;
            int local_grouping_id = json_object_get_int(json_object_object_get(jobj, "local_grouping_id"));
            pData[i++] = (uint8_t)local_grouping_id;
            *pDataLength = i;
        }
        break;
        case REMOTE_ASSOCIATION_CONFIGURATION_SET:
        {
            i = 0;
            int local_grouping_id = json_object_get_int(json_object_object_get(jobj, "local_grouping_id"));
            pData[i++] = (uint8_t)local_grouping_id;
            int remote_node_id = json_object_get_int(json_object_object_get(jobj, "remote_node_id"));
            pData[i++] = (uint8_t)remote_node_id;
            int remote_grouping_id = json_object_get_int(json_object_object_get(jobj, "remote_grouping_id"));
            pData[i++] = (uint8_t)remote_grouping_id;
            *pDataLength = i;
        }
        break;
        }
        break;

    default:
        break;
    }

    //code free memory
    json_object_put(jobj);
}

unsigned int HslToRgb(double h, double sl, double l, unsigned char * R,unsigned char * G, unsigned char * B)
{
    
    double v;
    double r,g,b;
 
    r = l;   // default to gray
    g = l;
    b = l;
    v = (l <= 0.5) ? (l * (1.0 + sl)) : (l + sl - l * sl);

    if (v > 0)
    {
        double m;
        double sv;
        int sextant;
        double fract, vsf, mid1, mid2;

        m = l + l - v;
        sv = (v - m ) / v;
        h *= 6.0;
        sextant = (int)h;
        fract = h - sextant;
        vsf = v * sv * fract;
        mid1 = m + vsf;
        mid2 = v - vsf;
        switch (sextant)
        {
            case 0:
                r = v;
                g = mid1;
                b = m;
                break;
            case 1:
                r = mid2;
                g = v;
                b = m;
                break;
            case 2:
                r = m;
                g = v;
                b = mid1;
                break;
            case 3:
                r = m;
                g = mid2;
                b = v;
                break;
            case 4:
                r = mid1;
                g = m;
                b = v;
                break;
            case 5:
                r = v;
                g = m;
                b = mid2;
                break;
        }
    }
    // ColorRGB rgb;
    // rgb.R = Convert.ToByte(r * 255.0f);
    // rgb.G = Convert.ToByte(g * 255.0f);
    // rgb.B = Convert.ToByte(b * 255.0f);
    *R=(unsigned char)(r*255);
    *G=(unsigned char)(g*255);
    *B=(unsigned char)(b*255);
    printf("RGB:%02X:%02X:%02X\n",*R,*G,*B);
    return 0;
}

void SwapBytes(void *pv, size_t n)
{
    char *p = pv;
    size_t lo, hi;
    for(lo=0, hi=n-1; hi>lo; lo++, hi--)
    {
        char tmp=p[lo];
        p[lo] = p[hi];
        p[hi] = tmp;
    }
}

uint32_t htoi (const char *ptr) 
{
    uint32_t value = 0;
    if(!ptr)
    {
        return value;
    }

    char ch = *ptr;
    
    while (ch == ' ' || ch == '\t')
        ch = *(++ptr);

    for (;;) {
        if (ch >= '0' && ch <= '9')
            value = (value << 4) + (ch - '0');
        else if (ch >= 'A' && ch <= 'F')
            value = (value << 4) + (ch - 'A' + 10);
        else if (ch >= 'a' && ch <= 'f')
            value = (value << 4) + (ch - 'a' + 10);
        else
        {
            return value;
        }

        ch = *(++ptr);
    }
}

int expo(int n)
{
    int i;
    int result = 1;
    for(i=0;i<n;i++)
    {
        result = result*10;
    }
    return result;
}

// need free after call
char *get_zwave_controller_version(void)
{
    char *version = NULL;
    TZWParam *pzwParam = (TZWParam *)malloc(sizeof(TZWParam));
    memset(pzwParam, 0x00, sizeof(TZWParam));
    pzwParam->misc_data = malloc(SIZE_256B);
    pzwParam->command = COMMAND_CLASS_SPECIFIC_GET_VERSION_FIRMWARE;
    zwaveSendCommand(pzwParam);
    if(!pzwParam->ret)
    {
        version = strdup(pzwParam->misc_data);
    }
    free(pzwParam->misc_data);
    free(pzwParam);
    return version;
}

int zwave_update_firmwre(char *path)
{
    int ret = -1;
    TZWParam *pzwParam = (TZWParam *)malloc(sizeof(TZWParam));
    memset(pzwParam, 0x00, sizeof(TZWParam));
    pzwParam->misc_data = (void *)path;
    pzwParam->command = COMMAND_CLASS_SPECIFIC_UPDATE_FIRMWARE;

    zwaveSendCommand(pzwParam);
    if(pzwParam->ret)
    {
        SLOGE("update zwave controller failed, try again\n");
        sleep(1);
        zwaveSendCommand(pzwParam);
    }
    ret = pzwParam->ret;
    free(pzwParam);
    return ret;
}

void convert_endpoint_to_string(uint8_t endpoint[], uint8_t enpointNum, char *output)
{
    if(!output)
    {
        return;
    }
    int i;

    for (i=0;i<enpointNum;i++)
    {
        if(i == 0)
        {
            sprintf(output, "%02X", endpoint[i]);
        }
        else
        {
            sprintf(output+strlen(output), ",%02X", endpoint[i]);
        }
    }
}

void convert_capability_to_string(uint8_t cap[], uint8_t capNum, int scheme,
                                int *multichannel_sec, int *configuration_sec, int *association_sec,
                                char *output, size_t outLength)
{
    if(!output)
    {
        return;
    }
    int i;
    int cx;

    for (i=0;i<capNum;i++)
    {
        if(COMMAND_CLASS_MULTI_CHANNEL_V3 == cap[i])
        {
            SET_VALUE(multichannel_sec, scheme);
        }

        if(COMMAND_CLASS_CONFIGURATION == cap[i])
        {
            SET_VALUE(configuration_sec, scheme);
        }

        if(COMMAND_CLASS_ASSOCIATION == cap[i])
        {
            SET_VALUE(association_sec, scheme);
        }

        if(i == 0)
        {
            cx = snprintf(output, outLength, "%d", cap[i]);
        }
        else
        {
            if(cx >=0 && cx<outLength)
            {
                cx += snprintf(output+cx, outLength-cx, ",%d", cap[i]);
            }
        }
    }
}

void get_capability(NOTIFY_TX_BUFFER_T pTxNotify, char *nonSecureCap, char *secCap, char *sec2Cap, 
                    int *multichannel_sec, int *configuration_sec, int *association_sec, size_t length)
{
    if(!nonSecureCap || !secCap || !sec2Cap)
    {
        return;
    }

    convert_capability_to_string(pTxNotify.AddNodeZPCNotify.zpc_node.node_capability.aCapability,
                                pTxNotify.AddNodeZPCNotify.zpc_node.node_capability.noCapability,
                                NON_SEC_SCHEME, multichannel_sec, configuration_sec, association_sec, nonSecureCap, length);
                
    if (pTxNotify.AddNodeZPCNotify.zpc_node.node_secure_scheme==SEC_0_SCHEME)
    {
        convert_capability_to_string(pTxNotify.AddNodeZPCNotify.zpc_node.node_capability_secureV0.aCapability,
                                pTxNotify.AddNodeZPCNotify.zpc_node.node_capability_secureV0.noCapability,
                                SEC_0_SCHEME, multichannel_sec, configuration_sec, association_sec, secCap, length);
    }

    if ((pTxNotify.AddNodeZPCNotify.zpc_node.node_secure_scheme==SEC_2_1_SCHEME) 
        || (pTxNotify.AddNodeZPCNotify.zpc_node.node_secure_scheme==SEC_2_2_SCHEME)
        || (pTxNotify.AddNodeZPCNotify.zpc_node.node_secure_scheme==SEC_2_3_SCHEME)
        )
    {
        convert_capability_to_string(pTxNotify.AddNodeZPCNotify.zpc_node.node_capability_secureV2.aCapability,
                                pTxNotify.AddNodeZPCNotify.zpc_node.node_capability_secureV2.noCapability,
                                pTxNotify.AddNodeZPCNotify.zpc_node.node_secure_scheme,
                                multichannel_sec, configuration_sec, association_sec, sec2Cap, length);
    }
}
