#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "action_control.h"
#include "database.h"
#include "verik_utils.h"
#include "slog.h"

extern int g_open_network;
extern sqlite3 *zwave_db;

static void add_new_element_to_queue(zwave_command_queue_t *queue, 
                             pthread_mutex_t *queueMutex, 
                             zwave_command_response_t response, 
                             TZWParam *pzwParam);

zwave_queue_priority_t zwave_queue_priority_table[]=
{
    {.queueId=HIGH_PRIORITY , .weight=10}, //highest
    {.queueId=LOW_PRIORITY , .weight=5}, //lowest
    {.queueId=DEAD_PRIORITY , .weight=1, .wait=DEFAULT_DEAD_WAIT}, //dead device
};

zwave_queue_priority_t *start_queue = &zwave_queue_priority_table[0];
zwave_queue_priority_t *end_queue = &zwave_queue_priority_table[0] + sizeof(zwave_queue_priority_table)/sizeof(zwave_queue_priority_t);

void init_queue_table(void)
{
    int i;
    for(i=0; i < sizeof(zwave_queue_priority_table)/sizeof(zwave_queue_priority_t); i++)
    {
        VR_INIT_LIST_HEAD(&zwave_queue_priority_table[i].queue.list);
        pthread_mutex_init(&zwave_queue_priority_table[i].queueMutex, 0);
        zwave_queue_priority_table[i].queue.command_index = 0;
    }
}

void update_dev_priority(char *nodeId, TZWParam *pzwParam)
{
    if(!nodeId || !pzwParam)
    {
        SLOGE("nodeId or pzwParam invalid\n");
        return;
    }

    zwave_dev_info_t *tmp = get_zwave_dev_from_id(nodeId);
    if(!tmp)
    {
        SLOGE("not found zwave dev with id = %s\n", nodeId);
        return;
    }

    if(pzwParam->ret == 0)
    {
        tmp->status.priority += SUCCESS_PRIZE_NUM;
        if(tmp->status.priority > MAX_PRIORITY_NUM)
        {
            tmp->status.priority = MAX_PRIORITY_NUM;
        }
    }
    else if(pzwParam->ret != 2)
    {
        if(!g_open_network)
        {
            tmp->status.priority -= FAILED_PRIZE_NUM;
            if(tmp->status.priority < DEAD_PRIORITY_NUM)
            {
                tmp->status.priority = DEAD_PRIORITY_NUM;
            }
        }
    }

    char priorityStr[SIZE_32B];
    sprintf(priorityStr, "%d", tmp->status.priority);
    set_register_database(_VR_CB_(zwave), nodeId, ST_PRIORITY, priorityStr, ST_REPLACE, 0);
}

zwave_queue_priority_t *get_queue_from_queueId(int queueId)
{
    zwave_queue_priority_t *tmp = &zwave_queue_priority_table[0];
    while(tmp < end_queue)
    {
        if(tmp->queueId == queueId)
        {
            return tmp;
        }
        tmp++;
    }

    return NULL;
}

zwave_command_queue_t *get_and_delete_first_element_in_queue(zwave_command_queue_t *queue,
                                                            pthread_mutex_t *queueMutex)
{
    zwave_command_queue_t *command = VR_list_entry(queue->list.next, zwave_command_queue_t, list);

    pthread_mutex_lock(queueMutex);
    VR_(list_del)(queue->list.next);
    queue->command_index--;
    pthread_mutex_unlock(queueMutex);
    
    return command;
}

void free_queue_element(zwave_command_queue_t *command)
{
    SAFE_FREE(command->response.data0);
    SAFE_FREE(command->response.data1);
    SAFE_FREE(command->response.data2);
    SAFE_FREE(command->response.compliantJson);
    SAFE_FREE(command->pzwParam->misc_data);
    SAFE_FREE(command->pzwParam);
    SAFE_FREE(command);
}

zwave_command_queue_t *get_first_element_in_queue(zwave_command_queue_t *queue)
{
    return VR_list_entry(queue->list.next, zwave_command_queue_t, list);
}

zwave_command_queue_t *get_last_element_in_queue(zwave_command_queue_t *queue)
{
    return VR_list_entry(queue->list.prev, zwave_command_queue_t, list);
}

/*
    return queue will be executed.
    dead queue will be executed if no command found in higher queue.
*/
int decide_queue_to_execute(void)
{
    int i, num_queue;
    num_queue = sizeof(zwave_queue_priority_table)/sizeof(zwave_queue_priority_t);
    for(i=0; i < num_queue; i++)
    {
        if(zwave_queue_priority_table[i].queue.command_index <= 0)// no command, reset counting
        {
            zwave_queue_priority_table[i].counting = 0;
            continue;
        }

        //has command, count equal weight, reset counting
        if(zwave_queue_priority_table[i].weight <= zwave_queue_priority_table[i].counting)
        {
            zwave_queue_priority_table[i].counting = 0;
            if(i == (num_queue-1))
            {
                i = 0;
            }
            continue;
        }

        if(DEAD_PRIORITY == zwave_queue_priority_table[i].queueId)
        {
            if(zwave_queue_priority_table[i].wait>0)
            {
                zwave_queue_priority_table[i].wait--;
                return NO_COMMAND;
            }
        }
        else
        {
            //remove dead wait if has command form higher priority queue.
            //zwave_queue_priority_table[num_queue-1].wait = 0;
        }

        zwave_queue_priority_table[i].counting++;
        return zwave_queue_priority_table[i].queueId;
    }

    zwave_queue_priority_table[num_queue-1].wait = DEFAULT_DEAD_WAIT; //reset dead wait if nocommand found
    return NO_COMMAND;
}

static int decide_queue_to_insert(zwave_command_response_t *response, zwave_dev_info_t *dev)
{
    char *method = &response->method[0];
    // char *nodeid = &response->nodeid[0];
    int cmdType = response->cmdType;

    if(!dev)
    {
        response->priority = INIT_PRIORITY_NUM;
        return HIGH_PRIORITY;//high priority
    }

    dev->totalCmd++;
    response->cmdId = dev->totalCmd;
    response->nodeId = dev->localId;
    response->priority = dev->status.priority;

    if(response->priority <= DEAD_PRIORITY_NUM)
    {
        return DEAD_PRIORITY;
    }

    if(!strcmp(method, ST_SET_BINARY_R))
    {
        int value = response->cmdValue;
        
        if(cmdType == COMMAND_TYPE_SET_ONOFF)
        {
            if(dev->status.onOff != value)
            {
                return HIGH_PRIORITY;
            }
            else
            {
                return LOW_PRIORITY;
            }
        }
        else
        {
            if(0 == dev->status.onOff && 0 != dev->status.dim)
            {
                return HIGH_PRIORITY;
            }

            if(dev->status.dim != value)
            {
                return HIGH_PRIORITY;
            }
            else
            {
                return LOW_PRIORITY;
            }
        }
    }
    else
    {
        return HIGH_PRIORITY;//high priority
    }
}

static int find_opposite_command(zwave_command_queue_t *queue, 
                             pthread_mutex_t *queueMutex, 
                             zwave_command_response_t response)
{
    int found = 0;
    zwave_command_queue_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &queue->list)
    {
        tmp = VR_(list_entry)(pos, zwave_command_queue_t, list);
        if( tmp->response.cmdType &&
            (tmp->response.nodeId == response.nodeId) &&
            ((tmp->response.cmdType != response.cmdType) || 
             (tmp->response.cmdValue != response.cmdValue)
             )
          )
        {
            found = 1;
            break;
        }
    }

    return found;
}

/*
    0: SUCCESS
    1: ERROR
*/
static int _insert_to_queue(int queueId, zwave_command_response_t response, TZWParam *pzwParam)
{
    int i, rc = 0;
    for(i=0; i < sizeof(zwave_queue_priority_table)/sizeof(zwave_queue_priority_t); i++)
    {
        if(zwave_queue_priority_table[i].queueId == queueId)
        {
            add_new_element_to_queue(&zwave_queue_priority_table[i].queue, 
                                    &zwave_queue_priority_table[i].queueMutex, 
                                    response, 
                                    pzwParam);
            return rc;
        }
    }

    /*not found queue to insert*/
    rc = 1;
    SAFE_FREE(response.data0);
    SAFE_FREE(response.data1);
    SAFE_FREE(response.data2);
    SAFE_FREE(response.compliantJson);
    SAFE_FREE(pzwParam->misc_data);
    SAFE_FREE(pzwParam);
    return rc;
}

/*
    re-insert to queue, do not free pointer in response and pzwParam
    need check opposite command in higher queue.
    if found opposite, no need to re-insert.

    0: SUCCESS
    1: ERROR
*/
int re_insert_to_a_queue(int queueId, zwave_command_response_t response, TZWParam *pzwParam)
{
    SLOGI("start re-insert to queue with Id = %d nodeId %s priority %d\n", queueId, response.nodeid, response.priority);
    int i, found_opposite = 0;

    if(!strcmp(response.method, ST_SET_BINARY_R))
    {
        for(i=0; i < sizeof(zwave_queue_priority_table)/sizeof(zwave_queue_priority_t); i++)
        {
            /*finding opposite command in current and higher queue*/
            if(queueId >= zwave_queue_priority_table[i].queueId &&
                zwave_queue_priority_table[i].queue.command_index != 0)
            {
                found_opposite = find_opposite_command(&zwave_queue_priority_table[i].queue, 
                                        &zwave_queue_priority_table[i].queueMutex, 
                                        response);
                if(found_opposite)
                {
                    break;
                }
            }
        }
    }

    if(!found_opposite)
    {
        found_opposite = _insert_to_queue(queueId, response, pzwParam);
    }
    else
    {
        SLOGI("found opposite not insert queue %d nodeId %s priority %d\n", queueId, response.nodeid, response.priority);
    }

    return found_opposite;
}

/*
    0: REMOVE SUCCESS
    1: NOT FOUND
*/
static int remove_opposite_command(zwave_command_queue_t *queue, 
                             pthread_mutex_t *queueMutex, 
                             zwave_command_response_t response)
{
    int rc = 1;
    pthread_mutex_lock(queueMutex);
    zwave_command_queue_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &queue->list)
    {
        tmp = VR_(list_entry)(pos, zwave_command_queue_t, list);
        if( tmp->response.cmdType &&
            (tmp->response.nodeId == response.nodeId) &&
            ((tmp->response.cmdType != response.cmdType) || 
             (tmp->response.cmdValue != response.cmdValue)
             )
          )
        {
            VR_(list_del)(pos);
            free_queue_element(tmp);
            queue->command_index--;
            rc = 0;
            break;
        }
    }
    pthread_mutex_unlock(queueMutex);

    return rc;
}

void insert_to_a_queue(int queueId, zwave_command_response_t response, TZWParam *pzwParam)
{
    string_to_hash((unsigned char *)&response, sizeof(zwave_command_response_t), 
                    response.hash, sizeof(response.hash));

    SLOGI("insert to queue with Id = %d nodeId %s priority %d\n", queueId, response.nodeid, response.priority);

    _insert_to_queue(queueId, response, pzwParam);
}

void insert_to_queue(zwave_command_response_t response, TZWParam *pzwParam, zwave_dev_info_t *dev)
{
    int queueId = 0, i = 0;
    string_to_hash((unsigned char *)&response, sizeof(zwave_command_response_t), 
                    response.hash, sizeof(response.hash));

    queueId = decide_queue_to_insert(&response, dev);
    SLOGI("insert to queue with Id = %d nodeId %s priority %d\n", queueId, response.nodeid, response.priority);

    if(!strcmp(response.method, ST_SET_BINARY_R))
    {
        /*finding opposite command in lower queue*/
        for(i=0; i < sizeof(zwave_queue_priority_table)/sizeof(zwave_queue_priority_t); i++)
        {
            if(queueId < zwave_queue_priority_table[i].queueId &&
                zwave_queue_priority_table[i].queue.command_index != 0)
            {
                int rc = remove_opposite_command(&zwave_queue_priority_table[i].queue, 
                                        &zwave_queue_priority_table[i].queueMutex, 
                                        response);
                if(dev && !rc)
                {
                    /*remove success, should increase cmdComplete*/
                    dev->cmdComplete++;
                }
            }
        }
    }

    if(_insert_to_queue(queueId, response, pzwParam))
    {
        /*insert failed, should increase cmdComplete
          because totalCmd and cmdId was increased*/
        dev->cmdComplete++;
    }
}

void moving_element_thread(void *data)
{
    if(!data)
    {
        SLOGE("data is NULL\n");
        return;
    }

    zwave_command_queue_t *action = ((moving_action_t *)data)->action;
    zwave_queue_priority_t *queueInfo = ((moving_action_t *)data)->queueInfo;
    add_new_element_to_queue(&queueInfo->queue, 
                             &queueInfo->queueMutex, 
                             action->response, 
                             action->pzwParam);
    free_queue_element(action);
    SAFE_FREE(data);
}

static void _moving_element(zwave_command_queue_t *queue, zwave_command_queue_t *tmp)
{
    if(!queue || !tmp)
    {
        return;
    }

    zwave_queue_priority_t *next_queue = VR_list_entry(queue, zwave_queue_priority_t, queue)+1;
    if(next_queue < end_queue && next_queue->queueId)
    {
        SLOGI("insert devId %s to queueId = %d\n", tmp->response.nodeid, next_queue->queueId);

        add_new_element_to_queue(&next_queue->queue, 
                             &next_queue->queueMutex, 
                             tmp->response, 
                             tmp->pzwParam);

        free_queue_element(tmp);
        // moving_action_t *data = (moving_action_t*)malloc(sizeof(moving_action_t));
        // data->action = tmp;
        // data->queueInfo = next_queue;
        // pthread_t moving_action_thread_t;
        // pthread_create(&moving_action_thread_t, NULL, (void *)&moving_element_thread, (void *)data);
        // pthread_detach(moving_action_thread_t);
    }
    else
    {
        SLOGI("remove devId %s from queueId %d\n", tmp->response.nodeid, (next_queue-1)->queueId);
        free_queue_element(tmp);
    }
}

static void moving_element_to_lower_queue(zwave_command_queue_t *queue,
                                        zwave_command_response_t response)
{
    zwave_command_queue_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &queue->list)
    {
        tmp = VR_(list_entry)(pos, zwave_command_queue_t, list);
        if(!strcmp(tmp->response.hash, response.hash))
        {
            VR_(list_del)(pos);
            _moving_element(queue, tmp);
            queue->command_index--;
            break;
        }
    }
}

static void _remove_queue_element(zwave_command_queue_t *queue,
                                zwave_command_response_t response)
{
    zwave_command_queue_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &queue->list)
    {
        tmp = VR_(list_entry)(pos, zwave_command_queue_t, list);
        if(!strcmp(tmp->response.hash, response.hash))
        {
            SLOGI("remove devId = %s from queue %d \n", response.nodeid, VR_list_entry(queue, zwave_queue_priority_t, queue)->queueId);
            VR_(list_del)(pos);
            free_queue_element(tmp);
            queue->command_index--;
        }
    }
}

void remove_element_from_queue(zwave_command_queue_t *queue, 
                             pthread_mutex_t *queueMutex, 
                             zwave_command_response_t response)
{
    pthread_mutex_lock(queueMutex);
    _remove_queue_element(queue, response);
    pthread_mutex_unlock(queueMutex);
}

/*
    this function will add element to list in the order of priority and deviceId.
    if 2 element is the same id, the element come later will stand behind.
    if 2 element is different id, higher priority stand first and lower priority is the last.
*/
static void add_element_to_list(struct VR_list_head *new, 
                                struct VR_list_head *head, 
                                int priority, char *nodeId)
{
    zwave_command_queue_t *tmp = NULL;

    struct VR_list_head *pos;
    struct VR_list_head *pos_priority;

    int found_priority = 0;
    int found_id = 0;

    VR_list_for_each(pos, head)
    {
        tmp = VR_list_entry(pos, zwave_command_queue_t, list);
        if(!strcmp(tmp->response.nodeid, nodeId))
        {
            pos = pos->next;
            if(pos != head)
            {
                found_id = 1;
            }
            break;
        }

        if(!found_priority)
        {
            if(tmp->response.priority < priority)
            {
                found_priority = 1;
                pos_priority = pos;
                // break;
            }
        }
    }

    if(found_id)
    {
        VR_list_add_tail(new, pos);
    }
    else if(found_priority)
    {
        VR_list_add_tail(new, pos_priority);
    }
    else
    {
        VR_list_add_tail(new, head);
    }
}

static void add_new_element_to_queue(zwave_command_queue_t *queue, 
                             pthread_mutex_t *queueMutex, 
                             zwave_command_response_t response, 
                             TZWParam *pzwParam)
{
    if(!pzwParam)
    {
        SLOGE("pzwParam not found\n");
        return;
    }

    int added = 0;
    zwave_command_queue_t *tmp = NULL;
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &queue->list)
    {
        tmp = VR_(list_entry)(pos, zwave_command_queue_t, list);
        if(!strcmp(tmp->response.hash, response.hash))
        {
            added = 1;
            SLOGI("COMMAND EXIST IN QUEUE\n");
            break;
        }
    }

    if(!added)
    {
        pthread_mutex_lock(queueMutex);

        zwave_command_queue_t *action = (zwave_command_queue_t *)malloc(sizeof(zwave_command_queue_t));
        memset(action, 0x00, sizeof(zwave_command_queue_t));

        action->pzwParam = (TZWParam*)malloc(sizeof(TZWParam));
        memcpy(action->pzwParam, (uint8_t*)pzwParam, sizeof(TZWParam));
        action->pzwParam->misc_data = pzwParam->misc_data;

        memcpy(&action->response, (void*)&response, sizeof(zwave_command_response_t));
        action->response.data0 = response.data0;
        action->response.data1 = response.data1;
        action->response.data2 = response.data2;
        action->response.compliantJson = response.compliantJson;

        if(MAX_COMMAND_IN_QUEUE == queue->command_index)
        {
            SLOGI("QUEUE MAX\n");
            zwave_command_queue_t *tmp = get_last_element_in_queue(queue);
            if(tmp->response.priority < action->response.priority)
            {
                moving_element_to_lower_queue(queue, tmp->response);//remove insert first
                add_element_to_list(&(action->list), &(queue->list), 
                                    action->response.priority,
                                    action->response.nodeid);
                queue->command_index++;
            }
            else
            {
                _moving_element(queue, action);
            }
        }
        else
        {
            add_element_to_list(&(action->list), &(queue->list), 
                                action->response.priority,
                                action->response.nodeid);
            queue->command_index++;
        }
        pthread_mutex_unlock(queueMutex);
    }
}

int check_command_in_queue_is_valid_to_execute(zwave_queue_priority_t *queueInfo)
{
    int rc = 0; /*execute*/
    if(!queueInfo)
    {
        return rc;
    }

    zwave_command_queue_t *commandInQ = get_first_element_in_queue(&queueInfo->queue);
    if(!commandInQ)
    {
        return rc;
    }

    int cmdId = commandInQ->response.cmdId;
    if(cmdId == 0)
    {
        return rc;
    }

    char *nodeid = commandInQ->response.nodeid;
    zwave_dev_info_t *zwave_dev = get_zwave_dev_from_id(nodeid);
    if(!zwave_dev)
    {
        return rc;
    }

    if((cmdId - zwave_dev->cmdComplete) > 1)/**/
    {
        zwave_queue_priority_t *prevQueue = queueInfo-1;
        if(prevQueue >= start_queue) /*found privious queue*/
        {
            if(prevQueue->queue.command_index > 0)
            {
                rc = 1; /*ignore*/
            }
            else
            {
                /*should we reset totalCmd and cmdComplete here?*/
                zwave_dev->totalCmd = 0;
                zwave_dev->cmdComplete = 0;
                // zwave_dev->cmdComplete++;
            }
        }
    }

    if(!rc) /*execute*/
    {
        zwave_dev->cmdComplete++;
    }

    if(zwave_dev->cmdComplete >= zwave_dev->totalCmd)
    {
        zwave_dev->totalCmd = 0;
        zwave_dev->cmdComplete = 0;
    }

    return rc;
}

int total_command_in_queue(void)
{
    int i,total = 0;
    for(i=0; i < sizeof(zwave_queue_priority_table)/sizeof(zwave_queue_priority_t); i++)
    {
        total += zwave_queue_priority_table[i].queue.command_index;
    }

    return total;
}

#if 0
void printf_queue(zwave_command_queue_t *queue)
{
    int i=0;
    zwave_command_queue_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &queue->list, list)
    {
        printf("#### dev %d ######\n", i);
        printf("nodeid = %s with priority =%d\n", tmp->response.nodeid, tmp->response.priority);
        printf("command = %s with value =%s\n", tmp->response.command, tmp->response.value);
        i++;
    }
}

void test_remove_queue(zwave_command_queue_t *queue, pthread_mutex_t *queueMutex)
{
    while(!VR_list_empty(&queue->list))
    {
        zwave_command_queue_t *tmp = get_and_delete_first_element_in_queue(queue, queueMutex);
        free_queue_element(tmp);
    }
}

void test_insert_queue()
{
    int i = 0;

    for(i =0; i < 200; i++)
    {
        zwave_command_response_t response;
        memset(&response, 0x00, sizeof(zwave_command_response_t));
        strcpy(response.method, ST_SET_BINARY_R);
        sprintf(response.nodeid, "%d", i);

        TZWParam *pzwParam = (TZWParam*)malloc(sizeof(TZWParam));
        memset(pzwParam, 0x00, sizeof(TZWParam));

        pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_REQUEST_SPECIFICATION_ZPC;
        pzwParam->param2=i;
        pzwParam->src_endpoint=0;

        insert_to_queue(response, pzwParam);
        free(pzwParam);
    }

    printf_queue(&zwave_queue_priority_table[0].queue);
    SLOGI("zwave_queue_priority_table[0].queue.command_index = %d\n", zwave_queue_priority_table[0].queue.command_index);
    sleep(5);
    printf_queue(&zwave_queue_priority_table[1].queue);
    SLOGI("zwave_queue_priority_table[1].queue.command_index = %d\n", zwave_queue_priority_table[1].queue.command_index);
    sleep(5);
    printf_queue(&zwave_queue_priority_table[2].queue);
    SLOGI("zwave_queue_priority_table[2].queue.command_index = %d\n", zwave_queue_priority_table[2].queue.command_index);


    SLOGI("\n\n\n ################# START REMOVE 0 ##################\n");
    test_remove_queue(&zwave_queue_priority_table[0].queue, &zwave_queue_priority_table[0].queueMutex);
    SLOGI("zwave_queue_priority_table[0].queue.command_index = %d\n", zwave_queue_priority_table[0].queue.command_index);
    printf_queue(&zwave_queue_priority_table[0].queue);
    SLOGI("\n\n\n ################# START REMOVE 1 ##################\n");
    test_remove_queue(&zwave_queue_priority_table[1].queue, &zwave_queue_priority_table[1].queueMutex);
    SLOGI("zwave_queue_priority_table[1].queue.command_index = %d\n", zwave_queue_priority_table[1].queue.command_index);
    printf_queue(&zwave_queue_priority_table[1].queue);
    SLOGI("\n\n\n ################# START REMOVE 2 ##################\n");
    test_remove_queue(&zwave_queue_priority_table[2].queue, &zwave_queue_priority_table[2].queueMutex);
    SLOGI("zwave_queue_priority_table[2].queue.command_index = %d\n", zwave_queue_priority_table[2].queue.command_index);
    printf_queue(&zwave_queue_priority_table[2].queue);
}

void test_decice_insert()
{
    zwave_queue_priority_table[0].queue.command_index = 0;
    zwave_queue_priority_table[0].counting = 0;

    zwave_queue_priority_table[1].queue.command_index = 0;
    zwave_queue_priority_table[1].counting = 0;

    zwave_queue_priority_table[2].queue.command_index = 3;
    zwave_queue_priority_table[2].counting = 0;
    SLOGI("\n\n########START TEST#############\n\n");
    SLOGI("zwave_queue_priority_table[0].queue.command_index = %d\n", zwave_queue_priority_table[0].queue.command_index);
    SLOGI("zwave_queue_priority_table[1].queue.command_index = %d\n", zwave_queue_priority_table[1].queue.command_index);
    SLOGI("zwave_queue_priority_table[2].queue.command_index = %d\n", zwave_queue_priority_table[2].queue.command_index);
    int i=0, ret;
    while (zwave_queue_priority_table[0].queue.command_index ||
            zwave_queue_priority_table[1].queue.command_index ||
            zwave_queue_priority_table[2].queue.command_index)
    {
        ret = decide_queue_to_execute();
        SLOGI("queueId = %d\n", ret);
        if(ret != NO_COMMAND)
        {
            zwave_queue_priority_table[ret-1].queue.command_index--;
        }

        if(i == 4)
        {
            zwave_queue_priority_table[0].queue.command_index = 30;
            zwave_queue_priority_table[0].counting = 0;

            zwave_queue_priority_table[1].queue.command_index = 20;
            zwave_queue_priority_table[1].counting = 0;
        }

        i++;

    }

    decide_queue_to_execute();

    zwave_queue_priority_table[2].queue.command_index = 3;
    zwave_queue_priority_table[2].counting = 0;
    while (zwave_queue_priority_table[0].queue.command_index ||
            zwave_queue_priority_table[1].queue.command_index ||
            zwave_queue_priority_table[2].queue.command_index)
    {
        ret = decide_queue_to_execute();
        SLOGI("queueId = %d\n", ret);
        if(ret != NO_COMMAND)
        {
            zwave_queue_priority_table[ret-1].queue.command_index--;
        }
    }

    SLOGI("zwave_queue_priority_table[0].queue.command_index = %d\n", zwave_queue_priority_table[0].queue.command_index);
    SLOGI("zwave_queue_priority_table[1].queue.command_index = %d\n", zwave_queue_priority_table[1].queue.command_index);
    SLOGI("zwave_queue_priority_table[2].queue.command_index = %d\n", zwave_queue_priority_table[2].queue.command_index);
}
#endif

