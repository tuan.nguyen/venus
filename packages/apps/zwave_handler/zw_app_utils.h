#ifndef _ZW_APP_UTILS_H_
#define _ZW_APP_UTILS_H_

#include <json-c/json.h>
#include "zw_serialapi/zw_classcmd.h"
#include "zw_api.h"
#include "utils.h"
#include "VR_define.h"
#include "VR_list.h"

#define TEST 1
#define NON_SEC_SCHEME 255
#define SEC_0_SCHEME     7
#define SEC_2_1_SCHEME   1
#define SEC_2_2_SCHEME   2
#define SEC_2_3_SCHEME   3

#define RATE_TYPE_BIT_MASK      0x03 //BIT1 + BIT2
#define SCALE_BIT_MASK          0x07 //BIT1 + BIT2 + BIT3
#define RATE_TYPE_POSITION      0x06 //<< 6 OR >> 6
#define SCALE_POSITION          0x03 //<< 3 OR >> 3

//color define
#define RGB_COLOR               "rgb"
#define TEMP_COLOR              "kt"

#define WARM_WHITE              0x00
#define COOL_WHITE              0x01
#define RED                     0x02
#define GREEN                   0x03
#define BLUE                    0x04

#define MAX_NUMBER_COMPONENT    0x05
#define NUMBER_COMPONENT_INDEX  2

#define WARM_WHITE_INDEX        3
#define COOL_WHITE_INDEX        5
#define RED_INDEX               7
#define GREEN_INDEX             9
#define BLUE_INDEX              11

#define DOORLOCK_USERCODE_DELETE   0
#define DOORLOCK_USERCODE_ADD      1

#define COMMAND_TYPE_SET_ONOFF         1
#define COMMAND_TYPE_SET_DIM           2

#define ALARM_VIA_SPEAKER_NO_DATA        0
#define ALARM_VIA_SPEAKER_ENABLE         1
#define ALARM_VIA_SPEAKER_DISABLE        2

#define SWAP(x) SwapBytes(&x, sizeof(x));

typedef struct _zwave_dev_status
{
    bool dimFirst;
    uint8_t onOff;
    uint8_t dim;
    uint8_t state; //1 alive, 0 dead
    uint32_t lastUpdate;
    uint32_t lastPoll;
    uint32_t wakeUp;
    int priority;
}zwave_dev_status_t; 

typedef struct _zwave_dev_info
{
    uint16_t localId;
    uint8_t deviceMode;
    uint8_t deviceType;
    uint8_t active;
    uint8_t endpointNum;
    uint8_t parentId;

    char serialId[SIZE_64B];
    char cloudId[SIZE_256B];
    char childrenId[SIZE_256B];
    char defaultSound[SIZE_256B];
    char VRDeviceType[SIZE_128B];

    // pthread_t getSoundFilesThread_t;

    json_object *json_capId;
    zwave_dev_status_t status;

    int totalCmd; //total_command has executed and current in queue.
    int cmdComplete; //command has executed.
    uint64_t lastExecuteTime; //the last control.

    uint8_t speakerAlarm; //allow device inform via PhD speaker or not.
    uint32_t temporary_disable_alarm_time;

    struct VR_list_head list;
}zwave_dev_info_t;

typedef struct _zwave_security_scheme
{
    const char *name;
    int scheme;
    int priority;
}zwave_security_scheme_t; 

typedef struct zwave_command_response
{
    char method[SIZE_64B];
    char nodeid[SIZE_64B];
    char command_class[SIZE_64B];
    char command[SIZE_64B];
    char value[SIZE_64B];
    char *data0;
    char *data1;
    char *data2;
    char data3[8];
    char data4[8];
    char data5[8];
    char hash[SIZE_256B];
    char *compliantJson;

/*shoud change value after calling hash function*/
    int priority;//for sort command in queue
    int cmdId; //equal totalCmd this time insert to queue.
    int cmdType; //to easy to distingush command,  0 default, no need to compare
    int cmdValue;
    uint16_t nodeId;
}zwave_command_response_t; 

typedef struct _zwave_adding_infor
{
    char *owner;
    char deviceName[SIZE_256B];
    char *deviceMode;
    char *typeBasic;
    char *isZwavePlus;
    char *roleType;
    char *nodeType;
    char *active;
    char *alexa;
}zwave_adding_infor;

void parsing(uint8_t cmdClass, uint8_t cmd, const char *string, uint8_t *pData, uint8_t *pDataLength);
unsigned int HslToRgb(double h, double sl, double l, unsigned char * R,unsigned char * G, unsigned char * B);
void SwapBytes(void *pv, size_t n);
uint32_t htoi (const char *ptr);
int expo(int n);

zwave_dev_info_t *get_zwave_dev_from_id(char *id);
zwave_dev_info_t *create_zwave_dev_from_id(char *id);
int _set_specification_prepare(zwave_command_response_t response);
int _get_specification_prepare(zwave_command_response_t response);
int _get_binary_prepare(zwave_command_response_t response);
int _set_binary_prepare(zwave_command_response_t response);

char* GetThermostatModeTypeStr(uint8_t mode);
char *get_zwave_controller_version(void);
int zwave_update_firmwre(char *path);
void update_zwave_dev_from_id(char *id, char *input, char *value);
void convert_capability_to_string(uint8_t cap[], uint8_t capNum, int scheme,
                                int *multichannel_sec, int *configuration_sec, int *association_sec,
                                char *output, size_t length);
void convert_endpoint_to_string(uint8_t endpoint[], uint8_t enpointNum, char *output);
void get_capability(NOTIFY_TX_BUFFER_T pTxNotify, char *nonSecureCap, char *secCap, char *sec2Cap, 
                    int *multichannel_sec, int *configuration_sec, int *association_sec, size_t length);
#endif