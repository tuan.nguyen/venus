#ifndef _DEV_STATE_H_
#define _DEV_STATE_H_

#define ACCUMULATED  1
#define INSTANT      0

#define HAS_PARAM  1
#define NO_PARAM   0

#define ST_ALARM_PARAM_OK                               "Ok"
#define ST_ALARM_PARAM_FAILED                           "Failed"
#define ST_ALARM_PARAM_NO_DATA                          "NoData"
#define ST_ALARM_PARAM_BELOW_LOW_THRESHOLD              "LowThreshold"
#define ST_ALARM_PARAM_ABOVE_HIGH_THRESHOLD             "HighThreshold"
#define ST_ALARM_PARAM_MAX                              "Max"
#define ST_ALARM_PARAM_COMPLETE                         "Completed"
#define ST_ALARM_PARAM_PERFORMING                       "Performing"
#define ST_ALARM_PARAM_DISABLE                          "Disable"
#define ST_ALARM_PARAM_ENABLE                           "Enable"
#define ST_ALARM_PARAM_NO_OBSTRUCTION                   "NoObstruction"
#define ST_ALARM_PARAM_OBSTRUCTION                      "Obstruction"
#define ST_ALARM_PARAM_CLEAN                            "Clean"
#define ST_ALARM_PARAM_SLIGHTLY_POLLUTED                "SlightlyPolluted"
#define ST_ALARM_PARAM_MODERATELY_POLLUTED              "ModeratelyPolluted"
#define ST_ALARM_PARAM_HIGHLY_POLLUTED                  "HighlyPolluted"
#define ST_ALARM_PARAM_OFF                              "Off"
#define ST_ALARM_PARAM_ON                               "On"
/*notification type value*/
#define ALARM_GENERIC_RESERVED                           0x00
#define ALARM_GENERIC_SMOKE_ALARM                        0x01
#define ALARM_GENERIC_CO_ALARM                           0x02
#define ALARM_GENERIC_CO2_ALARM                          0x03
#define ALARM_GENERIC_HEAT_ALARM                         0x04
#define ALARM_GENERIC_WATER_ALARM                        0x05
#define ALARM_GENERIC_ACCESS_CONTROL                     0x06
#define ALARM_GENERIC_HOME_SECURITY                      0x07
#define ALARM_GENERIC_POWER_MANAGEMENT                   0x08
#define ALARM_GENERIC_SYSTEM                             0x09
#define ALARM_GENERIC_EMERGENCY_ALARM                    0x0A
#define ALARM_GENERIC_CLOCK                              0x0B
#define ALARM_GENERIC_APPLIANCE                          0x0C
#define ALARM_GENERIC_HOME_HEALTH                        0x0D
#define ALARM_GENERIC_SIREN                              0x0E
#define ALARM_GENERIC_WATER_VALVE                        0x0F
#define ALARM_GENERIC_WEATHER_ALARM                      0x10
#define ALARM_GENERIC_IRRIGATION                         0x11
#define ALARM_GENERIC_GAS_ALARM                          0x12
#define ALARM_GENERIC_PEST_CONTROL                       0x13
#define ALARM_GENERIC_REQUEST_PENDING                    0xFF

/*notification type name*/
#define ST_ALARM_GENERIC_RESERVED                                    "unknown"
#define ST_ALARM_GENERIC_SMOKE_ALARM                                 "smokeAlarm"
#define ST_ALARM_GENERIC_CO_ALARM                                    "coAlarm"
#define ST_ALARM_GENERIC_CO2_ALARM                                   "co2Alarm"
#define ST_ALARM_GENERIC_HEAT_ALARM                                  "heatAlarm"
#define ST_ALARM_GENERIC_WATER_ALARM                                 "waterAlarm"
#define ST_ALARM_GENERIC_ACCESS_CONTROL                              "accessControl"
#define ST_ALARM_GENERIC_HOME_SECURITY                               "homeSecurity"
#define ST_ALARM_GENERIC_POWER_MANAGEMENT                            "powerManagement"
#define ST_ALARM_GENERIC_SYSTEM                                      "system"
#define ST_ALARM_GENERIC_EMERGENCY_ALARM                             "emergencyAlarm"
#define ST_ALARM_GENERIC_CLOCK                                       "clock"
#define ST_ALARM_GENERIC_APPLIANCE                                   "appliance"
#define ST_ALARM_GENERIC_HOME_HEALTH                                 "homeHealth"
#define ST_ALARM_GENERIC_SIREN                                       "siren"
#define ST_ALARM_GENERIC_WATER_VALVE                                 "waterValve"
#define ST_ALARM_GENERIC_WEATHER_ALARM                               "weatherAlarm"
#define ST_ALARM_GENERIC_IRRIGATION                                  "irrigation"
#define ST_ALARM_GENERIC_GAS_ALARM                                   "gasAlarm"
#define ST_ALARM_GENERIC_PEST_CONTROL                                "pestControl"
#define ST_ALARM_GENERIC_REQUEST_PENDING                             "requestPendingNotification"

#define ALARM_STATE_IDLE                                        0x00
#define ALARM_UNKHOWN_EVENT_STATE                               0xFE
#define ST_ALARM_STATE_IDLE                                     "stateIdle"
#define ST_ALARM_UNKHOWN_EVENT_STATE                            "unknown"

#define ALARM_SMOKE_STATE_IDLE                                  0x00
#define ALARM_SMOKE_DETECTED_LOCATION                           0x01
#define ALARM_SMOKE_DETECTED                                    0x02
#define ALARM_SMOKE_ALARM_TEST                                  0x03
#define ALARM_SMOKE_REPLACEMENT_REQUIRED                        0x04
#define ALARM_SMOKE_REPLACEMENT_REQUIRED_EOL                    0x05
#define ALARM_SMOKE_ALARM_SILENCED                              0x06
#define ALARM_SMOKE_MAINTENANCE_REQUIRED_PPI                    0x07
#define ALARM_SMOKE_MAINTENANCE_REQUIRED_DID                    0x08
#define ALARM_SMOKE_UNKHOWN_EVENT_STATE                         0xFE
#define ST_ALARM_SMOKE_STATE_IDLE                               "smokeIdle"
#define ST_ALARM_SMOKE_DETECTED_LOCATION                        "smokeDetected"
#define ST_ALARM_SMOKE_DETECTED                                 "smokeDetected"
#define ST_ALARM_SMOKE_ALARM_TEST                               "smokeAlarmTest"
#define ST_ALARM_SMOKE_REPLACEMENT_REQUIRED                     "smokeReplaceRequired"
#define ST_ALARM_SMOKE_REPLACEMENT_REQUIRED_EOL                 "smokeReplaceRequiredEOL"
#define ST_ALARM_SMOKE_ALARM_SILENCED                           "smokeAlarmSilenced"
#define ST_ALARM_SMOKE_MAINTENANCE_REQUIRED_PPI                 "smokeMaintenanceRequiredPPI"
#define ST_ALARM_SMOKE_MAINTENANCE_REQUIRED_DID                 "smokeMaintenanceRequiredDID"
#define ST_ALARM_SMOKE_UNKHOWN_EVENT_STATE                      "smokeUnknown"

#define ALARM_CO_STATE_IDLE                                     0x00
#define ALARM_CO_DETECTED_LOCATION                              0x01
#define ALARM_CO_DETECTED                                       0x02
#define ALARM_CO_TEST                                           0x03
#define ALARM_CO_REPLACEMENT_REQUIRED                           0x04
#define ALARM_CO_REPLACEMENT_REQUIRED_EOL                       0x05
#define ALARM_CO_ALARM_SILENCED                                 0x06
#define ALARM_CO_MAINTENANCE_REQUIRED_PPI                       0x07
#define ALARM_CO_UNKHOWN_EVENT_STATE                            0xFE
#define ST_ALARM_CO_STATE_IDLE                                  "COIdle"
#define ST_ALARM_CO_DETECTED_LOCATION                           "CODetected"
#define ST_ALARM_CO_DETECTED                                    "CODetected"
#define ST_ALARM_CO_TEST                                        "COTest"
#define ST_ALARM_CO_TEST_OK                                     "COTest"ST_ALARM_PARAM_OK
#define ST_ALARM_CO_TEST_FAILED                                 "COTest"ST_ALARM_PARAM_FAILED
#define ST_ALARM_CO_REPLACEMENT_REQUIRED                        "COReplaceRequired"
#define ST_ALARM_CO_REPLACEMENT_REQUIRED_EOL                    "COReplaceRequiredEOL"
#define ST_ALARM_CO_ALARM_SILENCED                              "COAlarmSilenced"
#define ST_ALARM_CO_MAINTENANCE_REQUIRED_PPI                    "COMaintenanceRequiredPPI"
#define ST_ALARM_CO_UNKHOWN_EVENT_STATE                         "COUnknown"

#define ALARM_CO2_STATE_IDLE                                    0x00
#define ALARM_CO2_DETECTED_LOCATION                             0x01
#define ALARM_CO2_DETECTED                                      0x02
#define ALARM_CO2_TEST                                          0x03
#define ALARM_CO2_REPLACEMENT_REQUIRED                          0x04
#define ALARM_CO2_REPLACEMENT_REQUIRED_EOL                      0x05
#define ALARM_CO2_ALARM_SILENCED                                0x06
#define ALARM_CO2_MAINTENANCE_REQUIRED_PPI                      0x07
#define ALARM_CO2_UNKHOWN_EVENT_STATE                           0xFE
#define ST_ALARM_CO2_STATE_IDLE                                 "CO2Idle"
#define ST_ALARM_CO2_DETECTED_LOCATION                          "CO2Detected"
#define ST_ALARM_CO2_DETECTED                                   "CO2Detected"
#define ST_ALARM_CO2_TEST                                       "CO2Test"
#define ST_ALARM_CO2_TEST_OK                                    "CO2Test"ST_ALARM_PARAM_OK
#define ST_ALARM_CO2_TEST_FAILED                                "CO2Test"ST_ALARM_PARAM_FAILED
#define ST_ALARM_CO2_REPLACEMENT_REQUIRED                       "CO2ReplaceRequired"
#define ST_ALARM_CO2_REPLACEMENT_REQUIRED_EOL                   "CO2ReplaceRequiredEOL"
#define ST_ALARM_CO2_ALARM_SILENCED                             "CO2AlarmSilenced"
#define ST_ALARM_CO2_MAINTENANCE_REQUIRED_PPI                   "CO2MaintenanceRequiredPPI"
#define ST_ALARM_CO2_UNKHOWN_EVENT_STATE                        "CO2Unknown"

#define ALARM_HEAT_STATE_IDLE                                   0x00
#define ALARM_HEAT_OVERHEAT_DETECTED_LOCATION                   0x01
#define ALARM_HEAT_OVERHEAT_DETECTED                            0x02
#define ALARM_HEAT_UNDER_HEAT_DETECTED_LOCATION                 0x05
#define ALARM_HEAT_UNDER_HEAT_DETECTED                          0x06
#define ALARM_HEAT_RAPID_TEMPERATURE_RISE_LOCATION              0x03
#define ALARM_HEAT_RAPID_TEMPERATURE_RISE                       0x04
#define ALARM_HEAT_RAPID_TEMPERATURE_FALL_LOCATION              0x0C
#define ALARM_HEAT_RAPID_TEMPERATURE_FALL                       0x0D
#define ALARM_HEAT_ALARM_TEST                                   0x07
#define ALARM_HEAT_ALARM_SILENCED                               0x09
#define ALARM_HEAT_REPLACEMENT_HEAT_REQUIRED_EOL                0x08
#define ALARM_HEAT_MAINTENANCE_HEAT_REQUIRED_DID                0x0A
#define ALARM_HEAT_MAINTENANCE_HEAT_REQUIRED_PPI                0x0B
#define ALARM_HEAT_UNKHOWN_EVENT_STATE                          0xFE
#define ST_ALARM_HEAT_STATE_IDLE                                "heatIdle"
#define ST_ALARM_HEAT_OVERHEAT_DETECTED_LOCATION                "overheatDetected"
#define ST_ALARM_HEAT_OVERHEAT_DETECTED                         "overheatDetected"
#define ST_ALARM_HEAT_UNDER_HEAT_DETECTED_LOCATION              "underheatDetected"
#define ST_ALARM_HEAT_UNDER_HEAT_DETECTED                       "underheatDetected"
#define ST_ALARM_HEAT_RAPID_TEMPERATURE_RISE_LOCATION           "rapidTempRise"
#define ST_ALARM_HEAT_RAPID_TEMPERATURE_RISE                    "rapidTempRise"
#define ST_ALARM_HEAT_RAPID_TEMPERATURE_FALL_LOCATION           "rapidTempFall"
#define ST_ALARM_HEAT_RAPID_TEMPERATURE_FALL                    "rapidTempFall"
#define ST_ALARM_HEAT_ALARM_TEST                                "heatAlarmTest"
#define ST_ALARM_HEAT_REPLACEMENT_REQUIRED                      "heatReplaceRequired"
#define ST_ALARM_HEAT_REPLACEMENT_REQUIRED_EOL                  "heatReplaceRequiredEOL"
#define ST_ALARM_HEAT_ALARM_SILENCED                            "heatAlarmSilenced"
#define ST_ALARM_HEAT_MAINTENANCE_REQUIRED_PPI                  "heatMaintenanceRequiredPPI"
#define ST_ALARM_HEAT_MAINTENANCE_REQUIRED_DID                  "heatMaintenanceRequiredDID"
#define ST_ALARM_HEAT_UNKHOWN_EVENT_STATE                       "heatUnknown"

#define ALARM_WATER_STATE_IDLE                                  0x00
#define ALARM_WATER_LEAK_DETECTED_LOCATION                      0x01
#define ALARM_WATER_LEAK_DETECTED                               0x02
#define ALARM_WATER_LEVEL_DROPPED_LOCATION                      0x03
#define ALARM_WATER_LEVEL_DROPPED                               0x04
#define ALARM_WATER_REPLACE_WATER_FILTER                        0x05
#define ALARM_WATER_FLOW_ALARM                                  0x06
#define ALARM_WATER_PRESSURE_ALARM                              0x07
#define ALARM_WATER_TEMPERATURE_ALARM                           0x08
#define ALARM_WATER_LEVEL_ALARM                                 0x09
#define ALARM_WATER_SUMP_PUMP_ACTIVE                            0x0A
#define ALARM_WATER_SUMP_PUMP_FAILURE                           0x0B
#define ALARM_WATER_UNKHOWN_EVENT_STATE                         0xFE
#define ST_ALARM_WATER_STATE_IDLE                               "waterIdle"
#define ST_ALARM_WATER_LEAK_DETECTED_LOCATION                   "waterLeakDetected"
#define ST_ALARM_WATER_LEAK_DETECTED                            "waterLeakDetected"
#define ST_ALARM_WATER_LEVEL_DROPPED_LOCATION                   "waterLevelDropped"
#define ST_ALARM_WATER_LEVEL_DROPPED                            "waterLevelDropped"
#define ST_ALARM_WATER_REPLACE_WATER_FILTER                     "replaceWaterFilter"
#define ST_ALARM_WATER_FLOW_ALARM                               "waterFlowAlarm"
#define ST_ALARM_WATER_FLOW_ALARM_NODATA                        "waterFlowAlarm"ST_ALARM_PARAM_NO_DATA
#define ST_ALARM_WATER_FLOW_ALARM_LOW_THRESHOLD                 "waterFlowAlarm"ST_ALARM_PARAM_BELOW_LOW_THRESHOLD
#define ST_ALARM_WATER_FLOW_ALARM_HIGH_THRESHOLD                "waterFlowAlarm"ST_ALARM_PARAM_ABOVE_HIGH_THRESHOLD
#define ST_ALARM_WATER_FLOW_ALARM_MAX                           "waterFlowAlarm"ST_ALARM_PARAM_MAX
#define ST_ALARM_WATER_PRESSURE_ALARM                           "waterPressureAlarm"
#define ST_ALARM_WATER_PRESSURE_ALARM_NODATA                    "waterPressureAlarm"ST_ALARM_PARAM_NO_DATA
#define ST_ALARM_WATER_PRESSURE_ALARM_LOW_THRESHOLD             "waterPressureAlarm"ST_ALARM_PARAM_BELOW_LOW_THRESHOLD
#define ST_ALARM_WATER_PRESSURE_ALARM_HIGH_THRESHOLD            "waterPressureAlarm"ST_ALARM_PARAM_ABOVE_HIGH_THRESHOLD
#define ST_ALARM_WATER_PRESSURE_ALARM_MAX                       "waterPressureAlarm"ST_ALARM_PARAM_MAX
#define ST_ALARM_WATER_TEMPERATURE_ALARM                        "waterTempAlarm"
#define ST_ALARM_WATER_TEMPERATURE_ALARM_NODATA                 "waterTempAlarm"ST_ALARM_PARAM_NO_DATA
#define ST_ALARM_WATER_TEMPERATURE_ALARM_LOW_THRESHOLD          "waterTempAlarm"ST_ALARM_PARAM_BELOW_LOW_THRESHOLD
#define ST_ALARM_WATER_TEMPERATURE_ALARM_HIGH_THRESHOLD         "waterTempAlarm"ST_ALARM_PARAM_ABOVE_HIGH_THRESHOLD
#define ST_ALARM_WATER_LEVEL_ALARM                              "waterLevelAlarm"
#define ST_ALARM_WATER_LEVEL_ALARM_NODATA                       "waterLevelAlarm"ST_ALARM_PARAM_NO_DATA
#define ST_ALARM_WATER_LEVEL_ALARM_LOW_THRESHOLD                "waterLevelAlarm"ST_ALARM_PARAM_BELOW_LOW_THRESHOLD
#define ST_ALARM_WATER_LEVEL_ALARM_HIGH_THRESHOLD               "waterLevelAlarm"ST_ALARM_PARAM_ABOVE_HIGH_THRESHOLD
#define ST_ALARM_WATER_SUMP_PUMP_ACTIVE                         "waterSumpPumpActive"
#define ST_ALARM_WATER_SUMP_PUMP_FAILURE                        "waterSumpPumpFailure"
#define ST_ALARM_WATER_UNKHOWN_EVENT_STATE                      "waterUnknown"

#define ALARM_AC_STATE_IDLE                                     0x00
#define ALARM_AC_MANUAL_LOCK_OPERATION                          0x01
#define ALARM_AC_MANUAL_UNLOCK_OPERATION                        0x02
#define ALARM_AC_RF_LOCK_OPERATION                              0x03
#define ALARM_AC_RF_UNLOCK_OPERATION                            0x04
#define ALARM_AC_KEYPAD_LOCK_OPERATION                          0x05
#define ALARM_AC_KEYPAD_UNLOCK_OPERATION                        0x06
#define ALARM_AC_MANUAL_NOT_FULLY_LOCKED_OPERATION              0x07
#define ALARM_AC_RF_NOT_FULLY_LOCKED_OPERATION                  0x08
#define ALARM_AC_AUTO_LOCK_LOCKED_OPERATION                     0x09
#define ALARM_AC_AUTO_LOCK_NOT_FULLY_LOCKED_OPERATION           0x0A
#define ALARM_AC_LOCK_JAMMED                                    0x0B
#define ALARM_AC_ALL_USER_CODE_DELETED                          0x0C
#define ALARM_AC_SINGLE_USER_CODE_DELETED                       0x0D
#define ALARM_AC_NEW_USER_CODE_ADDED                            0x0E
#define ALARM_AC_NEW_USER_CODE_NOT_ADDED                        0x0F
#define ALARM_AC_KEYPAD_TEMPORARY_DISABLED                      0x10
#define ALARM_AC_KEYPAD_BUSY                                    0x11
#define ALARM_AC_NEW_PROGRAM_CODE_ENTERED                       0x12
#define ALARM_AC_MANUALY_ENTER_USER_ACCESS                      0x13
#define ALARM_AC_UNLOCK_RF_INVALID_USER_CODE                    0x14
#define ALARM_AC_LOCKED_RF_INVALID_USER_CODE                    0x15
#define ALARM_AC_WINDOW_DOOR_OPEN                               0x16
#define ALARM_AC_WINDOW_DOOR_CLOSED                             0x17
#define ALARM_AC_WINDOW_DOOR_HANDLE_OPEN                        0x18
#define ALARM_AC_WINDOW_DOOR_HANDLE_CLOSED                      0x19
#define ALARM_AC_BARRIER_PERFORMING_INIT_PROCESS                0x40
#define ALARM_AC_BARRIER_OPERATION_FORCE_EXCEEDED               0x41
#define ALARM_AC_BARRIER_MOTOR_EXCEEDED                         0x42
#define ALARM_AC_BARRIER_OPERATION_PHYSICAL_EXCEEDED            0x43
#define ALARM_AC_BARRIER_UNABLE_PERFORM_REQUEST                 0x44
#define ALARM_AC_BARRIER_UNATTENDED_DISABLED                    0x45
#define ALARM_AC_BARRIER_MALFUNCTION                            0x46
#define ALARM_AC_BARRIER_VACATION_MODE                          0x47
#define ALARM_AC_BARRIER_SAFETY_BEAM_OBSTACLE                   0x48
#define ALARM_AC_BARRIER_SENSOR_NOT_DETECTED                    0x49
#define ALARM_AC_BARRIER_SENSOR_LOW_BATTERY_WARNING             0x4A
#define ALARM_AC_BARRIER_DETECTED_SHORT                         0x4B
#define ALARM_AC_BARRIER_ASSOCIATED_WITH_NON_ZWAVE_REMOTE       0x4C
#define ALARM_AC_UNKHOWN_EVENT_STATE                            0xFE
#define ST_ALARM_AC_STATE_IDLE                                  "accessControlIdle"
#define ST_ALARM_AC_MANUAL_LOCK_OPERATION                       "manualLock"
#define ST_ALARM_AC_MANUAL_UNLOCK_OPERATION                     "manualUnlock"
#define ST_ALARM_AC_RF_LOCK_OPERATION                           "rfLock"
#define ST_ALARM_AC_RF_UNLOCK_OPERATION                         "rfUnlock"
#define ST_ALARM_AC_KEYPAD_LOCK_OPERATION                       "keypadLock"
#define ST_ALARM_AC_KEYPAD_UNLOCK_OPERATION                     "keypadUnlock"
#define ST_ALARM_AC_MANUAL_NOT_FULLY_LOCKED_OPERATION           "manualNotFullyLocked"
#define ST_ALARM_AC_RF_NOT_FULLY_LOCKED_OPERATION               "rfNotFullyLocked"
#define ST_ALARM_AC_AUTO_LOCK_LOCKED_OPERATION                  "autoLocked"
#define ST_ALARM_AC_AUTO_LOCK_NOT_FULLY_LOCKED_OPERATION        "autoLockNotFullyLocked"
#define ST_ALARM_AC_LOCK_JAMMED                                 "lockJammed"
#define ST_ALARM_AC_ALL_USER_CODE_DELETED                       "allUserCodeDeleted"
#define ST_ALARM_AC_SINGLE_USER_CODE_DELETED                    "singleUserCodeDeleted"
#define ST_ALARM_AC_NEW_USER_CODE_ADDED                         "userCodeAdded"
#define ST_ALARM_AC_NEW_USER_CODE_NOT_ADDED                     "duplicateUserCode"
#define ST_ALARM_AC_KEYPAD_TEMPORARY_DISABLED                   "keypadTemporaryDisabled"
#define ST_ALARM_AC_KEYPAD_BUSY                                 "keypadBusy"
#define ST_ALARM_AC_NEW_PROGRAM_CODE_ENTERED                    "newProgramCodeEnteredUniqueCodeforLockConfiguration"
#define ST_ALARM_AC_MANUALY_ENTER_USER_ACCESS_CODE_EXCEEDED     "manualTryAccessLimited"
#define ST_ALARM_AC_UNLOCK_RF_INVALID_USER_CODE                 "rfUnlockInvalidUserCode"
#define ST_ALARM_AC_LOCKED_RF_INVALID_USER_CODE                 "rfLockedInvalidUserCode"
#define ST_ALARM_AC_WINDOW_DOOR_OPEN                            "doorOpen"
#define ST_ALARM_AC_WINDOW_DOOR_CLOSED                          "doorClosed"
#define ST_ALARM_AC_WINDOW_DOOR_HANDLE_OPEN                     "doorHandleOpen"
#define ST_ALARM_AC_WINDOW_DOOR_HANDLE_CLOSED                   "doorHandleClosed"
#define ST_ALARM_AC_BARRIER_PERFORMING_INIT_PROCESS             "barrierPerforInitProcess" 
#define ST_ALARM_AC_BARRIER_PERFORMING_INIT_PROCESS_PERFORMING  "barrierPerforInitProcess"ST_ALARM_PARAM_PERFORMING 
#define ST_ALARM_AC_BARRIER_PERFORMING_INIT_PROCESS_COMPLETED   "barrierPerforInitProcess"ST_ALARM_PARAM_COMPLETE
#define ST_ALARM_AC_BARRIER_OPERATION_FORCE_EXCEEDED            "barrierOperationExceeded"
#define ST_ALARM_AC_BARRIER_MOTOR_EXCEEDED                      "barrierMotorExceeded"
#define ST_ALARM_AC_BARRIER_OPERATION_PHYSICAL_EXCEEDED         "barrierPhysicalExceeded"
#define ST_ALARM_AC_BARRIER_UNABLE_PERFORM_REQUEST              "barrierUnablePerformRequest"
#define ST_ALARM_AC_BARRIER_UNATTENDED_DISABLED                 "barrierUnattendedDisabled"
#define ST_ALARM_AC_BARRIER_MALFUNCTION                         "barrierMalfunction"
#define ST_ALARM_AC_BARRIER_VACATION_MODE                       "barrierVacationMode"
#define ST_ALARM_AC_BARRIER_VACATION_MODE_ENABLE                "barrierVacationMode"ST_ALARM_PARAM_ENABLE
#define ST_ALARM_AC_BARRIER_VACATION_MODE_DISABLE               "barrierVacationMode"ST_ALARM_PARAM_DISABLE
#define ST_ALARM_AC_BARRIER_SAFETY_BEAM_OBSTACLE                "barrierSafetyBeamObstacle"
#define ST_ALARM_AC_BARRIER_SAFETY_BEAM_OBSTACLE_NO_OBSTRUCTION "barrierSafetyBeamObstacle"ST_ALARM_PARAM_NO_OBSTRUCTION
#define ST_ALARM_AC_BARRIER_SAFETY_BEAM_OBSTACLE_OBSTRUCTION    "barrierSafetyBeamObstacle"ST_ALARM_PARAM_OBSTRUCTION
#define ST_ALARM_AC_BARRIER_SENSOR_NOT_DETECTED                 "barrierSensorNotDetected"
#define ST_ALARM_AC_BARRIER_SENSOR_LOW_BATTERY_WARNING          "barrierSensorLowBatteryWarning"
#define ST_ALARM_AC_BARRIER_DETECTED_SHORT                      "barrierShortDetected"
#define ST_ALARM_AC_BARRIER_ASSOCIATED_WITH_NON_ZWAVE_REMOTE    "barrierAssociatedWithNonZWaveRemoteControl"
#define ST_ALARM_AC_UNKHOWN_EVENT_STATE                         "accessControlUnknown"

#define ALARM_HOME_STATE_IDLE                                   0x00
#define ALARM_HOME_INTRUSION_LOCATION                           0x01
#define ALARM_HOME_INTRUSION                                    0x02
#define ALARM_HOME_TEMPERATING_PRODUCT_REMOVE                   0x03
#define ALARM_HOME_TEMPERATING_INVALID_CODE                     0x04
#define ALARM_HOME_GLASS_BREAKAGE_LOCATION                      0x05
#define ALARM_HOME_GLASS_BREAKAGE                               0x06
#define ALARM_HOME_MOTION_DETECTION_LOCATION                    0x07
#define ALARM_HOME_MOTION_DETECTION                             0x08
#define ALARM_HOME_TEMPERATING_MOVED                            0x09
#define ALARM_HOME_UNKHOWN_EVENT_STATE                          0xFE
#define ST_ALARM_HOME_STATE_IDLE                                "homeSecurityIdle"
#define ST_ALARM_HOME_INTRUSION_LOCATION                        "intrusion"
#define ST_ALARM_HOME_INTRUSION                                 "intrusion"
#define ST_ALARM_HOME_TEMPERATING_PRODUCT_REMOVE                "tamperingProductCoverRemoved"
#define ST_ALARM_HOME_TEMPERATING_INVALID_CODE                  "tamperingInvalidCode"
#define ST_ALARM_HOME_GLASS_BREAKAGE_LOCATION                   "glassBreakage"
#define ST_ALARM_HOME_GLASS_BREAKAGE                            "glassBreakage"
#define ST_ALARM_HOME_MOTION_DETECTION_LOCATION                 "motionDetected"
#define ST_ALARM_HOME_MOTION_DETECTION                          "motionDetected"
#define ST_ALARM_HOME_TEMPERATING_MOVED                         "tamperingProductMoved"
#define ST_ALARM_HOME_UNKHOWN_EVENT_STATE                       "homeSecurityUnknown"

#define ALARM_PM_STATE_IDLE                                     0x00
#define ALARM_PM_POWER_APPLIED                                  0x01
#define ALARM_PM_AC_DISCONNECTED                                0x02
#define ALARM_PM_AC_REDISCONNECT                                0x03
#define ALARM_PM_SURGE_DETECTED                                 0x04
#define ALARM_PM_VOTAGE_DROP_DRIFT                              0x05
#define ALARM_PM_OVERCURRENT_DETECTED                           0x06
#define ALARM_PM_OVERVOTAGE_DETECTED                            0x07
#define ALARM_PM_OVERLOAD_DETECTED                              0x08
#define ALARM_PM_LOAD_ERROR                                     0x09
#define ALARM_PM_REPLACE_BATTERY_SOON                           0x0A
#define ALARM_PM_REPLACE_BATTERY_NOW                            0x0B
#define ALARM_PM_BATTERY_FLUID_LOW                              0x11
#define ALARM_PM_BATTERY_CHARGING                               0x0C
#define ALARM_PM_BATTERY_FULLY_CHARGED                          0x0D
#define ALARM_PM_CHARGE_BATTERY_SOON                            0x0E
#define ALARM_PM_CHARGE_BATTERY_NOW                             0x0F
#define ALARM_PM_BACKUP_BATTERY_LOW                             0x10
#define ALARM_PM_UNKHOWN_EVENT_STATE                            0xFE
#define ST_ALARM_PM_STATE_IDLE                                  "powerManagementIdle"
#define ST_ALARM_PM_POWER_APPLIED                               "powerApplied"
#define ST_ALARM_PM_AC_DISCONNECTED                             "ACDisconnected"
#define ST_ALARM_PM_AC_REDISCONNECT                             "ACReconnected"
#define ST_ALARM_PM_SURGE_DETECTED                              "surgeDetected"
#define ST_ALARM_PM_VOTAGE_DROP_DRIFT                           "voltageDrop"
#define ST_ALARM_PM_OVERCURRENT_DETECTED                        "overCurrentDetected"
#define ST_ALARM_PM_OVERVOTAGE_DETECTED                         "overVoltageDetected"
#define ST_ALARM_PM_OVERLOAD_DETECTED                           "overLoadDetected"
#define ST_ALARM_PM_LOAD_ERROR                                  "loadError"
#define ST_ALARM_PM_REPLACE_BATTERY_SOON                        "replaceBatterySoon"
#define ST_ALARM_PM_REPLACE_BATTERY_NOW                         "replaceBatteryNow"
#define ST_ALARM_PM_BATTERY_FLUID_LOW                           "batteryFluidIsLow"
#define ST_ALARM_PM_BATTERY_CHARGING                            "batteryIsCharging"
#define ST_ALARM_PM_BATTERY_FULLY_CHARGED                       "batteryIsFullyCharged"
#define ST_ALARM_PM_CHARGE_BATTERY_SOON                         "chargeBatterySoon"
#define ST_ALARM_PM_CHARGE_BATTERY_NOW                          "chargeBatteryNow"
#define ST_ALARM_PM_BACKUP_BATTERY_LOW                          "backupBatteryIsLow"
#define ST_ALARM_PM_UNKHOWN_EVENT_STATE                         "powerManagementUnknown"

#define ALARM_SYSTEM_STATE_IDLE                                 0x00
#define ALARM_SYSTEM_HARDWARE_FAILLURE                          0x01
#define ALARM_SYSTEM_HARDWARE_FAILLURE_MANUFACTUER              0x03
#define ALARM_SYSTEM_SOFTWARE_FAILURE                           0x02
#define ALARM_SYSTEM_SOFTWARE_FAILURE_MANUFACTURE               0x04
#define ALARM_SYSTEM_HEART_BEAT                                 0x05
#define ALARM_SYSTEM_TEMPERATING_PRODUCT_REMOVE                 0x06
#define ALARM_SYSTEM_EMERGENCY_SHUTOFF                          0x07
#define ALARM_SYSTEM_UNKHOWN_EVENT_STATE                        0xFE
#define ST_ALARM_SYSTEM_STATE_IDLE                              "systemIdle"
#define ST_ALARM_SYSTEM_HARDWARE_FAILLURE                       "hardwareFailure"
#define ST_ALARM_SYSTEM_HARDWARE_FAILLURE_MANUFACTUER           "hardwareFailure"
#define ST_ALARM_SYSTEM_SOFTWARE_FAILURE                        "softwareFailure"
#define ST_ALARM_SYSTEM_SOFTWARE_FAILURE_MANUFACTURE            "softwareFailure"
#define ST_ALARM_SYSTEM_HEART_BEAT                              "heartbeat"
#define ST_ALARM_SYSTEM_TEMPERATING_PRODUCT_REMOVE              "tamperingProductCoverRemoved"
#define ST_ALARM_SYSTEM_EMERGENCY_SHUTOFF                       "emergencyShutoff"
#define ST_ALARM_SYSTEM_UNKHOWN_EVENT_STATE                     "systemUnknown"

#define ALARM_EMERGENCY_STATE_IDLE                              0x00
#define ALARM_EMERGENCY_CONTACT_POLICE                          0x01
#define ALARM_EMERGENCY_CONTACT_FIRE_SERVICE                    0x02
#define ALARM_EMERGENCY_CONTACT_MEDICAL_SERVICE                 0x03
#define ALARM_EMERGENCY_UNKHOWN_EVENT_STATE                     0xFE
#define ST_ALARM_EMERGENCY_STATE_IDLE                           "emergencyIdle"
#define ST_ALARM_EMERGENCY_CONTACT_POLICE                       "contactPolice"
#define ST_ALARM_EMERGENCY_CONTACT_FIRE_SERVICE                 "contactFireService"
#define ST_ALARM_EMERGENCY_CONTACT_MEDICAL_SERVICE              "contactMedicalService"
#define ST_ALARM_EMERGENCY_UNKHOWN_EVENT_STATE                  "emergencyUnknown"

#define ALARM_CLOCK_STATE_IDLE                                  0x00
#define ALARM_CLOCK_WAKE_UP_ALERT                               0x01
#define ALARM_CLOCK_TIMER_ENDED                                 0x02
#define ALARM_CLOCK_TIME_REMAINING                              0x03
#define ALARM_CLOCK_UNKHOWN_EVENT_STATE                         0xFE
#define ST_ALARM_CLOCK_STATE_IDLE                               "clockIdle"
#define ST_ALARM_CLOCK_WAKE_UP_ALERT                            "wakeUpAlert"
#define ST_ALARM_CLOCK_TIMER_ENDED                              "timerEnded"
#define ST_ALARM_CLOCK_TIME_REMAINING                           "timeRemaining"
#define ST_ALARM_CLOCK_UNKHOWN_EVENT_STATE                      "clockUnknown"

#define ALARM_APP_STATE_IDLE                                    0x00
#define ALARM_APP_PROGRAM_STARTED                               0x01
#define ALARM_APP_PROGRAM_PROGRESS                              0x02
#define ALARM_APP_PROGRAM_COMPLETED                             0x03
#define ALARM_APP_REPLACE_MAIN_FILTER                           0x04
#define ALARM_APP_SUPPLYING_WATER                               0x06
#define ALARM_APP_BOILLING                                      0x08
#define ALARM_APP_WASHING                                       0x0A
#define ALARM_APP_RINSING                                       0x0C
#define ALARM_APP_DRAINING                                      0x0E
#define ALARM_APP_SPINNING                                      0x10
#define ALARM_APP_DRYING                                        0x12
#define ALARM_APP_FAILURE_SET_TARGET_TEMPERATURE                0x05
#define ALARM_APP_WATTER_SUPPLY_FAILURE                         0x07
#define ALARM_APP_BOILLING_FAILURE                              0x09
#define ALARM_APP_WASHING_FAILURE                               0x0B
#define ALARM_APP_RINSING_FAILURE                               0x0D
#define ALARM_APP_DRAINING_FAILURE                              0x0F
#define ALARM_APP_SPINNING_FAILURE                              0x11
#define ALARM_APP_DRYING_FAILURE                                0x13
#define ALARM_APP_FAN_FAILURE                                   0x14
#define ALARM_APP_COMPRESSOR_FAILURE                            0x15
#define ALARM_APP_UNKHOWN_EVENT_STATE                           0xFE
#define ST_ALARM_APP_STATE_IDLE                                 "applianceIdle"
#define ST_ALARM_APP_PROGRAM_STARTED                            "programStarted"
#define ST_ALARM_APP_PROGRAM_PROGRESS                           "programInProgress"
#define ST_ALARM_APP_PROGRAM_COMPLETED                          "programCompleted"
#define ST_ALARM_APP_REPLACE_MAIN_FILTER                        "replaceMainFilter"
#define ST_ALARM_APP_SUPPLYING_WATER                            "supplyingWater"
#define ST_ALARM_APP_BOILLING                                   "boilling"
#define ST_ALARM_APP_WASHING                                    "washing"
#define ST_ALARM_APP_RINSING                                    "rinsing"
#define ST_ALARM_APP_DRAINING                                   "draining"
#define ST_ALARM_APP_SPINNING                                   "spinning"
#define ST_ALARM_APP_DRYING                                     "drying"
#define ST_ALARM_APP_FAILURE_SET_TARGET_TEMPERATURE             "setTempFailed"
#define ST_ALARM_APP_WATTER_SUPPLY_FAILURE                      "waterSupplyFailed"
#define ST_ALARM_APP_BOILLING_FAILURE                           "boilingFailed"
#define ST_ALARM_APP_WASHING_FAILURE                            "washingFailed"
#define ST_ALARM_APP_RINSING_FAILURE                            "rinsingFailed"
#define ST_ALARM_APP_DRAINING_FAILURE                           "drainingFailed"
#define ST_ALARM_APP_SPINNING_FAILURE                           "spinningFailed"
#define ST_ALARM_APP_DRYING_FAILURE                             "dryingFailed"
#define ST_ALARM_APP_FAN_FAILURE                                "fanFailed"
#define ST_ALARM_APP_COMPRESSOR_FAILURE                         "compressorFailed"
#define ST_ALARM_APP_UNKHOWN_EVENT_STATE                        "applianceUnknown"

#define ALARM_HOME_HEALTH_STATE_IDLE                            0x00
#define ALARM_HOME_HEALTH_LEAVING_BED                           0x01 
#define ALARM_HOME_HEALTH_SITTING_ON_BED                        0x02
#define ALARM_HOME_HEALTH_LYING_ON_BED                          0x03
#define ALARM_HOME_HEALTH_SITTING_ON_BED_ADGE                   0x05
#define ALARM_HOME_HEALTH_POSTURE_CHANGED                       0x04
#define ALARM_HOME_HEALTH_VOLATILE_ORGANIC_LEVEL                0x06
#define ALARM_HOME_HEALTH_UNKHOWN_EVENT_STATE                   0xFE
#define ST_ALARM_HOME_HEALTH_STATE_IDLE                         "homeHealthIdle"
#define ST_ALARM_HOME_HEALTH_LEAVING_BED                        "leavingBed" 
#define ST_ALARM_HOME_HEALTH_SITTING_ON_BED                     "sittingOnBed"
#define ST_ALARM_HOME_HEALTH_LYING_ON_BED                       "lyingOnBed"
#define ST_ALARM_HOME_HEALTH_SITTING_ON_BED_ADGE                "sittingOnBedEdge"
#define ST_ALARM_HOME_HEALTH_POSTURE_CHANGED                    "postureChanged"
#define ST_ALARM_HOME_HEALTH_VOLATILE_ORGANIC                   "volatileOrganicCompoundLevel"
#define ST_ALARM_HOME_HEALTH_VOLATILE_ORGANIC_CLEAN             "volatileOrganicCompoundLevel"ST_ALARM_PARAM_CLEAN
#define ST_ALARM_HOME_HEALTH_VOLATILE_ORGANIC_SLIGHTLY          "volatileOrganicCompoundLevel"ST_ALARM_PARAM_SLIGHTLY_POLLUTED
#define ST_ALARM_HOME_HEALTH_VOLATILE_ORGANIC_MODERATELY        "volatileOrganicCompoundLevel"ST_ALARM_PARAM_MODERATELY_POLLUTED
#define ST_ALARM_HOME_HEALTH_VOLATILE_ORGANIC_HIGHLY            "volatileOrganicCompoundLevel"ST_ALARM_PARAM_HIGHLY_POLLUTED
#define ST_ALARM_HOME_HEALTH_UNKHOWN_EVENT_STATE                "homeHealthUnknown"

#define ALARM_SIREN_STATE_IDLE                                  0x00
#define ALARM_SIREN_ACTIVE                                      0x01
#define ALARM_SIREN_UNKHOWN_EVENT_STATE                         0xFE
#define ST_ALARM_SIREN_STATE_IDLE                               "sirenIdle"
#define ST_ALARM_SIREN_ACTIVE                                   "sirenActive"
#define ST_ALARM_SIREN_UNKHOWN_EVENT_STATE                      "sirenUnknown"

#define ALARM_WATER_VALVE_STATE_IDLE                            0x00
#define ALARM_WATER_VALVE_OPERATION                             0x01
#define ALARM_WATER_MASTER_VALVE_OPERATION                      0x02
#define ALARM_WATER_VALVE_SHORT_CIRCUIT                         0x03
#define ALARM_WATER_MASTER_VALVE_SHORT_CIRCUIT                  0x04
#define ALARM_WATER_VALVE_CURRENT_ALARM                         0x05
#define ALARM_WATER_MASTER_VALVE_CURRENT_ALARM                  0x06
#define ALARM_WATER_VALVE_UNKHOWN_EVENT_STATE                   0xFE
#define ST_ALARM_WATER_VALVE_STATE_IDLE                         "waterValveIdle"
#define ST_ALARM_WATER_VALVE_OPERATION                          "valveOperation"
#define ST_ALARM_WATER_VALVE_OPERATION_ON                       "valveOperation"ST_ALARM_PARAM_ON
#define ST_ALARM_WATER_VALVE_OPERATION_OFF                      "valveOperation"ST_ALARM_PARAM_OFF
#define ST_ALARM_WATER_MASTER_VALVE_OPERATION                   "masterValveOperation"
#define ST_ALARM_WATER_MASTER_VALVE_OPERATION_ON                "masterValveOperation"ST_ALARM_PARAM_ON
#define ST_ALARM_WATER_MASTER_VALVE_OPERATION_OFF               "masterValveOperation"ST_ALARM_PARAM_OFF
#define ST_ALARM_WATER_VALVE_SHORT_CIRCUIT                      "valveShortCircuit"
#define ST_ALARM_WATER_MASTER_VALVE_SHORT_CIRCUIT               "masterValveShortCircuit"
#define ST_ALARM_WATER_VALVE_CURRENT_ALARM                      "valveCurrentAlarm"
#define ST_ALARM_WATER_VALVE_CURRENT_ALARM_NODATA               "valveCurrentAlarm"ST_ALARM_PARAM_NO_DATA
#define ST_ALARM_WATER_VALVE_CURRENT_ALARM_LOW_THRESHOLD        "valveCurrentAlarm"ST_ALARM_PARAM_BELOW_LOW_THRESHOLD
#define ST_ALARM_WATER_VALVE_CURRENT_ALARM_HIGH_THRESHOLD       "valveCurrentAlarm"ST_ALARM_PARAM_ABOVE_HIGH_THRESHOLD
#define ST_ALARM_WATER_VALVE_CURRENT_ALARM_MAX                  "valveCurrentAlarm"ST_ALARM_PARAM_MAX
#define ST_ALARM_WATER_MASTER_VALVE_CURRENT_ALARM               "masterValveCurrentAlarm"
#define ST_ALARM_WATER_MASTER_VALVE_CURRENT_ALARM_NODATA        "masterValveCurrentAlarm"ST_ALARM_PARAM_NO_DATA
#define ST_ALARM_WATER_MASTER_VALVE_CURRENT_ALARM_LOW_THRESHOLD "masterValveCurrentAlarm"ST_ALARM_PARAM_BELOW_LOW_THRESHOLD
#define ST_ALARM_WATER_MASTER_VALVE_CURRENT_ALARM_HIGH_THRESHOLD "masterValveCurrentAlarm"ST_ALARM_PARAM_ABOVE_HIGH_THRESHOLD
#define ST_ALARM_WATER_MASTER_VALVE_CURRENT_ALARM_MAX           "masterValveCurrentAlarm"ST_ALARM_PARAM_MAX
#define ST_ALARM_WATER_VALVE_UNKHOWN_EVENT_STATE                "waterValveUnknown"

#define ALARM_WEATHER_STATE_IDLE                                0x00
#define ALARM_WEATHER_RAIN_ALARM                                0x01
#define ALARM_WEATHER_MOISTURE_ALARM                            0x02
#define ALARM_WEATHER_FREEZE_ALARM                              0x03
#define ALARM_WEATHER_UNKHOWN_EVENT_STATE                       0xFE
#define ST_ALARM_WEATHER_STATE_IDLE                             "weatherIdle"
#define ST_ALARM_WEATHER_RAIN_ALARM                             "rainAlarm"
#define ST_ALARM_WEATHER_MOISTURE_ALARM                         "moistureAlarm"
#define ST_ALARM_WEATHER_FREEZE_ALARM                           "freezeAlarm"
#define ST_ALARM_WEATHER_UNKHOWN_EVENT_STATE                    "weatherUnknown"

#define ALARM_IRRIGATION_STATE_IDLE                             0x00
#define ALARM_IRRIGATION_SCHEDULE_STARTED                       0x01
#define ALARM_IRRIGATION_SCHEDULE_FINISHED                      0x02
#define ALARM_IRRIGATION_VALVE_TABLE_RUN_STARTED                0x03
#define ALARM_IRRIGATION_VALVE_TABLE_RUN_FINISHED               0x04
#define ALARM_IRRIGATION_DEVICE_NOT_CONFIGURED                  0x05
#define ALARM_IRRIGATION_UNKHOWN_EVENT_STATE                    0xFE
#define ST_ALARM_IRRIGATION_STATE_IDLE                          "irrigationIdle"
#define ST_ALARM_IRRIGATION_SCHEDULE_STARTED                    "scheduleStarted"
#define ST_ALARM_IRRIGATION_SCHEDULE_FINISHED                   "scheduleFinished"
#define ST_ALARM_IRRIGATION_VALVE_TABLE_RUN_STARTED             "valveTableRunStarted"
#define ST_ALARM_IRRIGATION_VALVE_TABLE_RUN_FINISHED            "valveTableRunFinished"
#define ST_ALARM_IRRIGATION_DEVICE_NOT_CONFIGURED               "deviceIsNotConfigured"
#define ST_ALARM_IRRIGATION_UNKHOWN_EVENT_STATE                 "irrigationUnknown"

#define ALARM_GAS_STATE_IDLE                                    0x00
#define ALARM_GAS_COMBUSTIBLE_GAS_DETECTED_LOCATION             0x01
#define ALARM_GAS_COMBUSTIBLE_GAS_DETECTED                      0x02
#define ALARM_GAS_TOXIC_GAS_DETECTED_LOCATION                   0x03
#define ALARM_GAS_TOXIC_GAS_DETECTED                            0x04
#define ALARM_GAS_ALARM_TEST                                    0x05
#define ALARM_GAS_REPLACEMENT_REQUIRED                          0x06
#define ALARM_GAS_UNKHOWN_EVENT_STATE                           0xFE
#define ST_ALARM_GAS_STATE_IDLE                                 "gasIdle"
#define ST_ALARM_GAS_COMBUSTIBLE_GAS_DETECTED_LOCATION          "combustibleGasDetected"
#define ST_ALARM_GAS_COMBUSTIBLE_GAS_DETECTED                   "combustibleGasDetected"
#define ST_ALARM_GAS_TOXIC_GAS_DETECTED_LOCATION                "toxicGasDetected"
#define ST_ALARM_GAS_TOXIC_GAS_DETECTED                         "toxicGasDetected"
#define ST_ALARM_GAS_ALARM_TEST                                 "gasAlarmTest"
#define ST_ALARM_GAS_REPLACEMENT_REQUIRED                       "gasReplacementRequired"
#define ST_ALARM_GAS_UNKHOWN_EVENT_STATE                        "gasUnknown"

#define ALARM_PEST_STATE_IDLE                                   0x00
#define ALARM_PEST_TRAP_ARMED_LOCATION                          0x01
#define ALARM_PEST_TRAP_ARMED                                   0x02
#define ALARM_PEST_TRAP_REARMED_REQUIRED_LOCATION               0x03
#define ALARM_PEST_TRAP_REARMED_REQUIRED                        0x04
#define ALARM_PEST_DETECTED_LOCATION                            0x05
#define ALARM_PEST_DETECTED                                     0x06
#define ALARM_PEST_EXTERMINATED_LOCATION                        0x07
#define ALARM_PEST_EXTERMINATED                                 0x08
#define ALARM_PEST_UNKHOWN_EVENT_STATE                          0xFE
#define ST_ALARM_PEST_STATE_IDLE                                "pestIdle"
#define ST_ALARM_PEST_TRAP_ARMED_LOCATION                       "trapArmed"
#define ST_ALARM_PEST_TRAP_ARMED                                "trapArmed"
#define ST_ALARM_PEST_TRAP_REARMED_REQUIRED_LOCATION            "trapReArmRequired"
#define ST_ALARM_PEST_TRAP_REARMED_REQUIRED                     "trapReArmRequired"
#define ST_ALARM_PEST_DETECTED_LOCATION                         "pestDetected"
#define ST_ALARM_PEST_DETECTED                                  "pestDetected"
#define ST_ALARM_PEST_EXTERMINATED_LOCATION                     "pestExterminated"
#define ST_ALARM_PEST_EXTERMINATED                              "pestExterminated"
#define ST_ALARM_PEST_UNKHOWN_EVENT_STATE                       "pestUnknown"

/*#####################################################################################*/
/*###########################        BARRIER        #########################*/
/*#####################################################################################*/
#define BARRIER_SIGNAL_NOT_SUPPORTED                            0x00
#define BARRIER_SIGNAL_AUDIBLE_NOTIFICATION                     0x01
#define BARRIER_SIGNAL_VISAUL_NOTIFICATION                      0x02

#define ST_BARRIER_SIGNAL_NOT_SUPPORTED                         "notSupported"
#define ST_BARRIER_SIGNAL_AUDIBLE_NOTIFICATION                  "audible"
#define ST_BARRIER_SIGNAL_VISAUL_NOTIFICATION                   "visual"

typedef struct source_select
{
   uint8_t id;
   const char *name;
   uint8_t option;
}source_select_t;

typedef struct _class_infor
{
    uint8_t id;
    const char *name;
    size_t unitTableLen;
    source_select_t *unitTable;
}class_infor_t;

typedef struct _notification_param
{
    uint8_t notificationType;
    uint8_t notificationId;
    uint8_t tableLen;
    source_select_t *table;
}notification_param_t;

typedef struct _alarm_meaning_t
{
    const char *alarmPartern;
    const char *alarmMeaning;
    // const char *textToSpeechFormat;
}alarm_meaning_t;

int get_thermostat_setpoint_id(char *name);
int get_thermostat_mode_id(char *name);
int get_thermostat_fan_mode_id(char *name);

const char *get_thermostat_mode_name(uint8_t id);
const char *get_thermostat_setpoint_name(uint8_t id);
const char *get_thermostat_operating_name(uint8_t id);
const char *get_thermostat_fan_mode_name(uint8_t id);
const char *get_thermostat_fan_state_name(uint8_t id);
void get_thermostat_mode_from_bit_mask(uint8_t mask[], uint8_t mask_len, char **output);
void get_thermostat_setpoint_from_bit_mask(uint8_t mask[], uint8_t mask_len, char **output);

int get_sensor_id(char *sensorName, char *sensorUnitName, int *sensorUnit);
const char *get_sensor_name(int sensorId, int sensorUnit, const char **sensorUnitName);
void get_sensor_multilevel_from_bit_mask(uint8_t mask[], uint8_t mask_len, char **output);
const char *get_sensor_scale_from_bit_mask(int sensorId, uint8_t mask[], uint8_t mask_len, char **output);

int get_meter_id(char *meterName, char *meterUnitName, int *meterUnit, uint8_t *option);
const char *get_meter_name(int meterId, int meterUnit, const char **meterUnitName, int *option);
const char *get_meter_scale_from_bit_mask(int meterType, uint8_t mask[], uint8_t mask_len, char **output);
// int get_meter_unit_id(char *meterUnitName);

const char *get_notification_type_name(int notificationType, int notificationId, const char **notificationName, int *isParam);
const char *get_param_info(uint8_t notificationType, uint8_t notificationId, uint8_t paramIn);
const char *get_alarm_meaning(char *alarmPartern);

void get_barrier_signal_support_from_bit_mask(uint8_t mask[], uint8_t mask_len, char **output);
const char *get_barrier_signal_name(int signalId);
int get_barrier_signal_id(char *signalName);
const char *get_name_unsupported(uint8_t device_type, uint8_t specific_value);
void get_supported_key_attributes_from_bit_mask(uint8_t mask[], uint8_t mask_len, char **output);

#endif
