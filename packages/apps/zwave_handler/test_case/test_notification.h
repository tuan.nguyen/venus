#ifndef _TEST_NOTIFICATION_H_
#define _TEST_NOTIFICATION_H_

typedef struct table_parser
{
	uint8_t zwave_alarm_type;
	uint8_t zwave_alarm_event;
	uint8_t no_event_params;
	uint8_t event_param;
	char  *expected;
}table_parser_t;

typedef struct table_result
{
	char *lable;
	uint16_t id;
	char *actual;  
}table_result_t;
int test_notification();
#endif