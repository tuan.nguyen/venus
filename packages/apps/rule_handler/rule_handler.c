/******************************************************************************
 * Copyright (c) 2014, VEriK Systems. All rights reserved.
 *
 * Module: Service Provider
 *
 ******************************************************************************/
#include <stdio.h>
#include <unistd.h>
#include <libubus.h>
#include <libubox/blobmsg_json.h>
#include <sqlite3.h>

#include <pthread.h>
#include <fcntl.h>
#include <linux/input.h>
#include <sys/stat.h>
#include <json-c/json.h>
#include <curl/curl.h>
#include <uci.h>

// #include <openssl/bio.h>
// #include <openssl/conf.h>
// #include <openssl/evp.h>
// #include <openssl/err.h>
// #include <openssl/buffer.h>
// #include <openssl/sha.h>

#include "slog.h"
#include "VR_define.h"
#include "database.h"
#include "verik_utils.h"
#include "pmortem.h"
#include "rule.h"

#define ONE_CONDITION       1
#define TWO_CONDITIONS      2
#define THREE_CONDITIONS    3

static sqlite3 *rule_db;

static struct blob_buf buff;
static struct ubus_context *ctx;
static struct ubus_subscriber rule_notify;
static struct ubus_event_handler ubus_listener;

#define CONDITION_SIZE 3
// typedef struct _rule_structure 
// {
//     char rule_id[SIZE_128B];
//     char device_type[SIZE_128B];
//     char device_id[SIZE_32B];
//     char condition[SIZE_128B];
//     char value[SIZE_64B];
// } rule_structure;

char *g_receive_data = NULL;
typedef struct _nest_device
{   
    char device_type[SIZE_128B];
    char device_id[SIZE_256B];
    char device_hash[SIZE_256B];

    char camera_last_event_hash[SIZE_256B];
    struct _nest_device *next;
} nest_device;

typedef int (*new_info_callback)(nest_device *device, char *device_type, char *device_id, json_object *jobj);
typedef struct _nest_structure
{   
    char structure_id[SIZE_256B];
    char structure_hash[SIZE_256B];
    char away[SIZE_64B];
    int structure_exist;

    char smoke_co_alarms_array_hash[SIZE_256B];
    char thermostats_array_hash[SIZE_256B];
    char cameras_array_hash[SIZE_256B];

    nest_device *smoke_co_alarms_list;
    nest_device *thermostats_list;
    nest_device *cameras_list;

    struct _nest_structure *next; 
} nest_structure;

nest_structure *g_nest_structure_list = NULL;
char g_devices_nest_hash[SIZE_256B];
char g_structures_nest_hash[SIZE_256B];
pthread_mutex_t nest_listMutex;

// void string_to_hash(char *input, char *output);

int hash_diff(json_object *input, char *input_hash)
{
    char json_hash[SIZE_256B];
    const char*hash_message = (char*)json_object_get_string(input);
    string_to_hash((unsigned char *)hash_message, strlen(hash_message), json_hash, sizeof(json_hash));

    if(strcmp(json_hash, input_hash))
    {
        strcpy(input_hash, json_hash);
        return 1;
    }
    else
    {
        return 0;
    }
}

void remove_all_device(nest_device **devicelist)
{
    nest_device *tmp = *devicelist;
    while(*devicelist)
    {
        (*devicelist) = (*devicelist)->next;
        free(tmp);
        tmp = (*devicelist);
    }
}

static void add_device(nest_device **devicelist, char *device_type, 
                        char *device_id, int mode)
{
    if(mode)
    {
        pthread_mutex_lock(&nest_listMutex);
    }

    nest_device *new_node = (nest_device *)malloc(sizeof(nest_device));
    memset(new_node, 0x00, sizeof(nest_device));
    strcpy(new_node->device_type, device_type);
    strcpy(new_node->device_id, device_id);
    // strcpy(new_node->device_hash, hash);

    nest_device *tmp = *devicelist;
    // while(tmp)
    // {
    //     printf("name = %s action_time = %d\n", tmp->name, tmp->action_time);
    //     tmp=tmp->next;
    // }
    // tmp = g_allubus;
    if(tmp)
    {
        while(tmp->next)
        {
            tmp=tmp->next;
        }
        tmp->next = new_node;
    }
    else
    {
        *devicelist = new_node;
    }

    if(mode)
    {
        pthread_mutex_unlock(&nest_listMutex);
    }
}

void add_devices_to_structure_list(nest_device **devicelist, char *device_type, 
                                    json_object *info)
{
    if(info)
    {
        enum json_type type = json_object_get_type(info);
        if(type == json_type_array)
        {
            int i;
            int arraylen = json_object_array_length(info);

            for(i=0; i< arraylen; i++)
            {
                json_object *device_id = json_object_array_get_idx(info, i);
                add_device(devicelist, device_type, (char*)json_object_get_string(device_id), 0);
            }
        }
    }
}

static void add_nest_structure(char *structure_id, char *structure_hash, 
                                json_object *structure_info, int structure_exist)
{
    pthread_mutex_lock(&nest_listMutex);
    nest_structure *new_node = (nest_structure *)malloc(sizeof(nest_structure));
    memset(new_node, 0x00, sizeof(nest_structure));

    strcpy(new_node->structure_id, structure_id);
    strcpy(new_node->structure_hash, structure_hash);
    strcpy(new_node->away, json_object_get_string(json_object_object_get(structure_info, "away")));
    new_node->structure_exist = structure_exist;

    json_object *smoke_co_alarms_array = json_object_object_get(structure_info, "smoke_co_alarms");
    json_object *thermostats_array = json_object_object_get(structure_info, "thermostats");
    json_object *cameras_array = json_object_object_get(structure_info, "cameras");

    const char*hash_message = (char*)json_object_get_string(smoke_co_alarms_array);
    string_to_hash((unsigned char *)hash_message, strlen(hash_message), 
                    new_node->smoke_co_alarms_array_hash, sizeof(new_node->smoke_co_alarms_array_hash));

    hash_message = (char*)json_object_get_string(thermostats_array);
    string_to_hash((unsigned char *)hash_message, strlen(hash_message), 
                    new_node->thermostats_array_hash, sizeof(new_node->thermostats_array_hash));

    hash_message = (char*)json_object_get_string(cameras_array);
    string_to_hash((unsigned char *)hash_message, strlen(hash_message),
                    new_node->cameras_array_hash, sizeof(new_node->cameras_array_hash));

    add_devices_to_structure_list(&new_node->smoke_co_alarms_list, "smoke_co_alarms", smoke_co_alarms_array);
    add_devices_to_structure_list(&new_node->thermostats_list, "thermostats", thermostats_array);
    add_devices_to_structure_list(&new_node->cameras_list, "cameras", cameras_array);

    nest_structure *tmp = g_nest_structure_list;
    // while(tmp)
    // {
    //     printf("name = %s action_time = %d\n", tmp->name, tmp->action_time);
    //     tmp=tmp->next;
    // }
    // tmp = g_allubus;
    if(tmp)
    {
        while(tmp->next)
        {
            tmp=tmp->next;
        }
        tmp->next = new_node;
    }
    else
    {
        g_nest_structure_list = new_node;
    }
    pthread_mutex_unlock(&nest_listMutex);
}

static void remove_nest_structure(char* structure_id)
{
    pthread_mutex_lock(&nest_listMutex);
    nest_structure *tmp = g_nest_structure_list;
    nest_structure *pre = g_nest_structure_list;
    // while(tmp)
    // {
    //     printf("name = %s action_time = %d\n", tmp->name, tmp->action_time);
    //     tmp=tmp->next;
    // }
    // tmp = g_allubus;
    while(tmp)
    {
        if(!strcmp(tmp->structure_id, structure_id))
        {
            if(tmp == g_nest_structure_list)
            {
                g_nest_structure_list = tmp->next;
                remove_all_device(&tmp->smoke_co_alarms_list);
                remove_all_device(&tmp->thermostats_list);
                remove_all_device(&tmp->cameras_list);
                free(tmp);
            }
            else
            {
                pre->next = tmp->next;
                remove_all_device(&tmp->smoke_co_alarms_list);
                remove_all_device(&tmp->thermostats_list);
                remove_all_device(&tmp->cameras_list);
                free(tmp);
            }
            break;
        }
        pre=tmp;
        tmp=tmp->next;
    }
    pthread_mutex_unlock(&nest_listMutex);
}

void display_device_list(nest_device **devicelist)
{
    int i=0;
    nest_device *tmp = *devicelist;
    while(tmp)
    {
        printf("\t%d\n", i);
        printf("\tdevice type %s\n", tmp->device_type);
        printf("\tdevice id %s\n", tmp->device_id);
        printf("\tdevice_hash %s\n", tmp->device_hash);

        tmp=tmp->next;
        i++;
    }
}

void display_all_list()
{
    int i=0;
    nest_structure *tmp = g_nest_structure_list;
    while(tmp)
    {
        printf("#############################\n");
        printf("%d\n", i);
        printf("structure_id %s\n", tmp->structure_id);
        printf("structure_hash %s\n", tmp->structure_hash);
        printf("away = %s\n", tmp->away);
        printf("structure_exist = %d\n", tmp->structure_exist);

        display_device_list(&tmp->smoke_co_alarms_list);
        display_device_list(&tmp->thermostats_list);
        display_device_list(&tmp->cameras_list);

        tmp=tmp->next;
        i++;
    }
}

static void remove_device(nest_device **devicelist, char* device_id)
{
    printf("remove %s\n", device_id);
    display_device_list(devicelist);
    pthread_mutex_lock(&nest_listMutex);
    nest_device *tmp = *devicelist;
    nest_device *pre = *devicelist;
    // while(tmp)
    // {
    //     printf("name = %s action_time = %d\n", tmp->name, tmp->action_time);
    //     tmp=tmp->next;
    // }
    // tmp = g_allubus;
    while(tmp)
    {
        if(!strcmp(tmp->device_id, device_id))
        {
            if(tmp == (*devicelist))
            {
                *devicelist = tmp->next;
                free(tmp);
            }
            else
            {
                pre->next = tmp->next;
                free(tmp);
            }
            break;
        }
        pre=tmp;
        tmp=tmp->next;
    }
    pthread_mutex_unlock(&nest_listMutex);
}

typedef struct _daemon_service 
{
    const char *service_name;
    const char *name;
    uint32_t id;
    int timeout;
} daemon_service;

daemon_service daemon_services[] = {
    {"zigbee_handler",      "zigbee",            0,        0},
    {"zwave_handler",       "zwave",             0,        0},
    {"upnp_handler",        "upnp",              0,        0},
    {"alljoyn_handler",     "alljoyn",           0,        0},
    {"pubsub_handler",      "pubsub",            0,        0},
    {"venus_handler",       "venus",             0,        0},
};

static const struct blobmsg_policy msg_policy[1] = {
    [0] = { .name = "msg", .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy service_event_policy[2] = {
    [0] = { .name = "id", .type = BLOBMSG_TYPE_INT32 },
    [1] = { .name = "path", .type = BLOBMSG_TYPE_STRING },
};

static const struct ubus_method rule_methods[] = {
    // UBUS_METHOD("register_notify", register_notify, notify_policy),
};

static struct ubus_object_type rule_object_type =
    UBUS_OBJECT_TYPE("rule", rule_methods);

static struct ubus_object rule_object = {
    //.subscribe_cb = test_client_subscribe_cb,
    .name = "rule",
    .type = &rule_object_type,
    .methods = rule_methods,
    .n_methods = ARRAY_SIZE(rule_methods),
};

static void notify_handle_remove(struct ubus_context *ctx, struct ubus_subscriber *s, uint32_t id)
{
    SLOGI("Object %08x went away\n", id);
    int i,ret;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++) 
    {
        if (daemon_services[i].id == id)
        {
            ret = ubus_unsubscribe(ctx, &rule_notify, daemon_services[i].id);
            SLOGW("UNsubscribe object %s with id %08X: %s\n", daemon_services[i].name, daemon_services[i].id, ubus_strerror(ret));
            SLOGI("removing id\n");
            daemon_services[i].id = 0;
            daemon_services[i].timeout = 0;
        }
    }
}

static int rule_autooff_process_callback(void *action, int argc, char **argv, char **azColName)
{
    if(argv[0] && strlen(argv[0]) && argv[1] && strlen(argv[1]) && argv[2] && strlen(argv[2]))
    {
        if(!strncmp(action, argv[2], strlen(action)-1))
        {
            char value_action[8], value_rule[8];;
            strcpy(value_action, action+(strlen(action)-1));
            strcpy(value_rule, argv[2]+(strlen(argv[2])-1));
            if(!strcmp(value_action, value_rule))
            {
                //deactivate rule
            }
            else
            {
                //activate rule
                char crontabs_timer[64];
                time_t epoch_time = time(NULL);
                struct tm *tm_struct = localtime(&epoch_time);
                int hour = tm_struct->tm_hour;
                int minute = tm_struct->tm_min;

                char tmp[256];
                strncpy(tmp, argv[1], 255);
                char hour_in[32], min_in[32];
                char *save_tok;
                char *pch = strtok_r(tmp, ":", &save_tok);
                if(pch != NULL)
                {
                    strcpy(hour_in, pch);
                    pch = strtok_r(NULL, ":", &save_tok);
                }
                if(pch != NULL)
                {
                    strcpy(min_in, pch);
                }

                int min_set = minute + atoi(min_in);

                if(min_set >= 60)
                {
                    min_set = min_set - 60;
                    hour = hour + 1;
                }

                int hour_set = hour + atoi(hour_in);
                if(hour_set >=24)
                {
                    hour_set = hour_set - 24;
                }

                sprintf(crontabs_timer, "%d:%d;%s", hour_set, min_set, ST_AUTO_OFF);
                VR_(install_rule)(argv[0], crontabs_timer, argv[2]);

                sprintf(tmp, "%d", (unsigned)time(NULL));
                database_actions("rule_handler", rule_db, "UPDATE RULES set run='%s' where name='%s'", tmp, argv[0]);

                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
                json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_AUTO_OFF_ACTIVATE));
                json_object_object_add(jobj, ST_RULE_NAME, json_object_new_string(argv[0]));
                json_object_object_add(jobj, ST_CONDITIONS, json_object_new_string(argv[1]));
                json_object_object_add(jobj, ST_ACTION, json_object_new_string(argv[2]));
                json_object_object_add(jobj, ST_ACTIVE_TIME, json_object_new_string(tmp));
                json_object_put(jobj);
            }
            // printf("value_action = %s\n", value_action);
            // printf("value_rule = %s\n", value_rule);
        }
    }

    return 0;
}

void rule_autooff_process(char *rule_autooff_input)
{
    SLOGI("msgstr = %s\n", rule_autooff_input);
    char action[64], status[32];
    memset(action, 0x00, sizeof(action));
    memset(status, 0x00, sizeof(status));

    json_object *jobj = VR_(create_json_object)(rule_autooff_input);
    if(jobj)
    {
        json_object_object_foreach(jobj, key, val)
        {
            if(!strcasecmp(key, ST_METHOD) || !strcasecmp(key, ST_STATUS))
            {
                if(!strcasecmp(key, ST_STATUS))
                {
                    strncpy(status, json_object_get_string(val), sizeof(status)-1);
                }
            }
            else
            {
                size_t act_len = strlen(action);
                if(act_len)
                {
                    sprintf(action+act_len, ";%s", json_object_get_string(val));
                }
                else
                {
                    sprintf(action, "%s", json_object_get_string(val));
                }
            }
        }

        printf("action = %s\n", action);

        if(!strcasecmp(status, ST_SUCCESSFUL))
        {
            char sql[256];
            char *zErrMsg = 0;
            snprintf(sql, sizeof(sql), "SELECT name,conditions,actions from RULES where type='%s' AND status='%s'",
                        ST_AUTO_OFF, ST_ACTIVE);
            int rc = sqlite3_exec(rule_db, sql, rule_autooff_process_callback, action, &zErrMsg);
            if( rc != SQLITE_OK ){
                fprintf(stderr, "SQL error: %s\n", zErrMsg);
                sqlite3_free(zErrMsg);
            }
            else
            {
                printf("%s SUCCESS\n", sql);
            }
        }
        json_object_put(jobj);
        free(rule_autooff_input);
    }
    else
    {
        SLOGE("Failed to parse json\n");
    }
}

static int rule_d2d_process_callback(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 2)
    {
        return 0;
    }
    char *ruleId = argv[0];
    char *actions = argv[1];

    trigger_rule_via_shell_script(ruleId, actions);
    return 0;
}

static int check_device_status_change(const char *id, const char *feature,
                                    char *new_value, char conditions[CONDITION_SIZE][SIZE_256B],
                                    int relate)
{
    int res = 0;
    SEARCH_DATA_INIT_VAR(devStatus);
    searching_database("rule_handler", rule_db, get_last_data_cb, &devStatus, 
                    "SELECT value from PREVIOUS_STATUS where deviceId='%s' AND featureId='%s'",
                    id, feature);
    if(strcmp(devStatus.value, new_value))
    {
        sprintf(conditions[0], "%s;%s", id, new_value);
        if(relate && strcmp(new_value, ST_OFF_VALUE))
        {
            sprintf(conditions[1], "%s;1", id);
        }
        database_actions("rule_handler", rule_db, "UPDATE PREVIOUS_STATUS set "
                        "value='%s' where deviceId='%s' AND featureId='%s'", new_value, id, feature);
        res = 1;
    }
    FREE_SEARCH_DATA_VAR(devStatus);

    return res;
}

static void conditions_keypad_with_userId(json_object *jobj, const char *deviceId, const char *event, 
                                            char *conditions, size_t length)
{
    if(!jobj || !conditions || !deviceId || !event)
    {
        SLOGE("missing infor\n");
        return;
    }

    CHECK_JSON_OBJECT_EXIST(userIdObj, jobj, ST_USER_ID, done);
    enum json_type type = json_object_get_type(userIdObj);
    if(json_type_int != type)
    {
        goto done;
    }

    int userId = json_object_get_int(userIdObj);
    snprintf(conditions, length, "%s;%s;%d", deviceId, event, userId);/*userId is int*/
done:
    return;
}

void rule_d2d_process(void *rule_d2d_input)
{
    SLOGI("rule_d2d_process msgstr = %s\n", rule_d2d_input);
    char *data = rule_d2d_input;

    int i,k=0;
    int num_condition = 0;
    int condition_id[CONDITION_SIZE];
    char conditions[CONDITION_SIZE][SIZE_256B]={{0},{0},{0}};

    json_object *jobj = VR_(create_json_object)(data);
    if(!jobj)
    {
        SLOGE("failed to create json object\n");
        free(data);
        return;
    }

    CHECK_JSON_OBJECT_EXIST(method_obj, jobj, ST_METHOD, done);
    const char *local_id;

    json_object *udn_obj = json_object_object_get(jobj, ST_UDN);
    if(!udn_obj)
    {
        CHECK_JSON_OBJECT_EXIST(deviceid_obj, jobj, ST_DEVICE_ID, done);
        local_id = json_object_get_string(deviceid_obj);
    }
    else
    {
        local_id = json_object_get_string(udn_obj);
    }

    const char *method = json_object_get_string(method_obj);

    if(!strcmp(method, ST_SET_BINARY_R))
    {
        CHECK_JSON_OBJECT_EXIST(status_obj, jobj, ST_STATUS, done);
        const char *status = json_object_get_string(status_obj);
        if(!strcmp(status, ST_SUCCESSFUL)) //need check user id to know local or alexa control, duplicate update.
        {                                  //SUCCESSFULL for updating when local control.
            json_object *deviceTypeObj = NULL;
            if(json_object_object_get_ex(jobj, ST_DEVICE_TYPE, &deviceTypeObj))
            {
                const char *deviceType = json_object_get_string(deviceTypeObj);
                /*ignore this message with garadoor*/
                if(deviceType && !strcmp(deviceType, ST_GARAGE_DOOR_CONTROLLER))
                {
                    goto done;
                }
            }

            CHECK_JSON_OBJECT_EXIST(command_obj, jobj, ST_COMMAND, done);
            CHECK_JSON_OBJECT_EXIST(value_obj, jobj, ST_VALUE, done);
            const char *command = json_object_get_string(command_obj);
            const char *value = json_object_get_string(value_obj);
            if(value && strlen(value))
            {
                if(!strcmp(command, ST_ON_OFF))
                {
                    check_device_status_change(local_id, ST_ON_OFF, (char*)value, conditions, 1);
                }
            }
            else
            {
                SLOGE("cannot get value\n");
            }
        }
    }
    else if(!strcmp(method, ST_NOTIFY))
    {
        CHECK_JSON_OBJECT_EXIST(notifytype_obj, jobj, ST_NOTIFY_TYPE, done);
        const char *notifytype = json_object_get_string(notifytype_obj);
        if(!strcmp(notifytype, ST_CMD_CLASS_ZPC))
        {
            CHECK_JSON_OBJECT_EXIST(cmdClassObj, jobj, ST_CMD_CLASS, done);
            CHECK_JSON_OBJECT_EXIST(cmdObj, jobj, ST_CMD, done);
            const char *cmdClass = json_object_get_string(cmdClassObj);
            const char *cmd = json_object_get_string(cmdObj);
            if(!strcmp(cmd, ST_REPORT))
            {
                if(!strcmp(cmdClass, ST_DOOR_LOCK))/*for get door state only ?????*/
                {
                    CHECK_JSON_OBJECT_EXIST(lockStatusObj, jobj, ST_LOCK_STATUS, done);
                    char lockStatus[SIZE_32B];
                    snprintf(lockStatus, sizeof(lockStatus), "%d", json_object_get_int(lockStatusObj));
                    check_device_status_change(local_id, ST_ON_OFF, lockStatus, conditions, 0);
                }
                /*for alarm notify, no need check previous status*/
                else if(!strcmp(cmdClass, ST_ALARM) || 
                        !strcmp(cmdClass, ST_SENSOR_ALARM) || 
                        !strcmp(cmdClass, ST_SENSOR_BINARY) ||
                        !strcmp(cmdClass, ST_NOTIFICATION))
                {
                    CHECK_JSON_OBJECT_EXIST(alarmtype_obj, jobj, ST_ALARM_TYPE_FINAL, done);
                    const char *alarmtype = json_object_get_string(alarmtype_obj);
                    if(!strcmp(alarmtype, ST_UNKNOWN))
                    {
                        goto done;
                    }

                    int state = get_door_lock_state(alarmtype);
                    if(DOOR_RF_UNLOCK_VALUE == state)
                    {
                        snprintf(conditions[0], sizeof(conditions[0]), "%s;%s", local_id, ST_ON_VALUE);
                    }
                    else if(DOOR_RF_LOCK_VALUE == state)
                    {
                        snprintf(conditions[0], sizeof(conditions[0]), "%s;%s", local_id, ST_OFF_VALUE);
                    }
                    else if(DOOR_AUTO_LOCK_VALUE == state
                            || DOOR_ONE_TOUCH_LOCK_VALUE == state
                            )
                    {
                        snprintf(conditions[0], sizeof(conditions[0]), "%s;%s", local_id, ST_OFF_VALUE);
                    }
                    else if(DOOR_KEYPAD_LOCK_VALUE == state
                            || DOOR_KEYPAD_UNLOCK_VALUE == state
                            )
                    {
                        conditions_keypad_with_userId(jobj, local_id, alarmtype, conditions[0], sizeof(conditions[0]));
                        snprintf(conditions[1], sizeof(conditions[1]), "%s;%s", local_id, alarmtype);
                        if(DOOR_KEYPAD_UNLOCK_VALUE == state)
                        {
                            snprintf(conditions[2], sizeof(conditions[2]), "%s;%s", local_id, ST_ON_VALUE);
                        }
                        else
                        {
                            snprintf(conditions[2], sizeof(conditions[2]), "%s;%s", local_id, ST_OFF_VALUE);
                        }
                    }
                    else if(DOOR_MANUAL_LOCK_VALUE == state
                            || DOOR_MANUAL_UNLOCK_VALUE == state
                            )
                    {
                        snprintf(conditions[0], sizeof(conditions[0]), "%s;%s", local_id, alarmtype);
                        if(DOOR_MANUAL_UNLOCK_VALUE == state)
                        {
                            snprintf(conditions[1], sizeof(conditions[1]), "%s;%s", local_id, ST_ON_VALUE);
                        }
                        else
                        {
                            snprintf(conditions[1], sizeof(conditions[1]), "%s;%s", local_id, ST_OFF_VALUE);
                        }
                    }
                    else
                    {
                        snprintf(conditions[0], sizeof(conditions[0]), "%s;%s", local_id, alarmtype);
                    }
                }
                else if(!strcmp(cmdClass, ST_BARRIER_OPERATOR))
                {
                    CHECK_JSON_OBJECT_EXIST(barrierStatusObj, jobj, ST_BARRIER_STATUS, done);
                    char barrierStatus[SIZE_32B];
                    snprintf(barrierStatus, sizeof(barrierStatus), "%d", json_object_get_int(barrierStatusObj));

                    check_device_status_change(local_id, ST_ON_OFF, barrierStatus, conditions, 0);
                }
                else if(!strcmp(cmdClass, ST_BASIC) ||
                        !strcmp(cmdClass, ST_SWITCH_BINARY) ||
                        !strcmp(cmdClass, ST_SWITCH_ALL) ||
                        !strcmp(cmdClass, ST_SWITCH_MULTILEVEL))
                {
                    CHECK_JSON_OBJECT_EXIST(valueObj, jobj, ST_VALUE, done);
                    char value[SIZE_32B];
                    snprintf(value, sizeof(value), "%d", json_object_get_int(valueObj));
                    check_device_status_change(local_id, ST_ON_OFF, value, conditions, 1);
                }
                else if(!strcmp(cmdClass, ST_THERMOSTAT_MODE))
                {
                    CHECK_JSON_OBJECT_EXIST(thermostatModeObj, jobj, ST_THERMOSTAT_MODE, done);
                    const char *thermostatMode = json_object_get_string(thermostatModeObj);
                    check_device_status_change(local_id, ST_THERMOSTAT_MODE, (char*)thermostatMode, conditions, 0);
                }
            }
        }
        else if(!strcmp(notifytype, ST_BINARY_STATE_RESPONSE)) //upnp devices
        {
            CHECK_JSON_OBJECT_EXIST(value_obj, jobj, ST_VALUE, done);
            const char *value = json_object_get_string(value_obj);
            check_device_status_change(local_id, ST_ON_OFF, (char*)value, conditions, 0);
        }
        else if(!strcmp(notifytype, ST_ALARM)) //zigbee devices
        {
            CHECK_JSON_OBJECT_EXIST(alarmtype_obj, jobj, ST_ALARM_TYPE, done);
            const char *alarmType = json_object_get_string(alarmtype_obj);
            check_device_status_change(local_id, ST_ALARM, (char*)alarmType, conditions, 0);
        }
    }

    k = 0;
    for(i = 0; i<sizeof(conditions)/sizeof(conditions[0]); i++)
    {
        SLOGI("conditions[%d] = %s\n", i, conditions[i]);
        if(strlen(conditions[i]))
        {
            num_condition++;
            condition_id[k++]=i;
        }
    }

    if(0 == num_condition)
    {
        goto done;
    }

    char sql[SIZE_512B]={0};
    char *zErrMsg = 0;

    switch(num_condition)
    {
        case ONE_CONDITION:
        {
            snprintf(sql, sizeof(sql),
                        "SELECT id,actions from RULES where type='%s' AND conditions='%s' AND status='%s'",
                        ST_DEV_TO_DEV, conditions[condition_id[0]], ST_ACTIVE);
            break;
        }

        case TWO_CONDITIONS:
        {
            snprintf(sql, sizeof(sql),
                        "SELECT id,actions from RULES where type='%s' AND conditions in ('%s','%s') AND status='%s'",
                        ST_DEV_TO_DEV, conditions[condition_id[0]], conditions[condition_id[1]], ST_ACTIVE);
            break;
        }

        case THREE_CONDITIONS:
        {
            snprintf(sql, sizeof(sql),
                        "SELECT id,actions from RULES where type='%s' AND conditions in ('%s','%s','%s') AND status='%s'",
                        ST_DEV_TO_DEV, conditions[condition_id[0]], conditions[condition_id[1]], conditions[condition_id[2]],
                        ST_ACTIVE);
            break;
        }
    }

    int rc = sqlite3_exec(rule_db, sql, rule_d2d_process_callback, NULL, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        printf("%s SUCCESS\n", sql);
    }

done:
    json_object_put(jobj);
    free(data);
    return;
}

static int receive_notify_cb(struct ubus_context *ctx, struct ubus_object *obj,
                struct ubus_request_data *req, const char *method,
                struct blob_attr *msg)
{
    SLOGI("RULE receive_notify_cb from %s\n", method);
    const char *msgstr = "(unknown)";
    struct blob_attr *tb[1];
    blobmsg_parse(msg_policy, ARRAY_SIZE(msg_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
    {
        msgstr = blobmsg_data(tb[0]);
        SLOGI("####### RULE ACTIONS with msgstr = %s\n", msgstr);
        ////// Check rules /////
        if ( (strstr(msgstr, ST_METHOD_ACTION_COMPLETE) && strstr(msgstr, ST_ACTION_TYPE_SETBINARY)) //for upnp devices
                || 
                ( strstr(msgstr, ST_METHOD_NOTIFY) 
                    &&
                    (
                        ( strstr(msgstr, ST_NOTIFY_TYPE_BINARY_STATE) //zigbee & zwave
                        )
                        ||
                        ( strstr(msgstr, ST_NOTIFY_TYPE_ALARM) //zigbee & zwave
                        )
                        ||
                        strstr(msgstr, ST_CMD_CLASS_ALARM) ||
                        strstr(msgstr, ST_CMD_CLASS_SENSOR_ALARM) ||
                        strstr(msgstr, ST_CMD_CLASS_SENSOR_BINARY) ||
                        strstr(msgstr, ST_CMD_CLASS_NOTIFICATION) ||
                        strstr(msgstr, ST_CMD_CLASS_BARRIER_OPERATOR) ||
                        strstr(msgstr, ST_CMD_CLASS_DOORLOCK) ||
                        strstr(msgstr, ST_CMD_CLASS_BASIC) ||
                        strstr(msgstr, ST_CMD_CLASS_SWITCH_ALL) ||
                        strstr(msgstr, ST_CMD_CLASS_SWITCH_BINARY) ||
                        strstr(msgstr, ST_CMD_CLASS_SWITCH_MULTILEVEL)
                    )
                )
                || 
                ( 
                    strstr(msgstr, ST_METHOD_SETBINARY_R) && strstr(msgstr, ST_COMMAND_ON_OFF)
                )
            )
        {
            if(strstr(msgstr, ST_METHOD_SETBINARY_R) 
                && strstr(msgstr, ST_COMMAND_ON_OFF) 
                && strstr(msgstr, "\"value\": \"1\""))
            {
                SEARCH_DATA_INIT_VAR(rule_count);
                searching_database("rule_handler", rule_db, get_last_data_cb, &rule_count, 
                                    "SELECT Count(name) from RULES where status='%s' AND type='%s'", 
                                    ST_ACTIVE, ST_AUTO_OFF);

                if(atoi(rule_count.value) > 0)
                {
                    pthread_t rule_autooff_thread;
                    char *rule_autooff_input = (char*)malloc(strlen(msgstr) + 1);
                    strcpy(rule_autooff_input, msgstr);
                    pthread_create(&rule_autooff_thread, NULL, (void *)&rule_autooff_process, (void*)rule_autooff_input);
                    pthread_detach(rule_autooff_thread);
                }
                FREE_SEARCH_DATA_VAR(rule_count);
            }

            SEARCH_DATA_INIT_VAR(rule_count);
            searching_database("rule_handler", rule_db, get_last_data_cb, &rule_count, 
                            "SELECT Count(name) from RULES where status='%s' AND Type='%s'",
                            ST_ACTIVE, ST_DEV_TO_DEV);
            if(rule_count.len)
            {
                char *rule_d2d_input = (char*)malloc(strlen(msgstr) + 1);
                strcpy(rule_d2d_input, msgstr);

                pthread_t rule_d2d_thread;
                pthread_create(&rule_d2d_thread, NULL, (void *)&rule_d2d_process, (void*)rule_d2d_input);
                pthread_detach(rule_d2d_thread);
            }
            FREE_SEARCH_DATA_VAR(rule_count);
        }
    }   
    return 0;
}

static int init_ubus_service()
{
    const char *ubus_socket = NULL;

    uloop_init();

    ctx = ubus_connect(ubus_socket);
    if (!ctx) {
        SLOGE("Failed to connect to ubus\n");
        return -1;
    }

    ubus_add_uloop(ctx);

    return 0;
}

static void receive_event(struct ubus_context *ctx, struct ubus_event_handler *ev,
              const char *type, struct blob_attr *msg)
{
    if(!strcmp(type, "ubus.object.add"))
    {
        struct blob_attr *tb[2];
        blobmsg_parse(service_event_policy, ARRAY_SIZE(service_event_policy), tb, blob_data(msg), blob_len(msg));
        uint32_t id;
        int i, ret;
        const char *path = "unknown";

        if(tb[0] && tb[1])
        {
            id = blobmsg_get_u32(tb[0]);
            path = blobmsg_data(tb[1]);

            for(i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
            {
                if(!strcmp(daemon_services[i].name, path))
                {
                    if(daemon_services[i].id != 0)
                    {
                        ret = ubus_unsubscribe(ctx, &rule_notify, daemon_services[i].id);
                        SLOGW("UNsubscribe object %s with id %08X: %s\n", daemon_services[i].name, daemon_services[i].id, ubus_strerror(ret));
                    }
                    
                    daemon_services[i].id = id;
                    rule_notify.remove_cb = notify_handle_remove;
                    rule_notify.cb = receive_notify_cb;
                    ret = ubus_subscribe(ctx, &rule_notify, daemon_services[i].id);
                    SLOGW("Watching object %s with id %08X: %s\n", daemon_services[i].name, daemon_services[i].id, ubus_strerror(ret));
                }
            }
        }
    }
}

static void rule_ubus_service()
{ 
    int ret, i;

    ret = ubus_add_object(ctx, &rule_object);
    if (ret) {
        SLOGE("Failed to add_object object: %s\n", ubus_strerror(ret));
        return;
    }

    ret = ubus_register_subscriber(ctx, &rule_notify);
    if (ret)
    {
        SLOGE("Failed to add notify handler: %s\n", ubus_strerror(ret));
    }

    for(i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(ubus_lookup_id(ctx, daemon_services[i].name, &daemon_services[i].id))
        {
            SLOGI("Failed to look up %s object\n", daemon_services[i].name);
        }
        else
        {
            rule_notify.remove_cb = notify_handle_remove;
            rule_notify.cb = receive_notify_cb;
            ret = ubus_subscribe(ctx, &rule_notify, daemon_services[i].id);
            SLOGW("Watching object %s with id %08X: %s\n", daemon_services[i].name, daemon_services[i].id, ubus_strerror(ret));
        }
    }

    memset(&ubus_listener, 0, sizeof(ubus_listener));
    ubus_listener.cb = receive_event;
    ret = ubus_register_event_handler(ctx, &ubus_listener, "ubus.object.add");
    if (ret)
        SLOGE("Failed to register event handler: %s\n", ubus_strerror(ret));

    uloop_run();
}

static void free_ubus_service()
{
    ubus_free(ctx);
    uloop_done();

    if(buff.buf)
    {
        free(buff.buf);
    }

    if(rule_db)
        sqlite3_close(rule_db);
}

void parse_event(char *input, char *event, char *data)
{
    char *tok, *ch, *save_tok;
    tok = strtok_r(input, "\n", &save_tok);
    while(tok)
    {
        if((ch=strstr(tok, "event: ")))
        {
            strcpy(event, ch + strlen("event: "));
        }
        else if((ch=strstr(tok, "data: ")))
        {
            strcpy(data, ch + strlen("data: "));
        }
        tok = strtok_r(NULL, "\n", &save_tok);
    }
}

// char* base64Encode(unsigned char *str, int len)
// {
//     BIO *bio, *b64;
//     BUF_MEM *buf_encode;
//     unsigned char *encStr;

//     b64 = BIO_new(BIO_f_base64());
//     bio = BIO_new(BIO_s_mem());
//     b64 = BIO_push(b64, bio);
//     BIO_write(b64, str, len);
//     BIO_flush(b64);

//     BIO_get_mem_ptr(b64, &buf_encode);

//     encStr = (unsigned char*)calloc(1, buf_encode->length);
//     strncpy((char*)encStr, buf_encode->data, buf_encode->length);
//     encStr[buf_encode->length] = '\0';

//     BIO_free_all(b64);
//     return (char *)encStr;
// }

// void string_to_hash(char *input, char *output)
// {
//     if(input && strlen(input))
//     {
//         unsigned char sha[SHA_DIGEST_LENGTH];
//         SHA1((const unsigned char *)input, strlen(input), sha);

//         char *final = base64Encode(sha, SHA_DIGEST_LENGTH);
//         strcpy(output, final);
//         free(final);
//     }
// }

void detect_sound_motion(json_object *event)
{
    int has_sound = json_object_get_int(json_object_object_get(event, "has_sound"));
    int has_motion = json_object_get_int(json_object_object_get(event, "has_motion"));
    const char *start_time = json_object_get_string(json_object_object_get(event, "start_time"));
    const char *end_time = json_object_get_string(json_object_object_get(event, "end_time"));

    int start_time_milisecond, end_time_milisecond;
    unsigned int start_time_epoch = VR_(str_to_time)((char*)start_time, 
                                            "%4d-%2d-%2dT%2d:%2d:%2d.%3dZ", 
                                            &start_time_milisecond);
    unsigned int end_time_epoch = VR_(str_to_time)((char*)end_time,
                                             "%4d-%2d-%2dT%2d:%2d:%2d.%3dZ",
                                             &end_time_milisecond);

    // printf("start_time = %s start_time_epoch = %u, %d\n", start_time, start_time_epoch, start_time_milisecond);
    // printf("end_time = %s end_time_epoch = %u, %d\n", end_time, end_time_epoch, end_time_milisecond);
    // printf("time now %lu\n", time(NULL));
    if((start_time_epoch > end_time_epoch) ||
        ((start_time_epoch == end_time_epoch) && start_time_milisecond > end_time_milisecond))
    {
        if(has_sound && has_motion)
        {
            printf("got sound and motion\n");
        }
        else if(has_motion)
        {
            printf("got motion\n");
        }
        else if(has_sound)
        {
            printf("got sound\n");
        }
    }
    else if((start_time_epoch < end_time_epoch) ||
                ((start_time_epoch == end_time_epoch) && start_time_milisecond < end_time_milisecond))
    {
        if((time(NULL) - end_time_epoch) < 20) //ignore if message take more than 20s to came to venus,fix me
        {
            if(has_sound && has_motion)
            {
                printf("end sound and motion\n");
            }
            else if(has_motion)
            {
                printf("end motion\n");
            }
            else if(has_sound)
            {
                printf("end sound\n");
            }
        }
    }
}

int find_info_change(nest_device *device, char *device_type, char *device_id, json_object *device_info)
{
    if(!strcmp(device_type, "thermostats"))
    {
        // json_object_object_foreach(device_info, key, val)
        // {
        //     printf("key %s\n", key);
        //     printf("val %s\n", json_object_get_string(val));
        // }
    }
    else if(!strcmp(device_type, "cameras"))
    {
        json_object_object_foreach(device_info, key, val)
        {
            // printf("key %s\n", key);
            // printf("val %s\n", json_object_get_string(val));
            if(!strcmp(key, "last_event"))
            {
                json_object *web_url = json_object_object_get(val, "web_url");
                if(web_url)
                {
                    json_object_object_del(val, "web_url");
                }

                json_object *app_url = json_object_object_get(val, "app_url");
                if(app_url)
                {
                    json_object_object_del(val, "app_url");
                }

                json_object *urls_expire_time = json_object_object_get(val, "urls_expire_time");
                if(urls_expire_time)
                {
                    json_object_object_del(val, "urls_expire_time");
                }

                char last_event_hash[SIZE_256B];
                const char*hash_message = (char*)json_object_get_string(val);
                string_to_hash((unsigned char *)hash_message, strlen(hash_message), 
                                last_event_hash, sizeof(last_event_hash));
                nest_device *tmp = device;
                while(tmp)
                {
                    if(!strcmp(tmp->device_id, device_id))
                    {
                        if(!strcmp(tmp->camera_last_event_hash, last_event_hash))
                        {
                            //the same, ignore
                            break;
                        }
                        else
                        {
                            //got change, update here
                            strcpy(tmp->camera_last_event_hash, last_event_hash);
                            detect_sound_motion(val);
                            break;
                        }
                    }
                    tmp=tmp->next;
                }       
            }
        }
    }
    return 0;
}

void find_device_change(char *device_type, json_object *jobj, new_info_callback callback)
{
    json_object_object_foreach(jobj, device_id, device_info)
    {
        if(!strcmp(device_type, "cameras"))
        {
            json_object *web_url = json_object_object_get(device_info, "web_url");
            if(web_url)
            {
                json_object_object_del(device_info, "web_url");
            }

            json_object *app_url = json_object_object_get(device_info, "app_url");
            if(app_url)
            {
                json_object_object_del(device_info, "app_url");
            }
        }

        char device_info_hash[SIZE_256B];
        const char*hash_message = (char*)json_object_get_string(device_info);
        string_to_hash((unsigned char *)hash_message, strlen(hash_message), 
                        device_info_hash, sizeof(device_info_hash));

        const char *structure_id = json_object_get_string(json_object_object_get(device_info, "structure_id"));

        nest_structure *tmp = g_nest_structure_list;
        nest_device *device = NULL;
        while(tmp)
        {
            if(!strcmp(tmp->structure_id, structure_id))
            {  
                if(!strcmp(device_type, "smoke_co_alarms"))
                {
                    device = tmp->smoke_co_alarms_list;
                }
                else if(!strcmp(device_type, "thermostats"))
                {
                    device = tmp->thermostats_list;
                }
                else if(!strcmp(device_type, "cameras"))
                {
                    device = tmp->cameras_list;
                }
            }
            tmp=tmp->next;
        }

        while(device)
        {
            if(!strcmp(device->device_id, device_id))
            {
                if(!strcmp(device->device_hash, device_info_hash))
                {
                    //the same, ignore
                    break;
                }
                else
                {
                    //got change, update here
                    printf("device %s change\n", device->device_id);
                    callback(device, device_type, device_id, device_info);
                    strcpy(device->device_hash, device_info_hash);
                    break;
                }
            }
            device = device->next;
        }
    }
}

void find_and_update_device(nest_device **device_list, char *device_type,
                        json_object *device_list_array, char *device_list_hash)
{
    int i;
    char new_device_list_hash[SIZE_256B];
    const char*hash_message = (char*)json_object_get_string(device_list_array);
    string_to_hash((unsigned char *)hash_message, strlen(hash_message), 
                    new_device_list_hash, sizeof(new_device_list_hash));

    if(strcmp(device_list_hash, new_device_list_hash))
    {
        int arraylen = json_object_array_length(device_list_array);
        if(!arraylen)
        {
            remove_all_device(device_list);
        }
        else
        {
            nest_device *device = *device_list;
            int *struct_array_device = (int *)malloc(arraylen*sizeof(int)); //checking device is added or not
            memset(struct_array_device, 0x00, arraylen*sizeof(int));

            while(device)
            {
                for(i=0; i < arraylen; i++)
                {
                    json_object *device_id = json_object_array_get_idx(device_list_array, i);
                    if(!strcmp(device->device_id, (char*)json_object_get_string(device_id)))
                    {
                        if(!struct_array_device[i])
                        {
                            struct_array_device[i] = 1;
                        }
                        break;
                    }
                }

                if(i == arraylen)// if not found in json array, remove it
                {
                    nest_device *tmp = device;
                    device = device->next;
                    remove_device(device_list, tmp->device_id);
                }
                else
                {
                    device = device->next;
                }
            }

            for(i=0; i < arraylen; i++)
            {
                if(!struct_array_device[i]) //new device
                {
                    json_object *device_id = json_object_array_get_idx(device_list_array, i);
                    add_device(device_list, device_type, (char*)json_object_get_string(device_id), 1);
                }
            }

            free(struct_array_device);
        }
    }
}

//check device is exist or be removed then update to structure list
void update_device_list(nest_structure *structure, json_object *structure_info)
{
    json_object *smoke_co_alarms_array = json_object_object_get(structure_info, "smoke_co_alarms");
    json_object *thermostats_array = json_object_object_get(structure_info, "thermostats");
    json_object *cameras_array = json_object_object_get(structure_info, "cameras");

    if(smoke_co_alarms_array)
    {
        enum json_type type = json_object_get_type(smoke_co_alarms_array);
        if(type == json_type_array)
        {
            find_and_update_device(&structure->smoke_co_alarms_list,
                                    "smoke_co_alarms", 
                                    smoke_co_alarms_array, 
                                    structure->smoke_co_alarms_array_hash);
        }
    }
    else
    {
        remove_all_device(&structure->smoke_co_alarms_list);
    }

    if(thermostats_array)
    {
        enum json_type type = json_object_get_type(thermostats_array);
        if(type == json_type_array)
        {
            find_and_update_device(&structure->thermostats_list,
                                    "thermostats", 
                                    thermostats_array,
                                    structure->thermostats_array_hash);
        }
    }
    else
    {
        remove_all_device(&structure->thermostats_list);
    }

    if(cameras_array)
    {
        enum json_type type = json_object_get_type(cameras_array);
        if(type == json_type_array)
        {
            find_and_update_device(&structure->cameras_list,
                                    "cameras",
                                    cameras_array,
                                    structure->cameras_array_hash);
        }
    }
    else
    {
        remove_all_device(&structure->cameras_list);
    }
}

void find_structure_change(char *structure_id, json_object *structure_info)
{
    char structure_info_hash[SIZE_256B];
    const char* hash_message = (char*)json_object_get_string(structure_info);
    string_to_hash((unsigned char *)hash_message, strlen((char *)hash_message), 
                    structure_info_hash, sizeof(structure_info_hash));

    nest_structure *tmp_struct = g_nest_structure_list;
    while(tmp_struct)
    {
        if(!strcmp(tmp_struct->structure_id, structure_id))
        {
            tmp_struct->structure_exist = 0; //for checking structure has been exist or not
            if(!strcmp(tmp_struct->structure_hash, structure_info_hash))
            {
                // the same
                break;    
            }
            else
            {
                printf("structure %s got change\n", structure_id);
                update_device_list(tmp_struct, structure_info);
                strcpy(tmp_struct->structure_hash, structure_info_hash);
                json_object_object_foreach(structure_info, key, val)
                {
                    printf("%s: %s\n", key, json_object_get_string(val));
                }
                break;
            }
        }
        tmp_struct = tmp_struct->next;
    }

    if(!tmp_struct)
    {
        add_nest_structure(structure_id, structure_info_hash, structure_info, 0);
    }
}

size_t nest_event(void *ptr, size_t size, size_t nmemb)
{
    char *receive_data = (char*)ptr;
    if(g_receive_data)
    {
        g_receive_data = realloc(g_receive_data, strlen(g_receive_data)+size*nmemb);
        strcat(g_receive_data, receive_data);
    }
    else
    {
        g_receive_data = malloc(size*nmemb);
        strcpy(g_receive_data, receive_data);
    }

    if(receive_data[size*nmemb-1] == '\n')
    {
        // printf("g_receive_data = %s\n", g_receive_data);
        if(!g_receive_data)
        {
            return size*nmemb; 
        }

        json_object* jobj = VR_(create_json_object)(g_receive_data);
        if(jobj)
        {
            printf("%s\n", json_object_get_string(json_object_object_get(jobj, "error")));
            json_object_put(jobj);
        }
        else
        {
            char event[32];
            char *data = (char *)malloc(strlen(g_receive_data));
            memset(data, 0x00, strlen(g_receive_data));
            parse_event(g_receive_data, event, data);
            // printf("event = %s\n", event);
            // printf("data = %s\n", data);
            if(strncmp(data, "null", 4))
            {
                json_object* nest_data = VR_(create_json_object)(data);
                if(nest_data)
                {
                    json_object* alldata = json_object_object_get(nest_data, "data");
                    if(alldata)
                    {
                        json_object* devices = json_object_object_get(alldata, "devices");
                        json_object* structures = json_object_object_get(alldata, "structures");
                        
                        if(structures)
                        {
                            if(hash_diff(structures, g_structures_nest_hash))
                            {
                                json_object_object_foreach(structures, structure_id, structure_info)
                                {
                                    find_structure_change(structure_id, structure_info);
                                }

                                nest_structure *list_structure = g_nest_structure_list;
                                while(list_structure)
                                {
                                    if(list_structure->structure_exist == 1)
                                    {
                                        nest_structure *tmp = list_structure;
                                        list_structure=list_structure->next;
                                        remove_nest_structure(tmp->structure_id);
                                    }
                                    else
                                    {
                                        list_structure->structure_exist = 1;//re_set to exist
                                        list_structure=list_structure->next;
                                    }
                                }

                                display_all_list(g_nest_structure_list);
                            }
                        }

                        if(devices)
                        {
                            // //if devices got news, let check it
                            if(hash_diff(devices, g_devices_nest_hash))
                            {
                                json_object* thermostats = json_object_object_get(devices, "thermostats");
                                json_object* cameras = json_object_object_get(devices, "cameras");
                                find_device_change("thermostats", thermostats, find_info_change);
                                find_device_change("cameras", cameras, find_info_change);
                            }
                        }

                        // display_all_list(g_nest_structure_list);
                    }
                    else
                    {
                        SLOGE("Failed to get data structures");
                    }
                    json_object_put(nest_data);
                }
                else
                {
                    SLOGE("Failed to paser json data: %s\n", data);
                } 
            }
            free(data);
        }

        free(g_receive_data);
        g_receive_data = NULL;
    }

    return size*nmemb;
}


void nest_monitor_process(void *data)
{
    //https://developer-api.nest.com/?auth=c.sGpaMVP0aJJ6Ixc8nDmQPohOWmhEN2bF96xGkZslq5RjDCINPnGYmUP0CoHt299jwcKkOC360fxl5qjbqPt1fHm9zqk33hMcGB9jJB71dhvHDMkogPEJQALny0be5u62KcwHIxacZxKLiNuj
    char *token = (char*)data;
    if(token)
    {
        char *url = (char*)malloc(strlen(token) + 256);
        sprintf(url, "https://developer-api.nest.com/?auth=%s", token);

        CURL *curl;
        CURLcode res;
        curl_global_init(CURL_GLOBAL_DEFAULT);
        curl = curl_easy_init();
        if(curl) 
        {
            struct curl_slist *headers = NULL;
            headers = curl_slist_append(headers, "Accept:text/event-stream");

            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, nest_event);
            curl_easy_setopt(curl, CURLOPT_URL, url);
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
            curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
            curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 1L);
            curl_easy_setopt(curl, CURLOPT_CAPATH, CERTIFICATE_PATH);
            curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
            /* Perform the request, res will get the return code */ 
            res = curl_easy_perform(curl);

            if(res != CURLE_OK)
            {
                fprintf(stderr, "curl_easy_perform() failed: %s\n",
                      curl_easy_strerror(res));
            }
            
            curl_easy_cleanup(curl);
        }
        curl_global_cleanup();
        
        free(token);
        free(url);
    }
}

void VR_(handle_signal)(int signo)
{
    SLOGI("########### GOT SIGNAL %d\n #############", signo);
    if(signo == SIGSEGV)
    {
        uint32_t *up = (uint32_t *) &signo;
        pmortem_connect_and_send(up, 8 * 1024);
        fprintf(stderr, "SIGSEGV [%d].  Best guess fault address: %08x, ra: %08x, sig return: %p\n",
            signo, up[8], up[72], __builtin_return_address(0));
    }
    
    uloop_end();
    exit(1);
}

int main()
{
    SLOG_Init(SLOG_STDOUT, NULL);
    set_thread_name( "rule_handler" );
    struct sigaction action;

    char cmd[SIZE_256B];
    sprintf(cmd, "%s", "pgrep rule_handler | head -n -1 | cut -c-5 | xargs kill -9 &>/dev/null");
    system(cmd);

    memset(&action, 0, sizeof(action));
    action.sa_flags = (SA_NOCLDSTOP | SA_NOCLDWAIT | SA_RESTART);
    action.sa_handler = VR_(handle_signal);
    sigaction(SIGSEGV, &action, NULL);
    sigaction(SIGINT, &action, NULL);
    sigaction(SIGTERM, &action, NULL);
    sigaction(SIGKILL, &action, NULL);

    char buffer[2048];

    memset(buffer, 0, sizeof(buffer));

    pthread_mutex_init(&nest_listMutex, 0);

    open_database(RULE_DATABASE, &rule_db);

    SEARCH_DATA_INIT_VAR(token);
    searching_database("rule_handler", rule_db, get_last_data_cb, &token, 
                        "SELECT token from NEST where status='%s'", ST_ACTIVE);
    if(token.len)
    {
        pthread_t nest_monitor_thread;
        pthread_create(&nest_monitor_thread, NULL, (void *)&nest_monitor_process, (void*)token.value);
        pthread_detach(nest_monitor_thread);
    }
    else
    {
        FREE_SEARCH_DATA_VAR(token);
    }

    init_ubus_service();
    rule_ubus_service();
    free_ubus_service();
    return 0;
}


