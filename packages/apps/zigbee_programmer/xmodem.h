#ifndef _XMODEM_H_
#define _XMODEM_H_

#include <unistd.h>
#define SOH 0x01
#define STX 0x02
#define ACK 0x06
#define NAK 0x15
#define EOT	0x04
#define CAN	0x18	

#define MAXERRORS 10    /* max number of times to retry */
#define SECSIZE   128   /* CP/M sector, transmission block */
#define CPMEOF    26    /* End Of File (for CP/M) */
#define BS        8		/* Backspace */
#endif