#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include "xmodem.h"
#include "serial.h"
#include "gpio_intf.h"


#define SPEED                                       115200
#define READ_BINARY                                 "rb"
#define WRITE_BINARY                                "wb"
#define NULLFILE                                    NULL

#define ZIGBEE_CONSOLE_PORT_DEFAULT                 "/dev/ttyO4"
#define ZIGBEE_DEFAULT_FIRMWARE_PATH                "/lib/firmware/zigbee"
#define ZIGBEE_RECV_FIRMWARE_DEFAULT_NAME           "ETRX357_R309C.ebl"
#define ZIGBEE_SEND_FIRMWARE_DEFAULT_NAME           "FiveEndPointDev.ebl"
#define GPIO_PIN_CTRL_NUM                           48 
#define GPIO_PIN_RST_NUM                            60
#define GPIO_PIN_BOOTM_NUM                          45

int fd;
static void update_crc (uint8_t c, uint8_t *crc1,uint8_t *crc2);
static void error ();

void controller_enable() {
	/*
	 * Enable GPIO pins
	 */
	if (-1 == GPIOExport(GPIO_PIN_CTRL_NUM) || -1 == GPIOExport(GPIO_PIN_RST_NUM) || -1 == GPIOExport(GPIO_PIN_BOOTM_NUM))
		return;

	/*
	 * Set GPIO directions
	 */
	if (-1 == GPIODirection(GPIO_PIN_CTRL_NUM, OUT) || -1 == GPIODirection(GPIO_PIN_RST_NUM, OUT) || -1 == GPIODirection(GPIO_PIN_BOOTM_NUM, OUT))
		return;

	/* 
	 * Write data
	 */
	if (-1 == GPIOWrite(GPIO_PIN_CTRL_NUM, 0))
		printf("Can't write to CTRL PIN:%d\n", GPIO_PIN_CTRL_NUM);
	if (-1 == GPIOWrite(GPIO_PIN_BOOTM_NUM, 0))
		printf("Can't write to BOOTMODE PIN:%d\n", GPIO_PIN_BOOTM_NUM);
	if (-1 == GPIOWrite(GPIO_PIN_RST_NUM, 0))
		printf("Can't write to RST PIN:%d\n", GPIO_PIN_RST_NUM);
	sleep(1);
	if (-1 == GPIOWrite(GPIO_PIN_RST_NUM, 1))
		printf("Can't write to RST PIN:%d\n", GPIO_PIN_RST_NUM);

}

void controller_reset() {
	/*
	 * Disable GPIO pins
	 */
	if (-1 == GPIOUnexport(GPIO_PIN_CTRL_NUM) || -1 == GPIOUnexport(GPIO_PIN_RST_NUM) || -1 == GPIOUnexport(GPIO_PIN_BOOTM_NUM))
		return;
}

void sig_handler(int signo)
{
	if (signo == SIGINT)
	 	controller_reset();
	exit(EXIT_SUCCESS);
}

unsigned long fsize(char* file)
{
    FILE * f = fopen(file, "r");
    fseek(f, 0, SEEK_END);
    unsigned long len = (unsigned long)ftell(f);
    fclose(f);
    return len;
}

int xm_sendfile(char *tfile)
{
    FILE *fp;
    uint8_t chr, thechecksum, block, sector[SECSIZE];
    uint8_t crc1, crc2, mode, errcount, errcount2, two_can;
    int i,nbytes,speed=SPEED;
    long size,min,sec;

    if ((size = fsize (tfile)) == -1)
    {
        printf("xmodem: Can't open '%s' for read\n", tfile);
        return 1;
    }
    if ((fp = fopen (tfile, READ_BINARY)) == NULLFILE)
    {
        printf("xmodem: Can't open '%s' for read\n", tfile);
        return 1;
    }

    size = (size / 128L) + 1L;
    sec = size * 128L * 15L / speed;
    min = sec / 60L;
    sec = sec - min * 60L;
    printf("\nFile open: %ld records\n", size);
    printf("Send time: %ld min, %ld sec at %d baud\n", min, sec, speed);
    printf("To cancel: use CTRL-X numerous times\n");
    printf("Waiting receive ready signal\n");

    usleep(3000);

    errcount = 0;
    mode = 0;
    two_can = 0;
    block = 1;

    while (errcount<MAXERRORS)
    {
        getchar_t(fd,&chr);
        if (chr==NAK)
        {
            //checksum mode
            break;
        }

        if (chr=='C')
        {
            //CRC mode
            mode=1;
            break;
        }

        if (chr==CAN)
        {
            if (two_can)
            {
                printf("\nxmodem: Abort request received\n");
                (void) fclose (fp);
                return 1;
            }
            two_can=1;
        }else
        {
            two_can=0;
        }
        errcount++;
    }

    if(errcount>=MAXERRORS)
    {
        usleep(3000);
        printf("xmodem:Time out on ackknowledge\n");
        (void)fclose(fp);
        return 1;
    }
    two_can=0;

    while ((nbytes=(int)fread(sector,1,128,fp))!=0)
    {
        if (nbytes < SECSIZE)
        {
            /* fill short sector */
            for (i = nbytes; i < SECSIZE; i++)
                sector[i] = CPMEOF;
        }
        errcount=0;
        while (errcount<MAXERRORS)
        {
            putchar_t(fd,SOH);    //put the header
            putchar_t(fd,block);  //put block number
            chr=~block;     //put complement
            putchar_t(fd,chr);
            thechecksum=0;
            crc1=0;
            crc2=0;
            for (i=0;i<SECSIZE;i++)
            {
                putchar_t(fd,sector[i]);
                if (mode)
                {
                    update_crc(sector[i],&crc1,&crc2);
                }else
                {
                    thechecksum+=sector[i];
                }
            }
            if (mode)
            {
                update_crc (0, &crc1, &crc2);
                update_crc (0, &crc1, &crc2);
                putchar_t(fd,crc1);
                putchar_t(fd,crc2);
            }else
            {
                putchar_t(fd,thechecksum);
            }
            errcount2 = 0;

rec_loop:
            getchar_t(fd,&chr);
            if (chr==CAN)
            {
                if (two_can)
                {
                    printf("\nxmodem:Abort request received\n");
                    (void)fclose(fp);
                    return 1;
                }
                two_can=1;
            }else
            {
                two_can=0;
            }

            if(chr==ACK)
            {
                break;  //got it
            }

            if (chr!=NAK)
            {
                ++errcount2;
                if (errcount2>=MAXERRORS)
                {
                    printf("Noise on line\n");
                    (void) fclose(fp);
                    return 1;
                }
                goto rec_loop;
            }
            errcount++;
        }
        if (errcount>=MAXERRORS)
        {
            error();
            (void)fclose(fp);
            return 1;
        }
        block++;
    }

    errcount=0;
    while (errcount<MAXERRORS)
    {
        putchar_t(fd,EOT);
        getchar_t(fd,&chr);
        if (chr==ACK)
        {
            (void)fclose(fp);
            usleep(6000);
            printf("Xmodem: Download - %s", tfile);
            printf("Xmodem: File sent OK\n");
            return (0);
        }
        errcount++;
    }

    (void)fclose(fp);
    usleep(3000);
    error();
    return 1;
}

static int xm_recvfile(char *tfile)
{
    FILE *fp;
    uint8_t hdr,blk,cblk,tmp,thechecksum=0,crc1,crc2;
    uint8_t c1=0,c2=0,sum,block,sector[SECSIZE];
    uint8_t first,mode=0,errcount,two_can;
    int i;

    if (access(tfile,F_OK) != -1)
    {
        printf("xmodem: File %s already exists\n", tfile);
        return (1);
    }

    if ((fp = fopen (tfile, WRITE_BINARY)) == NULLFILE) {
        printf("xmodem: Can't open '%s' for write\n", tfile);
        return (1);
    }
    printf("File open - ready to receive\n");
    printf("To cancel: use CTRL-X numerous times\n");
    usleep(3000);

    errcount = 0;
    block = 1;
    first = 0;
    two_can = 0;

    usleep(3000);

    while (errcount<MAXERRORS)
    {
        if (errcount<(MAXERRORS/2))
        {
            putchar_t(fd,'C');
            mode=1;
        }else
        {
            putchar_t(fd,NAK);
            mode=0;
        }

        getchar_t(fd,&hdr);
        if (hdr==SOH)
        {
            first=1;
            break;
        }

        if (hdr==CAN)
        {
            if (two_can)
            {
                usleep(3000);
                printf("\nxmodem: Abort request received\n");
                (void) fclose (fp);
                unlink (tfile);
                return (1);
            }
            two_can=1;
        }else
        {
            two_can=0;
        }
        errcount++;
    }

    if (errcount >= MAXERRORS)
    {
        usleep(3000);
        printf("\nxmodem: Timed out on acknowledge\n");
        (void) fclose (fp);
        unlink (tfile);
        return (1);
    }
    errcount = 0;
    two_can = 0;

    while(errcount<MAXERRORS)
    {
        if(first)
        {
            hdr=SOH;
            first=0;
        }else
        {
            getchar_t(fd,&hdr);
        }

        if(hdr==CAN)
        {
            if (two_can)
            {
                usleep(3000);
                printf("\nxmodem: Abort request received\n");
                (void) fclose (fp);
                unlink (tfile);
                return (1);
            }
            two_can=1;
            continue;
        }else
        {
            two_can=0;
        }

        if (hdr==EOT) //done
        {
            break;
        }

        if (hdr!=SOH)
        {
            //read in junk for 6 seconds
            int atime=6000;
            uint8_t junk;
            while (atime-->0)
            {
                usleep(1000);
                getchar_t(fd,&junk);
            }
            first=0;
            goto nak;
        }
        getchar_t(fd,&blk);   //block number
        getchar_t(fd,&cblk);  //complement of block number
        crc1=0;
        crc2=0;
        sum=0;

        for (i=0;i<SECSIZE;i++)
        {
            getchar_t(fd,&sector[i]);
            if (mode)
            {
                update_crc(sector[i],&crc1,&crc2);
            }else
            {
                sum+=sector[i];
            }
        }

        if (mode)
        {
            getchar_t(fd,&c1);
            getchar_t(fd,&c2);
        }else
        {
            getchar_t(fd,&thechecksum);
        }

        if (blk != block && blk != (block - 1))
        {
            goto nak;
        }
        tmp = ~blk;
        if (cblk != tmp)
        {
            goto nak;
        }

        if (mode)
        {
            update_crc (0, &crc1, &crc2);
            update_crc (0, &crc1, &crc2);
            if (c1 != crc1 || c2 != crc2)
            {
                goto nak;
            }
        }else
        {
            if (thechecksum != sum)
            {
                goto nak;
            }
        }

        if (block==blk)
        {
            (void) fflush (fp);
            if (fwrite (sector, sizeof (sector[0]), SECSIZE, fp) != SECSIZE)
            {
                error();
                printf("         File write error - Partial file deleted\n");
                (void) fclose (fp);
                unlink (tfile);
                return 1;
            }
        }
        block=blk+1;
        putchar_t(fd,ACK);
        errcount=0;
        continue;

nak:
        putchar_t(fd,NAK);
        //do it over

        errcount++;
    }

    if (errcount == MAXERRORS)
    {
        error();
        (void) fclose (fp);
        unlink (tfile);
        return 1;
    }
    putchar_t(fd,ACK);
    usleep(3000);
    (void) fclose (fp);
    printf("Xmodem: Upload - %s", tfile);
    printf("Xmodem: File received OK\n");
    return (0);

}
/* exceeded the maximum number of retry's */
static void error()
{
    int i;
    for (i=0;i<9;i++)
    {
        putchar_t(fd,CAN);
    }
    usleep(1000);
    for (i=0;i<9;i++)
    {
        putchar_t (fd,BS);
    }
    usleep(3000);

    printf("\nxmodem: Exceeded error limit...aborting\n");
    return;
}
/* update the CRC bytes */

static void update_crc (unsigned char c, unsigned char *crc1, unsigned char *crc2)
{
    register int i, temp;
    register unsigned char carry, c_crc1, c_crc2;

    for (i = 0; i < 8; i++)
    {
        temp = c * 2;
        c = (uint8_t)(temp);    /* rotate left */
        carry = ((temp > 255) ? 1 : 0);
        temp = *crc2 * 2;
        *crc2 = (uint8_t)(temp);
        *crc2 |= carry; /* rotate with carry */
        c_crc2 = ((temp > 255) ? 1 : 0);
        temp = *crc1 * 2;
        *crc1 = (uint8_t)(temp);
        *crc1 |= c_crc2;
        c_crc1 = ((temp > 255) ? 1 : 0);
        if (c_crc1) {
            *crc2 ^= 0x21;
            *crc1 ^= 0x10;
        }
    }
    return;
}
void *reader(void *unused)
{
    int                 r;
    uint8_t             c[1];
    while (1)
    {
        r=read(fd,c,1);
        if (r==1)
        {
            printf("%c",c[0]);
            fflush(stdout);
            continue;
        }else
        {
            usleep(100);
        }

    }

    /* Never reached. */
    return NULL;
}
int main(int argc, char * const argv[])
{
    char ch=0;
    pthread_t   thread_reader;

    int c;
    char *pdevID = NULL;
    char *pfmPath = NULL;
    controller_enable();
    sleep(2);
     while(1) {
          	static struct option long_options[] =
          	{
            		/* These options don’t set a flag.
               		We distinguish them by their indices. */
            		{"help",  no_argument,       0, 'h'},
            		{"port",  required_argument, 0, 'p'},
            		{"firmware",  required_argument, 0, 'f'},
            		{0, 0, 0, 0}
          	};
          	/* getopt_long stores the option index here. */
          	int option_index = 0;

           	c = getopt_long (argc, argv, "hp:f:",
                          long_options, &option_index);

          	/* Detect the end of the options. */
          	if (c == -1)
                  	break;

          	switch(c) {
                  case 'h':
                      printf("Usage: zigbee-programmer [options]\n");
                      printf("Options:\n");
                      printf("--help(-h): print help information\n");
                      printf("--port(-p): specify the dedicated port to control zigbee controller.\n");
                      printf("--firmware(-f): specify the dedicated firmware path to flash to zigbee controller.\n");
                      printf("Without options, the default port(%s) is used to connect to zigbee controller\n", ZIGBEE_CONSOLE_PORT_DEFAULT);
                      printf("and the default firmware path(%s) is used to program zigbee controller\n", ZIGBEE_DEFAULT_FIRMWARE_PATH);
                      exit(EXIT_SUCCESS);
                  case 'p':
			  pdevID = malloc(strlen(optarg));
                          strncpy(pdevID, optarg, strlen(optarg));
                      break;
		  case 'f':
                          pfmPath = malloc(strlen(optarg));
                          strncpy(pfmPath, optarg, strlen(optarg));
                      break;

                  default:
                      printf("Invalid option, please use --help or -h to know how to use\n");
                      exit(EXIT_SUCCESS);
                      break;
          	}
      	}

     if (pdevID == NULL){
	pdevID = malloc(strlen(ZIGBEE_CONSOLE_PORT_DEFAULT));
	strncpy(pdevID, ZIGBEE_CONSOLE_PORT_DEFAULT, strlen(ZIGBEE_CONSOLE_PORT_DEFAULT));
     }

    //printf("open port:%s\n", pdevID);
    fd = open (pdevID, O_RDWR | O_NOCTTY | O_SYNC);

    if (fd < 0)
    {
        printf  ("error %d opening %s: %s\n", errno, "O4", strerror (errno));
        return 1;
    }
    set_interface_attribs (fd,B115200, 0,1);

    if (signal(SIGINT, sig_handler) == SIG_ERR)
	printf("\ncan't catch SIGINT\n");
    //write(fd,"\n",1);
    //sleep(1);
    //write(fd,"1\n",2);
    //sleep(2);
    int exit_return=0;
    char cmd[256];


    exit_return = pthread_create(&thread_reader, NULL, reader, NULL);
    if (exit_return) {
        printf("Cannot create reader thread: %s.\n", strerror(exit_return));
        exit(1);
    }
  
    putbuffer_t(fd,(uint8_t*)"m\r\n",3);
    sleep(1);
    putbuffer_t(fd,(uint8_t*)"1",1);
    sleep(3);
    pthread_cancel(thread_reader);
    if (pfmPath == NULL) 
    {
        pfmPath = malloc(strlen(ZIGBEE_DEFAULT_FIRMWARE_PATH) + strlen(ZIGBEE_RECV_FIRMWARE_DEFAULT_NAME) + 3);
        sprintf(pfmPath,"%s/%s",ZIGBEE_DEFAULT_FIRMWARE_PATH,ZIGBEE_RECV_FIRMWARE_DEFAULT_NAME);
    }
    exit_return = xm_sendfile (pfmPath);
    putbuffer_t(fd,(uint8_t*)"2",1);
    sleep(1);
    free(pfmPath);
    // while (ch!='Q')
    // {
    //     switch (ch)
    //     {

    //             case 'r':
    //             case 'R':
		  //   sprintf(cmd,"%s/%s",ZIGBEE_DEFAULT_FIRMWARE_PATH, ZIGBEE_RECV_FIRMWARE_DEFAULT_NAME);
		  //   //printf("receive at file:%s\n", cmd);
		  //   exit_return = xm_recvfile (cmd);
    //                 break;
    //             case 's':
    //             case 'S':
    //                 // putbuffer_t(fd,(uint8_t*)"m\r\n",3);
    //                 // sleep(1);
    //                 // putbuffer_t(fd,(uint8_t*)"1\r\n",3);
    //                 // sleep(10);
    //                 pthread_cancel(thread_reader);
    //     		    if (pfmPath == NULL) 
    //                 {
    //                     pfmPath = malloc(strlen(ZIGBEE_DEFAULT_FIRMWARE_PATH) + strlen(ZIGBEE_RECV_FIRMWARE_DEFAULT_NAME) + 3);
    //                     sprintf(pfmPath,"%s/%s",ZIGBEE_DEFAULT_FIRMWARE_PATH,ZIGBEE_RECV_FIRMWARE_DEFAULT_NAME);
    //     		    }
		  //           printf("send fw file:%s\n", pfmPath);
    //                 exit_return = xm_sendfile(pfmPath);
    //                 sleep(3);
    //                 putbuffer_t(fd,(uint8_t*)"2\r\n",3);
    //                 pthread_create(&thread_reader, NULL, reader, NULL);
    //                 break;
    //             case 'b':
    //             case 'B':
    //                 putbuffer_t(fd,(uint8_t*)"m\r\n",3);
    //                 sleep(1);
    //                 putbuffer_t(fd,(uint8_t*)"1\r\n",3);
    //                 sleep(3);
    //                 pthread_cancel(thread_reader);
    //                 sleep(2);
    //                 if (pfmPath == NULL) 
    //                 {
    //                     pfmPath = malloc(strlen(ZIGBEE_DEFAULT_FIRMWARE_PATH) + strlen(ZIGBEE_RECV_FIRMWARE_DEFAULT_NAME) + 3);
    //                     sprintf(pfmPath,"%s/%s",ZIGBEE_DEFAULT_FIRMWARE_PATH,ZIGBEE_RECV_FIRMWARE_DEFAULT_NAME);
    //                 }
    //                 printf("send fw file:%s\n", pfmPath);
    //                 exit_return = xm_sendfile(pfmPath);
    //                 sleep(3);
    //                 putbuffer_t(fd,(uint8_t*)"2\r\n",3);
    //                 pthread_create(&thread_reader, NULL, reader, NULL);
    //             case '1':
    //                 write(fd,"1",1);
    //                 break;
    //             case '2':
    //                 write(fd,"2",1);
    //                 break;
    //             case '3':
    //                 write(fd,"3",1);
    //                 break;
    //             default:
    //                 write(fd,"\n",1);
    //                 break;
    //     }
    //     printf("Command>>> ");
    //     //ch=toupper(getchar());
    //     scanf("%s",cmd);
    //     ch=toupper(cmd[0]);
    // }

    controller_reset();
    return 0;
}
