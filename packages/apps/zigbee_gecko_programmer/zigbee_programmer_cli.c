#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include "libverik-utils-serial.h"
#include "gpio_intf.h"
#include "timing.h" // contain initTimer

#include "fix-bug-build-code.h"
#include "handle-command.h"


#define SPEED 115200
#define READ_BINARY "rb"
#define WRITE_BINARY "wb"
#define NULLFILE NULL

#define ZIGBEE_CONSOLE_PORT_DEFAULT "/dev/ttyO4"
#define ZIGBEE_DEFAULT_FIRMWARE_PATH "/lib/firmware/zigbee"
#define ZIGBEE_RECV_FIRMWARE_DEFAULT_NAME "ncp-uart-rts-cts-use-with-serial-uart-btl-5.10.1.gbl"
#define ERROR_STATUS -1


/*how to use
enter: zigbee_gecko_programmer -p /dev/ttyO4 -f /lib/firmware/zigbee/ncp-uart-rts-cts-use-with-serial-uart-btl-5.10.1.gbl*/
int main(int argc, char *const argv[])
{
    //kill zigbee gecko
    char cmd[256];
    memset(cmd, 0x00, sizeof(cmd));
    sprintf(cmd, "%s", "pgrep zigbee | head -n -1 | cut -c-5 | xargs kill -9 &>/dev/null");
    system(cmd);

    int c;
    char *pdevID = NULL;
    char *pfmPath = NULL;

    while (1)
    {
        static struct option long_options[] =
            {
                /* These options don’t set a flag.
                    We distinguish them by their indices. */
                {"help", no_argument, 0, 'h'},
                {"port", required_argument, 0, 'p'},
                {"firmware", required_argument, 0, 'f'},
                {0, 0, 0, 0}};
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long(argc, argv, "hp:f:",
                        long_options, &option_index);

        /* Detect the end of the options. */
        if (c == ERROR_STATUS)
            break;

        switch (c)
        {
        case 'h':
            printf("Usage: zigbee-programmer [options]\n");
            printf("Options:\n");
            printf("--help(-h): print help information\n");
            printf("--port(-p): specify the dedicated port to control zigbee controller.\n");
            printf("--firmware(-f): specify the dedicated firmware path to flash to zigbee controller.\n");
            printf("Without options, the default port(%s) is used to connect to zigbee controller\n", ZIGBEE_CONSOLE_PORT_DEFAULT);
            printf("and the default firmware path(%s) is used to program zigbee controller\n", ZIGBEE_DEFAULT_FIRMWARE_PATH);
            exit(EXIT_SUCCESS);
        case 'p':
            pdevID = malloc(strlen(optarg));
            strncpy(pdevID, optarg, strlen(optarg));
            break;
        case 'f':
            pfmPath = malloc(strlen(optarg));
            strncpy(pfmPath, optarg, strlen(optarg));
            break;

        default:
            printf("Invalid option, please use --help or -h to know how to use\n");
            exit(EXIT_SUCCESS);
            break;
        }
    }

    if (pdevID == NULL)
    {
        pdevID = malloc(strlen(ZIGBEE_CONSOLE_PORT_DEFAULT));
        strncpy(pdevID, ZIGBEE_CONSOLE_PORT_DEFAULT, strlen(ZIGBEE_CONSOLE_PORT_DEFAULT));
    }
    if (pfmPath == NULL)
    {
        pfmPath = malloc(strlen(ZIGBEE_DEFAULT_FIRMWARE_PATH) + strlen(ZIGBEE_RECV_FIRMWARE_DEFAULT_NAME) + 3);
        sprintf(pfmPath, "%s/%s", ZIGBEE_DEFAULT_FIRMWARE_PATH, ZIGBEE_RECV_FIRMWARE_DEFAULT_NAME);
    }
    zigbeeProgrammerUpdateFirmware(pdevID, pfmPath);
    
    return 0;
}