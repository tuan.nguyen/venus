#ifndef _UTIL_INTELHEX_H_
#define _UTIL_INTELHEX_H_

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define flashMem_length 0x20000
#define BLANK_VALUE     0xFF
extern uint8_t flashMem[flashMem_length];

// Public APIs
int intelHex_loadfile(char *filename);


#endif
