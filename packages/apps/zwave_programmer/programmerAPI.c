#include "programmerAPI.h"
#include "util_crc.h"

uint8_t bNVRArray[256];
int fd;

int ZWPGM_enableInterface(void)
{
	uint8_t request[]= { PROG_EN_0_BYTE, PROG_EN_1_BYTE, PROG_EN_2_BYTE, PROG_EN_3_BYTE };
	uint8_t response[4];
	int response_length;

	memset(response, 0x00, 4);
	putbuffer_t(fd,request, 4);
	response_length = getbuffer_t(fd,response, 4);

	if((response_length == 2 || response_length == 4)
		 && (response[0] == PROG_EN_2_BYTE && response[1] == PROG_EN_3_BYTE))
	{

		return 0;
	}
	else
	{
		return -1;
	}
}

int ZWPGM_readSignature(void)
{
	uint8_t i;
	uint8_t count_Sig = 0;
	uint8_t request[]= { READ_SIG_FLASH_0_BYTE, 0x00, READ_SIG_FLASH_2_BYTE, READ_SIG_FLASH_3_BYTE };
	uint8_t response[4];
	int response_length;

	for (i = 0; i < NO_OF_SIGNATURE; i++)
	{
		memset(response, 0x00, sizeof(response));
		request[1]=i;
		putbuffer_t(fd,request, 4);

		response_length = getbuffer_t(fd,response,4);
		if ((response_length == 4)
                    &&(response[0] == READ_SIG_FLASH_0_BYTE)
                    && (response[1] == i)
                    && (response[2] == READ_SIG_FLASH_2_BYTE))
		{
			count_Sig++;
		}
	}
	if(count_Sig == NO_OF_SIGNATURE)
	{
		return 0;
	}else
	{
		return -1;
	}
}

int ZWPGM_checkState(void)
{

	uint8_t request[]= { CHK_STATE_0_BYTE, CHK_STATE_1_BYTE, CHK_STATE_2_BYTE, CHK_STATE_3_BYTE };
	uint8_t response[4];

	putbuffer_t(fd,request, 4);
	int response_length;
	response_length = getbuffer_t(fd,response,4);

	if ((response_length == 4)
		&& (response[0] == CHK_STATE_0_BYTE)
		&& (response[1] == CHK_STATE_1_BYTE)
		&& (response[2] == CHK_STATE_2_BYTE))
    {
		return response[3];
    }
    return -1;
}

int ZWPGM_readNVramByte(uint8_t index)
{
	if(index < 9 || index > 255)
	{
		printf("ZWPGM_readNVramByte: wrong index = %d\n", index);
		return -1;
	}

	int response_length;
	uint8_t request[]= { NVR_READ_0_BYTE, NVR_READ_1_BYTE, index, NVR_READ_3_BYTE };
	uint8_t response[4];

	putbuffer_t(fd,request, 4);
	response_length = getbuffer_t(fd,response, 4);

	if((response_length == 4)
		&& (response[0] == NVR_READ_0_BYTE)
		&& (response[1] == NVR_READ_1_BYTE))
	{
		return response[3];
	}
	return -1;
}

int ZWPGM_writeNVramByte(uint8_t index, uint8_t input)
{
	if(index < 9 || index > 255)
	{
		printf("ZWPGM_writeNVramByte: wrong index = %d\n", index);
		return -1;
	}

	int response_length;
	uint8_t request[]= { NVR_SET_0_BYTE, NVR_SET_1_BYTE, index, input};
	uint8_t response[4];
	uint8_t retry=NO_OF_RETRY;
	int 	ret_state;

	if(1)
	{
		putbuffer_t(fd,request, 4);
		response_length = getbuffer_t(fd,response, 4);
		if((response_length == 4)
			&& (response[0] == NVR_SET_0_BYTE)
			&& (response[1] == NVR_SET_1_BYTE)
			&& (response[2] == index)
			&& (response[3] == input))

		{
			while (retry-->0)
			{
				usleep(100);
				ret_state=ZWPGM_checkState();
				if ((ret_state!=-1)&&(!(ret_state&BIT3)))
				{
					return 0;
				}
			}
		}
	}
	return -1;
}


int ZWPGM_readSramByte(uint16_t addr)
{
	int response_length;
	uint8_t request[]={READ_SRAM_1_BYTE,0x00,0x00,0x00};
	uint8_t response[4];

	request[1]=(uint8_t)((addr>>8) & 0xff);
	request[2]=(uint8_t)((addr>>0) & 0xff);
	request[3]=0x00;

	putbuffer_t(fd,request, 4);
	response_length=getbuffer_t(fd,response, 4);
	if((response_length == 4)
			&& (response[0] == READ_SRAM_1_BYTE))
	{
		return response[3];
	}
	return -1;
}



int ZWPGM_writeSramByte(uint16_t addr,uint8_t input)
{
	int response_length;
	uint8_t request[]={WRITE_SRAM_1_BYTE,0x00,0x00,0x00};
	uint8_t response[4];
	request[1]=(uint8_t)((addr>>8) & 0xff);
	request[2]=(uint8_t)((addr>>0) & 0xff);
	request[3]=input;

	putbuffer_t(fd,request, 4);
	response_length=getbuffer_t(fd,response, 4);
	if((response_length == 4)
			&& (response[0] == WRITE_SRAM_1_BYTE))
	{
		return 0;
	}
	return -1;
}

int ZWPGM_continueRead(uint8_t* output)
{
	int response_length;
	uint8_t request[]={CONT_READ_MEM_0_BYTE,0x00,0x00,0x00};
	uint8_t response[4];


	putbuffer_t(fd,request, 4);
	response_length=getbuffer_t(fd,response, 4);
	if((response_length == 4)
			&& (response[0] == CONT_READ_MEM_0_BYTE))
	{
		output[0]=response[1];
		output[1]=response[2];
		output[2]=response[3];
		return 0;
	}

	return -1;
}

int ZWPGM_continueWrite(uint8_t* input)
{
	int response_length;
	uint8_t request[]={CONTINUE_WRITE_1_BYTE,0x00,0x00,0x00};
	uint8_t response[4];

	request[1]=input[0];
	request[2]=input[1];
	request[3]=input[2];

	putbuffer_t(fd,request, 4);
	response_length=getbuffer_t(fd,response, 4);
	if((response_length == 4)
			&& (response[0] == CONTINUE_WRITE_1_BYTE))
	{
		return 0;
	}

	return -1;
}

int ZWPGM_eraseChip(void)
{
	int response_length;
	int ret_state;
	uint8_t request[]= { ERASE_CHIP_0_BYTE, ERASE_CHIP_1_BYTE, ERASE_CHIP_2_BYTE, ERASE_CHIP_3_BYTE };
	uint8_t response[4];
	uint8_t retry=NO_OF_RETRY;

	putbuffer_t(fd,request, 4);
	response_length = getbuffer_t(fd,response, 4);

	if((response_length == 4)
		&& (response[0] == ERASE_CHIP_0_BYTE)
		&& (response[1] == ERASE_CHIP_1_BYTE)
		&& (response[2] == ERASE_CHIP_2_BYTE)
		&& (response[3] == ERASE_CHIP_3_BYTE))
	{
		while (retry-->0)
		{
			usleep(100000);
			ret_state=ZWPGM_checkState();
			if ((ret_state!=-1)&&(!(ret_state&BIT3)))
			{
				return 0;
			}
		}
	}

	return -1;
}


int  ZWPGM_readLockBits(uint8_t* lockBits)
{
	uint8_t num;
	int response_length;
	uint8_t response[4];
	uint8_t request[]={FLASH_READ_LOCK_1_BYTE,0x00,0x00,0x00};

	for (num=0; num<NO_OF_LOCKBITS;num++)
	{
		request[1]=num;
		putbuffer_t(fd,request, 4);
		response_length=getbuffer_t(fd,response, 4);
		if((response_length == 4)
				&& (response[0] == FLASH_READ_LOCK_1_BYTE)
				&& (response[1] == num))
		{
			lockBits[num]=response[3];
			continue;
		}else
		{
			return -1;
		}
	}
	return 0;
}

int ZWPGM_writeLockBits(uint8_t* lockBits)
{
	uint8_t num;
	int response_length;
	int ret_state;
	uint8_t response[4];
	uint8_t request[]={FLASH_WRITE_LOCK_1_BYTE,0x00,0x00,0x00};
	uint8_t retry=NO_OF_RETRY;

	for (num=0; num<NO_OF_LOCKBITS;num++)
	{
		request[1]=num;
		request[3]=lockBits[num];
		putbuffer_t(fd,request, 4);
		response_length= getbuffer_t(fd,response, 4);
		if((response_length == 4)
				&& (response[0] == FLASH_WRITE_LOCK_1_BYTE)
				&& (response[1] == num))
		{
			while (retry-->0)
			{
				usleep(20);
				ret_state=ZWPGM_checkState();
				if ((ret_state!=-1)&&(!(ret_state&BIT3)))
				{
					break;
				}
			}
			if (retry>0)
			{
				retry=NO_OF_RETRY;
				continue;
			}else
			{
				return -1;
			}

		}else
		{
			return -1;
		}

	}
	return 0;
}

int  ZWPGM_runCrcCheck(void)
{
	int response_length;
	int ret_state;
	uint8_t request[]= { RUN_CRC_CHK_0_BYTE, RUN_CRC_CHK_1_BYTE, RUN_CRC_CHK_2_BYTE, RUN_CRC_CHK_3_BYTE };
	uint8_t response[4];
	uint8_t retry=NO_OF_RETRY;

	putbuffer_t(fd,request, 4);
	response_length = getbuffer_t(fd,response, 4);

	if((response_length == 4)
		&& (response[0] == RUN_CRC_CHK_0_BYTE)
		&& (response[1] == RUN_CRC_CHK_1_BYTE)
		&& (response[2] == RUN_CRC_CHK_2_BYTE)
		&& (response[3] == RUN_CRC_CHK_3_BYTE))
	{
		while (retry-->0)
		{
			usleep(20000);
			ret_state=ZWPGM_checkState();
			printf("ret_state %02X\n",ret_state );
			if ((ret_state!=-1)&&(!(ret_state&BIT0)))
			{
				if (ret_state&BIT2)
				{
					printf("CRC fail\n");
					return -1;
				}else
				{

					return 0;
				}
			}
		}
	}
	printf("res fail\n");
	return -1;
}

int ZWPGM_eraseSector(uint8_t sector)
{
	int response_length;
	uint8_t request[]= {SECTOR_ERASE_ZW050X, sector, 0x00, 0x00};
	uint8_t response[4];
	uint8_t retry=NO_OF_RETRY;
	int ret_state;

	putbuffer_t(fd,request, 4);
	response_length = getbuffer_t(fd,response, 4);

	if((response_length == 4)
		&& (response[0] == SECTOR_ERASE_ZW050X)
		&& (response[1] == sector))
	{
		while (retry-->0)
		{
			usleep(1000);
			ret_state=ZWPGM_checkState();
			if ((ret_state!=-1)&&(!(ret_state&BIT3)))
			{
				return 0;
			}
		}
	}

	return -1;
}

int ZWPGM_writeFlashSector(uint8_t sector)
{
	int response_length;
	uint8_t request[]= {WRITE_FLASH_ZW050X, sector, 0x00, 0x00};
	uint8_t response[4];
	uint8_t retry=NO_OF_RETRY;
	int ret_state;

	putbuffer_t(fd,request, 4);
	response_length = getbuffer_t(fd,response, 4);

	if((response_length == 4)
		&& (response[0] == WRITE_FLASH_ZW050X)
		&& (response[1] == sector))
	{
		while (retry-->0)
		{
			usleep(10000);
			ret_state=ZWPGM_checkState();
			if ((ret_state!=-1)&&(!(ret_state&BIT3)))
			{
				return 0;
			}
		}
	}

	return -1;
}

int ZWPGM_readFlashSector(uint8_t sector)
{
	int response_length;
	uint8_t request[]= {READ_FLASH_MEM_0_BYTE, sector, 0x00, 0x00};
	uint8_t response[4];
	//uint8_t retry=NO_OF_RETRY;
	int ret_state = -1;

	putbuffer_t(fd,request, 4);
	response_length = getbuffer_t(fd,response, 4);

	if((response_length == 4)
		&& (response[0] == READ_FLASH_MEM_0_BYTE)
		&& (response[1] == sector))
	{
		ret_state = response[3];
	}

	return ret_state;
}


int ZWPGM_writeFlashPage(uint8_t *page, int pagelength, int sectorNumber)
{

	uint16_t num;
	int page_idx=0;
	int numberOfTriplets;
	int ret=-1;

	for (page_idx=pagelength-1;page_idx>=0;page_idx--)
	{
		if (page[page_idx]!=0xff)
		{
			pagelength=page_idx+1;
			break;
		}
	}

	if ((page_idx<0) && (page[0]==0xff))
	{
		printf("page %d\n",sectorNumber );
		return 0;
	}

	for (page_idx=0;page_idx<pagelength; page_idx++)
	{
		if (page[page_idx] != 0xff) break;
	}

	numberOfTriplets = (pagelength-page_idx-1)/3;
	
	ret=ZWPGM_writeSramByte(page_idx,page[page_idx]);
	page_idx++;
	if (ret==-1)
	{
		printf("Fail to write SRAM \n" );
		return -1;
	}


	for (num=0;num<numberOfTriplets;num++)
	{
		ret=ZWPGM_continueWrite(&page[page_idx]);
		if (ret==-1)
		{
			printf("Fail to write SRAM \n" );
			return -1;
		}
		page_idx+=3;

	}

	ret=ZWPGM_writeFlashSector(sectorNumber);
	if (ret==-1)
	{
		printf("Fail to write Flash sector \n" );
		return -1;
	}

	while (page_idx<pagelength)
	{
		if (page[page_idx] == 0xFF) 
		{
			page_idx++;
			continue;
		}
		ret=ZWPGM_writeSramByte((uint16_t)page_idx,page[page_idx]);
		page_idx++;
		if (ret==-1)
		{
			printf("Fail to write SRAM \n" );
			return -1;
		}

		ret=ZWPGM_writeFlashSector(sectorNumber);
		if (ret==-1)
		{
			printf("Fail to write Flash sector \n" );
			return -1;
		}
	}

	printf("write sectorNumber %d\n",sectorNumber);
	return 0;

}

int ZWPGM_readFlashPage(uint8_t *page, int pagelength, uint8_t sectorNumber)
{

	uint16_t page_idx=0;
	int numberOfTriplets,num;
	int ret;

	ret=ZWPGM_readFlashSector(sectorNumber);
	if (ret==-1)
	{
		printf("Fail to read Flash sector \n" );
			return -1;
	}

	page[page_idx++]=(uint8_t)ret;

	numberOfTriplets = (pagelength-1)/3;

	for (num=0;num<numberOfTriplets;num++)
	{
		ret=ZWPGM_continueRead(&page[page_idx]);
		if (ret==-1)
		{
			printf("Fail to read Flash sector \n" );
			return -1;
		}
		page_idx+=3;

	}

	uint8_t tempData[3];
	uint8_t complement=(uint8_t)((pagelength-1)%3);
	if (complement==1)
	{
		ret=ZWPGM_continueRead(tempData);
		if (ret==-1)
		{
			printf("Fail to read Flash sector \n" );
			return -1;
		}
		page[page_idx++]=tempData[0];
	}else
	{
		ret=ZWPGM_continueRead(tempData);
		if (ret==-1)
		{
			printf("Fail to read Flash sector \n" );
			return -1;
		}
		page[page_idx++]=tempData[0];
		page[page_idx++]=tempData[1];
	}
	return 0;
}


int ZWPGM_readNVramPage(void)
{
	int i;
	int ret;
	memset(bNVRArray,0xFF,256);
	for(i = NVR_START_ADDRESS; i <= NVR_END_ADDRESS; i++)
	{
		ret=ZWPGM_readNVramByte(i);
		if (ret==-1)
		{
			return -1;
		}
		bNVRArray[i]=(uint8_t)(ret&0xFF);

	}
	return 0;
}

int ZWPGM_loadDefaultNVramPage(void)
{
	memset(&bNVRArray[0],0xFF,sizeof(bNVRArray));

	bNVRArray[REV_ADR_1_1]=0x01;

	bNVRArray[SAWC_ADR_3_1]=0x0D;
	bNVRArray[SAWC_ADR_3_2]=0xE8;
	bNVRArray[SAWC_ADR_3_3]=0x8C;

	bNVRArray[PINS_ADR_1_1]=0xFF;

	bNVRArray[SAWB__ADR_1_1]=0x1B;

	bNVRArray[NVMCS_ADR_1_1]=0x15;

	bNVRArray[NVMT_ADR_1_1]=0x01;

	bNVRArray[NVMS_ADR_2_1]=0x00;
	bNVRArray[NVMS_ADR_2_2]=0x20;

	bNVRArray[NVMP_ADR_2_1]=0x00;
	bNVRArray[NVMP_ADR_2_2]=0x40;

	bNVRArray[VID_ADR_2_1]=0x06;
	bNVRArray[VID_ADR_2_2]=0x58;

	bNVRArray[PID_ADR_2_1]=0x02;
	bNVRArray[PID_ADR_2_2]=0x00;

	bNVRArray[CCAL_ADR_1_1]=0x04;

	bNVRArray[TXCAL1_ADR_1_1]=0x17;

	bNVRArray[TXCAL2_ADR_1_1]=0x13;

	return 0;

}
int ZWPGM_writeNVramPage(void)
{
	uint16_t wCRC16;
	int ret;
	uint8_t i;

	wCRC16=ccittCrc16(0x1D0F,&bNVRArray[NVR_START],NVR_END-NVR_START+1-2);
	bNVRArray[CRC16_MSB_ADR]=(uint8_t)((wCRC16>>8)&0xFF);
	bNVRArray[CRC16_LSB_ADR]=(uint8_t)((wCRC16>>0)&0xFF);
	for(i = NVR_START_ADDRESS; i <= NVR_END_ADDRESS; i++)
	{
		ret=ZWPGM_writeNVramByte(i,bNVRArray[i]);
		if (ret==-1)
		{
			return -1;
		}
	}
	return 0;
}
