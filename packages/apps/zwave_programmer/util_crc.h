#ifndef _UTIL_CRC_H_
#define _UTIL_CRC_H_
#include <stdint.h>
#define POLY 0x1021

// Public API
uint16_t ccittCrc16(uint16_t crc,uint8_t *pDataAddr,uint16_t bDataLen);
void calculateCrc(uint8_t* flashDataRaw);

#endif
