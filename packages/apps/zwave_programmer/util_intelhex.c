#include "util_intelhex.h"
uint32_t ex_addr;
uint8_t flashMem[flashMem_length];

int intelHex_parseHexLine(char *theline,int bytes[],uint32_t* addr,int *num,int *code)
{
	int sum,len,cksum;
	char *ptr;
	*num=0;
	if(theline[0]!=':') return 0;
	if(strlen(theline)<11) return 0;
	ptr=theline+1;
	if (!sscanf(ptr,"%02x",&len)) return 0;
	ptr+=2;
	if (strlen(theline)<(11+(len*2))) return 0;
	if (!sscanf(ptr,"%04x",addr)) return 0;
	ptr+=4;
	if (!sscanf(ptr,"%02x",code)) return 0;
	ptr+=2;
	if (*code==0x04)
	{
		if (!sscanf(ptr,"%04x",&ex_addr)) return 0;
		ptr+=4;
		sum=(len&0xFF)+((*addr>>8)&0xFF)+(*addr&0xFF)+(*code&0xFF)+((ex_addr>>8)&0xFF)+(ex_addr&0xFF);
		if (!sscanf(ptr,"%02x",&cksum)) return 0;
		if (((sum&0xFF)+(cksum&0xFF))&0xFF) return 0;
		ex_addr<<=16;
		return 1;
	}
	if (*code==0x02)
	{
		if (!sscanf(ptr,"%04x",&ex_addr)) return 0;
		ptr+=4;
		sum=(len&0xFF)+((*addr>>8)&0xFF)+(*addr&0xFF)+(*code&0xFF)+((ex_addr>>8)&0xFF)+(ex_addr&0xFF);
		if (!sscanf(ptr,"%02x",&cksum)) return 0;
		if (((sum&0xFF)+(cksum&0xFF))&0xFF) return 0;
		ex_addr<<=4;
		return 1;
	}

	sum=(len&0xFF)+((*addr>>8)&0xFF)+(*addr&0xFF)+(*code&0xFF);
	*addr+=ex_addr;

	while(*num!=len)
	{
		if (!sscanf(ptr,"%02x",&bytes[*num])) return 0;
		ptr+=2;
		sum+=bytes[*num]&0xFF;
		(*num)++;
		if (*num>=256) return 0;
	}
	if (!sscanf(ptr,"%02x",&cksum)) return 0;
	if (((sum&0xFF)+(cksum&0xFF))&0xFF) return 0;
	return 1;
}

int intelHex_loadfile(char *filename)
{
	char line[1000];
	FILE *fin;
	int n,status,bytes[256];
	int i,total=0,lineno=1;
	uint32_t addr;
	uint32_t minaddr=0xFFFFFFFF;
	uint32_t maxaddr=0;
	//int p=0;

	memset(flashMem,0xff,flashMem_length);
	if (strlen(filename)==0)
	{
		printf("Can't load a file without the filename");
		return -1;
	}

	fin=fopen(filename,"r");
	if (fin==NULL)
	{
		printf("Can't open file '%s' for reading.\n",filename);
		return -1;
	}
	while (!feof(fin)&&!ferror(fin))
	{
		line[0]='\0';
		fgets(line,1000,fin);
		if (line[strlen(line)-1]=='\n') line [strlen(line)-1]='\0';
		if (line[strlen(line)-1]=='\r') line [strlen(line)-1]='\0';
		if (intelHex_parseHexLine(line,bytes,&addr,&n,&status))
		{
			if (status==0)
			{
				for (i=0;i<n;i++)
				{
					flashMem[addr+i]=bytes[i]&0xff;
					total++;
					if(addr<minaddr) minaddr=addr;
					if (addr>maxaddr) maxaddr=addr;
				}
			}
			if (status==1)
			{
				fclose(fin);
				printf("Load %d bytes between ",total);
				printf("%04x to %04x\n",minaddr,maxaddr);
				return 1;
			}
		}
		else
		{
			printf("Error '%s',line:%d\n",filename,lineno);

		}
		lineno++;
	}
	return 1;
}
