#include "util_crc.h"
#include "util_intelhex.h"

uint16_t ccittCrc16(uint16_t crc,uint8_t *pDataAddr,uint16_t bDataLen)
{
    uint8_t WorkData;
    uint8_t bitMask;
    uint8_t NewBit;
    while(bDataLen--) 
    {
        WorkData=*pDataAddr++;
        for(bitMask=0x80;bitMask!=0;bitMask>>=1) 
        {
            
            NewBit=((WorkData&bitMask)!=0)^((crc&0x8000)!=0);
            crc<<=1;
            if (NewBit)
            {
                crc^=POLY;
            }
            
        }
        
    }
    return crc;
}
void chksumCrc32gentab(uint32_t* crc_tab)
{
    uint32_t crc, poly;
    int i, j;

    poly = 0xEDB88320;
    for (i = 0; i < 256; i++)
    {
        crc = (uint32_t)i;
        for (j = 8; j > 0; j--)
        {
            if ((crc & (uint32_t)1) > 0)
            {
                crc = (crc >> 1) ^ poly;
            }
            else
            {
                crc >>= 1;
            }
        }
        crc_tab[i] = crc;
    }
    return ;
}

void reverseBitsInBuf(uint8_t* buf, int offset, int len)
{
    int i;

    for (i = offset; i < (len + offset); i++)
    {
        buf[i] = (uint8_t)(((buf[i] << 4) & 0xF0) | ((buf[i] >> 4) & 0x0F));
        buf[i] = (uint8_t)(((buf[i] << 2) & 0xCC) | ((buf[i] >> 2) & 0x33));
        buf[i] = (uint8_t)(((buf[i] << 1) & 0xAA) | ((buf[i] >> 1) & 0x55));
    }
}


void calculateCrc(uint8_t* flashDataRaw)
{
    uint8_t isCrcOk = 1;
    uint8_t crcBuf[flashMem_length];
    uint8_t flashDataRawCrc[4];
    uint32_t crc_tab[256];
    uint32_t crc = 0xFFFFFFFF;
    uint32_t i;

    chksumCrc32gentab(crc_tab);
    memcpy(crcBuf,flashDataRaw,flashMem_length);

    flashDataRawCrc[0] = 0xFF;
    flashDataRawCrc[1] = 0xFF;
    flashDataRawCrc[2] = 0xFF;
    flashDataRawCrc[3] = 0xFF;

    if (flashDataRaw[flashMem_length - 4] != BLANK_VALUE ||
        flashDataRaw[flashMem_length - 3] != BLANK_VALUE ||
        flashDataRaw[flashMem_length - 2] != BLANK_VALUE ||
        flashDataRaw[flashMem_length - 1] != BLANK_VALUE)
    {
        flashDataRawCrc[0] = flashDataRaw[flashMem_length - 4];
        flashDataRawCrc[1] = flashDataRaw[flashMem_length - 3];
        flashDataRawCrc[2] = flashDataRaw[flashMem_length - 2];
        flashDataRawCrc[3] = flashDataRaw[flashMem_length - 1];

        crcBuf[flashMem_length - 4] = 0xFF;
        crcBuf[flashMem_length - 3] = 0xFF;
        crcBuf[flashMem_length - 2] = 0xFF;
        crcBuf[flashMem_length - 1] = 0xFF;
        isCrcOk = 0;
    }

    reverseBitsInBuf(crcBuf, 0, flashMem_length);

    for (i = 0; i < flashMem_length - 4; i++)
    {
        crc = ((crc >> 8) & 0x00FFFFFF) ^ crc_tab[(crc ^ crcBuf[i]) & 0xFF];
    }

    flashDataRaw[flashMem_length - 4] = (uint8_t)(0x000000FF & crc);
    flashDataRaw[flashMem_length - 3] = (uint8_t)(0x000000FF & (crc >> 8));
    flashDataRaw[flashMem_length - 2] = (uint8_t)(0x000000FF & (crc >> 16));
    flashDataRaw[flashMem_length - 1] = (uint8_t)(0x000000FF & (crc >> 24));

    reverseBitsInBuf(flashDataRaw, flashMem_length - 4, 4);
    
    if (isCrcOk == 0)
    {
        if (flashDataRawCrc[0] == flashDataRaw[flashMem_length - 4] &&
            flashDataRawCrc[1] == flashDataRaw[flashMem_length - 3] &&
            flashDataRawCrc[2] == flashDataRaw[flashMem_length - 2] &&
            flashDataRawCrc[3] == flashDataRaw[flashMem_length - 1])
        {
            isCrcOk = 1;
        }
    }
    
    return ;
}
