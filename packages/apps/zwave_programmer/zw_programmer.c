#include <signal.h>
#include "programmerAPI.h"
#include "serial.h"
#include "util_intelhex.h"
#include "util_crc.h"
#include "gpio_intf.h"

#define PAGE_LENGTH								2048
#define ZWAVE_DEFAULT_PORT 						"/dev/ttyO2"
#define ZWAVE_DEFAULT_FIRMWARE_PATH 			"/lib/firmware/zwave"
#define ZWAVE_DEFAULT_FIRMWARE_NAME 			"serialapi_controller_static_ZM5304_US.hex"
#define GPIO_PIN_CTRL_NUM 						49 
#define GPIO_PIN_RST_NUM  						46

void controller_enable() {
	/*
	 * Enable GPIO pins
	 */
	if (-1 == GPIOExport(GPIO_PIN_CTRL_NUM) || -1 == GPIOExport(GPIO_PIN_RST_NUM))
		return;

	/*
	 * Set GPIO directions
	 */
	if (-1 == GPIODirection(GPIO_PIN_CTRL_NUM, OUT) || -1 == GPIODirection(GPIO_PIN_RST_NUM, OUT))
		return;

	/* 
	 * Write data
	 */
	if (-1 == GPIOWrite(GPIO_PIN_CTRL_NUM, 0))
		printf("Can't write to CTRL PIN: %d\n", GPIO_PIN_CTRL_NUM);
	if (-1 == GPIOWrite(GPIO_PIN_RST_NUM, 0))
		printf("Can't write to RST PIN: %d\n", GPIO_PIN_RST_NUM);

	//printf("....Enabling pin.....FFFFFFFFFF....\n");

}

void controller_reset() {
	/*
	 * Disable GPIO pins
	 */
	if (-1 == GPIOUnexport(GPIO_PIN_CTRL_NUM) || -1 == GPIOUnexport(GPIO_PIN_RST_NUM))
		return;
}

void sig_handler(int signo)
{
	if (signo == SIGINT)
	 	controller_reset();
	exit(EXIT_SUCCESS);
}

uint8_t flashMem_com[flashMem_length];

uint8_t lockBits[9];
int main(int argc, char **argv)
{
	/*how to run
	killall zwave_handler
	zwave_programmer -f /lib/firmware/zwave/serialapi_controller_static_ZM5304_US.hex*/
	int ret=-1;
	uint32_t i;
	//uint8_t page[PAGE_LENGTH];
	uint8_t sector;
	int c;
    	char *pdevID = NULL;
    	char *pfmPath = NULL;

     	while(1) {
          	static struct option long_options[] =
          	{
            		/* These options don’t set a flag.
               		We distinguish them by their indices. */
            		{"help",  no_argument,       0, 'h'},
            		{"port",  required_argument, 0, 'p'},
            		{"firmware",  required_argument, 0, 'f'},
            		{0, 0, 0, 0}
          	};
          	/* getopt_long stores the option index here. */
          	int option_index = 0;

           	c = getopt_long (argc, argv, "hp:f:",
                          long_options, &option_index);

          	/* Detect the end of the options. */
          	if (c == -1)
                  	break;

          	switch(c) {
                  case 'h':
                      printf("Usage: zwave-programmer [options]\n");
                      printf("Options:\n");
                      printf("--help(-h): print help information\n");
                      printf("--port(-p): specify the dedicated port to control zwave controller.\n");
                      printf("--firmware(-f): specify the dedicated firmware path to flash to zwave controller.\n");
                      printf("Without options, the default port(%s) is used to connect to zwave controller\n", ZWAVE_DEFAULT_PORT);
                      printf("and the default firmware path(%s) is used to program zwave controller\n", ZWAVE_DEFAULT_FIRMWARE_PATH);
                      exit(EXIT_SUCCESS);
                  case 'p':
			  pdevID = malloc(strlen(optarg));
                          strncpy(pdevID, optarg, strlen(optarg));
                      break;
		  case 'f':
                          pfmPath = malloc(strlen(optarg));
                          strncpy(pfmPath, optarg, strlen(optarg));
                      break;

                  default:
                      printf("Invalid option, please use --help or -h to know how to use\n");
                      exit(EXIT_SUCCESS);
                      break;
          	}
      	}

     if (pdevID == NULL){
	pdevID = malloc(strlen(ZWAVE_DEFAULT_PORT));
	strncpy(pdevID, ZWAVE_DEFAULT_PORT, strlen(ZWAVE_DEFAULT_PORT));
     }

    controller_enable();
    sleep(5);
    if (signal(SIGINT, sig_handler) == SIG_ERR)
        printf("\ncan't catch SIGINT\n");


    fd = open (pdevID, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0)
    {
        printf  ("error %d opening %s: %s\n", errno, pdevID, strerror (errno));
        goto finish;
    }
    set_interface_attribs (fd,B115200, 0,2);
	ret=ZWPGM_enableInterface();
	if (ret==-1)
	{
		printf("ZWPGM_enableInterface fail\n");
		goto finish;
	}
	ret=ZWPGM_readSignature();
	if (ret==-1)
	{
		printf("ZWPGM_readSignature fail\n");
		goto finish;
	}

	ret=ZWPGM_readNVramPage();
	if (ret==-1)
	{
		printf("ZWPGM_readNVramPage fail\n");
		goto finish;
	}
	ZWPGM_loadDefaultNVramPage();
	for (i=0x10;i<=0x7F;i++)
	{
		printf("%02x ",bNVRArray[i] );
		if ((i-0x0F) %16 ==0)
			printf("\n");

	}
	ret=ZWPGM_readLockBits(lockBits);
	if (ret==-1)
	{
		printf("ZWPGM_readLockBits fail\n");
		goto finish;
	}
	printf("lockBits:");
	for (i=0;i<9;i++)
	{
		printf("%02x ",lockBits[i]);
	}
	printf("\n");

	if(pfmPath == NULL) {
		pfmPath = malloc(strlen(ZWAVE_DEFAULT_FIRMWARE_PATH) + strlen(ZWAVE_DEFAULT_FIRMWARE_NAME) + 1);
		sprintf(pfmPath,"%s/%s",ZWAVE_DEFAULT_FIRMWARE_PATH,ZWAVE_DEFAULT_FIRMWARE_NAME);
	}

	intelHex_loadfile(pfmPath);
	//memset(flashMem,0xff,flashMem_length);
	calculateCrc(flashMem);
	//printf("crc %02X %02X %02X %02X \n",flashMem[flashMem_length-4],flashMem[flashMem_length-3],flashMem[flashMem_length-2],flashMem[flashMem_length-1]);
	/*
	ret=ZWPGM_readFlashPage(page,PAGE_LENGTH,0);
	if (ret==-1)
	{
		printf("ZWPGM_readFlashPage fail\n");
		goto finish;
	}
	for (i=0;i<PAGE_LENGTH;i++)
	{
		printf("%02x ",page[i] );
		if ((i-0x0F) %16 ==0)
			printf("\n");

	}

	*/

	ret=ZWPGM_eraseChip();
	if (ret==-1)
	{
		printf("ZWPGM_eraseChip fail\n");
		goto finish;
	}


/*
	ret=ZWPGM_loadDefaultNVramPage();
	if (ret==-1)
	{
		printf("ZWPGM_loadDefaultNVramPage fail\n");
		goto finish;
	}
*/
	ret=ZWPGM_writeNVramPage();
	if (ret==-1)
	{
		printf("ZWPGM_writeNVramPage fail\n");
		goto finish;
	}

	for (sector=0;sector<64;sector++)
	{
		//printf("read sector %d\n",sector );
		ret=ZWPGM_writeFlashPage(&flashMem[sector*PAGE_LENGTH],PAGE_LENGTH,sector);
		if (ret==-1)
		{
			printf("ZWPGM_writeFlashPage fail\n");
			goto finish;
		}
	}
/*
	for (i=0;i<PAGE_LENGTH;i++)
	{
		printf("%02x ",page[i] );
		if ((i-0x0F) %16 ==0)
			printf("\n");

	}

	for (i=0;i<flashMem_length;i++)
	{
		if (flashMem[i]!=flashMem_com[i])
		{
			printf("index wrong =%d\n",i );
			goto finish;
		}
	}
	printf("Match\n");
	*/
/*
	ret=ZWPGM_readFlashPage(page,PAGE_LENGTH,0);
	if (ret==-1)
	{
		printf("ZWPGM_readFlashPage fail\n");
		goto finish;
	}
	for (i=0;i<PAGE_LENGTH;i++)
	{
		printf("%02x ",page[i] );
		if ((i-0x0F) %16 ==0)
			printf("\n");

	}
	*/
	ret=ZWPGM_runCrcCheck();
	if (ret==-1)
	{
		printf("ZWPGM_runCrcCheck fail\n");
		goto finish;
	}


	ret=ZWPGM_writeLockBits(lockBits);
	if (ret==-1)
	{
		printf("ZWPGM_writeLockBits fail\n");
		goto finish;
	}

finish:
	controller_reset();
	close(fd);
	return 0;
}
