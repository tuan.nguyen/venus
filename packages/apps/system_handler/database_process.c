#include <stdio.h>
#include <stdint.h>
#include <sys/stat.h>

#include "database.h"
#include "slog.h"
#include "VR_define.h"
#include "verik_utils.h"

#include "database_process.h"

static sqlite3 *db;

static int rename_table(char * table_name, char * new_table_name)
{
    int rc;
    char sql[256];
    char *zErrMsg = 0;

    sprintf(sql, "ALTER TABLE %s RENAME TO %s;", table_name, new_table_name);
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }

    return rc;
}

#if 1
typedef struct _zwCompliantCapConvert_t
{
    const char* capability;
    uint8_t capId;
} zwCompliantCapConvert_t;

zwCompliantCapConvert_t capConvertTable[]={
    {.capability="ALARM",                                        .capId=0x71},
    {.capability="NOTIFICATION",                                 .capId=0x71},
    {.capability="APPLICATION_STATUS",                           .capId=0x22},
    {.capability="ASSOCIATION",                                  .capId=0x85},
    {.capability="BASIC",                                        .capId=0x20},
    {.capability="BATTERY",                                      .capId=0x80},
    {.capability="CONFIGURATION",                                .capId=0x70},
    {.capability="CONTROLLER_REPLICATION",                       .capId=0x21},
    {.capability="CRC_16_ENCAP",                                 .capId=0x56},
    {.capability="DOOR_LOCK_LOGGING",                             .capId=0x4C},
    {.capability="DOOR_LOCK",                                    .capId=0x62},
    {.capability="LANGUAGE",                                     .capId=0x89},
    {.capability="MANUFACTURER_SPECIFIC",                        .capId=0x72},
    {.capability="METER",                                        .capId=0x32},
    {.capability="MULTI_CHANNEL_ASSOCIATION",                    .capId=0x8E},
    {.capability="MULTI_CHANNEL_V3",                             .capId=0x60},
    {.capability="MULTI_CMD",                                    .capId=0x8F},
    {.capability="POWER_LEVEL",                                  .capId=0x73},
    {.capability="PROTECTION",                                   .capId=0x75},
    {.capability="SCHEDULE_ENTRY_LOCK",                          .capId=0x4E},
    {.capability="SENSOR_ALARM",                                 .capId=0x9C},
    {.capability="SENSOR_BINARY",                                .capId=0x30},
    {.capability="SENSOR_MULTILEVEL",                            .capId=0x31},
    {.capability="SILENCE_ALARM",                                .capId=0x9D},
    {.capability="SIMPLE_AV_CONTROL",                            .capId=0x94},
    {.capability="SWITCH_ALL",                                   .capId=0x27},
    {.capability="SWITCH_BINARY",                                .capId=0x25},
    {.capability="SWITCH_MULTILEVEL",                            .capId=0x26},
    {.capability="THERMOSTAT_FAN_MODE",                          .capId=0x44},
    {.capability="THERMOSTAT_FAN_STATE",                         .capId=0x45},
    {.capability="THERMOSTAT_MODE",                              .capId=0x40},
    {.capability="THERMOSTAT_OPERATING_STATE",                   .capId=0x42},
    {.capability="THERMOSTAT_SETBACK",                           .capId=0x47},
    {.capability="THERMOSTAT_SETPOINT",                          .capId=0x43},
    {.capability="TIME_PARAMETERS",                              .capId=0x8B},
    {.capability="TIME",                                         .capId=0x8A},
    {.capability="USER_CODE",                                    .capId=0x63},
    {.capability="VERSION",                                      .capId=0x86},
    {.capability="WAKE_UP",                                      .capId=0x84},
    {.capability="SWITCH_COLOR",                                 .capId=0x33},
    {.capability="ASSOCIATION_GRP_INFO",                         .capId=0x59},
    {.capability="CENTRAL_SCENE",                                .capId=0x5B},
    {.capability="ANTITHEFT",                                    .capId=0x5D},
    {.capability="ZWAVE_PLUS_INFO",                              .capId=0x5E},
    {.capability="BARRIER_OPERATOR",                             .capId=0x66},
    {.capability="IRRIGATION",                                   .capId=0x6B},
    {.capability="WINDOW_COVERING",                              .capId=0x6A},
};

static void convert_capability(char *capability, char *result, int *scheme)
{
    if(!capability || !result)
    {
        return;
    }
    int i, firstTimeInsert=0;
    char *ch = NULL, *save_tok;
    ch = strtok_r(capability, ",", &save_tok);
    while(ch != NULL)
    {
        printf("ch = %s\n", ch);
        if(!strcmp(ch, "DOOR_LOCK") || !strcmp(ch, "BARRIER_OPERATOR"))
        {
            *scheme = SEC_0_SCHEME;
        }

        for(i = 0; i< sizeof(capConvertTable)/sizeof(zwCompliantCapConvert_t); i++)
        {
            if(!strcmp(capConvertTable[i].capability, ch))
            {
                if(!firstTimeInsert)
                {
                    sprintf(result, "%d", capConvertTable[i].capId);
                    firstTimeInsert = 1;
                }
                else
                {
                    sprintf(result+strlen(result), ",%d", capConvertTable[i].capId);
                }
            }
        }
        ch = strtok_r(NULL, ",", &save_tok);
    }
}

static int create_new_SUB_DEV_table()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE SUB_DEVICES("  \
             "owner             TEXT    NOT NULL," \
             "serial            TEXT   PRIMARY KEY NOT NULL," \
             "serialId          TEXT   NOT NULL," \
             "deviceType        TEXT   NOT NULL," \
             "typeBasic         TEXT," \
             "typeSpecific      TEXT," \
             "deviceMode        TEXT," \
             "friendlyName      TEXT," \
             "id                TEXT NOT NULL," \
             "capability        TEXT," \
             "profileId         TEXT," \
             "endpointNum       TEXT," \
             "parentId          TEXT," \
             "childrenId        TEXT," \
             "active            TEXT," \
             "isZWavePlus       TEXT," \
             "roleType          TEXT," \
             "nodeType          TEXT," \
             "alexa             TEXT NOT NULL,"\
             "type              TEXT NOT NULL);";

    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table SUB_DEVICES for zwave compliant created successfully\n");
    }
    
    return rc;
}

static int convert_data_sub_devices_cb(void *data, int argc, char **argv, char **azColName)
{
    int i;
    char *defaultEndPoint = "0";
    char *owner = NULL;
    char *serial = NULL;
    char *serialId = NULL;
    char *deviceType = NULL;
    char *friendlyName = NULL;
    char *id = NULL;
    char *capability = NULL;
    char *profileId = NULL;
    char *endpointNum = NULL;
    char *alexa = NULL;
    for(i=0; i< argc; i++)
    {
        if(!strcmp(azColName[i], "Owner"))
        {
            owner = argv[i];
        }
        else if(!strcmp(azColName[i], "Serial"))
        {
            serial = argv[i];
        }
        else if(!strcmp(azColName[i], "SerialID"))
        {
            serialId = argv[i];
        }
        else if(!strcmp(azColName[i], "DeviceType"))
        {
            deviceType = argv[i];
        }
        else if(!strcmp(azColName[i], "FriendlyName"))
        {
            friendlyName = argv[i];
        }
        else if(!strcmp(azColName[i], "ID"))
        {
            id = argv[i];
        }
        else if(!strcmp(azColName[i], "Capability"))
        {
            capability = argv[i];
        }
        else if(!strcmp(azColName[i], "ProfileID"))
        {
            profileId = argv[i];
        }
        else if(!strcmp(azColName[i], "EndPoint_Num"))
        {
            endpointNum = argv[i];
        }
        else if(!strcmp(azColName[i], "Alexa"))
        {
            alexa = argv[i];
        }
    }

    if(!endpointNum)
    {
        endpointNum = defaultEndPoint;
    }
    if(!profileId) //zwave devices
    {
        char newCap[SIZE_512B*4]={0};
        int scheme = NON_SEC_SCHEME;
        convert_capability(capability, newCap, &scheme);

        database_actions("system_handler", db, "INSERT INTO CAPABILITY"
                                "(deviceId, capId, sec, inOut, type)"
                                "VALUES('%s', '%s', '%d', '%s', '%s')",
                                id, newCap, scheme, "in", ST_ZWAVE);

        database_actions("system_handler", db, "INSERT INTO SUB_DEVICES (owner,serial,serialId,deviceType,friendlyName,id,endpointNum,"
                                                    "networkCap, parentId, active, alexa) "
                          "VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                          owner, serial, serialId, deviceType, friendlyName, id, endpointNum,
                          "0", "0", "yes", alexa);
    }
    else //zigbee
    {
        database_actions("system_handler", db, "INSERT INTO SUB_DEVICES (owner,serial,serialId,deviceType,friendlyName,id,capability,profileId,endpointNum,"
                                                    "parentId, alexa) "
                          "VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                          owner, serial, serialId, deviceType, friendlyName, id, capability, profileId, endpointNum, "0",
                          alexa);
    }
    
    return 0;
}

static int column_exist = 0;

static int check_column_exist_callback(void *column_name, int argc, char **argv, char **azColName)
{
    int i;
    for(i=0; i<argc; i++)
    {
        if(!strcmp(azColName[i], "name"))
        {
            if(argv[i] && !strcmp(argv[i], column_name))
            {
                column_exist = 1;
            }
        }
    }
   return 0;
}

static int check_column_exist(char * table_name, char * column_name)
{
    int rc;
    char sql[256];
    char *zErrMsg = 0;
    column_exist = 0;
    sprintf(sql, "PRAGMA table_info(%s);", table_name);
    rc = sqlite3_exec(db, sql, check_column_exist_callback, (void *)column_name, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    printf("column_exist = %d\n", column_exist);
    return column_exist;
}

int create_new_CONTROL_table()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE CONTROL_DEVS("  \
         "realName          TEXT        NOT NULL," \
         "friendlyName      TEXT        NOT NULL," \
         "udn               TEXT PRIMARY KEY NOT NULL," \
         "url               TEXT, "\
         "type              TEXT    NOT NULL,"\
         "added             TEXT    "\
         ");";

    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table CONTROL_DEVS for zwave compliant created successfully\n");
    }
    
    return rc;
}

static int convert_data_control_table_cb(void *data, int argc, char **argv, char **azColName)
{
    int i;

    char *realName = NULL;
    char *friendlyName = NULL;
    char *udn = NULL;
    char *url = NULL;
    char *type = NULL;
    char *added = NULL;
    
    for(i=0; i< argc; i++)
    {
        if(!strcmp(azColName[i], "RealName"))
        {
            realName = argv[i];
        }
        else if(!strcmp(azColName[i], "FriendlyName"))
        {
            friendlyName = argv[i];
        }
        else if(!strcmp(azColName[i], "UDN"))
        {
            udn = argv[i];
        }
        else if(!strcmp(azColName[i], "URL"))
        {
            url = argv[i];
        }
        else if(!strcmp(azColName[i], "Type"))
        {
            type = argv[i];
        }
        else if(!strcmp(azColName[i], "Added"))
        {
            added = argv[i];
        }
    }
    database_actions("system_handler", db, 
                    "INSERT INTO CONTROL_DEVS (realName,friendlyName,udn,url,type,added) "
                      "VALUES('%s','%s','%s','%s','%s','%s')",
                      realName, friendlyName, udn, url, type, added);
    return 0;
}

static int create_new_DEVICE_table()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE DEVICES("  \
         "deviceId        TEXT   PRIMARY KEY   NOT NULL," \
         "deviceType      TEXT                 NOT NULL," \
         "name            TEXT                         ," \
         "owner           TEXT                 NOT NULL," \
         "url        TEXT);";

    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table DEVICES for zwave compliant created successfully\n");
    }
    
    return rc;
}

static int convert_data_device_cb(void *data, int argc, char **argv, char **azColName)
{
    int i;

    char *deviceId = NULL;
    char *deviceType = NULL;
    char *name = NULL;
    char *owner = NULL;
    char *url = NULL;
    
    for(i=0; i< argc; i++)
    {
        if(!strcmp(azColName[i], "deviceid"))
        {
            deviceId = argv[i];
        }
        else if(!strcmp(azColName[i], "devicetype"))
        {
            deviceType = argv[i];
        }
        else if(!strcmp(azColName[i], "name"))
        {
            name = argv[i];
        }
        else if(!strcmp(azColName[i], "owner"))
        {
            owner = argv[i];
        }
        else if(!strcmp(azColName[i], "url"))
        {
            url = argv[i];
        }
    }
    database_actions("system_handler", db, 
                    "INSERT INTO DEVICES (deviceId,deviceType,name,owner,url)"
                      "VALUES('%s','%s','%s','%s','%s')",
                      deviceId, deviceType, name, owner, url);
    return 0;
}

static int create_new_GROUPS_table()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE GROUPS("  \
         "userId        TEXT                 NOT NULL," \
         "groupId       TEXT   PRIMARY KEY   NOT NULL," \
         "groupName     TEXT                 NOT NULL," \
         "device        TEXT                 NOT NULL" \
         ");";

    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table GROUPS for zwave compliant created successfully\n");
    }
    
    return rc;
}

static int convert_data_groups_cb(void *data, int argc, char **argv, char **azColName)
{
    int i;

    char *userId = NULL;
    char *groupId = NULL;
    char *groupName = NULL;
    char *device = NULL;

    for(i=0; i< argc; i++)
    {
        if(!strcmp(azColName[i], "UserID"))
        {
            userId = argv[i];
        }
        else if(!strcmp(azColName[i], "GroupID"))
        {
            groupId = argv[i];
        }
        else if(!strcmp(azColName[i], "GroupName"))
        {
            groupName = argv[i];
        }
        else if(!strcmp(azColName[i], "Device"))
        {
            device = argv[i];
        }
    }

    database_actions("system_handler", db, 
                    "INSERT INTO GROUPS (userId,groupId,groupName,device)"
                      "VALUES('%s','%s','%s','%s')",
                      userId, groupId, groupName, device);
    return 0;
}

static int create_new_RULES_table()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE RULES("  \
         "type          TEXT        NOT NULL," \
         "id            TEXT   PRIMARY KEY     NOT NULL," \
         "name          TEXT        NOT NULL," \
         "conditions    TEXT        NOT NULL," \
         "actions       TEXT        NOT NULL,"\
         "status        TEXT        NOT NULL,"\
         "run           TEXT        NOT NULL);";

    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table RULES for zwave compliant created successfully\n");
    }
    
    return rc;
}

static int convert_data_rules_cb(void *data, int argc, char **argv, char **azColName)
{
    // int i;

    // char *type = NULL;
    // char *id = NULL;
    // char *name = NULL;
    // char *conditions = NULL;
    // char *actions = NULL;
    // char *status = NULL;
    // char *run = NULL;
    
    // for(i=0; i< argc; i++)
    // {
    //     if(!strcmp(azColName[i], "Type"))
    //     {
    //         type = argv[i];
    //     }
    //     else if(!strcmp(azColName[i], "ID"))
    //     {
    //         id = argv[i];
    //     }
    //     else if(!strcmp(azColName[i], "Name"))
    //     {
    //         name = argv[i];
    //     }
    //     else if(!strcmp(azColName[i], "Conditions"))
    //     {
    //         conditions = argv[i];
    //     }
    //     else if(!strcmp(azColName[i], "Actions"))
    //     {
    //         actions = argv[i];
    //     }
    //     else if(!strcmp(azColName[i], "Status"))
    //     {
    //         status = argv[i];
    //     }
    //     else if(!strcmp(azColName[i], "Run"))
    //     {
    //         run = argv[i];
    //     }
    // }
    // database_actions("system_handler", db, 
    //                 "INSERT INTO RULES (type,id,name,conditions,actions,status,run)"
    //                   "VALUES('%s','%s','%s','%s','%s','%s','%s')",
    //                   type, id, name, conditions, actions, status, run);
    return 0;
}

static int create_new_USERS_table()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE USERS("  \
         "userId         TEXT     PRIMARY KEY     NOT NULL," \
         "shareId          TEXT     NOT NULL," \
         "name             TEXT     NOT NULL," \
         "enable           TEXT     NOT NULL," \
         "userType         TEXT     NOT NULL," \
         "shareTime        TEXT     ," \
         "options          TEXT);";

    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table USERS for zwave compliant created successfully\n");
    }
    
    return rc;
}

static int convert_data_users_cb(void *data, int argc, char **argv, char **azColName)
{
    int i;

    char *userId = NULL;
    char *shareId = NULL;
    char *name = NULL;
    char *enable = NULL;
    char *userType = NULL;
    char *shareTime = NULL;
    char *options = NULL;
    
    for(i=0; i< argc; i++)
    {
        if(!strcmp(azColName[i], "UserId"))
        {
            userId = argv[i];
        }
        else if(!strcmp(azColName[i], "ShareId"))
        {
            shareId = argv[i];
        }
        else if(!strcmp(azColName[i], "Name"))
        {
            name = argv[i];
        }
        else if(!strcmp(azColName[i], "Enable"))
        {
            enable = argv[i];
        }
        else if(!strcmp(azColName[i], "UserType"))
        {
            userType = argv[i];
        }
        else if(!strcmp(azColName[i], "ShareTime"))
        {
            shareTime = argv[i];
        }
        else if(!strcmp(azColName[i], "Options"))
        {
            options = argv[i];
        }
    }

    database_actions("system_handler", db, 
                    "INSERT INTO USERS (userId,shareId,name,enable,userType,shareTime,options)"
                      "VALUES('%s','%s','%s','%s','%s','%s','%s')",
                      userId, shareId, name, enable, userType, shareTime, options);
    return 0;
}

/*static int create_new_FEATURES_table()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE FEATURES("  \
         "deviceId        TEXT                 NOT NULL," \
         "featureId       TEXT                 NOT NULL," \
         "register        TEXT                 NOT NULL," \
         "data            TEXT                         ," \
         "type            TEXT                 NOT NULL" \
         ");";

    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table FEATURES for created successfully\n");
    }
    
    return rc;
}*/

// static int convert_data_features_cb(void *data, int argc, char **argv, char **azColName)
// {
//     int i;

//     char *deviceId = NULL;
//     char *featureId = NULL;
//     char *registerValue = NULL;
//     char *type = NULL;

//     for(i=0; i< argc; i++)
//     {
//         if(!strcmp(azColName[i], "deviceId"))
//         {
//             deviceId = argv[i];
//         }
//         else if(!strcmp(azColName[i], "featureId"))
//         {
//             featureId = argv[i];
//         }
//         else if(!strcmp(azColName[i], "register"))
//         {
//             registerValue = argv[i];
//         }
//         else if(!strcmp(azColName[i], "type"))
//         {
//             type = argv[i];
//         }
//     }

//     if(!strcmp(featureId, ST_USER_CODE))
//     {
//         char *save_tok, *tok;
//         tok = strtok_r(registerValue, ",", &save_tok);
//         while(tok != NULL)
//         {
//             char codeId[SIZE_32B] = {0};
//             char pass[SIZE_32B] = {0};
//             char *stok;
//             char *pch = strtok_r(tok, ":", &stok);
//             if(pch!=NULL)
//             {
//                 strncpy(codeId, pch, sizeof(codeId)-1);
//                 pch = strtok_r(NULL, ":", &stok);
//             }

//             if(pch!=NULL)
//             {
//                 strncpy(pass, pch, sizeof(pass)-1);
//             }

//             if(strlen(codeId) && strlen(pass))
//             {
//                 database_actions("system_handler", db, 
//                         "INSERT INTO FEATURES (deviceId,featureId,register,data,type)"
//                           "VALUES('%s','%s','%s','%s','%s')",
//                           deviceId, featureId, codeId, pass, type);
//             }
            
//             tok = strtok_r(NULL, ",", &save_tok);
//         }
//     }
//     else
//     {
//         database_actions("system_handler", db, 
//                     "INSERT INTO FEATURES (deviceId,featureId,register,type)"
//                       "VALUES('%s','%s','%s','%s')",
//                       deviceId, featureId, registerValue, type);
//     }
    
//     return 0;
// }

static int create_new_SUB_DEV2_table()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE SUB_DEVICES("  \
             "owner             TEXT    NOT NULL," \
             "serial            TEXT   PRIMARY KEY NOT NULL," \
             "serialId          TEXT   NOT NULL," \
             "deviceType        TEXT   NOT NULL," \
             "typeBasic         TEXT," \
             "typeSpecific      TEXT," \
             "deviceMode        TEXT," \
             "friendlyName      TEXT," \
             "id                TEXT NOT NULL," \
             "capability        TEXT," \
             "profileId         TEXT," \
             "endpointNum       TEXT," \
             "parentId          TEXT," \
             "childrenId        TEXT," \
             "active            TEXT," \
             "isZWavePlus       TEXT," \
             "roleType          TEXT," \
             "nodeType          TEXT," \
             "alexa             TEXT NOT NULL,"\
             "type              TEXT NOT NULL);";

    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table SUB_DEVICES for zwave compliant 2 created successfully\n");
    }
    
    return rc;
}

static int convert_data_sub_devices2_cb(void *data, int argc, char **argv, char **azColName)
{
    int i;
    char *owner = NULL;
    char *serial = NULL;
    char *serialId = NULL;
    char *deviceType = NULL;
    char *friendlyName = NULL;
    char *id = NULL;
    char *capability = NULL;
    char *profileId = NULL;
    char *endpointNum = NULL;
    char *networkCap = NULL;
    char *parentId = NULL;
    char *childrenId = NULL;
    char *active = NULL;
    char *alexa = NULL;
    for(i=0; i< argc; i++)
    {
        if(!strcmp(azColName[i], ST_OWNER))
        {
            owner = argv[i];
        }
        else if(!strcmp(azColName[i], ST_SERIAL))
        {
            serial = argv[i];
        }
        else if(!strcmp(azColName[i], ST_SERIAL_ID))
        {
            serialId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_DEVICE_TYPE))
        {
            deviceType = argv[i];
        }
        else if(!strcmp(azColName[i], ST_FRIENDLY_NAME))
        {
            friendlyName = argv[i];
        }
        else if(!strcmp(azColName[i], ST_ID))
        {
            id = argv[i];
        }
        else if(!strcmp(azColName[i], ST_CAPABILITY))
        {
            capability = argv[i];
        }
        else if(!strcmp(azColName[i], ST_PROFILE_ID))
        {
            profileId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_ENDPOINT_NUM))
        {
            endpointNum = argv[i];
        }
        else if(!strcmp(azColName[i], ST_ALEXA))
        {
            alexa = argv[i];
        }
        else if(!strcmp(azColName[i], ST_PARENT_ID))
        {
            parentId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_CHILDREN_ID))
        {
            childrenId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_ACTIVE))
        {
            active = argv[i];
        }
        else if(!strcmp(azColName[i], "networkCap"))
        {
            networkCap = argv[i];
        }
    }

    if(!profileId) //zwave devices /*work around detect zwave and zigbee device: firmware older 2.0.0*/
    {
        database_actions("system_handler", db, 
                        "INSERT INTO SUB_DEVICES (owner,serial,serialId,deviceType,friendlyName,id,endpointNum,"
                                                    "deviceMode, parentId, childrenId, active, alexa, type) "
                          "VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                          owner, serial, serialId, deviceType, friendlyName, id, endpointNum,
                          networkCap, parentId, childrenId, active, alexa, ST_ZWAVE);
    }
    else //zigbee
    {
        database_actions("system_handler", db, 
                        "INSERT INTO SUB_DEVICES (owner,serial,serialId,deviceType,friendlyName,id,capability,profileId,endpointNum,"
                                                    "parentId, alexa, type) "
                          "VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                          owner, serial, serialId, deviceType, friendlyName, id, capability, profileId, endpointNum, parentId,
                          alexa, ST_ZIGBEE);
    }
    
    return 0;
}

static int create_new_CAP_table()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE CAPABILITY("  \
         "deviceId        TEXT                 NOT NULL," \
         "capList           TEXT                 NOT NULL," \
         "scheme             TEXT                         ," \
         "inOut           TEXT                         ," \
         "endpointMem     TEXT                         ," \
         "type            TEXT                         ," \
         "option          TEXT                         ," \
         "PRIMARY KEY (deviceId, capList));";

    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table CAPABILITY for zwave compliant created successfully\n");
    }
    return rc;
}

static int convert_data_CAP_cb(void *data, int argc, char **argv, char **azColName)
{
    int i;
    char *deviceId = NULL;
    char *capList = NULL;
    char *scheme = NULL;
    char *inOut = NULL;
    char *type = NULL;
    char *option = NULL;

    for(i=0; i< argc; i++)
    {
        if(!strcmp(azColName[i], ST_DEVICE_ID))
        {
            deviceId = argv[i];
        }
        else if(!strcmp(azColName[i], "capId"))
        {
            capList = argv[i];
        }
        else if(!strcmp(azColName[i], "sec"))
        {
            scheme = argv[i];
        }
        else if(!strcmp(azColName[i], ST_IN_OUT))
        {
            inOut = argv[i];
        }
        else if(!strcmp(azColName[i], ST_TYPE))
        {
            type = argv[i];
        }
        else if(!strcmp(azColName[i], ST_OPTION))
        {
            option = argv[i];
        }
    }

    database_actions("system_handler", db, 
                    "INSERT INTO CAPABILITY (%s,%s,%s,%s,%s,%s)"
                      "VALUES('%s','%s','%s','%s','%s','%s')",
                      ST_DEVICE_ID, ST_CAP_LIST, ST_SCHEME, ST_IN_OUT, ST_TYPE, ST_OPTION,
                      deviceId, capList, scheme, inOut, type, option);
    return 0;
}

static int convert_data(char * table_name, db_callback callback)
{
    int  rc;
    char sql[256];
    char *zErrMsg = 0;

    sprintf(sql, "SELECT * FROM %s;", table_name);

    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    
    sprintf(sql, "DROP TABLE %s;", table_name);
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    return rc;
}

#endif

static int create_table_CONTROL_DEVS()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE CONTROL_DEVS("  \
         "realName          TEXT        NOT NULL," \
         "friendlyName      TEXT        NOT NULL," \
         "udn               TEXT PRIMARY KEY NOT NULL," \
         "url               TEXT, "\
         "type              TEXT    NOT NULL,"\
         "added             TEXT    "\
         ");";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
            if(!check_column_exist("CONTROL_DEVS", "realName"))
            {
                rename_table("CONTROL_DEVS", "TempOldTable");
                create_new_CONTROL_table();
                convert_data("TempOldTable", convert_data_control_table_cb);
            }
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table CONTROL_DEVS created successfully\n");
    }

    return rc;
}

static int create_table_SUB_DEVICES()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE SUB_DEVICES("  \
             "owner             TEXT    NOT NULL," \
             "serial            TEXT   PRIMARY KEY NOT NULL," \
             "serialId          TEXT   NOT NULL," \
             "deviceType        TEXT   NOT NULL," \
             "typeBasic         TEXT," \
             "typeSpecific      TEXT," \
             "deviceMode        TEXT," \
             "friendlyName      TEXT," \
             "id                TEXT NOT NULL," \
             "capability        TEXT," \
             "profileId         TEXT," \
             "endpointNum       TEXT," \
             "parentId          TEXT," \
             "childrenId        TEXT," \
             "active            TEXT," \
             "isZWavePlus       TEXT," \
             "roleType          TEXT," \
             "nodeType          TEXT," \
             "alexa             TEXT NOT NULL,"\
             "type              TEXT NOT NULL);";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
            if(!check_column_exist("SUB_DEVICES", "parentId"))
            {
                rename_table("SUB_DEVICES", "TempOldTable");
                create_new_SUB_DEV_table();
                convert_data("TempOldTable", convert_data_sub_devices_cb);
                database_actions("system_handler", db, "DROP TABLE TMP_SUBDEVS");
                database_actions("system_handler", db, "DROP TABLE TMP_SUBDEVS");
                database_actions("system_handler", db, "DROP TABLE WEMO_WIFI_CONFIG");
            }

            if(!check_column_exist("SUB_DEVICES", "typeSpecific"))
            {
                rename_table("SUB_DEVICES", "TempOldTable");
                create_new_SUB_DEV2_table();
                convert_data("TempOldTable", convert_data_sub_devices2_cb);
            }
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table SUB_DEVICES created successfully\n");
    }

    return rc;
}

static int create_table_TMP_SUBDEVS()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE TMP_SUBDEVS("  \
         "owner             TEXT    NOT NULL," \
         "serial            TEXT   PRIMARY KEY NOT NULL," \
         "friendlyName      TEXT," \
         "id                TEXT NOT NULL," \
         "capability        TEXT," \
         "profileID         TEXT," \
         "endpointNum      TEXT);";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table TMP_SUBDEVS created successfully\n");
    }

    return rc;
}

static int create_table_DEVICES()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE DEVICES("  \
         "deviceId        TEXT   PRIMARY KEY   NOT NULL," \
         "deviceType      TEXT                 NOT NULL," \
         "name            TEXT                         ," \
         "owner           TEXT                 NOT NULL," \
         "url        TEXT);";

   /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
            if(!check_column_exist("DEVICES", "deviceId"))
            {
                rename_table("DEVICES", "TempOldTable");
                create_new_DEVICE_table();
                convert_data("TempOldTable", convert_data_device_cb);
            }
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table DEVICES created successfully\n");
    }

    return rc;
}

static int create_table_CAP()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;
    sql = "CREATE TABLE CAPABILITY("  \
         "deviceId        TEXT                 NOT NULL," \
         "capList         TEXT                 NOT NULL," \
         "scheme          TEXT                         ," \
         "inOut           TEXT                         ," \
         "endpointMem     TEXT                         ," \
         "type            TEXT                         ," \
         "option          TEXT                         ," \
         "PRIMARY KEY (deviceId, capList));";

   /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
            if(!check_column_exist("CAPABILITY", "endpointMem"))
            {
                rename_table("CAPABILITY", "TempOldTable");
                create_new_CAP_table();
                convert_data("TempOldTable", convert_data_CAP_cb);
            }
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table CAPABILITY created successfully\n");
    }

    return rc;
}

static int create_table_FEATURES()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;
    //featureid 01 ONOFF, 02 DIM, 03 ASSOCIATION, 04 CONFIGURATION, 05 SENSOR_MULTILEVEL, 06 BATTERY, 07 COLOR, 08 USER_CODE, 09 DOOR_LOCK
    sql = "CREATE TABLE FEATURES("  \
         "deviceId        TEXT                 NOT NULL," \
         "featureId       TEXT                 NOT NULL," \
         "register        TEXT                 NOT NULL," \
         "type            TEXT                 NOT NULL" \
         ");";

   /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table FEATURES created successfully\n");
    }

    return rc;
}

static int create_table_METER()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;
    sql = "CREATE TABLE METER_DATA("\
         "deviceId        TEXT                 NOT NULL," \
         "meterType       TEXT                 NOT NULL," \
         "meterValue      TEXT                 NOT NULL," \
         "meterUnit       TEXT                 NOT NULL," \
         "type            TEXT                 NOT NULL" \
         ");";

   /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table METER_DATA created successfully\n");
    }

    return rc;
}

static int create_table_ASSOCIATION()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;
    sql = "CREATE TABLE ASSOCIATION_DATA("\
         "deviceId        TEXT                 NOT NULL," \
         "groupId         TEXT                 NOT NULL," \
         "nodeList        TEXT                 NOT NULL," \
         "type            TEXT                 NOT NULL" \
         ");";

   /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table ASSOCIATION_DATA created successfully\n");
    }

    return rc;
}

static int create_table_SENSOR()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;
    sql = "CREATE TABLE SENSOR_DATA("\
         "deviceId        TEXT                 NOT NULL," \
         "sensorType      TEXT                 NOT NULL," \
         "sensorValue     TEXT                 NOT NULL," \
         "sensorUnit      TEXT                 NOT NULL," \
         "type            TEXT                 NOT NULL" \
         ");";

   /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table SENSOR_DATA created successfully\n");
    }

    return rc;
}

static int create_table_USER_CODE()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;
    sql = "CREATE TABLE USER_CODE("\
         "deviceId        TEXT                 NOT NULL," \
         "userId          TEXT                 NOT NULL," \
         "pass            TEXT                 NOT NULL," \
         "type            TEXT                 NOT NULL" \
         ");";

   /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table USER_CODE created successfully\n");
    }

    return rc;
}

static int create_table_THERMOSTAT_SETPOINT()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;
    sql = "CREATE TABLE THERMOSTAT_SETPOINT("\
         "deviceId        TEXT                 NOT NULL," \
         "mode            TEXT                 NOT NULL," \
         "value           TEXT                 NOT NULL," \
         "unit            TEXT                 NOT NULL," \
         "type            TEXT                 NOT NULL" \
         ");";

   /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table THERMOSTAT_SETPOINT created successfully\n");
    }

    return rc;
}

static int create_table_MORE_PROPERTY()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;
    sql = "CREATE TABLE MORE_PROPERTY("\
         "deviceId        TEXT                 NOT NULL," \
         "deviceType      TEXT                 NOT NULL," \
         "propertyId      TEXT                 NOT NULL," \
         "key             TEXT                 NOT NULL," \
         "value           TEXT                 NOT NULL," \
         "type            TEXT                 NOT NULL" \
         ");";

   /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table MORE_PROPERTY created successfully\n");
    }

    return rc;
}

static int create_table_GROUPS()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;
    
    sql = "CREATE TABLE GROUPS("  \
         "userId        TEXT                 NOT NULL," \
         "groupId       TEXT   PRIMARY KEY   NOT NULL," \
         "groupName     TEXT                 NOT NULL," \
         "device        TEXT                 NOT NULL" \
         ");";

   /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
            if(!check_column_exist("GROUPS", "userId"))
            {
                rename_table("GROUPS", "TempOldTable");
                create_new_GROUPS_table();
                convert_data("TempOldTable", convert_data_groups_cb);
            }
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table GROUPS created successfully\n");
    }

    return rc;
}

static int create_table_WEMO_WIFI_CONFIG()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE WEMO_WIFI_CONFIG("  \
         "udn               TEXT        ," \
         "quality           TEXT        NOT NULL," \
         "ssid              TEXT    PRIMARY KEY    NOT NULL," \
         "config            TEXT        NOT NULL);";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table WEMO_WIFI_CONFIG created successfully\n");
    }

    return rc;
}

static int create_table_RULES()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE RULES("  \
         "type          TEXT        NOT NULL," \
         "id            TEXT   PRIMARY KEY     NOT NULL," \
         "name          TEXT        NOT NULL," \
         "conditions    TEXT        NOT NULL," \
         "actions       TEXT        NOT NULL,"\
         "status        TEXT        NOT NULL,"\
         "run           TEXT        NOT NULL);";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
            if(!check_column_exist("RULES", "type"))
            {
                rename_table("RULES", "TempOldTable");
                create_new_RULES_table();
                convert_data("TempOldTable", convert_data_rules_cb);
                database_actions("system_handler", db, "DROP TABLE NEST");
                database_actions("system_handler", db, "DROP TABLE PREVIOUS_STATUS");
            }
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table RULES created successfully\n");
    }

    return rc;
}

static int create_table_SCENES()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE SCENES("  \
         "deviceId        TEXT                 NOT NULL," \
         "groupId         TEXT                 NOT NULL," \
         "buttonId        TEXT                 NOT NULL," \
         "sceneId         TEXT                         ," \
         "timeActive      INTEGER                      ," \
         "state           TEXT                         ," \
         "option          TEXT                         ," \
         "type            TEXT                         ," \
         "PRIMARY KEY (deviceId, groupId));";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table SCENES created successfully\n");
    }

    return rc;
}

static int create_table_NEST()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE NEST("  \
         "token         TEXT   PRIMARY KEY     NOT NULL," \
         "status        TEXT        NOT NULL,"\
         "options       TEXT        );";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table NEST created successfully\n");
    }

    return rc;
}

static int create_table_DEV_PREVIOUS_STATUS()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE PREVIOUS_STATUS("  \
         "deviceId      TEXT        NOT NULL," \
         "featureId       TEXT        NOT NULL," \
         "value         TEXT        NOT NULL,"\
         "PRIMARY KEY (deviceId, featureId));";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table PREVIOUS_STATUS created successfully\n");
    }

    return rc;
}

static int create_table_WIFI_INFO()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE WIFI_INFO("  \
         "quality           TEXT        NOT NULL," \
         "ssid              TEXT        NOT NULL," \
         "encryption        TEXT        NOT NULL," \
         "mode              TEXT        NOT NULL," \
         "length            TEXT        NOT NULL," \
         "crc               TEXT        NOT NULL);";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table WIFI_INFO created successfully\n");
    }

    return rc;
}

static int create_table_USERS()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;

    sql = "CREATE TABLE USERS("  \
         "userId         TEXT     PRIMARY KEY     NOT NULL," \
         "shareId          TEXT     NOT NULL," \
         "name             TEXT     NOT NULL," \
         "enable           TEXT     NOT NULL," \
         "userType         TEXT     NOT NULL," \
         "shareTime        TEXT     ," \
         "options          TEXT);";

    /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
            if(!check_column_exist("USERS", "userId"))
            {
                rename_table("USERS", "TempOldTable");
                create_new_USERS_table();
                convert_data("TempOldTable", convert_data_users_cb);
            }
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table USERS created successfully\n");
    }

    return rc;
}

static int create_table_ALARM()
{
    int  rc;
    char *sql;
    char *zErrMsg = 0;
    sql = "CREATE TABLE ALARM("\
         "deviceId        TEXT                      NOT NULL," \
         "groupAlarm      TEXT                      NOT NULL," \
         "alarmTypeFinal  TEXT                      NOT NULL," \
         "type            TEXT                      NOT NULL," \
         "PRIMARY KEY (deviceId, groupAlarm));";

   /* Execute SQL statement */
    rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGW("%s\n", zErrMsg);
        if(strstr(zErrMsg, "already exists"))
        {
            rc = SQLITE_OK;
        }
        sqlite3_free(zErrMsg);
    }
    else
    {
        SLOGI("Table ALARM created successfully\n");
    }

    return rc;
}

static int Check_path_is_Dir_or_File(char *path)
{
    struct stat st={0};
    if(stat(path, &st) == -1)
    {
        return -1;
    }
    switch(st.st_mode & S_IFMT)
    {
        case S_IFBLK: SLOGI("%s: this is Block device\n", path); break;
        case S_IFCHR: SLOGI("%s: this is Character device\n", path); break;
        case S_IFDIR: SLOGI("%s: this is Directory\n", path); return 1; break;
        case S_IFIFO: SLOGI("%s: this is FIFO/pipe\n", path); break;
        case S_IFLNK: SLOGI("%s: this is Symlink\n", path); break;
        case S_IFREG: SLOGI("%s: this is Regular file\n", path); return 0; break;
        case S_IFSOCK: SLOGI("%s: this is Socket\n", path); break;
        default: SLOGI("%s: unknown\n", path); break;
    }
    return -1;
}

int system_create_database()
{
    int rc;
    rc = Check_path_is_Dir_or_File("/etc/database");
    if(rc == -1)
    {
        VR_(execute_system)("mkdir /etc/database");
    }
    //
    /* Open database */
    rc = sqlite3_open(DEVICES_DATABASE, &db);
    if( rc )
    {
        SLOGE("Can't open database: %s\n", sqlite3_errmsg(db));
        return -1;
    }
    else
    {
        SLOGI("Opened DEVICES_DATABASE successfully\n");
    }
    create_table_CONTROL_DEVS();/*zwave zigbee controler, upnp devices*/
    create_table_CAP();/*zwave capability*/
    create_table_SUB_DEVICES();/*zwave, zigbee device infor*/
    create_table_TMP_SUBDEVS();/**/
    create_table_DEVICES();/*using to sync resources to cloud*/
    create_table_FEATURES();/*store status of device*/
    create_table_METER();/*store meter value and unit*/
    create_table_ASSOCIATION();/*store node list infor*/
    create_table_SENSOR();/*store sensor value and unit*/
    create_table_USER_CODE();/*store door lock user code*/
    create_table_THERMOSTAT_SETPOINT();/*store thermostat setpoint*/
    create_table_MORE_PROPERTY();/*store property of device*/
    create_table_SCENES();
    create_table_ALARM();

    create_table_GROUPS();
    create_table_WEMO_WIFI_CONFIG();

    sqlite3_close(db);

/////////////////////////////////////////

    rc = sqlite3_open(RULE_DATABASE, &db);
    if( rc )
    {
        SLOGE("Can't open database: %s\n", sqlite3_errmsg(db));
        return -1;
    }
    else
    {
        SLOGI("Opened RULE_DATABASE successfully\n");
    }

    create_table_RULES();
    create_table_DEV_PREVIOUS_STATUS();
    create_table_NEST();
    sqlite3_close(db);

/////////////////////////////////////////

    rc = sqlite3_open(WIFI_INFO_DATABASE, &db);
    if( rc )
    {
        SLOGE("Can't open database: %s\n", sqlite3_errmsg(db));
        return -1;
    }
    else
    {
        SLOGI("Opened WIFI_INFO_DATABASE successfully\n");
    }

    create_table_WIFI_INFO();
    sqlite3_close(db);
/////////////////////////////////////////

    rc = sqlite3_open(USERS_DATABASE, &db);
    if( rc )
    {
        SLOGE("Can't open database: %s\n", sqlite3_errmsg(db));
        return -1;
    }
    else
    {
        SLOGI("Opened USERS_DATABASE database successfully\n");
    }

    create_table_USERS();
    sqlite3_close(db);
    return rc; 
}