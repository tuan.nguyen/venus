/******************************************************************************
 * Copyright (c) 2014, VEriK Systems. All rights reserved.
 *
 * Module: Service Provider
 *
 ******************************************************************************/
#include <stdio.h>
#include <unistd.h>
#include <libubus.h>
#include <libubox/blobmsg_json.h>
#include <pthread.h>
#include <json-c/json.h>
#include <uci.h>

#include "slog.h"
#include "VR_define.h"
#include "verik_utils.h"
#include "led.h"
#include "vr_sound.h"
#include "vr_rest.h"

#include "database.h"
#include "timer.h"
#include "database_process.h"
#include "handle_button.h"
#include "ping.h"

#define TIME_GET_TEMPERATURE     60000  /* 60s */
static sqlite3 *db;
static struct blob_buf buff;

int g_shmid = 0;
char *g_shm = NULL;

int g_led_shmid = 0;
char *g_led_shm = NULL;

int g_wireless_connected = 0;
int g_ready_inform = 0;
int g_retry_mqtt_connect = 0;

static char g_log_location[SIZE_256B] ={0};

extern int g_setup_mode;
extern timer_t g_inform_after_quit_setup;

static int g_wireless_configuring = 0;
static int g_cloud_connected = 0;
static int g_open_close_network_inform = 0; // 0 open, 1 close
static int g_do_not_change_led = 0;//after connected, we need to disable AP mode

typedef struct _daemon_service 
{
    const char *ubus_name;
    uint32_t status;
    const char *service_name;
    uint32_t id;
    int handle_power_cycle;
} daemon_service;

daemon_service daemon_services[] = {
    {"zigbee",            0,        "zigbee_handler",       0,          0},
    {"zwave",             0,        "zwave_handler",        0,          0},
    {"upnp",              0,        "upnp_handler",         0,          1},//no need handle power cycle
    {"alljoyn",           0,        "alljoyn_handler",      0,          0},
    {"pubsub",            0,        "pubsub_handler",       0,          0},
    {"rule",              0,        "rule_handler",         0,          1},
    {"venus",             0,        "venus_handler",        0,          1},
};

static struct ubus_context *ctx;
static struct ubus_event_handler ubus_listener;

static const struct blobmsg_policy network_event_policy[2] = {
    [0] = { .name = "action", .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = "interface", .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy service_event_policy[2] = {
    [0] = { .name = "id", .type = BLOBMSG_TYPE_INT32 },
    [1] = { .name = "path", .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy service_state[2] = {
    [0] = { .name = "state", .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = "reason", .type = BLOBMSG_TYPE_STRING },
};


static void restart_all_wifi_services(void)
{
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++) 
    {
        if (!strcmp (daemon_services[i].ubus_name, "upnp") ||
            !strcmp (daemon_services[i].ubus_name, "pubsub") || 
            !strcmp (daemon_services[i].ubus_name, "alljoyn")
            )
        {
            daemon_services[i].status = 0;
        }
    }

    int retry = 500;
    int alljoyn_daemon_pid =  get_processId_from_name("alljoyn-daemon");
    while((alljoyn_daemon_pid == -1) && retry>0)
    {
        retry--;
        alljoyn_daemon_pid =  get_processId_from_name("alljoyn-daemon");
        usleep(10000);
    }

    if(alljoyn_daemon_pid != -1)
    {
        VR_(execute_system)("/etc/init.d/alljoyn restart");
        sleep(1);
    }
    else
    {
        unsigned int uptime = get_system_up_time();
        SLOGI("uptime = %d\n", uptime);
        if(uptime > 20)
        {
            VR_(execute_system)("/etc/init.d/alljoyn restart");
            sleep(1);
        }
    }

    char cmd[SIZE_256B];
    snprintf(cmd, sizeof(cmd), "/etc/start_services.sh start_wifiservice >> %s", g_log_location);
    VR_(execute_system)(cmd);
}

static int check_service_allow_run(const char *ubus_name)
{
    if(!ubus_name)
    {
        return 0;
    }

    char cmd[SIZE_256B];
    sprintf(cmd, "start_services.@wifi-services[0].%s", ubus_name);

    char *allow = VR_(read_option)(NULL, cmd);
    if(!allow)
    {
        sprintf(cmd, "start_services.@extra-services[0].%s", ubus_name);
        allow = VR_(read_option)(NULL, cmd);
    }

    if(!allow)
    {
        return 0;
    }
    int a = strtol(allow, NULL, 0);
    free(allow);

    return a;
}
static void check_service_running_cb(struct uloop_timeout *timeout)
{
    int i;
    int retry_mqtt = 0;
    if(!g_wireless_connected)
    {
        return;
    }

    char cmd[SIZE_256B];
    // SLOGI("######## SERVICE CHECKING ###########\n");R
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++) 
    {
        // SLOGI("%s %d", daemon_services[i].ubus_name, daemon_services[i].status);
        if(!daemon_services[i].status)
        {
            char result[SIZE_32B];
            sprintf(cmd, "pgrep %s", daemon_services[i].service_name);
            VR_(run_command)(cmd, result, sizeof(result));
            SLOGI("%s result = %s\n", cmd, result);
            if(strtol(result, NULL, 0) == 0)
            {
                if(check_service_allow_run(daemon_services[i].ubus_name))
                {
                    sprintf(cmd, "/etc/start_services.sh start_service %s >> %s", daemon_services[i].service_name, g_log_location);
                    VR_(execute_system)((const char*)cmd);
                }
            }
            else
            {
                daemon_services[i].status = 1;
            }
        }
    }

    /*auto restart pubsub if it not connect to server every 10mins*/
    if(!g_cloud_connected)
    {
        char *cloudState = VR_(read_option)(NULL, UCI_CLOUD_STATE);
        if(cloudState && !strcmp(cloudState, "connected"))
        {
            int res = ping_command("8.8.8.8", 3);

            if(!res)
            {
               if(g_retry_mqtt_connect)
                {
                    if(check_service_allow_run("pubsub"))
                    {
                        sprintf(cmd, "/etc/start_services.sh start_service pubsub_handler >> %s", g_log_location);
                        VR_(execute_system)((const char*)cmd);
                    }
                }
                else
                {
                    retry_mqtt = 1;
                }
            }
        }
        SAFE_FREE(cloudState);
    }
    g_retry_mqtt_connect = retry_mqtt;
    uloop_timeout_set(timeout, 10*60*1000);
}

static struct uloop_timeout check_service_running = {
    .cb = check_service_running_cb,
};

/*
1: up
0: down
*/
static int check_sta_status()
{
    char status[SIZE_256B];
    VR_(run_command)("ifstatus cereswifi | grep '\"up\":'", status, sizeof(status));
    printf("check_sta_status %s\n", status);
    if(strstr(status, "true"))
    {
        return 1;
    }

    return 0;
}

static void handle_STA_down(void *data)
{
    int res = check_sta_status();
    // printf("handle_STA_down res = %d\n", res);
    if(1 == res)
    {
        return;
    }

    if(!g_setup_mode)
    {
        if(!g_do_not_change_led)
        {
            VR_(set_led_state)(g_led_shm, ST_STA_connecting);
        }
    }

    int ap_on = VR_(is_wireless_up)(ST_AP_MODE);
    int wpa_on = VR_(is_wireless_up)(ST_STA_MODE);

    if(!ap_on && wpa_on && !g_wireless_configuring)
    {
        if(g_wireless_connected)
        {
            g_cloud_connected = 0;
            if(!g_setup_mode && g_ready_inform)
            {
                VR_(inform_router_lost_connection)(data);
            }
        }
    }
    g_wireless_connected = 0;
}

static void receive_data_cb(struct ubus_request *req, int type, struct blob_attr *msg)
{

}

static void complete_data_cb(struct ubus_request *req, int ret)
{
    if(req)
    {
        free(req);
        req = NULL;
    }
}

static void inform_setup_wifi_success()
{
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++) 
    {
        if(!strcmp(daemon_services[i].ubus_name, "alljoyn") &&
            daemon_services[i].id)
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_STATE_R));
            JSON_ADD_STRING_SAFE(jobj, ST_STATE, "3");
            JSON_ADD_STRING_SAFE(jobj, ST_MESSAGE, "Validated");

            blob_buf_init(&buff, 0);
            blobmsg_add_string(&buff, ST_MESSAGE, (char *)json_object_to_json_string(jobj));

            struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));

            VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_SEND_PUBLISH, buff.head, req,
                            receive_data_cb, complete_data_cb, NULL);

            
            json_object_put(jobj);
            break;
        }
    }
}

static void handle_STA_up(void *data)
{
    g_do_not_change_led = 0;
    if(!g_setup_mode)
    {
        /*to quit retry state*/
        VR_(write_option)("/etc/alljoyn-onboarding", UCI_ALLJOYN_ONBOARDING_STATE"=3");
    }

    /*incase retry wifi connect successfull, AP and STA are on, we should disable AP*/
    if(VR_(is_wireless_up)(ST_AP_MODE))
    {
        char *cloudState = VR_(read_option)(NULL, UCI_CLOUD_STATE);
        if(cloudState && !strcmp(cloudState, ST_CONNECTED))
        {
            VR_(execute_system)("/etc/init.d/alljoyn-onboarding poll_connection &");
            VR_(set_led_state)(g_led_shm, ST_STA_connecting);
            if(!g_setup_mode && !g_inform_after_quit_setup)
            {
                VR_(inform_wifi_connecting)(data);
            }

            SAFE_FREE(cloudState);
            return;
        }

        SAFE_FREE(cloudState);
    }

    /*for inform user claimed that wifi configuration successful*/
    char *local_state = (char *)VR_(read_option)(NULL, UCI_LOCAL_STATE);
    if(local_state && !strcmp(local_state,"configured"))
    {
        VR_(set_led_state)(g_led_shm, ST_STA_connected_secure_not_internet);
        inform_setup_wifi_success();
    }
    SAFE_FREE(local_state);

    VR_(led_show)(g_led_shm);
    char *state = VR_(read_option)(NULL, "auto_config.@auto-config[0].state");
    if(state && !strcmp(state,"1"))
    {
        free(state);
    }
    else
    {
        restart_all_wifi_services();
        SAFE_FREE(state);
    }

    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++) 
    {
        SLOGI("%s %d", daemon_services[i].ubus_name, daemon_services[i].status);
    }

    g_wireless_connected = 1;
    uloop_timeout_set(&check_service_running, 2*60*1000);
}

static void handle_network_change(struct ubus_context *ctx, const char *interface, const char *action)
{
    if(!interface || !action)
    {
        return;
    }

    if(!strcmp(interface, "cereswifi"))
    {
        if(!strcmp(action,"ifdown"))
        {
            handle_STA_down((void *)ctx);
        }
        else if(!strcmp(action,"ifup"))
        {
            handle_STA_up((void *)ctx);
        }
    }
    else if(!strcmp(interface, "lan"))
    {
        if(!strcmp(action,"ifdown"))
        {
            //nothing
        }
        else if(!strcmp(action,"ifup"))
        {
            g_wireless_connected = 0;
            char *local_state = VR_(read_option)(NULL, UCI_LOCAL_STATE);
            if(local_state)
            {
                if(!strcmp(local_state, "available"))
                {
                    VR_(set_led_state)(g_led_shm, ST_AP_config_mode);
                }
                else
                {
                    SLOGI("g_setup_mode = %d\n", g_setup_mode);
                    if(g_setup_mode)
                    {
                        VR_(set_led_state)(g_led_shm, ST_AP_switch_mode);
                    }
                    else
                    {
                        if(!g_do_not_change_led)
                        {
                            VR_(set_led_state)(g_led_shm, ST_AP_mode);
                        }
                    }
                }
                free(local_state);
            }
            else
            {
                VR_(set_led_state)(g_led_shm, ST_AP_config_mode);
            }

            restart_all_wifi_services();

            int i;
            for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++) 
            {
                SLOGI("%s %d", daemon_services[i].ubus_name, daemon_services[i].status);
            }
        }
    }
}

static void restart_service(const char *service_name, uint32_t id)
{
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++) 
    {
        if(!strcmp (daemon_services[i].ubus_name, service_name))
        {
            if(daemon_services[i].id == 0 || daemon_services[i].id == id)
            {
                daemon_services[i].status = 0;
                char cmd[256];
                memset(cmd, 0x00, sizeof(cmd));
                sprintf(cmd, "/etc/start_services.sh start_service %s >> %s", daemon_services[i].service_name, g_log_location);
                VR_(execute_system)((const char*)cmd);
            }
        }
        SLOGI("%s %d", daemon_services[i].ubus_name, daemon_services[i].status);
    }
}

static void update_service_info(const char *service_name, uint32_t id)
{
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++) 
    {
        if (!strcmp (daemon_services[i].ubus_name, service_name))
        {
            daemon_services[i].status = 1;
            daemon_services[i].id = id;
        }
        SLOGI("%s %d", daemon_services[i].ubus_name, daemon_services[i].status);
    }
}

static void handle_onboarding_state(struct ubus_context *ctx, const char *state)
{
    if(!state)
    {
        return;
    }

    if(!strcmp(state, "notconfigured"))
    {
        char *local_state = VR_(read_option)(NULL, UCI_LOCAL_STATE);
        if(local_state)
        {
            if(!strcmp(local_state, "available"))
            {
                VR_(set_led_state)(g_led_shm, ST_AP_config_mode);
                VR_(introduce_system)((void *)ctx);
            }
            else
            {
                VR_(set_led_state)(g_led_shm, ST_AP_mode);
            }
            free(local_state);
        }
        else
        {
            VR_(set_led_state)(g_led_shm, ST_AP_config_mode);
            VR_(introduce_system)((void *)ctx);
        }
    }
    else if(!strcmp(state, "configured"))
    {
        g_wireless_configuring = 1;
        VR_(inform_receive_config)((void *)ctx);
        VR_(set_led_state)(g_led_shm, ST_STA_connecting);
        /*inform all ready after reconfig wifi*/
        g_ready_inform = 0;
    }
    else if(!strcmp(state, "validating_failed"))
    {
        VR_(inform_config_failed)((void *)ctx);
        VR_(set_led_state)(g_led_shm, ST_AP_config_mode);
    }
    else if(!strcmp(state, "retry") && !g_setup_mode)
    {
        VR_(set_led_state)(g_led_shm, ST_AP_mode);
        VR_(inform_became_access_point)((void *)ctx);
    }
    else if(!strcmp(state, "connected") && !g_setup_mode)
    {
        g_wireless_configuring = 0;
        VR_(led_show)(g_led_shm);
    }
    else if(!strcmp(state, "unauthorized"))
    {
        VR_(inform_wifi_wrong_pass)((void *)ctx);
    }
    else if(!strcmp(state, "error"))
    {
        // INFORM WITI PASSWORK MAYBE INCORRECT, PLEASE HELP TO RECONFIG IN APP
        // YOUR WIFI INFOMATION MAYBE IS NOT CORRECT, PLEASE TRY TO SETUP AGAIN.
        VR_(inform_wifi_wrong_pass)((void *)ctx);
    }
    else if(!strcmp(state, "updateLanNetwork"))
    {
        g_do_not_change_led = 1;
    }
}

static void handle_pubsub_state(struct ubus_context *ctx, const char *state, const char *reason)
{
    if(!state || !reason)
    {
        return;
    }

    if(!strcmp(state, "connect_failed"))
    {
        VR_(set_led_state)(g_led_shm, ST_STA_connected_secure_not_internet);
    }
    else if(!strcmp(state, "register_failed"))
    {
        VR_(set_led_state)(g_led_shm, ST_STA_connected_secure_not_internet);
        if(!strcmp(reason, "connectError"))
        {
            VR_(inform_register_connect_error)((void *)ctx);
        }
        else if(!strcmp(reason, "cloudError"))
        {
            VR_(inform_register_failed)((void *)ctx);
        }
    }
    else if(!strcmp(state, "claimed") || !strcmp(state, "registered"))
    {
        g_do_not_change_led = 1;
        /*restart alljoyn for sync deviceId is hubUUID*/
        // VR_(execute_system)("/etc/start_services.sh start_service alljoyn_handler");
        // VR_(write_option)(NULL, "firewall.@zone[-1].masq=0");
        // VR_(write_option)(NULL, UCI_WIFI_AP_MODE_DISABLED"=1");
        // VR_(execute_system)("wifi");
        // /etc/init.d/firewall restart
    }
    else if(!strcmp(state, "unclaimed"))
    {
        VR_(inform_unclaimed)((void *)ctx);
    }
    else if(!strcmp(state, "connected") && !g_setup_mode)
    {
        VR_(set_led_state)(g_led_shm, ST_STA_connected_secure_has_internet);
        if(!g_ready_inform)
        {
            VR_(inform_system_ready)((void *)ctx);
        }
        else
        {
            if(!g_cloud_connected)
            {
                VR_(inform_cloud_connected)((void *)ctx);
            }
        }
        g_cloud_connected = 1;
        g_ready_inform = 1;
    }
    else if(!strcmp(state, "disconnected"))
    {
        g_cloud_connected = 0;
        VR_(set_led_state)(g_led_shm, ST_STA_connected_secure_not_internet);
        if(g_ready_inform)
        {
            VR_(inform_cloud_disconnected)((void *)ctx);
        }
    }
    else if(!strcmp(state, "firmware_updating"))
    {
        VR_(set_led_state)(g_led_shm, ST_Firmware_updating);
        VR_(inform_firmware_updating)((void *)ctx);
    }
    else if(!strcmp(state, "firmware_updated"))
    {
        VR_(set_led_state)(g_led_shm, ST_Firmware_updated);
        VR_(inform_firmware_updated)((void *)ctx);
    }
}

static void handle_alljoyn_state(struct ubus_context *ctx, const char *state)
{
    if(!state)
    {
        return;
    }

    if(!strcmp(state, "has_internet"))
    {
        VR_(set_led_state)(g_led_shm, ST_STA_connected_unsecure_has_internet);
    }
    else if(!strcmp(state, "not_internet"))
    {
        VR_(set_led_state)(g_led_shm, ST_STA_connected_unsecure_not_internet);
    }
    else if(!strcmp(state, "firmware_updating"))
    {
        VR_(set_led_state)(g_led_shm, ST_Firmware_updating);
        VR_(inform_firmware_updating)((void *)ctx);
    }
    else if(!strcmp(state, "firmware_updated"))
    {
        VR_(set_led_state)(g_led_shm, ST_Firmware_updated);
        VR_(inform_firmware_updated)((void *)ctx);
    }
}

static void handle_zwave_zigbee_state(const char *state)
{
    if(!state)
    {
        return;
    }

    if(!strcmp(state, "open_network"))
    {
        if(!g_open_close_network_inform)
        {
            g_open_close_network_inform = 1;
            // SLOGI("VOICE OPEN NETWORK INFORM HERE\n");
            VR_(set_led_state)(g_led_shm, ST_Open_network);
        }
    }
    else if(!strcmp(state, "close_network"))
    {
        if(g_open_close_network_inform)
        {
            g_open_close_network_inform = 0;
            // SLOGI("VOICE CLOSE NETWORK INFORM HERE\n");
            VR_(set_led_state)(g_led_shm, ST_Close_network);
        }
    }
    else if(!strcmp(state, "uart_failed"))
    {
        VR_(set_led_state)(g_led_shm, ST_Error);
    }
}

static void receive_event(struct ubus_context *ctx, struct ubus_event_handler *ev,
              const char *type, struct blob_attr *msg)
{
    char *str = blobmsg_format_json(msg, true);
    SLOGI("{ \"%s\": %s }\n", type, str);
    free(str);

    if(!strcmp(type, "network.interface"))
    {
        struct blob_attr *tb[2];
        blobmsg_parse(network_event_policy, ARRAY_SIZE(network_event_policy), tb, blob_data(msg), blob_len(msg));
        const char *action = "unknown";
        const char *interface = "unknown";

        if(!tb[0] || !tb[1])
        {
            return;
        }

        action = blobmsg_data(tb[0]);
        interface = blobmsg_data(tb[1]);

        handle_network_change(ctx, interface, action);
    }
    else if(!strcmp(type, "ubus.object.remove"))
    {
        struct blob_attr *tb[2];
        blobmsg_parse(service_event_policy, ARRAY_SIZE(service_event_policy), tb, blob_data(msg), blob_len(msg));
        uint32_t id;
        const char *service = "unknown";
        if(!tb[0] || !tb[1])
        {
            return;
        }

        id = blobmsg_get_u32(tb[0]);
        service = blobmsg_data(tb[1]);

        restart_service(service, id);
    }
    else if(!strcmp(type, "ubus.object.add"))
    {
        struct blob_attr *tb[2];
        blobmsg_parse(service_event_policy, ARRAY_SIZE(service_event_policy), tb, blob_data(msg), blob_len(msg));
        uint32_t id;
        const char *service = "unknown";
        if(!tb[0] || !tb[1])
        {
            return;
        }

        id = blobmsg_get_u32(tb[0]);
        service = blobmsg_data(tb[1]);

        update_service_info(service, id);
    }
    else if(!strcmp(type, "onboarding"))
    {
        struct blob_attr *tb[1];
        blobmsg_parse(service_state, ARRAY_SIZE(service_state), tb, blob_data(msg), blob_len(msg));
        if(!tb[0])
        {
            return;
        }

        const char *state = blobmsg_data(tb[0]);
        handle_onboarding_state(ctx, state);
    }
    else if(!strcmp(type, "pubsub") && g_wireless_connected)
    {
        struct blob_attr *tb[2];
        blobmsg_parse(service_state, ARRAY_SIZE(service_state), tb, blob_data(msg), blob_len(msg));

        if(!tb[0])
        {
            return;
        }
        const char *state = blobmsg_data(tb[0]);
        const char *reason = ST_UNKNOWN;
        if(tb[1])
        {
            reason = blobmsg_data(tb[1]);
        }
        handle_pubsub_state(ctx, state, reason);
    }
    else if(!strcmp(type, "alljoyn"))
    {
        struct blob_attr *tb[1];
        blobmsg_parse(service_state, ARRAY_SIZE(service_state), tb, blob_data(msg), blob_len(msg));

        if(!tb[0])
        {
            return;
        }
        const char *state = blobmsg_data(tb[0]);
        handle_alljoyn_state(ctx, state);
    }
    else if(!strcmp(type, "zwave"))
    {
        struct blob_attr *tb[1];
        blobmsg_parse(service_state, ARRAY_SIZE(service_state), tb, blob_data(msg), blob_len(msg));

        if(!tb[0])
        {
            return;
        }
        const char *state = blobmsg_data(tb[0]);
        handle_zwave_zigbee_state(state);
    }
    else if(!strcmp(type, "zigbee"))
    {
        struct blob_attr *tb[1];
        blobmsg_parse(service_state, ARRAY_SIZE(service_state), tb, blob_data(msg), blob_len(msg));

        if(!tb[0])
        {
            return;
        }
        const char *state = blobmsg_data(tb[0]);
        handle_zwave_zigbee_state(state);
    }
}

static int init_ubus_service()
{
    const char *ubus_socket = NULL;

    uloop_init();

    ctx = ubus_connect(ubus_socket);
    if (!ctx) {
        SLOGE("Failed to connect to ubus\n");
        return -1;
    }

    ubus_add_uloop(ctx);

    return 0;
}

static int check_service_is_running(char *service_name)
{
    if(!service_name)
    {
        return 0;
    }

    int i, res = 0;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(daemon_services[i].service_name, service_name))
        {
            res = daemon_services[i].status;
            break;
        }
    }
    return res;
}

static int g_power_inform_value = 0;
static void check_power_cycle_cb(struct uloop_timeout *timeout)
{
    /*checking is power cycle*/
    if(!g_power_inform_value)
    {
        char *isPowerCycle = VR_(read_option)(NULL, UCI_FIRMWARE_POWER_CYCLE);
        if(!isPowerCycle)
        {
            return;
        }

        if(!strcmp(isPowerCycle, ST_ON_VALUE))
        {
            SLOGI("######## INFORM HUB IS POWERCYCLE #########\n");
            g_power_inform_value = 1;
            VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE_TIME"=%u", (unsigned)time(NULL)-10);
        }
        else
        {
            VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=1");
            free(isPowerCycle);
            return;
        }
    }

    if(!g_power_inform_value)
    {
        return;
    }

    int i, ret = 1;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!daemon_services[i].handle_power_cycle &&
            check_service_is_running((char*)daemon_services[i].service_name))
        {
            daemon_services[i].handle_power_cycle = 1;
            if(ctx)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_SERVICE, daemon_services[i].ubus_name);
                ubus_send_event(ctx, ST_POWER_CYCLE, buff.head);
            }
        }

        ret &= daemon_services[i].handle_power_cycle;
    }

    if(!ret)
    {
        uloop_timeout_set(timeout, 3000);
    }
}

static struct uloop_timeout power_cycle_timeout = {
    .cb = check_power_cycle_cb,
};

static void handle_temperature(struct uloop_timeout *timeout)
{
    float temp;
    char temperature[SIZE_32B];
    VR_(run_command)(GET_TEMP_COMMAND, temperature, sizeof(temperature));

    temp = strtol(temperature, NULL, 0);
    temp = temp/1000;

    // SLOGI("Temperature Current: %f oC\n", temp);

    /* If temperature more than 75 oC we need a warning*/
    if(temp > 75)
    {
        SLOGI("WARNING!! The temperature is at level dangerous\n");
    }

    uloop_timeout_set(timeout, TIME_GET_TEMPERATURE);
}

static struct uloop_timeout temperature = {
    .cb = handle_temperature,
};

#define ST_SET_LED_STATE        "setLed"

static const struct blobmsg_policy setLedStatePolicy[1] = {
    [0] = { .name = ST_STATE, .type = BLOBMSG_TYPE_STRING },
};

static int setLedState(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    const char *state = "(unknown)";
    struct blob_attr *tb[1];
    blobmsg_parse(setLedStatePolicy, ARRAY_SIZE(setLedStatePolicy), tb, blob_data(msg), blob_len(msg));
    if(tb[0])
    {
        state = blobmsg_data(tb[0]);
        VR_(set_led_state)(g_led_shm, (char*)state);
    }
    return 0;
}
static const struct ubus_method system_methods[] = {
    // UBUS_METHOD("register_notify", register_notify, notify_policy),
    UBUS_METHOD(ST_SET_LED_STATE, setLedState, setLedStatePolicy),
};

static struct ubus_object_type system_object_type =
    UBUS_OBJECT_TYPE("venus.system", system_methods);

static struct ubus_object system_object = {
    //.subscribe_cb = test_client_subscribe_cb,
    .name = "venus.system",
    .type = &system_object_type,
    .methods = system_methods,
    .n_methods = ARRAY_SIZE(system_methods),
};

static void manage_ubus_service()
{
    int ret, i;

    ret = ubus_add_object(ctx, &system_object);
    if (ret) {
        SLOGE("Failed to add_object object: %s\n", ubus_strerror(ret));
        return;
    }

    for(i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!ubus_lookup_id(ctx, daemon_services[i].ubus_name, &daemon_services[i].id))
        {
            SLOGW("FOUND object %s with id %08X: %s\n", daemon_services[i].ubus_name, daemon_services[i].id, ubus_strerror(ret));
            daemon_services[i].status = 1;
            SLOGI("%s %d", daemon_services[i].ubus_name, daemon_services[i].status);
        }
    }

    memset(&ubus_listener, 0, sizeof(ubus_listener));
    ubus_listener.cb = receive_event;
    ret = ubus_register_event_handler(ctx, &ubus_listener, "*");
    if (ret)
        SLOGE("Failed to register event handler: %s\n", ubus_strerror(ret));

    uloop_timeout_set(&power_cycle_timeout, 1000);
    uloop_timeout_set(&temperature, TIME_GET_TEMPERATURE);
    uloop_run();
}

static void free_ubus_service()
{
    if(ctx)
    {
        ubus_free(ctx);
    }
    uloop_done();
}

static int create_share_memory_database_cb(void *jobj, int argc, char **argv, char **azColName)
{
    char *cloudid = NULL;
    char *deviceid = NULL;
    char *devicetype = NULL;
    if(argv[0])
    {
        cloudid = argv[0];
    }
    if(argv[1])
    {
        deviceid = argv[1];
    }
    if(argv[2])
    {
        devicetype = argv[2];
    }

    if(jobj && cloudid)
    {
        json_object *cloud_dev = json_object_new_object();
        JSON_ADD_STRING_SAFE(cloud_dev, ST_ID, deviceid);
        JSON_ADD_STRING_SAFE(cloud_dev, ST_TYPE, devicetype);
        json_object_object_add(jobj, cloudid, cloud_dev);
    }

    return 0;
}

int main()
{
    SLOG_Init(SLOG_STDOUT, NULL);
    set_thread_name( "manage_service" );

    timerInit();

    if(system_create_database() != SQLITE_OK)
    {
        return 0;
    }

    get_log_location(g_log_location, sizeof(g_log_location));

    if(!shm_init(&g_shm, &g_shmid))
    {
        open_database(DEVICES_DATABASE, &db);
        json_object *jobj = json_object_new_object();
        searching_database("system_handler", db, create_share_memory_database_cb, (void *)jobj, 
                            "SELECT owner,deviceId,deviceType from DEVICES");

        char *share_memory = (char *)json_object_to_json_string(jobj);
        SLOGI("share_memory: %s\n", share_memory);
        shm_write_data(g_shm, share_memory);
        json_object_put(jobj);
    }

    if(!led_shm_init(&g_led_shm, &g_led_shmid, "/etc/led_control.sh"))
    {
        json_object *led_obj = json_object_new_object();
        json_object *in_queue = json_object_new_array();
        char *local_state = (char *)VR_(read_option)(NULL, UCI_LOCAL_STATE);
        if(local_state && !strcmp(local_state, "available"))
        {
            json_object_array_add(in_queue, json_object_new_string(ST_Builder_Default));
        }
        else
        {
            json_object_array_add(in_queue, json_object_new_string(ST_Default));
        }
        json_object_object_add(led_obj, ST_CURRENT_STATE, json_object_new_string("bootUp"));
        json_object_object_add(led_obj, ST_IN_QUEUE, in_queue);
        char *share_memory = (char *)json_object_to_json_string(led_obj);
        SLOGI("share_memory: %s\n", share_memory);
        shm_write_data(g_led_shm, share_memory);
        json_object_put(led_obj);

        SAFE_FREE(local_state);
    }

    pthread_t handle_button_thread;
    pthread_create(&handle_button_thread, NULL, (void *)&handle_button, NULL);

    init_ubus_service();
    manage_ubus_service();
    free_ubus_service();
    return 0;
}


