#ifndef DATABASE_PROCESS_H
#define DATABASE_PROCESS_H

#include <sqlite3.h>

#define NON_SEC_SCHEME 255
#define SEC_0_SCHEME     7

int system_create_database();

#endif