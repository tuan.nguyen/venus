#ifndef HANDLE_BUTTON_H
#define HANDLE_BUTTON_H

#include <linux/input.h>

#define EV_MAP( N ) [ N ] = #N

#define RESET_BUTTON 	KEY_1
#define WIFI_BUTTON 	KEY_3
#define TEMP_EVENT  	KEY_4

#define INFORM_NEARLY_SUCCESS      10 //10 * 15 * 10000(us)
#define RESET_DONE_HOLD_TIME       60 //60 * 15 * 10000(us)

#define INTERVAL_CHECKING          15 // 15 * 10000(us)
#define POOLING_TIME  			   10000 //us

#define WIFI_SETTING_HOLD_TIME     60 //60 * 15 * 10000(us)

#define QUITING_SETUP_MODE 4
#define ENTERING_SETUP_MODE 3
#define ENTER_SETUP_MODE 1
#define QUIT_SETUP_MODE 0

typedef struct _button_handle_info 
{
    struct input_event bt_ev;
    int press_times;
    int hold;
} button_handle_info;

#define BUTTON_HANDLE_PATH "/dev/input/event1"

int handle_button(void *data);

#endif