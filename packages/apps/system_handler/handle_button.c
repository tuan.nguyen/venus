#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#include "slog.h"
#include "VR_define.h"
#include "verik_utils.h"
#include "handle_button.h"
#include "vr_sound.h"
#include "led.h"
#include "timer.h"

static const char *EV_NAME[EV_MAX] = {
#include "evtable_EV.h"
};

static const char *KEY_NAME[KEY_MAX] = {
#include "evtable_KEY.h"
#include "evtable_BTN.h"
};

static const char *SW_NAME[SW_MAX] = {
#include "evtable_SW.h"
};

button_handle_info buttons[]=
{
    {{{0,0},    EV_KEY,     KEY_1,      0},      0,      0},/*restore*/
    {{{0,0},    EV_KEY,     KEY_3,      0},      0,      0},/*wiri*/
};

int g_speak_reset = 0;
int g_reset_success = 0;

int g_setup_mode = QUIT_SETUP_MODE;
int g_set_LED_quit_setup_mode = 0;
timer_t g_inform_after_quit_setup = 0;

extern int g_ready_inform;
extern int g_wireless_connected;
extern char *g_led_shm ;

const char *lookup_event_name_i( const int evtype, const int evcode ) {
    if (evtype == EV_KEY) {
        return (KEY_MAX >= evcode ? KEY_NAME[ evcode ] : NULL);
    }
    if (evtype == EV_SW) {
        return (SW_MAX >= evcode ? SW_NAME[ evcode ] : NULL);
    }
    return NULL;
}

const char *lookup_event_name( const struct input_event ev ) {
    return lookup_event_name_i( ev.type, ev.code );
}

const char *lookup_type_name_i( const int evtype ) {
    return (EV_MAX >= evtype ? EV_NAME[ evtype ] : NULL);
}

const char *lookup_type_name( const struct input_event ev ) {
    return lookup_type_name_i( ev.type );
}

// static void print_event(char* dev, struct input_event ev) {
//     const char *typename = lookup_type_name( ev );
//     const char *evname = lookup_event_name( ev );
//     if ( evname != NULL ) {
//         printf( "%d.%06d\t%s\t%s\t%d\t%s\n", (int)ev.time.tv_sec, (int)ev.time.tv_usec, typename, evname, ev.value, dev );
//     } else {
//         fprintf( stderr, "Unknown %s event id on %s : %d (value %d)\n", typename, dev, ev.code, ev.value );
//     }
//     fflush(stdout);
// }

static int get_button(uint16_t code, uint16_t value)
{
    int i;
    for(i=0; i<sizeof(buttons)/sizeof(button_handle_info); i++)
    {
        if((buttons[i].bt_ev.code == code) && (buttons[i].bt_ev.value != value))
        {
            return i;
        }
    }

    return -1;
}

int handle_button(void *data)
{
    set_thread_name( "handler_button" );
    int fd = -1;
    int count_down = INTERVAL_CHECKING, hold_time = 0;
    fd = open(BUTTON_HANDLE_PATH, O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd == -1) 
    {
        return(-1);
    }

    // Turn off blocking for reads, use (fd, F_SETFL, FNDELAY) if you want that
    fcntl(fd, F_SETFL, fcntl(fd, F_GETFL) | O_NONBLOCK);

    while(1)
    {
        struct input_event ev;
        int n = read( fd, &ev, sizeof(ev) );
        if ( n != sizeof(ev) ) 
        {
            goto calculator;
        }

        if (ev.type != EV_KEY && ev.type != EV_SW)
        {
            goto calculator;
        }

        // print_event( BUTTON_HANDLE_PATH, ev );
        switch (ev.code)
        {
            case RESET_BUTTON:
            {
                int i = get_button(ev.code, ev.value);
                if(i == -1)
                {
                    break;
                }

                if(ev.value)
                {
                    buttons[i].hold = 1;
                    VR_(set_led_state)(g_led_shm, ST_Reset);
                }
                else
                {
                    if((ev.time.tv_sec - buttons[i].bt_ev.time.tv_sec) <= 1)
                    {
                        buttons[i].press_times ++;
                    }

                    hold_time = 0;
                    buttons[i].hold = 0;
                    
                    if(!g_reset_success)
                    {
                        VR_(set_led_state)(g_led_shm, ST_Stop_reset);
                    }

                    if(g_speak_reset)
                    {
                        g_speak_reset = 0;
                    }
                    // printf("buttons[%d].press_times = %d\n", i, buttons[i].press_times);
                    // printf("buttons[%d].hold = %d\n", i, buttons[i].hold);
                }

                count_down = INTERVAL_CHECKING;
                buttons[i].bt_ev.time.tv_sec = ev.time.tv_sec;
                buttons[i].bt_ev.time.tv_usec = ev.time.tv_usec;
                buttons[i].bt_ev.value = ev.value;
                break;
            }

            case WIFI_BUTTON:
            {
                int i = get_button(ev.code, ev.value);
                if(i == -1)
                {
                    break;
                }

                if(ev.value)
                {
                    buttons[i].hold = 1;
                    if(g_setup_mode == QUIT_SETUP_MODE)
                    {
                        g_setup_mode = ENTERING_SETUP_MODE;
                    }
                    else if(g_setup_mode == ENTER_SETUP_MODE)
                    {
                        g_setup_mode = QUITING_SETUP_MODE;
                    }
                    VR_(set_led_state)(g_led_shm, ST_Switch_AP_mode);
                }
                else
                {
                    if(g_setup_mode == ENTERING_SETUP_MODE)
                    {
                        g_setup_mode = QUIT_SETUP_MODE;
                    }
                    else if(g_setup_mode == QUITING_SETUP_MODE)
                    {
                        g_setup_mode = ENTER_SETUP_MODE;
                    }

                    if((ev.time.tv_sec - buttons[i].bt_ev.time.tv_sec) <= 1)
                    {
                        buttons[i].press_times ++;
                    }

                    hold_time = 0;
                    buttons[i].hold = 0;

                    if(!g_set_LED_quit_setup_mode)
                    {
                        VR_(set_led_state)(g_led_shm, ST_Stop_switch_AP_mode);
                    }
                    else
                    {
                        g_set_LED_quit_setup_mode = 0;
                    }

                    if(g_speak_reset)
                    {
                        g_speak_reset = 0;
                    }
                    // printf("buttons[%d].press_times = %d\n", i, buttons[i].press_times);
                    // printf("buttons[%d].hold = %d\n", i, buttons[i].hold);
                }

                count_down = INTERVAL_CHECKING;
                buttons[i].bt_ev.time.tv_sec = ev.time.tv_sec;
                buttons[i].bt_ev.time.tv_usec = ev.time.tv_usec;
                buttons[i].bt_ev.value = ev.value;
                break;
            }

            case TEMP_EVENT:
            {
                if(ev.value)
                {
                    SLOGI("TEMPERATURE TOO HIGH\n");
                    VR_(execute_system)("poweroff");
                }
                else
                {
                    SLOGI("TEMPERATURE OK NOW\n");
                }
                break;
            }
        }

calculator:        
        if(!count_down)
        {
            int i;
            for(i=0; i<sizeof(buttons)/sizeof(button_handle_info); i++)
            {
                if(buttons[i].press_times == 3  && (buttons[i].bt_ev.code == RESET_BUTTON))
                {
                    printf("ACTION buttons[%d].press_times = %d\n", i, buttons[i].press_times);
                    // VR_(execute_system)("/etc/start_services.sh start_service radiomon >> /system.log");
                    buttons[i].press_times = 0;
                }

                if(buttons[i].hold && (buttons[i].bt_ev.code == RESET_BUTTON))
                {
                    count_down = INTERVAL_CHECKING;
                    hold_time++;
                    if(hold_time == INFORM_NEARLY_SUCCESS)
                    {
                        g_speak_reset = 1;
                        VR_(inform_reseting)(NULL);
                        // sleep(2);
                    }

                    if(hold_time < RESET_DONE_HOLD_TIME)
                    {
                        break;
                    }

                    g_reset_success = 1;
                    VR_(set_led_state)(g_led_shm, ST_Reset_done);

                    hold_time = 0;
                    buttons[i].hold = 0;
                    VR_(execute_system)("ubus call zwave reset");
                    VR_(execute_system)("ubus call zigbee reset");
                    sleep(2);
                    VR_(execute_system)("rm -rf /overlay/*");
                    VR_(execute_system)("reboot -f");
                }

                if(buttons[i].hold && (buttons[i].bt_ev.code == WIFI_BUTTON))
                {
                    count_down = INTERVAL_CHECKING;
                    hold_time++;
                    if(hold_time == INFORM_NEARLY_SUCCESS)
                    {
                        g_speak_reset = 1;
                        if(g_setup_mode == ENTERING_SETUP_MODE)
                        {
                            VR_(inform_entry_setup_mode)(NULL);
                        }
                        else if(g_setup_mode == QUITING_SETUP_MODE)
                        {
                            VR_(inform_exit_setup_mode)(NULL);
                        }
                    }

                    if(hold_time < WIFI_SETTING_HOLD_TIME) // >6s
                    {
                        break;
                    }

                    g_speak_reset = 0;
                    hold_time = 0;
                    buttons[i].hold = 0; 
                    char *onboardingState = VR_(read_option)("/etc/alljoyn-onboarding", UCI_ALLJOYN_ONBOARDING_STATE);
                    if(onboardingState)
                    {
                        SLOGI("onboardingState = %s\n", onboardingState);
                        int state = strtol(onboardingState, NULL, 0);
                        free(onboardingState);
                        if(state == 0)
                        {
                            /*meaning new device, alway ap mode*/
                            g_setup_mode = QUIT_SETUP_MODE;
                            break;
                        }
                        else if(state == 3)
                        {
                            /*meaning device connected*/
                            if(g_setup_mode == QUITING_SETUP_MODE)
                            {
                                if(!VR_(is_wireless_up)(ST_AP_MODE))
                                {
                                    g_setup_mode = QUIT_SETUP_MODE;
                                    VR_(inform_system_ready)(NULL);
                                    break;
                                }
                            }
                        }
                    }

                    if(g_setup_mode == ENTERING_SETUP_MODE)
                    {
                        /*update mode*/
                        g_setup_mode = ENTER_SETUP_MODE;

                        /*reset control quit setup mode*/
                        if(g_inform_after_quit_setup)
                        {
                            timerCancel(&g_inform_after_quit_setup);
                        }

                        VR_(inform_setup_mode_ready)(NULL);
                        VR_(set_led_state)(g_led_shm, ST_AP_switch_mode);
                        char *ssid = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_SSID);
                        if(ssid)
                        {
                            VR_(write_option)("/etc/alljoyn-onboarding", UCI_ALLJOYN_ONBOARDING_STATE"=1");
                            free(ssid);
                        }
                        else
                        {
                            VR_(write_option)("/etc/alljoyn-onboarding", UCI_ALLJOYN_ONBOARDING_STATE"=0");
                        }
                        VR_(execute_system)("/etc/setup_mode.sh enter &");
                    }
                    else if(g_setup_mode == QUITING_SETUP_MODE)
                    {
                        g_setup_mode = QUIT_SETUP_MODE;
                        g_ready_inform = 0;/*for said iam ready*/
                        char *ssid = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_SSID);
                        if(ssid)
                        {
                            g_set_LED_quit_setup_mode = 1;
                            VR_(inform_wifi_connecting)(NULL);
                            VR_(set_led_state)(g_led_shm, ST_Stop_switch_AP_mode);
                            VR_(set_led_state)(g_led_shm, ST_STA_connecting);

                            VR_(write_option)("/etc/alljoyn-onboarding", UCI_ALLJOYN_ONBOARDING_STATE"=3");
                            VR_(execute_system)("/etc/setup_mode.sh quit &");
                            // VR_(execute_system)("/etc/init.d/alljoyn-onboarding connect &");

                            timerStart(&g_inform_after_quit_setup, NULL, NULL, 15000, TIMER_ONETIME);
                            free(ssid);
                        }
                    }
                }
            }
        }
        else
        {
            count_down --;
        }
        usleep(POOLING_TIME);
    }
    close(fd);
}