/*
 * Copyright 2010-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

/**
 * @file subscribe_publish_sample.c
 * @brief simple MQTT publish and subscribe on the same topic
 *
 * This example takes the parameters from the aws_iot_config.h file and establishes a connection to the AWS IoT MQTT Platform.
 * It subscribes and publishes to the same topic - "sdkTest/sub"
 *
 * If all the certs are correct, you should see the messages received by the application in a loop.
 *
 * The application takes in the certificate path, host name , port and the number of times the publish should happen.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>

#include <signal.h>
#include <memory.h>
#include <sys/time.h>
#include <limits.h>
#include <aws_iot_mqtt_client.h>

#include <libubus.h>
#include <libubox/blobmsg_json.h>
#include <json-c/json.h>
#include <uci.h>
#include <curl/curl.h>

#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"
#include "aws_iot_config.h"

#include "slog.h"
#include "awsiot_handler.h"
#include "database.h"
#include "VR_define.h"

AWS_IoT_Client client;
static struct ubus_context *ctx;
static struct blob_buf buff;
static struct ubus_subscriber ubus_subscribe_handler;
static struct ubus_event_handler ubus_event_listener;

static sqlite3 *awsiot_db;
new_awsiot_ubus *g_awsiot_ubus = NULL;
static int list_count = 0;

pthread_mutex_t awsiot_ubus_listMutex;

daemon_service daemon_services[] = {
    {"zigbee_handler",      "zigbee",            0,        0},
    {"zwave_handler",       "zwave",             0,        0},
    {"upnp_handler",        "upnp",              0,        0},
};

static const struct blobmsg_policy msg_policy[1] = {
    [0] = { .name = "msg", .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy handle_ubus_notify_policy[__NOTIFY_SIZE] = {
    [NOTIFY_SERVICE] = { .name = "service_name", .type = BLOBMSG_TYPE_STRING },
    [NOTIFY_MSG] = { .name = "notify_message", .type = BLOBMSG_TYPE_STRING },
};

const char *cmd_support[] = {
    "notify",
    "set_time",
    "get_time",
    "auto_conf",
    "add_devices",
    "list_devices",
    "remove_device",
    "reset",
    "open_closenetwork",
    "get_subdevs",
    "get_binary",
    "set_binary",
    "change_name",
    "alexa",
    "identify",
    "firmware_actions",
    "set_rule",
    "get_rule",
    "rule_actions",
    "read_spec",
    "write_spec",
    "read_s_spec",
    "write_s_spec",
    "get_topic",
    "set_key",
    "rediscover",
    "create_group",
    "group_actions",
    "group_control",
    "network",
};

static char* VR_(read_option)(const char* option)
{
    struct uci_context *ctx;
    struct uci_ptr ptr;
    int ret;
    char* data = NULL;

    char *input = strdup(option);
    ctx = uci_alloc_context();
    if (!ctx)
    {
        SLOGI("Failed to alloc uci_context.\n");
        return data;
    }
    ret = uci_lookup_ptr(ctx, &ptr, input, 1);

    if(ret == UCI_OK)
    {
        if(ptr.o)
        {
            data = strdup(ptr.o->v.string);
        }     
    }
    uci_free_context(ctx);
    free(input);
    return data;
}

static void VR_(add_last_list)(new_awsiot_ubus *node)
{
    pthread_mutex_lock(&awsiot_ubus_listMutex);
    new_awsiot_ubus *tmp = g_awsiot_ubus;
    // while(tmp)
    // {
    //     printf("name = %s action_time = %d\n", tmp->name, tmp->action_time);
    //     tmp=tmp->next;
    // }
    // tmp = g_allubus;
    if(tmp)
    {
        while(tmp->next)
        {
            tmp=tmp->next;
        }
        tmp->next = node;
        list_count++;
    }
    else
    {
        g_awsiot_ubus = node;
        list_count++;
    }
    pthread_mutex_unlock(&awsiot_ubus_listMutex);
}

static void VR_(remove_list)(new_awsiot_ubus *node)
{
    pthread_mutex_lock(&awsiot_ubus_listMutex);
    new_awsiot_ubus *tmp = g_awsiot_ubus;
    new_awsiot_ubus *pre = g_awsiot_ubus;

    while(tmp)
    {
        if(tmp->action_time == node->action_time && tmp->req == node->req)
        {
            if(tmp == g_awsiot_ubus)
            {
                g_awsiot_ubus = tmp->next;
                free(tmp);
                list_count--;
            }
            else
            {
                pre->next = tmp->next;
                free(tmp);
                list_count--;
            }
            break;
        }
        pre=tmp;
        tmp=tmp->next;
    }
    pthread_mutex_unlock(&awsiot_ubus_listMutex);
}

static const char* VR_(get_string_data)(const char* message, const char* keyname){

    /* convert json message to json object */
    json_object * jobj = NULL;
    jobj = json_tokener_parse(message); //error in here

    if (!jobj)
    {
        return NULL;
    }

    /* scan json object */
    json_object_object_foreach(jobj, key, val)
    {
        if(!strcmp(key, keyname)) {
            return (const char*) json_object_get_string(val);
        }
    }
    return NULL;
}

void VR_(Send_message)(const char *Topic, QoS qos, const char *msg)
{
	IoT_Publish_Message_Params params;
	params.qos = qos;
	params.payload = (void *) msg;
	params.payloadLen = strlen(msg) + 1;
	SLOGI("msg = %s\n", msg);
    if(Topic && strlen(Topic))
    {
    	int rc = aws_iot_mqtt_publish(&client, Topic, strlen(Topic), &params);
    	//if(!rc)
    	//{
    		SLOGI("rc = %d\n", rc);
    	//}
    }
    else
    {
    	int rc = aws_iot_mqtt_publish(&client, PUBLISH_RES_TOPIC, strlen(PUBLISH_RES_TOPIC), &params);
    	// if(!rc)
    	// {
    		SLOGI("rc = %d\n", rc);
    	// }
    }
}

static void VR_(receive_data_cb)(struct ubus_request *req, int type, struct blob_attr *msg)
{
    const char *msgstr = "(unknown)";
    struct blob_attr *tb[__NOTIFY_MAX];
    blobmsg_parse(msg_policy, ARRAY_SIZE(msg_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
    {
        msgstr = blobmsg_data(tb[0]);
    }
    SLOGI("msgstr = %s\n", msgstr);
    new_awsiot_ubus *awsiot_ubus = ((new_awsiot_ubus *)(req->priv));

    if(awsiot_ubus)
    {
        SLOGI("req.priv = %x\n", req->priv);
        SLOGI("awsiot_ubus = %x\n", awsiot_ubus);

        SLOGI("awsiot_ubus.name = %s\n", awsiot_ubus->name);
        SLOGI("awsiot_ubus.type = %d\n", awsiot_ubus->type);
        SLOGI("awsiot_ubus.topic = %s\n", awsiot_ubus->topic);

        if(awsiot_ubus->name && strlen(awsiot_ubus->name))
        {
            //printf("cmd_support = %d\n", sizeof(cmd_support)/sizeof(char*));
            int i;
            for (i = 0; i < sizeof(cmd_support)/sizeof(char*); i++)
            {
                if(!strcmp(awsiot_ubus->name, cmd_support[i]))
                {
                    int j;
                    for (j = 0; j < sizeof(daemon_services)/sizeof(daemon_service); j++)
                    {
                        if(awsiot_ubus->service_name && (!strcmp((char*)awsiot_ubus->service_name, daemon_services[j].name)))
                        {
                            daemon_services[j].timeout = 0;
                        }
                    }
                    
                    switch(awsiot_ubus->type)
                    {
                        case 0:
                        {
                        	VR_(Send_message)(NULL, QOS0, msgstr);
                            break;
                        }
                        case 1:
                        {
                            VR_(Send_message)(awsiot_ubus->topic, QOS0, msgstr);
                            break;
                        }
                        case 2:
                        {
                            
                            break;
                        }
                        default:
                            SLOGW("not support\n");
                            break;
                    }

                    break;
                }
            }
        }
        // new_allubus *tmp = g_allubus;
        // while(tmp)
        // {
        //     printf("name = %s\n", tmp->name);
        //     printf("action_time = %d\n\n", tmp->action_time);
        //     tmp = tmp->next;
        // }

        VR_(remove_list)(awsiot_ubus);
        //free(data);

        // tmp = g_allubus;
        // while(tmp)
        // {
        //     printf("name = %s\n", tmp->name);
        //     printf("action_time = %d\n\n", tmp->action_time);
        //     tmp = tmp->next;
        // }
    }
}

static int test_count=0;
static void VR_(complete_data_cb)(struct ubus_request *req, int ret)
{
    printf("free req\n");
    // if(req->priv)
    // {
    //     free(req->priv);
    // };
    if(req)
    {
        free(req);
        req = NULL;
    }
    
    test_count++;
    printf("%d\n",test_count);
    printf("%d\n",list_count);
    printf("free done\n");
}

static int features_callback(void *status, int argc, char **argv, char **azColName)
{
    json_object_object_add(status, argv[0], json_object_new_string(argv[1] ? argv[1] : "NULL"));
    return 0;
}

static int listdevice_callback(void *devicelist, int argc, char **argv, char **azColName)
{
   //  int i;
   // for(i=0; i<argc; i++){

   //    printf("%s = %s | ", azColName[i], argv[i] ? argv[i] : "NULL");
   //    //if(data != NULL) strcpy(data, argv[i]);
   // }
   // printf("\n");
    int i;
    json_object * jobj = json_object_new_object();
    char device_ID[64];
    for(i=0; i<argc; i++)
    {
        if(!strcmp(azColName[i], "ID") || !strcmp(azColName[i], "UDN"))
        {
            strcpy(device_ID, argv[i]);
        }
        json_object_object_add(jobj, azColName[i], json_object_new_string(argv[i] ? argv[i] : "NULL"));
    }

    json_object * status = json_object_new_object();

    searching_database("awsiot_handler", awsiot_db, features_callback, (void *)status,
                        "SELECT featureid,register from FEATURES where deviceid='%s'", device_ID);

    json_object_object_add(jobj, "status", status);
    json_object_array_add(devicelist, jobj);
    return 0;
}

static void VR_(list_device)(const char *service_type, const char *msg)
{

    SLOGI("The command %s is called \n ", __func__);

    const char *topic = VR_(get_string_data)(msg, "topic");

    if(!strcmp(service_type, "all"))
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, "type", json_object_new_string("all"));
        json_object_object_add(jobj, "method", json_object_new_string("list_devicesR"));
        json_object *devicelist = json_object_new_array();

        searching_database("awsiot_handler", awsiot_db, listdevice_callback, (void *)devicelist,
                    "SELECT * from CONTROL_DEVS where Type='upnp'");

        searching_database("awsiot_handler", awsiot_db, listdevice_callback, (char *)devicelist, 
                    "SELECT Owner,SerialID,SUB_DEVICES.FriendlyName,ID,Capability,Type from SUB_DEVICES "
                    "LEFT JOIN CONTROL_DEVS on Owner=UDN ");

        json_object_object_add(jobj, "devicesList", devicelist);
        VR_(Send_message)(topic, QOS0, (char *)json_object_to_json_string(jobj));
    }
    else
    {
        new_awsiot_ubus *awsiot_ubus = (new_awsiot_ubus*)malloc(sizeof(new_awsiot_ubus));
        memset(awsiot_ubus, 0x00, sizeof(new_awsiot_ubus));
        strcpy(awsiot_ubus->name, "list_devices");
        SLOGI("topic = %s\n", topic);
        if(topic)
    	{
    		strcpy(awsiot_ubus->topic, topic);
        }

        VR_(add_last_list)(awsiot_ubus);

        int i;
        awsiot_ubus->type = 1;
        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            SLOGI("daemon_services[%d].name = %s id = %08X\n", i, daemon_services[i].name, daemon_services[i].id);
            if(!strcmp(service_type, daemon_services[i].name))
            {
                awsiot_ubus->service_name = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {
                    awsiot_ubus->action_time = (unsigned)time(NULL);

                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    awsiot_ubus->req=req;
                    
                    ubus_invoke_async(ctx, daemon_services[i].id, "list_devices", NULL, req);
                    req->data_cb = VR_(receive_data_cb);
                    req->complete_cb = VR_(complete_data_cb);
                    req->priv = (void *) awsiot_ubus;
                    ubus_complete_request_async(ctx, req);
                    break;
                }
            }
        }
    }
}

static void VR_(set_binary)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    const char* id = VR_(get_string_data)(msg, "id");
    const char* value = VR_(get_string_data)(msg, "value");
    const char* cmd = VR_(get_string_data)(msg, "cmd");
    const char* subdevID = VR_(get_string_data)(msg, "subdevID");

    const char* topic = VR_(get_string_data)(msg, "topic");

    printf("service_type = %s\n,", service_type);
    printf("id = %s\n,", id);
    printf("value = %s\n,", value);
    printf("cmd = %s\n,", cmd);
    printf("subdevID = %s\n,", subdevID);
    if(service_type && !strcasecmp(service_type, "venus"))
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, "method", json_object_new_string("set_binaryR"));
        json_object_object_add(jobj, "type", json_object_new_string("venus"));
        
        if(!strcasecmp(id, "white_leds"))
        {
            json_object_object_add(jobj, "deviceid", json_object_new_string("white_leds"));
            if(!strcmp(value, "0"))
            {
                system("/etc/led_control.sh Motion_UNOCCUPIED > /dev/console");
            }
            else
            {
                system("/etc/led_control.sh Motion_OCCUPIED > /dev/console");
            }
            json_object_object_add(jobj, "value", json_object_new_string(value));
            json_object_object_add(jobj, "status", json_object_new_string("successful"));
        }
        else if(!strcasecmp(id, "motion"))
        {
            json_object_object_add(jobj, "deviceid", json_object_new_string("motion"));
            char *venus_motion_effect = NULL;
            printf("start read option\n");
            venus_motion_effect = (char *)VR_(read_option)("security.@venus-motion[0].white_led");
            printf("end read option\n");
            if(!venus_motion_effect)
            {
                system("uci add security venus-motion");
                system("uci set security.@venus-motion[0].white_led='NO'");
                system("uci commit security");
            }

            if(!strcmp(value, "ON"))
            {
                if(venus_motion_effect && !strcmp(venus_motion_effect, "NO"))
                {
                    system("uci set security.@venus-motion[0].white_led='YES'");
                    system("uci commit security");
                    free(venus_motion_effect);
                }
            }
            else
            {
                if(venus_motion_effect && !strcmp(venus_motion_effect, "YES"))
                {
                    system("uci set security.@venus-motion[0].white_led='NO'");
                    system("uci commit security");
                    free(venus_motion_effect);
                }
            }
            json_object_object_add(jobj, "value", json_object_new_string(value));
            json_object_object_add(jobj, "status", json_object_new_string("successful"));
        }
        else if(!strcasecmp(id, "location"))
        {
            char *latitude = VR_(read_option)("security.@location[0].latitude");
            char *longitude = VR_(read_option)("security.@location[0].longitude");
            if(!latitude && !longitude)
            {
                char cmd[256];
                system("uci add security location");
                sprintf(cmd, "uci set security.@location[0].latitude='%s'", value); system(cmd);
                sprintf(cmd, "uci set security.@location[0].longitude='%s'", subdevID); system(cmd);
                system("uci commit security");
            }
            else
            {
                char cmd[256];
                sprintf(cmd, "uci set security.@location[0].latitude='%s'", value); system(cmd);
                sprintf(cmd, "uci set security.@location[0].longitude='%s'", subdevID); system(cmd);
                system("uci commit security");
                free(latitude);
                free(longitude);
            }
        }
        else 
        {
            json_object_object_add(jobj, "status", json_object_new_string("failed"));
            json_object_object_add(jobj, "reason", json_object_new_string("wrong device's id"));
        }
        VR_(Send_message)(topic, QOS0, (char *)json_object_to_json_string(jobj));
    }
    else
    {
        new_awsiot_ubus *awsiot_ubus = (new_awsiot_ubus*)malloc(sizeof(new_awsiot_ubus));
        memset(awsiot_ubus, 0x00, sizeof(new_awsiot_ubus));
        strcpy(awsiot_ubus->name, "set_binary");
        SLOGI("topic = %s\n", topic);
        if(topic)
    	{
    		strcpy(awsiot_ubus->topic, topic);
        }

        VR_(add_last_list)(awsiot_ubus);

        int i;
        awsiot_ubus->type = 0;
        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            SLOGI("daemon_services[%d].name = %s id = %08X\n", i, daemon_services[i].name, daemon_services[i].id);
            if(!strcmp(service_type, daemon_services[i].name))
            {
                awsiot_ubus->service_name = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {
                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, "id", id);
                    blobmsg_add_string(&buff, "cmd", cmd);
                    blobmsg_add_string(&buff, "value", value);
                    if(subdevID && strlen(subdevID)) 
                    {
                        if(!strcmp(service_type, "upnp"))
                        {
                            blobmsg_add_string(&buff, "subdevID", subdevID);
                        }
                    }

                    awsiot_ubus->action_time = (unsigned)time(NULL);

                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    awsiot_ubus->req=req;
                    ubus_invoke_async(ctx, daemon_services[i].id, "set_binary", buff.head, req);
                    req->data_cb = VR_(receive_data_cb);
                    req->complete_cb = VR_(complete_data_cb);
                    req->priv = (void *) awsiot_ubus;
                    ubus_complete_request_async(ctx, req);
                    break;
                }
            }
        }
    }
}

static void VR_(get_binary)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    const char* id = VR_(get_string_data)(msg, "id");
    const char* topic = VR_(get_string_data)(msg, "topic");
    const char* subdevID = VR_(get_string_data)(msg, "subdevID");

    if(service_type && !strcasecmp(service_type, "venus"))
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, "method", json_object_new_string("get_binaryR"));
        json_object_object_add(jobj, "type", json_object_new_string("venus"));
        if(!strcasecmp(id, "temp"))
        {
            json_object_object_add(jobj, "deviceid", json_object_new_string("temp"));
            char buff[32];
            memset(buff, 0x00, sizeof(buff));
            FILE *fp;
            fp = popen("cat /sys/bus/i2c/devices/0-0049/temp1_input", "r");
            if(fp)
            {
                fgets(buff, 31, fp);
                char *pos;
                if ((pos=strchr(buff, '\n')) != NULL)
                    *pos = '\0';
                pclose(fp);
            }
            if(strlen(buff))
            {
                json_object_object_add(jobj, "value", json_object_new_string(buff));
                json_object_object_add(jobj, "status", json_object_new_string("successful"));
            }
            else
            {
                json_object_object_add(jobj, "status", json_object_new_string("failed"));
            }
            VR_(Send_message)(topic, QOS0, (char *)json_object_to_json_string(jobj));
        }
        else if(!strcasecmp(id, "location"))
        {
            json_object_object_add(jobj, "deviceid", json_object_new_string("location"));
            
            char *latitude = VR_(read_option)("security.@location[0].latitude");
            char *longitude = VR_(read_option)("security.@location[0].longitude");

            if(latitude && longitude)
            {
                json_object_object_add(jobj, "latitude", json_object_new_string(latitude));
                json_object_object_add(jobj, "longitude", json_object_new_string(longitude));
                json_object_object_add(jobj, "status", json_object_new_string("successful"));
            }
            else
            {
                if(latitude)
                    free(latitude);
                if(longitude)
                    free(longitude);
                json_object_object_add(jobj, "status", json_object_new_string("failed"));
            }
            VR_(Send_message)(topic, QOS0, (char *)json_object_to_json_string(jobj));
        }
    }
    else
    {
        new_awsiot_ubus *awsiot_ubus = (new_awsiot_ubus*)malloc(sizeof(new_awsiot_ubus));
        memset(awsiot_ubus, 0x00, sizeof(new_awsiot_ubus));
        strcpy(awsiot_ubus->name, "get_binary");
        SLOGI("topic = %s\n", topic);
        if(topic)
    	{
    		strcpy(awsiot_ubus->topic, topic);
        }

        VR_(add_last_list)(awsiot_ubus);

        int i;
        awsiot_ubus->type = 1;
        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            SLOGI("daemon_services[%d].name = %s id = %08X\n", i, daemon_services[i].name, daemon_services[i].id);
            if(!strcmp(service_type, daemon_services[i].name))
            {
                awsiot_ubus->service_name = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {
                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, "id", id);
                    if(subdevID && strlen(subdevID)) 
                    {
                        if(!strcmp(service_type, "upnp"))
                        {
                            blobmsg_add_string(&buff, "subdevID", subdevID);
                        }
                    }

                    awsiot_ubus->action_time = (unsigned)time(NULL);

                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    awsiot_ubus->req=req;
                    ubus_invoke_async(ctx, daemon_services[i].id, "get_binary", buff.head, req);
                    req->data_cb = VR_(receive_data_cb);
                    req->complete_cb = VR_(complete_data_cb);
                    req->priv = (void *) awsiot_ubus;
                    ubus_complete_request_async(ctx, req);
                    break;
                }
            }
        }
    }
}

static void VR_(change_name)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    const char* id = VR_(get_string_data)(msg, "id");
    const char* value = VR_(get_string_data)(msg, "value");
	const char* subdevID = VR_(get_string_data)(msg, "subdevID");
	const char* topic = VR_(get_string_data)(msg, "topic");

    new_awsiot_ubus *awsiot_ubus = (new_awsiot_ubus*)malloc(sizeof(new_awsiot_ubus));
    memset(awsiot_ubus, 0x00, sizeof(new_awsiot_ubus));
    strcpy(awsiot_ubus->name, "change_name");
    SLOGI("topic = %s\n", topic);
    if(topic)
	{
		strcpy(awsiot_ubus->topic, topic);
    }

    VR_(add_last_list)(awsiot_ubus);

    int i;
    awsiot_ubus->type = 0;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        SLOGI("daemon_services[%d].name = %s id = %08X\n", i, daemon_services[i].name, daemon_services[i].id);
        if(!strcmp(service_type, daemon_services[i].name))
        {
            awsiot_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, "id", id);
                if(value && strlen(value)) 
                {
                    blobmsg_add_string(&buff, "value", value);
                }
                
                if(subdevID && strlen(subdevID)) 
                {
                    if(!strcmp(service_type, "upnp"))
                    {
                        blobmsg_add_string(&buff, "subdevID", subdevID);
                    }
                }

                awsiot_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                awsiot_ubus->req=req;
                ubus_invoke_async(ctx, daemon_services[i].id, "change_name", buff.head, req);
                req->data_cb = VR_(receive_data_cb);
                req->complete_cb = VR_(complete_data_cb);
                req->priv = (void *) awsiot_ubus;
                ubus_complete_request_async(ctx, req);
                break;
            }
        }
    }
}

static void VR_(reset)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    const char* topic = VR_(get_string_data)(msg, "topic");
    if (!strcmp(service_type, "hard"))
    {
        SLOGI("factory reset\n");
        system("rm -rf /overlay/*");
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, "method", json_object_new_string("resetR"));
        json_object_object_add(jobj, "reset_type", json_object_new_string(service_type));
        json_object_object_add(jobj, "status", json_object_new_string("successful"));
        VR_(Send_message)(topic, QOS0, (char *)json_object_to_json_string(jobj));
        system("reboot");
    }
    else if(!strcmp(service_type, "reboot"))
    {
        SLOGI("reboot\n");
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, "method", json_object_new_string("resetR"));
        json_object_object_add(jobj, "reset_type", json_object_new_string(service_type));
        json_object_object_add(jobj, "status", json_object_new_string("successful"));
        VR_(Send_message)(topic, QOS0, (char *)json_object_to_json_string(jobj));
        system("reboot");
    }
    else if(!strcmp(service_type, "wifi_settings"))
    {
        SLOGI("wifi_settings\n");
        system("rm -rf /overlay/etc/config/*");
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, "method", json_object_new_string("resetR"));
        json_object_object_add(jobj, "reset_type", json_object_new_string(service_type));
        json_object_object_add(jobj, "status", json_object_new_string("successful"));
        VR_(Send_message)(topic, QOS0, (char *)json_object_to_json_string(jobj));
        system("reboot");
    }
    else if(!strcmp(service_type, "upnp"))
    {
        new_awsiot_ubus *awsiot_ubus = (new_awsiot_ubus*)malloc(sizeof(new_awsiot_ubus));
        memset(awsiot_ubus, 0x00, sizeof(new_awsiot_ubus));

        strcpy(awsiot_ubus->name, "reset");
        if(topic)
		{
			strcpy(awsiot_ubus->topic, topic);
	    }
        awsiot_ubus->type = 1;

        VR_(add_last_list)(awsiot_ubus);
        int i;
        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(service_type, daemon_services[i].name))
            {
                awsiot_ubus->service_name = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {
                    awsiot_ubus->action_time = (unsigned)time(NULL);

                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    awsiot_ubus->req=req;

                    ubus_invoke_async(ctx, daemon_services[i].id, "reset", NULL, req);
                    req->data_cb = VR_(receive_data_cb);
                    req->complete_cb = VR_(complete_data_cb);
                    req->priv = (void *) awsiot_ubus;
                    ubus_complete_request_async(ctx, req);
                    
                    break;
                }
            }
        }
    }
}

static void VR_(get_specification)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    const char* id = VR_(get_string_data)(msg, "id");
    const char* class = VR_(get_string_data)(msg, "class");
    const char* command = VR_(get_string_data)(msg, "command");
    const char* data0 = VR_(get_string_data)(msg, "data0");
    const char* data1 = VR_(get_string_data)(msg, "data1");
    const char* data2 = VR_(get_string_data)(msg, "data2");
    const char* topic = VR_(get_string_data)(msg, "topic");

    new_awsiot_ubus *awsiot_ubus = (new_awsiot_ubus*)malloc(sizeof(new_awsiot_ubus));
    memset(awsiot_ubus, 0x00, sizeof(new_awsiot_ubus));

    strcpy(awsiot_ubus->name, "read_spec");
    if(topic)
	{
		strcpy(awsiot_ubus->topic, topic);
    }
    awsiot_ubus->type = 1;

    VR_(add_last_list)(awsiot_ubus);
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(service_type, daemon_services[i].name))
        {
            awsiot_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
            	blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, "id", id);
                blobmsg_add_string(&buff, "class", class);
                blobmsg_add_string(&buff, "cmd", command);
                if(data0 && strlen(data0)) blobmsg_add_string(&buff, "data0", data0);
                if(data1 && strlen(data1)) blobmsg_add_string(&buff, "data1", data1);
                if(data2 && strlen(data2)) blobmsg_add_string(&buff, "data2", data2);

                awsiot_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                awsiot_ubus->req=req;

                ubus_invoke_async(ctx, daemon_services[i].id, "get_specification", buff.head, req);
                req->data_cb = VR_(receive_data_cb);
                req->complete_cb = VR_(complete_data_cb);
                req->priv = (void *) awsiot_ubus;
                ubus_complete_request_async(ctx, req);
                
                break;
            }
        }
    }
}

static void VR_(set_specification)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    const char* id = VR_(get_string_data)(msg, "id");
    const char* class = VR_(get_string_data)(msg, "class");
    const char* command = VR_(get_string_data)(msg, "command");
    const char* data0 = VR_(get_string_data)(msg, "data0");
    const char* data1 = VR_(get_string_data)(msg, "data1");
    const char* data2 = VR_(get_string_data)(msg, "data2");
    const char* topic = VR_(get_string_data)(msg, "topic");

    new_awsiot_ubus *awsiot_ubus = (new_awsiot_ubus*)malloc(sizeof(new_awsiot_ubus));
    memset(awsiot_ubus, 0x00, sizeof(new_awsiot_ubus));

    strcpy(awsiot_ubus->name, "write_spec");
    if(topic)
	{
		strcpy(awsiot_ubus->topic, topic);
    }
    awsiot_ubus->type = 0;

    VR_(add_last_list)(awsiot_ubus);
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(service_type, daemon_services[i].name))
        {
            awsiot_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
            	blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, "id", id);
                blobmsg_add_string(&buff, "class", class);
                blobmsg_add_string(&buff, "cmd", command);
                if(data0 && strlen(data0)) blobmsg_add_string(&buff, "data0", data0);
                if(data1 && strlen(data1)) blobmsg_add_string(&buff, "data1", data1);
                if(data2 && strlen(data2)) blobmsg_add_string(&buff, "data2", data2);

                awsiot_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                awsiot_ubus->req=req;

                ubus_invoke_async(ctx, daemon_services[i].id, "set_specification", buff.head, req);
                req->data_cb = VR_(receive_data_cb);
                req->complete_cb = VR_(complete_data_cb);
                req->priv = (void *) awsiot_ubus;
                ubus_complete_request_async(ctx, req);
                
                break;
            }
        }
    }
}

static void VR_(read_s_spec)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    const char* id = VR_(get_string_data)(msg, "id");
    const char* class = VR_(get_string_data)(msg, "class");
    const char* command = VR_(get_string_data)(msg, "command");
    const char* data0 = VR_(get_string_data)(msg, "data0");

    const char* topic = VR_(get_string_data)(msg, "topic");

    new_awsiot_ubus *awsiot_ubus = (new_awsiot_ubus*)malloc(sizeof(new_awsiot_ubus));
    memset(awsiot_ubus, 0x00, sizeof(new_awsiot_ubus));

    strcpy(awsiot_ubus->name, "read_s_spec");
    if(topic)
	{
		strcpy(awsiot_ubus->topic, topic);
    }
    awsiot_ubus->type = 1;

    VR_(add_last_list)(awsiot_ubus);
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(service_type, daemon_services[i].name))
        {
            awsiot_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
            	blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, "id", id);
                blobmsg_add_string(&buff, "class", class);
                blobmsg_add_string(&buff, "cmd", command);
                if(data0 && strlen(data0)) blobmsg_add_string(&buff, "data0", data0);

                awsiot_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                awsiot_ubus->req=req;

                ubus_invoke_async(ctx, daemon_services[i].id, "get_secure_spec", buff.head, req);
                req->data_cb = VR_(receive_data_cb);
                req->complete_cb = VR_(complete_data_cb);
                req->priv = (void *) awsiot_ubus;
                ubus_complete_request_async(ctx, req);
                
                break;
            }
        }
    }
}

static void VR_(write_s_spec)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);

    const char* id = VR_(get_string_data)(msg, "id");
    const char* class = VR_(get_string_data)(msg, "class");
    const char* command = VR_(get_string_data)(msg, "command");
    const char* data0 = VR_(get_string_data)(msg, "data0");
    const char* data1 = VR_(get_string_data)(msg, "data1");
    const char* data2 = VR_(get_string_data)(msg, "data2");
    const char* topic = VR_(get_string_data)(msg, "topic");

    new_awsiot_ubus *awsiot_ubus = (new_awsiot_ubus*)malloc(sizeof(new_awsiot_ubus));
    memset(awsiot_ubus, 0x00, sizeof(new_awsiot_ubus));

    strcpy(awsiot_ubus->name, "write_s_spec");
    if(topic)
	{
		strcpy(awsiot_ubus->topic, topic);
    }
    awsiot_ubus->type = 0;

    VR_(add_last_list)(awsiot_ubus);
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(service_type, daemon_services[i].name))
        {
            awsiot_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
            	blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, "id", id);
                blobmsg_add_string(&buff, "class", class);
                blobmsg_add_string(&buff, "cmd", command);
                if(data0 && strlen(data0)) blobmsg_add_string(&buff, "data0", data0);
                if(data1 && strlen(data1)) blobmsg_add_string(&buff, "data1", data1);
                if(data2 && strlen(data2)) blobmsg_add_string(&buff, "data2", data2);

                awsiot_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                awsiot_ubus->req=req;

                ubus_invoke_async(ctx, daemon_services[i].id, "set_secure_spec", buff.head, req);
                req->data_cb = VR_(receive_data_cb);
                req->complete_cb = VR_(complete_data_cb);
                req->priv = (void *) awsiot_ubus;
                ubus_complete_request_async(ctx, req);
                break;
            }
        }
    }
}

static void VR_(identify)(const char *service_type, const char *msg)
{
    SLOGI("The command %s is called \n ", __func__);
    const char* id = VR_(get_string_data)(msg, "id");
    const char* value = VR_(get_string_data)(msg, "value");
	const char* subdevID = VR_(get_string_data)(msg, "subdevID");
	const char* topic = VR_(get_string_data)(msg, "topic");

    new_awsiot_ubus *awsiot_ubus = (new_awsiot_ubus*)malloc(sizeof(new_awsiot_ubus));
    memset(awsiot_ubus, 0x00, sizeof(new_awsiot_ubus));
    strcpy(awsiot_ubus->name, "identify");
    SLOGI("topic = %s\n", topic);
    if(topic)
	{
		strcpy(awsiot_ubus->topic, topic);
    }

    VR_(add_last_list)(awsiot_ubus);

    int i;
    awsiot_ubus->type = 0;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        SLOGI("daemon_services[%d].name = %s id = %08X\n", i, daemon_services[i].name, daemon_services[i].id);
        if(!strcmp(service_type, daemon_services[i].name))
        {
            awsiot_ubus->service_name = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, "id", id);
                if(value && strlen(value)) 
                {
                    blobmsg_add_string(&buff, "value", value);
                }
                
                if(subdevID && strlen(subdevID)) 
                {
                    if(!strcmp(service_type, "upnp"))
                    {
                        blobmsg_add_string(&buff, "subdevID", subdevID);
                    }
                }

                awsiot_ubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                awsiot_ubus->req=req;
                ubus_invoke_async(ctx, daemon_services[i].id, "identify", buff.head, req);
                req->data_cb = VR_(receive_data_cb);
                req->complete_cb = VR_(complete_data_cb);
                req->priv = (void *) awsiot_ubus;
                ubus_complete_request_async(ctx, req);
                break;
            }
        }
    }
}

struct FtpFile {
    const char *filename;
    FILE *stream;
};

struct myprogress {
    const char *topic;
    const char *action;
};

static size_t my_fwrite(void *buffer, size_t size, size_t nmemb, void *stream)
{
    struct FtpFile *out=(struct FtpFile *)stream;
    if(out && !out->stream) 
    {
        /* open file for writing */
        out->stream=fopen(out->filename, "wb");
        if(!out->stream)
        {
            return -1; /* failure, can't open file to write */
        }
        
    }
    return fwrite(buffer, size, nmemb, out->stream);
}

static int download_value = 100;

static int older_progress(void *p,
                          double dltotal, double dlnow,
                          double ultotal, double ulnow)
{
    int current_value = 0;
    struct myprogress *myp = (struct myprogress *)p;

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, "method", json_object_new_string("firmware_actionsR"));
    json_object_object_add(jobj, "action", json_object_new_string(myp->action));
    json_object_object_add(jobj, "state", json_object_new_string("downloading"));

    if(dltotal)
    {
        current_value = (int)(100*dlnow/dltotal);

        char value[8];
        sprintf(value, "%d", current_value);
        json_object_object_add(jobj, "value", json_object_new_string(value));
    }
    else
    {
        json_object_object_add(jobj, "value", json_object_new_string("0"));
        current_value = 0;
    }
    if(download_value != current_value)
    {
        download_value = current_value;
        VR_(Send_message)(NULL, QOS0, (char *)json_object_to_json_string(jobj));
    }

    return 0;
}

int VR_(download_file)(char *url, const char *user_password, const char *topic, const char* action, const char *filename)
{
    CURL *curl;
    CURLcode res = CURLE_BAD_DOWNLOAD_RESUME;
    struct FtpFile ftpfile={
        filename, /* name to store the file as if successful */
        NULL
    };

    struct myprogress prog;

    curl_global_init(CURL_GLOBAL_DEFAULT);

    curl = curl_easy_init();
    if(curl) 
    {
        prog.topic = topic;
        prog.action = action;
        curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, older_progress);
        curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, &prog);

// #if LIBCURL_VERSION_NUM >= 0x072000
//         curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, xferinfo);
//         curl_easy_setopt(curl, CURLOPT_XFERINFODATA, &prog);
// #endif
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
        curl_easy_setopt(curl, CURLOPT_USERPWD, user_password);
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, my_fwrite);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ftpfile);

        res = curl_easy_perform(curl);

        curl_easy_cleanup(curl);
    }

    if(ftpfile.stream)
    {
        fclose(ftpfile.stream); /* close the local file */
    }
    
    curl_global_cleanup();
    return res;
}

void updatefirmware(void *data)
{
    update_firmware_d *upfirm = (update_firmware_d *)data;
    SLOGI("upfirm->URL = %s\n", (char *)upfirm->URL);
    SLOGI("upfirm->topic = %s\n", (char *)upfirm->topic);

    const char *filename = "openwrt-omap-ext4.img.tar.gz";
    int res = VR_(download_file)((char *)upfirm->URL, "venus_firmware:v3nu5_f!rmw@r3",
                            (char *)upfirm->topic, "update", filename);

    if(CURLE_OK != res)
    {
        system("/etc/led_control.sh STA_connected > /dev/console");
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, "method", json_object_new_string("firmware_actionsR"));
        json_object_object_add(jobj, "action", json_object_new_string("update"));
        json_object_object_add(jobj, "status", json_object_new_string("failed"));
        VR_(Send_message)(upfirm->topic, QOS0, (char *)json_object_to_json_string(jobj));
        free(upfirm);
        system("reboot");
    }
    else
    {
        char command[256];
        memset(command, 0x00, sizeof(command));
        sprintf(command, "%s%s", "firmware_update.sh ", filename);
        SLOGI("command = %s\n", command);
        FILE *fp;
        fp = popen(command,"r");
        char buff[255];
        while(fgets(buff, 255, fp)!=NULL)
        {
            printf("buff = %s\n", buff);
        }
        pclose(fp);

        if(!strncasecmp(buff,"UPDATE FIRMWARE SUCCESS", strlen("UPDATE FIRMWARE SUCCESS")))
        {
            system("/etc/led_control.sh STA_connected > /dev/console");
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, "method", json_object_new_string("firmware_actionsR"));
            json_object_object_add(jobj, "action", json_object_new_string("update"));
            json_object_object_add(jobj, "status", json_object_new_string("successful"));
            json_object_object_add(jobj, "state", json_object_new_string("update done, restarting system"));

            char *latest_version = VR_(read_option)("firmware.@latest-version[0].value");
            if(latest_version && strlen(latest_version))
            {
                json_object_object_add(jobj, "version", json_object_new_string(latest_version));
                char cmd[255];
                sprintf(cmd, "uci set firmware.@current-version[0].value='%s'", latest_version);
                system(cmd);
                system("uci commit firmware");
                free(latest_version);
            }

            VR_(Send_message)(upfirm->topic, QOS0, (char *)json_object_to_json_string(jobj));
            free(upfirm);
            system("reboot");
        }
        else if(!strncasecmp(buff,"WRONG URL", strlen("WRONG URL")))
        {
            system("/etc/led_control.sh STA_connected > /dev/console");
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, "method", json_object_new_string("firmware_actionsR"));
            json_object_object_add(jobj, "action", json_object_new_string("update"));
            json_object_object_add(jobj, "status", json_object_new_string("failed"));
            VR_(Send_message)(upfirm->topic, QOS0, (char *)json_object_to_json_string(jobj));
            free(upfirm);
            system("reboot");
        }
        else if(!strncasecmp(buff,"UNTAR FAILED", strlen("UNTAR FAILED")))
        {
            system("/etc/led_control.sh STA_connected > /dev/console");
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, "method", json_object_new_string("firmware_actionsR"));
            json_object_object_add(jobj, "action", json_object_new_string("update"));
            json_object_object_add(jobj, "status", json_object_new_string("failed"));
            VR_(Send_message)(upfirm->topic, QOS0, (char *)json_object_to_json_string(jobj));
            free(upfirm);
            system("reboot");
        }
    }   
}

void VR_(firmware_actions)(const char *service_type, const char *msg) //no need to use service_type
{
	SLOGI("The command %s is called \n ", __func__);
    const char* action = VR_(get_string_data)(msg, "action");
    const char* version = VR_(get_string_data)(msg, "version");
    const char* topic = VR_(get_string_data)(msg, "topic");

    SLOGI("action = %s", action);
    SLOGI("version = %s", version);
    SLOGI("topic = %s", topic);

    if(action && strlen(action))
    {
        if(!strcasecmp(action, "update"))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, "method", json_object_new_string("firmware_actionsR"));
            json_object_object_add(jobj, "action", json_object_new_string(action));
            json_object_object_add(jobj, "status", json_object_new_string("successful"));
            json_object_object_add(jobj, "state", json_object_new_string("updating"));

            VR_(Send_message)(topic, QOS0, (char *)json_object_to_json_string(jobj));

            update_firmware_d *data = (update_firmware_d *)malloc(sizeof(update_firmware_d));
            memset(data, 0x00, sizeof(update_firmware_d));
            strncpy(data->URL, "ftp://ftp.veriksystems.com/openwrt-omap-ext4.img.tar.gz", 255);
            strncpy(data->topic, topic, 255);
            system("/etc/led_control.sh Firmware_updating > /dev/console");

            pthread_t upfirmware_thread;
            pthread_create(&upfirmware_thread, NULL, (void *)&updatefirmware, data);
        }
        else if(!strcasecmp(action, "get_latest"))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, "method", json_object_new_string("firmware_actionsR"));
            json_object_object_add(jobj, "action", json_object_new_string(action));

            FILE *fp;
            fp = popen("curl -u venus_firmware:v3nu5_f!rmw@r3 ftp://ftp.veriksystems.com/latest_version", "r");
            char buff[32], *pos;
            memset(buff, 0x00, sizeof(buff));
            fgets(buff, 31, fp);
            if ((pos=strchr(buff, '\n')) != NULL)
                *pos = '\0';
            pclose(fp);

            if(strlen(buff))
            {
                json_object_object_add(jobj, "status", json_object_new_string("successful"));
                json_object_object_add(jobj, "latest_version", json_object_new_string(buff));
                char cmd[255];
                sprintf(cmd, "uci set firmware.@latest-version[0].value='%s'", buff);
                system(cmd);
                system("uci commit firmware");
            }
            else
            {
                char *latest_version = VR_(read_option)("firmware.@latest-version[0].value");
                if(latest_version && strlen(latest_version))
                {
                    json_object_object_add(jobj, "status", json_object_new_string("successful"));
                    json_object_object_add(jobj, "latest_version", json_object_new_string(latest_version));
                    free(latest_version);
                }
                else
                {
                    json_object_object_add(jobj, "status", json_object_new_string("failed"));
                }
            }
            
            VR_(Send_message)(topic, QOS0, (char *)json_object_to_json_string(jobj));
        }
        else if(!strcasecmp(action, "get_current"))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, "method", json_object_new_string("firmware_actionsR"));
            json_object_object_add(jobj, "action", json_object_new_string(action));
            
            char *current_version = VR_(read_option)("firmware.@current-version[0].value");
            if(current_version && strlen(current_version))
            {
                json_object_object_add(jobj, "status", json_object_new_string("successful"));
                json_object_object_add(jobj, "current_version", json_object_new_string(current_version));
                free(current_version);
            }
            else
            {
                json_object_object_add(jobj, "status", json_object_new_string("failed"));
                json_object_object_add(jobj, "reason", json_object_new_string("couldn't get current_version"));
            }
            VR_(Send_message)(topic, QOS0, (char *)json_object_to_json_string(jobj));
        }
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, "method", json_object_new_string("firmware_actionsR"));
        json_object_object_add(jobj, "status", json_object_new_string("failed"));
        json_object_object_add(jobj, "reason", json_object_new_string("missing argument"));
        VR_(Send_message)(topic, QOS0, (char *)json_object_to_json_string(jobj));
    }
}

command_handler_t command_handler[] = {
    { .name = "change_name",        .handler = VR_(change_name)},
    { .name = "list_devices",       .handler = VR_(list_device)},
    // { .name = "open_network",       .handler = VR_(open_network)},
    // { .name = "close_network",      .handler = VR_(close_network)},
    { .name = "set_binary",         .handler = VR_(set_binary)},
    { .name = "get_binary",         .handler = VR_(get_binary)},
    // { .name = "set_dimmer",         .handler = VR_(set_dimmer)},
    { .name = "reset",              .handler = VR_(reset)},
    { .name = "get_specification",  .handler = VR_(get_specification)},
    { .name = "set_specification",  .handler = VR_(set_specification)},
    { .name = "read_s_spec",    	.handler = VR_(read_s_spec)},
    { .name = "write_s_spec",    	.handler = VR_(write_s_spec)},
    // { .name = "add_devices",        .handler = VR_(add_devices)},
    // { .name = "remove_device",      .handler = VR_(remove_device)},
    { .name = "firmware_actions",   .handler = VR_(firmware_actions)},
    { .name = "identify",           .handler = VR_(identify)},
};

static bool VR_(is_notify_command)(const char *msg)
{
    //parsing message
    const char *commandType = VR_(get_string_data)(msg, "type");
    if (commandType == NULL)
    {
    	return false;
    }
        
    if (!strcmp(commandType, "notify"))
    {
        return true;
    }

    return false;
}

static command_handler_f VR_(command_handler_search_by_name)(const char *name)
{
    int i = 0;
    for (; i < sizeof(command_handler)/sizeof (command_handler_t); i++)
    {
        if (!strcmp(name, command_handler[i].name))
        {
            return command_handler[i].handler;
        }
    }

    return NULL;
}

void VR_(handle_MQTT_callback)(AWS_IoT_Client *pClient, char *topicName, uint16_t topicNameLen,
						 IoT_Publish_Message_Params *params, void *pData) 
{
	INFO("Subscribe callback");
	INFO("%.*s\t%.*s", topicNameLen, topicName, (int)params->payloadLen, params->payload);

	if(VR_(is_notify_command)(params->payload))
    {
        SLOGI("The notify command\n");
        return; 
    }

    const char *type = VR_(get_string_data)(params->payload, "type");
    const char *method = VR_(get_string_data)(params->payload, "method");
    SLOGI("method = %s\n", method);
    if(method)
    {
    	command_handler_f func_handler = VR_(command_handler_search_by_name)(method);

        if (func_handler != NULL)
        {
        	func_handler(type, params->payload);
        }
        else
        {
            SLOGI("Currently not support (type: method) :%s %s\n", type, method);
        }  
    }
    INFO("end Subscribe callback");
}

void VR_(handle_disconnect_callback)(AWS_IoT_Client *pClient, void *data) 
{
	WARN("MQTT Disconnect");
	IoT_Error_t rc = FAILURE;
	if(NULL == data) {
		return;
	}

	AWS_IoT_Client *client = (AWS_IoT_Client *)data;
	if(aws_iot_is_autoreconnect_enabled(client)){
		INFO("Auto Reconnect is enabled, Reconnecting attempt will start now");
	}else{
		WARN("Auto Reconnect not enabled. Starting manual reconnect...");
		rc = aws_iot_mqtt_attempt_reconnect(client);
		if(NETWORK_RECONNECTED == rc){
			WARN("Manual Reconnect Successful");
		}else{
			WARN("Manual Reconnect Failed - %d", rc);
		}
	}
}

/**
 * @brief Default cert location
 */
char certDirectory[PATH_MAX + 1] = "/etc/certs";

/**
 * @brief Default MQTT HOST URL is pulled from the aws_iot_config.h
 */
char HostAddress[255] = AWS_IOT_MQTT_HOST;

/**
 * @brief Default MQTT port is pulled from the aws_iot_config.h
 */
uint32_t port = AWS_IOT_MQTT_PORT;

/**
 * @brief This parameter will avoid infinite loop of publish and exit the program after certain number of publishes
 */
uint32_t publishCount = 0;

void parseInputArgsForConnectParams(int argc, char** argv) {
	int opt;

	while (-1 != (opt = getopt(argc, argv, "h:p:c:x:"))) {
		switch (opt) {
		case 'h':
			strcpy(HostAddress, optarg);
			DEBUG("Host %s", optarg);
			break;
		case 'p':
			port = atoi(optarg);
			DEBUG("arg %s", optarg);
			break;
		case 'c':
			strcpy(certDirectory, optarg);
			DEBUG("cert root directory %s", optarg);
			break;
		case 'x':
			publishCount = atoi(optarg);
			DEBUG("publish %s times\n", optarg);
			break;
		case '?':
			if (optopt == 'c') {
				ERROR("Option -%c requires an argument.", optopt);
			} else if (isprint(optopt)) {
				WARN("Unknown option `-%c'.", optopt);
			} else {
				WARN("Unknown option character `\\x%x'.", optopt);
			}
			break;
		default:
			ERROR("Error in command line argument parsing");
			break;
		}
	}
}

static int VR_(handle_ubus_notify)(struct ubus_context *ctx, struct ubus_object *obj,
    						struct ubus_request_data *req, const char* method,
    						struct blob_attr *msg)
{
    // struct blob_attr *tb[__NOTIFY_MAX];
    // SLOGI("Recieved notify message.\n");

    // blobmsg_parse(ubus_method_notify_policy, __NOTIFY_MAX, tb, blob_data(msg), blob_len(msg));
    // if (!tb[SERVICE_NAME] || !tb[NOTIFY_MSG])
    // {
    //     SLOGI("Notify message lack of information.\n");
    //     return UBUS_STATUS_INVALID_ARGUMENT;
    // }

    // char* message = (char *)blobmsg_data(tb[NOTIFY_MSG]);

    // if(mosq && message != NULL)
    // {
    //     MosquittoPublishMsg(message);
    // }


    // blob_buf_init(&buff, 0);
    // blobmsg_add_string(&buff, "msg", "SUCCESS");
    // ubus_send_reply(ctx, req, buff.head);

    //FIXME: check if 0 or -1 
    return -1;
}



static const struct ubus_method awsiot_methods[] = 
{
    UBUS_METHOD("notify", VR_(handle_ubus_notify), handle_ubus_notify_policy),
};

static struct ubus_object_type awsiot_object_type =
UBUS_OBJECT_TYPE("awsiot", awsiot_methods);

static struct ubus_object awsiot_object = {
    .name = "awsiot",
    .type = &awsiot_object_type,
    .methods = awsiot_methods,
    .n_methods = ARRAY_SIZE(awsiot_methods),
};

static void VR_(handle_ubus_subscriber_remove)(struct ubus_context *ctx, struct ubus_subscriber *s, uint32_t id)
{
    SLOGI("Object %08x went away\n", id);
    int i;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++) 
    {
        if (daemon_services[i].id == id)
        {
            daemon_services[i].id = 0;
            break;
        }
    }
}

static int VR_(handle_ubus_notification)(struct ubus_context *ctx, struct ubus_object *obj,
								    struct ubus_request_data *req, const char *method,
								    struct blob_attr *msg)
{
	SLOGI("recive_notify_cb\n");
    const char *msgstr = "(unknown)";
    struct blob_attr *tb[1];
    blobmsg_parse(msg_policy, ARRAY_SIZE(msg_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
    {
        msgstr = blobmsg_data(tb[0]);
        SLOGI("msgstr = %s\n", msgstr);
        //Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, "notify", (char *)msgstr, 1);
    }   
	return 0;
}

static const struct blobmsg_policy ubus_event_policy[2] = {
    [0] = { .name = "id", .type = BLOBMSG_TYPE_INT32 },
    [1] = { .name = "path", .type = BLOBMSG_TYPE_STRING },
};

static void VR_(handle_ubus_event)(struct ubus_context *ctx, struct ubus_event_handler *ev,
              const char *type, struct blob_attr *msg)
{
    if(!strcmp(type, "ubus.object.add"))
    {
        struct blob_attr *tb[2];
        blobmsg_parse(ubus_event_policy, ARRAY_SIZE(ubus_event_policy), 
        				tb, blob_data(msg), blob_len(msg));
        uint32_t id;
        int i, ret;
        const char *path = "unknown";

        if(tb[0] && tb[1])
        {
            id = blobmsg_get_u32(tb[0]);
            path = blobmsg_data(tb[1]);

            for(i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
            {
                if(!strcmp(daemon_services[i].name, path))
                {
                    if(daemon_services[i].id != 0)
                    {
                        ret = ubus_unsubscribe(ctx, &ubus_subscribe_handler, daemon_services[i].id);
                        SLOGW("UNsubscribe object %s with id %08X: %s\n", 
                        	daemon_services[i].name, daemon_services[i].id, ubus_strerror(ret));
                    }
                    
                    daemon_services[i].id = id;
                    ubus_subscribe_handler.remove_cb = VR_(handle_ubus_subscriber_remove);
    				ubus_subscribe_handler.cb = VR_(handle_ubus_notification);
                    ret = ubus_subscribe(ctx, &ubus_subscribe_handler, daemon_services[i].id);
                    if (ret)
                    {
                        SLOGW("Could not subscribe %s with id %08X. Reason:  %s\n", 
                        	daemon_services[i].name, daemon_services[i].id, ubus_strerror(ret));
                    }
                    else
                    {
                        SLOGI("[INFO] Has successfully subscribed ubus object (name: id) = (%s, %08X) \n",
                         daemon_services[i].name, daemon_services[i].id);
                    }
                }    
            }
        }
    }
}

static void remove_action_timeout_cb(struct uloop_timeout *timeout)
{
    //printf("remove_action_timeout_cb\n");
    new_awsiot_ubus *tmp = g_awsiot_ubus;
    if(tmp)
    {
        unsigned int time_now = (unsigned)time(NULL);
        //printf("tmp->action_time = %d\n", tmp->action_time);
        if((time_now - tmp->action_time) > 40)
        {
            //printf("ACTION %s TIME %d WAS TIMEOUT\n", tmp->name, tmp->action_time);
            if(tmp->name && strlen(tmp->name))
            {
                int i;
                for (i = 0; i < sizeof(cmd_support)/sizeof(char*); i++)
                {
                    //printf("cmd_support[i] = %s\n", cmd_support[i]);
                    if(!strcmp(tmp->name, cmd_support[i]) && tmp->service_name)
                    {
                        char method[256];
                        sprintf(method, "%sR", tmp->name);
                        json_object *jobj = json_object_new_object();
                        json_object_object_add(jobj, "type", json_object_new_string((char*)tmp->service_name));
                        json_object_object_add(jobj, "method", json_object_new_string(method));
                        json_object_object_add(jobj, "status", json_object_new_string("failed"));
                        json_object_object_add(jobj, "reason", json_object_new_string("Action timeout"));
                        VR_(Send_message)(tmp->topic, QOS0, (char *)json_object_to_json_string(jobj));

                        int j;
                        for (j = 0; j< sizeof(daemon_services)/sizeof(daemon_service); j++)
                        {
                            if(!strcmp((char*)tmp->service_name, daemon_services[j].name))
                            {
                                daemon_services[j].timeout++;
                                if(daemon_services[j].timeout>2)
                                {
                                    printf("##### ACTION timeout %d, restart service %s #########\n", 
                                             daemon_services[j].timeout, daemon_services[j].name);
                                    daemon_services[j].timeout = 0;
                                    char cmd[256];
                                    sprintf(cmd, "/etc/start_services.sh start_service %s >> /system.log", daemon_services[j].service_name);
                                    system((const char*)cmd);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            // if(tmp->req)
            // {
            //     printf("start ubus abort request\n");
            //     //ubus_abort_request(ctx, tmp->req);
            //     printf("free request\n");
            //     free(tmp->req);
            //     printf("done request\n");
            // }
            
            printf("free tmp\n");
            VR_(remove_list)(tmp);
            //free(tmp);
            printf("done free tmp\n");
        }
    }
    else
    {
        //printf("list null\n");
    }
    uloop_timeout_set(timeout, 1000);
}

static struct uloop_timeout remove_action_timeout = {
    .cb = remove_action_timeout_cb,
};

static void pull_request_cb(struct uloop_timeout *timeout)
{
	//Max time the yield function will wait for read messages
	aws_iot_mqtt_yield(&client, 100);
	// if(NETWORK_ATTEMPTING_RECONNECT == rc)
	// {
	// 	INFO("-->sleep");
	// 	sleep(1);
	// 	// If the client is attempting to reconnect we will skip the rest of the loop.
	// 	continue;
	// }
    uloop_timeout_set(timeout, 50);
}

static struct uloop_timeout pull_request = {
    .cb = pull_request_cb,
};

static int VR_(ubus_service_init)()
{
    int ret, i;
    const char *ubus_socket = NULL;

    uloop_init();
    ctx = ubus_connect(ubus_socket);
    if (!ctx) {
        SLOGE("Failed to connect to ubus\n");
        return -1;
    }

    ubus_add_uloop(ctx);

    // ubus initialization 
    ret = ubus_add_object(ctx, &awsiot_object);
    if (ret) 
    {
        SLOGE("Failed to add_object object: %s\n", ubus_strerror(ret));
        return -1;
    }

    // ubus subscriber's initialization 
    ubus_subscribe_handler.remove_cb = VR_(handle_ubus_subscriber_remove);
    ubus_subscribe_handler.cb = VR_(handle_ubus_notification);            
    ret = ubus_register_subscriber(ctx, &ubus_subscribe_handler);

    for(i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(ubus_lookup_id(ctx, daemon_services[i].name, &daemon_services[i].id))
        {
            SLOGI("The ubus service %s is invisible \n", daemon_services[i].name);
        }
        else
        {
            ret = ubus_subscribe(ctx, &ubus_subscribe_handler, daemon_services[i].id);
            SLOGW("Watching object %s with id %08X: %s\n", daemon_services[i].name, 
            	daemon_services[i].id, ubus_strerror(ret));
        }
    }

    // ubus event's initialization 
    memset(&ubus_event_listener, 0, sizeof(ubus_event_listener));
    ubus_event_listener.cb = VR_(handle_ubus_event);
    ret = ubus_register_event_handler(ctx, &ubus_event_listener, "ubus.object.add");
    if (ret)
    {
        SLOGE("Failed to register event handler: %s\n", ubus_strerror(ret));
        return -1;
    }

    uloop_timeout_set(&pull_request, 50); //

    uloop_timeout_set(&remove_action_timeout, 1000);

    uloop_run();
    return 0;
}

static void VR_(free_ubus_service)(void)
{
    ubus_free(ctx);
    uloop_done();

    if(buff.buf)
    {
        free(buff.buf);
    }

    // if(rule_db)
    //     sqlite3_close(rule_db);

    if(awsiot_db)
    {
    	sqlite3_close(awsiot_db);
    }

    // if(support_devs_db)
    //     sqlite3_close(support_devs_db);
}

int main(int argc, char** argv) 
{
	SLOG_Init(SLOG_STDOUT, NULL);
    set_thread_name( "awsiot_service" );

    pthread_mutex_init(&awsiot_ubus_listMutex, 0);

    open_database(DEVICES_DATABASE, &awsiot_db);

	IoT_Error_t rc = FAILURE;
	//int32_t i = 0;

	char rootCA[PATH_MAX + 1];
	char clientCRT[PATH_MAX + 1];
	char clientKey[PATH_MAX + 1];
	char CurrentWD[PATH_MAX + 1];

	parseInputArgsForConnectParams(argc, argv);

	INFO("\nAWS IoT SDK Version %d.%d.%d-%s\n", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH, VERSION_TAG);
	INFO("HostAddress %s\n AWS_IOT_MQTT_CLIENT_ID %s\n", HostAddress, AWS_IOT_MQTT_CLIENT_ID);

	getcwd(CurrentWD, sizeof(CurrentWD));
	snprintf(rootCA, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_ROOT_CA_FILENAME);
	snprintf(clientCRT, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_CERTIFICATE_FILENAME);
	snprintf(clientKey, PATH_MAX + 1, "%s/%s/%s", CurrentWD, certDirectory, AWS_IOT_PRIVATE_KEY_FILENAME);

	DEBUG("rootCA %s", rootCA);
	DEBUG("clientCRT %s", clientCRT);
	DEBUG("clientKey %s", clientKey);

	IoT_Client_Init_Params mqttInitParams;
	mqttInitParams.enableAutoReconnect = false; // We enable this later below
	mqttInitParams.pHostURL =  HostAddress;
	mqttInitParams.port = port;
	mqttInitParams.pRootCALocation = rootCA;
	mqttInitParams.pDeviceCertLocation = clientCRT;
	mqttInitParams.pDevicePrivateKeyLocation = clientKey;
	mqttInitParams.mqttCommandTimeout_ms = 7000;
	mqttInitParams.tlsHandshakeTimeout_ms = 5000;
	mqttInitParams.isSSLHostnameVerify = true;
	mqttInitParams.disconnectHandler = VR_(handle_disconnect_callback);
	mqttInitParams.disconnectHandlerData = (void *)&client;

	rc = aws_iot_mqtt_init(&client, &mqttInitParams);
	if(SUCCESS != rc) 
	{
		ERROR("aws_iot_mqtt_init returned error : %d ", rc);
		return rc;
	}

	IoT_Client_Connect_Params connectParams = iotClientConnectParamsDefault;

	connectParams.keepAliveIntervalInSec = 10;
	connectParams.isCleanSession = true;
	connectParams.MQTTVersion = MQTT_3_1_1;
	connectParams.pClientID = AWS_IOT_MQTT_CLIENT_ID;
	connectParams.clientIDLen = strlen(AWS_IOT_MQTT_CLIENT_ID);
	connectParams.isWillMsgPresent = false;

	INFO("Connecting...");
	rc = aws_iot_mqtt_connect(&client, &connectParams);
	if(SUCCESS != rc) 
	{
		ERROR("Error(%d) connecting to %s:%d", rc, mqttInitParams.pHostURL, mqttInitParams.port);
	}
	/*
	 * Enable Auto Reconnect functionality. Minimum and Maximum time of Exponential backoff are set in aws_iot_config.h
	 *  #AWS_IOT_MQTT_MIN_RECONNECT_WAIT_INTERVAL
	 *  #AWS_IOT_MQTT_MAX_RECONNECT_WAIT_INTERVAL
	 */
	rc = aws_iot_mqtt_autoreconnect_set_status(&client, true);
	if(SUCCESS != rc)
	{
		ERROR("Unable to set Auto Reconnect to true - %d", rc);
		return rc;
	}

	INFO("Subscribing...");
	rc = aws_iot_mqtt_subscribe(&client, SUBSCRIBE_REQ_TOPIC, strlen(SUBSCRIBE_REQ_TOPIC), QOS0, 
								VR_(handle_MQTT_callback), NULL);
	if(SUCCESS != rc) 
	{
		ERROR("Error subscribing : %d ", rc);
		return rc;
	}

	VR_(ubus_service_init)();
	VR_(free_ubus_service)();

	return rc;
}

