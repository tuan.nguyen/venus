#ifdef __cplusplus
extern "C"{
#endif 

#ifndef _AWSIOT_HANDLER_H_
#define _AWSIOT_HANDLER_H_


#define VR_(_str) VR_##_str

typedef struct _daemon_service 
{
    const char *service_name;
    const char *name;
    uint32_t id;
    int timeout;
} daemon_service;

typedef struct _new_awsiot_ubus 
{
    char name[256];
    int type; // 0 :send all, 1: send with seassion id, 2: dont send.
    char topic[256];;
    void *service_name;

    unsigned int action_time;
    struct ubus_request *req;
    struct _new_awsiot_ubus *next;
} new_awsiot_ubus;

typedef struct _update_firmware 
{
    char URL[256];
    char topic[256];
} update_firmware_d;

typedef struct _group_action
{
    char *command;
    char *value;
} group_action_data;

enum {
    NOTIFY_ID,
    SERVICE_NAME,
    __NOTIFY_MAX
};

enum {
	NOTIFY_SERVICE,
	NOTIFY_MSG,
	__NOTIFY_SIZE
};

typedef void (* command_handler_f)(const char *service_type, const char *msg);

typedef struct command_handler_s
{
	const char *name;
	command_handler_f handler; 
} command_handler_t;


#define SUBSCRIBE_REQ_TOPIC              	"10000000000010/req"
#define PUBLISH_RES_TOPIC              		"10000000000010/res"

#ifdef AWS_IOT_MQTT_CLIENT_ID
#undef AWS_IOT_MQTT_CLIENT_ID
#endif

#ifdef AWS_IOT_ROOT_CA_FILENAME
#undef AWS_IOT_ROOT_CA_FILENAME
#endif

#ifdef AWS_IOT_CERTIFICATE_FILENAME
#undef AWS_IOT_CERTIFICATE_FILENAME
#endif

#ifdef AWS_IOT_PRIVATE_KEY_FILENAME
#undef AWS_IOT_PRIVATE_KEY_FILENAME
#endif

#define AWS_IOT_MQTT_CLIENT_ID			"10000000000010"
#define AWS_IOT_ROOT_CA_FILENAME       "root-CA.crt" ///< Root CA file name
#define AWS_IOT_CERTIFICATE_FILENAME   "0dbec418-certificate.pem.crt" ///< device signed certificate file name
#define AWS_IOT_PRIVATE_KEY_FILENAME   "0dbec418-private.pem.key" ///< Device private key filename

#endif
#ifdef __cplusplus
} /* extern "C" */
#endif