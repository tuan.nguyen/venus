/******************************************************************************
 * Copyright (c) 2014, VEriK Systems. All rights reserved.
 *
 * Module: Service Provider
 *
 ******************************************************************************/

#include <sqlite3.h> 
#include "config.h"
#include "alljoyn_service.h"

#define CONFIG_NAME "verik"

int check_network_configuration()
{
	//char* network_mode = read_option(CONFIG_NAME,"network_mode");
	return 0;
}

int set_network_configuration()
{
	return 0;
}

int main()
{
    SLOG_Init(SLOG_STDOUT, NULL);
    set_thread_name("alljoyn-service");

	//load and check network configuration
	check_network_configuration();

	//set the network mode follows the configuration
	set_network_configuration();

    char command[256];
    memset(command, 0x00, sizeof(command));
    sprintf(command, "%s", "pgrep alljoyn_handler | head -n -1 | cut -c-5 | xargs kill -9 &>/dev/null");
    system(command);

	//start alljoyn service
	start_service("Venus");
	
	return 0;
}

