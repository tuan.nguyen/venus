#ifndef ALLJOYN_CONTROLLER_H
#define ALLJOYN_CONTROLLER_H

#include <alljoyn_c/AboutData.h>
#include <alljoyn_c/AboutListener.h>
#include <alljoyn_c/AboutObj.h>
#include <alljoyn_c/AboutObjectDescription.h>
#include <alljoyn_c/AboutProxy.h>
#include <alljoyn_c/BusAttachment.h>
#include <alljoyn_c/Init.h>

#define ONBOARDING_INTERFACE 	"org.alljoyn.Onboarding"
#define ST_CLAIM_TOKEN    		"claimToken"
#define ST_NEW_HUB	    		"newHub"
#define ST_STYX_PATTERN         "13"

#include "VR_list.h"

typedef void (* new_connection_cb)(void *data);

typedef struct connection_list_t 
{
	char *deviceId;
	char *bus;
	char *path;
	alljoyn_sessionid sessionId;
    alljoyn_sessionlistener sessionlistener;

	struct VR_list_head list;
}connection_list;

typedef struct connection_data_t 
{
	new_connection_cb connection_cb; 
    alljoyn_busattachment busAtt;
}connection_data;

QStatus start_alljoyn(void);
void free_alljoyn_data();
void Send_ubus_notify(char *data);

void init_listen_connection(connection_data *connect_data, alljoyn_aboutlistener* aboutlistener);
void scan_interface(alljoyn_busattachment busAtt, char *interface);
void get_detected_hubs(void *devList);
void handle_setup_hub(alljoyn_busattachment busAtt, void *data);
int is_onboarding_connect(const char *busName);

void stop_scan_interface(alljoyn_busattachment busAtt, char *interface);
void free_listen_connection(alljoyn_busattachment busAtt, alljoyn_aboutlistener* aboutlistener);
#endif