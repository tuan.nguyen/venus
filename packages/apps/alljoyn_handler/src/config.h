/******************************************************************************
 * Copyright (c) 2014, VEriK Systems. All rights reserved.
 *
 * Module: Service Provider
 *
 ******************************************************************************/

#include <stdio.h>


#define EVENT_TEMPLATE "#!/bin/sh /etc/rc.common\n\nSTART=35\nUSE_PROCD=1\n\nservice_triggers()\n{\n\tprocd_add_reload_trigger %s\n}\n\nreload_service()\n{\n\tprocd_open_instance\n\tprocd_set_param command %s\n\tprocd_set_param respawn\n\tprocd_close_instance\n}"

int create_config(char* config, char* option[], char* data[], int size);

int create_event(char* event_name, char* command);

char* read_option(char* config, char* option);

int update_config(char* config, char* option, char* data);

int execute_event(char* event_name);