/******************************************************************************
 * Copyright (c) 2014, VEriK Systems. All rights reserved.
 *
 * Module: Service Provider
 * https://wiki.openssl.org/index.php/EVP_Symmetric_Encryption_and_Decryption
 ******************************************************************************/
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <curl/curl.h>

#include "alljoyn_controller.h"
#include "alljoyn_service.h"
#include "announce_service.h"
#include "database.h"
#include "group_apis.h"
#include "VR_define.h"
#include "verik_utils.h"
#include "verik_sec.h"
#include "vr_sound.h"
#include "vr_rest.h"
#include "timer.h"
#include "rule.h"
#include "led.h"
#include "ping.h"

/* Static top level message bus object */
static alljoyn_busattachment g_msgBus = NULL;
static alljoyn_busobject CommonObj = NULL;
static alljoyn_authlistener g_authListener;

static alljoyn_sessionopts opts = NULL;
static alljoyn_sessionportlistener listener = NULL;
static alljoyn_aboutdata aboutData = NULL;
static alljoyn_abouticon aboutIcon = NULL;
static alljoyn_aboutlistener aboutListener = NULL;

static char g_psk[SIZE_256B];
/* Constants */
//static const char* CONNECT_ARGS = "unix:abstract=alljoyn";
static const char* OBJECT_PATH = "/VEriK/Service";
static const alljoyn_sessionport SERVICE_PORT = 25;

static volatile sig_atomic_t g_interrupt = QCC_FALSE;

static struct ubus_context *ctx;
static struct ubus_subscriber alljoyn_notify;
static struct blob_buf buff;
static struct ubus_event_handler ubus_listener;
//static struct ubus_request g_req;

new_allubus *g_allubus = NULL;
static int list_count = 0;
static timer_t g_timer_scanwifi = 0;
static timer_t g_timer_inform_hub = 0;

pthread_mutex_t alljoyn_ubus_listMutex;
pthread_t thread_remove_actions_timeout;
static int remove_actions_timeout_enable = 1;
static int g_inform_power_cycle = 0;

static sqlite3 *rule_db;
static sqlite3 *dev_db;
static sqlite3 *support_devs_db;
static sqlite3 *user_db;

int g_shmid = 0;
char *g_shm = NULL;

int g_led_shmid = 0;
char *g_led_shm = NULL;

static int group_action_cb(void *data, char *devicetype, void *devicelist, int length);
// void Send_ubus_notify(char *data);

const char *cmd_support[] = {
    ST_NOTIFY,
    ST_SET_TIME,
    ST_GET_TIME,
    ST_AUTO_CONF,
    ST_ADD_DEVICES,
    ST_LIST_DEVICES,
    ST_REMOVE_DEVICE,
    ST_RESET,
    ST_OPEN_CLOSE_NETWORK,
    ST_GET_SUB_DEVS,
    ST_GET_BINARY,
    ST_SET_BINARY,
    ST_CHANGE_NAME,
    ST_ALEXA,
    ST_IDENTIFY,
    ST_FIRMWARE_ACTIONS,
    ST_SET_RULE,
    ST_GET_RULE,
    ST_RULE_ACTIONS,
    ST_READ_SPEC,
    ST_WRITE_SPEC,
    ST_READ_S_SPEC,
    ST_WRITE_S_SPEC,
    ST_REDISCOVER,
    ST_CREATE_GROUP,
    ST_GROUP_ACTIONS,
    ST_GROUP_CONTROL,
    ST_NETWORK,
    ST_READ_SPEC_CRC,
    ST_WRITE_SPEC_CRC,
    ST_REQUEST_ACCESS,
    ST_DENY_ACCESS,
    ST_LIST_USERS,
    ST_SHARE_ACCESS,
    ST_OB_REQUEST_ACCESS,
    ST_PUBLISH,
    ST_GET_NAME,
    ST_RULE_MANUAL,
    ST_LIST_GROUPS,
    ST_VENUS_ALARM,
    ST_SCENE_ACTIVATION,
    ST_SCENE_ACTIONS,
};

daemon_service daemon_services[] = {
    {"zigbee_handler",      "zigbee",            0,        0, 0},
    {"zwave_handler",       "zwave",             0,        0, 0},
    {"upnp_handler",        "upnp",              0,        0, 0},
    {"venus_handler",       "venus",             0,        0, 0},
};

static void add_last_list(new_allubus *node)
{
    pthread_mutex_lock(&alljoyn_ubus_listMutex);
    new_allubus *tmp = g_allubus;
    // while(tmp)
    // {
    //     printf("name = %s action_time = %d\n", tmp->name, tmp->action_time);
    //     tmp=tmp->next;
    // }
    // tmp = g_allubus;
    if(tmp)
    {
        while(tmp->next)
        {
            tmp=tmp->next;
        }
        tmp->next = node;
        list_count++;
    }
    else
    {
        g_allubus = node;
        list_count++;
    }
    pthread_mutex_unlock(&alljoyn_ubus_listMutex);
}

static void remove_list(new_allubus *node)
{
    pthread_mutex_lock(&alljoyn_ubus_listMutex);
    new_allubus *tmp = g_allubus;
    new_allubus *pre = g_allubus;
    // while(tmp)
    // {
    //     printf("name = %s action_time = %d\n", tmp->name, tmp->action_time);
    //     tmp=tmp->next;
    // }
    // tmp = g_allubus;
    while(tmp)
    {
        if(tmp->action_time == node->action_time && tmp->req == node->req)
        {
            if(tmp == g_allubus)
            {
                g_allubus = tmp->next;
                free(tmp);
                list_count--;
            }
            else
            {
                pre->next = tmp->next;
                free(tmp);
                list_count--;
            }
            break;
        }
        pre=tmp;
        tmp=tmp->next;
    }
    pthread_mutex_unlock(&alljoyn_ubus_listMutex);
}

static void Broadcast_message(char* name, char* msg, int _numArgs)
{
    Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, name, msg, _numArgs);
    sendMsgtoRemoteService(msg);
}

static const struct blobmsg_policy msg_policy[1] = {
    [0] = { .name = ST_UBUS_MESSAGE_RETURN_KEY, .type = BLOBMSG_TYPE_STRING },
};

// static const struct blobmsg_policy notify_policy[__NOTIFY_MAX] = {
//     [NOTIFY_ID] = { .name = "notify_id", .type = BLOBMSG_TYPE_INT32 },
//     [SERVICE_NAME] = { .name = "service_name", .type = BLOBMSG_TYPE_STRING },
// };

static const struct blobmsg_policy auto_scan_result_policy[1] = {
    [0] = { .name = ST_WEMO_SSID, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy set_binary_policy[5] = {
    [0] = { .name = ST_SERVICE, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_DEVICE_ID, .type = BLOBMSG_TYPE_STRING },
    [2] = { .name = ST_CMD, .type = BLOBMSG_TYPE_STRING },
    [3] = { .name = ST_VALUE, .type = BLOBMSG_TYPE_STRING },
    [4] = { .name = ST_SUBDEVID, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy setgroup_binary_policy[3] = {
    [0] = { .name = ST_GROUP_ID, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_CMD, .type = BLOBMSG_TYPE_STRING },
    [2] = { .name = ST_VALUE, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy network_policy[1] = {
    [0] = { .name = ST_STATE, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy write_spec_policy[]={
    [0] = { .name = ST_SERVICE, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_DEVICE_ID, .type = BLOBMSG_TYPE_STRING },
    [2] = { .name = ST_CLASS, .type = BLOBMSG_TYPE_STRING },
    [3] = { .name = ST_CMD, .type = BLOBMSG_TYPE_STRING },
    [4] = { .name = ST_DATA0, .type = BLOBMSG_TYPE_STRING },
    [5] = { .name = ST_DATA1, .type = BLOBMSG_TYPE_STRING },
    [6] = { .name = ST_DATA2, .type = BLOBMSG_TYPE_STRING },
};


static int test_count=0;
static void complete_data_cb(struct ubus_request *req, int ret)
{
    printf("free req\n");
    // if(req->priv)
    // {
    //     free(req->priv);
    // };
    if(req)
    {
        free(req);
        req = NULL;
    }
    
    test_count++;
    printf("%d\n",test_count);
    printf("%d\n",list_count);
    printf("free done\n");
}

static void receive_data_cb(struct ubus_request *req, int type, struct blob_attr *msg)
{
    SLOGI("receive msg from server");
    const char *msgstr = "(unknown)";
    struct blob_attr *tb[__NOTIFY_MAX];
    blobmsg_parse(msg_policy, ARRAY_SIZE(msg_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
    {
        msgstr = blobmsg_data(tb[0]);
    }
    SLOGI("msgstr = %s\n", msgstr);
    new_allubus *data = ((new_allubus *)(req->priv));

    if(data)
    {
        SLOGI("req.priv = %x\n", req->priv);
        SLOGI("alldata = %x\n", data);

        SLOGI("data.name = %s\n", data->name);
        SLOGI("data.id = %u\n", data->id);
        SLOGI("data.type = %d\n", data->type);
        SLOGI("data.numArgs = %d\n", data->numArgs);

        if(data->name && strlen(data->name))
        {
            //printf("cmd_support = %d\n", sizeof(cmd_support)/sizeof(char*));
            int i;
            for (i = 0; i < sizeof(cmd_support)/sizeof(char*); i++)
            {
                if(!strcmp(data->name, cmd_support[i]))
                {
                    int j;
                    if(!strcmp(data->name, ST_SET_BINARY))
                    {
                        VR_(set_led_state)(g_led_shm, ST_Done_secure_command);
                    }

                    for (j = 0; j < sizeof(daemon_services)/sizeof(daemon_service); j++)
                    {
                        if(data->data && (!strcmp((char*)data->data, daemon_services[j].name)))
                        {
                            daemon_services[j].timeout = 0;
                        }
                    }
                    
                    switch(data->type)
                    {
                        case ALL_SESSION:
                            if(strstr(msgstr, ST_SUCCESSFUL))
                            {
                                Broadcast_message(data->name, (char *)msgstr, data->numArgs);
                            }
                            else
                            {
                                Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, data->name, (char *)msgstr, data->numArgs);
                            }
                            // if(!strstr(msgstr, "\"type\": \"upnp\""))
                            // {
                            //     Send_ubus_notify((char*)msgstr);
                            // }
                            break;
                        case ONLY_SESSION:
                        {
                            Send_Signal(data->id, data->name, (char *)msgstr, data->numArgs);
                            break;
                        }
                        case 2:
                        {
                            sprintf(data->data + strlen((char *)data->data), "%s",  (char *)msgstr);
                            break;
                        }
                        default:
                            SLOGW("not support\n");
                            break;
                    }

                    break;
                }
            }
        }

        remove_list(data);
    }
}

static int auto_scan_result(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    SLOGI("auto_scan_result\n");
    const char *wemoSsid = "(unknown)";
    struct blob_attr *tb[1];
    blobmsg_parse(auto_scan_result_policy, ARRAY_SIZE(auto_scan_result_policy), tb, blob_data(msg), blob_len(msg));
    if(tb[0])
    {
        wemoSsid = blobmsg_data(tb[0]);
        printf("wemoSsid = %s\n", wemoSsid);
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
        //json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string("found_wemo_not_config"));
        json_object_object_add(jobj, "wemoSsid", json_object_new_string(wemoSsid));
        Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, ST_NOTIFY, (char *)json_object_to_json_string(jobj), NUM_ARGS_NOTIFY);
        json_object_put(jobj);
    }
    else
    {
        printf("missing wemoSsid\n");
    }
    return 0;
}

static void _write_spec(char *serviceName, char *deviceId, char *class,
                        char *command, char *data0, char *data1, char *data2)
{
    if(!serviceName || !deviceId || !class || !command)
    {
        return;
    }

    int i;
    new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
    memset(allubus, 0x00, sizeof(new_allubus));

    strcpy(allubus->name, ST_WRITE_SPEC);
    allubus->type = 0;
    allubus->numArgs = NUM_ARGS_WRITE_SPEC;
    add_last_list(allubus);
    
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if (!strcmp (daemon_services[i].name, serviceName))
        {
            allubus->data = (void*)daemon_services[i].name;
            if( daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, deviceId);
                blobmsg_add_string(&buff, ST_CLASS, class);
                blobmsg_add_string(&buff, ST_CMD, command);
                if(data0)
                {
                    blobmsg_add_string(&buff, ST_DATA0, data0);
                }
                if(data1)
                {
                    blobmsg_add_string(&buff, ST_DATA1, data1);
                }
                if(data2)
                {
                    blobmsg_add_string(&buff, ST_DATA2, data2);
                }

                allubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req_s = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                allubus->req=req_s;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_WRITE_SPEC, buff.head, req_s,
                                receive_data_cb, complete_data_cb, (void *) allubus);
                break;
            }
        }
    }
}

static int ubus_write_spec(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    const char *serviceName = "(unknown)";
    const char *id = "(unknown)";
    const char *class = "(unknown)";
    const char *cmd = "(unknown)";
    const char *data0 = NULL;
    const char *data1 = NULL;
    const char *data2 = NULL;

    struct blob_attr *data[7];
    blobmsg_parse(write_spec_policy, ARRAY_SIZE(write_spec_policy), data, blob_data(msg), blob_len(msg));
    if(data[0] && data[1] && data[2] && data[3])
    {
        serviceName = blobmsg_data(data[0]);
        id = blobmsg_data(data[1]);
        class = blobmsg_data(data[2]);
        cmd = blobmsg_data(data[3]);
        SLOGI("serviceName = %s\n", serviceName);
        SLOGI("id = %s\n", id);
        SLOGI("class = %s\n", class);
        SLOGI("cmd = %s\n", cmd);
        if(data[4])
        {
            data0 = blobmsg_data(data[4]);
        }
        if(data[5])
        {
            data1 = blobmsg_data(data[5]);
        }
        if(data[6])
        {
            data2 = blobmsg_data(data[6]);
        }

        _write_spec((char *)serviceName, (char *)id, (char *)class,
                    (char *)cmd, (char *)data0, (char *)data1, (char *)data2);

        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
        ubus_send_reply(ctx, req, buff.head);
    }
    else
    {
        SLOGW("Invalid argument");
    }
    return 0;
}

static void _set_binary(char *serviceName, char *deviceId,
                        char *command, char *value, char *subdevId)
{
    if(!serviceName || !deviceId || !command
        || !value)
    {
        return;
    }

    int i;
    new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
    memset(allubus, 0x00, sizeof(new_allubus));

    strcpy(allubus->name, ST_SET_BINARY);
    allubus->type = 0;
    allubus->numArgs = NUM_ARGS_SETBINARY;
    add_last_list(allubus);
    
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if (!strcmp (daemon_services[i].name, serviceName))
        {
            allubus->data = (void*)daemon_services[i].name;
            if( daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, deviceId);
                blobmsg_add_string(&buff, ST_CMD, command);
                blobmsg_add_string(&buff, ST_VALUE, value);
                if(subdevId) blobmsg_add_string(&buff, ST_SUBDEVID, subdevId);

                allubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req_s = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                allubus->req=req_s;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_SET_BINARY, buff.head, req_s,
                            receive_data_cb, complete_data_cb, (void *) allubus);
                break;
            }
        }
    }
}

static int set_binary(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    const char *serviceName = "(unknown)";
    const char *id = "(unknown)";
    const char *cmd = "(unknown)";
    const char *value = "(unknown)";
    const char *subdevId = NULL;

    struct blob_attr *data[5];
    blobmsg_parse(set_binary_policy, ARRAY_SIZE(set_binary_policy), data, blob_data(msg), blob_len(msg));
    if(data[0] && data[1] && data[2] && data[3])
    {
        serviceName = blobmsg_data(data[0]);
        id = blobmsg_data(data[1]);
        cmd = blobmsg_data(data[2]);
        value = blobmsg_data(data[3]);
        SLOGI("serviceName = %s\n", serviceName);
        SLOGI("id = %s\n", id);
        SLOGI("value = %s\n", value);
        if(data[4])
        {
            subdevId = blobmsg_data(data[4]);
        }

        if(serviceName && !strcasecmp(serviceName, ST_VENUS_SERVICE))
        {
            if(!strcasecmp(id, "alarm"))
            {
                char *alarm_enable = VR_(read_option)(NULL, UCI_ALARM_ENABLE);
                if(!alarm_enable)
                {
                    uci_do_add("security", "venus-alarm");
                    VR_(write_option)(NULL, UCI_ALARM_ENABLE"=1");
                }
                else
                {
                    free(alarm_enable);
                }

                if(!strcmp(value, "ON"))
                {
                    VR_(write_option)(NULL, UCI_ALARM_ENABLE"=1");
                }
                else
                {
                    VR_(write_option)(NULL, UCI_ALARM_ENABLE"=0");
                }

                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SET_BINARY_R));
                json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_VENUS_SERVICE));
                json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
                ubus_send_reply(ctx, req, buff.head);

                Broadcast_message(ST_NOTIFY, (char *)json_object_to_json_string(jobj), NUM_ARGS_NOTIFY);
                json_object_put(jobj);
            }
        }
        else
        {
            _set_binary((char *)serviceName, (char *)id,
                        (char *)cmd, (char *)value, (char *)subdevId);

            blob_buf_init(&buff, 0);
            blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
            ubus_send_reply(ctx, req, buff.head);
        }
    }
    else
    {
        SLOGW("Invalid argument");
    }
    return 0;
}

static int setgroup_binary(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    const char *groupId = "(unknown)";
    const char *cmd = "(unknown)";
    const char *value = "(unknown)";

    struct blob_attr *data[3];
    blobmsg_parse(setgroup_binary_policy, ARRAY_SIZE(setgroup_binary_policy), data, blob_data(msg), blob_len(msg));
    if(data[0] && data[1] && data[2])
    {
        groupId = blobmsg_data(data[0]);
        cmd = blobmsg_data(data[1]);
        value = blobmsg_data(data[2]);
        SLOGI("groupId = %s\n", groupId);
        SLOGI("cmd = %s\n", cmd);
        SLOGI("value = %s\n", value);

        SEARCH_DATA_INIT_VAR(device);

        searching_database("alljoyn_handler", dev_db, get_last_data_cb, &device, 
                            "SELECT device from GROUPS where groupId='%s'",
                            groupId);
        if(device.len)
        {
            json_object * jobj = VR_(create_json_object)(device.value);
            if(jobj)
            {
                group_action_data data;
                data.command = (char*)cmd;
                data.value = (char*)value;
                group_control_process(jobj, (void*)&data, group_action_cb);
                
                usleep(5000);
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
                ubus_send_reply(ctx, req, buff.head);
                json_object_put(jobj);
            }
            else
            {
                SLOGE("Failed to parse json\n");
            }
        }
        FREE_SEARCH_DATA_VAR(device);
    }
    else
    {
        SLOGW("Invalid argument");
    }
    return 0;
}

static int network_cb(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    int i;
    const char *state = "(unknown)";

    struct blob_attr *data[1];
    blobmsg_parse(network_policy, ARRAY_SIZE(network_policy), data, blob_data(msg), blob_len(msg));
    if(data[0])
    {
        state = blobmsg_data(data[0]);
        SLOGI("state = %s\n", state);

        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if((!strcmp("zigbee", daemon_services[i].name) || !strcmp("zwave", daemon_services[i].name)) 
                && daemon_services[i].id != 0)
            {
                printf("daemon_services[i].name = %s\n", daemon_services[i].name);
                new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
                memset(allubus, 0x00, sizeof(new_allubus));

                strcpy(allubus->name, ST_OPEN_CLOSE_NETWORK);
                allubus->type = 0;
                allubus->numArgs = 3;
                allubus->data = (void*)daemon_services[i].name;
                allubus->action_time = (unsigned)time(NULL);
                add_last_list(allubus);

                blob_buf_init(&buff, 0);

                struct ubus_request *req_s = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                allubus->req=req_s;

                if(!strcasecmp(state,ST_OPEN))
                {
                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_OPEN_NETWORK, buff.head, req_s,
                                    receive_data_cb, complete_data_cb, (void *) allubus);
                }
                else if(!strcasecmp(state,ST_CLOSE))
                {
                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_CLOSE_NETWORK, buff.head, req_s,
                                    receive_data_cb, complete_data_cb, (void *) allubus);
                }
            }
        }
        usleep(5000);
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
        ubus_send_reply(ctx, req, buff.head);

    }
    else
    {
        SLOGW("Invalid argument");
    }
    return 0;
}

static void notify_handle_remove(struct ubus_context *ctx, struct ubus_subscriber *s, uint32_t id)
{
    SLOGI("Object %08x went away\n", id);
    int i,ret;
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++) 
    {
        if (daemon_services[i].id == id)
        {
            ret = ubus_unsubscribe(ctx, &alljoyn_notify, daemon_services[i].id);
            SLOGW("UNsubscribe object %s with id %08X: %s\n", daemon_services[i].name, daemon_services[i].id, ubus_strerror(ret));
            SLOGI("removing id\n");
            daemon_services[i].id = 0;
            daemon_services[i].timeout = 0;
        }
    }
}

static int receive_notify_cb(struct ubus_context *ctx, struct ubus_object *obj,
                struct ubus_request_data *req, const char *method,
                struct blob_attr *msg)
{
    SLOGI("ALLJOYN receive_notify_cb from %s\n", method);
    const char *msgstr = "(unknown)";
    struct blob_attr *tb[1];
    blobmsg_parse(msg_policy, ARRAY_SIZE(msg_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
    {
        msgstr = blobmsg_data(tb[0]);
        SLOGI("msgstr notify = %s\n", msgstr);
        Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, ST_NOTIFY, (char *)msgstr, NUM_ARGS_NOTIFY);
    }
    return 0;
}

static const struct blobmsg_policy send_notify_policy[2] = {
    [0] = { .name = ST_SERVICE, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_MESSAGE, .type = BLOBMSG_TYPE_STRING },
};

static int send_notify(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    char *msgstr = blobmsg_format_json(msg, true);
    json_object *jobj = VR_(create_json_object)(msgstr);
    if(!jobj)
    {
        SAFE_FREE(msgstr);
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_FAILED);
        ubus_send_reply(ctx, req, buff.head);
        return 0;
    }

    CHECK_JSON_OBJECT_EXIST(serviceObj, jobj, ST_SERVICE, notify_done);
    CHECK_JSON_OBJECT_EXIST(messageObj, jobj, ST_MESSAGE, notify_done);

    const char *service = json_object_get_string(serviceObj);
    const char *message = json_object_get_string(messageObj);

    if(!strcmp(service, ST_PUBSUB))
    {
        Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, ST_NOTIFY, (char *)message, NUM_ARGS_NOTIFY);
    }
    else if(!strcmp(service, "shutdown"))
    {
        json_object *shutdownMsg = json_object_new_object();
        json_object_object_add(shutdownMsg, ST_TYPE, json_object_new_string(ST_ALLJOYN));
        json_object_object_add(shutdownMsg, ST_METHOD, json_object_new_string(ST_NOTIFY));
        json_object_object_add(shutdownMsg, ST_NOTIFY_TYPE, json_object_new_string("alljoynShutdown"));
        Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, ST_NOTIFY, (char *)json_object_to_json_string(shutdownMsg), NUM_ARGS_NOTIFY);
        json_object_put(shutdownMsg);
    }

notify_done:

    SAFE_FREE(msgstr);
    json_object_put(jobj);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
    ubus_send_reply(ctx, req, buff.head);
    
    return 0;
}

static const struct blobmsg_policy deny_access_policy[1] = {
    [0] = { .name = ST_USER_ID, .type = BLOBMSG_TYPE_STRING },
};

static int deny_access_ubus_handler(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    SLOGI("deny_access_ubus_handler\n");
    const char *userId = "(unknown)";

    struct blob_attr *tb[1];
    blobmsg_parse(deny_access_policy, ARRAY_SIZE(deny_access_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
    {
        userId = blobmsg_data(tb[0]);

        database_actions("alljoyn_handler", user_db, 
                        "delete from USERS where userId='%s'", userId);

        VR_(generate_128bit)(g_psk);
        VR_(write_option)(NULL, UCI_LOCAL_PSK"=%s", g_psk);

        json_object *notify_jobj = json_object_new_object();
        json_object_object_add(notify_jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
        json_object_object_add(notify_jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_PSK_CHANGED));
        Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, ST_NOTIFY, (char *)json_object_to_json_string(notify_jobj), NUM_ARGS_NOTIFY);
        json_object_put(notify_jobj);

        VR_(remove_deny_timer)((char*)userId); 
    }

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
    ubus_send_reply(ctx, req, buff.head);
    
    return 0;
}

static int send_publish(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    char *msgstr = blobmsg_format_json(msg, true);
    json_object *jobj = VR_(create_json_object)(msgstr);
    if(!jobj)
    {
        SAFE_FREE(msgstr);
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_FAILED);
        ubus_send_reply(ctx, req, buff.head);
        return 0;
    }

    CHECK_JSON_OBJECT_EXIST(serviceObj, jobj, ST_SERVICE, publish_done);
    CHECK_JSON_OBJECT_EXIST(messageObj, jobj, ST_MESSAGE, publish_done);

    const char *service = json_object_get_string(serviceObj);
    const char *message = json_object_get_string(messageObj);

    if(!strcmp(service, ST_PUBSUB))
    {
        Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, ST_PUBLISH, (char *)message, NUM_ARGS_NOTIFY);
    }

publish_done:

    SAFE_FREE(msgstr);
    json_object_put(jobj);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
    ubus_send_reply(ctx, req, buff.head);

    return 0;
}

static const struct blobmsg_policy trigger_policy[2] = {
    [0] = { .name = ST_RULE_ID, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_ACTIONS, .type = BLOBMSG_TYPE_STRING },
};

/*local always using TRIGGER MODE*/
static void _trigger_actions_(char *ruleId, char *actions)
{
    if(!ruleId || !actions)
    {
        return;
    }

    char *currentHubUUID = VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
    if(!currentHubUUID)
    {
        SLOGI("could not get hubUUID\n");
        return;
    }

    char *save_tok_actions;
    char *tok_action = strtok_r((char*)actions, "&&", &save_tok_actions);
    while(tok_action)
    {
        char *hubUUID = NULL;
        char *service = NULL;
        char *devId = NULL;
        char *cmd = NULL;
        char *mode = NULL;/*for thermostat only*/
        char *value = NULL;
        char *unit = NULL;/*for thermostat only*/

        char *save_tok_act;
        char *tok = strtok_r(tok_action, ";", &save_tok_act);
        GET_DATA_FROM_TOKEN_STRING(tok, hubUUID, ";", save_tok_act);
        if(strcmp(hubUUID, currentHubUUID))
        {
            /*send trigger to pubsub*/
            VR_(execute_system)("ubus call pubsub sendTrigger '{\"hubUUID\":\"%s\",\"id\":\"%s\"}' &", hubUUID, ruleId);
            goto next_action;
        }
        
        GET_DATA_FROM_TOKEN_STRING(tok, service, ";", save_tok_act);
        GET_DATA_FROM_TOKEN_STRING(tok, devId, ";", save_tok_act);
        GET_DATA_FROM_TOKEN_STRING(tok, cmd, ";", save_tok_act);
        if(cmd && !strcmp(ST_SET_TEMP, cmd))
        {
            GET_DATA_FROM_TOKEN_STRING(tok, mode, ";", save_tok_act);
        }
        GET_DATA_FROM_TOKEN_STRING(tok, value, ";", save_tok_act);

        if(cmd && !strcmp(ST_SET_TEMP, cmd))
        {
            GET_DATA_FROM_TOKEN_STRING(tok, unit, ";", save_tok_act);
            _write_spec(service, devId, (char*)ST_THERMOSTAT_SETPOINT, (char*)ST_SET,
                        mode, value, unit);
        }
        else if(cmd && !strcmp(ST_SET_MODE, cmd))
        {
            _write_spec(service, devId, (char*)ST_THERMOSTAT_MODE, (char*)ST_SET, value, NULL, NULL);
        }
        else
        {
            _set_binary(service, devId, cmd, value, NULL);
        }

next_action:
        tok_action = strtok_r(NULL, "&&", &save_tok_actions);

        SAFE_FREE(hubUUID);
        SAFE_FREE(service);
        SAFE_FREE(devId);
        SAFE_FREE(cmd);
        SAFE_FREE(mode);
        SAFE_FREE(value);
        SAFE_FREE(unit);
    }

    SAFE_FREE(currentHubUUID);
}

/*using by timer rule(crontab script) and d2d rule (rule handler)*/
static int trigger_handler(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    SLOGI("trigger_handler\n");
    struct blob_attr *tb[2];
    blobmsg_parse(trigger_policy, ARRAY_SIZE(trigger_policy), tb, blob_data(msg), blob_len(msg));
    const char *ruleId = "(unknown)";
    const char *actions = "(unknown)";

    if(!tb[0]  || !tb[1])
    {
        SLOGI("missing infor\n");
        goto done;
    }

    ruleId = blobmsg_data(tb[0]);
    actions = blobmsg_data(tb[1]);

    rule_report_timeline((char *)ruleId);
    _trigger_actions_((char *)ruleId, (char *)actions);
done:
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
    ubus_send_reply(ctx, req, buff.head);
    
    return 0;
}

static int stream_store(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    const char *serviceName = "(unknown)";
    const char *id = "(unknown)";
    // const char *cmd = "(unknown)";
    const char *durationTimeStr = "(unknown)";

    struct blob_attr *data[5];
    blobmsg_parse(set_binary_policy, ARRAY_SIZE(set_binary_policy), data, blob_data(msg), blob_len(msg));
    if(data[0] && data[1] && data[2] && data[3])
    {
        serviceName = blobmsg_data(data[0]);
        id = blobmsg_data(data[1]);
        // cmd = blobmsg_data(data[2]);
        durationTimeStr = blobmsg_data(data[3]);
        SLOGI("serviceName = %s\n", serviceName);
        SLOGI("id = %s\n", id);
        SLOGI("durationTime = %s\n", durationTimeStr);

        uint32_t durationTime = strtol(durationTimeStr, NULL, 0);

        if(serviceName && !strcasecmp(serviceName, ST_PLUTO))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_PLUTO));
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_STORE_R));
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(id));
            if(durationTime > 0)
            {
                json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_START));
            }
            else
            {
                json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_STOP));
            }
            json_object_object_add(jobj, ST_DURATION, json_object_new_string(durationTimeStr));

            Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, ST_STORE, (char *)json_object_to_json_string(jobj), NUM_ARGS_NOTIFY);
            json_object_put(jobj);

            blob_buf_init(&buff, 0);
            blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
            ubus_send_reply(ctx, req, buff.head);
        }
    }
    else
    {
        SLOGW("Invalid argument");
    }
    return 0;
}

static const struct ubus_method alljoyn_methods[] = {
    // UBUS_METHOD("register_notify", register_notify, notify_policy),
    UBUS_METHOD(ST_AUTO_SCAN_RESULT, auto_scan_result, auto_scan_result_policy),
    UBUS_METHOD(ST_WRITE_SPEC, ubus_write_spec, write_spec_policy),
    UBUS_METHOD(ST_NETWORK_UBUS, network_cb, network_policy),
    UBUS_METHOD(ST_SET_BINARY, set_binary, set_binary_policy),
    UBUS_METHOD(ST_STORE, stream_store, set_binary_policy),
    UBUS_METHOD(ST_SEND_NOTIFY, send_notify, send_notify_policy),
    UBUS_METHOD(ST_SEND_PUBLISH, send_publish, send_notify_policy),
    UBUS_METHOD(ST_SET_GROUP_BINARY, setgroup_binary, setgroup_binary_policy),
    UBUS_METHOD(ST_DENY_ACCESS, deny_access_ubus_handler, deny_access_policy),
    UBUS_METHOD(ST_TRIGGER, trigger_handler, trigger_policy),
};

static struct ubus_object_type alljoyn_object_type =
    UBUS_OBJECT_TYPE(ST_ALLJOYN, alljoyn_methods);

static struct ubus_object alljoyn_object = {
    //.subscribe_cb = test_client_subscribe_cb,
    .name = ST_ALLJOYN,
    .type = &alljoyn_object_type,
    .methods = alljoyn_methods,
    .n_methods = ARRAY_SIZE(alljoyn_methods),
};

static void VR_(handle_signal)(int sig)
{
    // pthread_cancel(thread_remove_actions_timeout);
    remove_actions_timeout_enable = 0;
    uloop_end();
    g_interrupt = QCC_TRUE;
}

// void Send_ubus_notify(char *data)
// {
//     SLOGI("ubus notify data = %s\n", data);
//     blob_buf_init(&buff, 0);
//     blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, data);
    
//     if(ctx)
//     {
//         SLOGI("start send ubus notify\n");
//         int rc = ubus_notify(ctx, &alljoyn_object, "alljoyn", buff.head, 0);
//         SLOGI("end send ubus notify rc = %d\n", rc);

//     }
// }

/* ObjectRegistered callback */
void busobject_object_registered(const void* context)
{
    SLOGI("ObjectRegistered has been called\n");
}

static void sessionportlistener_sessionjoined_cb(const void* context,
                                                 alljoyn_sessionport sessionPort,
                                                 alljoyn_sessionid id,
                                                 const char* joiner)
{
    QCC_UNUSED(context);
    QCC_UNUSED(sessionPort);
    //QCC_UNUSED(joiner);

    SLOGI("joiner = %s\n", joiner);
    SLOGI("Session Joined SessionId = %u\n", id);
}

static QCC_BOOL sessionportlistener_acceptsessionjoiner_cb(const void* context,
                                                           alljoyn_sessionport sessionPort,
                                                           const char* joiner,
                                                           const alljoyn_sessionopts opts)
{
    QCC_UNUSED(context);
    //QCC_UNUSED(joiner);
    SLOGI("joiner = %s\n", joiner);
    QCC_UNUSED(opts);
    if (sessionPort != SERVICE_PORT) {
        SLOGE("Rejecting join attempt on unexpected session port %d", sessionPort);
        return QCC_FALSE;
    }
    return QCC_TRUE;
}

QCC_BOOL AJ_CALL request_credentials(const void* context, const char* authMechanism, const char* authPeer, uint16_t authCount,
                                     const char* userName, uint16_t credMask, alljoyn_credentials credentials)
{
    if (!strcmp(authMechanism, "ALLJOYN_ECDHE_PSK")) 
    {
        if(is_onboarding_connect(authPeer))
        {
            alljoyn_credentials_setpassword(credentials, "8888");
        }
        else
        {
            alljoyn_credentials_setpassword(credentials, g_psk);
            alljoyn_credentials_setexpiration(credentials, 10);
        }
        return QCC_TRUE;
    }
    return QCC_FALSE;
}

static void inform_new_hub_could_config(void *data)
{
    json_object *hubsList = json_object_new_array();
    get_detected_hubs(hubsList);
    int arraylen = json_object_array_length(hubsList);
    if(arraylen == 0)
    {
        json_object_put(hubsList);
        return;
    }

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_NEW_HUB));
    json_object_object_add(jobj, ST_HUBS_LIST, hubsList);
    Broadcast_message(ST_NOTIFY, (char *)json_object_to_json_string(jobj), NUM_ARGS_NOTIFY);
    json_object_put(jobj);

    g_timer_inform_hub = 0;

    return;
}

void AJ_CALL authentication_complete(const void* context, const char* authMechanism, const char* peerName, QCC_BOOL success)
{
    printf("authentication_complete %s %s\n", authMechanism, success == QCC_TRUE ? ST_SUCCESSFUL : ST_FAILED);
    if(g_timer_inform_hub)
    {
        timerCancel(&g_timer_inform_hub);
    }

    timerStart(&g_timer_inform_hub, inform_new_hub_could_config, NULL, 10000, TIMER_ONETIME);
}

static alljoyn_sessionportlistener create_my_alljoyn_sessionportlistener()
{
    alljoyn_sessionportlistener_callbacks* callbacks =
        (alljoyn_sessionportlistener_callbacks*)
        malloc(sizeof(alljoyn_sessionportlistener_callbacks));

    callbacks->accept_session_joiner = sessionportlistener_acceptsessionjoiner_cb;
    callbacks->session_joined = sessionportlistener_sessionjoined_cb;
    return alljoyn_sessionportlistener_create(callbacks, NULL);
}

QStatus Send_Signal(alljoyn_sessionid sessionId, char* name, char* msg, int _numArgs)
{
    SLOGI("in Send_Signal\n");
    size_t numArgs = _numArgs;

    alljoyn_interfacedescription_member response_signal_member;

    alljoyn_interfacedescription responseIntf = alljoyn_busattachment_getinterface(g_msgBus, CONTROL_INTF);
    if(alljoyn_interfacedescription_getmember(responseIntf, name, &response_signal_member) == QCC_FALSE)
    {
        SLOGI("COULD NOT FOUND %s from CONTROL_INTF, try to get from MANAGE_INTF\n", name);
        responseIntf = alljoyn_busattachment_getinterface(g_msgBus, MANAGE_INTF);
        if(alljoyn_interfacedescription_getmember(responseIntf, name, &response_signal_member) == QCC_FALSE)
        {
            SLOGE("Failed to get interface description %s\n", name);
            return 1;
        }
    }

    SLOGI("msg = %s\n", msg);
    alljoyn_msgarg outArg = alljoyn_msgarg_array_create(_numArgs);
    QStatus status;

    switch(_numArgs)
    {
        case 1:
        {
            status = alljoyn_msgarg_array_set(outArg, &numArgs, "s", msg);
            break;
        };

        case 2:
        {
            status = alljoyn_msgarg_array_set(outArg, &numArgs, "ss", msg, NULL);
            break;
        };

        case 3:
        {
            status = alljoyn_msgarg_array_set(outArg, &numArgs, "sss", msg, NULL, NULL);
            break;
        };

        case 4:
        {
            status = alljoyn_msgarg_array_set(outArg, &numArgs, "ssss", msg, NULL, NULL, NULL);
            break;
        };

        case 5:
        {
            status = alljoyn_msgarg_array_set(outArg, &numArgs, "sssss", msg, NULL, NULL, NULL, NULL);
            break;
        };

        case 6:
        {
            status = alljoyn_msgarg_array_set(outArg, &numArgs, "ssssss", msg, NULL, NULL, NULL, NULL, NULL);
            break;
        };

        case 7:
        {
            status = alljoyn_msgarg_array_set(outArg, &numArgs, "sssssss", msg, NULL, NULL, NULL, NULL, NULL, NULL);
            break;
        };

        case 10:
        {
            status = alljoyn_msgarg_array_set(outArg, &numArgs, "ssssssssss", msg, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
            break;
        };
    };

    if (ER_OK != status) {
        SLOGW("Arg assignment failed: %s\n", QCC_StatusText(status));
    }
    status  = alljoyn_busobject_signal(CommonObj, NULL, sessionId, response_signal_member, outArg , _numArgs, 5000, 0, NULL);

    if(ER_OK != status)
    {
        SLOGW("Failed to send signal %s with id %u\n", name, sessionId);
    }
    alljoyn_msgarg_destroy(outArg);

    printf("end Send_Signal\n");
    return status;
}

/*############################# FOR LIST DEVICE ##############################*/
static int features_callback(void *status, int argc, char **argv, char **azColName)
{
    char *featureId = argv[0];
    char *registerId = argv[1];
    
    if(!strcmp(featureId, ST_WAKE_UP))
    {
        char *nodeId = argv[1];
        char *value = argv[2];
        char wakeUpInfo[SIZE_64B];
        sprintf(wakeUpInfo, "%s:%s", nodeId, value);

        json_object_object_add(status, featureId, json_object_new_string(wakeUpInfo));
    }
    else
    {
        json_object_object_add(status, featureId, json_object_new_string(registerId ? registerId : ""));
    }
    
    return 0;
}

static int capability_callback(void *data, int argc, char **argv, char **azColName)
{
    json_object *capability = (json_object *)data;

    if(!capability)
    {
        return 0;
    }

    int i;
    char *scheme = NULL;
    char *capList = NULL;
    char *endpointMem = NULL;
    for(i=0; i< argc; i++)
    {
        if(!strcmp(azColName[i], ST_SCHEME))
        {
            scheme = argv[i];
        }
        else if(!strcmp(azColName[i], ST_CAP_LIST))
        {
            capList = argv[i];
        }
        else if(!strcmp(azColName[i], ST_ENDPOINT_MEM))
        {
            endpointMem = argv[i];
        }
    }

    json_object *nonSecObj = json_object_new_object();
    JSON_ADD_STRING_SAFE(nonSecObj,ST_SCHEME,scheme);
    JSON_ADD_STRING_SAFE(nonSecObj,ST_CAP_LIST,capList);
    JSON_ADD_STRING_SAFE(nonSecObj,ST_ENDPOINT_MEM,endpointMem);
    json_object_array_add(capability, nonSecObj);

    return 0;
}

static int get_usercode_callback(void *userCode, int argc, char **argv, char **azColName)
{
    if(argc < 2)
    {
        return 0;
    }

    char *userId = argv[0];
    char *pass = argv[1];
    json_object *code = json_object_new_object();
    json_object_object_add(code, ST_USER_ID, json_object_new_string(userId ? userId : ""));
    json_object_object_add(code, ST_NAME, json_object_new_string(pass ? pass : ""));
    json_object_array_add(userCode, code);

    return 0;
}

static void get_door_lock_user_code(sqlite3 *dev_db, json_object *jobj, char *deviceId)
{
    if(!jobj)
    {
        return;
    }

    json_object *userCode = json_object_new_array();

    searching_database("alljoyn_handler", dev_db, get_usercode_callback, (void *)userCode,
                        "SELECT %s,%s from USER_CODE where %s='%s'"
                        , ST_USER_ID, ST_PASS,
                        ST_DEVICE_ID, deviceId);

    int arraylen = json_object_array_length(userCode);
    if(arraylen > 0)
    {
        json_object_object_add(jobj, ST_USER_CODE, userCode);
    }
    else
    {
        json_object_put(userCode);
    }
}

static int get_scene_callback(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 3)
    {
        SLOGE("less than 3\n");
        return 0;
    }

    json_object *scene = (json_object*)data;
    if(!scene)
    {
        SLOGE("scene is NULL\n");
        return 0;
    }

    char *buttonId = argv[0];
    char *sceneId = argv[1];
    char *timeActive = argv[2];
    
    json_object_object_add(scene, ST_BUTTON_ID, json_object_new_string(buttonId ? buttonId : ""));
    json_object_object_add(scene, ST_SCENE_ID, json_object_new_string(sceneId ? sceneId : ""));
    json_object_object_add(scene, ST_TIME_ACTIVE, json_object_new_string(timeActive ? timeActive : ""));
    return 0;
}

static int get_property_callback(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 2)
    {
        SLOGE("less than 2\n");
        return 0;
    }

    json_object *property = (json_object*)data;
    if(!property)
    {
        SLOGE("property is NULL\n");
        return 0;
    }

    char *key = argv[0];
    char *value = argv[1];
    
    json_object_object_add(property, key, json_object_new_string(value ? value : ""));
    return 0;
}

static int get_association_callback(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 3)
    {
        return 0;
    }

    if(!data)
    {
        return 0;
    }

    status_data *tmp = (status_data*)data;

    char *deviceId = argv[0];
    char *groupId = argv[1];
    char *nodeList = argv[2];
    json_object *asso = json_object_new_object();
    json_object_object_add(asso, ST_GROUP_ID, json_object_new_string(groupId ? groupId : ""));
    json_object_object_add(asso, ST_NODE_LIST, json_object_new_string(nodeList ? nodeList : ""));

    searching_database("alljoyn_handler", (sqlite3 *)tmp->db, get_property_callback, (void *)asso,
                        "SELECT key,value from MORE_PROPERTY where %s='%s' AND %s='%s'"
                        " AND %s='%s'", ST_DEVICE_ID, deviceId, ST_DEVICE_TYPE, ST_ASSOCIATION, 
                        ST_PROPERTY_ID, groupId);

    searching_database("alljoyn_handler", (sqlite3 *)tmp->db, get_scene_callback, (void *)asso,
                        "SELECT buttonId,sceneId,timeActive from SCENES where %s='%s' AND %s='%s'", 
                        ST_DEVICE_ID, deviceId, ST_GROUP_ID, groupId);
    // SLOGI("asso = %s\n", json_object_to_json_string(asso));
    json_object_array_add(tmp->object, asso);

    return 0;
}

static void get_association_data(sqlite3 *dev_db, json_object *jobj, char *deviceId)
{
    if(!jobj)
    {
        return;
    }

    json_object *association = json_object_new_array();

    status_data data;
    data.object = (void*)association;
    data.db = (void*)dev_db;

    searching_database("alljoyn_handler", dev_db, get_association_callback, (void *)&data,
                        "SELECT %s,%s,%s from ASSOCIATION_DATA where %s='%s'"
                        , ST_DEVICE_ID, ST_GROUP_ID, ST_NODE_LIST,
                        ST_DEVICE_ID, deviceId);

    int arraylen = json_object_array_length(association);
    if(arraylen > 0)
    {
        json_object_object_add(jobj, ST_ASSOCIATION, association);
    }
    else
    {
        json_object_put(association);
    }
}

static int get_meter_callback(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 4)
    {
        return 0;
    }

    if(!data)
    {
        return 0;
    }

    status_data *tmp = (status_data*)data;

    char *deviceId = argv[0];
    char *meterType = argv[1];
    char *meterValue = argv[2];
    char *meterUnit = argv[3];
    json_object *meter = json_object_new_object();
    json_object_object_add(meter, ST_METER_TYPE, json_object_new_string(meterType ? meterType : ""));
    json_object_object_add(meter, ST_METER_VALUE, json_object_new_string(meterValue ? meterValue : ""));
    json_object_object_add(meter, ST_METER_UNIT, json_object_new_string(meterUnit ? meterUnit : ""));

    searching_database("alljoyn_handler", (sqlite3 *)tmp->db, get_property_callback, (void *)meter,
                        "SELECT key,value from MORE_PROPERTY where %s='%s' AND %s='%s'"
                        " AND %s='%s'", ST_DEVICE_ID, deviceId, ST_DEVICE_TYPE, ST_METER, 
                        ST_PROPERTY_ID, meterType);
    json_object_array_add(tmp->object, meter);

    return 0;
}

static void get_meter_data(sqlite3 *dev_db, json_object *jobj, char *deviceId)
{
    if(!jobj)
    {
        return;
    }

    json_object *meter = json_object_new_array();
    status_data data;
    data.object = (void*)meter;
    data.db = (void*)dev_db;

    searching_database("alljoyn_handler", dev_db, get_meter_callback, (void *)&data,
                        "SELECT %s,%s,%s,%s from METER_DATA where %s='%s'"
                        , ST_DEVICE_ID, ST_METER_TYPE, ST_METER_VALUE, ST_METER_UNIT,
                        ST_DEVICE_ID, deviceId);

    int arraylen = json_object_array_length(meter);
    if(arraylen > 0)
    {
        json_object_object_add(jobj, ST_METER, meter);
    }
    else
    {
        json_object_put(meter);
    }
}

static int get_sensor_multilevel_callback(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 4)
    {
        return 0;
    }

    if(!data)
    {
        return 0;
    }

    status_data *tmp = (status_data*)data;

    char *deviceId = argv[0];
    char *sensorType = argv[1];
    char *sensorValue = argv[2];
    char *sensorUnit = argv[3];
    json_object *sensor = json_object_new_object();
    json_object_object_add(sensor, ST_SENSOR_TYPE, json_object_new_string(sensorType ? sensorType : ""));
    json_object_object_add(sensor, ST_VALUE, json_object_new_string(sensorValue ? sensorValue : ""));
    json_object_object_add(sensor, ST_UNIT, json_object_new_string(sensorUnit ? sensorUnit : ""));

    searching_database("alljoyn_handler", (sqlite3 *)tmp->db, get_property_callback, (void *)sensor,
                        "SELECT key,value from MORE_PROPERTY where %s='%s' AND %s='%s'"
                        " AND %s='%s'", ST_DEVICE_ID, deviceId, ST_DEVICE_TYPE, ST_SENSOR_MULTILEVEL, 
                        ST_PROPERTY_ID, sensorType);
    json_object_array_add(tmp->object, sensor);

    return 0;
}

static void get_sensor_multilevel_data(sqlite3 *dev_db, json_object *jobj, char *deviceId)
{
    if(!jobj)
    {
        return;
    }

    json_object *sensorMultilevel = json_object_new_array();
    status_data data;
    data.object = (void*)sensorMultilevel;
    data.db = (void*)dev_db;

    searching_database("alljoyn_handler", dev_db, get_sensor_multilevel_callback, (void *)&data,
                        "SELECT %s,%s,%s,%s from SENSOR_DATA where %s='%s'"
                        , ST_DEVICE_ID, ST_SENSOR_TYPE, ST_SENSOR_VALUE, ST_SENSOR_UNIT,
                        ST_DEVICE_ID, deviceId);

    int arraylen = json_object_array_length(sensorMultilevel);
    if(arraylen > 0)
    {
        json_object_object_add(jobj, ST_SENSOR_MULTILEVEL, sensorMultilevel);
    }
    else
    {
        json_object_put(sensorMultilevel);
    }
}

static int get_thermostat_setpoint_callback(void *setpointArray, int argc, char **argv, char **azColName)
{
    if(argc < 3)
    {
        return 0;
    }

    char *mode = argv[0];
    char *value = argv[1];
    char *unit = argv[2];
    json_object *setpoint = json_object_new_object();
    json_object_object_add(setpoint, ST_MODE, json_object_new_string(mode ? mode : ""));
    json_object_object_add(setpoint, ST_VALUE, json_object_new_string(value ? value : ""));
    json_object_object_add(setpoint, ST_UNIT, json_object_new_string(unit ? unit : ""));

    json_object_array_add(setpointArray, setpoint);

    return 0;
}

static void get_thermostat_setpoint_data(sqlite3 *dev_db, json_object *jobj, char *deviceId)
{
    if(!jobj)
    {
        return;
    }

    json_object *thermostatSetpoint = json_object_new_array();

    searching_database("alljoyn_handler", dev_db, get_thermostat_setpoint_callback, (void *)thermostatSetpoint,
                        "SELECT %s,%s,%s from THERMOSTAT_SETPOINT where %s='%s'"
                        , ST_MODE, ST_VALUE, ST_UNIT,
                        ST_DEVICE_ID, deviceId);

    int arraylen = json_object_array_length(thermostatSetpoint);
    if(arraylen > 0)
    {
        json_object_object_add(jobj, ST_THERMOSTAT_SETPOINT, thermostatSetpoint);
    }
    else
    {
        json_object_put(thermostatSetpoint);
    }
}

static int get_alarm_callback(void *alarm, int argc, char **argv, char **azColName)
{
    if(argc < 1)
    {
        return 0;
    }

    char *alarmTypeFinal = argv[0];
    json_object_array_add(alarm, json_object_new_string(alarmTypeFinal ? alarmTypeFinal : ""));
    return 0;
}

static void get_alarm_data(sqlite3 *dev_db, json_object *jobj, char *deviceId)
{
    if(!jobj)
    {
        return;
    }

    json_object *alarm = json_object_new_array();
    searching_database("alljoyn_handler", dev_db, get_alarm_callback, (void *)alarm,
                        "SELECT %s from ALARM where %s='%s'"
                        ,ST_ALARM_TYPE_FINAL, ST_DEVICE_ID, deviceId);
    int arraylen = json_object_array_length(alarm);

    if (arraylen > 0)
    {
        json_object_object_add(jobj, ST_ALARM_STATUS, alarm);
    }
    else
    {
        json_object_put(alarm);
    }

}

static int listdevice_callback(void *data, int argc, char **argv, char **azColName)
{
    if(!data)
    {
        return 0;
    }

    int i;
    json_object *jobj = json_object_new_object();
    char deviceId[SIZE_64B];
    char deviceType[SIZE_64B];

    status_data *tmp = (status_data*)data;

    for(i=0; i<argc; i++)
    {
        if(!strcmp(azColName[i], ST_ID) || !strcmp(azColName[i], ST_UDN))
        {
            strcpy(deviceId, argv[i]);
        }

        if(!strcmp(azColName[i], ST_TYPE))
        {
            strcpy(deviceType, argv[i]);
        }

        if(!strcmp(azColName[i], ST_PARENT_ID))
        {
            json_object_object_add(jobj, azColName[i], json_object_new_string(argv[i] ? argv[i] : "0"));
        }
        else
        {
            json_object_object_add(jobj, azColName[i], json_object_new_string(argv[i] ? argv[i] : ""));
        }
    }

    SEARCH_DATA_INIT_VAR(resourceId);
    searching_database("alljoyn_handler", (sqlite3 *)tmp->db, get_last_data_cb, &resourceId,
                    "SELECT owner from DEVICES where deviceId='%s'", 
                    deviceId);
    if(resourceId.len)
    {
        json_object_object_add(jobj, ST_CLOUD_RESOURCE, json_object_new_string(resourceId.value));
    }

    FREE_SEARCH_DATA_VAR(resourceId);

    json_object *capabilityObj = json_object_new_array();
    searching_database("alljoyn_handler", (sqlite3 *)tmp->db, capability_callback, (void *)capabilityObj,
                    "SELECT %s,%s,%s from CAPABILITY where deviceId='%s'", 
                    ST_SCHEME, ST_CAP_LIST, ST_ENDPOINT_MEM, deviceId);

    json_object_object_add(jobj, ST_CAPABILITY, capabilityObj);
    json_object *status = json_object_new_object();

    searching_database("alljoyn_handler", (sqlite3 *)tmp->db, features_callback, (void *)status,
                        "SELECT %s,%s from FEATURES where %s='%s'"
                        , ST_FEATURE_ID, ST_REGISTER,
                        ST_DEVICE_ID, deviceId);

    get_alarm_data((sqlite3 *)tmp->db, status, deviceId);

    int association=0, meter=0, sensorMultilevel=0, userCode=0, thermostatSetpoint=0;
    get_feature_device_support(capabilityObj, deviceType, &association,
                      &meter, &sensorMultilevel, &userCode, &thermostatSetpoint);

    if(association)
    {
        get_association_data((sqlite3 *)tmp->db, status, deviceId);
    }

    if(meter)
    {
        get_meter_data((sqlite3 *)tmp->db, status, deviceId);
    }

    if(sensorMultilevel)
    {
        get_sensor_multilevel_data((sqlite3 *)tmp->db, status, deviceId);
    }

    if(userCode)
    {
        get_door_lock_user_code((sqlite3 *)tmp->db, status, deviceId);
    }

    if(thermostatSetpoint)
    {
        get_thermostat_setpoint_data((sqlite3 *)tmp->db, status, deviceId);
    }

    json_object_object_add(jobj, ST_STATUS, status);
    json_object_array_add(tmp->object, jobj);
    return 0;
}

void listdevices_signal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    QStatus status;
    SLOGI("listdevices_signal \n");
    int i = 0;
    char* type;

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &type);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: type\n");
    }
    else
    {
        SLOGI("type = %s\n", type);

        alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

        if(!strcmp(type, ST_ALL))
        {
            char timestamp[SIZE_32B];
            unsigned int current_time = (unsigned)time(NULL);
            sprintf(timestamp, "%u", current_time);

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ALL));
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_LIST_DEVICES_R));
            json_object_object_add(jobj, ST_CLOUD_TIME_STAMP, json_object_new_string(timestamp));
            json_object *devicelist = json_object_new_array();

            char cmd[SIZE_256B];
            char database_file[SIZE_256B];
            snprintf(database_file, sizeof(database_file), TEMP_DEVICES_DATABASE, current_time);
            snprintf(cmd, sizeof(cmd), "cp %s %s", DEVICES_DATABASE, database_file);
            VR_(execute_system)(cmd);

            sqlite3 *tmp_dev_db;

            int rc = VR_(get_file_infor)(database_file);

            if(rc == -1)
            {
                tmp_dev_db = dev_db;
            }
            else
            {
                rc = open_database(database_file, &tmp_dev_db);
                if(rc == -1)
                {
                    tmp_dev_db = dev_db;
                }
            }

            status_data data;
            data.object = (void*)devicelist;
            data.db = (void*)tmp_dev_db;

            DISABLE_DATABASE_LOG();

            searching_database("alljoyn_handler", tmp_dev_db, listdevice_callback, (void *)&data,
                        "SELECT * from CONTROL_DEVS where type='%s'", ST_UPNP);

            searching_database("alljoyn_handler", tmp_dev_db, listdevice_callback, (char *)&data, 
                        "SELECT %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s from SUB_DEVICES ",
                        ST_OWNER, ST_SERIAL_ID, ST_DEVICE_TYPE, ST_TYPE_BASIC, ST_TYPE_SPECIFIC, ST_DEVICE_MODE,
                        ST_FRIENDLY_NAME, ST_ID, ST_ENDPOINT_NUM, ST_PARENT_ID, ST_IS_ZWAVE_PLUS,
                        ST_ROLE_TYPE, ST_NODE_TYPE, ST_TYPE, ST_OWNER);

            ENABLE_DATABASE_LOG();

            if(rc != -1)
            {
                close_database(&tmp_dev_db);
            }

            sprintf(cmd, "rm %s", database_file);
            VR_(execute_system)(cmd);

            json_object_object_add(jobj, ST_DEVICES_LIST, devicelist);
            Send_Signal(sessionId, ST_LIST_DEVICES, (char *)json_object_to_json_string(jobj), NUM_ARGS_LIST_DEVICES);
            json_object_put(jobj);
        }
        else
        {
            new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
            memset(allubus, 0x00, sizeof(new_allubus));
            strcpy(allubus->name, ST_LIST_DEVICES);
            SLOGI("sessionId = %u\n", sessionId);
            allubus->id = sessionId;
            add_last_list(allubus);

            allubus->type = ONLY_SESSION;
            allubus->numArgs = NUM_ARGS_LIST_DEVICES;

            for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
            {
                SLOGI("daemon_services[%d].name = %s id = %08X\n", i, daemon_services[i].name, daemon_services[i].id);
                if(!strcmp(type, daemon_services[i].name))
                {
                    allubus->data = (void*)daemon_services[i].name;
                    if(daemon_services[i].id != 0)
                    {
                        allubus->action_time = (unsigned)time(NULL);

                        struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                        allubus->req=req;

                        VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_LIST_DEVICES, NULL, req,
                                    receive_data_cb, complete_data_cb, (void *) allubus);
                        break;
                    }
                }
            }
        }
    }
}

/*############################# END LIST DEVICE ##############################*/

void setbinary_signal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("setbinary_signal_handle\n");
    QStatus status;
    int i;
    char *serviceName, *id, *subDevId, *value, *command;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &serviceName);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: serviceName\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: id\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &command);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: command\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &value);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: value\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 4), "s", &subDevId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: subDevId\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    SLOGI("serviceName = %s\n", serviceName);
    SLOGI("id = %s\n", id);
    SLOGI("command = %s\n", command);
    SLOGI("value = %s\n", value);
    SLOGI("subDevId = %s\n", subDevId);
    SLOGI("sessionId = %u\n", sessionId);

    if(serviceName && !strcmp(serviceName, ST_VENUS_SERVICE))
    {
        if(!strcmp(id, ST_LOCATION))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SET_BINARY_R));
            json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_VENUS_SERVICE));

            char *latitude = VR_(read_option)(NULL, UCI_LOCATION_LATITUDE);
            char *longitude = VR_(read_option)(NULL, UCI_LOCATION_LONGITUDE);
            if(!latitude && !longitude)
            {
                uci_do_add("security", ST_LOCATION);
                VR_(write_option)(NULL, UCI_LOCATION_LATITUDE"=%s", value);
                VR_(write_option)(NULL, UCI_LOCATION_LONGITUDE"=%s", subDevId);
            }
            else
            {
                VR_(write_option)(NULL, UCI_LOCATION_LATITUDE"=%s", value);
                VR_(write_option)(NULL, UCI_LOCATION_LONGITUDE"=%s", subDevId);
                free(latitude);
                free(longitude);
            }
            json_object_object_add(jobj, ST_LATITUDE, json_object_new_string(value));
            json_object_object_add(jobj, ST_LONGTITUDE, json_object_new_string(subDevId));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

            Broadcast_message(ST_SET_BINARY, (char *)json_object_to_json_string(jobj), NUM_ARGS_SETBINARY);
            json_object_put(jobj);
            return;
        }
    }

    new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
    memset(allubus, 0x00, sizeof(new_allubus));

    strcpy(allubus->name, ST_SET_BINARY);
    allubus->id = sessionId;
    allubus->type = ALL_SESSION;
    allubus->numArgs = NUM_ARGS_SETBINARY;
    add_last_list(allubus);

    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(serviceName, daemon_services[i].name))
        {
            allubus->data = (void*)daemon_services[i].name;
            if( daemon_services[i].id != 0)
            {
                //VR_(set_led_state)(g_led_shm, ST_Received_secure_command );
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                blobmsg_add_string(&buff, ST_CMD, command);
                blobmsg_add_string(&buff, ST_VALUE, value);
                if(subDevId && strlen(subDevId)) 
                {
                    if(!strcmp(serviceName, ST_UPNP))
                    {
                        blobmsg_add_string(&buff, ST_SUBDEVID, subDevId);
                    }
                }

                allubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                allubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_SET_BINARY, buff.head, req,
                                receive_data_cb, complete_data_cb, (void *) allubus);
                break;
            }
        }
    }
}

void getbinary_signal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("getbinary_signal_handle\n");
    QStatus status;
    int i;
    char *serviceName, *id, *subDevId;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &serviceName);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: serviceName\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: id\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &subDevId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: subDevId\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    if(serviceName && !strcasecmp(serviceName, "venus"))
    {
        if(!strcmp(id, ST_LOCATION))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_BINARY_R));
            json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_VENUS_SERVICE));

            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ST_LOCATION));
            
            char *latitude = VR_(read_option)(NULL, UCI_LOCATION_LATITUDE);
            char *longitude = VR_(read_option)(NULL, UCI_LOCATION_LONGITUDE);

            if(latitude && longitude)
            {
                json_object_object_add(jobj, ST_LATITUDE, json_object_new_string(latitude));
                json_object_object_add(jobj, ST_LONGTITUDE, json_object_new_string(longitude));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }
            else
            {
                if(latitude)
                    free(latitude);
                if(longitude)
                    free(longitude);
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            }
            Send_Signal(sessionId, ST_GET_BINARY, (char *)json_object_to_json_string(jobj), NUM_ARGS_GETBINARY);
            json_object_put(jobj);

            return;
        }
    }

    new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
    memset(allubus, 0x00, sizeof(new_allubus));

    strcpy(allubus->name, ST_GET_BINARY);
    allubus->id = sessionId;
    allubus->type = ONLY_SESSION;
    allubus->numArgs = NUM_ARGS_GETBINARY;
    SLOGI("serviceName = %s\n", serviceName);
    SLOGI("id = %s\n", id);
    SLOGI("epnum = %s\n", subDevId);
    SLOGI("sessionId = %u\n", sessionId);
    add_last_list(allubus);

    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(serviceName, daemon_services[i].name))
        {
            allubus->data = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                if(subDevId && strlen(subDevId)) 
                {
                    if(!strcmp(serviceName, ST_UPNP))
                    {
                        blobmsg_add_string(&buff, ST_SUBDEVID, subDevId);
                    }
                }

                allubus->action_time = (unsigned)time(NULL);
                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                allubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_GET_BINARY, buff.head, req,
                                receive_data_cb, complete_data_cb, (void *) allubus);
                
                break;
            }
        }
    }
}

void adddevices_signal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    QStatus status;
    SLOGI("adddevices_signal\n");
    int i = 0;
    char* type, *deviceId, *subDevId;

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &type);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: type\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &deviceId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: device id\n");
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &subDevId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: subDevId\n");
    }

    if(type && strlen(type) && deviceId && strlen(deviceId) && subDevId && strlen(subDevId))
    {
        SLOGI("type = %s\n", type);
        alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

        if(strcmp(type, "upnp"))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_TYPE, json_object_new_string(type));
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_ADD_DEVICES_R));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("add devices just support only upnp subdevices"));
            Send_Signal(sessionId, ST_ADD_DEVICES, (char *)json_object_to_json_string(jobj), NUM_ARGS_ADD_DEVICES);
            json_object_put(jobj);
        }
        else
        {
            new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
            memset(allubus, 0x00, sizeof(new_allubus));

            strcpy(allubus->name, ST_ADD_DEVICES);
            SLOGI("sessionId = %u\n", sessionId);
            allubus->id = sessionId;

            allubus->type = ONLY_SESSION;
            allubus->numArgs = NUM_ARGS_ADD_DEVICES;
            add_last_list(allubus);
            for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
            {
                if(!strcmp(ST_UPNP, daemon_services[i].name))
                {
                    allubus->data = (void*)daemon_services[i].name;
                    if(daemon_services[i].id != 0 )
                    {
                        allubus->action_time = (unsigned)time(NULL);

                        blob_buf_init(&buff, 0);
                        blobmsg_add_string(&buff, ST_ID, deviceId);
                        blobmsg_add_string(&buff, ST_SUBDEVID, subDevId);

                        struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                        allubus->req=req;

                        VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_ADD_DEVICES, buff.head, req,
                                    receive_data_cb, complete_data_cb, (void *) allubus);
                        
                        break;
                    }
                }
            }
        }
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_ADD_DEVICES_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(alljoyn_message_getsessionid(msg), ST_ADD_DEVICES, (char *)json_object_to_json_string(jobj), NUM_ARGS_ADD_DEVICES);
        json_object_put(jobj);
    }
}

struct myprogress {
    alljoyn_sessionid id;
    const char *action;
};

static int download_value = 100;

static int older_progress(void *p,
                          double dltotal, double dlnow,
                          double ultotal, double ulnow)
{
    int current_value = 0;
    struct myprogress *myp = (struct myprogress *)p;

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
    json_object_object_add(jobj, ST_ACTION, json_object_new_string(myp->action));
    json_object_object_add(jobj, ST_STATE, json_object_new_string(ST_DOWNLOADING));

    if(dltotal)
    {
        current_value = (int)(100*dlnow/dltotal);

        char value[8];
        sprintf(value, "%d", current_value);
        json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
    }
    else
    {
        json_object_object_add(jobj, ST_VALUE, json_object_new_string("0"));
        current_value = 0;
    }

    if(download_value != current_value)
    {
        download_value = current_value;
        Send_Signal(myp->id, ST_FIRMWARE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_FIRMWARE_ACTIONS);
        json_object_put(jobj);
    }
    else
    {
        json_object_put(jobj);
    }

    return 0;
}

void updatefirmware(void *data)
{
    update_firmware_d *upfirm = (update_firmware_d *)data;
    SLOGI("upfirm->URL = %s\n", (char *)upfirm->URL);
    SLOGI("upfirm->sessionId = %u\n", (alljoyn_sessionid) upfirm->sessionId);

    struct myprogress myp;
    myp.id = upfirm->sessionId;
    myp.action = ST_UPDATE;

    const char *filename = FIRMWARE_NAME;
    int res = VR_(download_file)((char *)upfirm->URL, filename, NULL, older_progress, (void*)&myp);

    if(CURLE_OK != res)
    {
        // VR_(led_show)();
        // VR_(execute_system)("ubus send alljoyn '{\"state\":\"download_failed\"}' &");
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
        json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        if(CURLE_COULDNT_RESOLVE_HOST == res)
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_FIRMWARE_FAILED_DOWNLOAD));
        }
        Send_Signal(upfirm->sessionId, ST_FIRMWARE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_FIRMWARE_ACTIONS);
        json_object_put(jobj);
        free(upfirm);
        VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
        VR_(execute_system)("reboot");
    }
    else
    {
        //verify checksum here
        char md5sum[SIZE_256B];
        char command[SIZE_256B];
        sprintf(command, "md5sum %s | cut -d' ' -f1", filename);
        VR_(run_command)(command, md5sum, sizeof(md5sum));

        char *fw_md5sum = (char*)VR_(read_option)(NULL, UCI_FIRMWARE_CHECKSUM);
        if(fw_md5sum)
        {
            SLOGI("fw_md5sum = %s\n", fw_md5sum);
            SLOGI("md5sum = %s\n", md5sum);
            if(strcmp(md5sum, fw_md5sum))
            {
                // VR_(execute_system)("ubus send alljoyn '{\"state\":\"wrong_checksum\"}' &");
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
                json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_FIRMWARE_WRONG_CHECKSUM));
                Send_Signal(upfirm->sessionId, ST_FIRMWARE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_FIRMWARE_ACTIONS);
                json_object_put(jobj);
                free(upfirm);
                VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
                VR_(execute_system)("reboot");
            }
        }
        else
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_FIRMWARE_MISSING_CHECKSUM));
            Send_Signal(upfirm->sessionId, ST_FIRMWARE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_FIRMWARE_ACTIONS);
            json_object_put(jobj);
            free(upfirm);
            VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
            VR_(execute_system)("reboot");
        }

        char *current_version = VR_(read_option)(NULL, UCI_FIRMWARE_CURRENT_VER);
        if(!current_version || !strlen(current_version))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_FIRMWARE_MISSING_VERSION));
            Send_Signal(upfirm->sessionId, ST_FIRMWARE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_FIRMWARE_ACTIONS);
            json_object_put(jobj);
            free(upfirm);
            VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
            VR_(execute_system)("reboot");
        }

        sprintf(command, "firmware_update.sh %s", filename);
        SLOGI("command = %s\n", command);
        FILE *fp;
        fp = popen(command,"r");
        char buff[255];
        while(fgets(buff, 255, fp)!=NULL)
        {
            printf("buff = %s\n", buff);
            usleep(10000);
        }
        pclose(fp);

        if(!strncasecmp(buff, ST_UPDATE_SUCCESS, strlen(ST_UPDATE_SUCCESS)))
        {
            VR_(execute_system)("ubus send alljoyn '{\"state\":\"firmware_updated\"}' &");
            sleep(5);
            json_object *jobj = json_object_new_object();

            VR_(write_option)(NULL, UCI_FIRMWARE_PRE_VER"=%s", current_version);
            free(current_version);

            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            json_object_object_add(jobj, ST_STATE, json_object_new_string("update done, restarting system"));

            Send_Signal(upfirm->sessionId, ST_FIRMWARE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_FIRMWARE_ACTIONS);
            json_object_put(jobj);
            free(upfirm);
            VR_(execute_system)("reboot");
        }
        else if(!strncasecmp(buff,ST_WRONG_URL, strlen(ST_WRONG_URL)))
        {
            // VR_(led_show)();
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_WRONG_URL));
            Send_Signal(upfirm->sessionId, ST_FIRMWARE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_FIRMWARE_ACTIONS);
            json_object_put(jobj);
            free(upfirm);
            VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
            VR_(execute_system)("reboot");
        }
        else if(!strncasecmp(buff,ST_UNTAR_FAILED, strlen(ST_UNTAR_FAILED)))
        {
            // VR_(led_show)();
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_UNTAR_FAILED));
            Send_Signal(upfirm->sessionId, ST_FIRMWARE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_FIRMWARE_ACTIONS);
            json_object_put(jobj);
            free(upfirm);
            VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
            VR_(execute_system)("reboot");
        }
    }   
}

void update_supported_database(void *data)
{
    CURLcode res;
    int *sessionid = (int*)(data);

    const char *filename = "/tmp/databases.tar.gz";
    char *link_supported_database = (char*)VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_DOWNLOAD_URL);

    SLOGI("link_supported_database: %s\n", link_supported_database);
    struct myprogress myp;
    myp.id = *sessionid;
    myp.action = ST_UPDATE_SUPPORT_DATABASE;

    res = VR_(download_file)(link_supported_database, filename, NULL, older_progress, (void*)&myp);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
    json_object_object_add(jobj, ST_ACTION, json_object_new_string(ST_UPDATE_SUPPORT_DATABASE));

    if(CURLE_OK != res)
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_int(res));
    }
    else
    {
        char md5sum[SIZE_256B];
        char command[SIZE_256B];
        sprintf(command, "md5sum %s | cut -d' ' -f1", filename);
        VR_(run_command)(command, md5sum, sizeof(md5sum));

        char *fw_md5sum = (char*)VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_CHECKSUM);

        if(fw_md5sum)
        {
            SLOGI("fw_md5sum = %s\n", fw_md5sum);
            SLOGI("md5sum = %s\n", md5sum);
            if(strcmp(md5sum, fw_md5sum))
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("wrong checksum"));
            }
            else
            {
                VR_(execute_system)("tar -xvzf /tmp/databases.tar.gz -C /tmp/");
                VR_(execute_system)("dd if=/tmp/supported_devices.db of=/etc/supported_devices.db");
                VR_(execute_system)("rm -rf /etc/sound; mv /tmp/sound/ /etc/");
                char *latest_version = VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_LATEST_VER);
                if(latest_version && strlen(latest_version))
                {
                    VR_(execute_system)("ubus send %s '{\"version\":\"%s\"}'", ST_UPDATE_SUPPORTED_DB, latest_version);
                    json_object_object_add(jobj, ST_VERSION, json_object_new_string(latest_version));

                    VR_(write_option)(NULL, UCI_FIRMWARE_DATABASE_CURRENT_VER"=%s", latest_version);
                    free(latest_version);
                }
                else
                {
                    VR_(execute_system)("ubus send %s '{\"version\":\"%s\"}'", ST_UPDATE_SUPPORTED_DB, ST_UNKNOWN);
                }
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                json_object_object_add(jobj, ST_STATE, json_object_new_string("update done"));
            }
        }
        else
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_int(res));
        }        
    }

    Send_Signal(*sessionid, ST_FIRMWARE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_FIRMWARE_ACTIONS);
    json_object_put(jobj);
    SAFE_FREE(sessionid);
    SAFE_FREE(link_supported_database);
}

void read_more_version_info(char *filename, json_object *version_info)
{
    if(!filename || !version_info)
    {
        return;
    }

    int i = 0;
    FILE *fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    fp = fopen(filename, "r");
    if(!fp)
    {
        return;
    }
    while ((read = getline(&line, &len, fp)) != -1)
    {
        char *pos;
        if ((pos=strchr(line, '\n')) != NULL)
            *pos = '\0';

        switch(i)
        {
            case 0:
                json_object_object_add(version_info, ST_VERSION, json_object_new_string(line));
                break;

            case 1:
                json_object_object_add(version_info, ST_BRANCH, json_object_new_string(line));
                break;

            case 2:
                json_object_object_add(version_info, ST_BUILD_DATE, json_object_new_string(line));
                break;

            case 3:
                json_object_object_add(version_info, ST_UPDATE_DATE, json_object_new_string(line));
                break;
        }
        i++;
    }
    if(line)
        free(line);
    fclose(fp);
}

void firmwareactions_signal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    char *action, *version;
    QStatus status;

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &action);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: action\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &version);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: version\n");
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);
    SLOGI("action = %s", action);
    SLOGI("version = %s", version);
    if(action && strlen(action))
    {
        if(!strcasecmp(action, ST_UPDATE))
        {
            VR_(execute_system)("ubus send alljoyn '{\"state\":\"firmware_updating\"}' &");

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            json_object_object_add(jobj, ST_STATE, json_object_new_string(ST_UPDATING));

            Send_Signal(sessionId, ST_FIRMWARE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_FIRMWARE_ACTIONS);
            json_object_put(jobj);

            update_firmware_d *data = (update_firmware_d *)malloc(sizeof(update_firmware_d));
            memset(data, 0x00, sizeof(update_firmware_d));

            char *url = (char*)VR_(read_option)(NULL, UCI_FIRMWARE_DOWNLOAD_URL);
            if(url)
            {
                strcpy(data->URL, url);
                free(url);
            }

            data->sessionId = sessionId;
            pthread_t upfirmware_thread;
            pthread_create(&upfirmware_thread, NULL, (void *)&updatefirmware, data);
            pthread_detach(upfirmware_thread);
        }
        else if(!strcasecmp(action, ST_GET_LATEST))
        {
            int res;
            char address[SIZE_512B], headerIn[SIZE_512B]={0};
            
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            
            char *firmware_address = (char*)VR_(read_option)(NULL,UCI_CLOUD_ADDRESS);
            char *firmware_current_version = VR_(read_option)(NULL, UCI_FIRMWARE_CURRENT_VER);
            char *hubUUID = (char*)VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
            char firmware_version[SIZE_256B];
            firmware_info_t firmware_data;
            // char device_name[SIZE_32B];
            // char device_rev[SIZE_32B];

            // VR_(run_command)(GET_BOARD_NAME_COMMAND, device_name, sizeof(device_name));
            // VR_(run_command)(GET_BOARD_REV_COMMAND, device_rev, sizeof(device_rev));

            get_data_firmware(PHD_BOARD, HW_REV_E, &firmware_data);

            if(firmware_current_version)
            {
                if(strstr(firmware_current_version, "Beta"))
                {
                    strcpy(firmware_version, "0.0.0");
                }
                else
                {
                    strcpy(firmware_version, firmware_current_version);
                }
            }
            else
            {
                strcpy(firmware_version, "0.0.0");
            }

            if(!firmware_address || 
                (firmware_address && !strcmp(firmware_address, PRODUCT_FIRMWARE_ADDR))
                )
            {
                sprintf(address, "%s"IS_UPDATE_LINK, FIRMWARE_ADDRESS, 
                                                      firmware_data.product_name, 
                                                      firmware_version,
                                                      hubUUID);
                snprintf(headerIn, sizeof(headerIn), "authorization: %s", firmware_data.token_product);
            }
            else
            {
                sprintf(address, "%s"IS_UPDATE_LINK, FIRMWARE_ADDRESS, 
                                                      firmware_data.dev_name, 
                                                      firmware_version,
                                                      hubUUID);
                snprintf(headerIn, sizeof(headerIn), "authorization: %s", firmware_data.token_dev);
            }

            char *result = NULL;
            int is_update = 0;
            res = VR_(get_request)(address, headerIn, NULL, NULL, &result, DEFAULT_TIMEOUT);
            if(!res)
            {
                SLOGI("result = %s\n", result);
                json_object *check_update_result = VR_(create_json_object)(result);
                if(check_update_result)
                {
                    json_object *error = json_object_object_get(check_update_result, "error");
                    if(error)
                    {
                        const char *message = json_object_get_string(json_object_object_get(error, "message"));
                        SLOGE("Checking update Error: %s\n", message);
                        json_object_object_add(jobj, ST_LATEST_VER, json_object_new_string(firmware_current_version));
                        json_object_object_add(jobj, ST_UPDATE, json_object_new_boolean(is_update));
                    }
                    else
                    {
                        is_update = json_object_get_boolean(json_object_object_get(check_update_result, "fwUpdate"));
                        const char *latestVersion = json_object_get_string(json_object_object_get(check_update_result, "version"));
                        json_object_object_add(jobj, ST_LATEST_VER, json_object_new_string(latestVersion));
                        json_object_object_add(jobj, ST_UPDATE, json_object_new_boolean(is_update));
                        if(is_update)
                        {
                            const char *fw_checksum = json_object_get_string(json_object_object_get(check_update_result, "fwChecksum"));
                            const char *fw_url = json_object_get_string(json_object_object_get(check_update_result, "fwUrl"));
                            VR_(write_option)(NULL, UCI_FIRMWARE_LATEST_VER"=%s", latestVersion);
                            VR_(write_option)(NULL, UCI_FIRMWARE_CHECKSUM"=%s", fw_checksum);
                            VR_(write_option)(NULL, UCI_FIRMWARE_DOWNLOAD_URL"=%s", fw_url);
                        }
                    }
                    json_object_put(check_update_result);
                }
                else
                {
                    SLOGI("failed to parser check update json format\n");
                    json_object_object_add(jobj, ST_LATEST_VER, json_object_new_string(firmware_current_version));
                    json_object_object_add(jobj, ST_UPDATE, json_object_new_boolean(is_update));
                }
            }
            else
            {
                json_object_object_add(jobj, ST_LATEST_VER, json_object_new_string(firmware_current_version));
                json_object_object_add(jobj, ST_UPDATE, json_object_new_boolean(is_update));
            }
            free(result);

            SAFE_FREE(firmware_current_version);
            SAFE_FREE(firmware_address);
            SAFE_FREE(hubUUID);

            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            Send_Signal(sessionId, ST_FIRMWARE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_FIRMWARE_ACTIONS);
            json_object_put(jobj);
        }
        else if(!strcasecmp(action, ST_GET_CURRENT))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
            
            char *ap_disabled = VR_(read_option)(NULL, UCI_WIFI_AP_MODE_DISABLED);
            char *firmware_current_version = VR_(read_option)(NULL, UCI_FIRMWARE_CURRENT_VER);
            char *firmware_newest_version = VR_(read_option)(NULL, UCI_FIRMWARE_LATEST_VER);
            char *supported_current_version = VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_CURRENT_VER);

            if(firmware_current_version && firmware_newest_version)
            {
                int res = VR_(version_cmp)(firmware_current_version, firmware_newest_version);
                if(res < 0)
                {
                    json_object_object_add(jobj, ST_UPDATE, json_object_new_boolean(true));
                }
                else
                {
                    json_object_object_add(jobj, ST_UPDATE, json_object_new_boolean(false));
                }
            }
            else
            {
                json_object_object_add(jobj, ST_UPDATE, json_object_new_boolean(false));
            }

            JSON_RETURN(jobj, ST_CURRENT_VER, firmware_current_version);
            JSON_RETURN(jobj, ST_LATEST_VER, firmware_newest_version);
            JSON_RETURN(jobj, ST_SP_CURRENT_VER, supported_current_version);

            json_object *apmode = json_object_new_object();

            char *apssid = VR_(read_option)(NULL, UCI_HUB_DEFAULT_NAME);
            char *apencryption = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_APENCRYPTION);
            JSON_RETURN(apmode, ST_SSID, apssid);
            JSON_RETURN(apmode, ST_ENCRYPTION, apencryption);

            if(ap_disabled)
            {
                if(!strcmp(ap_disabled, "0"))
                {
                    json_object_object_add(apmode, ST_STATE, json_object_new_string("active"));
                }
                else if(!strcmp(ap_disabled, "1"))
                {
                    json_object_object_add(apmode, ST_STATE, json_object_new_string("inactive"));
                }
            }
            else
            {
                json_object_object_add(apmode, ST_STATE, json_object_new_string("unknown"));
            }

            json_object *stamode = json_object_new_object();

            char *ssid = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_SSID);
            char *encryption = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_ENCRYPTION);
            JSON_RETURN(stamode, ST_SSID, ssid);
            JSON_RETURN(stamode, ST_ENCRYPTION, encryption);

            if(ap_disabled)
            {
                if(!strcmp(ap_disabled, "0"))
                {
                    char *state = VR_(read_option)(NULL, UCI_LOCAL_STATE);
                    char *sta_disabled = VR_(read_option)(NULL, UCI_WIFI_STA_DISABLED);
                    if(!state || !sta_disabled)
                    {
                        json_object_object_add(stamode, ST_STATE, json_object_new_string("inactive"));
                    }
                    else if(!strcmp(state, "available")) /*builder mode*/
                    {
                        if(!strcmp(sta_disabled, "0"))/*sta mode enable*/
                        {
                            char sta_result[SIZE_64B];
                            VR_(run_command)(GET_STA_STATUS_COMMAND, sta_result, sizeof(sta_result));
                            if(strcmp(sta_result, "COMPLETED"))
                            {
                                json_object_object_add(stamode, ST_STATE, json_object_new_string("inactive"));
                            }
                            else
                            {
                                json_object_object_add(stamode, ST_STATE, json_object_new_string("active"));
                                json_object_object_add(apmode, ST_STATE, json_object_new_string("inactive"));
                            }
                        }
                        else
                        {
                            json_object_object_add(stamode, ST_STATE, json_object_new_string("inactive"));
                        }
                    }
                    else
                    {
                        json_object_object_add(stamode, ST_STATE, json_object_new_string("inactive"));
                    }

                    SAFE_FREE(state);
                    SAFE_FREE(sta_disabled);
                }
                else if(!strcmp(ap_disabled, "1"))
                {
                    json_object_object_add(stamode, ST_STATE, json_object_new_string("active"));
                }
            }
            else
            {
                json_object_object_add(stamode, ST_STATE, json_object_new_string("unknown"));
            }

            json_object_object_add(jobj, ST_AP_MODE, apmode);
            json_object_object_add(jobj, ST_STA_MODE, stamode);

            json_object *version_info = json_object_new_object();
            read_more_version_info("/etc/venus_version", version_info);
            json_object_object_add(jobj, ST_VER_INFO, version_info);

            SAFE_FREE(ap_disabled);

            char temperature[SIZE_32B];
            VR_(run_command)(GET_TEMP_COMMAND, temperature, sizeof(temperature));
            json_object_object_add(jobj, ST_TEMP, json_object_new_string(temperature));

            char ipAddress[SIZE_32B];
            VR_(run_command)(GET_IP_ADDRESS_COMMAND, ipAddress, sizeof(ipAddress));
            json_object_object_add(jobj, ST_IP_ADDRESS, json_object_new_string(ipAddress));

            char macAddress[SIZE_32B];
            VR_(run_command)(GET_MAC_ADDRESS_COMMAND, macAddress, sizeof(macAddress));
            json_object_object_add(jobj, ST_MAC_ADDRESS, json_object_new_string(macAddress));

            char *bssid = VR_(read_option)(NULL, UCI_WIFI_AP_MAC_ADDRESS);
            JSON_RETURN(jobj, ST_BSSID, bssid);

            char *timezone = VR_(read_option)(NULL, UCI_SYSTEM_TIMEZONE);
            JSON_RETURN(jobj, ST_TIME_ZONE, timezone);

            int current_volume = VR_(get_volume)();
            json_object_object_add(jobj, ST_VOLUME, json_object_new_int(current_volume));

            unsigned int led_current = get_led_current();
            json_object_object_add(jobj, ST_LED_CURRENT, json_object_new_int(led_current));

            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            Send_Signal(sessionId, ST_FIRMWARE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_FIRMWARE_ACTIONS);
            json_object_put(jobj);
        }
        else if(!strcasecmp(action, ST_UPDATE_SUPPORT_DATABASE))
        {
            int res;
            char address[SIZE_512B], headerIn[SIZE_512B]={0};

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));

            char *firmware_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
            char *firmware_current_version = VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_CURRENT_VER);
            char *hubUUID = (char*)VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
            char firmware_version[SIZE_256B];

            if(firmware_current_version)
            {
                if(strlen(firmware_current_version) < 5)
                {
                    strcpy(firmware_version, "0.0.0");
                }
                else
                {
                    strcpy(firmware_version, firmware_current_version);
                }
            }
            else
            {
                strcpy(firmware_version, "0.0.0");
            }

            if(!firmware_address || 
                (firmware_address && !strcmp(firmware_address, PRODUCT_FIRMWARE_ADDR))
                )
            {
                char *requestId = VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_ID);
                char *token = VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_TOKEN);
                if(!requestId)
                {
                    requestId = strdup(DEFAULT_DATABASE_ID);
                }

                if(!token)
                {
                    token = strdup(DEFAULT_DATABASE_TOKEN);
                }
                sprintf(address, "%s"IS_UPDATE_LINK, FIRMWARE_ADDRESS, 
                                                      requestId,
                                                      firmware_version,
                                                      hubUUID);
                snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);

                free(requestId);
                free(token);
            }
            else
            {
                char *requestId = VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_ID_DEV);
                char *token = VR_(read_option)(NULL, UCI_FIRMWARE_DATABASE_TOKEN_DEV);
                if(!requestId)
                {
                    requestId = strdup(DEFAULT_DATABASE_ID);
                }

                if(!token)
                {
                    token = strdup(DEFAULT_DATABASE_TOKEN);
                }

                sprintf(address, "%s"IS_UPDATE_LINK, FIRMWARE_ADDRESS, 
                                                      requestId,
                                                      firmware_version,
                                                      hubUUID);
                snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);
                free(requestId);
                free(token);
            }
            char *result = NULL;
            res = VR_(get_request)(address, headerIn, NULL, NULL, &result, DEFAULT_TIMEOUT);
            if(!res)
            {
                SLOGI("result = %s\n", result);
                json_object *check_update_result = VR_(create_json_object)(result);
                if(check_update_result)
                {
                    json_object *error = json_object_object_get(check_update_result, "error");
                    if(error)
                    {
                        const char *message = json_object_get_string(json_object_object_get(error, "message"));
                        SLOGE("Checking update Error: %s\n", message);
                        json_object_object_add(jobj, ST_STATE, json_object_new_string("update done"));
                    }
                    else
                    {
                        int is_update = json_object_get_boolean(json_object_object_get(check_update_result, "fwUpdate"));
                        const char *latest_version = json_object_get_string(json_object_object_get(check_update_result, "version"));

                        if(is_update)
                        {
                            json_object_object_add(jobj, ST_STATE, json_object_new_string(ST_UPDATING));

                            const char *fw_checksum = json_object_get_string(json_object_object_get(check_update_result, "fwChecksum"));
                            const char *fw_url = json_object_get_string(json_object_object_get(check_update_result, "fwUrl"));
                            VR_(write_option)(NULL, UCI_FIRMWARE_DATABASE_LATEST_VER"=%s", latest_version);
                            VR_(write_option)(NULL, UCI_FIRMWARE_DATABASE_CHECKSUM"=%s", fw_checksum);
                            VR_(write_option)(NULL, UCI_FIRMWARE_DATABASE_DOWNLOAD_URL"=%s", fw_url);

                            int *id = (int *)malloc(sizeof(int));
                            *id = sessionId;
                            pthread_t update_supported_database_thread;
                            pthread_create(&update_supported_database_thread, NULL, (void *)&update_supported_database, id);
                            pthread_detach(update_supported_database_thread);
                        }
                        else
                        {
                            json_object_object_add(jobj, ST_STATE, json_object_new_string("update done"));
                        }
                    }
                    json_object_put(check_update_result);
                }
                else
                {
                    SLOGI("failed to parser check update json format\n");
                    json_object_object_add(jobj, ST_STATE, json_object_new_string("update done"));
                }
            }
            else
            {
                json_object_object_add(jobj, ST_STATE, json_object_new_string("update done"));
            }
            free(result);

            SAFE_FREE(firmware_current_version);
            SAFE_FREE(firmware_address);
            SAFE_FREE(hubUUID);

            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            Send_Signal(sessionId, ST_FIRMWARE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_FIRMWARE_ACTIONS);
            json_object_put(jobj);
        }
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_FIRMWARE_ACTIONS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("missing argument"));
        Send_Signal(sessionId, ST_FIRMWARE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_FIRMWARE_ACTIONS);
        json_object_put(jobj);
    }
}

/* AcceptSessionJoiner callback */
QCC_BOOL AJ_CALL accept_session_joiner(const void* context, alljoyn_sessionport sessionPort,
                                       const char* joiner,  const alljoyn_sessionopts opts)
{
    QCC_BOOL ret = QCC_FALSE;
    if (sessionPort != SERVICE_PORT) {
        SLOGI("Rejecting join attempt on unexpected session port %d\n", sessionPort);
    } else {
        SLOGI("Accepting join session request from %s (opts.proximity=%x, opts.traffic=%x, opts.transports=%x)\n",
               joiner, alljoyn_sessionopts_get_proximity(opts), alljoyn_sessionopts_get_traffic(opts), alljoyn_sessionopts_get_transports(opts));
        ret = QCC_TRUE;
    }
    return ret;
}

void rulemanual_signal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    char *rule_id, *actions;
    QStatus status;

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &rule_id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: rule_id\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &actions);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: actions\n");
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);
    SLOGI("rule_id = %s\n", rule_id);
    SLOGI("actions = %s\n", actions);

    if(rule_id && strlen(rule_id))
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RULE_MANUAL_R));
        json_object_object_add(jobj, ST_RULE_ID, json_object_new_string(rule_id));

        SEARCH_DATA_INIT_VAR(dbActions);
        searching_database("alljoyn_handler", rule_db, get_last_data_cb, 
                        &dbActions, "SELECT actions from RULES where id='%s'", rule_id);
        if(dbActions.len)
        {
            /*call trigger_action local to make command sequence 
            to prevent user press to fast and status is not correct*/
            rule_report_timeline(rule_id);
            _trigger_actions_(rule_id, dbActions.value);
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
        }
        else
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("rule not found"));
        }

        FREE_SEARCH_DATA_VAR(dbActions);

        Send_Signal(sessionId, ST_RULE_MANUAL, (char *)json_object_to_json_string(jobj), NUM_ARGS_RULE_MANUAL);
        json_object_put(jobj);
    }
    else if(actions && strlen(actions))
    {
        size_t action_length = strlen(actions);
        char *command = (char *)malloc(action_length+SIZE_512B);
        memset(command, 0x00, action_length+SIZE_512B);
        sprintf(command, "%s", "/etc/rule_control.sh \"dontcare\" \"dontcare\" ");

        char *ch, *save_tok;
        ch = strtok_r(actions, "&&", &save_tok);
        while(ch != NULL)
        {
            printf("ch = %s\n", ch);
            sprintf(command + strlen(command), "%s%s%s", "\"",ch,";\" ");
            ch = strtok_r(NULL, "&&", &save_tok);
        }
        sprintf(command+strlen(command), "%s", " &");
        SLOGI("command = %s\n", command);
        VR_(execute_system)(command);
        free(command);

        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RULE_MANUAL_R));
        json_object_object_add(jobj, ST_ACTIONS, json_object_new_string(actions));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
        Send_Signal(sessionId, ST_RULE_MANUAL, (char *)json_object_to_json_string(jobj), NUM_ARGS_RULE_MANUAL);
        json_object_put(jobj);
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RULE_MANUAL_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing argument"));
        Send_Signal(sessionId, ST_RULE_MANUAL, (char *)json_object_to_json_string(jobj), NUM_ARGS_RULE_MANUAL);
        json_object_put(jobj);
    }
}

void readspec_signal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    char *service_name, *device_id, *class, *command, *data0, *data1, *data2;
    int i;
    QStatus status;

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &service_name);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: service_name\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &device_id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: device_id\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &class);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: class\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &command);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: command\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 4), "s", &data0);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data0\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 5), "s", &data1);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data1\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 6), "s", &data2);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data2\n");
    }


    SLOGI("device_id = %s\n", device_id);
    SLOGI("class = %s\n", class);
    SLOGI("command = %s\n", command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);
    SLOGI("data2 = %s\n", data2);

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    if(service_name && strlen(service_name) && device_id && strlen(device_id) 
        && class && strlen(class) && command && strlen(command))
    {
        
        new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
        memset(allubus, 0x00, sizeof(new_allubus));

        strcpy(allubus->name, ST_READ_SPEC);
        allubus->id = sessionId;
        allubus->type = ONLY_SESSION;
        allubus->numArgs = NUM_ARGS_READ_SPEC;
        add_last_list(allubus);

        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(service_name, daemon_services[i].name))
            {
                allubus->data = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {

                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, ST_ID, device_id);
                    blobmsg_add_string(&buff, ST_CLASS, class);
                    blobmsg_add_string(&buff, ST_CMD, command);
                    if(data0 && strlen(data0)) blobmsg_add_string(&buff, ST_DATA0, data0);
                    if(data1 && strlen(data1)) blobmsg_add_string(&buff, ST_DATA1, data1);
                    if(data2 && strlen(data2)) blobmsg_add_string(&buff, ST_DATA2, data2);

                    allubus->action_time = (unsigned)time(NULL);

                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    allubus->req=req;

                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_READ_SPEC, buff.head, req,
                                    receive_data_cb, complete_data_cb, (void *) allubus);
                    
                    break;
                }
            }
        }
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_READ_SPEC_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_READ_SPEC, (char *)json_object_to_json_string(jobj), NUM_ARGS_READ_SPEC);
        json_object_put(jobj);
    }
}

void readspec_CRC_signal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    char *service_name, *device_id, *class, *command, *data0, *data1, *data2;
    char *source_endpoint, *destination_enpoint, *is_CRC;
    int i;
    QStatus status;

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &service_name);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: service_name\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &device_id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: device_id\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &class);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: class\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &command);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: command\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 4), "s", &data0);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data0\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 5), "s", &data1);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data1\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 6), "s", &data2);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data2\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 7), "s", &source_endpoint);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: source_endpoint\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 8), "s", &destination_enpoint);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: destination_enpoint\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 9), "s", &is_CRC);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: is_CRC\n");
    }

    SLOGI("device_id = %s\n", device_id);
    SLOGI("class = %s\n", class);
    SLOGI("command = %s\n", command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);
    SLOGI("data2 = %s\n", data2);

    SLOGI("source_endpoint = %s\n", source_endpoint);
    SLOGI("destination_enpoint = %s\n", destination_enpoint);
    SLOGI("is_CRC = %s\n", is_CRC);

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    if(service_name && strlen(service_name) && device_id && strlen(device_id) 
        && class && strlen(class) && command && strlen(command))
    {
        
        new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
        memset(allubus, 0x00, sizeof(new_allubus));

        strcpy(allubus->name, ST_READ_SPEC_CRC);
        allubus->id = sessionId;
        allubus->type = ONLY_SESSION;
        allubus->numArgs = NUM_ARGS_READ_SPEC_CRC;
        add_last_list(allubus);

        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(service_name, daemon_services[i].name))
            {
                allubus->data = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {
                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, ST_ID, device_id);
                    blobmsg_add_string(&buff, ST_CLASS, class);
                    blobmsg_add_string(&buff, ST_CMD, command);
                    if(data0 && strlen(data0)) blobmsg_add_string(&buff, ST_DATA0, data0);
                    if(data1 && strlen(data1)) blobmsg_add_string(&buff, ST_DATA1, data1);
                    if(data2 && strlen(data2)) blobmsg_add_string(&buff, ST_DATA2, data2);

                    if(source_endpoint && strlen(source_endpoint)) 
                        blobmsg_add_string(&buff, ST_SRC_ENDPOINT, source_endpoint);
                    if(destination_enpoint && strlen(destination_enpoint)) 
                        blobmsg_add_string(&buff, ST_DES_ENDPOINT, destination_enpoint);
                    if(is_CRC && strlen(is_CRC)) blobmsg_add_string(&buff, ST_IS_CRC, is_CRC);

                    allubus->action_time = (unsigned)time(NULL);

                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    allubus->req=req;

                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_READ_SPEC_CRC, buff.head, req,
                                    receive_data_cb, complete_data_cb, (void *) allubus);
                    
                    break;
                }
            }
        }
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_READ_SPEC_CRC_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_READ_SPEC_CRC, (char *)json_object_to_json_string(jobj), NUM_ARGS_READ_SPEC_CRC);
        json_object_put(jobj);
    }
}

void writespec_signal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("in writespec_signal_handle \n");
    int i;
    QStatus status;
    char *service_name, *device_id, *class, *command, *data0, *data1, *data2;

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &service_name);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: service_name\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &device_id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: device_id\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &class);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: class\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &command);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: command\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 4), "s", &data0);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data0\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 5), "s", &data1);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data1\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 6), "s", &data2);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data2\n");
    }

    SLOGI("device_id = %s\n", device_id);
    SLOGI("class = %s\n", class);
    SLOGI("command = %s\n", command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    if(service_name && strlen(service_name) && device_id && strlen(device_id) 
        && class && strlen(class) && command && strlen(command))
    {
        new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
        memset(allubus, 0x00, sizeof(new_allubus));

        strcpy(allubus->name, ST_WRITE_SPEC);
        allubus->id = sessionId;
        allubus->type = ALL_SESSION;
        allubus->numArgs = NUM_ARGS_WRITE_SPEC;
        add_last_list(allubus);

        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(service_name, daemon_services[i].name))
            {
                allubus->data = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {

                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, ST_ID, device_id);
                    blobmsg_add_string(&buff, ST_CLASS, class);
                    blobmsg_add_string(&buff, ST_CMD, command);
                    if(data0 && strlen(data0)) blobmsg_add_string(&buff, ST_DATA0, data0);
                    if(data1 && strlen(data1)) blobmsg_add_string(&buff, ST_DATA1, data1);
                    if(data2 && strlen(data2)) blobmsg_add_string(&buff, ST_DATA2, data2);

                    allubus->action_time = (unsigned)time(NULL);
                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    allubus->req=req;

                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_WRITE_SPEC, buff.head, req,
                                    receive_data_cb, complete_data_cb, (void *) allubus);
                    
                    break;
                }
            }
        }
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WRITE_SPEC_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_WRITE_SPEC, (char *)json_object_to_json_string(jobj), NUM_ARGS_WRITE_SPEC);
        json_object_put(jobj);
    }
}

void writespec_CRC_signal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("in writespec_CRC_signal_handle \n");
    int i;
    QStatus status;
    char *service_name, *device_id, *class, *command, *data0, *data1, *data2;
    char *source_endpoint, *destination_enpoint, *is_CRC;

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &service_name);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: service_name\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &device_id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: device_id\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &class);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: class\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &command);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: command\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 4), "s", &data0);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data0\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 5), "s", &data1);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data1\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 6), "s", &data2);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data2\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 7), "s", &source_endpoint);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: source_endpoint\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 8), "s", &destination_enpoint);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: destination_enpoint\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 9), "s", &is_CRC);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: is_CRC\n");
    }

    SLOGI("device_id = %s\n", device_id);
    SLOGI("class = %s\n", class);
    SLOGI("command = %s\n", command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);

    SLOGI("source_endpoint = %s\n", source_endpoint);
    SLOGI("destination_enpoint = %s\n", destination_enpoint);
    SLOGI("is_CRC = %s\n", is_CRC);

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    if(service_name && strlen(service_name) && device_id && strlen(device_id) 
        && class && strlen(class) && command && strlen(command))
    {
        new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
        memset(allubus, 0x00, sizeof(new_allubus));

        strcpy(allubus->name, ST_WRITE_SPEC_CRC);
        allubus->id = sessionId;
        allubus->type = ALL_SESSION;
        allubus->numArgs = NUM_ARGS_WRITE_SPEC_CRC;
        add_last_list(allubus);

        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(service_name, daemon_services[i].name))
            {
                allubus->data = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {

                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, ST_ID, device_id);
                    blobmsg_add_string(&buff, ST_CLASS, class);
                    blobmsg_add_string(&buff, ST_CMD, command);
                    if(data0 && strlen(data0)) blobmsg_add_string(&buff, ST_DATA0, data0);
                    if(data1 && strlen(data1)) blobmsg_add_string(&buff, ST_DATA1, data1);
                    if(data2 && strlen(data2)) blobmsg_add_string(&buff, ST_DATA2, data2);

                    if(source_endpoint && strlen(source_endpoint)) 
                        blobmsg_add_string(&buff, ST_SRC_ENDPOINT, source_endpoint);
                    if(destination_enpoint && strlen(destination_enpoint)) 
                        blobmsg_add_string(&buff, ST_DES_ENDPOINT, destination_enpoint);
                    if(is_CRC && strlen(is_CRC)) blobmsg_add_string(&buff, ST_IS_CRC, is_CRC);

                    allubus->action_time = (unsigned)time(NULL);
                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    allubus->req=req;

                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_WRITE_SPEC_CRC, buff.head, req,
                                    receive_data_cb, complete_data_cb, (void *) allubus);
                    
                    break;
                }
            }
        }
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WRITE_SPEC_CRC_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_WRITE_SPEC_CRC, (char *)json_object_to_json_string(jobj), NUM_ARGS_WRITE_SPEC_CRC);
        json_object_put(jobj);
    }
}

void read_s_spec_signal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    char *service_name, *device_id, *class, *command, *data0;
    int i;
    QStatus status;

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &service_name);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: service_name\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &device_id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: device_id\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &class);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: class\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &command);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: command\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 4), "s", &data0);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data0\n");
    }

    SLOGI("device_id = %s\n", device_id);
    SLOGI("class = %s\n", class);
    SLOGI("command = %s\n", command);
    SLOGI("data0 = %s\n", data0);

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    if(service_name && strlen(service_name) && device_id && strlen(device_id) 
        && class && strlen(class) && command && strlen(command))
    {

        new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
        memset(allubus, 0x00, sizeof(new_allubus));

        strcpy(allubus->name, ST_READ_S_SPEC);
        allubus->id = sessionId;
        allubus->type = ONLY_SESSION;
        allubus->numArgs = NUM_ARGS_READ_S_SPEC;
        add_last_list(allubus);

        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(service_name, daemon_services[i].name))
            {
                allubus->data = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {

                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, ST_ID, device_id);
                    blobmsg_add_string(&buff, ST_CLASS, class);
                    blobmsg_add_string(&buff, ST_CMD, command);
                    if(data0 && strlen(data0)) blobmsg_add_string(&buff, ST_DATA0, data0);

                    allubus->action_time = (unsigned)time(NULL);
                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    allubus->req=req;

                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_READ_S_SPEC, buff.head, req,
                                    receive_data_cb, complete_data_cb, (void *) allubus);
                    
                    break;
                }
            }
        }
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_READ_S_SPEC_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_READ_S_SPEC, (char *)json_object_to_json_string(jobj), NUM_ARGS_READ_S_SPEC);
        json_object_put(jobj);
    }
    
}

void write_s_spec_signal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    char *service_name, *device_id, *class, *command, *data0, *data1, *data2;
    int i;
    QStatus status;

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &service_name);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: service_name\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &device_id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: device_id\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &class);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: class\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &command);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: command\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 4), "s", &data0);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data0\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 5), "s", &data1);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data1\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 6), "s", &data2);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data2\n");
    }

    SLOGI("device_id = %s\n", device_id);
    SLOGI("class = %s\n", class);
    SLOGI("command = %s\n", command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);
    SLOGI("data2 = %s\n", data2);

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    if(service_name && strlen(service_name) && device_id && strlen(device_id) 
        && class && strlen(class) && command && strlen(command))
    {

        new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
        memset(allubus, 0x00, sizeof(new_allubus));

        strcpy(allubus->name, ST_WRITE_S_SPEC);
        allubus->id = sessionId;
        allubus->type = ALL_SESSION;
        allubus->numArgs = NUM_ARGS_WRITE_S_SPEC;
        add_last_list(allubus);

        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(service_name, daemon_services[i].name))
            {
                allubus->data = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {

                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, ST_ID, device_id);
                    blobmsg_add_string(&buff, ST_CLASS, class);
                    blobmsg_add_string(&buff, ST_CMD, command);
                    if(data0 && strlen(data0)) blobmsg_add_string(&buff, ST_DATA0, data0);
                    if(data1 && strlen(data1)) blobmsg_add_string(&buff, ST_DATA1, data1);
                    if(data2 && strlen(data2)) blobmsg_add_string(&buff, ST_DATA2, data2);

                    allubus->action_time = (unsigned)time(NULL);
                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    allubus->req=req;

                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_WRITE_S_SPEC, buff.head, req,
                                    receive_data_cb, complete_data_cb, (void *) allubus);
                    
                    break;
                }
            }
        }
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WRITE_S_SPEC_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_WRITE_S_SPEC, (char *)json_object_to_json_string(jobj), NUM_ARGS_WRITE_S_SPEC);
        json_object_put(jobj);
    }
    
}

void resetdevice_signal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    QStatus status;
    SLOGI("reset devices_signal\n");
    int i = 0;
    char* adminId, *type;

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &adminId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: adminId\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &type);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: type\n");
    }

    SLOGI("adminId = %s\n", adminId);
    SLOGI("type = %s\n", type);
    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    if(type && strlen(type))
    {
        if(strlen(type)) //reset service
        {
            if(!strcmp(type, ST_UPNP))
                //|| !strcmp(type, "zwave") || !strcmp(type, "zigbee"))
            {
                new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
                memset(allubus, 0x00, sizeof(new_allubus));

                strcpy(allubus->name, ST_RESET);
                SLOGI("sessionId = %u\n", sessionId);
                allubus->id = sessionId;
                allubus->type = ONLY_SESSION;
                allubus->numArgs = NUM_ARGS_RESET;
                add_last_list(allubus);

                for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
                {
                    if(!strcmp(type, daemon_services[i].name))
                    {
                        allubus->data = (void*)daemon_services[i].name;
                        if(daemon_services[i].id != 0)
                        {
                            allubus->action_time = (unsigned)time(NULL);

                            struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                            allubus->req=req;

                            VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_RESET, NULL, req,
                                    receive_data_cb, complete_data_cb, (void *) allubus);
                            
                            break;
                        }
                    }
                }
            }
            else
            {
                SEARCH_DATA_INIT_VAR(usertype);
                searching_database("alljoyn_handler", user_db, get_last_data_cb, &usertype, 
                            "SELECT userType from USERS where userId='%s'", adminId);

                if(!strcmp(usertype.value, ST_ADMIN))
                {
                    if(!strcmp(type, ST_HARD))
                    {
                        SLOGI("factory reset\n");
                        VR_(execute_system)("rm -rf /overlay/*");
                        json_object *jobj = json_object_new_object();
                        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RESET_R));
                        json_object_object_add(jobj, ST_RESET_TYPE, json_object_new_string(type));
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                        Send_Signal(sessionId, ST_RESET, (char *)json_object_to_json_string(jobj), NUM_ARGS_RESET);
                        json_object_put(jobj);
                        VR_(execute_system)("reboot");
                    }
                    else if(!strcmp(type, ST_REBOOT))
                    {
                        SLOGI("reboot\n");
                        json_object *jobj = json_object_new_object();
                        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RESET_R));
                        json_object_object_add(jobj, ST_RESET_TYPE, json_object_new_string(type));
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                        Send_Signal(sessionId, ST_RESET, (char *)json_object_to_json_string(jobj), NUM_ARGS_RESET);
                        json_object_put(jobj);
                        VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
                        VR_(execute_system)("reboot");
                    }
                    else if(!strcmp(type, ST_WIFI_SETTINGS))
                    {
                        SLOGI("wifi_settings\n");
                        VR_(execute_system)("rm -rf /overlay/etc/config/alljoyn-onboarding");
                        VR_(execute_system)("rm -rf /overlay/etc/config/wireless");
                        VR_(execute_system)("rm -rf /overlay/etc/alljoyn-onboarding");
                        json_object *jobj = json_object_new_object();
                        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RESET_R));
                        json_object_object_add(jobj, ST_RESET_TYPE, json_object_new_string(type));
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                        Send_Signal(sessionId, ST_RESET, (char *)json_object_to_json_string(jobj), NUM_ARGS_RESET);
                        json_object_put(jobj);
                        VR_(write_option)(NULL, UCI_FIRMWARE_POWER_CYCLE"=0");
                        VR_(execute_system)("reboot");
                    }
                }
                else
                {
                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RESET_R));
                    json_object_object_add(jobj, ST_RESET_TYPE, json_object_new_string(type));
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    json_object_object_add(jobj, ST_REASON, json_object_new_string("you are not allowed to do this"));
                    Send_Signal(sessionId, ST_RESET, (char *)json_object_to_json_string(jobj), NUM_ARGS_RESET);
                    json_object_put(jobj);
                }
                FREE_SEARCH_DATA_VAR(usertype);
            }
        }
    }
}

void removedevice_signal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    QStatus status;
    SLOGI("remove devices_signal\n");
    int i = 0;
    char *type, *device_id, *subdevID;

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &type);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: type\n");
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &device_id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: device id\n");
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &subdevID);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: subdevID\n");
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    if(type && strlen(type))
    {
        SLOGI("type = %s\n", type);
        
        new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
        memset(allubus, 0x00, sizeof(new_allubus));

        strcpy(allubus->name, ST_REMOVE_DEVICE);
        SLOGI("sessionId = %u\n", sessionId);
        allubus->id = sessionId;
        allubus->type = ALL_SESSION;
        allubus->numArgs = NUM_ARGS_REMOVE_DEVICE;
        add_last_list(allubus);

        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(type, daemon_services[i].name))
            {
                allubus->data = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {
                    blob_buf_init(&buff, 0);
                    if(device_id && strlen(device_id))
                    {
                        blobmsg_add_string(&buff, ST_ID, device_id);
                    }

                    if(subdevID && strlen(subdevID))
                    {
                        blobmsg_add_string(&buff, ST_SUBDEVID, subdevID);
                    }

                    allubus->action_time = (unsigned)time(NULL);
                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    allubus->req=req;

                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_REMOVE_DEVICE, buff.head, req,
                                    receive_data_cb, complete_data_cb, (void *) allubus);
                    
                    break;
                }
            }
        }
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_REMOVE_DEVICE_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_REMOVE_DEVICE, (char *)json_object_to_json_string(jobj), NUM_ARGS_REMOVE_DEVICE);
        json_object_put(jobj);
    }
}

void open_close_network(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    QStatus status;
    SLOGI("open_close_network devices_signal\n");
    int i = 0;
    char *type, *device_id, *action;

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &type);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: type\n");
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &device_id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: device id\n");
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &action);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: action\n");
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    if(type && strlen(type) && action && strlen(action))
    {
        if(!strcmp(type, ST_ALL))
        {
            // if(!strcasecmp(action,"open"))
            // {
            //     VR_(execute_system)("ubus send alljoyn '{\"state\":\"open_network\"}' &");
            // }
            // else if(!strcasecmp(action,"close"))
            // {
            //     VR_(execute_system)("ubus send alljoyn '{\"state\":\"close_network\"}' &");
            // }

            for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
            {
                if((!strcmp(ST_ZIGBEE, daemon_services[i].name) || !strcmp(ST_ZWAVE, daemon_services[i].name)) 
                    && daemon_services[i].id != 0)
                {
                    new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
                    memset(allubus, 0x00, sizeof(new_allubus));

                    strcpy(allubus->name, ST_OPEN_CLOSE_NETWORK);
                    allubus->id = sessionId;
                    allubus->type = ONLY_SESSION;
                    allubus->numArgs = NUM_ARGS_OPEN_CLOSE_NETWORK;
                    allubus->data = (void*)daemon_services[i].name;
                    allubus->action_time = (unsigned)time(NULL);
                    add_last_list(allubus);

                    blob_buf_init(&buff, 0);

                    struct ubus_request *req_s = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    allubus->req=req_s;

                    if(!strcasecmp(action,ST_OPEN))
                    {
                        VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_OPEN_NETWORK, buff.head, req_s,
                                    receive_data_cb, complete_data_cb, (void *) allubus);
                    }
                    else if(!strcasecmp(action,ST_CLOSE))
                    {
                        VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_CLOSE_NETWORK, buff.head, req_s,
                                    receive_data_cb, complete_data_cb, (void *) allubus);
                    }
                }
            }
        }
        else
        {
            SLOGI("type = %s\n", type);
            new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
            memset(allubus, 0x00, sizeof(new_allubus));
            strcpy(allubus->name, ST_OPEN_CLOSE_NETWORK);
            SLOGI("sessionId = %u\n", sessionId);
            allubus->id = sessionId;
            allubus->type = ONLY_SESSION;
            allubus->numArgs = NUM_ARGS_OPEN_CLOSE_NETWORK;
            add_last_list(allubus);

            for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
            {
                if(!strcmp(type, daemon_services[i].name))
                {
                    allubus->data = (void*)daemon_services[i].name;
                    if(daemon_services[i].id != 0)
                    {
                        allubus->action_time = (unsigned)time(NULL);

                        blob_buf_init(&buff, 0);
                        if(!strcmp(type, ST_UPNP) && strlen(device_id))
                        { 
                            blobmsg_add_string(&buff, ST_ID, device_id);
                        }

                        struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                        allubus->req=req;
                        
                        if(!strcasecmp(action,ST_OPEN))
                        {
                            VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_OPEN_NETWORK, buff.head, req,
                                        receive_data_cb, complete_data_cb, (void *) allubus);
                        }
                        else if(!strcasecmp(action,ST_CLOSE))
                        {
                            VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_CLOSE_NETWORK, buff.head, req,
                                        receive_data_cb, complete_data_cb, (void *) allubus);
                        }
                        
                        break;
                    }
                }
            }
        }
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_OPEN_CLOSE_NETWORK_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_OPEN_CLOSE_NETWORK, (char *)json_object_to_json_string(jobj), NUM_ARGS_OPEN_CLOSE_NETWORK);
        json_object_put(jobj);
    }
}

void get_sub_devs(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    QStatus status;
    SLOGI("get_sub_devs devices_signal\n");
    int i = 0;
    char *type, *device_id, *listtype;

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &type);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: type\n");
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &device_id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: device id\n");
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &listtype);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: listtype\n");
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    if(type && strlen(type) && device_id && strlen(device_id) && listtype && strlen(listtype))
    {
        SLOGI("type = %s\n", type);
        
        new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
        memset(allubus, 0x00, sizeof(new_allubus));

        strcpy(allubus->name, ST_GET_SUB_DEVS);
        SLOGI("sessionId = %u\n", sessionId);
        allubus->id = sessionId;
        allubus->type = ONLY_SESSION;
        allubus->numArgs = NUM_ARGS_GET_SUB_DEVICES;
        add_last_list(allubus);

        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(type, daemon_services[i].name))
            {
                allubus->data = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {
                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, ST_ID, device_id);
                    blobmsg_add_string(&buff, ST_LIST_TYPE, listtype);

                    allubus->action_time = (unsigned)time(NULL);
                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    allubus->req=req;

                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_GET_SUB_DEVS, buff.head, req,
                                        receive_data_cb, complete_data_cb, (void *) allubus); 
                    break;
                }
            }   
        }
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_SUB_DEVS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_GET_SUB_DEVS, (char *)json_object_to_json_string(jobj), NUM_ARGS_GET_SUB_DEVICES);
        json_object_put(jobj);
    }
}

void identify_devs(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("identify_devs \n");
    QStatus status;
    int i;
    char *servicename, *id, *subdevID, *value;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &servicename);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: servicename\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: id\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &value);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: value\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &subdevID);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: subdevID\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    SLOGI("servicename = %s\n", servicename);
    SLOGI("id = %s\n", id);
    SLOGI("value = %s\n", value);
    SLOGI("subdevID = %s\n", subdevID);
    SLOGI("sessionId = %u\n", sessionId);

    new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
    memset(allubus, 0x00, sizeof(new_allubus));
    strcpy(allubus->name, ST_IDENTIFY);
    allubus->id = sessionId;
    allubus->type = ALL_SESSION;
    allubus->numArgs = NUM_ARGS_IDENTYFY;
    add_last_list(allubus);

    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(servicename, daemon_services[i].name))
        {
            allubus->data = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                if(value && strlen(value)) 
                {
                    blobmsg_add_string(&buff, ST_VALUE, value);
                }
                
                if(subdevID && strlen(subdevID)) 
                {
                    if(!strcmp(servicename, ST_UPNP))
                    {
                        blobmsg_add_string(&buff, ST_SUBDEVID, subdevID);
                    }
                }

                allubus->action_time = (unsigned)time(NULL);
                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                allubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_IDENTIFY, buff.head, req,
                                        receive_data_cb, complete_data_cb, (void *) allubus); 
                
                break;
            }
        }
    }
}

void change_name(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("change_name \n");
    QStatus status;
    int i;
    char *servicename, *id, *subdevID, *value;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &servicename);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: servicename\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: id\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &value);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: value\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &subdevID);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: subdevID\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    SLOGI("servicename = %s\n", servicename);
    SLOGI("id = %s\n", id);
    SLOGI("value = %s\n", value);
    SLOGI("subdevID = %s\n", subdevID);
    SLOGI("sessionId = %u\n", sessionId);

    new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
    memset(allubus, 0x00, sizeof(new_allubus));

    strcpy(allubus->name, ST_CHANGE_NAME);
    allubus->id = sessionId;
    allubus->type = ALL_SESSION;
    allubus->numArgs = NUM_ARGS_CHANGENAME;
    add_last_list(allubus);

    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(servicename, daemon_services[i].name))
        {
            allubus->data = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                if(value && strlen(value)) 
                {
                    blobmsg_add_string(&buff, ST_VALUE, value);
                }
                
                if(subdevID && strlen(subdevID)) 
                {
                    if(!strcmp(servicename, ST_UPNP))
                    {
                        blobmsg_add_string(&buff, ST_SUBDEVID, subdevID);
                    }
                }

                allubus->action_time = (unsigned)time(NULL);
                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                allubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_CHANGE_NAME, buff.head, req,
                                        receive_data_cb, complete_data_cb, (void *) allubus); 
                
                break;
            }
        } 
    }
}

void get_name(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("get_name \n");
    QStatus status;
    int i;
    char *servicename, *id, *subdevID;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &servicename);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: servicename\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: id\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &subdevID);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: subdevID\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    SLOGI("servicename = %s\n", servicename);
    SLOGI("id = %s\n", id);
    SLOGI("subdevID = %s\n", subdevID);
    SLOGI("sessionId = %u\n", sessionId);

    new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
    memset(allubus, 0x00, sizeof(new_allubus));

    strcpy(allubus->name, ST_GET_NAME);
    allubus->id = sessionId;
    allubus->type = ALL_SESSION;
    allubus->numArgs = NUM_ARGS_GETNAME;
    add_last_list(allubus);

    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp("upnp", daemon_services[i].name))
        {
            allubus->data = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                
                if(subdevID && strlen(subdevID)) 
                {
                    if(!strcmp(servicename, ST_UPNP))
                    {
                        blobmsg_add_string(&buff, ST_SUBDEVID, subdevID);
                    }
                }

                allubus->action_time = (unsigned)time(NULL);
                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                allubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_GET_NAME, buff.head, req,
                                        receive_data_cb, complete_data_cb, (void *) allubus);           
                break;
            }
        } 
    }
}

void alexa(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("alexa \n");
    QStatus status;
    int i;
    char *servicename, *id, *subdevID, *value;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &servicename);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: servicename\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: id\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &value);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: value\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &subdevID);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: subdevID\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    SLOGI("servicename = %s\n", servicename);
    SLOGI("id = %s\n", id);
    SLOGI("value = %s\n", value);
    SLOGI("subdevID = %s\n", subdevID);
    SLOGI("sessionId = %u\n", sessionId);

    new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
    memset(allubus, 0x00, sizeof(new_allubus));

    strcpy(allubus->name, ST_ALEXA);
    allubus->id = sessionId;
    allubus->type = ALL_SESSION;
    allubus->numArgs = NUM_ARGS_ALEXA;
    add_last_list(allubus);

    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(servicename, daemon_services[i].name))
        {
            allubus->data = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, id);
                if(value && strlen(value)) 
                {
                    blobmsg_add_string(&buff, ST_VALUE, value);
                }
                
                if(subdevID && strlen(subdevID)) 
                {
                    if(!strcmp(servicename, ST_UPNP))
                    {
                        blobmsg_add_string(&buff, ST_SUBDEVID, subdevID);
                    }
                }

                allubus->action_time = (unsigned)time(NULL);
                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                allubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_ALEXA, buff.head, req,
                                        receive_data_cb, complete_data_cb, (void *) allubus);
                break;
            }
        } 
    }
}

void auto_conf(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("auto_conf\n");
    QStatus status;
    int i;
    char *servicename, *value;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &servicename);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: servicename\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &value);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: value\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    SLOGI("servicename = %s\n", servicename);
    SLOGI("value = %s\n", value);
    SLOGI("sessionId = %u\n", sessionId);

    new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
    memset(allubus, 0x00, sizeof(new_allubus));

    strcpy(allubus->name, ST_AUTO_CONF);
    allubus->id = sessionId;
    allubus->type = ALL_SESSION;
    allubus->numArgs = NUM_ARGS_AUTO_CONFIG;
    add_last_list(allubus);

    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(servicename, daemon_services[i].name))
        {
            allubus->data = (void*)daemon_services[i].name;
            if(daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                if(value && strlen(value)) 
                {
                    blobmsg_add_string(&buff, ST_SSID, value);
                }

                allubus->action_time = (unsigned)time(NULL);
                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                allubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_AUTO_CONF, buff.head, req,
                                        receive_data_cb, complete_data_cb, (void *) allubus);

                break;
            }
        }
    }
}

TimeZone g_timezones[]=
{
    {"Africa/Abidjan",                  "GMT0"},
    {"Africa/Accra",                    "GMT0"},
    {"Africa/Addis_Ababa",              "EAT-3"},
    {"Africa/Algiers",                  "CET-1"},
    {"Africa/Asmara",                   "EAT-3"},
    {"Africa/Bamako",                   "GMT0"},
    {"Africa/Bangui",                   "WAT-1"},
    {"Africa/Banjul",                   "GMT0"},
    {"Africa/Bissau",                   "GMT0"},
    {"Africa/Blantyre",                 "CAT-2"},
    {"Africa/Brazzaville",              "WAT-1"},
    {"Africa/Bujumbura",                "CAT-2"},
    {"Africa/Casablanca",               "WET0"},
    {"Africa/Ceuta",                    "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Africa/Conakry",                  "GMT0"},
    {"Africa/Dakar",                    "GMT0"},
    {"Africa/Dar_es_Salaam",            "EAT-3"},
    {"Africa/Djibouti",                 "EAT-3"},
    {"Africa/Douala",                   "WAT-1"},
    {"Africa/El_Aaiun",                 "WET0"},
    {"Africa/Freetown",                 "GMT0"},
    {"Africa/Gaborone",                 "CAT-2"},
    {"Africa/Harare",                   "CAT-2"},
    {"Africa/Johannesburg",             "SAST-2"},
    {"Africa/Kampala",                  "EAT-3"},
    {"Africa/Khartoum",                 "EAT-3"},
    {"Africa/Kigali",                   "CAT-2"},
    {"Africa/Kinshasa",                 "WAT-1"},
    {"Africa/Lagos",                    "WAT-1"},
    {"Africa/Libreville",               "WAT-1"},
    {"Africa/Lome",                     "GMT0"},
    {"Africa/Luanda",                   "WAT-1"},
    {"Africa/Lubumbashi",               "CAT-2"},
    {"Africa/Lusaka",                   "CAT-2"},
    {"Africa/Malabo",                   "WAT-1"},
    {"Africa/Maputo",                   "CAT-2"},
    {"Africa/Maseru",                   "SAST-2"},
    {"Africa/Mbabane",                  "SAST-2"},
    {"Africa/Mogadishu",                "EAT-3"},
    {"Africa/Monrovia",                 "GMT0"},
    {"Africa/Nairobi",                  "EAT-3"},
    {"Africa/Ndjamena",                 "WAT-1"},
    {"Africa/Niamey",                   "WAT-1"},
    {"Africa/Nouakchott",               "GMT0"},
    {"Africa/Ouagadougou",              "GMT0"},
    {"Africa/Porto-Novo",               "WAT-1"},
    {"Africa/Sao_Tome",                 "GMT0"},
    {"Africa/Tripoli",                  "EET-2"},
    {"Africa/Tunis",                    "CET-1"},
    {"Africa/Windhoek",                 "WAT-1WAST,M9.1.0,M4.1.0"},
    {"America/Adak",                    "HAST10HADT,M3.2.0,M11.1.0"},
    {"America/Anchorage",               "AKST9AKDT,M3.2.0,M11.1.0"},
    {"America/Anguilla",                "AST4"},
    {"America/Antigua",                 "AST4"},
    {"America/Araguaina",               "BRT3"},
    {"America/Argentina/Buenos_Aires",  "ART3"},
    {"America/Argentina/Catamarca",     "ART3"},
    {"America/Argentina/Cordoba",       "ART3"},
    {"America/Argentina/Jujuy",         "ART3"},
    {"America/Argentina/La_Rioja",      "ART3"},
    {"America/Argentina/Mendoza",       "ART3"},
    {"America/Argentina/Rio_Gallegos",  "ART3"},
    {"America/Argentina/Salta",         "ART3"},
    {"America/Argentina/San_Juan",      "ART3"},
    {"America/Argentina/Tucuman",       "ART3"},
    {"America/Argentina/Ushuaia",       "ART3"},
    {"America/Aruba",                   "AST4"},
    {"America/Asuncion",                "PYT4PYST,M10.1.0/0,M4.2.0/0"},
    {"America/Atikokan",                "EST5"},
    {"America/Bahia",                   "BRT3"},
    {"America/Barbados",                "AST4"},
    {"America/Belem",                   "BRT3"},
    {"America/Belize",                  "CST6"},
    {"America/Blanc-Sablon",            "AST4"},
    {"America/Boa_Vista",               "AMT4"},
    {"America/Bogota",                  "COT5"},
    {"America/Boise",                   "MST7MDT,M3.2.0,M11.1.0"},
    {"America/Cambridge Bay",           "MST7MDT,M3.2.0,M11.1.0"},
    {"America/Campo_Grande",            "AMT4AMST,M10.3.0/0,M2.3.0/0"},
    {"America/Cancun",                  "CST6CDT,M4.1.0,M10.5.0"},
    {"America/Caracas",                 "VET4:30"},
    {"America/Cayenne",                 "GFT3"},
    {"America/Cayman",                  "EST5"},
    {"America/Chicago",                 "CST6CDT,M3.2.0,M11.1.0"},
    {"America/Chihuahua",               "MST7MDT,M4.1.0,M10.5.0"},
    {"America/Costa_Rica",              "CST6"},
    {"America/Cuiaba",                  "AMT4AMST,M10.3.0/0,M2.3.0/0"},
    {"America/Curacao",                 "AST4"},
    {"America/Danmarkshavn",            "GMT0"},
    {"America/Dawson",                  "PST8PDT,M3.2.0,M11.1.0"},
    {"America/Dawson_Creek",            "MST7"},
    {"America/Denver",                  "MST7MDT,M3.2.0,M11.1.0"},
    {"America/Detroit",                 "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Dominica",                "AST4"},
    {"America/Edmonton",                "MST7MDT,M3.2.0,M11.1.0"},
    {"America/Eirunepe",                "AMT4"},
    {"America/El_Salvador",             "CST6"},
    {"America/Fortaleza",               "BRT3"},
    {"America/Glace_Bay",               "AST4ADT,M3.2.0,M11.1.0"},
    {"America/Goose_Bay",               "AST4ADT,M3.2.0/0:01,M11.1.0/0:01"},
    {"America/Grand_Turk",              "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Grenada",                 "AST4"},
    {"America/Guadeloupe",              "AST4"},
    {"America/Guatemala",               "CST6"},
    {"America/Guayaquil",               "ECT5"},
    {"America/Guyana",                  "GYT4"},
    {"America/Halifax",                 "AST4ADT,M3.2.0,M11.1.0"},
    {"America/Havana",                  "CST5CDT,M3.2.0/0,M10.5.0/1"},
    {"America/Hermosillo",              "MST7"},
    {"America/Indiana/Indianapolis",    "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Indiana/Knox",            "CST6CDT,M3.2.0,M11.1.0"},
    {"America/Indiana/Marengo",         "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Indiana/Petersburg",      "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Indiana/Tell_City",       "CST6CDT,M3.2.0,M11.1.0"},
    {"America/Indiana/Vevay",           "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Indiana/Vincennes",       "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Indiana/Winamac",         "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Inuvik",                  "MST7MDT,M3.2.0,M11.1.0"},
    {"America/Iqaluit",                 "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Jamaica",                 "EST5"},
    {"America/Juneau",                  "AKST9AKDT,M3.2.0,M11.1.0"},
    {"America/Kentucky/Louisville",     "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Kentucky/Monticello",     "EST5EDT,M3.2.0,M11.1.0"},
    {"America/La_Paz",                  "BOT4"},
    {"America/Lima",                    "PET5"},
    {"America/Los_Angeles",             "PST8PDT,M3.2.0,M11.1.0"},
    {"America/Maceio",                  "BRT3"},
    {"America/Managua",                 "CST6"},
    {"America/Manaus",                  "AMT4"},
    {"America/Marigot",                 "AST4"},
    {"America/Martinique",              "AST4"},
    {"America/Matamoros",               "CST6CDT,M3.2.0,M11.1.0"},
    {"America/Mazatlan",                "MST7MDT,M4.1.0,M10.5.0"},
    {"America/Menominee",               "CST6CDT,M3.2.0,M11.1.0"},
    {"America/Merida",                  "CST6CDT,M4.1.0,M10.5.0"},
    {"America/Mexico_City",             "CST6CDT,M4.1.0,M10.5.0"},
    {"America/Miquelon",                "PMST3PMDT,M3.2.0,M11.1.0"},
    {"America/Moncton",                 "AST4ADT,M3.2.0,M11.1.0"},
    {"America/Monterrey",               "CST6CDT,M4.1.0,M10.5.0"},
    {"America/Montevideo",              "UYT3UYST,M10.1.0,M3.2.0"},
    {"America/Montreal",                "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Montserrat",              "AST4"},
    {"America/Nassau",                  "EST5EDT,M3.2.0,M11.1.0"},
    {"America/New_York",                "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Nipigon",                 "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Nome",                    "AKST9AKDT,M3.2.0,M11.1.0"},
    {"America/Noronha",                 "FNT2"},
    {"America/North_Dakota/Center",     "CST6CDT,M3.2.0,M11.1.0"},
    {"America/North_Dakota/New Salem",  "CST6CDT,M3.2.0,M11.1.0"},
    {"America/Ojinaga",                 "MST7MDT,M3.2.0,M11.1.0"},
    {"America/Panama",                  "EST5"},
    {"America/Pangnirtung",             "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Paramaribo",              "SRT3"},
    {"America/Phoenix",                 "MST7"},
    {"America/Port_of_Spain",           "AST4"},
    {"America/Port-au-Prince",          "EST5"},
    {"America/Porto_Velho",             "AMT4"},
    {"America/Puerto_Rico",             "AST4"},
    {"America/Rainy_River",             "CST6CDT,M3.2.0,M11.1.0"},
    {"America/Rankin_Inlet",            "CST6CDT,M3.2.0,M11.1.0"},
    {"America/Recife",                  "BRT3"},
    {"America/Regina",                  "CST6"},
    {"America/Rio_Branco",              "AMT4"},
    {"America/Santa_Isabel",            "PST8PDT,M4.1.0,M10.5.0"},
    {"America/Santarem",                "BRT3"},
    {"America/Santo_Domingo",           "AST4"},
    {"America/Sao_Paulo",               "BRT3BRST,M10.3.0/0,M2.3.0/0"},
    {"America/Scoresbysund",            "EGT1EGST,M3.5.0/0,M10.5.0/1"},
    {"America/Shiprock",                "MST7MDT,M3.2.0,M11.1.0"},
    {"America/St_Barthelemy",           "AST4"},
    {"America/St_Johns",                "NST3:30NDT,M3.2.0/0:01,M11.1.0/0:01"},
    {"America/St_Kitts",                "AST4"},
    {"America/St_Lucia",                "AST4"},
    {"America/St_Thomas",               "AST4"},
    {"America/St_Vincent",              "AST4"},
    {"America/Swift_Current",           "CST6"},
    {"America/Tegucigalpa",             "CST6"},
    {"America/Thule",                   "AST4ADT,M3.2.0,M11.1.0"},
    {"America/Thunder_Bay",             "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Tijuana",                 "PST8PDT,M3.2.0,M11.1.0"},
    {"America/Toronto",                 "EST5EDT,M3.2.0,M11.1.0"},
    {"America/Tortola",                 "AST4"},
    {"America/Vancouver",               "PST8PDT,M3.2.0,M11.1.0"},
    {"America/Whitehorse",              "PST8PDT,M3.2.0,M11.1.0"},
    {"America/Winnipeg",                "CST6CDT,M3.2.0,M11.1.0"},
    {"America/Yakutat",                 "AKST9AKDT,M3.2.0,M11.1.0"},
    {"America/Yellowknife",             "MST7MDT,M3.2.0,M11.1.0"},
    {"Antarctica/Casey",                "WST-8"},
    {"Antarctica/Davis",                "DAVT-7"},
    {"Antarctica/DumontDUrville",       "DDUT-10"},
    {"Antarctica/Macquarie",            "MIST-11"},
    {"Antarctica/Mawson",               "MAWT-5"},
    {"Antarctica/McMurdo",              "NZST-12NZDT,M9.5.0,M4.1.0/3"},
    {"Antarctica/Rothera",              "ROTT3"},
    {"Antarctica/South_Pole",           "NZST-12NZDT,M9.5.0,M4.1.0/3"},
    {"Antarctica/Syowa",                "SYOT-3"},
    {"Antarctica/Vostok",               "VOST-6"},
    {"Arctic/Longyearbyen",             "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Asia/Aden",                       "AST-3"},
    {"Asia/Almaty",                     "ALMT-6"},
    {"Asia/Anadyr",                     "ANAT-11ANAST,M3.5.0,M10.5.0/3"},
    {"Asia/Aqtau",                      "AQTT-5"},
    {"Asia/Aqtobe",                     "AQTT-5"},
    {"Asia/Ashgabat",                   "TMT-5"},
    {"Asia/Baghdad",                    "AST-3"},
    {"Asia/Bahrain",                    "AST-3"},
    {"Asia/Baku",                       "AZT-4AZST,M3.5.0/4,M10.5.0/5"},
    {"Asia/Bangkok",                    "ICT-7"},
    {"Asia/Beirut",                     "EET-2EEST,M3.5.0/0,M10.5.0/0"},
    {"Asia/Bishkek",                    "KGT-6"},
    {"Asia/Brunei",                     "BNT-8"},
    {"Asia/Choibalsan",                 "CHOT-8"},
    {"Asia/Chongqing",                  "CST-8"},
    {"Asia/Colombo",                    "IST-5:30"},
    {"Asia/Damascus",                   "EET-2EEST,M4.1.5/0,M10.5.5/0"},
    {"Asia/Dhaka",                      "BDT-6"},
    {"Asia/Dili",                       "TLT-9"},
    {"Asia/Dubai",                      "GST-4"},
    {"Asia/Dushanbe",                   "TJT-5"},
    {"Asia/Gaza",                       "EET-2EEST,M3.5.6/0:01,M9.1.5"},
    {"Asia/Harbin",                     "CST-8"},
    {"Asia/Ho_Chi_Minh",                "ICT-7"},
    {"Asia/Saigon",                     "ICT-7"},
    {"Asia/Hong_Kong",                  "HKT-8"},
    {"Asia/Hovd",                       "HOVT-7"},
    {"Asia/Irkutsk",                    "IRKT-8IRKST,M3.5.0,M10.5.0/3"},
    {"Asia/Jakarta",                    "WIT-7"},
    {"Asia/Jayapura",                   "EIT-9"},
    {"Asia/Kabul",                      "AFT-4:30"},
    {"Asia/Kamchatka",                  "PETT-11PETST,M3.5.0,M10.5.0/3"},
    {"Asia/Karachi",                    "PKT-5"},
    {"Asia/Kashgar",                    "CST-8"},
    {"Asia/Kathmandu",                  "NPT-5:45"},
    {"Asia/Kolkata",                    "IST-5:30"},
    {"Asia/Krasnoyarsk",                "KRAT-7KRAST,M3.5.0,M10.5.0/3"},
    {"Asia/Kuala_Lumpur",               "MYT-8"},
    {"Asia/Kuching",                    "MYT-8"},
    {"Asia/Kuwait",                     "AST-3"},
    {"Asia/Macau",                      "CST-8"},
    {"Asia/Magadan",                    "MAGT-11MAGST,M3.5.0,M10.5.0/3"},
    {"Asia/Makassar",                   "CIT-8"},
    {"Asia/Manila",                     "PHT-8"},
    {"Asia/Muscat",                     "GST-4"},
    {"Asia/Nicosia",                    "EET-2EEST,M3.5.0/3,M10.5.0/4"},
    {"Asia/Novokuznetsk",               "NOVT-6NOVST,M3.5.0,M10.5.0/3"},
    {"Asia/Novosibirsk",                "NOVT-6NOVST,M3.5.0,M10.5.0/3"},
    {"Asia/Omsk",                       "OMST-7"},
    {"Asia/Oral",                       "ORAT-5"},
    {"Asia/Phnom_Penh",                 "ICT-7"},
    {"Asia/Pontianak",                  "WIT-7"},
    {"Asia/Pyongyang",                  "KST-9"},
    {"Asia/Qatar",                      "AST-3"},
    {"Asia/Qyzylorda",                  "QYZT-6"},
    {"Asia/Rangoon",                    "MMT-6:30"},
    {"Asia/Riyadh",                     "AST-3"},
    {"Asia/Sakhalin",                   "SAKT-10SAKST,M3.5.0,M10.5.0/3"},
    {"Asia/Samarkand",                  "UZT-5"},
    {"Asia/Seoul",                      "KST-9"},
    {"Asia/Shanghai",                   "CST-8"},
    {"Asia/Singapore",                  "SGT-8"},
    {"Asia/Taipei",                     "CST-8"},
    {"Asia/Tashkent",                   "UZT-5"},
    {"Asia/Tbilisi",                    "GET-4"},
    {"Asia/Tehran",                     "IRST-3:30IRDT,80/0,264/0"},
    {"Asia/Thimphu",                    "BTT-6"},
    {"Asia/Tokyo",                      "JST-9"},
    {"Asia/Ulaanbaatar",                "ULAT-8"},
    {"Asia/Urumqi",                     "CST-8"},
    {"Asia/Vientiane",                  "ICT-7"},
    {"Asia/Vladivostok",                "VLAT-10VLAST,M3.5.0,M10.5.0/3"},
    {"Asia/Yakutsk",                    "YAKT-9YAKST,M3.5.0,M10.5.0/3"},
    {"Asia/Yekaterinburg",              "YEKT-5YEKST,M3.5.0,M10.5.0/3"},
    {"Asia/Yerevan",                    "AMT-4AMST,M3.5.0,M10.5.0/3"},
    {"Atlantic/Azores",                 "AZOT1AZOST,M3.5.0/0,M10.5.0/1"},
    {"Atlantic/Bermuda",                "AST4ADT,M3.2.0,M11.1.0"},
    {"Atlantic/Canary",                 "WET0WEST,M3.5.0/1,M10.5.0"},
    {"Atlantic/Cape_Verde",             "CVT1"},
    {"Atlantic/Faroe",                  "WET0WEST,M3.5.0/1,M10.5.0"},
    {"Atlantic/Madeira",                "WET0WEST,M3.5.0/1,M10.5.0"},
    {"Atlantic/Reykjavik",              "GMT0"},
    {"Atlantic/South_Georgia",          "GST2"},
    {"Atlantic/St_Helena",              "GMT0"},
    {"Atlantic/Stanley",                "FKT4FKST,M9.1.0,M4.3.0"},
    {"Australia/Adelaide",              "CST-9:30CST,M10.1.0,M4.1.0/3"},
    {"Australia/Brisbane",              "EST-10"},
    {"Australia/Broken_Hill",           "CST-9:30CST,M10.1.0,M4.1.0/3"},
    {"Australia/Currie",                "EST-10EST,M10.1.0,M4.1.0/3"},
    {"Australia/Darwin",                "CST-9:30"},
    {"Australia/Eucla",                 "CWST-8:45"},
    {"Australia/Hobart",                "EST-10EST,M10.1.0,M4.1.0/3"},
    {"Australia/Lindeman",              "EST-10"},
    {"Australia/Lord_Howe",             "LHST-10:30LHST-11,M10.1.0,M4.1.0"},
    {"Australia/Melbourne",             "EST-10EST,M10.1.0,M4.1.0/3"},
    {"Australia/Perth",                 "WST-8"},
    {"Australia/Sydney",                "EST-10EST,M10.1.0,M4.1.0/3"},
    {"Europe/Amsterdam",                "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Andorra",                  "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Athens",                   "EET-2EEST,M3.5.0/3,M10.5.0/4"},
    {"Europe/Belgrade",                 "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Berlin",                   "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Bratislava",               "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Brussels",                 "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Bucharest",                "EET-2EEST,M3.5.0/3,M10.5.0/4"},
    {"Europe/Budapest",                 "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Chisinau",                 "EET-2EEST,M3.5.0/3,M10.5.0/4"},
    {"Europe/Copenhagen",               "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Dublin",                   "GMT0IST,M3.5.0/1,M10.5.0"},
    {"Europe/Gibraltar",                "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Guernsey",                 "GMT0BST,M3.5.0/1,M10.5.0"},
    {"Europe/Helsinki",                 "EET-2EEST,M3.5.0/3,M10.5.0/4"},
    {"Europe/Isle_of_Man",              "GMT0BST,M3.5.0/1,M10.5.0"},
    {"Europe/Istanbul",                 "EET-2EEST,M3.5.0/3,M10.5.0/4"},
    {"Europe/Jersey",                   "GMT0BST,M3.5.0/1,M10.5.0"},
    {"Europe/Kaliningrad",              "EET-2EEST,M3.5.0,M10.5.0/3"},
    {"Europe/Kiev",                     "EET-2EEST,M3.5.0/3,M10.5.0/4"},
    {"Europe/Lisbon",                   "WET0WEST,M3.5.0/1,M10.5.0"},
    {"Europe/Ljubljana",                "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/London",                   "GMT0BST,M3.5.0/1,M10.5.0"},
    {"Europe/Luxembourg",               "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Madrid",                   "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Malta",                    "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Mariehamn",                "EET-2EEST,M3.5.0/3,M10.5.0/4"},
    {"Europe/Minsk",                    "EET-2EEST,M3.5.0,M10.5.0/3"},
    {"Europe/Monaco",                   "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Moscow",                   "MSK-4"},
    {"Europe/Oslo",                     "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Paris",                    "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Podgorica",                "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Prague",                   "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Riga",                     "EET-2EEST,M3.5.0/3,M10.5.0/4"},
    {"Europe/Rome",                     "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Samara",                   "SAMT-3SAMST,M3.5.0,M10.5.0/3"},
    {"Europe/San_Marino",               "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Sarajevo",                 "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Simferopol",               "EET-2EEST,M3.5.0/3,M10.5.0/4"},
    {"Europe/Skopje",                   "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Sofia",                    "EET-2EEST,M3.5.0/3,M10.5.0/4"},
    {"Europe/Stockholm",                "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Tallinn",                  "EET-2EEST,M3.5.0/3,M10.5.0/4"},
    {"Europe/Tirane",                   "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Uzhgorod",                 "EET-2EEST,M3.5.0/3,M10.5.0/4"},
    {"Europe/Vaduz",                    "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Vatican",                  "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Vienna",                   "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Vilnius",                  "EET-2EEST,M3.5.0/3,M10.5.0/4"},
    {"Europe/Volgograd",                "VOLT-3VOLST,M3.5.0,M10.5.0/3"},
    {"Europe/Warsaw",                   "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Zagreb",                   "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Europe/Zaporozhye",               "EET-2EEST,M3.5.0/3,M10.5.0/4"},
    {"Europe/Zurich",                   "CET-1CEST,M3.5.0,M10.5.0/3"},
    {"Indian/Antananarivo",             "EAT-3"},
    {"Indian/Chagos",                   "IOT-6"},
    {"Indian/Christmas",                "CXT-7"},
    {"Indian/Cocos",                    "CCT-6:30"},
    {"Indian/Comoro",                   "EAT-3"},
    {"Indian/Kerguelen",                "TFT-5"},
    {"Indian/Mahe",                     "SCT-4"},
    {"Indian/Maldives",                 "MVT-5"},
    {"Indian/Mauritius",                "MUT-4"},
    {"Indian/Mayotte",                  "EAT-3"},
    {"Indian/Reunion",                  "RET-4"},
    {"Pacific/Apia",                    "WST11"},
    {"Pacific/Auckland",                "NZST-12NZDT,M9.5.0,M4.1.0/3"},
    {"Pacific/Chatham",                 "CHAST-12:45CHADT,M9.5.0/2:45,M4.1.0/3:45"},
    {"Pacific/Efate",                   "VUT-11"},
    {"Pacific/Enderbury",               "PHOT-13"},
    {"Pacific/Fakaofo",                 "TKT10"},
    {"Pacific/Fiji",                    "FJT-12"},
    {"Pacific/Funafuti",                "TVT-12"},
    {"Pacific/Galapagos",               "GALT6"},
    {"Pacific/Gambier",                 "GAMT9"},
    {"Pacific/Guadalcanal",             "SBT-11"},
    {"Pacific/Guam",                    "ChST-10"},
    {"Pacific/Honolulu",                "HST10"},
    {"Pacific/Johnston",                "HST10"},
    {"Pacific/Kiritimati",              "LINT-14"},
    {"Pacific/Kosrae",                  "KOST-11"},
    {"Pacific/Kwajalein",               "MHT-12"},
    {"Pacific/Majuro",                  "MHT-12"},
    {"Pacific/Marquesas",               "MART9:30"},
    {"Pacific/Midway",                  "SST11"},
    {"Pacific/Nauru",                   "NRT-12"},
    {"Pacific/Niue",                    "NUT11"},
    {"Pacific/Norfolk",                 "NFT-11:30"},
    {"Pacific/Noumea",                  "NCT-11"},
    {"Pacific/Pago_Pago",               "SST11"},
    {"Pacific/Palau",                   "PWT-9"},
    {"Pacific/Pitcairn",                "PST8"},
    {"Pacific/Ponape",                  "PONT-11"},
    {"Pacific/Port_Moresby",            "PGT-10"},
    {"Pacific/Rarotonga",               "CKT10"},
    {"Pacific/Saipan",                  "ChST-10"},
    {"Pacific/Tahiti",                  "TAHT10"},
    {"Pacific/Tarawa",                  "GILT-12"},
    {"Pacific/Tongatapu",               "TOT-13"},
    {"Pacific/Truk",                    "TRUT-10"},
    {"Pacific/Wake",                    "WAKT-12"},
    {"Pacific/Wallis",                  "WFT-12"},
};

static char g_timezone[128];

void set_time(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("set_time \n");
    QStatus status;
    int i;
    char *date_time, *timezone;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &date_time);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: date_time\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &timezone);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: timezone\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    SLOGI("date_time = %s\n", date_time);
    SLOGI("timezone = %s\n", timezone);

    if(date_time && strlen(date_time) && timezone && strlen(timezone))
    {
        strncpy(g_timezone, timezone, 128-1);
        char TZ_String[128];
        memset(TZ_String, 0x00, sizeof(TZ_String));
        for(i=0; i< sizeof(g_timezones)/sizeof(TimeZone); i++)
        {
            if(!strcmp(g_timezones[i].Location, timezone))
            {
                strcpy(TZ_String, g_timezones[i].TZ);
                break;
            }
        }

        if(strlen(TZ_String))
        {
            SLOGI("TZ_String = %s\n", TZ_String);
            char cmd[SIZE_256B];
            // VR_(write_option)(NULL,"system.@system[0].timezone=ICT-7");
            VR_(write_option)(NULL, UCI_SYSTEM_TIMEZONE"=%s", TZ_String);
            VR_(execute_system)("/etc/init.d/system restart");

            sprintf(cmd, "date -s \"%s\"", date_time);
            VR_(execute_system)(cmd);

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SET_TIME_R));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            Send_Signal(sessionId, ST_SET_TIME, (char *)json_object_to_json_string(jobj), NUM_ARGS_SET_TIME);
            json_object_put(jobj);
        }
        else
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SET_TIME_R));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("not support that location"));
            Send_Signal(sessionId, ST_SET_TIME, (char *)json_object_to_json_string(jobj), NUM_ARGS_SET_TIME);
            json_object_put(jobj);
        }
        
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SET_TIME_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("missing arguments"));
        Send_Signal(sessionId, ST_SET_TIME, (char *)json_object_to_json_string(jobj), NUM_ARGS_SET_TIME);
        json_object_put(jobj);
    }
}

void get_time(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("get_time \n");

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    char outstr[200];
    time_t t;
    struct tm *tmp;

    t = time(NULL);
    tmp = localtime(&t);
    if (tmp == NULL) {
        perror("localtime");
        exit(0);
    }

    if (strftime(outstr, sizeof(outstr), "%Y-%m-%d %H:%M:%S", tmp) == 0) {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_TIME_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("strftime returned 0"));
        Send_Signal(sessionId, ST_GET_TIME, (char *)json_object_to_json_string(jobj), NUM_ARGS_GET_TIME);
        json_object_put(jobj);
    }
    else
    {
        char *timezone = VR_(read_option)(NULL, UCI_SYSTEM_TIMEZONE);
        if(timezone)
        {
            printf("timezone = %s\n", timezone);
            int i;
            for(i=0; i< sizeof(g_timezones)/sizeof(TimeZone); i++)
            {
                if(!strcmp(g_timezones[i].Location, g_timezone) && !strcmp(g_timezones[i].TZ, timezone))
                {
                    sprintf(outstr+strlen(outstr), " %s", g_timezone);
                    break;
                }
            }

            if(i >= sizeof(g_timezones)/sizeof(TimeZone))
            {
                sprintf(outstr+strlen(outstr), " %s", timezone);
            }

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_TIME_R));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            json_object_object_add(jobj, ST_VALUE, json_object_new_string(outstr));
            Send_Signal(sessionId, ST_GET_TIME, (char *)json_object_to_json_string(jobj), NUM_ARGS_GET_TIME);
            json_object_put(jobj);

            free(timezone);
        }
        else
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_TIME_R));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("failed to get timezone"));
            Send_Signal(sessionId, ST_GET_TIME, (char *)json_object_to_json_string(jobj), NUM_ARGS_GET_TIME);
            json_object_put(jobj);
        }
    }
}

void rediscover(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("rediscover \n");

    QStatus status;
    int i;
    char *servicename, *device_type;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &servicename);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: servicename\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &device_type);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: device_type\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);
    SLOGI("servicename = %s\n", servicename);
    SLOGI("device_type = %s\n", device_type);

    new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
    memset(allubus, 0x00, sizeof(new_allubus));

    strcpy(allubus->name, ST_REDISCOVER);
    allubus->id = sessionId;
    allubus->type = ONLY_SESSION;
    allubus->numArgs = NUM_ARGS_REDISCOVER;
    add_last_list(allubus);

    if(servicename && strlen(servicename) && device_type && strlen(device_type))
    {
        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(servicename, daemon_services[i].name))
            {
                allubus->data = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {
                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, ST_DEVICE_TYPE, device_type);
                    
                    allubus->action_time = (unsigned)time(NULL);
                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    allubus->req=req;

                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_REDISCOVER, buff.head, req,
                                        receive_data_cb, complete_data_cb, (void *) allubus);
                    
                    break;
                }
            } 
        }
    }
}

static int listgroup_callback(void *grouplist, int argc, char **argv, char **azColName)
{
    int i;
    char *groupId = NULL;
    json_object * jobj = json_object_new_object();
    for(i=0; i<argc; i++)
    {
        if(!strcmp(azColName[i], ST_GROUP_ID))
        {
            groupId = argv[i];
        }

        if(!strcmp(azColName[i], ST_DEVICE))
        {
            if(argv[i])
            {
                json_object *devices = VR_(create_json_object)(argv[i]);
                if(devices)
                {
                    json_object_object_add(jobj, azColName[i], devices);
                }
            }
        }
        else
        {
            json_object_object_add(jobj, azColName[i], json_object_new_string(argv[i] ? argv[i] : "NULL"));
        }
    }

    if(groupId)
    {
        SEARCH_DATA_INIT_VAR(resourceId);
        searching_database("alljoyn_handler", dev_db, get_last_data_cb, &resourceId,
                        "SELECT owner from DEVICES where deviceId='%s'", 
                        groupId);
        if(resourceId.len)
        {
            json_object_object_add(jobj, ST_CLOUD_RESOURCE, json_object_new_string(resourceId.value));
        }
        FREE_SEARCH_DATA_VAR(resourceId);
    }

    json_object_array_add(grouplist, jobj);
    
    return 0;
}

void list_groups(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("list_groups\n");
    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_LIST_GROUPS_R));
    json_object *grouplist = json_object_new_array();

    searching_database("alljoyn_handler", dev_db, listgroup_callback, (void *)grouplist,
                            "SELECT * from GROUPS");

    json_object_object_add(jobj, ST_GROUP_LIST, grouplist);
    Send_Signal(sessionId, ST_LIST_GROUPS, (char *)json_object_to_json_string(jobj), NUM_ARGS_LIST_GROUPS);
    json_object_put(jobj);
}

static void cloud_create_group_thread(void *data)
{
    group_info *group_data = (group_info *)data;

    if(!group_data)
    {
        return;
    }

    char *userId = group_data->userId;
    char *appId = group_data->appId;
    char *groupId = group_data->groupId;
    char *groupName = group_data->groupName;
    char *groupDev = group_data->groupDev;

    int res = database_actions("alljoyn_handler", dev_db, 
                         "INSERT INTO GROUPS (userId,groupId,groupName,device) "
                         "VALUES('%s','%s','%s','%s')",
                         userId, groupId, groupName, groupDev);

    if(res)
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CREATE_GROUP_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
        json_object_object_add(jobj, ST_APP_ID, json_object_new_string(appId));
        json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
        json_object_object_add(jobj, ST_GROUP_DEV, json_object_new_string(groupDev));

        Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, ST_CREATE_GROUP, 
                        (char *)json_object_to_json_string(jobj), NUM_ARGS_CREATE_GROUP);

        json_object_put(jobj);
        goto done;
    }

    VR_(update_group_Etag)();
    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CREATE_GROUP_R));
    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
    json_object_object_add(jobj, ST_APP_ID, json_object_new_string(appId));
    json_object_object_add(jobj, ST_GROUP_ID, json_object_new_string(groupId));
    json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
    json_object_object_add(jobj, ST_GROUP_DEV, json_object_new_string(groupDev));

    char group_cloud_id[SIZE_64B];
    memset(group_cloud_id, 0x00, sizeof(group_cloud_id));
    res = VR_(cloud_create_group)(g_shm, groupName, groupDev, 
                                      group_cloud_id, sizeof(group_cloud_id));

    if(!res)
    {
        if(strlen(group_cloud_id))
        {
            database_actions("alljoyn_handler", dev_db, 
                            "INSERT INTO DEVICES(deviceId,deviceType,owner) "
                            "VALUES('%s','%s','%s')", groupId, ST_GROUP, group_cloud_id);
            shm_update_data(g_shm, group_cloud_id, group_data->groupId, ST_GROUP, SHM_ADD);

            json_object_object_add(jobj, ST_CLOUD_RESOURCE, json_object_new_string(group_cloud_id));
        }
    }

    Broadcast_message(ST_CREATE_GROUP, (char *)json_object_to_json_string(jobj), NUM_ARGS_CREATE_GROUP);
    json_object_put(jobj);

done:    
    SAFE_FREE(groupId);
    SAFE_FREE(groupName);
    SAFE_FREE(groupDev);
    SAFE_FREE(userId);
    SAFE_FREE(appId);
    SAFE_FREE(group_data);
}

void create_group(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("create_group\n");

    QStatus status;
    char *userId, *appId, *groupName, *groupDev;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &userId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: userid\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &appId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: appId\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &groupName);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: groupName\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &groupDev);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: groupDev\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);
    SLOGI("userId = %s\n", userId);
    SLOGI("appId = %s\n", appId);
    SLOGI("groupName = %s\n", groupName);
    SLOGI("groupDev = %s\n", groupDev);

    if( userId && strlen(userId) 
        && appId && strlen(appId) 
        && groupName && strlen(groupName) 
        && groupDev && strlen(groupDev)
       )
    {
        char groupId[SIZE_32B];
        sprintf(groupId, "%08X", VR_(generate_32bit)());

        SEARCH_DATA_INIT_VAR(name);
        searching_database("alljoyn_handler", dev_db, get_last_data_cb, &name, 
                            "SELECT groupName from GROUPS where groupId='%s'",
                            groupId);
        if(name.len)
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CREATE_GROUP_R));
            json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
            json_object_object_add(jobj, ST_APP_ID, json_object_new_string(appId));
            json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("group already has existed"));
            Send_Signal(sessionId, ST_CREATE_GROUP, (char *)json_object_to_json_string(jobj), NUM_ARGS_CREATE_GROUP);
            json_object_put(jobj);
            FREE_SEARCH_DATA_VAR(name);
            return;
        }

        searching_database("alljoyn_handler", dev_db, get_last_data_cb, &name, 
                            "SELECT groupName from GROUPS where groupName='%s'",
                            groupName);
        if(name.len)
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CREATE_GROUP_R));
            json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
            json_object_object_add(jobj, ST_APP_ID, json_object_new_string(appId));
            json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("group name already has existed"));
            Send_Signal(sessionId, ST_CREATE_GROUP, (char *)json_object_to_json_string(jobj), NUM_ARGS_CREATE_GROUP);
            json_object_put(jobj);
            FREE_SEARCH_DATA_VAR(name);
            return;
        }

        FREE_SEARCH_DATA_VAR(name);

        if(!VR_(check_group_dev_valid)(groupDev))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CREATE_GROUP_R));
            json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
            json_object_object_add(jobj, ST_APP_ID, json_object_new_string(appId));
            json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
            json_object_object_add(jobj, ST_GROUP_DEV, json_object_new_string(groupDev));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("group dev is invalid"));
            Send_Signal(sessionId, ST_CREATE_GROUP, (char *)json_object_to_json_string(jobj), NUM_ARGS_CREATE_GROUP);
            json_object_put(jobj);
            return;
        }

        group_info *group_data = (group_info *)malloc(sizeof(group_info));
        memset(group_data, 0, sizeof(group_info));

        group_data->groupId = strdup(groupId);
        group_data->groupName = strdup(groupName);
        group_data->groupDev = strdup(groupDev);
        group_data->userId = strdup(userId);
        group_data->appId = strdup(appId);

        pthread_t cloud_create_group_thread_t;
        pthread_create(&cloud_create_group_thread_t, NULL, (void *)&cloud_create_group_thread, (void *)group_data);
        pthread_detach(cloud_create_group_thread_t);
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CREATE_GROUP_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("missing argument"));
        Send_Signal(sessionId, ST_CREATE_GROUP, (char *)json_object_to_json_string(jobj), NUM_ARGS_CREATE_GROUP);
        json_object_put(jobj);
    }
}

//action:modify/delete userid, groupid
void group_actions(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("group_actions\n");

    QStatus status;
    char *action, *userId, *groupId, *groupName, *groupDev;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &action);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: action\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &userId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: userId\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &groupId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: groupId\n");
        return;
    }
    
    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);
    SLOGI("action = %s\n", action);
    SLOGI("userId = %s\n", userId);
    SLOGI("groupId = %s\n", groupId);
    // SLOGI("groupName = %s\n", groupName);
    // SLOGI("groupDev = %s\n", groupDev);

    if( action && strlen(action)
        && userId && strlen(userId)
        && groupId && strlen(groupId)
        )
    {
        if(!strcmp(action, ST_MODIFY))
        {
            status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &groupName);
            if (ER_OK != status) {
                SLOGE("Error reading alljoyn_message: groupName\n");
                return;
            }
            status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 4), "s", &groupDev);
            if (ER_OK != status) {
                SLOGE("Error reading alljoyn_message: groupDev\n");
                return;
            }

            SEARCH_DATA_INIT_VAR(database_group_id);
            searching_database("alljoyn_handler", dev_db, get_last_data_cb, &database_group_id, 
                                "SELECT groupId from GROUPS where groupName='%s' and groupId!='%s'",
                                groupName, groupId);
            if(database_group_id.len)
            {
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GROUP_ACTIONS_R));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("group name has existed"));
                json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
                json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
                json_object_object_add(jobj, ST_GROUP_ID, json_object_new_string(groupId));
                json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
                json_object_object_add(jobj, ST_NEW_NAME, json_object_new_string(groupName));
                Send_Signal(sessionId, ST_GROUP_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_GROUP_ACTIONS);
                json_object_put(jobj);
            }
            else
            {
                if(!VR_(check_group_dev_valid)(groupDev))
                {
                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GROUP_ACTIONS_R));
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
                    json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
                    json_object_object_add(jobj, ST_GROUP_ID, json_object_new_string(groupId));
                    json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
                    json_object_object_add(jobj, ST_GROUP_DEV, json_object_new_string(groupDev));
                    json_object_object_add(jobj, ST_REASON, json_object_new_string("group dev is invalid"));
                    Send_Signal(sessionId, ST_GROUP_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_GROUP_ACTIONS);
                    json_object_put(jobj);

                    return;
                }

                database_actions("alljoyn_handler", dev_db, 
                         "UPDATE GROUPS set %s='%s',%s='%s' where %s='%s'",
                         ST_GROUP_NAME, groupName, ST_DEVICE, groupDev, 
                         ST_GROUP_ID, groupId);

                VR_(update_group_Etag)();

                VR_(cloud_modify_group)(g_shm, groupId, groupName, groupDev);

                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GROUP_ACTIONS_R));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
                json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
                json_object_object_add(jobj, ST_GROUP_ID, json_object_new_string(groupId));
                json_object_object_add(jobj, ST_GROUP_NAME, json_object_new_string(groupName));
                json_object_object_add(jobj, ST_GROUP_DEV, json_object_new_string(groupDev));

                Broadcast_message(ST_GROUP_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_GROUP_ACTIONS);
                json_object_put(jobj);
            }

            FREE_SEARCH_DATA_VAR(database_group_id);
        }
        else
        {
            database_actions("alljoyn_handler", dev_db,
                    "DELETE from GROUPS where groupId='%s'", groupId);
            database_actions("alljoyn_handler", dev_db, "DELETE FROM DEVICES where deviceId='%s'", groupId);

            VR_(update_group_Etag)();
            VR_(cloud_delete_group)(g_shm, groupId);

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GROUP_ACTIONS_R));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
            json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
            json_object_object_add(jobj, ST_GROUP_ID, json_object_new_string(groupId));

            Broadcast_message(ST_GROUP_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_GROUP_ACTIONS);
            json_object_put(jobj);
        }
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GROUP_ACTIONS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("missing argument"));
        Send_Signal(sessionId, ST_GROUP_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_GROUP_ACTIONS);
        json_object_put(jobj);
    }
}

static int group_action_cb(void *data, char *devicetype, void *devicelist, int length)
{
    printf("devicetype = %s\n", devicetype);
    int i = 0;
    group_action_data *data_cb = (group_action_data *)data;
    printf("data_cb->command = %s\n", data_cb->command);
    printf("data_cb->value = %s\n", data_cb->value);

    printf("devicelist = %s\n", (char *)devicelist);
    
    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(devicetype, daemon_services[i].name))
        {
            if(daemon_services[i].id != 0)
            {
                char *save_tok;
                char *ch = strtok_r(devicelist, ",", &save_tok);
                while(ch != NULL)
                {
                    new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
                    memset(allubus, 0x00, sizeof(new_allubus));

                    strcpy(allubus->name, ST_SET_BINARY);
                    allubus->type = ALL_SESSION;
                    allubus->numArgs = NUM_ARGS_NOTIFY;
                    allubus->action_time = (unsigned)time(NULL);
                    allubus->data = (void*)daemon_services[i].name;
                    add_last_list(allubus);

                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, ST_ID, ch);
                    blobmsg_add_string(&buff, ST_CMD, data_cb->command);
                    blobmsg_add_string(&buff, ST_VALUE, data_cb->value);

                    struct ubus_request *req_s = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    allubus->req=req_s;

                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_SET_BINARY, buff.head, req_s,
                                        receive_data_cb, complete_data_cb, (void *) allubus);
                      
                    ch = strtok_r(NULL, ",", &save_tok);
                }
                break;
            }
        }
    }
    return 0;
}
//userid, groupid, command, value
void group_control(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("group_control\n");

    QStatus status;
    char *userid, *groupid, *command, *value;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &userid);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: userid\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &groupid);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: groupid\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &command);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: command\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &value);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: value\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);
    SLOGI("userid = %s\n", userid);
    SLOGI("groupid = %s\n", groupid);
    SLOGI("command = %s\n", command);
    SLOGI("value = %s\n", value);
    
    if( userid && strlen(userid) 
        && groupid && strlen(groupid) 
        && command && strlen(command)
        && value && strlen(value)
       )
    {
        SEARCH_DATA_INIT_VAR(device);

        searching_database("alljoyn_handler", dev_db, get_last_data_cb, &device, 
                            "SELECT device from GROUPS where groupId='%s'",
                            groupid);
        if(device.len)
        {
            json_object * jobj = VR_(create_json_object)(device.value);
            if(jobj)
            {
                group_action_data data;
                data.command = command;
                data.value = value;
                group_control_process(jobj, (void*)&data, group_action_cb);
                json_object_put(jobj);

                json_object *jobj_output = json_object_new_object();
                json_object_object_add(jobj_output, ST_METHOD, json_object_new_string(ST_GROUP_CONTROL_R));
                json_object_object_add(jobj_output, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                json_object_object_add(jobj_output, ST_USER_ID, json_object_new_string(userid));
                json_object_object_add(jobj_output, ST_GROUP_ID, json_object_new_string(groupid));
                json_object_object_add(jobj_output, ST_COMMAND, json_object_new_string(command));
                json_object_object_add(jobj_output, ST_VALUE, json_object_new_string(value));

                Broadcast_message(ST_GROUP_CONTROL, (char *)json_object_to_json_string(jobj_output), NUM_ARGS_GROUP_CONTROL);
                json_object_put(jobj_output);
            }
            else
            {
                SLOGE("Failed to parse json\n");
            }
        }
        else
        {

        }
        FREE_SEARCH_DATA_VAR(device);
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GROUP_CONTROL_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("missing argument"));
        Send_Signal(sessionId, ST_GROUP_CONTROL, (char *)json_object_to_json_string(jobj), NUM_ARGS_GROUP_CONTROL);
        json_object_put(jobj);
    }
}

void network_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("network_handle\n");

    int i;
    QStatus status;
    char *service_name, *device_id, *class, *command, *data0, *data1, *data2;
    
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &service_name);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: service_name\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &device_id);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: device_id\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &class);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: class\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &command);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: command\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 4), "s", &data0);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data0\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 5), "s", &data1);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data1\n");
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 6), "s", &data2);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: data2\n");
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    SLOGI("service_name = %s\n", service_name);
    SLOGI("device_id = %s\n", device_id);
    SLOGI("class = %s\n", class);
    SLOGI("command = %s\n", command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);
    SLOGI("data2 = %s\n", data2);
    
    if(service_name && strlen(service_name) && device_id && strlen(device_id) 
        && class && strlen(class) && command && strlen(command))
    {
        new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
        memset(allubus, 0x00, sizeof(new_allubus));

        strcpy(allubus->name, ST_NETWORK);
        allubus->id = sessionId;
        allubus->type = ONLY_SESSION;
        allubus->numArgs = NUM_ARGS_NETWORK;
        add_last_list(allubus);

        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(service_name, daemon_services[i].name))
            {
                allubus->data = (void*)daemon_services[i].name;
                if(daemon_services[i].id != 0)
                {
                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, ST_ID, device_id);
                    blobmsg_add_string(&buff, ST_CLASS, class);
                    blobmsg_add_string(&buff, ST_CMD, command);
                    if(data0 && strlen(data0)) blobmsg_add_string(&buff, ST_DATA0, data0);
                    if(data1 && strlen(data1)) blobmsg_add_string(&buff, ST_DATA1, data1);
                    if(data2 && strlen(data2)) blobmsg_add_string(&buff, ST_DATA2, data2);

                    allubus->action_time = (unsigned)time(NULL);
                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));

                    allubus->req=req;

                    VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_NETWORK, buff.head, req,
                                    receive_data_cb, complete_data_cb, (void *) allubus);

                    break;
                }
            }
        }
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NETWORK_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_NETWORK, (char *)json_object_to_json_string(jobj), NUM_ARGS_NETWORK);
        json_object_put(jobj);
    }
}

void share_access(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("share_access\n");

    QStatus status;
    char *adminId, *userId, *shareId, *username, *expiration;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &adminId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: adminId\n");
        return;
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &userId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: userId\n");
        return;
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &shareId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: shareId\n");
        return;
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &username);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: username\n");
        return;
    }

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 4), "s", &expiration);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: expiration\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);
    SLOGI("adminId = %s\n", adminId);
    SLOGI("userId = %s\n", userId);
    SLOGI("shareId = %s\n", shareId);
    SLOGI("username = %s\n", username);
    SLOGI("expiration = %s\n", expiration);

    if(adminId && strlen(adminId)
        && userId && strlen(userId)
        && shareId && strlen(shareId)
        && username && strlen(username)
        )
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SHARE_ACCESS_R));
        json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
        json_object_object_add(jobj, ST_SHARE_ID, json_object_new_string(shareId));
        json_object_object_add(jobj, ST_USER_NAME, json_object_new_string(username));
        
        SEARCH_DATA_INIT_VAR(usertype);
        searching_database("alljoyn_handler", user_db, get_last_data_cb, &usertype, 
                    "SELECT userType from USERS where userId='%s'", adminId);

        if(!strcmp(usertype.value, ST_ADMIN))
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

            VR_(remove_deny_timer)(userId);

            if(expiration && strlen(expiration) && strcmp(expiration, "0"))
            {
                time_t expiration_t = strtoul(expiration, NULL, 0);
                VR_(deny_with_timer)(userId, expiration_t);
                json_object_object_add(jobj, ST_EXPIRATION, json_object_new_string(expiration));
            }

            SEARCH_DATA_INIT_VAR(enable);
            searching_database("alljoyn_handler", user_db, get_last_data_cb, &enable, 
                        "SELECT enable from USERS where userId='%s'", userId);
            if(enable.len)
            {
                if(!strcmp(enable.value, ST_DENY))
                {
                    database_actions("alljoyn_handler", user_db, 
                        "UPDATE USERS set shareId='%s',enable='%s' where userId='%s'", shareId, ST_ENABLE, userId);
                }
            }
            else
            {
                database_actions("alljoyn_handler", user_db, 
                        "INSERT INTO USERS (userId,shareId,name,enable,userType) "\
                        "VALUES ('%s','%s','%s','%s','%s')", userId, shareId, username, ST_ENABLE, ST_USER);
            }

            FREE_SEARCH_DATA_VAR(enable);

            if(expiration && strlen(expiration) && strcmp(expiration, "0"))
            {
                database_actions("alljoyn_handler", user_db, 
                        "UPDATE USERS set shareTime='%s',options='%u' where userId='%s'", 
                        expiration, (unsigned)time(NULL), userId);
            }
            // else
            // {
            //     database_actions("alljoyn_handler", user_db, 
            //             "UPDATE USERS set shareTime=null,options=null where userId='%s'", 
            //             userId);
            // }
        }
        else
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("you are not allowed to do this"));
        }

        Send_Signal(sessionId, ST_SHARE_ACCESS, (char *)json_object_to_json_string(jobj), NUM_ARGS_SHARE_ACCESS);
        FREE_SEARCH_DATA_VAR(usertype);
        json_object_put(jobj);

        json_object *publish_jobj = json_object_new_object();
        json_object_object_add(publish_jobj, ST_METHOD, json_object_new_string(ST_PUBLISH));
        json_object_object_add(publish_jobj, ST_MESSAGE, json_object_new_string(ST_ACCESS_CHANGED));
        Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, ST_PUBLISH, (char *)json_object_to_json_string(publish_jobj), NUM_ARGS_PUBLISH);
        json_object_put(publish_jobj);
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SHARE_ACCESS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_SHARE_ACCESS, (char *)json_object_to_json_string(jobj), NUM_ARGS_SHARE_ACCESS);
        json_object_put(jobj);
    }
}

void request_access(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("request_access\n");

    QStatus status;
    char *userId;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &userId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: userId\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);
    SLOGI("userId = %s\n", userId);

    if(userId && strlen(userId))
    {    
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_REQUEST_ACCESS_R));

        SEARCH_DATA_INIT_VAR(status);
        searching_database("alljoyn_handler", user_db, get_last_data_cb, &status, 
                    "SELECT enable from USERS where userId='%s'", userId);
        if(!strcmp(status.value, ST_ENABLE))
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            char b64_encode[SIZE_256B];
            psk_encrypt(userId, g_psk, b64_encode);
            json_object_object_add(jobj, ST_PSK_ENCRYPT, json_object_new_string(b64_encode));

            // database_actions("alljoyn_handler", user_db, 
            //             "UPDATE USERS set SessionId='%u' where UserId='%s'", sessionId, userId);
        }
        else
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        }

        Send_Signal(sessionId, ST_REQUEST_ACCESS, (char *)json_object_to_json_string(jobj), NUM_ARGS_REQUEST_ACCESS);
        FREE_SEARCH_DATA_VAR(status);
        json_object_put(jobj);
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_REQUEST_ACCESS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_REQUEST_ACCESS, (char *)json_object_to_json_string(jobj), NUM_ARGS_REQUEST_ACCESS);
        json_object_put(jobj);
    }
}

// static int notify_passchanged(void *data, int argc, char **argv, char **azColName)
// {
//     if(argv[0] && strlen(argv[0]) 
//         && argv[1] && strlen(argv[1])) //0 UserID, 1 SessionID
//     {
//         char userid[SIZE_256B];
//         strcpy(userid, argv[0]);
//         char b64_encode[SIZE_256B];
//         psk_encrypt(userid, g_psk, b64_encode);
//         alljoyn_sessionid sessionId = strtol(argv[1], NULL, 0);

//         json_object *jobj = json_object_new_object();
//         json_object_object_add(jobj, ST_METHOD, json_object_new_string("notify"));
//         json_object_object_add(jobj, "notifytype", json_object_new_string("psk_changed"));
//         json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
//         json_object_object_add(jobj, "psk_encrypt", json_object_new_string(b64_encode));
//         Send_Signal(sessionId, "request_access", (char *)json_object_to_json_string(jobj), 1);
//         json_object_put(jobj);
//     }
//     return 0;
// }

void deny_access(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("deny_access\n");

    QStatus status;
    char *userId1, *userId2;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &userId1);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: userId1\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &userId2);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: user\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);
    SLOGI("userId1 = %s\n", userId1);
    SLOGI("userId2 = %s\n", userId2);

    if(userId1 && strlen(userId1) && userId2 && strlen(userId2))
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_DENY_ACCESS_R));
        SEARCH_DATA_INIT_VAR(usertype);
        searching_database("alljoyn_handler", user_db, get_last_data_cb, &usertype, 
                    "SELECT userType from USERS where userId='%s'", userId1);

        if(!strcmp(usertype.value, ST_ADMIN))
        {
            searching_database("alljoyn_handler", user_db, get_last_data_cb, &usertype, 
                    "SELECT userType from USERS where userId='%s'", userId2);

            if(!strcmp(usertype.value, ST_USER))
            {
                VR_(remove_deny_timer)(userId2);

                database_actions("alljoyn_handler", user_db, 
                        "DELETE from USERS where userId='%s'", userId2);

                json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId2));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                VR_(generate_128bit)(g_psk);

                VR_(write_option)(NULL, UCI_LOCAL_PSK"=%s", g_psk);
            }
            else
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("Could not remove admin"));
            }
        }
        else if(!strcmp(usertype.value, ST_USER))
        {
            searching_database("alljoyn_handler", user_db, get_last_data_cb, &usertype, 
                    "SELECT userType from USERS where userId='%s'", userId2);

            if(!strcmp(usertype.value, ST_USER) && !strcmp(userId1, userId2))
            {
                VR_(remove_deny_timer)(userId2);

                database_actions("alljoyn_handler", user_db, 
                        "DELETE from USERS where userId='%s'", userId2);

                json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId2));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                VR_(generate_128bit)(g_psk);

                VR_(write_option)(NULL, UCI_LOCAL_PSK"=%s", g_psk);
            }
            else
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("you are not allowed to do this"));
            }

        }
        Send_Signal(sessionId, ST_DENY_ACCESS, (char *)json_object_to_json_string(jobj), NUM_ARGS_DENY_ACCESS);
        FREE_SEARCH_DATA_VAR(usertype);
        json_object_put(jobj);

        json_object *notify_jobj = json_object_new_object();
        json_object_object_add(notify_jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
        json_object_object_add(notify_jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_PSK_CHANGED));
        Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, ST_NOTIFY, (char *)json_object_to_json_string(notify_jobj), NUM_ARGS_NOTIFY);
        json_object_put(notify_jobj);
        // //////////////////// send new pass to all users /////////////////
        // searching_database("alljoyn_handler", user_db, notify_passchanged, NULL, 
        //         "SELECT UserId,SessionId from USERS where Enable='enable'");
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_DENY_ACCESS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_DENY_ACCESS, (char *)json_object_to_json_string(jobj), NUM_ARGS_DENY_ACCESS);
        json_object_put(jobj);
    }
}

static int listuser_callback(void *userlist, int argc, char **argv, char **azColName)
{
   //  int i;
   // for(i=0; i<argc; i++){

   //    printf("%s = %s | ", azColName[i], argv[i] ? argv[i] : "NULL");
   //    //if(data != NULL) strcpy(data, argv[i]);
   // }
   // printf("\n");
    int i;
    json_object * jobj = json_object_new_object();
    for(i=0; i<argc; i++)
    {
        json_object_object_add(jobj, azColName[i], json_object_new_string(argv[i] ? argv[i] : "NULL"));
    }

    json_object_array_add(userlist, jobj);
    return 0;
}

void list_users(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("list_users\n");

    QStatus status;
    char *userid;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &userid);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: userid\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);
    SLOGI("userid = %s\n", userid);

    if(userid && strlen(userid))
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_LIST_USERS_R));
        SEARCH_DATA_INIT_VAR(usertype);
        searching_database("alljoyn_handler", user_db, get_last_data_cb, &usertype, 
                    "SELECT userType from USERS where userId='%s'", userid);

        if(!strcmp(usertype.value, ST_ADMIN))
        {
            json_object *userlist = json_object_new_array();
            searching_database("alljoyn_handler", user_db, listuser_callback, (char *)userlist, 
                    "SELECT userId,shareId,name,enable,shareTime,options from USERS where userType='%s'", ST_USER);
            json_object_object_add(jobj, ST_USER_LIST, userlist);
        }
        else if(!strcmp(usertype.value, ST_USER))
        {
            json_object *userlist = json_object_new_array();
            searching_database("alljoyn_handler", user_db, listuser_callback, (char *)userlist, 
                    "SELECT userId,shareId,name,enable,shareTime,options from USERS where userId='%s'", userid);
            json_object_object_add(jobj, ST_USER_LIST, userlist);
        }
        Send_Signal(sessionId, ST_LIST_USERS, (char *)json_object_to_json_string(jobj), NUM_ARGS_LIST_USERS);
        FREE_SEARCH_DATA_VAR(usertype);
        json_object_put(jobj);
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_LIST_USERS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_LIST_USERS, (char *)json_object_to_json_string(jobj), NUM_ARGS_LIST_USERS);
        json_object_put(jobj);
    }
}

void ob_request_access(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("ob_request_access\n");

    QStatus status;
    char *userid;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &userid);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: userid\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);
    SLOGI("userid = %s\n", userid);

    if(userid && strlen(userid))
    {    
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_OB_REQUEST_ACCESS_R));

        SEARCH_DATA_INIT_VAR(status);
        searching_database("alljoyn_handler", user_db, get_last_data_cb, &status, 
                    "SELECT enable from USERS where userId='%s'", userid);
        if(!strcmp(status.value, ST_ENABLE))
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            char b64_encode[SIZE_256B];
            char *ob_pass = (char *)VR_(read_option)(NULL, UCI_LOCAL_ONBOARDING_PSK);
            if(ob_pass)
            {
                psk_encrypt(userid, ob_pass, b64_encode);
                free(ob_pass);
            }
            else
            {
                VR_(write_option)(NULL, UCI_LOCAL_ONBOARDING_PSK"=8888");
                psk_encrypt(userid, "8888", b64_encode);
            }
            
            json_object_object_add(jobj, ST_OB_ENCRYPT, json_object_new_string(b64_encode));
        }
        else
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        }

        Send_Signal(sessionId, ST_OB_REQUEST_ACCESS, (char *)json_object_to_json_string(jobj), NUM_ARGS_OB_REQUEST_ACCESS);
        FREE_SEARCH_DATA_VAR(status);
        json_object_put(jobj);
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_OB_REQUEST_ACCESS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_OB_REQUEST_ACCESS, (char *)json_object_to_json_string(jobj), NUM_ARGS_OB_REQUEST_ACCESS);
        json_object_put(jobj);
    }
}

static void upload_log_thread(void *data)
{
    size_t log_length = get_file_length(DEFAULT_LOG_LOCATION);
    if(log_length > LOG_SIZE_ROTATE)
    {
        clean_file_content(DEFAULT_LOG_LOCATION);
        SLOGI("file log to large size %d, shoule be remove\n", log_length);
        return;
    }

    int rc = VR_(httpUploadFile)(DEFAULT_LOG_LOCATION);
    SLOGI("############ auto_upload_cb rc = %d ###########\n", rc);
    if(rc)
    {
        char *log_count = VR_(read_option)(NULL,"security.@remote-access[0].log_count");
        if(log_count)
        {
            int tmp = strtol(log_count,NULL,0);
            if(tmp == 10)
            {
                clean_file_content(DEFAULT_LOG_LOCATION);
            }
            else
            {
                tmp++;
                VR_(write_option)(NULL,"security.@remote-access[0].log_count=%d", tmp);
            }
            free(log_count);
        }
        else
        {
            clean_file_content(DEFAULT_LOG_LOCATION);
            VR_(write_option)(NULL,"security.@remote-access[0].log_count=0");
        }
        // json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
    }
    else
    {
        clean_file_content(DEFAULT_LOG_LOCATION);
        VR_(write_option)(NULL,"security.@remote-access[0].log_count=0");
        // json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    }
}

void send_report(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("send_report\n");

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SEND_REPORT_R));

    char cmd[SIZE_64B];
    snprintf(cmd, sizeof(cmd), "ps -w >> %s", DEFAULT_LOG_LOCATION);
    VR_(execute_system)(cmd);
    snprintf(cmd, sizeof(cmd), "cat /proc/meminfo >> %s", DEFAULT_LOG_LOCATION);
    VR_(execute_system)(cmd);
    snprintf(cmd, sizeof(cmd), "df >> %s", DEFAULT_LOG_LOCATION);
    VR_(execute_system)(cmd);

    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

    pthread_t send_report_t;
    pthread_create(&send_report_t, NULL, (void *)&upload_log_thread, NULL);
    pthread_detach(send_report_t);
    
    Send_Signal(sessionId, ST_SEND_REPORT, (char *)json_object_to_json_string(jobj), NUM_ARGS_SEND_REPORT);
    json_object_put(jobj);
}

void timer_scanwifi(void * data)
{
    SLOGI("start rescan wifi\n");
    VR_(execute_system)("/usr/sbin/wifi_scan");
    SLOGI("end rescan wifi\n");
}

void wifi_settings(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("wifi_settings\n");
    QStatus status;
    char *userid, *mode, *ssid, *password, *encryption, *channel;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &userid);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: userid\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &mode);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: mode\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &ssid);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: ssid\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &password);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: password\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 4), "s", &encryption);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: encryption\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 5), "s", &channel);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: channel\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    SLOGI("userid = %s\n", userid);
    SLOGI("mode = %s\n", mode);

#ifdef SHARE_USER_LIMIT
    char *local_state = VR_(read_option)(NULL, UCI_LOCAL_STATE);
    if(!local_state)
    {
        return;
    }


    if(!strcmp(local_state, ST_CONNECTED))
    {
        if(userid && strlen(userid))
        {
            SEARCH_DATA_INIT_VAR(usertype);
            searching_database("alljoyn_handler", user_db, get_last_data_cb, &usertype,
                        "SELECT userType from USERS where userId='%s'", userid);
            SLOGI("usertype.value = %s\n", usertype.value);
            if(strcmp(usertype.value, ST_ADMIN))
            {
                if(!strcmp(mode, ST_STA) || !strcmp(mode, ST_AP))
                {
                    free(local_state);
                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    json_object_object_add(jobj, ST_REASON, json_object_new_string("you are not allow to do this"));
                    Send_Signal(sessionId, ST_WIFI_SETTINGS, (char *)json_object_to_json_string(jobj), NUM_ARGS_WIFI_SETTINGS);
                    json_object_put(jobj);
                    FREE_SEARCH_DATA_VAR(usertype);
                    return;
                }
            }
            FREE_SEARCH_DATA_VAR(usertype);
        }
        else
        {
            free(local_state);
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
            Send_Signal(sessionId, ST_WIFI_SETTINGS, (char *)json_object_to_json_string(jobj), NUM_ARGS_WIFI_SETTINGS);
            json_object_put(jobj);
            return;
        }
        free(local_state);
    }
#endif
    if( mode && strlen(mode))
    {
        if(!strcmp(mode, ST_SCAN))
        {
            if(!g_timer_scanwifi)
            {
                timerStart(&g_timer_scanwifi, timer_scanwifi, NULL, 3000, TIMER_ONETIME);
            }

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
            json_object_object_add(jobj, ST_MODE, json_object_new_string(mode));
            
            json_object *scanlist = json_object_new_array();
            FILE *fp;
            char * line = NULL;
            size_t len = 0;
            ssize_t read;
            fp = fopen("/tmp/wifi_scan_results", "r");
            if(!fp)
            {
                return;
            }
            while ((read = getline(&line, &len, fp)) != -1)
            {
                char *pos;
                if ((pos=strchr(line, '\n')) != NULL)
                    *pos = '\0';
                char *save_tok;
                char* ssid_f = strtok_r((char*)line, "\t", &save_tok);
                char* auth_f = strtok_r(NULL, "\t", &save_tok);
                if (!ssid_f) 
                {
                    continue;
                }

                char encryption_s[SIZE_64B];
                char* authType = strtok_r(auth_f, "-", &save_tok);
                char* firstCipher = strtok_r(NULL, "-", &save_tok);
                char* secondCipher = strtok_r(NULL, "-", &save_tok);

                if (!authType) 
                {
                    continue;
                }

                if (!strcmp(authType, "WEP")) 
                {
                    strcpy(encryption_s, "wep");
                } 
                else if (!strcmp(authType, "Open")) 
                {
                    strcpy(encryption_s, "open");
                } 
                else if (!strcmp(authType, "WPA2")) 
                {
                    strcpy(encryption_s, "psk2");
                } 
                else if (!strcmp(authType, "WPA")) 
                {
                    strcpy(encryption_s, "psk");
                }
                if(!strcmp(encryption_s,"psk") || !strcmp(encryption_s,"psk2"))
                {
                    if(!firstCipher || !strcmp(firstCipher, "PSK"))
                    {
                        continue;
                    }
                    else if (!strcmp(firstCipher, "TKIP"))
                    {
                        strcat(encryption_s, "+tkip");
                        if(secondCipher && !strcmp(secondCipher, "CCMP"))
                        {
                            strcat(encryption_s, "+ccmp");
                        }
                    }
                    else if (!strcmp(firstCipher, "CCMP"))
                    {
                        if(secondCipher && !strcmp(secondCipher, "TKIP"))
                        {
                            strcat(encryption_s, "+tkip+ccmp");
                        }
                        else
                        {
                            strcat(encryption_s, "+ccmp");
                        }
                    }
                }

                json_object *ssid_obj = json_object_new_object();
                json_object_object_add(ssid_obj, ST_SSID, json_object_new_string(ssid_f));
                json_object_object_add(ssid_obj, ST_AUTH, json_object_new_string(encryption_s));
                json_object_array_add(scanlist, ssid_obj);
            }
            if(line)
                free(line);
            fclose(fp);

            json_object_object_add(jobj, ST_SCAN_LIST, scanlist);
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            Send_Signal(sessionId, ST_WIFI_SETTINGS, (char *)json_object_to_json_string(jobj), NUM_ARGS_WIFI_SETTINGS);
            json_object_put(jobj);
        }
        else if(!strcmp(mode, ST_GET))
        {
            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
            json_object_object_add(jobj, ST_MODE, json_object_new_string(mode));
            json_object *apmode = json_object_new_object();
            char *wifi_mode = VR_(read_option)(NULL, UCI_WIFI_STA_MODE);

            char *apssid = VR_(read_option)(NULL, UCI_HUB_DEFAULT_NAME);
            if(apssid)
            {
                json_object_object_add(apmode, ST_SSID, json_object_new_string(apssid));
                free(apssid);
            }

            char *apencryption = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_APENCRYPTION);
            if(apencryption)
            {
                json_object_object_add(apmode, ST_ENCRYPTION, json_object_new_string(apencryption));
                free(apencryption);
            }

            char *apkey = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_APKEY);
            if(apkey)
            {
                char b64_encode[SIZE_256B];
                psk_encrypt(userid, apkey, b64_encode);
                json_object_object_add(apmode, ST_KEY, json_object_new_string(b64_encode));
                free(apkey);
            }

            if(wifi_mode)
            {
                if(!strcmp(wifi_mode, ST_AP))
                {
                    json_object_object_add(apmode, ST_STATE, json_object_new_string("active"));
                }
                else if(!strcmp(wifi_mode, ST_STA))
                {
                    json_object_object_add(apmode, ST_STATE, json_object_new_string("inactive"));
                }
            }
            else
            {
                json_object_object_add(apmode, ST_STATE, json_object_new_string("unknown"));
            }

            json_object *stamode = json_object_new_object();
            char *ssid = VR_(read_option)(NULL,UCI_ALLJOYN_ONBOARDING_SSID);
            if(ssid)
            {
                json_object_object_add(stamode, ST_SSID, json_object_new_string(ssid));
                free(ssid);
            }
            char *encryption = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_ENCRYPTION);
            if(encryption)
            {
                json_object_object_add(stamode, ST_ENCRYPTION, json_object_new_string(encryption));
                free(encryption);
            }
            char *key = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_KEY);
            if(key)
            {
                char b64_encode[SIZE_256B];
                psk_encrypt(userid, key, b64_encode);
                json_object_object_add(stamode, ST_KEY, json_object_new_string(b64_encode));
                free(key);
            }
            if(wifi_mode)
            {
                if(!strcmp(wifi_mode, ST_AP))
                {
                    json_object_object_add(stamode, ST_STATE, json_object_new_string("inactive"));
                }
                else if(!strcmp(wifi_mode, ST_STA))
                {
                    json_object_object_add(stamode, ST_STATE, json_object_new_string("active"));
                }
            }
            else
            {
                json_object_object_add(stamode, ST_STATE, json_object_new_string("unknown"));
            }

            if(wifi_mode)
            {
                free(wifi_mode);
            }

            json_object_object_add(jobj, ST_AP_MODE, apmode);
            json_object_object_add(jobj, ST_STA_MODE, stamode);
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            Send_Signal(sessionId, ST_WIFI_SETTINGS, (char *)json_object_to_json_string(jobj), NUM_ARGS_WIFI_SETTINGS);
            json_object_put(jobj);
        }
        else
        {
            if( ssid && strlen(ssid) &&
                password && strlen(password) &&
                encryption && strlen(encryption) &&
                channel && strlen(channel)
                )
            {
                if(!strcmp(mode, ST_AP))
                {
                    VR_(write_option)(NULL, UCI_HUB_NAME"=%s", ssid);
                    VR_(write_option)(NULL, UCI_HUB_ENCRYPTION"=%s", encryption);
                    VR_(write_option)(NULL, UCI_HUB_KEY"=%s", password);
                    VR_(write_option)(NULL, UCI_HUB_CHANNEL"=%s", channel);
                    if(strlen(ssid) > 32)
                    {
                        char tmp[SIZE_64B];
                        strncpy(tmp, ssid, SIZE_32B);
                        tmp[SIZE_32B] = '\0';
                        VR_(write_option)(NULL, UCI_HUB_DEFAULT_NAME"=%s", tmp);
                    }
                    else
                    {
                        VR_(write_option)(NULL, UCI_HUB_DEFAULT_NAME"=%s", ssid);
                    }
                    VR_(write_option)(NULL, UCI_ALLJOYN_ONBOARDING_APENCRYPTION"=%s", encryption);
                    VR_(write_option)(NULL, UCI_ALLJOYN_ONBOARDING_APKEY"=%s", password);
                }
                else if(!strcmp(mode, ST_STA))
                {
                    VR_(write_option)(NULL, UCI_ALLJOYN_ONBOARDING_SSID"=%s", ssid);
                    VR_(write_option)(NULL, UCI_ALLJOYN_ONBOARDING_ENCRYPTION"=%s", encryption);
                    VR_(write_option)(NULL, UCI_ALLJOYN_ONBOARDING_KEY"=%s", password);
                    VR_(write_option)("/etc/alljoyn-onboarding", UCI_ALLJOYN_ONBOARDING_STATE"=1");
                    VR_(write_option)(NULL, UCI_CLOUD_UPDATE_SSID"=%s", ST_FALSE);
                    VR_(execute_system)("ubus send onboarding '{\"state\":\"configured\"}' &");
                }
                else
                {
                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    json_object_object_add(jobj, ST_REASON, json_object_new_string("wrong mode"));
                    Send_Signal(sessionId, ST_WIFI_SETTINGS, (char *)json_object_to_json_string(jobj), NUM_ARGS_WIFI_SETTINGS);
                    json_object_put(jobj);
                }

                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
                json_object_object_add(jobj, ST_MODE, json_object_new_string(mode));
                json_object_object_add(jobj, ST_SSID, json_object_new_string(ssid));
                json_object_object_add(jobj, ST_PASSWORD, json_object_new_string(password));
                json_object_object_add(jobj, ST_ENCRYPTION, json_object_new_string(encryption));
                json_object_object_add(jobj, ST_CHANNEL, json_object_new_string(channel));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                Send_Signal(sessionId, ST_WIFI_SETTINGS, (char *)json_object_to_json_string(jobj), NUM_ARGS_WIFI_SETTINGS);
                json_object_put(jobj);

                if(!strcmp(mode, ST_STA))
                {
                    VR_(execute_system)("/etc/init.d/alljoyn-onboarding restart");
                }
            }
            else
            {
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
                Send_Signal(sessionId, ST_WIFI_SETTINGS, (char *)json_object_to_json_string(jobj), NUM_ARGS_WIFI_SETTINGS);
                json_object_put(jobj);
            }
        }     
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WIFI_SETTINGS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_WIFI_SETTINGS, (char *)json_object_to_json_string(jobj), NUM_ARGS_WIFI_SETTINGS);
        json_object_put(jobj);
    }
}

void venus_alarm(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("venus_alarm\n");
    QStatus status;
    char *userId, *devId, *serviceName, *value;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &userId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: userId\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &serviceName);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: serviceName\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &devId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: devId\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &value);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: value\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    SLOGI("userId = %s\n", userId);
    SLOGI("serviceName = %s\n", serviceName);
    SLOGI("devId = %s\n", devId);
    SLOGI("value = %s\n", value);
    if(!userId || !strlen(userId) ||
        !serviceName || !strlen(serviceName) ||
        !devId || !strlen(devId) ||
        !value || !strlen(value)
        )
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_VENUS_ALARM_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_VENUS_ALARM, (char *)json_object_to_json_string(jobj), NUM_ARGS_VENUS_ALARM);
        json_object_put(jobj);
    }

#ifdef SHARE_USER_LIMIT
    char *local_state = (char *)VR_(read_option)(NULL, UCI_LOCAL_STATE);
    if(local_state)
    {
        if(!strcmp(local_state, ST_CONNECTED))
        {
            SEARCH_DATA_INIT_VAR(usertype);
            searching_database("alljoyn_handler", user_db, get_last_data_cb, &usertype,
                        "SELECT userType from USERS where userId='%s'", userId);

            if(usertype.len && strcmp(usertype.value, ST_ADMIN))
            {
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_VENUS_ALARM_R));
                json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
                json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(devId));
                json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("you are not allow to do this"));
                Send_Signal(sessionId, ST_VENUS_ALARM, (char *)json_object_to_json_string(jobj), NUM_ARGS_VENUS_ALARM);
                json_object_put(jobj);
                FREE_SEARCH_DATA_VAR(usertype);
                return;
            }
            FREE_SEARCH_DATA_VAR(usertype);
        }
        free(local_state);
    }
#endif

    int i;
    new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
    memset(allubus, 0x00, sizeof(new_allubus));

    strcpy(allubus->name, ST_VENUS_ALARM);
    allubus->id = sessionId;
    allubus->type = ALL_SESSION;
    allubus->numArgs = NUM_ARGS_VENUS_ALARM;
    add_last_list(allubus);

    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(serviceName, daemon_services[i].name))
        {
            allubus->data = (void*)daemon_services[i].name;
            if( daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, devId);
                blobmsg_add_string(&buff, ST_VALUE, value);

                allubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                allubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_VENUS_ALARM, buff.head, req,
                                        receive_data_cb, complete_data_cb, (void *) allubus);
                break;
            }
        }
    }
}

void venus_alarm_v2(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("venus_alarm version 2\n");
    QStatus status;
    char *userId, *devId, *serviceName, *value, *temporaryTime;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &userId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: userId\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &serviceName);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: serviceName\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &devId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: devId\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &value);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: value\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 4), "s", &temporaryTime);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: temporaryTime\n");
        return;
    }
    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    SLOGI("userId = %s\n", userId);
    SLOGI("serviceName = %s\n", serviceName);
    SLOGI("devId = %s\n", devId);
    SLOGI("value = %s\n", value);
    SLOGI("temporaryTime = %s\n", temporaryTime);
    if(!userId || !strlen(userId) ||
        !serviceName || !strlen(serviceName) ||
        !devId || !strlen(devId) ||
        !value || !strlen(value)
        )
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_VENUS_ALARM_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_VENUS_ALARM_V2, (char *)json_object_to_json_string(jobj), NUM_ARGS_VENUS_ALARM_V2);
        json_object_put(jobj);
    }

#ifdef SHARE_USER_LIMIT
    char *local_state = (char *)VR_(read_option)(NULL, UCI_LOCAL_STATE);
    if(local_state)
    {
        if(!strcmp(local_state, ST_CONNECTED))
        {
            SEARCH_DATA_INIT_VAR(usertype);
            searching_database("alljoyn_handler", user_db, get_last_data_cb, &usertype,
                        "SELECT userType from USERS where userId='%s'", userId);

            if(usertype.len && strcmp(usertype.value, ST_ADMIN))
            {
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_VENUS_ALARM_R));
                json_object_object_add(jobj, ST_USER_ID, json_object_new_string(userId));
                json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(devId));
                json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
                if(temporaryTime)
                {
                    json_object_object_add(jobj, ST_TEMPORARY_TIME, json_object_new_string(temporaryTime));
                }
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("you are not allow to do this"));
                Send_Signal(sessionId, ST_VENUS_ALARM_V2, (char *)json_object_to_json_string(jobj), NUM_ARGS_VENUS_ALARM_V2);
                json_object_put(jobj);
                FREE_SEARCH_DATA_VAR(usertype);
                return;
            }
            FREE_SEARCH_DATA_VAR(usertype);
        }
        free(local_state);
    }
#endif

    int i;
    new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
    memset(allubus, 0x00, sizeof(new_allubus));

    strcpy(allubus->name, ST_VENUS_ALARM_V2);
    allubus->id = sessionId;
    allubus->type = ALL_SESSION;
    allubus->numArgs = NUM_ARGS_VENUS_ALARM_V2;
    add_last_list(allubus);

    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(serviceName, daemon_services[i].name))
        {
            allubus->data = (void*)daemon_services[i].name;
            if( daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, devId);
                blobmsg_add_string(&buff, ST_VALUE, value);
                if(temporaryTime)
                {
                    blobmsg_add_string(&buff, ST_TEMPORARY_TIME, temporaryTime);
                }

                allubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                allubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_VENUS_ALARM_V2, buff.head, req,
                                        receive_data_cb, complete_data_cb, (void *) allubus);
                break;
            }
        }
    }
}

void scene_activation(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("scene_activation\n");
    QStatus status;
    char *userId, *devId, *serviceName, *buttonId;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &userId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: userId\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &serviceName);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: serviceName\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &devId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: devId\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &buttonId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: buttonId\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    SLOGI("userId = %s\n", userId);
    SLOGI("serviceName = %s\n", serviceName);
    SLOGI("devId = %s\n", devId);
    SLOGI("buttonId = %s\n", buttonId);
    if(!userId || !strlen(userId) ||
        !serviceName || !strlen(serviceName) ||
        !devId || !strlen(devId) ||
        !buttonId || !strlen(buttonId)
        )
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SCENE_ACTIVATION_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_SCENE_ACTIVATION, (char *)json_object_to_json_string(jobj), NUM_ARGS_SCENE_ACTIVATION);
        json_object_put(jobj);
    }

    int i;
    new_allubus *allubus = (new_allubus*)malloc(sizeof(new_allubus));
    memset(allubus, 0x00, sizeof(new_allubus));

    strcpy(allubus->name, ST_SCENE_ACTIVATION);
    allubus->id = sessionId;
    allubus->type = ALL_SESSION;
    allubus->numArgs = NUM_ARGS_SCENE_ACTIVATION;
    add_last_list(allubus);

    for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp(serviceName, daemon_services[i].name))
        {
            allubus->data = (void*)daemon_services[i].name;
            if( daemon_services[i].id != 0)
            {
                blob_buf_init(&buff, 0);
                blobmsg_add_string(&buff, ST_ID, devId);
                blobmsg_add_string(&buff, ST_BUTTON_ID, buttonId);

                allubus->action_time = (unsigned)time(NULL);

                struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                allubus->req=req;

                VR_ubus_invoke_async(ctx, daemon_services[i].id, ST_SCENE_ACTIVATION, buff.head, req,
                                        receive_data_cb, complete_data_cb, (void *) allubus);
                break;
            }
        }
    }

    // scene_infor *scene_info = (scene_infor*)malloc(sizeof(scene_infor));
    // snprintf(scene_info->deviceId, sizeof(scene_info->deviceId), "%s", devId);
    // snprintf(scene_info->buttonId, sizeof(scene_info->buttonId), "%s", buttonId);

    // pthread_t active_scene_t;
    // pthread_create(&active_scene_t, NULL, (void *)&active_scene_thread, (void *)scene_info);
    // pthread_detach(active_scene_t);
}

void scene_actions(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("scene_actions\n");
    QStatus status;
    char *userId, *devId, *action, *buttonId, *sceneId;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &userId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: userId\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &action);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: serviceName\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 2), "s", &devId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: devId\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 3), "s", &buttonId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: buttonId\n");
        return;
    }
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 4), "s", &sceneId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: buttonId\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    SLOGI("userId = %s\n", userId);
    SLOGI("action = %s\n", action);
    SLOGI("devId = %s\n", devId);
    SLOGI("buttonId = %s\n", buttonId);
    if(!userId || !strlen(userId) ||
        !action || !strlen(action) ||
        !devId || !strlen(devId) ||
        !buttonId || !strlen(buttonId)
        )
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SCENE_ACTIONS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        Send_Signal(sessionId, ST_SCENE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_SCENE_ACTIONS);
        json_object_put(jobj);

        return;
    }

    if(!strcmp(action, ST_PAIR))
    {
        update_senceId_database("alljoyn_handler", dev_db, devId, buttonId, sceneId);
    }
    else if(!strcmp(action, ST_UNPAIR))
    {
        update_senceId_database("alljoyn_handler", dev_db, devId, buttonId, "");
    }

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SCENE_ACTIONS_R));
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(devId));
    json_object_object_add(jobj, ST_ACTION, json_object_new_string(action));
    json_object_object_add(jobj, ST_BUTTON_ID, json_object_new_string(buttonId));
    json_object_object_add(jobj, ST_SCENE_ID, json_object_new_string(sceneId));
    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

    Broadcast_message(ST_SCENE_ACTIONS, (char *)json_object_to_json_string(jobj), NUM_ARGS_SCENE_ACTIONS);
    json_object_put(jobj);
}

static int list_status_callback(void *data, int argc, char **argv, char **azColName)
{
    if(!data)
    {
        return 0;
    }

    int i;
    char deviceId[SIZE_64B];
    char deviceType[SIZE_64B];

    status_data *tmp = (status_data*)data;

    for(i=0; i<argc; i++)
    {
        if(!strcmp(azColName[i], ST_ID) || !strcmp(azColName[i], ST_UDN))
        {
            strcpy(deviceId, argv[i]);
        }

        if(!strcmp(azColName[i], ST_TYPE))
        {
            strcpy(deviceType, argv[i]);
        }
    }

    json_object *jobj = json_object_new_object();
    json_object *status = json_object_new_object();

    json_object *capabilityObj = json_object_new_array();
    searching_database("alljoyn_handler", (sqlite3 *)tmp->db, capability_callback, (void *)capabilityObj,
                    "SELECT %s,%s,%s from CAPABILITY where deviceId='%s'",
                    ST_SCHEME, ST_CAP_LIST, ST_ENDPOINT_MEM, deviceId);

    searching_database("alljoyn_handler", (sqlite3*)tmp->db, features_callback, (void *)status,
                        "SELECT %s,%s from FEATURES where %s='%s'"
                        , ST_FEATURE_ID, ST_REGISTER,
                        ST_DEVICE_ID, deviceId);

    get_alarm_data((sqlite3 *)tmp->db, status, deviceId);
    int association=0, meter=0, sensorMultilevel=0, userCode=0, thermostatSetpoint=0;
    get_feature_device_support(capabilityObj, deviceType, &association,
                      &meter, &sensorMultilevel, &userCode, &thermostatSetpoint);
    if(association)
    {
        get_association_data((sqlite3 *)tmp->db, status, deviceId);
    }

    if(meter)
    {
        get_meter_data((sqlite3 *)tmp->db, status, deviceId);
    }

    if(sensorMultilevel)
    {
        get_sensor_multilevel_data((sqlite3 *)tmp->db, status, deviceId);
    }

    if(userCode)
    {
        get_door_lock_user_code((sqlite3 *)tmp->db, status, deviceId);
    }

    if(thermostatSetpoint)
    {
        get_thermostat_setpoint_data((sqlite3 *)tmp->db, status, deviceId);
    }

    json_object_object_add(jobj, ST_ID, json_object_new_string(deviceId));
    json_object_object_add(jobj, ST_STATUS, status);
    json_object_array_add((json_object*)tmp->object, jobj);
    json_object_put(capabilityObj);
    return 0;
}
/*support get end device status*/
void get_status(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("get_status\n");
    QStatus status;
    char *type = NULL;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &type);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: type\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    if(!type)
    {
        json_object *jobj = json_object_new_object();
        // json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ALL));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_STATUS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("mising infor"));
        Send_Signal(sessionId, ST_GET_STATUS, (char *)json_object_to_json_string(jobj), NUM_GET_STATUS);
        json_object_put(jobj);
        return;
    }

    char timestamp[SIZE_32B];
    unsigned int current_time = (unsigned)time(NULL);
    sprintf(timestamp, "%u", current_time);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(type));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_STATUS_R));
    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    json_object_object_add(jobj, ST_CLOUD_TIME_STAMP, json_object_new_string(timestamp));
    json_object *devicelist = json_object_new_array();

    char cmd[SIZE_256B];
    char database_file[SIZE_256B];
    snprintf(database_file, sizeof(database_file), TEMP_DEVICES_DATABASE, current_time);
    snprintf(cmd, sizeof(cmd), "cp %s %s", DEVICES_DATABASE, database_file);
    VR_(execute_system)(cmd);

    sqlite3 *tmp_dev_db;

    int rc = VR_(get_file_infor)(database_file);

    if(rc == -1)
    {
        tmp_dev_db = dev_db;
    }
    else
    {
        rc = open_database(database_file, &tmp_dev_db);
        if(rc == -1)
        {
            tmp_dev_db = dev_db;
        }
    }

    status_data data;
    data.object = (void*)devicelist;
    data.db = (void*)tmp_dev_db;

    DISABLE_DATABASE_LOG();

    if(!strcmp(type, ST_ALL))
    {
        searching_database("alljoyn_handler", tmp_dev_db, list_status_callback, (void *)&data,
                    "SELECT udn from CONTROL_DEVS where type='%s'", ST_UPNP);

        searching_database("alljoyn_handler", tmp_dev_db, list_status_callback, (char *)&data,
                    "SELECT %s,%s from SUB_DEVICES ", ST_ID, ST_TYPE);
    }
    else
    {
        //upnp has onOff status only.
        searching_database("alljoyn_handler", tmp_dev_db, list_status_callback, (void *)&data,
                    "SELECT udn from CONTROL_DEVS where type='%s'", ST_UPNP);

        searching_database("alljoyn_handler", tmp_dev_db, list_status_callback, (char *)&data,
                    "SELECT id,type from SUB_DEVICES where id in"
                    "(select distinct deviceId from FEATURES where featureId!='lastUpdate'"
                    "and deviceId not in (select distinct deviceId from FEATURES where featureId='lastUpdate'))");

        searching_database("alljoyn_handler", tmp_dev_db, list_status_callback, (char *)&data,
                    "SELECT id,type from SUB_DEVICES where id in"
                    "(select distinct deviceId from FEATURES where featureId='lastUpdate'"
                    "and register>%s)", type);
    }

    ENABLE_DATABASE_LOG();
    if(rc != -1)
    {
        close_database(&tmp_dev_db);
    }

    sprintf(cmd, "rm %s", database_file);
    VR_(execute_system)(cmd);

    json_object_object_add(jobj, ST_DEVICES_LIST, devicelist);
    Send_Signal(sessionId, ST_GET_STATUS, (char *)json_object_to_json_string(jobj), NUM_GET_STATUS);
    json_object_put(jobj);
}

/*support get token*/
void get_token(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    QStatus status;
    char *userId = NULL;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &userId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: userId\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    char *remoteState = (char *)VR_(read_option)(NULL, UCI_CLOUD_STATE);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_TOKEN_R));

    if(!userId)
    {
        goto tokenFailed;
    }

    if(!remoteState || !strcmp(remoteState, ST_CONNECTED))
    {
        goto tokenFailed;
    }

    SEARCH_DATA_INIT_VAR(userType);
    searching_database("alljoyn_handler", user_db, get_last_data_cb, &userType,
                        "SELECT userType from USERS where userId='%s'", userId);
    if(strcmp(userType.value, ST_ADMIN))
    {
        FREE_SEARCH_DATA_VAR(userType);
        goto tokenFailed;
    }
    FREE_SEARCH_DATA_VAR(userType);

    char *claimToken = (char *)VR_(read_option)(NULL, UCI_LOCAL_CLAIM_TOKEN);
    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    json_object_object_add(jobj, ST_CLAIM_TOKEN, json_object_new_string(claimToken?claimToken:ST_UNKNOWN));
    Send_Signal(sessionId, ST_GET_TOKEN, (char *)json_object_to_json_string(jobj), NUM_GET_TOKEN);
    json_object_put(jobj);
    SAFE_FREE(claimToken);
    return;

tokenFailed:
    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
    json_object_object_add(jobj, ST_REASON, json_object_new_string("you are not allow to do this"));
    Send_Signal(sessionId, ST_GET_TOKEN, (char *)json_object_to_json_string(jobj), NUM_GET_TOKEN);
    json_object_put(jobj);
    SAFE_FREE(remoteState);
    return;
}

static void setup_hub_thread(void *data)
{
    if(data)
    {
        handle_setup_hub(g_msgBus, data);
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SETUP_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
        json_object_object_add(jobj, ST_HUBS_LIST, data);
        Broadcast_message(ST_SETUP, (char *)json_object_to_json_string(jobj), NUM_SETUP);
        json_object_put(jobj);
    }
}

void setup_hub(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    QStatus status;
    char *hubInput = NULL;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &hubInput);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: hubInput\n");
        return;
    }

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    json_object *hubsList = VR_(create_json_object)(hubInput);
    if(!hubsList)
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SETUP_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("missing input"));
        Send_Signal(sessionId, ST_SETUP, (char *)json_object_to_json_string(jobj), NUM_SETUP);
        json_object_put(jobj);
        return;
    }

    pthread_t setup_hub_thread_t;
    pthread_create(&setup_hub_thread_t, NULL, (void *)&setup_hub_thread, hubsList);
    pthread_detach(setup_hub_thread_t);

    return;
}

/*support get hub state*/
void get_state(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("get_state\n");
    char *state = NULL;
    char *message = NULL;

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_STATE_R));

    get_hub_state(&state, &message);

    JSON_ADD_STRING_SAFE(jobj, ST_STATE, state);
    JSON_ADD_STRING_SAFE(jobj, ST_MESSAGE, message);
    Send_Signal(sessionId, ST_GET_STATE, (char *)json_object_to_json_string(jobj), NUM_GET_STATE);
    json_object_put(jobj);

    SAFE_FREE(state);
    SAFE_FREE(message);
}

void change_state(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    SLOGI("change_state\n");
    int retry = 3;
    char *remote_state = (char *)VR_(read_option)(NULL, UCI_CLOUD_STATE);
    if(remote_state)
    {
        if(!strcmp(remote_state, "registered"))
        {
            while(retry && strcmp(remote_state, ST_CLAIMED))
            {
                sleep(1);
                free(remote_state);
                remote_state = (char *)VR_(read_option)(NULL, UCI_CLOUD_STATE);
                retry --;
            }
        }
        
        if(!strcmp(remote_state, ST_CLAIMED))
        {
            update_about_deviceId(SERVICE_PORT, &aboutData, "6");
            VR_(write_option)(NULL, UCI_WIFI_AP_MODE_DISABLED"=1");
            VR_(execute_system)("wifi");
            VR_(execute_system)("/etc/init.d/ble restart &");
        }
    }

    SAFE_FREE(remote_state);
}

/* Exposed concatenate method */
void AJ_CALL ping_handle(alljoyn_busobject bus, const alljoyn_interfacedescription_member* member, alljoyn_message msg)
{
    // SLOGI("receive ping\n");
    QStatus status;
    alljoyn_msgarg outArg;
    outArg = alljoyn_msgarg_create_and_set("s", "ok");
    status = alljoyn_busobject_methodreply_args(bus, msg, outArg, 1);
    if (ER_OK != status) {
        printf("Ping: Error sending reply\n");
    }
    alljoyn_msgarg_destroy(outArg);
}

void AJ_CALL request_access_method_handle(alljoyn_busobject bus, const alljoyn_interfacedescription_member* member, alljoyn_message msg)
{
    SLOGI("request_access\n");

    QStatus status;
    alljoyn_msgarg outArg;
    char *userId;

    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &userId);
    if (ER_OK != status) {
        SLOGE("Error reading alljoyn_message: userId\n");
        return;
    }

    SLOGI("userId = %s\n", userId);
    if(userId && strlen(userId))
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_REQUEST_ACCESS_R));

        SEARCH_DATA_INIT_VAR(status);
        searching_database("alljoyn_handler", user_db, get_last_data_cb, &status,
                    "SELECT enable from USERS where userId='%s'", userId);
        if(!strcmp(status.value, ST_ENABLE))
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            char b64_encode[SIZE_256B];
            psk_encrypt(userId, g_psk, b64_encode);
            json_object_object_add(jobj, ST_PSK_ENCRYPT, json_object_new_string(b64_encode));

            // database_actions("alljoyn_handler", user_db,
            //             "UPDATE USERS set SessionId='%u' where UserId='%s'", sessionId, userId);
        }
        else
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        }

        outArg = alljoyn_msgarg_create_and_set("s", (char *)json_object_to_json_string(jobj));
        alljoyn_busobject_methodreply_args(bus, msg, outArg, 1);
        FREE_SEARCH_DATA_VAR(status);
        json_object_put(jobj);
    }
    else
    {
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_REQUEST_ACCESS_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("FAILED to get arguments"));
        outArg = alljoyn_msgarg_create_and_set("s", (char *)json_object_to_json_string(jobj));
        alljoyn_busobject_methodreply_args(bus, msg, outArg, 1);
        json_object_put(jobj);
    }

    alljoyn_msgarg_destroy(outArg);
}

QStatus CreateInterface()
{
    QStatus status;
    alljoyn_interfacedescription Control_Interface = NULL;
    status = alljoyn_busattachment_createinterface_secure(g_msgBus, CONTROL_INTF, &Control_Interface, AJ_IFC_SECURITY_REQUIRED);
    if (status == ER_OK) 
    {
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_NOTIFY, "s",  NULL, "inStr", 0);

        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_SET_TIME, "ss",  NULL, "inStr1,inStr2", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_GET_TIME, "s",  NULL, "inStr", 0);
        //userid, groupid, group name, deviceid1, devicetype1, deviceid2, devicetype2
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_CREATE_GROUP, "ssss",  NULL, "userId,appId,groupName,groupDev", 0);
        //action:add/remove/changename, userid, groupid, deviceid, devicetype
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_GROUP_ACTIONS, "sssss",  NULL, "inStr1,inStr2,inStr3,inStr4,inStr5", 0);
        //userid, groupid, command, value
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_GROUP_CONTROL, "ssss",  NULL, "inStr1,inStr2,inStr3,inStr4", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_LIST_GROUPS, "s",  NULL, "return", 0);

        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_AUTO_CONF, "ss",  NULL, "inStr1,inStr2", 0);

        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_SEND_REPORT, "ss",  NULL, "inStr1,inStr2", 0);

        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_ADD_DEVICES, "sss",  NULL, "inStr1,inStr2,inStr3", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_LIST_DEVICES, "s",  NULL, "inStr", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_REMOVE_DEVICE, "sss",  NULL, "inStr1,inStr2,inStr3", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_RESET, "ss",  NULL, "admin,actions", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_REDISCOVER, "ss",  NULL, "inStr1,inStr2", 0);

        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_OPEN_CLOSE_NETWORK, "sss",  NULL, "inStr1,inStr2,inStr3", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_GET_SUB_DEVS, "sss",  NULL, "inStr1,inStr2,inStr3", 0);

        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_NETWORK, "sssssss",  NULL, "inStr1,inStr2,inStr3,inStr4,inStr5,inStr6,inStr7", 0);

        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_GET_BINARY, "sss",  NULL, "inStr1,inStr2,inStr3", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_SET_BINARY, "sssss",  NULL, "inStr1,inStr2,inStr3,inStr4,inStr5", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_CHANGE_NAME, "ssss",  NULL, "servicename,id,value,subdevID", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_GET_NAME, "sss",  NULL, "servicename,id,subdevID", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_ALEXA, "ssss",  NULL, "inStr1,inStr2,inStr3,inStr4", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_IDENTIFY, "ssss",  NULL, "inStr1,inStr2,inStr3,inStr4", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_FIRMWARE_ACTIONS, "ss",  NULL, "inStr1,inStr2", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_SET_RULE, "ssss",  NULL, "inStr1,inStr2,inStr3,inStr4", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_GET_RULE, "s",  NULL, "inStr", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_RULE_ACTIONS, "ssssss",  NULL, "inStr1,inStr2,inStr3,inStr4,inStr5,inStr6", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_RULE_MANUAL, "ss",  NULL, "inStr1,inStr2", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_VENUS_ALARM, "ssss",  NULL, "userId,serviceName,devId,value", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_VENUS_ALARM_V2, "sssss",  NULL, "userId,serviceName,devId,value,temporaryTime", 0);

        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_SCENE_ACTIVATION, "ssss",  NULL, "userId,serviceName,devId,buttonId", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_SCENE_ACTIONS, "sssss",  NULL, "userId,action,devId,buttonId,sceneId", 0);

        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_READ_SPEC, "sssssss",  NULL, "inStr1,inStr2,inStr3,inStr4,inStr5,inStr6,inStr7", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_WRITE_SPEC, "sssssss",  NULL, "inStr1,inStr2,inStr3,inStr4,inStr5,inStr6,inStr7", 0);

        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_READ_SPEC_CRC, "ssssssssss",  NULL, "inStr1,inStr2,inStr3,inStr4,inStr5,inStr6,inStr7,inStr8,inStr9,inStr10", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_WRITE_SPEC_CRC, "ssssssssss",  NULL, "inStr1,inStr2,inStr3,inStr4,inStr5,inStr6,inStr7,inStr8,inStr9,inStr10", 0);

        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_READ_S_SPEC, "sssss",  NULL, "inStr1,inStr2,inStr3,inStr4,inStr5", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_WRITE_S_SPEC, "sssssss",  NULL, "inStr1,inStr2,inStr3,inStr4,inStr5,inStr6,inStr7", 0);

        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_SHARE_ACCESS, "sssss",  NULL, "adminId,userId,shareId,userName,expiration", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_DENY_ACCESS, "ss",  NULL, "admin,user", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_LIST_USERS, "s",  NULL, "users", 0);

        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_WIFI_SETTINGS, "ssssss",  NULL, "admin,mode,ssid,password,encryption,channel", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_GET_STATUS, "s",  NULL, "type", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_GET_TOKEN, "s",  NULL, "admin", 0);
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_SETUP, "s",  NULL, "hubsList", 0);

/// support styx ///////////////
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_METHOD_CALL, ST_PING, "s",  "s", "inStr1,outstr1", 0); //trigger secure
        alljoyn_interfacedescription_addmember(Control_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_STORE, "s",  NULL, "inStr1", 0); //send store command

        alljoyn_interfacedescription_activate(Control_Interface);
        SLOGI("Control_Interface Created\n");
    } 
    else 
    {
        SLOGW("Failed to create interface %s\n", CONTROL_INTF);
    }

    alljoyn_interfacedescription Manage_Interface = NULL;
    status = alljoyn_busattachment_createinterface(g_msgBus, MANAGE_INTF, &Manage_Interface);
    if (status == ER_OK)
    {
        alljoyn_interfacedescription_addmember(Manage_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_REQUEST_ACCESS, "s",  NULL, "inStr1", 0);
        alljoyn_interfacedescription_addmember(Manage_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_OB_REQUEST_ACCESS, "s",  NULL, "userID", 0);
        alljoyn_interfacedescription_addmember(Manage_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_PUBLISH, "s",  NULL, ST_UBUS_MESSAGE_RETURN_KEY, 0);
        alljoyn_interfacedescription_addmember(Manage_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_GET_STATE, "s",  NULL, "inStr", 0);
        alljoyn_interfacedescription_addmember(Manage_Interface, ALLJOYN_MESSAGE_SIGNAL, ST_CHANGE_STATE, "s",  NULL, "inStr", 0);

        alljoyn_interfacedescription_addmember(Manage_Interface, ALLJOYN_MESSAGE_METHOD_CALL, ST_REQUEST_ACCESS_2, "s",  "s", "inStr1,outstr1", 0);
        alljoyn_interfacedescription_activate(Manage_Interface);
    }
    else 
    {
        SLOGW("Failed to create interface %s\n", MANAGE_INTF);
    }
    return status;
}

QStatus CreateBusObject(const char* path)
{
    QStatus status = ER_OK;
    QCC_BOOL foundMember = QCC_FALSE;
    alljoyn_interfacedescription ControlIntf = NULL;
    alljoyn_interfacedescription_member list_devices_member;
    alljoyn_interfacedescription_member set_binary_member;
    alljoyn_interfacedescription_member get_binary_member;
    alljoyn_interfacedescription_member add_devices_member;
    alljoyn_interfacedescription_member firmware_actions_member;
    // alljoyn_interfacedescription_member set_rule_member;
    // alljoyn_interfacedescription_member get_rule_member;
    // alljoyn_interfacedescription_member rule_actions_member;
    alljoyn_interfacedescription_member rule_manual_member;
    alljoyn_interfacedescription_member read_spec_member;
    alljoyn_interfacedescription_member write_spec_member;
    alljoyn_interfacedescription_member read_secure_spec_member;
    alljoyn_interfacedescription_member write_secure_spec_member;
    alljoyn_interfacedescription_member remove_device_member;
    alljoyn_interfacedescription_member reset_device_member;
    alljoyn_interfacedescription_member open_close_network_member;
    alljoyn_interfacedescription_member get_sub_devices_member;
    alljoyn_interfacedescription_member identify_devices_member;
    alljoyn_interfacedescription_member change_name_device_member;
    alljoyn_interfacedescription_member get_name_device_member;
    alljoyn_interfacedescription_member alexa_support_member;
    alljoyn_interfacedescription_member set_time_member;
    alljoyn_interfacedescription_member get_time_member;
    alljoyn_interfacedescription_member auto_conf_member;
    alljoyn_interfacedescription_member rediscover_member;
    alljoyn_interfacedescription_member create_group_member;
    alljoyn_interfacedescription_member list_groups_member;
    alljoyn_interfacedescription_member group_actions_member;
    alljoyn_interfacedescription_member group_control_member;
    alljoyn_interfacedescription_member network_member;
    alljoyn_interfacedescription_member read_spec_CRC_member;
    alljoyn_interfacedescription_member write_spec_CRC_member;
    alljoyn_interfacedescription_member share_access_member;
    alljoyn_interfacedescription_member deny_access_member;
    alljoyn_interfacedescription_member list_users_member;
    alljoyn_interfacedescription_member send_report_member;
    alljoyn_interfacedescription_member wifi_settings_member;
    alljoyn_interfacedescription_member venus_alarm_member;
    alljoyn_interfacedescription_member venus_alarm_v2_member;
    alljoyn_interfacedescription_member ping_member;
    alljoyn_interfacedescription_member scene_member;
    alljoyn_interfacedescription_member scene_actions_member;
    alljoyn_interfacedescription_member get_status_member;
    alljoyn_interfacedescription_member get_token_member;
    alljoyn_interfacedescription_member setup_member;
    // alljoyn_interfacedescription_member store_member;
    // alljoyn_busobject_methodentry methodEntries[] = {
    //     { &list_devices_member, listdevices_method },
    // };

    alljoyn_busobject_callbacks busObjCbs = {
        NULL,
        NULL,
        &busobject_object_registered,
        NULL
    };

    CommonObj = alljoyn_busobject_create(path, QCC_FALSE, &busObjCbs, NULL);
    ControlIntf = alljoyn_busattachment_getinterface(g_msgBus, CONTROL_INTF);
    assert(ControlIntf);
    alljoyn_busobject_addinterface(CommonObj, ControlIntf);
    alljoyn_busobject_setannounceflag(CommonObj, ControlIntf, ANNOUNCED);

    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_LIST_DEVICES, &list_devices_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to list_devices_member of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, listdevices_signal_handle, list_devices_member, NULL);

    SLOGI("name = %s",  alljoyn_interfacedescription_getname(ControlIntf));

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_SET_BINARY, &set_binary_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to setbinary member of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, setbinary_signal_handle, set_binary_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_ADD_DEVICES, &add_devices_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to add devices member of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, adddevices_signal_handle, add_devices_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_REMOVE_DEVICE, &remove_device_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to remove_device member of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, removedevice_signal_handle, remove_device_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_RESET, &reset_device_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to reset member of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, resetdevice_signal_handle, reset_device_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_GET_BINARY, &get_binary_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to getbinary member of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, getbinary_signal_handle, get_binary_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_FIRMWARE_ACTIONS, &firmware_actions_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to update firmware member of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, firmwareactions_signal_handle, firmware_actions_member, NULL);

    // foundMember = QCC_FALSE;
    // foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_SET_RULE, &set_rule_member);
    // assert(foundMember == QCC_TRUE);
    // if (!foundMember)
    // {
    //     SLOGW("Failed to set rule member of interface\n");
    // }

    // alljoyn_busattachment_registersignalhandler(g_msgBus, setrule_signal_handle, set_rule_member, NULL);

    // foundMember = QCC_FALSE;
    // foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_GET_RULE, &get_rule_member);
    // assert(foundMember == QCC_TRUE);
    // if (!foundMember)
    // {
    //     SLOGW("Failed to get rule member of interface\n");
    // }

    // alljoyn_busattachment_registersignalhandler(g_msgBus, getrule_signal_handle, get_rule_member, NULL);

    // foundMember = QCC_FALSE;
    // foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_RULE_ACTIONS, &rule_actions_member);
    // assert(foundMember == QCC_TRUE);
    // if (!foundMember)
    // {
    //     SLOGW("Failed to rule actions of interface\n");
    // }

    // alljoyn_busattachment_registersignalhandler(g_msgBus, ruleactions_signal_handle, rule_actions_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_RULE_MANUAL, &rule_manual_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to rule actions of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, rulemanual_signal_handle, rule_manual_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_READ_SPEC, &read_spec_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to read_spec of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, readspec_signal_handle, read_spec_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_WRITE_SPEC, &write_spec_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to write_spec of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, writespec_signal_handle, write_spec_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_READ_S_SPEC, &read_secure_spec_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to read_s_spec of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, read_s_spec_signal_handle, read_secure_spec_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_WRITE_S_SPEC, &write_secure_spec_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to write_s_spec of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, write_s_spec_signal_handle, write_secure_spec_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_OPEN_CLOSE_NETWORK, &open_close_network_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to open_closenetwork of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, open_close_network, open_close_network_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_GET_SUB_DEVS, &get_sub_devices_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to get_subdevs of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, get_sub_devs, get_sub_devices_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_IDENTIFY, &identify_devices_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to identify_devs of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, identify_devs, identify_devices_member, NULL);


    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_CHANGE_NAME, &change_name_device_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to change_name of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, change_name, change_name_device_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_GET_NAME, &get_name_device_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to change_name of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, get_name, get_name_device_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_ALEXA, &alexa_support_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to alexa of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, alexa, alexa_support_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_SET_TIME, &set_time_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to set time of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, set_time, set_time_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_GET_TIME, &get_time_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to get time of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, get_time, get_time_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_AUTO_CONF, &auto_conf_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to get time of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, auto_conf, auto_conf_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_REDISCOVER, &rediscover_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to rediscover of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, rediscover, rediscover_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_CREATE_GROUP, &create_group_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to create_group of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, create_group, create_group_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_LIST_GROUPS, &list_groups_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to list_groups of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, list_groups, list_groups_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_GROUP_ACTIONS, &group_actions_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to group_actions of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, group_actions, group_actions_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_GROUP_CONTROL, &group_control_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to group_control of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, group_control, group_control_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_NETWORK, &network_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to network_handle of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, network_handle, network_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_READ_SPEC_CRC, &read_spec_CRC_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to read_spec_CRC of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, readspec_CRC_signal_handle, read_spec_CRC_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_WRITE_SPEC_CRC, &write_spec_CRC_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to write_spec_CRC of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, writespec_CRC_signal_handle, write_spec_CRC_member, NULL);

    foundMember = QCC_FALSE;
    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_SHARE_ACCESS, &share_access_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to write_spec_CRC of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, share_access, share_access_member, NULL);

    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_DENY_ACCESS, &deny_access_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to deny_access of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, deny_access, deny_access_member, NULL);

    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_LIST_USERS, &list_users_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to list_users of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, list_users, list_users_member, NULL);

    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_SEND_REPORT, &send_report_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to send_report of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, send_report, send_report_member, NULL);

    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_WIFI_SETTINGS, &wifi_settings_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to wifi_settings of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, wifi_settings, wifi_settings_member, NULL);

    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_VENUS_ALARM, &venus_alarm_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to venus alarm of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, venus_alarm, venus_alarm_member, NULL);

    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_VENUS_ALARM_V2, &venus_alarm_v2_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to venus alarm version 2 of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, venus_alarm_v2, venus_alarm_v2_member, NULL);

    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_SCENE_ACTIVATION, &scene_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to scene_activation of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, scene_activation, scene_member, NULL);

    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_SCENE_ACTIONS, &scene_actions_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to scene_actions of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, scene_actions, scene_actions_member, NULL);

    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_GET_STATUS, &get_status_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to get status of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, get_status, get_status_member, NULL);

    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_GET_TOKEN, &get_token_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to get_token of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, get_token, get_token_member, NULL);

    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_SETUP, &setup_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to setup of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, setup_hub, setup_member, NULL);

    // foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_STORE, &store_member);
    // assert(foundMember == QCC_TRUE);
    // if (!foundMember)
    // {
    //     SLOGW("Failed to venus alarm of interface\n");
    // }

    // alljoyn_busattachment_registersignalhandler(g_msgBus, venus_alarm, store_member, NULL);

    foundMember = alljoyn_interfacedescription_getmember(ControlIntf, ST_PING, &ping_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to ping of interface\n");
    }

    alljoyn_busobject_addmethodhandler(CommonObj, ping_member, ping_handle, NULL);

///////////////////////////////////////////////////////
    alljoyn_interfacedescription ManageIntf = NULL;
    alljoyn_interfacedescription_member request_access_member;
    alljoyn_interfacedescription_member request_access_method_member;
    alljoyn_interfacedescription_member ob_request_access_member;
    alljoyn_interfacedescription_member get_state_member;
    alljoyn_interfacedescription_member change_state_member;

    ManageIntf = alljoyn_busattachment_getinterface(g_msgBus, MANAGE_INTF);
    assert(ManageIntf);
    alljoyn_busobject_addinterface(CommonObj, ManageIntf);
    alljoyn_busobject_setannounceflag(CommonObj, ManageIntf, ANNOUNCED);

    foundMember = alljoyn_interfacedescription_getmember(ManageIntf, ST_REQUEST_ACCESS, &request_access_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to request_access of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, request_access, request_access_member, NULL);


    foundMember = alljoyn_interfacedescription_getmember(ManageIntf, ST_OB_REQUEST_ACCESS, &ob_request_access_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to ob_request_access of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, ob_request_access, ob_request_access_member, NULL);

    foundMember = alljoyn_interfacedescription_getmember(ManageIntf, ST_GET_STATE, &get_state_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to get_state of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, get_state, get_state_member, NULL);

    foundMember = alljoyn_interfacedescription_getmember(ManageIntf, ST_CHANGE_STATE, &change_state_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to change state of interface\n");
    }

    alljoyn_busattachment_registersignalhandler(g_msgBus, change_state, change_state_member, NULL);

    foundMember = alljoyn_interfacedescription_getmember(ManageIntf, ST_REQUEST_ACCESS_2, &request_access_method_member);
    assert(foundMember == QCC_TRUE);
    if (!foundMember)
    {
        SLOGW("Failed to request access method\n");
    }

    alljoyn_busobject_addmethodhandler(CommonObj, request_access_method_member, request_access_method_handle, NULL);


    return status;
}

QStatus RegisterBusObject(alljoyn_busobject busObj)
{
    QStatus status;
    status = alljoyn_busattachment_registerbusobject(g_msgBus, busObj);
    if(ER_OK != status)
    {
        SLOGE("Failed to register bus object. Error: %s\n", QCC_StatusText(status));
    }
    return status;
}

QStatus EnableSecurity()
{
    /*
     * enable security
     * note the location of the keystore file has been specified and the
     * isShared parameter is being set to true. So this keystore file can
     * be used by multiple applications
     */
    QStatus status;
    alljoyn_authlistener_callbacks callbacks = {
        request_credentials,
        NULL,
        NULL,
        authentication_complete
    };
    g_authListener = alljoyn_authlistener_create(&callbacks, NULL);

    /*
     * alljoyn_busattachment_enablepeersecurity function is called by
     * applications that want to use authentication and encryption. This
     * function call must be made after alljoyn_busattachment_start and
     * before calling alljoyn_busattachment_connect.
     *
     * In most situations a per-application keystore file is generated.
     * However, this code specifies the location of the keystore file and
     * the isShared parameter is being set to QCC_TRUE. The resulting
     * keystore file can be used by multiple applications.
     */
    status = alljoyn_busattachment_enablepeersecurity(g_msgBus, "ALLJOYN_ECDHE_PSK", g_authListener,
                                                      "/.alljoyn_keystore/central.ks", QCC_TRUE);
    if (ER_OK != status) 
    {
        printf("alljoyn_busattachment_enablepeersecurity failed (%s)\n", QCC_StatusText(status));
    } 
    else 
    {
        printf("alljoyn_busattachment_enablepeersecurity Successful\n");
    }
    return status;
}

QStatus StartBusAttachment()
{
    QStatus status;
    status = alljoyn_busattachment_start(g_msgBus);
    if (ER_OK == status) 
    {
        SLOGI("BusAttachment started.\n");
    }
    else 
    {
        SLOGE("FAILED to start BusAttachment (%s)\n", QCC_StatusText(status));
        return 1;
    }
    return status;
}

QStatus ConnectBusAttachment()
{
    QStatus status;
    status = alljoyn_busattachment_connect(g_msgBus, NULL);
    if (ER_OK == status) 
    {
        SLOGI("BusAttachment connect succeeded. BusName %s\n", alljoyn_busattachment_getuniquename(g_msgBus));
    } 
    else
    {
        SLOGE("FAILED to connect to router node (%s)\n", QCC_StatusText(status));
    }
    return status;
}

QStatus CreateSession(alljoyn_sessionopts* opts, alljoyn_sessionportlistener* listener)
{
    QStatus status;
    alljoyn_sessionport sessionPort = SERVICE_PORT;

    *opts = alljoyn_sessionopts_create(ALLJOYN_TRAFFIC_TYPE_MESSAGES, QCC_FALSE,
                                   ALLJOYN_PROXIMITY_ANY,
                                   ALLJOYN_TRANSPORT_ANY);
    *listener = create_my_alljoyn_sessionportlistener();
    status = alljoyn_busattachment_bindsessionport(g_msgBus, &sessionPort,
                                                   *opts, *listener);
    if (ER_OK != status) {
        SLOGE("Failed to BindSessionPort (%s)\n", QCC_StatusText(status));
    }
    return status;
}

void restart_service(void *data)
{
    if(!data)
    {
        return;
    }
    int i;
    for (i = 0; i< sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp((char*)data, daemon_services[i].service_name))
        {
            daemon_services[i].timer_restart = 0;
        }
    }
}

static void remove_action_timeout_cb(struct uloop_timeout *timeout)
{
    //printf("remove_action_timeout_cb\n");
    while(remove_actions_timeout_enable)
    {
        new_allubus *tmp = g_allubus;
        if(tmp)
        {
            unsigned int time_now = (unsigned)time(NULL);
            //printf("tmp->action_time = %d\n", tmp->action_time);
            if((time_now - tmp->action_time) > 40)
            {
                //printf("ACTION %s TIME %d WAS TIMEOUT\n", tmp->name, tmp->action_time);
                if(tmp->name && strlen(tmp->name))
                {
                    int i;
                    for (i = 0; i < sizeof(cmd_support)/sizeof(char*); i++)
                    {
                        //printf("cmd_support[i] = %s\n", cmd_support[i]);
                        if(!strcmp(tmp->name, cmd_support[i]) && tmp->data)
                        {
                            char method[256];
                            sprintf(method, "%sR", tmp->name);
                            json_object *jobj = json_object_new_object();
                            json_object_object_add(jobj, ST_TYPE, json_object_new_string((char*)tmp->data));
                            json_object_object_add(jobj, ST_METHOD, json_object_new_string(method));
                            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                            json_object_object_add(jobj, ST_REASON, json_object_new_string("Action timeout"));
                            Send_Signal(tmp->id, tmp->name, (char *)json_object_to_json_string(jobj), tmp->numArgs);
                            json_object_put(jobj);

                            int j;
                            for (j = 0; j< sizeof(daemon_services)/sizeof(daemon_service); j++)
                            {
                                if(!strcmp((char*)tmp->data, daemon_services[j].name))
                                {
                                    daemon_services[j].timeout++;
                                    if(daemon_services[j].timeout>2)
                                    {
                                        if(!daemon_services[j].timer_restart)
                                        {
                                            if((daemon_services[j].restart_time > tmp->action_time) && tmp->action_time != 0)
                                            {
                                                daemon_services[j].timeout = 0;
                                                break;
                                            }
                                            SLOGI("##### ACTION timeout %d, restart service %s #########\n", 
                                                 daemon_services[j].timeout, daemon_services[j].name);
                                            daemon_services[j].timeout = 0;
                                            daemon_services[j].restart_time = (unsigned int)time(NULL);
                                            timerStart(&daemon_services[j].timer_restart, restart_service, 
                                                                            (void*)daemon_services[j].service_name, 
                                                                            10000, TIMER_ONETIME);
                                            char cmd[256], log_location[SIZE_128B]={0};
                                            get_log_location(log_location, sizeof(log_location));

                                            sprintf(cmd, "/etc/start_services.sh start_service %s >> %s", daemon_services[j].service_name, log_location);
                                            VR_(execute_system)((const char*)cmd);
                                            break;
                                        }
                                        else
                                        {
                                            daemon_services[j].timeout = 0;
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                remove_list(tmp);
            }
        }
        sleep(1);
    }
}

static int g_internet_connect_state = 0; //0:not_check, 1:has_internet, 2:not_internet 

static void check_internet_connection_cb(struct uloop_timeout *timeout)
{
    char *local_state = (char *)VR_(read_option)(NULL, UCI_LOCAL_STATE);
    if(local_state && !strcmp(local_state, "available"))
    {
        //use this state to check internet when venus in unsecure mode
        int sta_mode_up = VR_(is_wireless_up)(ST_STA_MODE);
        if(sta_mode_up)
        {
            SLOGI("check_internet_connection\n");
            int res = ping_command("8.8.8.8", 3);
            if(0 == res)
            {
                if(g_internet_connect_state != 1)
                {
                    VR_(execute_system)("ubus send alljoyn '{\"state\":\"has_internet\"}' &");
                    g_internet_connect_state = 1;
                }
            }
            else
            {
                if(g_internet_connect_state != 2)
                {
                    VR_(execute_system)("ubus send alljoyn '{\"state\":\"not_internet\"}' &");
                    g_internet_connect_state = 2;
                }
            }
        }
        uloop_timeout_set(timeout, 3*60*1000);
    }

    if(local_state)
    {
        free(local_state);
        local_state = NULL;
    }
}

// static struct uloop_timeout remove_action_timeout = {
//     .cb = remove_action_timeout_cb,
// };

static struct uloop_timeout check_internet_connection = {
    .cb = check_internet_connection_cb,
};

static int init_ubus_service()
{
    const char *ubus_socket = NULL;

    uloop_init();

    ctx = ubus_connect(ubus_socket);
    if (!ctx) {
        SLOGE("Failed to connect to ubus\n");
        return -1;
    }

    ubus_add_uloop(ctx);

    return 0;
}

static const struct blobmsg_policy service_event_policy[2] = {
    [0] = { .name = "id", .type = BLOBMSG_TYPE_INT32 },
    [1] = { .name = "path", .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy update_supported_event[1] = {
    [0] = { .name = "version", .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy service_state[1] = {
    [0] = { .name = "state", .type = BLOBMSG_TYPE_STRING },
};

static void receive_event(struct ubus_context *ctx, struct ubus_event_handler *ev,
              const char *type, struct blob_attr *msg)
{
    if(!strcmp(type, "ubus.object.add"))
    {
        struct blob_attr *tb[2];
        blobmsg_parse(service_event_policy, ARRAY_SIZE(service_event_policy), tb, blob_data(msg), blob_len(msg));
        uint32_t id;
        int i, ret;
        const char *path = "unknown";

        if(tb[0] && tb[1])
        {
            id = blobmsg_get_u32(tb[0]);
            path = blobmsg_data(tb[1]);
            SLOGI("path = %s id =%u\n", path, id);
            for(i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
            {
                if(!strcmp(daemon_services[i].name, path))
                {
                    if(daemon_services[i].id != 0)
                    {
                        ret = ubus_unsubscribe(ctx, &alljoyn_notify, daemon_services[i].id);
                        SLOGW("UNsubscribe object %s with id %08X: %s\n", daemon_services[i].name, daemon_services[i].id, ubus_strerror(ret));
                    }
                    
                    daemon_services[i].id = id;
                    daemon_services[i].restart_time = (unsigned int)time(NULL);
                    if(daemon_services[i].timer_restart)
                    {
                        timerCancel(&daemon_services[i].timer_restart);
                    }
                    daemon_services[i].timer_restart = 0;
                    alljoyn_notify.remove_cb = notify_handle_remove;
                    alljoyn_notify.cb = receive_notify_cb;
                    ret = ubus_subscribe(ctx, &alljoyn_notify, daemon_services[i].id);
                    SLOGW("Watching object %s with id %08X: %s\n", daemon_services[i].name, daemon_services[i].id, ubus_strerror(ret));
                }
            }
        }
    }
    else if(!strcmp(type, ST_UPDATE_SUPPORTED_DB))
    {
        SLOGI("alljoyn re-open supported database\n");
        if(support_devs_db)
        {
            sqlite3_close(support_devs_db);
        }

        open_database(SUPPORTED_DEVS_DATABASE, &support_devs_db);

        struct blob_attr *tb[1];
        blobmsg_parse(update_supported_event, ARRAY_SIZE(update_supported_event), tb, blob_data(msg), blob_len(msg));

        const char *version = "unknown";
        if(tb[0])
        {
            version = blobmsg_data(tb[0]);
        }
        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ALLJOYN));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
        json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_UPDATE_SUPPORTED_DB));
        json_object_object_add(jobj, ST_VERSION, json_object_new_string(version));
        Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, ST_NOTIFY, (char *)json_object_to_json_string(jobj), NUM_ARGS_NOTIFY);
        json_object_put(jobj);
    }
    else if(!strcmp(type, ST_POWER_CYCLE))
    {
        if(!g_inform_power_cycle)
        {
            SLOGI("ALLJOYN INFORM POWER CYCLE\n");
            g_inform_power_cycle = 1;
        }
    }
    else if(!strcmp(type, "onboarding"))
    {
        struct blob_attr *tb[1];
        blobmsg_parse(service_state, ARRAY_SIZE(service_state), tb, blob_data(msg), blob_len(msg));
        if(!tb[0])
        {
            return;
        }

        const char *state = blobmsg_data(tb[0]);
        if(!strcmp(state, "validating_failed"))
        {
            update_about_state(SERVICE_PORT, &aboutData, "0");

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_STATE_R));
            JSON_ADD_STRING_SAFE(jobj, ST_STATE, "0");
            JSON_ADD_STRING_SAFE(jobj, ST_MESSAGE, "unconfigured");
            Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, ST_GET_STATE, (char *)json_object_to_json_string(jobj), NUM_GET_STATE);
            json_object_put(jobj);
        }
        else if(!strcmp(state, "connected"))
        {
            char *state = NULL, *message = NULL;
            get_hub_state(&state, &message);

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_STATE_R));
            update_about_state(SERVICE_PORT, &aboutData, state);
            JSON_ADD_STRING_SAFE(jobj, ST_STATE, state);
            JSON_ADD_STRING_SAFE(jobj, ST_MESSAGE, message);

            Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, ST_GET_STATE, (char *)json_object_to_json_string(jobj), NUM_GET_STATE);
            json_object_put(jobj);
            SAFE_FREE(state);
            SAFE_FREE(message);
        }
        else if(!strcmp(state, "configured"))
        {
            update_about_state(SERVICE_PORT, &aboutData, "1");
        }
    }
    else if(!strcmp(type, ST_PUBSUB))
    {
        char *msgstr = blobmsg_format_json(msg, true);
        json_object *jobj = VR_(create_json_object)(msgstr);
        if(!jobj)
        {
            SAFE_FREE(msgstr);
            return;
        }

        json_object *stateObj = NULL;
        json_object_object_get_ex(jobj, ST_STATE, &stateObj);
        if(stateObj)
        {
            const char *state = json_object_get_string(stateObj);
            if(!strcmp(state, "claimed"))
            {
                update_about_deviceId(SERVICE_PORT, &aboutData, "6");
            }
            else if(!strcmp(state, "registered"))
            {
                update_about_state(SERVICE_PORT, &aboutData, "6");
            }
            else if(!strcmp(state, "register_failed"))
            {
                update_about_state(SERVICE_PORT, &aboutData, "7");
            }
            else if(!strcmp(state, "connected"))
            {
                update_about_state(SERVICE_PORT, &aboutData, "8");
            }
            else if(!strcmp(state, ST_PSK_CHANGED))
            {
                char *psk = VR_(read_option)(NULL, UCI_LOCAL_PSK);
                if(psk && strcmp(g_psk, psk))
                {
                    strncpy(g_psk, psk, sizeof(g_psk)-1);
                }

                SAFE_FREE(psk);
            }
            goto pubsub_done;
        }

        json_object *hubsListObj = NULL;
        json_object_object_get_ex(jobj, ST_HUBS_LIST, &hubsListObj);
        json_object *hubsList = VR_(create_json_object)((char*)json_object_to_json_string(hubsListObj));

        pthread_t setup_hub_thread_t;
        pthread_create(&setup_hub_thread_t, NULL, (void *)&setup_hub_thread, hubsList);
        pthread_detach(setup_hub_thread_t);

pubsub_done:
        json_object_put(jobj);
        SAFE_FREE(msgstr);
        // char *msgstr = blobmsg_format_json(msg, true);
        // Send_Signal(ALLJOYN_SESSION_ID_ALL_HOSTED, ST_NOTIFY, (char *)msgstr, NUM_ARGS_NOTIFY);
    }
}

static void client_alljoyn()
{ 
    int ret, i;

    ret = ubus_add_object(ctx, &alljoyn_object);
    if (ret) {
        SLOGE("Failed to add_object object: %s\n", ubus_strerror(ret));
        return;
    }

    ret = ubus_register_subscriber(ctx, &alljoyn_notify);
    if (ret)
    {
        SLOGE("Failed to add notify handler: %s\n", ubus_strerror(ret));
    }

    for(i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(ubus_lookup_id(ctx, daemon_services[i].name, &daemon_services[i].id))
        {
            // SLOGI("The ubus service %s is invisible \n", daemon_services[i].name);
        }
        else
        {
            alljoyn_notify.remove_cb = notify_handle_remove;
            alljoyn_notify.cb = receive_notify_cb;
            ret = ubus_subscribe(ctx, &alljoyn_notify, daemon_services[i].id);
            SLOGW("Watching object %s with id %08X: %s\n", daemon_services[i].name, daemon_services[i].id, ubus_strerror(ret));
        }
    }

    memset(&ubus_listener, 0, sizeof(ubus_listener));
    ubus_listener.cb = receive_event;
    ret = ubus_register_event_handler(ctx, &ubus_listener, "ubus.object.add");
    if (ret)
    {
        SLOGE("Failed to register event handler ubus.object.add: %s\n", ubus_strerror(ret));
    }

    ret = ubus_register_event_handler(ctx, &ubus_listener, ST_UPDATE_SUPPORTED_DB);
    if (ret)
    {
        SLOGE("Failed to register event handler %s: %s\n", ST_UPDATE_SUPPORTED_DB, ubus_strerror(ret));
    }

    ret = ubus_register_event_handler(ctx, &ubus_listener, ST_POWER_CYCLE);
    if (ret)
    {
        SLOGE("Failed to register event handler %s: %s\n", ST_POWER_CYCLE, ubus_strerror(ret));
    }

    ret = ubus_register_event_handler(ctx, &ubus_listener, "onboarding");
    if (ret)
    {
        SLOGE("Failed to register event handler onboarding: %s\n", ubus_strerror(ret));
    }

    ret = ubus_register_event_handler(ctx, &ubus_listener, ST_PUBSUB);
    if (ret)
    {
        SLOGE("Failed to register event handler %s: %s\n", ST_PUBSUB, ubus_strerror(ret));
    }

    // uloop_timeout_set(&remove_action_timeout, 1000);

    uloop_timeout_set(&check_internet_connection, 60*1000);

    uloop_run();
}

static void free_ubus_service()
{
    if(ctx)
    {
        ubus_free(ctx);
    }
    uloop_done();

    if(buff.buf)
    {
        free(buff.buf);
    }

    if(rule_db)
        sqlite3_close(rule_db);

    if(dev_db)
        sqlite3_close(dev_db);

    if(support_devs_db)
        sqlite3_close(support_devs_db);

    if(user_db)
        sqlite3_close(user_db);
}

int init_state()
{
    char *local_state = NULL;
    local_state = (char *)VR_(read_option)(NULL, UCI_LOCAL_STATE);
    printf("local_state = %s\n", local_state);
    if(local_state && !strcmp(local_state, "available"))
    {
        free(local_state);
        local_state = NULL;
        // return -1; still run to support request ob password
    }

    if(local_state && !strcmp(local_state, "configured"))
    {
        char psk[SIZE_256B];
        VR_(generate_128bit)(psk);
        int sta_mode_up = VR_(is_wireless_up)(ST_STA_MODE);
        if(sta_mode_up) //STA mode
        {
            VR_(write_option)(NULL, UCI_LOCAL_PSK"=%s", psk);
            VR_(write_option)(NULL, UCI_LOCAL_ONBOARDING_PSK"=%08X", VR_(generate_32bit)());
            VR_(write_option)(NULL, UCI_LOCAL_STATE"=connected");
        }

        char *userUUID = (char *)VR_(read_option)(NULL, UCI_CLOUD_USER_UUID);
        if(userUUID && strlen(userUUID))
        {
            database_actions("alljoyn_handler", user_db, 
                        "DELETE from USERS where userType='admin'");
            database_actions("alljoyn_handler", user_db, 
                        "INSERT INTO USERS (userId,shareId,name,enable,userType) "\
                        "VALUES ('%s','%s','%s','%s','%s')", userUUID,
                        ST_ADMIN, ST_ADMIN, ST_ENABLE, ST_ADMIN);

            free(userUUID);
        }
        else
        {
            free(local_state);
            SLOGI("not found userUUID\n");
            return -1;
        }

        //insert share userId
        char *userId = (char *)VR_(read_option)(NULL, UCI_CLOUD_USER_ID);
        if(userId && strlen(userId))
        {
            database_actions("alljoyn_handler", user_db,
                        "INSERT INTO USERS (userId,shareId,name,enable,userType) "\
                        "VALUES ('%s','%s','%s','%s','%s')", userId,
                        ST_USER, ST_USER, ST_ENABLE, ST_USER);
        }
    }

    if(local_state)
    {
        free(local_state);
    }

    char *psk = (char *)VR_(read_option)(NULL, UCI_LOCAL_PSK);
    if(psk)
    {
        strncpy(g_psk, psk, sizeof(g_psk)-1);
        free(psk);
    }
    else //ensure g_psk always have value
    {
        VR_(generate_128bit)(g_psk);
        VR_(write_option)(NULL, UCI_LOCAL_PSK"=%s", g_psk);
        //VR_(write_option)(NULL,"security.@local-access[0].state=connected");
    }

    SEARCH_DATA_INIT_VAR(admin_id);

    searching_database("alljoyn_handler", user_db, get_last_data_cb, &admin_id, 
                    "SELECT userId from USERS where userType='%s'", ST_ADMIN);

    if(!admin_id.len)
    {
        char *userUUID = (char *)VR_(read_option)(NULL, UCI_CLOUD_USER_UUID);
        if(userUUID && strlen(userUUID))
        {
            if(strcmp(userUUID,"e78a0881b7d947f293d0afe8062f9999"))
            {
                database_actions("alljoyn_handler", user_db, 
                            "INSERT INTO USERS (userId,shareId,name,enable,userType) "\
                            "VALUES ('%s','%s','%s','%s','%s')", userUUID, ST_ADMIN,
                            ST_ADMIN, ST_ENABLE, ST_ADMIN);
            }
            // database_actions("alljoyn_handler", user_db, 
            //             "DELETE from USERS where UserType='admin'");
            free(userUUID);
        }
        else
        {
            SLOGI("not found userUUID\n");
            FREE_SEARCH_DATA_VAR(admin_id);
            return -1;
        }
    }
    FREE_SEARCH_DATA_VAR(admin_id);
    return 0;
}

static void found_new_hub_can_config(void *data)
{
    if(g_timer_inform_hub)
    {
        timerCancel(&g_timer_inform_hub);
    }
    timerStart(&g_timer_inform_hub, inform_new_hub_could_config, NULL, 20000, TIMER_ONETIME);
}

int start_service(char* service_name)
{
    timerInit();
    tls_init();
    init_curl_request_list();

    open_database(USERS_DATABASE, &user_db);
    if(init_state())
    {
        if(user_db)
            sqlite3_close(user_db);
        return -1;
    }
    pthread_mutex_init(&alljoyn_ubus_listMutex, 0);

    open_database(RULE_DATABASE, &rule_db);
    open_database(DEVICES_DATABASE, &dev_db);
    open_database(SUPPORTED_DEVS_DATABASE, &support_devs_db);

    if(shm_init(&g_shm, &g_shmid))
    {
        SLOGI("Failed to init share memory\n");
        return 0;
    }

    if(led_shm_init(&g_led_shm, &g_led_shmid, "/etc/led_control.sh"))
    {
        SLOGI("Failed to init share memory\n");
        return 0;
    }

    // SLOGI("AllJoyn Library version: %s\n", alljoyn_getversion());
    // SLOGI("AllJoyn Library build info: %s\n", alljoyn_getbuildinfo());

    signal(SIGINT, VR_(handle_signal));
    signal(SIGTERM, VR_(handle_signal));
    signal(SIGKILL, VR_(handle_signal));

    QStatus status = alljoyn_init();
    if (ER_OK != status) {
        SLOGE("alljoyn_init failed (%s)\n", QCC_StatusText(status));
        return 1;
    }

/*    if (alljoyn_routerinit() != ER_OK) {
        alljoyn_shutdown();
        return 1;
    }*/

    g_msgBus = alljoyn_busattachment_create(service_name, QCC_TRUE);
    if (!g_msgBus) {
        status = ER_OUT_OF_MEMORY;
    }

    if (ER_OK == status) {
        status = CreateInterface();
    }

    if (ER_OK == status) {
        status = CreateBusObject(OBJECT_PATH);
    }
    else
    {
        SLOGW("CreateInterface failed\n");
    }

    if (ER_OK == status) {
        status = StartBusAttachment();
    }
    else
    {
        SLOGW("CreateBusObject failed\n");
    }

    if (ER_OK == status) {
        status = RegisterBusObject(CommonObj);
    }
    else
    {
        SLOGW("StartBusAttachment failed\n");
    }

    if (ER_OK == status) {
        status = EnableSecurity();
    }
    else
    {
        SLOGW("StartBusAttachment failed\n");
    }

    if (ER_OK == status) {
        status = ConnectBusAttachment();
    }
    else
    {
        SLOGW("RegisterBusObject failed\n");
    }

    if (ER_OK == status) {
        status = CreateSession(&opts, &listener);
    }
    else
    {
        SLOGW("ConnectBusAttachment failed\n");
    }

    if (ER_OK == status) {
        status = create_announce_service(&aboutData, &aboutIcon);
    }
    else
    {
        SLOGW("CreateSession failed\n");
    }

    if (ER_OK == status) {
        status = announce(g_msgBus, SERVICE_PORT, aboutData, aboutIcon);
        char *description;
        alljoyn_aboutdata_getdescription(aboutData, &description, "en");

        SLOGI("description = %s\n", description);
    }
    else
    {
        SLOGW("create_announce_service failed\n");
    }

    connection_data *connect_data = (connection_data *)malloc(sizeof(connection_data));
    memset(connect_data, 0x00, sizeof(connection_data));
    connect_data->busAtt = g_msgBus;
    connect_data->connection_cb = found_new_hub_can_config;

    init_listen_connection(connect_data, &aboutListener);
    scan_interface(g_msgBus, ONBOARDING_INTERFACE);

    pthread_create(&thread_remove_actions_timeout, NULL, (void *) &remove_action_timeout_cb, NULL);
    pthread_detach(thread_remove_actions_timeout);

    pthread_t curl_request_thread;
    pthread_create(&curl_request_thread, NULL, (void *) &request_thread, NULL);
    pthread_detach(curl_request_thread);

    init_ubus_service();
    client_alljoyn();
    SLOGI("++++++++++++ exit alljoyn service +++++++++++++\n");

    stop_scan_interface(g_msgBus, ONBOARDING_INTERFACE);
    free_listen_connection(g_msgBus, &aboutListener);
    free_ubus_service();
    free(connect_data);

    alljoyn_busattachment_stop(g_msgBus);
    alljoyn_busattachment_join(g_msgBus);

    if (opts) 
    {
        alljoyn_sessionopts_destroy(opts);
    }

    if(listener)
    {
        alljoyn_sessionportlistener_destroy(listener);
    }
        
    if(aboutData)
    {
        alljoyn_aboutdata_destroy(aboutData);
        alljoyn_abouticon_destroy(aboutIcon);
    }

    destroy_announce_service();
    if (g_msgBus) 
    {
        alljoyn_busattachment_destroy(g_msgBus);
    }

    if (CommonObj) 
    {
        alljoyn_busobject_destroy(CommonObj);
    }

    if(g_authListener)
    {
        alljoyn_authlistener_destroy(g_authListener);
    }
    //alljoyn_routershutdown();
    alljoyn_shutdown();

    return 0;
}
