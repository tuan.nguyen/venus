/******************************************************************************
 * Copyright (c) 2014, VEriK Systems. All rights reserved.
 *
 * Module: Service Provider
 *
 ******************************************************************************/
#ifndef ALLJOYN_SERVICE_H
#define ALLJOYN_SERVICE_H

#include <assert.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <alljoyn_c/DBusStdDefines.h>
#include <alljoyn_c/BusAttachment.h>
#include <alljoyn_c/version.h>
#include <alljoyn_c/Status.h>

#include <libubus.h>
#include <libubox/blobmsg_json.h>

#include "slog.h"
#include <json-c/json.h>
 
#define CONTROL_INTF "com.verik.bus.VENUS_CONTROL"
#define MANAGE_INTF "com.verik.bus.VENUS_MANAGE"

#define ST_CLAIM_TOKEN      "claimToken"
#define ST_GET_TOKEN        "getToken"
#define ST_GET_TOKEN_R      "getTokenR"

#define NUM_ARGS_NOTIFY                 1
#define NUM_ARGS_LIST_DEVICES           1
#define NUM_ARGS_LIST_GROUPS            1
#define NUM_ARGS_SETBINARY              5
#define NUM_ARGS_GETBINARY              3
#define NUM_ARGS_ADD_DEVICES            3
#define NUM_ARGS_FIRMWARE_ACTIONS       2
#define NUM_ARGS_SET_RULE               4
#define NUM_ARGS_GET_RULE               1
#define NUM_ARGS_RULE_ACTIONS           6
#define NUM_ARGS_RULE_MANUAL            2
#define NUM_ARGS_READ_SPEC              7
#define NUM_ARGS_READ_SPEC_CRC          10
#define NUM_ARGS_WRITE_SPEC             7
#define NUM_ARGS_WRITE_SPEC_CRC         10
#define NUM_ARGS_READ_S_SPEC            5
#define NUM_ARGS_WRITE_S_SPEC           7
#define NUM_ARGS_RESET                  2
#define NUM_ARGS_REMOVE_DEVICE          3
#define NUM_ARGS_OPEN_CLOSE_NETWORK     3
#define NUM_ARGS_GET_SUB_DEVICES        3
#define NUM_ARGS_IDENTYFY               4
#define NUM_ARGS_CHANGENAME             4
#define NUM_ARGS_ALEXA                  4
#define NUM_ARGS_AUTO_CONFIG            2
#define NUM_ARGS_SET_TIME               2
#define NUM_ARGS_GET_TIME               1
#define NUM_ARGS_REDISCOVER             2
#define NUM_ARGS_CREATE_GROUP           4
#define NUM_ARGS_GROUP_ACTIONS          5
#define NUM_ARGS_GROUP_CONTROL          4
#define NUM_ARGS_NETWORK                7
#define NUM_ARGS_SHARE_ACCESS           5
#define NUM_ARGS_REQUEST_ACCESS         1
#define NUM_ARGS_DENY_ACCESS            2
#define NUM_ARGS_LIST_USERS             1
#define NUM_ARGS_OB_REQUEST_ACCESS      1
#define NUM_ARGS_PUBLISH                1
#define NUM_ARGS_SEND_REPORT            2
#define NUM_ARGS_WIFI_SETTINGS          6
#define NUM_ARGS_GETNAME                3
#define NUM_ARGS_VENUS_ALARM            4
#define NUM_ARGS_VENUS_ALARM_V2         5
#define NUM_ARGS_SCENE_ACTIVATION       4
#define NUM_ARGS_SCENE_ACTIONS          5
#define NUM_GET_STATE                   1
#define NUM_GET_STATUS                  1
#define NUM_GET_TOKEN                   1
#define NUM_SETUP                       1
 
#define ALL_SESSION 0
#define ONLY_SESSION 1

#define ACTIVE 1
#define INACTIVE 0

extern int g_rule_d2d;
extern int g_rule_autooff;

typedef struct _new_allubus 
{
    char name[256];
    int type; // 0 :send all, 1: send with seassion id, 2: dont send.
    alljoyn_sessionid id;
    int numArgs;
    void *data;

    unsigned int action_time;
    struct ubus_request *req;
    struct _new_allubus *next;
} new_allubus;

typedef struct _daemon_service 
{
    const char *service_name;
    const char *name;
    uint32_t id;
    int timeout;
    timer_t timer_restart;
    unsigned int restart_time;
} daemon_service;

typedef struct _update_firmware 
{
    char URL[256];
    alljoyn_sessionid sessionId;
} update_firmware_d;

typedef struct _group_action
{
    char *command;
    char *value;
} group_action_data;

enum {
    NOTIFY_ID,
    SERVICE_NAME,
    __NOTIFY_MAX
};

typedef struct _TimeZone 
{
    const char *Location;
    const char *TZ;
} TimeZone;

int start_service(char* service_name);

int send_sessionless_signal(char* msg);

int send_session_signal(const char* destination, uint32_t session_id, alljoyn_msgarg msg, int msg_len);

QStatus Send_Signal(alljoyn_sessionid sessionId, char* name, char* msg, int _numArgs);

void install_rule(char* rule_name, char* conditions, char* actions);

#endif