/******************************************************************************
 * Copyright AllSeen Alliance. All rights reserved.
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ******************************************************************************/
#include <signal.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

// #include <json-c/json.h>

#include "slog.h"
#include "VR_define.h"
#include "verik_utils.h"
#include "alljoyn_controller.h"

static pthread_mutex_t g_connection_queueMutex;
static connection_list g_connection_list;
/**
 * Print out the fields found in the AboutData. Only fields with known signatures
 * are printed out.  All others will be treated as an unknown field.
 */
// static void printAboutData(alljoyn_aboutdata aboutData, const char* language, int tabNum) {
//     size_t count = alljoyn_aboutdata_getfields(aboutData, NULL, 0);
//     size_t i = 0;
//     int j = 0;
//     size_t k = 0;
//     const char** fields = (const char**) malloc(sizeof(char*) * count);
//     alljoyn_aboutdata_getfields(aboutData, fields, count);
//     for (i = 0; i < count; ++i) {
//         for (j = 0; j < tabNum; ++j) {
//             printf("\t");
//         }
//         printf("Key: %s", fields[i]);

//         alljoyn_msgarg tmp = alljoyn_msgarg_create();
//         alljoyn_aboutdata_getfield(aboutData, fields[i], &tmp, language);
//         printf("\t");
//         char signature[256] = { 0 };
//         alljoyn_msgarg_signature(tmp, signature, 16);
//         if (!strncmp(signature, "s", 1)) {
//             const char* tmp_s;
//             alljoyn_msgarg_get(tmp, "s", &tmp_s);
//             printf("%s", tmp_s);
//         } else if (!strncmp(signature, "as", 2)) {
//             size_t las;
//             alljoyn_msgarg as_arg = alljoyn_msgarg_create();
//             alljoyn_msgarg_get(tmp, "as", &las, &as_arg);
//             size_t j =0;
//             for (j = 0; j < las; ++j) {
//                 const char* tmp_s;
//                 alljoyn_msgarg_get(alljoyn_msgarg_array_element(as_arg, j), "s", &tmp_s);
//                 printf("%s ", tmp_s);
//             }
//         } else if (!strncmp(signature, "ay", 2)) {
//             size_t lay;
//             uint8_t* pay;
//             alljoyn_msgarg_get(tmp, "ay", &lay, &pay);
//             for (k = 0; k < lay; ++k) {
//                 printf("%02x ", pay[k]);
//             }
//         } else {
//             printf("User Defined Value\tSignature: %s", signature);
//         }
//         printf("\n");
//     }
//     free((void*)fields);
// }

// static void print_connection()
// {
//     int i = 0;
//     connection_list *tmp = NULL;
//     struct VR_list_head *pos, *q;
//     VR_(list_for_each_safe)(pos, q, &g_connection_list.list)
//     {
//         tmp = VR_(list_entry)(pos, connection_list, list);
        
//         printf("####### connection %d\n", i);
//         printf("deviceId %s\n", tmp->deviceId);
//         printf("bus %s\n", tmp->bus);
//         printf("path %s\n", tmp->path);
//         printf("sessionId %d\n", tmp->sessionId);
//         i++;
//     }
// }

static int add_new_connection(char *deviceId, char *bus, char *path,
                              alljoyn_sessionid sessionId,
                              alljoyn_sessionlistener listener)
{
    if(!deviceId || !bus || !path || !listener)
    {
        return -1;
    }

    pthread_mutex_lock(&g_connection_queueMutex);
    int res = 0;
    connection_list *tmp = NULL;
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_connection_list.list)
    {
        tmp = VR_(list_entry)(pos, connection_list, list);
        if(tmp->deviceId && !strcmp(tmp->deviceId, deviceId))
        {
            res = 1;
        }
    }
    
    if(!res)
    {
        connection_list *new = (connection_list *)malloc(sizeof(connection_list));
        memset(new, 0x00, sizeof(connection_list));

        new->deviceId = strdup(deviceId);
        new->bus = strdup(bus);
        new->path = strdup(path);
        new->sessionId = sessionId;
        new->sessionlistener = listener;

        VR_(list_add_tail)(&(new->list), &(g_connection_list.list));
    }
    else
    {
        alljoyn_sessionlistener_destroy(listener);
    }
    pthread_mutex_unlock(&g_connection_queueMutex);
    return res;
}

static int remove_connection(alljoyn_sessionid sessionId)
{
    pthread_mutex_lock(&g_connection_queueMutex);
    connection_list *tmp = NULL;
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_connection_list.list)
    {
        tmp = VR_(list_entry)(pos, connection_list, list);
        if(tmp->sessionId == sessionId)
        {
            VR_(list_del)(pos);
            free(tmp->deviceId);
            free(tmp->bus);
            free(tmp->path);
            alljoyn_sessionlistener_destroy(tmp->sessionlistener);
        }
    }
    pthread_mutex_unlock(&g_connection_queueMutex);
    return 0;
}

static connection_list *get_connection(char *deviceId)
{
    if(!deviceId)
    {
        return NULL;
    }

    pthread_mutex_lock(&g_connection_queueMutex);
    connection_list *tmp = NULL;
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_connection_list.list)
    {
        tmp = VR_(list_entry)(pos, connection_list, list);
        if(tmp->deviceId && !strcmp(tmp->deviceId, deviceId))
        {
            pthread_mutex_unlock(&g_connection_queueMutex);
            return tmp;
        }
    }
    pthread_mutex_unlock(&g_connection_queueMutex);
    return NULL;
}

static void alljoyn_sessionlistener_connect_lost_cb(const void* context,
                                                    alljoyn_sessionid sessionId,
                                                    alljoyn_sessionlostreason reason)
{
    QCC_UNUSED(context);
    SLOGI("SessionLost sessionId = %u, Reason = %d\n", sessionId, reason);
    remove_connection(sessionId);
}

static alljoyn_sessionlistener create_sessionlistener()
{
    alljoyn_sessionlistener_callbacks* callbacks =
        (alljoyn_sessionlistener_callbacks*)
        malloc(sizeof(alljoyn_sessionlistener_callbacks));
    callbacks->session_member_added = NULL;
    callbacks->session_member_removed = NULL;
    callbacks->session_lost = alljoyn_sessionlistener_connect_lost_cb;

    return alljoyn_sessionlistener_create(callbacks, NULL);
}

static QStatus SetInfo(alljoyn_busattachment busAtt, char *busName, char *path,
                       alljoyn_sessionid sessionId, char *userId, char *claimToken,
                       char *serverURL, char *firmwareUrl)
{
    if(!busName || !path || !userId || !serverURL ||
        !firmwareUrl || !claimToken)
    {
        return -1;
    }

    QStatus status;

    alljoyn_proxybusobject proxyObject = alljoyn_proxybusobject_create(busAtt, busName, path, sessionId);
    alljoyn_proxybusobject_secureconnection(proxyObject, QCC_TRUE);

    alljoyn_interfacedescription onboardingInterface = NULL;
    onboardingInterface = alljoyn_busattachment_getinterface(busAtt, ONBOARDING_INTERFACE);
    if(!onboardingInterface)
    {
        alljoyn_busattachment_createinterface_secure(busAtt, ONBOARDING_INTERFACE, &onboardingInterface, AJ_IFC_SECURITY_REQUIRED);
        alljoyn_interfacedescription_addmember(onboardingInterface, ALLJOYN_MESSAGE_METHOD_CALL,
                                               "SetInfo", "ssss", "n", "userId,claimToken,serverURL,firmwareUrl", 0); //trigger secure
    }

    alljoyn_proxybusobject_addinterface(proxyObject, onboardingInterface);

    size_t numArgs = 4;
    alljoyn_message replyMsg = alljoyn_message_create(busAtt);
    alljoyn_msgarg outArg = alljoyn_msgarg_array_create(numArgs);
    alljoyn_msgarg_array_set(outArg, &numArgs, "ssss", userId, claimToken, serverURL, firmwareUrl);

    status = alljoyn_proxybusobject_methodcall(proxyObject, ONBOARDING_INTERFACE, "SetInfo", outArg, 4, replyMsg, 5000, 0);
    alljoyn_proxybusobject_destroy(proxyObject);

    return status;
}

static void announced_cb(const void* context,
                         const char* busName,
                         uint16_t version,
                         alljoyn_sessionport port,
                         const alljoyn_msgarg objectDescriptionArg,
                         const alljoyn_msgarg aboutDataArg)
{
    connection_data *connect_data = (connection_data *) context;
    alljoyn_aboutobjectdescription objectDescription = alljoyn_aboutobjectdescription_create();
    alljoyn_aboutobjectdescription_createfrommsgarg(objectDescription, objectDescriptionArg);
    // printf("*********************************************************************************\n");
    // printf("Announce signal discovered\n");
    // printf("\tFrom bus %s\n", busName);
    // printf("\tAbout version %hu\n", version);
    // printf("\tSessionPort %hu\n", port);
    // printf("\tObjectDescription:\n");

    alljoyn_aboutobjectdescription aod = alljoyn_aboutobjectdescription_create();
    alljoyn_aboutobjectdescription_createfrommsgarg(aod, objectDescriptionArg);

    // size_t path_num = alljoyn_aboutobjectdescription_getpaths(aod, NULL, 0);
    // const char** paths = (const char**) malloc(sizeof(const char*) * path_num);
    // alljoyn_aboutobjectdescription_getpaths(aod, paths, path_num);

    // size_t i = 0;
    // for (i = 0; i < path_num; ++i) {
    //     printf("\t\t%s\n", paths[i]);
    //     size_t intf_num = alljoyn_aboutobjectdescription_getinterfaces(aod, paths[i], NULL, 0);
    //     const char** intfs = (const char**) malloc(sizeof(const char*) * intf_num);
    //     alljoyn_aboutobjectdescription_getinterfaces(aod, paths[i], intfs, intf_num);
    //     size_t j = 0;
    //     for (j = 0; j < intf_num; ++j) {
    //         printf("\t\t\t%s\n", intfs[j]);
    //     }
    //     free((void*)intfs);
    //     intfs = NULL;
    // }
    // free((void*) paths);
    // paths = NULL;

    // printf("\tAboutData:\n");
    alljoyn_aboutdata aboutData = alljoyn_aboutdata_create_full(aboutDataArg, "en");
    // printAboutData(aboutData, NULL, 2);
    // printf("*********************************************************************************\n");

    char *deviceId = NULL;
    alljoyn_aboutdata_getdeviceid(aboutData, &deviceId);

    if(!deviceId)
    {
        goto failed;
    }

    if(strncmp(deviceId, ST_STYX_PATTERN, 2))
    {
        goto failed;
    }

    if(!connect_data->busAtt) 
    {
        goto failed;
    }

    bool isOnboardingInterface = false;
    isOnboardingInterface = alljoyn_aboutobjectdescription_hasinterface(aod, ONBOARDING_INTERFACE);
    if(!isOnboardingInterface)
    {
        goto failed;
    }

    alljoyn_sessionid sessionId;
    alljoyn_sessionopts sessionOpts =
        alljoyn_sessionopts_create(ALLJOYN_TRAFFIC_TYPE_MESSAGES,
                                   QCC_FALSE, ALLJOYN_PROXIMITY_ANY,
                                   ALLJOYN_TRANSPORT_ANY);

    alljoyn_sessionlistener listener = create_sessionlistener();
    alljoyn_busattachment_enableconcurrentcallbacks(connect_data->busAtt);
    QStatus status = alljoyn_busattachment_joinsession(connect_data->busAtt, busName, port, listener,
                                                &sessionId, sessionOpts);
    if (ER_OK == status && 0 != sessionId)
    {
        const char* path = NULL;
        alljoyn_aboutobjectdescription_getinterfacepaths(objectDescription,
                                                         ONBOARDING_INTERFACE,
                                                         &path, 2);
        SLOGI("found new hub could config: %s, %s, %d\n", deviceId, busName, sessionId);
        add_new_connection(deviceId, (char*)busName, (char*)path, sessionId, listener);
        // print_connection();
        connect_data->connection_cb(NULL);
    }
    else
    {
        alljoyn_sessionlistener_destroy(listener);
    }
    alljoyn_sessionopts_destroy(sessionOpts);

failed:
    alljoyn_aboutdata_destroy(aboutData);
    alljoyn_aboutobjectdescription_destroy(aod);
    alljoyn_aboutobjectdescription_destroy(objectDescription);
}

static alljoyn_aboutlistener create_aboutlistener(connection_data *data)
{
    alljoyn_aboutlistener_callback* callback = 
            (alljoyn_aboutlistener_callback*) malloc(sizeof(alljoyn_aboutlistener_callback));
    callback->about_listener_announced = announced_cb;
    alljoyn_aboutlistener aboutlistener = alljoyn_aboutlistener_create(callback, data);
    return aboutlistener;
}

static void destroy_aboutlistener(alljoyn_busattachment busAtt, alljoyn_aboutlistener* aboutlistener)
{
    if(aboutlistener) 
    {
        alljoyn_busattachment_unregisteraboutlistener(busAtt, *aboutlistener);
        alljoyn_aboutlistener_destroy(*aboutlistener);
        *aboutlistener = NULL;
    }
}

void init_listen_connection(connection_data *connect_data, alljoyn_aboutlistener* aboutlistener)
{
    VR_INIT_LIST_HEAD(&g_connection_list.list);
    pthread_mutex_init(&g_connection_queueMutex, 0);

    *aboutlistener = create_aboutlistener(connect_data);
    alljoyn_busattachment_registeraboutlistener(connect_data->busAtt, *aboutlistener);
}

void scan_interface(alljoyn_busattachment busAtt, char *interface)
{
    alljoyn_busattachment_whoimplements_interface(busAtt, interface);
}

void stop_scan_interface(alljoyn_busattachment busAtt, char *interface)
{
    alljoyn_busattachment_cancelwhoimplements_interface(busAtt, interface);
}

void free_listen_connection(alljoyn_busattachment busAtt, alljoyn_aboutlistener* aboutlistener)
{
    destroy_aboutlistener(busAtt, aboutlistener);
}

void get_detected_hubs(void *devList)
{
    if(!devList)
    {
        return;
    }

    if(VR_list_empty(&g_connection_list.list))
    {
        return;
    }

    enum json_type type = json_object_get_type(devList);
    if(json_type_array != type)
    {
        return;
    }

    connection_list *tmp = NULL;
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_connection_list.list)
    {
        tmp = VR_(list_entry)(pos, connection_list, list);
        if(tmp && tmp->deviceId)
        {
            json_object *dev = json_object_new_object();
            JSON_ADD_STRING_SAFE(dev, ST_DEVICE_ID, tmp->deviceId);
            json_object_array_add(devList, dev);
        }
    }

    return;
}

void handle_setup_hub(alljoyn_busattachment busAtt, void *data)
{
    if(!data)
    {
        SLOGE("data invalid\n");
        return;
    }

    enum json_type type = json_object_get_type(data);
    if(json_type_array != type)
    {
        goto done;
    }

    char *userUUID = VR_(read_option)(NULL, UCI_CLOUD_USER_UUID);
    char *serverURL = VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    char *firmwareUrl = VR_(read_option)(NULL, UCI_CLOUD_FIRMWARE_ADDRESS);

    if(!userUUID || !serverURL || !firmwareUrl)
    {
        SAFE_FREE(userUUID);
        SAFE_FREE(serverURL);
        SAFE_FREE(firmwareUrl);
        goto done;
    }

    int i;
    int arraylen = json_object_array_length(data);

    for (i=0; i< arraylen; i++)
    {
        json_object *jvalue = json_object_array_get_idx(data, i);
        CHECK_JSON_OBJECT_EXIST(deviceIdObj, jvalue, ST_DEVICE_ID, _next_element_);
        CHECK_JSON_OBJECT_EXIST(claimTokenObj, jvalue, ST_CLAIM_TOKEN, _next_element_);
        const char *deviceId = json_object_get_string(deviceIdObj);
        const char *claimToken = json_object_get_string(claimTokenObj);

        connection_list *connection = get_connection((char*)deviceId);
        if(!connection)
        {
            goto _next_element_;
        }

        if(!SetInfo(busAtt, connection->bus, connection->path, connection->sessionId,
                userUUID, (char*)claimToken, serverURL, firmwareUrl))
        {
            json_object_object_add(jvalue, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
        }
        else
        {
            json_object_object_add(jvalue, ST_STATUS, json_object_new_string(ST_FAILED));
        }
        remove_connection(connection->sessionId);
        continue;

_next_element_:
        json_object_object_add(jvalue, ST_STATUS, json_object_new_string(ST_FAILED));
        continue;
    }

    SAFE_FREE(userUUID);
    SAFE_FREE(serverURL);
    SAFE_FREE(firmwareUrl);

done:
    return;
}

int is_onboarding_connect(const char *busName)
{
    int res = 0;
    if(!busName)
    {
        return res;
    }

    pthread_mutex_lock(&g_connection_queueMutex);
    connection_list *tmp = NULL;
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_connection_list.list)
    {
        tmp = VR_(list_entry)(pos, connection_list, list);
        if(tmp->bus && !strcmp(tmp->bus, busName))
        {
            res = 1;
            break;
        }
    }
    pthread_mutex_unlock(&g_connection_queueMutex);
    return res;
}