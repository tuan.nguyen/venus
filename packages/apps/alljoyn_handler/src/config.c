/******************************************************************************
 * Copyright (c) 2014, VEriK Systems. All rights reserved.
 *
 * Module: Service Provider
 *
 ******************************************************************************/

#include "config.h"
#include <string.h>
#include <stdlib.h>


int create_config(char* config, char* option[], char* data[], int size)
{
	FILE *fd;
	char* option_data;
	int i;

	char* refix_place = "/etc/config/";
	char* refix_config = "config ";
	char* refix_option = "\toption ";

	char file_name[strlen(refix_place)+strlen(config)];
	char config_name[strlen(refix_config)+strlen(config)];

	strcpy(file_name, refix_place);
	strcat(file_name, config);

	strcpy(config_name, refix_config);
	strcat(config_name, config);

	fd = fopen(file_name,"a+");

	if (fd != NULL)
	{
		//write config name 
		fprintf(fd, "%s\n",config_name);

		//write config option
		for (i = 0; i < size; i++)
		{
			option_data = malloc(strlen(refix_option)+strlen(option[i])+strlen(data[i])+3);
			strcpy(option_data, refix_option);
			strcat(option_data, option[i]);
			strcat(option_data, " '");
			strcat(option_data, data[i]);
			strcat(option_data, "'");

			//write option
			fprintf(fd, "%s\n",option_data);
			free(option_data);
		}

		fclose(fd);
	}

	return 0;
}


int create_event(char* event_name, char* command)
{
	FILE *fd;

	char* refix_event = "/etc/init.d/";

	char file_name[strlen(refix_event)+strlen(event_name)];
	char system_command_enable[strlen(file_name)+7];
	char system_command_chmod[strlen(file_name)+9];

	strcpy(file_name,refix_event);
	strcat(file_name,event_name);


	strcpy(system_command_enable, file_name);
	strcat(system_command_enable, " enable");

	strcpy(system_command_chmod,"chmod +x ");
	strcat(system_command_chmod, file_name);

	fd = fopen(file_name, "a+");

	if(fd != NULL)
	{
		fprintf(fd, EVENT_TEMPLATE, event_name, command);
		fclose(fd);
	}

	//enable event
	system(system_command_chmod);
	system(system_command_enable);

	return 0;
}

char* read_option(char* config, char* option)
{
	FILE *fd;

	int CUR_MAX = 4095;
	char* buffer;
	int ch;
	int ch_count = 0;
	int ch_len = 0;
	char* rs;
	int i;

	char* refix = "/etc/config/";
	char* refix_option = "option ";
	int refix_len = strlen(refix);
	int config_len = strlen(config);
	int option_len = strlen(option) + strlen(refix_option) + 2;

	char file_name[refix_len + config_len];

	strcpy(file_name, refix);
	strcat(file_name, config);

	fd =fopen(file_name, "r");

	if (fd != NULL)
	{
		while (ch != EOF)
		{
			buffer = (char*) malloc(sizeof(char) * CUR_MAX);
			ch_count = 0;
			ch_len = 0;

			while(ch != '\n')
			{
				if (ch_count == CUR_MAX)
				{
					CUR_MAX *= 2;
					ch_count = 0;
					buffer = realloc(buffer, CUR_MAX);
				}

				ch = getc(fd);
				buffer[ch_len] = ch;
				ch_len++;
				ch_count++;
			}

			char* match = strstr(buffer, option);

			if (match != NULL)
			{
				int data_len = strlen(buffer) - option_len - 1;
				char option_data[data_len];
				
				for( i =0; i < (data_len -1); i++)
				{
					option_data[i] = buffer[option_len+i];
				}

				option_data[data_len-1] = '\0';

				rs = (char*) malloc(data_len - 2);
				strcpy(rs, option_data);
				free(buffer);
				break;
			}

			ch = getc(fd);
		}
	}
	return rs;
}


int update_config(char* config, char* option, char* data)
{
	char* refix_option = "\toption ";
	char* refix_place = "/etc/config/";

	int refix_option_len = strlen(refix_option);
	int refix_place_len = strlen(refix_place);
	int config_len = strlen(config);
	//int option_len = strlen(option);
	int data_len = strlen(data);

	char option_pattern[refix_option_len + data_len];
	char option_data[refix_option_len + data_len + 3];
	char config_file[refix_place_len + config_len];
	char config_tmp[refix_place_len + config_len + 4];
	char replace_option[(refix_option_len * 2) + (data_len * 2) + refix_place_len + (config_len * 2) + 24];
	char remove_org[refix_place_len + config_len + 3];
	char replace_config[(refix_place_len * 2) + (config_len * 2) + 8];

	strcpy(option_pattern, refix_option);
	strcat(option_pattern, option);

	strcpy(option_data, option_pattern);
	strcat(option_data, " '");
	strcat(option_data, data);
	strcat(option_data,"'");

	strcpy(config_file,refix_place);
	strcat(config_file,config);

	strcpy(config_tmp,refix_place);
	strcat(config_tmp,config);
	strcat(config_tmp,".tmp");

	strcpy(replace_config, "mv ");
	strcat(replace_config, config_tmp);
	strcat(replace_config, " ");
	strcat(replace_config, config_file);

	strcpy(replace_option, "sed \"s/");
	strcat(replace_option, option_pattern);
	strcat(replace_option, ".*/");
	strcat(replace_option, option_data);
	strcat(replace_option, "/g\" ");
	strcat(replace_option, config_file);
	strcat(replace_option, " > ");
	strcat(replace_option, config_tmp);

	strcpy(remove_org, "rm ");
	strcat(remove_org, config_file);

	//execute command
	system(replace_option);
	system(remove_org);
	system(replace_config);

	return 0;
}


int execute_event(char* event_name)
{
	char* pre_command = "ubus call service event '{ \"type\": \"config.change\", \"data\": { \"package\": \"";
	char* pos_command = "\" }}'"; 


	int pre_len = strlen(pre_command);
	int pos_len = strlen(pos_command);
	int event_len = strlen(event_name);

	char event_command[pre_len + pos_len + event_len];

	strcpy(event_command, pre_command);
	strcat(event_command, event_name);
	strcat(event_command, pos_command);

	system(event_command);

	return 0;
}
