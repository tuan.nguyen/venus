/******************************************************************************
 * Copyright (c) 2014, VEriK Systems. All rights reserved.
 *
 * Module: Service Provider
 *
 ******************************************************************************/

#include <signal.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include <alljoyn_c/AboutData.h>
#include <alljoyn_c/AboutIcon.h>
#include <alljoyn_c/AboutListener.h>
#include <alljoyn_c/AboutObj.h>
#include <alljoyn_c/AboutIconObj.h>
#include <alljoyn_c/AboutObjectDescription.h>
#include <alljoyn_c/AboutProxy.h>
#include <alljoyn_c/Init.h>

#include <alljoyn_c/DBusStdDefines.h>
#include <alljoyn_c/BusAttachment.h>
#include <alljoyn_c/version.h>
#include <alljoyn_c/Status.h>

#include "slog.h"

static const uint8_t ALLJOYN_FLAG_SESSIONLESS        = 0x10;

typedef struct {
	char* path;
	int interface_len;
	char* interface[];
} object_description;


QStatus create_announce_service(alljoyn_aboutdata *aboutData, alljoyn_abouticon *aboutIcon);

QStatus add_object_description(object_description* object[], int size);

QStatus announce(alljoyn_busattachment bus, alljoyn_sessionport sessionPort, alljoyn_aboutdata aboutData, alljoyn_abouticon aboutIcon);

void update_about_state(alljoyn_sessionport sessionPort, alljoyn_aboutdata *aboutData, char *state);
void update_about_deviceId(alljoyn_sessionport sessionPort, alljoyn_aboutdata *aboutData, char *state);
void destroy_announce_service();