/******************************************************************************
 * Copyright (c) 2014, VEriK Systems. All rights reserved.
 *
 * Module: Service Provider
 *
 ******************************************************************************/

#include <json-c/json.h>
#include "announce_service.h"
#include "verik_utils.h"
#include "VR_define.h"

//device information
//static const char* DEVICE_ID = "93c06771-c725-48c2-b1ff-6a2a59d445b8";
// static const char* DEVICE_NAME = "Venus";
static const char* APP_NAME = "Venus Service";
static const char* DEFAULT_LANGUAGE = "en";
static const char* MANUFACTURER = "VEriK Systems";
// static const char* MODEL_NUMBER = "VENUS-08B";
//static const char* SUPPORTED_LANGUAGE[1] = {"en"};
static const char* DESCRIPTION = "This is PhD board";
static const char* DATE_OF_MANUFACTURE = "2015-07-09";
static const char* SOFTWARE_VERSION = "venus-s-v1.0b";
//static const char* ALLJOYN_SOFTWARE_VERSION = "v14.06.00a";
static const char* HARDWARE_VERSION = "venus-h-v1.0b";
static const char* SUPPORT_URL = "www.zinnoinc.com";

static alljoyn_aboutobj aboutObj;
static alljoyn_abouticonobj aboutIconObj;

static uint8_t appId[] = { 0x00, 0x42, 0x42, 0x42,
                        0x1E, 0x82, 0x11, 0xE4,
                        0x86, 0x51, 0xD1, 0x56,
                        0x1D, 0x5D, 0x46, 0xB0 };

static QStatus set_announce_hub_state(alljoyn_aboutdata *aboutData)
{
    QStatus status;
    char *state = NULL, *message = NULL;
    get_hub_state(&state, &message);
    if(state)
    {
        appId[0] = strtol(state, NULL, 0);
        status = alljoyn_aboutdata_setappid(*aboutData, appId, 16);
    }
    else
    {
        status = alljoyn_aboutdata_setappid(*aboutData, appId, 16);
    }

    SAFE_FREE(state);
    SAFE_FREE(message);

    return status;
}

static QStatus set_announce_hub_name(alljoyn_aboutdata *aboutData)
{
    QStatus status;
    char devName[SIZE_64B];
    get_hub_name(devName, sizeof(devName));
    status = alljoyn_aboutdata_setdevicename(*aboutData, devName, DEFAULT_LANGUAGE);
    return status;
}

static QStatus set_announce_model_number(alljoyn_aboutdata *aboutData)
{
    QStatus status;
    char bssid[SIZE_64B];
    get_hub_bssid(bssid, sizeof(bssid));
    status = alljoyn_aboutdata_setmodelnumber(*aboutData, bssid);
    return status;
}

static QStatus set_announce_device_id(alljoyn_aboutdata *aboutData)
{
    QStatus status;
    /* DeviceId is a string encoded 128bit UUID */
    char *devid = NULL;
    char *remote_state = (char *)VR_(read_option)(NULL, UCI_CLOUD_STATE);
    if(remote_state)
    {
        if(!strcmp(remote_state, ST_CONNECTED) || !strcmp(remote_state, ST_CLAIMED))
        {
            devid = (char *)VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
        }
        else
        {
            devid = (char *)VR_(read_option)(NULL, UCI_LOCAL_HUB_ID);
        }
        free(remote_state);
    }
    else
    {
        devid = (char *)VR_(read_option)(NULL, UCI_LOCAL_HUB_ID);
    }

    SLOGI("devid %s\n", devid);
    if(!devid || !strcmp(devid, "default"))
    {
        char deviceId[SIZE_64B];
        get_hub_id(deviceId, sizeof(deviceId));
        status = alljoyn_aboutdata_setdeviceid(*aboutData, (const char*)deviceId);
    }
    else
    {
        status = alljoyn_aboutdata_setdeviceid(*aboutData, (const char*)devid);
    }

    SAFE_FREE(devid);
    return status;
}

QStatus create_announce_service(alljoyn_aboutdata *aboutData, alljoyn_abouticon *aboutIcon)
{
	QStatus status;
	*aboutData = alljoyn_aboutdata_create("en");
    
    // alljoyn_msgarg arg = alljoyn_msgarg_create();
    // status = alljoyn_msgarg_set(arg, "s", "888-555-1234");
    // status = alljoyn_aboutdata_setfield(*aboutData, "SupportNumber", arg, "en");

    *aboutIcon = alljoyn_abouticon_create();

    status = alljoyn_abouticon_seturl(*aboutIcon, "image/jpeg", "http://192.168.1.125/index.jpeg");
    if(status != ER_OK)
    {
        SLOGE("Failed to set icon url\n");
    }

    status = set_announce_model_number(aboutData);
    status = set_announce_device_id(aboutData);
    status = set_announce_hub_state(aboutData);
    status = set_announce_hub_name(aboutData);

    status = alljoyn_aboutdata_setdescription(*aboutData, DESCRIPTION, DEFAULT_LANGUAGE);
    status = alljoyn_aboutdata_setappname(*aboutData, APP_NAME, DEFAULT_LANGUAGE);
    status = alljoyn_aboutdata_setmanufacturer(*aboutData, MANUFACTURER, DEFAULT_LANGUAGE);
    status = alljoyn_aboutdata_setdateofmanufacture(*aboutData, DATE_OF_MANUFACTURE);
    status = alljoyn_aboutdata_setsoftwareversion(*aboutData, SOFTWARE_VERSION);
    status = alljoyn_aboutdata_sethardwareversion(*aboutData, HARDWARE_VERSION);
    status = alljoyn_aboutdata_setsupporturl(*aboutData, SUPPORT_URL);

    if (!alljoyn_aboutdata_isvalid(*aboutData, "en")) 
    {
        SLOGE("failed to setup about data.\n");
        return 1;
    }
    else
    {
        SLOGI("Succeeded to setup about data.\n");
    }

    return status;
}

QStatus announce(alljoyn_busattachment bus, alljoyn_sessionport sessionPort, alljoyn_aboutdata aboutData, alljoyn_abouticon aboutIcon)
{
	/* Announce about signal */
    aboutObj = alljoyn_aboutobj_create(bus, ANNOUNCED);
    aboutIconObj = alljoyn_abouticonobj_create(bus, aboutIcon);
    /*
     * Note the ObjectDescription that is part of the Announce signal is found
     * automatically by introspecting the BusObjects registered with the bus
     * attachment.
     */
    QStatus status = alljoyn_aboutobj_announce(aboutObj, sessionPort, aboutData);
    if (ER_OK == status) {
        SLOGE("AboutObj Announce Succeeded.\n");
        return ER_OK;
    } else {
        SLOGE("AboutObj Announce failed (%s)\n", QCC_StatusText(status));
        return 1;
    }
}

void destroy_announce_service()
{
	//alljoyn_aboutobj_unannounce(aboutObj);
    alljoyn_abouticonobj_destroy(aboutIconObj);
	alljoyn_aboutobj_destroy(aboutObj);
}

void update_about_state(alljoyn_sessionport sessionPort, alljoyn_aboutdata *aboutData, char *state)
{
    if(state)
    {
        alljoyn_aboutobj_unannounce(aboutObj);
        appId[0] = strtol(state, NULL, 0);
        alljoyn_aboutdata_setappid(*aboutData, appId, 16);
        alljoyn_aboutobj_announce(aboutObj, sessionPort, *aboutData);
    }
}

void update_about_deviceId(alljoyn_sessionport sessionPort, alljoyn_aboutdata *aboutData, char *state)
{
    alljoyn_aboutobj_unannounce(aboutObj);
    set_announce_device_id(aboutData);
    if(state)
    {
        appId[0] = strtol(state, NULL, 0);
        alljoyn_aboutdata_setappid(*aboutData, appId, 16);
    }
    alljoyn_aboutobj_announce(aboutObj, sessionPort, *aboutData);
}
