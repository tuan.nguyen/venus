#define COLOR_OFF	"\x1B[0m"
#define COLOR_RED	"\x1B[0;91m"
#define COLOR_GREEN	"\x1B[0;92m"
#define COLOR_YELLOW	"\x1B[0;93m"
#define COLOR_BLUE	"\x1B[0;94m"
#define COLOR_BOLDGRAY	"\x1B[1;30m"
#define COLOR_BOLDWHITE	"\x1B[1;37m"

#define blu_debug(M, ...) printf(M,##__VA_ARGS__)

void gatt_add_service(GDBusProxy *proxy);
void gatt_remove_service(GDBusProxy *proxy);

void gatt_add_characteristic(GDBusProxy *proxy);
void gatt_remove_characteristic(GDBusProxy *proxy);

void gatt_add_descriptor(GDBusProxy *proxy);
void gatt_remove_descriptor(GDBusProxy *proxy);

void gatt_list_attributes(const char *device);
GDBusProxy *gatt_select_attribute(const char *path);

gboolean gatt_write_attribute_non_interact(GDBusProxy *proxy, char *data, int length,
		GDBusReturnFunction function);
gboolean gatt_read_attribute_non_interact(GDBusProxy *proxy, GDBusReturnFunction function);
void gatt_notify_attribute(GDBusProxy *proxy, bool enable);

void gatt_add_manager(GDBusProxy *proxy);
void gatt_remove_manager(GDBusProxy *proxy);

void gatt_register_profile(DBusConnection *conn, GDBusProxy *proxy,
								wordexp_t *w);
void gatt_unregister_profile(DBusConnection *conn, GDBusProxy *proxy);
