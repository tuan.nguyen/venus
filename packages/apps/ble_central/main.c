#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <sys/signalfd.h>
#include <wordexp.h>

#include <libubus.h>
#include <libubox/blobmsg_json.h>
#include <json-c/json.h>
#include "slog.h"

#include <glib.h>

#include "gdbus.h"
#include "gatt.h"

#define VERSION 	"Titan BLE Central"

#define COLORED_NEW	COLOR_GREEN "NEW" COLOR_OFF
#define COLORED_CHG	COLOR_YELLOW "CHG" COLOR_OFF
#define COLORED_DEL	COLOR_RED "DEL" COLOR_OFF

#define PROMPT_ON	COLOR_BLUE "[bluetooth]" COLOR_OFF "# "
#define PROMPT_OFF	"[bluetooth]# "

#define LE_ACTION_TIME_WAIT	2
#define LE_ACTION_CONNECTION_TIME_WAIT 1

static struct ubus_context *ctx;
static struct blob_buf buff;
void Send_ubus_notify_asyn(char *data);

static const struct blobmsg_policy scan_policy[2]={
    [0] = { .name = "mode", .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = "scantime", .type = BLOBMSG_TYPE_STRING },
};

enum BluetoothDeviceStatus {
	DEVICE_UNKNOWN		= 0,
	DEVICE_IS_SCANNED	= (1 << 0),
	DEVICE_PAIRED 		= (1 << 1),
	DEVICE_CONNECTED	= (1 << 2),
	DEVICE_DISCONNECTED 	= (1 << 3),
};

enum BluetoothTransport {
	TRANSPORT_UNKNOWN	= 0,
	TRANSPORT_LE		= (1 << 1),
	TRANSPORT_BREDR 	= (1 << 2),
};

typedef enum BluetoothDeviceStatus BluetoothDeviceStatus;
typedef enum BluetoothTransport BluetoothTransport;

static gboolean le_action_wakeup(gpointer data);
static void cmd_quit(void);

struct bluetooth_device {
	char *address;
	char *name;
	gboolean trusted;
	BluetoothDeviceStatus status;
	BluetoothTransport transport_type;
};

static gboolean option_version = FALSE;
static BluetoothTransport scan_transport_type = TRANSPORT_UNKNOWN;

static int scan_interval;

static GMainLoop *main_loop;
static DBusConnection *dbus_conn;

static GDBusProxy *agent_manager;
static char *auto_register_agent = NULL;

static GDBusProxy *default_ctrl;
static GDBusProxy *default_dev;
static GDBusProxy *default_attr;
static GList *ctrl_list;
static GList *dev_list;
static GList *bluetooth_device_list;

static guint input = 0;

#define DISTANCE_VAL_INVALID    0x7FFF
static gint filtered_scan_rssi = DISTANCE_VAL_INVALID;
static gint filtered_scan_pathloss = DISTANCE_VAL_INVALID;
static GSList *filtered_scan_uuids;
static char *filtered_scan_transport;

static void proxy_leak(gpointer data)
{
	blu_debug("Leaking proxy %p\n", data);
}

static void connect_handler(DBusConnection *connection, void *user_data)
{
	blu_debug("%s: Line:%d in file:%s\n", __FUNCTION__, __LINE__, __FILE__);
}

static void disconnect_handler(DBusConnection *connection, void *user_data)
{
	blu_debug("%s: Line:%d in file:%s\n", __FUNCTION__, __LINE__, __FILE__);

	g_list_free(ctrl_list);
	ctrl_list = NULL;

	default_ctrl = NULL;

	g_list_free(dev_list);
	dev_list = NULL;
}

static void print_adapter(GDBusProxy *proxy, const char *description)
{
	DBusMessageIter iter;
	const char *address, *name;

	if (g_dbus_proxy_get_property(proxy, "Address", &iter) == FALSE)
		return;

	dbus_message_iter_get_basic(&iter, &address);

	if (g_dbus_proxy_get_property(proxy, "Alias", &iter) == TRUE)
		dbus_message_iter_get_basic(&iter, &name);
	else
		name = "<unknown>";

	blu_debug("%s%s%sController %s %s %s\n",
			description ? "[" : "",
			description ? : "",
			description ? "] " : "",
			address, name,
			default_ctrl == proxy ? "[default]" : "");
}

static void print_iter(const char *label, const char *name,
						DBusMessageIter *iter)
{
	dbus_bool_t valbool;
	dbus_uint32_t valu32;
	dbus_uint16_t valu16;
	dbus_int16_t vals16;
	unsigned char byte;
	const char *valstr;
	DBusMessageIter subiter;
	char *entry;

	if (iter == NULL) {
		blu_debug("%s%s is nil\n", label, name);
		return;
	}

	switch (dbus_message_iter_get_arg_type(iter)) {
		case DBUS_TYPE_INVALID:
			blu_debug("%s%s is invalid\n", label, name);
			break;
		case DBUS_TYPE_STRING:
		case DBUS_TYPE_OBJECT_PATH:
			dbus_message_iter_get_basic(iter, &valstr);
			blu_debug("%s%s: %s\n", label, name, valstr);
			break;
		case DBUS_TYPE_BOOLEAN:
			dbus_message_iter_get_basic(iter, &valbool);
			blu_debug("%s%s: %s\n", label, name,
					valbool == TRUE ? "yes" : "no");
			break;
		case DBUS_TYPE_UINT32:
			dbus_message_iter_get_basic(iter, &valu32);
			blu_debug("%s%s: 0x%06x\n", label, name, valu32);
			break;
		case DBUS_TYPE_UINT16:
			dbus_message_iter_get_basic(iter, &valu16);
			blu_debug("%s%s: 0x%04x\n", label, name, valu16);
			break;
		case DBUS_TYPE_INT16:
			dbus_message_iter_get_basic(iter, &vals16);
			blu_debug("%s%s: %d\n", label, name, vals16);
			break;
		case DBUS_TYPE_BYTE:
			dbus_message_iter_get_basic(iter, &byte);
			blu_debug("%s%s: 0x%02x\n", label, name, byte);
			break;
		case DBUS_TYPE_VARIANT:
			dbus_message_iter_recurse(iter, &subiter);
			print_iter(label, name, &subiter);
			break;
		case DBUS_TYPE_ARRAY:
			dbus_message_iter_recurse(iter, &subiter);
			while (dbus_message_iter_get_arg_type(&subiter) !=
					DBUS_TYPE_INVALID) {
				print_iter(label, name, &subiter);
				dbus_message_iter_next(&subiter);
			}
			break;
		case DBUS_TYPE_DICT_ENTRY:
			dbus_message_iter_recurse(iter, &subiter);
			entry = g_strconcat(name, " Key", NULL);
			print_iter(label, entry, &subiter);
			g_free(entry);

			entry = g_strconcat(name, " Value", NULL);
			dbus_message_iter_next(&subiter);
			print_iter(label, entry, &subiter);
			g_free(entry);
			break;
		default:
			blu_debug("%s%s has unsupported type\n", label, name);
			break;
	}
}

static gboolean device_is_child(GDBusProxy *device, GDBusProxy *master)
{
	DBusMessageIter iter;
	const char *adapter, *path;

	if (!master)
		return FALSE;

	if (g_dbus_proxy_get_property(device, "Adapter", &iter) == FALSE)
		return FALSE;

	dbus_message_iter_get_basic(&iter, &adapter);
	path = g_dbus_proxy_get_path(master);

	if (!strcmp(path, adapter))
		return TRUE;

	return FALSE;
}

static gboolean service_is_child(GDBusProxy *service)
{
	GList *l;
	DBusMessageIter iter;
	const char *device, *path;

	if (g_dbus_proxy_get_property(service, "Device", &iter) == FALSE)
		return FALSE;

	dbus_message_iter_get_basic(&iter, &device);

	for (l = dev_list; l; l = g_list_next(l)) {
		GDBusProxy *proxy = l->data;

		path = g_dbus_proxy_get_path(proxy);
		if (!strcmp(path, device))
			return TRUE;
	}

	return FALSE;
}

static void set_default_device(GDBusProxy *proxy, const char *attribute)
{
	DBusMessageIter iter;

	default_dev = proxy;
	if (proxy == NULL) {
		default_attr = NULL;
		goto done;
	}

	if (!g_dbus_proxy_get_property(proxy, "Alias", &iter)) {
		if (!g_dbus_proxy_get_property(proxy, "Address", &iter))
			goto done;
	}
done:
	return;
}

static int check_manufacture_data(GDBusProxy *proxy)
{
	DBusMessageIter iter, value, value2, value3;
	DBusMessageIter subiter;
	dbus_uint16_t valu16;
	uint8_t *byte;
	int len;

	if (g_dbus_proxy_get_property(proxy, "ManufacturerData", &iter) == FALSE)
		return -1;
	/* DICT -> Key, Variant -> Array */
	dbus_message_iter_recurse(&iter, &value);
	if (dbus_message_iter_get_arg_type(&value) == DBUS_TYPE_DICT_ENTRY) {
		dbus_message_iter_recurse(&value, &subiter);
		if (dbus_message_iter_get_arg_type(&subiter) == DBUS_TYPE_UINT16) {
			dbus_message_iter_get_basic(&subiter, &valu16);
			dbus_message_iter_next(&subiter);
			if (dbus_message_iter_get_arg_type(&subiter) == DBUS_TYPE_VARIANT) {
				dbus_message_iter_recurse(&subiter, &value2);
				if (dbus_message_iter_get_arg_type(&value2) == DBUS_TYPE_ARRAY) {
					dbus_message_iter_recurse(&value2, &value3);
					dbus_message_iter_get_fixed_array(&value3, &byte, &len);
					if (len < 0) {
						blu_debug("Unable to parse value\n");
						return -1;
					}
				}
			}
		}
	}

	return (byte[0] | (byte[1] >> 8));
}

static struct bluetooth_device *find_device_in_list(GDBusProxy *proxy)
{
	GList *list;
	const char *address;
	DBusMessageIter iter;
	struct bluetooth_device *device;

	if (g_dbus_proxy_get_property(proxy, "Address", &iter) == FALSE)
		return;

	dbus_message_iter_get_basic(&iter, &address);

	for (list = g_list_first(bluetooth_device_list); list;
			list = g_list_next(list)) {
		device = list->data;
		if (strcmp(device->address, address))
			continue;

		return device;
	}

	return NULL;
}

static void append_scanned_list(GDBusProxy *proxy)
{
	DBusMessageIter iter;
	char *address, name;
	struct bluetooth_device *device;

	if (find_device_in_list(proxy))
		return;

	device = g_try_new0(struct bluetooth_device, 1);
	device->address = g_strdup(address);

	if (g_dbus_proxy_get_property(proxy, "Alias", &iter) == TRUE)
		dbus_message_iter_get_basic(&iter, &name);
	else
		name = "unknown";
	device->name = g_strdup(name);

	device->status = DEVICE_IS_SCANNED;
	device->transport_type = scan_transport_type;
	bluetooth_device_list = g_list_append(bluetooth_device_list, device);

	return;
}

static void device_added(GDBusProxy *proxy)
{
	DBusMessageIter iter;
	dev_list = g_list_append(dev_list, proxy);

	//append_scanned_list(proxy);

	if (default_dev)
		return;
	if (g_dbus_proxy_get_property(proxy, "Connected", &iter)) {
		dbus_bool_t connected;

		dbus_message_iter_get_basic(&iter, &connected);
		if (connected)
			set_default_device(proxy, NULL);
	}
}

static void proxy_added(GDBusProxy *proxy, void *user_data)
{
	const char *interface;

	interface = g_dbus_proxy_get_interface(proxy);
	if (!strcmp(interface, "org.bluez.Device1")) {
		if (device_is_child(proxy, default_ctrl) == TRUE) {
			device_added(proxy);
		}
	} else if (!strcmp(interface, "org.bluez.Adapter1")) {
		ctrl_list = g_list_append(ctrl_list, proxy);
		if (!default_ctrl)
			default_ctrl = proxy;

		print_adapter(proxy, COLORED_NEW);
	} else if (!strcmp(interface, "org.bluez.AgentManager1")) {
		if (!agent_manager) {
			agent_manager = proxy;
		}
	} else if (!strcmp(interface, "org.bluez.GattService1")) {
		if (service_is_child(proxy))
			gatt_add_service(proxy);
	} else if (!strcmp(interface, "org.bluez.GattCharacteristic1")) {
		gatt_add_characteristic(proxy);
	} else if (!strcmp(interface, "org.bluez.GattDescriptor1")) {
		gatt_add_descriptor(proxy);
	} else if (!strcmp(interface, "org.bluez.GattManager1")) {
		gatt_add_manager(proxy);
	}
}

static void set_default_attribute(GDBusProxy *proxy)
{
	const char *path;

	default_attr = proxy;

	path = g_dbus_proxy_get_path(proxy);

	set_default_device(default_dev, path);
}

static void proxy_removed(GDBusProxy *proxy, void *user_data)
{
	const char *interface;

	interface = g_dbus_proxy_get_interface(proxy);

	if (!strcmp(interface, "org.bluez.Device1")) {
		if (device_is_child(proxy, default_ctrl) == TRUE) {
			struct bluetooth_device *device;

			dev_list = g_list_remove(dev_list, proxy);

			if (default_dev == proxy)
				set_default_device(NULL, NULL);

			device = find_device_in_list(proxy);
			if (device == NULL)
				return;

			bluetooth_device_list = g_list_remove(bluetooth_device_list,
							device);
		}
	} else if (!strcmp(interface, "org.bluez.Adapter1")) {
		ctrl_list = g_list_remove(ctrl_list, proxy);

		print_adapter(proxy, COLORED_DEL);

		if (default_ctrl == proxy) {
			default_ctrl = NULL;
			set_default_device(NULL, NULL);

			g_list_free(dev_list);
			dev_list = NULL;
		}
	} else if (!strcmp(interface, "org.bluez.AgentManager1")) {
		if (agent_manager == proxy) {
			agent_manager = NULL;
		}
	} else if (!strcmp(interface, "org.bluez.GattService1")) {
		gatt_remove_service(proxy);

		if (default_attr == proxy)
			set_default_attribute(NULL);
	} else if (!strcmp(interface, "org.bluez.GattCharacteristic1")) {
		gatt_remove_characteristic(proxy);

		if (default_attr == proxy)
			set_default_attribute(NULL);
	} else if (!strcmp(interface, "org.bluez.GattDescriptor1")) {
		gatt_remove_descriptor(proxy);

		if (default_attr == proxy)
			set_default_attribute(NULL);
	} else if (!strcmp(interface, "org.bluez.GattManager1")) {
		gatt_remove_manager(proxy);
	}
}

static void handle_device_property_changed(GDBusProxy *proxy, DBusMessageIter *iter,
				const char *address, const char *name)
{
	struct bluetooth_device *device;

	device = find_device_in_list(address);
	if (device == NULL)
		return;

	if (strcmp(name, "alias") == 0) {
		char *alias;

		dbus_message_iter_get_basic(iter, &alias);
		g_free(device->name);
		device->name = g_strdup(alias);
	} else if (strcmp(name, "Connected") == 0) {
		dbus_bool_t connected;

		dbus_message_iter_get_basic(iter, &connected);
		if (connected && default_dev == NULL) {
			set_default_device(proxy, NULL);
			device->status = DEVICE_CONNECTED;
		} else if (!connected && default_dev == proxy) {
			set_default_device(NULL, NULL);
			device->status = DEVICE_DISCONNECTED;
		}
	}
}

static void property_changed(GDBusProxy *proxy, const char *name,
					DBusMessageIter *iter, void *user_data)
{
	const char *interface;

	interface = g_dbus_proxy_get_interface(proxy);
	SLOGI("interface = %s\n", interface);
	if (!strcmp(interface, "org.bluez.Device1")) {
		if (device_is_child(proxy, default_ctrl) == TRUE) {
			DBusMessageIter addr_iter;
			char *str;
			const char *address = NULL;
			if (g_dbus_proxy_get_property(proxy, "Address",
							&addr_iter) == TRUE) {

				dbus_message_iter_get_basic(&addr_iter,
								&address);
				str = g_strdup_printf("[" COLORED_CHG
						"] Device %s ", address);
				Send_ubus_notify_asyn(str);
			} 
			else
				str = g_strdup("");

			//handle_device_property_changed(proxy, iter, address, name);
			print_iter(str, name, iter);
			g_free(str);
		}
	} else if (!strcmp(interface, "org.bluez.Adapter1")) {
		DBusMessageIter addr_iter;
		char *str;
		if (g_dbus_proxy_get_property(proxy, "Address",
						&addr_iter) == TRUE) {
			const char *address;

			dbus_message_iter_get_basic(&addr_iter, &address);
			str = g_strdup_printf("[" COLORED_CHG
						"] Controller %s ", address);
		} else
			str = g_strdup("");
		if(str)
		{
			print_iter(str, name, iter);
			g_free(str);
		}	

	} else if (proxy == default_attr) {
		blu_debug("Notify signal\n");
	}
}

static void message_handler(DBusConnection *connection,
					DBusMessage *message, void *user_data)
{
	SLOGI("[SIGNAL] %s.%s\n", dbus_message_get_interface(message),
					dbus_message_get_member(message));
}

static GDBusProxy *find_proxy_by_address(GList *source, const char *address)
{
	GList *list;

	for (list = g_list_first(source); list; list = g_list_next(list)) {
		GDBusProxy *proxy = list->data;
		DBusMessageIter iter;
		const char *str;

		if (g_dbus_proxy_get_property(proxy, "Address", &iter) == FALSE)
			continue;

		dbus_message_iter_get_basic(&iter, &str);

		if (!strcmp(str, address))
			return proxy;
	}

	return NULL;
}

static gboolean check_default_ctrl(void)
{
	if (!default_ctrl) {
		blu_debug("No default controller available\n");
		return FALSE;
	}

	return TRUE;
}

static gboolean parse_argument_on_off(const char *arg, dbus_bool_t *value)
{
	if (!arg || !strlen(arg)) {
		SLOGI("Missing on/off argument\n");
		return FALSE;
	}

	if (!strcmp(arg, "on") || !strcmp(arg, "yes")) {
		*value = TRUE;
		return TRUE;
	}

	if (!strcmp(arg, "off") || !strcmp(arg, "no")) {
		*value = FALSE;
		return TRUE;
	}

	blu_debug("Invalid argument %s\n", arg);
	return FALSE;
}

static void start_discovery_reply(DBusMessage *message, void *user_data)
{
	dbus_bool_t enable = GPOINTER_TO_UINT(user_data);
	DBusError error;

	dbus_error_init(&error);

	if (dbus_set_error_from_message(&error, message) == TRUE) {
		blu_debug("Failed to %s discovery: %s\n",
				enable == TRUE ? "start" : "stop", error.name);
		dbus_error_free(&error);
		return;
	}
}

static void cmd_scan(const char *arg)
{
	dbus_bool_t enable;
	const char *method;

	if (parse_argument_on_off(arg, &enable) == FALSE)
		return;

	if (check_default_ctrl() == FALSE)
		return;

	if (enable == TRUE)
		method = "StartDiscovery";
	else
		method = "StopDiscovery";

	if (g_dbus_proxy_method_call(default_ctrl, method,
				NULL, start_discovery_reply,
				GUINT_TO_POINTER(enable), NULL) == FALSE) {
		blu_debug("Failed to %s discovery\n",
					enable == TRUE ? "start" : "stop");
		return;
	}
}

static void append_variant(DBusMessageIter *iter, int type, void *val)
{
	DBusMessageIter value;
	char sig[2] = { type, '\0' };

	dbus_message_iter_open_container(iter, DBUS_TYPE_VARIANT, sig, &value);

	dbus_message_iter_append_basic(&value, type, val);

	dbus_message_iter_close_container(iter, &value);
}

static void dict_append_entry(DBusMessageIter *dict, const char *key,
		int type, void *val)
{
	DBusMessageIter entry;

	if (type == DBUS_TYPE_STRING) {
		const char *str = *((const char **) val);

		if (str == NULL)
			return;
	}

	dbus_message_iter_open_container(dict, DBUS_TYPE_DICT_ENTRY,
			NULL, &entry);

	dbus_message_iter_append_basic(&entry, DBUS_TYPE_STRING, &key);

	append_variant(&entry, type, val);

	dbus_message_iter_close_container(dict, &entry);
}

struct set_discovery_filter_args {
	char *transport;
	dbus_uint16_t rssi;
	dbus_int16_t pathloss;
	GSList *uuids;
};

static void set_discovery_filter_setup(DBusMessageIter *iter, void *user_data)
{
	struct set_discovery_filter_args *args = user_data;
	DBusMessageIter dict;

	dbus_message_iter_open_container(iter, DBUS_TYPE_ARRAY,
			DBUS_DICT_ENTRY_BEGIN_CHAR_AS_STRING
			DBUS_TYPE_STRING_AS_STRING
			DBUS_TYPE_VARIANT_AS_STRING
			DBUS_DICT_ENTRY_END_CHAR_AS_STRING, &dict);

	if (args->uuids != NULL) {
		DBusMessageIter entry, value, arrayIter;
		char *uuids = "UUIDs";
		GSList *l;

		dbus_message_iter_open_container(&dict, DBUS_TYPE_DICT_ENTRY,
				NULL, &entry);
		/* dict key */
		dbus_message_iter_append_basic(&entry, DBUS_TYPE_STRING,
				&uuids);

		dbus_message_iter_open_container(&entry, DBUS_TYPE_VARIANT,
				"as", &value);

		dbus_message_iter_open_container(&value, DBUS_TYPE_ARRAY, "s",
				&arrayIter);

		for (l = args->uuids; l != NULL; l = g_slist_next(l))
			/* list->data contains string representation of uuid */
			dbus_message_iter_append_basic(&arrayIter,
					DBUS_TYPE_STRING,
					&l->data);

		dbus_message_iter_close_container(&value, &arrayIter);

		/* close vararg*/
		dbus_message_iter_close_container(&entry, &value);

		/* close entry */
		dbus_message_iter_close_container(&dict, &entry);
	}

	if (args->pathloss != DISTANCE_VAL_INVALID)
		dict_append_entry(&dict, "Pathloss", DBUS_TYPE_UINT16,
				&args->pathloss);

	if (args->rssi != DISTANCE_VAL_INVALID)
		dict_append_entry(&dict, "RSSI", DBUS_TYPE_INT16, &args->rssi);

	if (args->transport != NULL)
		dict_append_entry(&dict, "Transport", DBUS_TYPE_STRING,
				&args->transport);

	dbus_message_iter_close_container(iter, &dict);
}

static void set_discovery_filter_reply(DBusMessage *message, void *user_data)
{
	DBusError error;

	dbus_error_init(&error);
	if (dbus_set_error_from_message(&error, message) == TRUE) {
		SLOGI("SetDiscoveryFilter failed: %s\n", error.name);
		dbus_error_free(&error);
		return;
	}

	if (strcmp(filtered_scan_transport, "le") == 0)
		scan_transport_type = TRANSPORT_LE;
	else if (strcmp(filtered_scan_transport, "bredr") == 0)
		scan_transport_type = TRANSPORT_BREDR;

}

static void cmd_set_scan_filter_commit(void)
{
	struct set_discovery_filter_args args;

	args.uuids = NULL;
	args.pathloss = filtered_scan_pathloss;
	args.rssi = filtered_scan_rssi;
	args.transport = filtered_scan_transport;
	args.uuids = filtered_scan_uuids;

	if (check_default_ctrl() == FALSE)
		return;

	if (g_dbus_proxy_method_call(default_ctrl, "SetDiscoveryFilter",
				set_discovery_filter_setup, set_discovery_filter_reply,
				&args, NULL) == FALSE) {
		SLOGI("Failed to set discovery filter\n");
		return;
	}
}

static void cmd_set_scan_filter_transport(const char *arg)
{
	g_free(filtered_scan_transport);

	if (!arg || !strlen(arg))
		filtered_scan_transport = NULL;
	else
		filtered_scan_transport = g_strdup(arg);

	cmd_set_scan_filter_commit();
}

static void cmd_set_scan_filter_clear(const char *arg)
{
	/* set default values for all options */
	filtered_scan_rssi = DISTANCE_VAL_INVALID;
	filtered_scan_pathloss = DISTANCE_VAL_INVALID;
	g_slist_free_full(filtered_scan_uuids, g_free);
	filtered_scan_uuids = NULL;
	g_free(filtered_scan_transport);
	filtered_scan_transport = NULL;

	cmd_set_scan_filter_commit();
}

static struct GDBusProxy *find_device(const char *arg)
{
	GDBusProxy *proxy;

	if (!arg || !strlen(arg)) {
		if (default_dev)
			return default_dev;
		blu_debug("Missing device address argument\n");
		return NULL;
	}

	proxy = find_proxy_by_address(dev_list, arg);
	if (!proxy) {
		blu_debug("Device %s not available\n", arg);
		return NULL;
	}

	return proxy;
}

static void pair_reply(DBusMessage *message, void *user_data)
{
	DBusError error;
	GDBusProxy *proxy = user_data;
	struct bluetooth_device *device;

	dbus_error_init(&error);

	if (dbus_set_error_from_message(&error, message) == TRUE) {
		blu_debug(">> Failed to pair: %s\n", error.name);
		dbus_error_free(&error);
		return;
	}

	device = find_device_in_list(proxy);
	if (device == NULL)
		return;

	device->status = DEVICE_PAIRED;
	return;
}

static void cmd_pair(const char *arg)
{
	GDBusProxy *proxy;

	proxy = find_device(arg);
	if (!proxy) {
		blu_debug(">> Device %s not available. Please scan first\n", arg);
		return;
	}

	if (g_dbus_proxy_method_call(proxy, "Pair", NULL, pair_reply,
							proxy, NULL) == FALSE) {
		blu_debug(">> Failed to send Pair method\n");
		return;
	}
}

static void remove_device_reply(DBusMessage *message, void *user_data)
{
	DBusError error;
	char * path = user_data;

	dbus_error_init(&error);

	if (dbus_set_error_from_message(&error, message) == TRUE) {
		blu_debug("Failed to remove device: %s\n", error.name);
		dbus_error_free(&error);
		return;
	}
}

static void cmd_trust_reply(const DBusError *error, void *user_data)
{
	GDBusProxy *proxy = user_data;
	struct bluetooth_device *device;

	device = find_device_in_list(proxy);
	if (device == NULL)
		return;

	device->trusted = !device->trusted;
}

static void cmd_trust(const char *arg)
{
	GDBusProxy *proxy;
	dbus_bool_t trusted;

	proxy = find_device(arg);
	if (!proxy)
		return;

	trusted = TRUE;

	if (g_dbus_proxy_set_property_basic(proxy, "Trusted",
				DBUS_TYPE_BOOLEAN, &trusted,
				cmd_trust_reply, NULL, NULL) == TRUE)
		return;
}

static void cmd_untrust(const char *arg)
{
	GDBusProxy *proxy;
	dbus_bool_t trusted;

	proxy = find_device(arg);
	if (!proxy)
		return;

	trusted = FALSE;

	if (g_dbus_proxy_set_property_basic(proxy, "Trusted",
				DBUS_TYPE_BOOLEAN, &trusted,
				cmd_trust_reply, NULL, NULL) == TRUE)
		return;
}

static void remove_device_setup(DBusMessageIter *iter, void *user_data)
{
	const char *path = user_data;

	dbus_message_iter_append_basic(iter, DBUS_TYPE_OBJECT_PATH, &path);
}

static void remove_device(GDBusProxy *proxy)
{
	char *path;

	path = g_strdup(g_dbus_proxy_get_path(proxy));

	blu_debug("remove path:%s\n", path);

	if (g_dbus_proxy_method_call(default_ctrl, "RemoveDevice",
						remove_device_setup,
						remove_device_reply,
						path, g_free) == FALSE) {
		blu_debug("Failed to remove device\n");
		g_free(path);
	}
}

static void cmd_remove(const char *arg)
{
	GDBusProxy *proxy;

	if (!arg || !strlen(arg)) {
		blu_debug("Missing device address argument\n");
		return;
	}

	if (check_default_ctrl() == FALSE)
		return;

	if (strcmp(arg, "*") == 0) {
		GList *list;

		for (list = g_list_first(dev_list); list; list = g_list_next(list)) {
			GDBusProxy *proxy = list->data;
			remove_device(proxy);
		}

		return;
	}

	proxy = find_proxy_by_address(dev_list, arg);
	if (!proxy) {
		blu_debug("Device %s not available\n", arg);
		return;
	}

	remove_device(proxy);
}

static void cmd_quit(void)
{
	g_main_loop_quit(main_loop);
}

static void connect_reply(DBusMessage *message, void *user_data)
{
	GDBusProxy *proxy = user_data;
	DBusError error;

	dbus_error_init(&error);

	if (dbus_set_error_from_message(&error, message) == TRUE) {
		blu_debug(">> Failed to connect: %s\n", error.name);
		dbus_error_free(&error);
		return;
	}

	set_default_device(proxy, NULL);
}

static void cmd_connect(const char *arg)
{
	GDBusProxy *proxy;

	if (!arg || !strlen(arg)) {
		blu_debug(">> Missing device address argument\n");
		return;
	}

	proxy = find_proxy_by_address(dev_list, arg);
	if (!proxy) {
		blu_debug(">> Device %s not available. Please scan first\n", arg);
		return;
	}

	if (g_dbus_proxy_method_call(proxy, "Connect", NULL, connect_reply,
							proxy, NULL) == FALSE) {
		blu_debug(">> Failed to send dbus connect method\n");
		return;
	}
}

static void disconn_reply(DBusMessage *message, void *user_data)
{
	GDBusProxy *proxy = user_data;
	DBusError error;

	dbus_error_init(&error);

	if (dbus_set_error_from_message(&error, message) == TRUE) {
		blu_debug(">> Failed to disconnect: %s\n", error.name);
		dbus_error_free(&error);
		return;
	}
}

static void cmd_disconn(const char *arg)
{
	GDBusProxy *proxy;

	proxy = find_device(arg);
	if (!proxy) {
		blu_debug(">> Device %s not available. Please scan first\n", arg);
		return;
	}

	if (g_dbus_proxy_method_call(proxy, "Disconnect", NULL, disconn_reply,
							proxy, NULL) == FALSE) {
		blu_debug(">> Failed to send Disconnect method\n");
		return;
	}

	if (strlen(arg) == 0) {
		DBusMessageIter iter;

		if (g_dbus_proxy_get_property(proxy, "Address", &iter) == TRUE)
			dbus_message_iter_get_basic(&iter, &arg);
	}
}

static void cmd_select_attribute(const char *arg)
{
	GDBusProxy *proxy;

	if (!arg || !strlen(arg)) {
		blu_debug("Missing attribute argument\n");
		return;
	}

	if (!default_dev) {
		blu_debug("No device connected\n");
		return;
	}

	proxy = gatt_select_attribute(arg);
	if (proxy)
		set_default_attribute(proxy);
}

static struct GDBusProxy *find_attribute(const char *arg)
{
	GDBusProxy *proxy;

	if (!arg || !strlen(arg)) {
		if (default_attr)
			return default_attr;
		blu_debug("Missing attribute argument\n");
		return NULL;
	}

	proxy = gatt_select_attribute(arg);
	if (!proxy) {
		blu_debug("Attribute %s not available\n", arg);
		return NULL;
	}

	return proxy;
}

static void cmd_notify(const char *arg)
{
	dbus_bool_t enable;

	if (parse_argument_on_off(arg, &enable) == FALSE)
		return;

	if (!default_attr) {
		blu_debug("No attribute selected\n");
		return;
	}

	gatt_notify_attribute(default_attr, enable ? true : false);
}

static gboolean signal_handler(GIOChannel *channel, GIOCondition condition,
							gpointer user_data)
{
	static bool terminated = false;
	struct signalfd_siginfo si;
	ssize_t result;
	int fd;

	if (condition & (G_IO_NVAL | G_IO_ERR | G_IO_HUP)) {
		g_main_loop_quit(main_loop);
		return FALSE;
	}

	fd = g_io_channel_unix_get_fd(channel);

	result = read(fd, &si, sizeof(si));
	if (result != sizeof(si))
		return FALSE;

	switch (si.ssi_signo) {
		case SIGINT:
		case SIGTERM:
			g_main_loop_quit(main_loop);
			break;
		case SIGUSR1:
			blu_debug("SIGUSR1\n");
			break;
		case SIGUSR2:
			blu_debug("SIGUSR2\n");
			break;
	}

	return TRUE;
}

static guint setup_signalfd(void)
{
	GIOChannel *channel;
	guint source;
	sigset_t mask;
	int fd;

	sigemptyset(&mask);
	sigaddset(&mask, SIGINT);
	sigaddset(&mask, SIGTERM);
	sigaddset(&mask, SIGUSR1);
	sigaddset(&mask, SIGUSR2);

	if (sigprocmask(SIG_BLOCK, &mask, NULL) < 0) {
		perror("Failed to set signal mask");
		return 0;
	}

	fd = signalfd(-1, &mask, 0);
	if (fd < 0) {
		perror("Failed to create signal descriptor");
		return 0;
	}

	channel = g_io_channel_unix_new(fd);

	g_io_channel_set_close_on_unref(channel, TRUE);
	g_io_channel_set_encoding(channel, NULL, NULL);
	g_io_channel_set_buffered(channel, FALSE);

	source = g_io_add_watch(channel,
				G_IO_IN | G_IO_HUP | G_IO_ERR | G_IO_NVAL,
				signal_handler, NULL);

	g_io_channel_unref(channel);

	return source;
}

static GOptionEntry options[] = {
	{ "version", 'v', 0, G_OPTION_ARG_NONE, &option_version,
				"Show version information and exit" },
	{ NULL },
};

static void client_ready(GDBusClient *client, void *user_data)
{
	guint *input = user_data;

	blu_debug("%s: Line:%d in file:%s\n", __FUNCTION__, __LINE__, __FILE__);
}

static gboolean le_action_wakeup(gpointer data)
{
	gboolean repeat = FALSE;

	// SLOGI("Sen in");
	cmd_scan("off");
	// SLOGI("Sen out");
	return repeat;
}

static void bluetooth_dev_list_free(gpointer data)
{
	struct bluetooth_device *device = data;

	g_free(device->name);
	g_free(device->address);
	return;
}

static int scan_devices(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
	const char *mode = "(unknown)";
	const char *scantime = "(unknown)";

    struct blob_attr *data[2];
    blobmsg_parse(scan_policy, ARRAY_SIZE(scan_policy), data, blob_data(msg), blob_len(msg));

    if(data[0] && data[1])
    {
    	mode = blobmsg_data(data[0]);
    	scantime = blobmsg_data(data[1]);
    	SLOGI("mode = %s\n", mode);
    	SLOGI("scantime = %s\n", scantime);
    	if(!strcmp(mode, "classic"))
    	{
    		cmd_scan("on");
    		g_timeout_add_seconds(strtol(scantime, NULL ,0), le_action_wakeup, NULL);
    	}
    	else if(!strcmp(mode, "ble"))
    	{

    	}
    }
    return 0;
}

static const struct ubus_method blueth_methods[] = {
    UBUS_METHOD("scan_devices", scan_devices, scan_policy),
    // UBUS_METHOD("open_network", open_closenetwork, null_policy),
    // UBUS_METHOD("close_network", open_closenetwork, null_policy),
    // UBUS_METHOD("remove_device", remove_device, get_policy),
    // UBUS_METHOD("set_binary", setbinary, setbinary_policy),
    // UBUS_METHOD("get_binary", getbinary, get_policy),
    // UBUS_METHOD("get_specification", getspecification, get_specification),
    // UBUS_METHOD("set_specification", setspecification, set_specification),
    // UBUS_METHOD("set_secure_spec", setsecurespecification, set_specification),
    // UBUS_METHOD("get_secure_spec", getsecurespecification, get_secure_specification),
    // UBUS_METHOD("reset", reset_device, null_policy),
    // UBUS_METHOD("identify", identify, set_policy),
    // UBUS_METHOD("change_name", changename, set_policy),
    // UBUS_METHOD("alexa", alexa, set_policy),
    // UBUS_METHOD("check_dev_state", checking_devices_state, null_policy),
    // UBUS_METHOD("network", network, set_specification),
    // UBUS_METHOD("get_specification_CRC", getspecification_CRC, specification_CRC),
    // UBUS_METHOD("set_specification_CRC", setspecification_CRC, specification_CRC),
    // UBUS_METHOD("venus_alarm", venus_alarm, alarm_policy),
};

static struct ubus_object_type blueth_object_type =
    UBUS_OBJECT_TYPE("bluetooth", blueth_methods);

static struct ubus_object blueth_object = {
    .name = "bluetooth",
    .type = &blueth_object_type,
    .methods = blueth_methods,
    .n_methods = ARRAY_SIZE(blueth_methods),
};

void notify_status_cb(struct ubus_notify_request *req,
					       int idx, int ret)
{
	SLOGI("idx = %d ret = %d\n", idx, ret);
}

void notify_complete_cb(struct ubus_notify_request *req,
					       int idx, int ret)
{
	SLOGI("idx = %d ret = %d\n", idx, ret);
	free(req);
}

void Send_ubus_notify_asyn(char *data)
{
    SLOGI("ubus notify data = %s\n", data);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, "msg", data);
    
    if(ctx)
    {
    	struct ubus_notify_request *req = (struct ubus_notify_request *)malloc(sizeof(struct ubus_notify_request));
        ubus_notify_async(ctx, &blueth_object, "bluetooth", buff.head, req);
    	req->status_cb = notify_status_cb;
    	req->complete_cb = notify_complete_cb;
    	ubus_complete_request_async(ctx, &(req->req));
    }
}

static int init_ubus_service()
{
    const char *ubus_socket = NULL;

    uloop_init();

    ctx = ubus_connect(ubus_socket);
    if (!ctx) {
        fprintf(stderr, "Failed to connect to ubus\n");
        return -1;
    }

    ubus_add_uloop(ctx);

    return 0;
}

static void blueth_ubus_service()
{
    int ret;

    ret = ubus_add_object(ctx, &blueth_object);
    if (ret)
        fprintf(stderr, "Failed to add object: %s\n", ubus_strerror(ret));

    //uloop_timeout_set(&polling_notify, 1000);

    uloop_run();
}

static void free_ubus_service()
{
    ubus_free(ctx);
    uloop_done();

    if(buff.buf)
    {
        free(buff.buf);
    }
}

void dbus_thread()
{
	GError *error = NULL;
	GDBusClient *client;
	guint signal;
	main_loop = g_main_loop_new(NULL, FALSE);
	dbus_conn = g_dbus_setup_bus(DBUS_BUS_SYSTEM, NULL, NULL);

	signal = setup_signalfd();

	client = g_dbus_client_new(dbus_conn, "org.bluez", "/org/bluez");

	g_dbus_client_set_connect_watch(client, connect_handler, NULL);
	g_dbus_client_set_disconnect_watch(client, disconnect_handler, NULL);

	g_dbus_client_set_signal_watch(client, message_handler, NULL);

	g_dbus_client_set_proxy_handlers(client, proxy_added, proxy_removed,
							property_changed, NULL);

	input = 0;
	g_dbus_client_set_ready_watch(client, client_ready, &input);

	// g_timeout_add_seconds(LE_ACTION_TIME_WAIT, le_action_wakeup, NULL);
	g_main_loop_run(main_loop);

	g_dbus_client_unref(client);
	g_source_remove(signal);
	if (input > 0)
		g_source_remove(input);

	dbus_connection_unref(dbus_conn);
	g_main_loop_unref(main_loop);

	g_list_free_full(ctrl_list, proxy_leak);
	g_list_free_full(dev_list, proxy_leak);

	g_list_free_full(bluetooth_device_list, bluetooth_dev_list_free);
done:
	return 0;
}

int main(int argc, char *argv[])
{
	char cmd[256];
    sprintf(cmd, "%s", "ps | grep ble_central | head -n -3 | cut -c-5 | xargs kill -9 &>/dev/null");
    system(cmd);

	SLOG_Init(SLOG_STDOUT, NULL);
    set_thread_name( "ble_central" );

	pthread_t dbus_thread_t;
    pthread_create(&dbus_thread_t, NULL, (void *)&dbus_thread, NULL);
    pthread_detach(dbus_thread_t);

	init_ubus_service();
	blueth_ubus_service();
	free_ubus_service();

// 	GOptionContext *context;
// 	GError *error = NULL;
// 	GDBusClient *client;
// 	guint signal;

// 	context = g_option_context_new(NULL);
// 	g_option_context_add_main_entries(context, options, NULL);

// 	if (g_option_context_parse(context, &argc, &argv, &error) == FALSE) {
// 		if (error != NULL) {
// 			g_printerr("%s\n", error->message);
// 			g_error_free(error);
// 		} else
// 			g_printerr("An unknown error occurred\n");
// 		goto done;
// 	}

// 	g_option_context_free(context);
// 	if (option_version == TRUE) {
// 		printf("%s\n", VERSION);
// 		goto done;
// 	}

// 	main_loop = g_main_loop_new(NULL, FALSE);
// 	dbus_conn = g_dbus_setup_bus(DBUS_BUS_SYSTEM, NULL, NULL);
// 	printf("2");
// 	signal = setup_signalfd();
// 	printf("3");
// 	client = g_dbus_client_new(dbus_conn, "org.bluez", "/org/bluez");

// 	g_dbus_client_set_connect_watch(client, connect_handler, NULL);
// 	g_dbus_client_set_disconnect_watch(client, disconnect_handler, NULL);

// 	g_dbus_client_set_signal_watch(client, message_handler, NULL);

// 	g_dbus_client_set_proxy_handlers(client, proxy_added, proxy_removed,
// 							property_changed, NULL);
// 	printf("5");
// 	input = 0;
// 	g_dbus_client_set_ready_watch(client, client_ready, &input);

// 	g_timeout_add_seconds(LE_ACTION_TIME_WAIT, le_action_wakeup, NULL);
// 	g_main_loop_run(main_loop);
// 	printf("7");
// 	g_dbus_client_unref(client);
// 	g_source_remove(signal);
// 	if (input > 0)
// 		g_source_remove(input);

// 	dbus_connection_unref(dbus_conn);
// 	g_main_loop_unref(main_loop);

// 	g_list_free_full(ctrl_list, proxy_leak);
// 	g_list_free_full(dev_list, proxy_leak);

// 	g_list_free_full(bluetooth_device_list, bluetooth_dev_list_free);
// done:
	return 0;

}
