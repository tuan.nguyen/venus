#include <stdio.h>
#include "command_api.h"
#include "wemo_upnp_api.h"

int GetNetworkList(char* UDN, char *param_val[], void *cookie)
{
  return PluginCtrlPointSendAction(0, UDN, "GetNetworkList", 0x00, 0x00, 0x00, cookie);
}

int GetNetworkStatus(char* UDN, char *param_val[], void *cookie)
{
  return PluginCtrlPointSendAction(0, UDN, "GetNetworkStatus", 0x00, 0x00, 0x00, cookie);
}

int GetMacAddr(char* UDN, char *param_val[], void *cookie){
  return PluginCtrlPointSendAction(2, UDN, "GetMacAddr", 0x00, 0x00, 0x00, cookie);
}

int Connect_network(char* UDN, char *param_val[], void *cookie)
{
  char* paramNames[] = {"ssid", "auth", "password", "encrypt", "channel"};
  return PluginCtrlPointSendAction(0, UDN, "ConnectHomeNetwork", (const char **)paramNames, param_val, 0x05, cookie);
}

int Close_setup(char* UDN, char *param_val[], void *cookie)
{
  return PluginCtrlPointSendAction(0, UDN, "CloseSetup", 0x00, 0x00, 0x00, cookie);
}

int GetIconURL(char* UDN, char *param_val[], void *cookie)
{
  PluginCtrlPointSendAction(2, UDN, "GetIconURL", 0x00, 0x00, 0x00, cookie);
  return 1;
}

int GetBinaryState(char* UDN, char *param_val[], void *cookie)
{
  return PluginCtrlPointSendAction(2, UDN, "GetBinaryState", 0x00, 0x00, 0x00, cookie);
}

int SetBinaryState(char* UDN, char *param_val[], void *cookie)
{
  char *paramNames[] = {"BinaryState", "brightness"};
  return PluginCtrlPointSendAction(2, UDN, "SetBinaryState", (const char **)paramNames, param_val, 2, cookie);
}

int GetFriendlyName(char* UDN, char *param_val[], void *cookie)
{
  return PluginCtrlPointSendAction(2, UDN, "GetFriendlyName", 0x00, 0x00, 0x00, cookie);
}

int GetDeviceStatus(char* UDN, char *param_val[], void *cookie)
{
  char *paramNames[] = {"DeviceIDs"};
  return PluginCtrlPointSendAction(7, UDN, "GetDeviceStatus", (const char **)paramNames, param_val, 1, cookie);
}
////////////////////WEMO_LINK///////////////////////////////
int opennetwork(char* UDN, char *param_val[], void *cookie)
{
  char *paramNames[] = {"DevUDN"};
  return PluginCtrlPointSendAction(7, UDN, "OpenNetwork", (const char **)paramNames, param_val, 1, cookie);
}

int closenetwork(char* UDN, char *param_val[], void *cookie)
{
  char *paramNames[] = {"DevUDN"};
  return PluginCtrlPointSendAction(7, UDN, "CloseNetwork", (const char **)paramNames, param_val, 1, cookie);
}


int GetEndDevices(char* UDN, char *param_val[], void *cookie)
{
  char *paramNames[] = {"DevUDN", "ReqListType"};
  return PluginCtrlPointSendAction(7, UDN, "GetEndDevices", (const char **)paramNames, param_val, 2, cookie);
}

int AddSubDevice(char* UDN, char *param_val[], void *cookie)
{
  char *paramNames[] = {"DeviceIDs"};
  return PluginCtrlPointSendAction(7, UDN, "AddDevice", (const char **)paramNames, param_val, 1, cookie);
}

int RemoveSubDevice(char* UDN, char *param_val[], void *cookie)
{
  char *paramNames[] = {"DeviceIDs"};
  return PluginCtrlPointSendAction(7, UDN, "RemoveDevice", (const char **)paramNames, param_val, 1, cookie);
}

int SetDeviceStatus(char* UDN, char *param_val[], void *cookie)
{
  char *paramNames[] = {"DeviceStatusList"};
  return PluginCtrlPointSendAction(7, UDN, "SetDeviceStatus", (const char **)paramNames, (char **)param_val, 1, cookie);
}













