#ifndef WEMO_UPNP_API_H
#define WEMO_UPNP_API_H

#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <upnp/upnp.h>
#include <upnp/upnptools.h>

#include <json-c/json.h>

#include "utility.h"
#include "upnpCommon.h"
#include "command_api.h"

#include "slog.h"
#include "database.h"
#include "VR_define.h"
#include "group_apis.h"
#include "verik_utils.h"

#define SERVICE_SERVCOUNT 8
#define SIZE_256B 256
#define PLUGIN_ERROR 0
#define PLUGIN_SUCCESS 1

#define SAFE_FREE_XML(_A) \
    do { \
        if (_A) { \
            FreeXmlSource(_A); \
            _A=NULL; \
        } \
    } while (0)

extern int debug_level;
extern char *Database_Location;
extern int MAX_DISCOVER_TIMEOUT;
sqlite3 *upnp_db;

typedef int (*upnp_action)(char* real_name, char *param_val[], void *cookie);
struct device_upnp_action
{
  upnp_action         pUpnpAction;
};

typedef struct device_upnp_action device_action;

struct service {
  char ServiceId[SIZE_256B];
  char ServiceType[SIZE_256B];
  char EventURL[SIZE_256B];
  char ControlURL[SIZE_256B];
  char SID[SIZE_256B];
};

struct Device {
  char UDN[SIZE_256B];
  char DescDocURL[SIZE_256B];
  char FriendlyName[SIZE_256B];
  char PresURL[SIZE_256B];
  int  AdvrTimeOut;
  struct service services[SERVICE_SERVCOUNT];
};

struct DeviceNode {
  struct Device device;
  struct DeviceNode *next;
  int IsDeviceRequestUpdate;
};
typedef struct DeviceNode CtrlPluginDeviceNode, *gGlobalDeviceList;

struct cmdloop_commands {
    /* the string  */
  const char *str;
    /* the command */
  int cmdnum;

  upnp_action pUpnpAction;
} cmdloop_commands;

typedef struct _upnp_cmd_data {
  char *deviceID;
  char *subdevID;
  char *result;
} upnp_cmd_data;

enum cmdloop_tvcmds {
  REDISCOVER = 0,
  GETBINARYSTATUS,
  SETBINARYSTATUS,
  GETICONURL,
  GETNUMBERDEV,
  GETFRIENDNAME,
  GETDEVICESTATUS,
  OPENNETWORK,
  CLOSENETWORK,
  GETENDDEVS,
  ADDSUBDEV,
  REMOVESUBDEV,
  SETDEVSTATUS,
  GETNETWORKLIST,
  CONNECTNET,
  GETMACADDR,
  GETBRIGHTNESS,
  SETBRIGHTNESS,
};

typedef struct command_t 
{
  const char *name;
  int cmdnum;
}command;

enum cmdname {
  ADDRESS = 0,
  FREQUENCY,
  QUALITY,
  SSID,
  IEEE,
  GROUP_CIPHER,
  PAIRWISE,
  AUTH,
  WPA,
  WEMO_DEVICE,
};

typedef struct notify
{
    uint8_t notify_status;
    uint8_t notify_message[1024*4];
}notify_t;

typedef struct notify_queue
{
    notify_t notify[50];
    uint8_t  notify_index;
}notify_queue_t; 

int  init_upnp_sdk(char *ip_address, unsigned short port);

void create_device_from_database();
int CtrlPointDiscoverDevices(char *, void*);
int CtrlPointCallbackEventHandler(Upnp_EventType,void *,void * );
void CtrlPointProcessDeviceDiscoveryEx(IXML_Document *, const char *, int , int);
void subscribeDeviceServices(IXML_Document *, const char *, CtrlPluginDeviceNode*);
int CtrlPointProcessCommand(char *cmd, char* real_name, char *param_val[], void *cookie);
int PluginCtrlPointSendAction(int , char * , const char *, const char **, char **, int , void*);

void ProcessReceiveActionComplete(struct Upnp_Action_Complete *a_event, void * cookie);
void CtrlPointProcessNotifyAction(struct Upnp_Event *);
int StopPluginCtrlPoint(void);
int CtrlPointRemoveAll(void);
int CtrlPointDeleteNode( CtrlPluginDeviceNode *node );

void CtrlPointProcessDeviceByebye(IXML_Document *DescDoc);

CtrlPluginDeviceNode* GetDeviceNodeBySID(const char*);

void LockDeviceSync();
void UnlockDeviceSync();

int update_database(char *, int, char *);
int search_database(char *filename, char *key, char *dev, char*sub_dev, char *ID, char *result);

int copy_data(char *datain, char kind, int place, char *result);

int free_devicenode(CtrlPluginDeviceNode **);

void free_param_val(struct upnpmsg *msg);

int save_DeviceNode(CtrlPluginDeviceNode *devnode, char *filename);
int read_DeviceNode(CtrlPluginDeviceNode *devnode, char *filename);

void printfresult(const char *result);

void RemoveDevicenodeByUDN(CtrlPluginDeviceNode **devnode, char *UDN);
int GetPlaceFromID(char *ID);

void createPasswordKeyData();
void encryptPassword(char *, char *);
int baseDecode(char *, char *, int *);
int decryptPassword(char *, char *);
int create_decrypt_pass(char*, char*);

void polling_wemo_connect_status();
void remove_device_with_id(char *UDN);
#endif

