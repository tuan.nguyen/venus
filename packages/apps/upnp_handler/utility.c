#include "utility.h"

ssize_t readln(int fd, void *buffer, size_t n)
{
    ssize_t numRead;
    size_t totRead;
    char *buf;
    char ch;
    if (n <= 0 || buffer == NULL) {
        return -1;
    }
    buf = buffer;//Allow further pointer arithmetic
    totRead = 0;
    for (;;) {
        numRead = read(fd, &ch, 1);
        if (numRead <= 0) { //Error or EOF
            return -1;
        } else { //One character read
            if (ch == '\n') {
                break;
            }
            if (totRead < n - 1) { //Discard > (n - 1) bytes
                totRead++;
                *buf++ = ch;
            }
        }
    }
    *buf = '\0';
    return totRead;
}

ssize_t readws(int fd, void *buffer, size_t n)
{
    ssize_t numRead;
    size_t totRead;
    char *buf;
    char ch;
    if (n <= 0 || buffer == NULL) {
        return -1;
    }
    buf = buffer;//Allow further pointer arithmetic
    totRead = 0;
    for (;;) {
        numRead = read(fd, &ch, 1);
        if (numRead <= 0) { //Error or EOF
            return -1;
        } else { //One character read
            if (ch == '\r') {
                continue; //next ch should be \n?
            }
            if (ch == '\n') {
                break;
            }
            if (ch == ' ') {
                //eat all the leading white spaces
                if (totRead == 0) { continue; }
                //stop otherwise
                else { break; }
            }
            //Escape character, read one more char and bypassing
            //all special character processing
            if (ch == '\\') {
                numRead = read(fd, &ch, 1);
                if (numRead <= 0) { //Error or EOF
                    return -1;
                }
            }
            if (totRead < n - 1) { //Discard > (n - 1) bytes
                totRead++;
                *buf++ = ch;
            }
        }
    }
    *buf = '\0';
    return totRead;
}

void freeupnpmsg(struct upnpmsg *msg) {

    int i;
    for(i = 0; i < msg->param_num; i++) {
        free(msg->param_val[i]);
    }
    free(msg);
}

char* trim (char *s)
{
    int i;
    while (isspace (*s)) s++;   // skip left side white spaces
    for (i = strlen (s) - 1; (isspace (s[i])); i--) ;   // skip right side white spaces
    s[i + 1] = '\0';
    return s;
}

int Check_path_is_Dir_or_File(char *path, const char *text)
{
    struct stat st={0};
    if(stat(path, &st) == -1)
    {
        printf("%s\n", text);
        return -2;
    }
    switch(st.st_mode & S_IFMT)
    {
        case S_IFBLK: LOG(LOG_DBG, "%s: this is Block device, %s, program exitting\n", path, text); break;
        case S_IFCHR: LOG(LOG_DBG, "%s: this is Character device, %s, program exitting\n", path, text); break;
        case S_IFDIR: LOG(LOG_DBG, "%s: this is Directory.\n", path); return 1; break;
        case S_IFIFO: LOG(LOG_DBG, "%s: this is FIFO/pipe, %s, program exitting\n", path, text); break;
        case S_IFLNK: LOG(LOG_DBG, "%s: this is Symlink, %s, program exitting\n", path, text); break;
        case S_IFREG: LOG(LOG_DBG, "%s: this is Regular file.\n", path); return 0; break;
        case S_IFSOCK: LOG(LOG_DBG, "%s: this is Socket, %s, program exitting\n", path, text); break;
        default: LOG(LOG_DBG, "%s: unknown\n", path); break;
    }
    return -1;
}

int make_daemon()
{
//--------------------------------------------
//Create a daemon process
// * Is a child of 'init'
// * Does not associate with any controlling terminal
//--------------------------------------------

    //Fork a child process and exit. Child will become a child of init
    //and is guaranteed not to be a process group leader
    switch (fork()) {
        case -1: return -1; //Failed
        case  0: break;     //Child continues
        default: exit(0);   //Parent exits
    }

    //Start a new session and break the association with the
    //any controlling terminal
    int rv;
    rv = setsid();

    if (rv == -1) { //Failed to start a new session
        return -1;
    }

    //We don't want the daemon to reacquire any controlling
    //terminal. Make another fork to resign from session leader
    switch (fork()) { 
        case -1: return -1; //Failed
        case  0: break;     //(Grand)child continues
        default: exit(0);   //Parent exits
    }


//--------------------------------------------
//Setup appropriate environment for the daemon
//--------------------------------------------

    // Set appropriate umask
    umask(0);

    // Change to appropriate working directory
    chdir("/");

    //Close all unused open files inherited from (grand)parent
    int fd;
    for (fd = 0; fd < MAX_FD; fd++) {
        close(fd);
    }

    //Reopen STDIN/STDOUT/STDERR and redirect them to /dev/null to prevent
    //library functions that operate on these FDs from unexpectedly fail
    //Ensure FD 0 is closed (incase someone messed up the code in previous
    //step. Open /dev/null. The fd should be 0 (i.e. STDIN_FILEN0)
    close(STDIN_FILENO);
    fd = open("/dev/null", O_RDWR); //Can add a check for fd==0 here.

    //Redirect STDOUT/STDERR to /dev/null
    dup2(STDIN_FILENO, STDOUT_FILENO);
    dup2(STDIN_FILENO, STDERR_FILENO);
    
    return 0;
}

char* read_option(const char* option)
{
    struct uci_context *ctx;
    struct uci_ptr ptr;
    int ret;
    char* data = NULL;

    char *input = strdup(option);
    ctx = uci_alloc_context();
    if (!ctx)
    {
        printf("Failed to alloc uci_context.\n");
        return data;
    }
    ret = uci_lookup_ptr(ctx, &ptr, input, 1);

    if(ret == UCI_OK)
    {
        if(ptr.o)
        {
            data = strdup(ptr.o->v.string);
        }
    }

    uci_free_context(ctx);
    free(input);

    return data;
}