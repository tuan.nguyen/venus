//#include <signal.h>
#include "utility.h"
#include "wemo_upnp_api.h"

#define BUF_SIZE 255
#define MAX_LENGTH_INDEX 20

char *path_default = "/tmp/upnp.conf";
char *Database_path = "/etc";
char *Database_Location = NULL;

enum arguments {
    d = 0,  //device id
    c,      //config file
    v,      //value
    l,      //log
    t,      //type of device
    o,      //time out
    ci,      //capability id
    cv,       //capability value
    ct         //capability time
};

struct opts {
    /* the string  */
  const char *str;
    /* the command */
  int optnum;

} opts;

struct opts opt_list[] = {
    {"-d",              d},
    {"-c",              c},
    {"--config-file",   c},
    {"-v",              v},
    {"--value",         v},
    {"-b",              v}, //for bulb
    {"-k",              v}, // for kind of subdev, all_list, pair list
    {"-l",              l},
    {"--debug",         l},
    {"-t",              t},
    {"--dev-type",      t},
    {"--timeout",       o},

    {"-ci",             ci},
    {"-cv",             cv},
    {"-ct",             ct}
};

struct commandlist {
    const char *command;
};

struct commandlist command_list[]= {
    "help",
    "show",
    "rediscover",
    "getbinary",
    "setbinary",
    "geticonurl",
    "getname",
    "opennetwork",
    "discoversubdev",
    "setdevstatus",
    "getdevstatus",
    "closenetwork",
    "addsubdev",
    "removesubdev",
    "getnetlist"
};

int commands_check(char *cmd)
{
    int i;
    int numcmds = sizeof(command_list)/sizeof(struct commandlist);
    for(i=0; i<numcmds; i++)
    {
        if(!strcmp(command_list[i].command, cmd))
        {
            return 1;
        }
    }
    return 0;
}

void help(void) {
    printf("WeMo Controll App version 0.4\n\n");
    printf("usage: ./client command [option] argv\n\n");

    printf("options use for all command\n");
    printf("    -c --config-file <path_of_config>\t\tChoosing config file.\n"
           "    -l --debug \t\t\t\t\tShow debug log.\n\n"
            );
    printf("    Available Commands:\n");
    printf("\tshow \t\t\t\t\tShow the list of devices discovered and\n"
                "\t\t\t\t\t\ttheir basic info.\n");
    printf("\trediscover -t <type_of_device>\t\tRescan Belkin devices in network.\n"
                "\t   --dev-type <type of device>\t\tThe device type has to be matched\n"
                                        "\t\t\t\t\t\twith the value of <deviceType> tag\n"
                                        "\t\t\t\t\t\tin setup.xml files (not necessarily\n"
                                        "\t\t\t\t\t\tmatch with (sub)string in UDN fields).\n"
            );
    printf("\tgetbinary -d <device ID> \t\tGet status of device.Device ID is the\n"
                              "\t\t\t\t\t\tfirst field on each row in the device\n"
                              "\t\t\t\t\t\tlist returned by the 'show' command.\n"
            );
    printf("\tsetbinary -d <device ID> -v <setvalue>\tSet status of device.\n"
                        "\t\t\t    --value <setvalue>\n"
            );
    printf("\tgeticonurl -d <device ID>\t\tGet icon url of device.\n");
    printf("\tgetname -d <device ID>\t\t\tGet device name.\n");
    printf("\topennetwork -d <device ID> \t\topen network to permit zigbee could join to network\n");
    printf("\tclosenetwork -d <device ID> \t\tclose network\n");
    printf("\tdiscoversubdev -d <device ID> -k <ALL_LIST/SCAN_LIST/PAIRED_LIST/UNPAIRED_LIST> \n");
    printf("\tsetdevstatus -d <device ID> -b <bulb id> -ci <10006/10008> -cv <0->255> -ct <0->65535>\n");
    printf("\tgetdevstatus -d <device ID> -b <bulb id>\n");
    printf("\taddsubdev -d <device ID> -b <bulb id>\n");
    printf("\tremovesubdev -d <device ID> -b <bulb id>\n");
    printf("\n");
}

int check_opt(char *argv, struct opts opt_list[], int numopt)
{
    int j;
    for(j=0; j< numopt; j++)
    {
        if(strcasecmp(argv, opt_list[j].str ) == 0)
        {
            return opt_list[j].optnum;
        }
    }
    return -1;
}

int getdata(char **param_val, char *argv, const char *argument, int numopt)
{
    if(check_opt(argv, opt_list, numopt) != -1) 
    {
        printf("need value for argument %s\n", argument);
    }
    else
    {
        *param_val = (char *)malloc(strlen(argv)+1);
        strcpy(*param_val, argv);
        return 1;
    }
    return 0;    
}

int parse_arg(int *i, int argc, const char* argument, struct upnpmsg *temp, int num_param, char **argv, int numopt)
{
    if(*i+1 < argc)
    {
        if(getdata(&temp->param_val[num_param], argv[*i+1], argument, numopt))
        {
            temp->param_num++;
            *i++;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        printf("need value for argument %s\n", argument);
        return 0;
    }  
    return 1;
}
static int callback(void *data, int argc, char **argv, char **azColName){
   int i;
   printf(" + ");
   for(i=1; i<argc; i++){
        if(i == (argc - 1))
            printf("%s ", argv[i] ? argv[i] : "NULL");
        else 
            printf("%s | ", argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}

static int db_callback(void *data, int argc, char **argv, char **azColName){
    char bridge[64];
    memset(bridge, 0x00, sizeof(bridge));
    int i;
    for(i=0; i<argc; i++){
        if(i == (argc - 1))
            printf("%s ", argv[i] ? argv[i] : "NULL");
        else 
            printf("%s | ", argv[i] ? argv[i] : "NULL");
        if(strstr(argv[i], "uuid:Bridge"))
        {
            strncpy(bridge, argv[i], sizeof(bridge)-1);
        }
    }
    printf("\n");
    if(bridge && strlen(bridge))
    {
        char sql[256];
        char *zErrMsg = 0;
        sprintf(sql, "%s%s%s", "SELECT * from SUB_DEVICES where Owner='", bridge, "';");
        int rc = sqlite3_exec(upnp_db, sql, callback, 0, &zErrMsg);
        if( rc != SQLITE_OK ){
          fprintf(stderr, "SQL error: %s\n", zErrMsg);
          sqlite3_free(zErrMsg);
        }
    }
    
    return 0;
}

void signalhandler(int signum)
{
    printf("Sorry, program got segmentation fault, see you again\n");
    exit(1);
}

int main (int argc, char ** argv)
{
    signal(SIGSEGV, signalhandler);

    struct upnpmsg *temp = (struct upnpmsg*)malloc(sizeof(struct upnpmsg));
    int rec, i, optnum;
    int numopt = sizeof(opt_list)/sizeof(opts);

    char *config_path = NULL;
    char *upnp_ip_address = NULL;
    unsigned short int upnp_port = 0;

//////////////////////// init upnp message /////////////////////
    for(i=0; i < MAX_PARAM_NUM; i++)
    {
        temp->param_val[i] = NULL;
    }
    temp->param_num = 0;
    memset(temp->real_name, 0x00, sizeof(temp->real_name));
///////////////////////////////////////////////////////////////
    if(argc == 1)
    {
        help();
        goto end;
    }

    strncpy(temp->cmd, argv[1], strlen(argv[1]));
    
    LOG(LOG_DBG, "temp->cmd = %s\n", temp->cmd);
    rec = commands_check(temp->cmd);

    if(!rec)
    {
        printf("%s : Wrong command\n", temp->cmd);
        goto end;
    }

    if (!strcmp(temp->cmd,"help")) 
    {
        help();
        goto end;
    }
    printf("######################################################\n");
    printf("############# Start check configuration ##############\n\n");

    for(i=2; i< argc; i++)
    {
        optnum = check_opt(argv[i], opt_list, numopt);
        switch(optnum)
        {
            case d:
            {
                if(i+1 < argc)
                {
                    if(check_opt(argv[i+1], opt_list, numopt) != -1) 
                    {
                        printf("need value for argument -d\n");
                        goto end;
                    }
                    else
                    {
                        strncpy(temp->real_name, argv[i+1], sizeof(temp->real_name)-1);
                        i++;
                    }    
                }
                else
                {
                    printf("need value for argument -d\n");
                    goto end;
                }
                break;
            }

            case c:
            {
                if(i+1 < argc)
                {
                    if(check_opt(argv[i+1], opt_list, numopt) != -1) 
                    {
                        printf("need value for argument -c or --config_file\n");
                        goto end;
                    }
                    else
                    {
                        config_path = (char *)malloc(strlen(argv[i+1])+1);
                        strcpy(config_path, argv[i+1]);
                        i++;

                        int tmp = Check_path_is_Dir_or_File(config_path, "File does NOT exist, need a FILE for option \"-c\" or \"--config-file\"");
                        if(tmp != 0)
                        {
                            if(tmp == 1)
                            {
                                printf("Need a File for option \"-c\" or \"--config-file\"\n");
                            }
                            goto end;
                        }

                        FILE *tmp_fp;
                        tmp_fp = fopen(config_path, "r");
                        if(tmp_fp == NULL)
                        {
                            printf("can not open file %s\n", config_path);
                            goto end;
                        }
                        fclose(tmp_fp);
                    }
                }
                else
                {
                    printf("need value for argument -c or --config_file\n");
                    goto end;
                }
                break;
            }

            case t:
            {
                if(i+1 < argc)
                {
                    if(check_opt(argv[i+1], opt_list, numopt) != -1) 
                    {
                        printf("need value for argument -t or --dev-type\n");
                        goto end;
                    }
                    else
                    {
                        temp->param_val[0] = (char *)malloc(strlen(argv[i+1])+19);
                        strcpy(temp->param_val[0], "urn:Belkin:device:");
                        strcat(temp->param_val[0], argv[i+1]);
                        temp->param_num++;
                        i++;
                    }    
                }
                else
                {
                    printf("need value for argument -t or --dev-type\n");
                    goto end;
                }
                break;
            }

            case v:
            {
               if(!parse_arg(&i, argc, "-v or --value", temp, 0, argv, numopt))
               {
                    goto end;
               }
               break;
            }

            case ci:
            {
               if(!parse_arg(&i, argc, "-v or --value", temp, 1, argv, numopt))
               {
                    goto end;
               }
               break;
            }

            case cv:
            {
               if(!parse_arg(&i, argc, "-v or --value", temp, 2, argv, numopt))
               {
                    goto end;
               }
               break;
            }

            case ct:
            {
               if(!parse_arg(&i, argc, "-v or --value", temp, 3, argv, numopt))
               {
                    goto end;
               }
               break;
            }

            case o:
            {
                if(i+1 < argc)
                {
                    if(check_opt(argv[i+1], opt_list, numopt) != -1) 
                    {
                        printf("need value for argument --timeout\n");
                        goto end;
                    }
                    else
                    {
                        MAX_DISCOVER_TIMEOUT = atoi(argv[i+1]);
                        i++;
                    }    
                }
                else
                {
                    printf("need value for argument --timeout\n");
                    goto end;
                }
                break;
            }

            case l:
            {
                if(i+1 < argc)
                {
                    if(check_opt(argv[i+1], opt_list, numopt) == -1)
                    {
                        printf("no need argument behind -l or --debug\n");
                        goto end;
                    }
                }
                debug_level = 1;
                break;
            }

            default:
            {
                if(argv[i][0] == '-') 
                {
                    printf("not support argument %s\n", argv[i]);
                    goto end;
                }
            }
        }
    }

    if(config_path == NULL)
    {
        char *tmp_Database_Location = getenv("UPNP_DATABASE_PATH");
        if(tmp_Database_Location != NULL && strlen(tmp_Database_Location))
        {
            Database_Location = (char *)malloc(strlen(tmp_Database_Location)+1);
            strcpy(Database_Location, tmp_Database_Location);
            printf("Got database location from environment\n");
        }
        
        config_path = (char *)malloc(strlen(path_default)+1);
        strcpy(config_path, path_default);
    }

    rec = Check_path_is_Dir_or_File(config_path, ""); //0 : File, 1 : Dir
    if(rec != 0)
    {
        printf("can not open %s, using default config\n", config_path);
        if(Database_Location == NULL)
        {
            Database_Location = (char *)malloc(strlen(Database_path)+1);
            strcpy(Database_Location, Database_path);
        }
    }
    else
    {
        printf("start read config from %s\n", config_path);
        char tmp[128];
        FILE *fp;
        fp = fopen(config_path, "r");
        if(fp!=NULL)
        {
            while(!feof(fp))
            {
                fgets(tmp, 128, fp);

                if(tmp[0] == '#') {
                    continue; //skipping comments
                }
                if(strstr(tmp, "upnp_ip_address")!=NULL)
                {
                    char *ch = strstr(tmp, "=");
                    char ip[16];
                    upnp_ip_address = (char *)calloc(1,16);
                    strcpy(ip, tmp+(ch-tmp)+1);
                    strcpy(upnp_ip_address, trim(ip));
                }
                if(strstr(tmp, "upnp_client_port")!=NULL)
                {
                    char *ch = strstr(tmp, "=");
                    upnp_port = atoi(tmp+(ch-tmp)+1);
                }

                if(Database_Location == NULL)
                {
                    if(strstr(tmp, "upnp_database_path")!=NULL)
                    {
                        char *ch = strstr(tmp, "=");
                        char link[SIZE_256B];
                        strcpy(link, tmp+(ch-tmp)+1);
                        free(Database_Location);
                        Database_Location = (char *)malloc(strlen(link)+1);
                        strcpy(Database_Location, trim(link));
                    }
                }
            }
                fclose(fp);   
        }
        else
        {
            printf("can not open %s, please check it\n", config_path);
            goto end;
        }
    }

    if(Database_Location == NULL)
    {
        Database_Location = (char *)malloc(strlen(Database_path)+1);
        strcpy(Database_Location, Database_path);
    }

    LOG(LOG_DBG, "config_path = %s\n", config_path);
    printf("Database_Location = %s\n", Database_Location);
    LOG(LOG_DBG, "debug_level = %d\n", debug_level);
    LOG(LOG_DBG, "temp->real_name = %s\n", temp->real_name);
    for(i=0; i< MAX_PARAM_NUM; i++)
    {
        LOG(LOG_DBG, "param_val %d value %s\n", i, temp->param_val[i]);
    }

    rec = Check_path_is_Dir_or_File(Database_Location, "Directory does NOT exist, need a Directory for database, "
                                                      "please check upnp_database_path variable, \nProgram exiting."); //0 : File, 1 : Dir
    if(rec != 1)
    {
        if(rec == 0)
        {
            printf("Need a Directory for database\n");
        }
        goto end;
    }

    if (!strcmp(temp->cmd,"show")) 
    {
        printf("\n####### End check configuration, Start program #######\n");
        printf("######################################################\n\n");

        printf("\n");
        printf("-----Results-----\n");

        char *zErrMsg = 0;
        char DtbLocation[strlen(Database_Location)+strlen("/devicelist.db")+1];
        strcpy(DtbLocation, Database_Location);
        strcat(DtbLocation, "/devicelist.db");
        /* Open database */
        int rc = sqlite3_open(DtbLocation, &upnp_db);
        if( rc )
        {
            fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(upnp_db));
            return -1;
        }

        char *sql = "SELECT *from CONTROL_DEVS where Type='upnp'";
        rc = sqlite3_exec(upnp_db, sql, db_callback, 0, &zErrMsg);
        if( rc != SQLITE_OK )
        {
          fprintf(stderr, "SQL error: %s\n", zErrMsg);
          sqlite3_free(zErrMsg);
        }

        printf("=================\n");
        printf("\n");
        if(upnp_db)
            sqlite3_close(upnp_db);
        goto end;
    }

    if(strcmp(temp->cmd,"rediscover"))
    {
        if(temp->real_name[0] == 0x00)
        {
            printfresult("Need Device Identification, Use option \"-d\" <Device ID>\n");
            goto end;
        }
    }

    rec = init_upnp_sdk(upnp_ip_address, upnp_port);
    if (rec != 0) {
        printf("init_upnp_sdk failed! Program exiting...\n");
        goto end;
    }

    printf("\n####### End check configuration, Start program #######\n");
    printf("######################################################\n\n");

    CtrlPointProcessCommand(temp->cmd, temp->real_name, temp->param_val, NULL);
    StopPluginCtrlPoint();
end:

    if(upnp_ip_address)
    {
        free(upnp_ip_address);
    }

    if(Database_Location)
    {
        free(Database_Location);
    }

    if(config_path)
    {
        free(config_path);
    }

    if(temp)
    {
        free_param_val(temp);
        free(temp);
    }
    exit(0);
}
