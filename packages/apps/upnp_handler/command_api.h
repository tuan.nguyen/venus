#ifndef COMMAND_API_H
#define COMMAND_API_H

int GetNetworkList(char *UDN, char *param_val[], void *cookie);
int Close_setup(char* UDN, char *param_val[], void *cookie);
int Connect_network(char* UDN, char *param_val[], void *cookie);
int GetMacAddr(char* UDN, char *param_val[], void *cookie);
int GetNetworkStatus(char* UDN, char *param_val[], void *cookie);

int GetBinaryState(char *UDN, char *param_val[], void *cookie);
int SetBinaryState(char *UDN, char *param_val[], void *cookie);
int GetFriendlyName(char *UDN, char *param_val[], void *cookie);
int GetIconURL(char *UDN, char *param_val[], void *cookie);

int GetBrightness(char* UDN, char *param_val[], void *cookie);
int SetBrightness(char* UDN, char *param_val[], void *cookie);

////////////////WEMO LINK////////////////////////////////
int opennetwork(char *UDN, char *param_val[], void *cookie);
int closenetwork(char* UDN, char *param_val[], void *cookie);
int GetEndDevices(char *UDN, char *param_val[], void *cookie);
int AddSubDevice(char* UDN, char *param_val[], void *cookie);
int RemoveSubDevice(char* UDN, char *param_val[], void *cookie);
int SetDeviceStatus(char *UDN, char *param_val[], void *cookie);
int GetDeviceStatus(char *UDN, char *param_val[], void *cookie);

#endif
