#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h> 
#include <unistd.h>

#include "aes_inc.h"
#include "wemo_upnp_api.h"
#include "vr_rest.h"
#include "ping.h"

extern ithread_mutex_t notify_lock;
extern notify_queue_t g_notify;

#ifndef DBUG
int debug_level = 0;
#else
int debug_level = 1;
#endif

extern int g_wemo_device_up;
extern int g_wemo_device_state_disconnect;
extern int g_wemo_device_state_NOT_CONNECTED;
extern int g_wemo_device_found_device;

extern char *g_shm;
extern int g_shmid;

extern char g_hub_ip[SIZE_32B];
extern char g_subnet_mask[SIZE_32B];

#define PASSWORD_SALT_LEN   (8+1)
#define PASSWORD_IV_LEN     (16+1)
#define PASSWORD_KEYDATA_LEN    256
#define PASSWORD_MAX_LEN    128
#define DEFAULT_EXPIRES    3600*24

int getmacsuccess=0;
char PasswordKeyData[64];
char DeviceMacAddress[64];
char DeviceSerialNo[64];

int MAX_DISCOVER_TIMEOUT = 30;
int default_timeout = 1801;
int ctrlpt_handle = -1;

short int print_update = 0;//just for print update device

CtrlPluginDeviceNode *GlobalDeviceList = NULL;

//#endif

ithread_mutex_t DeviceListMutex;

char* ServiceList = "urn:Belkin:service:WiFiSetup:1;urn:Belkin:service:basicevent:1;urn:Belkin:service:bridge:1";

char *PluginDeviceServiceType[] = { "urn:Belkin:service:WiFiSetup:1",
"urn:Belkin:service:timesync:1",
"urn:Belkin:service:basicevent:1",
"urn:Belkin:service:firmwareupdate:1",
"urn:Belkin:service:rules:1",
"urn:Belkin:service:metainfo:1",
"urn:Belkin:service:remoteaccess:1",
"urn:Belkin:service:bridge:1"
};

char *Devlist_Location = NULL;
char *Devs_Location = NULL;

int init_upnp_sdk(char *ip_address, unsigned short port)
{
    ithread_mutex_init(&DeviceListMutex, 0);

    ////////////////////////////CREATE DATABASE////////////////////////
    if(open_database(DEVICES_DATABASE, &upnp_db) == -1)
    {
        return UPNP_E_INIT_FAILED;
    }

    int rect = UpnpInit(ip_address, port);

    if(UPNP_E_SUCCESS != rect)
    {
        printf("fail to init upnp with error %d\n", rect);
        UpnpFinish();
        return UPNP_E_INIT_FAILED;
    }

    if (!port) 
    {
        port = UpnpGetServerPort();
    }

    if (!ip_address)
    {
        ip_address = UpnpGetServerIpAddress();
    }
    LOG(LOG_DBG, "start upnp with ip_address = %s port = %d", ip_address, port);

    rect = UpnpRegisterClient(CtrlPointCallbackEventHandler,
                                &ctrlpt_handle, &ctrlpt_handle );

    if(UPNP_E_SUCCESS != rect)
    {
        printf("fail to register client: %d\n",rect);
        UpnpFinish();
        return UPNP_E_INIT_FAILED;
    }
    
    return 0;
}

static uint32_t convert_ip_to_uint(char *ip)
{
    int a, b, c, d;
    uint32_t addr = 0;

    if (sscanf(ip, "%d.%d.%d.%d", &a, &b, &c, &d) != 4)
        return 0;
    addr = a << 24;
    addr |= b << 16;
    addr |= c << 8;
    addr |= d;
    return addr;
}

static int check_ip_in_range(char *ip,char *network,char *mask)
{
    uint32_t ip_addr = convert_ip_to_uint(ip);
    uint32_t network_addr = convert_ip_to_uint(network);
    uint32_t mask_addr = convert_ip_to_uint(mask);
    uint32_t net_lower = (network_addr & mask_addr);
    uint32_t net_upper = (net_lower | (~mask_addr));

    if (ip_addr >= net_lower &&
        ip_addr <= net_upper)
    {
        return 1;
    }

    return 0;
}

static int get_ip_address(char *location, char *result, size_t result_length)
{
    int res = -1;
    if(!location || !result)
    {
        return res;
    }

    char tmp[SIZE_256B];
    strncpy(tmp, location, sizeof(tmp)-1);
    tmp[sizeof(tmp)-1]='\0';

    char *double_slash = strstr(tmp, "//");
    if(!double_slash)
    {
        return res;
    }

    char *fist_colon_after_double_slash = strchr(double_slash, ':');
    if(!fist_colon_after_double_slash)
    {
        return res;
    }

    size_t ip_length = fist_colon_after_double_slash - double_slash - 2;
    if(ip_length > result_length)
    {
        return res;
    }

    res = 0;
    strncpy(result, double_slash+2, ip_length);
    result[ip_length] = '\0';
    return res;
}

static int upnpdevices_callback(void *data, int argc, char **argv, char **azColName)
{
    if(!data)
    {
        return 0;
    }

    if(argc != 1)
    {
        return 0;
    }

    char *location = argv[0];

    if(!location)
    {
        return 0;
    }

    char ip_address[SIZE_32B];
    if(!get_ip_address(location, ip_address, sizeof(ip_address)))
    {
        printf("ip_address %s\n", ip_address);
        int res = check_ip_in_range(ip_address, g_hub_ip, g_subnet_mask);;
        if(!res)
        {
            SLOGI("location %s is not in range %s/%s", location, g_hub_ip, g_subnet_mask);
            return 0;
        }
        else
        {
            printf("ip_address %s\n", ip_address);
            int res = ping_command(ip_address, 1);
            if(res)
            {
                return 0;
            }
        }
    }
    json_object_array_add(data, json_object_new_string(location));

    return 0;
}

void create_device_from_database()
{
    int i;
    json_object *url = json_object_new_array();
    searching_database(_VR_(upnp), upnpdevices_callback, url,
                        "SELECT url from CONTROL_DEVS where type='%s';", ST_UPNP);

    int arraylen = json_object_array_length(url);
    for(i =0; i< arraylen; i++)
    {
        json_object *jvalue = json_object_array_get_idx(url, i);
        const char *location = json_object_get_string(jvalue);
        IXML_Document *DescDoc = NULL;
        int ret;
        if(( ret = UpnpDownloadXmlDoc( location, &DescDoc ) ) != UPNP_E_SUCCESS)
        {
            SLOGE("UpnpDownloadXmlDoc fail");
        }
        else
        {
            CtrlPointProcessDeviceDiscoveryEx(DescDoc, location, DEFAULT_EXPIRES, 1);
        }

        if( DescDoc )
        {
            ixmlDocument_free(DescDoc);
        }
    }
    json_object_put(url);
}

int CtrlPointDiscoverDevices(char *PDT, void *Cookie)
{
    LOG(LOG_DBG, "start discover with MAX_DISCOVER_TIMEOUT = %d\n", MAX_DISCOVER_TIMEOUT);
    char PluginDeviceType[64];
    if(PDT==NULL)
    {
        strcpy(PluginDeviceType, "urn:Belkin:device:*");
    }
    else
    {
        strcpy(PluginDeviceType, PDT);
    }
    
    int rect = -1;
    if (-1 == ctrlpt_handle)
    {
        printf("fail to discover\n");
        return 0x01;
    }

    rect = UpnpSearchAsync(ctrlpt_handle, MAX_DISCOVER_TIMEOUT, PluginDeviceType, Cookie);
    if( UPNP_E_SUCCESS != rect )
    {
        return UPNP_E_INVALID_DEVICE;
    }

    //sleep(MAX_DISCOVER_TIMEOUT);
    return UPNP_E_SUCCESS;
}

int CtrlPointCallbackEventHandler(Upnp_EventType EventType,
                                    void *Event,
                                    void *Cookie )
{
    int isAdv = 0x00;
    upnp_cmd_data *upnp_data = (upnp_cmd_data*)Cookie;

    switch ( EventType )
    {
        case UPNP_DISCOVERY_ADVERTISEMENT_ALIVE:
        {
            struct Upnp_Discovery *d_event = ( struct Upnp_Discovery * )Event;
            if(strstr(d_event->ServiceType, "basicevent"))
            {
                int rnd = (30 + (rand() % 30));
                isAdv=0x01;
                sleep(rnd);
            }
        }

        case UPNP_DISCOVERY_SEARCH_RESULT:
        {   
            struct Upnp_Discovery *d_event = ( struct Upnp_Discovery * )Event;
            IXML_Document *DescDoc = NULL;
            int ret;

            if( d_event->ErrCode != UPNP_E_SUCCESS )
            {
                SLOGE("Error in Discovery Callback -- %d", d_event->ErrCode);
            }

            if( ( ret = UpnpDownloadXmlDoc( d_event->Location, &DescDoc ) ) != UPNP_E_SUCCESS)
            {
                SLOGE("UpnpDownloadXmlDoc fail");
            }
            else
            {
                CtrlPointProcessDeviceDiscoveryEx(DescDoc, d_event->Location, d_event->Expires, isAdv);
            }

            if( DescDoc )
            {
                ixmlDocument_free(DescDoc);
            }  
            break;
        }

        case UPNP_DISCOVERY_SEARCH_TIMEOUT:
            /*
            Nothing to do here...
            */
        {
            char *state = (char *) read_option("auto_config.@auto-config[0].state");
            if(state && !strcmp(state, "1"))
            {
                free(state);
                if(g_wemo_device_found_device)
                {
                    system("uci set auto_config.@auto-config[0].state=2");
                    system("uci commit auto_config");
                    system("mv /etc/config/ret_wireless /etc/config/wireless");
                    system("wifi");
                    g_wemo_device_found_device=0;
                }
            }
            SLOGI("search timeout");
            break;
        }
            

        case UPNP_DISCOVERY_ADVERTISEMENT_BYEBYE:
        {

            SLOGI("UPNP_DISCOVERY_ADVERTISEMENT_BYEBYE");

            struct Upnp_Discovery *d_event = ( struct Upnp_Discovery * )Event;
            IXML_Document *DescDoc = NULL;
            int ret;

            if( d_event->ErrCode != UPNP_E_SUCCESS )
            {
                return d_event->ErrCode;
            }

            if( ( ret = UpnpDownloadXmlDoc( d_event->Location, &DescDoc ) ) != UPNP_E_SUCCESS)
            {

                return d_event->ErrCode;                    
            }
            else
            {
                CtrlPointProcessDeviceByebye(DescDoc);
            }

            if( DescDoc )
            {
                ixmlDocument_free(DescDoc);
            }
            break;
        }

        case UPNP_CONTROL_ACTION_COMPLETE:
        {
            struct Upnp_Action_Complete *a_event =
            ( struct Upnp_Action_Complete * )Event;

            if (0x00 == a_event)
            {
                break;
            }
            printf("UPNP_CONTROL_ACTION_COMPLETE, failure a_event->ErrCode = %d\n", a_event->ErrCode);
            if( a_event->ErrCode != UPNP_E_SUCCESS )
            {
                printf("UPNP_CONTROL_ACTION_COMPLETE, failure a_event->ErrCode = %d\n", a_event->ErrCode);
                
                char *state = (char *) read_option("auto_config.@auto-config[0].state");
                if(state && !strcmp(state, "1"))
                {
                    database_actions(_VR_(upnp), "UPDATE WEMO_WIFI_CONFIG set config='FAILURE' where config='configuring'");
                    system("uci set auto_config.@auto-config[0].state=2");
                    system("uci commit auto_config");
                    system("mv /etc/config/ret_wireless /etc/config/wireless");
                    system("wifi");
                }

                if(upnp_data)
                {
                    if(!state || strcmp(state, "1"))
                    {
                        SEARCH_DATA_INIT_VAR(tmp_data);
                        searching_database(_VR_CB_(upnp), &tmp_data,
                                        "SELECT friendlyName from CONTROL_DEVS where udn='%s' AND added='yes'",
                                         upnp_data->deviceID);

                        if(tmp_data.len)
                        {
                            json_object *jobj = json_object_new_object();
                            json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
                            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_ACTION_COMPLETE));
                            json_object_object_add(jobj, ST_FRIENDLY_NAME, json_object_new_string(tmp_data.value));
                            json_object_object_add(jobj, ST_UDN, json_object_new_string(upnp_data->deviceID));
                            //json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string("BinaryState"));
                            json_object_object_add(jobj, ST_STATUS, json_object_new_string("failed"));

                            if(a_event->ErrCode == -207)
                            {
                                json_object_object_add(jobj, "reason", json_object_new_string("ERROR: Action Fail, TimeOut"));
                            }
                            else if(a_event->ErrCode == 600)
                            {
                                json_object_object_add(jobj, "reason", json_object_new_string("ERROR: Action Fail, Invalid Argument"));
                            }
                            else
                            {
                                json_object_object_add(jobj, "reason", json_object_new_string("ERROR: Action Fail, device is disconnected"));
                            }
                            
                            ithread_mutex_lock(&notify_lock);
                            strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
                            g_notify.notify_index=g_notify.notify_index+1;
                            ithread_mutex_unlock(&notify_lock);

                            json_object_put(jobj);
                        }
                        FREE_SEARCH_DATA_VAR(tmp_data);
                    }
                    SAFE_FREE(upnp_data->deviceID);
                    SAFE_FREE(upnp_data->subdevID);
                    SAFE_FREE(upnp_data->result);
                    SAFE_FREE(upnp_data);
                }

                SAFE_FREE(state);                
            }
            else
            {
                SLOGI("UPNP_CONTROL_ACTION_COMPLETE, success");
                // printf("UPNP_CONTROL_ACTION_COMPLETE, success\n");
                // printf("cookie = %s\n", (char *)Cookie);
                ProcessReceiveActionComplete(a_event, Cookie);
            }
            break;
        }

        case UPNP_CONTROL_GET_VAR_COMPLETE:
        {
            break;
        }

        case UPNP_EVENT_RECEIVED:
        {
            struct Upnp_Event *e_event = ( struct Upnp_Event * )Event;
            CtrlPointProcessNotifyAction(e_event);
            break;
        }

        case UPNP_EVENT_SUBSCRIBE_COMPLETE:
        {
            break;
        }  


        case UPNP_EVENT_UNSUBSCRIBE_COMPLETE:
        case UPNP_EVENT_RENEWAL_COMPLETE:
        case UPNP_EVENT_AUTORENEWAL_FAILED:
        case UPNP_EVENT_SUBSCRIPTION_EXPIRED:
        case UPNP_EVENT_SUBSCRIPTION_REQUEST:
        case UPNP_CONTROL_GET_VAR_REQUEST:
        case UPNP_CONTROL_ACTION_REQUEST:
            break;
    }
    return 0;
}

void CtrlPointProcessDeviceDiscoveryEx(IXML_Document *DescDoc, const char *location, int expires, int isAdv)
{
    int ret;
    int found = 0;
    int unlocked = 0;

    char *UDN = NULL;
    char *friendlyName = NULL;
    char *baseURL = NULL;
    char *relURL = NULL;
    char *deviceType = NULL;
    char presURL[256];

    struct DeviceNode *deviceNode;
    struct DeviceNode *tmpdevnode;

    UDN                 = Util_GetFirstDocumentItem(DescDoc, "UDN");
    friendlyName        = Util_GetFirstDocumentItem(DescDoc, "friendlyName");
    baseURL             = Util_GetFirstDocumentItem(DescDoc, "URLBase");
    relURL              = Util_GetFirstDocumentItem(DescDoc, "presentationURL");
    deviceType          = Util_GetFirstDocumentItem(DescDoc, "deviceType");

    LOG(LOG_DBG, "location = %s\n", location);
    LOG(LOG_DBG, "UND = %s\n", UDN );
    LOG(LOG_DBG, "friendlyName = %s\n", friendlyName);
    LOG(LOG_DBG, "baseURL = %s\n", baseURL);
    LOG(LOG_DBG, "relURL = %s\n", relURL);
    LOG(LOG_DBG, "deviceType = %s\n", deviceType);

    if(!deviceType || !UDN || !friendlyName || !relURL)
    {
        return;
    }

    if(!strstr(deviceType, "urn:Belkin:device") || strstr(UDN, "zigbee") || strstr(UDN, "zwave"))
    {
        LOG(LOG_DBG, "not BelKin device");
        FreeXmlSource(UDN);
        FreeXmlSource(friendlyName);
        FreeXmlSource(baseURL);
        FreeXmlSource(relURL);
        FreeXmlSource(deviceType);
        return;
    }

    if(!strstr(deviceType, "bridge") && 
       !strstr(deviceType, "lightswitch") &&
       !strstr(deviceType, "sensor") &&
       !strstr(deviceType, "controllee") &&
       !strstr(deviceType, "dimmer") &&
       !strstr(deviceType, "insight"))
    {
        printf("not support device %s\n", deviceType);
        FreeXmlSource(UDN);
        FreeXmlSource(friendlyName);
        FreeXmlSource(baseURL);
        FreeXmlSource(relURL);
        FreeXmlSource(deviceType);
        return;
    }

    ret = UpnpResolveURL((baseURL ? baseURL : location), relURL, presURL);

    if (UPNP_E_SUCCESS != ret)
    {
        LOG(LOG_DBG, "Error generating presURL from %s + %s",baseURL, relURL);
    }

    ithread_mutex_lock(&DeviceListMutex);
    CtrlPluginDeviceNode *tmp_GlobalDeviceList = GlobalDeviceList;
    while(tmp_GlobalDeviceList != NULL)
    {
        LOG(LOG_DBG, "tmp_GlobalDeviceList->device.UDN= %s\n", tmp_GlobalDeviceList->device.UDN);
        tmp_GlobalDeviceList = tmp_GlobalDeviceList->next;
    }

    tmpdevnode = GlobalDeviceList;

    while (tmpdevnode)
    {
        if (strcmp(tmpdevnode->device.UDN, UDN) == 0)
        {
            found = 1;
            break;
        }

        if (!tmpdevnode->next) 
        {
            break;//- The latest one
        }
        tmpdevnode = tmpdevnode->next;
    }

    if(found)
    {
        //printf("found %s\n", friendlyName);
        tmpdevnode->device.AdvrTimeOut = expires;

        if (isAdv)
        {
            CtrlPointDeleteNode(tmpdevnode);
            tmpdevnode->IsDeviceRequestUpdate = 0x01;
        }

        if (tmpdevnode->IsDeviceRequestUpdate)
        {
            subscribeDeviceServices(DescDoc, location, tmpdevnode);
                    //- Device Management
            strncpy(tmpdevnode->device.DescDocURL, location, SIZE_256B - 1);
            strncpy(tmpdevnode->device.FriendlyName, friendlyName, SIZE_256B - 1);
            strncpy(tmpdevnode->device.PresURL, presURL, SIZE_256B - 1);
            tmpdevnode->device.AdvrTimeOut = expires;
            tmpdevnode->IsDeviceRequestUpdate = 0x00;
            ///// create id name for device /////

            printf("%s | %s | %s\n", friendlyName, UDN, location);

            SEARCH_DATA_INIT_VAR(tmp_result);
            searching_database(_VR_CB_(upnp), &tmp_result, "SELECT udn from CONTROL_DEVS where udn='%s'", UDN);
            if(!tmp_result.len)
            {
                database_actions(_VR_(upnp), "INSERT INTO %s VALUES('%s','%s','%s','%s','%s','%s')", 
                    "CONTROL_DEVS (realName,friendlyName,udn,url,type,added)", 
                    deviceType, friendlyName, UDN, location, ST_UPNP, "yes");

                char id_resource[SIZE_256B];
                uuid_make(id_resource, sizeof(id_resource));

                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
                json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_NEW_DEVICE));
                json_object_object_add(jobj, ST_REAL_NAME, json_object_new_string(deviceType));
                json_object_object_add(jobj, ST_FRIENDLY_NAME, json_object_new_string(friendlyName));
                json_object_object_add(jobj, ST_UDN, json_object_new_string(UDN));
                json_object_object_add(jobj, ST_LOCATION, json_object_new_string(location));

                json_object *json_post_resources = json_object_new_object();
                json_object *resources = json_object_new_object();
                json_object_object_add(resources, ST_REAL_NAME, json_object_new_string(deviceType));
                json_object_object_add(resources, ST_FRIENDLY_NAME, json_object_new_string(friendlyName));
                json_object_object_add(resources, ST_UDN, json_object_new_string(UDN));
                json_object_object_add(resources, ST_URL, json_object_new_string(location));
                json_object_object_add(resources, ST_TYPE, json_object_new_string(ST_UPNP));

                json_object_object_add(json_post_resources, ST_CLOUD_RESOURCE, resources);
                json_object_object_add(json_post_resources, ST_ID, json_object_new_string(id_resource));
                json_object_object_add(json_post_resources, ST_CLOUD_LOCAL_ID, json_object_new_string(UDN));
                json_object_object_add(json_post_resources, ST_CLOUD_SERIAL_ID, json_object_new_string(deviceType));
                json_object_object_add(json_post_resources, ST_NAME, json_object_new_string(friendlyName));

                const char *data = json_object_to_json_string(json_post_resources);
                int res = VR_(post_resources)((char *)data, id_resource,
                                        sizeof(id_resource)-1, RESOURCES_TIMEOUT);
                if(res > 0)
                {
                    inform_pubsub_post_failed(UDN, ST_UPNP, (char *)data);
                }

                update_resource_id(_VR_CB_(upnp), UDN, id_resource);
                shm_update_data(g_shm, id_resource, UDN, ST_UPNP, SHM_ADD);

                json_object_put(json_post_resources);

                json_object_object_add(jobj, ST_CLOUD_RESOURCE, json_object_new_string(strlen(id_resource)?id_resource:""));
                ithread_mutex_lock(&notify_lock);
                strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
                g_notify.notify_index=g_notify.notify_index+1;
                ithread_mutex_unlock(&notify_lock);

                json_object_put(jobj);
            }
            else
            {
                database_actions(_VR_(upnp), "UPDATE CONTROL_DEVS SET friendlyName='%s', url='%s', added='yes' "
                                            "WHERE udn='%s';", 
                                            friendlyName, location, UDN);

                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
                json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_UPDATE));
                json_object_object_add(jobj, ST_REAL_NAME, json_object_new_string(deviceType));
                json_object_object_add(jobj, ST_FRIENDLY_NAME, json_object_new_string(friendlyName));
                json_object_object_add(jobj, ST_UDN, json_object_new_string(UDN));
                json_object_object_add(jobj, ST_LOCATION, json_object_new_string(location));

                ithread_mutex_lock(&notify_lock);
                strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
                g_notify.notify_index=g_notify.notify_index+1;
                ithread_mutex_unlock(&notify_lock);

                json_object_put(jobj);
            }
            FREE_SEARCH_DATA_VAR(tmp_result);
        }
        // else
        // {
        //     json_object *jobj = json_object_new_object();
        //     json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
        //     json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
        //     json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string("update"));
        //     json_object_object_add(jobj, "realname", json_object_new_string(deviceType));
        //     json_object_object_add(jobj, "friendlyName", json_object_new_string(friendlyName));
        //     json_object_object_add(jobj, "UDN", json_object_new_string(UDN));
        //     json_object_object_add(jobj, "location", json_object_new_string(location));

        //     ithread_mutex_lock(&notify_lock);
        //     strcpy(g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
        //     g_notify.notify_index=g_notify.notify_index+1;
        //     ithread_mutex_unlock(&notify_lock);
        // }
    } 
    else 
    {
        deviceNode = (CtrlPluginDeviceNode *)malloc(sizeof(CtrlPluginDeviceNode));
        memset(deviceNode, 0, sizeof(CtrlPluginDeviceNode));

        if (0x00 == deviceNode)
        {
            LOG(LOG_DBG, "Error: Can not allocate device memory");
            goto quit;
        }

        deviceNode->IsDeviceRequestUpdate = 0x00;
        strncpy(deviceNode->device.UDN, UDN, sizeof(deviceNode->device.UDN)-1);

        subscribeDeviceServices(DescDoc, location, deviceNode);

        strncpy(deviceNode->device.DescDocURL, location, SIZE_256B - 1);
        strncpy(deviceNode->device.FriendlyName, friendlyName, SIZE_256B - 1);
        strncpy(deviceNode->device.PresURL, presURL, SIZE_256B - 1);
        deviceNode->device.AdvrTimeOut = expires;

        deviceNode->next = NULL;

        if (!tmpdevnode)
        {
            GlobalDeviceList = deviceNode;
        }
        else
        {
            tmpdevnode->next = deviceNode;
        }

        printf("\n/////////// FOUND DEVICE ////////////\n");
        printf("%s | %s | %s\n\n", friendlyName, UDN, location);

        SEARCH_DATA_INIT_VAR(tmp_result);
        searching_database(_VR_CB_(upnp), &tmp_result, "SELECT udn from CONTROL_DEVS where udn='%s'", UDN);
        if(!tmp_result.len)
        {
            database_actions(_VR_(upnp), "INSERT INTO %s VALUES('%s','%s','%s','%s','%s','%s')", 
                "CONTROL_DEVS (realName,friendlyName,udn,url,type,added)", 
                deviceType, friendlyName, UDN, location, ST_UPNP, "yes");

            char id_resource[SIZE_256B];
            uuid_make(id_resource, sizeof(id_resource));

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_NEW_DEVICE));
            json_object_object_add(jobj, ST_REAL_NAME, json_object_new_string(deviceType));
            json_object_object_add(jobj, ST_FRIENDLY_NAME, json_object_new_string(friendlyName));
            json_object_object_add(jobj, ST_UDN, json_object_new_string(UDN));
            json_object_object_add(jobj, ST_LOCATION, json_object_new_string(location));

            json_object *json_post_resources = json_object_new_object();
            json_object *resources = json_object_new_object();
            json_object_object_add(resources, ST_REAL_NAME, json_object_new_string(deviceType));
            json_object_object_add(resources, ST_FRIENDLY_NAME, json_object_new_string(friendlyName));
            json_object_object_add(resources, ST_UDN, json_object_new_string(UDN));
            json_object_object_add(resources, ST_URL, json_object_new_string(location));
            json_object_object_add(resources, ST_TYPE, json_object_new_string(ST_UPNP));

            json_object_object_add(json_post_resources, ST_CLOUD_RESOURCE, resources);
            json_object_object_add(json_post_resources, ST_ID, json_object_new_string(id_resource));
            json_object_object_add(json_post_resources, ST_CLOUD_LOCAL_ID, json_object_new_string(UDN));
            json_object_object_add(json_post_resources, ST_CLOUD_SERIAL_ID, json_object_new_string(deviceType));
            json_object_object_add(json_post_resources, ST_NAME, json_object_new_string(friendlyName));

            const char *data = json_object_to_json_string(json_post_resources);
            int res = VR_(post_resources)((char *)data, id_resource,
                                        sizeof(id_resource), RESOURCES_TIMEOUT);
            if (res > 0)
            {
                inform_pubsub_post_failed(UDN, ST_UPNP, (char *)data);
            }

            update_resource_id(_VR_CB_(upnp), UDN, id_resource);
            shm_update_data(g_shm, id_resource, UDN, ST_UPNP, SHM_ADD);

            json_object_put(json_post_resources);

            json_object_object_add(jobj, ST_CLOUD_RESOURCE, json_object_new_string(strlen(id_resource)?id_resource:""));
            ithread_mutex_lock(&notify_lock);
            strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
            g_notify.notify_index=g_notify.notify_index+1;
            ithread_mutex_unlock(&notify_lock);

            json_object_put(jobj);
        }
        else
        {
            database_actions(_VR_(upnp), "UPDATE CONTROL_DEVS SET friendlyName='%s', url='%s', added='yes' "
                                        "WHERE udn='%s';", 
                                        friendlyName, location, UDN);

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_UPDATE));
            json_object_object_add(jobj, ST_REAL_NAME, json_object_new_string(deviceType));
            json_object_object_add(jobj, ST_FRIENDLY_NAME, json_object_new_string(friendlyName));
            json_object_object_add(jobj, ST_UDN, json_object_new_string(UDN));
            json_object_object_add(jobj, ST_LOCATION, json_object_new_string(location));

            ithread_mutex_lock(&notify_lock);
            strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
            g_notify.notify_index=g_notify.notify_index+1;
            ithread_mutex_unlock(&notify_lock);

            json_object_put(jobj);
        }
        FREE_SEARCH_DATA_VAR(tmp_result);

        char *state = NULL;
        state = (char *)read_option("auto_config.@auto-config[0].state");
        if(state)
        {
            if(!strcmp(state, "1"))
            {

                g_wemo_device_found_device=0;

                SEARCH_DATA_INIT_VAR(ssid);
                searching_database(_VR_CB_(upnp), &ssid, "SELECT ssid from WEMO_WIFI_CONFIG where config='start_config'");
                if(ssid.len)
                {
                    char name[16], id[4];
                    copy_data(ssid.value, '.', 1, name);
                    copy_data(ssid.value, '.', 2, id);
                    if(strstr(friendlyName, name) && !strcmp((UDN+strlen(UDN)-3), id))
                    {
                        ithread_mutex_unlock(&DeviceListMutex);
                        unlocked = 1;

                        database_actions(_VR_(upnp), "UPDATE WEMO_WIFI_CONFIG set udn='%s' where ssid='%s'",
                                            UDN, ssid.value);

                        CtrlPointProcessCommand("getmac", UDN, NULL, NULL);
                    }
                }
                else
                {
                    int retry=20;
                    ssid.len=0;
                    searching_database(_VR_CB_(upnp), &ssid, "SELECT ssid from WEMO_WIFI_CONFIG where config='configuring'");
                    while(ssid.len && retry>0)
                    {
                        char name[16], id[4];
                        copy_data(ssid.value, '.', 1, name);
                        copy_data(ssid.value, '.', 2, id);
                        if(strstr(friendlyName, name) && !strcmp((UDN+strlen(UDN)-3), id))
                        {
                            ithread_mutex_unlock(&DeviceListMutex);
                            unlocked = 1;
                            GetNetworkStatus(UDN, NULL, NULL);
                            sleep(3);
                            ssid.len=0;
                            searching_database(_VR_CB_(upnp), &ssid, "SELECT ssid from WEMO_WIFI_CONFIG where config='configuring'");
                        }
                        retry--;
                    }
                    if(!retry)
                    {
                        database_actions(_VR_(upnp), "UPDATE WEMO_WIFI_CONFIG set config='start_config' where ssid='%s'",
                                            ssid.value);
                        CtrlPointProcessCommand("getmac", UDN, NULL, NULL);
                    }
                }
                FREE_SEARCH_DATA_VAR(ssid);
            }
            free(state);
        }
    }
quit:
    if(!unlocked)
        ithread_mutex_unlock(&DeviceListMutex);

    FreeXmlSource(UDN);
    FreeXmlSource(deviceType);
    FreeXmlSource(friendlyName);
    FreeXmlSource(baseURL);
    FreeXmlSource(relURL);
    return;
}

void subscribeDeviceServices(IXML_Document *DescDoc, const char *location, CtrlPluginDeviceNode *deviceNode)
{
    int   ret = 1;
    int service;
    char *serviceId[SERVICE_SERVCOUNT]      = {NULL, NULL, NULL, NULL, NULL, NULL, NULL};
    char *eventURL[SERVICE_SERVCOUNT]        = {NULL, NULL, NULL, NULL, NULL, NULL, NULL};
    char *controlURL[SERVICE_SERVCOUNT]      = {NULL, NULL, NULL, NULL, NULL, NULL, NULL};
    Upnp_SID eventSID[SERVICE_SERVCOUNT];
    int TimeOut[SERVICE_SERVCOUNT]         = {default_timeout, default_timeout, default_timeout, default_timeout, default_timeout,  default_timeout, default_timeout};

    for (service = 0; service < SERVICE_SERVCOUNT; service++)
    {
        if (0x00 != strstr(ServiceList,  PluginDeviceServiceType[service]))
        {
            if (Util_FindAndParseService(DescDoc, location, PluginDeviceServiceType[service], &serviceId[service], &eventURL[service], &controlURL[service]))
            {
            LOG(LOG_DBG, "PluginDeviceServiceType[%d] = %s\n",service,PluginDeviceServiceType[service]);
            LOG(LOG_DBG, "ServiceID[%d] = %s\n",service,serviceId[service]);
            LOG(LOG_DBG, "eventURL[%d] = %s\n",service,eventURL[service]);
            LOG(LOG_DBG, "controlURL[%d] = %s\n",service,controlURL[service]);
            ret = UpnpSubscribe(ctrlpt_handle, eventURL[service], &TimeOut[service], eventSID[service]);
                if (ret == UPNP_E_SUCCESS)
                {
                if (NULL != serviceId[service]){
                    strncpy(deviceNode->device.services[service].ServiceId, serviceId[service], SIZE_256B - 1);
                }
                if (NULL != PluginDeviceServiceType[service]){
                    strncpy(deviceNode->device.services[service].ServiceType, PluginDeviceServiceType[service], SIZE_256B - 1);
                }
                if (NULL != controlURL[service]){
                    strncpy(deviceNode->device.services[service].ControlURL, controlURL[service], SIZE_256B - 1);
                }
                if (NULL != eventURL[service]){
                    strncpy(deviceNode->device.services[service].EventURL, eventURL[service], SIZE_256B - 1);
                }
                if (NULL != eventSID[service]){
                    strncpy(deviceNode->device.services[service].SID, eventSID[service], SIZE_256B - 1);
                }  
                }
                else
                {
                    LOG(LOG_DBG, "####### Error Subscribing to EventURL -- %s\n", PluginDeviceServiceType[service]);
                }
            }   
        }
    }

    for (service = 0; service < SERVICE_SERVCOUNT; service++)
    {
        FreeXmlSource(serviceId[service]);
        FreeXmlSource(controlURL[service]);
        FreeXmlSource(eventURL[service]);
    }
}

int PluginCtrlPointSendAction(int service, char *UDN, const char *actionname, const char **param_name, char **param_val, int param_count, void * cookie)
{
    LOG(LOG_DBG, "in Plugin Ctrl Point Send Action");

    CtrlPluginDeviceNode *devnode = NULL;
    CtrlPluginDeviceNode *tmp = GlobalDeviceList;
    IXML_Document *actionNode = NULL;

    int rect = -1;
    int param;

    upnp_cmd_data *upnp_data = (upnp_cmd_data *)cookie;

    char szTmpEventURL[SIZE_256B];
    memset(szTmpEventURL, 0x00, sizeof(szTmpEventURL));
    
    //-Lock and unlock quickly so that no delay 
    LockDeviceSync();

    SEARCH_DATA_INIT_VAR(tmp_result);
    searching_database(_VR_CB_(upnp), &tmp_result, "SELECT udn from CONTROL_DEVS where udn='%s'", UDN);
    if(tmp_result.len)
    {
        while(tmp)
        {
            if(!strcmp(tmp->device.UDN, UDN))
            {
                devnode=tmp;
                rect = UPNP_E_SUCCESS;
                break;
            }
            tmp = tmp->next;
        }
    }
    else
    {
        if(upnp_data) 
            strcpy(upnp_data->result, "not found device database");
        UnlockDeviceSync();
        FREE_SEARCH_DATA_VAR(tmp_result);
        return rect;
    }
    FREE_SEARCH_DATA_VAR(tmp_result);

    if (UPNP_E_SUCCESS == rect)
    { 
        if (0x00 != devnode)
        {
            strncpy(szTmpEventURL, devnode->device.services[service].ControlURL, sizeof(szTmpEventURL)-1);
        }
    }
    else 
    {
        if(upnp_data)
            strcpy(upnp_data->result, "not found device in global list");
        UnlockDeviceSync();
        return rect;
    };

    UnlockDeviceSync();
    printf("szTmpEventURL = %s\n", szTmpEventURL);
    if (0x00 != strlen(szTmpEventURL))
    {
        LOG(LOG_DBG, "start make action");
        if (0 == param_count)
        {  
            actionNode = UpnpMakeAction(actionname, PluginDeviceServiceType[service], 0, NULL);
        }
        else
        {
            for (param = 0; param < param_count; param++)
            {
                rect = UpnpAddToAction(&actionNode, actionname, PluginDeviceServiceType[service], param_name[param], param_val[param]);
                if (0x00 != rect)
                {
                    LOG(LOG_DBG, "UpnpAddToAction: can not add action to list");
                }
            }

        }
        LOG(LOG_DBG, "start send action");
        if(PluginDeviceServiceType[service] != NULL)
        {
            rect = UpnpSendActionAsync(ctrlpt_handle, szTmpEventURL, PluginDeviceServiceType[service], NULL, actionNode, CtrlPointCallbackEventHandler, cookie);
            if (rect != UPNP_E_SUCCESS)
            { 
                printf("Error in UpnpSendAction -- %d\n", rect);
            }
        }

        if (actionNode)
        {
            ixmlDocument_free(actionNode);
        }
    }
    else
    {
        if(upnp_data) 
            strcpy(upnp_data->result, "device is not support this command");
        rect = -1;
    }

    LOG(LOG_DBG, "end Action");
    return rect;
}

void LockDeviceSync()
{
    ithread_mutex_lock(&DeviceListMutex);
}

void UnlockDeviceSync()
{
    ithread_mutex_unlock(&DeviceListMutex);
}


struct cmdloop_commands cmdloop_cmdlist[] = {
 {"rediscover",    REDISCOVER,       NULL},  
 {"getbinary",     GETBINARYSTATUS,  &GetBinaryState},
 {"setbinary",     SETBINARYSTATUS,  &SetBinaryState},
 {"geticonurl",    GETICONURL,       &GetIconURL},
 {"getnumberdev",  GETNUMBERDEV,     NULL},
 {"getname",       GETFRIENDNAME,    &GetFriendlyName},
 {"getdevstatus",  GETDEVICESTATUS,  &GetDeviceStatus},
 {"opennetwork",   OPENNETWORK,      &opennetwork},
 {"closenetwork",  CLOSENETWORK,     &closenetwork},
 {"discoversubdev",GETENDDEVS,       &GetEndDevices},
 {"addsubdev",     ADDSUBDEV,        &AddSubDevice},
 {"removesubdev",  REMOVESUBDEV,     &RemoveSubDevice},
 {"getnetlist",    GETNETWORKLIST,   &GetNetworkList},
 {"setdevstatus",  SETDEVSTATUS,     &SetDeviceStatus},
 {"connect",       CONNECTNET,       &Connect_network},
 {"getmac",        GETMACADDR,       &GetMacAddr}
};

int CtrlPointProcessCommand(char *cmd, char *UDN, char *param_val[], void *cookie)
{
    int i;
    int cmdnum = -1;
    int cmdfound = 0;
    int ret = -1;

    upnp_cmd_data *upnp_data = (upnp_cmd_data*)cookie;

    int numcmds = sizeof(cmdloop_cmdlist)/sizeof(cmdloop_commands);
    for (i = 0; i < numcmds; ++i) 
    {
        if (strcasecmp(cmd, cmdloop_cmdlist[i].str ) == 0)
        {
            cmdnum = cmdloop_cmdlist[i].cmdnum;
            cmdfound++;
            break;
        }
    }

    LOG(LOG_DBG, "cmdnum = %d\n", cmdnum);

    switch (cmdnum)
    {
        case REDISCOVER:
        {
            ret = CtrlPointDiscoverDevices(NULL, NULL);
            break;
        }

        case GETBINARYSTATUS:
        {
            ret = GetBinaryState(UDN, param_val, cookie);
            break;
        }

        case GETNETWORKLIST:
        {
            ret = GetNetworkList(UDN, param_val, cookie);
            break;
        }

        case SETBINARYSTATUS:
        {
            if(upnp_data)
            {
                if(!param_val[0])
                {
                    printfresult("need value for setbinary command\n");
                    strcpy(upnp_data->result, "need value for setbinary command");
                    break;
                }

                ret = SetBinaryState(UDN, param_val, cookie);
            }
            break;
        }

        case GETICONURL:
        {
            GetIconURL(UDN, param_val, cookie);
            break;
        }

        case GETNUMBERDEV:
        {     
            break;
        }

        case GETFRIENDNAME:
        {
            ret = GetFriendlyName(UDN, param_val, cookie);
            break;
        }

        case GETDEVICESTATUS:
        {
            ret = GetDeviceStatus(UDN, param_val, cookie);      
            break;
        }

        case OPENNETWORK:
        {
           
            ret = opennetwork(UDN, param_val, cookie);

            break;
        }

        case CLOSENETWORK:
        {
            
            ret = closenetwork(UDN, param_val, cookie);

            break;
        }

        case SETDEVSTATUS:
        {
            ret = SetDeviceStatus(UDN, param_val, cookie);
            break;
        }

        case GETENDDEVS:
        {
            LOG(LOG_DBG, "param_val[0]=%s\n", param_val[0]);
            LOG(LOG_DBG, "param_val[1]=%s\n", param_val[1]);

            ret = GetEndDevices(UDN, param_val, cookie);

            break;
        }

        case ADDSUBDEV:
        {
            ret = AddSubDevice(UDN, param_val, cookie);
            break;
        }

        case REMOVESUBDEV:
        {
            ret = RemoveSubDevice(UDN, param_val, cookie);
            break;
        }

        case GETMACADDR:
        {
            ret = GetMacAddr(UDN, param_val, cookie);
            break;
        }

        case CONNECTNET:
        {
            // char pass[] = "veriksystems2015";
            // char param[PASSWORD_MAX_LEN+1];
            // memset(param, 0x00, sizeof(param));
            // create_decrypt_pass(pass, param);
            // printf("encrypt pass = %s\n", param);
            char ssid[16], auth[16], password[48], encryption[16], channel[8];
            char *tmp;
            if((tmp = (char*)read_option("auto_config.@auto-config[0].2_4GHz_ssid")))
            {
                strcpy(ssid, tmp);
                free(tmp);
            }

            if((tmp = (char*)read_option("auto_config.@auto-config[0].2_4GHz_auth")))
            {
                strcpy(auth, tmp);
                free(tmp);
            }

            if((tmp = (char*)read_option("auto_config.@auto-config[0].2_4GHz_encryption")))
            {
                strcpy(encryption, tmp);
                free(tmp);
            }

            if((tmp = (char*)read_option("auto_config.@auto-config[0].2_4GHz_channel")))
            {
                strcpy(channel, tmp);
                free(tmp);
            }

            if((tmp = (char*)read_option("ret_wireless.@wifi-iface[0].key")))
            {
                strcpy(password, tmp);
                free(tmp);
            }
            
            if(!strcmp(auth, encryption))
            {
                memset(password, 0x00, sizeof(password));
            }

            char cipher_password[PASSWORD_MAX_LEN+1];
            memset(cipher_password, 0x00, sizeof(cipher_password));
            create_decrypt_pass(password, cipher_password);
            
            printf("ssid = %s\n", ssid);
            printf("auth = %s\n", auth);
            printf("password = %s\n", password);
            printf("cipher_password = %s\n", cipher_password);
            printf("encryption = %s\n", encryption);
            printf("channel = %s\n", channel);

            const char *param_val1[5];
            param_val1[0] = &ssid[0];
            param_val1[1] = &auth[0];
            param_val1[2] = &cipher_password[0];
            param_val1[3] = &encryption[0];
            param_val1[4] = &channel[0];

            ret = Connect_network(UDN, (char**)param_val1, cookie);

            break;
        }

        default:
        {
            printf("ERROR: Wrong Command\n");
            break;
        }
    }
    return ret;
}


void ProcessReceiveActionComplete(struct Upnp_Action_Complete *a_event, void * cookie)
{  
    IXML_Document *DOCS = a_event->ActionResult;

    upnp_cmd_data *upnp_data = (upnp_cmd_data*) cookie;
    IXML_Document *ActionDoc;
    IXML_Node *DevList = NULL;
    IXML_Node *DevInfo = NULL;
    IXML_Node *tmp = NULL;

    char data[1024*2];
    memset(data, 0, sizeof(data));

    int re_use_data = 0;

    if(DOCS != NULL) 
    {
        LOG(LOG_DBG, "%s\n", DOCS->n.firstChild->nodeName);

        if(!strcmp(DOCS->n.firstChild->nodeName, "u:OpenNetworkResponse"))
        {
            if(upnp_data)
            {
                SEARCH_DATA_INIT_VAR(tmp_data);
                searching_database(_VR_CB_(upnp), &tmp_data, 
                                    "SELECT friendlyName from CONTROL_DEVS where udn='%s' AND added='yes'",
                                    upnp_data->deviceID);

                if(tmp_data.len)
                {
                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_ACTION_COMPLETE));
                    json_object_object_add(jobj, ST_FRIENDLY_NAME, json_object_new_string(tmp_data.value));
                    json_object_object_add(jobj, ST_UDN, json_object_new_string(upnp_data->deviceID));
                    json_object_object_add(jobj, ST_ACTION_TYPE, json_object_new_string(ST_OPEN_NETWORK));
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

                    ithread_mutex_lock(&notify_lock);
                    strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
                    g_notify.notify_index=g_notify.notify_index+1;
                    ithread_mutex_unlock(&notify_lock);

                    json_object_put(jobj);
                }
                FREE_SEARCH_DATA_VAR(tmp_data);
            }
        }
        else if(!strcmp(DOCS->n.firstChild->nodeName, "u:CloseNetworkResponse"))
        {
            if(upnp_data)
            {
                SEARCH_DATA_INIT_VAR(tmp_data);
                searching_database(_VR_CB_(upnp), &tmp_data,
                                "SELECT FriendlyName from CONTROL_DEVS where UDN='%s' AND Added='YES'", 
                                upnp_data->deviceID);

                if(tmp_data.len)
                {
                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_ACTION_COMPLETE));
                    json_object_object_add(jobj, ST_FRIENDLY_NAME, json_object_new_string(tmp_data.value));
                    json_object_object_add(jobj, ST_UDN, json_object_new_string(upnp_data->deviceID));
                    json_object_object_add(jobj, ST_ACTION_TYPE, json_object_new_string(ST_CLOSE_NETWORK));
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

                    ithread_mutex_lock(&notify_lock);
                    strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
                    g_notify.notify_index=g_notify.notify_index+1;
                    ithread_mutex_unlock(&notify_lock);

                    json_object_put(jobj);
                }
                FREE_SEARCH_DATA_VAR(tmp_data);
            }
        }
        else if(!strcmp(DOCS->n.firstChild->nodeName, "u:GetBinaryStateResponse") 
                || !strcmp(DOCS->n.firstChild->nodeName, "u:SetBinaryStateResponse"))
        {
            const char *actionType = ST_GET_BINARY;
            const char *brightnessName = "brightness";

            if(!strcmp(DOCS->n.firstChild->nodeName, "u:SetBinaryStateResponse"))
            {
                actionType = ST_SET_BINARY;
                brightnessName = "Brightness";
            }

            char *BinaryState = Util_GetFirstDocumentItem(DOCS, "BinaryState");
            char *brightness = Util_GetFirstDocumentItem(DOCS, brightnessName);//maybe not exist
            // printf("BinaryState = %s\n", BinaryState);  
            if(!BinaryState && !brightness)
            {
                goto done;
            }

            if(!upnp_data)
            {
                SAFE_FREE_XML(BinaryState);
                SAFE_FREE_XML(brightness);
                goto done;
            }

            SEARCH_DATA_INIT_VAR(tmp_data);
            searching_database(_VR_CB_(upnp), &tmp_data,
                                "SELECT friendlyName from CONTROL_DEVS where udn='%s' AND added='yes'",
                                upnp_data->deviceID);

            if(tmp_data.len)
            {
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_ACTION_COMPLETE));
                //json_object_object_add(jobj, "friendlyname", json_object_new_string(tmp_data));
                json_object_object_add(jobj, ST_UDN, json_object_new_string(upnp_data->deviceID));
                json_object_object_add(jobj, ST_ACTION_TYPE, json_object_new_string(actionType));

                if(!BinaryState)
                {
                     BinaryState = strdup(ST_ON_VALUE);
                }

                if(strchr(BinaryState, ','))
                {
                    char *save_tok;
                    char *tok = strtok_r(BinaryState, ",", &save_tok);
                    if(tok != NULL)
                    {
                        set_register_database(_VR_CB_(upnp), upnp_data->deviceID, ST_ON_OFF, (const char*)tok, ST_REPLACE, 0);
                        tok = strtok_r(NULL, ",", &save_tok);
                        set_register_database(_VR_CB_(upnp), upnp_data->deviceID, ST_DIM, (const char*)tok, ST_REPLACE, 0);
                    }
                    json_object_object_add(jobj, ST_VALUE, json_object_new_string(BinaryState));
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                }
                else
                {
                    char *tok = strtok(BinaryState, "|");
                    if(tok)
                    {
                        if(!strcmp(tok, "Error"))
                        {
                            re_use_data=1;
                            CtrlPointProcessCommand("getbinary", (char*)upnp_data->deviceID, NULL, (void*)upnp_data);
                            // json_object_object_add(jobj, ST_VALUE, json_object_new_string("error"));
                        }
                        else
                        {
                            int tmp_value = strtol(tok, NULL, 10);
                            if(tmp_value > 1)
                            {
                                tok = ST_ON_VALUE;
                            }

                            set_register_database(_VR_CB_(upnp), upnp_data->deviceID, ST_ON_OFF, (const char*)tok, ST_REPLACE, 0);

                            if(brightness)
                            {
                                char value[SIZE_32B];
                                snprintf(value, sizeof(value), "%s,%s", tok, brightness);
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
                                set_register_database(_VR_CB_(upnp), upnp_data->deviceID, ST_DIM, (const char*)brightness, ST_REPLACE, 0);
                            }
                            else
                            {
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string(tok));
                            }
                        }
                    }
                    else
                    {
                        if(brightness)
                        {
                            char value[SIZE_32B];
                            snprintf(value, sizeof(value), "%s,%s", BinaryState, brightness);
                            json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
                            set_register_database(_VR_CB_(upnp), upnp_data->deviceID, ST_DIM, (const char*)brightness, ST_REPLACE, 0);
                        }
                        else
                        {

                            json_object_object_add(jobj, ST_VALUE, json_object_new_string(BinaryState));
                        }
                        set_register_database(_VR_CB_(upnp), upnp_data->deviceID, ST_ON_OFF, (const char*)BinaryState, ST_REPLACE, 0);
                    }
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                }

                if(!re_use_data)
                {
                    ithread_mutex_lock(&notify_lock);
                    strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
                    g_notify.notify_index=g_notify.notify_index+1;
                    ithread_mutex_unlock(&notify_lock);
                }

                json_object_put(jobj);
            }
            FREE_SEARCH_DATA_VAR(tmp_data);
            //printfresult(data);
            SAFE_FREE_XML(BinaryState);
            SAFE_FREE_XML(brightness);
        }
        else if(!strcmp(DOCS->n.firstChild->nodeName, "u:GetEndDevicesResponse")) 
        {
             //////get end devs wemo link////
            char *DeviceLists = Util_GetFirstDocumentItem(DOCS, "DeviceLists");
            if(DeviceLists) 
            {
                json_object *jobj = json_object_new_object();
                json_object *devicelist = json_object_new_array();
                if(upnp_data)
                {
                    SEARCH_DATA_INIT_VAR(tmp_data);
                    searching_database(_VR_CB_(upnp), &tmp_data,
                                    "SELECT friendlyName from CONTROL_DEVS where udn='%s' AND added='yes'", 
                                    upnp_data->deviceID);

                    if(tmp_data.len)
                    {
                        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
                        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_ACTION_COMPLETE));
                        json_object_object_add(jobj, ST_FRIENDLY_NAME, json_object_new_string(tmp_data.value));
                        json_object_object_add(jobj, ST_UDN, json_object_new_string(upnp_data->deviceID));
                        json_object_object_add(jobj, ST_ACTION_TYPE, json_object_new_string(ST_GET_SUB_DEVS));
                        json_object_object_add(jobj, ST_LIST_TYPE, json_object_new_string(upnp_data->subdevID));
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                    }
                    FREE_SEARCH_DATA_VAR(tmp_data);
                }

                LOG(LOG_DBG, "DeviceLists = %s\n", DeviceLists);
                if(strcmp(DeviceLists, "0") != 0) 
                {
                    printf("start parse\n\n"); 
                    if(ixmlParseBufferEx(DeviceLists, &ActionDoc) == IXML_SUCCESS)
                    {
                        database_actions(_VR_(upnp), "DELETE FROM TMP_SUBDEVS;");

                        SEARCH_DATA_INIT_VAR(Owner);
                        char ctrl_url[strlen(a_event->CtrlUrl)+1];
                        char url[strlen(a_event->CtrlUrl)+1];
                        strcpy(ctrl_url, a_event->CtrlUrl);
                        char *save_tok;
                        char *tok = strtok_r(ctrl_url, "/", &save_tok);
                        strcpy(url, tok);
                        tok = strtok_r(NULL, "/", &save_tok);
                        sprintf(url+strlen(url), "//%s", tok);
                        strcat(url, "/setup.xml");
                        searching_database(_VR_CB_(upnp), &Owner, 
                                            "SELECT udn from CONTROL_DEVS where url='%s'", url);

                        DevList = ixmlNode_getFirstChild((IXML_Node *)ActionDoc)->firstChild;
                        LOG(LOG_DBG,"DevList->nodeName = %s\n", DevList->nodeName);
                        while(DevList != NULL)
                        {
                            LOG(LOG_DBG,"DeviceListType = %s\n", DevList->firstChild->firstChild->nodeValue);
                            DevInfo = DevList->firstChild->nextSibling->firstChild;
                            LOG(LOG_DBG,"DevInfo->nodeName = %s\n", DevInfo->nodeName);
                            while(DevInfo != NULL)
                            {
                                char friendlyName[256];
                                char ID[256];
                                char Capability[256];
                                
                                memset(friendlyName, 0x00, sizeof(friendlyName));
                                memset(ID, 0x00, sizeof(ID));
                                memset(Capability, 0x00, sizeof(Capability));

                                tmp = DevInfo->firstChild;

                                tmp = tmp->nextSibling;
                                if(tmp->firstChild != NULL)
                                {
                                    printf("Device ID= %s\n", tmp->firstChild->nodeValue);
                                    strcpy(ID, tmp->firstChild->nodeValue);
                                }

                                tmp = tmp->nextSibling;
                                if(tmp->firstChild != NULL)
                                {
                                    printf("FriendlyName= %s\n", tmp->firstChild->nodeValue);
                                    strcpy(friendlyName, tmp->firstChild->nodeValue);
                                }

                                tmp = tmp->nextSibling->nextSibling->nextSibling;
                                if(tmp->firstChild != NULL)
                                {
                                    printf("CapabilityID= %s\n", tmp->firstChild->nodeValue);
                                    strcpy(Capability, tmp->firstChild->nodeValue);
                                }

                                if(Owner.len && strlen(ID) && strlen(friendlyName))
                                {
                                    database_actions(_VR_(upnp), "INSERT INTO %s VALUES('%s','%s','%s','%s','%s')", 
                                        "TMP_SUBDEVS (owner,serial,friendlyName,id,capability)", Owner.value, ID, friendlyName, ID, Capability);
                                    json_object * device = json_object_new_object();
                                    json_object_object_add(device, ST_OWNER, json_object_new_string(Owner.value));
                                    json_object_object_add(device, ST_SERIAL, json_object_new_string(ID));
                                    json_object_object_add(device, ST_FRIENDLY_NAME, json_object_new_string(friendlyName));
                                    json_object_object_add(device, ST_ID, json_object_new_string(ID));
                                    json_object_object_add(device, ST_CAPABILITY, json_object_new_string(Capability));
                                    json_object_array_add(devicelist, device);
                                }
                                    
                                DevInfo = DevInfo->nextSibling;
                            }
                            DevList = DevList->nextSibling;
                        }          

                        if(ActionDoc) 
                        {
                            ixmlDocument_free(ActionDoc);
                        }

                        FREE_SEARCH_DATA_VAR(Owner);
                    }
                }

                json_object_object_add(jobj, ST_DEVICES_LIST, devicelist);
                
                FreeXmlSource(DeviceLists);

                ithread_mutex_lock(&notify_lock);
                strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
                g_notify.notify_index=g_notify.notify_index+1;
                ithread_mutex_unlock(&notify_lock);

                json_object_put(jobj);
            }
        }
        else if(!strcmp(DOCS->n.firstChild->nodeName, "u:AddDeviceResponse"))
        {
            if(upnp_data)
            {
                SEARCH_DATA_INIT_VAR(tmp_data);
                searching_database(_VR_CB_(upnp), &tmp_data,
                                    "SELECT friendlyName from CONTROL_DEVS where udn='%s' AND Added='yes'", 
                                    upnp_data->deviceID);

                if(tmp_data.len)
                {
                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_ACTION_COMPLETE));
                    json_object_object_add(jobj, ST_FRIENDLY_NAME, json_object_new_string(tmp_data.value));
                    json_object_object_add(jobj, ST_UDN, json_object_new_string(upnp_data->deviceID));
                    json_object_object_add(jobj, ST_ACTION_TYPE, json_object_new_string(ST_ADD_SUB_DEVS));
                    json_object_object_add(jobj, ST_SUBDEVID, json_object_new_string(upnp_data->subdevID));
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

                    ithread_mutex_lock(&notify_lock);
                    strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
                    g_notify.notify_index=g_notify.notify_index+1;
                    ithread_mutex_unlock(&notify_lock);

                    json_object_put(jobj);
                }
                FREE_SEARCH_DATA_VAR(tmp_data);
            }
        }
        else if(!strcmp(DOCS->n.firstChild->nodeName, "u:RemoveDeviceResponse"))
        {
            if(upnp_data)
            {
                SEARCH_DATA_INIT_VAR(tmp_data);
                searching_database(_VR_CB_(upnp), &tmp_data,
                                    "SELECT friendlyName from CONTROL_DEVS where udn='%s' AND added='yes'", 
                                    upnp_data->deviceID);

                if(tmp_data.value)
                {
                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_ACTION_COMPLETE));
                    json_object_object_add(jobj, ST_FRIENDLY_NAME, json_object_new_string(tmp_data.value));
                    if(upnp_data->deviceID)
                    {
                        json_object_object_add(jobj, ST_UDN, json_object_new_string(upnp_data->deviceID));
                    }
                    json_object_object_add(jobj, ST_ACTION_TYPE, json_object_new_string(ST_REMOVE_SUB_DEV));
                    if(upnp_data->subdevID)
                    {
                        json_object_object_add(jobj, ST_SUBDEVID, json_object_new_string(upnp_data->subdevID));
                    }
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

                    if(upnp_data->deviceID && upnp_data->subdevID)
                    {
                        database_actions(_VR_(upnp), "DELETE FROM SUB_DEVICES where owner='%s' AND serial='%s';",
                                        upnp_data->deviceID, upnp_data->subdevID);
                    }

                    ithread_mutex_lock(&notify_lock);
                    strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
                    g_notify.notify_index=g_notify.notify_index+1;
                    ithread_mutex_unlock(&notify_lock);

                    json_object_put(jobj);
                }
                FREE_SEARCH_DATA_VAR(tmp_data);
            }
        }
        else if(!strcmp(DOCS->n.firstChild->nodeName, "u:GetDeviceStatusResponse"))
        {
            if(upnp_data)
            {
                SEARCH_DATA_INIT_VAR(tmp_data);
                searching_database(_VR_CB_(upnp), &tmp_data,
                                    "SELECT friendlyName from CONTROL_DEVS where udn='%s' AND added='yes'", 
                                    upnp_data->deviceID);

                if(tmp_data.len)
                {
                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_ACTION_COMPLETE));
                    json_object_object_add(jobj, ST_FRIENDLY_NAME, json_object_new_string(tmp_data.value));
                    json_object_object_add(jobj, ST_UDN, json_object_new_string(upnp_data->deviceID));
                    json_object_object_add(jobj, ST_ACTION_TYPE, json_object_new_string(ST_GET_SUB_DEV_STATUS));
                    json_object_object_add(jobj, ST_SUBDEVID, json_object_new_string(upnp_data->subdevID));
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

                    json_object *value = json_object_new_object();
                    

                    char *DeviceStatusList = Util_GetFirstDocumentItem(DOCS, "DeviceStatusList");
                    if(DeviceStatusList)
                    {
                        //printf("DeviceStatusList = %s\n", DeviceStatusList);
                        if(ixmlParseBufferEx(DeviceStatusList, &ActionDoc) == IXML_SUCCESS)
                        {
                            DevList = ixmlNode_getFirstChild((IXML_Node *)ActionDoc)->firstChild;
                            printf("DevList->nodeName = %s\n", DevList->nodeName);
                            if(DevList->firstChild != NULL) 
                            {
                                tmp = DevList->firstChild;
                            }

                            printf("\n");
                            printf("-----Results-----\n");
                            if(tmp->firstChild != NULL)
                            {
                                printf("Is Group = %s\n", tmp->firstChild->nodeValue);
                                json_object_object_add(value, "Is Group", json_object_new_string(tmp->firstChild->nodeValue));
                            }
                            tmp = tmp->nextSibling;

                            if(tmp->firstChild != NULL)
                            {
                                printf("Device ID = %s\n", tmp->firstChild->nodeValue);
                                json_object_object_add(value, "Device ID", json_object_new_string(tmp->firstChild->nodeValue));
                            }
                            tmp = tmp->nextSibling;

                            if(tmp->firstChild != NULL)
                            {
                                printf("CapabilityID ID = %s\n", tmp->firstChild->nodeValue);
                                json_object_object_add(value, "CapabilityID ID", json_object_new_string(tmp->firstChild->nodeValue)); 
                            }
                            tmp = tmp->nextSibling;

                            if(tmp->firstChild != NULL)
                            {
                                printf("CapabilityID Value = %s\n", tmp->firstChild->nodeValue);
                                json_object_object_add(value, "CapabilityID Value", json_object_new_string(tmp->firstChild->nodeValue));
                            }
                            printf("=================\n");
                            printf("\n");

                            if(ActionDoc) 
                            {
                                ixmlDocument_free(ActionDoc);
                            }
                        }
                        
                        FreeXmlSource(DeviceStatusList);
                    }

                    json_object_object_add(jobj, ST_VALUE, value);

                    ithread_mutex_lock(&notify_lock);
                    strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
                    g_notify.notify_index=g_notify.notify_index+1;
                    ithread_mutex_unlock(&notify_lock);

                    json_object_put(jobj);
                }
                FREE_SEARCH_DATA_VAR(tmp_data);
            }
            
        }
        else if(!strcmp(DOCS->n.firstChild->nodeName, "u:GetNetworkListResponse"))
        {
            /* code */
            char *netlist = Util_GetFirstDocumentItem(DOCS, "NetworkList");
            if(netlist) 
            {
                

                FreeXmlSource(netlist);
            }
        }
        else if(!strcmp(DOCS->n.firstChild->nodeName, "u:GetMacAddrResponse"))
        {
            char *MacAddress = Util_GetFirstDocumentItem(DOCS, "MacAddr");
            char *SerialNo = Util_GetFirstDocumentItem(DOCS, "SerialNo");
            if(MacAddress && SerialNo)
            {
                strcpy(DeviceMacAddress, MacAddress);
                strcpy(DeviceSerialNo, SerialNo);

                SEARCH_DATA_INIT_VAR(UDN);
                searching_database(_VR_CB_(upnp), &UDN, 
                                    "SELECT udn from WEMO_WIFI_CONFIG where config='start_config'");

                SEARCH_DATA_INIT_VAR(ssid);
                searching_database(_VR_CB_(upnp), &ssid, 
                                    "SELECT ssid from WEMO_WIFI_CONFIG where config='start_config'");

                if(UDN.len && ssid.len)
                {
                    upnp_cmd_data *upnp_data_connect = (upnp_cmd_data *)malloc(sizeof(upnp_cmd_data));
                    memset(upnp_data_connect, 0x00, sizeof(upnp_cmd_data));

                    char *ssid_connected = (char*)malloc(128);
                    char *result = (char*)calloc(256,sizeof(char));
                    strcpy(ssid_connected, ssid.value);
                    upnp_data_connect->deviceID=ssid_connected;
                    upnp_data_connect->result=result;
                    if(CtrlPointProcessCommand("connect", UDN.value, NULL, (void*)upnp_data_connect))
                    {
                        SAFE_FREE(upnp_data_connect->deviceID);
                        SAFE_FREE(upnp_data_connect->result);
                        SAFE_FREE(upnp_data_connect);
                    }
                }
                FREE_SEARCH_DATA_VAR(UDN);
                FREE_SEARCH_DATA_VAR(ssid);
                
            }
            if(MacAddress) {
               FreeXmlSource(MacAddress);
            }

            if(SerialNo) {  
               FreeXmlSource(SerialNo);       
            }
        }
        else if(!strcmp(DOCS->n.firstChild->nodeName, "u:ConnectHomeNetworkResponse"))
        {
            char *PairingStatus = Util_GetFirstDocumentItem(DOCS, "PairingStatus");
            if(PairingStatus)
            {
                printf("PairingStatus = %s\n", PairingStatus);
                printf("#########################################################\n");
                printf("############# PairingStatus: %s ###############\n", PairingStatus);
                printf("#########################################################\n");
                FreeXmlSource(PairingStatus);
            }

            g_wemo_device_up=1;

            if(upnp_data)
            {
                database_actions(_VR_(upnp), "UPDATE WEMO_WIFI_CONFIG set config='configuring' where ssid='%s'",
                        upnp_data->deviceID);
            }
            sleep(15);
            if(g_wemo_device_up)
            {
                printf("#########################################################\n");
                printf("############ g_wemo_device_up %d #######\n", g_wemo_device_up);
                printf("#########################################################\n");
                polling_wemo_connect_status();
            }
        }
        else if(!strcmp(DOCS->n.firstChild->nodeName, "u:GetNetworkStatusResponse"))
        {
            char *NetworkStatus = Util_GetFirstDocumentItem(DOCS, "NetworkStatus");
            if(NetworkStatus)
            {
                printf("NetworkStatus = %s\n", NetworkStatus);
                if(!strcmp(NetworkStatus, "1"))
                {
                    g_wemo_device_state_NOT_CONNECTED=0;
                    SEARCH_DATA_INIT_VAR(UDN);
                    searching_database(_VR_CB_(upnp), &UDN, 
                                    "SELECT udn from WEMO_WIFI_CONFIG where config='configuring'");
                    if(UDN.len)
                    {
                        printf("#########################################################\n");
                        printf("############ CLOSE SETUP %s #######\n", UDN.value);
                        printf("#########################################################\n");
                        Close_setup(UDN.value, NULL, NULL);
                    }
                    database_actions(_VR_(upnp), "UPDATE WEMO_WIFI_CONFIG set config='configured' where config='configuring'");
                    FREE_SEARCH_DATA_VAR(UDN);
                }
                else if (!strcmp(NetworkStatus, "3"))
                {
                    if(g_wemo_device_state_NOT_CONNECTED > 10)
                    {
                        g_wemo_device_state_NOT_CONNECTED = 0;

                        SEARCH_DATA_INIT_VAR(UDN);
                        searching_database(_VR_CB_(upnp), &UDN, 
                                        "SELECT udn from WEMO_WIFI_CONFIG where config='configuring'");
                        if(UDN.len)
                        {
                            printf("#########################################################\n");
                            printf("############ CLOSE SETUP %s #######\n", UDN.value);
                            printf("#########################################################\n");
                            Close_setup(UDN.value, NULL, NULL);
                        }
                        database_actions(_VR_(upnp), "UPDATE WEMO_WIFI_CONFIG set config='configured' where config='configuring'");
                        FREE_SEARCH_DATA_VAR(UDN);
                    }
                    else
                    {
                        g_wemo_device_state_NOT_CONNECTED ++;
                    }
                    
                }
                else if(!strcmp(NetworkStatus, "2"))
                {
                    database_actions(_VR_(upnp), "UPDATE WEMO_WIFI_CONFIG set config='FAILURE' where config='configuring'");
                    system("uci set auto_config.@auto-config[0].state=2");
                    system("uci commit auto_config");
                    system("mv /etc/config/ret_wireless /etc/config/wireless");
                    system("wifi");
                }
                else
                {
                    if(g_wemo_device_state_disconnect < 15)
                    {
                        g_wemo_device_state_disconnect++;
                    }
                    else
                    {
                        SEARCH_DATA_INIT_VAR(UDN);
                        searching_database(_VR_CB_(upnp), &UDN, 
                                            "SELECT udn from WEMO_WIFI_CONFIG where config='configuring'");
                        if(UDN.len)
                        {
                            database_actions(_VR_(upnp), "UPDATE WEMO_WIFI_CONFIG set config='start_config' where config='configuring'");
                            CtrlPointProcessCommand("getmac", UDN.value, NULL, NULL);
                        }
                        g_wemo_device_state_disconnect=0;
                        FREE_SEARCH_DATA_VAR(UDN);
                    }
                }
                FreeXmlSource(NetworkStatus);
            }
        }
        else if(!strcmp(DOCS->n.firstChild->nodeName, "u:CloseSetupResponse"))
        {
            char *status = Util_GetFirstDocumentItem(DOCS, ST_STATUS);
            if(status && !strcmp(status, "success"))
            {
                SEARCH_DATA_INIT_VAR(wemo_ssid);
                searching_database(_VR_CB_(upnp), &wemo_ssid, 
                                    "SELECT ssid from WEMO_WIFI_CONFIG where config='NO' LIMIT 1");
                if(wemo_ssid.len)
                {
                    char cmd[256];
                    database_actions(_VR_(upnp), "UPDATE WEMO_WIFI_CONFIG set config='start_config' where ssid='%s'", wemo_ssid.value);
                    sprintf(cmd, "uci set wireless.@wifi-iface[0].ssid='%s'", wemo_ssid.value);system(cmd);
                    system("uci set wireless.@wifi-iface[0].encryption=Open");
                    system("uci delete wireless.@wifi-iface[0].key");
                    system("uci commit wireless");
                    system("uci set auto_config.@auto-config[0].state=1");
                    system("uci commit auto_config");
                    printf("#########################################################\n");
                    printf("############# AUTO CONFIG: %s ###############\n", wemo_ssid.value);
                    printf("#########################################################\n");
                    system("wifi");
                }
                else
                {
                    printf("\n###################################################\n");
                    printf("########## AUTO CONFIG SUCCESS ####################\n");
                    printf("###################################################\n\n");
                    system("uci set auto_config.@auto-config[0].state=2");
                    system("uci commit auto_config");
                    system("mv /etc/config/ret_wireless /etc/config/wireless");
                    system("wifi");
                }
                FREE_SEARCH_DATA_VAR(wemo_ssid);
            }
        }
        else if(!strcmp(DOCS->n.firstChild->nodeName, "u:GetFriendlyNameResponse"))
        {
            char *name = Util_GetFirstDocumentItem(DOCS, "FriendlyName");
            if(name) 
            {
                if(upnp_data)
                {
                    SEARCH_DATA_INIT_VAR(tmp_data);
                    searching_database(_VR_CB_(upnp), &tmp_data,
                                        "SELECT friendlyName from CONTROL_DEVS where udn='%s' AND added='yes'", 
                                        upnp_data->deviceID);

                    if(tmp_data.len)
                    {
                        if(strcmp(tmp_data.value, name))
                        {
                            database_actions(_VR_(upnp), "UPDATE CONTROL_DEVS set friendlyName='%s' where udn='%s'",
                                                        name, upnp_data->deviceID);
                        }
                        json_object *jobj = json_object_new_object();
                        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
                        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_ACTION_COMPLETE));
                        json_object_object_add(jobj, ST_UDN, json_object_new_string(upnp_data->deviceID));
                        json_object_object_add(jobj, ST_ACTION_TYPE, json_object_new_string(ST_GET_NAME));
                        json_object_object_add(jobj, ST_VALUE, json_object_new_string(name));
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

                        ithread_mutex_lock(&notify_lock);
                        strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
                        g_notify.notify_index=g_notify.notify_index+1;
                        ithread_mutex_unlock(&notify_lock);

                        json_object_put(jobj);
                    }
                    FREE_SEARCH_DATA_VAR(tmp_data);
                }
                FreeXmlSource(name);
            }
        }
        else
        {
            //get icon url
            char *url_icon = Util_GetFirstDocumentItem(DOCS, "URL");
            if(url_icon) 
            {
                strcpy(data, "Url icon =");
                sprintf(data+strlen(data), " %s\n", url_icon);
                //printf("url = %s\n", url_icon);
                printfresult(data);
                FreeXmlSource(url_icon);
            }

             ////////// setdevstatus//////
            char *ErrorDeviceIDs = Util_GetFirstDocumentItem(DOCS, "ErrorDeviceIDs"); 
            if(ErrorDeviceIDs)
            {
                //printf("ErrorDeviceIDs = %s\n", ErrorDeviceIDs);
                if(ErrorDeviceIDs[0] != 0x00)
                {
                    printf("\n");
                    printf("-----Results-----\n");
                    printf("ERROR with bulb have id = %s\n", ErrorDeviceIDs);
                    printf("=================\n");
                    printf("\n"); 
                }
                // else 
                // {
                //     printfresult("Action Success\n");
                // }
                free(ErrorDeviceIDs); //maybe ErrorDeviceIDs = 0x00, function FreeXmlSource cant free
            }
        }
    }
    else 
    {
        printf("DOCS NULL\n");
    }

done:
    // printf("free upnp_data\n");
    if(upnp_data && !re_use_data)
    {
        SAFE_FREE(upnp_data->deviceID);
        SAFE_FREE(upnp_data->subdevID);
        SAFE_FREE(upnp_data->result);
        free(upnp_data);
    }
}  

void CtrlPointProcessNotifyAction(struct Upnp_Event *e_event)
{  
    IXML_Document *ActionDoc;
    IXML_Node *StateEvent = NULL;
    IXML_Node *tmp = NULL;
    IXML_Document *DOCS = e_event->ChangedVariables;

    CtrlPluginDeviceNode *deviceNode = GetDeviceNodeBySID(e_event->Sid);
    // printf("%s\n", ixmlDocumenttoString(DOCS));
    // if(deviceNode != NULL)
    // { 
    //     printf("Received Notify from Device: %s\n", deviceNode->device.UDN);
    // }

    if(DOCS != NULL) 
    {
        char *BinaryState = Util_GetFirstDocumentItem(DOCS, "BinaryState");
        if(BinaryState)
        {
            // printf("BinaryState = %s\n", BinaryState);

            SEARCH_DATA_INIT_VAR(tmp_data);

            if(deviceNode->device.UDN)
                searching_database(_VR_CB_(upnp), &tmp_data,
                                "SELECT friendlyName from CONTROL_DEVS where udn='%s' AND added='yes'", 
                                deviceNode->device.UDN);

            if(tmp_data.len)
            {
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
                if(deviceNode->device.UDN)
                {
                    json_object_object_add(jobj, ST_UDN, json_object_new_string(deviceNode->device.UDN));
                    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_BINARY_STATE_RESPONSE));
                    //set_register_database(_VR_CB_(upnp), deviceNode->device.UDN, ST_ON_OFF, (const char*)BinaryState, ST_REPLACE);
                    if(strchr(BinaryState, ','))
                    {
                        char *save_tok;
                        char *tok = strtok_r(BinaryState, ",", &save_tok);
                        if(tok != NULL)
                        {
                            set_register_database(_VR_CB_(upnp), deviceNode->device.UDN, ST_ON_OFF, (const char*)tok, ST_REPLACE, 0);
                            tok = strtok_r(NULL, ",", &save_tok);
                            set_register_database(_VR_CB_(upnp), deviceNode->device.UDN, ST_DIM, (const char*)tok, ST_REPLACE, 0);
                        }

                        json_object_object_add(jobj, ST_VALUE, json_object_new_string(BinaryState));
                    }
                    else 
                    {
                        char *tok = strtok(BinaryState, "|");
                        if(tok)
                        {
                            if(!strcmp(tok, "0"))
                            {
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string(tok));
                                set_register_database(_VR_CB_(upnp), deviceNode->device.UDN, ST_ON_OFF, (const char*)tok, ST_REPLACE, 0);
                            }
                            else
                            {
                                json_object_object_add(jobj, ST_VALUE, json_object_new_string("1"));
                                set_register_database(_VR_CB_(upnp), deviceNode->device.UDN, ST_ON_OFF, (const char*)"1", ST_REPLACE, 0);
                            }
                        }
                        else
                        {
                            set_register_database(_VR_CB_(upnp), deviceNode->device.UDN, ST_ON_OFF, (const char*)BinaryState, ST_REPLACE, 0);
                            json_object_object_add(jobj, ST_VALUE, json_object_new_string(BinaryState));
                        }
                    }
                }

                ithread_mutex_lock(&notify_lock);
                strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
                g_notify.notify_index=g_notify.notify_index+1;
                ithread_mutex_unlock(&notify_lock);

                json_object_put(jobj);
            }
            FREE_SEARCH_DATA_VAR(tmp_data);
            FreeXmlSource(BinaryState);
        }

        char *Brightness = Util_GetFirstDocumentItem(DOCS, "Brightness");
        if(Brightness)
        {
            SEARCH_DATA_INIT_VAR(tmp_data);

            if(deviceNode->device.UDN)
                searching_database(_VR_CB_(upnp), &tmp_data,
                                "SELECT friendlyName from CONTROL_DEVS where udn='%s' AND added='yes'",
                                deviceNode->device.UDN);

            if(tmp_data.len)
            {
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
                if(deviceNode->device.UDN)
                {
                    json_object_object_add(jobj, ST_UDN, json_object_new_string(deviceNode->device.UDN));
                    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_BINARY_STATE_RESPONSE));

                    SEARCH_DATA_INIT_VAR(onOffValue);
                    searching_database(_VR_CB_(upnp), &onOffValue,
                                "SELECT register from FEATURES where deviceId='%s' and featureId='%s'",
                                deviceNode->device.UDN, ST_ON_OFF);
                    if(onOffValue.len)
                    {
                        char value[SIZE_32B];
                        snprintf(value, sizeof(value), "%s,%s", onOffValue.value, Brightness);
                        json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
                    }
                    else
                    {
                        json_object_object_add(jobj, ST_VALUE, json_object_new_string(Brightness));
                    }
                    FREE_SEARCH_DATA_VAR(onOffValue);
                    set_register_database(_VR_CB_(upnp), deviceNode->device.UDN, ST_DIM, (const char*)Brightness, ST_REPLACE, 0);
                }

                ithread_mutex_lock(&notify_lock);
                strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
                g_notify.notify_index=g_notify.notify_index+1;
                ithread_mutex_unlock(&notify_lock);

                json_object_put(jobj);
            }
            FREE_SEARCH_DATA_VAR(tmp_data);
            FreeXmlSource(Brightness);
        }

        char *StatusChange = Util_GetFirstDocumentItem(DOCS, "StatusChange");
        if(StatusChange)
        {
            if(StatusChange[0] != 0x00) 
            {
                LOG(LOG_DBG,"StatusChange = %s\n", StatusChange);
                if(ixmlParseBufferEx(StatusChange, &ActionDoc) == IXML_SUCCESS)
                {
                    StateEvent = ixmlNode_getFirstChild((IXML_Node *)ActionDoc);
                    LOG(LOG_DBG,"StateEvent->nodeName = %s\n", StateEvent->nodeName);
                    if(StateEvent->firstChild != NULL) 
                    {
                        tmp = StateEvent->firstChild;
                    }

                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
                    if(deviceNode->device.UDN)
                        json_object_object_add(jobj, "UDN", json_object_new_string(deviceNode->device.UDN));
                    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_STATUS_CHANGE));

                    if(tmp->firstChild != NULL)
                    {
                        printf("Device ID= %s\n", tmp->firstChild->nodeValue);

                        json_object_object_add(jobj, ST_SUBDEVID, json_object_new_string(tmp->firstChild->nodeValue));

                    }

                    tmp = tmp->nextSibling;
                    if(tmp->firstChild != NULL)
                    {
                        printf("CapabilityId= %s\n", tmp->firstChild->nodeValue);

                        json_object_object_add(jobj, ST_CAPABILITY_ID, json_object_new_string(tmp->firstChild->nodeValue));

                    }

                    tmp = tmp->nextSibling;
                    if(tmp->firstChild != NULL)
                    {
                        printf("Value= %s\n", tmp->firstChild->nodeValue);

                        json_object_object_add(jobj, ST_VALUE, json_object_new_string(tmp->firstChild->nodeValue));

                    }

                    SEARCH_DATA_INIT_VAR(tmp_data);
                    if(deviceNode->device.UDN)
                        searching_database(_VR_CB_(upnp), &tmp_data,
                                            "SELECT friendlyName from CONTROL_DEVS where udn='%s' AND added='yes'", 
                                            deviceNode->device.UDN);

                    if(tmp_data.len)
                    {
                        json_object_object_add(jobj, ST_FRIENDLY_NAME, json_object_new_string(tmp_data.value));

                        ithread_mutex_lock(&notify_lock);
                        strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message,json_object_to_json_string(jobj));
                        g_notify.notify_index=g_notify.notify_index+1;
                        ithread_mutex_unlock(&notify_lock);
                    }   

                    if(ActionDoc) 
                    {
                        ixmlDocument_free(ActionDoc);
                    }
                    FREE_SEARCH_DATA_VAR(tmp_data);
                    json_object_put(jobj);
                }
            }
            free(StatusChange);
        }

        char *SensorChange = Util_GetFirstDocumentItem(DOCS, "SensorChange");
        if(SensorChange)
        {
            if(SensorChange[0] != 0x00) 
            {
                LOG(LOG_DBG,"SensorChange = %s\n", StatusChange);
                if(ixmlParseBufferEx(SensorChange, &ActionDoc) == IXML_SUCCESS)
                {
                    StateEvent = ixmlNode_getFirstChild((IXML_Node *)ActionDoc);
                    LOG(LOG_DBG,"StateEvent->nodeName = %s\n", StateEvent->nodeName);
                    if(StateEvent->firstChild != NULL) 
                    {
                        tmp = StateEvent->firstChild;
                    }

                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
                    if(deviceNode->device.UDN)
                        json_object_object_add(jobj, "UDN", json_object_new_string(deviceNode->device.UDN));
                    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string("SensorChange"));

                    if(tmp->firstChild != NULL)
                    {
                        printf("Device ID= %s\n", tmp->firstChild->nodeValue);

                        json_object_object_add(jobj, "subdevid", json_object_new_string(tmp->firstChild->nodeValue));

                    }

                    tmp = tmp->nextSibling;
                    if(tmp->firstChild != NULL)
                    {
                        printf("CapabilityId= %s\n", tmp->firstChild->nodeValue);

                        json_object_object_add(jobj, "capabilityid", json_object_new_string(tmp->firstChild->nodeValue));

                    }

                    tmp = tmp->nextSibling;
                    if(tmp->firstChild != NULL)
                    {
                        printf("Value= %s\n", tmp->firstChild->nodeValue);

                        json_object_object_add(jobj, ST_VALUE, json_object_new_string(tmp->firstChild->nodeValue));

                    }

                    SEARCH_DATA_INIT_VAR(tmp_data);
                    if(deviceNode->device.UDN)
                        searching_database(_VR_CB_(upnp), &tmp_data,
                                            "SELECT friendlyName from CONTROL_DEVS where udn='%s' AND added='yes'", 
                                            deviceNode->device.UDN);

                    if(tmp_data.len)
                    {
                        json_object_object_add(jobj, ST_FRIENDLY_NAME, json_object_new_string(tmp_data.value));

                        ithread_mutex_lock(&notify_lock);
                        strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message,json_object_to_json_string(jobj));
                        g_notify.notify_index=g_notify.notify_index+1;
                        ithread_mutex_unlock(&notify_lock);
                    }

                    if(ActionDoc) 
                    {
                        ixmlDocument_free(ActionDoc);
                    }
                    FREE_SEARCH_DATA_VAR(tmp_data);
                    json_object_put(jobj);
                }
            }
            free(StatusChange);
        }  
    }
}

CtrlPluginDeviceNode* GetDeviceNodeBySID(const char* SID)
{
    CtrlPluginDeviceNode *node = GlobalDeviceList;
    while(node)
    {
        if ((!strcmp(SID, node->device.services[2].SID)) || (!strcmp(SID, node->device.services[7].SID)))
        {
            LOG(LOG_DBG, "device found: %s", node->device.UDN);
            break;
        }
        else
        {
            node = node->next;
        }
    }

    return node;
}

int StopPluginCtrlPoint(void)
{
    if (-1 == ctrlpt_handle)
    return 0x00;
    CtrlPointRemoveAll();
    UpnpUnRegisterClient( ctrlpt_handle );
    free_devicenode(&GlobalDeviceList);
    UpnpFinish();
    ctrlpt_handle = -1;

    SAFE_FREE(Devlist_Location);       
    SAFE_FREE(Devs_Location);       

    LOG(LOG_DBG, "Stop plugin Ctrl success");
    return PLUGIN_SUCCESS;
}

int CtrlPointRemoveAll(void)
{
    LockDeviceSync();

    CtrlPluginDeviceNode *curdevnode;
    CtrlPluginDeviceNode *next;

    curdevnode = GlobalDeviceList;

    while (curdevnode) 
    {
        curdevnode->IsDeviceRequestUpdate = 0x01;  //- Reset, requested update when next discovery respone comes
        CtrlPointDeleteNode(curdevnode);
        next = curdevnode->next;
        curdevnode = next;
    }

    UnlockDeviceSync();

    return PLUGIN_SUCCESS;
}

int CtrlPointDeleteNode( CtrlPluginDeviceNode *node )
{
    
    int rc, service;

    if (NULL == node) 
    {
        return 0;  
    }

    for (service = 0; service < SERVICE_SERVCOUNT; service++)
    {
        if ((2 == service) || (0 == service) || (7 == service))
        {
            if (strcmp(node->device.services[service].SID, "") != 0)
            {
                rc = UpnpUnSubscribe(ctrlpt_handle, node->device.services[service].SID);
                if (UPNP_E_SUCCESS == rc) 
                {
                }
                else 
                {
                }
            }
        }
    }

    return PLUGIN_SUCCESS;
}

void CtrlPointProcessDeviceByebye(IXML_Document *DescDoc)
{
    printf("start ssdp byebye process\n");
    char* szUDN;
    if (0x00 == DescDoc)
    {
        LOG(LOG_DBG, "NO xml file found");  
        return;
    }

    szUDN = Util_GetFirstDocumentItem(DescDoc, "UDN");
    if ((0x00 == szUDN) || 0x00 == strlen(szUDN))
    {
        LOG(LOG_DBG, "Can not find device UDN");
        goto endbyebye;
    }

    remove_device_with_id((char*)szUDN);
endbyebye:
    FreeXmlSource(szUDN);
    return;
}

void remove_device_with_id(char *UDN)
{
    if(!UDN)
    {
        return; 
    }

    SEARCH_DATA_INIT_VAR(tmp_result);
    searching_database(_VR_CB_(upnp), &tmp_result,
                        "SELECT udn from CONTROL_DEVS where udn='%s'", UDN); 
    if(tmp_result.len)
    {
        database_actions(_VR_(upnp), "DELETE from CONTROL_DEVS where udn='%s'", UDN);
        remove_and_update_device_in_group(_VR_CB_(upnp), g_shm, UDN, ST_UPNP, NULL);
        database_actions(_VR_(upnp), "DELETE from FEATURES where deviceId='%s'", UDN);

        SEARCH_DATA_INIT_VAR(resource_id);
        searching_database(_VR_CB_(upnp), &resource_id, 
                        "SELECT owner from DEVICES where deviceId='%s'", 
                        UDN);
        if(resource_id.len)
        {
            VR_(delete_resources)(resource_id.value, RESOURCES_TIMEOUT);
            database_actions(_VR_(upnp), "DELETE from DEVICES where owner='%s'", resource_id.value);
            shm_update_data(g_shm, resource_id.value, NULL, NULL, SHM_DELETE);
        }
        FREE_SEARCH_DATA_VAR(resource_id);
        RemoveDevicenodeByUDN(&GlobalDeviceList, UDN);
    }
    FREE_SEARCH_DATA_VAR(tmp_result);
}

int free_devicenode(CtrlPluginDeviceNode **devnode)
{
    CtrlPluginDeviceNode *next;
    if((*devnode) == NULL)
    {
        return 0;    
    }
    else 
    {
        while((*devnode) != NULL) 
        {
            next = (*devnode)->next;
            free((*devnode));
            (*devnode) = next;
        }
    }
    return 1;
}

int copy_data(char *datain, char kind, int place, char *result)
{
    int length = strlen(datain);
    int i,times = 0;
    char *buf = result;
    for(i=0; i<length; i++)
    {
        if(times != place)
        {
            if(datain[i] == kind)
            {
                times++;
            } 
        }
        else
        {
            if(datain[i] == kind)
            {
                *buf = '\0';
                return 1;
                break;
            }
            else
            {
                *buf++ = datain[i]; 
            }
        }
    }
    *buf = '\0';
    return 0;
}

void free_param_val(struct upnpmsg *msg)
{
    int i;
    for(i=0; i<MAX_PARAM_NUM; i++)
    {
        if(NULL != msg->param_val[i])
        {
            free(msg->param_val[i]);
            msg->param_val[i] = NULL;
        }
    }
}

void RemoveDevicenodeByUDN(CtrlPluginDeviceNode **devnode, char *UDN)
{
    LockDeviceSync();
    CtrlPluginDeviceNode *tmp = *devnode;
    CtrlPluginDeviceNode *tmp_global = *devnode;
    while(tmp_global != NULL)
    {
        if(strstr(tmp_global->device.UDN, UDN) != NULL)
        {
            if(tmp == tmp_global) //delete first node
            {
                tmp = tmp_global->next;
                *devnode = tmp;             
                CtrlPointDeleteNode(tmp_global);
                free(tmp_global);
                tmp_global = tmp;
            }
            else
            {
                tmp->next = tmp_global->next;              
                CtrlPointDeleteNode(tmp_global);
                free(tmp_global);
                tmp_global = tmp->next;
            }
        }
        else
        {
            tmp = tmp_global;
            tmp_global = tmp_global->next;
        }
    }
    UnlockDeviceSync();
}

void printfresult(const char *result)
{
    printf("\n");
    printf("-----Results-----\n");
    printf("%s", result);
    printf("=================\n");
    printf("\n");
}

int GetPlaceFromID(char *ID)
{
    if(!strcmp(ID, "RealName"))
    {
        return 0;
    }
    if(!strcmp(ID, "Name"))
    {
        return 1;
    }
    if(!strcmp(ID, "UDN"))
    {
        return 2;
    }
    if(!strcmp(ID, "Location"))
    {
        return 3;
    }
    if(!strcmp(ID, "ID"))
    {
        return 1;
    }
    return -1;
}

int create_decrypt_pass(char *input, char *output){
   createPasswordKeyData();
   char ciphertext[PASSWORD_MAX_LEN];
   memset(ciphertext,0, sizeof(ciphertext));
   encryptPassword(input, ciphertext);
   strcpy(output, ciphertext);
   return 1;
}
void createPasswordKeyData()
{
    memset(PasswordKeyData,0,sizeof(PasswordKeyData));
    /* copy 3 MSB of the MAC address */
    memcpy(PasswordKeyData, DeviceMacAddress, 6);
    /* Append the  serial number */
    strncat(PasswordKeyData,DeviceSerialNo, sizeof(PasswordKeyData)-strlen(PasswordKeyData)-1);
    /* Now copy 3 LSB of the MAC address */
    strncat(PasswordKeyData,DeviceMacAddress+6, sizeof(PasswordKeyData)-strlen(PasswordKeyData)-1);
}


void encryptPassword(char *input, char *finalstr)
{
    unsigned char key_data[PASSWORD_KEYDATA_LEN];
    unsigned char salt[PASSWORD_SALT_LEN];
    unsigned char iv[PASSWORD_IV_LEN];
    int key_data_len, salt_len, iv_len;
    unsigned char *ciphertext = (unsigned char *)input;
    int len;
    char basePassword[256];
    char* encStr;
    char lenstr[4];
    //int cipher_len = 0;

    memset(key_data, 0, sizeof(key_data));
    memset(salt, 0, sizeof(salt));
    memset(iv, 0, sizeof(iv));
    //memset(finalstr, 0, sizeof(finalstr));
    memset(basePassword, 0, sizeof(basePassword));
    memset(lenstr, 0, sizeof(lenstr));

    len = strlen(input);
    strncpy((char *)key_data, PasswordKeyData, sizeof(key_data)-1);
    key_data_len = strlen((char *)key_data);
    memcpy(salt, PasswordKeyData, PASSWORD_SALT_LEN-1);
    memcpy(iv, PasswordKeyData, PASSWORD_IV_LEN-1);
    salt_len = strlen((char *)salt);
    iv_len = strlen((char *)iv);

    ciphertext = pluginAES128Encrypt(key_data, key_data_len, salt, salt_len, iv, iv_len, input, &len);
    if(!ciphertext)
    {
        LOG(LOG_DBG, "pluginAESEncrypt failed!!!");
        return;
    }
    ciphertext[len] = '\0';

    encStr = base64Encode(ciphertext, len);
    len = strlen(encStr);
    LOG(LOG_DBG,"Base 64 encoded ciphertext len: %d",len);
    //cipher_len = len;

    strncpy(finalstr,encStr,PASSWORD_MAX_LEN);

    memset(lenstr, 0, sizeof(lenstr));

    LOG(LOG_DBG,"Final String to be sent: %s",finalstr);

    SAFE_FREE(ciphertext); 

}  

int baseDecode(char *str, char *decStr, int *clen)
{
    char outBuf[SIZE_256B];
    int len = 0;
    BIO *bio, *b64;

    memset(outBuf,0,SIZE_256B);
    b64 = BIO_new(BIO_f_base64());
    LOG(LOG_DBG, "Base64 Encoded Message - %s",str);
    bio = BIO_new_mem_buf(str, strlen(str));
    bio = BIO_push(b64, bio);
    len = BIO_read(b64, &outBuf, SIZE_256B);
    LOG(LOG_DBG, "Base64 Decoded Message Len = %d",len);
    *clen = len;
    memcpy(decStr, outBuf, len);

    BIO_free_all(b64);
    return 0;
}
