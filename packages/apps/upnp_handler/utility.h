#ifndef UTILITY_H
#define UTILITY_H

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <sys/un.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <ifaddrs.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <syslog.h>
#include <signal.h>
#include <ctype.h>

#include <uci.h>

#define SUN_BACKLOG 4
#define SIN_BACKLOG 4
 
#define OBJ_PERMS (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)
#define WRITE_SEM 0
#define READ_SEM 1

#define SIZE_PARAM_IN_SHM 96

#define SZ_PARAM_NUM   32
#define SZ_PARAM_VAL   96
#define SZ_DEVICEINDEX 32

#define MSG_RESPONSE_LENGTH 1024

#define min(m,n) ((m) < (n) ? (m) : (n))
#define max(m,n) ((m) > (n) ? (m) : (n))

#define MAX_PARAM_NUM 10

#define MAX_FD 1024

#ifdef DAEMON_MODE
	char log_message[8192];
	#define LOG_DBG LOG_DEBUG
	#define LOG(level, ...)  \
	  snprintf(log_message,sizeof(log_message),__VA_ARGS__);\
	  syslog(level, "%s:%d:%s",__FILE__,__LINE__,log_message);
#else
	#define LOG_DBG (1)
	#define LOG(level, ...)  \
	do {  \
	if (level <= debug_level) { \
	  printf("%s:%d:", __FILE__, __LINE__); \
	  printf(__VA_ARGS__); \
	  printf("\n"); \
	} \
	} while (0)
#endif

extern int debug_level;

struct upnpmsg
{
    char cmd[64];
    char UDN[48];
    int  param_num;
    char *param_val[MAX_PARAM_NUM];
};

typedef struct ssdp_msg
{
    char localip[16];
    int time_expires;
    char DeviceID[256];
    char DeviceType[256];
    char ServiceType[256];
    char Location[256];
}ssdpmsg;

struct Cookie
{
    struct upnpmsg    * data;
};

ssize_t readln(int fd, void *buffer, size_t n);
ssize_t readws(int fd, void *buffer, size_t n);

void freeupnpmsg(struct upnpmsg *msg);
char* trim (char *s);

int Check_path_is_Dir_or_File(char *path, const char *text);

int make_daemon();
char* read_option(const char* option);

#endif
