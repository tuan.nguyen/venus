#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <libubus.h>
#include <libubox/blobmsg_json.h>
#include "wemo_upnp_api.h"

#define SLEEP_TIME 200 //micro second

#define BUFF_SIZE 256
enum {
    ID,
    VALUE,
    SUBDEVID,
    __MAX
};

char *path_default = "/tmp/upnp.conf";
char *Database_path = "/etc";
char *Database_Location = NULL;

char *upnp_ip_address = NULL;
char *PluginDeviceType = NULL;
unsigned short int upnp_port = 0;

notify_queue_t g_notify;
ithread_mutex_t notify_lock;

int g_wemo_device_up = 0;
int g_wemo_device_state_disconnect = 0;
int g_wemo_device_state_NOT_CONNECTED = 0;
int g_wemo_device_found_device = 0;

int g_shmid = 0;
char *g_shm = NULL;

char g_hub_ip[SIZE_32B];
char g_subnet_mask[SIZE_32B];

static struct ubus_event_handler ubus_listener;
static struct ubus_context *ctx = NULL;
static struct blob_buf buff;
uint32_t id;

void Send_ubus_notify(char *data);

int execute_system(const char*cmd)
{
    if (!cmd) {
        return -1;
    }
    printf("Executing %s...", cmd);
    int result = system(cmd);
    if (result==0){
        printf("OK\n");
    }
    else{
        printf("System result=%d\n", result);
    }
    if (-1 == result) {
        printf("Error executing system: %s\n", cmd);
    }
    return result;
}

static void notify_active_cb(struct uloop_timeout *timeout)
{
    if(g_notify.notify_index>0)
    {
        Send_ubus_notify((char*)g_notify.notify[0].notify_message);

        ithread_mutex_lock(&notify_lock);
        g_notify.notify_index--;
        memcpy((uint8_t*) &g_notify.notify[0],(uint8_t*)&g_notify.notify[1],g_notify.notify_index*sizeof(notify_t));
        ithread_mutex_unlock(&notify_lock);
    }
    uloop_timeout_set(timeout, 100);
}

static struct uloop_timeout polling_notify = {
    .cb = notify_active_cb,
};

static const struct blobmsg_policy null_policy[0] = {
};

static int features_callback(void *status, int argc, char **argv, char **azColName){

    json_object_object_add(status, argv[1], json_object_new_string(argv[2] ? argv[2] : "NULL"));
    return 0;
}

static int callback(void *data, int argc, char **argv, char **azColName){
    //printf("in callback\n");
    int i;
    json_object * jobj = json_object_new_object();
    for(i=0; i<argc; i++){
        //printf("%s = %s | ", azColName[i], argv[i] ? argv[i] : "NULL");
        json_object_object_add(jobj, azColName[i], json_object_new_string(argv[i] ? argv[i] : "NULL"));
    }

    json_object * status = json_object_new_object();

    searching_database(_VR_(upnp), features_callback, (void *)status,
                        "SELECT *from FEATURES where deviceId=='%s'", argv[3]);

    json_object_object_add(jobj, ST_STATUS, status);

    json_object_array_add(data, jobj);
    return 0;
}

static int listdevices_callback(void *data, int argc, char **argv, char **azColName){
    //printf("listdevices_callback\n");
    char bridge[64];
    memset(bridge, 0x00, sizeof(bridge));
    json_object * jobj = json_object_new_object();
    
    int i;
    for(i=0; i<argc -1; i++){
        //printf("%s = %s | ", azColName[i], argv[i] ? argv[i] : "NULL");
        json_object_object_add(jobj, azColName[i], json_object_new_string(argv[i] ? argv[i] : "NULL"));
        if(strstr(argv[i], "uuid:Bridge"))
        {
            strncpy(bridge, argv[i], sizeof(bridge)-1);
        }
    }

    if(strlen(bridge))
    {
        json_object *subdevlist = json_object_new_array();
        searching_database(_VR_(upnp), callback, (void *)subdevlist,
                        "SELECT * from SUB_DEVICES where owner='%s'", bridge);

        json_object_object_add(jobj, ST_SUB_DEV_LIST, subdevlist);
    }
    else
    {
        json_object * status = json_object_new_object();

        searching_database(_VR_(upnp), features_callback, (void *)status,
                        "SELECT *from FEATURES where deviceId=='%s'", argv[2]);

        json_object_object_add(jobj, ST_STATUS, status);
    }
    json_object_array_add(data, jobj);
    return 0;
}

static int list_devices(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_LIST_DEVICES_R));
    json_object *devicelist = json_object_new_array();

    searching_database(_VR_(upnp), listdevices_callback, (void *)devicelist,
                        "SELECT * from CONTROL_DEVS where type='upnp';");

    json_object_object_add(jobj, ST_DEVICES_LIST, devicelist);

    usleep(500);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}


static const struct blobmsg_policy binary_policy[__MAX] = {
    [ID] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
    [VALUE] = { .name = ST_VALUE, .type = BLOBMSG_TYPE_STRING },
    [SUBDEVID] = { .name = ST_SUBDEVID, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy setbinary_policy[4]={
    [0] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_CMD, .type = BLOBMSG_TYPE_STRING },
    [2] = { .name = ST_VALUE, .type = BLOBMSG_TYPE_STRING },
    [3] = { .name = ST_SUBDEVID, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy network_policy[1] = {
    [ID] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy getsub_policy[2] = {
    [ID] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
    [VALUE] = { .name = ST_LIST_TYPE, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy add_remove_policy[2] = {
    [ID] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
    [VALUE] = { .name = ST_SUBDEVID, .type = BLOBMSG_TYPE_STRING },
};

static int setbinary(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[4];
    const char *deviceID = "(unknown)";
    const char *command = "(unknown)";
    const char *value = "(unknown)";
    const char *subdevid = "(unknown)";

    blobmsg_parse(setbinary_policy, ARRAY_SIZE(setbinary_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
        deviceID = blobmsg_data(tb[0]);
    if (tb[1])
        command = blobmsg_data(tb[1]);
    if (tb[2])
        value = blobmsg_data(tb[2]);
    if (tb[3])
        subdevid = blobmsg_data(tb[3]);

    upnp_cmd_data *upnp_data = (upnp_cmd_data *)malloc(sizeof(upnp_cmd_data));
    memset(upnp_data, 0x00, sizeof(upnp_cmd_data));

    char *result = (char*)malloc(512);
    memset(result, 0x00, 512);
    char *devID = (char*)malloc(strlen(deviceID)+1);
    strcpy(devID, deviceID);
    upnp_data->deviceID = (void*)devID;
    upnp_data->result = (void*)result;

    SLOGI("deviceID = %s\n", deviceID);
    SLOGI("command = %s\n", command);
    SLOGI("value = %s\n", value);
    SLOGI("subdevid = %s\n", subdevid);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SET_BINARY_R));
    if (tb[3])
    {
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceID));
        json_object_object_add(jobj, ST_COMMAND, json_object_new_string(command));
        json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(ST_SUB_DEV));
        json_object_object_add(jobj, ST_SUBDEVID, json_object_new_string(subdevid));
        json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));

        char *subdevID = (char*)malloc(strlen(subdevid)+1);
        upnp_data->subdevID = (void*)subdevID;

        char param_val[1024];

        strcpy(param_val,"<?xml version=\"1.0\"?><DeviceStatusList><DeviceStatus><IsGroupAction>NO</IsGroupAction><DeviceID available=\"YES\">");
        sprintf(param_val+strlen(param_val), "%s%s", subdevid, "</DeviceID><CapabilityID>");//device ID
        if(!strcasecmp(command, ST_ON_OFF))
        {
            sprintf(param_val+strlen(param_val), "%s%s", "10006", "</CapabilityID><CapabilityValue>");//capability ID
        }
        else if(!strcasecmp(command, ST_DIM))
        {
            sprintf(param_val+strlen(param_val), "%s%s", "10008", "</CapabilityID><CapabilityValue>");//capability ID
        }
        
        sprintf(param_val+strlen(param_val), "%s%s", value, "</CapabilityValue>");//value
        sprintf(param_val+strlen(param_val), "%s", "</DeviceStatus></DeviceStatusList>");

        const char *test = param_val;

        if(!CtrlPointProcessCommand("setdevstatus", (char*)deviceID, (char **)&test, (void*)upnp_data))
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string("send success"));
        }
        else
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(result));

            free(upnp_data->result);
            free(upnp_data->deviceID);
            if(upnp_data->subdevID)
                free(upnp_data->subdevID);
            free(upnp_data);
        }
    }
    else
    {
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceID));
        json_object_object_add(jobj, ST_COMMAND, json_object_new_string(command));
        json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(ST_NORMAL_DEV));
        json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));

        int res = -1;
        const char *param[2];
        if(!strcmp(command, ST_ON_OFF))
        {
            param[0] = value;
            param[1] = ST_OFF_VALUE;
            res = CtrlPointProcessCommand("setbinary", (char*)deviceID, (char **)&param, (void*)upnp_data);
        }
        else
        {
            param[0] = ST_ON_VALUE;
            param[1] = value;
            res = CtrlPointProcessCommand("setbinary", (char*)deviceID, (char **)param, (void*)upnp_data);
        }

        if(!res)
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string("send success"));
        }
        else
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(result));

            free(upnp_data->result);
            free(upnp_data->deviceID);
            if(upnp_data->subdevID)
                free(upnp_data->subdevID);
            free(upnp_data);
        }
    }


    usleep(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);
    return 0;
}

static int getbinary(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[__MAX];
    const char *deviceID = "(unknown)";
    const char *subdevid = "(unknown)";

    blobmsg_parse(binary_policy, ARRAY_SIZE(binary_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[ID])
        deviceID = blobmsg_data(tb[ID]);
    if (tb[SUBDEVID])
        subdevid = blobmsg_data(tb[SUBDEVID]);

    upnp_cmd_data *upnp_data = (upnp_cmd_data *)malloc(sizeof(upnp_cmd_data));
    memset(upnp_data, 0x00, sizeof(upnp_cmd_data));

    char *result = (char*)malloc(512);
    memset(result, 0x00, 512);
    char *devID = (char*)malloc(strlen(deviceID)+1);
    strcpy(devID, deviceID);
    upnp_data->deviceID = (void*)devID;
    upnp_data->result = (void*)result;
    
    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_BINARY_R));

    if (tb[SUBDEVID])
    {
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceID));
        json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(ST_SUB_DEV));
        json_object_object_add(jobj, ST_SUBDEVID, json_object_new_string(subdevid));

        char *subdevID = (char*)malloc(strlen(subdevid)+1);
        strcpy(subdevID, subdevid);
        upnp_data->subdevID = (void*)subdevID;

        const char *param[1];
        param[0] = subdevid;

        if(!CtrlPointProcessCommand("getdevstatus", (char*)deviceID, (char **)param, (void*)upnp_data))
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string("send success"));
        }
        else
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(result));

            free(upnp_data->result);
            free(upnp_data->deviceID);
            if(upnp_data->subdevID)
                free(upnp_data->subdevID);
            free(upnp_data);
        }
    }
    else
    {
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceID));
        json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(ST_NORMAL_DEV));

        int res = CtrlPointProcessCommand("getbinary", (char*)deviceID, NULL, (void*)upnp_data);

        if(!res)
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string("send success"));
        }
        else
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(result));

            free(upnp_data->result);
            free(upnp_data->deviceID);
            free(upnp_data);
        }
    }

    usleep(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);
    return 0;
}

static int getname(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[__MAX];
    const char *deviceID = "(unknown)";
    // const char *subdevid = "(unknown)";

    blobmsg_parse(binary_policy, ARRAY_SIZE(binary_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[ID])
        deviceID = blobmsg_data(tb[ID]);
    // if (tb[SUBDEVID])
    //     subdevid = blobmsg_data(tb[SUBDEVID]);

    upnp_cmd_data *upnp_data = (upnp_cmd_data *)malloc(sizeof(upnp_cmd_data));
    memset(upnp_data, 0x00, sizeof(upnp_cmd_data));

    char *result = (char*)malloc(512);
    memset(result, 0x00, 512);
    char *devID = (char*)malloc(strlen(deviceID)+1);
    strcpy(devID, deviceID);
    upnp_data->deviceID = (void*)devID;
    upnp_data->result = (void*)result;
    
    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_NAME_R));

    if (tb[SUBDEVID])
    {
        SAFE_FREE(upnp_data->result);
        SAFE_FREE(upnp_data->deviceID);
        SAFE_FREE(upnp_data->subdevID);
        SAFE_FREE(upnp_data);
    }
    else
    {
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceID));
        json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(ST_NORMAL_DEV));

        if(!CtrlPointProcessCommand("getname", (char*)deviceID, NULL, (void*)upnp_data))
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string("send success"));
        }
        else
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(result));

            SAFE_FREE(upnp_data->result);
            SAFE_FREE(upnp_data->deviceID);
            SAFE_FREE(upnp_data);
        }
    }

    usleep(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);
    return 0;
}

static int adddevices_callback(void *deviceinfo, int argc, char **argv, char **azColName)
{
    int i;
    for(i=0; i<argc; i++)
    {
        json_object_object_add(deviceinfo, azColName[i], json_object_new_string(argv[i] ? argv[i] : "NULL"));
    }

    SEARCH_DATA_INIT_VAR(tmp_data);
    searching_database(_VR_CB_(upnp), &tmp_data,
                        "SELECT serial from SUB_DEVICES where serial='%s'", argv[1]);
    if(!tmp_data.len)
    {
        database_actions(_VR_(upnp), "INSERT INTO %s VALUES('%s','%s','%s','%s','%s','%s')", 
                    "SUB_DEVICES (owner,serial,friendlyName,id,capability,alexa)", 
                        argv[0], argv[1], argv[2], argv[3], argv[4], "no");
    }
    else
    {
        database_actions(_VR_(upnp), "UPDATE SUB_DEVICES set owner='%s', id='%s' where serial='%s'",
                        argv[0], argv[3], argv[1]);
    }

    FREE_SEARCH_DATA_VAR(tmp_data);
    return 0;
}

static int adddevices(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[2];
    const char *deviceID = "(unknown)";
    const char *subdevid = "(unknown)";

    blobmsg_parse(add_remove_policy, ARRAY_SIZE(add_remove_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
        deviceID = blobmsg_data(tb[0]);

    if (tb[1])
        subdevid = blobmsg_data(tb[1]);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_ADD_DEVICES_R));
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceID));

    if(tb[0] && tb[1])
    {
        json_object *deviceinfo = json_object_new_object();

        searching_database(_VR_(upnp), adddevices_callback, (void *)deviceinfo,
                        "SELECT * from TMP_SUBDEVS where id='%s'", subdevid);
        
        upnp_cmd_data *upnp_data = (upnp_cmd_data *)malloc(sizeof(upnp_cmd_data));
        memset(upnp_data, 0x00, sizeof(upnp_cmd_data));

        char *result = (char*)malloc(512);
        memset(result, 0x00, 512);
        char *devID = (char*)malloc(strlen(deviceID)+1);
        char *subdevID = (char*)malloc(strlen(subdevid)+1);
        strcpy(devID, deviceID);
        strcpy(subdevID, subdevid);
        upnp_data->deviceID = (void*)devID;
        upnp_data->result = (void*)result;
        upnp_data->subdevID = (void*)subdevID;

        const char *param[1];
        param[0] = subdevid;

        if(!CtrlPointProcessCommand("addsubdev", (char*)deviceID, (char **)param, (void*)upnp_data))
        {
            json_object_object_add(jobj, ST_DEVICE_INFO, deviceinfo);
            json_object_object_add(jobj, ST_STATUS, json_object_new_string("send success"));
        }
        else
        {
            json_object_object_add(jobj, ST_DEVICE_INFO, deviceinfo);
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(result));

            free(upnp_data->result);
            free(upnp_data->deviceID);
            if(upnp_data->subdevID)
                free(upnp_data->subdevID);
            free(upnp_data);
        }
    }
    else
    {
        // //SendCommand("rediscover", NULL, NULL, NULL, NULL, result);
    }

    usleep(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);
    return 0; 
}

static int removedevice(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[2];
    const char *deviceID = "(unknown)";
    const char *subdevid = "(unknown)";

    blobmsg_parse(add_remove_policy, ARRAY_SIZE(add_remove_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
        deviceID = blobmsg_data(tb[0]);

    if (tb[1])
        subdevid = blobmsg_data(tb[1]);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_REMOVE_DEVICE_R));
    if (tb[0])
    {
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceID));
    }
    if (tb[1])
    {
        json_object_object_add(jobj, ST_SUBDEVID, json_object_new_string(subdevid));
    }

    if(tb[0] && tb[1])
    {
        upnp_cmd_data *upnp_data = (upnp_cmd_data *)malloc(sizeof(upnp_cmd_data));
        memset(upnp_data, 0x00, sizeof(upnp_cmd_data));

        char *result = (char*)malloc(512);
        memset(result, 0x00, 512);
        char *devID = (char*)malloc(strlen(deviceID)+1);
        char *subdevID = (char*)malloc(strlen(subdevid)+1);
        strcpy(devID, deviceID);
        strcpy(subdevID, subdevid);
        upnp_data->deviceID = (void*)devID;
        upnp_data->result = (void*)result;
        upnp_data->subdevID = (void*)subdevID;

        const char *param[1];
        param[0] = subdevid;

        if(!CtrlPointProcessCommand("removesubdev", (char*)deviceID, (char **)param, (void*)upnp_data))
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string("send success"));
        }
        else
        {
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(jobj, ST_REASON, json_object_new_string(result));

            free(upnp_data->result);
            free(upnp_data->deviceID);
            if(upnp_data->subdevID)
                free(upnp_data->subdevID);
            free(upnp_data);
        }
    }
    else if(tb[0])
    {
        remove_device_with_id((char *)deviceID);
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    }

    usleep(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

// static int added_callback(void *data, int argc, char **argv, char **azColName){

//     json_object *jobj = json_object_new_object();
//     json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
//     json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
//     json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string("newdevice"));
//     json_object_object_add(jobj, "realname", json_object_new_string(argv[0]));
//     json_object_object_add(jobj, "friendlyName", json_object_new_string(argv[1]));
//     json_object_object_add(jobj, "UDN", json_object_new_string(argv[2]));
//     json_object_object_add(jobj, "location", json_object_new_string(argv[3]));

//     database_actions(_VR_(upnp), "UPDATE CONTROL_DEVS SET Added='YES' "
//                             "WHERE RealName='%s' AND UDN='%s';", 
//                                 argv[0], argv[2]);

//     ithread_mutex_lock(&notify_lock);
//     strcpy(g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(jobj));
//     g_notify.notify_index=g_notify.notify_index+1;
//     ithread_mutex_unlock(&notify_lock);
//     return 0;
// }

// void adding_data(void *data)
// {
//     char sql[256];
//     char *zErrMsg = 0;
//     sprintf(sql, "SELECT *from CONTROL_DEVS where Added=='NO'");
//     int rc = sqlite3_exec(upnp_db, sql, added_callback, NULL, &zErrMsg);
//     if( rc != SQLITE_OK ){
//         fprintf(stderr, "SQL error: %s\n", zErrMsg);
//         sqlite3_free(zErrMsg);
//     }
//     else
//     {
//         printf("%s SUCCESS\n", sql);
//     }

//     // StopPluginCtrlPoint();
//     // if (init_upnp_sdk(NULL, 0)) 
//     // {
//     //     execute_system("upnp_handler");
//     // }
//     // CtrlPointProcessCommand("rediscover", NULL, NULL, NULL);
// }

static int open_closenetwork(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[1];
    const char *deviceID = "(unknown)";

    blobmsg_parse(network_policy, ARRAY_SIZE(network_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[ID])
        deviceID = blobmsg_data(tb[ID]);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
    
    if(!strcmp(method, ST_OPEN_NETWORK))
    {
        if(!strcmp(deviceID, "(unknown)"))
        {
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_OPEN_NETWORK_R));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            // pthread_t adding_thread;
            // pthread_create(&adding_thread, NULL, (void *)&adding_data, NULL);
        }
        else
        {
            upnp_cmd_data *upnp_data = (upnp_cmd_data *)malloc(sizeof(upnp_cmd_data));
            memset(upnp_data, 0x00, sizeof(upnp_cmd_data));

            char *result = (char*)malloc(512);
            char *devID = (char*)malloc(strlen(deviceID)+1);

            memset(result, 0x00, 512);

            upnp_data->deviceID = (void*)devID;
            upnp_data->result = (void*)result;

            strcpy(devID, deviceID);
            const char *param[1];
            param[0] = deviceID;

            if(!CtrlPointProcessCommand("opennetwork", (char*)deviceID, (char**)param, (void*)upnp_data))
            {
                json_object_object_add(jobj, "deviceID", json_object_new_string(deviceID));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string("send success"));
            }
            else
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string(result));

                free(upnp_data->result);
                free(upnp_data->deviceID);
                free(upnp_data);
            }
        }
    }
    else if(!strcmp(method, ST_CLOSE_NETWORK))
    {
        if(!strcmp(deviceID, "(unknown)"))
        {
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CLOSE_NETWORK_R));
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
        }
        else
        {
            upnp_cmd_data *upnp_data = (upnp_cmd_data *)malloc(sizeof(upnp_cmd_data));
            memset(upnp_data, 0x00, sizeof(upnp_cmd_data));

            char *result = (char*)malloc(512);
            char *devID = (char*)malloc(strlen(deviceID)+1);
            memset(result, 0x00, 512);

            upnp_data->deviceID = (void*)devID;
            upnp_data->result = (void*)result;

            strcpy(devID, deviceID);
            const char *param[1];
            param[0] = deviceID;

            if(!CtrlPointProcessCommand("closenetwork", (char*)deviceID, (char**)param, (void*)upnp_data))
            {
                json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceID));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string("send success"));
            }
            else
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string(result));

                free(upnp_data->result);
                free(upnp_data->deviceID);
                free(upnp_data);
            }
        }
    }
    
    usleep(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

// static int getsubdevs_callback(void *devicelist, int argc, char **argv, char **azColName)
// {
//     int i;
//     json_object * jobj = json_object_new_object();
//     for(i=0; i<argc; i++)
//     {
//         json_object_object_add(jobj, azColName[i], json_object_new_string(argv[i] ? argv[i] : "NULL"));
//     }
//     json_object_array_add(devicelist, jobj);
//     return 0;
// }

static int getsubdevs(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[2];
    const char *deviceID = "(unknown)";
    const char *listtype = "(unknown)";

    blobmsg_parse(getsub_policy, ARRAY_SIZE(getsub_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
        deviceID = blobmsg_data(tb[0]);

    if (tb[1])
        listtype = blobmsg_data(tb[1]);

    upnp_cmd_data *upnp_data = (upnp_cmd_data *)malloc(sizeof(upnp_cmd_data));
    memset(upnp_data, 0x00, sizeof(upnp_cmd_data));

    char *result = (char*)malloc(512);
    memset(result, 0x00, 512);
    char *devID = (char*)malloc(strlen(deviceID)+1);
    char *list_type = (char*)malloc(strlen(listtype)+1);
        
    strcpy(devID, deviceID);
    strcpy(list_type, listtype);
    upnp_data->deviceID = (void*)devID;
    upnp_data->result = (void*)result;
    upnp_data->subdevID = (void*)list_type;

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_SUB_DEVS_R));


    const char *param[2];
    param[0] = deviceID;
    param[1] = listtype;

    if(!CtrlPointProcessCommand("discoversubdev", (char*)deviceID, (char **)param, (void*)upnp_data))
    {
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceID));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string("send success"));
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string(result));

        free(upnp_data->result);
        free(upnp_data->deviceID);
        if(upnp_data->subdevID)
            free(upnp_data->subdevID);
        free(upnp_data);
    }


    //SendCommand("discoversubdev", deviceID, listtype, NULL, NULL, result);
    // if(strstr(result, "GETENDDEVS SUCCESS"))
    // {
    //     json_object_object_add(jobj, "deviceID", json_object_new_string(deviceID));
    //     json_object_object_add(jobj, "listtype", json_object_new_string(listtype));
    //     json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    //     json_object *devicelist = json_object_new_array();
    //     char *sql = "SELECT *from TMP_SUBDEVS";
    //     char *zErrMsg = 0;
    //     int rc = sqlite3_exec(upnp_db, sql, getsubdevs_callback, (void *)devicelist, &zErrMsg);
    //     if( rc != SQLITE_OK )
    //     {
    //       fprintf(stderr, "SQL error: %s\n", zErrMsg);
    //       strcpy(result, zErrMsg);
    //       sqlite3_free(zErrMsg);
    //     }
    //     json_object_object_add(jobj, "devicesList", devicelist);
    // }
    // else
    // {
    //     json_object_object_add(jobj, "deviceID", json_object_new_string(deviceID));
    //     json_object_object_add(jobj, "listtype", json_object_new_string(listtype));
    //     json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
    //     json_object_object_add(jobj, ST_REASON, json_object_new_string(result));
    // }

    usleep(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

command commands[] = {
    {"Address: ",                   ADDRESS},
    {"Frequency:",               FREQUENCY},
    {"Signal level=",               QUALITY},
    {"ESSID:\"",                      SSID},
    {"IE: IEEE 802.11i/",             IEEE},
    {"Pairwise Ciphers",              PAIRWISE},
    {"Group Cipher :",                GROUP_CIPHER},
    {"Authentication Suites",         AUTH},
    {"IE: WPA",                       WPA},
    {"DD07000C4303000000",      WEMO_DEVICE},
};

static int interface_exist(const char* if_name)
{
    FILE *fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    fp=popen("iw dev | grep \"Interface\" | cut -f 2 -s -d\" \"", "r");
    while ((read = getline(&line, &len, fp)) != -1)
    {
        char *pos;
        if ((pos=strchr(line, '\n')) != NULL)
            *pos = '\0';
        if(!strcmp(line, if_name))
        {
            free(line);
            pclose(fp);
            return 1;
        }
            
    }
    if(line)
        free(line);
    pclose(fp);
    return 0;
}

static void get_data_from_file(char *ch, const char* name, char* ret, int num)
{
    strncpy(ret, ch+strlen(name), num);
    char *pos;
    if ((pos=strchr(ret, '\n')) != NULL)
         *pos = '\0';
}

static int scan_wifi()
{
    FILE *fp;
    int i;
    char *line = NULL, *ch;
    char cmd[256];
    size_t len = 0;
    ssize_t read;

    int using_24Ghz = 0;
    int support_WPA1 = 0, support_WPA2 = 0;
    int already_connected = 0;
    char addr_connected[48], encryption[16], auth[32], encryption_wpa1[16], auth_wpa1[32];;
    char address[48], quality[32], wemo_ssid[128], ssid[128];

    memset(address, 0x00, sizeof(address));
    memset(quality, 0x00, sizeof(quality));
    memset(wemo_ssid, 0x00, sizeof(wemo_ssid));
    memset(ssid, 0x00, sizeof(ssid));

//read all status of wlan0 and stored in auto-config
    fp = popen("wpa_cli -i wlan0 status", "r");
    while ((read = getline(&line, &len, fp)) != -1) 
    {
        if((ch=strstr(line, "bssid=")))
        {
            get_data_from_file(ch, "bssid=", addr_connected, sizeof(addr_connected));
        }
        else if((ch=strstr(line, "freq=")))
        {
            if(!strncmp(line+strlen("freq="), "24", 2))
            {
                using_24Ghz=1;
            }
        }
        else if((ch=strstr(line, "ssid=")))
        {
            char tmp[128];
            get_data_from_file(ch, "ssid=", tmp, sizeof(tmp));
            
            if(using_24Ghz)
            {
                sprintf(cmd, "uci set auto_config.@auto-config[0].2_4GHz_ssid='%s'", tmp);
            }
            else
            {
                sprintf(cmd, "uci set auto_config.@auto-config[0].5GHz_ssid='%s'", tmp);
            }
            system(cmd);
        }
        else if((ch=strstr(line, "pairwise_cipher=")))
        {
            get_data_from_file(ch, "pairwise_cipher=", encryption, sizeof(encryption));
        }
        else if((ch=strstr(line, "group_cipher=")))
        {
            char tmp[16];
            get_data_from_file(ch, "group_cipher=", tmp, sizeof(tmp));
            if(!strcmp(encryption, tmp))
            {
                if(!strcmp(encryption,"CCMP"))
                {
                    strcpy(encryption, "AES");
                }
                if(using_24Ghz)
                {
                    sprintf(cmd, "uci set auto_config.@auto-config[0].2_4GHz_encryption='%s'", encryption);
                }
                else
                {
                    sprintf(cmd, "uci set auto_config.@auto-config[0].5GHz_encryption='%s'", encryption);
                }
                system(cmd);
            }
            else
            {
                if(!strcmp(encryption,"CCMP"))
                {
                    strcpy(encryption, "AES");
                }
                if(using_24Ghz)
                {
                    sprintf(cmd, "uci set auto_config.@auto-config[0].2_4GHz_encryption='%s%s'", tmp, encryption);
                }
                else
                {
                    sprintf(cmd, "uci set auto_config.@auto-config[0].5GHz_encryption='%s%s'", tmp, encryption);
                }
                system(cmd);
            }
        }
        else if((ch=strstr(line, "key_mgmt=")))
        {
            char tmp[128];
            strcpy(tmp, ch+strlen("key_mgmt="));
            char *pos;
            if ((pos=strchr(tmp, '\n')) != NULL)
                 *pos = '\0';

            copy_data(tmp, '-', 0, auth);
            copy_data(tmp, '-', 1, auth+strlen(auth));
            if(using_24Ghz)
            {
                sprintf(cmd, "uci set auto_config.@auto-config[0].2_4GHz_auth='%s'", auth);
            }
            else
            {
                sprintf(cmd, "uci set auto_config.@auto-config[0].5GHz_auth='%s'", auth);
            }
            system(cmd);
        }
    }
    system("uci commit auto_config");
    if(fp)
        pclose(fp);

    printf("addr_connected = %s\n", addr_connected);

    if(!interface_exist("wlan1"))
    {
        system("iw phy0 interface add wlan1 type managed");
        system("ifconfig wlan1 up");
    }

    fp=popen("iwlist wlan1 scanning | "
                "grep -e \"Address:\""
                    " -e \"Frequency:\""
                    " -e \"Signal level=\""
                    " -e \"ESSID:\""
                    " -e \"IE: IEEE 802.11i/\""
                    " -e \"Group Cipher :\""
                    " -e \"Pairwise Ciphers\""
                    " -e \"Authentication Suites\""
                    " -e \"IE: WPA\""
                    " -e \"IE: Unknown: DD\"", "r");
    while ((read = getline(&line, &len, fp)) != -1) 
    {
        for(i = 0; i<sizeof(commands)/sizeof(command); i++)
        {
            if((ch=strstr(line, commands[i].name)))
            {
                switch(commands[i].cmdnum)
                {
                    case ADDRESS:
                    {
                        if(strlen(address) || already_connected)
                        {
                            printf("already_connected = %d\n", already_connected);
                            printf("address = %s\n", address);

                            printf("support_WPA1 = %d\n", support_WPA1);
                            printf("support_WPA2 = %d\n", support_WPA2);
                            printf("using_24Ghz = %d\n", using_24Ghz);

                            printf("encryption = %s\n", encryption);
                            printf("encryption_wpa1 = %s\n", encryption_wpa1);
                            printf("auth = %s\n", auth);
                            printf("auth_wpa1 = %s\n", auth_wpa1);
                        }
                        


                        if(already_connected )
                        {
                            if(support_WPA1 || support_WPA2)
                            {
                                char cmd1[256];
                                if(using_24Ghz)
                                {
                                    strcpy(cmd, "uci set auto_config.@auto-config[0].2_4GHz_auth='");
                                    strcpy(cmd1, "uci set auto_config.@auto-config[0].2_4GHz_encryption='");
                                }
                                else
                                {
                                    strcpy(cmd, "uci set auto_config.@auto-config[0].5GHz_auth='");
                                    strcpy(cmd1, "uci set auto_config.@auto-config[0].5GHz_encryption='");
                                }
                                
                                if(support_WPA1)
                                {
                                    sprintf(cmd+strlen(cmd), "%s", auth_wpa1);
                                    sprintf(cmd1+strlen(cmd1), "%s", encryption_wpa1);
                                }
                                if(support_WPA2)
                                {
                                    sprintf(cmd+strlen(cmd), "%s", auth);
                                    if(!support_WPA1)
                                        sprintf(cmd1+strlen(cmd1), "%s", encryption);
                                }
                                strcat(cmd, "'");strcat(cmd1, "'");
                                system(cmd); system(cmd1);
                                system("uci commit auto_config");
                            }
                        }

                        if(strlen(address))
                        {
                            if(support_WPA1 || support_WPA2)
                            {
                                char cmd1[256];
                                if(using_24Ghz)
                                {
                                    strcpy(cmd, "uci set auto_config.@auto-config[0].5GHz_auth='");
                                    strcpy(cmd1, "uci set auto_config.@auto-config[0].5GHz_encryption='");
                                }
                                else
                                {
                                    strcpy(cmd, "uci set auto_config.@auto-config[0].2_4GHz_auth='");
                                    strcpy(cmd1, "uci set auto_config.@auto-config[0].2_4GHz_encryption='");
                                }

                                if(support_WPA1)
                                {
                                    sprintf(cmd+strlen(cmd), "%s", auth_wpa1);
                                    sprintf(cmd1+strlen(cmd1), "%s", encryption_wpa1);
                                }
                                if(support_WPA2)
                                {
                                    sprintf(cmd+strlen(cmd), "%s", auth);
                                    if(!support_WPA1)
                                        sprintf(cmd1+strlen(cmd1), "%s", encryption);
                                }
                                strcat(cmd, "'");strcat(cmd1, "'");
                                system(cmd); system(cmd1);
                                system("uci commit auto_config");
                            }
                        }
                        

                        already_connected = 0;
                        memset(address, 0x00, sizeof(address));
                        memset(ssid, 0x00, sizeof(ssid));
                        memset(auth, 0x00, sizeof(auth));
                        memset(encryption, 0x00, sizeof(encryption));
                        support_WPA1 = 0;
                        support_WPA2 = 0;

                        //printf("ch+strlen(commands[i].name) =%s\n", ch+strlen(commands[i].name));
                        if(!strncasecmp(addr_connected, ch+strlen(commands[i].name), 15)
                            && strncasecmp(addr_connected, ch+strlen(commands[i].name), 17))
                        {
                            strncpy(address, ch+strlen(commands[i].name), 17);
                            address[17] = '\0';
                            //printf("address = %s\n", address);
                        }
                        else if(!strncasecmp(addr_connected, ch+strlen(commands[i].name), 17))
                        {
                            already_connected = 1;
                        }
                        
                        break;
                    }

                    case FREQUENCY:
                    {
                        if(strlen(address) || already_connected)
                        {
                            //printf("line = %s\n", line);
                            char channel[8];
                            char *ch1 = strchr(line, ')');
                            char *ch2 = strstr(line, "Channel");

                            strncpy(channel, ch2+strlen("Channel")+1, ch1-1-ch2-strlen("Channel"));
                            channel[ch1 - 1 - ch2 - strlen("Channel")] = '\0';
                            if(strstr(line, "2.4"))
                            {
                                sprintf(cmd, "uci set auto_config.@auto-config[0].2_4GHz_channel='%s'", channel);
                                system(cmd);
                                system("uci commit auto_config");
                            }
                            else if(strstr(line, "5."))
                            {
                                sprintf(cmd, "uci set auto_config.@auto-config[0].5GHz_channel='%s'", channel);
                                system(cmd);
                                system("uci commit auto_config");
                            }

                            // if(already_connected)
                            //     already_connected = 0;
                        }
                        break;
                    }

                    case QUALITY:
                    {
                        strncpy(quality, ch+strlen(commands[i].name), 7);
                        quality[7] = '\0';
                        break;
                    }

                    case SSID:
                    {
                        //printf("line = %s\n", line);
                        char *ch1 = strchr(line, '\n');
                        if(strlen(address) && !strlen(ssid))
                        {
                            strncpy(ssid, ch+strlen(commands[i].name), ch1-1-ch-strlen(commands[i].name));
                            ssid[ch1 - 1 - ch - strlen(commands[i].name)] = '\0';

                            if(using_24Ghz)
                            {
                                sprintf(cmd, "uci set auto_config.@auto-config[0].5GHz_ssid='%s'", ssid);
                                system(cmd);
                                system("uci commit auto_config");
                            }
                            else
                            {
                                sprintf(cmd, "uci set auto_config.@auto-config[0].2_4GHz_ssid='%s'", ssid);
                                system(cmd);
                                system("uci commit auto_config");
                            }
                            //memset(address, 0x00, sizeof(address));
                            //memset(ssid, 0x00, sizeof(ssid));
                        }
                        
                        if(strstr(ch+strlen(commands[i].name), "WeMo"))
                        {
                            strncpy(wemo_ssid, ch+strlen(commands[i].name), ch1-1-ch-strlen(commands[i].name));
                            wemo_ssid[ch1 - 1 - ch - strlen(commands[i].name)] = '\0';
                        }
                        break;
                    }

                    case IEEE:
                    {
                        if(strlen(address) || already_connected)
                        {
                            support_WPA2 = 1;
                            //printf("line = %s\n", line);
                            memset(auth, 0x00, sizeof(auth));
                            get_data_from_file(ch, "IE: IEEE 802.11i/", auth, 4);
                            //printf("auth = %s\n", auth);
                        }
                        break;
                    }

                    case GROUP_CIPHER:
                    {
                        if(strlen(address) || already_connected)
                        {
                            //printf("line = %s\n", line);
                            if(support_WPA1)
                            {
                                memset(encryption_wpa1, 0x00, sizeof(encryption_wpa1));
                                get_data_from_file(ch, "Group Cipher : ", encryption_wpa1, sizeof(encryption_wpa1));
                                //printf("encryption_wpa1 = %s\n", encryption_wpa1);
                            }
                            else
                            {
                                memset(encryption, 0x00, sizeof(encryption));
                                get_data_from_file(ch, "Group Cipher : ", encryption, sizeof(encryption));
                                //printf("encryption = %s\n", encryption);
                            }
                        }
                        break;
                    }

                    case PAIRWISE:
                    {
                        if(strlen(address) || already_connected)
                        {
                            //printf("line = %s\n", line);
                            char tmp[128];
                            memset(tmp, 0x00, sizeof(tmp));
                            get_data_from_file(ch, "Pairwise Ciphers (*) : ", tmp, sizeof(tmp));
                            //printf("tmp = %s\n", tmp);

                            if(support_WPA1)
                            {
                                if(strcmp(encryption_wpa1,tmp))
                                {
                                    if(!strcmp(encryption_wpa1, "TKIP"))
                                    {
                                        if(strstr(tmp, "CCMP"))
                                            strcat(encryption_wpa1, "AES");
                                    }
                                    else if(!strcmp(encryption_wpa1, "CCMP"))
                                    {
                                        if(strstr(tmp, "TKIP"))
                                            strcpy(encryption_wpa1,"TKIPAES");
                                    }
                                }
                                else
                                {
                                    if(!strcmp(encryption_wpa1, "CCMP"))
                                        strcpy(encryption_wpa1,"AES");
                                }

                                //printf("encryption_wpa1 = %s\n", encryption_wpa1);
                            }
                            else
                            {
                                if(strcmp(encryption,tmp))
                                {
                                    if(!strcmp(encryption, "TKIP"))
                                    {
                                        if(strstr(tmp, "CCMP"))
                                            strcat(encryption, "AES");
                                    }
                                    else if(!strcmp(encryption, "CCMP"))
                                    {
                                        if(strstr(tmp, "TKIP"))
                                            strcpy(encryption,"TKIPAES");
                                    }
                                }
                                else
                                {
                                    if(!strcmp(encryption, "CCMP"))
                                        strcpy(encryption,"AES");
                                }

                                //printf("encryption = %s\n", encryption);
                            }
                        }
                        break;
                    }

                    case AUTH:
                    {
                        if(strlen(address) || already_connected)
                        {
                            //printf("line = %s\n", line);
                            char tmp[16];
                            memset(tmp, 0x00, sizeof(tmp));
                            get_data_from_file(ch, "Authentication Suites (*) : ", tmp, sizeof(tmp));
                            if(support_WPA1)
                            {
                                
                                strcat(auth_wpa1, tmp);
                                //printf("auth_wpa1 = %s\n", auth_wpa1);
                            }
                            else
                            {
                                strcat(auth, tmp);
                                //printf("auth = %s\n", auth);
                            }
                        }
                        break;
                    }

                    case WPA:
                    {
                        if(strlen(address) || already_connected)
                        {
                            //printf("line = %s\n", line);
                            support_WPA1 = 1;
                            if(support_WPA2)
                                strcpy(auth_wpa1, "WPA1");
                            else
                                strcpy(auth_wpa1, "WPA");
                        }
                        break;
                    }


                    case WEMO_DEVICE:
                    {
                        if(strlen(wemo_ssid))
                            database_actions(_VR_(upnp), "INSERT INTO %s VALUES('%s','%s','%s')", 
                                                "WEMO_WIFI_CONFIG (quality,ssid,config)", 
                                            quality, wemo_ssid, "NO");
                        break;
                    }
                }
            break;
            }
        }
    }
    if(line)
    {
        free(line);
    }
    pclose(fp);
    return 0;
}

void auto_config_wemo(void *data)
{
    const char* list = (const char*) data;
    printf("list = %s\n", list);
    if(!strcmp(list, "(unknown)"))
    {
        database_actions(_VR_(upnp), "DELETE from WEMO_WIFI_CONFIG;");
    }
    
    scan_wifi();

    int retry = 2;
    char *_2_4SSID = (char*)read_option("auto_config.@auto-config[0].2_4GHz_ssid");
    char *_2_4auth = (char*)read_option("auto_config.@auto-config[0].2_4GHz_auth");
    char *_2_4encryption = (char*)read_option("auto_config.@auto-config[0].2_4GHz_encryption");
    char *_2_4channel = (char*)read_option("auto_config.@auto-config[0].2_4GHz_channel");

    while( (!_2_4SSID || !_2_4auth || !_2_4encryption || !_2_4channel) && retry > 0)
    {
        scan_wifi();
        if(_2_4SSID)
        {
            free(_2_4SSID);
            _2_4SSID = (char*)read_option("auto_config.@auto-config[0].2_4GHz_ssid");
        }

        if(_2_4auth)
        {
            free(_2_4auth);
            _2_4auth = (char*)read_option("auto_config.@auto-config[0].2_4GHz_auth");
        }

        if(_2_4encryption)
        {
            free(_2_4encryption);
            _2_4encryption = (char*)read_option("auto_config.@auto-config[0].2_4GHz_encryption");
        }

        if(_2_4channel)
        {
            free(_2_4channel);
            _2_4channel = (char*)read_option("auto_config.@auto-config[0].2_4GHz_channel");
        }

        retry --;
    }

    if(retry)
    {
        SEARCH_DATA_INIT_VAR(wemo_ssid);
        searching_database(_VR_CB_(upnp), &wemo_ssid, 
                            "SELECT ssid from WEMO_WIFI_CONFIG where config='NO' LIMIT 1");
        if(wemo_ssid.len)
        {
            char cmd[256];
            execute_system("cp /etc/config/wireless /etc/config/ret_wireless");
            database_actions(_VR_(upnp), "UPDATE WEMO_WIFI_CONFIG set config='start_config' where ssid='%s'",
                                wemo_ssid.value);
            sprintf(cmd, "uci set wireless.@wifi-iface[0].ssid='%s'", wemo_ssid.value);execute_system(cmd);
            execute_system("uci set wireless.@wifi-iface[0].encryption=Open");
            execute_system("uci delete wireless.@wifi-iface[0].key");
            execute_system("uci commit wireless");
            execute_system("uci set auto_config.@auto-config[0].state=1");
            execute_system("uci commit auto_config");

            printf("#########################################################\n");
            printf("############# AUTO CONFIG: %s ###############\n", wemo_ssid.value);
            printf("#########################################################\n");
            execute_system("wifi");
        }
        else
        {
            json_object *auto_config = json_object_new_object();
            json_object_object_add(auto_config, ST_TYPE, json_object_new_string(ST_UPNP));
            json_object_object_add(auto_config, ST_METHOD, json_object_new_string(ST_NOTIFY));
            json_object_object_add(auto_config, ST_NOTIFY_TYPE, json_object_new_string(ST_AUTO_CONF));
            json_object_object_add(auto_config, ST_STATUS, json_object_new_string(ST_FAILED));
            json_object_object_add(auto_config, ST_REASON, json_object_new_string("Not found any wemo device"));

            ithread_mutex_lock(&notify_lock);
            strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(auto_config));
            g_notify.notify_index=g_notify.notify_index+1;
            ithread_mutex_unlock(&notify_lock);

            json_object_put(auto_config);
        }
        FREE_SEARCH_DATA_VAR(wemo_ssid);
    }
    else
    {
        json_object *auto_config = json_object_new_object();
        json_object_object_add(auto_config, ST_TYPE, json_object_new_string(ST_UPNP));
        json_object_object_add(auto_config, ST_METHOD, json_object_new_string(ST_NOTIFY));
        json_object_object_add(auto_config, ST_NOTIFY_TYPE, json_object_new_string(ST_AUTO_CONF));
        json_object_object_add(auto_config, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(auto_config, ST_REASON, json_object_new_string("Could not get wifi config"));

        ithread_mutex_lock(&notify_lock);
        strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(auto_config));
        g_notify.notify_index=g_notify.notify_index+1;
        ithread_mutex_unlock(&notify_lock);

        json_object_put(auto_config);
    }
    
    if(_2_4SSID)
    {
        free(_2_4SSID);
    }

    if(_2_4auth)
    {
        free(_2_4auth);
    }

    if(_2_4encryption)
    {
        free(_2_4encryption);
    }

    if(_2_4channel)
    {
        free(_2_4channel);
    }
}

static const struct blobmsg_policy autoconfig_policy[1] = {
    [0] = { .name = "ssid", .type = BLOBMSG_TYPE_STRING },
};

static int autoconfig(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[1];
    const char *ssid_dev = "(unknown)";

    blobmsg_parse(autoconfig_policy, ARRAY_SIZE(autoconfig_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
        ssid_dev = blobmsg_data(tb[0]);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_AUTO_CONF_R));

    if (tb[0])
    {
        json_object_object_add(jobj, ST_SSID, json_object_new_string(ssid_dev));
    }
        
    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_CONFIGURING));

    pthread_t auto_config_wemo_thread;
    pthread_create(&auto_config_wemo_thread, NULL, (void *)&auto_config_wemo, (void*)ssid_dev);

    usleep(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

static const struct blobmsg_policy reset_policy[0] = {
};

static int reset(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RESET_R));
        
    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

    StopPluginCtrlPoint();
    if (init_upnp_sdk(NULL, 0)) 
    {
        execute_system("upnp_handler");
    }
    CtrlPointProcessCommand("rediscover", NULL, NULL, NULL);

    usleep(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

static const struct blobmsg_policy rediscover_policy[1] = {
    [0] = { .name = ST_DEVICE_TYPE, .type = BLOBMSG_TYPE_STRING },
};


static int rediscover(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[1];
    const char *device_type = "(unknown)";

    blobmsg_parse(rediscover_policy, ARRAY_SIZE(rediscover_policy), tb, blob_data(msg), blob_len(msg));
    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_UPNP));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_REDISCOVER_R));
    

    if (tb[0])
    {
        device_type = blobmsg_data(tb[0]);
        char PDT[64];
        sprintf(PDT, "urn:Belkin:device:%s:1", device_type);
        json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(PDT));
        CtrlPointDiscoverDevices(PDT, NULL);
    }
    else
    {
        json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string("urn:Belkin:device:*"));
        CtrlPointDiscoverDevices(NULL, NULL);
    }

    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

    usleep(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

static const struct ubus_method upnp_methods[] = {
    UBUS_METHOD(ST_GET_BINARY, getbinary, binary_policy),
    UBUS_METHOD(ST_SET_BINARY, setbinary, setbinary_policy),
    UBUS_METHOD(ST_LIST_DEVICES, list_devices, null_policy),
    UBUS_METHOD(ST_ADD_DEVICES, adddevices, add_remove_policy),
    UBUS_METHOD(ST_REMOVE_DEVICE, removedevice, add_remove_policy),
    UBUS_METHOD(ST_OPEN_NETWORK, open_closenetwork, network_policy),
    UBUS_METHOD(ST_CLOSE_NETWORK, open_closenetwork, network_policy),
    UBUS_METHOD(ST_GET_SUB_DEVS, getsubdevs, getsub_policy),
    UBUS_METHOD(ST_GET_NAME, getname, binary_policy),
    UBUS_METHOD(ST_AUTO_CONF, autoconfig, autoconfig_policy),
    UBUS_METHOD(ST_RESET, reset, reset_policy),
    UBUS_METHOD(ST_REDISCOVER, rediscover, rediscover_policy),
};

static struct ubus_object_type upnp_object_type =
    UBUS_OBJECT_TYPE(ST_UPNP, upnp_methods);

static struct ubus_object upnp_object = {
    .name = ST_UPNP,
    .type = &upnp_object_type,
    .methods = upnp_methods,
    .n_methods = ARRAY_SIZE(upnp_methods),
};

static const struct blobmsg_policy network_event_policy[2] = {
    [0] = { .name = "action", .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = "interface", .type = BLOBMSG_TYPE_STRING },
};

static void retry_connect(char *wemo_ssid)
{
    char *retry = (char*)read_option("auto_config.@auto-config[0].retry");
    if(retry && !strcmp(retry,"3"))
    {
        printf("\n###################################################\n");
        printf("############# COULD NOT CONNECT TO: %s##########\n", wemo_ssid);
        printf("###################################################\n\n");
        execute_system("uci set auto_config.@auto-config[0].state=2");
        execute_system("uci set auto_config.@auto-config[0].retry=0");
        execute_system("uci commit auto_config");
        free(retry);
        execute_system("upnp_handler");
    }
    else
    {
        sleep(7);
        char cmd[256];
        sprintf(cmd, "uci set wireless.@wifi-iface[0].ssid='%s'", wemo_ssid);execute_system(cmd);
        execute_system("uci set wireless.@wifi-iface[0].encryption=Open");
        execute_system("uci delete wireless.@wifi-iface[0].key");
        execute_system("uci commit wireless");
        if(retry && !strcmp(retry,"1"))
        {
            printf("\n###################################################\n");
            printf("############# RETRY CONNECT TO: %s : 2\n", wemo_ssid);
            printf("###################################################\n\n");
            execute_system("uci set auto_config.@auto-config[0].retry=2");
        }
        else if(retry && !strcmp(retry,"2"))
        {
            printf("\n###################################################\n");
            printf("############# RETRY CONNECT TO: %s : 3\n", wemo_ssid);
            printf("###################################################\n\n");
            execute_system("uci set auto_config.@auto-config[0].retry=3");
        }
        else
        {
            printf("\n###################################################\n");
            printf("############# RETRY CONNECT TO: %s : 1\n", wemo_ssid);
            printf("###################################################\n\n");
            execute_system("uci set auto_config.@auto-config[0].retry=1");
        }
        if(retry)
        {
            free(retry);
        }
        execute_system("uci commit auto_config");
        execute_system("wifi");
    }
}

void polling_wemo_connect_status()
{
    SEARCH_DATA_INIT_VAR(wemo_ssid);
    searching_database(_VR_CB_(upnp), &wemo_ssid, 
                    "SELECT ssid from WEMO_WIFI_CONFIG where config='configuring'");

    if(wemo_ssid.len)
    {
        execute_system("uci set auto_config.@auto-config[0].retry=0");
        execute_system("uci commit auto_config");

        char *current_ssid = (char*)read_option("wireless.@wifi-iface[0].ssid");
        if(current_ssid)
        {
            printf("\n###################################################\n");
            printf("############# CURRENT CONNECTED: %s###########\n", current_ssid);
            printf("###################################################\n\n");
        }
        if(current_ssid && !strcmp(current_ssid, wemo_ssid.value))
        {
            SEARCH_DATA_INIT_VAR(UDN);
            searching_database(_VR_CB_(upnp), &UDN, 
                            "SELECT udn from WEMO_WIFI_CONFIG where config='configuring'");
            printf("\n###################################################\n");
            printf("############# START POLLING CONNECTION %s #############\n", UDN.value);
            printf("###################################################\n\n");
            while(UDN.len)
            {
                if(GetNetworkStatus(UDN.value, NULL, NULL))
                {
                    break;
                }
                sleep(3);
                UDN.len=0;
                searching_database(_VR_CB_(upnp), &UDN, 
                                "SELECT udn from WEMO_WIFI_CONFIG where config='configuring'");
            }

            FREE_SEARCH_DATA_VAR(UDN);
        }
        else
        {
            retry_connect(wemo_ssid.value);
        }
    }

    FREE_SEARCH_DATA_VAR(wemo_ssid);
}

static void receive_event(struct ubus_context *ctx, struct ubus_event_handler *ev,
              const char *type, struct blob_attr *msg)
{
    char *str;

    str = blobmsg_format_json(msg, true);
    SLOGI("{ \"%s\": %s }\n", type, str);
    free(str);

    if(!strcmp(type, "network.interface"))
    {
        struct blob_attr *tb[2];
        blobmsg_parse(network_event_policy, ARRAY_SIZE(network_event_policy), tb, blob_data(msg), blob_len(msg));
        const char *action = "unknown";
        const char *interface = "unknown";

        if(tb[0] && tb[1])
        {
            action = blobmsg_data(tb[0]);
            interface = blobmsg_data(tb[1]);

            if(!strcmp(interface, "cereswifi"))
            {
                if(!strcmp(action,"ifdown"))
                {
                    g_wemo_device_up=0;
                }
                else
                if(!strcmp(action,"ifup"))
                {
                    char *state = NULL;
                    state = (char *)read_option("auto_config.@auto-config[0].state");
                    if(state && !strcmp(state,"1"))
                    {
                        SEARCH_DATA_INIT_VAR(wemo_ssid);
                        searching_database(_VR_CB_(upnp), &wemo_ssid, 
                                        "SELECT ssid from WEMO_WIFI_CONFIG where config='start_config'");
                        if(wemo_ssid.len)
                        {
                            char *current_ssid = (char*)read_option("wireless.@wifi-iface[0].ssid");
                            if(current_ssid)
                            {
                                printf("\n###################################################\n");
                                printf("############# CURRENT CONNECTED: %s###########\n", current_ssid);
                                printf("###################################################\n\n");
                            }
                                
                            if(current_ssid && !strcmp(current_ssid, wemo_ssid.value))
                            {
                                printf("\n###################################################\n");
                                printf("############# REDISCOVER DEVICES ##################\n" );
                                printf("###################################################\n\n");
                                execute_system("uci set auto_config.@auto-config[0].retry=0");
                                execute_system("uci commit auto_config");
                                free(current_ssid);
                                StopPluginCtrlPoint();
                                if (init_upnp_sdk(NULL, 0)) 
                                {
                                    execute_system("upnp_handler");
                                }
                                g_wemo_device_found_device=1;
                                CtrlPointProcessCommand("rediscover", NULL, NULL, NULL);
                            }
                            else
                            {
                                retry_connect(wemo_ssid.value);
                            }
                        }
                        else
                        {
                            polling_wemo_connect_status();
                        }
                        FREE_SEARCH_DATA_VAR(wemo_ssid);
                        free(state);
                    }
                }
            }
        }
    }
}

static int wemo_config_return_callback(void *wemo_list, int argc, char **argv, char **azColName){
    json_object *jstring = json_object_new_string(argv[0]);
    json_object_array_add(wemo_list,jstring);
    
    return 0;
}

static int upnp_ubus_main(void)
{
    int ret;
    printf("ubus_add_object\n");
    ret = ubus_add_object(ctx, &upnp_object);
    if (ret)
        fprintf(stderr, "Failed to add object: %s\n", ubus_strerror(ret));

    memset(&ubus_listener, 0, sizeof(ubus_listener));
    ubus_listener.cb = receive_event;
    ret = ubus_register_event_handler(ctx, &ubus_listener, "network.interface");
    if (ret)
        SLOGE("Failed to register event handler: %s\n", ubus_strerror(ret));

    uloop_timeout_set(&polling_notify, 1000);

//     //Initializing UPnP SDK
//     ret = init_upnp_sdk(upnp_ip_address, upnp_port);
//     if (ret != 0) {
//         printf("init_upnp_sdk failed! Program exiting...\n");
//         return ret;
//     }

//     if(upnp_ip_address)
//     {
//         free(upnp_ip_address);
//     }
// //Starting discorvery service in this main thread
//     create_device_from_database();
    
//     if(PluginDeviceType!=NULL)
//     {
//         printf("PluginDeviceType= %s\n", PluginDeviceType);
//         ret = CtrlPointDiscoverDevices(PluginDeviceType, NULL);
//         free(PluginDeviceType);
//     }
//     else
//     {
//         ret = CtrlPointDiscoverDevices(NULL, NULL);
//     }

    /////////////////// notify auto config done ///////////////
    char *state = (char *) read_option("auto_config.@auto-config[0].state");
    if(state && !strcmp(state, "2"))
    {
        json_object *auto_config = json_object_new_object();
        json_object_object_add(auto_config, ST_TYPE, json_object_new_string(ST_UPNP));
        json_object_object_add(auto_config, ST_METHOD, json_object_new_string(ST_NOTIFY));
        json_object_object_add(auto_config, ST_NOTIFY_TYPE, json_object_new_string(ST_AUTO_CONF));
        json_object_object_add(auto_config, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
        json_object *wemo_list = json_object_new_array();

        searching_database(_VR_(upnp), wemo_config_return_callback, (void *)wemo_list,
                        "SELECT ssid from WEMO_WIFI_CONFIG where config='configured'");

        json_object_object_add(auto_config, ST_WEMO_LIST, wemo_list);

        sleep(3);//wait alljoyn on
        ithread_mutex_lock(&notify_lock);
        strcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, json_object_to_json_string(auto_config));
        g_notify.notify_index=g_notify.notify_index+1;
        ithread_mutex_unlock(&notify_lock);

        json_object_put(auto_config);

        system("uci set auto_config.@auto-config[0].state=0");
        system("uci commit auto_config");

        free(state);
    }
    printf("################# start uloop_run \n");
    uloop_run();
    return 0;
}

void Send_ubus_notify(char *data)
{
    SLOGI("msg = %s\n", data);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, data);
    
    if(ctx)
    {
        ubus_notify(ctx, &upnp_object, ST_UPNP, buff.head, 0);
    }
}

void help(void) {
    printf("WeMo Controll App version 0.4\n");
    printf("This is a discovery daemon app\n");
    printf("usage: ./wempupnpd [option] argv\n\n");
    printf("options use for this app\n");
    printf("    -c --config-file <path_of_config>\t\tChoosing config file.\n"
           "    -l --debug \t\t\t\t\tRun with more log.\n\n"
           "    -h --help  \t\t\t\t\tShow help.\n\n"    
            );
    printf("\n");
}

void signalhandler(int signum)
{
    if(signum == SIGSEGV)
    {
        printf("Sorry, program got segmentation fault, see you again\n");
        exit(1);
    }

    if(signum == SIGINT)
    {
        printf("handler SIGINT\n");
        uloop_end();
    }
    
}

int main (int argc, char ** argv)
{   
    SLOG_Init(SLOG_STDOUT, NULL);
    signal(SIGSEGV, signalhandler);
    signal(SIGINT, signalhandler);
    /*sigset_t sigset;
    sigemptyset(&sigset);
    sigaddset(&sigset, SIGINT);
    int signumber;
    sigprocmask(SIG_BLOCK, &sigset, NULL);*/
    ///

    char command[256];
    memset(command, 0x00, sizeof(command));
    sprintf(command, "%s", "pgrep upnp_handler | head -n -1 | cut -c-5 | xargs kill -9 &>/dev/null");
    system(command);

// #ifdef DAEMON_MODE
//     openlog(argv[0], LOG_PID, LOG_USER);
//     make_daemon();
// #endif

    VR_(run_command)(GET_IP_ADDRESS_COMMAND, g_hub_ip, sizeof(g_hub_ip));
    VR_(run_command)(GET_MASK_ADDRESS_COMMAND, g_subnet_mask, sizeof(g_subnet_mask));

    int i;
    ithread_mutex_init(&notify_lock, 0);

    memset(&g_notify, 0x00, sizeof(notify_queue_t));
    char *config_path = NULL;
    ssdpmsg msg;
    memset(&msg, 0x00, sizeof(msg));
    int ret=0;

    printf("######################################################\n");
    printf("############# Start check configuration ##############\n\n");
///////////////// get options //////////////////////    
    for (i = 1; i < argc; i++) {
        if (!strcmp(argv[i],"--help") || !strcmp(argv[i],"-h"))
        {
            system("clear");
            help();
            goto end;   
        }

        if (!strcmp(argv[i],"--dev-type") || !strcmp(argv[i],"-t"))
        {
            //PluginDeviceType = (char*)malloc(strlen(argv[i+1])+strlen("urn:Belkin:device:")+1);
            //strcpy(PluginDeviceType, "urn:Belkin:device:");
            //strcat(PluginDeviceType, argv[++i]);
            PluginDeviceType = (char*)malloc(strlen(argv[i+1])+1);
            strcat(PluginDeviceType, argv[++i]);
            continue;
        }

        if (!strcmp(argv[i],"--config-file") || !strcmp(argv[i],"-c"))
        {
            config_path = (char *)malloc(strlen(argv[i+1])+1);
            strcpy(config_path, argv[++i]);

            ret = Check_path_is_Dir_or_File(config_path, "File does NOT exist, need a FILE for option \"-c\" or \"--config-file\""); //0 : File, 1 : Dir
            if(ret != 0)
            {
                if(ret == 1)
                {
                    printf("Need a FILE for option \"-c\" or \"--config-file\"\n");
                }
                else
                {
                    printf("Error, program exiting\n");
                    printf("Try this command again with option \"-l\" or \"--debug\" to know what happened\n");
                }
                goto end;
            }

            FILE *tmp_fp;
            tmp_fp = fopen(config_path, "r");
            if(tmp_fp == NULL)
            {
                printf("Can not open file %s\n", config_path);
                goto end;
            }
            fclose(tmp_fp);
            continue;
        }
        if (!strcmp(argv[i],"--debug") || !strcmp(argv[i],"-l"))
        {
            debug_level = 1;
            continue;
        }
        printf("Unknown option: %s\n",argv[i]);
    }

///////////////// get environment //////////////////
    if(config_path == NULL)
    {
        char *tmp_Database_Location = getenv("UPNP_DATABASE_PATH");
        if(tmp_Database_Location != NULL && strlen(tmp_Database_Location))
        {
            Database_Location = (char *)malloc(strlen(tmp_Database_Location)+1);
            strcpy(Database_Location, tmp_Database_Location);
            printf("Got database location from environment\n");
        }
        
        config_path = (char *)malloc(strlen(path_default)+1);
        strcpy(config_path, path_default);  
    }

///////////////// read config //////////////////////
    ret = Check_path_is_Dir_or_File(config_path, ""); //0 : File, 1 : Dir
    if(ret != 0)
    {
        printf("can not open %s, using default config\n", config_path);
        if(Database_Location == NULL)
        {
            Database_Location = (char *)malloc(strlen(Database_path)+1);
            strcpy(Database_Location, Database_path);
        }
    }
    else
    {
        printf("start read config from %s\n", config_path);
        char tmp[128];
        FILE *fp;
        fp = fopen(config_path, "r");
        if(fp!=NULL)
        {
            while(!feof(fp))
            {
                fgets(tmp, 128, fp);

                if(tmp[0] == '#') {
                    continue; //skipping comments
                }
                if(strstr(tmp, "upnp_ip_address")!=NULL)
                {
                    char *ch = strstr(tmp, "=");
                    char ip[16];
                    upnp_ip_address = (char *)calloc(1,16);
                    strcpy(ip, tmp+(ch-tmp)+1);
                    strcpy(upnp_ip_address, trim(ip));
                    strcpy(msg.localip, upnp_ip_address);
                }
                if(strstr(tmp, "upnp_daemon_port")!=NULL)
                {
                    char *ch = strstr(tmp, "=");
                    upnp_port = atoi(tmp+(ch-tmp)+1);
                }
                if(Database_Location == NULL)
                {
                    if(strstr(tmp, "upnp_database_path")!=NULL)
                    {
                        char *ch = strstr(tmp, "=");
                        char link[SIZE_256B];
                        strcpy(link, tmp+(ch-tmp)+1);
                        free(Database_Location);
                        Database_Location = (char *)malloc(strlen(link)+1); //need free when catch ctrl+C
                        strcpy(Database_Location, trim(link));
                    }
                }
                if(strstr(tmp, "time_expires")!=NULL)
                {
                    char *ch = strstr(tmp, "=");
                    msg.time_expires = atoi(tmp+(ch-tmp)+1);
                }
                if(strstr(tmp, "Location")!=NULL)
                {
                    char *ch = strstr(tmp, "=");
                    char tmp_buf[SIZE_256B];
                    strcpy(tmp_buf, tmp+(ch-tmp)+1);
                    strcpy(msg.Location, trim(tmp_buf));
                }
                if(strstr(tmp, "DeviceType")!=NULL)
                {
                    char *ch = strstr(tmp, "=");
                    char tmp_buf[SIZE_256B];
                    strcpy(tmp_buf, tmp+(ch-tmp)+1);
                    strcpy(msg.DeviceType, trim(tmp_buf));
                }
                if(strstr(tmp, "DeviceID")!=NULL)
                {
                    char *ch = strstr(tmp, "=");
                    char tmp_buf[SIZE_256B];
                    strcpy(tmp_buf, tmp+(ch-tmp)+1);
                    strcpy(msg.DeviceID, trim(tmp_buf));
                }
                if(strstr(tmp, "ServiceType")!=NULL)
                {
                    char *ch = strstr(tmp, "=");
                    char tmp_buf[SIZE_256B];
                    strcpy(tmp_buf, tmp+(ch-tmp)+1);
                    strcpy(msg.ServiceType, trim(tmp_buf));
                }
            }
            fclose(fp);
        }
        else
        {
            printf("can not open %s, please check it\n", config_path);
            goto end;
        }
    }
    

    if(Database_Location == NULL)
    {
        Database_Location = (char *)malloc(strlen(Database_path)+1);
        strcpy(Database_Location, Database_path);
    }

    LOG(LOG_DBG, "config_path = %s\n", config_path);
    printf("Database_Location = %s\n", Database_Location);

    ret = Check_path_is_Dir_or_File(Database_Location, "Directory does NOT exist, need a Directory for database, "
                                                      "please check upnp_database_path variable, \nProgram exiting."); //0 : File, 1 : Dir
    if(ret != 1)
    {
        if(ret == 0)
        {
            printf("NEED a Directory for database\n");
        }
        goto end;
    }


    printf("\n####### End check configuration, Start program #######\n");
    printf("######################################################\n\n");

    if(shm_init(&g_shm, &g_shmid))
    {
        SLOGI("Failed to init share memory\n");
        return 0;
    }

//////////// start ubus service /////////////////
    const char *ubus_socket = NULL;
    int ch;

    while ((ch = getopt(argc, argv, "cs:o:")) != -1) {
        switch (ch) {
        case 's':
            ubus_socket = optarg;
            break;
        case 'o':
            MAX_DISCOVER_TIMEOUT = atoi(optarg);
            printf("MAX_DISCOVER_TIMEOUT = %d\n", MAX_DISCOVER_TIMEOUT);
            break;
        default:
            break;
        }
    }

    argc -= optind;
    argv += optind;

        //Initializing UPnP SDK
    ret = init_upnp_sdk(upnp_ip_address, upnp_port);
    if (ret != 0) {
        printf("init_upnp_sdk failed! Program exiting...\n");
        return ret;
    }

    if(upnp_ip_address)
    {
        free(upnp_ip_address);
    }
//Starting discorvery service in this main thread
    create_device_from_database();
    
    if(PluginDeviceType!=NULL)
    {
        printf("PluginDeviceType= %s\n", PluginDeviceType);
        ret = CtrlPointDiscoverDevices(PluginDeviceType, NULL);
        free(PluginDeviceType);
    }
    else
    {
        ret = CtrlPointDiscoverDevices(NULL, NULL);
    }

    uloop_init();

/*    while(1)
    {  
        LOG(LOG_DBG, "start wait sig\n");
        sigwait(&sigset, &signumber);
        LOG(LOG_DBG, "signumber = %d\n", signumber);
        if(signumber == SIGINT)
        {
            //printf("Caught SIGINT\n");
            //break;
            uloop_end();
        }
        //usleep(1000);
    }*/
    printf("connect ubus\n");

    if(ret != UPNP_E_SUCCESS)
    {
        printf("fail to discover\n");
    }

    ctx = ubus_connect(ubus_socket);
    if (!ctx) {
        fprintf(stderr, "Failed to connect to ubus\n");
        return -1;
    }
    ubus_add_uloop(ctx);
    
    if(upnp_ubus_main())
    {
       goto end; 
    }

    printf("Program Exitting\n");
    StopPluginCtrlPoint();

    ubus_free(ctx);
    uloop_done();

    if(buff.buf)
    {
        free(buff.buf);
    }

end:

    if(upnp_db)
    {
        sqlite3_close(upnp_db);
    }

    if(upnp_ip_address)
    {
        free(upnp_ip_address);
    }

    if(Database_Location)
    {
        free(Database_Location);
    }

    if(config_path)
    {
        free(config_path);
    }

    return 0;
}

