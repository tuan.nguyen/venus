#include <stdio.h>
#include <string.h>
#include "manage_adding.h"
#include "slog.h"
#include "verik_utils.h"
#include "timing.h"
#include "database.h"
#define WAITING_RESPONSE_FROM_DEVICE 5000   //5s
pthread_mutex_t zigbee_dev_adding_actions_queueMutex;
zigbee_adding_process_queue_t g_dev_adding_actions;
extern int g_open_network;
extern sqlite3 *support_devs_db;
extern sqlite3 *zigbee_db;

void init_dev_adding_actions_list()
{
    VR_INIT_LIST_HEAD(&g_dev_adding_actions.list);
    pthread_mutex_init(&zigbee_dev_adding_actions_queueMutex, 0);
}

int add_new_dev_to_list(void *node_add)
{
    int res = 0;
    pthread_mutex_lock(&zigbee_dev_adding_actions_queueMutex);
    zigbee_adding_process_queue_t *input = (zigbee_adding_process_queue_t *)node_add;
    zigbee_adding_process_queue_t *tmp = NULL;

    VR_(list_for_each_entry)
    (tmp, &(g_dev_adding_actions.list), list)
    {
        if (!strcmp(tmp->deviceId, input->deviceId))
        {
            res = 1;
        }
    }

    if (!res)
    {
        VR_(list_add_tail)
        (&(input->list), &(g_dev_adding_actions.list));
    }

    pthread_mutex_unlock(&zigbee_dev_adding_actions_queueMutex);
    return res;
}

void remove_dev_from_list(char *deviceId)
{
    pthread_mutex_lock(&zigbee_dev_adding_actions_queueMutex);
    zigbee_adding_process_queue_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)
    (pos, q, &g_dev_adding_actions.list)
    {
        tmp = VR_(list_entry)(pos, zigbee_adding_process_queue_t, list);
        if (!strcmp(tmp->deviceId, deviceId))
        {
            VR_(list_del)
            (pos);
            free(tmp);
        }
    }
    pthread_mutex_unlock(&zigbee_dev_adding_actions_queueMutex);
}

void remove_action_from_list(adding_actions_list_t *actionsList, 
    pthread_mutex_t *actionsListMutex, zigbee_command_response_t response)//ok
{
    adding_actions_list_t *tmp = NULL;
    struct VR_list_head *pos, *q;

    pthread_mutex_lock(actionsListMutex);
    VR_(list_for_each_safe)
    (pos, q, &actionsList->list)
    {
        tmp = VR_(list_entry)(pos, adding_actions_list_t, list);
        if (!strcmp(tmp->response.hash, response.hash))
        {
            SLOGI("tmp->nodeid = %s\n", tmp->response.nodeid);
            SLOGI("tmp->command_class = %s\n", tmp->response.commandCluster);
            SLOGI("tmp->command = %s\n", tmp->response.command);

            VR_(list_del)
            (pos);
            SLOGI("start free\n");
            free(tmp);
            SLOGI("end free\n");
        }
    }
    pthread_mutex_unlock(actionsListMutex);
}

void add_new_action_to_list(adding_actions_list_t *actionsList, pthread_mutex_t *actionsListMutex, 
    zigbee_command_response_t response)
{
    int added = 0;
    adding_actions_list_t *tmp = NULL;
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)
    (pos, q, &actionsList->list)
    {
        tmp = VR_(list_entry)(pos, adding_actions_list_t, list);
        if (!strcmp(tmp->response.hash, response.hash))
        {
            added = 1;
            break;
        }
    }

    if (!added)
    {
        adding_actions_list_t *actionNode = (adding_actions_list_t *)(malloc)(sizeof(adding_actions_list_t));
        memset(actionNode, 0x00, sizeof(adding_actions_list_t));

        memcpy(&actionNode->response, (void *)&response, sizeof(zigbee_command_response_t));

        pthread_mutex_lock(actionsListMutex);
        VR_(list_add_tail)
        (&(actionNode->list), &(actionsList->list));
        pthread_mutex_unlock(actionsListMutex);
    }
}

int wait_dev_response(zigbee_adding_process_queue_t *addingNode, zigbee_command_response_t response)//OK
{
    SLOGI("wait_dev_response device %s action %s-%s\n", response.nodeid, response.method, response.commandCluster);
    int res = 0;

    string_to_hash((unsigned char *)&response, sizeof(zigbee_command_response_t), response.hash, sizeof(response.hash));

    add_new_action_to_list(&addingNode->actionsList, &addingNode->actionsListMutex, response);

    int timercount = WAITING_RESPONSE_FROM_DEVICE;
    while (
        (!VR_list_empty(&addingNode->actionsList.list)) && (timercount > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
        timercount--;
    }

    if (timercount == 0)
    {
        SLOGI("device %s action %s-%s timeout\n", response.nodeid, response.method, response.commandCluster);
        res = 1;
        remove_action_from_list(&addingNode->actionsList, &addingNode->actionsListMutex, response);
    }

    return res;
}

static void adding_actions_process(char *deviceId, json_object *addingObj, int sync) //ok
{
    SLOGI("adding_actions_process %s, sync %d \n", deviceId, sync);
    if (!addingObj)
    {
        return;
    }

    enum json_type type = json_object_get_type(addingObj);
    if (type != json_type_array)
    {
        return;
    }

    int i = 0, res = 0;
    int arraylen = json_object_array_length(addingObj);
    zigbee_adding_process_queue_t *addingNode = NULL;

    if (sync) //create a dev
    {
        addingNode = (zigbee_adding_process_queue_t *)(malloc)(sizeof(zigbee_adding_process_queue_t));
        memset(addingNode, 0x00, sizeof(zigbee_adding_process_queue_t));
        strncpy(addingNode->deviceId, deviceId, sizeof(addingNode->deviceId));
        VR_INIT_LIST_HEAD(&addingNode->actionsList.list);
        pthread_mutex_init(&addingNode->actionsListMutex, 0);
        add_new_dev_to_list(addingNode); //add dev to g_dev_adding_actions
    }
    for (i = 0; i < arraylen; i++)
    {
        int action;
        json_object *jvalue;

        zigbee_command_response_t response;
        memset(&response, 0x00, sizeof(zigbee_command_response_t));

        jvalue = json_object_array_get_idx(addingObj, i);
        action = json_object_get_int(jvalue);
        printf("adding_actions_process.action %d\n", action);
        switch (action)
        {
        case GET_BINARY:
            strcpy(response.method, ST_GET_BINARY_R);
            strcpy(response.nodeid, deviceId);

            _get_binary_prepare(response);
            break;

        case SET_BINARY:
            strcpy(response.method, ST_SET_BINARY_R);
            strcpy(response.nodeid, deviceId);
            strcpy(response.command, ST_ON_OFF);
            strcpy(response.value, ST_ON_VALUE);

            _set_binary_prepare(response);
            break;

        case GET_BATTERY:
            SLOGI("GET BATTERY\n");
            strcpy(response.method, ST_READ_S_SPEC_R);
            strcpy(response.nodeid, deviceId);
            strcpy(response.commandCluster, ST_BATTERY);
            strcpy(response.command, ST_GET);

            _get_specification_prepare(response);
            break;

        case GET_TEMP:
            SLOGI("GET TEMP\n");
            strcpy(response.method, ST_READ_SPEC_R);
            strcpy(response.nodeid, deviceId);
            strcpy(response.commandCluster, ST_SENSOR_MULTILEVEL);
            strcpy(response.command, ST_GET);
            strcpy(response.data0, ST_MULTILEVEL_SENSOR_AIR_TEMPERATURE);
            _get_specification_prepare(response);
            break;

        case GET_HUMIDITY:
            SLOGI("GET HUMIDITY\n");
            strcpy(response.method, ST_READ_SPEC_R);
            strcpy(response.nodeid, deviceId);
            strcpy(response.commandCluster, ST_SENSOR_MULTILEVEL);
            strcpy(response.command, ST_GET);
            strcpy(response.data0, ST_MULTILEVEL_SENSOR_HUMIDITY);

            _get_specification_prepare(response);
            break;
        /*
        case GET_LUMINANCE:
            strcpy(response.method, ST_READ_SPEC_R);
            strcpy(response.nodeid, deviceId);
            strcpy(response.commandCluster, ST_SENSOR_MULTILEVEL);
            strcpy(response.command, ST_GET);
            strcpy(response.data0, ST_MULTILEVEL_SENSOR_LUMINANCE);

            _get_specification_prepare(response);
            break;

        case GET_UV:
            strcpy(response.method, ST_READ_SPEC_R);
            strcpy(response.nodeid, deviceId);
            strcpy(response.commandCluster, ST_SENSOR_MULTILEVEL);
            strcpy(response.command, ST_GET);
            strcpy(response.data0, ST_MULTILEVEL_SENSOR_ULTRAVIOLET);

            _get_specification_prepare(response);
            break;

        case GET_METER:
            strcpy(response.method, ST_READ_SPEC_R);
            strcpy(response.nodeid, deviceId);
            strcpy(response.commandCluster, ST_METER);
            strcpy(response.command, ST_GET);

            _get_specification_prepare(response);
            break;

        case GET_THERMOSTAT_MODE:
            SLOGI("GET_THERMOSTAT_MODE\n");
            strcpy(response.method, ST_READ_SPEC_R);
            strcpy(response.nodeid, deviceId);
            strcpy(response.commandCluster, ST_THERMOSTAT_MODE);
            strcpy(response.command, ST_GET);

            _get_specification_prepare(response);
            break;

        case GET_THERMOSTAT_SET_POINT:
            SLOGI("GET_THERMOSTAT_SET_POINT\n");
            strcpy(response.method, ST_READ_SPEC_R);
            strcpy(response.nodeid, deviceId);
            strcpy(response.commandCluster, ST_THERMOSTAT_SETPOINT);
            strcpy(response.command, ST_GET);
            strcpy(response.data0, ST_COOLING);

            _get_specification_prepare(response);
            strcpy(response.data0, ST_HEATING);
            _get_specification_prepare(response);
            break;

        case GET_THERMOSTAT_OPERATING:
            SLOGI("GET_THERMOSTAT_OPERATING\n");
            strcpy(response.method, ST_READ_SPEC_R);
            strcpy(response.nodeid, deviceId);
            strcpy(response.commandCluster, ST_THERMOSTAT_OPERATING_STATE);
            strcpy(response.command, ST_GET);

            _get_specification_prepare(response);
            break;

        case GET_THERMOSTAT_FAN_MODE:
            SLOGI("GET_THERMOSTAT_FAN_MODE\n");
            strcpy(response.method, ST_READ_SPEC_R);
            strcpy(response.nodeid, deviceId);
            strcpy(response.commandCluster, ST_THERMOSTAT_FAN_MODE);
            strcpy(response.command, ST_GET);

            _get_specification_prepare(response);
            break;

        case GET_THERMOSTAT_FAN_OPERATING:
            SLOGI("GET_THERMOSTAT_FAN_OPERATING\n");
            strcpy(response.method, ST_READ_SPEC_R);
            strcpy(response.nodeid, deviceId);
            strcpy(response.commandCluster, ST_THERMOSTAT_FAN_STATE);
            strcpy(response.command, ST_GET);

            _get_specification_prepare(response);
            break;
        */
        default:
            break;
        }

        if (sync) //get status of action 1 --> finish --> get status action 2
        {
            res = wait_dev_response(addingNode, response); //insert action to dev and remove it if timeout
            if (res)                                       //timeout
            {
                break;
            }
        }
    }

    if (sync)
    {
        remove_dev_from_list(addingNode->deviceId);
    }
}

void _remove_adding_actions(adding_actions_list_t *actionsList, pthread_mutex_t *actionsListMutex, json_object *jobj)//ok
{
    if (!jobj)
    {
        return;
    }
    /*

    json_object *commandObj = NULL;
    json_object_object_get_ex(jobj, ST_COMMAND, &commandObj);
    const char *command = json_object_get_string(commandObj);
    */
    json_object *commandObj = NULL;
    json_object *commandClassObj = NULL;
    json_object_object_get_ex(jobj, ST_CMD_CLASS, &commandClassObj);
    json_object_object_get_ex(jobj, ST_CMD, &commandObj);

    if(!commandClassObj)
    {
        return;
    }
    // char method[SIZE_32B];
    char cmd[SIZE_32B];
    const char *command = json_object_get_string(commandObj);
    const char *commandClass = json_object_get_string(commandClassObj);

    if(!strcmp(command, ST_REPORT))
    {
        strncpy(cmd, ST_GET, sizeof(cmd)-1);
    }
    adding_actions_list_t *tmp = NULL;
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)
    (pos, q, &actionsList->list)
    {
        tmp = VR_(list_entry)(pos, adding_actions_list_t, list);
        if (!strcmp(tmp->response.method, ST_GET_BINARY_R) || !strcmp(tmp->response.method, ST_SET_BINARY_R))
        {
            if(!strcmp(commandClass, ST_BASIC) 
                || !strcmp(commandClass, ST_SWITCH_ALL)
                || !strcmp(commandClass, ST_SWITCH_BINARY)
                || !strcmp(commandClass, ST_SWITCH_MULTILEVEL)
                || !strcmp(commandClass, ST_DOOR_LOCK)
                || !strcmp(commandClass, ST_BARRIER_OPERATOR)
                )
            {
                if(!strcmp(command, ST_REPORT))
                {
                    remove_action_from_list(actionsList, actionsListMutex, tmp->response);
                }
            }
        }
        else
        {
            if(!strcmp(tmp->response.commandCluster, commandClass)
                || !strcmp(tmp->response.command, cmd)
                )
            {
                remove_action_from_list(actionsList, actionsListMutex, tmp->response);
            }
        }
    }
}

void remove_dev_adding_actions(char *deviceId, json_object *jobj) //ok
{
    zigbee_adding_process_queue_t *tmp = NULL;
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)
    (pos, q, &g_dev_adding_actions.list)
    {
        tmp = VR_(list_entry)(pos, zigbee_adding_process_queue_t, list);
        if (!strcmp(tmp->deviceId, deviceId))
        {
            SLOGI("start _remove_adding_actions deviceId = %s\n", tmp->deviceId);
            _remove_adding_actions(&tmp->actionsList, &tmp->actionsListMutex, jobj);
        }
    }
}

void adding_actions_devices_prepare(char *deviceId) //ok
{
    SLOGI("adding_actions_devices_prepare %s\n", deviceId);
    if (!deviceId)
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(actions);
    zigbee_dev_info_t *dev = get_zigbee_dev_from_id(deviceId);
    if (!dev)
    {
        SEARCH_DATA_INIT_VAR(serialId);
        searching_database(_VR_CB_(zigbee), &serialId,
                           "SELECT serialId from SUB_DEVICES where id='%s'",
                           deviceId);
        if (serialId.len)
        {
            searching_database("zigbee", support_devs_db, zigbee_cb, &actions,
                               "SELECT Actions from SUPPORT_DEVS where SerialID='%s'",
                               serialId.value);
        }
        FREE_SEARCH_DATA_VAR(serialId);
    }
    else
    {
        searching_database("zigbee", support_devs_db, zigbee_cb, &actions,
                           "SELECT Actions from SUPPORT_DEVS where SerialID='%s'",
                           dev->serialId);
    }

    if (!actions.len)
    {
        goto addingDone;
    }

    json_object *actionsObject = VR_(create_json_object)(actions.value);
    if (!actionsObject)
    {
        goto addingDone;
    }

    json_object *addingObj = NULL;
    json_object *syncActionObj = NULL;
    json_object_object_get_ex(actionsObject, ST_ADDING, &addingObj);
    json_object_object_get_ex(actionsObject, ST_SYNC_ACTION, &syncActionObj);

    if (!addingObj)
    {
        json_object_put(actionsObject);
        goto addingDone;
    }

    if (syncActionObj)
    {
        const char *syncAction = json_object_get_string(syncActionObj);
        if (!strcmp(syncAction, ST_TRUE))
        {
            adding_actions_process(deviceId, addingObj, 1);
        }
        else
        {
            adding_actions_process(deviceId, addingObj, 0);
        }
    }
    else
    {
        adding_actions_process(deviceId, addingObj, 0);
    }

    json_object_put(actionsObject);

addingDone:
    SAFE_FREE(deviceId);
    FREE_SEARCH_DATA_VAR(actions);
}