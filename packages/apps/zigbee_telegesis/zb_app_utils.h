#ifndef _ZB_APP_UTILS_H_
#define _ZB_APP_UTILS_H_

#include <json-c/json.h>
#include "fix-bug-build-code.h"
#include "interface-zigbee-telegesis-handler.h"
#include "utils.h"
#include "VR_define.h"
#include "VR_list.h"

#define TEST 1
#define NON_SEC_SCHEME 255
#define SEC_0_SCHEME     7
#define SEC_2_1_SCHEME   1
#define SEC_2_2_SCHEME   2
#define SEC_2_3_SCHEME   3

#define RATE_TYPE_BIT_MASK      0x03 //BIT1 + BIT2
#define SCALE_BIT_MASK          0x07 //BIT1 + BIT2 + BIT3
#define RATE_TYPE_POSITION      0x06 //<< 6 OR >> 6
#define SCALE_POSITION          0x03 //<< 3 OR >> 3
//color define
#define RGB_COLOR               "rgb"
#define TEMP_COLOR              "kt"

#define DOORLOCK_USERCODE_DELETE   0
#define DOORLOCK_USERCODE_ADD      1

#define SWAP(x) SwapBytes(&x, sizeof(x));

typedef struct _zigbee_dev_status
{
    bool dimFirst;
    uint8_t onOff;
    uint8_t dim;
    uint8_t state; //1 alive, 0 dead
    uint32_t lastUpdate;
    uint32_t wakeUp;
    int priority;
}zigbee_dev_status_t; 

typedef struct _zigbee_dev_info
{
    uint32_t localId;//max 24bit (23-->16 endpoint), 15-->0: destAddr
    uint64_t serial;
    uint8_t deviceMode;
    uint8_t active;
    uint8_t endpointNum;
    uint8_t parentId;

    char serialId[SIZE_64B];
    char cloudId[SIZE_256B];
    char childrenId[SIZE_256B];
    char defaultSound[SIZE_256B];
    char VRDeviceType[SIZE_128B];

    char capList[SIZE_256B];

    zigbee_dev_status_t status;

    struct VR_list_head list;
    // struct _zigbee_dev_info *next;
}zigbee_dev_info_t;

typedef struct _old_dev_info
{
    char owner[SIZE_64B];
    char serial[SIZE_64B];
    char serialId[SIZE_64B];
    char deviceType[SIZE_16B];
    char friendlyName[SIZE_128B];
    char id[SIZE_16B];
    char capList[SIZE_256B];
    char profileId[SIZE_64B];
    char endpointNum[SIZE_64B];
    char parentId[SIZE_256B];
    char alexa[SIZE_16B];
    int numDevZigbee;

    // struct _zigbee_dev_info *next;
}old_dev_info_t;

typedef struct _zigbee_security_scheme
{
    const char *name;
    int scheme;
    int priority;
}zigbee_security_scheme_t; 

typedef struct zigbee_command_response
{
    char method[SIZE_64B];
    char nodeid[SIZE_64B];
    char commandCluster[SIZE_64B];
    char command[SIZE_64B];
    char value[SIZE_64B];
    char data0[SIZE_256B];//Tuan Nguyen change to char *data0
    char data1[SIZE_64B];//Tuan Nguyen change to char *data1
    char data2[SIZE_64B];//Tuan Nguyen change to char *data2
    char data3[8];
    char data4[8];
    char data5[8];
    char hash[SIZE_256B];
    char *compliantJson;

    int priority;//for sort command in queue
}zigbee_command_response_t; 

typedef struct _zigbee_adding_info
{
    char *owner;
    char *deviceName;
    char *deviceMode;//listening or non-listening
    char *active;
    char *alexa;
}zigbee_adding_info;

int RgbToHsl(uint8_t r_int, uint8_t g_int, uint8_t b_int, uint8_t *h_int, uint8_t *s_int, uint8_t *l_int); // Alpha value is simply passed through
uint32_t htoi (const char *ptr);
uint64_t htoi64 (const char *ptr);

/*for manage adding*/
zigbee_dev_info_t *get_zigbee_dev_from_id(char *id);
zigbee_dev_info_t *create_zigbee_dev_from_id(char *id);
int _get_specification_prepare(zigbee_command_response_t response);
int _get_binary_prepare(zigbee_command_response_t response);
int _set_binary_prepare(zigbee_command_response_t response);
#endif