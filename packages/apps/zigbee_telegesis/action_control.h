#ifndef _ACTION_CONTROL_H_
#define _ACTION_CONTROL_H_

#include "zb_app_utils.h"

#define MAX_COMMAND_IN_QUEUE 40
#define MAX_PRIORITY_NUM 400
#define INIT_PRIORITY_NUM 10
#define DEAD_PRIORITY_NUM 0

#define SUCCESS_PRIZE_NUM 1
#define FAILED_PRIZE_NUM 10

#define DEFAULT_DEAD_WAIT 10

enum 
{
	NO_COMMAND = 0,
	HIGH_PRIORITY = 1,
	LOW_PRIORITY = 2,
	DEAD_PRIORITY = 3,
};

typedef struct zigbee_command_queue
{
    zigbee_command_response_t response;
    ZigbeeTelegesisHandlerParam *pzbParam;
    uint8_t  command_index; // 0 -> MAX_QUEUE-1

    struct VR_list_head list;
}zigbee_command_queue_t; 

typedef struct _zigbee_queue_priority
{
    int queueId;
    zigbee_command_queue_t queue; //becareful when change name of this struct, using in _moving_action_to_low_queue
    pthread_mutex_t queueMutex;
    int weight;
    int counting;
    int wait;
}zigbee_queue_priority_t;

typedef struct _moving_action
{
    zigbee_command_queue_t *action;
    zigbee_queue_priority_t *queueInfo;
}moving_action_t; 

// init all queue in table
void init_queue_table(void);

//return queueId
int decide_queue_to_execute(void);

//update zigbee dev priority value(linklist and database)
void update_dev_priority(char *nodeId, ZigbeeTelegesisHandlerParam *pzbParam);

//insert a element to queue.
void insert_to_queue(zigbee_command_response_t response, ZigbeeTelegesisHandlerParam *pzbParam);
void insert_to_a_queue(int queueId, zigbee_command_response_t response, ZigbeeTelegesisHandlerParam *pzbParam);

//return a queueInfo pointer has queueId
zigbee_queue_priority_t *get_queue_from_queueId(int queueId);

//return first element in queue, remove all reference to queue, need call free queue element to free.
zigbee_command_queue_t *get_and_delete_first_element_in_queue(zigbee_command_queue_t *queue,
                                                            pthread_mutex_t *queueMutex);
void free_queue_element(zigbee_command_queue_t *command);

#endif