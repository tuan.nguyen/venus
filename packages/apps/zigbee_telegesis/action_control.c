#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "action_control.h"
#include "database.h"
#include "verik_utils.h"
#include "slog.h"

extern int g_open_network;
extern sqlite3 *zigbee_db;

static void add_new_element_to_queue(zigbee_command_queue_t *queue, 
                             pthread_mutex_t *queueMutex, 
                             zigbee_command_response_t response, 
                             ZigbeeTelegesisHandlerParam *pzbParam);

zigbee_queue_priority_t zigbee_queue_priority_table[]=
{
    {.queueId=HIGH_PRIORITY , .weight=10}, //highest
    {.queueId=LOW_PRIORITY , .weight=5}, //lowest
    {.queueId=DEAD_PRIORITY , .weight=1, .wait=DEFAULT_DEAD_WAIT}, //dead device
};

zigbee_queue_priority_t *end_queue = &zigbee_queue_priority_table[0] + sizeof(zigbee_queue_priority_table)/sizeof(zigbee_queue_priority_t);
//end_queue = &zigbee_queue_priority_table[3]
void init_queue_table(void)
{
    int i;
    for(i=0; i < sizeof(zigbee_queue_priority_table)/sizeof(zigbee_queue_priority_t); i++)
    {
        VR_INIT_LIST_HEAD(&zigbee_queue_priority_table[i].queue.list);/*list next, prev*/
        pthread_mutex_init(&zigbee_queue_priority_table[i].queueMutex, 0);
        zigbee_queue_priority_table[i].queue.command_index = 0;
    }
}

void update_dev_priority(char *nodeId, ZigbeeTelegesisHandlerParam *pzbParam)
{
    if(!nodeId || !pzbParam)
    {
        SLOGE("nodeId or pzbParam invalid\n");
        return;
    }

    zigbee_dev_info_t *tmp = get_zigbee_dev_from_id(nodeId);
    if(!tmp)
    {
        SLOGE("not found zigbee dev with id = %s\n", nodeId);
        return;
    }

    if(pzbParam->ret == 0)
    {
        tmp->status.priority += SUCCESS_PRIZE_NUM;
    }
    else
    {
        if(!g_open_network)
        {
            tmp->status.priority -= FAILED_PRIZE_NUM;
            if(tmp->status.priority < DEAD_PRIORITY_NUM)
            {
                tmp->status.priority = DEAD_PRIORITY_NUM;
            }
        }
    }

    char priorityStr[SIZE_32B];
    sprintf(priorityStr, "%d", tmp->status.priority);
    set_register_database(_VR_CB_(zigbee), nodeId, ST_PRIORITY, priorityStr, ST_REPLACE, 0);
}

zigbee_queue_priority_t *get_queue_from_queueId(int queueId)
{
    zigbee_queue_priority_t *tmp = &zigbee_queue_priority_table[0];
    while(tmp < end_queue)
    {
        if(tmp->queueId == queueId)
        {
            return tmp;
        }
        tmp++;
    }

    return NULL;
}

zigbee_command_queue_t *get_and_delete_first_element_in_queue(zigbee_command_queue_t *queue,
                                                            pthread_mutex_t *queueMutex)
{
    zigbee_command_queue_t *command = VR_list_entry(queue->list.next, zigbee_command_queue_t, list);

    pthread_mutex_lock(queueMutex);
    VR_(list_del)(queue->list.next);
    queue->command_index--;
    pthread_mutex_unlock(queueMutex);
    
    return command;
}

void free_queue_element(zigbee_command_queue_t *command)
{
    
    SAFE_FREE(command->response.compliantJson);
    //SAFE_FREE(command->pzbParam->misc_data);
    SAFE_FREE(command->pzbParam);
    SAFE_FREE(command);
    
}

zigbee_command_queue_t *get_first_element_in_queue(zigbee_command_queue_t *queue)
{
    return VR_list_entry(queue->list.next, zigbee_command_queue_t, list);
}

zigbee_command_queue_t *get_last_element_in_queue(zigbee_command_queue_t *queue)
{
    return VR_list_entry(queue->list.prev, zigbee_command_queue_t, list);
}

int decide_queue_to_execute(void)
{
    int i, num_queue;
    num_queue = sizeof(zigbee_queue_priority_table)/sizeof(zigbee_queue_priority_t);
    for(i=0; i < num_queue; i++)
    {
        if(zigbee_queue_priority_table[i].queue.command_index <= 0)// no command, reset counting
        {
            zigbee_queue_priority_table[i].counting = 0;
            continue;
        }

        //has command, count equal weight, reset counting
        if(zigbee_queue_priority_table[i].weight <= zigbee_queue_priority_table[i].counting)
        {
            zigbee_queue_priority_table[i].counting = 0;
            if(i == (num_queue-1))
            {
                i = 0;
            }
            continue;
        }

        if(DEAD_PRIORITY == zigbee_queue_priority_table[i].queueId)
        {
            if(zigbee_queue_priority_table[i].wait>0)
            {
                zigbee_queue_priority_table[i].wait--;
                return NO_COMMAND;
            }
        }
        else
        {
            //remove dead wait if has command form higher priority queue.
            //zigbee_queue_priority_table[num_queue-1].wait = 0;
        }

        zigbee_queue_priority_table[i].counting++;
        return zigbee_queue_priority_table[i].queueId;
    }

    zigbee_queue_priority_table[num_queue-1].wait = DEFAULT_DEAD_WAIT; //reset dead wait if nocommand found
    return NO_COMMAND;
}

int decide_queue_to_insert(zigbee_command_response_t *response)
{
    char *method = &response->method[0];
    char *nodeid = &response->nodeid[0];
    char *command = &response->command[0];

    zigbee_dev_info_t *dev = get_zigbee_dev_from_id(nodeid);
    if(!dev)
    {
        response->priority = INIT_PRIORITY_NUM;
        return HIGH_PRIORITY;//high priority
    }

    response->priority = dev->status.priority;

    if(response->priority <= DEAD_PRIORITY_NUM)
    {
        return DEAD_PRIORITY;
    }

    if(!strcmp(method, ST_SET_BINARY_R))
    {
        char *value = &response->value[0];
        int input_value = strtol(value, NULL, 0);
        
        if(!strcmp(command, ST_ON_OFF))
        {
            if(dev->status.onOff != input_value)
            {
                return HIGH_PRIORITY;
            }
            else
            {
                return LOW_PRIORITY;
            }
        }
        else
        {
            if(dev->status.dim != input_value)
            {
                return HIGH_PRIORITY;
            }
            else
            {
                return LOW_PRIORITY;
            }
        }
    }
    else
    {
        return HIGH_PRIORITY;//high priority
    }
}

void insert_to_a_queue(int queueId, zigbee_command_response_t response, ZigbeeTelegesisHandlerParam *pzbParam)
{
    SLOGI("insert to queue with Id = %d nodeId %s priority %d\n", queueId, response.nodeid, response.priority);
    int i;
    for(i=0; i < sizeof(zigbee_queue_priority_table)/sizeof(zigbee_queue_priority_t); i++)
    {
        if(zigbee_queue_priority_table[i].queueId == queueId)
        {
            add_new_element_to_queue(&zigbee_queue_priority_table[i].queue, 
                                    &zigbee_queue_priority_table[i].queueMutex, 
                                    response, 
                                    pzbParam);
        }
    }
}

void insert_to_queue(zigbee_command_response_t response, ZigbeeTelegesisHandlerParam *pzbParam)
{
    int queueId = 0, i = 0;
    string_to_hash((unsigned char *)&response, sizeof(zigbee_command_response_t), 
                    response.hash, sizeof(response.hash));

    queueId = decide_queue_to_insert(&response);
    SLOGI("insert to queue with Id = %d nodeId %s priority %d\n", queueId, response.nodeid, response.priority);

    for(i=0; i < sizeof(zigbee_queue_priority_table)/sizeof(zigbee_queue_priority_t); i++)
    {
        if(zigbee_queue_priority_table[i].queueId == queueId)
        {
            add_new_element_to_queue(&zigbee_queue_priority_table[i].queue, 
                                    &zigbee_queue_priority_table[i].queueMutex, 
                                    response, 
                                    pzbParam);
        }
    }
}

void moving_element_thread(void *data)
{
    if(!data)
    {
        SLOGE("data is NULL\n");
        return;
    }

    zigbee_command_queue_t *action = ((moving_action_t *)data)->action;
    zigbee_queue_priority_t *queueInfo = ((moving_action_t *)data)->queueInfo;
    add_new_element_to_queue(&queueInfo->queue, 
                             &queueInfo->queueMutex, 
                             action->response, 
                             action->pzbParam);
    free_queue_element(action);
    SAFE_FREE(data);
}

static void _moving_element(zigbee_command_queue_t *queue, zigbee_command_queue_t *tmp)
{
    zigbee_queue_priority_t *next_queue = VR_list_entry(queue, zigbee_queue_priority_t, queue)+1;
    if(next_queue < end_queue && next_queue->queueId)
    {
        SLOGI("insert devId %s to queueId = %d\n", tmp->response.nodeid, next_queue->queueId);
        moving_action_t *data = (moving_action_t*)malloc(sizeof(moving_action_t));
        data->action = tmp;
        data->queueInfo = next_queue;
        pthread_t moving_action_thread_t;
        pthread_create(&moving_action_thread_t, NULL, (void *)&moving_element_thread, (void *)data);
        pthread_detach(moving_action_thread_t);
    }
    else
    {
        SLOGI("remove devId %s from queueId %d\n", tmp->response.nodeid, (next_queue-1)->queueId);
        SAFE_FREE(tmp->response.compliantJson);
        SAFE_FREE(tmp->pzbParam);
        SAFE_FREE(tmp);
    }
}

static void moving_element_to_lower_queue(zigbee_command_queue_t *queue,
                                        zigbee_command_response_t response)
{
    zigbee_command_queue_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &queue->list)
    {
        tmp = VR_(list_entry)(pos, zigbee_command_queue_t, list);
        if(!strcmp(tmp->response.hash, response.hash))
        {
            VR_(list_del)(pos);
            _moving_element(queue, tmp);
            queue->command_index--;
            break;
        }
    }
}

static void _remove_queue_element(zigbee_command_queue_t *queue,
                                zigbee_command_response_t response)
{
    zigbee_command_queue_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &queue->list)
    {
        tmp = VR_(list_entry)(pos, zigbee_command_queue_t, list);
        if(!strcmp(tmp->response.hash, response.hash))
        {
            SLOGI("remove devId = %s from queue %d \n", response.nodeid, VR_list_entry(queue, zigbee_queue_priority_t, queue)->queueId);
            VR_(list_del)(pos);
            free_queue_element(tmp);
            queue->command_index--;
        }
    }
}

void remove_element_from_queue(zigbee_command_queue_t *queue, 
                             pthread_mutex_t *queueMutex, 
                             zigbee_command_response_t response)
{
    pthread_mutex_lock(queueMutex);
    _remove_queue_element(queue, response);
    pthread_mutex_unlock(queueMutex);
}

static void add_element_to_list(struct VR_list_head *new, struct VR_list_head *head, int priority)
{
    zigbee_command_queue_t *tmp = NULL;
    struct VR_list_head *pos;

    int found = 0;
    VR_list_for_each(pos, head)
    {
        tmp = VR_list_entry(pos, zigbee_command_queue_t, list);
        if(tmp->response.priority < priority)
        {
            found = 1;
            break;
        }
    }

    if(!found)
    {
        VR_list_add_tail(new, head);
    }
    else
    {
        VR_list_add_tail(new, pos);
    }
}

static void add_new_element_to_queue(zigbee_command_queue_t *queue, 
                             pthread_mutex_t *queueMutex, 
                             zigbee_command_response_t response, 
                             ZigbeeTelegesisHandlerParam *pzbParam)
{
    if(!pzbParam)
    {
        SLOGE("pzbParam not found\n");
        return;
    }

    int added = 0;
    zigbee_command_queue_t *tmp = NULL;
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &queue->list)
    {
        tmp = VR_(list_entry)(pos, zigbee_command_queue_t, list);
        if(!strcmp(tmp->response.hash, response.hash))
        {
            added = 1;
            SLOGI("COMMAND EXIST IN QUEUE\n");
            break;
        }
    }

    if(!added)
    {
        pthread_mutex_lock(queueMutex);

        zigbee_command_queue_t *action = (zigbee_command_queue_t *)malloc(sizeof(zigbee_command_queue_t));
        memset(action, 0x00, sizeof(zigbee_command_queue_t));

        action->pzbParam = (ZigbeeTelegesisHandlerParam*)malloc(sizeof(ZigbeeTelegesisHandlerParam));
        memcpy(action->pzbParam, (uint8_t*)pzbParam, sizeof(ZigbeeTelegesisHandlerParam));
        //action->pzbParam->misc_data = pzbParam->misc_data;

        memcpy(&action->response, (void*)&response, sizeof(zigbee_command_response_t));
        action->response.compliantJson = response.compliantJson;

        if(MAX_COMMAND_IN_QUEUE == queue->command_index)
        {
            SLOGI("QUEUE MAX\n");
            zigbee_command_queue_t *tmp = get_last_element_in_queue(queue);
            if(tmp->response.priority < action->response.priority)
            {
                moving_element_to_lower_queue(queue, tmp->response);//remove first insert
                add_element_to_list(&(action->list), &(queue->list), action->response.priority);
                queue->command_index++;
            }
            else
            {
                _moving_element(queue, action);
            }
        }
        else
        {
            add_element_to_list(&(action->list), &(queue->list), action->response.priority);
            queue->command_index++;
        }
        pthread_mutex_unlock(queueMutex);
    }
}

