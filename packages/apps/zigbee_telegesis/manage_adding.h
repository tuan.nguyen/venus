#ifndef _MANAGE_ADDING_H_
#define _MANAGE_ADDING_H_

#include "zb_app_utils.h"

typedef struct _adding_actions_list
{
    zigbee_command_response_t response;
    struct VR_list_head list;
} adding_actions_list_t;

typedef struct _zigbee_adding_process_queue
{
    char deviceId[SIZE_32B];
    adding_actions_list_t actionsList;
    pthread_mutex_t actionsListMutex;

    struct VR_list_head list;
} zigbee_adding_process_queue_t;

void init_dev_adding_actions_list();
void adding_actions_devices_prepare(char *deviceId);
void remove_dev_adding_actions(char *deviceId, json_object *jobj);

#endif