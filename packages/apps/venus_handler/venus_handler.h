#ifndef VENUS_HANDLER_H
#define VENUS_HANDLER_H

#include "VR_list.h"

typedef struct _dev_info
{
    char serialId[SIZE_64B];
    char cloudId[SIZE_256B];
    char deviceId[SIZE_256B];
    char deviceType[SIZE_128B];

    bool speakerAlarm;

    struct VR_list_head list;
}dev_info_t;

typedef struct venus_command_response
{
    char method[SIZE_64B];
    char deviceId[SIZE_64B];
    char command[SIZE_64B];
    char value[SIZE_64B];
    char hash[SIZE_256B];
}venus_command_response_t;

typedef struct venus_command_queue
{
    venus_command_response_t response;
    uint16_t  command_index;

    struct VR_list_head list;
}venus_command_queue_t; 

#define NOTIFY_SET_BINARY 0
#define NOTIFY_GET_BINARY 1

#define GET_MAC_ADDRESS_WITHOUT_COLON_COMMAND   "cat /sys/class/net/wlan0/address | sed 's/://g'"

#define SMART_HUB_MOTION_SERIAL_ID              "smart_hub_motion"
#define SMART_HUB_WHITE_LED_SERIAL_ID           "smart_hub_white_led"

#define SMART_HUB_MOTION_PATTERN                "_motion"
#define SMART_HUB_WHITE_LED_PATTERN             "_white_led"

#define SMART_HUB_MOTION_TYPE                   "motion"
#define SMART_HUB_WHITE_LED_TYPE                "white_led"

#define SMART_HUB_MOTION_DEFAULT_NAME           "Zinno Smart Hub Motion Sensor"
#define SMART_HUB_MOTION_DEFAULT_SOUND_FILE     "Zinno_Smart_Hub_Motion_Sensor"
#define SMART_HUB_WHITE_LED_DEFAULT_NAME        "Zinno Smart Hub White LED"


#endif