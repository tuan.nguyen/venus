#ifndef HANDLE_MOTION_H
#define HANDLE_MOTION_H

#include <linux/input.h>
#include <stdint.h>

#define EV_MAP( N ) [ N ] = #N

#define MOTION_EVENT 	KEY_2
#define POOLING_TIME  			   10000 //us

#define NOTIFY_MOTION     2

#define BUTTON_HANDLE_PATH "/dev/input/event1"

typedef struct notify
{
    uint8_t notify_type;
    uint8_t notify_message[1024];
}notify_t;

typedef struct notify_queue
{
    notify_t notify[20];
    uint8_t  notify_index;
}notify_queue_t; 

void handle_motion(void *data);

#endif