/******************************************************************************
 * Copyright (c) 2014, VEriK Systems. All rights reserved.
 *
 * Module: Service Provider
 *
 ******************************************************************************/
#include <stdio.h>
#include <unistd.h>
#include <libubus.h>
#include <libubox/blobmsg_json.h>

#include "slog.h"
#include "handle_motion.h" 
#include "VR_define.h"
#include "verik_utils.h"
#include "venus_handler.h"
#include "vr_rest.h"
#include "database.h"
#include "vr_sound.h"

#define INFORM_READY_SETUP_TIMEOUT  (30*60)
#define READY_SETUP_SOUND           "/etc/sound/system/ReadySetup"

dev_info_t g_dev_list;
venus_command_queue_t g_queue;
notify_queue_t g_notify;
pthread_mutex_t notify_lock;

int g_shmid = 0;
char *g_shm = NULL;

char g_motion_id[SIZE_128B] = {0};

static sqlite3 *dev_db;
static sqlite3 *support_devs_db;

static int g_dev_num = 0;
static struct blob_buf buff;
static struct ubus_context *ctx;
static struct ubus_event_handler ubus_listener;

static int venus_command_process_enable = 1;
int venus_motion_process_enable = 1;

static int g_sound_alarm_enable = 0;
static int g_dev_setup = 0;
static unsigned int g_last_setup_inform = 0;

static void init_dev_setup(void);

static int init_ubus_service()
{
    const char *ubus_socket = NULL;

    uloop_init();

    ctx = ubus_connect(ubus_socket);
    if (!ctx) {
        SLOGE("Failed to connect to ubus\n");
        return -1;
    }

    ubus_add_uloop(ctx);

    return 0;
}

static dev_info_t *get_dev_from_id(char *deviceId)
{
    if(!deviceId)
    {
        return NULL;
    }

    dev_info_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_dev_list.list)
    {
        tmp = VR_(list_entry)(pos, dev_info_t, list);
        if(!strcmp(tmp->deviceId, deviceId))
        {
            return tmp;
        }
    }

    return NULL;
}

static void insert_to_queue(venus_command_response_t response)
{
    string_to_hash((unsigned char *)&response, sizeof(venus_command_response_t), 
                    response.hash, sizeof(response.hash));

    int added = 0;
    venus_command_queue_t *tmp = NULL;
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &g_queue.list)
    {
        tmp = VR_(list_entry)(pos, venus_command_queue_t, list);
        if(!strcmp(tmp->response.hash, response.hash))
        {
            added = 1;
            SLOGI("COMMAND EXIST IN QUEUE\n");
            break;
        }
    }

    if(!added)
    {
        venus_command_queue_t *new_queue_element = 
                    (venus_command_queue_t *)(malloc)(sizeof(venus_command_queue_t));
        memset(new_queue_element, 0x00, sizeof(venus_command_queue_t));
        memcpy(&new_queue_element->response, (void*)&response, sizeof(venus_command_queue_t));

        VR_(list_add_tail)(&(new_queue_element->list), &(g_queue.list));
        g_queue.command_index++;
    }
}

static venus_command_queue_t *get_and_delete_first_element_in_queue(void)
{
    venus_command_queue_t *command = VR_list_entry(g_queue.list.next, venus_command_queue_t, list);
    VR_(list_del)(g_queue.list.next);
    g_queue.command_index--;
    return command;
}

static const struct blobmsg_policy setbinary_policy[3]={
    [0] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_CMD, .type = BLOBMSG_TYPE_STRING },
    [2] = { .name = ST_VALUE, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy get_policy[1]={
    [0] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy alarm_policy[]={
    [0] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_VALUE, .type = BLOBMSG_TYPE_STRING },
};

static int setbinary(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[3];
    const char *deviceId = ST_UNKNOWN;
    const char *command = ST_UNKNOWN;
    const char *value = ST_UNKNOWN;

    blobmsg_parse(setbinary_policy, ARRAY_SIZE(setbinary_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0] && tb[1] && tb[2])
    {
        deviceId = blobmsg_data(tb[0]);
        command = blobmsg_data(tb[1]);
        value = blobmsg_data(tb[2]);
    }
    else
    {
        return 0;
    }

    SLOGI("deviceId = %s\n", deviceId);
    SLOGI("command = %s\n", command);
    SLOGI("value = %s\n", value);

    venus_command_response_t response;
    memset(&response, 0x00, sizeof(venus_command_response_t));
    strcpy(response.method, ST_SET_BINARY_R);
    strcpy(response.deviceId, deviceId);
    strcpy(response.command, command);
    strcpy(response.value, value);

    insert_to_queue(response);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_VENUS_SERVICE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SET_BINARY_R));
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceId));
    json_object_object_add(jobj, ST_COMMAND, json_object_new_string(command));
    json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));

    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_RECEIVED));

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

static int getbinary(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[1];
    const char *deviceId = ST_UNKNOWN;
    blobmsg_parse(get_policy, ARRAY_SIZE(get_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
    {
        deviceId = blobmsg_data(tb[0]);
    }
    else
    {
        return 0;
    }

    SLOGI("deviceId = %s\n", deviceId);
    
    venus_command_response_t response;
    memset(&response, 0x00, sizeof(venus_command_response_t));
    strcpy(response.method, ST_GET_BINARY_R);
    strcpy(response.deviceId, deviceId);
    
    insert_to_queue(response);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_VENUS_SERVICE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_BINARY_R));
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceId));
    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_RECEIVED));

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

static int venus_alarm(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    struct blob_attr *tb[2];
    blobmsg_parse(alarm_policy, ARRAY_SIZE(alarm_policy), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_VENUS_SERVICE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_VENUS_ALARM_R));

    if(tb[0] && tb[1])
    {
        const char *id = blobmsg_data(tb[0]);
        const char *value = blobmsg_data(tb[1]);

        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(id));
        json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
        if(strcmp(id, ST_ALL))
        {
            dev_info_t *dev = get_dev_from_id((char*)id);

            if(!strcmp(value, ST_ON_VALUE))
            {
                g_sound_alarm_enable = 1;
                set_register_database("venus_handler", dev_db, get_last_data_cb, 
                                    id, ST_SOUND_ALARM_ENABLE, ST_ON_VALUE, ST_REPLACE, 0);
                if(dev)
                {
                    dev->speakerAlarm = true;
                }
            }
            else if(!strcmp(value, ST_OFF_VALUE))
            {
                set_register_database("venus_handler", dev_db, get_last_data_cb, 
                                    id, ST_SOUND_ALARM_ENABLE, ST_OFF_VALUE, ST_REPLACE, 0);
                VR_(cancel_alarm)(ctx, (char*)id);
                if(dev)
                {
                    dev->speakerAlarm = false;
                }
            }
        }
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    }    
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_MISSING_ARGUMENT));
    }

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj)); 
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

static const struct ubus_method venus_methods[] = {
    UBUS_METHOD(ST_SET_BINARY, setbinary, setbinary_policy),
    UBUS_METHOD(ST_GET_BINARY, getbinary, get_policy),
    UBUS_METHOD(ST_VENUS_ALARM, venus_alarm, alarm_policy),
};

static struct ubus_object_type venus_object_type =
    UBUS_OBJECT_TYPE(ST_VENUS_SERVICE, venus_methods);

static struct ubus_object venus_object = {
    //.subscribe_cb = test_client_subscribe_cb,
    .name = ST_VENUS_SERVICE,
    .type = &venus_object_type,
    .methods = venus_methods,
    .n_methods = ARRAY_SIZE(venus_methods),
};

void Send_ubus_notify(char *data)
{
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, data);
    
    if(ctx)
    {
        ubus_notify(ctx, &venus_object, ST_VENUS_SERVICE, buff.head, -1);
    }
}

static void notify_active_cb(struct uloop_timeout *timeout)
{
    if(g_notify.notify_index>0)
    {
        if(NOTIFY_MOTION == g_notify.notify[0].notify_type)
        {
            if(!strlen(g_motion_id))
            {
                SEARCH_DATA_INIT_VAR(motion_id);
                searching_database("venus_handler", dev_db, get_last_data_cb, 
                            &motion_id, "SELECT %s from SUB_DEVICES where %s='%s' and %s='%s'", 
                            ST_ID, ST_OWNER, ST_VENUS_SERVICE, ST_SERIAL_ID, SMART_HUB_MOTION_SERIAL_ID);

                if(motion_id.len)
                {
                    strncpy(g_motion_id, motion_id.value, sizeof(g_motion_id)-1);
                }
                else
                {
                    FREE_SEARCH_DATA_VAR(motion_id);
                    goto next;
                }
                FREE_SEARCH_DATA_VAR(motion_id);
            }

            uint8_t res = g_notify.notify[0].notify_message[0];

            json_object *jobj = json_object_new_object();
            json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_VENUS_SERVICE));
            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(g_motion_id));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CMD_CLASS_ZPC));
            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SENSOR_ALARM));
            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));

            if(res == 1)
            {
                json_object_object_add(jobj, ST_ALARM_TYPE_FINAL, json_object_new_string(ST_MOTION_DETECTED));
                Send_ubus_notify((char*)json_object_to_json_string(jobj));
            }
            else if(res == 0)
            {
                json_object_object_add(jobj, ST_ALARM_TYPE_FINAL, json_object_new_string(ST_MOTION_CLEARED));
                Send_ubus_notify((char*)json_object_to_json_string(jobj));
            }

            /*dev is not setup yet*/
            if(res == 1 && !g_dev_setup)
            {
                unsigned int uptime = get_system_up_time();
                unsigned int tmp = uptime/INFORM_READY_SETUP_TIMEOUT;
                if(tmp > g_last_setup_inform)
                {
                    g_last_setup_inform = tmp;
                    char *lastInformStr = VR_(read_option)("/tmp", "inform.@setup[0].lastInform");
                    if(lastInformStr)
                    {
                        unsigned int lastInform = strtol(lastInformStr, NULL, 10);
                        unsigned int currentTime = (unsigned)time(NULL);
                        if((currentTime - lastInform) > INFORM_READY_SETUP_TIMEOUT)
                        {
                            insert_alarm_command(g_motion_id, 1, 0, READY_SETUP_SOUND);
                        }
                    }
                    else
                    {
                        insert_alarm_command(g_motion_id, 1, 0, READY_SETUP_SOUND);
                    }

                    SAFE_FREE(lastInformStr);
                }
                json_object_put(jobj);
                goto next;
            }

            if(res == 1 && g_sound_alarm_enable)
            {
                dev_info_t *dev = get_dev_from_id((char*)g_motion_id);
                if(!dev)
                {
                    json_object_put(jobj);
                    goto next;
                }

                char path[SIZE_512B] = {0};
                int delayTime = 0;
                int repeat = create_link(dev->speakerAlarm, ST_MOTION_DETECTED, g_motion_id, ST_VENUS_SERVICE,
                                        SMART_HUB_MOTION_DEFAULT_SOUND_FILE, path, sizeof(path), &delayTime, NULL);

                if(strlen(path) && (repeat >= 0))
                {
                    insert_alarm_command(g_motion_id, repeat, delayTime, path);
                }
                /*because a process could not send a event to itself*/
                // VR_(alarm)((void *)ctx, dev->speakerAlarm, (char *)ST_MOTION_DETECTED,g_motion_id,
                //             ST_VENUS_SERVICE, SMART_HUB_MOTION_DEFAULT_SOUND_FILE);
            }

            json_object_put(jobj);
        }
        else
        {
            Send_ubus_notify((char*)g_notify.notify[0].notify_message);
        }
next:
        pthread_mutex_lock(&notify_lock);
        g_notify.notify_index--;
        memcpy((uint8_t*) &g_notify.notify[0],(uint8_t*)&g_notify.notify[1],g_notify.notify_index*sizeof(notify_t));
        pthread_mutex_unlock(&notify_lock);
    }

    uloop_timeout_set(timeout, 100);

    return;
}

static struct uloop_timeout polling_notify = {
    .cb = notify_active_cb,
};

void handle_led_sound(void *data)
{
    if(!data)
    {
        SLOGE("data invalid\n");
        return;
    }

    char *msg = (char *)data;
    json_object *jobj = VR_(create_json_object)(msg);
    if(!jobj)
    {
        free(msg);
        return;
    }

    CHECK_JSON_OBJECT_EXIST(typeObj, jobj, ST_TYPE, done);
    const char *type = json_object_get_string(typeObj);
    if(!strcmp(type, ST_SOUND))
    {
        CHECK_JSON_OBJECT_EXIST(deviceIdObj, jobj, ST_DEVICE_ID, done);
        CHECK_JSON_OBJECT_EXIST(cmdObj, jobj, ST_CMD, done);
        const char *deviceId = json_object_get_string(deviceIdObj);
        int cmd = json_object_get_int(cmdObj);

        if(PLAY_SOUND_CMD == cmd)
        {
            CHECK_JSON_OBJECT_EXIST(repeatObj, jobj, ST_REPEAT, done);
            CHECK_JSON_OBJECT_EXIST(delayTimeObj, jobj, ST_DELAY_TIME, done);
            CHECK_JSON_OBJECT_EXIST(fileObj, jobj, ST_FILE, done);

            int repeat = json_object_get_int(repeatObj);
            int delayTime = json_object_get_int(delayTimeObj);
            const char *file = json_object_get_string(fileObj);
            insert_alarm_command((char *)deviceId, repeat, delayTime, (char *)file);
        }
        else if(STOP_SOUND_CMD == cmd)
        {
            cancel_alarm_thread((char *)deviceId);
        }
        else if(TEMPORARY_DISABLE_SOUND_CMD == cmd)
        {
            CHECK_JSON_OBJECT_EXIST(disableTimeObj, jobj, ST_DISABLE_TIME, done);
            int disableTime = json_object_get_int(disableTimeObj);
            temporary_disable_alarm_thread((char *)deviceId, disableTime);
        }
        else if(DISABLE_ALL_SOUND_CMD == cmd)
        {
            disable_all_alarms_thread();
        }
    }

done:
    json_object_put(jobj);
    free(msg);
}

static void receive_event(struct ubus_context *ctx, struct ubus_event_handler *ev,
              const char *type, struct blob_attr *msg)
{
    if(!msg)
    {
        return;
    }

    if(!strcmp(type, "onboarding") || !strcmp(type, ST_PUBSUB))
    {
        char *msgstr = blobmsg_format_json(msg, true);
        json_object *jobj = VR_(create_json_object)(msgstr);
        if(!jobj)
        {
            SAFE_FREE(msgstr);
            return;
        }

        json_object *stateObj = NULL;
        json_object_object_get_ex(jobj, ST_STATE, &stateObj);
        if(!stateObj)
        {
            json_object_put(jobj);
            SAFE_FREE(msgstr);
            return;
        }

        const char *state = json_object_get_string(stateObj);
        if(!strcmp(state, "validating_failed"))
        {
            g_dev_setup = 0;
        }
        else if(!strcmp(state, "connected"))
        {
            init_dev_setup();
        }
        else if(!strcmp(state, "configured"))
        {
            g_dev_setup = 1;
        }
        else if(!strcmp(state, "claimed"))
        {
            g_dev_setup = 1;
        }
        else if(!strcmp(state, "registered"))
        {
            g_dev_setup = 1;
        }
        else if(!strcmp(state, "register_failed"))
        {
            g_dev_setup = 1;
        }

        json_object_put(jobj);
        SAFE_FREE(msgstr);
    }
    else if(!strcmp(type, ST_LED_AND_SOUND))
    {
        char *msgstr = blobmsg_format_json(msg, true);
        SLOGI("msgstr = %s\n", msgstr);

        pthread_t handle_led_sound_thread;
        pthread_create(&handle_led_sound_thread, NULL, (void *)&handle_led_sound, (void *)msgstr);
        pthread_detach(handle_led_sound_thread);
    }

    return;
}

static void venus_ubus_service()
{
    int ret;
    //uint32_t id;

    ret = ubus_add_object(ctx, &venus_object);
    if (ret) {
        SLOGE("Failed to add_object object: %s\n", ubus_strerror(ret));
        return;
    }

    memset(&ubus_listener, 0, sizeof(ubus_listener));
    ubus_listener.cb = receive_event;
    ret = ubus_register_event_handler(ctx, &ubus_listener, ST_LED_AND_SOUND);
    if (ret)
    {
        SLOGE("Failed to register event handler %s: %s\n", ST_LED_AND_SOUND, ubus_strerror(ret));
    }

    ret = ubus_register_event_handler(ctx, &ubus_listener, "onboarding");
    if (ret)
    {
        SLOGE("Failed to register event handler onboarding %s\n", ubus_strerror(ret));
    }

    ret = ubus_register_event_handler(ctx, &ubus_listener, ST_PUBSUB);
    if (ret)
    {
        SLOGE("Failed to register event handler %s %s\n", ST_PUBSUB, ubus_strerror(ret));
    }

    uloop_timeout_set(&polling_notify, 1000);

    uloop_run();
}

static void free_ubus_service()
{
    if(ctx)
    {
        ubus_free(ctx);
    }
    uloop_done();
}

int add_last_list(void *node_add)
{
    int res = 0;
    dev_info_t *input = (dev_info_t *)node_add;
    dev_info_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &(g_dev_list.list), list)
    {
        if(!strcmp(tmp->deviceId, input->deviceId))
        {
            res = 1;
            break;
        }
    }

    if(!res)
    {
        VR_(list_add_tail)(&(input->list), &(g_dev_list.list));
        g_dev_num++;
    }

    return res;
}

static int _create_dev(void *dev)
{
    if(!dev)
    {
        return 1;
    }

    dev_info_t *new_dev = (dev_info_t *)dev;

    int res = add_last_list(new_dev);
    if(res)
    {
        free(new_dev);
    }

    return res;
}

static int init_dev_info_db_callback(void *data, int argc, char **argv, char **azColName)
{
    if(argc < 3)
    {
        return 0;
    }

    int i;
    char *serialId = NULL;
    char *deviceType = NULL;
    char *deviceId = NULL;

    for(i=0; i<argc;i++)
    {
        // printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
        if(!strcmp(azColName[i], ST_SERIAL_ID))
        {
            serialId = argv[i];
        }
        else if(!strcmp(azColName[i], ST_DEVICE_TYPE))
        {
            deviceType = argv[i];
        }
        else if(!strcmp(azColName[i], ST_ID))
        {
            deviceId = argv[i];
        }
    }

    dev_info_t *new_dev = (dev_info_t *)(malloc)(sizeof(dev_info_t));
    memset(new_dev, 0x00, sizeof(dev_info_t));

    strncpy(new_dev->deviceId, deviceId, sizeof(new_dev->deviceId)-1);
    strncpy(new_dev->serialId, serialId, sizeof(new_dev->serialId)-1);
    strncpy(new_dev->deviceType, deviceType, sizeof(new_dev->deviceType)-1);
    new_dev->speakerAlarm = false;

    if(!strcmp(deviceType,SMART_HUB_MOTION_TYPE))
    {
        strncpy(g_motion_id, deviceId, sizeof(g_motion_id)-1);

        SEARCH_DATA_INIT_VAR(sound_alarm_enable);
        searching_database("venus_handler", dev_db, get_last_data_cb, &sound_alarm_enable,
                            "SELECT register from FEATURES where deviceId='%s' and featureId='%s'",
                            deviceId, ST_SOUND_ALARM_ENABLE);
        if(!sound_alarm_enable.len)
        {
            new_dev->speakerAlarm = false;
            set_register_database("venus_handler", dev_db, get_last_data_cb,
                            g_motion_id, ST_SOUND_ALARM_ENABLE, ST_OFF_VALUE, ST_REPLACE, 0);
        }
        else if(!strcmp(sound_alarm_enable.value, ST_ON_VALUE))
        {
            new_dev->speakerAlarm = true;
        }

        FREE_SEARCH_DATA_VAR(sound_alarm_enable);
    }

    _create_dev((void*)new_dev);
    return 0;
}

static void get_friendly_name(char *serialId, char *nameReturn, size_t length, const char *defaultName)
{
    SEARCH_DATA_INIT_VAR(dbAction);
    searching_database("venus_handler", support_devs_db, get_last_data_cb, 
                    &dbAction, "SELECT %s from SUPPORT_DEVS where SerialID='%s'", 
                    ST_FRIENDLY_NAME, serialId);
    if(dbAction.len)
    {
        strncpy(nameReturn, dbAction.value, length-1);
    }
    else
    {
        strncpy(nameReturn, defaultName, length-1);
    }

    FREE_SEARCH_DATA_VAR(dbAction);
}

static void post_resources_to_cloud(char *Owner, char *Serial, char *SerialID, char *DeviceType,
                                    char *FriendlyName, char *ID, char *capabilityJsonStr, 
                                    char **cloudId)
{
    char resource_id[SIZE_256B];
    uuid_make(resource_id, sizeof(resource_id));

    json_object *json_post_resources = json_object_new_object();
    json_object *resources = json_object_new_object();
    json_object *CapabilityObj = VR_(create_json_object)(capabilityJsonStr);
    JSON_ADD_STRING_SAFE(resources, ST_OWNER, Owner);
    JSON_ADD_STRING_SAFE(resources, ST_SERIAL, Serial);
    JSON_ADD_STRING_SAFE(resources, ST_SERIAL_ID, SerialID);
    JSON_ADD_STRING_SAFE(resources, ST_DEVICE_TYPE, DeviceType);
    JSON_ADD_STRING_SAFE(resources, ST_FRIENDLY_NAME, FriendlyName);
    JSON_ADD_STRING_SAFE(resources, ST_ID, ID);
    if(CapabilityObj)
    {
        json_object_object_add(resources, ST_CAPABILITY, CapabilityObj);
    }
    JSON_ADD_STRING_SAFE(resources, ST_TYPE, ST_VENUS_SERVICE);

    json_object_object_add(json_post_resources, ST_CLOUD_RESOURCE, resources);
    json_object_object_add(json_post_resources, ST_ID, json_object_new_string(resource_id));
    JSON_ADD_STRING_SAFE(json_post_resources, ST_CLOUD_LOCAL_ID, ID);
    JSON_ADD_STRING_SAFE(json_post_resources, ST_CLOUD_SERIAL_ID, SerialID);
    JSON_ADD_STRING_SAFE(json_post_resources, ST_NAME, FriendlyName);

    const char *data = json_object_to_json_string(json_post_resources);

    int res = VR_(post_resources)((char *)data, resource_id, sizeof(resource_id), RESOURCES_TIMEOUT);
    if(res > 0)
    {
        inform_pubsub_post_failed(ID, ST_VENUS_SERVICE, (char *)data);
    }

    update_resource_id("venus_handler", dev_db, get_last_data_cb, ID, resource_id);
    shm_update_data(g_shm, resource_id, ID, ST_VENUS_SERVICE, SHM_ADD);

    if(cloudId)
    {
        *cloudId = strdup(resource_id);
    }

    json_object_put(json_post_resources);
}

static void insert_db_and_port_to_cloud(char *serialId, char *deviceId, char *deviceName, char *deviceType)
{
    if(!serialId || !deviceId || !deviceName || !deviceType)
    {
        return;
    }

    int res = database_actions("venus_handler", dev_db, "INSERT INTO SUB_DEVICES"
                    "('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')"
                    "VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                    ST_OWNER, ST_SERIAL, ST_SERIAL_ID, ST_DEVICE_TYPE, ST_FRIENDLY_NAME, ST_ID, ST_PARENT_ID, 
                    ST_ACTIVE, ST_ALEXA, ST_TYPE,
                    ST_VENUS_SERVICE, deviceId, serialId, deviceType, deviceName, deviceId, "0", ST_YES, ST_NO, ST_VENUS_SERVICE);
    if(res)
    {
        SLOGE("failed to insert database\n");
        return;
    }

    char *cloudId = NULL;
    if(VR_(is_wireless_up)(ST_STA_MODE))
    {
        post_resources_to_cloud(ST_VENUS_SERVICE, deviceId, serialId, deviceType,
                                deviceName, deviceId, NULL, &cloudId);
    }

    dev_info_t *new_dev = (dev_info_t *)(malloc)(sizeof(dev_info_t));
    memset(new_dev, 0x00, sizeof(dev_info_t));

    strncpy(new_dev->deviceId, deviceId, sizeof(new_dev->deviceId)-1);
    strncpy(new_dev->serialId, serialId, sizeof(new_dev->serialId)-1);
    strncpy(new_dev->deviceType, deviceType, sizeof(new_dev->deviceType)-1);

    new_dev->speakerAlarm = true;

    if(cloudId)
    {
        strncpy(new_dev->cloudId, cloudId, sizeof(new_dev->cloudId)-1);
    }

    _create_dev((void*)new_dev);
    SAFE_FREE(cloudId);
}

static void insert_smart_device_to_db(void)
{
    char motion_id[SIZE_128B];
    char white_led_id[SIZE_128B];
    char motion_name[SIZE_128B];
    char white_led_name[SIZE_128B];

    char mac_address[SIZE_32B] = {0};
    VR_(run_command)(GET_MAC_ADDRESS_WITHOUT_COLON_COMMAND, mac_address, sizeof(mac_address));

    snprintf(motion_id, sizeof(motion_id), "%s%s", mac_address, SMART_HUB_MOTION_PATTERN);
    snprintf(white_led_id, sizeof(white_led_id), "%s%s", mac_address, SMART_HUB_WHITE_LED_PATTERN);

    strncpy(g_motion_id, motion_id, sizeof(g_motion_id)-1);

    get_friendly_name(SMART_HUB_MOTION_SERIAL_ID, motion_name, sizeof(motion_name), SMART_HUB_MOTION_DEFAULT_NAME);
    get_friendly_name(SMART_HUB_WHITE_LED_SERIAL_ID, white_led_name, sizeof(white_led_name), SMART_HUB_WHITE_LED_DEFAULT_NAME);

    insert_db_and_port_to_cloud(SMART_HUB_MOTION_SERIAL_ID, motion_id, motion_name, SMART_HUB_MOTION_TYPE);
    set_register_database("venus_handler", dev_db, get_last_data_cb, motion_id, ST_ALARM, ST_MOTION_CLEARED, ST_REPLACE, 0);
    set_register_database("venus_handler", dev_db, get_last_data_cb, motion_id, ST_SOUND_ALARM_ENABLE, ST_OFF_VALUE, ST_REPLACE, 0);

    insert_db_and_port_to_cloud(SMART_HUB_WHITE_LED_SERIAL_ID, white_led_id, white_led_name, SMART_HUB_WHITE_LED_TYPE);
    set_register_database("venus_handler", dev_db, get_last_data_cb, white_led_id, ST_ON_OFF, "0", ST_REPLACE, 0);
}

void init_dev_list()
{
    VR_INIT_LIST_HEAD(&g_dev_list.list);

    searching_database("venus_handler", dev_db, init_dev_info_db_callback, NULL,
                        "SELECT serialId,deviceType,id from SUB_DEVICES where type='venus'");
    if(!g_dev_num)
    {
        insert_smart_device_to_db();
    }
}

void venus_command_process(void *data)
{
    while(venus_command_process_enable)
    {
        if(g_queue.command_index <= 0)
        {
            usleep(1000);
            continue;
        }

        venus_command_queue_t *commandInQ = get_and_delete_first_element_in_queue();
        if(!commandInQ)
        {
            SLOGW("commandInQ not found\n");
            usleep(1000);
            continue;
        }

        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_VENUS_SERVICE));

        char *command = commandInQ->response.command;
        char *value = commandInQ->response.value;
        char *deviceId = commandInQ->response.deviceId;
        char *method = commandInQ->response.method;

        if(!strcmp(method, ST_SET_BINARY_R))
        {
            JSON_ADD_STRING_SAFE(jobj, ST_METHOD, method);
            JSON_ADD_STRING_SAFE(jobj, ST_DEVICE_ID, deviceId);
            JSON_ADD_STRING_SAFE(jobj, ST_COMMAND, command);
            JSON_ADD_STRING_SAFE(jobj, ST_VALUE, value);

            if(strstr(deviceId, ST_VOLUME))
            {
                int volume = strtol(value, NULL, 10);
                if(VR_(set_volume)(volume))
                {
                    inform_volume_change();
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                }
                else
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                }
            }
            else if(strstr(deviceId, ST_LED_CURRENT))
            {
                int current = strtol(value, NULL, 10);
                set_led_current((unsigned int) current);
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }
            else
            {
                dev_info_t *dev = get_dev_from_id((char*)deviceId);
                if(!dev)
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    goto done;
                }

                if(strcmp(dev->deviceType, SMART_HUB_WHITE_LED_TYPE))
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    goto done;
                }

                if(!strcmp(value, "0"))
                {
                    VR_(execute_system)("/etc/led_control.sh White_leds_off");
                }
                else
                {
                    VR_(execute_system)("/etc/led_control.sh White_leds_on");
                }

                set_register_database("venus_handler", dev_db, get_last_data_cb, deviceId, ST_ON_OFF, value, ST_REPLACE, 0);
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }

            const char *message = json_object_to_json_string(jobj);
            pthread_mutex_lock(&notify_lock);
            g_notify.notify[g_notify.notify_index].notify_type= NOTIFY_SET_BINARY;
            memcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, message, strlen(message)+1);
            g_notify.notify_index=g_notify.notify_index+1;
            pthread_mutex_unlock(&notify_lock);
        }
        else if(!strcmp(method, ST_GET_BINARY_R))
        {
            JSON_ADD_STRING_SAFE(jobj, ST_METHOD, method);
            JSON_ADD_STRING_SAFE(jobj, ST_DEVICE_ID, deviceId);

            if(strstr(deviceId, ST_VOLUME))
            {
                int volume = VR_(get_volume)();
                json_object_object_add(jobj, ST_VALUE, json_object_new_int(volume));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }
            else
            {
                dev_info_t *dev = get_dev_from_id((char*)deviceId);
                if(!dev)
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    goto done;
                }

                if(strcmp(dev->deviceType, SMART_HUB_WHITE_LED_TYPE))
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    goto done;
                }

                char white_leds[SIZE_32B];
                VR_(run_command)(GET_WHITE_LED_COMMAND, white_leds, sizeof(white_leds));
                set_register_database("venus_handler", dev_db, get_last_data_cb, deviceId, ST_ON_OFF, white_leds, ST_REPLACE, 0);
                json_object_object_add(jobj, ST_VALUE, json_object_new_string(white_leds));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }

            const char *message = json_object_to_json_string(jobj);
            pthread_mutex_lock(&notify_lock);
            g_notify.notify[g_notify.notify_index].notify_type= NOTIFY_GET_BINARY;
            memcpy((char*)g_notify.notify[g_notify.notify_index].notify_message, message, strlen(message)+1);
            g_notify.notify_index=g_notify.notify_index+1;
            pthread_mutex_unlock(&notify_lock);
        }
done:
        SAFE_FREE(commandInQ);
        json_object_put(jobj);
        usleep(2000);
    }
}

static void init_dev_setup(void)
{
    char *devState = VR_(read_option)(NULL, UCI_LOCAL_STATE);
    if(devState)
    {
        if(strcmp(devState, "available"))
        {
            g_dev_setup = 1;
        }
    }

    SAFE_FREE(devState);
}

int main()
{
    SLOG_Init(SLOG_STDOUT, NULL);
    set_thread_name( "venus_service" );

    char cmd[256];
    memset(cmd, 0x00, sizeof(cmd));
    sprintf(cmd, "%s", "pgrep venus_handler | head -n -1 | cut -c-5 | xargs kill -9 &>/dev/null");
    system(cmd);

    open_database(DEVICES_DATABASE, &dev_db);
    open_database(SUPPORTED_DEVS_DATABASE, &support_devs_db);

    if(shm_init(&g_shm, &g_shmid))
    {
        SLOGI("Failed to init share memory\n");
        return 0;
    }

    pthread_mutex_init(&notify_lock, 0);
    memset(&g_notify, 0x00, sizeof(notify_queue_t));

    init_dev_setup();
    init_dev_list();
    init_sound_manage_list();

    VR_INIT_LIST_HEAD(&g_queue.list);
    g_queue.command_index=0;

    pthread_t venus_command_process_thread;
    pthread_create(&venus_command_process_thread, NULL, (void *)&venus_command_process, NULL);
    pthread_detach(venus_command_process_thread);

    pthread_t handle_motion_thread;
    pthread_create(&handle_motion_thread, NULL, (void *)&handle_motion, NULL);
    pthread_detach(handle_motion_thread);

    pthread_t handle_alarm_command;
    pthread_create(&handle_alarm_command, NULL, (void *)&handle_command_sound, NULL);
    pthread_detach(handle_alarm_command);

    pthread_t play_sound_thread;
    pthread_create(&play_sound_thread, NULL, (void *)&play_sound, NULL);
    pthread_detach(play_sound_thread);

    init_ubus_service();
    venus_ubus_service();
    free_ubus_service();
    return 0;
}


