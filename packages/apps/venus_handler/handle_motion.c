#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#include "handle_motion.h"
#include "slog.h"

// #include "VR_define.h"
#include "verik_utils.h"
// #include "vr_sound.h"
// #include "led.h"

extern notify_queue_t g_notify;
extern pthread_mutex_t notify_lock;
extern int venus_motion_process_enable;

static const char *EV_NAME[EV_MAX] = {
#include "evtable_EV.h"
};

static const char *KEY_NAME[KEY_MAX] = {
#include "evtable_KEY.h"
#include "evtable_BTN.h"
};

static const char *SW_NAME[SW_MAX] = {
#include "evtable_SW.h"
};

const char *lookup_event_name_i( const int evtype, const int evcode ) {
    if (evtype == EV_KEY) {
        return (KEY_MAX >= evcode ? KEY_NAME[ evcode ] : NULL);
    }
    if (evtype == EV_SW) {
        return (SW_MAX >= evcode ? SW_NAME[ evcode ] : NULL);
    }
    return NULL;
}

const char *lookup_event_name( const struct input_event ev ) {
    return lookup_event_name_i( ev.type, ev.code );
}

const char *lookup_type_name_i( const int evtype ) {
    return (EV_MAX >= evtype ? EV_NAME[ evtype ] : NULL);
}

const char *lookup_type_name( const struct input_event ev ) {
    return lookup_type_name_i( ev.type );
}

// static void print_event(char* dev, struct input_event ev) {
//     const char *typename = lookup_type_name( ev );
//     const char *evname = lookup_event_name( ev );
//     if ( evname != NULL ) {
//         printf( "%d.%06d\t%s\t%s\t%d\t%s\n", (int)ev.time.tv_sec, (int)ev.time.tv_usec, typename, evname, ev.value, dev );
//     } else {
//         fprintf( stderr, "Unknown %s event id on %s : %d (value %d)\n", typename, dev, ev.code, ev.value );
//     }
//     fflush(stdout);
// }

void handle_motion(void *data)
{
    int fd = -1;
    fd = open(BUTTON_HANDLE_PATH, O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd == -1) 
    {
        return;
    }

    // Turn off blocking for reads, use (fd, F_SETFL, FNDELAY) if you want that
    fcntl(fd, F_SETFL, fcntl(fd, F_GETFL) | O_NONBLOCK);

    while(venus_motion_process_enable)
    {
        struct input_event ev;
        int n = read( fd, &ev, sizeof(ev) );
        if ( n != sizeof(ev) ) 
        {
            usleep(POOLING_TIME);
            continue;
        }

        if (ev.type != EV_KEY && ev.type != EV_SW)
        {
            usleep(POOLING_TIME);
            continue;
        }

        // print_event( BUTTON_HANDLE_PATH, ev );
        switch (ev.code)
        {
            case MOTION_EVENT:
            {
                if(ev.value)
                {
                    pthread_mutex_lock(&notify_lock);
                    g_notify.notify[g_notify.notify_index].notify_type= NOTIFY_MOTION;
                    g_notify.notify[g_notify.notify_index].notify_message[0] = 1;
                    g_notify.notify_index=g_notify.notify_index+1;
                    pthread_mutex_unlock(&notify_lock);
                }
                else
                {
                    pthread_mutex_lock(&notify_lock);
                    g_notify.notify[g_notify.notify_index].notify_type= NOTIFY_MOTION;
                    g_notify.notify[g_notify.notify_index].notify_message[0] = 0;
                    g_notify.notify_index=g_notify.notify_index+1;
                    pthread_mutex_unlock(&notify_lock);
                }
                break;
            }
        }

        usleep(POOLING_TIME);
    }

    close(fd);
}