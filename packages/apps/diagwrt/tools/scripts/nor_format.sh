#!/bin/bash
# $1: uDiag-$(BOARD).img
# $2: boot/boot_nor/uDiag-nor.bin
# $3: uDiag-$(BOARD)-nor.bin
# $4: 524288

if [ ! -f $1 ]
then
    echo "Input file [$1] not found"
    exit
fi
if [ ! -f $2 ]
then
    echo "Input file [$2] not found"
    exit
fi

file1size=`wc -c <"$1"`
file2size=`wc -c <"$2"`
size=$4

padsize=$size
if [ $padsize -le $file2size ]
then
    echo "No Pad size: $padsize, $2: $file2size"
    exit
fi
let "padsize = $padsize - $file2size"

if [ $padsize -lt $file1size ]
then
    echo "No Pad size: $padsize, $1: $file1size"
    exit
fi

if [ $padsize -eq $file1size ]
then
    let "padsize = $padsize - $file1size"
    echo "Pad($1 + $padsize .B + $2)"
    echo " > $3"
    cat $1 $2 > $3
    exit
fi
let "padsize = $padsize - $file1size"

#echo "Pad size: $padsize"

dd if=/dev/zero of=tmp.bin bs=1 count=$padsize

cat $1 tmp.bin > temp.bin
cat temp.bin $2 > $3

rm -f tmp.bin
rm -f temp.bin

echo "Pad($1 + $padsize .B + $2)"
echo " > $3"

#filesize=`wc -c <"$3"`
#echo "New $3: $filesize"