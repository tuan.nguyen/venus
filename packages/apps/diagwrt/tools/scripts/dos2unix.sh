#!/bin/sh
convert_in_folder()
{
	for file in $1/*
	do
		echo $2/$file
		dos2unix -q $file
	done
	
	for file in $1/d2utmp*
	do
		rm $file
	done
	
	return
}
# Main here
if [ $1 ]; then
echo ""
echo " ##### Convert Folder '$1' to Unix Format with 5 subfolder #####"
echo ""
else
echo "";
echo "  >>> Please input Folder";
exit;
fi
DIR1=$1/
convert_in_folder $DIR1
#Step 1
cd $DIR1
for DIR2 in $(ls -d */)
do
convert_in_folder $DIR2 $DIR1
#Step 2
cd $DIR2
for DIR3 in $(ls -d */)
do
convert_in_folder $DIR3 $DIR1$DIR2
#Step 3
cd $DIR3
for DIR4 in $(ls -d */)
do
convert_in_folder $DIR4 $DIR1$DIR2$DIR3
#Step 4
cd $DIR4
for DIR5 in $(ls -d */)
do
convert_in_folder $DIR5 $DIR1$DIR2$DIR3$DIR4
done
cd ..
#End Step 4
done
cd ..
#End Step 3
done
cd ..
#End Step 2
done
cd ..
#End Step 1