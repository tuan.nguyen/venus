#!/bin/bash
if [ ! -f $1 ]
then
    echo "Input file [$1] not found"
    exit
fi
size=`wc -c <"$1"`
let "sizek = $size / 1024"
let "sizeb = $size % 1024"
let "sizea = $sizeb * 100"
let "sizel = $sizea / 1024"

echo "$1: $sizek.$sizel KB"
