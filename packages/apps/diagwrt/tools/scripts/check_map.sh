#!/bin/bash
#$1: _start
#$2: _end
#$3: resetvec
#$4: <name>

size=$(($3-$1+4))
sizek=$(($size / 1024))
let=$(($3-$2))
letk=$(($let / 1024))

echo ""
echo "   #### $4 size: $sizek KB ####"
echo "   '_start'=$1 '_end'=$2, 'resetvec'=$3"

if [ $let -lt 0 ]
then
echo -e "\033[41m   Overlap (size: $letk KB, $let B) !!!\033[0m"
exit 0
else
echo -e "\033[32m   Free (size: $letk KB, $let B)\033[0m"
exit 1
fi
echo ""