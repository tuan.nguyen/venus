/*
 * pciutils.c
 *
 *
 *  Copyright (c) 1997--2008 Martin Mares <mj@ucw.cz>
 *
 *	Can be freely distributed and used under the terms of the GNU GPL.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stddef.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdarg.h>
#include <errno.h>
#include <stdlib.h>
#include <malloc.h>
#include <linux/init.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <linux/list.h>
#include "internal.h"
#include "header.h"
#include "vediag/pciutils.h"
#include "linux/types.h"
#include "vediag_common.h"
#include <log.h>

#ifdef PCI_HAVE_PM_INTEL_CONF
#define GENOPT_INTEL "H:"
#define GENHELP_INTEL "-H <mode>\tUse direct hardware access (<mode> = 1 or 2)\n"
#else
#define GENOPT_INTEL
#define GENHELP_INTEL
#endif
#if defined(PCI_HAVE_PM_DUMP) && !defined(PCIUTILS_SETPCI)
#define GENOPT_DUMP "F:"
#define GENHELP_DUMP "-F <file>\tRead PCI configuration dump from a given file\n"
#else
#define GENOPT_DUMP
#define GENHELP_DUMP
#endif

#define GENERIC_OPTIONS "A:GO:" GENOPT_INTEL GENOPT_DUMP
#define GENERIC_HELP \
	"-A <method>\tUse the specified PCI access method (see `-A help' for a list)\n" \
	"-O <par>=<val>\tSet PCI access parameter (see `-O help' for a list)\n" \
	"-G\t\tEnable PCI access debugging\n" \
	GENHELP_INTEL GENHELP_DUMP
static char options[] = "nvbxs:d:ti:mgp:qkMDQ" GENERIC_OPTIONS ;

pthread_mutex_t host_mutex[CONFIG_PCIE_MAX_PORTS];

struct pci_access *pacc;
struct device *first_dev; /*for lspci used*/
struct pci_filter filter;		/* Device filter */
int pciutils_verbose;			/* Show detailed information */
static int opt_hex;				/* Show contents of config space as hexadecimal numbers */
static int opt_tree;			/* Show bus tree */
static int opt_map_mode;		/* Bus mapping mode enabled */
static int opt_domains = 2;			/* Show domain numbers (0=disabled, 1=auto-detected, 2=requested) */
static int opt_machine;			/* Generate machine-readable output */
static int opt_kernel;			/* Show kernel drivers */
char *opt_pcimap;			    /* Override path to Linux modules.pcimap */

static char help_msg[] =
"Usage: lspci [<switches>]\n"
"\n"
"Basic display modes:\n"
"-mm\t\tProduce machine-readable output (single -m for an obsolete format)\n"
"-t\t\tShow bus tree\n"
"\n"
"Display options:\n"
"-v\t\tBe verbose (-vv for very verbose)\n"
#ifdef PCI_OS_LINUX
"-k\t\tShow kernel drivers handling each device\n"
#endif
"-x\t\tShow hex-dump of the standard part of the config space\n"
"-xxx\t\tShow hex-dump of the whole config space (dangerous; root only)\n"
"-xxxx\t\tShow hex-dump of the 4096-byte extended config space (root only)\n"
"-b\t\tBus-centric view (addresses and IRQ's as seen by the bus)\n"
"-D\t\tAlways show domain numbers\n"
"\n"
"Resolving of device ID's to names:\n"
"-n\t\tShow numeric ID's\n"
"-nn\t\tShow both textual and numeric ID's (names & numbers)\n"
#ifdef PCI_USE_DNS
"-q\t\tQuery the PCI ID database for unknown ID's via DNS\n"
"-qq\t\tAs above, but re-query locally cached entries\n"
"-Q\t\tQuery the PCI ID database for all ID's via DNS\n"
#endif
"\n"
"Other options:\n"
"-i <file>\tUse specified ID database instead of %s\n"
#ifdef PCI_OS_LINUX
"-p <file>\tLook up kernel modules in a given file instead of default modules.pcimap\n"
#endif
"-M\t\tEnable `bus mapping' mode (dangerous; root only)\n"
"\n"
"PCI access options:\n"
GENERIC_HELP
;


static int seen_errors;

static struct pci_methods *pci_methods[PCI_ACCESS_MAX] = {
  NULL,
  &pm_linux_sysfs,
};

struct pci_aer_mask {
	struct list_head list;
	struct pci_dbdf bdf;
	uint32_t ce_mask;
	uint32_t uc_mask;
};

static LIST_HEAD(aer_default_mask);

void show_kernel(struct device *d UNUSED)
{
}

void show_kernel_machine(struct device *d UNUSED)
{
}

void show_kernel_cleanup(void)
{
}
char *_pci_lookup_name(struct pci_access *a, char *buf, int size, int flags, ...)
{
	return "";
}
char *pci_lookup_name(struct pci_access *a, char *buf, int size, int flags, ...)
__attribute__((weak, alias("_pci_lookup_name")));


/*============================================================================*/

void *
pci_malloc(struct pci_access *a, int size)
{
  void *x = malloc(size);

  if (!x)
    a->error("Out of memory (allocation of %d bytes failed)", size);
  return x;
}

void
pci_mfree(void *x)
{
  if (x)
    free(x);
}

char *
pci_strdup(struct pci_access *a, const char *s)
{
  int len = strlen(s) + 1;
  char *t = pci_malloc(a, len);
  memcpy(t, s, len);
  return t;
}

int
pci_lookup_method(char *name)
{
  int i;

  for (i=0; i<PCI_ACCESS_MAX; i++)
    if (pci_methods[i] && !strcmp(pci_methods[i]->name, name))
      return i;
  return -1;
}

char *
pci_get_method_name(int index)
{
  if (index < 0 || index >= PCI_ACCESS_MAX)
    return NULL;
  else if (!pci_methods[index])
    return "";
  else
    return pci_methods[index]->name;
}

static void pci_generic_error(char *msg, ...) {
	va_list args;

	va_start(args, msg);
	xerror("pcilib: ");
	vxerror(msg, args);
	xerror("\n");
	va_end(args);
}

static void pci_generic_warn(char *msg, ...) {
	va_list args;

	va_start(args, msg);
	xerror("pcilib: ");
	vxerror(msg, args);
	xerror("\n");
	va_end(args);
}

static void pci_generic_debug(char *msg, ...) {
	va_list args;

	va_start(args, msg);
	vxerror(msg, args);
	va_end(args);
}

static void pci_null_debug(char *msg UNUSED, ...)
{

}

void pci_init(struct pci_access *a)
{
	if (!a->error)
		a->error = pci_generic_error;
	if (!a->warning)
		a->warning = pci_generic_warn;
	if (!a->debug)
		a->debug = pci_generic_debug;
	if (!a->debugging)
		a->debug = pci_null_debug;

	if (a->method) {
		if (a->method >= PCI_ACCESS_MAX || !pci_methods[a->method])
			a->error("This access method is not supported.");
		a->methods = pci_methods[a->method];
	} else {
		unsigned int i;
		for (i = 0; i < PCI_ACCESS_MAX; i++)
			if (pci_methods[i]) {
				a->debug("Trying method %d...", i);
				if (pci_methods[i]->detect(a)) {
					a->debug("...OK\n");
					a->methods = pci_methods[i];
					a->method = i;
					break;
				}
				a->debug("...No.\n");
			}
		if (!a->methods)
			a->error("Cannot find any working access method.");
	}
	a->debug("Decided to use %s\n", a->methods->name);
	a->methods->init(a);
}

int config_fetch(struct device *d, unsigned int pos, unsigned int len)
{
	unsigned int end = pos + len;
	int result;

	while (pos < d->config_bufsize && len && d->present[pos])
		pos++, len--;
	while (pos + len <= d->config_bufsize && len && d->present[pos + len - 1])
		len--;
	if (!len)
		return 1;

	if (end > d->config_bufsize) {
		int orig_size = d->config_bufsize;
		while (end > d->config_bufsize)
			d->config_bufsize *= 2;
		d->config = xrealloc(d->config, d->config_bufsize);
		d->present = xrealloc(d->present, d->config_bufsize);
		memset(d->present + orig_size, 0, d->config_bufsize - orig_size);
	}
	result = pci_read_block(d->dev, pos, d->config + pos, len);
	if (result)
		memset(d->present + pos, 1, len);
	return result;
}

/*** Config space accesses ***/

static void check_conf_range(struct device *d, unsigned int pos,
		unsigned int len)
{
	while (len)
		if (!d->present[pos])
			die("Internal bug: Accessing non-read configuration byte at position %x", pos);
		else
			pos++, len--;
}

byte get_conf_byte(struct device *d, unsigned int pos)
{
	check_conf_range(d, pos, 1);
	return d->config[pos];
}

word get_conf_word(struct device *d, unsigned int pos)
{
	check_conf_range(d, pos, 2);
	return d->config[pos] | (d->config[pos + 1] << 8);
}

u32 get_conf_long(struct device *d, unsigned int pos)
{
	check_conf_range(d, pos, 4);
	return d->config[pos] | (d->config[pos + 1] << 8)
			| (d->config[pos + 2] << 16) | (d->config[pos + 3] << 24);
}

static void show_slot_name(struct device *d)
{
	struct pci_dev *p = d->dev;

	if (!opt_machine ? opt_domains : (p->domain || opt_domains >= 2))
		xinfo("%04x:", p->domain);
	xinfo("%02x:%02x.%d", p->bus, p->dev, p->func);
}

void get_subid(struct device *d, word *subvp, word *subdp)
{
	byte htype = get_conf_byte(d, PCI_HEADER_TYPE) & 0x7f;

	if (htype == PCI_HEADER_TYPE_NORMAL) {
		*subvp = get_conf_word(d, PCI_SUBSYSTEM_VENDOR_ID);
		*subdp = get_conf_word(d, PCI_SUBSYSTEM_ID);
	} else if (htype == PCI_HEADER_TYPE_CARDBUS && d->config_cached >= 128) {
		*subvp = get_conf_word(d, PCI_CB_SUBSYSTEM_VENDOR_ID);
		*subdp = get_conf_word(d, PCI_CB_SUBSYSTEM_ID);
	} else
		*subvp = *subdp = 0xffff;
}

static void show_terse(struct device *d)
{
	int c;
	struct pci_dev *p = d->dev;
	char classbuf[128], devbuf[128];

	show_slot_name(d);
	xinfo(" %s: %s",
			pci_lookup_name(pacc, classbuf, sizeof(classbuf), PCI_LOOKUP_CLASS,
					p->device_class),
			pci_lookup_name(pacc, devbuf, sizeof(devbuf),
					PCI_LOOKUP_VENDOR | PCI_LOOKUP_DEVICE, p->vendor_id,
					p->device_id));
	if ((c = get_conf_byte(d, PCI_REVISION_ID)))
		xinfo(" (rev %02x)", c);
	if (pciutils_verbose) {
		char *x;
		c = get_conf_byte(d, PCI_CLASS_PROG);
		x = pci_lookup_name(pacc, devbuf, sizeof(devbuf),
				PCI_LOOKUP_PROGIF | PCI_LOOKUP_NO_NUMBERS, p->device_class, c);
		if (c || x) {
			xinfo(" (prog-if %02x", c);
			if (x)
				xinfo(" [%s]", x);
			xinfo(")");
		}
	}
	xinfo("\n");

	if (pciutils_verbose || opt_kernel) {
		word subsys_v, subsys_d;
		char ssnamebuf[256];

		if (p->label)
			xinfo("\tDeviceName: %s", p->label);
		get_subid(d, &subsys_v, &subsys_d);
		if (subsys_v && subsys_v != 0xffff)
			xinfo("\tSubsystem: %s\n",
					pci_lookup_name(pacc, ssnamebuf, sizeof(ssnamebuf),
							PCI_LOOKUP_SUBSYSTEM | PCI_LOOKUP_VENDOR
									| PCI_LOOKUP_DEVICE, p->vendor_id,
							p->device_id, subsys_v, subsys_d));
	}
}

/*** Verbose output ***/

static void show_size(pciaddr_t x)
{
	static const char suffix[][2] = { "", "K", "M", "G", "T" };
	unsigned i;
	if (!x)
		return;
	for (i = 0; i < (sizeof(suffix) / sizeof(*suffix) - 1); i++) {
		if (x < 1024)
			break;
		x /= 1024;
	}
	xinfo(" [size=%u%s]", (unsigned) x, suffix[i]);
}

static void show_bases(struct device *d, int cnt)
{
	struct pci_dev *p = d->dev;
	word cmd = get_conf_word(d, PCI_COMMAND);
	int i;
	int virtual = 0;

	for (i = 0; i < cnt; i++) {
		pciaddr_t pos = p->base_addr[i];
		pciaddr_t len = (p->known_fields & PCI_FILL_SIZES) ? p->size[i] : 0;
		u32 flg = get_conf_long(d, PCI_BASE_ADDRESS_0 + 4 * i);
		if (flg == 0xffffffff)
			flg = 0;
		if (!pos && !flg && !len)
			continue;
		if (pciutils_verbose > 1)
			xinfo("\tRegion %d: ", i);
		else
			xinfo("\t");
		if (pos && !flg) /* Reported by the OS, but not by the device */
		{
			xinfo("[virtual] ");
			flg = pos;
			virtual = 1;
		}
		if (flg & PCI_BASE_ADDRESS_SPACE_IO) {
			pciaddr_t a = pos & PCI_BASE_ADDRESS_IO_MASK;
			xinfo("I/O ports at ");
			if (a || (cmd & PCI_COMMAND_IO))
				xinfo(PCIADDR_PORT_FMT, a);
			else if (flg & PCI_BASE_ADDRESS_IO_MASK)
				xinfo("<ignored>");
			else
				xinfo("<unassigned>");
			if (!virtual && !(cmd & PCI_COMMAND_IO))
				xinfo(" [disabled]");
		} else {
			int t = flg & PCI_BASE_ADDRESS_MEM_TYPE_MASK;
			pciaddr_t a = pos & PCI_ADDR_MEM_MASK;
			int done = 0;
			u32 z = 0;

			xinfo("Memory at ");
			if (t == PCI_BASE_ADDRESS_MEM_TYPE_64) {
				if (i >= cnt - 1) {
					xinfo("<invalid-64bit-slot>");
					done = 1;
				} else {
					i++;
					z = get_conf_long(d, PCI_BASE_ADDRESS_0 + 4 * i);
				}
			}
			if (!done) {
				if (a)
					xinfo(PCIADDR_T_FMT, a);
				else
					xinfo(
							((flg & PCI_BASE_ADDRESS_MEM_MASK) || z) ?
									"<ignored>" : "<unassigned>");
			}
			xinfo(" (%s, %sprefetchable)",
					(t == PCI_BASE_ADDRESS_MEM_TYPE_32) ? "32-bit" :
					(t == PCI_BASE_ADDRESS_MEM_TYPE_64) ? "64-bit" :
					(t == PCI_BASE_ADDRESS_MEM_TYPE_1M) ? "low-1M" : "type 3",
					(flg & PCI_BASE_ADDRESS_MEM_PREFETCH) ? "" : "non-");
			if (!virtual && !(cmd & PCI_COMMAND_MEMORY))
				xinfo(" [disabled]");
		}
		show_size(len);
		xinfo("\n");
	}
}

static void show_rom(struct device *d, int reg)
{
	struct pci_dev *p = d->dev;
	pciaddr_t rom = p->rom_base_addr;
	pciaddr_t len = (p->known_fields & PCI_FILL_SIZES) ? p->rom_size : 0;
	u32 flg = get_conf_long(d, reg);
	word cmd = get_conf_word(d, PCI_COMMAND);
	int virtual = 0;

	if (!rom && !flg && !len)
		return;
	xinfo("\t");
	if ((rom & PCI_ROM_ADDRESS_MASK) && !(flg & PCI_ROM_ADDRESS_MASK)) {
		xinfo("[virtual] ");
		flg = rom;
		virtual = 1;
	}
	xinfo("Expansion ROM at ");
	if (rom & PCI_ROM_ADDRESS_MASK)
		xinfo(PCIADDR_T_FMT, rom & PCI_ROM_ADDRESS_MASK);
	else if (flg & PCI_ROM_ADDRESS_MASK)
		xinfo("<ignored>");
	else
		xinfo("<unassigned>");
	if (!(flg & PCI_ROM_ADDRESS_ENABLE))
		xinfo(" [disabled]");
	else if (!virtual && !(cmd & PCI_COMMAND_MEMORY))
		xinfo(" [disabled by cmd]");
	show_size(len);
	xinfo("\n");
}

static void show_htype0(struct device *d)
{
	show_bases(d, 6);
	show_rom(d, PCI_ROM_ADDRESS);
	show_caps(d, PCI_CAPABILITY_LIST);
}

static void show_htype1(struct device *d)
{
	u32 io_base = get_conf_byte(d, PCI_IO_BASE);
	u32 io_limit = get_conf_byte(d, PCI_IO_LIMIT);
	u32 io_type = io_base & PCI_IO_RANGE_TYPE_MASK;
	u32 mem_base = get_conf_word(d, PCI_MEMORY_BASE);
	u32 mem_limit = get_conf_word(d, PCI_MEMORY_LIMIT);
	u32 mem_type = mem_base & PCI_MEMORY_RANGE_TYPE_MASK;
	u32 pref_base = get_conf_word(d, PCI_PREF_MEMORY_BASE);
	u32 pref_limit = get_conf_word(d, PCI_PREF_MEMORY_LIMIT);
	u32 pref_type = pref_base & PCI_PREF_RANGE_TYPE_MASK;
	word sec_stat = get_conf_word(d, PCI_SEC_STATUS);
	word brc = get_conf_word(d, PCI_BRIDGE_CONTROL);
	int verb = pciutils_verbose > 2;

	show_bases(d, 2);
	xinfo(
			"\tBus: primary=%02x, secondary=%02x, subordinate=%02x, sec-latency=%d\n",
			get_conf_byte(d, PCI_PRIMARY_BUS),
			get_conf_byte(d, PCI_SECONDARY_BUS),
			get_conf_byte(d, PCI_SUBORDINATE_BUS),
			get_conf_byte(d, PCI_SEC_LATENCY_TIMER));

	if (io_type != (io_limit & PCI_IO_RANGE_TYPE_MASK)
			|| (io_type != PCI_IO_RANGE_TYPE_16
					&& io_type != PCI_IO_RANGE_TYPE_32))
		xinfo("\t!!! Unknown I/O range types %x/%x\n", io_base, io_limit);
	else {
		io_base = (io_base & PCI_IO_RANGE_MASK) << 8;
		io_limit = (io_limit & PCI_IO_RANGE_MASK) << 8;
		if (io_type == PCI_IO_RANGE_TYPE_32) {
			io_base |= (get_conf_word(d, PCI_IO_BASE_UPPER16) << 16);
			io_limit |= (get_conf_word(d, PCI_IO_LIMIT_UPPER16) << 16);
		}
		if (io_base <= io_limit || verb)
			xinfo("\tI/O behind bridge: %08x-%08x\n", io_base,
					io_limit + 0xfff);
	}

	if (mem_type != (mem_limit & PCI_MEMORY_RANGE_TYPE_MASK) || mem_type)
		xinfo("\t!!! Unknown memory range types %x/%x\n", mem_base, mem_limit);
	else {
		mem_base = (mem_base & PCI_MEMORY_RANGE_MASK) << 16;
		mem_limit = (mem_limit & PCI_MEMORY_RANGE_MASK) << 16;
		if (mem_base <= mem_limit || verb)
			xinfo("\tMemory behind bridge: %08x-%08x\n", mem_base,
					mem_limit + 0xfffff);
	}

	if (pref_type != (pref_limit & PCI_PREF_RANGE_TYPE_MASK)
			|| (pref_type != PCI_PREF_RANGE_TYPE_32
					&& pref_type != PCI_PREF_RANGE_TYPE_64))
		xinfo("\t!!! Unknown prefetchable memory range types %x/%x\n",
				pref_base, pref_limit);
	else {
		pref_base = (pref_base & PCI_PREF_RANGE_MASK) << 16;
		pref_limit = (pref_limit & PCI_PREF_RANGE_MASK) << 16;
		if (pref_base <= pref_limit || verb) {
			if (pref_type == PCI_PREF_RANGE_TYPE_32)
				xinfo("\tPrefetchable memory behind bridge: %08x-%08x\n",
						pref_base, pref_limit + 0xfffff);
			else
				xinfo(
						"\tPrefetchable memory behind bridge: %08x%08x-%08x%08x\n",
						get_conf_long(d, PCI_PREF_BASE_UPPER32), pref_base,
						get_conf_long(d, PCI_PREF_LIMIT_UPPER32),
						pref_limit + 0xfffff);
		}
	}

	if (pciutils_verbose > 1)
		xinfo(
				"\tSecondary status: 66MHz%c FastB2B%c ParErr%c DEVSEL=%s >TAbort%c <TAbort%c <MAbort%c <SERR%c <PERR%c\n",
				FLAG(sec_stat, PCI_STATUS_66MHZ),
				FLAG(sec_stat, PCI_STATUS_FAST_BACK),
				FLAG(sec_stat, PCI_STATUS_PARITY),
				((sec_stat & PCI_STATUS_DEVSEL_MASK) == PCI_STATUS_DEVSEL_SLOW) ?
						"slow" :
				((sec_stat & PCI_STATUS_DEVSEL_MASK) == PCI_STATUS_DEVSEL_MEDIUM) ?
						"medium" :
				((sec_stat & PCI_STATUS_DEVSEL_MASK) == PCI_STATUS_DEVSEL_FAST) ?
						"fast" : "??",
				FLAG(sec_stat, PCI_STATUS_SIG_TARGET_ABORT),
				FLAG(sec_stat, PCI_STATUS_REC_TARGET_ABORT),
				FLAG(sec_stat, PCI_STATUS_REC_MASTER_ABORT),
				FLAG(sec_stat, PCI_STATUS_SIG_SYSTEM_ERROR),
				FLAG(sec_stat, PCI_STATUS_DETECTED_PARITY));

	show_rom(d, PCI_ROM_ADDRESS1);

	if (pciutils_verbose > 1) {
		xinfo(
				"\tBridgeCtl: Parity%c SERR%c NoISA%c VGA%c MAbort%c >Reset%c FastB2B%c\n",
				FLAG(brc, PCI_BRIDGE_CTL_PARITY),
				FLAG(brc, PCI_BRIDGE_CTL_SERR),
				FLAG(brc, PCI_BRIDGE_CTL_NO_ISA), FLAG(brc, PCI_BRIDGE_CTL_VGA),
				FLAG(brc, PCI_BRIDGE_CTL_MASTER_ABORT),
				FLAG(brc, PCI_BRIDGE_CTL_BUS_RESET),
				FLAG(brc, PCI_BRIDGE_CTL_FAST_BACK));
		xinfo("\t\tPriDiscTmr%c SecDiscTmr%c DiscTmrStat%c DiscTmrSERREn%c\n",
				FLAG(brc, PCI_BRIDGE_CTL_PRI_DISCARD_TIMER),
				FLAG(brc, PCI_BRIDGE_CTL_SEC_DISCARD_TIMER),
				FLAG(brc, PCI_BRIDGE_CTL_DISCARD_TIMER_STATUS),
				FLAG(brc, PCI_BRIDGE_CTL_DISCARD_TIMER_SERR_EN));
	}

	show_caps(d, PCI_CAPABILITY_LIST);
}

static void show_htype2(struct device *d)
{
	int i;
	word cmd = get_conf_word(d, PCI_COMMAND);
	word brc = get_conf_word(d, PCI_CB_BRIDGE_CONTROL);
	word exca;
	int verb = pciutils_verbose > 2;

	show_bases(d, 1);
	xinfo(
			"\tBus: primary=%02x, secondary=%02x, subordinate=%02x, sec-latency=%d\n",
			get_conf_byte(d, PCI_CB_PRIMARY_BUS),
			get_conf_byte(d, PCI_CB_CARD_BUS),
			get_conf_byte(d, PCI_CB_SUBORDINATE_BUS),
			get_conf_byte(d, PCI_CB_LATENCY_TIMER));
	for (i = 0; i < 2; i++) {
		int p = 8 * i;
		u32 base = get_conf_long(d, PCI_CB_MEMORY_BASE_0 + p);
		u32 limit = get_conf_long(d, PCI_CB_MEMORY_LIMIT_0 + p);
		limit = limit + 0xfff;
		if (base <= limit || verb)
			xinfo("\tMemory window %d: %08x-%08x%s%s\n", i, base, limit,
					(cmd & PCI_COMMAND_MEMORY) ? "" : " [disabled]",
					(brc & (PCI_CB_BRIDGE_CTL_PREFETCH_MEM0 << i)) ?
							" (prefetchable)" : "");
	}
	for (i = 0; i < 2; i++) {
		int p = 8 * i;
		u32 base = get_conf_long(d, PCI_CB_IO_BASE_0 + p);
		u32 limit = get_conf_long(d, PCI_CB_IO_LIMIT_0 + p);
		if (!(base & PCI_IO_RANGE_TYPE_32)) {
			base &= 0xffff;
			limit &= 0xffff;
		}
		base &= PCI_CB_IO_RANGE_MASK;
		limit = (limit & PCI_CB_IO_RANGE_MASK) + 3;
		if (base <= limit || verb)
			xinfo("\tI/O window %d: %08x-%08x%s\n", i, base, limit,
					(cmd & PCI_COMMAND_IO) ? "" : " [disabled]");
	}

	if (get_conf_word(d, PCI_CB_SEC_STATUS) & PCI_STATUS_SIG_SYSTEM_ERROR)
		xinfo("\tSecondary status: SERR\n");
	if (pciutils_verbose > 1)
		xinfo(
				"\tBridgeCtl: Parity%c SERR%c ISA%c VGA%c MAbort%c >Reset%c 16bInt%c PostWrite%c\n",
				FLAG(brc, PCI_CB_BRIDGE_CTL_PARITY),
				FLAG(brc, PCI_CB_BRIDGE_CTL_SERR),
				FLAG(brc, PCI_CB_BRIDGE_CTL_ISA),
				FLAG(brc, PCI_CB_BRIDGE_CTL_VGA),
				FLAG(brc, PCI_CB_BRIDGE_CTL_MASTER_ABORT),
				FLAG(brc, PCI_CB_BRIDGE_CTL_CB_RESET),
				FLAG(brc, PCI_CB_BRIDGE_CTL_16BIT_INT),
				FLAG(brc, PCI_CB_BRIDGE_CTL_POST_WRITES));

	if (d->config_cached < 128) {
		xinfo("\t<access denied to the rest>\n");
		return;
	}

	exca = get_conf_word(d, PCI_CB_LEGACY_MODE_BASE);
	if (exca)
		xinfo("\t16-bit legacy interface ports at %04x\n", exca);
	show_caps(d, PCI_CB_CAPABILITY_LIST);
}

static void show_verbose(struct device *d)
{
	struct pci_dev *p = d->dev;
	word status = get_conf_word(d, PCI_STATUS);
	word cmd = get_conf_word(d, PCI_COMMAND);
	word class = p->device_class;
	byte bist = get_conf_byte(d, PCI_BIST);
	byte htype = get_conf_byte(d, PCI_HEADER_TYPE) & 0x7f;
	byte latency = get_conf_byte(d, PCI_LATENCY_TIMER);
	byte cache_line = get_conf_byte(d, PCI_CACHE_LINE_SIZE);
	byte max_lat, min_gnt;
	byte int_pin = get_conf_byte(d, PCI_INTERRUPT_PIN);
	unsigned int irq = p->irq;

	show_terse(d);

	switch (htype) {
	case PCI_HEADER_TYPE_NORMAL:
		if (class == PCI_CLASS_BRIDGE_PCI)
			xinfo("\t!!! Invalid class %04x for header type %02x\n", class,
					htype);
		max_lat = get_conf_byte(d, PCI_MAX_LAT);
		min_gnt = get_conf_byte(d, PCI_MIN_GNT);
		break;
	case PCI_HEADER_TYPE_BRIDGE:
		if ((class >> 8) != PCI_BASE_CLASS_BRIDGE)
			xinfo("\t!!! Invalid class %04x for header type %02x\n", class,
					htype);
		min_gnt = max_lat = 0;
		break;
	case PCI_HEADER_TYPE_CARDBUS:
		if ((class >> 8) != PCI_BASE_CLASS_BRIDGE)
			xinfo("\t!!! Invalid class %04x for header type %02x\n", class,
					htype);
		min_gnt = max_lat = 0;
		break;
	default:
		xinfo("\t!!! Unknown header type %02x\n", htype);
		return;
	}

	if (p->phy_slot)
		xinfo("\tPhysical Slot: %s\n", p->phy_slot);

	if (pciutils_verbose > 1) {
		xinfo(
				"\tControl: I/O%c Mem%c BusMaster%c SpecCycle%c MemWINV%c VGASnoop%c ParErr%c Stepping%c SERR%c FastB2B%c DisINTx%c\n",
				FLAG(cmd, PCI_COMMAND_IO), FLAG(cmd, PCI_COMMAND_MEMORY),
				FLAG(cmd, PCI_COMMAND_MASTER), FLAG(cmd, PCI_COMMAND_SPECIAL),
				FLAG(cmd, PCI_COMMAND_INVALIDATE),
				FLAG(cmd, PCI_COMMAND_VGA_PALETTE),
				FLAG(cmd, PCI_COMMAND_PARITY), FLAG(cmd, PCI_COMMAND_WAIT),
				FLAG(cmd, PCI_COMMAND_SERR), FLAG(cmd, PCI_COMMAND_FAST_BACK),
				FLAG(cmd, PCI_COMMAND_DISABLE_INTx));
		xinfo(
				"\tStatus: Cap%c 66MHz%c UDF%c FastB2B%c ParErr%c DEVSEL=%s >TAbort%c <TAbort%c <MAbort%c >SERR%c <PERR%c INTx%c\n",
				FLAG(status, PCI_STATUS_CAP_LIST),
				FLAG(status, PCI_STATUS_66MHZ), FLAG(status, PCI_STATUS_UDF),
				FLAG(status, PCI_STATUS_FAST_BACK),
				FLAG(status, PCI_STATUS_PARITY),
				((status & PCI_STATUS_DEVSEL_MASK) == PCI_STATUS_DEVSEL_SLOW) ?
						"slow" :
				((status & PCI_STATUS_DEVSEL_MASK) == PCI_STATUS_DEVSEL_MEDIUM) ?
						"medium" :
				((status & PCI_STATUS_DEVSEL_MASK) == PCI_STATUS_DEVSEL_FAST) ?
						"fast" : "??",
				FLAG(status, PCI_STATUS_SIG_TARGET_ABORT),
				FLAG(status, PCI_STATUS_REC_TARGET_ABORT),
				FLAG(status, PCI_STATUS_REC_MASTER_ABORT),
				FLAG(status, PCI_STATUS_SIG_SYSTEM_ERROR),
				FLAG(status, PCI_STATUS_DETECTED_PARITY),
				FLAG(status, PCI_STATUS_INTx));
		if (cmd & PCI_COMMAND_MASTER) {
			xinfo("\tLatency: %d", latency);
			if (min_gnt || max_lat) {
				xinfo(" (");
				if (min_gnt)
					xinfo("%dns min", min_gnt * 250);
				if (min_gnt && max_lat)
					xinfo(", ");
				if (max_lat)
					xinfo("%dns max", max_lat * 250);
				xinfo(")");
			}
			if (cache_line)
				xinfo(", Cache Line Size: %d bytes", cache_line * 4);
			xinfo("\n");
		}
		if (int_pin || irq)
			xinfo("\tInterrupt: pin %c routed to IRQ " PCIIRQ_FMT "\n",
					(int_pin ? 'A' + int_pin - 1 : '?'), irq);
	} else {
		xinfo("\tFlags: ");
		if (cmd & PCI_COMMAND_MASTER)
			xinfo("bus master, ");
		if (cmd & PCI_COMMAND_VGA_PALETTE)
			xinfo("VGA palette snoop, ");
		if (cmd & PCI_COMMAND_WAIT)
			xinfo("stepping, ");
		if (cmd & PCI_COMMAND_FAST_BACK)
			xinfo("fast Back2Back, ");
		if (status & PCI_STATUS_66MHZ)
			xinfo("66MHz, ");
		if (status & PCI_STATUS_UDF)
			xinfo("user-definable features, ");
		xinfo("%s devsel",
				((status & PCI_STATUS_DEVSEL_MASK) == PCI_STATUS_DEVSEL_SLOW) ?
						"slow" :
				((status & PCI_STATUS_DEVSEL_MASK) == PCI_STATUS_DEVSEL_MEDIUM) ?
						"medium" :
				((status & PCI_STATUS_DEVSEL_MASK) == PCI_STATUS_DEVSEL_FAST) ?
						"fast" : "??");
		if (cmd & PCI_COMMAND_MASTER)
			xinfo(", latency %d", latency);
		if (irq)
			xinfo(", IRQ " PCIIRQ_FMT, irq);
		xinfo("\n");
	}

	if (bist & PCI_BIST_CAPABLE) {
		if (bist & PCI_BIST_START)
			xinfo("\tBIST is running\n");
		else
			xinfo("\tBIST result: %02x\n", bist & PCI_BIST_CODE_MASK);
	}

	switch (htype) {
	case PCI_HEADER_TYPE_NORMAL:
		show_htype0(d);
		break;
	case PCI_HEADER_TYPE_BRIDGE:
		show_htype1(d);
		break;
	case PCI_HEADER_TYPE_CARDBUS:
		show_htype2(d);
		break;
	}
}

/*** Machine-readable dumps ***/

static void show_hex_dump(struct device *d)
{
	unsigned int i, cnt;

	cnt = d->config_cached;
	if (opt_hex >= 3 && config_fetch(d, cnt, 256 - cnt)) {
		cnt = 256;
		if (opt_hex >= 4 && config_fetch(d, 256, 4096 - 256))
			cnt = 4096;
	}

	for (i = 0; i < cnt; i++) {
		if (!(i & 15))
			xinfo("%02x:", i);
		xinfo(" %02x", get_conf_byte(d, i));
		if ((i & 15) == 15)
			xinfo("\n");
	}
}

static void print_shell_escaped(char *c)
{
	xinfo(" \"");
	while (*c) {
		if (*c == '"' || *c == '\\')
			xinfo("\\");
		xinfo("%c", *c++);
	}
	xinfo("\"");
}

static void show_machine(struct device *d)
{
	struct pci_dev *p = d->dev;
	int c;
	word sv_id, sd_id;
	char classbuf[128], vendbuf[128], devbuf[128], svbuf[128], sdbuf[128];

	get_subid(d, &sv_id, &sd_id);

	if (pciutils_verbose) {
		xinfo((opt_machine >= 2) ? "Slot:\t" : "Device:\t");
		show_slot_name(d);
		xinfo("\n");
		xinfo("Class:\t%s\n",
				pci_lookup_name(pacc, classbuf, sizeof(classbuf),
						PCI_LOOKUP_CLASS, p->device_class));
		xinfo("Vendor:\t%s\n",
				pci_lookup_name(pacc, vendbuf, sizeof(vendbuf),
						PCI_LOOKUP_VENDOR, p->vendor_id, p->device_id));
		xinfo("Device:\t%s\n",
				pci_lookup_name(pacc, devbuf, sizeof(devbuf), PCI_LOOKUP_DEVICE,
						p->vendor_id, p->device_id));
		if (sv_id && sv_id != 0xffff) {
			xinfo("SVendor:\t%s\n",
					pci_lookup_name(pacc, svbuf, sizeof(svbuf),
							PCI_LOOKUP_SUBSYSTEM | PCI_LOOKUP_VENDOR, sv_id));
			xinfo("SDevice:\t%s\n",
					pci_lookup_name(pacc, sdbuf, sizeof(sdbuf),
							PCI_LOOKUP_SUBSYSTEM | PCI_LOOKUP_DEVICE,
							p->vendor_id, p->device_id, sv_id, sd_id));
		}
		if (p->phy_slot)
			xinfo("PhySlot:\t%s\n", p->phy_slot);
		if ((c = get_conf_byte(d, PCI_REVISION_ID)))
			xinfo("Rev:\t%02x\n", c);
		if ((c = get_conf_byte(d, PCI_CLASS_PROG)))
			xinfo("ProgIf:\t%02x\n", c);
		if (opt_kernel)
			show_kernel_machine(d);
	} else {
		show_slot_name(d);
		print_shell_escaped(
				pci_lookup_name(pacc, classbuf, sizeof(classbuf),
						PCI_LOOKUP_CLASS, p->device_class));
		print_shell_escaped(
				pci_lookup_name(pacc, vendbuf, sizeof(vendbuf),
						PCI_LOOKUP_VENDOR, p->vendor_id, p->device_id));
		print_shell_escaped(
				pci_lookup_name(pacc, devbuf, sizeof(devbuf), PCI_LOOKUP_DEVICE,
						p->vendor_id, p->device_id));
		if ((c = get_conf_byte(d, PCI_REVISION_ID)))
			xinfo(" -r%02x", c);
		if ((c = get_conf_byte(d, PCI_CLASS_PROG)))
			xinfo(" -p%02x", c);
		if (sv_id && sv_id != 0xffff) {
			print_shell_escaped(
					pci_lookup_name(pacc, svbuf, sizeof(svbuf),
							PCI_LOOKUP_SUBSYSTEM | PCI_LOOKUP_VENDOR, sv_id));
			print_shell_escaped(
					pci_lookup_name(pacc, sdbuf, sizeof(sdbuf),
							PCI_LOOKUP_SUBSYSTEM | PCI_LOOKUP_DEVICE,
							p->vendor_id, p->device_id, sv_id, sd_id));
		} else
			xinfo(" \"\" \"\"");
		xinfo("\n");
	}
}

void show_device(struct device *d)
{
	if (opt_machine)
		show_machine(d);
	else {
		if (pciutils_verbose)
			show_verbose(d);
		else
			show_terse(d);
		if (opt_kernel || pciutils_verbose)
			show_kernel(d);
	}
	if (opt_hex)
		show_hex_dump(d);
	if (pciutils_verbose || opt_hex)
		xinfo("\n");
}

struct device * scan_device(struct pci_dev *p)
{
	struct device *d;

	if (p->domain && !opt_domains)
		opt_domains = 1;
	if (!pci_filter_match(&filter, p)){
		xinfo("Info: Filtered device\n");
		return NULL;
	}
	d = xmalloc(sizeof(struct device));
	memset(d, 0, sizeof(*d));
	d->dev = p;
	d->config_cached = d->config_bufsize = 64;
	d->config = xmalloc(64);
	d->present = xmalloc(64);
	memset(d->present, 1, 64);
	if (!pci_read_block(p, 0, d->config, 64)) {
		xerror(
				"lspci: Unable to read the standard configuration space header of device %04x:%02x:%02x.%d\n",
				p->domain, p->bus, p->dev, p->func);
		seen_errors++;
		return NULL;
	}
	if ((d->config[PCI_HEADER_TYPE] & 0x7f) == PCI_HEADER_TYPE_CARDBUS) {
		/* For cardbus bridges, we need to fetch 64 bytes more to get the
		 * full standard header... */
		if (config_fetch(d, 64, 64))
			d->config_cached += 64;
	}
	pci_setup_cache(p, d->config, d->config_cached);
	pci_fill_info(p,
			PCI_FILL_IDENT | PCI_FILL_CLASS | PCI_FILL_IRQ | PCI_FILL_BASES
					| PCI_FILL_ROM_BASE | PCI_FILL_SIZES | PCI_FILL_PHYS_SLOT
					| PCI_FILL_LABEL);
	return d;
}

static void scan_devices(void)
{
	struct device *d;
	struct pci_dev *p;

	pci_scan_bus(pacc);
	for (p = pacc->devices; p; p = p->next){
		if ((d = scan_device(p))) {
			d->next = first_dev;
			first_dev = d;
			/*dbg("Added new device\n");*/
		}
	}
}

/*** Sorting ***/

static int compare_them(const void *A, const void *B)
{
	const struct pci_dev *a = (*(const struct device **) A)->dev;
	const struct pci_dev *b = (*(const struct device **) B)->dev;

	if (a->domain < b->domain)
		return -1;
	if (a->domain > b->domain)
		return 1;
	if (a->bus < b->bus)
		return -1;
	if (a->bus > b->bus)
		return 1;
	if (a->dev < b->dev)
		return -1;
	if (a->dev > b->dev)
		return 1;
	if (a->func < b->func)
		return -1;
	if (a->func > b->func)
		return 1;
	return 0;
}

static void sort_them(void)
{
	struct device **index, **h, **last_dev;
	int cnt;
	struct device *d;

	cnt = 0;
	for (d = first_dev; d; d = d->next)
		cnt++;

	h = index = xmalloc(sizeof(struct device *) * cnt);
	for (d = first_dev; d; d = d->next)
		*h++ = d;
	qsort(index, cnt, sizeof(struct device *), compare_them);
	last_dev = &first_dev;
	h = index;
	while (cnt--) {
		*last_dev = *h;
		last_dev = &(*h)->next;
		h++;
	}
	*last_dev = NULL;
}

static void show(void)
{
	struct device *d;

	for (d = first_dev; d; d = d->next)
		show_device(d);
}

static struct pci_dev* pci_find_device(struct pci_dbdf *d)
{
	struct pci_dev *dev;
	volatile struct device *tmp;

	for (tmp = first_dev; tmp; tmp = tmp->next) {
		dev = tmp->dev;
		/*dbg("Checking [%04x:%02x:%02x.%02x]\n", dev->domain, dev->bus,
				dev->dev, dev->func);*/
		if ((d->domain == dev->domain)
				&& (d->bus == dev->bus)
				&& (d->dev == dev->dev)
				&& (d->func == dev->func)) {
			return dev;
		}
	}

	dbg("Error: Device not found [%04x:%02x:%02x.%02x]\n", d->domain, d->bus,
			d->dev, d->func);
	return NULL;
}

int pci_read_config_byte(struct pci_dbdf *d, int offset, uint8_t* value)
{
	int ret = 0;

	if (d->domain >= SIZEOF_ARRAY(host_mutex))
		return 0;
	//pthread_mutex_lock(&host_mutex[d->domain]);
	struct pci_dev *dev = pci_find_device(d);
	if (dev)
		 ret = pci_read_byte(dev, offset, value);
	else
		dbg("Error: Device not found [%04x:%02x:%02x.%02x]\n", d->domain, d->bus,
				d->dev, d->func);

	//pthread_mutex_unlock(&host_mutex[d->domain]);
	return ret;
}

int pci_read_config_word(struct pci_dbdf *d, int offset, uint16_t* value)
{
	int ret = 0;

	if (d->domain >= SIZEOF_ARRAY(host_mutex))
		return 0;
	//pthread_mutex_lock(&host_mutex[d->domain]);

	struct pci_dev *dev = pci_find_device(d);
	if (dev)
		 ret = pci_read_word(dev, offset, value);
	else
		dbg("Error: Device not found [%04x:%02x:%02x.%02x]\n", d->domain, d->bus,
				d->dev, d->func);

	//pthread_mutex_unlock(&host_mutex[d->domain]);
	return ret;
}

int pci_read_config_dword(struct pci_dbdf *d, int offset, uint32_t* value)
{
	int ret = 0;

	if (d->domain >= SIZEOF_ARRAY(host_mutex))
		return 0;
	//pthread_mutex_lock(&host_mutex[d->domain]);
	struct pci_dev *dev = pci_find_device(d);
	if (dev)
		ret = pci_read_dword(dev, offset, value);
	else
		dbg("Error: Device not found [%04x:%02x:%02x.%02x]\n", d->domain, d->bus,
				d->dev, d->func);

	//pthread_mutex_unlock(&host_mutex[d->domain]);
	return ret;
}

int pci_write_config_byte(struct pci_dbdf *d, int offset, uint8_t value)
{
	int ret = 0;
	struct pci_dev *dev = pci_find_device(d);

	if (d->domain >= SIZEOF_ARRAY(host_mutex))
		return 0;
	if (dev){
		dbg("Error: Device not found [%04x:%02x:%02x.%02x]\n", d->domain, d->bus,
				d->dev, d->func);
		return 0;
	}
	//pthread_mutex_lock(&host_mutex[d->domain]);
	ret = pci_write_byte(dev, offset, value);
	//pthread_mutex_unlock(&host_mutex[d->domain]);
	return ret;
}

int pci_write_config_word(struct pci_dbdf *d, int offset, uint16_t value)
{
	int ret = 0;
	struct pci_dev *dev = pci_find_device(d);

	if (d->domain >= SIZEOF_ARRAY(host_mutex))
		return 0;
	if (!dev){
		dbg("Error: Device not found [%04x:%02x:%02x.%02x]\n", d->domain, d->bus,
				d->dev, d->func);
		return 0;
	}
	//pthread_mutex_lock(&host_mutex[d->domain]);
	ret = pci_write_word(dev, offset, value);
	//pthread_mutex_unlock(&host_mutex[d->domain]);
	return ret;
}

int pci_write_config_dword(struct pci_dbdf *d, int offset, uint32_t value)
{
	int ret = 0;
	struct pci_dev *dev = pci_find_device(d);

	if (d->domain >= SIZEOF_ARRAY(host_mutex))
		return 0;
	if (!dev){
		dbg("Error: Device not found [%04x:%02x:%02x.%02x]\n", d->domain, d->bus,
				d->dev, d->func);
		return -1;
	}
	//pthread_mutex_lock(&host_mutex[d->domain]);
	ret = pci_write_dword(dev, offset, value);
	//pthread_mutex_unlock(&host_mutex[d->domain]);
	return ret;
}

void pci_cleanup(struct pci_access *a)
{
	struct pci_dev *d, *e;

	for (d = a->devices; d; d = e) {
		e = d->next;
		pci_free_dev(d);
	}

	if (a->methods)
		a->methods->cleanup(a);
	pci_free_name_list(a);
	pci_free_params(a);
	pci_set_name_list_path(a, NULL, 0);
	pci_mfree(a);
}

#define PCI_IDS "pci.ids"
#define PCI_PATH_IDS_DIR "/usr/share/hwdata"
#define PCILIB_VERSION "3.3.1"
struct pci_access * pci_alloc(void)
{
	struct pci_access *a = malloc(sizeof(struct pci_access));
	int i;

	memset(a, 0, sizeof(*a));
	pci_set_name_list_path(a, PCI_PATH_IDS_DIR "/" PCI_IDS, 0);

	for (i = 0; i < PCI_ACCESS_MAX; i++)
		if (pci_methods[i] && pci_methods[i]->config)
			pci_methods[i]->config(a);
	return a;
}

static void pciutils_scan_all(void)
{
	pacc = pci_alloc();
	pacc->error = die;
	pci_init(pacc);
	scan_devices();
}

void pciutils_rescan(void)
{
	pci_cleanup(pacc);
	pci_filter_init(pacc, &filter);
	first_dev = NULL;
	pciutils_scan_all();
	sort_them();
}

int pciutils_ls(int argc, char* argv[])
{
	int i;

	pciutils_verbose = 0;
	opt_hex = 0;			/* Show contents of config space as hexadecimal numbers */
	opt_tree= 0;			/* Show bus tree */
	opt_map_mode = 0;		/* Bus mapping mode enabled */
	opt_domains = 2;		/* Show domain numbers (0=disabled, 1=auto-detected, 2=requested) */
	opt_machine = 0;		/* Generate machine-readable output */
	opt_kernel = 0;			/* Show kernel drivers */

	/*Reset getopt*/
	optind = 0;
	while ((i = getopt(argc, argv, options)) != -1) {
		switch (i) {
		case 'n':
			pacc->numeric_ids++;
			break;
		case 'v':
			pciutils_verbose++;
			break;
		case 'b':
			pacc->buscentric = 1;
			break;
#if 0
	 	case 's':
			if ((msg = pci_filter_parse_slot(&filter, optarg)))
				die("-s: %s", msg);
			dbg("Added filter to slot\n");
			break;
		case 'd':
			if ((msg = pci_filter_parse_id(&filter, optarg)))
				die("-d: %s", msg);
			break;
#endif
		case 'x':
			opt_hex++;
			break;
#if 0
		case 't':
			opt_tree++;
			break;
#endif
		case 'i':
			pci_set_name_list_path(pacc, optarg, 0);
			break;
		case 'm':
			opt_machine++;
			break;
		case 'p':
			opt_pcimap = optarg;
			break;
#ifdef PCI_OS_LINUX
			case 'k':
			opt_kernel++;
			break;
#endif
		case 'M':
			opt_map_mode++;
			break;
		case 'D':
			opt_domains = 2;
			break;
#ifdef PCI_USE_DNS
			case 'q':
			opt_query_dns++;
			break;
			case 'Q':
			opt_query_all = 1;
			break;
#else
		case 'q':
		case 'Q':
			die("DNS queries are not available in this version");
#endif
		default:
			dbg("In default case:\n");
			if (parse_generic_option(i, pacc, optarg))
				break;
			bad:xerror(help_msg, pacc->id_file_name);
			goto exit_error;
		}
	}

	if (optind < argc){
		xerror("Error: parse failed\n");
		goto bad;
	}

    if (opt_tree)
    	show_forest();
    else
    	show();

    return 0;

exit_error:
	return 1;
}

void vediag_pciutils_scan_devices(int force)
{
	char phypath[32] = {0};
	char devpath[32] = {0};
	struct pci_dev *dev;
	volatile struct device *tmp;
	int cnt = 0;

	/* Force rescan storage devices */
	if (force)
		vediag_update_system_config("COMMON", get_section_type(VEDIAG_PCIE), "0");

	char namebuf[256] = {0};
	for (tmp = first_dev; tmp; tmp = tmp->next) {
		dev = tmp->dev;
		memset(phypath, 0, sizeof(phypath));
		if (dev->bus) {
		    if (pci_lookup_name(pacc, namebuf, 256,
		                    PCI_LOOKUP_VENDOR | PCI_LOOKUP_DEVICE,
							dev->vendor_id, dev->device_id)) {
		        snprintf(phypath, 31, "%s", namebuf);
		    }
			snprintf(devpath, 31, "%04x:%02x:%02x.%02x", dev->domain,dev->bus,
					dev->dev, dev->func);
			vediag_add_identified_device(VEDIAG_PCIE, devpath, phypath);
			cnt++;
		}
	}
}

int vediag_pciutils_get_devname(struct pci_dbdf* p, char* buf)
{
	u16 vid = 0xffff, did = 0xffff;
	char namebuf[256] = {0};

	pci_read_config_word(p, PCI_VENDOR_ID, &vid);
	pci_read_config_word(p, PCI_DEVICE_ID, &did);

	if (vid == 0xffff || vid == 0x0){
		return false;
	}

	if (pci_lookup_name(pacc, namebuf, 256,
			PCI_LOOKUP_VENDOR | PCI_LOOKUP_DEVICE, vid, did)) {
		snprintf(buf, 31, "%s", namebuf);
		return true;
	} else {
		return false;
	}
}

void pciutils_default_aer_mask(void)
{
	struct device *tmp;
	int capptr;
	struct pci_aer_mask* mask_list;
	struct pci_dbdf sbdf = {0};
	struct pci_dev *dev;

	for (tmp = first_dev; tmp; tmp = tmp->next) {
		dev = tmp->dev;
		sbdf.domain = dev->domain;
		sbdf.bus = dev->bus;
		sbdf.dev = dev->dev;
		sbdf.func = dev->func;
		capptr = pci_find_ext_capability(&sbdf, PCI_EXT_CAP_ID_AER);
		if (capptr){
			mask_list = malloc(sizeof(struct pci_aer_mask));
			if (!mask_list) {
				xerror("Error: Malloc failed\n");
				return;
			}
			memcpy(&mask_list->bdf, &sbdf, sizeof(struct pci_dbdf));
			pci_read_config_dword(&sbdf, capptr + PCI_ERR_COR_MASK,
					&mask_list->ce_mask);
			pci_read_config_dword(&sbdf, capptr + PCI_ERR_UNCOR_MASK,
					&mask_list->uc_mask);

			list_add_tail(&mask_list->list, &aer_default_mask);
			xdebug("[%04x:%02x:%02x.%02x]: Added ce_mask = %08x, uc_mask = %08x\n",
					dev->domain, dev->bus, dev->dev, dev->func,
					mask_list->ce_mask, mask_list->uc_mask);
		}
	}

}

/**
 *
 * @param bdf
 * @param ce_mask
 * @param uc_mask
 * @return 1 if success, else 0
 */
int pci_get_aer_default_mask(struct pci_dbdf *bdf, uint32_t* ce_mask,
		uint32_t* uc_mask)
{
	struct pci_aer_mask* e;
	list_for_each_entry(e, &aer_default_mask, list) {
		if (e->bdf.domain == bdf->domain && e->bdf.bus == bdf->bus
				&& e->bdf.dev == bdf->dev && e->bdf.func == bdf->func){
			*ce_mask = e->ce_mask;
			*uc_mask = e->uc_mask;
			return 1;
		}
	}
	return 0;
}

core_initcall(pciutils_init)
{
	int i;

	for (i = 0; i < SIZEOF_ARRAY(host_mutex); i++){
		pthread_mutex_init(&host_mutex[i], NULL);
	}
	pci_filter_init(pacc, &filter);
	pciutils_scan_all();
	build_bridge_list();
	pciutils_default_aer_mask();
    return 0;
}
