/*
 * rand_utils.c
 */

#include <stddef.h>
#include <stdlib.h>
#include <time.h>

void get_random_bytes(void *buf, int nbytes)
{
	srand(time(NULL));
	char *tmp = (char *)buf;
	int i;
	for (i = 0; i < nbytes; i++) {
		tmp[i] = rand();
	}
}
