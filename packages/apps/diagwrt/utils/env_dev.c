
/* #define DEBUG */

#include <malloc.h>
#include <string.h>
#include <types.h>
#include <vediag_common.h>
#include <configs/config.h>
#include <log.h>
#include <vediag/environment.h>


static struct global_data gd_vars;
struct global_data *gd;

env_t *env_ptr = NULL;

#define ENV_FILE "vediag-env.bin"

int env_init(void)
{
	FILE *fp;

	gd = &gd_vars;
	gd->env_addr  = (ulong)&default_environment[0];
	gd->env_valid = 1;

	// create file if not exist
	fp = fopen(ENV_FILE, "ab+");
	fclose(fp);
	return 0;
}

#if defined(CONFIG_CMD_SAVEENV) && !defined(CONFIG_ENV_IS_NOWHERE)
int saveenv(void)
{
	FILE *fp;

	xinfo("Writing env ... ");

	fp = fopen(ENV_FILE, "wb");
	if (!fp) {
		xerror("Error: Cannot open %s\n", ENV_FILE);
		return -1;
	}
	if (fwrite((uint8_t*) env_ptr, 1, CONFIG_ENV_SIZE, fp) != CONFIG_ENV_SIZE) {
		xerror("Error: failed\n");
		return -1;
	} else {
		xinfo("ok\n");
	}
	fclose(fp);
	return 0;
}
#endif /*CMD_SAVEENV */

#if defined(CONFIG_CMD_CLEANENV) && !defined(CONFIG_ENV_IS_NOWHERE)
int cleanenv(void)
{
	int rc = 0;
	FILE *fp;
	void *buf = NULL;

	buf = malloc(CONFIG_ENV_SIZE);
	memset(buf, 0xff, CONFIG_ENV_SIZE);

	xinfo("Erase env ... ");

	fp = fopen(ENV_FILE, "wb");
	if (!fp) {
		xerror("Error: Cannot open %s\n", ENV_FILE);
		return -1;
	}
	if (fwrite((uint8_t*) buf, 1, CONFIG_ENV_SIZE, fp) != CONFIG_ENV_SIZE) {
		xerror("Error: failed\n");
		rc = -1;
	} else {
		xinfo("ok\n");
	}

	fclose(fp);
	free(buf);
	return rc;

}
#endif /*CMD_SAVEENV */

void env_relocate_spec(void)
{
	FILE *fp;
	size_t count;

	fp = fopen(ENV_FILE, "rb");
	if (!fp) {
		xerror("Error: Cannot open %s\n", ENV_FILE);
		goto exit;
	}

	count = fread((uint8_t *) env_ptr, 1, CONFIG_ENV_SIZE, fp);
	if (count != CONFIG_ENV_SIZE) {
		goto exit;
	}

	if (crc32(0, (char *)env_ptr->data, ENV_SIZE) == env_ptr->crc) {
		gd->env_addr  = (ulong)&(env_ptr->data);
		gd->env_valid = 1;
	} else {
		xerror ("\033[31m   Read Env from '%s' failed CRC!\033[0m\n", ENV_FILE);
		goto exit;
	}

	fclose(fp);
	return;
exit:
	fclose(fp);
	set_default_env(NULL);
}
