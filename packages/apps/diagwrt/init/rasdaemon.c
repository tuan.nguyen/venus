#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <linux/kernel.h>
#include <sys/sysinfo.h>
#include <pthread.h>
#include <sys/time.h>
#include <cmd.h>
#include <ras/kmsg-reader.h>
#include <log.h>
#include <time_util.h>
#include <strlib.h>
#include <ras.h>
#include <circbuf.h>
#include <linux/_kernel.h>

extern int vediag_exit;
long user_hz;

/*cpu, l2, l3, iob, xgic, smmu, mpa, smpro, pmpro, ahbc, trace, menet, tcu, ocm*/
CircularBuffer ras_cb_system;
/*pci*/
CircularBuffer ras_cb_pci;
/*sata, usb*/
CircularBuffer ras_cb_stor;
/*ddr*/
CircularBuffer ras_cb_ddr;

#define ras_dbg(fmt, arg...) do {\
		xdebug(fmt, ##arg);\
	} while(0)

static long start_time_sec;

/*
 * 0 CPU Report L1 memory errors from ICF, LSU, MMU units
 * 1 L2C Report L2 memory and operation timeout error
 * 2 L3C Report L3 memory errors
 * 4 MCU Report Memory Errors
 * 5 IOB Report IOB GLBL, RBM and TRANS errors
 * 6 XGIC Report XGIC errors
 * 7 SMMU Report SMMU0 and SMMU1 errors
 * 8 SOC Report SMpro, PMpro, AHBC, MENET, SATA, TRACE, USB, TCU, OCM, PCI errors
 * 9 MPA Report SMpro and PMpro errors
 */
 #define RAS_NR_LEVEL			10
 #define RAX_NR_ELEM_EACH_LEVEL	10
static char* ras_level[RAS_NR_LEVEL][RAX_NR_ELEM_EACH_LEVEL] = {
	/* level 0 */ {"ICF", "LSU", "MMU"},
	/* level 1 */ {"L2C"},
	/* level 2 */ {"L3C"},
	/* level 3 */ {"N/A"},
	/* level 4 */ {"MCU"},
	/* level 5 */ {"IOB"},
	/* level 6 */ {"XGIC"},
	/* level 7 */ {"SMMU"},
	/* level 8 */ {"SMPRO CSR", "PMPRO CSR", "AHBC", "MENET", "SATA", "TRACE", "USB", "TCU", "OCM", "PCI"},
	/* level 9 */ {"SMPRO", "PMPRO"}

};

/* Stop test if level of detected error < this */
static int ras_stop_level = 3; 

/**
 * Get sequence number of RAS message
 * @param msg
 * @param seqnum
 * @return true if success, else 0
 */
static int ras_get_seqnum(const char* msg, unsigned int* seqnum)
{
	/* {2}[Hardware Error]: Hardware error from APEI Generic Hardware Error Source: 0 */
#define SEQNUM_MAX_CHARS	16
	char *seq, *s;

	seq = calloc(SEQNUM_MAX_CHARS, 1);
	if (!seq) {
		printf("Error: %s %d: malloc failed\n", __FILE__, __LINE__);
		return false;
	}

	s = string_get_substring(msg, "{", "}", seq, SEQNUM_MAX_CHARS - 1);
	if (s) {
		*seqnum = strtoul(seq, NULL, 10);
	} else {
		free(seq);
		return false;
	}

	free(seq);
	return true;
}

static int ras_check_to_stop_vediag(const char* msg)
{
	int i, j;
	int len;
	for (i = 0; i < RAS_NR_LEVEL; i++) {
		for (j = 0; j < RAX_NR_ELEM_EACH_LEVEL; j++) {
			if (ras_level[i][j]) {
				len = min(strlen(msg), strlen(ras_level[i][j]));
				if (strncmp(msg, ras_level[i][j], len) == 0) {
					if (i < ras_stop_level) {
						return true;
					}
				}
			} else {
				continue;
			}
		}

	}
	
	return false;
}

static int ras_decode(const char* msg)
{
	/*
	 * msg example: "fru_text: ICF {M-C-}"
	 */
	ElemType *e;
	char *s, *tmp;

	if (!msg)
		return -1;

	e = calloc(1, sizeof(ElemType));
	if (!e) {
		xerror("Error: %s %d malloc failed\n", __FILE__, __LINE__);
		return -1;
	}

	tmp = strdup(msg);
	if (!tmp) {
		xerror("Error: %s %d malloc failed\n", __FILE__, __LINE__);
		free(e);
		return -1;
	}

	s = string_get_substring(msg, ":", "{", tmp, 31);
	if (s) {
		if (strstrip_s(tmp, e->buf) == NULL)
			return -1;

		if (strstr(e->buf, RAS_PCI)) {
			cbWrite(&ras_cb_pci, e);
			ras_dbg("  Added 1 element to PCI queue\n");
		} else if (strstr(e->buf, RAS_SATA)) {
			cbWrite(&ras_cb_stor, e);
			ras_dbg("  Added 1 element to STORAGE queue\n");
		} else if (strstr(e->buf, RAS_USB)) {
			cbWrite(&ras_cb_stor, e);
			ras_dbg("  Added 1 element to STORAGE queue\n");
		} else if (strstr(e->buf, RAS_DDR)) {
			cbWrite(&ras_cb_ddr, e);
			ras_dbg("  Added 1 element to DDR queue\n");
		} else {
			cbWrite(&ras_cb_system, e);
			ras_dbg("  Added 1 element to SYSTEM queue\n");
		}
		
		if (ras_check_to_stop_vediag(e->buf)) {
			vediag_err(VEDIAG_SYSTEM, "RAS",
					"Detected critical Hardware Error (%s), force stop vediag test\n",
					e->buf);
			stop_test_func(0, 0);
		}
	} else {
		free(e);
		return -1;
	}

	free(e);
	free(tmp);
	return 0;
}

static int ras_kmsg_parse(const char* msg)
{
	/*
	 * Example ras message:
	 *
	 * [ 4877.079558] {2}[Hardware Error]: Hardware error from APEI Generic Hardware Error Source: 0
	 * [ 4877.087831] {2}[Hardware Error]: It has been corrected by h/w and requires no further action
	 * [ 4877.096267] {2}[Hardware Error]: event severity: corrected
	 * [ 4877.101752] {2}[Hardware Error]:  Error 0, type: corrected
	 * [ 4877.107234] {2}[Hardware Error]:  fru_text: ICF {M-C-}
	 * [ 4877.112369] {2}[Hardware Error]:   section_type: general processor error
	 * [ 4877.119065] {2}[Hardware Error]:   error_type: 0x01
	 * [ 4877.123937] {2}[Hardware Error]:   cache error
	 * [ 4877.128370] {2}[Hardware Error]:   level: 1
	 * [ 4877.132547] {2}[Hardware Error]:   processor_id: 0x0000000000000003
	 * [ 4877.138811] {2}[Hardware Error]:  Error 1, type: info
	 * [ 4877.143858] {2}[Hardware Error]:  fru_text: esr
	 * [ 4877.150285] {2}[Hardware Error]:   section_type: general processor error
	 * [ 4877.156983] {2}[Hardware Error]:   requestor_id: 0x00000000de2dbe65
	 */
	enum {
		WAIT_ERROR_0,
		WAIT_FRUTEXT
	};
	static int state = WAIT_ERROR_0;
	char *p;
	static char pattern[64] = {0};
	unsigned int seq_num = 0;
	static double cur_time;

	switch (state) {
	case WAIT_ERROR_0:
		p = strstr(msg, "[Hardware Error]:  Error 0,");
		if (p != NULL) {
			if (ras_get_seqnum(msg, &seq_num) == true) {
				snprintf(pattern, 63, "{%d}[Hardware Error]:  fru_text:",
						seq_num);
			} else {
				snprintf(pattern, 63, "[Hardware Error]:  fru_text:");
			}
			state = WAIT_FRUTEXT;
			cur_time = get_time_sec();
		}
		break;
	case WAIT_FRUTEXT:
		p = strstr(msg, pattern);
		if (p != NULL) {
			p = strstr(msg, "fru_text");
			ras_dbg("[RAS] Detected fru_text = '%s'\n", p);
			ras_decode(p);
			state = WAIT_ERROR_0;
		}

		/* timeout waiting for frutext of this sequence */
		if (get_time_sec() - cur_time > 10) {
			state = WAIT_ERROR_0;
		}
	default:
		break;
	}

	return 0;
}

static void new_kmsg(struct dmesg_record *rec)
{
	char* buf;
	int len;

	if (rec->tv.tv_sec < start_time_sec) {
		return;
	}

	len = strlen(rec->mesg) + 64;
	if (len > 8192) {
		printf("Error: kmsg len too big\n");
		return;
	}

	buf = calloc(len, 1);
	if (!buf) {
		xerror("Error: %s %d malloc failed\n", __FILE__, __LINE__);
		return;
	}

	snprintf(buf, len, "[%5ld.%06ld] %s", (long) rec->tv.tv_sec,
			(long) rec->tv.tv_usec, rec->mesg);
	ras_kmsg_parse(buf);

	if (strstr(buf, "[Hardware Error]")) {
		xerror("%s\n", buf);
		fflush(stdout);
	}

	free(buf);
}

static void* dmesg_thread(void* data)
{
	static struct dmesg_control ctl;
	struct sysinfo info;

	xdebug("Start thread %s\n", __func__);
	
	memset(&ctl, 0x0, sizeof(ctl));

	ctl.action = SYSLOG_ACTION_READ_ALL;
	ctl.read_pos = KMSG_READ_BEGIN;
	ctl.time_fmt = DMESG_TIMEFTM_TIME,
	ctl.kmsg_handler = new_kmsg;

	init_kmsg(&ctl);

	if (sysinfo(&info) != 0) {
		xdebug("Error: sysinfo failed\n");
	} else {
		start_time_sec = info.uptime;
	}

	while (!vediag_exit){
		read_buffer(&ctl);
		sleep(1);
	}

	return NULL;
}

void ras_init(void)
{
	pthread_t thread;
	char *s;
	
	if (cbInit(&ras_cb_system, 64) != 0)
		return;
	if (cbInit(&ras_cb_pci, 64) != 0)
		return;
	if (cbInit(&ras_cb_stor, 64) != 0)
		return;
	if (cbInit(&ras_cb_ddr, 64) != 0)
		return;
	
	s = vediag_parse_system("ras_level");
	if (s) {
		ras_stop_level = strtoul(s, NULL, 10);
	}
	xdebug(" Info: ras_stop_level = %d\n", ras_stop_level);
	
	if (pthread_create(&thread, NULL, dmesg_thread, NULL)) {
		xerror("Error: Fail to create thread\n");
	}

	/* for back compatible, not used now */
	user_hz = sysconf(_SC_CLK_TCK);
}

void ras_exit(void)
{
	cbFree(&ras_cb_system);
	cbFree(&ras_cb_pci);
	cbFree(&ras_cb_stor);
	cbFree(&ras_cb_ddr);
}
