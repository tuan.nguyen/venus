
#ifndef _VEDIAG_GPIO_H_
#define _VEDIAG_GPIO_H_

#include <log.h>

/*#define GPIO_DBG	*//*Enable gpio debug*/

#ifdef GPIO_DBG
#define GPIODBG(fmt, args...)			xinfo(fmt, ##args)
#else
#define GPIODBG(fmt, args...)
#endif

#define GPIO_ERROR(fmt, args...)		xerror(fmt, ##args)

#define GPIO0_7_REG_BASE			0x126f0000 /*Non secure base*/
#define GPIO8_15_REG_BASE			0x126e0000 /*secure base*/
#define GPI_REG_BASE				0x126d0000 /*Non secure base*/

/*GPIO Register define*/

#define GPIO_SWPORTA_DR			0x0		/*Set Output signal*/
#define GPIO_SWPORTA_DDR		0x4		/*Set direction 0-Input(defalut), 1-Output*/
#define GPIO_INTEN				0x30	/*Enable External IRQ 1-enalbe*/
#define GPIO_INTTYPE_LEVEL		0x34	/*1 - Edge sensitive (not support), 0 - level sensitive*/
#define GPIO_INT_POLARITY		0x38	/*1 - IRQ active high, 0 - IRQ active low*/
#define GPIO_RAW_INTSTATUS		0x40	/*raw IRQ status write 1 to clear*/
#define GPIO_INTMASK			0x44	/*IRQ mask*/
#define GPIO_DEBOUNCE			0x48
#define GPIO_DEBOUNCE_COUNTER	0x70
#define GPIO_DEBOUNCE_TIMEBASE	0x74

/*Read only register*/
#define GPIO_PORTA_CTL			0x8		/*0 sw mode, 1 hw mode*/
#define GPIO_INTSTATUS			0x3c	/*IRQ status*/
#define GPIO_EXT_PORTA			0x50	/*Read Input value*/
#define GPIO_COMP_VERSION		0x6c

/*GPI Register define*/
#define GPI_EXT_PORTA			0x0 /*Read Input value*/
#define GPI_COMP_VERSION		0x4 /*Read Input value*/

typedef struct board_gpio{
	char *gpio_name;
	char *gpio_pin;
	int gpio_dir;			/* gpio pin direction */
	int gpio_val;			/* gpio pin value */
} board_gpio_t ;

typedef enum gpio_direction { GPIO_IN = 0, GPIO_IRQ_LEVEL_LOW, GPIO_IRQ_LEVEL_HIGH, GPIO_IRQ_EDGE_LOW, GPIO_IRQ_EDGE_HIGH, GPIO_OUT } gpio_direction_t;

typedef enum gpio_value { GPIO_LOW = 0, GPIO_HIGH } gpio_value_t;

typedef enum {
	/*GPI Pin*/
	GPI_0 = 0,	GPI_1,		GPI_2,		GPI_3,		GPI_4,		GPI_5,		GPI_6,		GPI_7
} gpi_pin_t;

typedef enum {
	/*GPIO Pin*/
	GPIO_0,		GPIO_1,		GPIO_2,		GPIO_3,		GPIO_4,		GPIO_5,		GPIO_6,		GPIO_7,
	GPIO_8,		GPIO_9,		GPIO_10,	GPIO_11,	GPIO_12, 	GPIO_13, 	GPIO_14, 	GPIO_15
}gpio_pin_t;

/*Xgene GPIO define*/
#define XGENE_GPI_MAX_PIN			8
#define XGENE_GPIO_MAX_PIN			16

/*Expander GPIO define*/


typedef struct gpio_ops {
	int (*get_direction)(gpio_pin_t gpio_pin);
	int (*set_direction)(gpio_pin_t gpio_pin, gpio_direction_t direction);
	int (*get_input_value)(gpio_pin_t gpio_pin);
	int (*set_output_value)(gpio_pin_t gpio, gpio_value_t value);
	int (*get_output_value)(gpio_pin_t gpio_pin);
	int (*get_irq_status)(gpio_pin_t gpio_pin);
} gpio_ops_t;

void xgene_gpio_init(gpio_ops_t **gpio_ops);
void xgene_gpi_init(gpio_ops_t **gpio_ops);

int gpio_get_direction(const char *gpio_pin);
int gpio_set_direction(const char *gpio_pin, gpio_direction_t direction);
int gpio_get_input_value(const char *gpio_pin);
int gpio_get_output_value(const char *gpio_pin);
int gpio_set_output_value(const char *gpio_pin, gpio_value_t value);
int gpio_get_irq_status(const char *gpio_pin);

#if 0
void gpio_board_init(void);
void gpio_board_status(void);
void gpio_pin_status(char * gpio_name, char *gpio_pin);
#endif

#endif /* _USB_H_ */
