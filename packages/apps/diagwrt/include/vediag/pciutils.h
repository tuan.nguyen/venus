/*
 *	The PCI Library
 *
 *	Copyright (c) 1997--2015 Martin Mares <mj@ucw.cz>
 *
 *	Can be freely distributed and used under the terms of the GNU GPL.
 */

#ifndef _PCI_LIB_H
#define _PCI_LIB_H

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <log.h>

#define __FILENAME__ \
	(strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)


#define SIZEOF_ARRAY(arr) (sizeof(arr) / sizeof((arr)[0]))


#if 1
#define dbg(fmt, args...) do{ \
	xinfo("\033[36m[%s %s() %d] \033[0m", __FILENAME__, __func__, __LINE__);\
	xinfo(fmt, ##args);}while(0)
#else
#define dbg(fmt, args...)
#endif

struct pci_dbdf {
	uint16_t domain;				/* PCI domain (host bridge) */
	uint8_t bus, dev, func;			/* Bus inside domain, device and function */
};

/* From include/uapi/linux/pci_regs.h*/
/*#define PCI_EXP_LNKSTA          18       Link Status */
#define  PCI_EXP_LNKSTA_CLS     0x000f  /* Current Link Speed */
#define  PCI_EXP_LNKSTA_CLS_2_5GB 0x0001 /* Current Link Speed 2.5GT/s */
#define  PCI_EXP_LNKSTA_CLS_5_0GB 0x0002 /* Current Link Speed 5.0GT/s */
#define  PCI_EXP_LNKSTA_CLS_8_0GB 0x0003 /* Current Link Speed 8.0GT/s */
#define  PCI_EXP_LNKSTA_NLW     0x03f0  /* Negotiated Link Width */
#define  PCI_EXP_LNKSTA_NLW_X1  0x0010  /* Current Link Width x1 */
#define  PCI_EXP_LNKSTA_NLW_X2  0x0020  /* Current Link Width x2 */
#define  PCI_EXP_LNKSTA_NLW_X4  0x0040  /* Current Link Width x4 */
#define  PCI_EXP_LNKSTA_NLW_X8  0x0080  /* Current Link Width x8 */
#define  PCI_EXP_LNKSTA_NLW_SHIFT 4     /* start of NLW mask in link status */
#define  PCI_EXP_LNKSTA_LT      0x0800  /* Link Training */
#define  PCI_EXP_LNKSTA_SLC     0x1000  /* Slot Clock Configuration */
#define  PCI_EXP_LNKSTA_DLLLA   0x2000  /* Data Link Layer Link Active */
#define  PCI_EXP_LNKSTA_LBMS    0x4000  /* Link Bandwidth Management Status */
#define  PCI_EXP_LNKSTA_LABS    0x8000  /* Link Autonomous Bandwidth Status */

/* PCIe Base Address        */
#define PCIE0_BASE_ADDR		0x1fe00000
#define PCIE1_BASE_ADDR		0x1fe40000
#define PCIE2_BASE_ADDR		0x1fe80000
#define PCIE3_BASE_ADDR		0x1fec0000
#define PCIE4_BASE_ADDR		0x1ff00000
#define PCIE5_BASE_ADDR		0x1ff40000
#define PCIE6_BASE_ADDR		0x1ff80000
#define PCIE7_BASE_ADDR		0x1ffc0000

#define CONFIG_PCIE_MAX_PORTS		8
#define XGENE_PCIE_MAX_OB_REGIONS	3
#define XGENE_PCIE_MAX_IB_REGIONS	5

enum {
	PTYPE_ENDPOINT = 0x0,
	PTYPE_LEGACY_ENDPOINT = 0x1,
	PTYPE_ROOT_PORT = 0x4,

	LNKW_X1 = 0x1,
	LNKW_X4 = 0x4,
	LNKW_X8 = 0x8,
	LNKW_X16 = 0x10,

	PCIE_GEN1 = 0x0,	/* 2.5G */
	PCIE_GEN2 = 0x1,	/* 5.0G */
	PCIE_GEN3 = 0x2,	/* 8.0G */
	PCIE_GEN_INVALID = 0xFF,
};

enum {
	PCI_PARENT_NOT_FOUND,
	PCI_PARENT_FOUND,
	PCI_DEV_IS_ROOT
};

#define PCI_TEST_OK					(0)
#define PCI_TEST_LNKSTAT_ERR		(-1)
#define PCI_TEST_USR_BREAK			(-2)
#define PCI_TEST_INTERNAL_ERR		(-3)
#define PCI_TEST_BERTCFG_ERR		(-4)
#define PCI_TEST_AER_ERR			(-5)
#define PCI_TEST_VENDORID_ERR		(-6)
#define PCI_TEST_CONFIG_ACCESS_ERR	(-7)
#define PCI_TEST_DEVICE_NOT_PRESENT	(-8)
#define PCI_TEST_RAS_ERR			(-9)

static inline void pci_err_to_string(int err, char* str, int len)
{
	if (err >= 0){
		snprintf(str, len, "No error");
		return;
	}

	if (err == PCI_TEST_LNKSTAT_ERR)
		snprintf(str, len, "Link Status Error");
	else if (err == PCI_TEST_USR_BREAK)
		snprintf(str, len, "User break");
	else if (err == PCI_TEST_INTERNAL_ERR)
		snprintf(str, len, "Internal Error");
	else if (err == PCI_TEST_BERTCFG_ERR)
		snprintf(str, len, "Bert Config Error");
	else if (err == PCI_TEST_AER_ERR)
		snprintf(str, len, "AER Error");
	else if (err == PCI_TEST_VENDORID_ERR)
		snprintf(str, len, "Wrong VendorID");
	else if (err == PCI_TEST_CONFIG_ACCESS_ERR)
		snprintf(str, len, "Config access error");
	else if (err == PCI_TEST_DEVICE_NOT_PRESENT)
		snprintf(str, len, "Device not present");
	else if (err == PCI_TEST_RAS_ERR)
		snprintf(str, len, "RAS Error");
	else
		snprintf(str, len, "Unknown Error");
}

#define PCI_CHECK_GEN_FROM_LNKSTAT_LNKCAP		(~0u)
#define PCI_CHECK_WIDTH_FROM_LNKSTAT_LNKCAP	(~0u)

struct pci_test {
	struct pci_dbdf *dev;
	uint16_t vid;
	unsigned long time;
	uint32_t gen;
	uint32_t width;
	uint32_t corr_mask;
	uint32_t uncorr_mask;
};

int pciutils_ls(int argc, char* argv[]);
int pci_read_config_byte(struct pci_dbdf *d, int offset, uint8_t* value);
int pci_read_config_word(struct pci_dbdf *d, int offset, uint16_t* value);
int pci_read_config_dword(struct pci_dbdf *d, int offset, uint32_t* value);
int pci_write_config_byte(struct pci_dbdf *d, int offset, uint8_t value);
int pci_write_config_word(struct pci_dbdf *d, int offset, uint16_t value);
int pci_write_config_dword(struct pci_dbdf *d, int offset, uint32_t value);
int pci_get_parent(const struct pci_dbdf* dev, struct pci_dbdf* parent);
void pciutils_rescan(void);
int pci_aer_check_chain(struct pci_dbdf *bdf, uint32_t ce_mask,
		uint32_t uc_mask, int verbose);
void pci_aer_clear_chain(struct pci_dbdf *bdf);
void pci_aer_mask_chain(struct pci_dbdf *bdf);
void pci_aer_unmask_chain(struct pci_dbdf *bdf, int all);
int pci_aer_test(struct pci_test* test, int progress);
int pci_parse_dbdf(char* name, struct pci_dbdf* p);
void free_bridge_list(void);
void build_bridge_list(void);
void vediag_pciutils_scan_devices(int force);
int pci_find_ext_capability(struct pci_dbdf *dev, int cap);
int pci_get_aer_default_mask(struct pci_dbdf *bdf, uint32_t* ce_mask,
		uint32_t* uc_mask);
int vediag_pciutils_get_devname(struct pci_dbdf* p, char* buf);

#endif
