
#ifndef _VEDIAG_LED_H_
#define _VEDIAG_LED_H_

#include <log.h>

struct led_test {
	char *name;
	int brightness;
	int max_brightness;
	char *trigger;
	int cycle;
	u32 interval; /* ms */
};

enum {
	LED_BLINK_TEST,
	LED_RING_TEST,
};

int led_blink_test(struct led_test *test, u32 *cycle);
int led_ring_test(struct led_test *test, u32 *cycle);

#endif /* _VEDIAG_LED_H_ */
