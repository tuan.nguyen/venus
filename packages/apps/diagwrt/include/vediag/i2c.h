
#ifndef _VEDIAG_I2C_H_
#define _VEDIAG_I2C_H_

#include <log.h>

#define DEFAULT_I2C_DEV  	"/dev/i2c-0"
#define DISP_LINE_LEN		16

struct i2c_test {
	char i2cdev[60];
	unsigned char i2caddr;
	int gpio_number;	/* GPIO3_4 = 3*32+4 = 100 */
	int gpio_value;		/* 0 | 1 */
	unsigned int pagesize;
	unsigned int addrcycle;
	int backup;			/* Disable = 0 | Enable != 0 */
	int fd;
};

int i2c_write(int fd, unsigned char chip, unsigned int addr, int alen,
		unsigned char * data, int len);
int i2c_read(int fd, unsigned char chip, unsigned int addr, int alen,
		unsigned char * buf, int len);
int i2c_probe(char *i2cdev, char *result);
int i2c_probe_test(struct i2c_test *i2ct, unsigned int offset);
int i2c_readid_test(struct i2c_test *i2ct, unsigned int idoffset, unsigned char identify);

enum {
	I2C_PROBE_TEST,
	I2C_READID_TEST,
	I2C_EEPROM_TEST,
	I2C_TEMPERATURE_TEST,
	I2C_RTC_TEST,
};

int eeprom_init(struct i2c_test *i2ct);
int eeprom_exit(struct i2c_test *i2ct);
int eeprom_read(struct i2c_test *eept, unsigned char *buffer, unsigned int offset, unsigned int length);
int eeprom_write(struct i2c_test *eept, unsigned char *buffer, unsigned int offset, unsigned int length);
int eeprom_test(struct i2c_test *eept, unsigned int offset, unsigned int length);

#endif /* _VEDIAG_I2C_H_ */
