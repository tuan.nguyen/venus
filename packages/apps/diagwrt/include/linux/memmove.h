#ifndef _LINUX_UNALIGNED_MEMMOVE_H
#define _LINUX_UNALIGNED_MEMMOVE_H

#include <types.h>
#include <string.h>

/* Use memmove here, so gcc does not insert a __builtin_memcpy. */

static inline ushort __get_unaligned_memmove16(const void *p)
{
	ushort tmp;
	memmove(&tmp, p, 2);
	return tmp;
}

static inline uint __get_unaligned_memmove32(const void *p)
{
	uint tmp;
	memmove(&tmp, p, 4);
	return tmp;
}

static inline ulong __get_unaligned_memmove64(const void *p)
{
	ulong tmp;
	memmove(&tmp, p, 8);
	return tmp;
}

static inline void __put_unaligned_memmove16(ushort val, void *p)
{
	memmove(p, &val, 2);
}

static inline void __put_unaligned_memmove32(uint val, void *p)
{
	memmove(p, &val, 4);
}

static inline void __put_unaligned_memmove64(ulong val, void *p)
{
	memmove(p, &val, 8);
}

#endif /* _LINUX_UNALIGNED_MEMMOVE_H */
