/*
 * init.h
 */

#ifndef INIT_H_
#define INIT_H_

#define CFG_PRIORITY_INIT_0 	101
#define CFG_PRIORITY_INIT_1		102
#define CFG_PRIORITY_INIT_2		103
#define CFG_PRIORITY_INIT_3		104
#define CFG_PRIORITY_INIT_4		105
#define CFG_PRIORITY_INIT_5		106
#define CFG_PRIORITY_INIT_6		107
#define CFG_PRIORITY_INIT_7		108
#define CFG_PRIORITY_INIT_8		109
#define CFG_PRIORITY_INIT_9		110
#define CFG_PRIORITY_INIT_10	111
#define CFG_PRIORITY_INIT_11	112

/* These are for everybody (although not all archs will actually
   discard it in modules) */
#if 0
#define __init		__section(.init.text) __cold notrace
#define __initdata	__section(.init.data)
#define __initconst	__constsection(.init.rodata)
#define __exitdata	__section(.exit.data)
#define __exit_call	__used __section(.exitcall.exit)
#else
#define __init
#define __initdata
#define __initconst
#define __exitdata
#define __exit_call
#endif

/*
 * Used for initialization calls..
 */
typedef int (*initcall_t)(void);
typedef void (*exitcall_t)(void);

#if 0
#define __define_initcall(fn, id)	\
	static initcall_t __initcall_##fn##id	\
	__attribute__ ((constructor (id))) = fn
#else
#define __define_initcall(fn, id)	\
	static int __attribute__ ((constructor (id))) __initcall_##fn(void)
#endif

/*
 * A "pure" initcall has no dependencies on anything else, and purely
 * initializes variables that couldn't be statically initialized.
 *
 * This only exists for built-in code, not for modules.
 * Keep main.c:initcall_level_names[] in sync.
 */
#define pure_initcall(fn)		__define_initcall(fn, CFG_PRIORITY_INIT_0)

#define core_initcall(fn)		__define_initcall(fn, CFG_PRIORITY_INIT_1)
#define postcore_initcall(fn)	__define_initcall(fn, CFG_PRIORITY_INIT_2)
#define arch_initcall(fn)		__define_initcall(fn, CFG_PRIORITY_INIT_3)
#define subsys_initcall(fn)		__define_initcall(fn, CFG_PRIORITY_INIT_4)
#define fs_initcall(fn)			__define_initcall(fn, CFG_PRIORITY_INIT_5)
#define fs_initcall_sync(fn)	__define_initcall(fn, CFG_PRIORITY_INIT_6)
#define device_initcall(fn)		__define_initcall(fn, CFG_PRIORITY_INIT_7)
#define late_initcall(fn)		__define_initcall(fn, CFG_PRIORITY_INIT_8)
#define test_initcall(fn)		__define_initcall(fn, CFG_PRIORITY_INIT_9)

#define __initcall(fn) device_initcall(fn)

#define __exitcall(fn) \
	static exitcall_t __exitcall_##fn __exit_call = fn

#define console_initcall(fn) \
	static initcall_t __initcall_##fn \
	__used __section(.con_initcall.init) = fn

#define security_initcall(fn) \
	static initcall_t __initcall_##fn \
	__used __section(.security_initcall.init) = fn

/**
 * module_init() - driver initialization entry point
 * @x: function to be run at kernel boot time or module insertion
 *
 * module_init() will either be called during do_initcalls() (if
 * builtin) or at module insertion time (if a module).  There can only
 * be one per module.
 */
#define module_init(x)	__initcall(x)

/**
 * module_exit() - driver exit entry point
 * @x: function to be run when driver is removed
 *
 * module_exit() will wrap the driver clean-up code
 * with cleanup_module() when used with rmmod when
 * the driver is a module.  If the driver is statically
 * compiled into the kernel, module_exit() has no effect.
 * There can only be one per module.
 */
#define module_exit(x)	__exitcall(x)

#endif /* INIT_H_ */
