/*
 * tree.h
 */

#ifndef TREE_H_
#define TREE_H_

#include <linux/list.h>
#include <dirent.h>
#include <sys/stat.h>

#ifndef FILENAME_MAX
#define FILENAME_MAX 260
#endif

typedef struct tree_node {
	char fname[FILENAME_MAX]; /* File name. */
	struct tree_node *parent;
	struct tree_node *child;
	struct list_head sibling;
	struct stat stat;
} tree_node_t;

#define CONFIG_TREE_INDENT	3

/**
 * init log before using
 */
int tree_log_init(int append);

/**
 * Close tree log after using
 */
void tree_log_close(void);

/**
 * Using tree log from another place
 */
struct tree_node *tree_get_log(void);

/**
 * Allocate a node with specified name
 */
struct tree_node *tree_alloc(const char *name);

struct tree_node *tree_alloc_fmt(const char *fmt, ...);

/**
 * Destroy a total tree
 */

void _tree_destroy(struct tree_node **root);
void tree_destroy(struct tree_node **node);

/**
 * Add a sibling to a node
 */
int tree_add_sibling(struct tree_node *newNode, struct tree_node *node);

/**
 * Add a child to a node
 */
struct tree_node *tree_add_node(struct tree_node *parent,
		struct tree_node *node);
struct tree_node *tree_add_node_name(struct tree_node *parent, const char *name);
struct tree_node *tree_trunc_node_name(struct tree_node *node, const char *name);
struct tree_node *tree_trunc_node_name_fmt(struct tree_node *node,
		const char *fmt, ...);
struct tree_node *tree_add_node_fmt(struct tree_node *parent, const char *fmt,
		...);

/**
 * Get root of the tree
 */
struct tree_node *tree_get_root(struct tree_node *node);

/**
 * Get node level
 */
int tree_get_node_level(struct tree_node *node);

/**
 * Iterate over a tree
 */
void _tree_traversal(struct tree_node *node, int indent);
void tree_traversal(struct tree_node *node);

#endif /* TREE_H_ */
