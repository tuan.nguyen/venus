/*
 * hexdump.h
 *
 */

#ifndef HEXDUMP_H_
#define HEXDUMP_H_

enum {
	DUMP_PREFIX_NONE,
	DUMP_PREFIX_ADDRESS,
	DUMP_PREFIX_OFFSET
};
void hex_dump_to_buffer(const void *buf, size_t len, int rowsize, int groupsize,
		char *linebuf, size_t linebuflen, int ascii);
void print_hex_dump(int prefix_type, int rowsize, int groupsize,
		const void *buf, size_t len, int ascii);
void print_hex_dump_bytes(int prefix_type, const void *buf, size_t len);
void log_hex_dump(int prefix_type, int rowsize, int groupsize,
		const void *buf, size_t len, int ascii);
void log_hex_dump_bytes(int prefix_type, const void *buf, size_t len);

#endif /* HEXDUMP_H_ */
