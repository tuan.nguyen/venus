/*
 * binary_tree.h
 *
 */

#ifndef BINARY_TREE_H_
#define BINARY_TREE_H_

#include <linux/types.h>

#define BT_DEFINE(name, ...)	bt_##name(__VA_ARGS__)
#define BT_DEFINE1(name, ...)	struct node *bt_##name(__VA_ARGS__)

struct node {
	int key;
	struct node *left;
	struct node *right;

	void *ptr; // point to user data
};

/**
 * Node case
 */
typedef enum {
	BT_NO_CHILDREN,
	BT_HAS_LEFTTREE_ONLY,
	BT_HAS_RIGHT_TREE_ONLY,
	BT_HAS_BOTH_SUBTREE,
} NODE_DESCENDANT;

/**
 * Allocate a new node
 */
BT_DEFINE1(newNode, int key);
/**
 * Delete node & all it' subtree
 */
void BT_DEFINE(destroy, struct node **node);
/**
 * Check the node to see if it has:
 * 		1. no children
 * 		2. has left subtree only
 * 		3. has right subtree only
 * 		4. has both subtree
 */
NODE_DESCENDANT BT_DEFINE(node_descendant, struct node *node);
/**
 * Find largest key in the tree node
 */
BT_DEFINE1(find_largest, struct node *node);

/**
 * Find smallest key in the tree node
 */
BT_DEFINE1(find_smallest, struct node *node);

/**
 * Insertion:
 * This operation inserts a new node in the binary search tree (BST).
 * While inserting we need to make sure that it does not violate the rule of a BST.
 * Inorder to insert a new node we need to search an empty tree (or subtree in a tree)
 * and then insert the new node.
 * Insertion always takes place as a leaf node.
 * We use the concept of recursion in the insertion function in BST.
 * The function is stated below and some of the important conventions in the algorithm is:-
 */
BT_DEFINE1(insertion, struct node **curNode, int key);

/**
 * Search:
 * If the node to be searched is less than the root, go towards the left subtree in BST.
 * If the node to be searched is greater than the root, than go towards the right subtree of the BST.
 * Follow step 1 & 2 recursively until we find the element to be searched.
 * After traveling the entire BST, if the element is not found then the element does not exist in the BST.
 */
bool BT_DEFINE(search, struct node *root, int key);

/**
 * Get a node from the tree
 */
struct node *bt_getNode(struct node *root, int key);

/**
 * Deletion:
 * If the node to be deleted has no children than we just delete the node.
 * If the node to be deleted has a right subtree then delete the node and
 * 		attach the right subtree with the parent of the deleted node.
 * If the node to be deleted has a left subtree then after deleting the node attach
 * 		the left subtree with the parent of the deleted node.
 * If the node to be deleted has a left & right subtree, then we can delete the node but
 * 		the resultant tree can be unbalanced.
 * 	//Node to be deleted is in the middle of a tree (Condition 4)
 *   	1.Save root in a temporary variable temp
 *  	2.Find the largest element in left subtree
 *   		largest= largestBST( left_subtree);
 *  		Find the smallest element in the right subtree
 * 			smallest=smallestBST( right_subtree);
 *		3.Move the smallest or largest variable to temp and substitute of tree
 */
void BT_DEFINE(deletion, struct node **node, int key);

/**
 * Traversal:
 * In-order
 */
void BT_DEFINE(inorder, struct node *node);

/**
 * Traversal:
 * Pre-order
 */
void BT_DEFINE(preorder, struct node *node);

/**
 * Traversal:
 * Pre-order
 */
void BT_DEFINE(postorder, struct node *node);

#endif /* BINARY_TREE_H_ */
