/*
 * cirbuf.h
 */

#ifndef CIRBUF_H_
#define CIRBUF_H_

#include <pthread.h> /*for mutex*/

/* Opaque buffer element type.  This would be defined by the application. */
typedef struct {
	char buf[128];
} ElemType;

/* Circular buffer object */
typedef struct {
	int size; /* maximum number of elements           */
	int tail; /* index of oldest element              */
	int head; /* index at which to write new element  */
	ElemType *elems; /* vector of elements                   */
	pthread_mutex_t _mutex;
} CircularBuffer;

/* Return count in buffer.  */
#define CIRC_CNT(head,tail,size) (((head) - (tail)) % (size))

/* Return space available, 0..size-1.  We always leave one free char
 as a completely full buffer has head == tail, which is the same as
 empty.
 */
#define CIRC_SPACE(head,tail,size) ((size) - CIRC_CNT((head + 1),(tail),(size)))

/* Return count up to the end of the buffer.  Carefully avoid
 accessing head and tail more than once, so they can change
 underneath us without returning inconsistent results.
 */
#define CIRC_CNT_TO_END(head,tail,size) \
	({int end = (size) - (tail); \
	  int n = ((head) + end) % (size); \
	  n < end ? n : end;})

/* Return space available up to the end of the buffer.
 */
#define CIRC_SPACE_TO_END(head,tail,size) \
	({int end = (size) - 1 - (head); \
	  int n = (end + (tail)) % (size); \
	  n <= end ? n : end+1;})

int cbInit(CircularBuffer *cb, int size);
void cbFree(CircularBuffer *cb);
int cbIsFull(CircularBuffer *cb);
int cbIsEmpty(CircularBuffer *cb);
void cbWrite(CircularBuffer *cb, ElemType *elem);
void cbRead(CircularBuffer *cb, ElemType *elem);
#endif /* CIRBUF_H_ */
