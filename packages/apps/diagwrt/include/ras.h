/*
 * ras.h
 *
 *  Created on: Jun 13, 2017
 */

#ifndef INCLUDE_RAS_H_
#define INCLUDE_RAS_H_

#include <circbuf.h>

void ras_init(void);
void ras_exit(void);

#define RAS_PCI		"PCI"
#define RAS_SATA	"SATA"
#define RAS_USB		"USB"
#define RAS_DDR		"MCU"

/*cpu, l2, l3, iob, xgic, smmu, mpa, smpro, pmpro, ahbc, trace, menet, tcu, ocm*/
extern CircularBuffer ras_cb_system;
/*pci*/
extern CircularBuffer ras_cb_pci;
/*sata, usb*/
extern CircularBuffer ras_cb_stor;
/*ddr*/
extern CircularBuffer ras_cb_ddr;
#endif /* INCLUDE_RAS_H_ */
