/*
 * memory.h
 *
 */

#ifndef MEMORY_H_
#define MEMORY_H_

/*
 * These function are used for read/write real address
 */
extern void reg_write(unsigned long addr, unsigned int val, u32 len);
extern unsigned int reg_read(unsigned long addr, u32 len);
#define writel(v, x)    reg_write((unsigned long)x, v, 4)
#define readl(x)   		reg_read((unsigned long)x, 4)
#define writew(v, x) 	reg_write((unsigned long)x, v, 2)
#define readw(x)   		reg_read((unsigned long)x, 2)
#define writeb(v, x) 	reg_write((unsigned long)x, v, 1)
#define readb(x)   		reg_read((unsigned long)x, 1)

#endif /* MEMORY_H_ */
