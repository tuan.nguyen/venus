/*
 * bitsperlong.h
 *
 */

#ifndef BITSPERLONG_H_
#define BITSPERLONG_H_

#if defined(__x86_64__) || defined(__aarch64__)
#define BITS_PER_LONG 64
#else
#define BITS_PER_LONG 32
#endif /* CONFIG_64BIT */

#ifndef BITS_PER_LONG_LONG
#define BITS_PER_LONG_LONG 64
#endif

#endif /* BITSPERLONG_H_ */
