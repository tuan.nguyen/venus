/*
 * vediagmod.h
 *
 */

#ifndef VEDIAG_MODULE_H_
#define VEDIAG_MODULE_H_

#include <linux/ioctl.h>
#include <pthread.h>

#ifdef VEDIAG_DBG
#define vediagdbg(fmt,args...)	printk(KERN_INFO "vediagmod:" fmt, ##args)
#else
#define vediagdbg(fmt,args...) do {} while (0)
#endif

/*
 * The major device number. We can't rely on dynamic
 * registration any more, because ioctls need to know
 * it.
 */
#define VEDIAG_MAJOR_NUM 22

/*
 * The name of the device file
 */
#define VEDIAG_DEV_NAME "vediagdev"

#endif /* VEDIAG_MODULE_H_ */
