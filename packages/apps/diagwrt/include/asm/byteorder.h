/*
 * byteorder.h
 *
 */

#ifndef BYTEORDER_H_
#define BYTEORDER_H_

#include <endian.h>

#if (__BYTE_ORDER == __LITTLE_ENDIAN)
#include <linux/byteorder/little_endian.h>
#elif (__BYTE_ORDER == __BIG_ENDIAN)
#include <linux/byteorder/big_endian.h>
#else
#error "error in byteorder"
#endif

#include <linux/byteorder/generic.h>

#endif /* BYTEORDER_H_ */
