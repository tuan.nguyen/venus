/*
 * cmd.h
 */

#ifndef CMD_H_
#define CMD_H_

#include <linux/compiler.h>
#include <linux/init.h>
#include <linux/list.h>
#define CONFIG_LONGHELP

enum {
	COMMAND_OK,
	COMMAND_ERROR
};

/*
 * Command Table
 */
struct cmd_tbl_s {
	char *name; /* Command Name			*/
	int maxargs; /* maximum number of arguments	*/
	int repeatable; /* autorepeat allowed?		*/
	/* Implementation function	*/
	int (*cmd)(struct cmd_tbl_s *, int, int, char *[]);
	char *usage; /* Usage message	(short)	*/
#ifdef	CONFIG_LONGHELP
	char *help; /* Help  message	(long)	*/
#endif
#ifdef CONFIG_AUTO_COMPLETE
/* do auto completion on the arguments */
	int (*complete)(int argc, char *argv[], char last_char, int maxv, char *cmdv[]);
#endif
	struct list_head lst;
};

typedef struct cmd_tbl_s	cmd_tbl_t;
#define CONSTRUCTOR_CMD(name, cmd, id)	\
		void  __constructor(id) __constructor_##name(void) {	\
			add_cmd(cmd);	\
}

#ifdef  CONFIG_LONGHELP
#define VEDIAG_CMD(name,maxargs,rep,cmd,usage,help) \
cmd_tbl_t __var_cmd_##name = {#name, maxargs, rep, cmd, usage, help};	\
CONSTRUCTOR_CMD(name, &__var_cmd_##name, CFG_PRIORITY_INIT_10)
#else	/* no long help info */
#define VEDIAG_CMD(name,maxargs,rep,cmd,usage,help) \
cmd_tbl_t __var_cmd_##name = {#name, maxargs, rep, cmd, usage};	\
CONSTRUCTOR_CMD(name, &__var_cmd_##name, CFG_PRIORITY_INIT_10)
#endif	/* CONFIG_SYS_LONGHELP */

void add_cmd(cmd_tbl_t *cmd);
void remove_cmd(cmd_tbl_t *cmd);
int cmd_usage(cmd_tbl_t *cmdtp);
cmd_tbl_t *find_cmd (const char *cmd);
int cmd_get_data_size(char* arg, int default_size);

#endif /* CMD_H_ */
