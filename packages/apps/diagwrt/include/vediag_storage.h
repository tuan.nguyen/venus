/*
 * vediagnet.h
 *
 */

#ifndef VEDIAG_NET_H_
#define VEDIAG_NET_H_

/* Include for openning socket to get ipaddress */
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>

#define SYS_ERROR_MESSAGE_SIZE			256
#define STORAGE_SECTOR_SIZE				512
#define STORAGE_PARTITION_NUM_SUPPORT	8
#define STORAGE_INT_FIO_PATH			"/etc/diagwrt"
#define STORAGE_INT_STRESSAPP_PATH		"/etc/diagwrt"
#define STORAGE_FILE_TEST_NAME			"diag_stor"

#define STORAGE_DEFAULT_THREADS			1
#define STORAGE_DEFAULT_LOOP			10
#define STORAGE_DEFAULT_RUNTIME			30
#define STORAGE_DEFAULT_TEST_SIZE		(500 << 20)//0x80000000ULL //1GB
#define STORAGE_DEFAULT_OFFSET   		0 //0x100000000ULL // offset from 8GB

#define STORAGE_DEFAULT_RAW_CHUNK_SIZE			(10 << 20)
#define STORAGE_DEFAULT_FIO_CHUNK_SIZE			(10 << 20)
#define STORAGE_DEFAULT_STRESSAPP_CHUNK_SIZE	(10 << 20)

/* Initial value -> if diff. them, it means user input. Otherwise it means use config file
 * In the case config file has invalid value, it use default value
 */
#define STORAGE_INITIAL_DEVMASK   		0xFFFFFFFFULL
#define STORAGE_INITIAL_OFFSET   		~0ULL
#define STORAGE_INITIAL_TEST_SIZE		0
#define STORAGE_INITIAL_THREADS			0
#define STORAGE_INITIAL_RUNTIME			0
#define STORAGE_INITIAL_LOOP			0
#define STORAGE_INITIAL_CHUNK_SIZE		0
#define STORAGE_TEST_DISABLE			"No"




typedef enum {
	STORAGE_NONE	= 0,
	STORAGE_DET,			/* Card Detection error */
	STORAGE_SCAN,			/* Scan card/device error */
	STORAGE_SEEK,			/* Seek error */
	STORAGE_ERASE,			/* Erase error */
	STORAGE_DEVCTRL,		/* Device control error */
	STORAGE_ALLOCMEM,		/* Allocate memory */
	STORAGE_RD_BK,			/* Read for back-up test error */
	STORAGE_WR,				/* Write test error */
	STORAGE_RD,				/* Read test error */
	STORAGE_VERIFY,			/* Verify/compare data test error */
	STORAGE_WR_BK,			/* Write for back-up test error */
	STORAGE_LINK,			/* Error when running link up test */
	STORAGE_INIT_PHY,		/* Error when initialization PHY */
	STORAGE_INIT_PART,		/* Error when initialization PARTNER */
	STORAGE_INIT_CTL,		/* Error when initialization CONTROLLER */
	STORAGE_BOOT,			/* Error when booting */
	STORAGE_KBOOT,			/* Error when booting kernel */
	STORAGE_RAS_ERR,		/* RAS Hardware error */
} err_apps_t;

enum {
	STORAGE_RW_TEST,

	STORAGE_FIO_TEST,			//auto-sel fio from exist app or internal app
	STORAGE_EXTERNAL_FIO_TEST,	//server has installed FIO
	STORAGE_INTERNAL_FIO_TEST,	//fio app built and package in diagwrt

	STORAGE_STRESSAPP_TEST,
	STORAGE_EXTERNAL_STRESSAPP_TEST,
	STORAGE_INTERNAL_STRESSAPP_TEST,
};

enum {
	STORAGE_MODE_RAW_TEST,
	STORAGE_MODE_FILE_TEST
};

struct vediag_storage_partition_info {
	char name[64];
	int type;
	int bootable;
	int rootfs;
	unsigned long long start_sector;
	unsigned long long end_sector;
	unsigned long long total_size;
	unsigned long long used_size;
	unsigned long long free_size;
};

struct partition {
        unsigned char boot_flag;        /* 0 = Not active, 0x80 = Active */
        unsigned char chs_begin[3];
        unsigned char sys_type;         /* For example : 82 --> Linux swap, 83 --> Linux native partition, ... */
        unsigned char chs_end[3];
        unsigned char start_sector[4];
        unsigned char nr_sector[4];
};


struct vediag_storage_test_info {
	/* info */
	char *interface_name;
	char *diskpath;			/*/dev/sdX */
	char *testpath;			/* folder for file test */
	char *devpath;			/* logical path created by kernel when enumerate device */
	char *phypath;			/* physical path */
	char outfile[TEXT_LINE_SIZE];
	unsigned long long disk_size;

	int partition_num;
	struct vediag_storage_partition_info partition[STORAGE_PARTITION_NUM_SUPPORT];

	/* test info */
	char *apppath;			/* location of app need to call to test */
	int test_performance;
	int ext_check_err;		/* Check error from first parent interface */
};


/************************************************ DEF for performance test ****************************************/
enum {
	SEQ_READ	= 0,
	SEQ_WRITE,
	RAN_READ,
	RAN_WRITE,
};

enum {
	PCI_NVME_STORAGE,
	PCI_M2_STORAGE,
	USB_STORAGE,
	SSD_STORAGE,
	HDD_STORAGE,
	UNKNOWN_STORAGE,
};

struct stor_info {
	unsigned char id_model[128];
	unsigned char id_revision[128];
	unsigned char id_serial[128];
};

struct stor_test_result {
	int errors;

	int rddone;
	unsigned long long rdsize;		//byte
	unsigned long long rdspd_avr;	//bit/s
	unsigned long long rdspd_max;
	unsigned long long rdspd_min;

	int wrdone;
	unsigned long long wrsize;
	unsigned long long wrspd_avr;
	unsigned long long wrspd_max;
	unsigned long long wrspd_min;
};

struct stor_test_perf_result {
	struct stor_test_result	sequential;
	struct stor_test_result	random;
};

struct stor_test_avl {
	char *name;							//ID_MODEL (USB/SATA) or ID_SERIAL (NVME)
	char *vendor;						//ID_VENDOR
	char *rev;							//ID_REVISION
	int type;
	unsigned long long sizeGB;
	unsigned long long seq_rdMBs;
	unsigned long long seq_wrMBs;
	unsigned long long ran_rdMBs;
	unsigned long long ran_wrMBs;
	int validated;						//Speed is validated by Validation/Diag Team or specs
};


/************************************************ LIB for vepath_device ********************************************/
#define VEDIAG_MAX_STR_ARG_SIZE	TEXT_LINE_SIZE

struct vepath_attr {
	char *name;
	char *value;
};

struct vepath_device {
	int index;				/* level of device in path */
	char *name;
	char *path;				/* logical path (system path) */

	char *subsystems;
	char *subsystems_path;
	char *drivers;
	char *drivers_path;
	char *firmware_node;
	char *firmware_node_path;

	struct vepath_attr *attrs;

	struct list_head relations;
};

char *vepath_alloc_n_get_devpath(char *diskpath);
int vepath_scan_device(struct vepath_device *dev, char *diskpath);


/************************************************ LIB for PHY path ********************************************/
struct acpi_ip {
	char *acpi_name;
	char *ip_name;
};

#define ACPI_IP_TABLE {\
}

struct omap_hsmmc_ip {
	char *omap_hsmmc_name;
	char *omap_hsmmc_port;
};

#define OMAP_HSMMC_TABLE {\
	{ "48060000.mmc", "0" },\
	{ "481d8000.mmc", "1" },\
}

void vepath_scan_phypath(char *result, struct vepath_device *dev);
char *vepath_alloc_n_get_phypath(char *diskpath);

#endif
