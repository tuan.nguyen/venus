/*
 * vediagmod.h
 *
 */

#ifndef VEDIAG_COMMON_H_
#define VEDIAG_COMMON_H_

#include <linux/types.h>
#include <pthread.h>

#define F1_KEY				"\e[11~"
#define F2_KEY				"\e[12~"
#define F3_KEY				"\e[13~"
#define F4_KEY				"\e[14~"
#define F5_KEY				"\e[15~"
#define F6_KEY				"\e[16~"

#define F7_KEY				"\e[18~"
#define F8_KEY				"\e[19~"
#define F9_KEY				"\e[20~"

#define GUI_KEY_SEQ			F1_KEY
#define CMD_KEY_SEQ			F2_KEY
#define COLLAPSE_KEY_SEQ	F3_KEY
#define STOP_KEY_SEQ		F4_KEY
#define REFRESH_KEY_SEQ		F5_KEY
#define YES_KEY_SEQ			F8_KEY
#define NO_KEY_SEQ			F9_KEY

enum {
	VEDIAG_SYSTEM = 0,
	VEDIAG_NET,
	VEDIAG_SGMII,
	VEDIAG_RGMII,
	VEDIAG_XFI,
	VEDIAG_USB,
	VEDIAG_SATA,
	VEDIAG_MEM,
	VEDIAG_PCIE,
	VEDIAG_LED,
	VEDIAG_EVENT,
	VEDIAG_I2C,
	VEDIAG_AUDIO,
	VEDIAG_MICRO,
	VEDIAG_BLUETOOTH,
	VEDIAG_FIRMWARE,
	VEDIAG_ZIGBEE,
	VEDIAG_ZWAVE,
	VEDIAG_SDIO,
	VEDIAG_STORAGE,
	VEDIAG_SENSOR,
	VEDIAG_DEMO = 99,
};

#define VEDIAG_INTERFACES { \
    {VEDIAG_SYSTEM, "SYSTEM" }, \
    {VEDIAG_MEM, "MEMORY" }, \
    {VEDIAG_SATA, "SATA"}, \
    {VEDIAG_NET, "NET"}, \
    {VEDIAG_SGMII, "SGMII"}, \
    {VEDIAG_RGMII, "RGMII"}, \
    {VEDIAG_XFI, "XFI"}, \
    {VEDIAG_PCIE, "PCIE"}, \
	{VEDIAG_LED, "LED"}, \
	{VEDIAG_EVENT, "EVENT"}, \
	{VEDIAG_I2C, "I2C"}, \
	{VEDIAG_AUDIO, "AUDIO"}, \
	{VEDIAG_MICRO, "MICRO"}, \
	{VEDIAG_BLUETOOTH, "BLUETOOTH"}, \
	{VEDIAG_FIRMWARE, "FIRMWARE"}, \
	{VEDIAG_ZWAVE, "ZWAVE"}, \
	{VEDIAG_ZIGBEE, "ZIGBEE"}, \
    {VEDIAG_SDIO, "SDIO"}, \
    {VEDIAG_USB, "USB"}, \
    {VEDIAG_DEMO, "DEMO"}, \
    {VEDIAG_STORAGE, "STORAGE"}, \
    {-1, NULL}, \
}

const char *vediag_get_IP_name(int IP);
int xprintf(const char *fmt, ...);
void vediag_err(int IP, char* dbgInfo, const char *fmt, ...);
void vediag_info(int IP, char* dbgInfo, const char *fmt, ...);
void vediag_warn(int IP, char* dbgInfo, const char *fmt, ...);
void vediag_debug(int IP, char* dbgInfo, const char *fmt, ...);
int vediag_getopt(int nargc, char * const *nargv, const char *ostr);
void lock_print(void);
void unlock_print(void);

#define TEXT_LINE_SIZE					1024

/* INI parser */
#define VEDIAG_SYSTEM_CONFIG_FILE_PATH "/etc/diagwrt/system.ini"
#define VEDIAG_SYSTEM_CONFIG_FILE      "system.ini"
#define VEDIAG_TEST_CONFIG_FILE_PATH	  "/etc/diagwrt/testall.ini"
#define VEDIAG_TEST_CONFIG_FILE        "testall.ini"
#define VEDIAG_DEVICE_NUM_KEY          "Total"
#define VEDIAG_SENSOR_NAME_KEY         "sensor"
#define VEDIAG_SENSOR_INTERVAL_KEY     "sensor_interval"
#define VEDIAG_DEVICE_KEY              "Device"
#define VEDIAG_DEVICED_KEY             "Deviced"
#define VEDIAG_PHYSICAL_KEY            "Physical"
#define VEDIAG_APPLICATION_KEY         "App"
#define VEDIAG_ARGUMENT_KEY            "Argument"
#define STRESSAPPTEST_LOCAL 		  "/etc/diagwrt/stressapptest"

char *get_section_type(int type);
int vediag_check_system_config(void);
int vediag_check_test_config(void);
char * vediag_parse_system(const char *key);
char * vediag_parse_system_section(const char *section, const char *key);
char * vediag_parse_config(const char *cfg_file, const char *section, const char *key);
int vediag_update_config(const char *cfg_file, const char *section, const char *key, char *value);
int vediag_update_system_config(const char *section, const char *key, char *value);
int vediag_add_identified_device(int type, char *device, char *physical);
char * vediag_find_device(int dev_type, const char *physical_dev);
char * vediag_find_physical_device(int dev_type, const char *device);
int vediag_get_test_arguments(char *cfg_file, char *section, struct list_head *arguments);

void vediag_scan_memory_device(int force);
void vediag_scan_storage_device(int force);
void vediag_scan_network_device(int force);
void vediag_scan_pci_device(int force);
void vediag_scan_led_device(int force);
void vediag_scan_event_device(int force);
void vediag_scan_i2c_device(int force);
void vediag_scan_audio_device(int force);
void vediag_scan_micro_device(int force);
void vediag_scan_bluetooth_device(int force);
void vediag_scan_firmware_device(int force);
void vediag_scan_zwave_device(int force);
void vediag_scan_zigbee_device(int force);

#define DIAG_MAXIMUM_TEST		24
#define VEDIAG_MAX_THREADS		64

typedef enum {
	VEDIAG_NOERR = 0,
	VEDIAG_STOP,
	VEDIAG_NOMEM,
	VEDIAG_NODEV,
	VEDIAG_NOCFG,
	VEDIAG_THREAD,
	VEDIAG_TEST_ERR,
}xerr;

struct vediag_test_status {
	u16 mtid;						/* Master test ID */
	u16 stid;						/* Slave test ID */
	int index;						/* Test number index */
	int pid;						/* Process ID of test */
	int remain;						/* Runtime remain */
	int percent;					/* Percent of task */
	int comp;						/* Runtime complete */
	int pass;						/* Pass time */
	int runtimes;
	int threads;					/* Number of threads */
	struct timeval start;			/* Time start test */
	struct timeval now;				/* Current time of test */
	struct list_head status;		/* Add to global list */
#ifdef VEDIAG_MANAGER_USING_MUTEX
	pthread_mutex_t mutex;
#else
	pthread_spinlock_t lock;
#endif
	int stat;						/* Status if test */
	int result;
	int should_suspend;
	int should_stop;
	u64	size;						/* Size of test */
	char result_detail[24];
	char name[16];
	char desc[16];
	char app[8];
#define VEDIAG_TEST_STATUS_CREATED	0x00000001
#define VEDIAG_TEST_STATUS_UPDATED	0x00000002
#define VEDIAG_TEST_RESULT_UPDATED	0x00000004
#define VEDIAG_TEST_STATE_CHANGED	0x00000008
#define VEDIAG_TEST_STATE_COMP		0x00000010
#define VEDIAG_TEST_STATE_FAIL		0x00000020
	int flags;
	int test_type;
	int performance;
	int finish;
	int refcnt;
	struct vediag_test_status *child;
	struct vediag_test_status *next;
	char message[24];
};

struct vediag_test_control {
	pthread_mutex_t mutex;
	int test_type;
	int runtime;
	u64 tested_size;
	int phy_complete;
	int data_running;
	int data_stop;
	int transfering;
	int test_restart;
	int error;
	char err_msg[24];
	void *test_data;
	void *phy_data;
};

#define MAX_MESS_DBG_INFO	128
struct vediag_test_info {
	char interface[16];
	char dbg_info[MAX_MESS_DBG_INFO]; /*store debug info when print out*/
	char *cfg_file;
	char *device;
	char *deviced;
	char *physical_dev;
	char *application;
	int test_app;
	struct list_head arguments;
	int test_type;
	int test_mode;
	char *enable;
	int threads;
	int core_nums;
	int runtime;
	int performance;
	u32 dev_mask;
	u32 depend_dev_mask;
	u64 offset;
	u64 total_size;
	u64 chunk_size;
#define VEDIAG_TEST_FULL_SIZE		0x1
#define VEDIAG_TEST_PERFORMANCE		0x2
	u32 flags;
	struct vediag_test_status *stat;
	struct vediag_test_control *test_ctrl;
	void *phy_data;
	void *data;
};

struct vediag_test_device {
	int type;			/* Type of test */
	char *name;
	char *cmd;			/* Command to do this test */
	char *desc;			/* Descriptor of test */
	char *default_app;	/* Default test app */
#define VEDIAG_SUPPORT_PHY_TEST		0x1
#define VEDIAG_TEST_CASE_TYPE		0x2
#define VEDIAG_TEST_MANUAL			0x4
	int flags;			/* Flags */
	/* Function to execute this test */
	int (*test_finish) (void *dev_info);
	int (*create_argument) (struct list_head *args);
	int (*get_test_app) (char *app);
	int (*get_num_dev) (struct vediag_test_info *info);
	int (*test_start) (int argc,char *argv[], void *dev_info);
	int (*test_control) (void *info);
	int (*check_dev) (struct vediag_test_info *info, struct vediag_test_info *dev_info, char **error);
	void *(*test_execute) (void *dev_info);
	int (*clear_error) (void *dev_info);
	int (*check_error) (void *dev_info);
	int (*test_complete) (void *data);
	struct list_head list;		/* Use to add to system */
};

struct vediag_test_argument {
	char *key;
	char *value;
	struct list_head entry;
};

#define VEDIAG_REPORT_NO_UPDATE			0
#define VEDIAG_REPORT_SCAN				1
#define VEDIAG_REPORT_READ_WRITE			2

#define VEDIAG_REPORT_NOT_START			0
#define VEDIAG_REPORT_EXCUTING			1
#define VEDIAG_REPORT_SUSPEND			2
#define VEDIAG_REPORT_FINISH				3

#define VEDIAG_REPORT_REQUEST_RETURN			0
#define VEDIAG_REPORT_UPDATE_STATUS			1
#define VEDIAG_REPORT_CREATE_NEW_ENTRY		2
#define VEDIAG_REPORT_UPDATE_RESULT			3

char *vediag_get_argument(struct vediag_test_info *dev_info, char *key);
void vediag_set_argument(char *key, char *value, struct list_head *arguments);
char *vediag_get_default_application(int test_type);
void vediag_get_default_argument(int test_type, struct list_head *arg_list);
void vediag_free_test_info(struct vediag_test_info *dev_info);

#if defined(CONFIG_VEDIAG_TEST_MANAGER)
void vediag_gui_connect(int connected);
int vediag_test_manager_init(void);
int vediag_test_register(struct vediag_test_device *new);
struct vediag_test_status *vediag_add_new_status(u16 mtid, u16 stid, char *name, char *desc);
int vediag_demo_test_register(void);
int vediag_storage_test_register(void);
void vediag_setup_device_dbg_info(struct vediag_test_info *dev_info);
#endif

#if defined(CONFIG_VEDIAG_TEST_GUI)
#define VEDIAG_SIMPLE_REPORT				0
#define VEDIAG_FULL_REPORT				1
#define VEDIAG_SYSTEM_REPORT				2

struct vediag_system_report {
	int temp_num;
	int pow_num;
	int temp[10];
	float fpow[4];
} __attribute__ ((packed));

#define REPORT_STATE_CHANGE		0x00000001
struct vediag_report {
	u16 mtid;
	u16 stid;
	u16	index;
	u16 rp_type;
	union {
		struct {
			int result;
			u32 rsv[9];
		} req_return;
		struct {
			int percent;
			int runtime;
			int state;
			int result;
			u32 size_l;
			u32 size;
			int flags;
			int type;
			u32 rsv[3];
		} status;
		struct {
			int threads;
			char name[16];
			char detail[16];
			char app[8];
		} new_entry;
		struct {
			int result;
			int percent;
			char result_detail[24];
			u32 rsv[4];
		} result;
	} detail;
} __attribute__ ((packed));

struct vediag_full_report {
	u16 mtid;
	u16 stid;
	u16 index;
	u16 percent;
	int result;
	int threads;
	int runtimes;
	int state;
	u32 size_l;
	u32 size;
	char name[16];
	char detail[16];
	char app[8];
	char status[24];
	int flags;
} __attribute__ ((packed));

int vediag_gui_init(void);
int vediag_gui_add_report(int type, void *report);
const char *vediag_report_get_result(int result);
bool vediag_check_gui_mode(void);
int is_diag_running(void);
int stop_test_func(int count, int key);
#endif

/*
 * Environment
 */
char *xgetenv(const char *name);
int xsetenv(const char *varname, const char *varvalue);
void env_relocate(void);
unsigned long getenv_ulong(const char *name, int base,
		unsigned long default_val);
int get_env_id(void);
int set_env_id(void);
int env_init(void);
int run_command(const char *cmd);
int check_key_result(int IP, char *device, int remind, int interval);

#endif /* VEDIAG_COMMON_H_ */
