1. How to build diagwrt
cd openwrt/
make package/verik/diagwrt/{clean,compile,install} V=s -j1

2. Manual generate ipk for omap architect
cd ../packages/apps/diagwrt/ipk
./ipk.sh

3. Install
opkg install diagwrt-0.2.ipk

4. Execute diagwrt --> Read Diagwrt User Manual
cd /etc/diagwrt
./venus_setup.sh

5. Edit test config file.
vi venus_setup.sh --> Update SSID/password
cd /etc/diagwrt
vi venus_reve_testall.ini --> Update wifi config file, iperf server.
