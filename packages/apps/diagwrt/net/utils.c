/*
 * net utilities
 */

#include <common.h>
#include <listbuff.h>
#include <dirent.h>
#include <vediag_net.h>

#define I210_DRIVER_NAME 		"igb"
#define E1000_DRIVER_NAME 		"e1000e"
#define XGENE_RGMII_DRIVER_NAME "xgene-enet-v2"
#define PCIE_DRIVER_NAME 		"pcieport"
#define NET_FOLDER_NAME 		"net"

/* Token */
#define DRIVER_TOKEN 			"DRIVER"
#define PCI_CLASS_TOKEN 		"PCI_CLASS"
#define PCI_ID_TOKEN 			"PCI_ID"
#define PCI_SUBSYS_ID_TOKEN 	"PCI_SUBSYS_ID"
#define PCI_SLOT_NAME_TOKEN 	"PCI_SLOT_NAME"
#define MODALIAS_TOKEN 			"MODALIAS"
#define INTERFACE_TOKEN 		"INTERFACE"
#define IFINDEX_TOKEN 			"IFINDEX"

#define CURRENT_DIR_TOKEN		"."
#define PREV_DIR_TOKEN			".."

struct listbuff *get_device_path_of_interface(const char *intf)
{
	struct listbuff *lb = NULL, *tmp;
	struct listbuff *lbInOne = NULL;
	char *line;
	char buff[1024];
	int count;
	FILE *p_file = NULL;
	memset(buff, 0, sizeof(buff));
#if 0
	sprintf(buff, "grep -s -r \"%s\" /sys/devices", intf);
#else
	sprintf(buff, "find /sys/devices -name \"%s\"", intf);
#endif
	p_file = popen(buff, "r");
	if (likely(p_file != NULL)) {
		while (1) {
			memset(buff, 0, sizeof(buff));
			line = fgets(buff, sizeof(buff), p_file);
			if (line == NULL) {
				break;
			} else {
				tmp = import_listbuff(line, strlen(line));
				if (lb == NULL) {
					lb = tmp;
				} else {
					listbuff_add_tail(tmp, lb);
				}
			}
		}

		pclose(p_file);
	}

	if (lb != NULL) {
		/** ADD NULL */
		memset(buff, 0, sizeof(buff));
		count = sprintf(buff, "%c", '\0');
		tmp = import_listbuff(buff, count);
		listbuff_add_tail(tmp, lb);

		lbInOne = listbuff_all_in_one(lb);
		free_listbuff_list(&lb);
	}

	return lbInOne;
}

/**
 * Get parent of the specified names <code>names</code>
 */
struct listbuff *get_dir(const char *names)
{
	struct listbuff *lb = NULL;
	char *lastslash = NULL;
	char *source, *dest;
	int foundpos;
	int clen = strlen(names);
	if (names[clen - 1] == '/') {
		clen -= 1;
	}

	source = (char *) malloc(clen + 1);
	memcpy(source, names, clen);
	source[clen] = '\0';

	lastslash = strrchr(source, '/');
	if (lastslash != NULL) {
		foundpos = lastslash - source;
		dest = (char *) malloc(foundpos + 1);
		strncpy(dest, source, foundpos);
		dest[foundpos] = '\0';
		lb = import_listbuff(dest, foundpos + 1);
		free(dest);
	}

	free(source);
	return lb;
}

struct listbuff *get_token_value_from_uevent(const char *path,
		const char *token)
{
	struct listbuff *lb = NULL;
	int len = strlen(path);
	if (path[len - 1] == '/' || path[len - 1] == '\n') {
		len--;
	}

	int flen = strlen("/uevent");
	int tlen = len + flen;
	char *filepath = (char *) malloc(tlen + 1);
	memcpy(filepath, path, len);
	memcpy(filepath + len, "/uevent", flen);
	filepath[tlen] = '\0';

	FILE *file = fopen(filepath, "r");
	if (file == NULL) {
		free(filepath);
		return NULL;
	}

	int c;
	int size = 1024;
	int idx;
	char *buff = (char *) malloc(size);
	char *tmpbuff;
	int tmpsize;
	int tokenlen = strlen(token);
	int valuepos = tokenlen + 1;
	do {
		idx = 0;
		do {
			c = fgetc(file);
			if (c != EOF && c != '\n')
				buff[idx++] = (char) c;
			else
				break;

		} while (1);
		buff[idx] = '\0';

		if (strstr(buff, token) != NULL) {
			tmpsize = idx - valuepos;
			tmpbuff = (char *) malloc(tmpsize + 1);
			memcpy(tmpbuff, buff + valuepos, tmpsize);
			tmpbuff[tmpsize] = '\0';

			lb = import_listbuff(tmpbuff, tmpsize + 1);
			free(tmpbuff);

			break;
		}

	} while (c != EOF);

	free(buff);
	fclose(file);
	free(filepath);
	return lb;
}

int get_portindex_of_eth(const char *path)
{
	int numport = 0;
	int ifindex = 0;
	int ifindex1 = 0, ifindex2;
	DIR *dir;
	struct dirent *d;
	char *fpath;
	int tlen;

	struct listbuff *tmp = get_token_value_from_uevent(path, IFINDEX_TOKEN);
	if (tmp != NULL) {
		ifindex = strtoul(tmp->buff, NULL, 0);
		ifindex1 = ifindex;
		free_listbuff_list(&tmp);
	}

	struct listbuff *parent = get_dir(path);
	if (unlikely(parent == NULL)) {
		return 0;
	}

	dir = opendir(parent->buff);
	if (dir) {
		while ((d = readdir(dir)) != NULL) {
			if ((strcmp(d->d_name, CURRENT_DIR_TOKEN) == 0)
					|| (strcmp(d->d_name, PREV_DIR_TOKEN) == 0)) {
				continue;
			}

			tlen = strlen(parent->buff) + strlen("/") + strlen(d->d_name);
			fpath = (char *) malloc(tlen + 1);
			strncpy(fpath, parent->buff, strlen(parent->buff));
			strncpy(fpath + strlen(parent->buff), "/", 1);
			strncpy(fpath + strlen(parent->buff) + 1, d->d_name, strlen(d->d_name));
			fpath[tlen] = '\0';
			tmp = get_token_value_from_uevent(fpath, IFINDEX_TOKEN);
			if (tmp != NULL) {
				ifindex2 = strtoul(tmp->buff, NULL, 0);
				if (ifindex2 < ifindex1) {
					ifindex1 = ifindex2;
				}
				free_listbuff_list(&tmp);
			}

			free(fpath);
			numport++;
		}

		closedir(dir);
	}

	free_listbuff_list(&parent);
	return (numport > 1) ? (ifindex - ifindex1) : -1;
}

/**
 * Warning! friendly name don't have NULL character
 * So, don't export outside, just internal using
 */
static struct listbuff *get_driver_friendly_name(const char *drvname)
{
	int n;
	char buff[32];
	struct listbuff *lb = NULL;
	if (strcmp(drvname, MELLANOX_10G_DRIVER_NAME) == 0) {
		memset(buff, 0, sizeof(buff));
		n = sprintf(buff, MELLANOX_10G_NAME);
		lb = import_listbuff(buff, n);
	} else if (strcmp(drvname, MELLANOX_100G_DRIVER_NAME) == 0) {
		memset(buff, 0, sizeof(buff));
		n = sprintf(buff, MELLANOX_100G_NAME);
		lb = import_listbuff(buff, n);
	} else if (strcmp(drvname, E1000_DRIVER_NAME) == 0) {
		memset(buff, 0, sizeof(buff));
		n = sprintf(buff, "E1000e");
		lb = import_listbuff(buff, n);
	} else if (strcmp(drvname, I210_DRIVER_NAME) == 0) {
		memset(buff, 0, sizeof(buff));
		n = sprintf(buff, "I210");
		lb = import_listbuff(buff, n);
	} else if (strcmp(drvname, XGENE_RGMII_DRIVER_NAME) == 0) {
		memset(buff, 0, sizeof(buff));
		n = sprintf(buff, "RGMII");
		lb = import_listbuff(buff, n);
	} else {
		memset(buff, 0, sizeof(buff));
		n = sprintf(buff, "%s", drvname);
		lb = import_listbuff(buff, n);
	}

	return lb;
}

struct listbuff *get_hwname_of_eth_interface(const char *intf)
{
	char buff[16];
	char *token;
	int count;
	int idx = 0;
	int match = 1;
	int portidx = 0;
	int busidx = 0;
	int needBus = 0;
	struct listbuff *lb = NULL;
	struct listbuff *lbInOne = NULL;
	struct listbuff *tmp = NULL;
	struct listbuff *tmp2 = NULL;
	struct listbuff *parent1 = NULL;
	struct listbuff *parent2 = NULL;
	struct listbuff *parent3 = NULL;
	struct listbuff *path = get_device_path_of_interface(intf);
	if (unlikely(path == NULL)) {
		return NULL;
	}

	parent1 = get_dir(path->buff);
	if (parent1 != NULL) {
		parent2 = get_dir(parent1->buff);
	}
	if (parent2 != NULL) {
		parent3 = get_dir(parent2->buff);
	}

	if (parent2 != NULL) {
		/* GET PCI SLOT */
		tmp = get_token_value_from_uevent(parent2->buff, PCI_SLOT_NAME_TOKEN);
		if (tmp != NULL) {
#ifdef CONFIG_VENUS
			match = 0;
#else
			match = 1;
#endif
			idx = 0;
			token = strtok(tmp->buff, ":");
			while (token) {
				if (idx == match) {
					portidx = strtoul(token, NULL, 16);
					memset(buff, 0, sizeof(buff));
					count = sprintf(buff, "PCI%d-", portidx);
					tmp2 = import_listbuff(buff, count);
					if (lb == NULL) {
						lb = tmp2;
					} else {
						listbuff_add_tail(tmp2, lb);
					}

					break;
				}
				token = strtok(NULL, ":");
				idx++;
			}

			free_listbuff_list(&tmp);
		}

		needBus = 0;
		if (parent3 != NULL) {
			/* GET PCI SYBSYS ID */
			tmp = get_token_value_from_uevent(parent3->buff, PCI_SUBSYS_ID_TOKEN);
			if (tmp != NULL) {
				if (strcmp(tmp->buff, "0000:0000") != 0) {
					needBus = 1;
				}
				free_listbuff_list(&tmp);
			}
		}

		if (needBus) {
			/* GET PCI BUS */
			tmp = get_token_value_from_uevent(parent2->buff, PCI_SLOT_NAME_TOKEN);
			if (tmp != NULL) {
#ifdef CONFIG_VENUS
				match = 1;
#else
				match = 2;
#endif
				idx = 0;
				token = strtok(tmp->buff, ":");
				while (token) {
					if (idx == match) {
						busidx = strtoul(token, NULL, 16);
						memset(buff, 0, sizeof(buff));
						count = sprintf(buff, "B%x-", busidx);
						tmp2 = import_listbuff(buff, count);
						if (lb == NULL) {
							lb = tmp2;
						} else {
							listbuff_add_tail(tmp2, lb);
						}

						break;
					}
					token = strtok(NULL, ":");
					idx++;
				}

				free_listbuff_list(&tmp);
			}
		}

		/* GET DRIVER NAME */
		tmp = get_token_value_from_uevent(parent2->buff, DRIVER_TOKEN);
		if (likely(tmp != NULL)) {
			tmp2 = get_driver_friendly_name(tmp->buff);
			if (tmp2 != NULL) {
				if (lb == NULL) {
					lb = tmp2;
				} else {
					listbuff_add_tail(tmp2, lb);
				}
			}

			portidx = get_portindex_of_eth(path->buff);
			if (portidx >= 0) {
				memset(buff, 0, sizeof(buff));
				count = sprintf(buff, "%d", portidx);
				tmp2 = import_listbuff(buff, count);
				if (lb == NULL) {
					lb = tmp2;
				} else {
					listbuff_add_tail(tmp2, lb);
				}
			}

			free_listbuff_list(&tmp);
		}

		/** ADD NULL */
		memset(buff, 0, sizeof(buff));
		count = sprintf(buff, "%c", '\0');
		tmp2 = import_listbuff(buff, count);
		if (lb == NULL) {
			lb = tmp2;
		} else {
			listbuff_add_tail(tmp2, lb);
		}
	}

	if (lb != NULL) {
		lbInOne = listbuff_all_in_one(lb);
		free_listbuff_list(&lb);
	}

	free_listbuff_list(&parent1);
	free_listbuff_list(&parent2);
	free_listbuff_list(&path);

	return lbInOne;
}
EXPORT_SYMBOL(get_hwname_of_eth_interface);
