
/**
 * log.c
 * @author Sy Nguyen
 */

#include <stdio.h>
#include <fcntl.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <time.h>
#include <log.h>
#include <sys/unistd.h>
#include <cmd.h>
#include <linux/bug.h>
#include <linux/list.h>
#include <linux/init.h>
#ifdef WIN32
#include <process.h>
#include <windows.h>
#else
#include <pthread.h>
#endif

#include <vediag_common.h>
#include <log.h>
#include <errno.h>

#if 0
static const char *DEFAULT_LOG_NAME = "diagwrt.log";
#endif

static char current_name[128];
static FILE *file = NULL;
#ifdef WIN32
CRITICAL_SECTION wr_lock;
CRITICAL_SECTION rd_lock;
#else
pthread_mutex_t wr_lock;
pthread_mutex_t rd_lock;
#endif

int log_write(const void *buf, size_t size);
int log_read(void *buf, size_t size, int off);
int log_file(const char *fmt, ...);

void log_close(void);

int log_init(const char *name)
{
	log_close();
	memset(current_name, 0, sizeof(current_name));
	strncpy(current_name, name, strlen(name));
	/**
	 * fopen()
	 *
	 * FILE *fopen(const char *path, const char *mode);
	 *
	 * The fopen() function is used to open a file and associates an I/O stream with it.
	 * This function takes two arguments.
	 * The first argument is a pointer to a string containing name of the file to be opened while
	 * the second argument is the mode in which the file is to be opened. The mode can be :
	 * 'r' 	: Open text file for reading. The stream is positioned at the beginning of the file.
	 * 'r+' : Open for reading and writing. The stream is positioned at the beginning of the file.
	 * 'w'  : Truncate file to zero length or create text file for writing.
	 * 			The stream is positioned at the beginning of the file.
	 * 'w+' : Open for reading and writing. The file is created if it does not exist, otherwise it is truncated.
	 * 			The stream is positioned at the beginning of the file.
	 * 'a'  : Open for appending (writing at end of file).
	 * 			The file is created if it does not exist. The stream is positioned at the end of the file.
	 * 'a+' : Open for reading and appending (writing at end of file).
	 * 			The file is created if it does not exist. The initial file position for reading is at the beginning of the file, but output is always appended to the end of the file.
	 *
	 * The fopen() function returns a FILE stream pointer on success while it returns NULL in case of a failure.
	 */
	file = fopen(current_name, "w+");
	if (!file) {
		BUG();
		exit(1);
	}

	time_t rawtime = time(NULL);
	struct tm *t;
	t = localtime(&rawtime);
	log_file("\n/**\n"
			" * logged on %d/%d/%d %02d:%02d:%02d\n"
			" */\n",
			t->tm_mon + 1, t->tm_mday, t->tm_year + 1900, t->tm_hour, t->tm_min,
			t->tm_sec);
	return 0;
}

void log_close(void)
{
	if (!file)
		return;
	/**
	 * If the given stream was open for writing (or if it was open for updating and the
	 * last i/o operation was an output operation) any unwritten data in
	 * its output buffer is written to the file.
	 */
	fflush(file);
	/**
	 * The fclose() function first flushes the stream opened by fopen()
	 * and then closes the underlying descriptor. Upon successful completion
	 * this function returns 0 else end of file (eof) is returned. In case of failure,
	 * if the stream is accessed further then the behavior remains undefined.
	 */
	fclose(file);
}

/**
 * fwrite()
 *
 * The functions fread/fwrite are used for reading/writing data from/to the
 * file opened by fopen function. These functions accept three arguments.
 * The first argument is a pointer to buffer used for reading/writing the data.
 * The data read/written is in the form of 'nmemb' elements each 'size' bytes long.
 *
 * In case of success, fread/fwrite return the number of bytes actually
 * read/written from/to the stream opened by fopen function.
 * In case of failure, a lesser number of byes (then requested to read/write)
 * is returned.
 *
 * size_t fwrite(const void *ptr, size_t size, size_t nmemb, FILE *stream);
 *
 * @Parameters
 * ptr 		-- This is the pointer to the array of elements to be written.
 * size 	-- This is the size in bytes of each element to be written.
 * nmemb 	-- This is the number of elements, each one with a size of size bytes.
 * stream 	-- This is the pointer to a FILE object that specifies an output stream.
 *
 * @Return Value
 * This function returns the total number of elements successfully
 * written is returned as a size_t object,
 * which is an integral data type.If this number differs
 * from the nmemb parameter, it will show an error.
 */
int log_write(const void *buf, size_t size)
{
	int ret;
#ifdef WIN32
	EnterCriticalSection(&wr_lock);
#else
	pthread_mutex_lock(&wr_lock);
#endif
	ret = fwrite(buf, 1, size, file);
	ret += fflush(file);
#ifdef WIN32
	LeaveCriticalSection(&wr_lock);
#else
	pthread_mutex_unlock(&wr_lock);
#endif
	return ret;
}

int log_read(void *buf, size_t size, int off)
{
	int ret;
#ifdef WIN32
	EnterCriticalSection(&rd_lock);
#else
	pthread_mutex_lock(&wr_lock);
#endif
	ret = fread(buf, 1, size, file);
#ifdef WIN32
	LeaveCriticalSection(&rd_lock);
#else
	pthread_mutex_unlock(&wr_lock);
#endif
	return ret;
}

/**
 * The C library function int vsprintf(char *str, const char *format, va_list arg)
 * sends formatted output to a string using an argument list passed to it.
 *
 * @Parameters
 * str 		-- This is the array of char elements where the resulting string is to be stored.
 * arg 		-- An object representing the variable arguments list. This should be initialized by the va_start macro defined in <stdarg>.
 * format 	-- This is the C string that contains the text to be written to the str.
 * 				It can optionally contain embedded format tags that are replaced by
 * 				the values specified in subsequent additional arguments and formatted as requested.
 * 				Format tags prototype: %[flags][width][.precision][length]specifier, as explained below:
 *
 * specifier	Output
 * c			Character.
 * d or i		Signed decimal integer
 * e			Scientific notation (mantissa/exponent) using e character
 * E			Scientific notation (mantissa/exponent) using E character
 * f			Decimal floating point
 * g			Use the shorter of %e or %f.
 * G			Use the shorter of %E or %f
 * o			Signed octal
 * s			String of characters
 * u			Unsigned decimal integer
 * x			Unsigned hexadecimal integer
 * X			Unsigned hexadecimal integer (capital letters)
 * p			Pointer address
 * n			Nothing printed.
 * %			Character.
 *
 *
 * flags	Description
 * -		Left-justify within the given field width; Right justification is the default (see width sub-specifier).
 * +		Forces to preceed the result with a plus or minus sign (+ or -) even for positive numbers. By default, only negative numbers are preceded with a - sign..
 * (space)	If no sign is going to be written, a blank space is inserted before the value.
 * #		Used with o, x or X specifiers the value is preceeded with 0, 0x or 0X respectively for values different than zero. Used with e, E and f, it forces the written output to contain a decimal point even if no digits would follow. By default, if no digits follow, no decimal point is written. Used with g or G the result is the same as with e or E but trailing zeros are not removed.
 * 0		Left-pads the number with zeroes (0) instead of spaces, where padding is specified (see width sub-specifier).
 *
 *
 * width	Description
 * (number)	Minimum number of characters to be printed. If the value to be printed is shorter than this number, the result is padded with blank spaces. The value is not truncated even if the result is larger.
 * *		The width is not specified in the format string, but as an additional integer value argument preceding the argument that has to be formatted.
 *
 *
 * .precision 	Description
 * .number		For integer specifiers (d, i, o, u, x, X): precision specifies the minimum number of digits to be written. If the value to be written is shorter than this number, the result is padded with leading zeros. The value is not truncated even if the result is longer. A precision of 0 means that no character is written for the value 0. For e, E and f specifiers: this is the number of digits to be printed after de decimal point. For g and G specifiers: This is the maximum number of significant digits to be printed. For s: this is the maximum number of characters to be printed. By default all characters are printed until the ending null character is encountered. For c type: it has no effect. When no precision is specified, the default is 1. If the period is specified without an explicit value for precision, 0 is assumed.
 * .*			The precision is not specified in the format string, but as an additional integer value argument preceding the argument that has to be formatted.
 *
 *
 * length	Description
 * h		The argument is interpreted as a short int or unsigned short int (only applies to integer specifiers: i, d, o, u, x and X).
 * l		The argument is interpreted as a long int or unsigned long int for integer specifiers (i, d, o, u, x and X), and as a wide character or wide character string for specifiers c and s.
 * L		The argument is interpreted as a long double (only applies to floating point specifiers: e, E, f, g and G).
 *
 * @Return Value
 * If successful, the total number of characters written is returned otherwise a negative number is returned.
 */
int log_file(const char *fmt, ...)
{
	va_list args;
	int size, count = -1;
	char buffer[10240] = { [0 ... 10239] = 0 };

	va_start(args, fmt);
	size = vsprintf(buffer, fmt, args);
	if (size > 0) {
		count = log_write(buffer, size);
	}
	va_end(args);
	return count;
}

pure_initcall(log_constructor)
{
#ifdef WIN32
	/**
	 * Initializes a critical section object and sets the spin count for the critical section.
	 * When a thread tries to acquire a critical section that is locked,
	 * the thread spins: it enters a loop which iterates spin count times,
	 * checking to see if the lock is released.
	 * If the lock is not released before the loop finishes,
	 *  the thread goes to sleep to wait for the lock to be released.
	 */
	if (!InitializeCriticalSectionAndSpinCount(&wr_lock, 0x00000400)) {
		BUG();
		return -1;
	}

	if (!InitializeCriticalSectionAndSpinCount(&rd_lock, 0x00000400)) {
		BUG();
		return -1;
	}
#endif
	int rc = -1;

#if 0
	rc = log_init(DEFAULT_LOG_NAME);
	if (rc) {
	    printf("Log init failed\n");
	    return rc;
	}
#endif

#if 1 /*CONFIG_XLOG*/
#define XLOG_CONF_GLOBAL \
        "[global]"                                "\n"\
        ""                                        "\n"

#define XLOG_CONF_LEVELS \
        "[levels]"                                "\n"\
        ""                                        "\n"
#define XLOG_CONF_FORMATS \
        "[formats]"                               "\n"\
        "nfmt = \"%%m\""                          "\n"\
        "xfmt = \"%%m\""                          "\n"\
        ""                                        "\n"
#if 0

#define XLOG_CONF_RULES \
        "[rules]"                                            "\n"\
        "*.!DEBUG  >stdout; nfmt"                            "\n"\
        "vediag_.!NOTICE  \"vediag_%Y%m%d_%H%M%S.log\"; xfmt"  "\n"
#else

#define XLOG_CONF_RULES \
        "[rules]"                                            "\n"\
        "*.!DEBUG  >stdout; nfmt"                            "\n"\
        VEDIAG_ZLOG_CATEGORY "_.!NOTICE  \"%s\"; xfmt"        "\n"\

#define XLOG_CONF_NAMEDATE \
        "%Y%m%d_%H%M%S"

#define XLOG_FILENAME_FMT \
		"diagwrt_%s_%s.log"

#endif

	FILE* pfile = NULL;
	time_t curr_time;
	struct tm *tm = NULL;
	char zlog_conf_rules[1024] = { 0 };
	char zlog_conf_date[256] = { 0 };
	char zlog_conf_hostname[256] = { 0 };
    char zlog_log_filepath[256] = { 0 };
    char zlog_log_filename[256] = { 0 };

    /* force change file permission */
    chmod(VEDIAG_ZLOG_CONFFILE, S_IRUSR | S_IWUSR);

    /* use the default settings in stead */
    pfile = fopen(VEDIAG_ZLOG_CONFFILE, "wt");
    if (pfile) {
        /* get current time & format it */
        time(&curr_time);
        tm = localtime(&curr_time);

        /* log file name */
        gethostname(zlog_conf_hostname, sizeof(zlog_conf_hostname) - 1);
        strftime(zlog_conf_date, sizeof(zlog_conf_date) - 1, XLOG_CONF_NAMEDATE, tm);
        snprintf(zlog_log_filename, sizeof(zlog_log_filename) - 1, XLOG_FILENAME_FMT, zlog_conf_hostname, zlog_conf_date);
        /* zlog rules */
        snprintf(zlog_conf_rules, sizeof(zlog_conf_rules) - 1, XLOG_CONF_RULES, zlog_log_filename);

        /* write default config to file */
        fprintf(pfile, XLOG_CONF_GLOBAL);
        fprintf(pfile, XLOG_CONF_LEVELS);
        fprintf(pfile, XLOG_CONF_FORMATS);
        fprintf(pfile, zlog_conf_rules);
        fflush(pfile);
        fclose(pfile);
    } else {
        printf("Cannot create zlog.conf [%s]\n", strerror(errno));
        return rc;
    }

    /* load config file */
    rc = dzlog_init(VEDIAG_ZLOG_CONFFILE, VEDIAG_ZLOG_CATEGORY);
    if (rc) {
        printf("Zlog init failed\n");
        return rc;
    }
    /* logging current log file path */
    if (getcwd(zlog_log_filepath, sizeof(zlog_log_filepath) - 1))
        xinfo("\nVEDIAG_LOG_DEBUG %s/%s\n", zlog_log_filepath, zlog_log_filename);
    else {
        xinfo("\nVEDIAG_LOG_DEBUG %s\n", zlog_log_filename);
        xerror("%s\n", strerror(errno));
    }

#endif

	return 0;
}

static void __destructor(CFG_PRIORITY_INIT_10)
log_destructor(void)
{
	log_close();

#ifdef CONFIG_XLOG
	zlog_fini();
#endif

#ifdef WIN32
	DeleteCriticalSection(&wr_lock);
	DeleteCriticalSection(&rd_lock);
#endif
}

#ifdef CONFIG_COMMAND_LOG
int do_log_info(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[]);
int do_log_rename(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[]);

typedef struct log_cmd {
	char *name;
	int (*cmd)(struct cmd_tbl_s *, int, int, char *[]);
} log_cmd_t;

log_cmd_t logs_cmd[] = {
		{"info", do_log_info},
		{"rename", do_log_rename},
};

/**
 * find command table entry for a command
 */
log_cmd_t *find_log_cmd(const char *cmd)
{
	log_cmd_t *cmdtp = NULL;
	int len;
	const char *p;
	int found = 0;
	if (cmd == NULL) {
		return NULL;
	}
	int size = ARRAY_SIZE(logs_cmd);
	int i;

	len = ((p = strchr(cmd, '.')) == NULL) ? strlen(cmd) : (p - cmd);

	log_cmd_t *iter = NULL;
	for (i = 0; i < size; i++) {
		iter = &logs_cmd[i];
		if (strncmp(cmd, iter->name, len) == 0) {
			if (len == strlen(iter->name)) {
				return iter; /* full match */
			}
			cmdtp = iter;
			found++;
		}
	}

	if (found == 1) {
		return cmdtp;
	}

	return NULL;
}

int do_log_rename(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[]) {
	if (argc < 2) {
		cmd_usage(cmdtp);
		return -1;
	}

	return log_init(argv[1]);
}

int do_log_info(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[]) {
	printf("log name: %s\n", current_name);
	return 0;
}

int do_log(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[]) {
	if (argc < 2) {
		cmd_usage(cmdtp);
		return -1;
	}

	log_cmd_t *log = find_log_cmd(argv[1]);
	if (!log) {
		printf("Not found command %s\n", argv[1]);
		cmd_usage(cmdtp);
		return -1;
	}

	argc--;
	argv++;

	return (*log->cmd)(cmdtp, flag, argc, argv);
}

VEDIAG_CMD(log, 16, 1, do_log,
		"Type help log for help",
		"\n"	\
		"log info	-- log information as name, size, position ...\n"	\
		"log show 	-- display content of log on console\n"	\
		"log rename <filename>	-- rename current log file"
		);

#endif
