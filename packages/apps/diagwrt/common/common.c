
#include <common.h>
#include <listbuff.h>


/**
 * Return output stream of the specified command 'cmd'
 */
struct listbuff *get_outputstream_of_shell_command(const char *cmd)
{
	struct listbuff *lb = NULL, *tmp;
	struct listbuff *lb_all_in_one = NULL;
	char *line;
	char buff[1024];
	int count;
	FILE *p_file = NULL;

	p_file = popen(cmd, "r");
	if (likely(p_file != NULL)) {
		while (1) {
			memset(buff, 0, sizeof(buff));
			line = fgets(buff, sizeof(buff), p_file);
			if (line == NULL) {
				break;
			} else {
				tmp = import_listbuff(line, strlen(line));
				if (lb == NULL) {
					lb = tmp;
				} else {
					listbuff_add_tail(tmp, lb);
				}
			}
		}

		pclose(p_file);
	}

	if (lb != NULL) {
		/** ADD NULL */
		memset(buff, 0, sizeof(buff));
		count = sprintf(buff, "%c", '\0');
		tmp = import_listbuff(buff, count);
		listbuff_add_tail(tmp, lb);

		/** All in one */
		lb_all_in_one = listbuff_all_in_one(lb);
		free_listbuff_list(&lb);
	}

	return lb_all_in_one;
}

EXPORT_SYMBOL(get_outputstream_of_shell_command);
