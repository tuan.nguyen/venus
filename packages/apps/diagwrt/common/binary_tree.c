#include <common.h>
#include <string.h>
#include <binary_tree.h>
#include <log.h>

#define BT_DEBUG
#ifdef BT_DEBUG
#define btdbg(x, fmt...) xinfo(x, ##fmt)
#else
#define btdbg(x, fmt...)
#endif
/**
 * Allocate a new node
 */
BT_DEFINE1(newNode, int key)
{
	int size = sizeof(struct node);
	struct node *node = (struct node *) malloc(size);
	if (!node) {
		xerror("out of memory\n");
		return NULL;
	}
	memset(node, 0, size);
	node->key = key;
	return node;
}

/**
 * Delete node & all it' subtree
 * Walk all the tree and free each node
 */
void BT_DEFINE(destroy, struct node **node)
{
	struct node *tmpNode = *node;
	if (!tmpNode) {
		return;
	}
	bt_destroy(&tmpNode->left);
	bt_destroy(&tmpNode->right);
	btdbg("%s: destroy a node with key %d\n", __func__, tmpNode->key);
	free(tmpNode);
	tmpNode = NULL;
	*node = tmpNode;
}

/**
 * Check the node to see if it has:
 * 		1. no children
 * 		2. has left subtree only
 * 		3. has right subtree only
 * 		4. has both subtree
 */
NODE_DESCENDANT inline BT_DEFINE(node_descendant, struct node *node)
{
	if (node->left && node->right)
		return BT_HAS_BOTH_SUBTREE;
	else if (node->left)
		return BT_HAS_LEFTTREE_ONLY;
	else if (node->right)
		return BT_HAS_RIGHT_TREE_ONLY;
	else
		return BT_NO_CHILDREN;
}

/**
 * Find largest key in the tree node
 */
BT_DEFINE1(find_largest, struct node *node)
{
	if (!node)
		return NULL;
	if (node->right)
		return bt_find_largest(node->right);
	else {
		btdbg("%s, found a node with largest key %d\n", __func__, node->key);
		return node;
	}
}

/**
 * Find smallest key in the tree node
 */
BT_DEFINE1(find_smallest, struct node *node)
{
	if (!node)
		return NULL;
	if (node->left)
		return bt_find_smallest(node->left);
	else {
		btdbg("%s, found a node with smallest key %d\n", __func__, node->key);
		return node;
	}
}

/**
 * Insertion:
 * This operation inserts a new node in the binary search tree (BST).
 * While inserting we need to make sure that it does not violate the rule of a BST.
 * Inorder to insert a new node we need to search an empty tree (or subtree in a tree)
 * and then insert the new node.
 * Insertion always takes place as a leaf node.
 * We use the concept of recursion in the insertion function in BST.
 * The function is stated below and some of the important conventions in the algorithm is:-
 */
BT_DEFINE1(insertion, struct node **curNode, int key)
{
	struct node *tmpnode = *curNode;
	if (!tmpnode) {
		tmpnode = bt_newNode(key);
		*curNode = tmpnode;
		btdbg("%s: insert a new node with key %d\n", __func__, tmpnode->key);
		return tmpnode;
	}
	if (key < tmpnode->key) {
		return bt_insertion(&tmpnode->left, key);
	} else {
		return bt_insertion(&tmpnode->right, key);
	}
}

/**
 * Search:
 * If the node to be searched is less than the root, go towards the left subtree in BST.
 * If the node to be searched is greater than the root, than go towards the right subtree of the BST.
 * Follow step 1 & 2 recursively until we find the element to be searched.
 * After traveling the entire BST, if the element is not found then the element does not exist in the BST.
 */
bool BT_DEFINE(search, struct node *root, int key)
{
	if (!root)
		return false;
	if (key < root->key) {
		return bt_search(root->left, key);
	} else if (key > root->key) {
		return bt_search(root->right, key);
	} else {
		return true;
	}
}

/**
 * Get a node from the tree
 */
struct node *bt_getNode(struct node *root, int key)
{
	if (!root)
		return NULL;
	if (key < root->key) {
		return bt_getNode(root->left, key);
	} else if (key > root->key) {
		return bt_getNode(root->right, key);
	} else {
		return root;
	}
}

/**
 * Deletion:
 * If the node to be deleted has no children than we just delete the node.
 * If the node to be deleted has a right subtree then delete the node and
 * 		attach the right subtree with the parent of the deleted node.
 * If the node to be deleted has a left subtree then after deleting the node attach
 * 		the left subtree with the parent of the deleted node.
 * If the node to be deleted has a left & right subtree, then we can delete the node but
 * 		the resultant tree can be unbalanced.
 * 	//Node to be deleted is in the middle of a tree (Condition 4)
 *   	1.Save root in a temporary variable temp
 *  	2.Find the largest element in left subtree
 *   		largest= largestBST( left_subtree);
 *  		Find the smallest element in the right subtree
 * 			smallest=smallestBST( right_subtree);
 *		3.Move the smallest or largest variable to temp and substitute of tree
 */
void BT_DEFINE(deletion, struct node **node, int key)
{
	struct node *tmpNode = *node;
	if (!tmpNode)
		return;
	if (key < tmpNode->key) {
		return bt_deletion(&tmpNode->left, key);
	} else if (key > tmpNode->key) {
		return bt_deletion(&tmpNode->right, key);
	}

	struct node *cursor;
	switch (bt_node_descendant(tmpNode)) {
	case BT_NO_CHILDREN:
		free(tmpNode);
		tmpNode = NULL;
		*node = tmpNode;
		break;
	case BT_HAS_LEFTTREE_ONLY:
		cursor = tmpNode->left;
		free(tmpNode);
		tmpNode = cursor;
		*node = tmpNode;
		break;
	case BT_HAS_RIGHT_TREE_ONLY:
		cursor = tmpNode->right;
		free(tmpNode);
		tmpNode = cursor;
		*node = tmpNode;
		break;
	case BT_HAS_BOTH_SUBTREE:
		cursor = bt_find_largest(tmpNode->right);
		if (!cursor)
			cursor = bt_find_smallest(tmpNode->left);
		if (cursor) {
			tmpNode->key = cursor->key;
			free(cursor);
			cursor = NULL;
			*node = tmpNode;
		}
		break;
	}
}

/**
 * Traversal:
 * In-order
 */
void BT_DEFINE(inorder, struct node *node)
{
	if (node) {
		bt_inorder(node->left);
		xinfo("%d ", node->key);
		bt_inorder(node->right);
	}
}

/**
 * Traversal:
 * Pre-order
 */
void BT_DEFINE(preorder, struct node *node)
{
	if (node) {
		xinfo("%d ", node->key);
		bt_preorder(node->left);
		bt_preorder(node->right);
	}
}

/**
 * Traversal:
 * Pre-order
 */
void BT_DEFINE(postorder, struct node *node)
{
	if (node) {
		bt_preorder(node->left);
		bt_preorder(node->right);
		xinfo("%d ", node->key);
	}
}

