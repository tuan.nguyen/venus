
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <cmd.h>
#include <string.h>
#include <linux/err.h>
#include <vediag_common.h>
#include <pthread.h>
#include "iniparser.h"
#include <readline/readline.h>
#include <readline/history.h>
#include <log.h>

extern char file_system[128];
extern char file_testconfig[128];

struct test_cfg {
	int test_type;
	int test_num;
	char *device;
	char *physical;
	char *app;
	char *arg;
};

static char *get_config_value(dictionary *dic, char *section, char *key)
{
	char keym[128];

	memset(keym, 0, sizeof(keym));
	sprintf(keym, "%s:%s", section, key);
	return iniparser_getstring(dic, keym, NULL);
}

char *get_config_from_file(char *cfg_file, char *section, char *key)
{
	char *value;
	dictionary *dic;

	dic = iniparser_load(cfg_file);
	if (dic == NULL) {
		xerror("Can't parse file [%s]", cfg_file);
		return NULL;
	}
	
	value = strdup(get_config_value(dic, section, key));
	iniparser_freedict(dic);

	xinfo("file %s sec %s key %s value %s \n", cfg_file, section, key, value);
	return value;
}

static void set_config_value(dictionary *dic, char *section, char *key, char *value)
{
	char keym[128];

	memset(keym, 0, sizeof(keym));
	if (key != NULL)
		sprintf(keym, "%s:%s", section, key);
	else
		sprintf(keym, "%s", section);
	iniparser_set(dic, keym, value);
}

static int get_total_test(dictionary *dic, int type)
{
	int cnt = 0;
	char *val;
	val = get_config_value(dic, get_section_type(type), "Total");
	if (val)
		cnt = strtoul(val, NULL, 0);

	return cnt;
}

static void update_argument(dictionary *dic, char *argument, char *key, char *value)
{
	set_config_value(dic, argument, key, value);
}

static int check_section(dictionary *dic, char *section)
{
    int     i;
    int     nsec;
    char *secname;

    nsec = iniparser_getnsec(dic);
    for (i=0 ; i<nsec ; i++) {
        secname = iniparser_getsecname(dic, i) ;
        if (strcasecmp(secname, section) == 0)
        	return 1;
    }
    return 0;
}

static void vediag_print_argument(dictionary *dic, char *argument)
{
    int     i, j ;
    char    keym[128];
    int     nsec ;
    char *secname;
    int     seclen;

    nsec = iniparser_getnsec(dic);
    for (i=0 ; i<nsec ; i++) {
        secname = iniparser_getsecname(dic, i) ;
        if (strcasecmp(secname, argument) == 0) {
			seclen  = (int)strlen(secname);
			sprintf(keym, "%s:", secname);
			for (j=0 ; j<dic->size ; j++) {
				if (dic->key[j]==NULL)
					continue ;
				if (!strncmp(dic->key[j], keym, seclen+1)) {
					if (dic->val[j])
						xinfo("     + %s = %s\n",
								dic->key[j]+seclen+1,
								dic->val[j] ? dic->val[j] : "");
				}
			}
        	return;
        }
    }

    /* If don't find argument section, add */
    update_argument(dic, argument, NULL, NULL);
}

static void __vediag_print_test_config(dictionary *dic, int type, int arg_show)
{
	char *val;
	char *device;
	char *app, *physical, *arg;
	char section[64];
	int test, cnt = 0;

	val = get_config_value(dic, get_section_type(type), "Total");
	if (val)
		cnt = strtoul(val, NULL, 0);
	xinfo("\n[%s test %d device]\n", get_section_type(type), cnt);
	for (test = 1; test <= cnt; test++) {
		memset(section, 0, sizeof(section));
		sprintf(section, "%s%d", get_section_type(type), test);
		device = get_config_value(dic, section, "Device");
		physical = get_config_value(dic, section, "Physical");
		app = get_config_value(dic, section, "App");
		arg = get_config_value(dic, section, "Argument");
		if (!device && !physical) {
			xinfo("%d. %s test %d: Not found config\n", test, get_section_type(type), test);
			continue;
		}
		xinfo("%d. %s test %d\n", test, get_section_type(type), test);
		if (device)
			xinfo("   - Device:   %s\n", device);
		if (physical)
			xinfo("   - Physical: %s\n", physical);
		if (app)
			xinfo("   - App:      %s\n", app);
		if (arg) {
			xinfo("   - Argument: %s\n", arg);
			if (arg_show) {
				vediag_print_argument(dic, arg);
			}
		}
	}
}

static void delete_test_cfg(dictionary *dic, int type)
{
	char *val;
	char value[32];
	int cnt = 0;

	val = get_config_value(dic, get_section_type(type), "Total");
	if (val)
		cnt = strtoul(val, NULL, 0);

	if (cnt > 0) {
		memset(value, 0, sizeof(value));
		sprintf(value, "%d", cnt - 1);
		set_config_value(dic, get_section_type(type), "Total", value);
	}
}

static void free_test_cfg(struct test_cfg *test)
{
	if (!test)
		return;

	if (test->device)
		free(test->device);
	if (test->physical)
		free(test->physical);
	if (test->app)
		free(test->app);
	if (test->arg)
		free(test->arg);
	free(test);
}

static void update_test_cfg(dictionary *dic, struct test_cfg *test, int print)
{
	char *val;
	char section[64], value[32];
	int cnt = 0;
	int new_test = 0;

	val = get_config_value(dic, get_section_type(test->test_type), "Total");
	if (val)
		cnt = strtoul(val, NULL, 0);

	if (test->test_num == (cnt + 1))
		new_test = 1;
	else if (test->test_num <= 0 || test->test_num > (cnt + 1))
		return;

	memset(section, 0, sizeof(section));
	sprintf(section, "%s%d", get_section_type(test->test_type), test->test_num);

	if (print) {
		xinfo("Update test cfg\n");
		if (test->device)
			xinfo("   - Device:   %s\n", test->device);
		if (test->physical)
			xinfo("   - Physical: %s\n", test->physical);
		if (test->app)
			xinfo("   - App:      %s\n", test->app);
		if (test->arg)
			xinfo("   - Argument: %s\n", test->arg);
	}

	if (new_test)
		set_config_value(dic, section, NULL, NULL);

	if ((test->test_type != VEDIAG_STORAGE) && test->device) {
		set_config_value(dic, section, "Device", test->device);
	}
	if (((test->test_type == VEDIAG_STORAGE) || (test->test_type == VEDIAG_NET)) && 
		test->physical)
		set_config_value(dic, section, "Physical", test->physical);
	if (test->app)
		set_config_value(dic, section, "App", test->app);
	if (test->arg)
		set_config_value(dic, section, "Argument", test->arg);

	/* Check argument section */
	if (test->arg && !check_section(dic, test->arg))
		update_argument(dic, test->arg, NULL, NULL);

	if (new_test) {
		memset(value, 0, sizeof(value));
		sprintf(value, "%d", cnt + 1);
		set_config_value(dic, get_section_type(test->test_type), "Total", value);
	}
}

static struct test_cfg *get_test_cfg(dictionary *dic, int type, int test_num)
{
	char *val;
	char section[64];
	int cnt = 0;
	struct test_cfg *test;

	/* Parse memory test */
	val = get_config_value(dic, get_section_type(type), "Total");
	if (val)
		cnt = strtoul(val, NULL, 0);
	if (test_num > cnt || test_num < 0) {
		return NULL;
	}

	test = malloc(sizeof(struct test_cfg));
	memset(test, 0, sizeof(struct test_cfg));
	test->test_type = type;
	test->test_num = test_num;
	test->device = NULL;
	test->physical = NULL;
	test->app = NULL;
	test->arg = NULL;

	memset(section, 0, sizeof(section));
	sprintf(section, "%s%d", get_section_type(type), test_num);
	val = get_config_value(dic, section, "Device");
	if (val)
		test->device = strdup(val);
	val = get_config_value(dic, section, "Physical");
	if (val)
		test->physical = strdup(val);
	val = get_config_value(dic, section, "App");
	if (val)
		test->app = strdup(val);
	val = get_config_value(dic, section, "Argument");
	if (val)
		test->arg = strdup(val);

	return test;
}

static void vediag_print_test_config(dictionary *dic)
{
	__vediag_print_test_config(dic, VEDIAG_MEM, 0);
	__vediag_print_test_config(dic, VEDIAG_STORAGE, 0);
	__vediag_print_test_config(dic, VEDIAG_NET, 0);
	__vediag_print_test_config(dic, VEDIAG_PCIE, 0);
}

#define PRINT_MAIN_CONFIG_TITLE() xinfo("===== MAIN PAGE =====\n")
#define PRINT_TEST_TILE(type) xinfo("===== %s PAGE =====\n", get_section_type(type))

static int vediag_test_management(char *cfg_file)
{
	dictionary * dic;
	FILE *file;
	int fd;
	char key;
	int complete = 0, state_complete = 0;
	int state = 0;
	int type = 0;
	int test_num = 0, parse_num = 0, new_test = 0, delete = 0;
	struct test_cfg * test = NULL;
	int step = 0;
	char *value;
	char *arg_key, *arg_value;
	char prompt[128];

	/* Check file exist */
	if (access(cfg_file, 0) != 0) {
		fd = open(cfg_file, O_RDWR|O_CREAT, 0777);
		if (fd != -1)
			close(fd);
		else{
			xinfo("Can't create %s file\n", cfg_file);
			return -1;
		}
	}

	/* Load dictionary */
	dic = iniparser_load(cfg_file);
	if (dic == NULL) {
		xerror("Can't parse file [%s]", cfg_file);
		return -1;
	}
	state = 0;
	while(!complete) {
		switch (state) {
		case 0:
			type = 0;
			state_complete = 0;
			xinfo("\n\n");
			while (!state_complete) {
				xinfo("\033[2J\033[0;0H----- %s -----\n", cfg_file);
				PRINT_MAIN_CONFIG_TITLE();
				vediag_print_test_config(dic);
				xinfo("\n\nPress 'm' for memory test; 's' for storage test, 'n' for network test, 'p' for PCI test\n");
				xinfo("Press ESC then ENTER to exit\n");
				key = getchar();
				if (key == '\e')
					complete = 1;
				else if (key == 'm') {
					type = VEDIAG_MEM;
					state = 20;
				}
				else if (key == 's') {
					type = VEDIAG_STORAGE;
					state = 20;
				}
				else if (key == 'n') {
					type = VEDIAG_NET;
					state = 20;
				}
				else if (key == 'p') {
					type = VEDIAG_PCIE;
					state = 20;
				}
				else if (key == '\n') {
					state_complete = 1;
				}
			}
			break;
		case 20: /* Config test */
			state_complete = 0;
			new_test = 0;
			parse_num = 0;
			test_num = 0;
			xinfo("\n\n");
			while (!state_complete) {
				xinfo("\033[2J\033[0;0H----- %s -----\n", cfg_file);
				PRINT_TEST_TILE(type);
				__vediag_print_test_config(dic, type, 1);
				xinfo("Press 'n' for new test; 'd' to delete lastest test. Select the number of test want to config\n");
				xinfo("Press ESC to return previous\n");
				key = getchar();
				if (key == '\e')
					state = 0;
				else if (key == 'd') {
					state = 23;
				}
				else if (key == 'n') {
					state = 22;
					new_test = 1;
				}
				else if (key >= '0' && key <= '9') {
					/* Parse test number */
					parse_num = 1;
					test_num = (test_num * 10 ) + (key - '0');
				}
				else if (key == '\n') {
					if (test_num > 0) {
						state = 22;
					}
					state_complete = 1;
				}
				else {
					if(parse_num == 1) {
						test_num = 0;
					}
				}
			}
			break;
		case 22: /* Edit exist test */
			state_complete = 0;
			state = 20;
			free_test_cfg(test);
			if (new_test) {
				test = malloc(sizeof(struct test_cfg));
				memset(test, 0, sizeof(struct test_cfg));
				test->test_type = type;
				test->test_num = get_total_test(dic, type) + 1;
				test_num = test->test_num;
			}
			else {
				test = get_test_cfg(dic, type, test_num);
				if (!test) {
					xinfo("Not found %s test number %d\n", get_section_type(type), test_num);
					state = 20;
					state_complete = 1;
					break;
				}
			}
			step = 0;
			delete = 0;
			while (!state_complete) {
				xprint("\033[2J\033[0;0H");
				xinfo("----- %s -----\n", cfg_file);
				xinfo("%d. %s test %d\n", test_num, get_section_type(type), test_num);
				if (test->device)
					xinfo("   - Device:   %s\n", test->device);
				if (test->physical)
					xinfo("   - Physical: %s\n", test->physical);
				if (test->app)
					xinfo("   - App:      %s\n", test->app);
				if (test->arg)
					xinfo("   - Argument: %s\n", test->arg);

				xinfo("\n\nType new value. Use '*' to delete\n");
				memset(prompt, 0, sizeof(prompt));
				if (step == 0)
					sprintf(prompt, "Change 'Device' from '%s' to :", test->device ? test->device : "");
				else if (step == 1)
					sprintf(prompt, "Change 'Physical' from '%s' to :", test->physical ? test->physical : "");
				else if (step == 2)
					sprintf(prompt, "Change 'App' from '%s' to :", test->app ? test->app : "");
				else if (step == 3)
					sprintf(prompt, "Change 'Argument' from '%s' to :", test->arg ? test->arg : "");

				value = readline(prompt);
				/* detect delete key */
				if (strlen(value) == 1 && value[0] == '*')
					delete = 1;

				if ((step == 0) && (strlen(value) > 0)) {
					if (test->device)
						free(test->device);
					if (delete)
						test->device = NULL;
					else
						test->device = strdup(value);
				}
				if ((step == 1) && (strlen(value) > 0)) {
					if (test->physical)
						free(test->physical);
					if (delete)
						test->physical = NULL;
					else
						test->physical = strdup(value);
				}
				if ((step == 2) && (strlen(value) > 0)) {
					if (test->app)
						free(test->app);
					if (delete)
						test->app = NULL;
					else
						test->app = strdup(value);
				}
				if ((step == 3) && (strlen(value) > 0)) {
					if (test->arg)
						free(test->arg);
					if (delete)
						test->arg = NULL;
					else
						test->arg = strdup(value);
				}
				free(value);
				step ++;
				/* Edit arguments */
				if (step == 4 && test->arg)
					state = 30;
				delete = 0;
				if (step > 3)
					state_complete = 1;
			}
			update_test_cfg(dic, test, 1);
			break;
		case 23: /* Delete latest test */
			delete_test_cfg(dic, type);
			state = 20;
			break;
		case 30: /* Edit arguments */
			state_complete = 0;
			step = 0;
			arg_key = NULL;
			arg_value = NULL;
			while (!state_complete) {
				xinfo("\033[2J\033[0;0H----- %s -----\n", cfg_file);
				xinfo("===== %s =====\n", test->arg);
				vediag_print_argument(dic, test->arg);
				memset(prompt, 0, sizeof(prompt));
				if (step == 0)
					sprintf(prompt, "Select argument:");
				else if (step == 1)
					sprintf(prompt, "'%s' value:", arg_key);
				value = readline(prompt);
				if (step == 0){
					if (strlen(value) > 0)
						arg_key = strdup(value);
					else
						arg_key = NULL;
				}
				if (step == 1){
					if (strlen(value) > 0)
						arg_value = strdup(value);
					else
						arg_value = NULL;
				}
				if (!arg_key) {
					state = 20;
					state_complete = 1;
				}
				step++;
				if (step > 1)
					state_complete = 1;
			}
			update_argument(dic, test->arg, arg_key, arg_value);
			break;
		case 31:
			state_complete = 0;
			xinfo("\033[2J\033[0;0H----- %s -----\n", cfg_file);
		default:
			break;
		}
	}

	/* Save INI file */
	file = fopen(cfg_file, "w");
	iniparser_dump_ini(dic, file);
	fclose(file);
	iniparser_freedict(dic);

	return 0;
}

int __vediag_create_test(dictionary * dic, int test_type)
{
	char *val;
	char section[64], value[64];
	int cnt = 0, dev;
	struct test_cfg *test;
	struct list_head *arg_list;
	struct vediag_test_argument *arg;

	val = vediag_parse_system(get_section_type(test_type));
	if (val) {
		cnt = strtoul(val, NULL, 0);
		free(val);
	}
	/* Update total */
	set_config_value(dic, get_section_type(test_type), NULL, NULL);
	set_config_value(dic, get_section_type(test_type), "Total", "0");

	for (dev = 1; dev <= cnt; dev ++) {
		test = malloc(sizeof(struct test_cfg));
		if (!test){
			xerror("Out of memory.\n");
			return -1;
		}
		memset(test, 0, sizeof(struct test_cfg));
		memset(section, 0, sizeof(section));
		sprintf(section, "%s%d", get_section_type(test_type), dev);
		test->device = vediag_parse_config(file_system, section, VEDIAG_DEVICE_KEY);
		test->physical = vediag_parse_config(file_system, section, VEDIAG_PHYSICAL_KEY);
		memset(value, 0, sizeof(value));
		sprintf(value, "%s%d argument", get_section_type(test_type), dev);
		test->test_type = test_type;
		test->test_num = dev;
		val = vediag_get_default_application(test_type);
		if (val)
			test->app = strdup(val);
		test->arg = strdup(value);
		update_test_cfg(dic, test, 0);

		/* Create argument */
		arg_list = malloc(sizeof(struct list_head));
		INIT_LIST_HEAD(arg_list);
		vediag_get_default_argument(test_type, arg_list);

		while (!list_empty(arg_list)) {
			arg = list_entry(arg_list->next,
						struct vediag_test_argument, entry);
			if(arg) {
				list_del_init(&arg->entry);
				update_argument(dic, test->arg, arg->key, arg->value);
				if (arg->key)
					free(arg->key);
				if (arg->value)
					free(arg->value);
				free(arg);
			}
		}

		free_test_cfg(test);
	}
    return 0;
}

static int __vediag_create_network_test(dictionary * dic, int test_type)
{
#include <vediag_net.h>
	char *val;
	char section[64], value[64];
	int cnt = 0, dev;
	struct test_cfg *test;
	struct list_head *arg_list;
	struct vediag_test_argument *arg;
	int len;
	int fuck = 0;

	val = vediag_parse_system(get_section_type(test_type));
	if (val) {
		cnt = strtoul(val, NULL, 0);
		free(val);
	}

	/* Update total */
	set_config_value(dic, get_section_type(test_type), NULL, NULL);
	set_config_value(dic, get_section_type(test_type), "Total", "0");

	for (fuck = 1, dev = 1; dev <= cnt;dev++) {
		test = malloc(sizeof(struct test_cfg));
		if (!test){
			xerror("Out of memory.\n");
			return -1;
		}
		memset(test, 0, sizeof(struct test_cfg));
		memset(section, 0, sizeof(section));
		sprintf(section, "%s%d", get_section_type(test_type), dev);
		test->device = vediag_parse_config(file_system, section, VEDIAG_DEVICE_KEY);
#ifdef CONFIG_VENUS
		test->physical = vediag_parse_config(file_system, section, VEDIAG_PHYSICAL_KEY);
		if (test->physical != NULL) {
			if (strstr(test->physical, "MLX1") != NULL && xgetenv(FORCE_REMOTE_ENV) == NULL) {
				free_test_cfg(test);
				continue;
			}
			if (strstr(test->physical, "PCI2") != NULL
					&& strstr(test->physical, MELLANOX_100G_NAME) != NULL
					&& xgetenv(FORCE_REMOTE_ENV) == NULL) {
				free_test_cfg(test);
				continue;
			}
		}

		if (xgetenv(IPERF_APP_ENV) != NULL) {
			if (strstr(test->physical, MELLANOX_10G_NAME) != NULL) {
				test->app = strdup(NETWORK_IPERF_APP_NAME);
			} else if (strstr(test->physical, MELLANOX_100G_NAME) != NULL) {
				test->app = strdup(NETWORK_IPERF_APP_NAME);
			} else {
				val = vediag_get_default_application(test_type);
				if (val)
				test->app = strdup(val);
			}
		} else {
			val = vediag_get_default_application(test_type);
			if (val)
			test->app = strdup(val);
		}
#endif
		memset(value, 0, sizeof(value));
		sprintf(value, "%s%d argument", get_section_type(test_type), fuck);
		test->test_type = test_type;
		test->test_num = fuck;
		test->arg = strdup(value);
		update_test_cfg(dic, test, 0);

		/* Create argument */
		arg_list = malloc(sizeof(struct list_head));
		INIT_LIST_HEAD(arg_list);
		vediag_get_default_argument(test_type, arg_list);

		while (!list_empty(arg_list)) {
			arg = list_entry(arg_list->next,
						struct vediag_test_argument, entry);
			if(arg) {
				list_del_init(&arg->entry);
				update_argument(dic, test->arg, arg->key, arg->value);
				if (arg->key)
					free(arg->key);
				if (arg->value)
					free(arg->value);
				free(arg);
			}
		}
#ifdef CONFIG_VENUS
		if (xgetenv(FORCE_REMOTE_ENV) != NULL) {
			update_argument(dic, test->arg, NETWORK_LOOPBACK_TOKEN, "no");
			if (strstr(test->physical, MELLANOX_10G_NAME) != NULL || strstr(test->physical, MELLANOX_100G_NAME) != NULL) {
				if (xgetenv(IPERF_APP_ENV) != NULL) {
					xprint("[%s] xgetenv(IPERF_APP_ENV) != NULL\n", test->physical);
					if (test->app)
						free(test->app);
					test->app = strdup(NETWORK_IPERF_APP_NAME);
					update_test_cfg(dic, test, 0);
					update_argument(dic, test->arg, NETWORK_INTERVAL_TOKEN, DEFAULT_IPERF_INTERVAL);
					update_argument(dic, test->arg, NETWORK_IPERF_THREAD_TOKEN, DEFAULT_IPERF_THREAED);
					update_argument(dic, test->arg, NETWORK_THREAD_TOKEN, "1");
					update_argument(dic, test->arg, NETWORK_TIME_TOKEN, DEFAULT_TEST_TIME);
					update_argument(dic, test->arg, NETWORK_CHUNK_SIZE_TOKEN, "");
					update_argument(dic, test->arg, NETWORK_TEST_SIZE_TOKEN, "");
					update_argument(dic, test->arg, NETWORK_TEST_TIME_TOKEN, DEFAULT_TEST_TIME);
				}
			}
		} else {
			if (strstr(test->physical, "MLX0") != NULL) {
				update_argument(dic, test->arg, NETWORK_LOOPBACK_TOKEN, "yes");
				update_argument(dic, test->arg, NETWORK_USER_TOKEN, "root");
				update_argument(dic, test->arg, NETWORK_PASSWORD_TOKEN, "root");
				val = strdup(test->physical);
				len = strlen(val);
				*(val + len - 1) = 0x31;
				update_argument(dic, test->arg, NETWORK_SERVER_TOKEN, val);
				free(val);

				if (xgetenv(IPERF_APP_ENV) != NULL) {
					if (test->app)
						free(test->app);
					test->app = strdup(NETWORK_IPERF_APP_NAME);
					update_test_cfg(dic, test, 0);
					update_argument(dic, test->arg, NETWORK_INTERVAL_TOKEN, DEFAULT_IPERF_INTERVAL);
					update_argument(dic, test->arg, NETWORK_IPERF_THREAD_TOKEN, DEFAULT_IPERF_THREAED);
					update_argument(dic, test->arg, NETWORK_THREAD_TOKEN, "8");
					update_argument(dic, test->arg, NETWORK_TIME_TOKEN, DEFAULT_TEST_TIME);
					update_argument(dic, test->arg, NETWORK_CHUNK_SIZE_TOKEN, "");
					update_argument(dic, test->arg, NETWORK_TEST_SIZE_TOKEN, "");
					update_argument(dic, test->arg, NETWORK_TEST_TIME_TOKEN, DEFAULT_TEST_TIME);
					update_argument(dic, test->arg, NETWORK_IPERF_MIN_SPEED_TOKEN, MELLANOX_10G_MIN_SPEED);
				}
			} else if (strstr(test->physical, "PCI0") != NULL
					&& strstr(test->physical, MELLANOX_100G_NAME) != NULL) {
				update_argument(dic, test->arg, NETWORK_LOOPBACK_TOKEN, "yes");
				update_argument(dic, test->arg, NETWORK_USER_TOKEN, "root");
				update_argument(dic, test->arg, NETWORK_PASSWORD_TOKEN, "root");
//				update_argument(dic, test->arg, NETWORK_SERVER_TOKEN, "PCI2-"MELLANOX_100G_NAME);
				val = strdup(test->physical);
				*(val + 3) = 0x32;
				update_argument(dic, test->arg, NETWORK_SERVER_TOKEN, val);
				if (xgetenv(IPERF_APP_ENV) != NULL) {
					if (test->app)
						free(test->app);
					test->app = strdup(NETWORK_IPERF_APP_NAME);
					update_test_cfg(dic, test, 0);
					update_argument(dic, test->arg, NETWORK_INTERVAL_TOKEN, DEFAULT_IPERF_INTERVAL);
					update_argument(dic, test->arg, NETWORK_IPERF_THREAD_TOKEN, DEFAULT_IPERF_THREAED);
					update_argument(dic, test->arg, NETWORK_THREAD_TOKEN, "16");
					update_argument(dic, test->arg, NETWORK_TIME_TOKEN, DEFAULT_TEST_TIME);
					update_argument(dic, test->arg, NETWORK_CHUNK_SIZE_TOKEN, "");
					update_argument(dic, test->arg, NETWORK_TEST_SIZE_TOKEN, "");
					update_argument(dic, test->arg, NETWORK_TEST_TIME_TOKEN, DEFAULT_TEST_TIME);
					update_argument(dic, test->arg, NETWORK_IPERF_MIN_SPEED_TOKEN, MELLANOX_100G_MIN_SPEED);
				}
			}
		}


#endif

		free_test_cfg(test);
		fuck++;
	}

    return 0;
}

int vediag_create_test(char *test_file)
{
	FILE *file;
	dictionary * dic;

	xinfo("Create new test case '%s'\n", test_file);
	/* Create new dictionary */
	dic = dictionary_new(0);

	__vediag_create_test(dic, VEDIAG_MEM);
	__vediag_create_test(dic, VEDIAG_STORAGE);
	/*__vediag_create_test(dic, VEDIAG_NET);*/
	__vediag_create_network_test(dic, VEDIAG_NET);
	__vediag_create_test(dic, VEDIAG_PCIE);
	__vediag_create_test(dic, VEDIAG_LED);

	/* Save INI file */
	file = fopen(test_file, "w");
	iniparser_dump_ini(dic, file);
	fclose(file);
	iniparser_freedict(dic);

    return 0;
}

int vediag_test_config (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int arp;
	char *test_file = NULL;

    if ((arp = vediag_getopt(argc, argv, "-f")) > 0)
    	test_file = argv[arp + 1];

    if(((test_file != NULL) && (strncmp(test_file, "testall.ini", strlen("testall.ini")) == 0)) || 
    	(test_file == NULL)) {
    	memset(file_testconfig, 0, sizeof(file_testconfig));
    	strncpy(file_testconfig, "testall.ini", strlen("testall.ini"));
    }

    /* Create new test case */
    if ((arp = vediag_getopt(argc, argv, "-a")) > 0) {
    	if (!test_file) 
    		return vediag_create_test("testall.ini");
    	else
    		return vediag_create_test(test_file);
    }

    /* Configure test case */
	if (!test_file)
		return vediag_test_management("testall.ini");
	else
		return vediag_test_management(test_file);

	xinfo ("Usage:\n%s\n", cmdtp->usage);
#ifdef	CONFIG_LONGHELP
	xinfo ("%s\n", cmdtp->help);
#endif
	return 1;
}

VEDIAG_CMD(
	vetest,	4,	0,	vediag_test_config,
    "Config diag test",
	"vetest [option]                       - Config test case\n"
	"       -f <file>                     - Config test file. Default is 'testall.ini' file\n"
	"       -a                            - Add all device in system to test"
)
