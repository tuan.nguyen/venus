/*
 * cmd_ini.c
 *
 *  Created on: Mar 8, 2017
 *      Author: nnguyen
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "iniparser.h"
#include <cmd.h>
#include <log.h>

void create_example_ini_file(void)
{
	FILE * file;

	file = fopen("example.ini", "w");
	fprintf(file,
			"\n\
			#\n\
			# This is an example of ini file\n\
			#\n\
			\n\
			[Pizza]\n\
			\n\
			Ham       = yes ;\n\
			Mushrooms = TRUE ;\n\
			Capres    = 0 ;\n\
			Cheese    = NO ;\n\
			\n\
			\n\
			[Wine]\n\
			\n\
			Grape     = Cabernet Sauvignon ;\n\
			Year      = 1989 ;\n\
			Country   = Spain ;\n\
			Alcohol   = 12.5  ;\n\
			\n\
			#\n\
			# end of file\n\
			#\n");

	fclose(file);
}

int parse_example_ini_file(char *ini_name)
{
	dictionary * dic;

	/* Some temporary variables to hold query results */
	int b;
	int i;
	double d;
	char * s;

	dic = iniparser_load(ini_name);
	if (dic == NULL) {
	    xerror("cannot parse file [%s]", ini_name);
		return -1;
	}
	iniparser_dump(dic, stderr);

	/* Get pizza attributes */
	xinfo("Pizza:\n");

	b = iniparser_getboolean(dic, "pizza:ham", -1);
	xinfo("Ham:       [%d]\n", b);
	b = iniparser_getboolean(dic, "pizza:mushrooms", -1);
	xinfo("Mushrooms: [%d]\n", b);
	b = iniparser_getboolean(dic, "pizza:capres", -1);
	xinfo("Capres:    [%d]\n", b);
	b = iniparser_getboolean(dic, "pizza:cheese", -1);
	xinfo("Cheese:    [%d]\n", b);

	/* Get wine attributes */
	xinfo("Wine:\n");
	s = iniparser_getstring(dic, "wine:grape", NULL);
	if (s) {
	    xinfo("grape:     [%s]\n", s);
	} else {
	    xinfo("grape:     not found\n");
	}
	i = iniparser_getint(dic, "wine:year", -1);
	if (i > 0) {
	    xinfo("year:      [%d]\n", i);
	} else {
	    xinfo("year:      not found\n");
	}
	s = iniparser_getstring(dic, "wine:country", NULL);
	if (s) {
	    xinfo("country:   [%s]\n", s);
	} else {
	    xinfo("country:   not found\n");
	}
	d = iniparser_getdouble(dic, "wine:alcohol", -1.0);
	if (d > 0.0) {
	    xinfo("alcohol:   [%g]\n", d);
	} else {
	    xinfo("alcohol:   not found\n");
	}

	iniparser_freedict(dic);
	return 0;
}

int parse_ini_file(char *ini_name)
{
	dictionary * dic;

	dic = iniparser_load(ini_name);
	if (dic == NULL) {
	    xerror("cannot parse file [%s]", ini_name);
		return -1;
	}
	iniparser_dump(dic, stderr);

	iniparser_freedict(dic);
	return 0;
}

int search_ini_key(char *ini_name, char *key)
{
	dictionary * dic;
	char * s;

	dic = iniparser_load(ini_name);
	if (dic == NULL) {
		xerror("cannot parse file [%s]", ini_name);
		return -1;
	}

	s = iniparser_getstring(dic, key, NULL);
	if (s) {
	    xinfo("Key =  %s\nValue = %s\n", key, s);
	} else {
	    xinfo("%s --> not found\n", key);
	}

	iniparser_freedict(dic);
	return 0;
}

static int do_iniparser(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int status;

	if (argc < 2) {
		create_example_ini_file();
		status = parse_example_ini_file("example.ini");
	}

	if (argc >= 3) {
		status = search_ini_key(argv[1], argv[2]);
	} else {
		status = parse_ini_file(argv[1]);
	}

	return status;
}

VEDIAG_CMD(iniparser, 16, 0, do_iniparser,
		"For testing iniparser library",
		"")
