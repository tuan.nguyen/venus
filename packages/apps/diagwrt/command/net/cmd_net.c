/*
 * cmd_net.c
 *
 *  Created on: Mar 22, 2017
 *      Author: nnguyen
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <string.h>
#include <sys/socket.h>
#include <linux/socket.h>
#include <linux/in.h>
#include <linux/if.h>
#include <linux/sockios.h>

#include <time.h>       /* time */
#include <time_util.h>


#include <linux/netdevice.h>
#include <linux/net.h>

#include <ctype.h>

#include <common.h>
#include <listbuff.h>

#include <cmd.h>
#include <vediagmod.h>
#include <vediag_common.h>
#include <vediag_net.h>

static char *__get_hw_address_from_interface_name(char *interface_name)
{
	struct ifreq ifr;
	int ret = 0;

	char *dev = interface_name;
	unsigned char mac_address[6];
	static char hw_addr[HARDWARE_ADDRESS_LENGTH];

	/* Open socket */
	int file = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
	if (file < 0) {
		printf("Could not open socket \"%s\"(error code %d)", dev, file);
		return NULL;
	}

	/* Zero mem for ifr */
	memset(&ifr, 0, sizeof(ifr));

	/* I want IP address attached to devname */
	strcpy(ifr.ifr_name, dev);

	/* Get info */
	ret = ioctl(file, SIOCGIFHWADDR, &ifr);	//SIOCGIFHWADDR: Get HW Address

	/* Close socket */
	close(file);

	printf("Debug Info: Index = %d | Family: %d | Name: %s\n", ifr.ifr_ifindex, ifr.ifr_addr.sa_family, ifr.ifr_name);

	if(ret != 0) {
		printf("No MAC adress for %s or %s does not exist -> Fail\n", dev, dev);
		return NULL;
	} else {
		memcpy(mac_address, ifr.ifr_hwaddr.sa_data, 6);
		snprintf(hw_addr, HARDWARE_ADDRESS_LENGTH, "%02x:%02x:%02x:%02x:%02x:%02x", mac_address[0], mac_address[1], mac_address[2],
				mac_address[3], mac_address[4], mac_address[5]);
		printf("%s's MAC: %s\n", dev, hw_addr);
		return hw_addr;
	}
}

#define ARG_TOTAL_NUMBER 13
static int do_vediag_iperf_test(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	/* testiperf <eth1> <eth2> <time> */
	static int first_time = 0;
	int ret = 0;
	unsigned int time_sec;
	char dev[10] = "eth2";
	char dev2[10] = "eth3";
	char hw_addr[HARDWARE_ADDRESS_LENGTH];
	char cmd[100];

	if (argc < 4) {
		printf("Error: Invalid argument\n");
		goto exit;
	}

	memset(dev, 0, sizeof(dev));
	strcpy(dev, argv[1]);
	
	memset(dev2, 0, sizeof(dev2));
	strcpy(dev2, argv[2]);
	time_sec = strtoul(argv[3], NULL, 10);	

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "ifconfig %s 12.50.0.1 netmask 255.255.255.0", dev);
	ret = system(cmd);
	if (ret) goto exit;

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "ifconfig %s 12.50.1.1 netmask 255.255.255.0", dev2);
	ret = system(cmd);
	if (ret) goto exit;

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "iptables -t nat -L");
	ret = system(cmd);
	if (ret) goto exit;

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "iptables -t nat -A POSTROUTING -s 12.50.0.1 -d 12.60.1.1 -j SNAT --to-source 12.60.0.1");
	ret = system(cmd);
	if (ret) goto exit;

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "iptables -t nat -A PREROUTING -d 12.60.0.1 -j DNAT --to-destination 12.50.0.1");
	ret = system(cmd);
	if (ret) goto exit;

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "iptables -t nat -A POSTROUTING -s 12.50.1.1 -d 12.60.0.1 -j SNAT --to-source 12.60.1.1");
	ret = system(cmd);
	if (ret) goto exit;

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "iptables -t nat -A PREROUTING -d 12.60.1.1 -j DNAT --to-destination 12.50.1.1");
	ret = system(cmd);
	if (ret) goto exit;

	if (first_time == 0) {
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "ip route add 12.60.1.1 dev %s", dev);
		ret = system(cmd);
		if (ret) {
			printf("FAIL! %s\n", cmd);
			printf("System return code = %d\n", ret);
			/*goto exit;*/
		}

		/*get_hw_address_from_interface_name*/
		strncpy(hw_addr, __get_hw_address_from_interface_name(dev2), HARDWARE_ADDRESS_LENGTH);
		printf("HW ADDR:%s\n", hw_addr);
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "arp -i %s -s 12.60.1.1 %s", dev, hw_addr);
		ret = system(cmd);
		if (ret) goto exit;

		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "ip route add 12.60.0.1 dev %s", dev2);
		ret = system(cmd);
		if (ret) {
			printf("FAIL! %s\n", cmd);
			printf("System return code = %d\n", ret);
			/*goto exit;*/
		}

		/*get_hw_address_from_interface_name*/
		strncpy(hw_addr, __get_hw_address_from_interface_name(dev), HARDWARE_ADDRESS_LENGTH);
		printf("HW ADDR:%s\n", hw_addr);
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "arp -i %s -s 12.60.0.1 %s", dev2, hw_addr);
		ret = system(cmd);
		if (ret) goto exit;

		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "ping -c 5 12.60.1.1");
		ret = system(cmd);
		if (ret) {
			printf("FAIL! %s\n", cmd);
			printf("System return code = %d\n", ret);
			/*goto exit;*/
		}

		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "iperf -B 12.50.0.1 -s &"); /*disable print at server*/
		printf(" Run server: %s\n", cmd);
		ret = system(cmd);
		if (ret)
			goto exit;

		first_time = 1;
	}

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "iperf -B 12.50.1.1 -c 12.60.0.1 -i 5 -P 8 -t %u", time_sec);
	printf(" Run client: %s\n", cmd);
	ret = system(cmd);
	if (ret)
		goto exit;

exit:

	if (ret != 0) {
		printf("System return code = %d\n", ret);
		printf("Usage: testiperf <eth1> <eth2> <time_seconds>\n");
	}

	return ret;
}

VEDIAG_CMD(testiperf, 16, 0, do_vediag_iperf_test,
			"vediag iperf_test utilities",
			"testiperf <eth1> <eth2> <time_seconds>")

extern struct listbuff *get_hwname_of_eth_interface(const char *intf);

static int do_inet(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	if (argc < 2) {
		return cmd_usage(cmdtp);
	}

	struct listbuff *hwname = get_hwname_of_eth_interface(argv[1]);
	if (likely(hwname != NULL)) {
		printf("\nHW Name = %s\n", (char *) hwname->buff);
	}

	free_listbuff_list(&hwname);
	return 0;
}


VEDIAG_CMD(inet, 16, 0, do_inet,
		"For Identify ethernet interface",
		"Usage:\n" \
		"inet <interface>"
		)
