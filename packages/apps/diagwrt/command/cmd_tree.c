/*
 * cmd_tree.c
 *
 *  Created on: Feb 7, 2014
 *      Author: nnguyen
 */

#include <stdio.h>
#include <string.h>
#include <tree.h>
#include <log.h>
#include <time_util.h>
#include <cmd.h>

/*#define LIST_DIR_DEBUG*/
#ifdef LIST_DIR_DEBUG
#define ldirdbg(x, fmt ...)	    xinfo(x, ##fmt)
#else
#define ldirdbg(x, fmt ...)		do {}while(0);
#endif

long folder_num = 0;
long file_num = 0;
int list_dir(const char *path, struct tree_node **node)
{
	DIR *d;
	struct dirent *dir;
	struct tree_node *tmp = *node;
	struct tree_node *child = NULL;
	char fpath[FILENAME_MAX + 1];
	d = opendir(path);
	if (d) {
		if (!tmp) {
			tmp = tree_alloc(path);
			*node = tmp;
		}
		while ((dir = readdir(d)) != NULL) {
			sprintf(fpath, "%s%s%s%c", path, "/", dir->d_name, 0);
			ldirdbg("%s, fpath=%s\n", __func__, fpath);
			if ((strcmp(dir->d_name, ".") == 0)
					|| (strcmp(dir->d_name, "..") == 0))
				continue;
			child = tree_add_node_name(tmp, dir->d_name);
			if (stat(fpath, &child->stat) == 0) {
				if (child->stat.st_mode & S_IFDIR) {
					folder_num++;
					list_dir(fpath, &child);
				} else {
					file_num++;
				}
			}
		}
		closedir(d);
		return 0;
	} else {
		xerror("Couldn't open folder %s\n", path);
		return -1;
	}
}

static int do_tree(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	if (argc < 2) {
		cmd_usage(cmdtp);
		return -1;
	}
	const char *path = argv[1];

	struct timeval startTime;
	gettimeofday(&startTime, NULL);
	struct tree_node *root = tree_alloc(path);
	xinfo("Iterating folder %s ...\n", path);
	list_dir(root->fname, &root);
	tree_traversal(root);
	tree_destroy(&root);

	if (argc >= 3) {
		xdebug("\n%ld folder, %ld files\n", folder_num, file_num);
		struct timeval stopTime;
		gettimeofday(&stopTime, NULL);
		struct timeval elapsedTime;
		timeval_subtract(&elapsedTime, &stopTime, &startTime);
		xdebug("ElapsedTime: %ld (ms)\n",
				(elapsedTime.tv_sec * 1000) + (elapsedTime.tv_usec / 1000));
	}

	xinfo("Done.\n");

	return 0;
}

VEDIAG_CMD(tree, 16, 0, do_tree,
		"Listing folder & show in tree format",
		"")
