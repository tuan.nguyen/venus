/*
 * cmds.c
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#ifdef __USE_READLINE__
#include <readline/readline.h>
#include <readline/history.h>
#endif

#include <cmd.h>
#include <graphic.h>
#include <log.h>

/*#define CMD_DEBUG*/
#ifdef CMD_DEBUG
#define cmd_dbg(x, fmt ...) xinfo(x, ##fmt)
#else
#define cmd_dbg(x, fmt ...) do{}while(0)
#endif

static LIST_HEAD(cmd_table);
#define list_add_cmd(cmd) \
	list_add(&cmd->lst, &cmd_table)
#define list_del_cmd(cmd) \
	list_del(&cmd->lst)
#define list_for_each_cmd(cmd) \
	list_for_each_entry(cmd, &cmd_table, lst)

extern int vediag_exit;

/**
 * Check the command in table to see if it's already exist
 */
cmd_tbl_t *check_cmd(const char *cmd)
{
	cmd_dbg("->check_cmd: %s\n", cmd->name);
	cmd_tbl_t *iter = NULL;
	list_for_each_cmd(iter) {
		if (strcmp(cmd, iter->name) == 0) {
			return iter;
		}
	}

	return NULL;
}

/**
 * Add command
 */
void add_cmd(cmd_tbl_t *cmd)
{
	cmd_dbg("->add_cmd: %s\n", cmd->name);
	int sts = -1;
	if (check_cmd(cmd->name) == NULL) {
		list_add_cmd(cmd);
		sts = 0;
	}
	xdebug("Registered command \"%s\" is %s\n", cmd->name,
	            (sts != 0) ? "FAIL!!!" : "OK");
}

/**
 * Remove command
 */
void remove_cmd(cmd_tbl_t *cmd)
{
	cmd_dbg("->remove_cmd: %s", cmd->name);
	list_del_cmd(cmd);
}

/**
 * Counting number of command
 */
static int count_cmd(void)
{
	int i = 0;
	cmd_tbl_t *cmdtp = NULL;
	list_for_each_cmd(cmdtp) {
		i++;
	}
	return i;
}


/*
 * Use puts() instead of printf() to avoid printf buffer overflow
 * for long help messages
 */
static int do_help (cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	int i;
	int rcode = 0;

	if (argc == 1) { /*show list of commands */

		int cmd_items = count_cmd();
		cmd_tbl_t *cmd_array[cmd_items];
		int i = 0, j, swaps;

		/* Make array of commands from .uboot_cmd section */
		list_for_each_cmd(cmdtp) {
			cmd_array[i++] = cmdtp;
		}

		/* Sort command list (trivial bubble sort) */
		for (i = cmd_items - 1; i > 0; --i) {
			swaps = 0;
			for (j = 0; j < i; ++j) {
				if (strcmp(cmd_array[j]->name, cmd_array[j + 1]->name) > 0) {
					cmd_tbl_t *tmp;
					tmp = cmd_array[j];
					cmd_array[j] = cmd_array[j + 1];
					cmd_array[j + 1] = tmp;
					++swaps;
				}
			}
			if (!swaps)
				break;
		}

		/* print short help (usage) */
		for (i = 0; i < cmd_items; i++) {
			const char *name = cmd_array[i]->name;
			const char *usage = cmd_array[i]->usage;

			if (usage == NULL)
				continue;
			xprint("%-16s - %s\n", name, usage);
		}
		return 0;
	}

	/*
	 * command help (long version)
	 */
	for (i = 1; i < argc; ++i) {
		if ((cmdtp = find_cmd(argv[i])) != NULL) {
#ifdef	CONFIG_LONGHELP
			/* found - print (long) help info */
			if (cmdtp->help) {
				xprint(cmdtp->help);
			} else {
				xprint("- No help available.\n");
				rcode = 1;
			}
			xprint("\n");
#else	/* no long help available */
			if (cmdtp->usage)
			xprint (cmdtp->usage);
#endif	/* CONFIG_LONGHELP */
		} else {
			xprint("Unknown command '%s' - try 'help'"
					" without arguments for list of all"
					" known commands\n\n", argv[i]);
			rcode = 1;
		}
	}
	return rcode;
}


VEDIAG_CMD(
	help, 32, 1, do_help,
	"print online help",
	"[command ...]\n"
	"    - show help information (for 'command')\n"
	"'help' prints online help for the monitor commands.\n\n"
	"Without arguments, it prints a short usage message for all commands.\n\n"
	"To get detailed help information for specific commands you can type\n"
  "'help' with one or more command names as arguments.\n"
);

int do_exit(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int r;

	r = 0;
	if (argc > 1)
		r = strtoul(argv[1], NULL, 10);

	vediag_exit = r + 1;

	return 0;
}

VEDIAG_CMD( exit, 2, 1, do_exit,
		"exit program",
		"    - exit functionality\n");

int do_quit(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int r;

	r = 0;
	if (argc > 1)
		r = strtoul(argv[1], NULL, 10);

	vediag_exit = r + 1;

	return 0;
}

VEDIAG_CMD( quit, 2, 1, do_quit,
		"quit program",
		"    - quit functionality\n");


/**
 * find command table entry for a command
 */
cmd_tbl_t *find_cmd(const char *cmd)
{
	cmd_dbg("->find_cmd: %s\n", cmd);
	cmd_tbl_t *cmdtp = NULL;
	int len;
	const char *p;
	int found = 0;
	if (cmd == NULL) {
		return NULL;
	}

	/*
	 * Some commands allow length modifiers (like "cp.b");
	 * compare command name only until first dot.
	 */
	len = ((p = strchr(cmd, '.')) == NULL) ? strlen(cmd) : (p - cmd);

	cmd_tbl_t *iter = NULL;
	list_for_each_cmd(iter) {
		if (strncmp(cmd, iter->name, len) == 0) {
			if (len == strlen(iter->name)) {
				return iter; /* full match */
			}
			cmdtp = iter;
			found++;
		}
	}

	if (found == 1) {
		return cmdtp;
	}

	return NULL;
}

int cmd_usage(cmd_tbl_t *cmdtp)
{
	xprint("%s - %s\n\n", cmdtp->name, cmdtp->usage);

#ifdef	CONFIG_SYS_LONGHELP
	xprint("Usage:\n%s ", cmdtp->name);

	if (!cmdtp->help) {
		xprint("- No additional help available.\n");
		return 1;
	}

	xprint("%s\n",cmdtp->help);
	xprint('\n');
#endif	/* CONFIG_SYS_LONGHELP */
	return 0;
}

int cmd_get_data_size(char* arg, int default_size)
{
	/* Check for a size specification .b, .w or .l.
	 */
	int len = strlen(arg);
	if (len > 2 && arg[len-2] == '.') {
		switch(arg[len-1]) {
		case 'b':
			return 1;
		case 'w':
			return 2;
		case 'l':
			return 4;
		case 's':
			return -2;
		default:
			return -1;
		}
	}
	return default_size;
}
