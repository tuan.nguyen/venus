/*  This file is part of "sshpass", a tool for batch running password ssh authentication
 *  Copyright (C) 2006, 2015 Lingnu Open Source Consulting Ltd.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version, provided that it was accepted by
 *  Lingnu Open Source Consulting Ltd. as an acceptable license for its
 *  projects. Consult http://www.lingnu.com/licenses.html
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Define to 1 if you have the <fcntl.h> header file. */
#define HAVE_FCNTL_H 1

/* Define to 1 if you have the `fork' function. */
#define HAVE_FORK 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if your system has a GNU libc compatible `malloc' function, and
 to 0 otherwise. */
#define HAVE_MALLOC 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have the `posix_openpt' function. */
#define HAVE_POSIX_OPENPT 1

/* Define to 1 if you have the `select' function. */
#define HAVE_SELECT 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the `strdup' function. */
#define HAVE_STRDUP 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the <sys/ioctl.h> header file. */
#define HAVE_SYS_IOCTL_H 1

/* Define to 1 if you have the <sys/select.h> header file. */
#define HAVE_SYS_SELECT_H 1

/* Define to 1 if you have the <sys/socket.h> header file. */
#define HAVE_SYS_SOCKET_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have <sys/wait.h> that is POSIX.1 compatible. */
#define HAVE_SYS_WAIT_H 1

/* Define to 1 if you have the <termios.h> header file. */
#define HAVE_TERMIOS_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to 1 if you have the `vfork' function. */
#define HAVE_VFORK 1

/* Define to 1 if you have the <vfork.h> header file. */
/* #undef HAVE_VFORK_H */

/* Define to 1 if `fork' works. */
#define HAVE_WORKING_FORK 1

/* Define to 1 if `vfork' works. */
#define HAVE_WORKING_VFORK 1

/* Name of package */
#define PACKAGE "sshpass"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT ""

/* Define to the full name of this package. */
#define PACKAGE_NAME "sshpass"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "sshpass 1.06"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "sshpass"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.06"

/* Password prompt to use */
#define PASSWORD_PROMPT "assword"

/* Define as the return type of signal handlers (`int' or `void'). */
#define RETSIGTYPE void

/* Define to the type of arg 1 for `select'. */
#define SELECT_TYPE_ARG1 int

/* Define to the type of args 2, 3 and 4 for `select'. */
#define SELECT_TYPE_ARG234 (fd_set *)

/* Define to the type of arg 5 for `select'. */
#define SELECT_TYPE_ARG5 (struct timeval *)

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Enable extensions on AIX 3, Interix.  */
#ifndef _ALL_SOURCE
# define _ALL_SOURCE 1
#endif
/* Enable GNU extensions on systems that have them.  */
#ifndef _GNU_SOURCE
# define _GNU_SOURCE 1
#endif
/* Enable threading extensions on Solaris.  */
#ifndef _POSIX_PTHREAD_SEMANTICS
# define _POSIX_PTHREAD_SEMANTICS 1
#endif
/* Enable extensions on HP NonStop.  */
#ifndef _TANDEM_SOURCE
# define _TANDEM_SOURCE 1
#endif
/* Enable general extensions on Solaris.  */
#ifndef __EXTENSIONS__
# define __EXTENSIONS__ 1
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <sys/select.h>

#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#if HAVE_TERMIOS_H
#include <termios.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>

#include <cmd.h>
#include <log.h>

enum program_return_codes {
	RETURN_NOERROR,
	RETURN_INVALID_ARGUMENTS,
	RETURN_CONFLICTING_ARGUMENTS,
	RETURN_RUNTIME_ERROR,
	RETURN_PARSE_ERRROR,
	RETURN_INCORRECT_PASSWORD,
	RETURN_HOST_KEY_UNKNOWN,
	RETURN_HOST_KEY_CHANGED,
};

// Some systems don't define posix_openpt
#ifndef HAVE_POSIX_OPENPT
int
posix_openpt(int flags)
{
	return open("/dev/ptmx", flags);
}
#endif

struct args_struct{
	enum {
		PWT_STDIN,
		PWT_FILE,
		PWT_FD,
		PWT_PASS
	} pwtype;
	union {
		const char *filename;
		int fd;
		const char *password;
	} pwsrc;

	const char *pwprompt;
	int verbose;
	int masterpt;
	int ourtty;
	int slavept;
	int prevmatch;	// If the "password" prompt is repeated, we have the wrong password.
};

int runprogram(int argc, char *argv[], struct args_struct * priv_args);

static void show_help(void)
{
	xprint(  "Usage: " PACKAGE_NAME " [-f|-d|-p|-e] [-hV] command parameters\n"
			"   -f filename   Take password to use from file\n"
			"   -d number     Use number as file descriptor for getting password\n"
			"   -p password   Provide password as argument (security unwise)\n"
			"   -e            Password is passed as env-var \"SSHPASS\"\n"
			"   With no parameters - password will be taken from stdin\n\n"
			"   -P prompt     Which string should sshpass search for to detect a password prompt\n"
			"   -v            Be verbose about what you're doing\n"
			"   -h            Show help (this screen)\n"
			"   -V            Print version information\n"
			"At most one of -f, -d, -p or -e should be used\n");
}

// Parse the command line. Fill in the "args" global struct with the results. Return argv offset
// on success, and a negative number on failure
static int parse_options(int argc, char *argv[], struct args_struct *priv_args)
{
	int error = -1;
	int opt;

	// Set the default password source to stdin
	priv_args->pwtype = PWT_STDIN;
	priv_args->pwsrc.fd = 0;
	optind = 1;

#define VIRGIN_PWTYPE if( priv_args->pwtype!=PWT_STDIN ) { \
    xerror("Conflicting password source\n"); \
    error=RETURN_CONFLICTING_ARGUMENTS; }

	while ((opt = getopt(argc, argv, "+f:d:p:P:heVv")) != -1 && error == -1) {
		switch (opt) {
		case 'f':
			// Password should come from a file
			VIRGIN_PWTYPE
			;

			priv_args->pwtype = PWT_FILE;
			priv_args->pwsrc.filename = optarg;
			break;
		case 'd':
			// Password should come from an open file descriptor
			VIRGIN_PWTYPE
			;

			priv_args->pwtype = PWT_FD;
			priv_args->pwsrc.fd = atoi(optarg);
			break;
		case 'p':
			// Password is given on the command line
			VIRGIN_PWTYPE
			;

			priv_args->pwtype = PWT_PASS;
			priv_args->pwsrc.password = strdup(optarg);
#if 0
			// Hide the original password from the command line
			{
				int i;

				for (i = 0; optarg[i] != '\0'; ++i)
					optarg[i] = 'z';
			}
#endif
			break;
		case 'P':
			priv_args->pwprompt = optarg;
			break;
		case 'v':
			priv_args->verbose = 1;
			break;
		case 'e':
			VIRGIN_PWTYPE
			;

			priv_args->pwtype = PWT_PASS;
			priv_args->pwsrc.password = getenv("SSHPASS");
			if (priv_args->pwsrc.password == NULL) {
				xinfo("sshpass: -e option given but SSHPASS environment variable not set\n");
				error = RETURN_INVALID_ARGUMENTS;
			}
			break;
		case '?':
		case ':':
			error = RETURN_INVALID_ARGUMENTS;
			break;
		case 'h':
			error = RETURN_NOERROR;
			break;
		case 'V':
			xinfo(  "%s\n"
					"(C) 2006-2011 Lingnu Open Source Consulting Ltd.\n"
					"(C) 2015-2016 Shachar Shemesh\n"
					"This program is free software, and can be distributed under the terms of the GPL\n"
					"See the COPYING file for more information.\n"
					"\n"
					"Using \"%s\" as the default password prompt indicator.\n",
					PACKAGE_STRING, PASSWORD_PROMPT);
			return 0;
		}
	}

	if (error >= 0)
		return -(error + 1);
	else
		return optind;
}

int do_sshpass(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int ret = 0;
	struct args_struct *priv_args;

	priv_args = (struct args_struct *) malloc(sizeof(struct args_struct));
	if (priv_args == NULL) {
		xinfo("Not enough resources\n");
		return -1;
	}

	/* Initialize private arguments*/
	memset(priv_args, 0, sizeof(struct args_struct));

	/* Parse to get options */
	int opt_offset = parse_options(argc, argv, priv_args);

	if (opt_offset < 0) {
		show_help();
		free(priv_args);
		return -(opt_offset + 1); // -1 becomes 0, -2 becomes 1 etc.
	}

	if (argc - opt_offset < 1) {
		show_help();
		free(priv_args);
		return 0;
	}

	/* Run main program with private arguments */
	ret = runprogram(argc - opt_offset, argv + opt_offset, priv_args);
	free(priv_args);
	return ret;
}

VEDIAG_CMD(sshpass, 16, 0, do_sshpass,
		"","")

int handleoutput(int fd, struct args_struct *priv_args);

/* Global variables so that this information be shared with the signal handler */
static int ourtty; // Our own tty
static int masterpt;
void window_resize_handler(int signum);
void sigchld_handler(int signum);
pthread_mutex_t tty_mutex = PTHREAD_MUTEX_INITIALIZER;

int runprogram(int argc, char *argv[], struct args_struct *priv_args)
{
	struct winsize ttysize; // The size of our tty

	// We need to interrupt a select with a SIGCHLD. In order to do so, we need a SIGCHLD handler
	signal(SIGCHLD, sigchld_handler);

	// Create a pseudo terminal for our process
	priv_args->masterpt = posix_openpt(O_RDWR);

	if (priv_args->masterpt == -1) {
		xinfo("Failed to get a pseudo terminal %s\n", strerror(errno));
		return RETURN_RUNTIME_ERROR;
	}

	fcntl(priv_args->masterpt, F_SETFL, O_NONBLOCK);

	if (grantpt(priv_args->masterpt) != 0) {
		xinfo("Failed to change pseudo terminal's permission %s\n", strerror(errno));
		return RETURN_RUNTIME_ERROR;
	}
	if (unlockpt(priv_args->masterpt) != 0) {
		xinfo("Failed to unlock pseudo terminal %s\n", strerror(errno));
		return RETURN_RUNTIME_ERROR;
	}

	/*pthread_mutex_lock(&tty_mutex);*/
	priv_args->ourtty = open("/dev/tty", 0);

	if (priv_args->ourtty != -1 && ioctl(priv_args->ourtty, TIOCGWINSZ, &ttysize) == 0) {
		masterpt = priv_args->masterpt;
		ourtty = priv_args->ourtty;
		signal(SIGWINCH, window_resize_handler);

		ioctl(priv_args->masterpt, TIOCSWINSZ, &ttysize);
	}

	const char *name = ptsname(priv_args->masterpt);
	/*
	 Comment no. 3.14159

	 This comment documents the history of code.

	 We need to open the priv_args->slavept inside the child process, after "setsid", so that it becomes the controlling
	 TTY for the process. We do not, otherwise, need the file descriptor open. The original approach was to
	 close the fd immediately after, as it is no longer needed.

	 It turns out that (at least) the Linux kernel considers a master ptty fd that has no open slave fds
	 to be unused, and causes "select" to return with "error on fd". The subsequent read would fail, causing us
	 to go into an infinite loop. This is a bug in the kernel, as the fact that a master ptty fd has no slaves
	 is not a permenant problem. As long as processes exist that have the slave end as their controlling TTYs,
	 new slave fds can be created by opening /dev/tty, which is exactly what ssh is, in fact, doing.

	 Our attempt at solving this problem, then, was to have the child process not close its end of the slave
	 ptty fd. We do, essentially, leak this fd, but this was a small price to pay. This worked great up until
	 openssh version 5.6.

	 Openssh version 5.6 looks at all of its open file descriptors, and closes any that it does not know what
	 they are for. While entirely within its prerogative, this breaks our fix, causing sshpass to either
	 hang, or do the infinite loop again.

	 Our solution is to keep the slave end open in both parent AND child, at least until the handshake is
	 complete, at which point we no longer need to monitor the TTY anyways.
	 */

	volatile int childpid = fork();
	if (childpid == 0) {
		// Child

		// Detach us from the current TTY
		setsid();
		// This line makes the ptty our controlling tty. We do not otherwise need it open
		priv_args->slavept = open(name, O_RDWR);
		close(priv_args->slavept);

		close(priv_args->masterpt);

		char **new_argv = malloc(sizeof(char *) * (argc + 1));

		int i;

		for (i = 0; i < argc; ++i) {
			new_argv[i] = argv[i];
		}

		new_argv[i] = NULL;

		execvp(new_argv[0], new_argv);

		xinfo("sshpass: Failed to run command %s\n", strerror(errno));
		return RETURN_RUNTIME_ERROR;
	} else if (childpid < 0) {
		xinfo("sshpass: Failed to create child process %s\n", strerror(errno));
		return RETURN_RUNTIME_ERROR;
	}

	pthread_mutex_lock(&tty_mutex);

	// We are the parent
	priv_args->slavept = open(name, O_RDWR | O_NOCTTY);

	int status = 0;
	int terminate = 0;
	pid_t wait_id;
	sigset_t sigmask, sigmask_select;

	// Set the signal mask during the select
	sigemptyset(&sigmask_select);

	// And during the regular run
	sigemptyset(&sigmask);
	sigaddset(&sigmask, SIGCHLD);

	sigprocmask(SIG_SETMASK, &sigmask, NULL);

	do {
		if (!terminate) {
			fd_set readfd;

			FD_ZERO(&readfd);
			FD_SET(priv_args->masterpt, &readfd);

			int selret = pselect(priv_args->masterpt + 1, &readfd, NULL, NULL, NULL,
					&sigmask_select);

			if (selret > 0) {
				if (FD_ISSET(priv_args->masterpt, &readfd)) {
					int ret;
					if ((ret = handleoutput(priv_args->masterpt, priv_args))) {
						// Authentication failed or any other error

						// handleoutput returns positive error number in case of some error, and a negative value
						// if all that happened is that the slave end of the pt is closed.
						if (ret > 0) {
							close(priv_args->masterpt); // Signal ssh that it's controlling TTY is now closed
							priv_args->masterpt = 0;

							close(priv_args->slavept);
							priv_args->slavept = 0;
						}

						terminate = ret;

						// Not needed (terminate = ret > 0 --> repeat the above close slavept)
						if (terminate) {
							close(priv_args->slavept);
						}
					}
				}
			}
			wait_id = waitpid(childpid, &status, WNOHANG);
		} else {
			wait_id = waitpid(childpid, &status, 0);
		}
	} while (wait_id == 0 || (!WIFEXITED(status) && !WIFSIGNALED(status)));

	if (priv_args->masterpt > 0) {
		close(priv_args->masterpt); // Signal ssh that it's controlling TTY is now closed
	}
	if (priv_args->slavept > 0) {
		close(priv_args->slavept);
	}
	if (priv_args->ourtty > 0) {
		close(priv_args->ourtty);
	}
	priv_args->prevmatch = 0;	//Avoid Showing Wrong Password Message when Using Multiple Time
	pthread_mutex_unlock(&tty_mutex);

	if (terminate > 0) {
		return terminate;
	}
	else if (WIFEXITED(status)) {
		return WEXITSTATUS(status);
	}
	else {
		return 255;
	}
}

int match(const char *reference, const char *buffer, ssize_t bufsize, int state);
void write_pass(int fd, struct args_struct *priv_args);

int handleoutput(int fd, struct args_struct *priv_args)
{
	// We are looking for the string
	int state1 = 0, state2 = 0;
	int firsttime = 1;
	const char *compare1 = PASSWORD_PROMPT; // Asking for a password
	const char compare2[] = "The authenticity of host "; // Asks to authenticate host
	// static const char compare3[]="WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!"; // Warns about man in the middle attack
	// The remote identification changed error is sent to stderr, not the tty, so we do not handle it.
	// This is not a problem, as ssh exists immediately in such a case
	char buffer[256];
	int ret = 0;

	if (priv_args->pwprompt) {
		compare1 = priv_args->pwprompt;
	}

	if (priv_args->verbose && firsttime) {
		firsttime = 0;
		xinfo("SSHPASS searching for password prompt using match \"%s\"\n",
				compare1);
	}

	int numread = read(fd, buffer, sizeof(buffer) - 1);
	buffer[numread] = '\0';
	if (priv_args->verbose) {
		xinfo("SSHPASS read: %s\n", buffer);
	}

	state1 = match(compare1, buffer, numread, state1);

	// Are we at a password prompt?
	if (compare1[state1] == '\0') {
		if (!priv_args->prevmatch) {
			if (priv_args->verbose)
				xinfo("SSHPASS detected prompt. Sending password.\n");
			write_pass(fd, priv_args);
			state1 = 0;
			priv_args->prevmatch = 1;
		} else {
			// Wrong password - terminate with proper error code
			if (priv_args->verbose)
				xinfo("SSHPASS detected prompt, again. Wrong password. Terminating.\n");
			ret = RETURN_INCORRECT_PASSWORD;
		}
	}

	if (ret == 0) {
		state2 = match(compare2, buffer, numread, state2);

		// Are we being prompted to authenticate the host?
		if (compare2[state2] == '\0') {
			if (priv_args->verbose)
				xinfo("SSHPASS detected host authentication prompt. Exiting.\n");
			ret = RETURN_HOST_KEY_UNKNOWN;
		}
	}
	return ret;
}

int match(const char *reference, const char *buffer, ssize_t bufsize, int state)
{
	// This is a highly simplisic implementation. It's good enough for matching "Password: ", though.
	int i;
	for (i = 0; reference[state] != '\0' && i < bufsize; ++i) {
		if (reference[state] == buffer[i])
			state++;
		else {
			state = 0;
			if (reference[state] == buffer[i])
				state++;
		}
	}

	return state;
}

void write_pass_fd(int srcfd, int dstfd);

void write_pass(int fd, struct args_struct *priv_args)
{
	ssize_t r;
	switch (priv_args->pwtype) {
	case PWT_STDIN:
		write_pass_fd(STDIN_FILENO, fd);
		break;
	case PWT_FD:
		write_pass_fd(priv_args->pwsrc.fd, fd);
		break;
	case PWT_FILE: {
		int srcfd = open(priv_args->pwsrc.filename, O_RDONLY);
		if (srcfd != -1) {
			write_pass_fd(srcfd, fd);
			close(srcfd);
		}
	}
		break;
	case PWT_PASS:
		r = write(fd, priv_args->pwsrc.password, strlen(priv_args->pwsrc.password));
		r = write(fd, "\n", 1);
		if (r == -1) {
		}
		break;
	}
}

void write_pass_fd(int srcfd, int dstfd)
{
	int done = 0;
	ssize_t r;

	while (!done) {
		char buffer[40];
		int i;
		int numread = read(srcfd, buffer, sizeof(buffer));
		done = (numread < 1);
		for (i = 0; i < numread && !done; ++i) {
			if (buffer[i] != '\n')
				r = write(dstfd, buffer + i, 1);
			else
				done = 1;
		}
	}

	r = write(dstfd, "\n", 1);
	if (r == -1) {
	}
}

void window_resize_handler(int signum)
{
	struct winsize ttysize; // The size of our tty

	if (ioctl(ourtty, TIOCGWINSZ, &ttysize) == 0)
		ioctl(masterpt, TIOCSWINSZ, &ttysize);
}

// Do nothing handler - makes sure the select will terminate if the signal arrives, though.
void sigchld_handler(int signum)
{
}
