#include <stdlib.h>
#include <string.h>
#include <string_plus.h>
#include <sys/time.h>
#include <unistd.h>
#include <common.h>
#include <stdio.h>
#include <unistd.h>
#include <cmd.h>
#include <memory.h>
#include <time.h>
#include <time_util.h>
#include <vediag_common.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/kernel.h>
#include <log.h>
#include <vediag/i2c.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>

/* *****************************************************************************
 * DEFINE
 ******************************************************************************/
#define PAGE_BYTES  64

/* *****************************************************************************
 * PRIVATE FUNCTIONS PROTOTYPE
 ******************************************************************************/

/* *****************************************************************************
 * PRIVATE VARIABLE
 ******************************************************************************/
static struct i2c_test etest;
/* *****************************************************************************
 * PUPLIC VARIABLE
 ******************************************************************************/

/* *****************************************************************************
 * FUNCTIONS IMPLEMENT
 ******************************************************************************/

/*
 * eeprom_init()
 * @i2ct: input struct all i2c necessary
 * @return: 0 if pass, < 0 if fail.
 */
int eeprom_init(struct i2c_test *i2ct)
{
	int fd, ret;
	unsigned char tmp;
	char cmd[60];

	/* Sanity check */
	if (i2ct->i2cdev == NULL) {
		xerror("ERROR: i2cdev NULL\n");
		return -1;
	}

	/* Test open i2cdev */
	fd = open(i2ct->i2cdev, O_RDWR);
	if (fd < 0) {
		xerror("Failed to open I2C device!");
		return -1;
	}

	/* i2c timeout, 1 for 10ms ; if too small, i2c may lose response */
	if (ioctl(fd, I2C_TIMEOUT, 100) < 0) {
		xerror("%s() ioctl() failed!\n", __FUNCTION__);
		return -1;
	}

	/* i2c timeout, 1 for 10ms ; if too small, i2c may lose response */
	if (ioctl(fd, I2C_RETRIES, 100) < 0) {
		xerror("%s() ioctl() failed!\n", __FUNCTION__);
		return -1;
	}

	/* Store for later using */
	i2ct->fd = fd;

	if(i2ct->gpio_number != -1) {
		/* Check GPIO available */
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "ls -1 /sys/class/gpio/gpio%d", i2ct->gpio_number);
		vediag_debug(VEDIAG_I2C, i2ct->i2cdev, "cmd='%s'\n", cmd);
		ret = system(cmd);
		if (ret != 0) {
			memset(cmd, 0, sizeof(cmd));
			sprintf(cmd, "echo %d > /sys/class/gpio/export", i2ct->gpio_number);
			vediag_debug(VEDIAG_I2C, i2ct->i2cdev, "cmd='%s'\n", cmd);
			ret = system(cmd);
			if (ret != 0) {
				vediag_err(VEDIAG_I2C, i2ct->i2cdev, "export gpio[%d] failed\n", i2ct->gpio_number);
				return -EINVAL;
			}
		}
		/* GPIO active_low */
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "echo %d > /sys/class/gpio/gpio%d/active_low", i2ct->gpio_value, i2ct->gpio_number);
		vediag_debug(VEDIAG_I2C, i2ct->i2cdev, "cmd='%s'\n", cmd);
		ret = system(cmd);
		if (ret != 0) {
			vediag_err(VEDIAG_I2C, i2ct->i2cdev, "setting gpio[%d] %d-->active_low failed\n", i2ct->gpio_number, i2ct->gpio_value);
			return -EINVAL;
		}
		/* GPIO direction */
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "echo out > /sys/class/gpio/gpio%d/direction", i2ct->gpio_number);
		vediag_debug(VEDIAG_I2C, i2ct->i2cdev, "cmd='%s'\n", cmd);
		ret = system(cmd);
		if (ret != 0) {
			vediag_err(VEDIAG_I2C, i2ct->i2cdev, "setting gpio[%d] direction failed\n", i2ct->gpio_number);
			return -EINVAL;
		}
	}
	/* Test if device available */
	if (i2c_read(fd, i2ct->i2caddr, 0, i2ct->addrcycle, &tmp, 1) < 0) {
		/* Mark EEPROM not available */
		i2ct->fd = 0;
		xerror("%s(): EEPROM not respond\n", __FUNCTION__);
		close(fd);
		return -1;
	}

	return 0;
}

/*
 * eeprom_exit()
 * @i2ct: input struct all i2c necessary
 * @return: 0 if pass, < 0 if fail.
 */
int eeprom_exit(struct i2c_test *i2ct)
{
	/* Close i2cbus */
	close(i2ct->fd);
	/* Clear string */
	//memset(i2ct, 0, sizeof(struct i2c_test));
	i2ct->fd = -1;

	return 0;
}

/*
 * eeprom_write()
 *
 * @eept: input struct all i2c/eeprom necessary
 * @buffer: Buffer to write
 * @offset: offset of device
 * @length: length to write
 * @return: 0 if pass, < 0 if fail.
 */
int eeprom_write(struct i2c_test *eept, unsigned char *buffer, unsigned int offset, unsigned int length)
{
	unsigned int size = eept->pagesize, i = 0;

	while (i < length) {

		/* Sanity check */
		if((buffer + i) == NULL) {
			xerror("%s(): buffer[%d] NULL !\n", __FUNCTION__, i);
			return -1;
		}
		/* Check max = pagesize */
		size = min(eept->pagesize, length - i);

		if (i2c_write(eept->fd, eept->i2caddr, offset + i, eept->addrcycle, buffer + i, size) < 0) {
			xerror("%s(): Failed !\n", __FUNCTION__);
			return -1;
		}
		/* tWR min 5ms */
		usleep(5*1000);
		/* Increase index */
		i += size;
	}

	return 0;
}

/*
 * eeprom_read()
 *
 * @eept: input struct all i2c/eeprom necessary
 * @buffer: Buffer to store data
 * @offset: offset of device
 * @length: length to read
 * @return: 0 if pass, < 0 if fail.
 */
int eeprom_read(struct i2c_test *eept, unsigned char *buffer, unsigned int offset, unsigned int length)
{
	unsigned int size = eept->pagesize, i = 0;

	while (i < length) {

		/* Sanity check */
		if((buffer + i) == NULL) {
			xerror("%s(): buffer[%d] NULL !\n", __FUNCTION__, i);
			return -1;
		}
		/* Check max = pagesize */
		size = min(eept->pagesize, length - i);

		if (i2c_read(eept->fd, eept->i2caddr, offset + i, eept->addrcycle, buffer + i, size) < 0) {
			xerror("%s(): Failed !\n", __FUNCTION__);
			return -1;
		}

		/* Increase index */
		i += size;
	}

	return 0;
}

#define CONFIG_EEPROM_TEST_BACKUP

/*
 * eeprom_test()
 *
 * @eept: input struct all i2c/eeprom necessary
 * @offset: offset of device
 * @length: length to test
 * @return: 0 if pass, < 0 if fail.
 */
int eeprom_test(struct i2c_test *eept, unsigned int offset, unsigned int length)
{
	unsigned char *memwr = NULL, *memrd = NULL;
#ifdef CONFIG_EEPROM_TEST_BACKUP
	unsigned char *membk = NULL;
#endif
	int ret = 0;

	/* Malloc memory for test */
#ifdef CONFIG_EEPROM_TEST_BACKUP
	membk = (unsigned char*)calloc(length, sizeof(unsigned char));
	if (!membk) {
		vediag_err(VEDIAG_I2C, eept->i2cdev, "calloc 'membk' (size 0x%x) fail\n", length);
		ret = -1;
		goto exit;
	}
#endif
	memwr = (unsigned char*)calloc(length, sizeof(unsigned char));
	if (!memwr) {
		vediag_err(VEDIAG_I2C, eept->i2cdev, "calloc 'memwr' (size 0x%x) fail\n", length);
		ret = -1;
		goto exit;
	}

	memrd = (unsigned char*)calloc(length, sizeof(unsigned char));
	if (!memrd) {
		vediag_err(VEDIAG_I2C, eept->i2cdev, "calloc 'memrd' (size 0x%x) fail\n", length);
		ret = -1;
		goto exit;
	}

	/* Field pattern to buffer */
	urandom(memwr, length);

#ifdef CONFIG_EEPROM_TEST_BACKUP
	/* Backup data */
	if(eept->backup) {
		ret = eeprom_read(eept, membk, offset, length);
		if (ret) {
			ret = -1;
			vediag_err(VEDIAG_I2C, eept->i2cdev, "Read backup (size 0x%x) fail\n", length);
			goto exit;
		}
	}
#endif

	/* Write test */
	//sto_debug(VEDIAG_STORAGE, "%s Open for Write\n", diskpath);
	ret = eeprom_write(eept, memwr, offset, length);
	if (ret) {
		ret = -1;
		vediag_err(VEDIAG_I2C, eept->i2cdev, "Write (size 0x%x) fail\n", length);
		goto exit2bk;
	}

	/* Read test */
	ret = eeprom_read(eept, memrd, offset, length);
	if (ret) {
		ret = -1;
		vediag_err(VEDIAG_I2C, eept->i2cdev, "Read (size 0x%x) fail\n", length);
		goto exit2bk;
	}

	/* Compare RD/WR pattern */
	ret = memcmp(memrd, memwr, length);
	if (ret) {
		ret = -1;
		vediag_err(VEDIAG_I2C, eept->i2cdev, "Compare (size 0x%x) fail\n", length);
		goto exit2bk;
	}

	/* Write backup */
exit2bk:
#ifdef CONFIG_EEPROM_TEST_BACKUP
	if(eept->backup) {
		ret = eeprom_write(eept, membk, offset, length);
		if (ret) {
			ret = -1;
			vediag_err(VEDIAG_I2C, eept->i2cdev, "Write backup (size 0x%x) fail\n", length);
			goto exit;
		}
	}
#endif
exit:
#ifdef CONFIG_EEPROM_TEST_BACKUP
	if (membk)
		free(membk);
#endif
	if (memwr)
		free(memwr);
	if (memrd)
		free(memrd);

	return ret;
}

int do_eeprom(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int fd, ret = 0;
	unsigned char *buf;
	unsigned int offset, length, size, tmp;

	if (argc == 1) {
		goto usage;
	} else if ((strncmp(argv[1], "init", 4) == 0) && (argc >= 6)) {
		/* Clear string */
		memset(&etest, 0, sizeof(struct i2c_test));
		/* Copy i2cbus */
		strncpy(etest.i2cdev, argv[2], min(strlen(argv[2]), sizeof(etest.i2cdev)));
		xinfo("I2C dev : %s\n", etest.i2cdev);

		etest.i2caddr = (unsigned char)strtoul(argv[3], NULL, 16);
		etest.pagesize = (unsigned int)strtoul(argv[4], NULL, 0);
		etest.addrcycle = (unsigned int)strtoul(argv[5], NULL, 0);
		/* Default don't use GPIO */
		etest.gpio_number = -1;
		if(argc >= 8) {
			etest.gpio_number = (int)strtoul(argv[6], NULL, 0);
			etest.gpio_value = (int)strtoul(argv[7], NULL, 0);
		}
		if((tmp = eeprom_init(&etest)) == 0)
			xinfo("done.\n");
		return tmp;
	} else if (strncmp(argv[1], "exit", 4) == 0) {
		return eeprom_exit(&etest);
	} else if ((strncmp(argv[1], "dump", 4) == 0) && (argc == 4)) {
		/* Check if EEPROM initialized */
		if (etest.fd <= 0) {
			xerror("EEPROM not initialized, type command 'eep init ...'\n");
			goto usage;
		}
		/* Get offset from input param */
		offset = (unsigned int)strtoul(argv[2], NULL, 16);
		/* Get length from input param */
		length = (unsigned int)strtoul(argv[3], NULL, 0);
		buf = (unsigned char*) calloc(length, sizeof(unsigned char));

		if (eeprom_read(&etest, buf, offset, length) < 0) {
			xerror("EEPROM read failed!\n");
			free(buf);
			return -1;
		}
		print_buffer((unsigned int) offset, (void*) buf, 1,
				length, (unsigned int) (DISP_LINE_LEN));
		/* Free memory */
		free(buf);
		xinfo("done.\n");
	} else if ((strncmp(argv[1], "read", 4) == 0) && (argc == 5)) {
		/* Check if EEPROM initialized */
		if (etest.fd <= 0) {
			xerror("EEPROM not initialized, type command 'eep init ...'\n");
			goto usage;
		}
		/* Get offset from input param */
		offset = (unsigned int)strtoul(argv[2], NULL, 16);
		/* Get length from input param */
		length = (unsigned int)strtoul(argv[3], NULL, 0);

		buf = (unsigned char*) calloc(length, sizeof(unsigned char));
		if (eeprom_read(&etest, buf, offset, length) < 0) {
			xerror("EEPROM read failed!\n");
			free(buf);
			return -1;
		}

		/* Open or Create new file */
		fd = open(argv[4], O_CREAT | O_RDWR/*, S_IRUSR | S_IWUSR*/);
		if (fd < 0) {
			xerror("Open|Create file '%s' failed\n", argv[4]);
			free(buf);
			return -1;
		}
		/* Store data to file */
		if(write(fd, buf, length) != length) {
			xerror("Write file '%s' failed\n", argv[4]);
			free(buf);
			return -1;
		}
		/* Close file */
		close(fd);
		/* Free memory */
		free(buf);
		xinfo("done.\n");
	} else if ((strncmp(argv[1], "write", 4) == 0) && (argc == 5)) {
		/* Check if EEPROM initialized */
		if (etest.fd <= 0) {
			xerror("EEPROM not initialized, type command 'eep init ...'\n");
			goto usage;
		}
		/* Get offset from input param */
		offset = (unsigned int)strtoul(argv[2], NULL, 16);
		/* Get length from input param */
		length = (unsigned int)strtoul(argv[3], NULL, 0);

		/* Open or Create new file */
		fd = open(argv[4], O_RDONLY);
		if (fd < 0) {
			xerror("Open file '%s' failed\n", argv[4]);
			return -1;
		}

		/* Allocate buffer */
		buf = (unsigned char*) calloc(length, sizeof(unsigned char));
		if (buf == NULL) {
			xerror("Buffer request failed\n");
			return -1;
		}

		/* Read data from file */
		if((tmp = read(fd, buf, length)) != length) {
			if(tmp < 0) {
				xerror("Read file '%s' failed\n", argv[4]);
				close(fd);
				free(buf);
				return -1;
			} else if(tmp == 0) {
				xinfo("File length = 0\n");
				close(fd);
				free(buf);
				return 0;
			} else if(tmp < length) {
				size = tmp;
				/* Try to read rest of data */
				if((tmp = read(fd, buf + size, length - size)) != (length - size)) {
					if(tmp < 0) {
						xerror("Read rest of file '%s' failed\n", argv[4]);
						close(fd);
						free(buf);
						return -1;
					} else if(tmp == 0) {
						xinfo("File length (0x%x) smalller than expected (0x%x)\n", size, length);
						length = size;
					} else {
						xerror("Read rest of file '%s' error (%d)\n", argv[4], tmp);
						close(fd);
						free(buf);
						return -1;
					}
				}
			}
		}
		/* Close file */
		close(fd);
		if (eeprom_write(&etest, buf, offset, length) < 0) {
			xerror("EEPROM write failed!\n");
			free(buf);
			return -1;
		}
		/* Free memory */
		free(buf);
		xinfo("done.\n");
	} else if ((strncmp(argv[1], "test", 4) == 0) && (argc == 4)) {
		/* Check if EEPROM initialized */
		if (etest.fd <= 0) {
			xerror("EEPROM not initialized, type command 'eep init ...'\n");
			goto usage;
		}
		/* Get offset from input param */
		offset = (unsigned int)strtoul(argv[2], NULL, 16);
		/* Get length from input param */
		length = (unsigned int)strtoul(argv[3], NULL, 0);
		tmp = 0;
		while (tmp < length) {
			size = min(etest.pagesize, length - tmp);
			ret = eeprom_test(&etest, offset, length);
			if(ret != 0) {
				xinfo("fail.\n");
				break;
			}
			offset += size;
			tmp += size;
		}
		xinfo("done.\n");
		return ret;
	} else {
		goto usage;
	}
	return 0;

usage:
	xinfo("Usage: \n");
	xinfo("%s\n", cmdtp->help);

	return -1;
}

VEDIAG_CMD(eep, 6, 0, do_eeprom,
	"EEPROM Command Utilities",
	" eep init <i2cdev> <i2caddr> <pagesize> <addrcycle> [<gpionumber> <gpiovalue>] - Set/detect EEPROM parameters\r\n"
	" eep exit                                           - Close/free EEPROM device/memory\r\n"
	" eep dump <offset> <count>                          - Print EEPROM content\r\n"
	" eep read <offset> <count> <filename>               - Read/create output file\r\n"
	" eep write <offset> <count> <filename>              - Write from input file\r\n"
	" eep test <offset> <count>                          - Test EEPROM Read/Write/Verify\r\n"
	"\n Argument:\n"
	"    - <i2cdev>    : I2C device Ex: /dev/i2c-0\r\n"
	"    - <i2caddr>   : Hex 7-bit I2C address\r\n"
	"    - <pagesize>  : Hex/Decimal page size\r\n"
	"    - <addrcycle> : Hex/Decimal address cycle\r\n"
	"    - <gpionumber>: GPIO number\r\n"
	"    - <gpiovalue> : GPIO active 0|1\r\n"
	"    - <offset>    : Hex Eeprom offset\r\n"
	"    - <count>     : Hex/Decimal Length in byte\r\n"
	"    - <filename>  : In/output file name\r\n");
