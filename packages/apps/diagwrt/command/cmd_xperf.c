
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <linux/bug.h>
#include <vediag_common.h>
#include <cmd.h>
#include <optparse/optparse.h>
#include <log.h>
#include "iniparser/iniparser.h"
#include <time_util.h>

#define PERF_STORAGE_TEST_TIME	600		/*seconds*/
#define PERF_NET_TEST_TIME		1200	/*seconds*/

struct xperf_ip {
	char* cmd_name;    /* Command to do this test */
	char* ini_section; /* Section name in test config file */
};

struct xperf_arg {
	char testname[64];
	char inifile[128];
};

static struct xperf_ip supported_test[] ={
		{"stor", "storage"},
		{"net", "network"}
};

static struct xperf_arg arg;
static pthread_mutex_t cfg_file_lock = PTHREAD_MUTEX_INITIALIZER;
static int xperf_total_device(char* cfg_file, char* ip_section)
{
	char *val;
	int total_device = 0;

	val = vediag_parse_config(cfg_file, ip_section, VEDIAG_DEVICE_NUM_KEY);
	if (val) {
		total_device = strtoul(val, NULL, 0);
		free(val);
	}

	return total_device;
}

/**
 * Check if device is enable perfomance test
 *
 * @param cfg_file
 * @param ip_section
 * @param dev
 * @param physical	[Output]: physical of this device
 * @param time		[Output]: Test time
 * @return true or false
 */
static int xperf_perf_enable(char* cfg_file, char* ip_section, int dev,
		char* physical, int* test_time)
{
	char key[64] = {0};
	const char *s;
	int ret = false;

	pthread_mutex_lock(&cfg_file_lock);
	dictionary * dic = iniparser_load(cfg_file);
	if (dic == NULL) {
		goto exit;
	}

	/*get 'argument' parameter */
	snprintf(key, 63, "%s%d:%s", ip_section, dev + 1, VEDIAG_ARGUMENT_KEY);
	s = iniparser_getstring(dic, key, NULL);
	if (!s) {
		xerror("\033[31m '%s' key not found \033[0m\n", key);
		ret = false;
		goto exit;
	}

	/*get 'performance' parameter in argument section */
	snprintf(key, 63, "%s:performance", s);
	ret = iniparser_getboolean(dic, key, false);
	if (ret) {
		/* get test time */
		if (strncmp(ip_section, "network", 6) == 0){
			snprintf(key, 63, "%s:test_time", s);
		} else {
			snprintf(key, 63, "%s:time", s);
		}

		*test_time = iniparser_getint(dic, key, -1);

		/* get physical */
		snprintf(key, 63, "%s%d:%s", ip_section, dev + 1, VEDIAG_PHYSICAL_KEY);
		s = iniparser_getstring(dic, key, NULL);
		snprintf(physical, 63, "%s", s);
		if (!s) {
			xerror("\033[31m '%s' key not found \033[0m\n", key);
			ret = false;
		}
	}
exit:
	pthread_mutex_unlock(&cfg_file_lock);
	return ret;
}

static int xperf_wait_finish(char* perf_cmd, int timeout)
{
	double start_time = get_time_sec();

	while (1) {
		if (is_diag_running() == false) {
			return 0;
		}
		if (get_time_sec() - start_time > timeout) {
			xerror("Error: Test '%s' not stop after %d seconds\n",
					perf_cmd, timeout);
			return -1;
		}
		sleep(1);
	}
}

static int xperf_execute(char* cmd, char* cfg_file)
{
	char* section = NULL;
	int i, called = 0;
	char physical[64];
	int test_time, test_elapsed = 0;
	char perf_cmd[64] = {0};
	double start_time;
	int total_devices;
#define STOR_GROUP	4 /* max #devices can run at the same time */

	for (i = 0; i < ARRAY_SIZE(supported_test); i++) {
		if (strncmp(cmd, supported_test[i].cmd_name,
				strlen(supported_test[i].cmd_name)) == 0) {
			section = supported_test[i].ini_section;
			break;
		}
	}

	if (!section) {
		xerror("Error: Section name for '%s' not found\n", cmd);
		return -1;
	}

	/*
	 * - With storage, will run max 4 devices simultaneously to save test time.
	 * - With net, will run sequentially
	 */
	total_devices = xperf_total_device(cfg_file, section);

	if ((strncmp(cmd, "stor", 4) == 0)) {
		for (i = 0; i < total_devices; i++) {
			memset(physical, 0, sizeof(physical));
			if (xperf_perf_enable(cfg_file, section, i, physical,
					&test_time) == false) {
				continue;
			}
			snprintf(perf_cmd, 63, "diag %s -pf -phy %s", cmd, physical);
			xinfo("\033[36m Run performance cmd: '%s'\033[0m\n", perf_cmd);
			run_command(perf_cmd);
			called++;
			if (called < STOR_GROUP) {
				if (i < total_devices - 1) {
					continue;
				}
			}

			// get max test time
			if (test_elapsed < test_time) {
				test_elapsed = test_time;
			}
			called = 0;

			xperf_wait_finish(perf_cmd, test_elapsed + 60);
		}
	} else {
		for (i = 0; i < total_devices; i++) {
			memset(physical, 0, 64);
			test_time = 0;
			if (xperf_perf_enable(cfg_file, section, i, physical,
					&test_time) == false) {
				continue;
			}

			snprintf(perf_cmd, 63, "diag %s -pf -phy %s", cmd, physical);
			start_time = get_time_sec();
			xinfo("\033[36m Run performance cmd: '%s'\033[0m\n", perf_cmd);
			run_command(perf_cmd);
			xperf_wait_finish(perf_cmd, test_time + 60);
		}
	}


	return 0;
}

/**
 * Get elapsed time of all device of test type
 * @param cmd
 * @param cfg_file
 * @param elapsed_time
 * @return
 */
static int xperf_estimate_time(char* cmd, char* cfg_file, int* elapsed_time)
{
	char* section = NULL;
	int i, called = 0;
	char physical[64];
	int test_time, test_elapsed = 0, total_time = 0;
	int total_devices;
#define STOR_GROUP	4 /* max #devices can run at the same time */

	for (i = 0; i < ARRAY_SIZE(supported_test); i++) {
		if (strncmp(cmd, supported_test[i].cmd_name,
				strlen(supported_test[i].cmd_name)) == 0) {
			section = supported_test[i].ini_section;
			break;
		}
	}

	if (!section) {
		xerror("Error: Section name for '%s' not found\n", cmd);
		return -1;
	}
	
	total_devices = xperf_total_device(cfg_file, section);

	if ((strncmp(cmd, "stor", 4) == 0)) {
		for (i = 0; i < total_devices; i++) {
			memset(physical, 0, sizeof(physical));
			test_time = 0;
			if (xperf_perf_enable(cfg_file, section, i, physical,
					&test_time) == false) {
				continue;
			}
			called++;
			if (called < STOR_GROUP) {
				if (i < total_devices - 1) {
					continue;
				}
			}

			// get max test time
			if (test_elapsed < test_time) {
				test_elapsed = test_time;
			}

			called = 0;
			total_time += test_elapsed;
			test_elapsed = 0;
		}
	} else {
		for (i = 0; i < total_devices; i++) {
			memset(physical, 0, 64);
			test_time = 0;
			if (xperf_perf_enable(cfg_file, section, i, physical,
					&test_time) == false) {
				continue;
			}
			total_time += test_time;
		}
	}

	*elapsed_time = total_time;
	return 0;
}

static void* xperf_main(void *data)
{
	int i;
	char* default_folder = "/etc/diagwrt/";
	char cfgfile[128] = {0};
	char* filename;

	char* testname = arg.testname;
	char* inifile = arg.inifile;
	int total_time = 0;
	int test_elapsed_time = 0;

	if (strlen(testname)) {
		for (i = 0; i < ARRAY_SIZE(supported_test); i++) {
			if (strncmp(testname, supported_test[i].cmd_name,
							strlen(supported_test[i].cmd_name)) == 0) {
				break;
			}
		}
		if (i == ARRAY_SIZE(supported_test)) {
			xerror("Error: Invalid performance test name '%s'\n", testname);
			return NULL;
		}
	}

	if (strlen(inifile)) {
		filename = inifile;
	} else {
		filename = vediag_parse_system("Test default");
	}

	/* Check if file is exist in current dir or default dir */
	snprintf(cfgfile, 127, "%s", filename);
	if (access(cfgfile, 0) != 0) {
		snprintf(cfgfile, 127, "%s%s", default_folder, filename);
		if (access(cfgfile, 0) != 0) {
			xerror("Error: test file '%s' not found\n", filename);
			return NULL;
		} else {
			xdebug(" Found test config file: '%s' at '/etc/diagwrt/'", filename);
		}
	} else {
		xdebug(" Found test config file: '%s' at current folder\n", filename);
	}

	/* Execute test */
	total_time = 0;
	for (i = 0; i < ARRAY_SIZE(supported_test); i++) {
		if (strlen(testname)) {
			if (xperf_estimate_time(testname, cfgfile, &test_elapsed_time) == 0){
				total_time += test_elapsed_time;
			}
			break;
		} else {
			if (xperf_estimate_time(supported_test[i].cmd_name, cfgfile,
					&test_elapsed_time) == 0){
				total_time += test_elapsed_time;
			}
		}
	}
	xinfo("\n PERFORMANCE TOTAL TEST TIME: %d\n", total_time);
	xinfo(" PERFORMANCE TOTAL TEST TIME: %d\n\n", total_time);

	/* Execute test */
	for (i = 0; i < ARRAY_SIZE(supported_test); i++) {
		if (strlen(testname)) {
			xperf_execute(testname, cfgfile);
			goto exit;
		} else {
			xperf_execute(supported_test[i].cmd_name, cfgfile);
		}
	}
exit:
	xinfo("DIAG PERFORMANCE TEST DONE\n");

	return NULL;
}

static int do_xperf(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	char* inifile = NULL;
	int option;
	struct optparse options;
	char* testname = NULL;

	optparse_init(&options, argv);
    while ((option = optparse(&options, "n:f:")) != -1) {
        switch (option) {
        case 'n':
        	testname = options.optarg;
            break;
        case 'f':
        	inifile = options.optarg;
            break;
        case '?':
            xerror("%s: %s\n", argv[0], options.errmsg);
            return -1;
        }
    }

    pthread_t thread;
    memset(&arg, 0x0, sizeof(arg));
    if (testname)
    	snprintf(arg.testname, 63, "%s", testname);
    if (inifile)
    	snprintf(arg.inifile, 127, "%s", inifile);

    return pthread_create(&thread, NULL, xperf_main, NULL);
}

VEDIAG_CMD(xperf, 1, 1, do_xperf,
		"Run performance test",
		" xperf [-n <stor/net>] -f <ini_test_file>\r\n"
		"")
