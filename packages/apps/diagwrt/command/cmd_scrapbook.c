/*
 * cmd_scrapbook.c
 *
 * It is used to try minor thing before coding
 */
#include <common.h>
#include <stdint.h>
#include <string.h>
#include <linux/list.h>
#include <cmd.h>
#include <log.h>
#include <listbuff.h>

/*#define TEST_DATA_TYPE*/
#define TEST_COMMON_DOT_C
static int do_scrapbook(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
#ifdef TEST_DATA_TYPE
    xinfo("gcc version: \t%d\n", GCC_VERSION);
	xinfo("sizeof(long long): \t%d\n", (int) sizeof(long long));
	xinfo("sizeof(long): \t%d\n", (int) sizeof(long));
	xinfo("sizeof(int): \t%d\n", (int) sizeof(int));
	xinfo("sizeof(short): \t%d\n", (int) sizeof(short));
	xinfo("sizeof(char): \t%d\n", (int) sizeof(char));
	xinfo("sizeof(char*): \t%d\n", (int) sizeof(char *));
	xinfo("sizeof(void*): \t%d\n", (int) sizeof(void *));
	xinfo("BITS_PER_LONG: \t%d\n", (int) BITS_PER_LONG);
#endif

#ifdef TEST_COMMON_DOT_C
	char *default_cmd = "iperf -B 12.50.1.1 -c 12.60.0.1 -i 5 -P 8 -t 30";
	char *cmd = default_cmd;
	if (argc >= 2) {
		cmd = argv[1];
	}
	struct listbuff *lb = get_outputstream_of_shell_command(cmd);
	if (lb != NULL) {
		xinfo("outputstream (%d):\n", lb->len);
		xinfo("%s\n", (char *)lb->buff);
		free_listbuff_list(&lb);
	}
#endif

	return 0;
}

VEDIAG_CMD(scrapbook, 32, 0, do_scrapbook,
		"The cmd's \"cmd_scrapbook.c\" is used to try some minor code",
		"Usage:\n"
		"scrapbook [...] \n")
