
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <common.h>
#include <stdio.h>
#include <unistd.h>
#include <cmd.h>
#include <vediag/pcie_x16_regs_addrmap.h>
#include <memory.h>
#include <time_util.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include "../drivers/pciutils/header.h"
#include "vediag/pciutils.h"
#include <linux/kernel.h>
#include <log.h>
#include <vediag/gpio.h>
#include <ras.h>

void progress_bar(unsigned int percent);
int pci_parse_dbdf(char* name, struct pci_dbdf* p);

static int pci_get_gen_width_cap(struct pci_dbdf* bdf, u32* gen, u32* width);
static int pci_get_gen_width(struct pci_dbdf* bdf, u32* gen, u32* width);
static int pci_find_capability(struct pci_dbdf *dev, int cap);
static int pci_cfg_idtest(struct pci_dbdf *bdf, u16 vendorID, ulong time,
		uint32_t ce_mask, uint32_t uc_mask, int progress);

struct pcie_link_info {
	uint32_t parent_cap_gen;
	uint32_t parent_cap_width;
	uint32_t parent_sts_gen;
	uint32_t parent_sts_width;
	uint32_t dev_cap_gen;
	uint32_t dev_cap_width;
};

static int pcie_inbound_parsing(int host)
{
	int ret = 0;
	void *csr_base;
	u64 val64;
	u32 val32;
	u64 lo, hi;
	int i;

	if (host == 0)
		csr_base = (void*) PCIE0_BASE_ADDR;
	else if (host == 1)
		csr_base = (void*) PCIE1_BASE_ADDR;
	else if (host == 2)
		csr_base = (void*) PCIE2_BASE_ADDR;
	else if (host == 3)
		csr_base = (void*) PCIE3_BASE_ADDR;
	else if (host == 4)
		csr_base = (void*) PCIE4_BASE_ADDR;
	else if (host == 5)
		csr_base = (void*) PCIE5_BASE_ADDR;
	else if (host == 6)
		csr_base = (void*) PCIE6_BASE_ADDR;
	else if (host == 7)
		csr_base = (void*) PCIE7_BASE_ADDR;
	else {
	    xinfo("Error: Invalid host num(%d)\n", host);
		return -1;
	}

	for (i = 0; i < XGENE_PCIE_MAX_IB_REGIONS; i++) {
		/*
		 * (IBARH,IBARL,20?h0)
		 */
		val32 = readl(csr_base + SM_PCIE_CSR_REGS_IBARBASE1_L__ADDR + i * 0x8);
		lo = FIELD_IBARBASE1_L_ADDR_RD(val32);
		val32 = readl(csr_base + SM_PCIE_CSR_REGS_IBARBASE1_H__ADDR + i * 0x8);
		hi = FIELD_IBARBASE1_H_ADDR_RD(val32);

		val64 = (hi << 32 | lo) << 20;
		xinfo("\t hb->ib_mem_addr[%d].IBARBASE = %llx\n", i, val64);

		/*
		 * (IBARLIMIT_H,IBARLIMIT_L,20?h0)
		 */
		val32 = readl(csr_base + SM_PCIE_CSR_REGS_IBARLIMIT1_L__ADDR + i * 0x8);
		lo = FIELD_IBARLIMIT1_L_ADDR_RD(val32);
		val32 = readl(csr_base + SM_PCIE_CSR_REGS_IBARLIMIT1_H__ADDR + i * 0x8);
		hi = FIELD_IBARLIMIT1_H_ADDR_RD(val32);
		val64 = ((hi << 32) | lo) << 20;
		xinfo("\t hb->ib_mem_addr[%d].IBARLIMIT = %llx, valid = %d\n", i, val64,
				((val32 & (1 << 16)) == 0) ? 0 : 1);

		/*
		 * (PIMx_CFG)
		 */
		val32 = readl(csr_base + SM_PCIE_CSR_REGS_PIM1_CFG__ADDR + i * 0x4);
		val32 = FIELD_PIM1_CFG_BASE_RD(val32);
		xinfo("\t hb->ib_mem_addr[%d].Base@PIMx_CFG = 0x%08x\n\n", i, val32);
	}

	return ret;
}

static int pcie_inbound_info(cmd_tbl_t* cmdtp, int argc, char* argv[])
{
	int host;
	if (argc != 3){
	    xinfo("Usage:\n");
	    xinfo("%s\n", cmdtp->help);
		return -1;
	}
	host = strtoul(argv[2], NULL, 10);
	if (host >= CONFIG_PCIE_MAX_PORTS){
	    xinfo("Error: Invalid Host number\n");
		return -1;
	}

	return pcie_inbound_parsing(host);
}

static int pcie_outbound_parsing(int host)
{
	void *csr_base;
	u32 val32;
	uint64_t base;
	uint64_t limit;
	uint64_t pom;
	uint32_t valid;
	uint32_t wr_PCIEAttr;
	uint32_t rd_PCIEAttr;
	uint32_t io_type;
	uint32_t poml, pomh;
	int i;

	if (host == 0)
		csr_base = (void*) PCIE0_BASE_ADDR;
	else if (host == 1)
		csr_base = (void*) PCIE1_BASE_ADDR;
	else if (host == 2)
		csr_base = (void*) PCIE2_BASE_ADDR;
	else if (host == 3)
		csr_base = (void*) PCIE3_BASE_ADDR;
	else if (host == 4)
		csr_base = (void*) PCIE4_BASE_ADDR;
	else if (host == 5)
		csr_base = (void*) PCIE5_BASE_ADDR;
	else if (host == 6)
		csr_base = (void*) PCIE6_BASE_ADDR;
	else if (host == 7)
		csr_base = (void*) PCIE7_BASE_ADDR;
	else {
	    xinfo("Error: Invalid host num(%d)\n", host);
		return -1;
	}

	for (i = 0; i < XGENE_PCIE_MAX_OB_REGIONS; i++){
		val32 = readl(csr_base + SM_PCIE_CSR_REGS_OMR1_CFG__ADDR + i * 0x8);

		base = (uint64_t)(FIELD_OMR1_CFG_BASE_RD(val32)) << 20;
		valid = FIELD_OMR1_CFG_VALID_RD(val32);
		wr_PCIEAttr = FIELD_OMR1_CFG_WR_PCIEATTR_RD(val32);
		rd_PCIEAttr = FIELD_OMR1_CFG_RD_PCIEATTR_RD(val32);
		io_type = FIELD_OMR1_CFG_IO_TYPE_RD(val32);

		val32 = readl(csr_base + SM_PCIE_CSR_REGS_OMR1_LIMIT__ADDR + i * 0x8);
		limit = FIELD_OMR1_LIMIT_ADDR_RD(val32);
		limit = (limit << 20) + 0xfffffUL;

		val32 =  readl(csr_base + SM_PCIE_CSR_REGS_POMBASE1_L__ADDR + i * 0x8);
		poml = FIELD_POMBASE1_L_ADDR_RD(val32);
		val32 =  readl(csr_base + SM_PCIE_CSR_REGS_POMBASE1_H__ADDR + i * 0x8);
		pomh = FIELD_POMBASE1_H_ADDR_RD(val32);
		pom = ((uint64_t)pomh << 32 | poml) << 20;

		xinfo("[region %d] config OMR/POM with:\n", i);
		xinfo("  base  = 0x%lx,\n", base);
		xinfo("  limit = 0x%lx,\n", limit);
		xinfo("  POM   = 0x%lx \n", pom);
		xinfo("  wr_PCIEAttr = %u\n"
				"  rd_PCIEAttr = %u\n"
				"  io_type     = %u\n"
				"  valid       = %u\n\n",
				 wr_PCIEAttr, rd_PCIEAttr, io_type, valid);

	}
	return 0;

}

static int pcie_outbound_info(cmd_tbl_t* cmdtp, int argc, char* argv[])
{
	int host;

	if (argc != 3){
	    xinfo("Usage:\n");
	    xinfo("%s\n", cmdtp->help);
		return -1;
	}
	host = strtoul(argv[2], NULL, 10);
	if (host >= CONFIG_PCIE_MAX_PORTS){
	    xinfo("Error: Invalid Host number\n");
		return -1;
	}

	return pcie_outbound_parsing(host);
}

/* return the PCIe link speed from the given link status */
static uint32_t extract_speed(uint16_t linkstat)
{
	uint32_t speed;

	switch (linkstat & PCI_EXP_LNKSTA_CLS) {
	default: /* not defined, assume Gen1 */
	case PCI_EXP_LNKSTA_CLS_2_5GB:
		speed = PCIE_GEN1; /* Gen 1, 2.5GHz */
		break;
	case PCI_EXP_LNKSTA_CLS_5_0GB:
		speed = PCIE_GEN2; /* Gen 2, 5GHz */
		break;
	case PCI_EXP_LNKSTA_CLS_8_0GB:
		speed = PCIE_GEN3; /* Gen 3, 8GHz */
		break;
	}
	return speed;
}

/* return the PCIe link speed from the given link status */
static uint32_t extract_width(uint16_t linkstat)
{
	return (linkstat & PCI_EXP_LNKSTA_NLW) >> PCI_EXP_LNKSTA_NLW_SHIFT;
}

static int pci_check_link_status(struct pci_dbdf* dev, int test_gen,
		int test_width)
{
	struct pci_dbdf parent = {0};
	int ret;
	uint16_t vendor_id = 0xffff, device_id = 0xffff;
	uint16_t pcie_flags = 0xffff, port_type;
	uint32_t gen_to_check, width_to_check;
	char* gpi_pin = "";
	int capptr, is_pcie = true;
	struct pcie_link_info linkinfo = {0};

	/* Check if device exist */
	pci_read_config_word(dev, PCI_VENDOR_ID, &vendor_id);
	if (vendor_id == 0xffff) {
		/*
		 * If device is nearest host, will do check card present.
		 * Skylark host is always <domain>.0.0.0
		 */
		if (dev->bus == 1){
			if (dev->domain == 0){
				gpi_pin = PCIE0_GPIO_PRSNT;
			} else if (dev->domain == 2){
				gpi_pin = PCIE2_GPIO_PRSNT;
			} else if (dev->domain == 4){
				gpi_pin = PCIE4_GPIO_PRSNT;
			} else if (dev->domain == 6){
				gpi_pin = PCIE6_GPIO_PRSNT;
			} else if (dev->domain == 7){
				gpi_pin = PCIE7_GPIO_PRSNT;
			} else {
				xerror("Error: Invalid domain (%d)\n", dev->domain);
				return PCI_TEST_LNKSTAT_ERR;
			}

			ret = gpio_get_input_value(gpi_pin);
			if (ret == 0) {
				/*active Low, device is present*/
				xerror("Error: Read VendorID return 0xffff\n");
				return PCI_TEST_CONFIG_ACCESS_ERR;
			} else if (ret == 1) {
				/*active Low, device is not present*/
				xerror("Error: Device is not present\n");
				return PCI_TEST_DEVICE_NOT_PRESENT;
			} else {
				xerror("Error: Read GPI failed\n");
				return PCI_TEST_INTERNAL_ERR;
			}
		}
	}

	/*
	 * Skip compare link status between PCIe downstream port and upstream port
	 * of PCIe switch
	 */
	capptr = pci_find_capability(dev, PCI_CAP_ID_EXP);
	if (capptr) {
		if (pci_read_config_word(dev, capptr + PCI_EXP_FLAGS, &pcie_flags) == 0) {
			xerror("Error: Read PCIe CAP failed\n");
			return PCI_TEST_CONFIG_ACCESS_ERR;
		}
		port_type = (pcie_flags & PCI_EXP_FLAGS_TYPE) >> 4;
		if (port_type == PCI_EXP_TYPE_DOWNSTREAM) {
			return PCI_TEST_OK;
		}
	}

	ret = pci_get_parent(dev, &parent);
	if (ret == PCI_DEV_IS_ROOT){
	    xerror("Error: device is Root\n");
		return PCI_TEST_LNKSTAT_ERR;
	} else if (ret == PCI_PARENT_NOT_FOUND){
		xerror("Error: Cannot found parent of [%04x:%02x:%02x.%02x]\n",
				dev->domain, dev->bus, dev->dev, dev->func);
		return PCI_TEST_LNKSTAT_ERR;
	}

	/*
	 * Get Link Cap of parent
	 * TODO: if use PCI switch(not PCIe switch), will fail here!
	 */
	if (pci_get_gen_width_cap(&parent, &linkinfo.parent_cap_gen,
			&linkinfo.parent_cap_width) != COMMAND_OK) {
		xerror("Error: Get gen/width fail\n");
		return PCI_TEST_LNKSTAT_ERR;
	}

	/* Get Link Cap of Device */
	if (pci_get_gen_width_cap(dev, &linkinfo.dev_cap_gen,
			&linkinfo.dev_cap_width) != COMMAND_OK) {
		xdebug("Warning: Get gen/width of [%04x:%02x:%02x.%02x] failed\n",
				dev->domain, dev->bus, dev->dev, dev->func);
		is_pcie = false;
	}

	/* Get min Gen/Width of parent and Device if 'test_gen' is not forced */
	if (test_gen == PCI_CHECK_GEN_FROM_LNKSTAT_LNKCAP) {
		if (is_pcie)
			gen_to_check = min(linkinfo.parent_cap_gen, linkinfo.dev_cap_gen);
		else
			gen_to_check = linkinfo.parent_cap_gen;
	} else {
		gen_to_check = test_gen;
	}

	if (test_width == PCI_CHECK_WIDTH_FROM_LNKSTAT_LNKCAP) {
		if (is_pcie)
			width_to_check = min(linkinfo.parent_cap_width, linkinfo.dev_cap_width);
		else
			width_to_check = linkinfo.parent_cap_width;
	} else {
		width_to_check = test_width;
	}

	/* Get Link Status of parent */
	if (pci_get_gen_width(&parent, &linkinfo.parent_sts_gen,
			&linkinfo.parent_sts_width) != COMMAND_OK) {
		xerror("Error: Get gen width fail\n");
		return PCI_TEST_LNKSTAT_ERR;
	}

	if (pci_read_config_word(&parent, PCI_VENDOR_ID, &vendor_id) == 0){
		xerror("Error: Read PCI_VENDOR_ID failed\n");
		return PCI_TEST_LNKSTAT_ERR;
	}

	if (pci_read_config_word(&parent, PCI_DEVICE_ID, &device_id) == 0) {
		xerror("Error: Read PCI_DEVICE_ID failed\n");
		return PCI_TEST_LNKSTAT_ERR;
	}

	xinfo("   Parent [%04x:%02x:%02x.%02x]: %04x:%04x - LnkCap = x%d-Gen%d, LnkSts = x%d-Gen%d\n",
			parent.domain, parent.bus, parent.dev, parent.func, vendor_id,
			device_id, linkinfo.parent_cap_width, linkinfo.parent_cap_gen + 1,
			linkinfo.parent_sts_width, linkinfo.parent_sts_gen + 1);

	if (pci_read_config_word(dev, PCI_VENDOR_ID, &vendor_id) == 0){
		xerror("Error: Read PCI_VENDOR_ID failed\n");
		return PCI_TEST_LNKSTAT_ERR;
	}

	if (pci_read_config_word(dev, PCI_DEVICE_ID, &device_id) == 0) {
		xerror("Error: Read PCI_DEVICE_ID failed\n");
		return PCI_TEST_LNKSTAT_ERR;
	}

	if (is_pcie) {
		xinfo("   Device [%04x:%02x:%02x.%02x]: %04x:%04x - LnkCap = x%d-Gen%d\n",
				dev->domain, dev->bus, dev->dev, dev->func, vendor_id,
				device_id, linkinfo.dev_cap_width, linkinfo.dev_cap_gen + 1);
	} else {
		xinfo("   Device [%04x:%02x:%02x.%02x]: %04x:%04x - LnkCap = <NA>\n",
				dev->domain, dev->bus, dev->dev, dev->func, vendor_id, device_id);
	}

	/* check link */
	if (gen_to_check != linkinfo.parent_sts_gen
			|| width_to_check != linkinfo.parent_sts_width) {
		xerror("Error: Wrong Gen/Width, expect x%d-Gen%d, but got x%d-Gen%d\n",
				width_to_check, gen_to_check + 1,
				linkinfo.parent_sts_width, linkinfo.parent_sts_gen + 1);
		return PCI_TEST_LNKSTAT_ERR;
	}

	return PCI_TEST_OK;
}

static int pci_scan_test(struct pci_dbdf *dev, int gen, int width)
{
	return pci_check_link_status(dev, gen, width);

}

static int pci_get_gen_width_cap(struct pci_dbdf* bdf, u32* gen, u32* width)
{
	int capptr;
	uint16_t lnkcap;

	/* Get PCI Express Capability pointer*/
	capptr = pci_find_capability(bdf, PCI_CAP_ID_EXP);
	if (!capptr) {
		return COMMAND_ERROR;
	}

	/*check link status*/
	if (pci_read_config_word(bdf, capptr + PCI_EXP_LNKCAP, &lnkcap) == 0) {
		return COMMAND_ERROR;
	}

	*gen = extract_speed(lnkcap);
	*width = extract_width(lnkcap);

	return COMMAND_OK;
}

static int pci_get_gen_width(struct pci_dbdf* bdf, u32* gen, u32* width)
{
	int capptr;
	uint16_t lnksta;

	/* Get PCI Express Capability pointer*/
	capptr = pci_find_capability(bdf, PCI_CAP_ID_EXP);
	if (!capptr) {
		return COMMAND_ERROR;
	}

	/*check link status*/
	if (pci_read_config_word(bdf, capptr + PCI_EXP_LNKSTA, &lnksta) == 0) {
		return COMMAND_ERROR;
	}

	*gen = extract_speed(lnksta);
	*width = extract_width(lnksta);

	return COMMAND_OK;
}

static int pci_cfg_display(struct pci_dbdf* bdf, u64 addr, u64 size, u64 length)
{
#define DISP_LINE_LEN	16
	ulong i, nbytes, linebytes;
	int rc = 0;

	if (length == 0)
		length = 0x40 / size; /* Standard PCI configuration space */

	/* Print the lines.
	 * once, and all accesses are with the specified bus width.
	 */
	nbytes = length * size;
	do {
		uint	val4;
		ushort  val2;
		u_char	val1;

		xinfo("%08llx:", addr);
		linebytes = (nbytes > DISP_LINE_LEN) ? DISP_LINE_LEN : nbytes;
		for (i = 0; i < linebytes; i += size) {
			if (size == 4) {
				pci_read_config_dword(bdf, addr, &val4);
				xinfo(" %08x", val4);
			} else if (size == 2) {
				pci_read_config_word(bdf, addr, &val2);
				xinfo(" %04x", val2);
			} else {
				pci_read_config_byte(bdf, addr, &val1);
				xinfo(" %02x", val1);
			}
			addr += size;
		}
		xinfo("\n");
		nbytes -= linebytes;
		if (ctrlc()) {
			rc = 1;
			break;
		}
	} while (nbytes > 0);

	return (rc);
}

static int pci_cfg_write(struct pci_dbdf* bdf, u64 addr, u64 size, u64 value)
{
	if (size == 4) {
		pci_write_config_dword(bdf, addr, value);
	} else if (size == 2) {
		ushort val = value & 0xffff;
		pci_write_config_word(bdf, addr, val);
	} else {
		u_char val = value & 0xff;
		pci_write_config_byte(bdf, addr, val);
	}
	return 0;
}

static u32 pci_aer_uncorr_details(u32 val)
{

	/* UNCORRECTABLE ERROR
	Bit[4] ? DataLink_Protocol_Error_Status
	Bit[5] - Surprise_Down_Error_Status
	Bits[11:6] ? Reserved == 0
	Bit[12] - Poisoned_TLP_Status
	Bit[13] ? Flow_Control_Protocol_Error_Status
	Bit[14] - Completion_Timeout_Status
	Bit[15] - Completer_Abort_Status
	Bit[16] - Unexpected_Completion_Status
	Bit[17] - Receiver_Overflow_Status
	Bit[18] - Malformed_TLP_Status
	Bit[19] - ECRC_Error_Status
	Bit[20] ? Unsupported_Request_Error_Status
	Bit[21] ? Reserved = 0
	Bit[22]- Uncorrectable Internal Error Status
	Bits[31:23] ? Reserved == 0*/
	u32 uncor = 0;
	if (val & (1 << 4)) {
	    xinfo("\033[31m \t DataLink_Protocol_Error\n\033[0m");
		uncor = 1;
	}
	if (val & (1 << 5)) {
	    xinfo("\033[31m \t Surprise_Down_Error\n\033[0m");
		uncor = 1;
	}
	if (val & (1 << 12)) {
	    xinfo("\033[31m \t Poisoned_TLP\n\033[0m");
		uncor = 1;
	}
	if (val & (1 << 13)) {
	    xinfo("\033[31m \t Flow_Control_Protocol_Error\n\033[0m");
		uncor = 1;
	}
	if (val & (1 << 14)) {
	    xinfo("\033[31m \t Completion_Timeout\n\033[0m");
		uncor = 1;
	}
	if (val & (1 << 15)) {
	    xinfo("\033[31m \t Completer_Abort\n\033[0m");
		uncor = 1;
	}
	if (val & (1 << 16)) {
	    xinfo("\033[31m \t Unexpected_Completion\n\033[0m");
		uncor = 1;
	}
	if (val & (1 << 17)) {
	    xinfo("\033[31m \t Receiver_Overflow\n\033[0m");
		uncor = 1;
	}
	if (val & (1 << 18)) {
	    xinfo("\033[31m \t Malformed_TLP\n\033[0m");
		uncor = 1;
	}
	if (val & (1 << 19)) {
	    xinfo("\033[31m \t ECRC_Error\n\033[0m");
		uncor = 1;
	}
	if (val & (1 << 20)) {
	    xinfo("\033[31m \t Unsupported_Request_Error\n\033[0m");
		uncor = 1;
	}
	if (val & (1 << 22)) {
	    xinfo("\033[31m \t Uncorrectable Internal Error\n\033[0m");
		uncor = 1;
	}
	return uncor;
}

static void pci_aer_parse_log(struct pci_dbdf *bdf, ulong aer_addr)
{
	/*
	 * PCI-E Common TLP Header Fields
	 */
	#define	PCIE_TLP_FMT_3DW	0x00
	#define	PCIE_TLP_FMT_4DW	0x20
	#define	PCIE_TLP_FMT_3DW_DATA	0x40
	#define	PCIE_TLP_FMT_4DW_DATA	0x60

	#define	PCIE_TLP_TYPE_MEM	0x0
	#define	PCIE_TLP_TYPE_MEMLK	0x1
	#define	PCIE_TLP_TYPE_IO	0x2
	#define	PCIE_TLP_TYPE_CFG0	0x4
	#define	PCIE_TLP_TYPE_CFG1	0x5
	#define	PCIE_TLP_TYPE_MSG	0x10
	#define	PCIE_TLP_TYPE_CPL	0xA
	#define	PCIE_TLP_TYPE_CPLLK	0xB
	#define	PCIE_TLP_TYPE_MSI	0x18

	#define	PCIE_TLP_MRD3		(PCIE_TLP_FMT_3DW | PCIE_TLP_TYPE_MEM)
	#define	PCIE_TLP_MRD4		(PCIE_TLP_FMT_4DW | PCIE_TLP_TYPE_MEM)
	#define	PCIE_TLP_MRDLK3		(PCIE_TLP_FMT_3DW | PCIE_TLP_TYPE_MEMLK)
	#define	PCIE_TLP_MRDLK4		(PCIE_TLP_FMT_4DW | PCIE_TLP_TYPE_MEMLK)
	#define	PCIE_TLP_MRDWR3		(PCIE_TLP_FMT_3DW_DATA | PCIE_TLP_TYPE_MEM)
	#define	PCIE_TLP_MRDWR4		(PCIE_TLP_FMT_4DW_DATA | PCIE_TLP_TYPE_MEM)
	#define	PCIE_TLP_IORD		(PCIE_TLP_FMT_3DW | PCIE_TLP_TYPE_IO)
	#define	PCIE_TLP_IOWR		(PCIE_TLP_FMT_3DW_DATA | PCIE_TLP_TYPE_IO)
	#define	PCIE_TLP_CFGRD0		(PCIE_TLP_FMT_3DW | PCIE_TLP_TYPE_CFG0)
	#define	PCIE_TLP_CFGWR0		(PCIE_TLP_FMT_3DW_DATA | PCIE_TLP_TYPE_CFG0)
	#define	PCIE_TLP_CFGRD1		(PCIE_TLP_FMT_3DW | PCIE_TLP_TYPE_CFG1)
	#define	PCIE_TLP_CFGWR1		(PCIE_TLP_FMT_3DW_DATA | PCIE_TLP_TYPE_CFG1)
	#define	PCIE_TLP_MSG		(PCIE_TLP_FMT_4DW | PCIE_TLP_TYPE_MSG)
	#define	PCIE_TLP_MSGD		(PCIE_TLP_FMT_4DW_DATA | PCIE_TLP_TYPE_MSG)
	#define	PCIE_TLP_CPL		(PCIE_TLP_FMT_3DW | PCIE_TLP_TYPE_CPL)
	#define	PCIE_TLP_CPLD		(PCIE_TLP_FMT_3DW_DATA | PCIE_TLP_TYPE_CPL)
	#define	PCIE_TLP_CPLLK		(PCIE_TLP_FMT_3DW | PCIE_TLP_TYPE_CPLLK)
	#define	PCIE_TLP_CPLDLK		(PCIE_TLP_FMT_3DW_DATA | PCIE_TLP_TYPE_CPLLK)
	#define	PCIE_TLP_MSI32		(PCIE_TLP_FMT_3DW_DATA | PCIE_TLP_TYPE_MSI)
	#define	PCIE_TLP_MSI64		(PCIE_TLP_FMT_4DW_DATA | PCIE_TLP_TYPE_MSI)

	#define	PCIE_REQ_ID_BUS_SHIFT	8
	#define	PCIE_REQ_ID_BUS_MASK	0xFF00
	#define	PCIE_REQ_ID_DEV_SHIFT	3
	#define	PCIE_REQ_ID_DEV_MASK	0x00F8
	#define	PCIE_REQ_ID_FUNC_SHIFT	0
	#define	PCIE_REQ_ID_FUNC_MASK	0x0007
	#define	PCIE_REQ_ID_ARI_FUNC_MASK	0x00FF

	#define	PCIE_CPL_STS_SUCCESS	0
	#define	PCIE_CPL_STS_UR		1
	#define	PCIE_CPL_STS_CRS	2
	#define	PCIE_CPL_STS_CA		4

	typedef struct pcie_tlp_hdr {
		uint32_t	len	:10,
				rsvd3   :2,
				attr    :2,
				ep	:1,
				td	:1,
				rsvd2   :4,
				tc	:3,
				rsvd1   :1,
				type    :5,
				fmt	:2,
				rsvd0   :1;
	} pcie_tlp_hdr_t;

	typedef struct pcie_mem64 {
		uint32_t	fbe	:4,
				lbe	:4,
				tag	:8,
				rid	:16;
		uint32_t	addr1;
		uint32_t	rsvd0   :2,
				addr0   :30;
	} pcie_mem64_t;

	typedef struct pcie_cfg {
		uint32_t	fbe	:4,
				lbe	:4,
				tag	:8,
				rid	:16;
		uint32_t	rsvd1   :2,
				reg	:6,
				extreg  :4,
				rsvd0   :4,
				func    :3,
				dev	:5,
				bus	:8;
	} pcie_cfg_t;

	typedef struct pcie_cpl {
		uint32_t	laddr   :7,
				rsvd0   :1,
				tag	:8,
				rid	:16;
		uint32_t	bc	:12,
				bcm	:1,
				status  :3,
				cid	:16;
	} pcie_cpl_t;

#define MY_PCIE_BUS(d)	(((d) >> 8) & 0xff)
#define MY_PCIE_DEV(d)	(((d) >> 3) & 0x1f)
#define MY_PCIE_FUNC(d)	(((d) ) & 0x7)

	void* ptr;

	u32 val0, val1, val2, val3;
#define AER_HEADER_LOG_1ST_OFFSET	0x1c
	pci_read_config_dword(bdf, aer_addr + AER_HEADER_LOG_1ST_OFFSET + 0x0, &val0);
	pci_read_config_dword(bdf, aer_addr + AER_HEADER_LOG_1ST_OFFSET + 0x4, &val1);
	pci_read_config_dword(bdf, aer_addr + AER_HEADER_LOG_1ST_OFFSET + 0x8, &val2);
	pci_read_config_dword(bdf, aer_addr + AER_HEADER_LOG_1ST_OFFSET + 0xc, &val3);

	pcie_tlp_hdr_t *tlp_hdr;
	tlp_hdr = (pcie_tlp_hdr_t *)&val0;
	ptr = &val2;
	pcie_cpl_t* cpl = (pcie_cpl_t *)ptr;
	xinfo("--------------Dump AER log-------------------------\n");
	switch (tlp_hdr->type) {
	case PCIE_TLP_TYPE_IO:
	case PCIE_TLP_TYPE_MEM:
	case PCIE_TLP_TYPE_MEMLK: {
	    xinfo(
				"PCIE_TLP_TYPE_IO/PCIE_TLP_TYPE_MEM/PCIE_TLP_TYPE_MEMLK (type = 0x%x)\n",
				tlp_hdr->type);
		pcie_mem64_t *pmp = ptr;
		xinfo("Request ID: %02x:%02x.%02x\n", MY_PCIE_BUS(pmp->rid),
				MY_PCIE_DEV(pmp->rid), MY_PCIE_FUNC(pmp->rid));
		if (tlp_hdr->fmt & 0x1)
		    xinfo("fmt bit 1 = 1\n");
		else
		    xinfo("fmt bit 1 = 0\n");

		break;
	}

	case PCIE_TLP_TYPE_CFG0:
	case PCIE_TLP_TYPE_CFG1: {
	    xinfo("PCIE_TLP_TYPE_CFG\n");
		pcie_cfg_t *pcp = ptr;
		xinfo("BDF: %02x:%02x.%02x\n", pcp->bus, pcp->dev, pcp->func);

		break;
	}

	case PCIE_TLP_TYPE_CPL:
	case PCIE_TLP_TYPE_CPLLK:
	    xinfo(" TLP Completion type: \n");
	    xinfo("\tCompleter ID: %02x:%02x.%02x\n", MY_PCIE_BUS(cpl->cid),
				MY_PCIE_DEV(cpl->cid), MY_PCIE_FUNC(cpl->cid));
	    xinfo("\tCompl Status: 0x%x\n", cpl->status);
	    xinfo("\tBCM: 0x%x\n", cpl->bcm);
	    xinfo("\tByteCount: 0x%x\n\n", cpl->bc);

	    xinfo("\tRequester ID: %02x:%02x.%02x\n",
				MY_PCIE_BUS(((pcie_cpl_t * )ptr)->rid),
				MY_PCIE_DEV(((pcie_cpl_t * )ptr)->rid), MY_PCIE_FUNC(cpl->rid));
	    xinfo("\tTag: 0x%x\n", cpl->tag);
	    xinfo("\tLower address: 0x%x\n", cpl->laddr);
		break;
	}

}

static void pci_aer_corr_details(u32 val)
{
	/* CORRECTABLE ERRORS
	Bit[0] - Receiver_Error_Status
	Bits[5:1] ? Reserved == 0
	Bit[6] - Bad_TLP_Status
	Bit[7] - Bad_DLLP_Status
	Bit[8] - Replay_Num_Rollover_Status
	Bits[11:9] ? Reserved == 000
	Bit[12] - Replay_Timer_Timeout_Status
	Bit[13] - Advisory_Non_Fatal_Error_Status
	Bit[14] - Corrected Internal Error Status
	Bit[15] ? Header Log Overflow Status
	Bits[31:16] ? Reserved == 0*/

	if(val & 1)
	    xinfo("\033[31m \t Receiver_Error\n \033[0m");
	if(val & (1 << 6))
	    xinfo("\033[31m \t Bad_TLP\n \033[0m");
	if(val & (1 << 7))
	    xinfo("\033[31m \t Bad_DLLP\n \033[0m");
	if(val & (1 << 8))
	    xinfo("\033[31m \t Replay_Num_Rollover \033[0m\n");
	if(val & (1 << 12))
	    xinfo("\033[31m \t Replay_Timer_Timeout \033[0m\n");
	if(val & (1 << 13))
	    xinfo("\033[31m \t Advisory_Non_Fatal_Error \033[0m\n");
	if(val & (1 << 14))
	    xinfo("\033[31m \t Corrected Internal Error \033[0m\n");
	if(val & (1 << 15))
	    xinfo("\033[31m \t Header Log Overflow \033[0m\n");
}

/*
 * -------------------------------------- Borrowed from Linux drivers/pci/pci.c
 */
#define PCI_FIND_CAP_TTL     48

/* Extended Capabilities (PCI-X 2.0 and Express) */
#define PCI_EXT_CAP_ID(header)	(header & 0x0000ffff)
#define PCI_EXT_CAP_VER(header)	((header >> 16) & 0xf)
#define PCI_EXT_CAP_NEXT(header) ((header >> 20) & 0xffc)
#define PCI_CFG_SPACE_SIZE		256
#define PCI_CFG_SPACE_EXP_SIZE		4096

static int __pci_find_next_cap_ttl(struct pci_dbdf *dev, u8 pos, int cap,
		int *ttl)
{
        u8 id;

        while ((*ttl)--) {
                pci_read_config_byte(dev, pos, &pos);
                if (pos < 0x40)
                        break;
                pos &= ~3;
                pci_read_config_byte(dev, pos + PCI_CAP_LIST_ID,
                                         &id);
                if (id == 0xff)
                        break;
                if (id == cap)
                        return pos;
                pos += PCI_CAP_LIST_NEXT;
        }
        return 0;
}

static int __pci_find_next_cap(struct pci_dbdf *dev, u8 pos, int cap)
{
        int ttl = PCI_FIND_CAP_TTL;

        return __pci_find_next_cap_ttl(dev, pos, cap, &ttl);
}

static int __pci_bus_find_cap_start(struct pci_dbdf *dev)
{
    u16 status;
	u8 hdr_type;

	pci_read_config_byte(dev, PCI_HEADER_TYPE, &hdr_type);
	pci_read_config_word(dev, PCI_STATUS, &status);
        if (!(status & PCI_STATUS_CAP_LIST))
                return 0;

        switch (hdr_type & 0x7f) {
        case PCI_HEADER_TYPE_NORMAL:
        case PCI_HEADER_TYPE_BRIDGE:
                return PCI_CAPABILITY_LIST;
        case PCI_HEADER_TYPE_CARDBUS:
                return PCI_CB_CAPABILITY_LIST;
        default:
                return 0;
        }

        return 0;
}

/**
 * pci_find_capability - query for devices' capabilities
 * @dev: PCI device to query
 * @cap: capability code
 *
 * Tell if a device supports a given PCI capability.
 * Returns the address of the requested capability structure within the
 * device's PCI configuration space or 0 in case the device does not
 * support it.  Possible values for @cap:
 *
 *  %PCI_CAP_ID_PM           Power Management
 *  %PCI_CAP_ID_AGP          Accelerated Graphics Port
 *  %PCI_CAP_ID_VPD          Vital Product Data
 *  %PCI_CAP_ID_SLOTID       Slot Identification
 *  %PCI_CAP_ID_MSI          Message Signalled Interrupts
 *  %PCI_CAP_ID_CHSWP        CompactPCI HotSwap
 *  %PCI_CAP_ID_PCIX         PCI-X
 *  %PCI_CAP_ID_EXP          PCI Express
 */
static int pci_find_capability(struct pci_dbdf *dev, int cap)
{
        int pos;

        pos = __pci_bus_find_cap_start(dev);
        if (pos)
                pos = __pci_find_next_cap(dev, pos, cap);

        return pos;
}

/**
 * Find next PCIe Extended Capability
 * @param dev
 * @param start
 * @param cap
 * @return
 */
static int pci_find_next_ext_capability(struct pci_dbdf *dev, int start,
		int cap)
{
	u32 header;
	int ttl;
	int pos = PCI_CFG_SPACE_SIZE;

	/* minimum 8 bytes per capability */
	ttl = (PCI_CFG_SPACE_EXP_SIZE - PCI_CFG_SPACE_SIZE) / 8;

	if (start)
		pos = start;

	if (pci_read_config_dword(dev, pos, &header) == 0)
		return 0;

	/*
	 * If we have no capabilities, this is indicated by cap ID,
	 * cap version and next pointer all being 0.
	 */
	if (header == 0)
		return 0;

	while (ttl-- > 0) {
		if (PCI_EXT_CAP_ID(header) == cap && pos != start)
			return pos;

		pos = PCI_EXT_CAP_NEXT(header);
		if (pos < PCI_CFG_SPACE_SIZE)
			break;

		if (pci_read_config_dword(dev, pos, &header) == 0)
			break;
	}

	return 0;
}

/**
 * pci_find_ext_capability - Find an extended capability
 * @dev: PCI device to query
 * @cap: capability code
 *
 * Returns the address of the requested extended capability structure
 * within the device's PCI configuration space or 0 if the device does
 * not support it.  Possible values for @cap:
 *
 *  %PCI_EXT_CAP_ID_ERR		Advanced Error Reporting
 *  %PCI_EXT_CAP_ID_VC		Virtual Channel
 *  %PCI_EXT_CAP_ID_DSN		Device Serial Number
 *  %PCI_EXT_CAP_ID_PWR		Power Budgeting
 */
int pci_find_ext_capability(struct pci_dbdf *dev, int cap)
{
	return pci_find_next_ext_capability(dev, 0, cap);
}

static int pci_cfg_idtest(struct pci_dbdf *bdf, u16 vendorID, ulong time,
		uint32_t ce_mask, uint32_t uc_mask, int progress)
{
	double runtime = 0;
	double start_time, current_time; /*ms*/
	u16 _word;
	uint32_t percent, prev_percent = 0xffff;
	u16 vid, did;
	int ret;

	ret = pci_read_config_word(bdf, PCI_VENDOR_ID, &vid);
	if (ret == 0) {
	    xerror("  [%04x:%02x:%02x.%02x] Read config failed\n", bdf->domain,
				bdf->bus, bdf->dev, bdf->func);
		return PCI_TEST_VENDORID_ERR;
	}
	ret = pci_read_config_word(bdf, PCI_DEVICE_ID, &did);
	if (ret == 0) {
		xerror("  [%04x:%02x:%02x.%02x] Read config failed\n", bdf->domain,
				bdf->bus, bdf->dev, bdf->func);
		return PCI_TEST_VENDORID_ERR;
	}

	xinfo(" AER testing in %lu sec at [%04x:%02x:%02x.%02x - %04x:%04x]...\n",
		time, bdf->domain, bdf->bus, bdf->dev, bdf->func, vid, did);

	/* Clear AER fist*/
	pci_aer_clear_chain(bdf);

	if (progress)
	    xinfo(" Running: [  0%%] [                      ]");

	start_time = get_time_sec();
	while (1) {
		/* Only check vendor ID if input 'vendorID' is not equal 0xffff */
		if (pci_read_config_word(bdf, PCI_VENDOR_ID, &_word) == 0) {
			xerror("  [%04x:%02x:%02x.%02x] Read config failed\n", bdf->domain,
					bdf->bus, bdf->dev, bdf->func);
			return PCI_TEST_VENDORID_ERR;
		}
		if (vendorID != 0xffff && _word != vendorID) {
			xerror(" Test VendorID fail, expect %04x, got %04x\n", vendorID,
					_word);
			return PCI_TEST_VENDORID_ERR;
		}

		/* Check AER of device an its parent(s)*/
		ret = pci_aer_check_chain(bdf, ce_mask, uc_mask, 1) ;
		if (ret != PCI_TEST_OK)
			return ret;

		current_time = get_time_sec();
		runtime = current_time - start_time;

		percent = (uint32_t) (runtime * 100 / time);
		if (percent > 100)
			percent = 100;

		if (prev_percent != percent) {
			if (progress)
				progress_bar(percent);
			prev_percent = percent;
		}

		/* Check RAS error */
		if (!cbIsEmpty(&ras_cb_pci)) {
			ElemType *e;
			e = calloc(1, sizeof(ElemType));
			if (!e) {
				xerror("Error: %s %d malloc failed\n", __FILE__, __LINE__);
				goto exit;
			}

			uint16_t portnum;

			cbRead(&ras_cb_pci, e);/* example: e->buf = "PCI6"*/

			if (strlen(e->buf) != 4) {
				xinfo("Warning: [RAS]  %s %d: Invalid PCI format (%s)\n",
						__FILE__, __LINE__, e->buf);
				free(e);
				goto exit;
			}

			portnum = e->buf[3] - '0';
			if (portnum > 7) {
				xinfo("Warning: [RAS]  %s %d: Invalid PCI port (%d)\n",
						__FILE__, __LINE__, portnum);
			} else {
				if (bdf->domain == portnum) {
					return PCI_TEST_RAS_ERR;
				} else {
					/* write back to queue */
					cbWrite(&ras_cb_pci, e);
				}
			}

			free(e);

		}
exit:
		if (runtime >= time) {
		    xinfo(" Test Pass in %ld sec(s)\n\n", time);
			break;
		}

		if (ctrlc()) {
		    xinfo(" <BREAK>\n");
			return PCI_TEST_USR_BREAK;
		}
	}

	return PCI_TEST_OK;
}

static int pci_aer(cmd_tbl_t *cmdtp, int argc, char *argv[])
{
	/*
	 * pci aer <s:b:d.f> test [time]
	 * pci aer <s:b:d.f> <mask/unmask>
	 */
	struct pci_dbdf sbdf = {0};
	int ret = 0;
	ulong time = 30;
	struct pci_test test = {0};

	if (argc < 4 || argc > 5)
		goto usage;

	if (pci_parse_dbdf(argv[2], &sbdf) != COMMAND_OK) {
	    xinfo("Error: Invalid <d:b:d.f>\n");
		goto usage;
	}
	if (strncmp(argv[3], "test", 4) == 0) {
		if (argc == 5)
			time = strtoul(argv[4], NULL, 10);

		test.dev = &sbdf;
		test.vid = 0xffff;
		test.time = time;
		test.gen = PCI_CHECK_GEN_FROM_LNKSTAT_LNKCAP;
		test.width = PCI_CHECK_WIDTH_FROM_LNKSTAT_LNKCAP;
		ret = pci_aer_test(&test, 1);
	} else if (strncmp(argv[3], "mask", 4) == 0) {
		pci_aer_mask_chain(&sbdf);
	} else if (strncmp(argv[3], "unmask", 4) == 0) {
		pci_aer_unmask_chain(&sbdf, 1);
	} else {
		goto usage;
	}

	return ret;

usage:
    xinfo("Usage: \n");
    xinfo("%s\n", cmdtp->help);
	return -1;
}

void progress_bar(unsigned int percent)
{
	/*strlen(" Running: [100%] [ ==================== ]") + 1*/
#define PROGRESS_STR_SZ		42
	static char progr_str[PROGRESS_STR_SZ];
	static char bar_str[20];
	char *p;
	int i;

	for (i = 0; i < 20; i++) {
		if (i <= percent / 5) {
			bar_str[i] = '=';
			continue;
		}
		if (i == percent / 5 + 1)
			bar_str[i] = '>';
		else
			bar_str[i] = ' ';
	}

	for (i = 0; i < PROGRESS_STR_SZ - 1; i++)
	    xinfo("\b");

	p = &bar_str[0];
	sprintf(progr_str, " Running: [%3d%%] [ %s ]", percent, p);
	progr_str[PROGRESS_STR_SZ - 1] = '\0';
	p = &progr_str[0];
	while (*p)
	    xinfo("%c", *p++);
	if (percent == 100)
	    xinfo("\n");

	fflush(stdout);
}

/**
 * Unmask AER
 * @param bdf
 * @param all: if true will unmask all, else unmask to default as before running
 * diagwrt
 */
void pci_aer_unmask_chain(struct pci_dbdf *bdf, int all)
{
	int capptr, capptr_dev;
	struct pci_dbdf parent = { 0 };
	uint32_t ce_mask = 0, uc_mask = 0;

	capptr_dev = pci_find_ext_capability(bdf, PCI_EXT_CAP_ID_AER);
	if (!capptr_dev) {
	    xdebug("  Info: AER CAP of Device %04x:%02x:%02x.%02x not found!\n",
				bdf->domain, bdf->bus, bdf->dev, bdf->func);
	} else {
		if (all) {
			pci_write_config_dword(bdf, capptr_dev + PCI_ERR_COR_MASK, 0);
			pci_write_config_dword(bdf, capptr_dev + PCI_ERR_UNCOR_MASK, 0);
		} else {
			if (pci_get_aer_default_mask(bdf, &ce_mask, &uc_mask)) {
				pci_write_config_dword(bdf, capptr_dev + PCI_ERR_COR_MASK,
						ce_mask);
				pci_write_config_dword(bdf, capptr_dev + PCI_ERR_UNCOR_MASK,
						uc_mask);
			}
		}
	}

	memcpy(&parent, bdf, sizeof(parent));
	while (pci_get_parent(&parent, &parent) == PCI_PARENT_FOUND) {
		/* Get PCI Express Capability pointer */
		capptr = pci_find_ext_capability(&parent, PCI_EXT_CAP_ID_AER);
		if (!capptr) {
		    xdebug("Info: AER CAP of%04x:%02x:%02x.%02x not found!\n",
					parent.domain, parent.bus, parent.dev, parent.func);
		} else {
			if (all) {
				pci_write_config_dword(&parent, capptr + PCI_ERR_COR_MASK, 0);
				pci_write_config_dword(&parent, capptr + PCI_ERR_UNCOR_MASK, 0);
			} else {
				if (pci_get_aer_default_mask(&parent, &ce_mask, &uc_mask)) {
					pci_write_config_dword(&parent, capptr + PCI_ERR_COR_MASK,
							ce_mask);
					pci_write_config_dword(&parent, capptr + PCI_ERR_UNCOR_MASK,
							uc_mask);
				}
			}
		}

		if (ctrlc()) {
		    xinfo(" <BREAK>\n");
		}
	}
}

int pci_parse_dbdf(char* name, struct pci_dbdf* p)
{
	/*domain.bus.dev.func
	 * Ex: 0000:01:00.0
	 * */
	unsigned int idomain, ifunc, idev, ibus;

	char* pch;
	if (strlen(name) > 16)
		return COMMAND_ERROR;

	//3rd dot
	pch = strrchr(name, '.');
	if(pch){
		*pch = 0;
		pch++;
		ifunc = strtoul(pch, NULL, 16);
	} else
		return COMMAND_ERROR;

	//2nd dot
	pch = strrchr(name, ':');
	if(pch){
		*pch = 0;
		pch++;
		idev = strtoul(pch, NULL, 16);
	} else
		return COMMAND_ERROR;

	//1st :
	pch = strrchr(name, ':');
	if(pch){
		*pch = 0;
		pch++;
		ibus = strtoul(pch, NULL, 16);
	} else
		return COMMAND_ERROR;

	idomain = strtoul(name, NULL, 16);

	p->domain = idomain;
	p->bus = ibus;
	p->dev = idev;
	p->func = ifunc;

	return COMMAND_OK;
}

/**
 * Mask AER interrupt
 * @param bdf
 */
void pci_aer_mask_chain(struct pci_dbdf *bdf)
{
	int capptr, capptr_dev;
	struct pci_dbdf parent = { 0 };

	capptr_dev = pci_find_ext_capability(bdf, PCI_EXT_CAP_ID_AER);
	if (!capptr_dev) {
	    xdebug("  Info: AER CAP of Device %04x:%02x:%02x.%02x not found!\n",
				bdf->domain, bdf->bus, bdf->dev, bdf->func);
	} else {
		pci_write_config_dword(bdf, capptr_dev + PCI_ERR_COR_MASK, 0xffffffff);
		pci_write_config_dword(bdf, capptr_dev + PCI_ERR_UNCOR_MASK,
				0xffffffff);
	}

	memcpy(&parent, bdf, sizeof(parent));
	while (pci_get_parent(&parent, &parent) == PCI_PARENT_FOUND) {
		/* Get PCI Express Capability pointer */
		capptr = pci_find_ext_capability(&parent, PCI_EXT_CAP_ID_AER);
		if (!capptr) {
		    xdebug("Info: AER CAP of %04x:%02x:%02x.%02x not found!\n",
					parent.domain, parent.bus, parent.dev, parent.func);
		} else {
			pci_write_config_dword(&parent, capptr + PCI_ERR_COR_MASK,
					0xffffffff);
			pci_write_config_dword(&parent, capptr + PCI_ERR_UNCOR_MASK,
					0xffffffff);
		}

		if (ctrlc()) {
		    xinfo(" <BREAK>\n");
		}
	}
}

void pci_aer_clear_chain(struct pci_dbdf *bdf)
{
	int capptr, capptr_dev;
	struct pci_dbdf parent = { 0 };

	capptr_dev = pci_find_ext_capability(bdf, PCI_EXT_CAP_ID_AER);
	if (!capptr_dev) {
	    xdebug("  Info: AER CAP of Device %04x:%02x:%02x.%02x not found!\n",
				bdf->domain, bdf->bus, bdf->dev, bdf->func);
	} else {
		pci_write_config_dword(bdf, capptr_dev + PCI_ERR_COR_STATUS,
				0xffffffff);
		pci_write_config_dword(bdf, capptr_dev + PCI_ERR_UNCOR_STATUS,
				0xffffffff);
	}

	memcpy(&parent, bdf, sizeof(parent));
	while (pci_get_parent(&parent, &parent) == PCI_PARENT_FOUND) {
		/* Get PCI Express Capability pointer */
		capptr = pci_find_ext_capability(&parent, PCI_EXT_CAP_ID_AER);
		if (!capptr) {
		    xdebug("Info: AER CAP of %04x:%02x:%02x.%02x not found!\n",
					parent.domain, parent.bus, parent.dev, parent.func);
		} else {
			pci_write_config_dword(&parent, capptr + PCI_ERR_COR_STATUS,
					0xffffffff);
			pci_write_config_dword(&parent, capptr + PCI_ERR_UNCOR_STATUS,
					0xffffffff);
		}

		if (ctrlc()) {
		    xinfo(" <BREAK>\n");
		}
	}
}

/**
 * Get AER of device and all its parent(s)
 * @param bdf
 * @return IDTEST_AER_OK if no AER
 */
int pci_aer_check_chain(struct pci_dbdf *bdf, uint32_t ce_mask,
		uint32_t uc_mask, int verbose)
{
	int capptr, capptr_dev;
	struct pci_dbdf parent = { 0 };
	u32 val = 0xffffffff;

	capptr_dev = pci_find_ext_capability(bdf, PCI_EXT_CAP_ID_AER);
	if (!capptr_dev) {
		/*xdebug("Info: AER CAP of Device %04x:%02x:%02x.%02x not found!\n",
				bdf->domain, bdf->bus, bdf->dev, bdf->func);*/
	} else {
		/* Check AER of device */
		if (pci_read_config_dword(bdf, capptr_dev + PCI_ERR_COR_STATUS, &val)
				== 0) {
			xerror("  [%04x:%02x:%02x.%02x] Read config failed\n", bdf->domain,
					bdf->bus, bdf->dev, bdf->func);
			return PCI_TEST_CONFIG_ACCESS_ERR;
		}

		if ((val & ~ce_mask) && verbose) {
			xerror("  [%04x:%02x:%02x.%02x] AER Correctable Error Status:\n",
					bdf->domain, bdf->bus, bdf->dev, bdf->func);
			pci_aer_corr_details(val);
			pci_write_config_dword(bdf, capptr_dev + PCI_ERR_COR_STATUS, val);
			return PCI_TEST_AER_ERR;
		}

		if (pci_read_config_dword(bdf, capptr_dev + PCI_ERR_UNCOR_STATUS, &val)
				== 0) {
			xerror("  [%04x:%02x:%02x.%02x] Read config failed\n", bdf->domain,
					bdf->bus, bdf->dev, bdf->func);
			return PCI_TEST_CONFIG_ACCESS_ERR;
		}
		if ((val & ~uc_mask) && verbose) {
			xerror("  [%04x:%02x:%02x.%02x] AER UnCorrectable Error Status:\n",
					bdf->domain, bdf->bus, bdf->dev, bdf->func);
			pci_aer_uncorr_details(val);
			pci_aer_parse_log(bdf, capptr_dev);
			pci_write_config_dword(bdf, capptr_dev + PCI_ERR_UNCOR_STATUS, val);
			return PCI_TEST_AER_ERR;
		}
	}

	/* Check AER of parent(s) */
	memcpy(&parent, bdf, sizeof(parent));
	while (pci_get_parent(&parent, &parent) == PCI_PARENT_FOUND) {
		/* Get PCI Express Capability pointer */
		capptr = pci_find_ext_capability(&parent, PCI_EXT_CAP_ID_AER);
		if (!capptr) {
			/*xdebug("Info: AER CAP of%04x:%02x:%02x.%02x not found!\n",
					parent.domain, parent.bus, parent.dev, parent.func);*/
			continue;
		}

		if (pci_read_config_dword(&parent, capptr + PCI_ERR_COR_STATUS, &val)
				== 0) {
			xerror("  [%04x:%02x:%02x.%02x] Read config failed\n",
					parent.domain, parent.bus, parent.dev, parent.func);
			return PCI_TEST_CONFIG_ACCESS_ERR;
		}
		if ((val & ~ce_mask) && verbose) {
		    xerror("  [%04x:%02x:%02x.%02x] AER Correctable Error Status:\n",
					parent.domain, parent.bus, parent.dev, parent.func);
			pci_aer_corr_details(val);
			pci_write_config_dword(&parent, capptr + PCI_ERR_COR_STATUS, val);
			return PCI_TEST_AER_ERR;
		}

		if (pci_read_config_dword(&parent, capptr + PCI_ERR_UNCOR_STATUS, &val)
				== 0) {
			xerror("  [%04x:%02x:%02x.%02x] Read config failed\n",
					parent.domain, parent.bus, parent.dev, parent.func);
			return PCI_TEST_CONFIG_ACCESS_ERR;
		}

		if ((val & ~uc_mask) && verbose) {
			xerror("  [%04x:%02x:%02x.%02x] AER UnCorrectable Error Status:\n",
					parent.domain, parent.bus, parent.dev, parent.func);
			pci_aer_uncorr_details(val);
			pci_aer_parse_log(&parent, capptr);
			pci_write_config_dword(&parent, capptr + PCI_ERR_UNCOR_STATUS, val);
			return PCI_TEST_AER_ERR;
		}

		if (ctrlc()) {
		    xinfo(" <BREAK>\n");
			return PCI_TEST_USR_BREAK;
		}
	}

	return PCI_TEST_OK;
}

/**
 * Test Scan & AER
 * @param port
 * @param vendorID
 * @param time
 * @param progress : Show progress
 * @return
 */
int pci_aer_test(struct pci_test* test, int progress)
{
	int ret;

	ret = pci_scan_test(test->dev, test->gen, test->width);
	if (ret == PCI_TEST_OK){
		ret = pci_cfg_idtest(test->dev, test->vid, test->time, test->corr_mask,
				test->uncorr_mask, progress);
	}

	return ret;

}

void pci_aer_show(struct pci_dbdf *bdf)
{
	u32 val;
	u32 uncor = 0;
	int capptr;
	u16 vid, did;

	pci_read_config_word(bdf, PCI_VENDOR_ID, &vid);
	pci_read_config_word(bdf, PCI_DEVICE_ID, &did);

	// Get PCI Express Capability pointer
	capptr = pci_find_ext_capability(bdf, PCI_EXT_CAP_ID_AER);
	if (!capptr) {
	    xerror("Error: AER CAP not found!\n");
		return;
	}

	xinfo("\n");
	xinfo(" AER for [%04x:%02x:%02x.%02x - %04x:%04x]\n", bdf->domain,
			bdf->bus, bdf->dev,	bdf->func, vid, did);
	pci_read_config_dword(bdf, capptr + PCI_ERR_COR_STATUS, &val);
	if (val) {
	    xinfo("  Correctable Error Status:\n");
		pci_aer_corr_details(val);
		pci_write_config_dword(bdf, capptr + PCI_ERR_COR_STATUS, val);
	} else {
	    xinfo("  No AER Correctable Error\n");
	}

	pci_read_config_dword(bdf, capptr + PCI_ERR_UNCOR_STATUS, &val);
	uncor = pci_aer_uncorr_details(val);
	if (uncor) {
		pci_aer_parse_log(bdf, capptr);
		pci_write_config_dword(bdf, capptr + PCI_ERR_UNCOR_STATUS, val);
	} else {
	    xinfo("  No AER Uncorrectable Error\n");
	}

	xinfo("\n");
}


static void* pci_thread(void* data)
{
	struct pci_test test = {0};

	test.dev = (struct pci_dbdf*) data;
	test.vid = 0xffff;
	test.gen = PCI_CHECK_GEN_FROM_LNKSTAT_LNKCAP;
	test.width = PCI_CHECK_WIDTH_FROM_LNKSTAT_LNKCAP;
	test.time = 10;

	xinfo("\033[32m Testing on [%04x:%02x:%02x.%02x]\033[0m \n",
			test.dev->domain, test.dev->bus, test.dev->dev, test.dev->func);

	pci_aer_test(&test, 0);
	xinfo("\033[31m  [%04x:%02x:%02x.%02x] finish\033[0m\n", test.dev->domain,
			test.dev->bus, test.dev->dev, test.dev->func);

	return NULL;
}


int pci_test(int argc, char* argv[])
{
	int ret, i;
	static struct pci_dbdf sbdf[5];
	pthread_t thread[5];
	char str[16] = {0};
	int nr_threads = 1;

	if (argc !=2 && argc != 3){
		xerror("Error: arugment\n");
		return 0;
	}
	if (argc == 3){
		nr_threads = strtoul(argv[2], NULL, 10);
		if (nr_threads > 3)
			nr_threads = 3;
	}
	xinfo("nr_threads = %d\n", nr_threads);

	strcpy(str, "0001:01:00.0");
	pci_parse_dbdf(str, &sbdf[0]);

	strcpy(str, "0001:02:00.0");
	pci_parse_dbdf(str, &sbdf[1]);

	strcpy(str, "0001:00:00.0");
	pci_parse_dbdf(str, &sbdf[2]);

	strcpy(str, "3:1:0.0");
	pci_parse_dbdf(str, &sbdf[3]);

	strcpy(str, "4:1:0.0");
	pci_parse_dbdf(str, &sbdf[4]);

	for (i = 0; i < nr_threads; i++) {
		ret = pthread_create(&thread[i], NULL, pci_thread, (void *) &sbdf[i]);
	}

	for (i = 0; i < nr_threads; i++) {
		ret = pthread_join(thread[i], NULL);
	}

	return 0;
}

int do_pci(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int ret = 0;
	struct pci_dbdf sbdf = {0};
	int i;

	if (argc > 1) {
		if (strncmp(argv[1], "ls", 2) == 0) {
			for (i = 0; i < argc - 1; i++){
				argv[i] = argv[i + 1];
			}
			argc--;
			pciutils_ls(argc, argv);
		} else if (strncmp(argv[1], "parent", 4) == 0) {
			if (argc == 3){
				if (pci_parse_dbdf(argv[2], &sbdf) != COMMAND_OK){
					xinfo("Error: Invalid bus <s:b:d.f>\n");
					goto usage;
				}
				struct pci_dbdf parent = {0};
				ret = pci_get_parent(&sbdf, &parent);
				if (ret == PCI_PARENT_FOUND) {
				    xinfo(" Parent of %04x:%02x:%02x.%02x: %04x:%02x:%02x.%02x\n",
							sbdf.domain, sbdf.bus, sbdf.dev, sbdf.func,
							parent.domain, parent.bus, parent.dev, parent.func);
				} else if (ret == PCI_DEV_IS_ROOT){
				    xinfo(" Device %04x:%02x:%02x.%02x is Root\n",
							sbdf.domain, sbdf.bus, sbdf.dev, sbdf.func);
				} else {
				    xinfo(" Error: Parent not found\n");
				}
			}
			else
				goto usage;
		} else if (strncmp(argv[1], "ob", 2) == 0) {
			ret = pcie_outbound_info(cmdtp, argc, argv);
		} else if (strncmp(argv[1], "ib", 2) == 0) {
			ret = pcie_inbound_info(cmdtp, argc, argv);
		} else if (strcmp(argv[1], "aer") == 0) {
			ret = pci_aer(cmdtp, argc, argv);
		} else if (strcmp(argv[1], "test") == 0) {
			ret = pci_test(argc, argv);
		} else {
			goto usage;
		}
	} else {
		goto usage;
	}

	return ret;

usage:
    xinfo("Usage: \n");
    xinfo("%s\n", cmdtp->help);
	return -1;
}

int do_pcicfg(cmd_tbl_t* cmdtp, int flag, int argc, char* argv[])
{
	/*
	 * pcicfg header <bus.dev.func>
	 * pcicfg display <bus.dev.func> <address> [count]
	 * pcicfg write <bus.dev.func> <address> <value>
	 * pcicfg idtest <b>.<d>.<f> <vendorID> <time_sec>
	 * pcicfg aer <b>.<d>.<f>
	 */

	ulong addr = 0, value = 0, size = 0, time_sec = 30, vendorID;
	struct pci_dbdf sbdf;
	char cmd = 's';

	if (argc > 1)
		cmd = argv[1][0];

	switch (cmd) {
	case 'd':		/* display */
	case 'w':		/* write */
	case 'h':		/* header */
	case 'a':		/* aer display*/
	case 'i':		/* id test*/
		/* Check for a size specification. */
		if (argc < 3)
			goto usage;

		size = cmd_get_data_size(argv[1], 4);
		if (pci_parse_dbdf(argv[2], &sbdf) != COMMAND_OK){
		    xinfo("Error: Invalid bus <s:b:d.f>\n");
			goto usage;
		}
		if (argc > 3)
			addr = strtoul(argv[3], NULL, 16);
		if (argc > 4)
			value = strtoul(argv[4], NULL, 16);

		switch (cmd) {
			case 'd':		/* display */
				return pci_cfg_display(&sbdf, addr, size, value);
			case 'w':		/* write */
				if (argc < 5)
					goto usage;
				return pci_cfg_write(&sbdf, addr, size, value);
			case 'a':
				pci_aer_show(&sbdf);
				return 0;
			case 'i':
				if (argc < 4 || argc > 5)
					goto usage;
				vendorID = strtoul(argv[3], NULL, 16);
				if (argc == 5)
					time_sec = strtoul(argv[4], NULL, 10);
				return pci_cfg_idtest(&sbdf, vendorID, 0x0, 0x0, time_sec, 1);
		}
		break;

	default:
		goto usage;
	}

usage:
    xinfo("Usage:\n");
    xinfo("%s\n", cmdtp->help);
	return 1;
}

VEDIAG_CMD(pci, 32, 0, do_pci,
	"PCI Commands Utilities",
	" pci ls [-v[vv]] [-t]            - List all PCI devices\r\n"
	" pci ob <host>                   - Dump outbound config of host\r\n"
	" pci ib <host>                   - Dump inbound config of host\r\n"
	" pci aer <s:b:d.f> test [time]   - Read ID and check AER, default 30s\r\n"
	" pci aer <s:b:d.f> mask          - Mask AER interrupt\r\n"
	" pci aer <s:b:d.f> unmask        - UnMask AER interrupt\r\n"
	"\n Argument:\n"
	"    - <s:b:d.f> : domain, bus, device, function of PCI device\r\n");

VEDIAG_CMD(pcicfg, 32, 0, do_pcicfg,
		"PCI commands to access configuration space",
		" pcicfg display[.b, .w, .l] <s:b:d.f> [Offset] [Count]\r\n"
		"    Display PCI configuration space\r\n"
		" pcicfg write[.b, .w, .l] <d>:<b>:<d>.<f> <address> <value>\r\n"
		"    Write to CFG address\r\n"
		" pcicfg idtest <s:b:d.f> <vendorID> [time]\r\n"
		"    Compare Vendor ID and check AER in 'time' second, default 30s\r\n"
		" pcicfg aer <s:b:d.f>\n"
		"    Show AER status of device\n"
		"\n Argument:\n"
		"    - s:b:d.f : domain, bus, device, function of PCI device\r\n"
		"    - .b/.w/.l: display follow byte/word/double-words\r\n"
		"    - Offset  : of configuration space need to dump\r\n"
		"    - Count   : the number of bytes/words/double-words need to dump\r\n");

