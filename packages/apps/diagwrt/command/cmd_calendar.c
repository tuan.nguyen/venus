/*
 * cmd_time.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <cmd.h>
#include <log.h>
#include <time_util.h>
#include <types.h>

int days_in_month[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
char *months[] = { " ", "\n\n\nJanuary", "\n\n\nFebruary", "\n\n\nMarch",
		"\n\n\nApril", "\n\n\nMay", "\n\n\nJune", "\n\n\nJuly", "\n\n\nAugust",
		"\n\n\nSeptember", "\n\n\nOctober", "\n\n\nNovember", "\n\n\nDecember" };

static int determinedaycode(int year)
{
	int daycode;
	int d1, d2, d3;

	d1 = (year - 1.) / 4.0;
	d2 = (year - 1.) / 100.;
	d3 = (year - 1.) / 400.;
	daycode = (year + d1 - d2 + d3) % 7;
	return daycode;
}

static int determineleapyear(int year)
{
	if ((((year % 4) == false) && ((year % 100) != false))
			|| ((year % 400) == false)) {
		days_in_month[2] = 29;
		return true;
	} else {
		days_in_month[2] = 28;
		return false;
	}
}

static void calendar(int year, int daycode)
{
	int month, day;
	for (month = 1; month <= 12; month++) {
	    xinfo("%s", months[month]);
	    xinfo("\n\nSun  Mon  Tue  Wed  Thu  Fri  Sat\n");

		// Correct the position for the first date
		for (day = 1; day <= 1 + daycode * 5; day++) {
		    xinfo(" ");
		}

		// Print all the dates for one month
		for (day = 1; day <= days_in_month[month]; day++) {
		    xinfo("%2d", day);

			// Is day before Sat? Else start next line Sun.
			if ((day + daycode) % 7 > 0)
			    xinfo("   ");
			else
			    xinfo("\n ");
		}
		// Set position for next month
		daycode = (daycode + days_in_month[month]) % 7;
	}
}

static int do_calendar(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	time_t seconds;
	struct tm *t;
	seconds = time(NULL);
	/**
	 * Number of second since 01Jan1970
	 */
	xinfo("%lu hours since January 1 1970\n", (long)(seconds / 3600));
	t = localtime(&seconds);
	/**
	 * Display mm/dd/yyyy
	 * hh:mm:ss
	 */
	xinfo("%d/%d/%d ", t->tm_mon + 1, t->tm_mday, t->tm_year + 1900);
	xinfo("%d:%d:%d\n", t->tm_hour, t->tm_min, t->tm_sec);

	/**
	 * Calendar
	 */
	int year = 2013;
	int daycode = determinedaycode(year);
	determineleapyear(year);
	calendar(year, daycode);

	xinfo("\n");
	return 0;
}

VEDIAG_CMD(calendar, 32, 1, do_calendar,
		"Display calendar",
		"")
