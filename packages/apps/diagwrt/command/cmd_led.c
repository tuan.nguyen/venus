
#include <stdlib.h>
#include <string.h>
#include <string_plus.h>
#include <sys/time.h>
#include <unistd.h>
#include <common.h>
#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <cmd.h>
#include <memory.h>
#include <time.h>
#include <time_util.h>
#include <vediag_common.h>
#include <pthread.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/kernel.h>
#include <log.h>
#include <vediag/led.h>

int vediag_list_led_device(void)
{
	char ledname[60];
	int total_dev = 0;
	FILE *devf, *modef;
	int line_num = 0;
	char *line;
	char buf[TEXT_LINE_SIZE], cmd[TEXT_LINE_SIZE], mode[TEXT_LINE_SIZE];

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "ls -1 /sys/class/leds/");
	vediag_debug(VEDIAG_LED, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			memset(ledname, 0, sizeof(ledname));
			strncpy(ledname, line, min(sizeof(ledname), strlen(line)));

			/* Remove  LF, CR, CRLF, LFCR */
			ledname[strcspn(ledname, "\r\n")] = 0;

			vediag_info(VEDIAG_LED, "", "%s:\n", ledname);

			memset(mode, 0, sizeof(mode));
			sprintf(mode, "cat /sys/class/leds/%s/trigger", ledname);
			vediag_debug(VEDIAG_LED, "command:", "%s\n", mode);
			modef = popen(mode, "r");
			if (modef != NULL) {
				while (1) {
					memset(buf, 0, sizeof(buf));
					line = fgets(buf, sizeof(buf), modef);
					if (line == NULL) break;
					vediag_info(VEDIAG_LED, "", "%s\n", line);
				}
			}
			pclose(modef);

			total_dev++;
		}
		pclose(devf);
		vediag_info(VEDIAG_LED, "", "Found LED: %d\n", total_dev);
	}
	return total_dev;
}


int led_cmd_test(struct led_test *test, char *cmd_on, char *cmd_off)
{
	int ret = 0;
	//int i;

	/* Sanity check */
	if(cmd_on == NULL || cmd_off == NULL) {
		vediag_err(VEDIAG_LED, test->name, "cmd_on='%s', cmd_off='%s'\n", cmd_on, cmd_off);
		return -EINVAL;
	}

	//for (i = 0; i < test->cycle; i++) {
		vediag_debug(VEDIAG_LED, test->name, "cmd_on='%s'\n", cmd_on);
		ret = system(cmd_on);
		if (ret != 0) {
			vediag_err(VEDIAG_LED, test->name, "cmd_on='%s' failed\n", cmd_on);
			return -EINVAL;
		}
		usleep(test->interval*1000);

		vediag_debug(VEDIAG_LED, test->name, "cmd_off='%s'\n", cmd_off);
		ret = system(cmd_off);
		if (ret != 0) {
			vediag_err(VEDIAG_LED, test->name, "cmd_off='%s' failed\n", cmd_off);
			return -EINVAL;
		}
		usleep(test->interval*1000);
	//}

	return 0;
}

int led_blink_test(struct led_test *test, u32 *cycle)
{
	char cmd1[TEXT_LINE_SIZE], cmd2[TEXT_LINE_SIZE];
	int ret = 0;

	/* Sanity check */
	if(test->name == NULL) {
		vediag_err(VEDIAG_LED, "", "NULL device\n");
		return -EINVAL;
	}

	/* Setting brightness */
	memset(cmd1, 0, sizeof(cmd1));
	sprintf(cmd1, "echo %d > /sys/class/leds/%s/brightness", test->brightness, test->name);
	vediag_debug(VEDIAG_LED, "command:", "%s\n", cmd1);
	ret = system(cmd1);
	if (ret != 0) {
		vediag_err(VEDIAG_LED, test->name, "Setting brightness failed\n");
		return -EINVAL;
	}

	/* Test trigger */
	if((test->trigger != NULL) && (strcmp(test->trigger, "none") != 0)) {
		memset(cmd1, 0, sizeof(cmd1));
		sprintf(cmd1, "echo %s > /sys/class/leds/%s/trigger", test->trigger, test->name);
		memset(cmd2, 0, sizeof(cmd2));
		sprintf(cmd2, "echo %s > /sys/class/leds/%s/trigger", "none", test->name);
		ret = led_cmd_test(test, cmd1, cmd2);
		if (ret != 0) {
			vediag_err(VEDIAG_LED, test->name, "Test trigger failed\n");
			return -EINVAL;
		}
	}

	/* Test brightness */
	if(test->brightness) {
		memset(cmd1, 0, sizeof(cmd1));
		sprintf(cmd1, "echo %s > /sys/class/leds/%s/brightness", "1", test->name);
		memset(cmd2, 0, sizeof(cmd2));
		sprintf(cmd2, "echo %s > /sys/class/leds/%s/brightness", "0", test->name);
		ret = led_cmd_test(test, cmd1, cmd2);
		if (ret != 0) {
			vediag_err(VEDIAG_LED, test->name, "Test brightness failed\n");
			return -EINVAL;
		}
	}

	*cycle += 1;

	return 0;
}


int led_ring_test(struct led_test *test, u32 *cycle)
{
	char color[60];
	char cmd1[60], cmd2[60];
	int ret = 0;

	/* Sanity check */
	if(test->name == NULL) {
		vediag_err(VEDIAG_LED, "", "NULL device\n");
		return -EINVAL;
	}

	strncpy(color, test->name, min(sizeof(color), strlen(test->name)));
	/* Remove  "_" */
	color[strcspn(color, "_")] = '\0';

	/*
	 * cat /proc/titan_turn
	 * echo '<1:on|0:off> <color> <time> <onfly>' > /proc/titan_dim - Led ring turn on/off/dim-on/dim-off
	 * Color: RED | BLUE | GREEN | YELLOW | PURPLE | CAYON
	 * Time: Time for 1 cycle, milisecond
	 * On-Fly: 1 None FW updating, Use for Dim/Turn Off
	 * Ex: echo '1 RED 1500 0' > /proc/titan_turn - DIM On Red Led in 1.5s
	 * Ex: echo '0 RED 1500 1' > /proc/titan_turn - DIM Off Red Led in 1.5s, None FW updating
	 * Ex: echo '1 RED 0 0' > /proc/titan_turn - Turn On Red Led
	 * Ex: echo '0 RED 0 1' > /proc/titan_turn - Turn Off Red Led, None FW updating
	 */

	/* Turn On */
	memset(cmd1, 0, sizeof(cmd1));
	sprintf(cmd1, "echo '1 %s 0 0' > /proc/titan_turn", color);
	memset(cmd2, 0, sizeof(cmd2));
	sprintf(cmd2, "echo '0 %s 0 0' > /proc/titan_turn", color);
	ret = led_cmd_test(test, cmd1, cmd2);
	if (ret != 0) {
		vediag_err(VEDIAG_LED, test->name, "Test ring failed\n");
		return -EINVAL;
	}

	*cycle += 1;

	return 0;
}

#if 1

#else
int fgets_s(FILE *fp, char *buffer, size_t buflen)
{
	char *line = NULL;
	int len;

	printf("10\n");
	line = fgets(buffer, buflen, fp);
	if(line == NULL) {
		return 0;
	}
	len = strlen(line) - 1;
	if (line[len] == '\n') {
		line[len] = '\0';
	}

	printf("12\n");
	return len;
}

/*
 * Tester must watch LED
 * And give status PASS or FAIL
 * Timeout as FAIL
 */
int led_check_result(struct led_test *test, int timeout, char *input_key)
{
	char stdin_buf[16];
	int length = 0;

	memset(stdin_buf, 0, sizeof(stdin_buf));
	printf("1\n");
	length = fgets_s(stdin, stdin_buf, sizeof(stdin_buf));
	printf("2\n");
	if(length == 0) {
		return -ETIME;
	}

	strlwr(stdin_buf);

	printf("input_key='%s', stdin_buf='%s'\n", input_key, stdin_buf);
	if(strncmp(input_key, stdin_buf, strlen(input_key)) == 0)
		return 0;
	else
		return -EINVAL;
}
#endif

int do_led(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	struct led_test test;
	int ret = 0, cycle = 0;

	if (argc > 1) {
		if (strncmp(argv[1], "ls", 2) == 0) {
			vediag_list_led_device();
		} else if (strcmp(argv[1], "test") == 0) {
			if(argc < 6)
				goto usage;

			memset(&test, 0, sizeof(struct led_test));

			test.name = argv[2];
			test.brightness = !!strtoul(argv[3], NULL, 0);
			test.max_brightness = strtoul(argv[4], NULL, 0);
			if(test.max_brightness > 255)
				test.max_brightness = 255;
			test.trigger = argv[5];
			test.interval = 500;
			test.cycle = 5;
			if(argc >= 7)
				test.cycle = strtoul(argv[6], NULL, 0);
			for(cycle = 0; cycle < test.cycle; ) {
				ret = led_blink_test(&test, &cycle);
				if(ret != 0)
					break;
				printf("LED: '%s' Pass/Fail ?\n  Press y/Y or n/N\n", test.name);
				ret = check_input_key(TESTER_INTERACT_TIMEOUT, 'y');
				if(ret == 0) {
					printf("Result: %s\n", "PASS");
					return 0;
				} else if(ret == -EINVAL) {
					break;
				}
			}
			printf("Result: %s\n", "FAIL");
		} else {
			goto usage;
		}
	} else {
		goto usage;
	}

	return ret;

usage:
    xinfo("Usage: \n");
    xinfo("%s\n", cmdtp->help);

	return -1;
}

VEDIAG_CMD(led, 32, 0, do_led,
	"LED Command Utilities",
	" led ls 				          - List all LED devices\r\n"
	" led test <name> <brightness> <max_brightness> <trigger> [iter] 	- Execute test\r\n"
	"\n Argument:\n"
	"    - <name> : Devices name or Group name\r\n"
	"    - <brightness> : 0=off or 1=on \r\n"
	"    - <max_brightness> : [0:255] \r\n"
	"    - <trigger> : Get from list \r\n"
	"    - [iter] : Iteration\r\n");
