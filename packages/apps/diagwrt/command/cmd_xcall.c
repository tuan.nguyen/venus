
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <cmd.h>
#include <log.h>
#include <time_util.h>
#include <types.h>
#include <linux/list.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>

/* The value returned is -1 on error (e.g., fork(2) failed),
 * and the return status of the command otherwise. This latter
 * return status is in the format specified in wait(2). Thus,
 * the exit code of the command will be WEXITSTATUS(status).
 * In case /bin/sh could not be executed, the exit status
 * will be that of a command that does exit(127).
 * If the value of command is NULL, system() returns nonzero if
 * the shell is available, and zero if not.
 * system() does not affect the wait status of any other children.
*/
int vediag_call_kernel_cmd(int argc, char *argv[])
{
#define VEDIAG_MAX_ARGUMENT_SIZE_BYTES		256

	int ret, i, loc;
	char *ptr, *cmd;

	cmd = malloc(argc*VEDIAG_MAX_ARGUMENT_SIZE_BYTES);
	if (!cmd) {
		xinfo("malloc fail !!\n");
		return -1;
	}

	memset(cmd, 0, argc*VEDIAG_MAX_ARGUMENT_SIZE_BYTES);
	ptr = cmd;
	for (i = 0, loc = 0; i < argc; i++) {
		sprintf(ptr, "%s ", argv[i]);
		loc += strlen(argv[i]) + 1;
		ptr = cmd + loc;
	}

	xinfo("call cmd: %s\n\n", cmd);
	ret = system(cmd);

	if (cmd)
		free(cmd);

	// printf("\nsystem() call return 0x%x\n", ret);
	return ret;
}

static int do_xcall(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int ret = 0;

	if (argc < 2) {
		xinfo ("Usage:\n%s\n", cmdtp->usage);
		return -1;
	}

	ret = vediag_call_kernel_cmd(argc - 1, &argv[1]);

	return ret;
}

VEDIAG_CMD(xcall, 1024, 1, do_xcall,
		"Call another command",
		" xcall <command> - call command\r\n"
		"")
