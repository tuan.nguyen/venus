
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string_plus.h>
#include <unistd.h>
#include <stdarg.h>
#include <cmd.h>
#include <linux/err.h>
#include <vediag_common.h>
#include <pthread.h>
#include <ifaddrs.h>
#include <sys/stat.h>
#include <log.h>
#include <vediag/i2c.h>
#include "iniparser.h"
#include <vediag_storage.h>
#include <listbuff.h>
#include "vediag/pciutils.h"

char file_system[128] = VEDIAG_SYSTEM_CONFIG_FILE_PATH;
char file_testconfig[128] = VEDIAG_TEST_CONFIG_FILE_PATH;
char *file_test_default = NULL;
extern struct listbuff *get_hwname_of_eth_interface(const char *intf);

static pthread_mutex_t cfg_file_lock = PTHREAD_MUTEX_INITIALIZER;

#define VEDIAG_INFO_PREFIX		""
#define VEDIAG_WARN_PREFIX		"VEDIAG_WARNING: "
#define VEDIAG_ERR_PREFIX		"VEDIAG_ERROR: "

struct _token {
    int v;
    const char *s;
};

static pthread_mutex_t print_mutex = PTHREAD_MUTEX_INITIALIZER;

void lock_print(void)
{
	pthread_mutex_lock(&print_mutex);
}

void unlock_print(void)
{
	pthread_mutex_unlock(&print_mutex);
}

static const char *tokentostr(const struct _token *toks, int v)
{
    if (toks != NULL) {
        while (toks->s != NULL) {
            if (toks->v == v)
                return toks->s;
            toks++;
        }
    }
    return "Unknown";
}

const char *vediag_get_IP_name(int IP)
{
    struct _token interfaces[] = VEDIAG_INTERFACES;
    return tokentostr(&interfaces[0], IP);
}

int xprintf(const char *fmt, ...)
{
	int rc;
	va_list args;

	va_start(args, fmt);
	lock_print();

#ifdef CONFIG_XLOG

	vxinfo(fmt, args);
	rc = 1;

#else

	rc = vfprintf(stdout, fmt, args);
	fflush(stdout);

#endif

	unlock_print();
	va_end(args);

	return rc;
}

void vediag_err(int IP, char* dbgInfo, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	lock_print();
	xprint("\033[1;91m");
	xerror("%s%s %s ", VEDIAG_ERR_PREFIX, vediag_get_IP_name(IP),
			dbgInfo ? dbgInfo : "");
    vxerror(fmt, args);
    xprint("\033[0m");
	fflush(stdout);
	unlock_print();
	va_end(args);
}

void vediag_info(int IP, char* dbgInfo, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	lock_print();
    xprint("\033[34m");
    xinfo("%s%s %s ", VEDIAG_INFO_PREFIX, vediag_get_IP_name(IP),
    		dbgInfo ? dbgInfo : "");
    vxinfo(fmt, args);
    xprint("\033[0m");
	fflush(stdout);
	unlock_print();
	va_end(args);
}

void vediag_debug(int IP, char *dbgInfo, const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    lock_print();
    xdebug("%s%s %s ", VEDIAG_INFO_PREFIX, vediag_get_IP_name(IP),
    		dbgInfo ? dbgInfo : "");
    vxdebug(fmt, args);
    fflush(stdout);
    unlock_print();
    va_end(args);
}


void vediag_warn(int IP, char *dbgInfo, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	lock_print();
	xprint("\033[34m");
	xinfo("%s%s %s ", VEDIAG_WARN_PREFIX, vediag_get_IP_name(IP),
			dbgInfo ? dbgInfo : "");
    vxinfo(fmt, args);
    xprint("\033[0m");
	fflush(stdout);
	unlock_print();
	va_end(args);
}

int vediag_getopt(int nargc, char * const *nargv, const char *ostr)
{
	int i, point;
	char *p;

	for (i = 0, point = -1; i < nargc; i++) {
		p = nargv[i];
		if (strcmp(p, ostr) == 0) {
			point = i;
			break;
		}
	}

	return (point); /* return pointer of next argurement */
}

/*
 * INI PARSER
 */
char *get_section_type(int type)
{
	if (type == VEDIAG_MEM)
		return "memory";
	else if (type == VEDIAG_STORAGE)
		return "storage";
	else if (type == VEDIAG_NET)
		return "network";
	else if (type == VEDIAG_PCIE)
		return "pci";
	else if (type == VEDIAG_LED)
		return "led";
	else if (type == VEDIAG_EVENT)
		return "event";
	else if (type == VEDIAG_I2C)
		return "i2c";
	else if (type == VEDIAG_AUDIO)
		return "audio";
	else if (type == VEDIAG_MICRO)
		return "micro";
	else if (type == VEDIAG_BLUETOOTH)
		return "bluetooth";
	else if (type == VEDIAG_FIRMWARE)
		return "fw";
	else if (type == VEDIAG_ZWAVE)
			return "zwave";
	else if (type == VEDIAG_ZIGBEE)
			return "zigbee";
	else
		return "Unknown";
}

void vediag_create_default_system(void)
{
	vediag_update_system_config("Common", NULL, NULL);
	vediag_update_system_config("Common", "Test default", VEDIAG_TEST_CONFIG_FILE);
#if defined(CONFIG_SENSOR_NAME)
	vediag_update_system_config("Common", VEDIAG_SENSOR_NAME_KEY, CONFIG_SENSOR_NAME);
	vediag_update_system_config("Common", VEDIAG_SENSOR_INTERVAL_KEY, "10");
#endif
#ifdef CONFIG_VEDIAG_STORAGE_TEST
	vediag_update_system_config("Common", get_section_type(VEDIAG_STORAGE), "0");
#endif
	vediag_update_system_config("Common", get_section_type(VEDIAG_NET), "0");
#ifdef CONFIG_VEDIAG_PCI_TEST
	vediag_update_system_config("Common", get_section_type(VEDIAG_PCIE), "0");
#endif
	vediag_update_system_config("Common", "autocmd", "diag all");
	vediag_update_system_config("Common", "autodelay", "5");
#ifdef CONFIG_RAS
	vediag_update_system_config("Common", "ras_level", "3");
#endif

	/* Scan device */
	vediag_scan_memory_device(1);
#ifdef CONFIG_VEDIAG_STORAGE_TEST
	vediag_scan_storage_device(1);
#endif
#ifdef CONFIG_VEDIAG_NET_TEST
	vediag_scan_network_device(1);
#endif
#ifdef CONFIG_VEDIAG_PCI_TEST
	vediag_scan_pci_device(1);
#endif
#ifdef CONFIG_VEDIAG_LED_TEST
	vediag_scan_led_device(1);
#endif
#ifdef CONFIG_VEDIAG_EVENT_TEST
	vediag_scan_event_device(1);
#endif
#ifdef CONFIG_VEDIAG_I2C_TEST
	vediag_scan_i2c_device(1);
#endif
#ifdef CONFIG_VEDIAG_AUDIO_TEST
	vediag_scan_audio_device(1);
#endif
#ifdef CONFIG_VEDIAG_MICRO_TEST
	vediag_scan_micro_device(1);
#endif
#ifdef CONFIG_VEDIAG_BLUETOOTH_TEST
	vediag_scan_bluetooth_device(1);
#endif
#ifdef CONFIG_VEDIAG_FIRMWARE
	vediag_scan_firmware_device(1);
#endif
#ifdef CONFIG_VEDIAG_ZWAVE_TEST
	vediag_scan_zwave_device(1);
#endif
#ifdef CONFIG_VEDIAG_ZIGBEE_TEST
	vediag_scan_zigbee_device(1);
#endif
}

int vediag_check_system_config(void)
{
	int fd;
	char file_path[128];

	memset(file_path, 0, sizeof(file_path));
	sprintf(file_path, "/etc/diagwrt/%s", VEDIAG_SYSTEM_CONFIG_FILE);
	memset(file_system, 0, sizeof(file_system));

	/* Check file exist */
	if (access(VEDIAG_SYSTEM_CONFIG_FILE, 0) == 0) {
		sprintf(file_system, "%s", VEDIAG_SYSTEM_CONFIG_FILE);
	} else if (access(file_path, 0) == 0) {
		sprintf(file_system, "/etc/diagwrt/%s", VEDIAG_SYSTEM_CONFIG_FILE);
	} else {
		sprintf(file_system, "%s", VEDIAG_SYSTEM_CONFIG_FILE);
		fd = open(VEDIAG_SYSTEM_CONFIG_FILE, O_RDWR|O_CREAT, 0777);
		if (fd != -1){
			close(fd);
			vediag_create_default_system();
		}
		else {
			xerror("Can't create '%s'\n", VEDIAG_SYSTEM_CONFIG_FILE);
			return -1;
		}	
	}

	/**
	 * file_system is exist but no section "Common" (maybe empty file )
	 */
	if (vediag_parse_config(file_system, "Common", "Test default") == NULL) {
		vediag_create_default_system();
	}

	return 0;
}

int vediag_check_test_config(void)
{
	int fd;
	char file_path[128];

	file_test_default = vediag_parse_system("Test default");
	if (!file_test_default) {
		xerror("Error: File system.ini 'Test default' not found \n");
		return -1;
	}

	memset(file_path, 0, sizeof(file_path));
	sprintf(file_path, "/etc/diagwrt/%s", file_test_default);

	/* Check file exist */
	if (access(file_test_default, 0) == 0) { /*First check in preset director */
		sprintf(file_testconfig, "%s", file_test_default);
	} else if (access(file_path, 0) == 0) { /*Second check in /etc/diagwrt/ */
		sprintf(file_testconfig, "/etc/diagwrt/%s", file_test_default);
	} else {
		xerror("Error: Can't find %s in both preset director and /etc/diagwrt/ path \n", file_test_default);
		fd = open(file_test_default, O_RDWR|O_CREAT, 0777);
		if (fd != -1)
			close(fd);
		else {
			xerror("Error: Can't create '%s' file\n", file_test_default);
			return -1;
		}
		sprintf(file_testconfig, "%s", file_test_default);
	}

	return 0;
}

void vediag_set_argument(char *key, char *value, struct list_head *arguments)
{
	struct vediag_test_argument *arg;

	arg = malloc(sizeof(struct vediag_test_argument));
	if (arg) {
		memset(arg, 0, sizeof(struct vediag_test_argument));
		if (key)
			arg->key = strdup(key);
		if (value)
			arg->value = strdup(value);
		list_add_tail(&arg->entry, arguments);
	}
}

int vediag_get_test_arguments(char *cfg_file, char *section, struct list_head *arguments)
{
	dictionary * dic;
	struct vediag_test_argument *arg;

    int     i, j ;
    char    keym[128];
    int     nsec ;
    char *secname ;
    int     seclen ;

    pthread_mutex_lock(&cfg_file_lock);
    dic = iniparser_load(cfg_file);
    pthread_mutex_unlock(&cfg_file_lock);
    nsec = iniparser_getnsec(dic);
    for (i=0 ; i<nsec ; i++) {
        secname = iniparser_getsecname(dic, i) ;
        if (strcasecmp(secname, section) == 0) {
			seclen  = (int)strlen(secname);
			sprintf(keym, "%s:", secname);
			for (j=0 ; j<dic->size ; j++) {
				if (dic->key[j]==NULL)
					continue ;
				if (!strncmp(dic->key[j], keym, seclen+1)) {
					if (dic->val[j]) {
						arg = malloc(sizeof(struct vediag_test_argument));
						if (arg) {
							memset(arg, 0, sizeof(struct vediag_test_argument));
							arg->key = strdup(dic->key[j]+seclen+1);
							arg->value = strdup(dic->val[j]);
							list_add_tail(&arg->entry, arguments);
						}
					}
				}
			}
        	goto exit;
        }
    }
exit:
	iniparser_freedict(dic);
	return 0;
}

int vediag_update_config(const char *cfg_file, const char *section, const char *key, char *value)
{
	dictionary * dic;
	char keym[128];
	FILE *file;

	/* Load dictionary */
	dic = iniparser_load((char*)cfg_file);
	if (dic == NULL) {
		xerror("cannot parse file [%s]", cfg_file);
		return -1;
	}

	/* Update key value */
	memset(keym, 0, sizeof(keym));
	if (key != NULL)
		sprintf(keym, "%s:%s", section, key);
	else
		sprintf(keym, "%s", section);
	iniparser_set(dic, keym, value);

	/* Re-write INI file */
	file = fopen(cfg_file, "w");
	if (file == NULL) {
	    xerror("could not open file [%s]", cfg_file);
		iniparser_freedict(dic);
		return -1;
	}

	iniparser_dump_ini(dic, file);
	fclose(file);
	iniparser_freedict(dic);

	return 0;
}

int vediag_update_system_config(const char *section, const char *key, char *value)
{
	return vediag_update_config(file_system, section, key, value);
}

int vediag_add_identified_device(int type, char *device, char *physical)
{
	char section[64], value[16];
	char *val = NULL;
	int cnt = 0;
	int dev;
	char *device_ori, *physical_ori;

	vediag_info( type, device, ": '%s'\n", physical);

	/* Find number of device */
	val = vediag_parse_system(get_section_type(type));
	if (val)
		cnt = strtoul(val, NULL, 0);
	else
		return 0;

	if(val)
		free(val);

	for (dev = 1; dev <= cnt; dev++) {
		memset(section, 0 , sizeof(section));
		sprintf(section, "%s%d", get_section_type(type), dev);
	    //val = vediag_parse_system_section(section, VEDIAG_DEVICE_KEY);
	    /* Update exist device */
	    device_ori = vediag_parse_system_section(section, VEDIAG_DEVICE_KEY);
	    physical_ori = vediag_parse_system_section(section, VEDIAG_PHYSICAL_KEY);
	    if (device_ori && strcasecmp(device_ori, device) == 0){
	    	if (!physical_ori || (strcasecmp(physical_ori, "Unknown") == 0)){
	    		return vediag_update_system_config(section, VEDIAG_PHYSICAL_KEY,
				physical ? physical : "Unknown");
	    	}
			if(device_ori)
				free(device_ori);
			if(physical_ori)
				free(physical_ori);

	    	return 0;
	    }
		if(device_ori)
			free(device_ori);
		if(physical_ori)
			free(physical_ori);
	}

	/*Add new device */
	cnt += 1;
	memset(value, 0 , sizeof(value));
	sprintf(value, "%d", cnt);
	vediag_update_system_config("COMMON", get_section_type(type), value);

	memset(section, 0 , sizeof(section));
	sprintf(section, "%s%d", get_section_type(type), cnt);
	vediag_update_system_config(section, NULL, NULL);
	vediag_update_system_config(section, VEDIAG_DEVICE_KEY, device);
	vediag_update_system_config(section, VEDIAG_PHYSICAL_KEY, physical);

	return 0;
}

char * vediag_parse_config(const char *cfg_file, const char *section, const char *key)
{
	dictionary * dic;
	char keym[128];
	char *ret = NULL;
	const char *val;

	pthread_mutex_lock(&cfg_file_lock);
	dic = iniparser_load((char*)cfg_file);
	if (dic == NULL) {
		xerror("Can't parse file [%s]\n", cfg_file);
		goto exit;
	}
	memset(keym, 0, sizeof(keym));
	sprintf(keym, "%s:%s", section, key);
	val = iniparser_getstring(dic, keym, NULL);
	if (val != NULL)
		ret = strdup(val);
	iniparser_freedict(dic);

exit:
	pthread_mutex_unlock(&cfg_file_lock);
	return ret;
}

char * vediag_parse_system(const char *key)
{
	return vediag_parse_config(file_system, "COMMON", key);
}

char * vediag_parse_system_section(const char *section, const char *key)
{
	return vediag_parse_config(file_system, section, key);
}

char * vediag_find_device(int dev_type, const char *physical_dev)
{
	int i, total_dev = 0;
	char *val;
	char section[64];

	val = vediag_parse_system(get_section_type(dev_type));
	if (val == NULL)
		return NULL;

	total_dev = strtoul(val, NULL, 0);
	if(val)
		free(val);

	for (i = 1; i <= total_dev; i++) {
		memset(section, 0 , sizeof(section));
		sprintf(section, "%s%d", get_section_type(dev_type), i);
		val = vediag_parse_system_section(section, VEDIAG_PHYSICAL_KEY);
		if( val != NULL && (strcasecmp(val, physical_dev) == 0))
			return vediag_parse_system_section(section, VEDIAG_DEVICE_KEY);
	}

	return NULL;
}

char * vediag_find_physical_device(int dev_type, const char *device)
{
	int i, total_dev = 0;
	char *val;
	char section[64];

	val = vediag_parse_system(get_section_type(dev_type));
	if (val == NULL)
		return NULL;

	total_dev = strtoul(val, NULL, 0);
	if(val)
		free(val);

	for (i = 1; i <= total_dev; i++) {
		memset(section, 0 , sizeof(section));
		sprintf(section, "%s%d", get_section_type(dev_type), i);
		val = vediag_parse_system_section(section, VEDIAG_DEVICE_KEY);
		if( val != NULL && (strncasecmp(val, device, strlen(val)) == 0))
			return vediag_parse_system_section(section, VEDIAG_PHYSICAL_KEY);
	}

	return NULL;
}

void vediag_scan_memory_device(int force)
{
	/* Force rescan storage devices */
	if (force)
		vediag_update_system_config("COMMON", get_section_type(VEDIAG_MEM), "0");
	vediag_add_identified_device(VEDIAG_MEM, "MEM", "DDR3");
}

void vediag_scan_storage_device(int force)
{
	char diskpath[60];
	char *phypath = NULL, *start = NULL, *end = NULL, *ptr = NULL;
	int total_dev = 0;
	FILE *devf;
	int line_num = 0;
	char *line;
	char buf[TEXT_LINE_SIZE], cmd[60];
	u32 i;

	/* Force rescan storage devices */
	if (force)
		vediag_update_system_config("COMMON", get_section_type(VEDIAG_STORAGE), "0");

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "lsblk -p -o name,type | grep 'disk'");
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			start = NULL;
			end = NULL;
			for(i = 0; i < strlen(line); i++) {
				ptr = line + i;
				if (strncmp(ptr, "/dev/", 5) == 0) {
					start = ptr;
					i = i + 5;
					continue;
				}
				if (start && (((ptr[0] < 'a') || (ptr[0] > 'z')) && 
					((ptr[0] < '0') || (ptr[0] > '9')))) {
					end = ptr;
					break;
				}

			}
			memset(diskpath, 0, sizeof(diskpath));
			strncpy(diskpath, start, end - start);
			vediag_debug(VEDIAG_STORAGE, NULL, "%d - %d --> '%s'\n", end, start, diskpath);

			phypath = vepath_alloc_n_get_phypath(diskpath);
	    	vediag_add_identified_device(VEDIAG_STORAGE, diskpath, phypath ? phypath : "Unknown");
	    	total_dev++;

	    	if(phypath)
	    		free(phypath);
		}
		pclose(devf);
	}
}

void vediag_scan_network_device(int force)
{
#define NETWORK_LOOPBACK_INTERFACE_NAME	"lo"
	struct ifaddrs *addrs,*tmp;
	int total_dev = 0;

	getifaddrs(&addrs);
	tmp = addrs;

	if (force)
		vediag_update_system_config("COMMON", get_section_type(VEDIAG_NET), "0");

	/* Re-scan network devices */
	while (tmp != NULL)
	{
	    if (tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_PACKET) {
			if (tmp->ifa_name
					&& strncmp(tmp->ifa_name, NETWORK_LOOPBACK_INTERFACE_NAME,
							2) == 0) {
				tmp = tmp->ifa_next;
				continue;
			}

			struct listbuff *hwname = get_hwname_of_eth_interface(tmp->ifa_name);
			if (hwname != NULL) {
				vediag_add_identified_device(VEDIAG_NET, tmp->ifa_name, (char *) hwname->buff);
				free_listbuff_list(&hwname);
			} else {
				char unknown_device[256];
				memset(unknown_device, 0, sizeof(unknown_device));
				sprintf(unknown_device, "Unknown-%s", tmp->ifa_name);
				vediag_add_identified_device(VEDIAG_NET, tmp->ifa_name, unknown_device);
			}

	    	total_dev++;
	    }
	    tmp = tmp->ifa_next;
	}

	freeifaddrs(addrs);
}

#ifdef CONFIG_VEDIAG_PCI_TEST
void vediag_scan_pci_device(int force)
{
#ifdef CONFIG_PCIUTILS
	vediag_pciutils_scan_devices(force);
#endif
}
#endif

#ifdef CONFIG_VEDIAG_LED_TEST
void vediag_scan_led_device(int force)
{
	char ledpath[60];
	char *phypath = NULL;
	int total_dev = 0;
	FILE *devf;
	int line_num = 0;
	char *line;
	char buf[TEXT_LINE_SIZE], cmd[60];

	/* Force rescan led devices */
	if (force)
		vediag_update_system_config("COMMON", get_section_type(VEDIAG_LED), "0");

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "ls -1 /sys/class/leds/");
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			memset(ledpath, 0, sizeof(ledpath));
			strncpy(ledpath, line, min(sizeof(ledpath), strlen(line)));

			/* Remove  LF, CR, CRLF, LFCR */
			ledpath[strcspn(ledpath, "\r\n")] = 0;

			vediag_debug(VEDIAG_LED, NULL, "%d - %d --> '%s'\n", 0, 0, ledpath);

			phypath = vepath_alloc_n_get_phypath(ledpath);
			if(phypath == NULL)
				continue;
			strcat(phypath, "_");
			strcat(phypath, ledpath);
			vediag_add_identified_device(VEDIAG_LED, ledpath, phypath);
			total_dev++;
			free(phypath);
		}
		pclose(devf);
	}
}
#endif

#ifdef CONFIG_VEDIAG_EVENT_TEST
#define CONFIG_VEDIAG_EVENT_GPIO_ONLY

void vediag_scan_event_device(int force)
{
	char *phypath = NULL;
	int total_event = 0;
	FILE *devf;
	int line_num = 0;
	char *line, *word, *type, *exttype;
	char buf[TEXT_LINE_SIZE], cmd[60];

	/* Force rescan led devices */
	if (force)
		vediag_update_system_config("COMMON", get_section_type(VEDIAG_EVENT), "0");

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "cat /proc/interrupts");
	//vediag_debug(VEDIAG_EVENT, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			/* Check if first word is not format 'dddd:' */
			word = get_word_in_string(line, " ", 0);
			//vediag_debug(VEDIAG_EVENT, "vediag_list_event_device", "word#0: '%s'\n", word);
			if ((word == NULL) || (check_string_isdigit(word, strlen(word) - 1) == 0)) {
				free(word);
				continue;
			}
			free(word);

#ifdef CONFIG_VEDIAG_EVENT_GPIO_ONLY
			/* Check type GPIO */
			type = get_word_in_string(line, " ", 2);
			//vediag_debug(VEDIAG_EVENT, "vediag_list_event_device", "type#2: '%s'\n", type);
			/* If event no name */
			if (type == NULL)
				continue;

			if (strcmp(type, "GPIO") != 0) {
				/* Kernel 3.18.20:
				 *  29:          0  44e07000.gpio   6  mmc0
				 *  42:         22  44e07000.gpio  19  Motion_sensor
				 *  91:          0  481ac000.gpio   2  Restore_button
				 *  94:          0  481ac000.gpio   5  WPS_button
				 */
				exttype = get_word_in_string(type, ".", 1);
				if (exttype == NULL)
					continue;

				if (strcmp(exttype, "gpio") != 0) {
					free(exttype);
					continue;
				}
				free(exttype);
			}
			free(type);
#endif
			/* Get event name */
			word = get_word_in_string(line, " ", 4);
			//vediag_debug(VEDIAG_EVENT, "vediag_list_event_device", "word#4: '%s'\n", word);
			/* If event no name */
			if(word == NULL)
				continue;

			/* Remove  LF, CR, CRLF, LFCR */
			word[strcspn(word, "\r\n")] = 0;

			//vediag_debug(VEDIAG_EVENT, "", "%s", word);

			phypath = (char*) calloc(VEDIAG_MAX_STR_ARG_SIZE, sizeof(char));
			if (!phypath) {
				xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
				return;
			}
			strcat(phypath, "IRQ_");
#ifdef CONFIG_VEDIAG_EVENT_GPIO_ONLY
			strcat(phypath, "GPIO_");
#endif
			strcat(phypath, word);
			vediag_add_identified_device(VEDIAG_EVENT, word, phypath);
			free(phypath);
			free(word);
			total_event++;
		}
		pclose(devf);
		vediag_debug(VEDIAG_EVENT, "", "Found : %d\n", total_event);
	}
}
#endif

#ifdef CONFIG_VEDIAG_I2C_TEST
void vediag_scan_i2c_device(int force)
{
	char *phypath = NULL;
	int total_i2c = 0;
	FILE *devf;
	int line_num = 0, i;
	char *line, i2cdev[60], *dev;
	char buf[TEXT_LINE_SIZE], cmd[60], i2cfound[TEXT_LINE_SIZE];

	/* Force rescan led devices */
	if (force)
		vediag_update_system_config("COMMON", get_section_type(VEDIAG_I2C), "0");

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "ls -1 /dev/i2c*");
	vediag_debug(VEDIAG_I2C, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			/* Remove  LF, CR, CRLF, LFCR */
			line[strcspn(line, "\r\n")] = 0;

			memset(i2cdev, 0, sizeof(i2cdev));
			strncpy( i2cdev, line, min(sizeof(i2cdev), strlen(line)));
			vediag_debug(VEDIAG_I2C, i2cdev, "found!\n");

			total_i2c = i2c_probe( i2cdev, i2cfound);
			vediag_debug(VEDIAG_I2C, i2cdev, "found : %d\n", total_i2c);

			phypath = (char*) calloc(VEDIAG_MAX_STR_ARG_SIZE, sizeof(char));
			if (!phypath) {
				xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
				return;
			}
			for (i = 0; i < total_i2c; i++) {
				/* Clear string */
				memset( phypath, 0, VEDIAG_MAX_STR_ARG_SIZE * sizeof(char));

				/* '/dev/'i2c* */
				strcat(phypath, i2cdev+5);
				strcat(phypath, "_");
				/* Get each device */
				dev = get_word_in_string(i2cfound, " ", i);
				if (dev == NULL)
					break;
				strcat(phypath, dev);
				free(dev);

				vediag_debug(VEDIAG_I2C, i2cdev, "physical : %s\n", phypath);
				vediag_add_identified_device(VEDIAG_I2C, phypath, phypath);
			}
			free(phypath);
		}
		pclose(devf);

	}
}
#endif

#ifdef CONFIG_VEDIAG_AUDIO_TEST
void vediag_scan_audio_device(int force)
{
	char *phypath = NULL;
	int total_audio = 0;
	FILE *devf;
	int line_num = 0;
	char *line, *card, *dev, *ptr;
	char buf[TEXT_LINE_SIZE], cmd[60];

	/* Force rescan devices */
	if (force)
		vediag_update_system_config("COMMON", get_section_type(VEDIAG_AUDIO), "0");

	/*
	 * $ aplay -l
	 * **** List of PLAYBACK Hardware Devices ****
	 * card 0: AudioPCI [Ensoniq AudioPCI], device 0: ES1371/1 [ES1371 DAC2/ADC]
	 *   Subdevices: 1/1
	 *   Subdevice #0: subdevice #0
	 * card 0: AudioPCI [Ensoniq AudioPCI], device 1: ES1371/2 [ES1371 DAC1]
	 *   Subdevices: 1/1
	 *   Subdevice #0: subdevice #0
	 *
	 * ==> hw:CARD=AudioPCI,DEV=0
	 */
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "aplay -l");
	vediag_debug(VEDIAG_AUDIO, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			/* Check if first word named 'card' */
			card = get_word_in_string(line, " ", 0);
			//vediag_debug(VEDIAG_EVENT, "vediag_list_event_device", "word#0: '%s'\n", card);
			if ((card == NULL) || (strcmp(card, "card") != 0)) {
				free(card);
				continue;
			}
			free(card);

			/* Get card name */
			card = get_word_in_string(line, " ", 2);
			//vediag_debug(VEDIAG_EVENT, "vediag_list_event_device", "word#0: '%s'\n", card);
			if (card == NULL) {
				continue;
			}
			/* Get device number */
			ptr = get_word_in_string(line, "device ", 1);
			if (ptr == NULL) {
				continue;
			}
			dev = get_word_in_string(ptr, " ", 0);
			//vediag_debug(VEDIAG_EVENT, "vediag_list_event_device", "word#0: '%s'\n", card);
			if (dev == NULL) {
				free(ptr);
				continue;
			}
			free(ptr);
			/* Remove ":" */
			dev[strlen(dev)-1] = '\0';

			phypath = (char*) calloc(VEDIAG_MAX_STR_ARG_SIZE, sizeof(char));
			if (!phypath) {
				xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
				free(card);
				free(dev);
				return;
			}
			/* ==> 'hw:CARD=AudioPCI,DEV=0' */
			strcat(phypath, "hw:CARD=");
			strcat(phypath, card);
			strcat(phypath, ",DEV=");
			strcat(phypath, dev);
			/* Free pointer */
			free(card);
			free(dev);

			vediag_debug(VEDIAG_AUDIO, "Found:", "'%s'\n", phypath);
			vediag_add_identified_device(VEDIAG_AUDIO, phypath, phypath);
			/* Free pointer */
			free(phypath);
			total_audio++;
		}
		pclose(devf);
		vediag_debug(VEDIAG_AUDIO, "Total", "found : %d\n", total_audio);
	}
}
#endif

#ifdef CONFIG_VEDIAG_MICRO_TEST
void vediag_scan_micro_device(int force)
{
	char *phypath = NULL;
	int total_micro = 0;
	FILE *devf;
	int line_num = 0;
	char *line, *card, *dev, *ptr;
	char buf[TEXT_LINE_SIZE], cmd[60];

	/* Force rescan devices */
	if (force)
		vediag_update_system_config("COMMON", get_section_type(VEDIAG_MICRO), "0");

	/*
	 * $ arecord -l
	 * **** List of CAPTURE Hardware Devices ****
	 * card 0: AudioPCI [Ensoniq AudioPCI], device 0: ES1371/1 [ES1371 DAC2/ADC]
	 *   Subdevices: 1/1
	 *   Subdevice #0: subdevice #0
	 */
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "arecord -l");
	vediag_debug(VEDIAG_MICRO, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			/* Check if first word named 'card' */
			card = get_word_in_string(line, " ", 0);
			//vediag_debug(VEDIAG_MICRO, "vediag_list_event_device", "word#0: '%s'\n", card);
			if ((card == NULL) || (strcmp(card, "card") != 0)) {
				free(card);
				continue;
			}
			free(card);

			/* Get card name */
			card = get_word_in_string(line, " ", 2);
			//vediag_debug(VEDIAG_MICRO, "vediag_list_event_device", "word#0: '%s'\n", card);
			if (card == NULL) {
				continue;
			}
			/* Get device number */
			ptr = get_word_in_string(line, "device ", 1);
			if (ptr == NULL) {
				continue;
			}
			dev = get_word_in_string(ptr, " ", 0);
			//vediag_debug(VEDIAG_MICRO, "vediag_list_event_device", "word#0: '%s'\n", card);
			if (dev == NULL) {
				free(ptr);
				continue;
			}
			free(ptr);
			/* Remove ":" */
			dev[strlen(dev)-1] = '\0';

			phypath = (char*) calloc(VEDIAG_MAX_STR_ARG_SIZE, sizeof(char));
			if (!phypath) {
				xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
				free(card);
				free(dev);
				return;
			}
			/* ==> 'hw:CARD=AudioPCI,DEV=0' */
			strcat(phypath, "hw:CARD=");
			strcat(phypath, card);
			strcat(phypath, ",DEV=");
			strcat(phypath, dev);
			/* Free pointer */
			free(card);
			free(dev);

			vediag_debug(VEDIAG_MICRO, "Found:", "'%s'\n", phypath);
			vediag_add_identified_device(VEDIAG_MICRO, phypath, phypath);
			/* Free pointer */
			free(phypath);
			total_micro++;
		}
		pclose(devf);
		vediag_debug(VEDIAG_MICRO, "Total", "found : %d\n", total_micro);
	}
}
#endif

#ifdef CONFIG_VEDIAG_BLUETOOTH_TEST
void vediag_scan_bluetooth_device(int force)
{
	int bt_num = 0, total_bt = 0;
	FILE *devf;
	int line_num = 0, ret = 0;
	char *line;
	char buf[TEXT_LINE_SIZE], cmd[60], device[60], addr[60];

	/* Force rescan devices */
	if (force)
		vediag_update_system_config("COMMON", get_section_type(VEDIAG_BLUETOOTH), "0");

	/*
	 * hcitool dev
	 * Devices:
	 *         hci0    20:C3:8F:81:C8:8C
	 */
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "hcitool dev");
	vediag_debug(VEDIAG_BLUETOOTH, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			//vediag_debug(VEDIAG_BLUETOOTH, "Line", "%d: '%s'\n", line_num, line);
			memset(addr, 0, sizeof(addr));
			ret = sscanf(line, "    hci%d    %s", &bt_num, addr);
			if(ret < 2) {
				//vediag_debug(VEDIAG_BLUETOOTH, "", "%d: '%s' ret(%d)\n", bt_num, addr, ret);
				continue;
			}
			//vediag_debug(VEDIAG_BLUETOOTH, "Found:", "hci%d addr='%s'\n", bt_num, addr);

			/* ==> device=hci0, physical=20:C3:8F:81:C8:8C */
			memset(device, 0, sizeof(device));
			sprintf(device, "hci%d", bt_num);
			vediag_debug(VEDIAG_BLUETOOTH, "Found:", "%s, addr='%s'\n", device, addr);
			vediag_add_identified_device(VEDIAG_BLUETOOTH, device, addr);
			total_bt++;
		}
		pclose(devf);
		vediag_debug(VEDIAG_BLUETOOTH, "Total", "found : %d\n", total_bt);
	}
}
#endif

#ifdef CONFIG_VEDIAG_FIRMWARE
void vediag_scan_firmware_device(int force)
{
	/* Force rescan devices */
	if (force)
		vediag_update_system_config("COMMON", get_section_type(VEDIAG_FIRMWARE), "0");

	vediag_add_identified_device(VEDIAG_FIRMWARE, "fw0", "fw0");
}
#endif

#ifdef CONFIG_VEDIAG_ZWAVE_TEST
void vediag_scan_zwave_device(int force)
{
	int total_zw = 0;
	FILE *devf;
	int line_num = 0, ret = 0;
	char *line;
	char buf[TEXT_LINE_SIZE], cmd[60], homedevice[60], homeserial[60];
	unsigned int homeid, nodeid;

	/* Force rescan devices */
	if (force)
		vediag_update_system_config("COMMON", get_section_type(VEDIAG_ZWAVE), "0");

	/*
	 * zwave_tools -l
	 *
	 * Z-Wave ZWave Controller Static based serial API found
	 * List Node
	 * SUC ID 000
	 * Device HomeID F4D86B7D, NodeID 001
	 * Device is Primary Controller, (Real primary), (No nodes)
	 * Node 001 TRUE  00 Static Controller
	 * Listening nodes registered 0
	 * Node 0 NodeID=001 Type=Static Controller
	 * pass
	 *
	 */
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "zwave_tools -l");
	vediag_debug(VEDIAG_ZWAVE, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			//vediag_debug(VEDIAG_ZWAVE, "Line", "%d: '%s'\n", line_num, line);
			ret = sscanf(line, "Device HomeID %08X, NodeID %03X", &homeid, &nodeid);
			if(ret < 2) {
				//vediag_debug(VEDIAG_ZWAVE, "", "%d: '%s' ret(%d)\n", bt_num, addr, ret);
				continue;
			}
			vediag_debug(VEDIAG_ZWAVE, "Found:", "HomeID %08X, NodeID %03X\n", homeid, nodeid);

			/* ==> device=HomeID1, physical=F4D86B7D */
			memset(homedevice, 0, sizeof(homedevice));
			sprintf(homedevice, "HomeID%X", nodeid);
			memset(homeserial, 0, sizeof(homeserial));
			sprintf(homeserial, "%08X", homeid);

			vediag_debug(VEDIAG_ZWAVE, "Found:", "%s, %s\n", homedevice, homeserial);
			vediag_add_identified_device(VEDIAG_ZWAVE, homedevice, homeserial);
			total_zw++;
		}
		pclose(devf);
		vediag_debug(VEDIAG_ZWAVE, "Total", "found : %d\n", total_zw);
	}
}
#endif

#ifdef CONFIG_VEDIAG_ZIGBEE_TEST
void vediag_scan_zigbee_device(int force)
{
	int total_zb = 0;
	FILE *devf;
	int line_num = 0, ret = 0;
	char *line;
	char buf[TEXT_LINE_SIZE], cmd[60], homedevice[60], homeserial[60];
	unsigned int homeid, shortid;

	/* Force rescan devices */
	if (force)
		vediag_update_system_config("COMMON", get_section_type(VEDIAG_ZIGBEE), "0");

	/*
	 * zigbee_tools -w
	 * Reset info: 11 (SOFTWARE)
	 * ezsp ver 0x05 stack type 0x02 stack ver. [5.10.1 GA build 80]
	 * Ezsp Config: set source route table size to 0x00FA:Success: set
	 * Ezsp Config: set security level to 0x0005:Success: set
	 * Ezsp Config: set address table size to 0x0002:Success: set
	 * Ezsp Config: set TC addr cache to 0x0002:Success: set
	 * Ezsp Config: set stack profile to 0x0002:Success: set
	 * Ezsp Config: set MAC indirect TX timeout to 0x1E00:Success: set
	 * Ezsp Config: set max hops to 0x001E:Success: set
	 * Ezsp Config: set tx power mode to 0x8000:Success: set
	 * Ezsp Config: set supported networks to 0x0001:Success: set
	 * Ezsp Policy: set binding modify to "allow for valid endpoints & clusters only":Success: set
	 * Ezsp Policy: set message content in msgSent to "return":Success: set
	 * Ezsp Value : set maximum incoming transfer size to 0x00000052:Success: set
	 * Ezsp Value : set maximum outgoing transfer size to 0x00000052:Success: set
	 * Ezsp Config: set binding table size to 0x0010:Success: set
	 * Ezsp Config: set key table size to 0x0000:Success: set
	 * Ezsp Config: set max end device children to 0x0020:Success: set
	 * NCP supports maxing out packet buffers
	 * Ezsp Config: set packet buffers to 255
	 * Ezsp Config: set end device poll timeout to 0x0005:Success: set
	 * Ezsp Config: set end device poll timeout shift to 0x0006:Success: set
	 * Ezsp Config: set zll group addresses to 0x0000:Success: set
	 * Ezsp Config: set zll rssi threshold to 0xFF80:Success: set
	 * Ezsp Config: set transient key timeout to 0x0078:Success: set
	 * Ezsp Endpoint 1 added, profile 0x0104, in clusters: 3, out clusters 10
	 * Ezsp Endpoint 242 added, profile 0xA1E0, in clusters: 1, out clusters 1
	 * ......................................................
	 * ...........waiting for program do something...........
	 * EMBER_NETWORK_UP 0x0000
	 * Short ID: 0x0000, EUI64: (>)000B57FFFEA13243, Pan ID: 0xCBC7
	 *
	 */
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "zigbee_tools -w");
	vediag_debug(VEDIAG_ZIGBEE, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			//vediag_debug(VEDIAG_ZIGBEE, "Line", "%d: '%s'\n", line_num, line);
			ret = sscanf(line, "Short ID: 0x%x, EUI64: (>)%*[A-Z0-9], Pan ID: 0x%x", &shortid, &homeid);
			if(ret < 2) {
				//vediag_debug(VEDIAG_ZIGBEE, "", "%d: '%s' ret(%d)\n", bt_num, addr, ret);
				continue;
			}
			vediag_debug(VEDIAG_ZIGBEE, "Found:", "ShortID %X, HomeID %08X\n", shortid, homeid);

			/* ==> device=HomeID0, physical=F4D86B7D */
			memset(homedevice, 0, sizeof(homedevice));
			sprintf(homedevice, "HomeID%X", shortid);
			memset(homeserial, 0, sizeof(homeserial));
			sprintf(homeserial, "%08X", homeid);

			vediag_debug(VEDIAG_ZIGBEE, "Found:", "%s, %s\n", homedevice, homeserial);
			vediag_add_identified_device(VEDIAG_ZIGBEE, homedevice, homeserial);
			total_zb++;
		}
		pclose(devf);
		vediag_debug(VEDIAG_ZIGBEE, "Total", "found : %d\n", total_zb);
	}
}
#endif

arch_initcall(scan_devices)
{
	int force = 1;
	int fd;
	char file_path[128];
	
	xinfo("Scan devices...\n");

	memset(file_path, 0, sizeof(file_path));
	sprintf(file_path, "/etc/diagwrt/%s", VEDIAG_SYSTEM_CONFIG_FILE);
	memset(file_system, 0, sizeof(file_system));

	/* Check file exist */
	if (access(VEDIAG_SYSTEM_CONFIG_FILE, 0) == 0) {
		sprintf(file_system, "%s", VEDIAG_SYSTEM_CONFIG_FILE);
	} else if (access(file_path, 0) == 0) {
		sprintf(file_system, "/etc/diagwrt/%s", VEDIAG_SYSTEM_CONFIG_FILE);
	} else {
		sprintf(file_system, "%s", VEDIAG_SYSTEM_CONFIG_FILE);
		fd = open(VEDIAG_SYSTEM_CONFIG_FILE, O_RDWR|O_CREAT, 0777);
		if (fd != -1) {
			close(fd);
			vediag_create_default_system();
		}
		else {
			xerror("Can't create '%s'\n", VEDIAG_SYSTEM_CONFIG_FILE);
			return -1;
		}
	}

	vediag_scan_memory_device(force);
#ifdef CONFIG_VEDIAG_STORAGE_TEST
	vediag_scan_storage_device(force);
#endif
#ifdef CONFIG_VEDIAG_NET_TEST
	vediag_scan_network_device(force);
#endif
#ifdef CONFIG_VEDIAG_PCI_TEST
	vediag_scan_pci_device(force);
#endif
#ifdef CONFIG_VEDIAG_LED_TEST
	vediag_scan_led_device(force);
#endif
#ifdef CONFIG_VEDIAG_EVENT_TEST
	vediag_scan_event_device(force);
#endif
#ifdef CONFIG_VEDIAG_I2C_TEST
	vediag_scan_i2c_device(force);
#endif
#ifdef CONFIG_VEDIAG_AUDIO_TEST
	vediag_scan_audio_device(force);
#endif
#ifdef CONFIG_VEDIAG_MICRO_TEST
	vediag_scan_micro_device(force);
#endif
#ifdef CONFIG_VEDIAG_BLUETOOTH_TEST
	vediag_scan_bluetooth_device(force);
#endif
#ifdef CONFIG_VEDIAG_FIRMWARE
	vediag_scan_firmware_device(force);
#endif
#ifdef CONFIG_VEDIAG_ZWAVE_TEST
	vediag_scan_zwave_device(force);
#endif
#ifdef CONFIG_VEDIAG_ZIGBEE_TEST
	vediag_scan_zigbee_device(force);
#endif
	return 0;
}

/*arch_initcall(stop_chrony_service)
{
	int ret = 0;

	xinfo("Stop Chrony Service...");
	ret = system("sudo systemctl stop chronyd.service");
	if (ret == 0) {
		xinfo("done\n");
	} else {
		xinfo("fail\n");
	}

	return ret;
}*/
