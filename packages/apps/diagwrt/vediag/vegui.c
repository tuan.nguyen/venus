
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <cmd.h>
#include <string.h>
#include <graphic.h>
#include <vediag_common.h>
#include <pthread.h>
#include <sys/time.h>
#include <linux/err.h>
#include <linux/bug.h>
#include <errno.h>
#include <log.h>
#include <readline/readline.h>

/*
 * VED test GUI
 */
#define VEDIAG_GUI_WATCHDOG			30		/* seconds */
#define VEDIAG_STAT_TIMING			2000/* ms */

#define CMD_VEDIAG_STAT_NOTHING		0
#define CMD_VEDIAG_STAT_START		1
#define CMD_VEDIAG_STAT_STOP			2

#define VEDIAG_STAT_STATE_IDLE		0
#define VEDIAG_STAT_STATE_RUN		1

#define VEDIAG_GUI_IDLE		0
#define VEDIAG_GUI_RUNNING	1

enum {
	GUI_OFF = 0,
	GUI_ON,
	GUI_CMD,
};

extern void printk_set_enable(int enable);
int vediag_gui_enable = 0;

struct vediag_gui_thread_data {
	int cmd;
	pthread_mutex_t cmd_ready_mutex;
	pthread_cond_t cmd_ready;
	pthread_mutex_t cmd_complete_mutex;
	pthread_cond_t cmd_complete;
	pthread_t gui_ctrl;
	char *board;
	char *sensor;
	int total_line;
	int gui_ctrl_state;
	int stdout;
	int logfile;
	int watchdog;
	int show_completed;
	int gui_state;
};

static struct vediag_gui_thread_data *vediag_gui_ctrl;

static LIST_HEAD(vediag_gui_entry_list);
static pthread_mutex_t gui_entry_list_mutex = PTHREAD_MUTEX_INITIALIZER;

static pthread_mutex_t vediag_gui_mutex = PTHREAD_MUTEX_INITIALIZER;

struct vediag_gui_entry {
	u16 mtid;
	u16 stid;
	int line;
	int id;
	int threads;
	char name[17];
	char detail[17];
	char app[9];
	char state[17];
	char result_detail[25];
	int result;
	int percent;
	int runtime;
	int completed;
	struct list_head list;
	struct list_head child;
	pthread_mutex_t child_mutex;
	int finish;
};

#define for_each_of_gui_entry(x)	for(x = vediag_gui_entry_list.next ; x != &vediag_gui_entry_list; x = x->next)

struct vediag_gui_report {
	int type;
	void *report;
	struct list_head list;
};

static LIST_HEAD(vediag_gui_report_list);
static pthread_mutex_t gui_report_list_mutex = PTHREAD_MUTEX_INITIALIZER;

#define VEDIAG_GUI_CONSOLE_LINE			30

int gprintf(const char *format, ...)
{
	int rc = 0;
    va_list args;
    if (vediag_gui_ctrl->stdout >= 0) {
    	va_start(args, format);
		FILE *output = fdopen(vediag_gui_ctrl->stdout, "w");
		if (output) {
			rc = vfprintf(output, format, args);
			fflush(output);
		}
		va_end(args);
    }

    return rc;
}

void vediag_gui_console_lock(void)
{
	pthread_mutex_lock(&vediag_gui_mutex);
}

void vediag_gui_console_unlock(void)
{
	pthread_mutex_unlock(&vediag_gui_mutex);
}

int vediag_gui_add_report(int type, void *report)
{
	struct vediag_gui_report *new;
	new = malloc(sizeof(struct vediag_gui_report));
	if(IS_ERR(new)) {
		xerror("System error: Failed to alloc report request (%s)\n",__func__);
		return -1;
	}
	new->type = type;
	if(type == VEDIAG_SYSTEM_REPORT) {
		new->report = malloc(sizeof(struct vediag_system_report));
		if(IS_ERR(new->report)) {
			xerror("System error: Failed to alloc report (%s)\n",__func__);
			return -1;
		}
		memcpy(new->report, report, sizeof(struct vediag_system_report));
	}
	else {
		new->report = report;
	}
	pthread_mutex_lock(&gui_report_list_mutex);
	list_add_tail(&new->list, &vediag_gui_report_list);
	pthread_mutex_unlock(&gui_report_list_mutex);

	return 0;
}

static void vediag_gui_print_time(int runtimes)
{
	int days = 0, hours = 0, minutes = 0, seconds = 0;
	int temp = runtimes;
	int max = 0;

	if(temp >= 86400) {
		days = temp / 86400;
		temp %= 86400;
		max = 3;
	}

	if(temp >= 3600) {
		hours = temp / 3600;
		temp %= 3600;
		if(max == 0)
			max = 2;
	}

	if(temp >= 60) {
		minutes = temp / 60;
		temp %= 60;
		if(max == 0)
			max = 1;
	}

	seconds = temp;
	switch(max) {
	case 3:
		gprintf("%*d:%*d:%*d:%*d",
				2, days,
				2, hours,
				2, minutes,
				2, seconds);
		break;
	case 2:
		gprintf("%*d:%*d:%*d",
				5, hours,
				2, minutes,
				2, seconds);
		break;
	case 1:
		gprintf("%*d:%*d",
				8, minutes,
				2, seconds);
		break;
	case 0:
		gprintf("%*d",
				11, seconds);
		break;
	default:
		break;
	}
}

static void vediag_gui_print_percent(int percent)
{
	int i;

	set_graphic_mode(GRAPH_BACKGROUND_COLOR_GREEN);
	for(i = 0; i < ((percent * 20) / 100); i++) {
		if((percent == 100) && (i == 9)) {
			gprintf("%d", percent / 10);
			i++;
			continue;
		}
		else if(i == 10){
			gprintf("%d", percent / 10);
			continue;
		}
		if(i == 11){
			gprintf("%d", percent % 10);
			continue;
		}
		if(i == 12){
			gprintf("%%");
			continue;
		}
		gprintf(" ");
	}

	set_graphic_mode(GRAPH_BACKGROUND_COLOR_YELLOW);
	for(i = ((percent * 20) / 100); i < 20; i++) {
		if(i == 10){
			gprintf("%d", percent / 10);
			continue;
		}
		if(i == 11){
			gprintf("%d", percent % 10);
			continue;
		}
		if(i == 12){
			gprintf("%%");
			continue;
		}
		gprintf(" ");
	}
	set_graphic_mode(GRAPH_ALL_ATTRIBUTE_OFF);
}

void vediag_gui_get_size(char *str, const char *pre, unsigned long long size)
{
	unsigned long m = 0, n;
	unsigned long long f;
	static const char names[] = {'E', 'P', 'T', 'G', 'M', 'K'};
	unsigned long d = 10 * ARRAY_SIZE(names);
	char c = 0;
	unsigned int i;

	for (i = 0; i < ARRAY_SIZE(names); i++, d -= 10) {
		if (size >> d) {
			c = names[i];
			break;
		}
	}

	if (!c) {
		if (pre)
			sprintf(str, "%s %lluB", pre, size);
		else
			sprintf(str, "%lluB", size);
		return;
	}

	n = size >> d;
	f = size & ((1ULL << d) - 1);

	/* If there's a remainder, deal with it */
	if (f) {
		m = (10ULL * f + (1ULL << (d - 1))) >> d;

		if (m >= 10) {
			m -= 10;
			n += 1;
		}
	}

	if (m) {
		if (pre)
			sprintf(str, "%s %lu.%ld%ciB", pre, n, m, c);
		else
			sprintf(str, "%lu.%ld%ciB", n, m, c);
	} else {
		if (pre)
			sprintf(str, "%s %lu%ciB", pre, n, c);
		else
			sprintf(str, "%lu%ciB", n, c);
	}
}

#ifdef CONFIG_VEDIAG_SENSORS
extern void vediag_sensor_print(const char *name, int print);
static void vediag_gui_update_sensors(void)
{
	if (vediag_gui_ctrl->sensor != NULL)
		vediag_sensor_print(vediag_gui_ctrl->sensor, 1);
}
#endif
static void vediag_gui_update_common(void)
{
	vediag_gui_console_lock();

	set_cursor_position(1,0);
	erase_line();
	gprintf("Board: %s - ", vediag_gui_ctrl->board ? vediag_gui_ctrl->board : "Unknown");
	fflush(stdout);
#ifdef CONFIG_VEDIAG_SENSORS
	vediag_gui_update_sensors();
#endif
	fflush(stdout);

	vediag_gui_console_unlock();
}

static void vediag_gui_print_entry(struct vediag_gui_entry *entry)
{
	set_cursor_position(entry->line + 2,0);
	erase_line();
	if(entry->result == VEDIAG_STOP)
		set_graphic_mode(GRAPH_FOREGROUND_COLOR_YELLOW);
	else if (entry->result != VEDIAG_NOERR)
		set_graphic_mode(GRAPH_FOREGROUND_COLOR_RED);
	gprintf("%2d.%-2d %-16s %-16s ",
			entry->stid,
			entry->id,
			entry->name,
			entry->detail);
	vediag_gui_print_time(entry->runtime);
	if (entry->threads > 0)
		gprintf(" %2d", entry->threads);
	else
		gprintf("   ");
	gprintf(" %-8s",
			entry->app ? entry->app : (entry->id > 0 ? "unknown" : ""));
	gprintf(" %-8s ",
			entry->state);
	if (entry->result != VEDIAG_NOERR)
		set_graphic_mode(GRAPH_ALL_ATTRIBUTE_OFF);

	vediag_gui_print_percent(entry->percent);
	if(entry->result == VEDIAG_STOP)
		set_graphic_mode(GRAPH_FOREGROUND_COLOR_YELLOW);
	else if (entry->result != VEDIAG_NOERR)
		set_graphic_mode(GRAPH_FOREGROUND_COLOR_RED);
	gprintf(" %s \n", entry->result_detail);
	if (entry->result != VEDIAG_NOERR)
		set_graphic_mode(GRAPH_ALL_ATTRIBUTE_OFF);
}

static void vediag_gui_refresh(int show_complete)
{
	struct vediag_gui_entry *entry, *child;

	pthread_mutex_lock(&gui_entry_list_mutex);

	vediag_gui_console_lock();
	erase_display();
//	save_cursor_position();
	set_cursor_position(2,0);
	set_graphic_mode(GRAPH_FOREGROUND_COLOR_BLUE);
	gprintf("%s %-16s %-16s %*s %-2s %-8s %-8s %-20s %s\n",
			" IDX ",
			"Name",
			"Description",
			11, "RunTime",
			" T",
			"App",
			"Detail",
			"Progress",
			"Result");
	set_graphic_mode(GRAPH_ALL_ATTRIBUTE_OFF);

	vediag_gui_ctrl->total_line = 0;
	list_for_each_entry(entry, &vediag_gui_entry_list, list){
		/* Print status */
		if(entry) {
			if (entry->completed == 1 && show_complete == 0)
				continue;

			pthread_mutex_lock(&entry->child_mutex);
			entry->line = vediag_gui_ctrl->total_line + 1;
			vediag_gui_ctrl->total_line ++;
			vediag_gui_print_entry(entry);
			list_for_each_entry(child, &entry->child, list){
				if(child) {
					child->line = vediag_gui_ctrl->total_line + 1;
					vediag_gui_ctrl->total_line ++;
					vediag_gui_print_entry(child);
				}
			}
			pthread_mutex_unlock(&entry->child_mutex);
		}
	}
//	restore_cursor_position();
	vediag_gui_console_unlock();
	pthread_mutex_unlock(&gui_entry_list_mutex);
}

void vediag_gui_restore(void)
{
	int i;
	vediag_gui_console_lock();
//	save_cursor_position();
	for(i = 0; i < VEDIAG_GUI_CONSOLE_LINE; i++){
		set_cursor_position(i,0);
		erase_line();
	}
	vediag_gui_console_unlock();

	vediag_gui_update_common();

	vediag_gui_refresh(vediag_gui_ctrl->show_completed);
}

static struct vediag_gui_entry *vediag_gui_find_entry(u16 stid)
{
	struct vediag_gui_entry *entry;

	pthread_mutex_lock(&gui_entry_list_mutex);
	list_for_each_entry(entry, &vediag_gui_entry_list, list){
		if (entry->stid == stid){
			pthread_mutex_unlock(&gui_entry_list_mutex);
			return entry;
		}
	}
	pthread_mutex_unlock(&gui_entry_list_mutex);

	return NULL;
}

static struct vediag_gui_entry *vediag_gui_find_child(struct vediag_gui_entry *parent, int index)
{
	struct vediag_gui_entry *entry;

	list_for_each_entry(entry, &parent->child, list){
		if (entry->id == index){
			return entry;
		}
	}

	return NULL;
}

static void vediag_report_get_state(char *str, int state, u64 size)
{
	switch (state) {
	case VEDIAG_REPORT_SCAN:
#if 0
		if(size >= 0) {
			if(size < 10) {
				sprintf(str,"Scan %lld dev(s)", size);
			}
			else {
				vediag_gui_get_size(str, "Scan", size);
			}
		} else {
			sprintf(str,"Scan");
		}
#else
		sprintf(str," ");
#endif
		break;
	case VEDIAG_REPORT_READ_WRITE:
		if(size > 0) {
			vediag_gui_get_size(str, NULL, size);
		}
		else {
			sprintf(str, " ");
		}
		break;
	default:
		sprintf(str,"Unknown");
		break;
	}
}

const char *vediag_report_get_result(int result)
{
	switch (result) {
	case VEDIAG_REPORT_NOT_START:
		return "Waiting";
		break;
	case VEDIAG_REPORT_EXCUTING:
		return "Running";
		break;
	case VEDIAG_REPORT_SUSPEND:
		return "Suspending";
		break;
	case VEDIAG_REPORT_FINISH:
		return "Finish";
		break;
	default:
		return "Unknown";
		break;
	}
}

static void vediag_gui_update_report(void *report_data)
{
	struct vediag_gui_entry *entry, *child, *parent;
	struct vediag_report *report;
	int new_flag = 0;
	int child_flag = 0;

	report = (struct vediag_report *) report_data;
	parent = vediag_gui_find_entry(report->stid);

	if(parent == NULL) {
		new_flag = 1;
	}

	if(report->index > 0) {
		child = vediag_gui_find_child(parent, report->index);
		if(child == NULL) {
			new_flag = 1;
		}
		else {
			entry = child;
		}
		child_flag = 1;
	}
	else {
		entry = parent;
	}

	/* Report of new test */
	if(new_flag) {
		entry = malloc(sizeof(struct vediag_gui_entry));
		if(IS_ERR(entry)) {
			xerror("System error: Failed to alloc memory (%s)\n",__func__);
			return;
		}
		memset(entry, 0, sizeof(struct vediag_gui_entry));
		INIT_LIST_HEAD(&entry->child);
		entry->mtid = report->mtid;
		entry->stid = report->stid;
		entry->id = report->index;
		entry->line = vediag_gui_ctrl->total_line + 1;
		vediag_gui_ctrl->total_line ++;
		entry->finish = 1;
		pthread_mutex_init(&entry->child_mutex, NULL);
	}

	switch (report->rp_type) {
	/* TODO: add when run in server system */
	case VEDIAG_REPORT_REQUEST_RETURN:
		break;
	case VEDIAG_REPORT_UPDATE_STATUS:
		if(report->detail.status.runtime >= 0) {
			entry->runtime = report->detail.status.runtime;
		}
		if(report->detail.status.percent >= 0) {
			entry->percent = report->detail.status.percent;
		}
		if(report->detail.status.flags & REPORT_STATE_CHANGE) {
			u64 size = (u64)(report->detail.status.size) << 32 | report->detail.status.size_l;
			vediag_report_get_state(entry->state, report->detail.status.state, size);
		}
		strcpy(entry->result_detail, vediag_report_get_result(report->detail.status.result));
		break;
	case VEDIAG_REPORT_CREATE_NEW_ENTRY:
		if(child_flag && new_flag)
			parent->finish++;
		if (report->detail.new_entry.name)
			strncpy(entry->name, report->detail.new_entry.name, sizeof(report->detail.new_entry.name));
		if (report->detail.new_entry.detail)
			strncpy(entry->detail, report->detail.new_entry.detail, sizeof(report->detail.new_entry.detail));
		if (report->detail.new_entry.app)
			strncpy(entry->app, report->detail.new_entry.app, sizeof(report->detail.new_entry.app));
		if (report->detail.new_entry.threads > 0)
			entry->threads = report->detail.new_entry.threads;
		strcpy(entry->result_detail, vediag_report_get_result(VEDIAG_REPORT_NOT_START));
		break;
	case VEDIAG_REPORT_UPDATE_RESULT:
		if(report->detail.result.percent >= 0) {
			entry->percent = report->detail.result.percent;
		}
		entry->completed = 1;
		entry->result = report->detail.result.result;
		strncpy(entry->result_detail, report->detail.result.result_detail, sizeof(report->detail.result.result_detail));
		parent->finish--;
		if (parent->finish <= 0)
			parent->completed = 1;
		break;
	default:
		break;
	}

	if(new_flag) {
		if(child_flag) {
			pthread_mutex_lock(&parent->child_mutex);
			list_add_tail(&entry->list, &parent->child);
			pthread_mutex_unlock(&parent->child_mutex);
		}
		else {
			list_add_tail(&entry->list, &vediag_gui_entry_list);
		}
		vediag_gui_refresh(vediag_gui_ctrl->show_completed);
	}
	else {
		vediag_gui_console_lock();
		vediag_gui_print_entry(entry);
		vediag_gui_console_unlock();
	}

	free(report_data);
}

static void vediag_gui_update_report_full(void *report_data)
{
	struct vediag_gui_entry *entry, *child, *parent;
	struct vediag_full_report *report;
	int new_flag = 0;
	int child_flag = 0;
	u64 size;

	report = (struct vediag_full_report *) report_data;
	parent = vediag_gui_find_entry(report->stid);

	if(parent == NULL) {
		new_flag = 1;
	}

	if(report->index > 0) {
		child = vediag_gui_find_child(parent, report->index);
		if(child == NULL) {
			new_flag = 1;
		}
		else {
			entry = child;
		}
		child_flag = 1;
	}
	else {
		entry = parent;
	}

	/* Report of new test */
	if(new_flag) {
		entry = malloc(sizeof(struct vediag_gui_entry));
		if(IS_ERR(entry)) {
			xerror("System error: Failed to alloc memory (%s)\n",__func__);
			return;
		}
		memset(entry, 0, sizeof(struct vediag_gui_entry));
		INIT_LIST_HEAD(&entry->child);
		entry->mtid = report->mtid;
		entry->stid = report->stid;
		entry->id = report->index;
		entry->line = vediag_gui_ctrl->total_line + 1;
		vediag_gui_ctrl->total_line ++;
		entry->finish = 1;
		pthread_mutex_init(&entry->child_mutex, NULL);
	}

	if(child_flag && new_flag)
		parent->finish++;
	if (report->name)
		strncpy(entry->name, report->name, sizeof(report->name));
	if (report->detail)
		strncpy(entry->detail, report->detail, sizeof(report->detail));
	if (report->app)
		strncpy(entry->app, report->app, sizeof(report->app));
	if (report->status)
		strncpy(entry->result_detail, report->status, sizeof(report->status));
	if (report->threads > 0)
		entry->threads = report->threads;

	if ((report->flags & VEDIAG_TEST_STATE_COMP) == VEDIAG_TEST_STATE_COMP) {
		entry->result = report->result;
		entry->completed = 1;
	}

	entry->runtime = report->runtimes;
	entry->percent = report->percent;

	size = (u64)(report->size) << 32 | report->size_l;
	vediag_report_get_state(entry->state, report->state, size);

	if(new_flag) {
		if(child_flag) {
			pthread_mutex_lock(&parent->child_mutex);
			list_add_tail(&entry->list, &parent->child);
			pthread_mutex_unlock(&parent->child_mutex);
		}
		else {
			list_add_tail(&entry->list, &vediag_gui_entry_list);
		}
		vediag_gui_refresh(vediag_gui_ctrl->show_completed);
	}
	else {
		vediag_gui_console_lock();
		vediag_gui_print_entry(entry);
		vediag_gui_console_unlock();
	}

	free(report_data);
}

static void vediag_gui_update_system(void *report_data)
{
	vediag_gui_update_common();
	free(report_data);
}

void vediag_gui_update_mid(int old_mid, int new_mid)
{
	struct vediag_gui_entry *entry;
	struct vediag_gui_report *report;
	struct vediag_report *simple_report;
	struct vediag_full_report *full_report;

	/* Update MID in gui entry list */
	pthread_mutex_lock(&gui_entry_list_mutex);
	list_for_each_entry(entry, &vediag_gui_entry_list, list){
		if (entry->mtid == old_mid){
			entry->mtid = new_mid;
		}
	}
	pthread_mutex_unlock(&gui_entry_list_mutex);

	pthread_mutex_lock(&gui_report_list_mutex);
	list_for_each_entry(report, &vediag_gui_report_list, list){
		switch (report->type) {
		case VEDIAG_SIMPLE_REPORT:
			simple_report = (struct vediag_report *)report->report;
			if(simple_report->mtid == old_mid) {
				simple_report->mtid = new_mid;
			}
			break;
		case VEDIAG_FULL_REPORT:
			full_report = (struct vediag_full_report *)report->report;
			if(full_report->mtid == old_mid) {
				full_report->mtid = new_mid;
			}
			break;
		default:
			break;
		}
	}
	pthread_mutex_unlock(&gui_report_list_mutex);
}

static void __vediag_start_gui(void)
{
	/* Update board and sensor */
	if (vediag_gui_ctrl->board)
		free(vediag_gui_ctrl->board);
	vediag_gui_ctrl->board = vediag_parse_system("Board");

	if (vediag_gui_ctrl->sensor)
		free(vediag_gui_ctrl->sensor);
	vediag_gui_ctrl->sensor = vediag_parse_system("Sensor");

	vediag_gui_console_lock();
	/* Store stdout */
	/* FIXME@hdang: change to the main log file */
	vediag_gui_ctrl->logfile = open("/dev/null", O_RDWR | O_APPEND, 0777);
	vediag_gui_ctrl->stdout = dup(fileno(stdout));
	dup2(vediag_gui_ctrl->logfile, fileno(stdout));

	save_cursor_position();
	erase_display();
	restore_cursor_position();
	vediag_gui_console_unlock();

	vediag_gui_update_common();

	vediag_gui_refresh(vediag_gui_ctrl->show_completed);

	vediag_gui_ctrl->gui_state = GUI_ON;
	vediag_gui_ctrl->watchdog = VEDIAG_GUI_WATCHDOG;
#if defined(CONFIG_VEDIAG_TEST_MANAGER)
	vediag_gui_connect(1);
#endif
}

static void __vediag_stop_gui(void)
{
	struct vediag_gui_report *report;
	struct vediag_gui_entry *entry;
	struct vediag_gui_entry *child;

	/* Clear report list */
	pthread_mutex_lock(&gui_report_list_mutex);
	while (!list_empty(&vediag_gui_report_list)) {
		report = list_entry(vediag_gui_report_list.next,
					struct vediag_gui_report, list);
		if(report) {
			list_del_init(&report->list);
			free(report->report);
			free(report);
		}
	}
	pthread_mutex_unlock(&gui_report_list_mutex);

	/* Clear GUI list */
	pthread_mutex_lock(&gui_entry_list_mutex);
	while (!list_empty(&vediag_gui_entry_list)) {
		entry = list_entry(vediag_gui_entry_list.next,
					struct vediag_gui_entry, list);
		while (!list_empty(&entry->child)) {
			child = list_entry(entry->child.next,
						struct vediag_gui_entry, list);
			list_del_init(&child->list);
			free(child);
		}
		list_del_init(&entry->list);
		free(entry);
	}
	pthread_mutex_unlock(&gui_entry_list_mutex);
	vediag_gui_ctrl->total_line = 0;
	/* restore stdout */
	dup2(vediag_gui_ctrl->stdout, fileno(stdout));
	close(vediag_gui_ctrl->logfile);
	vediag_gui_ctrl->stdout = -1;
	vediag_gui_ctrl->gui_state = GUI_OFF;

#if defined(CONFIG_VEDIAG_TEST_MANAGER)
	vediag_gui_connect(0);
#endif
}

static void *vediag_gui_ctrl_thread(void * __pdata)
{
	struct vediag_gui_report *report;

	for( ; ; ) {
		switch(vediag_gui_ctrl->gui_ctrl_state) {
		case VEDIAG_GUI_IDLE:
			pthread_mutex_lock(&vediag_gui_ctrl->cmd_ready_mutex);
			pthread_cond_wait(&vediag_gui_ctrl->cmd_ready, &vediag_gui_ctrl->cmd_ready_mutex);

			if (vediag_gui_ctrl->cmd == CMD_VEDIAG_STAT_START) {
				/* Start GUI */
				__vediag_start_gui();
				/* Clear command */
				vediag_gui_ctrl->cmd = CMD_VEDIAG_STAT_NOTHING;
				vediag_gui_ctrl->gui_ctrl_state = VEDIAG_GUI_RUNNING;
				/* Response command completed */
				pthread_mutex_lock(&vediag_gui_ctrl->cmd_complete_mutex);
				pthread_cond_signal(&vediag_gui_ctrl->cmd_complete);
				pthread_mutex_unlock(&vediag_gui_ctrl->cmd_complete_mutex);
			}
			pthread_mutex_unlock(&vediag_gui_ctrl->cmd_ready_mutex);
			break;
		case VEDIAG_GUI_RUNNING:
			/* Check if has a stop command come */
			if(vediag_gui_ctrl->cmd == CMD_VEDIAG_STAT_STOP) {
				__vediag_stop_gui();

				/* Update GUI ctrol state */
				vediag_gui_ctrl->gui_ctrl_state = VEDIAG_GUI_IDLE;
				vediag_gui_ctrl->cmd = CMD_VEDIAG_STAT_NOTHING;
				pthread_mutex_lock(&vediag_gui_ctrl->cmd_complete_mutex);
				pthread_cond_signal(&vediag_gui_ctrl->cmd_complete);
				pthread_mutex_unlock(&vediag_gui_ctrl->cmd_complete_mutex);
				break;
			}

			/* Process for report */
			vediag_gui_console_lock();
			save_cursor_position();
			vediag_gui_console_unlock();
#ifdef CONFIG_VEDIAG_SENSORS
			vediag_gui_update_common();
#endif
			pthread_mutex_lock(&gui_report_list_mutex);
			while (!list_empty(&vediag_gui_report_list)) {
				report = list_entry(vediag_gui_report_list.next,
						    struct vediag_gui_report, list);
				list_del_init(&report->list);

				if(report) {
					switch (report->type) {
					case VEDIAG_SIMPLE_REPORT:
						vediag_gui_update_report(report->report);
						break;
					case VEDIAG_FULL_REPORT:
						vediag_gui_update_report_full(report->report);
						break;
					case VEDIAG_SYSTEM_REPORT:
						vediag_gui_update_system(report->report);
						break;
					default:
						break;
					}
					free(report);
				}
			}
//			printk("Process report\n");
			pthread_mutex_unlock(&gui_report_list_mutex);
			vediag_gui_console_lock();
			restore_cursor_position();
			vediag_gui_console_unlock();

			vediag_gui_ctrl->watchdog--;
			if (!vediag_gui_ctrl->watchdog) {
				vediag_gui_ctrl->watchdog = VEDIAG_GUI_WATCHDOG;
				vediag_gui_refresh(vediag_gui_ctrl->show_completed);
			}

			sleep(1);
			break;
		default:
			break;
		}
//		schedule();
		sleep(0);
	}

	pthread_exit(NULL);
}

int vediag_gui_exit (void)
{
	/* Check if GUI is running */
	if(vediag_gui_ctrl->gui_state == GUI_OFF) {
	    xinfo("Not in GUI mode\n");
		return 1;
	}

	/* Send command to vediag status thread */
	pthread_mutex_lock(&vediag_gui_ctrl->cmd_ready_mutex);
	vediag_gui_ctrl->cmd = CMD_VEDIAG_STAT_STOP;
	pthread_cond_signal(&vediag_gui_ctrl->cmd_ready);
	pthread_mutex_unlock(&vediag_gui_ctrl->cmd_ready_mutex);

	/* Wait complete from vediag status thread */
	pthread_mutex_lock(&vediag_gui_ctrl->cmd_complete_mutex);
	pthread_cond_wait(&vediag_gui_ctrl->cmd_complete, &vediag_gui_ctrl->cmd_complete_mutex);
	pthread_mutex_unlock(&vediag_gui_ctrl->cmd_complete_mutex);

	rl_clear_message();
	rl_on_new_line();

	return 0;
}

int vediag_gui_begin (void)
{
	if(vediag_gui_ctrl->gui_state != GUI_OFF) {
	    xinfo("Already in GUI mode\n");
		return 1;
	}

	/* Send command to vediag status thread */
	pthread_mutex_lock(&vediag_gui_ctrl->cmd_ready_mutex);
	vediag_gui_ctrl->cmd = CMD_VEDIAG_STAT_START;
	pthread_cond_signal(&vediag_gui_ctrl->cmd_ready);
	pthread_mutex_unlock(&vediag_gui_ctrl->cmd_ready_mutex);

	/* Wait complete from vediag status thread */
	pthread_mutex_lock(&vediag_gui_ctrl->cmd_complete_mutex);
	pthread_cond_wait(&vediag_gui_ctrl->cmd_complete, &vediag_gui_ctrl->cmd_complete_mutex);
	pthread_mutex_unlock(&vediag_gui_ctrl->cmd_complete_mutex);

	return 0;
}

static int refresh_func(int count, int key)
{
	if (vediag_gui_ctrl->gui_state == GUI_OFF)
		return 0;

	rl_clear_message();
	rl_on_new_line();
	vediag_gui_refresh(vediag_gui_ctrl->show_completed);

	return 0;
}

static int gui_func(int count, int key)
{
	if (vediag_gui_ctrl->gui_state == GUI_ON)
		vediag_gui_exit();
	else if (vediag_gui_ctrl->gui_state == GUI_OFF)
		vediag_gui_begin();
	return 0;
}

static int collapse_running_func(int count, int key)
{
	if (vediag_gui_ctrl->gui_state == GUI_OFF)
		return 0;

	rl_clear_message();
	rl_on_new_line();
	vediag_gui_ctrl->show_completed = 1 - vediag_gui_ctrl->show_completed;
	vediag_gui_refresh(vediag_gui_ctrl->show_completed);
	return 0;
}

static int command_mode_func(int count, int key)
{
	if (vediag_gui_ctrl->gui_state!= GUI_ON)
		return 0;

	vediag_gui_ctrl->gui_state = GUI_CMD;

	/* Restore stdout */
	int org_stdout;

	/* Jump to command line at line 40 */
	set_cursor_position(40, 0);
	erase_line();

	org_stdout = vediag_gui_ctrl->stdout;
	vediag_gui_ctrl->stdout = -1;
	FILE *outstream = fdopen(org_stdout, "w");
	rl_outstream = outstream;

	rl_clear_message();
	rl_on_new_line();
	readline("[Command] :");

	/* Change stdout to GUI */
	rl_outstream = stdout;
	vediag_gui_ctrl->stdout = org_stdout;

	set_cursor_position(40, 0);
	erase_line();
	vediag_gui_ctrl->gui_state = GUI_ON;

	return 0;
}

int vediag_gui_init(void)
{
	int ret;

	xinfo("Start VED GUI controller ... ");

	vediag_gui_ctrl = malloc(sizeof(struct vediag_gui_thread_data));
	if(vediag_gui_ctrl == NULL ){
	    xinfo("System error: Failed to alloc memory (%s)\n",__func__);
		return 1;
	}

	memset(vediag_gui_ctrl,0,sizeof(struct vediag_gui_thread_data));
	pthread_cond_init (&vediag_gui_ctrl->cmd_ready, NULL);
	pthread_mutex_init(&vediag_gui_ctrl->cmd_ready_mutex, NULL);
	pthread_cond_init (&vediag_gui_ctrl->cmd_complete, NULL);
	pthread_mutex_init(&vediag_gui_ctrl->cmd_complete_mutex, NULL);

	ret = pthread_create(&vediag_gui_ctrl->gui_ctrl, NULL, vediag_gui_ctrl_thread, (void *)vediag_gui_ctrl);
	if (ret) {
	    xinfo("failed: %s\n", strerror(errno));
		return -1;
	}
	vediag_gui_ctrl->stdout = -1;
	vediag_gui_ctrl->total_line = 0;

	vediag_gui_ctrl->board = vediag_parse_system("Board");
	vediag_gui_ctrl->sensor = vediag_parse_system("Sensor");
	vediag_gui_ctrl->logfile = open("/dev/null", O_RDWR | O_CREAT | O_TRUNC, 0777);
	close(vediag_gui_ctrl->logfile);
	vediag_gui_ctrl->show_completed = 1;
	vediag_gui_ctrl->gui_state = GUI_OFF;

	rl_bind_keyseq(CMD_KEY_SEQ, command_mode_func);
	rl_bind_keyseq(COLLAPSE_KEY_SEQ, collapse_running_func);
	rl_bind_keyseq(GUI_KEY_SEQ, gui_func);
	rl_bind_keyseq(REFRESH_KEY_SEQ, refresh_func);

	xinfo("done\n");
	return 0;
}

bool vediag_check_gui_mode(void)
{
	return (vediag_gui_ctrl->gui_state != GUI_OFF);
}

int vediag_gui_clear(int mode)
{
	struct vediag_gui_entry *entry;
	struct vediag_gui_entry *child;

	pthread_mutex_lock(&gui_entry_list_mutex);

	if(mode == 1) {
		while (!list_empty(&vediag_gui_entry_list)) {

			entry = list_entry(vediag_gui_entry_list.next,
						struct vediag_gui_entry, list);
			while (!list_empty(&entry->child)) {
				child = list_entry(entry->child.next,
							struct vediag_gui_entry, list);
				list_del_init(&child->list);
			}
			list_del_init(&entry->list);
		}
		vediag_gui_ctrl->total_line = 0;
	}
	else {
		struct list_head *report_list;
		for(report_list = vediag_gui_entry_list.next ; report_list != &vediag_gui_entry_list; ) {

			entry = container_of(report_list, struct vediag_gui_entry, list);
			report_list = report_list->next;

			if(entry && !entry->finish) {
				while (!list_empty(&entry->child)) {
					child = list_entry(entry->child.next,
								struct vediag_gui_entry, list);
					list_del_init(&child->list);
				}
				list_del(&entry->list);
			}
		}

		/* Update line_number */
		vediag_gui_ctrl->total_line = 0;
		list_for_each_entry(entry, &vediag_gui_entry_list, list) {
			if(entry ) {
				entry->line = vediag_gui_ctrl->total_line + 1;
				vediag_gui_ctrl->total_line++;
				list_for_each_entry(child, &entry->child, list) {
					child->line = vediag_gui_ctrl->total_line + 1;
					vediag_gui_ctrl->total_line++;
				}
			}
		}
	}
	pthread_mutex_unlock(&gui_entry_list_mutex);

	vediag_gui_refresh(vediag_gui_ctrl->show_completed);

	return 0;
}

int vediag_gui_execute (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int arp;

	if(argc == 1) {
		goto help;
	}

	if(strcmp(argv[1], "on") == 0) {
		return vediag_gui_begin();
	}

	else if(strcmp(argv[1], "off") == 0) {
		return vediag_gui_exit();
	}

	else if(strcmp(argv[1], "clr") == 0) {
		if ((arp = vediag_getopt(argc, argv, "-f")) > 0){
			return vediag_gui_clear(0);
		}
	}

help:
	xprint ("Usage:\n%s\n", cmdtp->usage);
	return 1;
}

VEDIAG_CMD(
	gui,	3,	0,	vediag_gui_execute,
	"GUI command",
	"   => print help gor GUI command\n"
	"gui on          - Go to GUI mode\n"
	"gui off         - Exit GUI mode GUI\n"
	"gui clr [opt]   - Clear GUI entry\n"
	"        [opt] : -f Clear finished\n"
	"gui help        - GUI helping\n"
)

subsys_initcall(vediag_gui)
{
	return vediag_gui_init();
}
