
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmd.h>
#include <linux/err.h>
#include <vediag_common.h>
#include <pthread.h>
#include <time.h>
#include <vediag/firmware.h>
#include <time_util.h>
#include <log.h>
#include <strlib.h>
#include <ras.h>
#include <circbuf.h>
#include <string_plus.h>

static LIST_HEAD(testlist);
static pthread_mutex_t _mutex = PTHREAD_MUTEX_INITIALIZER;
static uint32_t test_count;

struct firmware_test_list {
	struct list_head l;
	char* devname;
};

struct firmware_test_info {
	char firmware_script[120];		/* Script name */
	char firmware_file[120];		/* Firmware name */
	int flags;						/* 1: cmd input file */
} firmware_test_info_s = {0};

static int firmware_get_app(char *app)
{
	xinfo("app='%s'\n", app);

	if (app && !strcmp(app, "/bin/sh"))
		return FIRMWARE_BIN_SH;
	else
		return FIRMWARE_BIN_SH;
}

static int firmware_create_arg(struct list_head *arg_list)
{
	vediag_set_argument("firmware_script", "/bin/firmware_update.sh", arg_list);
	vediag_set_argument("firmware_file", "openwrt-venus-ext4.img.tar.gz", arg_list);

	return 0;
}

static int firmware_check_dev(struct vediag_test_info *info,
		struct vediag_test_info *dev_info, char **error)
{
	char name[8];
	struct firmware_test_info *test_arg;
	int dev;
	char *tmp;

	if (IS_ERR_OR_NULL((void *) info)) {
		vediag_err(VEDIAG_FIRMWARE, NULL, "%s(): info NULL\n", __func__);
		return 0;
	}

	if (IS_ERR_OR_NULL((void *) dev_info)) {
		vediag_err(VEDIAG_FIRMWARE, NULL, "%s(): dev_info NULL\n", __func__);
		return 0;
	}

	/*xmanager will change 'dev_mask' to device index (count from 1)*/
	dev = dev_info->dev_mask;

	memset(name, 0, sizeof(name));

#if 1
	test_arg = &firmware_test_info_s;
#else
	test_arg = (struct firmware_test_info *) malloc(sizeof(struct firmware_test_info));
	if (IS_ERR_OR_NULL(test_arg)) {
		vediag_err(VEDIAG_FIRMWARE, dev_info->dbg_info, "Allocate memory failed\n");
		*error = strdup("Out of memory");
		return 0;
	}
	memset(test_arg, 0, sizeof(struct firmware_test_info));
#endif
	dev_info->total_size = 0;

	tmp = vediag_get_argument(dev_info, "firmware_script");
	if (tmp != NULL) {
		strncpy(test_arg->firmware_script, tmp, min(sizeof(test_arg->firmware_script), strlen(tmp)));
		free(tmp);
	}

	if(test_arg->flags != 0) {
		dev_info->total_size++;
	} else {
		tmp = vediag_get_argument(dev_info, "firmware_file");
		if (tmp != NULL) {
			strncpy(test_arg->firmware_file, tmp, min(sizeof(test_arg->firmware_file), strlen(tmp)));
			free(tmp);
			dev_info->total_size++;
		}
	}
	dev_info->data = (void *) test_arg;
	dev_info->threads = info->threads;
	//dev_info->total_size = 1;
	dev_info->dev_mask = 0;

	xinfo("[%s] Test arguments:\n", dev_info->device);
	xinfo("    test_app     		= %d\n", dev_info->test_app);
	xinfo("    firmware_script     	= %s\n", test_arg->firmware_script);
	xinfo("    firmware_file    	= %s\n", test_arg->firmware_file);
	xinfo("    record_list  		= %lld\n", dev_info->total_size);

	struct firmware_test_list *new = malloc(sizeof(struct firmware_test_list));
	if (!new){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	new->devname = malloc(32);
	if (!new->devname){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	list_add_tail(&new->l, &testlist);

	pthread_mutex_lock(&_mutex);
	test_count++;
	pthread_mutex_unlock(&_mutex);

    return 1;
}

static int firmware_test_start(int argc, char *argv[], void * _data)
{
	int arp;
	struct vediag_test_info *info = (struct vediag_test_info *) _data;
	struct firmware_test_info *test_arg = &firmware_test_info_s;

	xdebug("%s Entry\n", __func__);
	
	memset(test_arg, 0, sizeof(struct firmware_test_info));

	/* Select test device name */
	info->dev_mask = 0xffffffff;	//no input
	
	if ((arp = vediag_getopt(argc, argv, "-f")) > 0) {
		info->cfg_file = malloc(strlen(argv[arp + 1]));
		strcpy(info->cfg_file, argv[arp + 1]);
	}

	/* xDiag> diag fw [-p <fw_path>] */
	if ((arp = vediag_getopt(argc, argv, "-p")) > 0) {
		test_arg->flags = 1;
		strncpy(test_arg->firmware_file, argv[arp + 1], min(sizeof(test_arg->firmware_file), strlen(argv[arp + 1])));
	}

	info->threads = 1;

	pthread_mutex_lock(&_mutex);
	test_count = 0;
	pthread_mutex_unlock(&_mutex);

	return sizeof(struct vediag_test_info);
}

static int firmware_program(char *device, struct firmware_test_info *test, u32 *size)
{
	int ret;
	char cmd[1024];

	/* /bin/firmware_update.sh fw_path */
	{
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "%s %s", test->firmware_script, test->firmware_file);
		vediag_info(VEDIAG_FIRMWARE, device, "cmd='%s'\n", cmd);
		ret = system(cmd);
		if (ret != 0) {
			vediag_err(VEDIAG_FIRMWARE, device, "exec cmd '%s' failed (%d)\n", cmd, ret);
			return -1;
		} else {
			*size += 1;
		}
	}

	return 0;
}

static void *firmware_test_execute(void * __data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) __data;
	struct vediag_test_control *test_ctrl = info->test_ctrl;
	struct firmware_test_info *test_arg = (struct firmware_test_info *)info->data;
	int test_done = 0;
	u32 size = 0;

	while (!test_done) {
		if (test_ctrl->data_stop) {
			test_done = 1;
			continue;
		}

		if(info->test_app == FIRMWARE_BIN_SH) {
			vediag_info(VEDIAG_FIRMWARE, info->dbg_info, "Exec: script='%s', firmware='%s'\n", test_arg->firmware_script, test_arg->firmware_file);
			test_ctrl->error = firmware_program(info->device, test_arg, &size);
			test_done = 1;
		}

		if(test_ctrl->error == 0) {
			if(info->test_app == FIRMWARE_BIN_SH) {
				test_ctrl->tested_size += size;
			}
		}
		
		if (test_ctrl->error) {
			vediag_err(VEDIAG_FIRMWARE, info->dbg_info, "FIRMWARE failed\n");
			while (!test_ctrl->test_restart) {
				sleep(1);
			}
			test_ctrl->test_restart = 0;
		}

		/* Check to complete test */
		if(test_ctrl->tested_size >= info->total_size) {
			test_done = 1;
			//test_ctrl->error = 1;
		}

		sleep(0);
	}

	test_ctrl->data_running--;
	pthread_exit(NULL);
}

static int firmware_test_complete(void *data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) data;
	struct firmware_test_info *test_arg = (struct firmware_test_info *) (info->data);
	struct firmware_test_list *e;
	struct list_head *pos, *q;

	pthread_mutex_lock(&_mutex);
	test_count--;
	pthread_mutex_unlock(&_mutex);

	if (test_count == 0) {
		list_for_each_entry(e, &testlist, l) {

			if (e->devname) {
				free(e->devname);
			}
		}

		list_for_each_safe(pos, q, &testlist) {
			e = list_entry(pos, struct firmware_test_list, l);
			list_del(pos);
			free(e);
		}
	}

	if (test_arg)
		free(test_arg);

	return 0;
}

struct vediag_test_device vediag_firmware = {
    .type           = VEDIAG_FIRMWARE,
	.flags			= VEDIAG_TEST_MANUAL,
    .name           = "FIRMWARE",
    .cmd            = "fw",
    .desc           = "FIRMWARE",
	.default_app	= "/bin/sh",
	.get_test_app	= firmware_get_app,
    .test_start     = firmware_test_start,
	.create_argument= firmware_create_arg,
    .check_dev      = firmware_check_dev,
    .test_execute   = firmware_test_execute,
    .test_complete  = firmware_test_complete,
};

late_initcall(diag_firmware_test_register)
{
    return vediag_test_register(&vediag_firmware);
}
