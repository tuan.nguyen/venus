
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmd.h>
#include <linux/err.h>
#include <vediag_common.h>
#include <pthread.h>
#include <time.h>
#include <vediag/zwave.h>
#include <time_util.h>
#include <log.h>
#include <strlib.h>
#include <ras.h>
#include <circbuf.h>
#include <string_plus.h>

static LIST_HEAD(testlist);
static pthread_mutex_t _mutex = PTHREAD_MUTEX_INITIALIZER;
static uint32_t test_count;

struct zwave_test_list {
	struct list_head l;
	char* devname;
};

#define ZWAVE_CMD_SET	2
struct zwave_test_info {
	char zwave_fwver[20];				/* Firmware File */
	char zwave_cmd_fwver[20];			/* Command to Read Firmware Version */

	char zwave_fwpath[100];				/* Firmware File */
	int zwave_program;					/* Flag fw program */
	char zwave_cmd_prog[20];			/* Command to Program Firmware */

	int zwave_reset; 					/* Reset Controller */
	char zwave_cmd_reset[20];			/* Command to Reset Controller */

	int zwave_add; 						/* Add a new device */
	char zwave_cmd_add[20];				/* Command to Add New Device */

	int zwave_cmd_retry;				/* Number of retry for command Set(work around for release 5.0/6.0) */
	char zwave_cmd_set1[20];			/* Basic command set range['0' off, '255' on] */
	int zwave_interval;					/* Delay interval second */
	char zwave_cmd_set2[20];			/* Basic command set range['0' off, '255' on] */
	int zwave_cmd_loop;					/* Number of loop command */

	int zwave_rssi_min;					/* 500-series Z-Wave chip is from -94 dBm to -32 dBm */
	int zwave_rssi_max;
	
	int zwave_remove; 					/* Remove a new device */
	char zwave_cmd_remove[20];			/* Command to Remove Device */
};

static int zwave_get_app(char *app)
{
	xinfo("app='%s'\n", app);

	if (app && !strcmp(app, "zwave_tools"))
		return ZWAVE_TOOLS_TEST;
	else
		return ZWAVE_TOOLS_TEST;
}

static int zwave_create_arg(struct list_head *arg_list)
{
	vediag_set_argument("zwave_fwpath", "/lib/firmware/zwave/serialapi_controller_static_ZM5304_US_4_61.hex", arg_list);
	vediag_set_argument("zwave_cmd_prog", "-p", arg_list);
	vediag_set_argument("zwave_fwver", "4.61", arg_list);
	vediag_set_argument("zwave_cmd_fwver", "-V", arg_list);

	vediag_set_argument("zwave_reset", "1", arg_list);
	vediag_set_argument("zwave_cmd_reset", "-c", arg_list);

	vediag_set_argument("zwave_add", "1", arg_list);
	vediag_set_argument("zwave_cmd_add", "-a", arg_list);

	vediag_set_argument("zwave_cmd_retry", "0", arg_list);
	vediag_set_argument("zwave_cmd_set1", "255", arg_list);
	vediag_set_argument("zwave_interval", "5", arg_list);
	vediag_set_argument("zwave_cmd_set2", "0", arg_list);

	vediag_set_argument("zwave_rssi_min", "-94", arg_list);
	vediag_set_argument("zwave_rssi_max", "-32", arg_list);

	vediag_set_argument("zwave_cmd_loop", "3", arg_list);

	vediag_set_argument("zwave_remove", "1", arg_list);
	vediag_set_argument("zwave_cmd_remove", "-r", arg_list);

	return 0;
}

static int zwave_check_dev(struct vediag_test_info *info,
		struct vediag_test_info *dev_info, char **error)
{
	char name[8];
	struct zwave_test_info *test_arg;
	int dev;
	char *tmp;

	if (IS_ERR_OR_NULL((void *) info)) {
		vediag_err(VEDIAG_ZWAVE, NULL, "%s(): info NULL\n", __func__);
		return 0;
	}

	if (IS_ERR_OR_NULL((void *) dev_info)) {
		vediag_err(VEDIAG_ZWAVE, NULL, "%s(): dev_info NULL\n", __func__);
		return 0;
	}

	/*xmanager will change 'dev_mask' to device index (count from 1)*/
	dev = dev_info->dev_mask;

	memset(name, 0, sizeof(name));

	test_arg = (struct zwave_test_info *) malloc(sizeof(struct zwave_test_info));
	if (IS_ERR_OR_NULL(test_arg)) {
		vediag_err(VEDIAG_ZWAVE, dev_info->dbg_info, "Allocate memory failed\n");
		*error = strdup("Out of memory");
		return 0;
	}
	memset(test_arg, 0, sizeof(struct zwave_test_info));

	if (!dev_info->physical_dev) {
		char section[64] = { 0 };
		snprintf(section, 63, "%s%d", get_section_type(dev_info->test_type),
				dev_info->dev_mask);
		*error = strdup("Parse physical failed");
		vediag_err(VEDIAG_ZWAVE, dev_info->dbg_info,
				"Parse physical failed, section '%s'\n", section);
		return 0;
	}

	dev_info->total_size = 0;

	/* Get Firmware Path */
	test_arg->zwave_program =  0;
	tmp = vediag_get_argument(dev_info, "zwave_fwpath");
	if (tmp != NULL) {
		strncpy(test_arg->zwave_fwpath, tmp, min(sizeof(test_arg->zwave_fwpath), strlen(tmp)));
		free(tmp);
		dev_info->total_size++;
		/* Set flag program */
		test_arg->zwave_program =  1;
	}
	/* Get Program Command Option */
	tmp = vediag_get_argument(dev_info, "zwave_cmd_prog");
	if (tmp != NULL) {
		strncpy(test_arg->zwave_cmd_prog, tmp, min(sizeof(test_arg->zwave_cmd_prog), strlen(tmp)));
		free(tmp);
	}
	/* Get Firmware Version */
	tmp = vediag_get_argument(dev_info, "zwave_fwver");
	if (tmp != NULL) {
		strncpy(test_arg->zwave_fwver, tmp, min(sizeof(test_arg->zwave_fwver), strlen(tmp)));
		free(tmp);
	}
	/* Get Firmware Version Command Option */
	tmp = vediag_get_argument(dev_info, "zwave_cmd_fwver");
	if (tmp != NULL) {
		strncpy(test_arg->zwave_cmd_fwver, tmp, min(sizeof(test_arg->zwave_cmd_fwver), strlen(tmp)));
		free(tmp);
	}

	/* Default Reset Controller */
	test_arg->zwave_reset = 1;
	tmp = vediag_get_argument(dev_info, "zwave_reset");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zwave_reset = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}
	test_arg->zwave_reset = !!test_arg->zwave_reset;
	/* Get Reset Command Option */
	tmp = vediag_get_argument(dev_info, "zwave_cmd_reset");
	if (tmp != NULL) {
		strncpy(test_arg->zwave_cmd_reset, tmp, min(sizeof(test_arg->zwave_cmd_reset), strlen(tmp)));
		free(tmp);
	}

	/* Default Add new device */
	test_arg->zwave_add = 1;
	tmp = vediag_get_argument(dev_info, "zwave_add");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zwave_add = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}
	test_arg->zwave_add = !!test_arg->zwave_add;
	dev_info->total_size += test_arg->zwave_add;
	/* Get Add Command Option */
	tmp = vediag_get_argument(dev_info, "zwave_cmd_add");
	if (tmp != NULL) {
		strncpy(test_arg->zwave_cmd_add, tmp, min(sizeof(test_arg->zwave_cmd_add), strlen(tmp)));
		free(tmp);
	}

	/* Number of retry for command Set(work around for release 5.0/6.0) */
	test_arg->zwave_cmd_retry = 0;
	tmp = vediag_get_argument(dev_info, "zwave_cmd_retry");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zwave_cmd_retry = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}

	/* Default cmd Off-On Light Bulb */
	tmp = vediag_get_argument(dev_info, "zwave_cmd_set1");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			strncpy(test_arg->zwave_cmd_set1, tmp, min(strlen(tmp), sizeof(test_arg->zwave_cmd_set1)));
		}
		free(tmp);
	}
	test_arg->zwave_interval = 5;
	tmp = vediag_get_argument(dev_info, "zwave_interval");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zwave_interval = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}
	tmp = vediag_get_argument(dev_info, "zwave_cmd_set2");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			strncpy(test_arg->zwave_cmd_set2, tmp, min(strlen(tmp), sizeof(test_arg->zwave_cmd_set2)));
		}
		free(tmp);
	}
	test_arg->zwave_rssi_min = -94; /* dBm */
	tmp = vediag_get_argument(dev_info, "zwave_rssi_min");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zwave_rssi_min = strtol(tmp, NULL, 0);
		}
		free(tmp);
	}
	test_arg->zwave_rssi_max = -32; /* dBm */
	tmp = vediag_get_argument(dev_info, "zwave_rssi_max");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zwave_rssi_max = strtol(tmp, NULL, 0);
		}
		free(tmp);
	}
	/* Default Cmd Loop */
	test_arg->zwave_cmd_loop = 1;
	tmp = vediag_get_argument(dev_info, "zwave_cmd_loop");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zwave_cmd_loop = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}
	dev_info->total_size += test_arg->zwave_cmd_loop*2;
	/* Default Remove new device after test completed */
	test_arg->zwave_remove = 1;
	tmp = vediag_get_argument(dev_info, "zwave_remove");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->zwave_remove = (uint32_t) strtoul(tmp, NULL, 0);
		}
		free(tmp);
	}
	test_arg->zwave_remove = !!test_arg->zwave_remove;
	dev_info->total_size += test_arg->zwave_remove;
	/* Get Remove Command Option */
	tmp = vediag_get_argument(dev_info, "zwave_cmd_remove");
	if (tmp != NULL) {
		strncpy(test_arg->zwave_cmd_remove, tmp, min(sizeof(test_arg->zwave_cmd_remove), strlen(tmp)));
		free(tmp);
	}

	dev_info->data = (void *) test_arg;
	dev_info->threads = info->threads;
	dev_info->dev_mask = 0;

	xinfo("[%s] Test arguments:\n", dev_info->device);
	xinfo("    test_app     		= %d\n", dev_info->test_app);

	xinfo("    zwave_program		= %d\n", test_arg->zwave_program);
	xinfo("    zwave_fwpath		= %s\n", test_arg->zwave_fwpath);
	xinfo("    zwave_cmd_prog		= %s\n", test_arg->zwave_cmd_prog);
	xinfo("    zwave_fwver			= %s\n", test_arg->zwave_fwver);
	xinfo("    zwave_cmd_fwver		= %s\n", test_arg->zwave_cmd_fwver);

	xinfo("    zwave_reset			= %d\n", test_arg->zwave_reset);
	xinfo("    zwave_cmd_reset		= %s\n", test_arg->zwave_cmd_reset);

	xinfo("    zwave_add			= %d\n", test_arg->zwave_add);
	xinfo("    zwave_cmd_add		= %s\n", test_arg->zwave_cmd_add);

	xinfo("    zwave_cmd_retry		= %d\n", test_arg->zwave_cmd_retry);
	xinfo("    zwave_cmd_set1		= %s\n", test_arg->zwave_cmd_set1);
	xinfo("    zwave_interval		= %d sec\n", test_arg->zwave_interval);
	xinfo("    zwave_cmd_set2		= %s\n", test_arg->zwave_cmd_set2);

	xinfo("    zwave_rssi_min		= %d\n", test_arg->zwave_rssi_min);
	xinfo("    zwave_rssi_max		= %d\n", test_arg->zwave_rssi_max);

	xinfo("    zwave_cmd_loop		= %d\n", test_arg->zwave_cmd_loop);

	xinfo("    zwave_remove		= %d\n", test_arg->zwave_remove);
	xinfo("    zwave_cmd_remove	= %s\n", test_arg->zwave_cmd_remove);

	xinfo("    Total events			= %lld\n", dev_info->total_size);

	struct zwave_test_list *new = malloc(sizeof(struct zwave_test_list));
	if (!new){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	new->devname = malloc(32);
	if (!new->devname) {
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	list_add_tail(&new->l, &testlist);

	pthread_mutex_lock(&_mutex);
	test_count++;
	pthread_mutex_unlock(&_mutex);

    return 1;
}

static int zwave_test_start(int argc, char *argv[], void * _data)
{
	int arp;
	struct vediag_test_info *info = (struct vediag_test_info *) _data;
	//struct zwave_test_info *test_arg = (struct zwave_test_info *)info->data;

	xdebug("%s Entry\n", __func__);

	//info->total_size = 0xffffffff;

	/* Select test device name */
	info->dev_mask = 0xffffffff;	//no input
	if ((arp = vediag_getopt(argc, argv, "-d")) > 0) {
		if (isdigit(argv[arp + 1][0]))
			info->dev_mask =  strtoul(argv[arp + 1], NULL, 0);
	}

	if ((arp = vediag_getopt(argc, argv, "-f")) > 0) {
		info->cfg_file = malloc(strlen(argv[arp + 1]));
		strcpy(info->cfg_file, argv[arp + 1]);
	}

	info->threads = 1;

	pthread_mutex_lock(&_mutex);
	test_count = 0;
	pthread_mutex_unlock(&_mutex);

	return sizeof(struct vediag_test_info);
}

static int zwave_tools_fw_program(char *device, struct zwave_test_info *test_arg)
{
	FILE *devf;
	int line_num = 0, ret = -1;
	char *line;
	char buf[TEXT_LINE_SIZE], cmd[60], version[60];
	char *fwpath = test_arg->zwave_fwpath;
	char *fwver = test_arg->zwave_fwver;

	if(fwver != NULL) {
		/*
		 * zwave_tools -V
		 * Z-Wave ZWave Controller Static based serial API found
		 * Get FW Version
		 * Firmware Version: Z-Wave 4.61
		 * pass
		 *
		 */
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "zwave_tools %s", test_arg->zwave_cmd_fwver);
		vediag_debug(VEDIAG_ZWAVE, "command:", "%s\n", cmd);
		devf = popen(cmd, "r");
		if (devf != NULL) {
			while (1) {
				memset(buf, 0, sizeof(buf));
				line = fgets(buf, sizeof(buf), devf);

				if (line == NULL) break;
				line_num++;
				memset(version, 0, sizeof(version));
				ret = sscanf(line, "Firmware Version: Z-Wave %s", version);
				if(ret < 1) {
					ret = -1;
					//vediag_debug(VEDIAG_ZWAVE, device, "%d: '%s' ret(%d)\n", bt_num, addr, ret);
					continue;
				}
				if(strcmp(fwver, version) == 0) {
					vediag_info(VEDIAG_ZWAVE, device, "Firmware version updated '%s'\n", version);
					pclose(devf);
					return 0;
				} else {
					vediag_info(VEDIAG_ZWAVE, device, "Firmware version current '%s'\n", version);
					break;
				}
			}
			pclose(devf);
		}
	}
	/*
	 * zwave_tools -p <firmware>
	 */
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "zwave_tools %s %s", test_arg->zwave_cmd_prog, fwpath);
	vediag_debug(VEDIAG_ZWAVE, "command:", "%s\n", cmd);
	ret = system(cmd);
	if (ret < 0) {
		vediag_err(VEDIAG_ZWAVE, device, "program failed (%d)\n", ret);
	}
	return ret;
}

#define ZWAVE_ADD_DEVICE_RETRY	3
static int zwave_tools_add_device(char *device, struct zwave_test_info *test_arg, int *nodeid_add)
{
	FILE *devf;
	int line_num = 0, ret = -1, nodeid;
	char *line;
	char buf[TEXT_LINE_SIZE], cmd[60];

	/* Check if enable Reset command */
	if(test_arg->zwave_reset) {
		/*
		 * zwave_tools -c
		 * Z-Wave ZWave Controller Static based serial API found
		 * Reset Controller
		 * pass
		 */
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "zwave_tools %s", test_arg->zwave_cmd_reset);
		vediag_debug(VEDIAG_ZWAVE, "command:", "%s\n", cmd);
		ret = system(cmd);
		if (ret < 0) {
			vediag_err(VEDIAG_ZWAVE, device, "Reset Controller failed (%d)\n", ret);
			return -1;
		}
	}

	/*
	 * zwave_tools -a
	 *
	 * Z-Wave ZWave Controller Static based serial API found
	 * Add Node
	 * ANN: [ADD_NODE_STATUS_LEARN_READY(01)] - Press node button... (Nodeinformation length 0)
	 * Node[003] NodeType[0x11]:Multilevel Switch has just added.
	 * pass
	 */
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "zwave_tools %s", test_arg->zwave_cmd_add);
	vediag_debug(VEDIAG_ZWAVE, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			ret = sscanf(line, "Node[%03u] NodeType", &nodeid);
			if(ret < 1) {
				ret = -1;
				//vediag_debug(VEDIAG_ZWAVE, device, "%d: '%s' ret(%d)\n", bt_num, addr, ret);
				continue;
			}
			*nodeid_add = nodeid;
			vediag_debug(VEDIAG_ZWAVE, device, "Added NodeID=%03u\n", nodeid);
			ret = 0;
			break;
		}
		pclose(devf);
	}
	return ret;
}

static int zwave_tools_cmd_set(char *device, struct zwave_test_info *test_arg, int nodeid, char *set_cmd)
{
	FILE *devf;
	int line_num = 0, ret = -1, funcid, rssi_hex, rssi_dBm, retry = 0;
	char *line;
	char buf[TEXT_LINE_SIZE], cmd[60];
	int rssi_min = test_arg->zwave_rssi_min;
	int rssi_max = test_arg->zwave_rssi_max;

	/*
	 * zwave_tools -n 2 -s 255
	 *
	 * Z-Wave ZWave Controller Static based serial API found
	 * NodeID[7] Cmd Set=255
	 * FUNC_ID = 0x13, RSSI = 0xCF (-49 dBm)
	 * SEND DATA COMMAND is successful
	 * pass
	 */

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "zwave_tools -n 0x%X %s", nodeid, set_cmd);
	vediag_debug(VEDIAG_ZWAVE, "command:", "%s\n", cmd);
cmd_retry:
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			ret = sscanf(line, "FUNC_ID = 0x%x, RSSI = 0x%x (%d dBm)", &funcid, &rssi_hex, &rssi_dBm);
			if(ret < 3) {
				ret = -1;
				continue;
			}
			if((rssi_dBm > rssi_min) && (rssi_dBm < rssi_max)) {
				vediag_info(VEDIAG_ZWAVE, device, "RSSI: %d dBm pass in range [%d %d]\n", rssi_dBm, rssi_min, rssi_max);
				ret = 0;
			} else {
				vediag_err(VEDIAG_ZWAVE, device, "RSSI: %d dBm fail out of range [%d %d]\n", rssi_dBm, rssi_min, rssi_max);
				ret = -1;
			}
			break;
		}
		pclose(devf);
	}

	/* Check if last command failure, device not respond */
	if ((line == NULL) && (test_arg->zwave_cmd_retry > 0) && (retry < test_arg->zwave_cmd_retry)) {
		retry++;
		vediag_warn(VEDIAG_ZWAVE, device, "Command Failed, do retry %d (max %d)\n", retry, test_arg->zwave_cmd_retry);
		goto cmd_retry;
	}

	return ret;
}

static int zwave_tools_remove_device(char *device, struct zwave_test_info *test_arg, int nodeid)
{
	FILE *devf;
	int line_num = 0, ret = -1, node;
	char *line;
	char buf[TEXT_LINE_SIZE], cmd[60];

	/*
	 * zwave_tools -r
	 *
	 * Z-Wave ZWave Controller Static based serial API found
	 * Remove Node
	 * RNN: [REMOVE_NODE_STATUS_LEARN_READY(01)] - Waiting... (Nodeinformation length 0)
	 * RNN: [REMOVE_NODE_STATUS_DONE(06)]  - Removing Node 004 (Multilevel Switch(11))... (Nodeinformation length 14)
	 * Node[004] NodeType[0x11]:Multilevel Switch
	 * pass
	 */
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "zwave_tools %s 0x%X", test_arg->zwave_cmd_remove, nodeid);
	vediag_debug(VEDIAG_ZWAVE, "command:", "%s\n", cmd);
	devf = popen(cmd, "r");
	if (devf != NULL) {
		while (1) {
			memset(buf, 0, sizeof(buf));
			line = fgets(buf, sizeof(buf), devf);

			if (line == NULL) break;
			line_num++;

			ret = sscanf(line, "Node[%03u] NodeType", &node);
			if(ret < 1) {
				ret = -1;
				//vediag_debug(VEDIAG_ZWAVE, device, "%d: '%s' ret(%d)\n", bt_num, addr, ret);
				continue;
			}
			if(node != nodeid) {
				ret = -2;
				vediag_err(VEDIAG_ZWAVE, device, "Removed wrong Node %03u, expected NodeID=%03u\n", node, nodeid);
				break;
			}
			vediag_debug(VEDIAG_ZWAVE, device, "Removed NodeID=%03u\n", nodeid);
			ret = 0;
			break;
		}
		pclose(devf);
	}
	return ret;
}

static void *zwave_test_execute(void * __data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) __data;
	struct vediag_test_control *test_ctrl = info->test_ctrl;
	struct zwave_test_info *test_arg = (struct zwave_test_info *)info->data;
	int test_done = 0, iteration = 0, nodeid = -1, retry = 0;

	while (!test_done) {
		if (test_ctrl->data_stop) {
			test_done = 1;
			continue;
		}

		if(info->test_app == ZWAVE_TOOLS_TEST) {
			if(test_arg->zwave_program != 0) {
				vediag_info(VEDIAG_ZWAVE, info->dbg_info, "zwave program firmware path '%s'\n", test_arg->zwave_fwpath);
				test_ctrl->error |= zwave_tools_fw_program(info->device, test_arg);
				/* Clear flag */
				test_arg->zwave_program = 0;
				iteration++;
				retry = 0;
			} else if(test_arg->zwave_add != 0) {
				vediag_info(VEDIAG_ZWAVE, info->dbg_info, "zwave add new device\n");
				sleep(1);
				if(zwave_tools_add_device(info->device, test_arg, &nodeid) < 0) {
					retry++;
					if(retry >= ZWAVE_ADD_DEVICE_RETRY) {
						test_ctrl->error |= 1;
					}
				} else {
					vediag_info(VEDIAG_ZWAVE, info->dbg_info, "zwave added nodeid[%d] successfully\n", nodeid);
					/* Clear flag */
					test_arg->zwave_add = 0;
					iteration++;
					retry = 0;
				}
			} else if(test_arg->zwave_cmd_loop > 0) {
				vediag_info(VEDIAG_ZWAVE, info->dbg_info, "zwave cmd set[%d]: [%s %ds %s], check RSSI[%d %d]\n",
						test_arg->zwave_cmd_loop, test_arg->zwave_cmd_set1, test_arg->zwave_interval, test_arg->zwave_cmd_set2,
						test_arg->zwave_rssi_min, test_arg->zwave_rssi_max);
				/* Delay interval sec */
				sleep(test_arg->zwave_interval);
				test_ctrl->error |= zwave_tools_cmd_set(info->device, test_arg, nodeid, test_arg->zwave_cmd_set1);
				iteration++;
				/* Delay interval sec */
				sleep(test_arg->zwave_interval);
				test_ctrl->error |= zwave_tools_cmd_set(info->device, test_arg, nodeid, test_arg->zwave_cmd_set2);
				iteration++;
				test_arg->zwave_cmd_loop--;
			} else if(test_arg->zwave_remove != 0) {
				vediag_info(VEDIAG_ZWAVE, info->dbg_info, "zwave remove device nodeid[%03u]\n", nodeid);
				sleep(1);
				if(zwave_tools_remove_device(info->device, test_arg, nodeid) < 0) {
					retry++;
					if(retry >= ZWAVE_ADD_DEVICE_RETRY) {
						test_ctrl->error |= 1;
					}
				} else {
					vediag_info(VEDIAG_ZWAVE, info->dbg_info, "zwave removed nodeid[%03u] successfully\n", nodeid);
					/* Clear flag */
					test_arg->zwave_remove = 0;
					iteration++;
					retry = 0;
				}
			}
		}

		/* Check to stop */
		if(info->test_app == ZWAVE_TOOLS_TEST) {
			test_ctrl->tested_size = iteration;
			if(test_ctrl->tested_size >= info->total_size) {
				test_done = 1;
			}
		}

		if (test_ctrl->error) {
			vediag_err(VEDIAG_ZWAVE, info->dbg_info, "ZWAVE failed\n");
			while (!test_ctrl->test_restart) {
				sleep(1);
			}
			test_ctrl->test_restart = 0;
		}

		sleep(0);
	}

	test_ctrl->data_running--;
	pthread_exit(NULL);
}

static int zwave_test_complete(void *data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) data;
	struct zwave_test_info *test_arg = (struct zwave_test_info *) (info->data);
	struct zwave_test_list *e;
	struct list_head *pos, *q;

	pthread_mutex_lock(&_mutex);
	test_count--;
	pthread_mutex_unlock(&_mutex);

	if (test_count == 0) {
		list_for_each_entry(e, &testlist, l) {

			if (e->devname) {
				free(e->devname);
			}
		}

		list_for_each_safe(pos, q, &testlist) {
			e = list_entry(pos, struct zwave_test_list, l);
			list_del(pos);
			free(e);
		}
	}

	if (test_arg)
		free(test_arg);

	return 0;
}

struct vediag_test_device vediag_zwave = {
    .type           = VEDIAG_ZWAVE,
	.flags			= VEDIAG_TEST_MANUAL,
    .name           = "ZWAVE",
    .cmd            = "zwave",
    .desc           = "ZWAVE TEST",
	.default_app	= "zwave_tools",
	.get_test_app	= zwave_get_app,
    .test_start     = zwave_test_start,
	.create_argument= zwave_create_arg,
    .check_dev      = zwave_check_dev,
    .test_execute   = zwave_test_execute,
    .test_complete  = zwave_test_complete,
};

late_initcall(diag_zwave_test_register)
{
    return vediag_test_register(&vediag_zwave);
}
