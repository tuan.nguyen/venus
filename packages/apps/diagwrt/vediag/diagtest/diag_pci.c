
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmd.h>
#include <string.h>
#include <linux/err.h>
#include <vediag_common.h>
#include <pthread.h>
#include <vediag/pciutils.h>
#include <time_util.h>
#include <log.h>
#include <strlib.h>
#include <ras.h>
#include <circbuf.h>

static LIST_HEAD(testlist);
static pthread_mutex_t _mutex = PTHREAD_MUTEX_INITIALIZER;
static uint32_t test_count;

struct pci_test_list {
	struct list_head l;
	struct pci_dbdf bdf;
	char* devname;
};

struct pci_test_info {
	struct pci_dbdf bdf;
	u16 vid;
	int gen;
	int width;
	uint32_t corr_mask;
	uint32_t uncorr_mask;
};

#define PCI_DEFAULT_RUNNING_TIME	60
#define PCI_MIN_RUNNING_TIME		30

static int pci_create_arg(struct list_head *arg_list)
{
	vediag_set_argument("VendorID", "", arg_list);
	vediag_set_argument("Time", "600", arg_list);
	vediag_set_argument("Gen", "", arg_list);
	vediag_set_argument("Width", "", arg_list);
	vediag_set_argument("AER_correctable_mask", "0x0", arg_list);
	vediag_set_argument("AER_uncorrectable_mask", "0x0", arg_list);

	return 0;
}

static int pci_check_dev(struct vediag_test_info *info,
		struct vediag_test_info *dev_info, char **error)
{
	char name[8];
	struct pci_test_info *test_arg;
	int dev;
	uint32_t gen;
	char *tmp;
	char sbdf[16] = {0};

	if (IS_ERR_OR_NULL((void *) info)) {
		vediag_err(VEDIAG_PCIE, NULL, "%s(): info NULL\n", __func__);
		return 0;
	}

	if (IS_ERR_OR_NULL((void *) dev_info)) {
		vediag_err(VEDIAG_PCIE, NULL, "%s(): dev_info NULL\n", __func__);
		return 0;
	}

	/*xmanager will change 'dev_mask' to device index (count from 1)*/
	dev = dev_info->dev_mask;

	memset(name, 0, sizeof(name));

	test_arg = (struct pci_test_info *) malloc(sizeof(struct pci_test_info));
	if (IS_ERR_OR_NULL(test_arg)) {
		vediag_err(VEDIAG_PCIE, dev_info->dbg_info, "Allocate memory failed\n");
		*error = strdup("Out of memory");
		return 0;
	}

	if (!dev_info->physical_dev) {
		char section[64] = { 0 };
		snprintf(section, 63, "%s%d", get_section_type(dev_info->test_type),
				dev_info->dev_mask);
		*error = strdup("Parse physical failed");
		vediag_err(VEDIAG_PCIE, dev_info->dbg_info,
				"Parse physical failed, section '%s'\n", section);
		return 0;
	}

	/* Get argument */
	strncpy(sbdf, dev_info->device, sizeof(sbdf) - 1);
	if (pci_parse_dbdf(sbdf, &test_arg->bdf) != COMMAND_OK) {
		*error = strdup("Invalid bus number");
		vediag_err(VEDIAG_PCIE, dev_info->dbg_info, "Invalid bus number <domain:bus:dev.func>\n");
		return 0;
	}

	tmp = vediag_get_argument(dev_info, "VendorID");
	if (!tmp) {
		test_arg->vid = 0xffff;
	} else {
		if (strlen(tmp))
			test_arg->vid = (uint16_t) strtoul(tmp, NULL, 16);
		else
			test_arg->vid = 0xffff;
	}

	if (test_arg->vid == 0) {
		test_arg->vid = 0xffff;
	}

	tmp = vediag_get_argument(dev_info, "Gen");
	if (!tmp) {
		test_arg->gen = PCI_CHECK_GEN_FROM_LNKSTAT_LNKCAP;
	} else {
		if (strlen(tmp)) {
			gen = (uint32_t) strtoul(tmp, NULL, 10);
			if (gen == 1) {
				test_arg->gen = PCIE_GEN1;
			} else if (gen == 2) {
				test_arg->gen = PCIE_GEN2;
			} else if (gen == 3) {
				test_arg->gen = PCIE_GEN3;
			} else {
				*error = strdup("Invalid Gen");
				vediag_err(VEDIAG_PCIE, dev_info->dbg_info,
						"Parse PCI Gen failed (%u)\n", gen);
				return 0;
			}
		} else {
			test_arg->gen = PCI_CHECK_GEN_FROM_LNKSTAT_LNKCAP;
		}
		free(tmp);
	}

	tmp = vediag_get_argument(dev_info, "Width");
	if (!tmp) {
		test_arg->width = PCI_CHECK_WIDTH_FROM_LNKSTAT_LNKCAP;
	} else {
		if (strlen(tmp)) {
			test_arg->width = (int) strtoul(tmp, NULL, 10);
			if (test_arg->width != 1 && test_arg->width != 2
					&& test_arg->width != 4 && test_arg->width != 8
					&& test_arg->width != 16) {
				*error = strdup("Invalid width");
				vediag_err(VEDIAG_PCIE, dev_info->dbg_info,
						"Parse PCI Width failed (%u)\n", test_arg->width);
				return 0;
			}
		} else {
			test_arg->width = PCI_CHECK_WIDTH_FROM_LNKSTAT_LNKCAP;
		}
		free(tmp);
	}

	if (info->runtime > 0) {
		dev_info->runtime = info->runtime;
	} else {
		tmp = vediag_get_argument(dev_info, "Time");
		if (!tmp) {
			dev_info->runtime = PCI_DEFAULT_RUNNING_TIME;
		} else {
			if (strlen(tmp)) {
				dev_info->runtime = (int) strtoul(tmp, NULL, 10);
				if (dev_info->runtime == 0) {
					dev_info->runtime = PCI_DEFAULT_RUNNING_TIME;
					vediag_info(VEDIAG_PCIE, dev_info->dbg_info,
							"runtime = 0, set to default %ds\n",
							dev_info->runtime);
				}
				if (dev_info->runtime < PCI_MIN_RUNNING_TIME) {
					dev_info->runtime = PCI_MIN_RUNNING_TIME;
					vediag_info(VEDIAG_PCIE, dev_info->dbg_info,
							"runtime < Min, set to min %ds\n",
							dev_info->runtime);
				}
			} else {
				dev_info->runtime = PCI_DEFAULT_RUNNING_TIME;
			}
			free(tmp);
		}
	}

	tmp = vediag_get_argument(dev_info, "AER_correctable_mask");
	if (!tmp) {
		test_arg->corr_mask = 0x0;
	} else {
		if (strlen(tmp)) {
			test_arg->corr_mask = (uint32_t) strtoul(tmp, NULL, 16);
		} else {
			test_arg->corr_mask = 0x0;
		}
		free(tmp);
	}

	tmp = vediag_get_argument(dev_info, "AER_uncorrectable_mask");
	if (!tmp) {
		test_arg->uncorr_mask = 0x0;
	} else {
		if (strlen(tmp)) {
			test_arg->uncorr_mask = (uint32_t) strtoul(tmp, NULL, 16);
		} else {
			test_arg->uncorr_mask = 0x0;
		}
		free(tmp);
	}

	dev_info->data = (void *) test_arg;
	dev_info->threads = info->threads;
	dev_info->total_size = info->total_size;
	dev_info->dev_mask = 0;

	xinfo("\n [%s] AER Test arguments:\n", dev_info->device);
	if (test_arg->vid != 0xffff)
		xinfo("    VendorID     = 0x%04x\n", test_arg->vid);
	else
		xinfo("    VendorID     = Any Vendor\n");
	if (test_arg->gen != PCI_CHECK_GEN_FROM_LNKSTAT_LNKCAP)
		xinfo("    Gen          = %d\n", test_arg->gen);
	else
		xinfo("    Gen          = Any Gen\n");

	if (test_arg->width != PCI_CHECK_WIDTH_FROM_LNKSTAT_LNKCAP)
		xinfo("    Width        = %d\n", test_arg->width);
	else
		xinfo("    Width        = Any width\n");
	xinfo("    corr_mask    = 0x%08x\n", test_arg->corr_mask);
	xinfo("    uncorr_mask  = 0x%08x\n", test_arg->uncorr_mask);
	xinfo("    run time     = %d seconds\n\n", dev_info->runtime);

	pci_aer_mask_chain(&test_arg->bdf);
	struct pci_test_list *new = malloc(sizeof(struct pci_test_list));
	if (!new){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	new->devname = malloc(32);
	if (!new->devname){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	if (vediag_pciutils_get_devname(&test_arg->bdf, new->devname) != false) {
		char* tmp = strdup(new->devname);
		strcrop_s(new->devname, tmp);
		if (tmp && strncmp(dev_info->physical_dev, tmp, strlen(tmp))){
			vediag_warn(VEDIAG_PCIE, NULL, "Devices setup are changed, please update system.ini file!\n");
			vediag_warn(VEDIAG_PCIE, NULL, " In system.ini: %s '%s'\n", dev_info->device,
					dev_info->physical_dev);
			vediag_warn(VEDIAG_PCIE, NULL, " Current setup: %s '%s'\n", dev_info->device,
					new->devname);
		}
		if (tmp)
			free(tmp);
	}

	memcpy(&new->bdf, &test_arg->bdf, sizeof(struct pci_dbdf));
	list_add_tail(&new->l, &testlist);

	pthread_mutex_lock(&_mutex);
	test_count++;
	pthread_mutex_unlock(&_mutex);

	/* Clear RAS error in PCI queue */
	double start_time = get_time_sec();
	while (!cbIsEmpty(&ras_cb_pci)) {
		ElemType *e;
		e = calloc(1, sizeof(ElemType));
		if (!e)
			break;

		cbRead(&ras_cb_pci, e);
		free(e);

		/* break if error is bumped too fast*/
		if (get_time_sec() - start_time > 3)
			break;
	}

    return 1;
}

static int pci_test_start(int argc, char *argv[], void * _data)
{
	int arp;
	struct vediag_test_info *info = (struct vediag_test_info *) _data;

	xdebug("%s Entry\n", __func__);

	info->total_size = 0xffffffff;
	info->dev_mask = 0xffffffff;

	if ((arp = vediag_getopt(argc, argv, "-f")) > 0) {
		info->cfg_file = malloc(strlen(argv[arp + 1]));
		strcpy(info->cfg_file, argv[arp + 1]);
	}

	info->threads = 1;

	pthread_mutex_lock(&_mutex);
	test_count = 0;
	pthread_mutex_unlock(&_mutex);

	return sizeof(struct vediag_test_info);
}

static void *pci_test_execute(void * __data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) __data;
	struct vediag_test_control *test_ctrl = info->test_ctrl;
	struct pci_test_info *test_arg = (struct pci_test_info *)info->data;
	int test_done = 0;
	const int test_time = 10;
	struct pci_test test = {0};

	test.dev = &test_arg->bdf;
	test.vid = test_arg->vid;
	test.time = test_time;
	test.gen = test_arg->gen;
	test.width = test_arg->width;
	test.corr_mask = test_arg->corr_mask;
	test.uncorr_mask = test_arg->uncorr_mask;

	while (!test_done) {
		if (test_ctrl->data_stop) {
			test_done = 1;
			continue;
		}

		test_ctrl->error = pci_aer_test(&test, 0);

		if (test_ctrl->error) {
			pci_err_to_string(test_ctrl->error, test_ctrl->err_msg, 23);
			vediag_err(VEDIAG_PCIE, info->dbg_info, "AER failed\n");
			while (!test_ctrl->test_restart) {
				sleep(1);
			}
			test_ctrl->test_restart = 0;
		}
		sleep(0);
	}

	test_ctrl->data_running--;
	pthread_exit(NULL);
}

static int pci_test_complete(void *data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) data;
	struct pci_test_info *test_arg = (struct pci_test_info *) (info->data);
	struct pci_test_list *e;
	struct list_head *pos, *q;

	pthread_mutex_lock(&_mutex);
	test_count--;
	pthread_mutex_unlock(&_mutex);

	if (test_count == 0) {
		list_for_each_entry(e, &testlist, l) {
			pci_aer_unmask_chain(&e->bdf, 0);
			xinfo(" Unmasked [%04x:%02x:%02x.%02x]\n",
					e->bdf.domain, e->bdf.bus, e->bdf.dev, e->bdf.func);
			if (e->devname) {
				free(e->devname);
			}
		}

		list_for_each_safe(pos, q, &testlist) {
			e = list_entry(pos, struct pci_test_list, l);
			list_del(pos);
			free(e);
		}
	}

	if (test_arg)
		free(test_arg);

	return 0;
}

struct vediag_test_device vediag_pci = {
    .type           = VEDIAG_PCIE,
    .name           = "PCI",
    .cmd            = "pci",
    .desc           = "PCI TEST",
	.default_app	= "AER",
    .test_start     = pci_test_start,
	.create_argument= pci_create_arg,
    .check_dev      = pci_check_dev,
    .test_execute   = pci_test_execute,
    .test_complete  = pci_test_complete,
};

late_initcall(diag_ddr_test_register)
{
    return vediag_test_register(&vediag_pci);
}
