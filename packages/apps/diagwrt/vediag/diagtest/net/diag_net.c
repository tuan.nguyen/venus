
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <linux/err.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <sys/socket.h>
#include <linux/socket.h>
#include <linux/if.h>
#include <linux/sockios.h>
#include <cmd.h>
#include <vediag_common.h>
#include <vediag_net.h>

#include "net_subs.h"

static int remote_port_count = 0;

static void *net_test_execute(void *__data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) __data;
	struct vediag_test_control *test_ctrl = info->test_ctrl;
	struct vediag_net_test_info *net_info =
			(struct vediag_net_test_info *) info->data;
	int ret = 0;
	char *tmp = NULL;
	//char cmd[100];

	if (unlikely((test_ctrl == NULL) ||
			(info == NULL) ||
			(net_info == NULL))) {
		pthread_exit(NULL);
		return info;
	}
#if 0
	// Stop NetworkManager
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "sudo systemctl stop NetworkManager.service");
	xinfo(CYAN_COLOR"Net initial configure call: %s\n"NONE_COLOR, cmd);
	ret = system("sudo systemctl stop NetworkManager.service");
	if (ret != 0) {
		if (WIFEXITED(ret)) {
			vediag_err(VEDIAG_NET, net_info->hw_name, "FAIL: %s, return %d\n",
					cmd, WEXITSTATUS(ret));
		} else {
			vediag_err(VEDIAG_NET, net_info->hw_name,
					"FAIL: %s, child has not terminated correctly\n", cmd);
		}
	}
#endif
	// Get default gateway here
	if (net_info->test_mode == REMOTE) {
		tmp = get_default_gateway(net_info->interface_name);
		__sync_sub_and_fetch(&remote_port_count, 1);
		if (tmp) {
			strcpy(net_info->default_gateway, tmp);
		} else {
			//test_ctrl->error += 1;
			vediag_warn(VEDIAG_NET, net_info->hw_name, "Fail to get default gateway for net test\n");
			//goto exit;
		}
	}

	while (remote_port_count) {
		usleep(100000);
	}

	// Do net test initial configuration
	ret = net_test_initial_configuration(net_info);
	if (ret) {
		test_ctrl->error += 1;
		vediag_err(VEDIAG_NET, net_info->hw_name,
				"Fail to configure for net test");
		goto exit;
	}

	switch (net_info->app) {
	case NET_SCP_TEST:
		xinfo("[%s] Starting SCP test\n", net_info->hw_name);
		ret = scp_test_execute(info);
		if (ret) {
			vediag_err(VEDIAG_NET, net_info->hw_name,
					"FAIL SCP test - Return value: %d\n", ret);
		}
		break;
	case NET_IPERF_TEST:
		xinfo("[%s] Starting IPERF test\n", net_info->hw_name);
		ret = iperf_test_execute(info);
		if (ret) {
			vediag_err(VEDIAG_NET, net_info->hw_name,
					"FAIL IPERF test - Return value: %d\n", ret);
		}
		break;
	default:
		break;
	}

exit:
	__sync_sub_and_fetch(&test_ctrl->data_running, 1);
	xinfo("[%s] Thread is done\n", net_info->hw_name);
	pthread_exit(NULL);
	return info;
}

static int net_test_complete(void *data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) data;
	struct vediag_net_test_info *net_info = (struct vediag_net_test_info *) (info->data);

	clear_initial_config_flag();
	free_net_test_info(&net_info);

	return 0;
}

static inline int net_test_help(void)
{
	xinfo("\nNETWORK test:\n"
			"  diag net [opt ...]"
			"  Options: \n"
			"    -h                   : print help\n"
			"    -phy <physical_name> : only test device with specific physical name\n"
			"    -dev <device_name>   : only test device with specific device name\n"
			"    -d <dev_mask>        : only test device(s) indicated by device mask (e.g: \"-d 0x3\" -> test device 1, 2)\n"
			"    -s <bytes>           : specify the test size (hex format)\n"
			"    -time <seconds>      : specify the test time (second), will override -s option\n"
			"    -t <thread>          : specify the number of threads to run test\n"
			);

	return 0;
}

static int net_test_start (int argc, char *argv[], void * _data)
{
	u32 dev_select = 0xFFFFFFFF;
	int num_of_threads = DEFAULT_THREADS;
	int arp;
	struct vediag_test_info *info = (struct vediag_test_info *) _data;
	int show_help = 0;

	if ((arp = vediag_getopt(argc, argv, "-h")) > 0) {
		return net_test_help();
	}

	if ((arp = vediag_getopt(argc, argv, "-phy")) > 0) {
		if ((arp + 1) < argc) {
			info->physical_dev = strdup(argv[arp + 1]);
			xinfo("[NET] Physical name: %s\n", info->physical_dev);
		} else {
			show_help = 1;
			xinfo("[NET] -phy option found but lacking argument <physical_name> - using default value (all devices)\n");
		}
	} else if ((arp = vediag_getopt(argc, argv, "-dev")) > 0) {
		if ((arp + 1) < argc) {
			info->device = strdup(argv[arp + 1]);
			xinfo("[NET] Device name: %s\n", info->device);
		} else {
			show_help = 1;
			xinfo("[NET] -phy option found but lacking argument <physical_name> - using default value (all devices)\n");
		}
	} else if ((arp = vediag_getopt(argc, argv, "-d")) > 0) {
		if ((arp + 1) < argc) {
			dev_select = strtoul(argv[arp + 1], NULL, 0);
		} else {
			show_help = 1;
			xinfo("[NET] -d option found but lacking argument <dev_mask> - using default value (all devices)\n");
		}
	}
	info->dev_mask = dev_select;

	//info->total_size = DEFAULT_NETWORK_TEST_SIZE;
	if ((arp = vediag_getopt(argc, argv, "-s")) > 0) {
		if ((arp + 1) < argc) {
			info->total_size = strtoul(argv[arp + 1], NULL, 0);
			if (info->total_size < 0)
				info->total_size = DEFAULT_NETWORK_TEST_SIZE;
		} else {
			show_help = 1;
			xinfo("[NET] -s option found but lacking argument <bytes> - using default value 0x%x byte(s)\n", DEFAULT_NETWORK_TEST_SIZE);
		}
	}

	info->runtime = 0;
	if ((arp = vediag_getopt(argc, argv, "-time")) > 0) {
		if ((arp + 1) < argc) {
			info->runtime = strtoul(argv[arp + 1], NULL, 0);
			if (info->runtime < 0) {
				info->runtime = DEFAULT_NETWORK_TEST_TIME;
				show_help = 1;
				xinfo("[NET] Invalid argument -time <seconds> - using default value %d second(s)\n", DEFAULT_NETWORK_TEST_TIME);
			}
		} else {
			show_help = 1;
			xinfo("[NET] -time option found but lacking argument <seconds> - using default value %d second(s)\n", DEFAULT_NETWORK_TEST_TIME);
		}
	}

    /* Check if test performance */
    if ((arp = vediag_getopt(argc, argv, "-pf")) > 0) {
        info->flags |= VEDIAG_TEST_PERFORMANCE;
    	xinfo("[NET] Test performance (only for iperf - no effect on other tests)\n");
    }

	/* Get thread option */
	if ((arp = vediag_getopt(argc, argv, "-t")) > 0) {
		if ((arp + 1) < argc) {
			num_of_threads = strtoul(argv[arp + 1], NULL, 0);
			if (num_of_threads > VEDIAG_MAX_THREADS)
				num_of_threads = VEDIAG_MAX_THREADS;

			if (num_of_threads < 0) {
				num_of_threads = DEFAULT_THREADS;
			}
			info->threads = num_of_threads;
			xinfo("[NET] Running %d thread\n", num_of_threads);
		} else {
			show_help = 1;
			xinfo("[NET] -t option found but lacking argument <thread> - using default value %d thread(s)\n", DEFAULT_THREADS);
		}
	}

	if (show_help) {
		net_test_help();
	}

	return sizeof(struct vediag_test_info);
}

static int net_create_arg(struct list_head *arg_list)
{
	vediag_set_argument(NETWORK_LOOPBACK_TOKEN, DEFAULT_LOOPBACK_MODE, arg_list);
	vediag_set_argument(NETWORK_SERVER_TOKEN, DEFAULT_SERVER_IP, arg_list);
	vediag_set_argument(NETWORK_USER_TOKEN, DEFAULT_USER_NAME, arg_list);
	vediag_set_argument(NETWORK_PASSWORD_TOKEN, DEFAULT_PASSWORD, arg_list);
	vediag_set_argument(NETWORK_FILE_TOKEN, DEFAULT_FILE_NAME, arg_list);
	vediag_set_argument(NETWORK_CHUNK_SIZE_TOKEN, DEFAULT_FILE_SIZE, arg_list);
	vediag_set_argument(NETWORK_TEST_SIZE_TOKEN, DEFAULT_TEST_SIZE, arg_list);
	vediag_set_argument(NETWORK_TEST_TIME_TOKEN, DEFAULT_TEST_TIME, arg_list);
	vediag_set_argument("Performance", "No", arg_list);

	return 0;
}

static int net_get_app(char *app)
{
	if (app && !strcmp(app, "scp"))
		return NET_SCP_TEST;
	else if (app && !strcmp(app, "iperf"))
		return NET_IPERF_TEST;
	else {
		if (app == NULL) {
			xerror("ERROR: NET get app FAIL - app: NULL\n");
		} else {
			xerror("ERROR: NET get app FAIL - app: %s\n", app);
		}
		return NET_INVALID_TEST;
	}
}

static int net_check_dev(struct vediag_test_info *info, struct vediag_test_info *dev_info, char **error)
{
    struct vediag_net_test_info *test_arg;
    char *token_value;
    char section[64];


    if(IS_ERR_OR_NULL(info)) {
    	xinfo("%s(): info NULL\n", __func__);
    	return 0;
    }

    if(IS_ERR_OR_NULL(dev_info)) {
    	xinfo("%s(): dev_info NULL\n", __func__);
    	return 0;
    }

    memset(section, 0, sizeof(section));
    sprintf(section, "%s%d", get_section_type(dev_info->test_type), dev_info->dev_mask);

	test_arg = (struct vediag_net_test_info *) malloc(sizeof(struct vediag_net_test_info));
    if(IS_ERR_OR_NULL(test_arg)) {
    	*error = strdup("Out of memory");
		vediag_err(VEDIAG_NET, dev_info->physical_dev, "[%s] Allocate memory failed\n", section);
    	return 0;
    }

    memset(test_arg, 0, sizeof(struct vediag_net_test_info));

	/**
	 * Duplicate interface/hw name, please don't assign pointer
	 */
    if (dev_info->device)
    	test_arg->interface_name = strdup(dev_info->device);
    else {
    	*error = strdup("Interface name not found");
		vediag_err(VEDIAG_NET, dev_info->physical_dev, "[%s] Interface name not found\n", section);
		free_net_test_info(&test_arg);
		return 0;
    }

    if (dev_info->physical_dev)
    	test_arg->hw_name = strdup(dev_info->physical_dev);
    else {
    	*error = strdup("Physical name not found");
		vediag_err(VEDIAG_NET, dev_info->physical_dev, "[%s] Physical name not found\n", section);
		free_net_test_info(&test_arg);
		return 0;
    }

	/**
	 * Parse arguments from the specified *.ini file
	 */
	test_arg->app = dev_info->test_app;
	if (test_arg->app == NET_INVALID_TEST) {
		*error = strdup("Invalid Test App");
		vediag_err(VEDIAG_NET, dev_info->physical_dev, "[%s] Invalid test app. Available apps: scp, iperf.\n", section);
		free_net_test_info(&test_arg);
		return 0;
	}

#if 0
	/* Parse mode */
	token_value = vediag_get_argument(dev_info, NETWORK_MODE_TOKEN);
	if (token_value != NULL) {
		test_arg->test_mode = strtoul(token_value, NULL, 0);
	}
#else
	/* Parse loopback mode */
	token_value = vediag_get_argument(dev_info, NETWORK_LOOPBACK_TOKEN);
	if (token_value != NULL) {
		if (strcmp(token_value, "yes") == 0) {
			test_arg->test_mode = 1;
		}
		free(token_value);
	}
#endif

	/**
	 * Parse server, if loopback mode then server is interface name,
	 * otherwise IP address
	 */
	test_arg->server = vediag_get_argument(dev_info, NETWORK_SERVER_TOKEN);
	test_arg->user = vediag_get_argument(dev_info, NETWORK_USER_TOKEN);
	test_arg->pass = vediag_get_argument(dev_info, NETWORK_PASSWORD_TOKEN);
	test_arg->folder_name = vediag_get_argument(dev_info, NETWORK_FILE_TOKEN);

	/**
	 * Parse chunk size
	 */
	if (info->chunk_size > 0) {
		dev_info->chunk_size = info->chunk_size;
		test_arg->file_size = info->chunk_size;
	}
	else {
		token_value = vediag_get_argument(dev_info, NETWORK_CHUNK_SIZE_TOKEN);
		if (token_value != NULL) {
			test_arg->file_size = strtoul(token_value, NULL, 16);
			free(token_value);
		}
	}

	/**
	 * Parse test size
	 */
	if (info->total_size > 0) {
		dev_info->total_size = info->total_size;
	} else {
		token_value = vediag_get_argument(dev_info, NETWORK_TEST_SIZE_TOKEN);
		if (token_value != NULL) {
			dev_info->total_size = strtoul(token_value, NULL, 16);
			if (dev_info->total_size < 0)
				dev_info->total_size = DEFAULT_NETWORK_TEST_SIZE;

			info->total_size = dev_info->total_size;
			free(token_value);
		}
	}
	/**
	 * Parse interval
	 */
	token_value = vediag_get_argument(dev_info, NETWORK_INTERVAL_TOKEN);
	if (token_value != NULL) {
		test_arg->interval = strtoul(token_value, NULL, 0);
		free(token_value);
	}

	/**
	 * Parse number of iperf threads
	 */
	token_value = vediag_get_argument(dev_info, NETWORK_IPERF_THREAD_TOKEN);
	if (token_value != NULL) {
		test_arg->iperf_thread = strtoul(token_value, NULL, 0);
		free(token_value);
	}

	/**
	 * Parse iperf time, (integer value)
	 */
	token_value = vediag_get_argument(dev_info, NETWORK_TIME_TOKEN);
	if (token_value != NULL) {
		test_arg->time = strtoul(token_value, NULL, 0);
		free(token_value);
	}

	/**
	 * Parse port, (integer value)
	 */
	token_value = vediag_get_argument(dev_info, NETWORK_PORT_TOKEN);
	if (token_value != NULL) {
		test_arg->port = strtoul(token_value, NULL, 0);
		free(token_value);
	}

	/**
	 * Parse window size (string value)
	 */
	test_arg->window_size = vediag_get_argument(dev_info, NETWORK_WINDOWSIZE_TOKEN);

	/**
	 * Parse minimum iperf speed (int value) (Mbits/s)
	 */
	token_value = vediag_get_argument(dev_info, NETWORK_IPERF_MIN_SPEED_TOKEN);
	if (token_value != NULL) {
		test_arg->min_speed = strtoul(token_value, NULL, 0);
		free(token_value);
	}

	/**
	 * Use /tmp as default folder
	 */
	if (unlikely(test_arg->folder_name == NULL)) {
		test_arg->folder_name = (char *) malloc(8);
		memset(test_arg->folder_name, 0, 8);
		strcpy(test_arg->folder_name, "/tmp");
	}

	/**
	 * Use port 5001 as default port for iperf
	 */
	if (unlikely(test_arg->port == 0)) {
		test_arg->port = DEFAULT_IPERF_PORT;
	}

	/**
	 * Parse test time, (integer value - optional/override Iperf_time)
	 */
	token_value = vediag_get_argument(dev_info, NETWORK_TEST_TIME_TOKEN);
	if (token_value != NULL) {
		test_arg->time = strtoul(token_value, NULL, 0);
		free(token_value);
	}

	/* Check if test performance */
	if (info->flags & VEDIAG_TEST_PERFORMANCE) {
		test_arg->test_performance = 1;
		/* Parse performance time (optional/override Iperf_time, Test_time) */
		token_value = vediag_get_argument(dev_info, NETWORK_PERFORMANCE_TIME_TOKEN);
		if (token_value != NULL) {
			test_arg->time = strtoul(token_value, NULL, 0);
			free(token_value);
		}
	} else {
		test_arg->test_performance = 0;
	}

	/* If -time option is chosen, override all */
	if (info->runtime > 0) {
		test_arg->time = info->runtime;
	}

	/* Finalize runtime */
	info->runtime = test_arg->time;
	dev_info->runtime = test_arg->time;

	/* Check number of threads for iperf3 loopback*/
	if (test_arg->app == NET_IPERF_TEST && test_arg->test_mode == 1) {
		if (info->threads > DEFAULT_THREADS) {
			dev_info->threads = info->threads;
		} else {
			token_value = vediag_get_argument(dev_info, NETWORK_THREAD_TOKEN);
			if (token_value) {
				dev_info->threads = strtoul(token_value, NULL, 0);
				free(token_value);
			} else {
				dev_info->threads = DEFAULT_THREADS;
			}
		}
	} else {
		dev_info->threads = DEFAULT_THREADS;
	}
	test_arg->threads = dev_info->threads;

	/* Check ARG's validity */
	switch (test_arg->app) {
	case NET_IPERF_TEST:
		pthread_mutex_init(&test_arg->iperf_mutex, NULL);
		if (unlikely(((test_arg->interval == 0) ||
				(test_arg->iperf_thread == 0) ||
				(test_arg->time == 0) ||
				(test_arg->port == 0) ||
				!test_arg->server
				))) {
			*error = strdup("Invalid Arguments");
			vediag_err(VEDIAG_NET, dev_info->physical_dev, "< IPERF > [%s] Invalid arguments, please fill in %s(string), %s(decimal), %s(decimal), %s(decimal)\n",
								section, NETWORK_SERVER_TOKEN, NETWORK_INTERVAL_TOKEN, NETWORK_IPERF_THREAD_TOKEN, NETWORK_TIME_TOKEN);
			dump_net_test_info(test_arg);
			free_net_test_info(&test_arg);
			return 0;
		}
		xinfo("< IPERF > [%s] Running %d thread(s)\n", test_arg->hw_name, test_arg->threads);
		break;
	case NET_SCP_TEST:
		if (unlikely((!test_arg->server ||
				!test_arg->user ||
				!test_arg->pass ||
				!test_arg->folder_name ||
				(test_arg->file_size == 0)
				))) {
			*error = strdup("Invalid Arguments");
			vediag_err(VEDIAG_NET, dev_info->physical_dev, "< SCP > [%s] Invalid arguments, please fill in %s(string), %s(string), %s(string), %s(hex)\n",
					section, NETWORK_SERVER_TOKEN, NETWORK_USER_TOKEN, NETWORK_PASSWORD_TOKEN, NETWORK_CHUNK_SIZE_TOKEN);
			dump_net_test_info(test_arg);
			free_net_test_info(&test_arg);
			return 0;
		}
		xinfo("< SCP > [%s] Running %d thread(s)\n", test_arg->hw_name, test_arg->threads);
		break;
	default:
		xinfo("< NET > [%s] Running %d thread(s)\n", test_arg->hw_name, test_arg->threads);
		break;
	}

	if (test_arg->test_mode == REMOTE) {
		__sync_add_and_fetch(&remote_port_count, 1);
	}

	dev_info->data = (void *) test_arg;

	xinfo("< NET > [%s] Total 0x%llx \n", test_arg->hw_name, info->total_size);

    return 1;
}

struct vediag_test_device vediag_net = {
	.type			= VEDIAG_NET,
	.name			= "NET",
	.cmd			= "net",
	.desc			= "NETWORK TEST",
	.default_app	= "scp",
	.create_argument= net_create_arg,
	.get_test_app	= net_get_app,
    .test_start     = net_test_start,
    .check_dev      = net_check_dev,
    .test_execute   = net_test_execute,
    .test_complete  = net_test_complete,
};

late_initcall(vediag_net_test_register)
{
	return vediag_test_register(&vediag_net);
}

