/*
 * net_subs.c
 *
 *  Created on: Mar 23, 2017
 *      Author: mipham
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h>
#include <linux/err.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <sys/socket.h>
#include <linux/socket.h>
#include <linux/if.h>
#include <linux/sockios.h>
#include <pthread.h>
#include <cmd.h>
#include <vediag_common.h>
#include <vediag_net.h>
#include "net_subs.h"

static pthread_mutex_t initial_config_mutex = PTHREAD_MUTEX_INITIALIZER;
static int initial_config = 0;
static char remote_port_list[1000];
static int count_remote_port = 0;

#define RT_PATH	"/etc/iproute2/rt_tables"
static pthread_mutex_t remote_settings_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t route_table_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t netstat_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t interface_addr_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t interface_hw_addr_mutex = PTHREAD_MUTEX_INITIALIZER;

int check_and_insert_route_table(FILE *rt_file, char *rt_name);

void clear_remote_port_list(void)
{
	memset(remote_port_list, 0, sizeof(remote_port_list));
	count_remote_port = 0;
}

void append_remote_port_list(char *interface_name)
{
	pthread_mutex_lock(&remote_settings_mutex);

	sprintf(remote_port_list, "%s %s", remote_port_list, interface_name);
	count_remote_port++;

	pthread_mutex_unlock(&remote_settings_mutex);
}

void clear_initial_config_flag(void)
{
	initial_config = NOT_CONFIGURED;
	return ;
}

int net_test_initial_configuration(struct vediag_net_test_info *net_info)
{
	int ret = 0;
	//char cmd[100];

	pthread_mutex_lock(&initial_config_mutex);
	if (initial_config == NOT_CONFIGURED) {
		initial_config = CONFIGURED;
#if 0
		// Release DHCP IPs
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "sudo dhclient -r");
		xinfo(CYAN_COLOR"Net initial configure call: %s\n"NONE_COLOR, cmd);
		ret = system(cmd);
		if (ret != 0) {
			if (WIFEXITED(ret)) {
				vediag_err(VEDIAG_NET, net_info->hw_name, "FAIL: %s, return %d\n",
						cmd, WEXITSTATUS(ret));
			} else {
				vediag_err(VEDIAG_NET, net_info->hw_name,
						"FAIL: %s, child has not terminated correctly\n", cmd);
			}
			initial_config = CONFIGURE_FAILED;
		}

		// Get DHCP IPs
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "sudo dhclient -v -timeout %d", DHCLIENT_TIME_OUT);
		xinfo(CYAN_COLOR"Net initial configure call: %s\n"NONE_COLOR, cmd);
		ret = system(cmd);
		if (ret != 0 && ret != 256) {
			if (WIFEXITED(ret)) {
				vediag_err(VEDIAG_NET, net_info->hw_name, "FAIL: %s, return %d\n",
						cmd, WEXITSTATUS(ret));
			} else {
				vediag_err(VEDIAG_NET, net_info->hw_name,
						"FAIL: %s, child has not terminated correctly\n", cmd);
			}
			initial_config = CONFIGURE_FAILED;
		}

		// Remove known hosts
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "sudo rm -f /root/.ssh/known_hosts");
		xinfo(CYAN_COLOR"Net initial configure call: %s\n"NONE_COLOR, cmd);
		ret = system(cmd);
		if (ret != 0) {
			if (WIFEXITED(ret)) {
				vediag_err(VEDIAG_NET, net_info->hw_name, "FAIL: %s, return %d\n",
						cmd, WEXITSTATUS(ret));
			} else {
				vediag_err(VEDIAG_NET, net_info->hw_name,
						"FAIL: %s, child has not terminated correctly\n", cmd);
			}
			initial_config = CONFIGURE_FAILED;
		}
#endif
	}

	if (initial_config == CONFIGURE_FAILED) {
		ret = 1;
		vediag_err(VEDIAG_NET, net_info->hw_name,
				"Fail to configure for net test");
	}
	pthread_mutex_unlock(&initial_config_mutex);

//	sleep(5);
	return ret;
}

#define CMD_LEN			1000
#define LINE_LEN		256
#define ARG_LEN			30

/*
 * Check if the port is available to designated ip address
 */
int check_available_iperf_port(char *ip, int port)
{
	int ret = 0;
	int valid_port = 0;
	char cmd[CMD_LEN];
	char tmp[LINE_LEN];
	FILE *stream;

	while (!valid_port) {
		valid_port = 1;
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "netstat -lnt > "IPERF_PROCESS_CHECK_FILE);
		ret = system(cmd);
		if (ret) {
			xerror("[WARNING] Iperf port %d of ip address %s was not checked\n", port, ip);
			break;
		}

		stream = fopen(IPERF_PROCESS_CHECK_FILE, "r");
		if (stream) {
			char line[LINE_LEN];
			while (fgets(line, sizeof(line), stream)) {
				memset(tmp, 0, sizeof(tmp));
				sprintf(tmp, "%s:%d", ip, port);
				if (strstr(line, tmp) != NULL) {
					valid_port = 0;
					port++;
					break;
				}
			}
			fclose(stream);
		} else {
			xerror("[WARNING] Iperf port %d of ip address %s was not checked (Cannot open %s)\n", port, ip, IPERF_PROCESS_CHECK_FILE);
			break;
		}
	}

	return port;
}

/*
 * Check if command is executed
 * Return 0 if not executed
 * Return -1 if cannot check
 * Return [pid] if executed
 */
int pid_of_command(char *command)
{
	int ret = 0;
	int pid = 0;
	char cmd[CMD_LEN];
	FILE *stream;

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "pgrep -f \"%s\" > "IPERF_PROCESS_CHECK_FILE, command);
	ret = system(cmd);
	if (ret) {
		xerror("[WARNING] Command %s was not checked\n", command);
		return -1;
	}

	stream = fopen(IPERF_PROCESS_CHECK_FILE, "r");
	if (stream) {
		char line[CMD_LEN];
		fgets(line, sizeof(line), stream);
		pid = strtoul(line, NULL, 10);
		fclose(stream);
	} else {
		xerror("[WARNING] Command %s was not checked (Cannot open %s)\n", command, IPERF_PROCESS_CHECK_FILE);
	}

	return pid;
}

static void free_iperf_result_info(struct iperf_result_info **info)
{
	struct iperf_result_info *tmp = *info;

	if (tmp) {
		free(tmp);
	}
}

static int get_iperf_result_info_from_line(char *line, struct iperf_result_info *info, int direction, int version)
{
	int ret = 0;

	//xinfo("LINE='%s', direction=%d, version=%d \n", line, direction, version);

	switch (version) {
	case 3:
		if (direction == TRANSMIT) {
			/*                [ ID]      Interval   sec     Transfer Unit  Bandwidth Unit  Retransmit    Sender/Receiver */
			if (sscanf(line, "[%*[^]]]   %*s        %*s     %s       %s    %s        %s    %d            %*s",
					     info->transfer_size,
					     info->transfer_size_unit,
					     info->transfer_speed,
					     info->transfer_speed_unit,
					     &info->retransmit) >= 4) {
				ret = 0;
			} else {
				ret = -1;
			}
		} else if (direction == RECEIVE) {
			/*                [ ID]      Interval   sec     Transfer Unit  Bandwidth Unit  -Retransmit    Sender/Receiver */
			if (sscanf(line, "[%*[^]]]   %*s        %*s     %s       %s    %s        %s                   %*s",
						 info->transfer_size,
						 info->transfer_size_unit,
						 info->transfer_speed,
						 info->transfer_speed_unit) == 4) {
				ret = 0;
				info->retransmit = -1;	// Return invalid retr value
			} else {
				ret = -1;
			}
		}
		break;
	case 2:	//Reserved
	default:
		break;
	}

	if(ret == 0)
		xdebug("transfer_size='%s', transfer_size_unit='%s', transfer_speed='%s', transfer_speed_unit='%s'\n",
				info->transfer_size,
				info->transfer_size_unit,
				info->transfer_speed,
				info->transfer_speed_unit);
	return ret;

}

int get_iperf_result(FILE* stream, int runtime, int thread_num, int direction)
{
	int found = 0;
	char end_phrase[30];
	char line[LINE_LEN];
	char end_line[LINE_LEN];
	char *tmp;
	struct iperf_result_info *iperf_rslt_info;
	int tmp_avg_speed = 0;

	// Malloc struct iperf_result_info
	iperf_rslt_info = (struct iperf_result_info *) malloc(sizeof(struct iperf_result_info));
	if (iperf_rslt_info == NULL) {
		xerror("Cannot malloc struct iperf_result_info\n");
		return 0;
	}
	memset(iperf_rslt_info, 0, sizeof(struct iperf_result_info));

	/*
	 * How to get test size:
	 *
	 * iperf
	 * [  3] local 10.110.1.45 port 40523 connected with 10.110.1.1 port 5001
	 * [ ID] Interval       Transfer     Bandwidth
	 * [  3]  0.0-30.0 sec   254 MBytes  71.0 Mbits/sec
	 *
	 * [ ID] Interval       Transfer     Bandwidth
	 * [  4]  0.0-31.0 sec  30.4 MBytes  8.21 Mbits/sec
	 * [  3]  0.0-31.2 sec  31.0 MBytes  8.33 Mbits/sec
	 * [SUM]  0.0-31.2 sec  61.4 MBytes  16.5 Mbits/sec
	 *
	 * iperf3
	 * [  6]  29.00-30.00  sec  2.34 MBytes  19.6 Mbits/sec
	 * [  8]  29.00-30.00  sec  1.81 MBytes  15.2 Mbits/sec
	 * [ 10]  29.00-30.00  sec  1.77 MBytes  14.8 Mbits/sec
	 * [ 12]  29.00-30.00  sec  2.30 MBytes  19.3 Mbits/sec
	 * [SUM]  29.00-30.00  sec  8.22 MBytes  68.9 Mbits/sec
	 */
	memset(end_phrase, 0, sizeof(end_phrase));
	sprintf(end_phrase, "%s  0.0-%d.", (thread_num > 1) ? "[SUM]" : "", runtime);
	found = 0;

	//xinfo("Looking format='%s'\n", end_phrase);
	// Start saving log
	fseek(stream, 0, SEEK_SET);
	while(fgets(line, sizeof(line), stream)) {
		//xinfo("Line='%s'\n", line);
		// Look for result line
		//if (found == 0 && ((tmp = strstr(line, end_phrase)) != NULL) && (strstr(line, (direction == RECEIVE) ? "receiver" : "sender") != NULL))
		if (found == 0 && ((tmp = strstr(line, end_phrase)) != NULL))
		{
			found = 1;
			strncpy(end_line, line, LINE_LEN);
			if (get_iperf_result_info_from_line(end_line, iperf_rslt_info, direction, 3) == 0) {
				tmp_avg_speed = strtoul(iperf_rslt_info->transfer_speed, NULL, 0);
				if (tmp_avg_speed == 0) {
					found = 0;
				}
			}
		}
	}

	if (found == 0) {
		memset(end_phrase, 0, sizeof(end_phrase));
		sprintf(end_phrase, "%s   0.00-%d.", (thread_num > 1) ? "[SUM]" : "", runtime - 1);
		//xinfo("Looking format='%s'\n", end_phrase);
		fseek(stream, 0, SEEK_SET);
		while(fgets(line, sizeof(line), stream)) {
			//xinfo("Line='%s'\n", line);
			// Look for result line
			//if (found == 0 && ((tmp = strstr(line, end_phrase)) != NULL) && (strstr(line, (direction == RECEIVE) ? "receiver" : "sender") != NULL))
			if (found == 0 && ((tmp = strstr(line, end_phrase)) != NULL))
			{
				found = 1;
				strncpy(end_line, line, LINE_LEN);
				if (get_iperf_result_info_from_line(end_line, iperf_rslt_info, direction, 3) == 0) {
					tmp_avg_speed = strtoul(iperf_rslt_info->transfer_speed, NULL, 0);
					if (tmp_avg_speed == 0) {
						found = 0;
					}
				}
			}
		}
	}

	free_iperf_result_info(&iperf_rslt_info);

	return tmp_avg_speed;
}

void free_net_test_info(struct vediag_net_test_info **info)
{
	struct vediag_net_test_info *tmp = *info;
	if (tmp) {
		if (tmp->interface_name) {
			free(tmp->interface_name);
		}

		if (tmp->hw_name) {
			free(tmp->hw_name);
		}

		if (tmp->server) {
			free(tmp->server);
		}

		if (tmp->user) {
			free(tmp->user);
		}

		if (tmp->pass) {
			free(tmp->pass);
		}

		if (tmp->folder_name) {
			free(tmp->folder_name);
		}

		if (tmp->window_size) {
			free(tmp->window_size);
		}

		if (tmp->lb_interface_name) {
			free(tmp->lb_interface_name);
		}

		if (tmp->app == NET_IPERF_TEST) {
			pthread_mutex_destroy(&tmp->iperf_mutex);
		}

		free(tmp);
		tmp = NULL;
		*info = tmp;
	}
}

void dump_net_test_info(struct vediag_net_test_info *info)
{
	if (info != NULL) {
		nettestdebug("\n");
		nettestdebug("=================== [%s] ===================\n", (info->hw_name != NULL) ? info->hw_name : "null");
		nettestdebug("Interface\t= %s\n", (info->interface_name != NULL) ? info->interface_name : "null");
		nettestdebug("Ip_addr\t\t= %s\n", info->ip_addr);
		nettestdebug("App\t\t= %d\n", info->app);
		nettestdebug("Server\t\t= %s\n", (info->server != NULL) ? info->server : "null");
		nettestdebug("User\t\t= %s\n", (info->user != NULL) ? info->user : "null");
		nettestdebug("Pass\t\t= %s\n", (info->pass != NULL) ? info->pass : "null");
		nettestdebug("Time\t\t= %d\n", info->time);
		nettestdebug("Folder_name\t= %s\n", (info->folder_name != NULL) ? info->folder_name : "null");
		nettestdebug("File_size\t= %lldB\n", info->file_size);
		nettestdebug("Interval\t= %d\n", info->interval);
		nettestdebug("Thread\t\t= %d\n", info->iperf_thread);
		nettestdebug("Window_size\t= %s\n", (info->window_size != NULL) ? info->window_size : "null");
		nettestdebug("Iperf_Min_Speed\t= %d\n", info->min_speed);
		nettestdebug("Loopback\t= %d\n", info->test_mode);
		nettestdebug("Lb_intf_name\t= %s\n", (info->lb_interface_name != NULL) ? info->lb_interface_name : "null");
		nettestdebug("Lb_ip_addr\t= %s\n", info->lb_ip_addr);
		nettestdebug("Hw_addr\t\t= %s\n", info->hw_addr);
		nettestdebug("===================================================\n");
	}
}

/* Like strncpy but make sure the resulting string is always 0 terminated. */
char *safe_strncpy(char *dst, const char *src, size_t size)
{
    dst[size-1] = '\0';
    return strncpy(dst,src,size-1);
}

static char *get_name(char *name, char *p)
{
	while (isspace(*p))
		p++;
	while (*p) {
		if (isspace(*p))
			break;
		if (*p == ':') { /* could be an alias */
			char *dot = p, *dotname = name;
			*name++ = *p++;
			while (isdigit(*p))
				*name++ = *p++;
			if (*p != ':') { /* it wasn't, backup */
				p = dot;
				name = dotname;
			}
			if (*p == '\0')
				return NULL;
			p++;
			break;
		}
		*name++ = *p++;
	}
	*name++ = '\0';
	return p;
}

struct ip_dic {
	char name[IFNAMSIZ];
	char *ipaddr;

	struct list_head list;
};

LIST_HEAD(ip_table);
pthread_mutex_t ip_table_mutex = PTHREAD_MUTEX_INITIALIZER;

char *get_ip_for_interface(const char *interface)
{
	struct ip_dic *dic = NULL;
	struct ip_dic *new = NULL;
	pthread_mutex_lock(&ip_table_mutex);
	list_for_each_entry(dic, &ip_table, list) {
		if (strcmp(dic->name, interface) == 0) {
			pthread_mutex_unlock(&ip_table_mutex);
			return dic->ipaddr;
		}
	}

	static int A = 180;
	static int B = 168;
	static int C = 0;
	static int D = 1;

	A++;
	char *ipaddr = (char *) malloc(IFNAMSIZ);
	memset(ipaddr, 0, IFNAMSIZ);

	sprintf(ipaddr, "%d.%d.%d.%d", A, B, C, D);
	new = (struct ip_dic *)malloc(sizeof(struct ip_dic));
	memset(new, 0, sizeof(struct ip_dic));
	strcpy(new->name, interface);
	new->ipaddr = ipaddr;

	list_add_tail(&new->list, &ip_table);

	pthread_mutex_unlock(&ip_table_mutex);
	return new->ipaddr;
}

char *get_ip_for_hw_name(const char *hwname)
{
	static char *PCI0_MLX0 = "172.168.0.1";
	static char *PCI0_MLX1 = "173.168.0.1";
	static char *PCI2_MLX0 = "174.168.0.1";
	static char *PCI2_MLX1 = "175.168.0.1";
	static char *PCI4_MLX0 = "176.168.0.1";
	static char *PCI4_MLX1 = "177.168.0.1";

	char *ipaddr = PCI0_MLX0;

	if (strncmp(hwname, "PCI0-MLX0", 9) == 0) {
		ipaddr = PCI0_MLX0;
	} else if (strncmp(hwname, "PCI0-MLX1", 9) == 0) {
		ipaddr = PCI0_MLX1;
	} else if (strncmp(hwname, "PCI2-MLX0", 9) == 0) {
		ipaddr = PCI2_MLX0;
	} else if (strncmp(hwname, "PCI2-MLX1", 9) == 0) {
		ipaddr = PCI2_MLX1;
	} else if (strncmp(hwname, "PCI4-MLX0", 9) == 0) {
		ipaddr = PCI4_MLX0;
	} else if (strncmp(hwname, "PCI4-MLX1", 9) == 0) {
		ipaddr = PCI4_MLX1;
	} else {
		ipaddr = get_ip_for_interface(hwname);
	}

	return ipaddr;
}

static int procnetdev_version(char *buf)
{
	if (strstr(buf, "compressed"))
		return 3;
	if (strstr(buf, "bytes"))
		return 2;
	return 1;
}

static int get_dev_fields(char *bp, struct interface *ife, int version)
{
	switch (version) {
	case 3:
		sscanf(bp,
		"%llu %llu %lu %lu %lu %lu %lu %lu %llu %llu %lu %lu %lu %lu %lu %lu",
			   &ife->stats.rx_bytes,
			   &ife->stats.rx_packets,
			   &ife->stats.rx_errors,
			   &ife->stats.rx_dropped,
			   &ife->stats.rx_fifo_errors,
			   &ife->stats.rx_frame_errors,
			   &ife->stats.rx_compressed,
			   &ife->stats.rx_multicast,

			   &ife->stats.tx_bytes,
			   &ife->stats.tx_packets,
			   &ife->stats.tx_errors,
			   &ife->stats.tx_dropped,
			   &ife->stats.tx_fifo_errors,
			   &ife->stats.collisions,
			   &ife->stats.tx_carrier_errors,
			   &ife->stats.tx_compressed);
		break;
    case 2:
		sscanf(bp, "%llu %llu %lu %lu %lu %lu %llu %llu %lu %lu %lu %lu %lu",
			   &ife->stats.rx_bytes,
			   &ife->stats.rx_packets,
			   &ife->stats.rx_errors,
			   &ife->stats.rx_dropped,
			   &ife->stats.rx_fifo_errors,
			   &ife->stats.rx_frame_errors,

			   &ife->stats.tx_bytes,
			   &ife->stats.tx_packets,
			   &ife->stats.tx_errors,
			   &ife->stats.tx_dropped,
			   &ife->stats.tx_fifo_errors,
			   &ife->stats.collisions,
			   &ife->stats.tx_carrier_errors);
		ife->stats.rx_multicast = 0;
		break;
    case 1:
		sscanf(bp, "%llu %lu %lu %lu %lu %llu %lu %lu %lu %lu %lu",
			   &ife->stats.rx_packets,
			   &ife->stats.rx_errors,
			   &ife->stats.rx_dropped,
			   &ife->stats.rx_fifo_errors,
			   &ife->stats.rx_frame_errors,

			   &ife->stats.tx_packets,
			   &ife->stats.tx_errors,
			   &ife->stats.tx_dropped,
			   &ife->stats.tx_fifo_errors,
			   &ife->stats.collisions,
			   &ife->stats.tx_carrier_errors);
		ife->stats.rx_bytes = 0;
		ife->stats.tx_bytes = 0;
		ife->stats.rx_multicast = 0;
		break;
	}
	return 0;
}

static struct interface *add_interface(char *name)
{
    struct interface *new;

    new = (struct interface *)malloc(sizeof(struct interface));
    if (new != NULL) {
    	safe_strncpy(new->name, name, IFNAMSIZ);
    }

    return new;
}

void ife_print_long(struct interface *ptr)
{
	if (ptr->statistics_valid) {
	nettestdebug(("RX packets:%llu errors:%lu dropped:%lu overruns:%lu frame:%lu\n"),
		   ptr->stats.rx_packets, ptr->stats.rx_errors,
		   ptr->stats.rx_dropped, ptr->stats.rx_fifo_errors,
		   ptr->stats.rx_frame_errors);
	nettestdebug(("TX packets:%llu errors:%lu dropped:%lu overruns:%lu carrier:%lu\n"),
	       ptr->stats.tx_packets, ptr->stats.tx_errors,
	       ptr->stats.tx_dropped, ptr->stats.tx_fifo_errors,
	       ptr->stats.tx_carrier_errors);
	}
}

static pthread_mutex_t interface_info_mutex = PTHREAD_MUTEX_INITIALIZER;

static struct interface *get_interface_info(char *interface_name)
{
	FILE *fh;
	char buf[512];
	struct interface *ife = NULL;
	int proc_net_dev_version = 1;
	int err;

	pthread_mutex_lock(&interface_info_mutex);

	fh = fopen(_PATH_PROCNET_DEV, "r");
	if (!fh) {
		xerror("Warning: cannot open %s. Limited output.\n", _PATH_PROCNET_DEV);
		pthread_mutex_unlock(&interface_info_mutex);
		return NULL;
	}
	fgets(buf, sizeof buf, fh);	/* eat line */
	fgets(buf, sizeof buf, fh);
	proc_net_dev_version = procnetdev_version(buf);

	err = 0;
	char *s, name[IFNAMSIZ];
	while (fgets(buf, sizeof(buf), fh)) {
		s = get_name(name, buf);
		if (strcmp(name, interface_name) != 0) {
			continue;
		}
		ife = add_interface(name);
		if (ife) {
			get_dev_fields(s, ife, proc_net_dev_version);
			ife->statistics_valid = 1;
			/*ife_print_long(ife);*/
		}
		break;
	}

	fclose(fh);
	pthread_mutex_unlock(&interface_info_mutex);
	return ife;
}

void get_flags_from_interface_name(char *interface_name)
{
	struct ifreq ifr;
	char *dev = interface_name;

	pthread_mutex_lock(&interface_info_mutex);

	/* Open socket */
	int file = socket(AF_INET6, SOCK_DGRAM, IPPROTO_IP);
	if (file < 0) {
		xerror("Could not open socket \"%s\"(error code %d)", dev, file);
		return;
	}

	/* Zero mem for ifr */
	memset(&ifr, 0, sizeof(ifr));

	/* I want IP address attached to devname */
	strcpy(ifr.ifr_name, dev);

	/* Get info */
	ioctl(file, SIOCGIFFLAGS, &ifr); //SIOCGIFFLAGS: Get Flags

	/* Close socket */
	close(file);

	nettestdebug("Debug Info: Flags: %hu | Name: %s\n",
			ifr.ifr_flags, ifr.ifr_name);
	pthread_mutex_unlock(&interface_info_mutex);
}

static void free_interface_hw_address_info(struct interface_hw_address_info **info)
{
	struct interface_hw_address_info *tmp = *info;

	if (tmp) {
		free(tmp);
	}
}

static int get_interface_hw_address_info_from_system_file(char *interface_name, struct interface_hw_address_info* info)
{
	pthread_mutex_lock(&interface_hw_addr_mutex);

	char line[LINE_LEN];
	FILE *stream;
	int ret = 0;
	char file_name[LINE_LEN];

	/* Get HW address */
	memset(file_name, 0, sizeof(file_name));
	sprintf(file_name, "/sys/class/net/%s/address", interface_name);

	stream = fopen(file_name, "r");
	if (!stream) {
		xerror("Cannot open %s\n", file_name);
		pthread_mutex_unlock(&interface_hw_addr_mutex);
		return -1;
	}

	if(fgets(line, sizeof(line), stream)) {
		safe_strncpy(info->hw_addr, line, sizeof(info->hw_addr));
		ret = 0;
	} else {
		ret = -1;
	}
	fclose(stream);

	/* Get broadcast HW address */
	memset(file_name, 0, sizeof(file_name));
	sprintf(file_name, "/sys/class/net/%s/broadcast", interface_name);

	stream = fopen(file_name, "r");
	if (!stream) {
		xerror("Cannot open %s\n", file_name);
		pthread_mutex_unlock(&interface_hw_addr_mutex);
		return -1;
	}

	if(fgets(line, sizeof(line), stream)) {
		safe_strncpy(info->brd_hw_addr, line, sizeof(info->brd_hw_addr));
		ret = 0;
	} else {
		ret = -1;
	}
	fclose(stream);

	pthread_mutex_unlock(&interface_hw_addr_mutex);
	return ret;
}

char *get_hw_address_from_interface_name(char *interface_name, int addr_type)
{
	pthread_mutex_lock(&interface_info_mutex);

	static char hw_addr[HARDWARE_ADDRESS_LENGTH];
	char addr_type_string[30];
//	char cmd[CMD_LEN];
	char *tmp = NULL, *ret;
	struct interface_hw_address_info *iface_hw_addr_info;

	// Malloc interface_address_info struct
	iface_hw_addr_info = (struct interface_hw_address_info *)malloc(sizeof(struct interface_hw_address_info));
	if (iface_hw_addr_info == NULL) {
		xerror("Cannot malloc struct interface_hw_address_info\n");
		pthread_mutex_unlock(&interface_info_mutex);
		return NULL;
	}
	memset(iface_hw_addr_info, 0, sizeof(struct interface_hw_address_info));

	// Use ip addr to get interface info
//	memset(cmd, 0, sizeof(cmd));
//	sprintf(cmd, "ip addr | grep \"%s:\" -A 1 | grep \"link\" > %s", interface_name, GET_HW_ADDR_FILE);
//	system(cmd);

	// Open GET_HW_ADDR_FILE and parse interface info
//	if(get_interface_hw_address_info_from_file(GET_HW_ADDR_FILE, iface_hw_addr_info) == 0) {
	if(get_interface_hw_address_info_from_system_file(interface_name, iface_hw_addr_info) == 0) {
		memset(addr_type_string, 0, 30);
		switch (addr_type) {
		case BC_HW_ADDR:
			sprintf(addr_type_string, "broadcast MAC address");
			tmp = iface_hw_addr_info->brd_hw_addr;
			break;
		case HW_ADDR:
			sprintf(addr_type_string, "MAC address");
			tmp = iface_hw_addr_info->hw_addr;
			break;
		}
		if (tmp) {
			safe_strncpy(hw_addr, tmp, sizeof(hw_addr));
			xinfo(YELLOW_COLOR"%s's %s: %s\n"NONE_COLOR, interface_name, addr_type_string, hw_addr);
			ret = hw_addr;
		} else {
			ret = NULL;
		}
	} else {
		nettestdebug("No %s for %s or %s does not exist -> Fail\n", addr_type_string, interface_name, interface_name);
		ret = NULL;
	}

	free_interface_hw_address_info(&iface_hw_addr_info);
	pthread_mutex_unlock(&interface_info_mutex);
	return ret;

}

#if 0
char *get_hw_address_from_interface_name(const char *interface_name)
{
	struct ifreq ifr;
	int ret = 0;

	const char *dev = interface_name;
	unsigned char mac_address[6];
	static char hw_addr[HARDWARE_ADDRESS_LENGTH];

	pthread_mutex_lock(&interface_info_mutex);

	/* Open socket */
	int file = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
	if (file < 0) {
		xerror("Could not open socket \"%s\"(error code %d)", dev, file);
		pthread_mutex_unlock(&interface_info_mutex);
		return NULL;
	}

	/* Zero mem for ifr */
	memset(&ifr, 0, sizeof(ifr));

	/* I want IP address attached to devname */
	strcpy(ifr.ifr_name, dev);

	/* Get info */
	ret = ioctl(file, SIOCGIFHWADDR, &ifr);	//SIOCGIFHWADDR: Get HW Address

	/* Close socket */
	close(file);

	nettestdebug("Debug Info: Index = %d | Family: %d | Name: %s\n", ifr.ifr_ifindex, ifr.ifr_addr.sa_family, ifr.ifr_name);

	if(ret != 0) {
		xerror("No MAC adress for %s or %s does not exist -> Fail\n", dev, dev);
		pthread_mutex_unlock(&interface_info_mutex);
		return NULL;
	} else {
		memcpy(mac_address, ifr.ifr_hwaddr.sa_data, 6);
		snprintf(hw_addr, HARDWARE_ADDRESS_LENGTH, "%02x:%02x:%02x:%02x:%02x:%02x", mac_address[0], mac_address[1], mac_address[2],
				mac_address[3], mac_address[4], mac_address[5]);
		xinfo("%s's MAC: %s\n", dev, hw_addr);
		pthread_mutex_unlock(&interface_info_mutex);
		return hw_addr;
	}
}
#endif

u64 net_check_rx_error(char *interface_name)
{
	struct interface *ife;
	u64 error = 0;

	ife = get_interface_info(interface_name);
	if (ife) {
		error = ife->stats.rx_errors;
		free(ife);
	}

	return error;
}

u64 net_check_tx_error(char *interface_name)
{
	struct interface *ife;
	u64 error = 0;

	ife = get_interface_info(interface_name);
	if (ife) {
		error = ife->stats.tx_errors;
		free(ife);
	}

	return error;
}

void delete_existing_file(char * file_name)
{
	int ret = 0;

	ret = remove(file_name);

	if (ret == 0) {
		nettestdebug("File %s deleted successfully\n", file_name);
	} else {
		nettestdebug("Error: unable to delete the file %s\n", file_name);
	}
}

#define BUFFER_SIZE		1048576
/*
 * Create new random file in the specified directory with the specified 'file_size'
 *
 * @file_name:			file directory
 * @file_size:			file size
 * return	-1: Fail/Error
 * 			 0: Successful
 */
int create_random_file(char *file_name, unsigned long file_size)
{
	int i = 0;
	FILE *file;
	char *buff;
	int ret = 0;
	int index;
	int elements;
	int remain;
	int count;

	nettestdebug("==> Create file %s with size %ld\n", file_name, file_size);

	/* initialize random seed: */
	srand(time(NULL));

	file = fopen(file_name, "wb+");
	if (unlikely(file == NULL)) {
		xerror("Could not create file %s\n", file_name);
		return -EPERM;
	}

	buff = (char *) malloc(BUFFER_SIZE);
	if (unlikely(buff == NULL)) {
		xerror("Could not malloc buffer for random file\n");
		fclose(file);
		return -ENOMEM;
	}

	// Put random characters to input buffer
	for (i = 0; i < BUFFER_SIZE; i++) {
		buff[i] = rand() % 74 + 48;
	}

	index = 0;
	while (index < file_size) {
		remain = file_size - index;
		elements = (remain > BUFFER_SIZE) ? BUFFER_SIZE : remain;
		count = fwrite(buff, 1, elements, file);
		if (count <= 0)
			break;

		index += count;
	}

	free(buff);
	fflush(file);
	fclose(file);
	return ret;
}

/*
 * compare_test_files	compare 2 files with name <file name 1> and <file name 2> in length of <file size>
 * @sent 				file name 1
 * @received			file name 2
 * @file_size 			file size
 */
int compare_test_files(char *sent, char *received, unsigned long file_size)
{
	int ret = RETURN_NOERROR;
	int fds = 0;
	int fdr = 0;

	unsigned long read_size = 0;

	char *sbuffer;
	char *rbuffer;

	sbuffer = (char *) malloc(BUFFER_SIZE);
	if (sbuffer == NULL) {
		xerror("compare_test_files: Not enough resources\n");
		return RETURN_INSUFFICIENT_RESOURCES;
	}

	rbuffer = (char *) malloc(BUFFER_SIZE);
	if (rbuffer == NULL) {
		free(sbuffer);
		xerror("compare_test_files: Not enough resources\n");
		return RETURN_INSUFFICIENT_RESOURCES;
	}

	memset(sbuffer, 0, BUFFER_SIZE);
	memset(rbuffer, 0, BUFFER_SIZE);

	fds = open(sent, O_RDONLY);
	if (fds < 0) {
		xerror("Could not open put file \"%s\"(error code %d)\n", sent, fds);
		free(sbuffer);
		free(rbuffer);
		return RETURN_FILE_ERROR;
	} else {
		nettestdebug("Opened file \"%s\"\n", sent);
	}

	fdr = open(received, O_RDONLY);
	if (fdr < 0) {
		xerror("Could not open return file \"%s\"(error code %d)\n", received, fdr);
		close(fds);
		free(sbuffer);
		free(rbuffer);
		return RETURN_FILE_ERROR;
	} else {
		nettestdebug("Opened file \"%s\"\n", received);
	}

	while (file_size > 0) {
		read_size = (file_size < BUFFER_SIZE) ? file_size : BUFFER_SIZE;
		ret = read(fds, sbuffer, read_size);
		if (ret == -1) {
			xerror("Cannot read %s for comparing\n", sent);
			free(sbuffer);
			free(rbuffer);
			close(fds);
			close(fdr);
			return RETURN_FILE_ERROR;
		}
		ret = read(fdr, rbuffer, read_size);
		if (ret == -1) {
			xerror("Cannot read %s for comparing\n", received);
			free(sbuffer);
			free(rbuffer);
			close(fds);
			close(fdr);
			return RETURN_FILE_ERROR;
		}
		if ((ret = memcmp(sbuffer, rbuffer, read_size)) != 0) {
			xerror("Two files are different -> FAIL\n");
			free(sbuffer);
			free(rbuffer);
			close(fds);
			close(fdr);
			return RETURN_COMPARE_FAIL;
		}
		if (file_size < BUFFER_SIZE) {
			break;
		} else {
			file_size = file_size - BUFFER_SIZE;
		}
	}

	nettestdebug("Both files are the same -> PASS\n");
	free(sbuffer);
	free(rbuffer);
	close(fds);
	close(fdr);

	return ret;
}

static pthread_mutex_t loopback_settings_mutex = PTHREAD_MUTEX_INITIALIZER;

int loopback_port_2_port_settings(char *src_interface,
		char *src_addr, char *dst_interface, char *dst_addr, char *dummy_server)
{
	char hw_addr[HARDWARE_ADDRESS_LENGTH];
	char cmd[100];
	int ret = 0;

	struct in_addr src_in_addr;
	struct in_addr dst_in_addr;
	unsigned char *ucp;
	char *tmp;

	char src_dummy[IFNAMSIZ];
	char dst_dummy[IFNAMSIZ];

	memset(src_dummy, 0, sizeof(src_dummy));
	memset(dst_dummy, 0, sizeof(dst_dummy));

	inet_aton(src_addr, &src_in_addr);
	inet_aton(dst_addr, &dst_in_addr);

	ucp = (unsigned char *) &src_in_addr;
	ucp[1]++;
	tmp = inet_ntoa(src_in_addr);
	strcpy(src_dummy, tmp);

	ucp = (unsigned char *) &dst_in_addr;
	ucp[1]++;
	tmp = inet_ntoa(dst_in_addr);
	strcpy(dst_dummy, tmp);

	if (dummy_server != NULL) {
		sprintf(dummy_server, "%s%c", dst_dummy, '\0');
	}

	pthread_mutex_lock(&loopback_settings_mutex);

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "ifconfig %s %s netmask 255.255.255.0", src_interface, src_addr);
	ret = system(cmd);
	if (ret != 0) {
		if (WIFEXITED(ret)) {
			xerror("FAIL: %s, return %d\n", cmd, WEXITSTATUS(ret));
		} else {
			xerror("FAIL: %s, child has not terminated correctly\n", cmd);
		}
		pthread_mutex_unlock(&loopback_settings_mutex);
		return -EINVAL;
	}

	tmp = polling_address_ready(src_interface, IP_ADDRESS_POLLING_TIME);
	if (likely(tmp != NULL)) {
		safe_strncpy(src_addr, tmp, IFNAMSIZ);
	} else {
		xerror("FAIL: cannot get ip address from %s\n", src_interface);
		return -EINVAL;
	}

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "ifconfig %s %s netmask 255.255.255.0", dst_interface,	dst_addr);
	ret = system(cmd);
	if (ret != 0) {
		if (WIFEXITED(ret)) {
			xerror("FAIL: %s, return %d\n", cmd, WEXITSTATUS(ret));
		} else {
			xerror("FAIL: %s, child has not terminated correctly\n", cmd);
		}
		pthread_mutex_unlock(&loopback_settings_mutex);
		return -EINVAL;
	}

	tmp = polling_address_ready(dst_interface, IP_ADDRESS_POLLING_TIME);
	if (likely(tmp != NULL)) {
		safe_strncpy(dst_addr, tmp, IFNAMSIZ);
	} else {
		xerror("FAIL: cannot get ip address from %s\n", dst_interface);
		return -EINVAL;
	}

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "iptables -t nat -C POSTROUTING -s %s -d %s -j SNAT --to-source %s", src_addr, dst_dummy, src_dummy);
	ret = system(cmd);
	if (ret == 256) {
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "iptables -t nat -A POSTROUTING -s %s -d %s -j SNAT --to-source %s", src_addr, dst_dummy, src_dummy);
		ret = system(cmd);
		if (ret != 0) {
			if (WIFEXITED(ret)) {
				xerror("FAIL: %s, return %d\n", cmd, WEXITSTATUS(ret));
			} else {
				xerror("FAIL: %s, child has not terminated correctly\n", cmd);
			}
			pthread_mutex_unlock(&loopback_settings_mutex);
			return -EINVAL;
		}
	}

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "iptables -t nat -C PREROUTING -d %s -j DNAT --to-destination %s", src_dummy, src_addr);
	ret = system(cmd);
	if (ret == 256) {
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "iptables -t nat -A PREROUTING -d %s -j DNAT --to-destination %s", src_dummy, src_addr);
		ret = system(cmd);
		if (ret != 0) {
			if (WIFEXITED(ret)) {
				xerror("FAIL: %s, return %d\n", cmd, WEXITSTATUS(ret));
			} else {
				xerror("FAIL: %s, child has not terminated correctly\n", cmd);
			}
			pthread_mutex_unlock(&loopback_settings_mutex);
			return -EINVAL;
		}
	}

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "iptables -t nat -C POSTROUTING -s %s -d %s -j SNAT --to-source %s", dst_addr, src_dummy, dst_dummy);
	ret = system(cmd);
	if (ret == 256) {
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "iptables -t nat -A POSTROUTING -s %s -d %s -j SNAT --to-source %s", dst_addr, src_dummy, dst_dummy);
		ret = system(cmd);
		if (ret != 0) {
			if (WIFEXITED(ret)) {
				xerror("FAIL: %s, return %d\n", cmd, WEXITSTATUS(ret));
			} else {
				xerror("FAIL: %s, child has not terminated correctly\n", cmd);
			}
			pthread_mutex_unlock(&loopback_settings_mutex);
			return -EINVAL;
		}
	}

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "iptables -t nat -C PREROUTING -d %s -j DNAT --to-destination %s", dst_dummy, dst_addr);
	ret = system(cmd);
	if (ret == 256) {
		memset(cmd, 0, sizeof(cmd));
		sprintf(cmd, "iptables -t nat -A PREROUTING -d %s -j DNAT --to-destination %s", dst_dummy, dst_addr);
		ret = system(cmd);
		if (ret != 0) {
			if (WIFEXITED(ret)) {
				xerror("FAIL: %s, return %d\n", cmd, WEXITSTATUS(ret));
			} else {
				xerror("FAIL: %s, child has not terminated correctly\n", cmd);
			}
			pthread_mutex_unlock(&loopback_settings_mutex);
			return -EINVAL;
		}
	}

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "ip route add %s dev %s", dst_dummy, src_interface);
	xinfo("%s\n", cmd);
	ret = system(cmd);
	if (ret !=0 && ret != 512) {
		if (WIFEXITED(ret)) {
			xerror("FAIL: %s, return %d\n", cmd, WEXITSTATUS(ret));
		} else {
			xerror("FAIL: %s, child has not terminated correctly\n", cmd);
		}
		pthread_mutex_unlock(&loopback_settings_mutex);
		return -EINVAL;
	}
	if (ret == 512) {
		xerror("%s somethings fishy\n", cmd);
	}

	/*get_hw_address_from_interface_name*/
	tmp = get_hw_address_from_interface_name(dst_interface, HW_ADDR);
	if (tmp == NULL) {
		xerror("FAIL: get hw_address of %s\n", dst_interface);
		pthread_mutex_unlock(&loopback_settings_mutex);
		return -EINVAL;
	}

	strncpy(hw_addr, tmp, HARDWARE_ADDRESS_LENGTH);
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "arp -i %s -s %s %s", src_interface, dst_dummy, hw_addr);
	ret = system(cmd);
	if (ret != 0) {
		if (WIFEXITED(ret)) {
			xerror("FAIL: %s, return %d\n", cmd, WEXITSTATUS(ret));
		} else {
			xerror("FAIL: %s, child has not terminated correctly\n", cmd);
		}
		pthread_mutex_unlock(&loopback_settings_mutex);
		return -EINVAL;
	}

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "ip route add %s dev %s", src_dummy, dst_interface);
	ret = system(cmd);
	if (ret !=0 && ret != 512) {
		if (WIFEXITED(ret)) {
			xerror("FAIL: %s, return %d\n", cmd, WEXITSTATUS(ret));
		} else {
			xerror("FAIL: %s, child has not terminated correctly\n", cmd);
		}
		pthread_mutex_unlock(&loopback_settings_mutex);
		return -EINVAL;
	}
	if (ret == 512) {
		xerror("%s somethings fishy\n", cmd);
	}

	/*get_hw_address_from_interface_name*/
	tmp = get_hw_address_from_interface_name(src_interface, HW_ADDR);
	if (tmp == NULL) {
		xerror("FAIL: get hw_address of %s\n", src_interface);
		pthread_mutex_unlock(&loopback_settings_mutex);
		return -EINVAL;
	}
	strncpy(hw_addr, tmp, HARDWARE_ADDRESS_LENGTH);
	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "arp -i %s -s %s %s", dst_interface, src_dummy, hw_addr);
	ret = system(cmd);
	if (ret != 0) {
		if (WIFEXITED(ret)) {
			xerror("FAIL: %s, return %d\n", cmd, WEXITSTATUS(ret));
		} else {
			xerror("FAIL: %s, child has not terminated correctly\n", cmd);
		}
		pthread_mutex_unlock(&loopback_settings_mutex);
		return -EINVAL;
	}

	memset(cmd, 0, sizeof(cmd));
	sprintf(cmd, "ping -W 5 -c 5 -I %s %s", src_interface, dst_dummy);
	xinfo("%s\n", cmd);
	ret = system(cmd);
	if (ret) {
		if (WIFEXITED(ret)) {
			xerror("FAIL: %s, return %d\n", cmd, WEXITSTATUS(ret));
		} else {
			xerror("FAIL: %s, child has not terminated correctly\n", cmd);
		}
	}

	pthread_mutex_unlock(&loopback_settings_mutex);

	if (WIFEXITED(ret)) {
		return WEXITSTATUS(ret);
	} else {
		return ret;
	}
}

/*
static int do_loopback_settings(cmd_tbl_t *cmdtp, int flag, int argc,
		char *argv[])
{
	char *src_addr = "10.50.0.1";
	char *dst_addr = "10.50.1.1";
	int ret = 0;
	if (argc < 3) {
		return cmd_usage(cmdtp);
	}

	ret = loopback_port_2_port_settings(argv[1], src_addr, argv[2], dst_addr, NULL);
	if (ret != 0) {
		printf("loopback settings for %s vs %s fail, return code = %d\n",
				argv[1], argv[2], ret);
	}

	return ret;
}

VEDIAG_CMD(lbs, 16, 0, do_loopback_settings,
		"For trying loopback settings",
		"Usage:\n" \
		"lbs <src> <dst>"
		)
*/


static int ip_to_hex(struct in_addr address)
{
	int ret_hex = 0;
	unsigned char *ucp;

	ucp = (unsigned char *) &address;
	ret_hex = (ucp[0] << 24) + (ucp[1] << 16) + (ucp[2] << 8) + ucp[3];

	return ret_hex;
}

char *get_address_from_interface_name(char *interface_name, int addr_type)
{
	struct ifreq ifr;
	char *dev = interface_name;
	char *ip_addr;
	int addr_type_macro = 0;
	char addr_type_string[30];

	pthread_mutex_lock(&interface_info_mutex);

	/* Open socket */
	int file = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
	if (file < 0) {
		xerror("Could not open socket \"%s\"(error code %d)", dev, file);
		pthread_mutex_unlock(&interface_info_mutex);
		return NULL;
	}

	/* Zero mem for ifr */
	memset(&ifr, 0, sizeof(ifr));

	/* I want IP address attached to devname */
	strcpy(ifr.ifr_name, dev);

	/* Get info */
	memset(addr_type_string, 0, 30);
	switch (addr_type) {
	case BC_ADDR:
		addr_type_macro = SIOCGIFBRDADDR;
		sprintf(addr_type_string, "broadcast address");
		break;
	case NETMASK:
		addr_type_macro = SIOCGIFNETMASK;
		sprintf(addr_type_string, "netmask");
		break;
	case IP_ADDR:
		addr_type_macro = SIOCGIFADDR;
		sprintf(addr_type_string, "IP address");
		break;
	}
	ioctl(file, addr_type_macro, &ifr);

	/* Close socket */
	close(file);

	nettestdebug("Debug Info: Index = %d | Family: %d | Name: %s\n",
			ifr.ifr_ifindex, ifr.ifr_addr.sa_family, ifr.ifr_name);

	if (ifr.ifr_addr.sa_family != 2) {
		nettestdebug("No %s for %s or %s does not exist -> Fail\n", addr_type_string, dev, dev);
		pthread_mutex_unlock(&interface_info_mutex);
		return NULL;
	} else {
		ip_addr = inet_ntoa(((struct sockaddr_in *) &ifr.ifr_addr)->sin_addr);
		nettestdebug("%s's %s: %s\n", ifr.ifr_name, addr_type_string, ip_addr);
		pthread_mutex_unlock(&interface_info_mutex);
		return ip_addr;
	}
}

static int netmask_to_decimal(char *netmask)
{
	struct in_addr netmask_in_addr;
	int netmask_hex = 0;
	int decimal = 0;

	inet_aton(netmask, &netmask_in_addr);
	netmask_hex = ip_to_hex(netmask_in_addr);
	xdebug("Netmask Hex: 0x%08x\n", netmask_hex);

	while (netmask_hex != 0) {
		netmask_hex = netmask_hex << 1;
		decimal++;
	}

	return decimal;
}

static char *get_network_address_from_ip_and_netmask(char *ip, char *netmask)
{
	struct in_addr ip_in_addr, netmask_in_addr;
	unsigned char *ip_p, *netmask_p;
	int ip_hex = 0, netmask_hex = 0;

	unsigned char net_addr_p[4];
	static char net_addr[IFNAMSIZ];
	int net_addr_hex = 0;
	int i = 0;

	inet_aton(ip, &ip_in_addr);
	ip_hex = ip_to_hex(ip_in_addr);
	xdebug("IP Hex: 0x%08x\n", ip_hex);

	inet_aton(netmask, &netmask_in_addr);
	netmask_hex = ip_to_hex(netmask_in_addr);
	xdebug("Netmask Hex: 0x%08x\n", netmask_hex);

	ip_p = (unsigned char *) &ip_in_addr;
	netmask_p = (unsigned char *) &netmask_in_addr;

	net_addr_hex = ip_hex & netmask_hex;
	xdebug("Netaddr Hex: 0x%08x\n", net_addr_hex);

	for (i = 0; i < 4; i++) {
		net_addr_p[i] = ip_p[i] & netmask_p[i];
	}

	memset(net_addr, 0, IFNAMSIZ);
	sprintf(net_addr, "%d.%d.%d.%d", net_addr_p[0], net_addr_p[1], net_addr_p[2], net_addr_p[3]);

	return net_addr;
}

int check_and_insert_route_table(FILE *rt_file, char *rt_name)
{
	char *tmp = NULL;
	static int rt_number = 100;
	char rt_line[30];
	char rt_number_str[4];
	char line[LINE_LEN];
	char cmd[CMD_LEN];
	int found = 0;


	// Check for diag route table name
	found = 0;
	fseek(rt_file, 0, SEEK_SET);
	while(fgets(line, sizeof(line), rt_file)) {
		// Look for diag route table name
		if (found == 0 && ((tmp = strstr(line, rt_name)) != NULL)) {
			found = 1;
			break;
		}
	}
	if (found == 0) {	//If not found diag route table name, check for table number
again:
		found = 0;
		fseek(rt_file, 0, SEEK_SET);
		memset(rt_number_str, 0, 4);
		sprintf(rt_number_str, "%d ", rt_number);
		while(fgets(line, sizeof(line), rt_file)) {
			// Look for result line
			if (found == 0 && ((tmp = strstr(line, rt_number_str)) != NULL)) {
				found = 1;
				break;
			}
		}
		if (found) {
			rt_number++;
			goto again;
		}

		// Done using rt_tables file
		fclose(rt_file);

		if (rt_number >= ROUTE_TABLE_MAX_ENTRY) {
			xerror("RT FILE FULL\n");
			pthread_mutex_unlock(&route_table_mutex);
			return 1;
		} else {
			// Format route entry - <num> <table_name>
			memset(rt_line, 0, 30);
			sprintf(rt_line, "%d %s", rt_number, rt_name);
			// Append in rt_tables file
			memset(cmd, 0, CMD_LEN);
			sprintf(cmd, "echo %s >> %s", rt_line, RT_PATH);
			xdebug("%s\n", cmd);
			system(cmd);
		}
	} else {	// If found diag route table name, flush the table
		memset(cmd, 0, CMD_LEN);
		sprintf(cmd, "ip route flush table %s", rt_name);
		xdebug("%s\n", cmd);
		system(cmd);

		// Done using rt_tables file
		fclose(rt_file);
	}

	return 0;
}

int remote_port_settings(char *interface_name, char *ip_addr, char *default_gateway, char *rt_name)
{
	char *tmp = NULL;
	char netmask[IFNAMSIZ];
	char net_addr[IFNAMSIZ];
	int netmask_dec = 0;
	FILE *rt_file = NULL;
	char cmd[CMD_LEN];

	pthread_mutex_lock(&remote_settings_mutex);
	// Open and check inside of rt_tables file
	pthread_mutex_lock(&route_table_mutex);
	rt_file = fopen(RT_PATH, "r");
	if (rt_file) {
		if (check_and_insert_route_table(rt_file, rt_name) != 0) {
			pthread_mutex_unlock(&route_table_mutex);
			return 1;
		}

		/* *
		 * Add new diag route table
		 * */
		// Add default gateway address to diag route table
		memset(cmd, 0, CMD_LEN);
		sprintf(cmd, "ip route add table %s default via %s dev %s", rt_name, default_gateway, interface_name);
		xdebug("%s\n", cmd);
		system(cmd);

		// Get Netmask
		tmp = get_address_from_interface_name(interface_name, NETMASK);
		if (!tmp) {
			xerror("NETMASK addr of %s not found\n", interface_name);
			return 1;
		}
		memset(netmask, 0, IFNAMSIZ);
		safe_strncpy(netmask, tmp, IFNAMSIZ);
		xdebug("netmask addr of %s: %s\n", interface_name, netmask);

		// Netmask to decimal
		netmask_dec = netmask_to_decimal(netmask);
		if (netmask_dec == 0) {
			xerror("NETMASK is invalid\n");
			return 1;
		}
		xdebug("netmask decimal of %s: %d\n", interface_name, netmask_dec);

		// Get network address
		tmp = get_network_address_from_ip_and_netmask(ip_addr, netmask);
		if (!tmp) {
			xerror("NETWORK addr of %s not found\n", interface_name);
			return 1;
		}
		memset(net_addr, 0, IFNAMSIZ);
		safe_strncpy(net_addr, tmp, IFNAMSIZ);
		xdebug("network addr of %s: %s\n", interface_name, netmask);

		// Add modified address to diag route table
		memset(cmd, 0, CMD_LEN);
		sprintf(cmd, "ip route add %s/%d dev %s src %s table %s", net_addr, netmask_dec, interface_name, ip_addr, rt_name);
		xdebug("%s\n", cmd);
		system(cmd);

		// Add route table to ip rule
		memset(cmd, 0, CMD_LEN);
		sprintf(cmd, "ip rule add table %s from %s", rt_name, ip_addr);
		xdebug("%s\n", cmd);
		system(cmd);
	} else {
		xerror("Cannot open %s\n", RT_PATH);
		pthread_mutex_unlock(&route_table_mutex);
		return 1;
	}

	pthread_mutex_unlock(&route_table_mutex);
	pthread_mutex_unlock(&remote_settings_mutex);
	return 0;
}

int delete_ip_rule(char *rt_name)
{
	int ret = 0;
	char cmd[CMD_LEN];

	pthread_mutex_lock(&remote_settings_mutex);

	memset(cmd, 0, CMD_LEN);
	sprintf(cmd, "ip rule delete from 0/0 to 0/0 table %s", rt_name);
	xdebug("%s\n", cmd);
	ret = system(cmd);

	pthread_mutex_unlock(&remote_settings_mutex);
	return ret;
}

char *polling_address_ready(char *interface_name, int milliseconds)
{
	char *tmp = NULL;
	int timeout, millisec_interval = IP_ADDRESS_POLLING_MS_INTERVAL;

	timeout = milliseconds;
	while (timeout > 0) {
		tmp = get_address_from_interface_name(interface_name, IP_ADDR);
		if (tmp) {
			break;
		}
		timeout -= millisec_interval;
		usleep(millisec_interval * 1000);
	}

	return tmp;
}

char *get_default_gateway_ip_from_file(FILE* stream)
{
	int found = 0;
	int j = 0, k = 0;
	char line[LINE_LEN];
	char end_line[LINE_LEN];
	char *tmp, *token;
	char *saveptr1 = NULL;
	char *arg[ARG_LEN];
	static char default_gateway_ip[IFNAMSIZ];

	// Malloc arg pointers
	for (j = 0; j < ARG_LEN; j++) {
		arg[j] = (char *) malloc(LINE_LEN);
		memset(arg[j], 0, LINE_LEN);
		if (arg[j] == NULL) {
			vediag_err(VEDIAG_NET, NULL, "Malloc failed\n");
			for (k = 0; k < j; k++) {
				free(arg[k]);
			}
			return NULL;
		}
	}

	found = 0;
	// Start saving log
	fseek(stream, 0, SEEK_SET);
	if(fgets(line, sizeof(line), stream)) {
		strncpy(end_line, line, LINE_LEN);
		// Seperate result line
		for (j = 0, tmp = end_line; ; j++, tmp = NULL) {
			token = strtok_r(tmp, " ", &saveptr1);
			if (token == NULL) {
				break;
			}
			strcpy(arg[j], token);
		}
		strcpy(default_gateway_ip, arg[1]);
	}

	for (j = 0; j < ARG_LEN; j++) {
		if (arg[j])
			free(arg[j]);
	}

	return default_gateway_ip;
}

static void free_netstat_info(struct netstat_info **info)
{
	struct netstat_info *tmp = *info;
	if (tmp) {
		free(tmp);
	}
}

static int get_netstat_info_from_file(char *file_name, struct netstat_info *info)
{
	pthread_mutex_lock(&netstat_mutex);
	FILE* stream;
	char line[LINE_LEN];
	int ret = 0;

	stream = fopen(file_name, "r");
	if (!stream) {
		xerror("Cannot open %s\n", file_name);
		pthread_mutex_unlock(&netstat_mutex);
		return -1;
	}

	if(fgets(line, sizeof(line), stream)) {
		sscanf(line, "%s %s %s %s %d %d %d %s",
				info->destination, info->gateway, info->genmask, info->flags, &info->MSS, &info->window, &info->irtt, info->iface);
	} else {
		ret = -1;
	}

	fclose(stream);
	pthread_mutex_unlock(&netstat_mutex);
	return ret;
}

#define DEFAULT_GATEWAY_FIND	"default_gateway_find"
char *get_default_gateway(char *interface_name)
{
	pthread_mutex_lock(&initial_config_mutex);

	//char cmd[CMD_LEN];
	char *default_gateway_ip;
	FILE *stream = NULL;
#if 0
	memset(cmd, 0, CMD_LEN);
	sprintf(cmd, "sudo dhclient -r");
	xdebug("%s\n", cmd);
	system(cmd);

	memset(cmd, 0, CMD_LEN);
	sprintf(cmd, "sudo dhclient -timeout %d %s", DHCLIENT_TIME_OUT, interface_name);
	xdebug("%s\n", cmd);
	system(cmd);

	memset(cmd, 0, CMD_LEN);
	sprintf(cmd, "sudo netstat -nr | grep \"UG\" > %s", DEFAULT_GATEWAY_FIND);
	xdebug("%s\n", cmd);
	system(cmd);
#endif
	stream = fopen(DEFAULT_GATEWAY_FIND, "r");
	if (!stream) {
		xerror("Cannot open %s\n", DEFAULT_GATEWAY_FIND);
		pthread_mutex_unlock(&initial_config_mutex);
		return NULL;
	}

	default_gateway_ip = get_default_gateway_ip_from_file(stream);
	xdebug("Default gateway is %s\n", default_gateway_ip);

	pthread_mutex_unlock(&initial_config_mutex);
	return default_gateway_ip;
}

//#define NET_CMD_FOR_DEBUG

#if defined(NET_CMD_FOR_DEBUG)
static int setup_remote_ip_rule(char *interface_name)
{
	char *tmp = NULL;
	char bc_addr[IFNAMSIZ];
	char netmask[IFNAMSIZ];
	char ip_addr[IFNAMSIZ];
	char net_addr[IFNAMSIZ];
	int netmask_dec = 0;
	static int rt_number = 100;
	FILE *rt_file = NULL;
	char rt_name[20];
	char rt_line[30];
	char rt_number_str[4];
	char line[LINE_LEN];
	char cmd[CMD_LEN];
	int found = 0;

	// Get ip address
	tmp = get_address_from_interface_name(interface_name, IP_ADDR);
	if (likely(tmp != NULL)) {
		safe_strncpy(ip_addr, tmp, IFNAMSIZ);
	} else {
		return -EIO;
	}

	// Format diag route table name
	memset(rt_name, 0, 20);
	sprintf(rt_name, "diag_%s_table", interface_name);

	// Open and check inside of rt_tables file
	rt_file = fopen(RT_PATH, "r");
	if (rt_file) {
		// Check for diag route table name
		found = 0;
		fseek(rt_file, 0, SEEK_SET);
		while(fgets(line, sizeof(line), rt_file)) {
			// Look for diag route table name
			if (found == 0 && ((tmp = strstr(line, rt_name)) != NULL)) {
				found = 1;
				break;
			}
		}
		if (found == 0) {	//If not found diag route table name, check for table number
again:
			found = 0;
			fseek(rt_file, 0, SEEK_SET);
			memset(rt_number_str, 0, 4);
			sprintf(rt_number_str, "%d ", rt_number);
			while(fgets(line, sizeof(line), rt_file)) {
				// Look for result line
				if (found == 0 && ((tmp = strstr(line, rt_number_str)) != NULL)) {
					found = 1;
					break;
				}
			}
			if (found) {
				rt_number++;
				goto again;
			}
			if (rt_number >= 253) {
				xerror("RT FILE FULL\n");
				return 1;
			} else {
				// Format route entry - <num> <table_name>
				memset(rt_line, 0, 30);
				sprintf(rt_line, "%d %s", rt_number, rt_name);
				// Append in rt_tables file
				memset(cmd, 0, CMD_LEN);
				sprintf(cmd, "echo %s >> %s", rt_line, RT_PATH);
				xinfo("%s\n", cmd);
				system(cmd);
			}
		} else {	// If found diag route table name, flush the table
			memset(cmd, 0, CMD_LEN);
			sprintf(cmd, "ip route flush table %s", rt_name);
			xinfo("%s\n", cmd);
			system(cmd);
		}

		/* *
		 * Add new diag route table
		 * */
		// Get BC addr
		tmp = get_address_from_interface_name(interface_name, BC_ADDR);
		if (!tmp) {
			xerror("BC addr of %s not found\n", interface_name);
			return 1;
		}
		memset(bc_addr, 0, IFNAMSIZ);
		safe_strncpy(bc_addr, tmp, IFNAMSIZ);
		xinfo("BC addr of %s: %s\n", interface_name, bc_addr);

		// Change to .254
		bc_addr[strlen(bc_addr) - 1]--;
		xinfo("Modify addr of %s to .254: %s\n", interface_name, bc_addr);

		// Add modified address to diag route table
		memset(cmd, 0, CMD_LEN);
		sprintf(cmd, "ip route add table %s default via %s dev %s", rt_name, bc_addr, interface_name);
		xinfo("%s\n", cmd);
		system(cmd);

		// Get Netmask
		tmp = get_address_from_interface_name(interface_name, NETMASK);
		if (!tmp) {
			xerror("NETMASK addr of %s not found\n", interface_name);
			return 1;
		}
		memset(netmask, 0, IFNAMSIZ);
		safe_strncpy(netmask, tmp, IFNAMSIZ);
		xinfo("netmask addr of %s: %s\n", interface_name, netmask);

		// Netmask to decimal
		netmask_dec = netmask_to_decimal(netmask);
		if (netmask_dec == 0) {
			xerror("NETMASK is invalid\n");
			return 1;
		}
		xinfo("netmask decimal of %s: %d\n", interface_name, netmask_dec);

		// Get network address
		tmp = get_network_address_from_ip_and_netmask(ip_addr, netmask);
		if (!tmp) {
			xerror("NETWORK addr of %s not found\n", interface_name);
			return 1;
		}
		memset(net_addr, 0, IFNAMSIZ);
		safe_strncpy(net_addr, tmp, IFNAMSIZ);
		xinfo("network addr of %s: %s\n", interface_name, netmask);

		// Add modified address to diag route table
		memset(cmd, 0, CMD_LEN);
		sprintf(cmd, "ip route add %s/%d dev %s src %s table %s", net_addr, netmask_dec, interface_name, ip_addr, rt_name);
		xinfo("%s\n", cmd);
		system(cmd);

		// Add route table to ip rule
		memset(cmd, 0, CMD_LEN);
		sprintf(cmd, "ip rule add table %s from %s", rt_name, ip_addr);
		xinfo("%s\n", cmd);
		system(cmd);
	} else {
		xerror("Cannot open %s\n", RT_PATH);
		return 1;
	}

	return 0;
}


static int do_get_broadcast(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int ret = 0;

	if (argc < 2) {
		return cmd_usage(cmdtp);
	}

	//ret = setup_remote_ip_rule(argv[1]);

	char *default_gateway_ip = NULL;
	default_gateway_ip = get_default_gateway(argv[1]);
	if (!default_gateway_ip) {
		xerror("No default gateway ip found for %s\n", argv[1]);
		ret = 1;
	} else {
		xinfo("Default gateway ip found for %s: %s\n", argv[1], default_gateway_ip);
		ret = 0;
	}


	return ret;
}


VEDIAG_CMD(getbc, 16, 0, do_get_broadcast,
		"Get Broadcast address of ethernet interface",
		"Usage:\n" \
		"getbc <interface>"
		)

#endif
