
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <cmd.h>
#include <linux/err.h>
#include <vediag_common.h>
#include <pthread.h>
#include <time.h>
#include <vediag/event.h>
#include <time_util.h>
#include <log.h>
#include <strlib.h>
#include <ras.h>
#include <circbuf.h>
#include <string_plus.h>

static LIST_HEAD(testlist);
static pthread_mutex_t _mutex = PTHREAD_MUTEX_INITIALIZER;
static uint32_t test_count;

struct event_test_list {
	struct list_head l;
	char* devname;
};

struct event_test_info {
	//char mode[40];
	int cycle;
	int timeout; /* nsec */
};

#define EVENT_DEFAULT_RUNNING_TIME		60 /* sec */
#define EVENT_MIN_RUNNING_TIME			10 /* sec */

#define EVENT_MIN_RUNNING_CYCLE		2
#define EVENT_DEF_RUNNING_CYCLE		3
#define EVENT_MIN_RUNNING_INTERVAL		100 /* msec */
#define EVENT_DEF_RUNNING_INTERVAL		500 /* msec */

#define EVENT_DEF_RUNNING_TIMEOUT		20

static int event_create_arg(struct list_head *arg_list)
{
	vediag_set_argument("timeout", "60", arg_list);
	//vediag_set_argument("mode", "interrupt", arg_list);
	vediag_set_argument("cycle", "3", arg_list);
	//vediag_set_argument("interval", "500", arg_list);

	return 0;
}

static int event_check_dev(struct vediag_test_info *info,
		struct vediag_test_info *dev_info, char **error)
{
	char name[8];
	struct event_test_info *test_arg;
	int dev;
	char *tmp;

	if (IS_ERR_OR_NULL((void *) info)) {
		vediag_err(VEDIAG_EVENT, NULL, "%s(): info NULL\n", __func__);
		return 0;
	}

	if (IS_ERR_OR_NULL((void *) dev_info)) {
		vediag_err(VEDIAG_EVENT, NULL, "%s(): dev_info NULL\n", __func__);
		return 0;
	}

	/*xmanager will change 'dev_mask' to device index (count from 1)*/
	dev = dev_info->dev_mask;

	memset(name, 0, sizeof(name));

	test_arg = (struct event_test_info *) malloc(sizeof(struct event_test_info));
	if (IS_ERR_OR_NULL(test_arg)) {
		vediag_err(VEDIAG_EVENT, dev_info->dbg_info, "Allocate memory failed\n");
		*error = strdup("Out of memory");
		return 0;
	}

	if (!dev_info->physical_dev) {
		char section[64] = { 0 };
		snprintf(section, 63, "%s%d", get_section_type(dev_info->test_type),
				dev_info->dev_mask);
		*error = strdup("Parse physical failed");
		vediag_err(VEDIAG_EVENT, dev_info->dbg_info,
				"Parse physical failed, section '%s'\n", section);
		return 0;
	}

	test_arg->cycle = EVENT_DEF_RUNNING_CYCLE;
	tmp = vediag_get_argument(dev_info, "cycle");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->cycle = (uint32_t) strtoul(tmp, NULL, 0);
			if(test_arg->cycle < EVENT_MIN_RUNNING_CYCLE)
				test_arg->cycle = EVENT_MIN_RUNNING_CYCLE;
		}
		free(tmp);
	}

	test_arg->timeout = EVENT_DEF_RUNNING_TIMEOUT;
	tmp = vediag_get_argument(dev_info, "timeout");
	if (tmp != NULL) {
		if (strlen(tmp)) {
			test_arg->timeout = (uint32_t) strtoul(tmp, NULL, 0);
			if(test_arg->timeout < EVENT_DEF_RUNNING_TIMEOUT)
				test_arg->timeout = EVENT_DEF_RUNNING_TIMEOUT;
		}
		free(tmp);
	}

	dev_info->data = (void *) test_arg;
	dev_info->threads = info->threads;
	dev_info->total_size = test_arg->cycle/*info->total_size*/;
	dev_info->dev_mask = 0;

	xinfo("\n [%s] Test arguments:\n", dev_info->device);
	xinfo("    timeout     		= %d sec\n", test_arg->timeout);
	xinfo("    cycle     		= %d\n", test_arg->cycle);

	struct event_test_list *new = malloc(sizeof(struct event_test_list));
	if (!new){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	new->devname = malloc(32);
	if (!new->devname){
		xerror("Error: malloc failed: %d\n", __LINE__);
		return 0;
	}

	list_add_tail(&new->l, &testlist);

	pthread_mutex_lock(&_mutex);
	test_count++;
	pthread_mutex_unlock(&_mutex);

    return 1;
}

static int event_test_start(int argc, char *argv[], void * _data)
{
	int arp;
	struct vediag_test_info *info = (struct vediag_test_info *) _data;
	//struct event_test_info *test_arg = (struct event_test_info *)info->data;

	xdebug("%s Entry\n", __func__);

	//info->total_size = test_arg->cycle;
	info->dev_mask = 0xffffffff;

	if ((arp = vediag_getopt(argc, argv, "-f")) > 0) {
		info->cfg_file = malloc(strlen(argv[arp + 1]));
		strcpy(info->cfg_file, argv[arp + 1]);
	}

	info->threads = 1;

	pthread_mutex_lock(&_mutex);
	test_count = 0;
	pthread_mutex_unlock(&_mutex);

	return sizeof(struct vediag_test_info);
}

static void *event_test_execute(void * __data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) __data;
	struct vediag_test_control *test_ctrl = info->test_ctrl;
	struct event_test_info *test_arg = (struct event_test_info *)info->data;
	int test_done = 0;
	struct event_test test = {0};

	test.name = info->device;
	//test.mode = &test_arg->mode[0];
	test.timeout = test_arg->timeout;
	test.cycle = test_arg->cycle;

	/* Check event# before monitoring */
	test.count1 = event_count(&test);
	sleep(1);
	
	while (!test_done) {
		if (test_ctrl->data_stop) {
			test_done = 1;
			continue;
		}

		test.count2 = event_count(&test);
		if (test.count2 > test.count1) {
			test_ctrl->tested_size += test.count2 - test.count1;
			test.count1 = test.count2;
		}
		/* Check timeout */
		if (info->stat->runtimes > test.timeout)
			test_ctrl->error = 1;

		if (test_ctrl->error) {
			vediag_err(VEDIAG_EVENT, info->dbg_info, "EVENT failed\n");
			while (!test_ctrl->test_restart) {
				sleep(1);
			}
			test_ctrl->test_restart = 0;
		}
		sleep(0);
	}

	test_ctrl->data_running--;
	pthread_exit(NULL);
}

static int event_test_complete(void *data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) data;
	struct event_test_info *test_arg = (struct event_test_info *) (info->data);
	struct event_test_list *e;
	struct list_head *pos, *q;

	pthread_mutex_lock(&_mutex);
	test_count--;
	pthread_mutex_unlock(&_mutex);

	if (test_count == 0) {
		list_for_each_entry(e, &testlist, l) {
			if (e->devname) {
				free(e->devname);
			}
		}

		list_for_each_safe(pos, q, &testlist) {
			e = list_entry(pos, struct event_test_list, l);
			list_del(pos);
			free(e);
		}
	}

	if (test_arg)
		free(test_arg);

	return 0;
}

struct vediag_test_device vediag_event = {
    .type           = VEDIAG_EVENT,
	.flags			= VEDIAG_TEST_MANUAL,
    .name           = "EVENT",
    .cmd            = "event",
    .desc           = "EVENT TEST",
	.default_app	= "event",
    .test_start     = event_test_start,
	.create_argument= event_create_arg,
    .check_dev      = event_check_dev,
    .test_execute   = event_test_execute,
    .test_complete  = event_test_complete,
};

late_initcall(diag_event_test_register)
{
    return vediag_test_register(&vediag_event);
}
