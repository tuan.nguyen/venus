
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmd.h>
#include <string.h>
#include <linux/err.h>
#include <vediagmod.h>
#include <pthread.h>
#include <vediag_common.h>
#include <dirent.h>
#include <sys/stat.h>
#include <vediag_storage.h>
#include <log.h>
#include <linux/fs.h>
#include <sys/utsname.h>
#include <sys/time.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <ras.h>
#include <time_util.h>

//#define gettid() syscall(SYS_gettid)

//#define VEDIAG_STORAGE_DEBUG
#if defined(VEDIAG_STORAGE_DEBUG)
#define sto_debug(IP, fmt, args...)	 xdebug("%s"fmt, vediag_get_IP_name(IP), ##args)
#else
#define sto_debug(IP, fmt, args...)
#endif

/******** DEF for performance test ************/
#define STORAGE_PERF_PERCENT_PASS	(70) //70%
#define STORAGE_TEST_AVL_TABLE	 {	/* FIO test Osprey SATA2 */\
	/*	name, vendor, revision, type, sizeGB, seq_rdMBs, seq_wrMBs, ran_rdMBs, ran_wrMBs, validated */\
	/* SSD disk */\
	{	"Corsair_Neutron_GTX_SSD",	NULL,		"M311",	SSD_STORAGE, 	120,	524,	309,	0,	0,	1	},\
	{	"Corsair_Neutron_XT_SSD",	NULL,	"SAFC02.2",	SSD_STORAGE, 	240,	486,	359,	0,	0,	1	},\
	{	"Samsung_SSD_850_PRO",		NULL,	"EXM02B6Q",	SSD_STORAGE, 	128,	520,	512,	0,	0,	1	},\
	{	"INTEL_SSDSC2BB120G4",		NULL,	"D2010355",	SSD_STORAGE, 	120,	297,	128,	0,	0,	1	},\
	{	"INTEL_SSDSC2BB120G4",		NULL,	"D2010370",	SSD_STORAGE, 	120,	340,	134,	0,	0,	1	},\
	{	"Micron_M500_MTFDDAK120MAV",NULL,		"MU03",	SSD_STORAGE, 	120,	499,	121,	0,	0,	1	},\
	{	"SanDisk_SDSSDHP128G",		NULL,	"X2316RL",	SSD_STORAGE, 	120,	471,	268,	0,	0,	1	},\
	{	"Samsung_SSD_840_PRO",		NULL,	"DXM06B0Q",	SSD_STORAGE, 	128,	172,	86,		0,	0,	1	},\
	{	"INTEL_SSDSC2BW120A4",		NULL,		"DC32",	SSD_STORAGE, 	120,	492,	479,	0,	0,	1	},\
	{	"KINGSTON_SKC300S37A120G",	NULL,	"507KC4",	SSD_STORAGE, 	120,	503,	500,	0,	0,	1	},\
	{	"Corsair_Force_GT",			NULL,		"5.03",	SSD_STORAGE, 	120,	502,	365,	0,	0,	1	},\
	/* HDD disk */\
	{	"WDC_WD600PF4PZ",			NULL,	"01.01C01",	HDD_STORAGE,	6000,	497,	496,	0,	0,	1	},\
	{	"ST500DM002",				NULL,		"KC45",	HDD_STORAGE,	500,	404,	92,		0,	0,	1	},\
	/* USB disk */\
	{	"USB_Flash_Drive",			"Lexar",	"1100",	USB_STORAGE,	16,		34,		1,		0,	0,	1	},\
	{	"DataTraveler_3.0",			"Kingston",	"PMAP",	USB_STORAGE,	8,		37,		6,		0,	0,	1	},\
	{	"DataTraveler_3.0",			"Kingston",	"1.01",	USB_STORAGE,	8,		28,		1,		0,	0,	1	},\
	{	"DataTraveler_3.0",			"Kingston",	"",		USB_STORAGE,	8,		28,		10,		0,	0,	1	},\
	{	"USB_2.0_FD",				"PNY",		"8192",	USB_STORAGE,	8,		32,		2,		0,	0,	1	},\
	{	"Silicon-Power16G",			"UFD_3.0",	"PMAP",	USB_STORAGE,	16,		15,		3,		0,	0,	1	},\
	{	"Silicon-Power8G",			"UFD_3.0",	"PMAP",	USB_STORAGE,	16,		15,		3,		0,	0,	1	},\
	{	"EXPRESS_ST4",				"STT",		"0.00",	USB_STORAGE,	16,		38,		3,		0,	0,	1	},\
	/* PCIe NVME */\
	{	"Samsung SSD 960 EVO 250GB",NULL, 		NULL,	PCI_NVME_STORAGE,250,	1628,	1353,	0,	0,	1	},\
	/* */\
};
struct stor_test_avl stor_test_avl_tbl[] = STORAGE_TEST_AVL_TABLE;


static int storage_check_mount_partition(struct vediag_test_info *info, char *disk_path, char *ret_mount_path);
static int storage_mount_partition(struct vediag_test_info *info, char *disk_path, char *mount_path);
static int storage_unmount_path(struct vediag_test_info *info, char *disk_path);
static int storage_get_num_partition(struct vediag_test_info *info, char *disk_path);
static int storage_get_partition_path(struct vediag_test_info *info, char *disk_path, int partition_num, char *ret_partitione_path);





/********************************************* LIB for storage partition *******************************************/


/********************************************* LIB for storage common *******************************************/
static int storage_check_path_exist(char *path)
{
	struct stat fileStat;

	if (stat(path,&fileStat) >= 0)
	    return 0;
	else
	   	return -1;
}

static int storage_create_dir(struct vediag_test_info *info, char *path)
{
	int ret = 0;

	/* Check exist */
	if (!storage_check_path_exist(path)) {
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Directory %s created before\n", path);
		goto exit;
	}

	if (mkdir(path, 0777) == 0)
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Make directory %s OK\n", path);
	else {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "Make directory %s FAIL !!\n", path);
		ret = -1;
	}

exit:
	return ret;
}

static int storage_remove_path(struct vediag_test_info *info, char *path)
{
	int ret = 0;
	char *cmd = NULL;

	/* Check exist */
	if (storage_check_path_exist(path)) {
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Directory %s no need to remove because non-exist\n", path);
		goto exit;
	}

    /* Malloc */
	cmd = malloc(TEXT_LINE_SIZE);
	if (!cmd) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc FAIL !!\n", __FUNCTION__, __LINE__);
        ret = -1;
        goto exit;
	}
    memset(cmd, 0, TEXT_LINE_SIZE);

    /* Delete */
	sprintf(cmd, "rm -rf %s", path);
	if(system(cmd) == 0) {
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Remove directory %s OK\n", path);
	} else {
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Cannot remove directory %s !!\n", path);
		ret = -1;
	}

exit:
	if (cmd)
		free(cmd);

	return ret;
}

static int storage_unmount_path(struct vediag_test_info *info, char *path)
{
	int ret = 0;
	char *cmd = NULL;

    /* Malloc */
	cmd = malloc(TEXT_LINE_SIZE);
	if (!cmd) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc FAIL !!\n", __FUNCTION__, __LINE__);
        ret = -1;
        goto exit;
	}
    memset(cmd, 0, TEXT_LINE_SIZE);

 	/* UNMount */
	sprintf(cmd, "umount %s", path);
	vediag_info(VEDIAG_STORAGE, info->dbg_info, "UN-Mount cmd %s\n", cmd);
	if(system(cmd) == 0)
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Un-mount %s OK\n", path);
	else {
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Cannot Un-mount %s !!\n", path);
		ret = -1;
		goto exit;
	}

exit:
	if (cmd)
		free(cmd);

	return ret;
}

static unsigned long long storage_get_size(struct vediag_test_info *info, char *diskpath)
{
	int fd;
	unsigned long long total_size = 0;

	fd = open(diskpath, O_RDONLY);
	if (fd < 0) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "Fail to open to get size !! \n");
		goto exit;
	}

	ioctl(fd, BLKGETSIZE64, &total_size);		/* BLKGETSIZE64 return size in bytes, BLKGETSIZE return size in blocks but device must < 2TB */
	close(fd);

	xdebug("%s size %lld\n", diskpath, total_size);

exit:
	return total_size;
}

static int storage_get_stor_info(struct vediag_test_info *info, char *diskpath, struct stor_info *stor_info)
{
	int ret = 0, linenum = 0;
	char *cmd, *buf, *line, *ptr;
	FILE* devf = NULL;

	/* Malloc */
	cmd = malloc(TEXT_LINE_SIZE);
	buf = malloc(TEXT_LINE_SIZE);
	if (!cmd || !buf) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto exit;
	}
	memset(cmd, 0, TEXT_LINE_SIZE);
	memset(buf, 0, TEXT_LINE_SIZE);

	sprintf(cmd, "udevadm info %s", diskpath);
	devf = popen(cmd, "r");
	if (!devf) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s Call FAIL !!\n", cmd);
		ret = -1;
		goto exit;
	}

	while (1) {
		memset(buf, 0, TEXT_LINE_SIZE);
		line = fgets(buf, TEXT_LINE_SIZE, devf);

		if (line == NULL) break;
		linenum++;
		//vediag_debug(VEDIAG_STORAGE, NULL, "%s", line);

		if ((ptr = strstr(line, "MODEL="))) {
			strcpy(stor_info->id_model, ptr + 6);
			if (stor_info->id_model[strlen(stor_info->id_model)] == '\n')
				stor_info->id_model[strlen(stor_info->id_model)] = 0;
		}else if ((ptr = strstr(line, "REVISION="))) {
			strcpy(stor_info->id_revision, ptr + 9);
			if (stor_info->id_revision[strlen(stor_info->id_revision)] == '\n')
				stor_info->id_revision[strlen(stor_info->id_revision)] = 0;
		}else if ((ptr = strstr(line, "SERIAL="))) {
			strcpy(stor_info->id_serial, ptr + 7);
			if (stor_info->id_serial[strlen(stor_info->id_serial)] == '\n')
				stor_info->id_serial[strlen(stor_info->id_serial)] = 0;
		}
	}

	if (pclose(devf) == -1)
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s Close FAIL !!\n", cmd);

exit:
	if (cmd)
		free(cmd);

	if (buf)
		free(buf);

	return ret;
}

static int storage_chk_hdd(struct vediag_test_info *info, char *diskpath)
{
	int ret = UNKNOWN_STORAGE;
	char *fpath, *line;
	FILE* fd = NULL;

	/* Malloc */
	fpath = malloc(TEXT_LINE_SIZE);
	line = malloc(TEXT_LINE_SIZE);
	if (!fpath || !line) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto exit;
	}
	memset(fpath, 0, TEXT_LINE_SIZE);
	memset(line, 0, TEXT_LINE_SIZE);

	/* Should get 1 for hard disks and 0 for a SSD */
	sprintf(fpath, "/sys/block/%s/queue/rotational", diskpath + 5);	//diskpath /dev/sdX

    fd = fopen(fpath, "rb");
    if (fd) {
    	if (fgets(line, TEXT_LINE_SIZE, fd) != NULL) {
    		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s = %s \n", fpath, line);
    		if (line[0] == '1')
    			ret = HDD_STORAGE;	//HDD
    		else if (line[0] == '0')
    			ret = SSD_STORAGE; 	//SSD
    		else
    			ret = UNKNOWN_STORAGE;
    	}
    	if (fclose(fd) != 0)
    		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s %d Close file fail !!\n", __FUNCTION__, __LINE__);
    } else {
    	vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s %d Open file fail !!\n", __FUNCTION__, __LINE__);
    	ret = UNKNOWN_STORAGE;
    }

    vediag_debug(VEDIAG_STORAGE, info->dbg_info, "is %s\n",
    		ret==HDD_STORAGE?"HDD":ret==SSD_STORAGE?"SSD":"Unknown");

exit:
	if (fpath)
		free(fpath);

	if (line)
		free(line);

	return ret;
}

static char *storage_alloc_n_get_apppath(int test_app)
{
	char *path = NULL, *cmd = NULL, *apppath = NULL;
	struct utsname buffer;

	/* malloc (max string length 1024) */
	path = malloc(TEXT_LINE_SIZE);
	cmd = malloc(TEXT_LINE_SIZE);
	apppath = malloc(TEXT_LINE_SIZE);
	if (!path || !cmd || !apppath) {
		xerror("%s %d Alloc error !!\n", __FUNCTION__, __LINE__);
		goto err_exit;
	}
	memset(path, 0, TEXT_LINE_SIZE);
	memset(cmd, 0, TEXT_LINE_SIZE);
	memset(apppath, 0, TEXT_LINE_SIZE);

	/* FIO check */
	if (test_app == STORAGE_FIO_TEST) {
		if (uname(&buffer) == 0) {
			if (strcmp(buffer.machine, "aarch64") == 0 ||
				strcmp(buffer.machine, "x86_64") == 0) {
				sprintf(path, "%s/fio_%s", STORAGE_INT_FIO_PATH, buffer.machine);
				sprintf(cmd, "%s --version", path);
			}
		}

		if (strlen(path) && !system(cmd))	// internal app is first check
			strcpy(apppath, path);
		else if (!system("fio --version"))
			strcpy(apppath, "fio");
	} else if (test_app == STORAGE_EXTERNAL_FIO_TEST) {
		if (!system("fio --version"))
			strcpy(apppath, "fio");
	} else if (test_app == STORAGE_INTERNAL_FIO_TEST) {
		if (uname(&buffer) == 0) {
			if (strcmp(buffer.machine, "aarch64") == 0 ||
				strcmp(buffer.machine, "x86_64") == 0) {
				sprintf(path, "%s/fio_%s", STORAGE_INT_FIO_PATH, buffer.machine);
				sprintf(cmd, "%s --version", path);
			}
		}

		if (strlen(path) && !system(cmd))
			strcpy(apppath, path);
	}

	/* Stressapp check */
	if (test_app == STORAGE_STRESSAPP_TEST) {
		sprintf(path, "%s/stressapptest", STORAGE_INT_STRESSAPP_PATH);
		sprintf(cmd, "%s --help", path);

		if (strlen(path) && !system(cmd))	// internal app is first check
			strcpy(apppath, path);
		else if (!system("stressapptest --version"))
			strcpy(apppath, "stressapptest");
	} else if (test_app == STORAGE_EXTERNAL_STRESSAPP_TEST) {
		if (!system("stressapptest --version"))
			strcpy(apppath, "fio");
	} else if (test_app == STORAGE_INTERNAL_STRESSAPP_TEST) {
		sprintf(path, "%s/stressapptest", STORAGE_INT_STRESSAPP_PATH);
		sprintf(cmd, "%s --version", path);

		if (strlen(path) && !system(cmd))
			strcpy(apppath, path);
	}

	/* RW by low level function */
	if (test_app == STORAGE_RW_TEST)
		strcpy(apppath, "RW lowlvl");

	goto exit;

err_exit:
	if (apppath)
		free(apppath);
	apppath = NULL;

exit:
	if (path)
		free(path);

	if (cmd)
		free(cmd);

	return apppath;
}

static int storage_check_rootfs(struct vediag_test_info *info, char *disk_path)
{
	int i, num, rootfs = 0;
	FILE *devf;
	int line_num = 0;
	char *line;
	char *buf = NULL;
	char *partpath = NULL;

    /* Malloc */
	partpath = malloc(64);
    buf = malloc(TEXT_LINE_SIZE);
 	if (!partpath || !buf) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc fail\n", __FUNCTION__, __LINE__);
		rootfs = -1;
        goto exit;
	}
	memset(partpath, 0, 64);
    memset(buf, 0, TEXT_LINE_SIZE);

	num = storage_get_num_partition(info, disk_path);
	if (num == -1) {
		rootfs = -1;
		goto exit;
	}

	i = 0;
	do {
		if (num == 0)
			sprintf(partpath, "%s", disk_path);
		else {
			if (storage_get_partition_path(info, disk_path, i + 1, partpath)) {	//partition count from 1
				vediag_err(VEDIAG_STORAGE, info->dbg_info, "Not find partition for test !!\n");
				rootfs = -1;
				goto exit;
			}
		}

		//ubuntu
		if (!rootfs) {
			line_num = 0;
			devf = popen("findmnt -runo source,target /", "r");
			if (devf != NULL) {
				while (1) {
					memset(buf, 0, TEXT_LINE_SIZE);
					line = fgets(buf, TEXT_LINE_SIZE, devf);

					if (line == NULL) break;
					line_num++;

					if (line_num == 1 && strncmp(partpath, line, strlen(partpath)) == 0)
						rootfs = 1;
				}
				pclose(devf);
			}
		} else {
			xdebug("%d %s NULL !\n", __LINE__, "findmnt -runo source,target /");
		}

		//centos
		if (!rootfs) {
			line_num = 0;
			devf = popen("findmnt -runo source,target /boot", "r");
			if (devf != NULL) {
				while (1) {
					memset(buf, 0, TEXT_LINE_SIZE);
					line = fgets(buf, TEXT_LINE_SIZE, devf);

					if (line == NULL) break;
					line_num++;

					if (line_num == 1 && strncmp(partpath, line, strlen(partpath)) == 0)
						rootfs = 1;
				}
				pclose(devf);
			}
		} else {
			xdebug("%d %s NULL !\n", __LINE__, "findmnt -runo source,target /boot");
		}

		if (rootfs)
			break;

		i++;
	}while (i < num);

exit:
 	if (partpath)
 		free(partpath);

 	if (buf)
 		free(buf);

	return rootfs;
}

static int storage_get_num_partition(struct vediag_test_info *info, char *disk_path)
{
	char *cmd = NULL, *buf = NULL;
	int linenum = 0, i, num = 0;

	FILE *devf;
	char *line;

    /* Malloc */
    cmd = malloc(TEXT_LINE_SIZE);
    buf = malloc(TEXT_LINE_SIZE);
	if (!cmd || !buf) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc FAIL !!\n", __FUNCTION__, __LINE__);
        num = -1;
        goto exit;
	}
    memset(cmd, 0, TEXT_LINE_SIZE);
    memset(buf, 0, TEXT_LINE_SIZE);

 	sprintf(cmd, "lsblk -p %s -o name", disk_path);

	devf = popen(cmd, "r");
    if (!devf) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s Call FAIL !!\n", cmd);
        num = -1;
    	goto exit;
    }

	while (1) {
		memset(buf, 0, TEXT_LINE_SIZE);
		line = fgets(buf, TEXT_LINE_SIZE, devf);

		if (line == NULL) break;
		linenum++;
		//vediag_debug(VEDIAG_STORAGE, NULL, "%s", line);

		if (linenum > 2) {
			for(i = 0; i < strlen(line); i++) {
				if (strncmp(line + i, disk_path, strlen(disk_path)) == 0) {
					num++;
					break;
				}
			}
		}
	}

	if (pclose(devf) == -1)
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s Close FAIL !!\n", cmd);

	vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Partition num %d\n", num);

exit:
	if (cmd)
		free(cmd);

	if (buf)
		free(buf);

	return num;
}

static int storage_get_partition_path(struct vediag_test_info *info, char *disk_path, int partition_num, char *ret_partitione_path)
{
	char *cmd = NULL, *buf = NULL;
	int linenum = 0, i, ret = 0;

	FILE *devf;
	char *line;

	if (!ret_partitione_path) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Not input return pointer !!\n", __FUNCTION__, __LINE__);
        ret = -1;
        goto exit;
	}

    /* Malloc */
    cmd = malloc(TEXT_LINE_SIZE);
    buf = malloc(TEXT_LINE_SIZE);
	if (!cmd || !buf) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc FAIL !!\n", __FUNCTION__, __LINE__);
        ret = -1;
        goto exit;
	}
    memset(cmd, 0, TEXT_LINE_SIZE);
    memset(buf, 0, TEXT_LINE_SIZE);

 	sprintf(cmd, "lsblk -p %s -o name", disk_path);

	devf = popen(cmd, "r");
    if (!devf) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s Call FAIL !!\n", cmd);
        ret = -1;
    	goto exit;
    }

	while (1) {
		memset(buf, 0, TEXT_LINE_SIZE);
		line = fgets(buf, TEXT_LINE_SIZE, devf);

		if (line == NULL) break;
		linenum++;
		//vediag_debug(VEDIAG_STORAGE, NULL, "%s", line);

		if (linenum == (partition_num + 2)) {	//partition_num = 1, 2, 3, ...
			for(i = 0; i < strlen(line); i++) {
				if (strncmp(line + i, disk_path, strlen(disk_path)) == 0) {
					strcpy(ret_partitione_path, line + i);
					break;
				}
			}
		}
	}

	if (pclose(devf) == -1)
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s Close FAIL !!\n", cmd);

	if (!strlen(ret_partitione_path)) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "Cannot find name of partition %d !!\n", partition_num);
        ret = -1;
    	goto exit;
	}else {
		if (ret_partitione_path[strlen(ret_partitione_path) - 1] == '\n')
			ret_partitione_path[strlen(ret_partitione_path) - 1] = 0;
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Partition %d: %s \n", partition_num, ret_partitione_path);
	}

exit:
	if (cmd)
		free(cmd);

	if (buf)
		free(buf);

	return ret;
}

static int storage_check_format_partition(struct vediag_test_info *info, char *disk_path, char *ret_format_type)
{
	char *cmd = NULL, *buf = NULL;
	char diskname[16], fstype[16];
	int linenum = 0, i, ret = 0;

	FILE *devf;
	char *line;

    /* Malloc */
    cmd = malloc(TEXT_LINE_SIZE);
    buf = malloc(TEXT_LINE_SIZE);
	if (!cmd || !buf) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc FAIL !!\n", __FUNCTION__, __LINE__);
        ret = -1;
        goto exit;
	}
    memset(cmd, 0, TEXT_LINE_SIZE);
    memset(buf, 0, TEXT_LINE_SIZE);

    memset(diskname, 0, sizeof(diskname));
    memset(fstype, 0, sizeof(fstype));

 	sprintf(cmd, "lsblk -f %s -o name,fstype", disk_path);
	sprintf(diskname, "%s", disk_path + 5);

	devf = popen(cmd, "r");
    if (!devf) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s Call FAIL !!\n", cmd);
        ret = -1;
    	goto exit;
    }

	while (1) {
		memset(buf, 0, TEXT_LINE_SIZE);
		line = fgets(buf, TEXT_LINE_SIZE, devf);

		if (line == NULL) break;
		linenum++;
		vediag_debug(VEDIAG_STORAGE, NULL, "%s", line);

		if (linenum == 2 && strncmp(diskname, line, strlen(diskname)) == 0) {
			for(i = strlen(diskname); i < strlen(line); i++) {
				if( (line[i] >= 'a' && line[i] <= 'z') || (line[i] >= 'A' && line[i] <= 'Z')) {
					vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s formatted before\n", disk_path);
					strcpy(fstype, line + i);
					break;
				}
			}
		}
	}

	if (pclose(devf) == -1)
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s Close FAIL !!\n", cmd);

	if (strlen(fstype)) {
		if (ret_format_type)
			strcpy(ret_format_type, fstype);
	}else {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "Check not format !!\n");
		ret = -1;
	}

exit:
	if (cmd)
		free(cmd);

	if (buf)
		free(buf);

	return ret;
}

static int storage_format_partition(struct vediag_test_info *info, char *disk_path, char *format_type)
{
#define DEFAULT_FORMAT_TYPE 	 "vfat"
	int ret = 0;
	char *type = NULL;
	char *cmd = NULL;

    /* Malloc */
	cmd = malloc(TEXT_LINE_SIZE);
	if (!cmd) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc FAIL !!\n", __FUNCTION__, __LINE__);
        ret = -1;
        goto exit;
	}
    memset(cmd, 0, TEXT_LINE_SIZE);

    /* Unmount before format if have */
    if (!storage_check_mount_partition(info, disk_path, NULL)) {
    	if (storage_unmount_path(info, disk_path)) {
    		vediag_err(VEDIAG_STORAGE, info->dbg_info, "Cannot Un-mount to format !!\n");
    		ret = -1;
    		goto exit;
    	}
    }

    /* Format call */
    type = format_type?format_type:DEFAULT_FORMAT_TYPE;

	sprintf(cmd, "mkfs.%s %s -I", type, disk_path);
	vediag_info(VEDIAG_STORAGE, info->dbg_info, "Format %s\n", cmd);
	if(system(cmd) == 0) {
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Format %s OK\n", type);
	} else {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "Format %s FAIL !!\n", type);
		ret = -1;
		goto exit;
	}

exit:
	if (cmd)
		free(cmd);

	return ret;
}

static int storage_check_mount_partition(struct vediag_test_info *info, char *disk_path, char *ret_mount_path)
{
	int ret = 0;
	char *cmd = NULL, *mountpath = NULL, *buf = NULL;;

	FILE *devf;
	char *line;
	int i, linenum = 0;

    /* Malloc */
	cmd = malloc(TEXT_LINE_SIZE);
	mountpath = malloc(TEXT_LINE_SIZE);
	buf = malloc(TEXT_LINE_SIZE);
	if (!cmd || !mountpath || !buf) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc FAIL !!\n", __FUNCTION__, __LINE__);
        ret = -1;
        goto exit;
	}
    memset(cmd, 0, TEXT_LINE_SIZE);
    memset(mountpath, 0, TEXT_LINE_SIZE);
    memset(buf, 0, TEXT_LINE_SIZE);

    /* Check mount */
	sprintf(cmd, "findmnt %s", disk_path);
	vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Check mount %s\n", cmd);

	devf = popen(cmd, "r");
    if (!devf) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s Call FAIL !!\n", cmd);
        ret = -1;
    	goto exit;
    }

	while (1) {
		memset(buf, 0, TEXT_LINE_SIZE);
		line = fgets(buf, TEXT_LINE_SIZE, devf);

		if (line == NULL) break;
		linenum++;
		vediag_debug(VEDIAG_STORAGE, NULL, "%s", line);

		if (linenum == 2) {
			for(i = 0; i < strlen(line); i++) {
				if (line[i] == ' ')
					break;
			}

			if (!strncmp(line + i + 1, disk_path, strlen(disk_path)))
				strncpy(mountpath, line, i);
		}
	}

	if (pclose(devf) == -1)
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s Close FAIL !!\n", cmd);

	if (strlen(mountpath)) {
		if (ret_mount_path)
			strcpy(ret_mount_path, mountpath);
	}else {
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Check not mount !!\n");
		ret = -1;
	}

exit:
	if (cmd)
		free(cmd);

	if (mountpath)
		free(mountpath);

	if (buf)
		free(buf);

	return ret;
}

static int storage_mount_partition(struct vediag_test_info *info, char *disk_path, char *mount_path)
{
	int ret = 0;
	char *cmd = NULL;

    /* Malloc */
	cmd = malloc(TEXT_LINE_SIZE);
	if (!cmd) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc FAIL !!\n", __FUNCTION__, __LINE__);
        ret = -1;
        goto exit;
	}
    memset(cmd, 0, TEXT_LINE_SIZE);

    /* Create dir for mount */
	storage_remove_path(info, mount_path);
	if (storage_create_dir(info, mount_path)) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "Make directory %s FAIL !!\n", mount_path);
		ret = -1;
		goto exit;
	}

	/* Unmount to make sure destination dir not keep by another mount */
	storage_unmount_path(info, mount_path);
	sprintf(cmd, "mount %s %s", disk_path, mount_path);
	vediag_info(VEDIAG_STORAGE, info->dbg_info, "Mount cmd %s\n", cmd);
	if(system(cmd) == 0)
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "mount to %s OK\n", mount_path);
	else {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "mount to %s FAIL !!\n", mount_path);
		ret = -1;
		goto exit;
	}

exit:
	if (cmd)
		free(cmd);

	return ret;
}

static pthread_mutex_t _mutex = PTHREAD_MUTEX_INITIALIZER;

static char *storage_alloc_n_make_testpath_dir(struct vediag_test_info *info, char *disk_path)
{
	int ret = 0;
	char partpath[16];
	char fstype[16];
	struct timeval tm;

	char *dirname = NULL, *mountpath = NULL;
	int format_done = 0, mount_done = 0;
	int i, num, rootfs;

    pthread_mutex_lock(&_mutex);	// SHOULD improve mutex for each disk_path

    /* Malloc */
	dirname = malloc(TEXT_LINE_SIZE);
	mountpath = malloc(TEXT_LINE_SIZE);
	if (!dirname || !mountpath) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc fail\n", __FUNCTION__, __LINE__);
        ret = -1;
        goto exit;
	}
    memset(dirname, 0, TEXT_LINE_SIZE);
    memset(mountpath, 0, TEXT_LINE_SIZE);

    /* Format and mount
     * 1. Disk had no partition: if format -> use, otherwise -> format
     * 2. Disk had partitions: if a partition format -> use, otherwise -> find next partition
     * until SKIP, because maybe it's raw partition
     */
	rootfs = storage_check_rootfs(info, disk_path);
	if (rootfs < 0) {
        ret = -1;
        goto exit;
	} else if (rootfs > 0) {	//create folder in /tmp
		vediag_info(VEDIAG_STORAGE, info->dbg_info, "is rootfs\n");
		goto make_dir;
	}

	num = storage_get_num_partition(info, disk_path);
	if (num == -1) {
		ret = -1;
		goto exit;
	}

	i = 0;
	do {
		if (num == 0)
			sprintf(partpath, "%s", disk_path);
		else {
			if (storage_get_partition_path(info, disk_path, i + 1, partpath)) {	//partition count from 1
				vediag_err(VEDIAG_STORAGE, info->dbg_info, "Not find partition for test !!\n");
				ret = -1;
				goto exit;
			}
		}

		if (storage_check_format_partition(info, partpath, fstype)) {
			vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s not yet format\n", partpath);

			if (num == 0) {
				if (storage_format_partition(info, partpath, NULL)) {
					vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s format FAIL !!\n", partpath);
					ret = -1;
					goto exit;
				}else {
					vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s format OK\n", partpath);
					format_done = 1;
				}
			}
		}else {
			vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s formatted\n", partpath);
			format_done = 1;
			//fstype must vfat??
		}

		if (format_done) {
			if (storage_check_mount_partition(info, partpath, mountpath)) {
				memset(mountpath, 0, TEXT_LINE_SIZE);
				sprintf(mountpath, "/media/%s_%s", STORAGE_FILE_TEST_NAME, disk_path + 5);
				if (storage_mount_partition(info, partpath, mountpath)) {
					vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s mount FAIL !!\n", partpath);
					ret = -1;
					goto exit;
				}else {
					vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s mount OK\n", partpath);
					mount_done = 1;
				}
			}else {
				vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s mounted\n", partpath);
				mount_done = 1;
			}
		}

		if (mount_done)
			break;

		i++;
		format_done = 0;	// new check for next partition
	}while (i < num);

	if (!mount_done) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "Not have partition able to use for mount !!\n");
		ret = -1;
		goto exit;
	}

	vediag_info(VEDIAG_STORAGE, info->dbg_info, "Mounted %s to %s for test\n", partpath, mountpath);

    /* Create directory - path: mount_path/diag_stor_sdX_tid_id_timeusec */
make_dir:
	gettimeofday(&tm, NULL);
	if (rootfs > 0)
	    sprintf(dirname, "/tmp/%s_%s_%lu_%ld", STORAGE_FILE_TEST_NAME, disk_path + 5, pthread_self(), tm.tv_usec);
	 else
	    sprintf(dirname, "%s/%s_%s_%lu_%ld", mountpath, STORAGE_FILE_TEST_NAME, disk_path + 5, pthread_self(), tm.tv_usec);

	storage_remove_path(info, dirname);
	if (storage_create_dir(info, dirname)) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "Create dir %s FAIL !!\n", dirname);
		ret = -1;
		goto exit;
	} else {
		vediag_info(VEDIAG_STORAGE, info->dbg_info, "Created %s for test\n", dirname);
	}

exit:
	if(mountpath)
		free(mountpath);

	if (ret) {
		if(dirname)
			free(dirname);
		dirname = NULL;
	}

	//make sure unlock
	pthread_mutex_unlock(&_mutex);

	return dirname;
}
#define READP  		0
#define WRITEP 		1

#define CMD_TIMEOUT_SECS	300
#define KILL_TIMEOUT_SECS	10

#define CMD_ARG_MAX  	32
#define CMD_ARG_SIZE 	128
#define TIMEOUT_CMD_FIO 90
#define TIMEOUT_CMD_PER 30
#define TIMEOUT_CMD_STO 15

int storage_app_call_process(struct vediag_test_info *info, char *cmd, unsigned long timeout_secs)
{
	char *cmdbak = NULL, *arg[CMD_ARG_MAX];
	char *ptr;
	int child_ret = 0, ret = 0;
    char *rest = NULL;
    u32 i;
    pid_t childpid = 0;
	pid_t waitid = 0;
	int status = 0;
	struct timeval start_time, current_time, stop_time;
	struct vediag_test_control *test_ctrl = info->test_ctrl;

    /* Malloc */
	cmdbak = malloc(TEXT_LINE_SIZE);
	if (!cmdbak) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc FAIL !!\n", __FUNCTION__, __LINE__);
        ret = -1;
        goto exit;
	}
    memset(cmdbak, 0, TEXT_LINE_SIZE);

	for (i = 0; i < CMD_ARG_MAX; i++) {
		arg[i] = (char *) malloc(CMD_ARG_SIZE);
		memset(arg[i], 0, CMD_ARG_SIZE);
		if (!arg[i]) {
			vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc FAIL !!\n", __FUNCTION__, __LINE__);
	        ret = -1;
	        goto exit;
		}
	}
	/* Parse command string to argv array (last argv must null) */
	strncpy(cmdbak, cmd, TEXT_LINE_SIZE);
    i = 0;
    rest = cmdbak;
    while (i < CMD_ARG_MAX && (ptr = strtok_r(rest, " ", &rest))) {
    	strncpy(arg[i], ptr, CMD_ARG_SIZE);
    	//printf("arg[%d] %s\n", i, arg[i]);
        i++;
    }
	    for (; i < CMD_ARG_MAX; i++) {
    	if (arg[i]) {
    		free(arg[i]);
    		arg[i] = NULL;
    	}
    }
	// child_ret = execvp(arg[0], arg);

	gettimeofday(&start_time, NULL);
	gettimeofday(&current_time, NULL);

	/* Create child process */
	childpid = fork();

	if (childpid < 0) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info,
				"%s %d Fork child error\n", __FUNCTION__, __LINE__);
		ret = -1;
		goto exit;
	}

	/* Child process jump here */
	if (childpid == 0) {
		// devf = popen(cmd, "r");
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Child] Check child PID %d Running\n",
								childpid);
		child_ret = execvp(arg[0], arg);
	} else {
		u32 stop = 0, stop_kill = 0;
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Parent] Check child PID %d Running\n",
									childpid);
		while(current_time.tv_sec < start_time.tv_sec + timeout_secs) {
			gettimeofday(&current_time, NULL);
			if (test_ctrl->data_stop) {
				if(stop == 0) {
					gettimeofday(&stop_time, NULL);
					stop = 1;
				}
				if((current_time.tv_sec - stop_time.tv_sec) > TIMEOUT_CMD_STO) {
					stop_kill = 1;
					break;
				}
			}
			waitid = waitpid(childpid, &status, WNOHANG);
			if(waitid == childpid) {
				vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Parent] Check child PID %d just exited (%d), status: %d)\n",
					childpid, waitid, status);
				break;
			}
		}
		if ((stop_kill) || (current_time.tv_sec >= start_time.tv_sec + timeout_secs)) {
			int killtimeout = KILL_TIMEOUT_SECS;
			/* Check timeout and kill child if need */

			vediag_info(VEDIAG_STORAGE, info->dbg_info, "[Parent] child PID %d %d %d was timeout %d --> kill\n", childpid, waitid, status, killtimeout);
			kill(childpid, SIGTERM);

			// Because client command is child of diagwrt, need to clean up <defunct> by waitpid
			while (killtimeout >= 0 && waitpid(childpid, &status, WNOHANG) != -1) {
				usleep(1000000);
				killtimeout--;
			}

			if (killtimeout < 0) {
				vediag_info(VEDIAG_STORAGE, info->dbg_info, "[Parent] Kill child PID %d UN-successful (will force kill)\n", childpid);
				kill(childpid, SIGKILL);

				// clean up <defunct> by waitpid
				killtimeout = KILL_TIMEOUT_SECS;
				while (killtimeout >= 0 && waitpid(childpid, &status, WNOHANG) != -1) {
					usleep(1000000);
					killtimeout--;
				}
			} else
				vediag_info(VEDIAG_STORAGE, info->dbg_info, "[Parent] Kill child PID %d successful\n", childpid);
			ret = 1;
		}

		goto exit;
	}

exit:
	if (cmdbak)
		free(cmdbak);

	for (i = 0; i < CMD_ARG_MAX; i++) {
		if (arg[i])
			free(arg[i]);
	}

	return ret;
}

int storage_app_fork_call(struct vediag_test_info *info, char *cmd, unsigned long timeout_secs,
		char *buff_output, unsigned long buff_size)
{
	int i = 0, ret = 0, child_ret = 0;

	char *cmdbak = NULL, *arg[CMD_ARG_MAX];
    char *rest = NULL;

	int fd[2], piped = 0;
	pid_t childpid = 0;
	pid_t waitid = 0;
	int status = 0;
	int cmdtimeout = 0, killtimeout = 0;
	struct timeval start_time, current_time;
	unsigned long time_passed = 0;

    unsigned long rdsize, rdcnt;
    int rddone = 0;
	char *ptr;

    /* Malloc */
	cmdbak = malloc(TEXT_LINE_SIZE);
	if (!cmdbak) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc FAIL !!\n", __FUNCTION__, __LINE__);
        ret = -1;
        goto exit;
	}
    memset(cmdbak, 0, TEXT_LINE_SIZE);

	for (i = 0; i < CMD_ARG_MAX; i++) {
		arg[i] = (char *) malloc(CMD_ARG_SIZE);
		memset(arg[i], 0, CMD_ARG_SIZE);
		if (!arg[i]) {
			vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc FAIL !!\n", __FUNCTION__, __LINE__);
	        ret = -1;
	        goto exit;
		}
	}

	/* Parse command string to argv array (last argv must null) */
	strncpy(cmdbak, cmd, TEXT_LINE_SIZE);
    i = 0;
    rest = cmdbak;
    while (i < CMD_ARG_MAX && (ptr = strtok_r(rest, " ", &rest))) {
    	strncpy(arg[i], ptr, CMD_ARG_SIZE);
    	//printf("arg[%d] %s\n", i, arg[i]);
        i++;
    }

    // Clear remains argument for execvp know the last position of args
    for (; i < CMD_ARG_MAX; i++) {
    	if (arg[i]) {
    		free(arg[i]);
    		arg[i] = NULL;
    	}
    }

	/* Create pipe */
	if (pipe(fd) < 0) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info,
				"%s %d Create pipe failed: %s\n", __FUNCTION__, __LINE__, strerror(errno));
        ret = -1;
        goto exit;
	}
	vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Create pipe successful\n");
	piped = 1;

	/* Create child process */
	childpid = fork();

	if (childpid < 0) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info,
				"%s %d Fork child error\n", __FUNCTION__, __LINE__);

		close(fd[READP]);
		close(fd[WRITEP]);
		ret = -1;
		goto exit;
	}

	/* Child process jump here */
	if (childpid == 0) {
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Child PID %ld] Start here\n", getpid());

		// redirect stdout & stderr to parent pipe
		int newfd;

		newfd = dup2(fd[WRITEP], STDOUT_FILENO);
		if (newfd == -1) {
			vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Child PID %ld] Cannot direct STDOUT to parent, error %s\n", getpid(), strerror(errno));
			child_ret = -1;
			close(fd[READP]);
			close(fd[WRITEP]);
			goto exit_child;
		}

		newfd = dup2(fd[WRITEP], STDERR_FILENO);
		if (newfd == -1) {
			vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Child PID %ld] Cannot direct STDERR to parent, error %s\n", getpid(), strerror(errno));
			child_ret = -1;
			close(fd[READP]);
			close(fd[WRITEP]);
			goto exit_child;
		}

		// close not-required pipes by child
		close(fd[READP]);
		close(fd[WRITEP]);

		// Execute command
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Child PID %ld] Execute command\n", getpid());
		child_ret = execvp(arg[0], arg);
		if (child_ret)
			vediag_err(VEDIAG_STORAGE, info->dbg_info,
					"[Child PID %d] Fail to execute command, error '%s'\n", getpid(), strerror(errno));
exit_child:
		exit(child_ret);
	} else {

		/* Parent process continue from here*/
		cmdtimeout = (timeout_secs > 5 * KILL_TIMEOUT_SECS) ? (timeout_secs - KILL_TIMEOUT_SECS) :
				(timeout_secs > 0) ? timeout_secs : CMD_TIMEOUT_SECS;
		killtimeout = KILL_TIMEOUT_SECS;
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Parent] Created child PID %d (timeout %d secs)\n",
				childpid, cmdtimeout + killtimeout);

		// close not required pipe by parent
		close(fd[WRITEP]);

		/* Wait child process */
		time_passed = 0;
		ptr = buff_output;
		rdsize = buff_size > TEXT_LINE_SIZE?TEXT_LINE_SIZE:buff_size;
		gettimeofday(&start_time, NULL);
		while (time_passed <= cmdtimeout) {
			// check child exited or not
			waitid = waitpid(childpid, &status, WNOHANG);

#if 0
			if (waitid == -1)
				vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s [Parent] Cannot check child PID %d status !!\n",
						childpid, waitid);
			else if (waitid == 0) {
				//vediag_debug(VEDIAG_STORAGE, "%s [Parent] Child PID %d is running\n", physical_dev, childpid);
			} else {	// child was exited or terminated
				vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Parent] Check child PID %d just exited (%d), status: %d)\n",
						childpid, waitid, status);
				break;
			}
#else
			if (status || waitid == childpid) {
				vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Parent] Check child PID %d just exited (%d), status: %d)\n",
											childpid, waitid, status);
				break;
			}
#endif

			// read child output if had buffer
			if (buff_output && !rddone) {
				rdcnt = read(fd[READP], ptr, rdsize);
				if (rdcnt == -1) {
					if (errno == EINTR) {
						vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Parent] Cannot read child PID %d pipe because it was interrupted !!\n",
								childpid);
					} else {
						vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Parent] Cannot read child PID %d pipe, error %s!!\n",
								childpid, strerror(errno));
					}
				} else if (rdcnt == 0) {
					rddone = 1;
				} else {
					ptr += rdcnt;

					// Check buffer overflow
					if (ptr + rdsize > buff_output + buff_size) {
						rdsize = buff_output + buff_size - (buff_output + buff_size);
						vediag_debug(VEDIAG_STORAGE, info->dbg_info,
								"[Parent] child PID %d output reach max buffer given --> decrease last read size %d to %d\n",
								childpid, TEXT_LINE_SIZE, rdsize);
					}else if (ptr >= buff_output + buff_size) {
						vediag_info(VEDIAG_STORAGE, info->dbg_info,
								"[Parent] child PID %d output oversize buffer given --> Stop read !!\n",
								childpid);
						rddone = 1;
					}
				}
			}

			// get time again to check timeout
			gettimeofday(&current_time, NULL);
			time_passed = current_time.tv_sec - start_time.tv_sec;

			// print time if need
			//vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Parent] Wait child PID %d at %lu seconds\n",
			//					childpid, time_passed);
		}

		/* Check timeout and kill child if need */
		if (time_passed > cmdtimeout) {
			vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Parent] child PID %d was timeout %d --> kill\n", childpid, cmdtimeout);
			kill(childpid, SIGTERM);

			// Because client command is child of diagwrt, need to clean up <defunct> by waitpid
			while (killtimeout >= 0 && waitpid(childpid, &status, WNOHANG) != -1) {
				usleep(1000000);
				killtimeout--;
			}

			if (killtimeout < 0) {
				vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Parent] Kill child PID %d UN-successful (will force kill)\n", childpid);
				kill(childpid, SIGKILL);

				// clean up <defunct> by waitpid
				killtimeout = KILL_TIMEOUT_SECS;
				while (killtimeout >= 0 && waitpid(childpid, &status, WNOHANG) != -1) {
					usleep(1000000);
					killtimeout--;
				}
			} else
				vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Parent] Kill child PID %d successful\n", childpid);

			ret = 1;
		} else if (status != 0) {
			if (WIFEXITED(status)) {
				vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Parent] child PID %d status %d, exited child_ret %d\n",
						childpid, status, WEXITSTATUS(status));
				ret = 2;
			} else if (WIFSIGNALED(status)) {
				vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Parent] child PID %d status %d, terminated by signal %d\n",
						childpid, status, WTERMSIG(status));
				ret = 3;
			} else if (WIFSTOPPED(status)) {
				vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Parent] child PID %d status %d, stopped by signal %d\n",
						childpid, status, WIFSTOPPED(status));
				ret = 4;
			} else {
				vediag_debug(VEDIAG_STORAGE, info->dbg_info, "[Parent] child PID %d status %d, terminated incorrectly (unknown)\n",
						childpid, status);
				ret = 5;
			}
		}
	}

exit:
	if (cmdbak)
		free(cmdbak);

	for (i = 0; i < CMD_ARG_MAX; i++) {
		if (arg[i])
			free(arg[i]);
	}

	if (childpid > 0 && piped) {
		close(fd[READP]);
	}

	return ret;
}

int storage_perf_result_check(struct vediag_test_info *info,
		struct stor_test_result *result, int mode)
{
	int ret = 0, i, found = 0;
	char *cmd = NULL, *buf = NULL;
	struct stor_info *storinfo;

	struct vediag_storage_test_info *test_arg = (struct vediag_storage_test_info *) info->data;
	char *diskpath = test_arg->diskpath;
	char *phypath = test_arg->phypath;
	unsigned long long currspd_bs = 0;
	int type = UNKNOWN_STORAGE;

	unsigned long long expspd_bs = 0, lowestspd_bs = 0;
	int avlnum = sizeof(stor_test_avl_tbl)/sizeof(struct stor_test_avl);

    /* Malloc */
    cmd = malloc(TEXT_LINE_SIZE);
    buf = malloc(TEXT_LINE_SIZE);
    storinfo = malloc(sizeof(struct stor_info));
	if (!cmd || !buf || !storinfo) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc FAIL !!\n", __FUNCTION__, __LINE__);
        ret = -1;
        goto exit;
	}
    memset(cmd, 0, TEXT_LINE_SIZE);
    memset(buf, 0, TEXT_LINE_SIZE);
    memset(storinfo, 0, sizeof(struct stor_info));

    /* Check mode */
    if (result->rddone) {
    	currspd_bs = result->rdspd_avr;
    }else if (result->wrdone) {
    	currspd_bs = result->wrspd_avr;
    }else {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "Unknown mode (%d) or progress not done to compare !!\n", mode);
        ret = -1;
        goto exit;
    }

    /* Get type storage */
	if (strstr(phypath, "PCI") || strstr(phypath, "pci")) {
		if (strstr(phypath + 3, "NVME") || strstr(phypath + 3, "nvme"))
			type = PCI_NVME_STORAGE;
		else if (strstr(phypath + 3, "M2") || strstr(phypath + 3, "M.2"))
			type = PCI_M2_STORAGE;
		else if (strstr(phypath + 3, "USB") || strstr(phypath + 3, "usb"))
			type = USB_STORAGE;
		else {
			if ((type = storage_chk_hdd(info, diskpath)) == -1)
				type = UNKNOWN_STORAGE;
		}
	}else if (strstr(phypath, "SATA") || strstr(phypath, "sata")) {
		if ((type = storage_chk_hdd(info, diskpath)) == -1)
			type = UNKNOWN_STORAGE;
	}else if (strstr(phypath, "USB") || strstr(phypath, "usb")) {
		type = USB_STORAGE;
	}
	vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s %d type %d\n", __FUNCTION__, __LINE__, type);

	/* Get ID_MODEL for compare */
	storage_get_stor_info(info, diskpath, storinfo);
	vediag_debug(VEDIAG_STORAGE, info->dbg_info, "ID_MODEL %s\n", storinfo->id_model);
	vediag_debug(VEDIAG_STORAGE, info->dbg_info, "ID_REVISION %s\n", storinfo->id_revision);
	vediag_debug(VEDIAG_STORAGE, info->dbg_info, "ID_SERIAL %s\n", storinfo->id_serial);

	/* If direct device, search in avl. If cannot find, compare with lowest device and show WARNING
	 * If non-direct device, compare with lowest device in avl of device type corresponds to, and show WARNING
	 */
	for(i = 0; i < avlnum; i++) {
		//printf("Search %d - %s: current %llu expected %llu\n", i, stor_test_avl_tbl[i].name, currspd_bs, expspd_bs);
	    if (mode == SEQ_READ) {
			expspd_bs = stor_test_avl_tbl[i].seq_rdMBs * 8 * (1 << 20);
	    }else if (mode == SEQ_WRITE) {
	    	expspd_bs = stor_test_avl_tbl[i].seq_wrMBs * 8 * (1 << 20);
	    }else if (mode == RAN_READ) {
	    	expspd_bs = stor_test_avl_tbl[i].ran_rdMBs * 8 * (1 << 20);
	    }else if (mode == RAN_WRITE) {
	    	expspd_bs = stor_test_avl_tbl[i].ran_wrMBs * 8 * (1 << 20);
	    }

		// find
		if ((strlen(storinfo->id_model) && strncmp(storinfo->id_model, stor_test_avl_tbl[i].name, strlen(stor_test_avl_tbl[i].name)) == 0) ||
			(strlen(storinfo->id_serial) && strncmp(storinfo->id_serial, stor_test_avl_tbl[i].name, strlen(stor_test_avl_tbl[i].name)) == 0)) {
			if (type == USB_STORAGE) {
				if (strlen(storinfo->id_revision) && strncmp(storinfo->id_revision, stor_test_avl_tbl[i].rev, strlen(stor_test_avl_tbl[i].rev)) == 0)
					found = 1;
				else if (!strlen(storinfo->id_revision) && stor_test_avl_tbl[i].rev && !strlen(stor_test_avl_tbl[i].rev))	//rev == ""
					found = 1;
				else
					found = 0;
			}else
				found = 1;
		}else
			found = 0;

		// found
		if (found) {
			// fail X percent
			if (currspd_bs < expspd_bs * STORAGE_PERF_PERCENT_PASS / 100) {
				vediag_err(VEDIAG_STORAGE, info->dbg_info, "Found ID_MODEL %s: Current speed %llu < %d%s Expected %llu FAIL !!\n",
						stor_test_avl_tbl[i].name,
						currspd_bs >= (8 * (1 << 20))?currspd_bs / (8 * (1 << 20)):currspd_bs / (8 * (1 << 10)),
						STORAGE_PERF_PERCENT_PASS, "%",
						expspd_bs >= (8 * (1 << 20))?expspd_bs / (8 * (1 << 20)):expspd_bs / (8 * (1 << 10)));
				ret = 1;
				goto exit;
			}else {
				vediag_info(VEDIAG_STORAGE, info->dbg_info, "Found ID_MODEL %s: Current speed %llu > %d%s Expected %llu OK\n",
						stor_test_avl_tbl[i].name,
						currspd_bs >= (8 * (1 << 20))?currspd_bs / (8 * (1 << 20)):currspd_bs / (8 * (1 << 10)),
						STORAGE_PERF_PERCENT_PASS, "%",
						expspd_bs >= (8 * (1 << 20))?expspd_bs / (8 * (1 << 20)):expspd_bs / (8 * (1 << 10)));
				break;
			}
		}

		// store lowest speed to next stage to use
		//printf("Previous Lowest speed %llu\n", i, stor_test_avl_tbl[i].name, lowestspd_bs);
		if (type == stor_test_avl_tbl[i].type) {
			if (!lowestspd_bs)	//first
				lowestspd_bs = expspd_bs;
			else if (expspd_bs < lowestspd_bs)
				lowestspd_bs = expspd_bs;
		}
	}

	/* If cannot find, continue compare with lowest speed device */
	if (i >= avlnum) {
		vediag_warn(VEDIAG_STORAGE, info->dbg_info, "NOT find device match ID_MODEL %s -- > Compare lowest speed %llu\n", storinfo->id_model,
				lowestspd_bs >= (8 * (1 << 20))?lowestspd_bs / (8 * (1 << 20)):lowestspd_bs / (8 * (1 << 10)));

		if (lowestspd_bs <= 0) {
			vediag_err(VEDIAG_STORAGE, info->dbg_info, "Cannot find lowest speed for this type (%d) device to compare !!\n", type);
			ret = -1;
			goto exit;
		}else if (currspd_bs < lowestspd_bs * STORAGE_PERF_PERCENT_PASS / 100) {
			vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s Current speed %llu < %d%s Lowest %llu FAIL !!\n",
					storinfo->id_model,
					currspd_bs >= (8 * (1 << 20))?currspd_bs / (8 * (1 << 20)):currspd_bs / (8 * (1 << 10)),
					STORAGE_PERF_PERCENT_PASS, "%",
					lowestspd_bs >= (8 * (1 << 20))?lowestspd_bs / (8 * (1 << 20)):lowestspd_bs / (8 * (1 << 10)));
			ret = 2;
			goto exit;
		}else {
			vediag_info(VEDIAG_STORAGE, info->dbg_info, "%s Current speed %llu > %d%s Lowest %llu OK\n",
					storinfo->id_model,
					currspd_bs >= (8 * (1 << 20))?currspd_bs / (8 * (1 << 20)):currspd_bs / (8 * (1 << 10)),
					STORAGE_PERF_PERCENT_PASS, "%",
					lowestspd_bs >= (8 * (1 << 20))?lowestspd_bs / (8 * (1 << 20)):lowestspd_bs / (8 * (1 << 10)));
		}
	}

exit:
	if (cmd)
		free(cmd);

	if (buf)
		free(buf);

	if (storinfo)
		free(storinfo);

	return ret;
}


/********************************************* LIB for storage RW low level test *******************************************/
int storage_raw_read(struct vediag_test_info *info, char *diskpath, void *buffer,
		unsigned long long offset,
		unsigned long long length)
{
	FILE *devf = NULL;
	char *errbuf;
    unsigned long long blkcnt, blkoff, off, blkret;
	int errbuf_len = SYS_ERROR_MESSAGE_SIZE;
	int ret = 0;

    blkoff = offset / STORAGE_SECTOR_SIZE;
    blkcnt = length / STORAGE_SECTOR_SIZE;
    off = blkoff * STORAGE_SECTOR_SIZE;

	errbuf = malloc(errbuf_len);
	if (!errbuf) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "Alloc fail !! \n");
		ret = -STORAGE_ALLOCMEM;
		goto exit;
	}

	sto_debug(VEDIAG_STORAGE, "%s Open for Read\n", diskpath);
    devf = fopen(diskpath, "rb");
    if(!devf)  {
    	memset(errbuf, 0, errbuf_len);
    	strerror_r(errno, errbuf, errbuf_len);
    	vediag_err(VEDIAG_STORAGE, info->dbg_info, "CANNOT open for Read: %s\n", errbuf);
        ret = -STORAGE_DET;
        goto exit;
    }
    setbuf(devf, NULL);       // Disable buffering

    sto_debug(VEDIAG_STORAGE, "%s Seek at sector %lld (0x%llx) \n", diskpath, blkoff, off);
    if (fseek(devf, off, SEEK_SET) != 0) {
    	memset(errbuf, 0, errbuf_len);
    	strerror_r(errno, errbuf, errbuf_len);
    	vediag_err(VEDIAG_STORAGE, info->dbg_info, "CANNOT move to sector %llx: %s\n", blkoff, errbuf);
        ret = -STORAGE_SEEK;
        goto exit;
    }

    blkret = fread(buffer, STORAGE_SECTOR_SIZE, blkcnt, devf);

    if (blkret != blkcnt) {
    	memset(errbuf, 0, errbuf_len);
    	strerror_r(errno, errbuf, errbuf_len);
    	vediag_err(VEDIAG_STORAGE, info->dbg_info, "READ NOT enough %d # %d blks: %s\n",
    			blkret, blkcnt, errbuf);
        ret = -STORAGE_RD;
        goto exit;
    }

    sto_debug(VEDIAG_STORAGE, "%s READ 0x%llx Bytes DONE\n", diskpath, blkret * STORAGE_SECTOR_SIZE);

exit:
	if (errbuf)
		free(errbuf);

	if (devf)
		fclose(devf);

	return ret;
}

static int storage_raw_write(struct vediag_test_info *info, char *diskpath, void *buffer,
		unsigned long long offset, unsigned long long length)
{
	FILE *devf = NULL;
    unsigned long long blkcnt, blkoff, off, blkret;
	char *errbuf;
	int errbuf_len = SYS_ERROR_MESSAGE_SIZE;
	int ret = 0;

    blkoff = offset / STORAGE_SECTOR_SIZE;
    blkcnt = length / STORAGE_SECTOR_SIZE;
    off = blkoff * STORAGE_SECTOR_SIZE;

	errbuf = malloc(errbuf_len);
	if (!errbuf) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "Alloc fail !! \n");
		ret = -STORAGE_ALLOCMEM;
		goto exit;
	}

	sto_debug(VEDIAG_STORAGE, "%s Open for Write\n", diskpath);
    devf = fopen(diskpath, "wb");
    if(!devf)  {
    	memset(errbuf, 0, errbuf_len);
    	strerror_r(errno, errbuf, errbuf_len);
    	vediag_err(VEDIAG_STORAGE, info->dbg_info, "CANNOT open for Write: %s\n", errbuf);
        ret = -STORAGE_DET;
        goto exit;
    }
    setbuf(devf, NULL);       // Disable buffering

    sto_debug(VEDIAG_STORAGE, "%s Seek at sector %lld (0x%llx) \n", diskpath, blkoff, off);
    if (fseek(devf, off, SEEK_SET) != 0) {
    	memset(errbuf, 0, errbuf_len);
    	strerror_r(errno, errbuf, errbuf_len);
    	vediag_err(VEDIAG_STORAGE, info->dbg_info, "CANNOT move to sector %llx: %s\n", blkoff, errbuf);
        ret = -STORAGE_SEEK;
        goto exit;
    }

    blkret = fwrite(buffer, STORAGE_SECTOR_SIZE, blkcnt, devf);
    if (blkret != blkcnt) {
    	memset(errbuf, 0, errbuf_len);
    	strerror_r(errno, errbuf, errbuf_len);
    	vediag_err(VEDIAG_STORAGE, info->dbg_info, "WRITE NOT enough %d # %d blks: %s\n",
    			blkret, blkcnt, errbuf);
        ret = -STORAGE_WR;
        goto exit;
    }

    sto_debug(VEDIAG_STORAGE, "%s WRITE 0x%llx Bytes DONE\n", diskpath, blkret * STORAGE_SECTOR_SIZE);

exit:
	if (errbuf)
		free(errbuf);

	if (devf)
		fclose(devf);

	return ret;
}

static int storage_rw_raw_test(struct vediag_test_info *info,
		unsigned long long offset,
		unsigned long long length)
{
    void *memwr = NULL, *memrd = NULL, *membk = NULL;
    unsigned long long blkcnt, blkoff, off, len;
    int ret = 0;

	struct vediag_storage_test_info *test_arg = (struct vediag_storage_test_info *) info->data;
	char *diskpath = test_arg->diskpath;

    blkoff = offset / STORAGE_SECTOR_SIZE;
    blkcnt = length / STORAGE_SECTOR_SIZE;
    off = blkoff * STORAGE_SECTOR_SIZE;
    len = blkcnt * STORAGE_SECTOR_SIZE;

    vediag_debug(VEDIAG_STORAGE, info->dbg_info, "\nSTORAGE RAW TEST: blkoff 0x%llx (0x%llx) blkcnt 0x%llx (0x%llx)\n",
    		blkoff, offset, blkcnt, len);

    /* Malloc memory for test */
    membk = malloc(len);
    if (!membk) {
    	vediag_err(VEDIAG_STORAGE, info->dbg_info, "Malloc 'membk' (size 0x%llx) fail\n", len);
        ret = -STORAGE_ALLOCMEM;
        goto exit;
    }
    memset(membk, 0, len);

    memwr = malloc(len);
    if (!memwr) {
    	vediag_err(VEDIAG_STORAGE, info->dbg_info, "Alloc 'memwr' (size 0x%llx) fail\n", len);
        ret = -STORAGE_ALLOCMEM;
        goto exit;
    }

    memrd = malloc(len);
    if (!memrd) {
    	vediag_err(VEDIAG_STORAGE, info->dbg_info, "Alloc 'memrd' (size 0x%llx) fail\n", len);
        ret = -STORAGE_ALLOCMEM;
        goto exit;
    }
    memset(memrd, 0, len);

    /* Field pattern to buffer */
#if defined(VEDIAG_USE_RANDOM_PATTERN)
	prandom_bytes(memwr, len);
#else
    memset(memwr, 0x5a, len);
#endif

    /* Backup data */
    sto_debug(VEDIAG_STORAGE, "%s Open for Backup\n", diskpath);
    ret = storage_raw_read(info, diskpath, membk, off, len);
    if (ret) {
        ret = -STORAGE_RD_BK;
        vediag_err(VEDIAG_STORAGE, info->dbg_info, "Backup 0x%llx Bytes FAIL\n", len);
        goto exit;
    }
    sto_debug(VEDIAG_STORAGE, "%s Backup 0x%llx Bytes DONE\n", diskpath, len);

    /* Write test */
    sto_debug(VEDIAG_STORAGE, "%s Open for Write\n", diskpath);
    ret = storage_raw_write(info, diskpath, memwr, off, len);
    if (ret) {
        ret = -STORAGE_WR;
        vediag_err(VEDIAG_STORAGE, info->dbg_info, "Write 0x%llx Bytes FAIL\n", len);
        goto exit2bk;
    }
    sto_debug(VEDIAG_STORAGE, "%s Write 0x%llx Bytes DONE\n", diskpath, len);

    /* Read test */
    sto_debug(VEDIAG_STORAGE, "%s Open for Read\n", diskpath);
    ret = storage_raw_read(info, diskpath, memrd, off, len);
    if (ret) {
        ret = -STORAGE_RD;
        vediag_err(VEDIAG_STORAGE, info->dbg_info, "Read 0x%llx Bytes FAIL\n", len);
        goto exit2bk;
    }
    sto_debug(VEDIAG_STORAGE, "%s Read 0x%llx Bytes DONE\n", diskpath, len);

#ifdef VEDIAG_STORAGE_DEBUG /* Dump data */
	int i;
	char *ptr = (char *)memrd;
	sto_debug("\n%s First sector (@%p)\n", diskpath, ptr);
	for(i = 0; i < STORAGE_SECTOR_SIZE; i++)
		sto_debug("%x ", ptr[i]);
	sto_debug("\n");
	sto_debug("\n%s Last sector (@%p)\n", diskpath, ptr);
	ptr = ptr + (blkcnt - 1) * STORAGE_SECTOR_SIZE;
	for(i = 0; i < STORAGE_SECTOR_SIZE; i++)
		sto_debug("%x ", ptr[i]);
	sto_debug("\n");
#endif

    /* Compare RD/WR pattern */
    ret = memcmp(memrd, memwr, len);
    if (ret) {
        ret = -STORAGE_VERIFY;
        vediag_err(VEDIAG_STORAGE, info->dbg_info, "Compare 0x%llx Bytes FAIL\n", len);
        goto exit2bk;
    }
    sto_debug(VEDIAG_STORAGE, "%s Compare 0x%llx Bytes OK\n", diskpath, len);

    /* Write backup */
exit2bk:
	if (membk) {
	    sto_debug(VEDIAG_STORAGE, "%s Open for Write Backup\n", diskpath);
	    ret = storage_raw_write(info, diskpath, membk, off, len);
	    if (ret) {
	        ret = -STORAGE_WR_BK;
	        vediag_err(VEDIAG_STORAGE, info->dbg_info, "Write Backup 0x%llx Bytes FAIL\n", len);
	        goto exit;
	    }
	    sto_debug(VEDIAG_STORAGE, "%s Write Backup 0x%llx Bytes DONE\n", diskpath, len);
	}

exit:
	if (membk)
		free(membk);
	if (memwr)
		free(memwr);
	if (memrd)
		free(memrd);

	return ret;
}


/********************************************* LIB for storage fio *******************************************/
static int storage_app_fio_parse_result(char *result, char *rd_result, char *wr_result,
		struct stor_test_result *ret)
{
	char *start, *end;
	char str[128];
	unsigned long long unit = 1;

	if (!ret)
		return -1;

	/* Get error */
	if (result && strlen(result)) {
		start = strstr(result, "err= ");
		if (!start)
			goto get_rd;

		start = start + 5;
		end = strstr(start, ":");
		if (!end)
			goto get_rd;

		memset(str, 0, 128);
		strncpy(str, start, end - start);

		ret->errors = strtoul(str, NULL, 0);
		printf("%s errors %s %d\n", __FUNCTION__, str, ret->errors);
	}

get_rd:
	/* Get rd info */
	if (rd_result && strlen(rd_result)) {
		//size
		start = strstr(rd_result, "io=");
		if (!start)
			goto get_wr;

		start = start + 3;
		end = strstr(start, ",");
		if (!end)
			goto get_wr;

		memset(str, 0, 128);
		strncpy(str, start, end - start);

		if (strstr(str, "GB"))
			unit = 1 << 30;
		else if (strstr(str, "MB"))
			unit = 1 << 20;
		else if (strstr(str, "KB"))
			unit = 1 << 10;

		start = strstr(str, ".");
		if (start)
			memset(start, 0, 1);

		ret->rdsize = strtoul(str, NULL, 0) * unit;	// by bytes
		printf("%s rdsize %s %llu Bytes\n", __FUNCTION__, str, ret->rdsize);

		// avr spd
		start = strstr(rd_result, "aggrb=");
		if (!start)
			goto get_wr;

		start = start + 6;
		end = strstr(start, ",");
		if (!end)
			goto get_wr;

		memset(str, 0, 128);
		strncpy(str, start, end - start);

		if (strstr(str, "GB"))
			unit = 1 << 30;
		else if (strstr(str, "MB"))
			unit = 1 << 20;
		else if (strstr(str, "KB"))
			unit = 1 << 10;
		else
			unit = 1;

		start = strstr(str, ".");
		if (start)
			memset(start, 0, 1);

		ret->rdspd_avr = strtoul(str, NULL, 0) * unit * 8; //by bits/s
		ret->rddone = 1;
		printf("%s seq_rdspd_avr %s %llu bits/s\n", __FUNCTION__, str, ret->rdspd_avr);
	}

get_wr:
	/* Get wr info */
	if (wr_result && strlen(wr_result)) {
		//size
		start = strstr(wr_result, "io=");
		if (!start)
			goto exit;

		start = start + 3;
		end = strstr(start, ",");
		if (!end)
			goto exit;

		memset(str, 0, 128);
		strncpy(str, start, end - start);

		if (strstr(str, "GB"))
			unit = 1 << 30;
		else if (strstr(str, "MB"))
			unit = 1 << 20;
		else if (strstr(str, "KB"))
			unit = 1 << 10;

		start = strstr(str, ".");
		if (start)
			memset(start, 0, 1);

		ret->wrsize = strtoul(str, NULL, 0) * unit;	// by bytes
		printf("%s wrsize %s %llu Bytes\n", __FUNCTION__, str, ret->wrsize);

		// avr spd
		start = strstr(wr_result, "aggrb=");
		if (!start)
			goto exit;

		start = start + 6;
		end = strstr(start, ",");
		if (!end)
			goto exit;

		memset(str, 0, 128);
		strncpy(str, start, end - start);

		if (strstr(str, "GB"))
			unit = 1 << 30;
		else if (strstr(str, "MB"))
			unit = 1 << 20;
		else if (strstr(str, "KB"))
			unit = 1 << 10;

		start = strstr(str, ".");
		if (start)
			memset(start, 0, 1);

		ret->wrspd_avr = strtoul(str, NULL, 0) * unit * 8; //by bits/s
		ret->wrdone = 1;
		printf("%s wrspd_avr %s %llu bits/s\n", __FUNCTION__, str, ret->wrspd_avr);
	}

exit:
	return 0;
}

static int storage_app_fio_raw_test(struct vediag_test_info *info,
		unsigned long long offset, unsigned long long length)
{
    void *membk = NULL;
    unsigned long long blkcnt, blkoff, off, len, len_MB, off_MB;
    int ret = 0;

    FILE *devf;
    char *cmd = NULL, *result = NULL, *result_done = NULL;
    int test_done = 0;

    char *line = NULL;
    char *buf = NULL;

	struct vediag_storage_test_info *test_arg = (struct vediag_storage_test_info *) info->data;
	char *diskpath = test_arg->diskpath;
	char *fiopath = test_arg->apppath;

    blkoff = offset / STORAGE_SECTOR_SIZE;
    blkcnt = length / STORAGE_SECTOR_SIZE;
    off = blkoff * STORAGE_SECTOR_SIZE;
    len = blkcnt * STORAGE_SECTOR_SIZE;

    vediag_debug(VEDIAG_STORAGE, info->dbg_info, "\nSTORAGE FIO TEST: blkoff 0x%llx (0x%llx) blkcnt 0x%llx (0x%llx)\n",
    		blkoff, offset, blkcnt, len);

    /* Malloc */
	cmd = malloc(TEXT_LINE_SIZE);
	result_done = malloc(TEXT_LINE_SIZE);
    buf = malloc(TEXT_LINE_SIZE);
    result = malloc(TEXT_LINE_SIZE);
    if (!cmd || !result_done || !buf || !result) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc fail\n", __FUNCTION__, __LINE__);
        ret = -1;
        goto exit;
	}
    memset(cmd, 0, TEXT_LINE_SIZE);
    memset(result_done, 0, TEXT_LINE_SIZE);
    memset(buf, 0, TEXT_LINE_SIZE);
    memset(result, 0, TEXT_LINE_SIZE);

    membk = malloc(len);
    if (!membk) {
    	vediag_err(VEDIAG_STORAGE, info->dbg_info, "Malloc 'membk' (size 0x%llx) fail\n", len);
        ret = -STORAGE_ALLOCMEM;
        goto exit;
    }
    memset(membk, 0, len);

    /* Run FIO test */
	len_MB = length / (1 << 20);
	off_MB = offset / (1 << 20);

	if (fiopath && strlen(fiopath)) {
		sprintf(cmd, "%s --offset=%lldM --direct=1 --rw=write --bs=64k --size=%lldM --iodepth=1 %s --numjobs=1 --name=%s --verify=md5",
			fiopath, off_MB, len_MB, strcmp(fiopath, "fio")?"":"--ioengine=libaio", diskpath);
	}else {
    	vediag_err(VEDIAG_STORAGE, info->dbg_info, "Not found FIO for test\n");
        ret = -1;
        goto exit;
	}

	vediag_debug(VEDIAG_STORAGE, info->dbg_info, "\nFIO cmd: %s\n", cmd);
	sprintf(result_done, "%s: (groupid=0, jobs=1):", diskpath);

    /* Backup data */
    sto_debug(VEDIAG_STORAGE, "%s Open for Backup\n", diskpath);
    ret = storage_raw_read(info, diskpath, membk, off, len);
    if (ret) {
        ret = -STORAGE_RD_BK;
        vediag_err(VEDIAG_STORAGE, info->dbg_info, "Backup 0x%llx Bytes FAIL\n", len);
        goto exit;
    }
    sto_debug(VEDIAG_STORAGE, "%s Backup 0x%llx Bytes DONE\n", diskpath, len);

    devf = popen(cmd, "r");
    if (!devf) {
        vediag_err(VEDIAG_STORAGE, info->dbg_info, "Cannot run test with %s\n", cmd);
        ret = -1;
        goto exit2bk;
    }

	while (1) {
		memset(buf, 0, TEXT_LINE_SIZE);
		line = fgets(buf, TEXT_LINE_SIZE, devf);

		if (line == NULL) break;
		//vediag_debug(VEDIAG_STORAGE, NULL, "%s", line);

		if (test_done == 0 && strncmp(line, result_done, 30) == 0) {
			test_done = 1;
			strcpy(result, line);
		}
	}

	if (pclose(devf) == -1)
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Close command fail !!\n", __FUNCTION__, __LINE__);

    if (!test_done) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "Test run ERROR\n");
		ret = -1;
		goto exit2bk;
    }

    /* Parse result */
	vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s\n", result);

	int i = 0;
	char *ptr;
	for(i = 0; i < strlen(result); i++) {
		ptr = result + i;
		if (strncmp(ptr, "err=", 4) == 0) {
			vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s\n", ptr);
			ptr = result + i + 5;
			if (strncmp(ptr, "0", 1) == 0) {
				vediag_debug(VEDIAG_STORAGE, info->dbg_info, "TEST PASS\n");
				goto exit2bk;
			}else {
				vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s\n", result);
				vediag_err(VEDIAG_STORAGE, info->dbg_info, "TEST FAIL\n");
				ret = -1;
				goto exit2bk;
			}
		}
	}

    /* Write backup */
exit2bk:
	if (membk) {
	    sto_debug(VEDIAG_STORAGE, "%s Open for Write Backup\n", diskpath);
	    ret = storage_raw_write(info, diskpath, membk, off, len);
	    if (ret) {
	        ret = -STORAGE_WR_BK;
	        vediag_err(VEDIAG_STORAGE, info->dbg_info, "Write Backup 0x%llx Bytes FAIL\n", len);
	        goto exit;
	    }
	    sto_debug(VEDIAG_STORAGE, "%s Write Backup 0x%llx Bytes DONE\n", diskpath, len);
	}

exit:
	if (cmd)
		free(cmd);

	if (result_done)
		free(result_done);

	if (buf)
		free(buf);

	if (result)
		free(result);

	if (membk)
		free(membk);

	return ret;
}

static int storage_app_fio_file_test(struct vediag_test_info *info,
        unsigned long long offset, unsigned long long length,
		unsigned long long time, int pfmode, struct stor_test_result *result)
{
	unsigned long long blkcnt, blkoff, off, len, len_MB /*, off_MB*/;
    int i, ret = 0, linenum = 0;
    char *test_name = NULL, *file_output = NULL;

    FILE *devf;
    char *cmd = NULL, *buf = NULL, *line = NULL;
    char *retcmp = NULL, *retstr = NULL, *rdstr = NULL, *wrstr = NULL;
    int chkdone = 0;
	struct timeval tm;
	char *ptr;
	u32 timeout_cmd = TIMEOUT_CMD_FIO;

	struct vediag_storage_test_info *test_arg = (struct vediag_storage_test_info *) info->data;
	char *diskpath = test_arg->diskpath;
	char *fiopath = test_arg->apppath;
	char *testpath = test_arg->testpath;
	char *outfile = test_arg->outfile;

    blkoff = offset / STORAGE_SECTOR_SIZE;
    blkcnt = length / STORAGE_SECTOR_SIZE;
    off = blkoff * STORAGE_SECTOR_SIZE;
    len = blkcnt * STORAGE_SECTOR_SIZE;

    vediag_debug(VEDIAG_STORAGE, info->dbg_info, "\nSTORAGE FIO TEST: blkoff 0x%llx (0x%llx) blkcnt 0x%llx (0x%llx) testpath %s \n",
            blkoff, offset, blkcnt, len, testpath);

    /* Malloc */
    test_name = malloc(128);
    file_output = malloc(128);
    cmd = malloc(TEXT_LINE_SIZE);
    retcmp = malloc(TEXT_LINE_SIZE);
    buf = malloc(TEXT_LINE_SIZE);
    retstr = malloc(TEXT_LINE_SIZE);
    rdstr = malloc(TEXT_LINE_SIZE);
    wrstr = malloc(TEXT_LINE_SIZE);
	if (!test_name || !file_output || !cmd || !retcmp || !buf || !retstr || !rdstr || !wrstr) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc fail\n", __FUNCTION__, __LINE__);
        ret = -1;
        goto exit;
	}
	memset(test_name, 0, 128);
	memset(file_output, 0, 128);
    memset(cmd, 0, TEXT_LINE_SIZE);
    memset(retcmp, 0, TEXT_LINE_SIZE);
    memset(buf, 0, TEXT_LINE_SIZE);
    memset(retstr, 0, TEXT_LINE_SIZE);
    memset(rdstr, 0, TEXT_LINE_SIZE);
    memset(wrstr, 0, TEXT_LINE_SIZE);

    /* Run FIO test */
    len_MB = length / (1 << 20);
    //off_MB = offset / (1 << 20);

    // create file for test/output
    gettimeofday(&tm, NULL);
    unsigned long tempvar = tm.tv_usec;
#if 0	// use if need print specific test name in log output for debug
    sprintf(test_name, "%s_%ld", diskpath + 5, tempvar);
#else
    sprintf(test_name, "%s_%s", STORAGE_FILE_TEST_NAME, diskpath + 5);
#endif
    sprintf(file_output, "/tmp/%s_%lu_%ld.txt", diskpath + 5, pthread_self(), tempvar);

    // create cmd for test
    if (fiopath && strlen(fiopath)) {
    	if (test_arg->test_performance) {	//performance test (must have run time)
			char *rwmode = pfmode==SEQ_WRITE?"write":pfmode==SEQ_READ?"read":pfmode==RAN_WRITE?"ranwrite":pfmode==RAN_READ?"ranread":NULL;
			
			if (!rwmode) {
			    vediag_err(VEDIAG_STORAGE, info->dbg_info, "Not found performance mode (%d) !!\n", pfmode);
			    ret = -1;
			    goto exit;			
			}
			
			sprintf(cmd, "%s --direct=1 --rw=%s --bs=1M --size=%lldM --iodepth=1 %s --time_based=1 --runtime=%llu --numjobs=8 --name=%s --directory=%s --output=%s --eta=never",
				fiopath, rwmode, len_MB, strcmp(fiopath, "fio")?"":"--ioengine=libaio", time, test_name, testpath, outfile);
			timeout_cmd = time + TIMEOUT_CMD_PER;
    	} else {
			// run by time
    		if (time > 0) {
				sprintf(cmd, "%s --direct=1 --rw=write --bs=64k --size=%lldM --iodepth=1 %s --time_based=1 --runtime=%llu --numjobs=1 --name=%s --directory=%s --verify=md5 --output=%s --eta=never",
					fiopath, len_MB, strcmp(fiopath, "fio")?"":"--ioengine=libaio", time, test_name, testpath, outfile);
			//run by size
    		}else {
				sprintf(cmd, "%s --direct=1 --rw=write --bs=64k --size=%lldM --iodepth=1 %s --numjobs=1 --name=%s --directory=%s --verify=md5 --output=%s --eta=never",
					fiopath, len_MB, strcmp(fiopath, "fio")?"":"--ioengine=libaio", test_name, testpath, outfile);
    		}
    	}
    } else {
        vediag_err(VEDIAG_STORAGE, info->dbg_info, "Not found FIO for test\n");
        ret = -1;
        goto exit;
    }
    vediag_debug(VEDIAG_STORAGE, info->dbg_info, "\nFIO cmd: %s\n", cmd);

    // create result string to check pass/fail
	sprintf(retcmp, "%s: (groupid=0, jobs=1):", test_name);	/* String to check FIO done */
    vediag_debug(VEDIAG_STORAGE, info->dbg_info, "\nretcmp: '%s: (groupid=0, jobs=1):'\n", test_name);

	ret = storage_app_call_process(info, cmd, timeout_cmd);
    if (ret == -1) {
        vediag_err(VEDIAG_STORAGE, info->dbg_info, "Call process for command %s error!! \n", cmd);
        ret = -1;
        goto exit;
    }

	usleep(1);
    // call app fio
    // devf = popen(cmd, "r");
    devf = fopen(outfile, "r");
    if (!devf) {
        vediag_err(VEDIAG_STORAGE, info->dbg_info, "Cannot run test with %s\n", cmd);
        ret = -1;
        goto exit;
    }

	vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s open devf %p\n", test_name, devf);
	while (1) {
		memset(buf, 0, TEXT_LINE_SIZE);
		line = fgets(buf, TEXT_LINE_SIZE, devf);

		if (line == NULL) break;
		linenum++;
		//vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s", line);

		if (!chkdone && strncmp(line, retcmp, strlen(retcmp)) == 0) {
			strcpy(retstr, line);
			chkdone = 1;
		}
		if(chkdone) {
			usleep(1);
			if (chkdone && time > 0 && strncmp(line, "   READ:", 8) == 0) {
				strcpy(rdstr, line);
			}

			if (chkdone && time > 0 && strncmp(line, "  WRITE:", 8) == 0) {
				strcpy(wrstr, line);
			}
		}
	}

	//if (pclose(devf) == -1)
	if (fclose(devf) == -1)
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Close command fail, error %s !!\n", __FUNCTION__, __LINE__, strerror(errno));

	/* Check result from app test */

    /* Parse result to get error number */
    if (!strlen(retstr)) {
    	vediag_err(VEDIAG_STORAGE, info->dbg_info, "Result is NULL (line %d) --> FAIL !!\n", linenum);
		ret = -1;
		goto exit;
	}
	vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s\n", retstr);

	for(i = 0; i < strlen(retstr); i++) {
		ptr = retstr + i;
		if (strncmp(ptr, "err=", 4) == 0) {
			//vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s\n", ptr);
			ptr = retstr + i + 5;
			if (strncmp(ptr, "0", 1) == 0) {
				vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s PASS\n", __FUNCTION__);
			}else {
				vediag_err(VEDIAG_STORAGE, info->dbg_info, "FIO test got FAIL !!\n");
				ret = -1;
			}
		}
	}

	/* Get return size if test by time / performance number */
	if (time > 0) {	
		storage_app_fio_parse_result(retstr, rdstr, wrstr, result);
		
		// normal test just both write and read verify with same size
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "real_size %d \n", result->wrsize);

		if (test_arg->test_performance) {		
			if (result->wrdone) {
				vediag_debug(VEDIAG_STORAGE, info->dbg_info, "wrstr %s\n", wrstr);
				vediag_info(VEDIAG_STORAGE, info->dbg_info, "%s WRITE SIZE = %llu %s, SPEED = %llu %s\n",
						(pfmode==SEQ_WRITE || pfmode==SEQ_READ)?"SEQUENTIAL":(pfmode==RAN_WRITE || pfmode==RAN_READ)?"RANDOM":"",
						result->wrsize >= (1 << 20)?result->wrsize / (1 << 20):result->wrsize / (1 << 10),
						result->wrsize >= (1 << 20)?"MB":"KB",
						result->wrspd_avr >= (8 * (1 << 20))?result->wrspd_avr / (8 * (1 << 20)):result->wrspd_avr / (8 * (1 << 10)),
						result->wrspd_avr >= (8 * (1 << 20))?"MB/s":"KB/s");
			} 
			
			if (result->rddone) {
				vediag_debug(VEDIAG_STORAGE, info->dbg_info, "rdstr %s\n", rdstr);
				vediag_info(VEDIAG_STORAGE, info->dbg_info, "%s READ SIZE = %llu %s, SPEED = %llu %s\n",
						(pfmode==SEQ_WRITE || pfmode==SEQ_READ)?"SEQUENTIAL":(pfmode==RAN_WRITE || pfmode==RAN_READ)?"RANDOM":"",
						result->rdsize >= (1 << 20)?result->rdsize / (1 << 20):result->rdsize / (1 << 10),
						result->rdsize >= (1 << 20)?"MB":"KB",
						result->rdspd_avr >= (8 * (1 << 20))?result->rdspd_avr / (8 * (1 << 20)):result->rdspd_avr / (8 * (1 << 10)),
						result->rdspd_avr >= (8 * (1 << 20))?"MB/s":"KB/s");
			}

			storage_perf_result_check(info, result, pfmode);
		}
	}

exit:
	if (test_name)
		free(test_name);

	if (file_output)
		free(file_output);

	if (cmd)
        free(cmd);

    if (retcmp)
        free(retcmp);

    if (buf)
        free(buf);

    if (retstr)
        free(retstr);

    return ret;
}


/********************************************* LIB for storage stressapp *******************************************/
static int storage_app_stressapp_parse_result(char *result,	struct stor_test_result *ret)
{
	char *start = NULL, *end = NULL, *tmp = NULL;
	char str[128];
	unsigned long long unit = 1;

	if (!ret)
		return -1;

	/* Get error */
	if (result && strlen(result)) {
		start = strstr(result, "with ");
		if (!start)
			goto get_spd;

		start = start + 5;
		end = strstr(start, " hardware");
		if (!end)
			goto get_spd;

		memset(str, 0, 128);
		strncpy(str, start, end - start);

		ret->errors = strtoul(str, NULL, 0);
		printf("%s errors %s %d\n", __FUNCTION__, str, ret->errors);

		start = strstr(result, "incidents, ");
		if (!start)
			goto get_spd;

		start = start + 11;
		end = strstr(start, " error");
		if (!end)
			goto get_spd;

		memset(str, 0, 128);
		strncpy(str, start, end - start);

		ret->errors += strtoul(str, NULL, 0);
	}

get_spd:
	/* Get size/speed info */
	if (result && strlen(result)) {
		//size
		start = strstr(result, "Completed: ");
		if (!start)
			goto exit;

		start = start + 11;

		if ((tmp = strstr(start, "G in")))
			unit = 1<<30;
		else if ((tmp = strstr(start, "M in")))
			unit = 1<<20;
		else if ((tmp = strstr(start, "K in")))
			unit = 1<<10;
		else if ((tmp = strstr(start, "B in")))
			unit = 1;

		end = strstr(start, ".");
		memset(str, 0, 128);
		if (end)
			strncpy(str, start, end - start);
		else if (tmp)
			strncpy(str, start, tmp - start);
		else
			goto exit;

		ret->wrsize = ret->rdsize = strtoul(str, NULL, 0) * unit;	// by bytes
		printf("%s size %s %llu Bytes\n", __FUNCTION__, str, ret->wrsize);

		// avr spd
		start = strstr(result, "s ");
		if (!start)
			goto exit;

		start = start + 2;

		if ((tmp = strstr(start, "GB/")))
			unit = 1<<30;
		else if ((tmp = strstr(start, "MB/")))
			unit = 1<<20;
		else if ((tmp = strstr(start, "KB/")))
			unit = 1<<10;
		else if ((tmp = strstr(start, "B/")))
			unit = 1;

		end = strstr(start, ".");
		memset(str, 0, 128);
		if (end)
			strncpy(str, start, end - start);
		else if (tmp)
			strncpy(str, start, tmp - start);
		else
			goto exit;


		ret->wrspd_avr = ret->rdspd_avr = strtoul(str, NULL, 0) * unit * 8; //by bits/s
		ret->wrdone = ret->rddone = 1;
		printf("%s seq_rdspd_avr %s %llu bits/s\n", __FUNCTION__, str, ret->wrspd_avr);
	}

exit:
	return 0;
}

static int storage_app_stressapp_file_test(struct vediag_test_info *info,
		unsigned long long offset, unsigned long long length,
		unsigned long long time, int pfmode, struct stor_test_result *result)
{
#define STRESSAPP_FILE_TEST_SIZEMB 	10

	unsigned long long blkcnt, blkoff, off, len, len_MB /*, off_MB*/;
    int ret = 0, linenum = 0;
    char *test_name = NULL, *file_output = NULL;

    FILE *devf;
    char *cmd = NULL, *buf = NULL, *line = NULL;
    char *retcmp = NULL, *retstr = NULL, *rdstr = NULL, *wrstr = NULL;
    int chkdone = 0;
	struct timeval tm;

	struct vediag_storage_test_info *test_arg = (struct vediag_storage_test_info *) info->data;
	char *diskpath = test_arg->diskpath;
	char *apppath = test_arg->apppath;
	char *testpath = test_arg->testpath;

    blkoff = offset / STORAGE_SECTOR_SIZE;
    blkcnt = length / STORAGE_SECTOR_SIZE;
    off = blkoff * STORAGE_SECTOR_SIZE;
    len = blkcnt * STORAGE_SECTOR_SIZE;

    vediag_debug(VEDIAG_STORAGE, info->dbg_info, "\nSTORAGE STRESSAPP TEST: blkoff 0x%llx (0x%llx) blkcnt 0x%llx (0x%llx) testpath %s \n",
            blkoff, offset, blkcnt, len, testpath);

    /* Malloc */
    test_name = malloc(128);
    file_output = malloc(128);
    cmd = malloc(TEXT_LINE_SIZE);
    retcmp = malloc(TEXT_LINE_SIZE);
    buf = malloc(TEXT_LINE_SIZE);
    retstr = malloc(TEXT_LINE_SIZE);
    rdstr = malloc(TEXT_LINE_SIZE);
    wrstr = malloc(TEXT_LINE_SIZE);
	if (!test_name || !file_output || !cmd || !retcmp || !buf || !retstr || !rdstr || !wrstr) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Alloc fail\n", __FUNCTION__, __LINE__);
        ret = -1;
        goto exit;
	}
	memset(test_name, 0, 128);
	memset(file_output, 0, 128);
    memset(cmd, 0, TEXT_LINE_SIZE);
    memset(retcmp, 0, TEXT_LINE_SIZE);
    memset(buf, 0, TEXT_LINE_SIZE);
    memset(retstr, 0, TEXT_LINE_SIZE);
    memset(rdstr, 0, TEXT_LINE_SIZE);
    memset(wrstr, 0, TEXT_LINE_SIZE);

    /* Run Stressapp test */
    len_MB = length / (1 << 20);
    //off_MB = offset / (1 << 20);

    // create file for test/output
    gettimeofday(&tm, NULL);
    unsigned long tempvar = tm.tv_usec;

    sprintf(test_name, "%s_%s", STORAGE_FILE_TEST_NAME, diskpath + 5);
    sprintf(file_output, "/tmp/%s_%lu_%ld.txt", diskpath + 5, pthread_self(), tempvar);

    // create cmd for test	// -v 20 if debug mode
    if (apppath && strlen(apppath)) {
    	if (time > 0) {	//test by time
    		sprintf(cmd, "%s -f %s/%s --filesize %d -M %d -m 0 -s %llu",
    						apppath, testpath, test_name, STRESSAPP_FILE_TEST_SIZEMB * (1 << 20),
							STRESSAPP_FILE_TEST_SIZEMB * 2, time);
    	} else {
    		vediag_err(VEDIAG_STORAGE, info->dbg_info, "Stressapp not support test by Size !!\n");
			ret = -1;
			goto exit;
    	}
    } else {
        vediag_err(VEDIAG_STORAGE, info->dbg_info, "Not found Stressapp for test\n");
        ret = -1;
        goto exit;
    }
    vediag_debug(VEDIAG_STORAGE, info->dbg_info, "\nStressapp cmd: %s\n", cmd);

    // create result string to check pass/fail
	sprintf(retcmp, "Stats: Completed:");	/* String to check FIO done */
    vediag_debug(VEDIAG_STORAGE, info->dbg_info, "\nretcmp: '%s'\n", retcmp);

    // call app stressapp
    devf = popen(cmd, "r");
    if (!devf) {
        vediag_err(VEDIAG_STORAGE, info->dbg_info, "Cannot run test with %s\n", cmd);
        ret = -1;
        goto exit;
    }

	vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s open devf %p\n", test_name, devf);
	while (1) {
		memset(buf, 0, TEXT_LINE_SIZE);
		line = fgets(buf, TEXT_LINE_SIZE, devf);

		if (line == NULL) break;
		linenum++;
		//vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s", line);

		if (!chkdone && strstr(line, retcmp)) {
			strcpy(retstr, line);
			chkdone = 1;
		}
	}

	if (pclose(devf) == -1)
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "%s %d Close command fail, error %s !!\n", __FUNCTION__, __LINE__, strerror(errno));

	/* Check result from app test */

    /* Parse result to get error number */
    if (!strlen(retstr)) {
    	vediag_err(VEDIAG_STORAGE, info->dbg_info, "Result is NULL (line %d) --> FAIL !!\n", linenum);
		ret = -1;
		goto exit;
	}
	vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s\n", retstr);

	char *ptr;
	ptr = strstr(retstr, "with");
	if (ptr) {
		if (strncmp(ptr + 5, "0", 1) == 0) {
			vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s PASS\n", __FUNCTION__);
		}else {
			vediag_err(VEDIAG_STORAGE, info->dbg_info, "Stressapp test got FAIL !!\n");
			ret = -1;
		}
	}

	ptr = strstr(retstr, "incidents,");
	if (ptr) {
		if (strncmp(ptr + 11, "0", 1) == 0) {
			vediag_debug(VEDIAG_STORAGE, info->dbg_info, "%s PASS\n", __FUNCTION__);
		}else {
			vediag_err(VEDIAG_STORAGE, info->dbg_info, "Stressapp test got FAIL !!\n");
			ret = -1;
		}
	}

	/* Get return size if test by time / performance number */
	if (time > 0) {
		storage_app_stressapp_parse_result(retstr, result);

		// normal test just both write and read verify with same size
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "real_size %d \n", result->wrsize);

		if (test_arg->test_performance) {
			//stressapp not separate Read/Write, so result r/w is same value
			vediag_info(VEDIAG_STORAGE, info->dbg_info, "SIZE = %llu %s, SPEED = %llu %s\n",
					result->wrsize >= (1 << 20)?result->wrsize / (1 << 20):result->wrsize / (1 << 10),
					result->wrsize >= (1 << 20)?"MB":"KB",
					result->wrspd_avr >= (8 * (1 << 20))?result->wrspd_avr / (8 * (1 << 20)):result->wrspd_avr / (8 * (1 << 10)),
					result->wrspd_avr >= (8 * (1 << 20))?"MB/s":"KB/s");

			//stressapp not test to get avl table, so check by user only
			vediag_warn(VEDIAG_STORAGE, info->dbg_info, "stressapp performance need to verify by user !\n");
			//storage_perf_result_check(info, result, pfmode);
		}
	}

exit:
	if (test_name)
		free(test_name);

	if (file_output)
		free(file_output);

	if (cmd)
        free(cmd);

    if (retcmp)
        free(retcmp);

    if (buf)
        free(buf);

    if (retstr)
        free(retstr);

    return ret;
}


/********************************************* Storage test function *******************************************/
static int storage_transfer_test(struct vediag_test_info *info, unsigned long long offset,
		unsigned long long length, unsigned long long time,	int pfmode, struct stor_test_result *result)
{
	int ret = 0;
	char *current_devpath = NULL;
	struct vediag_storage_test_info *test_arg = (struct vediag_storage_test_info *) info->data;

	/* Check path to see device exist or change */
#if 0
	current_devpath = vepath_alloc_n_get_devpath(test_arg->diskpath);
	if (current_devpath) {
		if (strcmp(test_arg->devpath, current_devpath) != 0) {
			vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Dev path: %s\n", test_arg->devpath);
			vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Current path: %s\n", current_devpath);
			vediag_err(VEDIAG_STORAGE, info->dbg_info, "State was changed (Reconnect or change another device?) !!\n");
			ret = -STORAGE_SCAN;
			goto exit;
		}
	}else {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "State was changed (Disconnect?) !!\n");
		ret = -STORAGE_SCAN;
		goto exit;
	}
#else
	if (storage_check_path_exist(test_arg->devpath)) {
		current_devpath = vepath_alloc_n_get_devpath(test_arg->diskpath);
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "State was changed (Disconnect/Reconnect ?) !!\n");
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Dev path: %s\n", test_arg->devpath);
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "Current path: %s\n", current_devpath);
		ret = -STORAGE_SCAN;
		goto exit;
	}
#endif

	/* Check testpath exist */
	if (test_arg->testpath && storage_check_path_exist(test_arg->testpath)) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "Directory for test %s NOT exist !!\n", test_arg->testpath);
		ret = -STORAGE_SCAN;
		goto exit;
	}

	/*if (info->test_mode == STORAGE_MODE_RAW_TEST && !storage_check_mount_partition(info, test_arg->diskpath, NULL)) {
		vediag_err(VEDIAG_STORAGE, info->dbg_info, "was mounted so not safe for RAW mode testing !!\n");
		ret = -STORAGE_SCAN;
		goto exit;
	}*/

	/* Run test */
	if (info->test_app == STORAGE_FIO_TEST ||
			   info->test_app == STORAGE_EXTERNAL_FIO_TEST ||
			   info->test_app == STORAGE_INTERNAL_FIO_TEST) {
        if(info->test_mode == STORAGE_MODE_RAW_TEST) {
        	if(test_arg->test_performance) {
        		vediag_err(VEDIAG_STORAGE, info->dbg_info, "FIO test no support performance test in Raw mode !!\n");
        		ret = -STORAGE_SCAN;
        		goto exit;
        	} else
        		ret = storage_app_fio_raw_test(info, offset, length);
        }else
            ret = storage_app_fio_file_test(info, offset, length, time, pfmode, result);	//currently no use time although it is supported
	} else if (info->test_app == STORAGE_STRESSAPP_TEST ||
			   info->test_app == STORAGE_EXTERNAL_STRESSAPP_TEST ||
			   info->test_app == STORAGE_INTERNAL_STRESSAPP_TEST) {
		 if(info->test_mode == STORAGE_MODE_RAW_TEST) {
				vediag_err(VEDIAG_STORAGE, info->dbg_info, "Stressapp test no support Raw mode !!\n");
				ret = -STORAGE_SCAN;
				goto exit;
		 } else
			 ret = storage_app_stressapp_file_test(info, offset, length, time, pfmode, result);
	} else {
		if(info->test_mode == STORAGE_MODE_RAW_TEST && !test_arg->test_performance)
			ret = storage_rw_raw_test(info, offset, length);
		else {
			vediag_err(VEDIAG_STORAGE, info->dbg_info, "RW test no support File mode or performance test!!\n");
			ret = -STORAGE_SCAN;
			goto exit;
		}
	}

	/* Get result & check error from path*/
	if (test_arg->ext_check_err) {
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "\nContinue check error from parent interface\n");
		vediag_debug(VEDIAG_STORAGE, info->dbg_info, "\nNOT SUPPORTED YET\n");
	}

exit:
	if (current_devpath)
		free(current_devpath);

	return ret;
}

#ifdef CONFIG_RAS
/**
 * Check storage RAS error
 * @param physical_dev
 * @return 0 if no error
 */
static int storage_ras_check(char* physical_dev)
{
	/*
	 * - physical_dev starter string: USB_1, SATA_2..
	 * - RAS error from queue: USB1, SATA23...
	 * Only check for USB and SATA, others storage through PCIe (LSI, NVMe) will
	 * be checked in diag pci
	 */
	int ret = STORAGE_NONE;
	if (cbIsEmpty(&ras_cb_stor)) {
		goto exit;
	}

	if (strncmp(physical_dev, "USB", 3) == 0) {
		/* RAS output for USB: "USB0", "USB1" */
		ElemType *e;
		int portnum = -1;

		e = calloc(1, sizeof(ElemType));
		if (!e) {
			xerror("Error: %s %d malloc failed\n", __FILE__, __LINE__);
			goto exit;
		}

		cbRead(&ras_cb_stor, e);/* example: e->buf = "USB1"*/

		if (strlen(e->buf) == 4) {
			portnum = e->buf[3] - '0';
		} else {
			xinfo("Warning: [RAS]  %s %d: Invalid USB format (%s)\n",
					__FILE__, __LINE__, e->buf);
			free(e);
			goto exit;
		}

		/* "USB_0", "USB_1"*/
		if (strlen(physical_dev) >= 5) {
			if (portnum == (physical_dev[4] - '0')) {
				vediag_err(VEDIAG_STORAGE, physical_dev, "Error: Detected RAS error\n");
				return -STORAGE_RAS_ERR;
			}
		} else {
			/* not for me, write back to queue */
			cbWrite(&ras_cb_stor, e);
		}

		free(e);

	} else if (strncmp(physical_dev, "SATA", 3) == 0) {
		/* RAS output for SATA: "SATA01", "SATA23" */
		ElemType *e;
		int portnum = -2;

		e = calloc(1, sizeof(ElemType));
		if (!e) {
			xerror("Error: %s %d malloc failed\n", __FILE__, __LINE__);
			goto exit;
		}

		cbRead(&ras_cb_stor, e);/* example: e->buf = "SATA23"*/

		if (strlen(e->buf) == 6) {
			portnum = e->buf[4] - '0';
		} else {
			xinfo("Warning: [RAS]  %s %d: Invalid SATA format (%s)\n",
					__FILE__, __LINE__, e->buf);
			/* not for me, write back to queue */
			cbWrite(&ras_cb_stor, e);
			free(e);
			goto exit;
		}

		/* ex: "SATA_3"*/
		if (strlen(physical_dev) >= 6) {
			if ((portnum == (physical_dev[5] - '0'))
					|| ((portnum + 1) == (physical_dev[5] - '0'))) {
				vediag_err(VEDIAG_STORAGE, physical_dev, "Error: Detected RAS error\n");
				return -STORAGE_RAS_ERR;
			}
		} else {
			/* not for me, write back to queue */
			cbWrite(&ras_cb_stor, e);
		}

		free(e);
	}
exit:
	return ret;
}
#endif

static void *storage_test_execute(void * __data)
{
    struct vediag_test_info *info = (struct vediag_test_info *) __data;
    struct vediag_test_control *test_ctrl = info->test_ctrl;
    struct vediag_storage_test_info *test_arg = (struct vediag_storage_test_info *) info->data;
    int test_done = 0;
    unsigned long long off = 0, chunk_size = 0, chunk_time = 0, total_size = info->total_size;
	unsigned long long run_off = 0, run_size = 0, run_time = 0; 

    int pfcnt = 0, pfmode[2] = { SEQ_WRITE, SEQ_READ };	
    struct stor_test_result result;

    vediag_info(VEDIAG_STORAGE, NULL, "Testing device %s (physical %s)\n",
               info->device,
    		   info->physical_dev ? info->physical_dev : "Unknown");

    /* Setup test condition */
    if (info->test_app == STORAGE_STRESSAPP_TEST ||
    		info->test_app == STORAGE_EXTERNAL_STRESSAPP_TEST ||
			info->test_app == STORAGE_INTERNAL_STRESSAPP_TEST) {
		off = 0;
		chunk_size = 0;
		
		if (test_arg->test_performance) {
			chunk_time = info->runtime / (sizeof(pfmode)/sizeof(int));
		}else 
			chunk_time = info->runtime > 10?10:info->runtime;
	}else if (info->test_app == STORAGE_FIO_TEST ||
    		info->test_app == STORAGE_EXTERNAL_FIO_TEST ||
			info->test_app == STORAGE_INTERNAL_FIO_TEST) {
		off = info->test_mode==STORAGE_MODE_FILE_TEST?0:info->offset;
		chunk_size = info->chunk_size?info->chunk_size:STORAGE_DEFAULT_FIO_CHUNK_SIZE;
		
		if (test_arg->test_performance) {
    		chunk_time = info->runtime / (sizeof(pfmode)/sizeof(int));
		}else
			chunk_time = 0;	//currently normal test fio no use time (except performance test) 
	}else {
		off = info->offset;
		chunk_size = info->chunk_size?info->chunk_size:STORAGE_DEFAULT_RAW_CHUNK_SIZE;
		chunk_time = 0;
	}
	
	if (chunk_size && chunk_size > total_size)
		chunk_size = total_size;
	
    /* Run test */
	vediag_info(VEDIAG_STORAGE, info->dbg_info, "Storage test: off 0x%llx total_size 0x%llx chunk_size 0x%llx chunk_time 0x%llx\n",
			off, total_size, chunk_size, chunk_time);
	run_off = off;
    while (!test_done) {
        if(test_ctrl->data_stop) {
        	test_done = 1;
        	continue;
        }

		// wait here until stop if test by time
    	if ((run_time && run_time >= info->runtime) || pfcnt >= sizeof(pfmode)/sizeof(int))
    		continue;

        sto_debug("-> %s info @%p: threads %d total_size 0x%llx run_size 0x%llx runtime %llu \n",
        		diskpath, info, info->threads, total_size, run_size, run_time);

        if(info->test_mode == STORAGE_MODE_RAW_TEST &&
           (run_off + run_size) > total_size) {
        	vediag_warn(VEDIAG_STORAGE, info->dbg_info, "[WARNING] Roll over the offset because of over total test size\n");
            run_off = off;
        }

        /* Transfer data */
		memset(&result, 0, sizeof(struct stor_test_result));
    	test_ctrl->error = storage_transfer_test(info, run_off, chunk_size, chunk_time, pfmode[pfcnt], &result);
    	//actually no use off when run test by time now
    	run_off += chunk_size;
#ifdef CONFIG_RAS
		test_ctrl->error |= storage_ras_check(info->physical_dev);
#endif
    	if(test_ctrl->error) {
			while(!test_ctrl->test_restart) {
				sleep(1);
			}
			test_ctrl->test_restart = 0;
        }else {
            /* Update tested size, offset and decrease size test */
        	if (chunk_time) {
        		test_ctrl->tested_size += result.wrdone?result.wrsize:result.rddone?result.rdsize:0;	
        	}else
        		test_ctrl->tested_size += chunk_size;
//        	schedule();
        	sleep(0);
        }
		
		if (test_arg->test_performance) 
    		pfcnt++;
    }

	test_ctrl->data_running --;
    pthread_exit(NULL);
}

static int storage_test_help(void)
{
	xprint("\nSTORAGE test:\n"
			"  diag storage [opt ...]"
			"  Options: \n"
			"    -h               : print help\n"
			"    -d </dev/sdX>    : select device test\n"
			"    -o <offset>      : offset >= 0\n"
			"    -s <size>        : size >= 0\n"
			"    -f               : test full size (high priority than size)\n"
			"    -t <threads>     : number of threads >= 1\n"
			"    -time <time>     : test by time (high priority than 'by size')\n"
			"    -mode <file|raw> : mode to test (recommend file)\n"
			"    -app <rw|fio>    : app to test\n"
			"    -chunk <size>    : chunk size in test progress\n"
			);

	return 0;
}

/* This function run first, then storage_get_devnum, and storage_check_dev */
static int storage_test_start (int argc, char *argv[], void * _data)
{
    int arp;
    struct vediag_test_info *info = (struct vediag_test_info *) _data;

    if ((arp = vediag_getopt(argc, argv, "h")) > 0) {
    	storage_test_help();
        return 0;
    }

    /* Select test device name /dev/sdX */
    info->dev_mask = STORAGE_INITIAL_DEVMASK;	//no input
    if ((arp = vediag_getopt(argc, argv, "-d")) > 0) {
    	if (strncmp(argv[arp + 1], "/dev/", 5) == 0)
    		info->device = strdup(argv[arp + 1]);
    	else if (isalpha(argv[arp + 1][0]))
    		info->physical_dev = strdup(argv[arp + 1]);
    	else if (isdigit(argv[arp + 1][0]))
    		info->dev_mask =  strtoul(argv[arp + 1], NULL, 0);
    	else {
        	storage_test_help();
            return 0;
    	}
    }else if ((arp = vediag_getopt(argc, argv, "-phy")) > 0)
    	info->physical_dev = strdup(argv[arp + 1]);
    else if ((arp = vediag_getopt(argc, argv, "-dev")) > 0)
    	info->device = strdup(argv[arp + 1]);

    /* Select performance test */
    if ((arp = vediag_getopt(argc, argv, "-pf")) > 0) {
        info->flags |= VEDIAG_TEST_PERFORMANCE;
        sto_debug(VEDIAG_STORAGE, "Select perfomance test (only for app test)\n");
    }

    /* Get offset option */
    info->offset = STORAGE_INITIAL_OFFSET;		//no input
    if ((arp = vediag_getopt(argc, argv, "-o")) > 0) {
    	unsigned long long dev_off = strtoul(argv[arp + 1], NULL, 0);
    	if (dev_off >= 0)
        	info->offset = dev_off;
    	else
    		info->offset = STORAGE_DEFAULT_OFFSET;
        sto_debug(VEDIAG_STORAGE, "Input Offset: 0x%llx\n", dev_off);
    }

    /* Get size option */
    info->total_size = STORAGE_INITIAL_TEST_SIZE;	//no input
    if ((arp = vediag_getopt(argc, argv, "-s")) > 0) {
        unsigned long long dev_size = strtoul(argv[arp + 1], NULL, 0);
        if (dev_size > 0)
        	info->total_size = dev_size;
        else
        	info->total_size = STORAGE_DEFAULT_TEST_SIZE;
        sto_debug(VEDIAG_STORAGE, "Input Size: 0x%llx\n", dev_size);
    }

    /* Check if test full device size */
    if ((arp = vediag_getopt(argc, argv, "-f")) > 0) {
    	sto_debug(VEDIAG_STORAGE, "Full size enable!");
        info->flags |= VEDIAG_TEST_FULL_SIZE;
    }

    /* Get thread option */
    info->threads = STORAGE_INITIAL_THREADS;	//no input
    if ((arp = vediag_getopt(argc, argv, "-t")) > 0) {
        int threads = strtoul(argv[arp + 1], NULL, 0);
        if (threads <= VEDIAG_MAX_THREADS)
        	info->threads = threads;
        else
        	info->threads = STORAGE_DEFAULT_THREADS;
    }

    /* Get time option for transfer data */
    info->runtime = STORAGE_INITIAL_RUNTIME;	//no input
    if ((arp = vediag_getopt(argc, argv, "-time")) > 0) {
    	int runtime = strtoul(argv[arp + 1], NULL, 0);
        if (runtime > 0)
        	info->runtime = runtime;
        else
        	info->runtime = STORAGE_DEFAULT_RUNTIME;
    }

    /* Get mode option for transfer data */
    info->test_mode = -1;	//no input
    if ((arp = vediag_getopt(argc, argv, "-mode")) > 0) {
    	if (strncmp(argv[arp + 1], "file", 4) == 0) {
    		info->test_mode = STORAGE_MODE_FILE_TEST;
    	}else	/* "raw" */
    		info->test_mode = STORAGE_MODE_RAW_TEST;
    }

    /* Get app option for transfer data */
    info->test_app = -1;	//no input
    if ((arp = vediag_getopt(argc, argv, "-app")) > 0) {
    	if (strncmp(argv[arp + 1], "fio", 3) == 0) {
    		info->test_app = STORAGE_FIO_TEST;
    	}else if (strncmp(argv[arp + 1], "ext_fio", 7) == 0) {
    		info->test_app = STORAGE_EXTERNAL_FIO_TEST;
    	}else if (strncmp(argv[arp + 1], "int_fio", 7) == 0) {
    		info->test_app = STORAGE_INTERNAL_FIO_TEST;
    	}else if (strncmp(argv[arp + 1], "stressapp", 9) == 0) {
    		info->test_app = STORAGE_STRESSAPP_TEST;
    	}else if (strncmp(argv[arp + 1], "ext_stressapp", 13) == 0) {
    		info->test_app = STORAGE_EXTERNAL_STRESSAPP_TEST;
    	}else if (strncmp(argv[arp + 1], "int_stressapp", 13) == 0) {
    		info->test_app = STORAGE_INTERNAL_STRESSAPP_TEST;
    	}else
    		info->test_app = STORAGE_RW_TEST;
    }

    /* Get chunk option for transfer data */
    info->chunk_size = STORAGE_INITIAL_CHUNK_SIZE;	//no input
    if ((arp = vediag_getopt(argc, argv, "-chunk")) > 0) {
    	info->chunk_size = strtoul(argv[arp + 1], NULL, 0);
        if (info->chunk_size <= 0)
        	info->chunk_size = STORAGE_DEFAULT_RAW_CHUNK_SIZE;
    }

    return sizeof(struct vediag_test_info);
}

static int storage_get_app(char *app)
{
	if (app && !strcmp(app, "fio"))
		return STORAGE_FIO_TEST;
	else if (app && !strcmp(app, "ext_fio"))
		return STORAGE_EXTERNAL_FIO_TEST;
	else if (app && !strcmp(app, "int_fio"))
		return STORAGE_INTERNAL_FIO_TEST;
	else if (app && !strcmp(app, "stressapp"))
		return STORAGE_STRESSAPP_TEST;
	else if (app && !strcmp(app, "ext_stressapp"))
		return STORAGE_EXTERNAL_STRESSAPP_TEST;
	else if (app && !strcmp(app, "int_stressapp"))
		return STORAGE_INTERNAL_STRESSAPP_TEST;
	else
		return STORAGE_RW_TEST;
}

static int storage_create_arg(struct list_head *arg_list)
{
	/*Offset or Full_size used for raw mode */
	// vediag_set_argument("Offset", "0", arg_list);
	// vediag_set_argument("Full_size", "Yes", arg_list);
	vediag_set_argument("Enable", "Yes", arg_list);
	vediag_set_argument("Threads", "1", arg_list);
	vediag_set_argument("Mode", "file", arg_list); //raw, file, director
	vediag_set_argument("Time", "600", arg_list);
	vediag_set_argument("Size", "0x40000000", arg_list);
	vediag_set_argument("Chunk_size", "0xA00000", arg_list);
	vediag_set_argument("Performance", "No", arg_list);
	// vediag_set_argument("Ext_check", "", arg_list);

	return 0;
}

static int storage_check_dev(struct vediag_test_info *info, struct vediag_test_info *dev_info, char **error)
{
    struct vediag_storage_test_info *test_arg = NULL;
	char *str;
	int ret = 1;	//1 is check OK

    if(IS_ERR_OR_NULL(info)) {
        vediag_err(VEDIAG_STORAGE, NULL, "%s(): info NULL\n", __func__);
    	ret = 0;
    	goto exit;
    }

    if(IS_ERR_OR_NULL(dev_info)) {
        vediag_err(VEDIAG_STORAGE, NULL, "%s(): dev_info NULL\n", __func__);
    	ret = 0;
    	goto exit;
    }

    test_arg = malloc(sizeof(struct vediag_storage_test_info));
    if(IS_ERR_OR_NULL(test_arg)) {
    	vediag_err(VEDIAG_STORAGE, dev_info->dbg_info, "Allocate memory failed\n");
    	*error = strdup("Out of memory");
    	ret = 0;
    	goto exit;
    }
    memset(test_arg, 0, sizeof(struct vediag_storage_test_info));

    /* Check device exist on system */
	if (storage_check_path_exist(dev_info->device)) {
		*error = strdup("No device");
    	ret = 0;
    	goto exit;
	}

	/* Argument from prompt is higher priority */
	if (info->test_app != -1)
		dev_info->test_app = info->test_app;

    if (info->test_mode != -1)
    	dev_info->test_mode = info->test_mode;
    else {
    	str = vediag_get_argument(dev_info, "Mode");
    	if (str && strcasecmp(str, "file") == 0) {
    		dev_info->test_mode = STORAGE_MODE_FILE_TEST;
            xinfo("mode file ############ \n");
        }
    	else {
    		dev_info->test_mode = STORAGE_MODE_RAW_TEST;
            xinfo("mode raw ############ \n");
        }

    	if (str)
    		free(str);
    }

    /* must check offset & size (or -f) after test_arg->mode */
	if (info->offset != STORAGE_INITIAL_OFFSET)
    	dev_info->offset = info->offset;
    else {
    	str = vediag_get_argument(dev_info, "Offset");
    	if (str && strlen(str))
    		dev_info->offset = strtoull(str, NULL, 0);
    	else
    		dev_info->offset = STORAGE_DEFAULT_OFFSET;

    	if (str)
    		free(str);
    }

    if (info->total_size != STORAGE_INITIAL_TEST_SIZE) {
    	dev_info->total_size = info->total_size;
    }else {
    	str = vediag_get_argument(dev_info, "Size");
    	if (str && strlen(str))
    		dev_info->total_size = strtoul(str, NULL, 0);
    	else
        	dev_info->total_size = STORAGE_DEFAULT_TEST_SIZE;

    	if (str)
    		free(str);
    }

	/* -f is high priority */
	if ((info->flags & VEDIAG_TEST_FULL_SIZE) == VEDIAG_TEST_FULL_SIZE) {
		dev_info->offset = 0;
		dev_info->total_size = storage_get_size(dev_info, dev_info->device);	/*Count storage free size*/
	} else {
    	str = vediag_get_argument(dev_info, "Full_size");
    	if (str && strlen(str) && (strcasecmp(str, "yes") == 0))	/* full_size is flag, enable if diff. 0 */
    		dev_info->total_size = storage_get_size(dev_info, dev_info->device);

    	if (str)
    		free(str);
	}

	/* Enable test this device or not */
	if (info->enable && (strncasecmp(info->enable, STORAGE_TEST_DISABLE, 2) == 0)) {
		dev_info->enable = strdup("No");
	} else {
    	str = vediag_get_argument(dev_info, "Enable");
    	if (str && strlen(str) && (strcasecmp(str, "No") == 0))	/* full_size is flag, enable if diff. 0 */
    		dev_info->enable = strdup("No");

    	if (str)
    		free(str);
	}

	if (info->threads != STORAGE_INITIAL_THREADS) {
    	dev_info->threads = info->threads;
	} else {
    	str = vediag_get_argument(dev_info, "Threads");
    	if (str && strlen(str))
    		dev_info->threads = strtoul(str, NULL, 0);
    	else
        	dev_info->threads = STORAGE_DEFAULT_THREADS;

    	if (str)
    		free(str);
    }

	/* Runtime must set by user input or config file */
    if (info->runtime != STORAGE_INITIAL_RUNTIME)
    	dev_info->runtime = info->runtime;
    else {
    	str = vediag_get_argument(dev_info, "Time");
    	if (str && strlen(str) && strtoul(str, NULL, 0))	/* runtime is flag, enable if diff. 0 */
    		dev_info->runtime = strtoul(str, NULL, 0);
    	else
    		dev_info->runtime = 0;	/*0: ignore run by time, use run by size */

    	if (str)
    		free(str);
    }

    if (info->chunk_size != STORAGE_INITIAL_CHUNK_SIZE)
    	dev_info->chunk_size = info->chunk_size;
    else {
    	str = vediag_get_argument(dev_info, "Chunk_size");
    	if (str && strlen(str))
    		dev_info->chunk_size = strtoul(str, NULL, 0);
    	else {
			if (dev_info->test_app == STORAGE_FIO_TEST ||
					dev_info->test_app == STORAGE_EXTERNAL_FIO_TEST ||
					dev_info->test_app == STORAGE_INTERNAL_FIO_TEST)
				dev_info->chunk_size = STORAGE_DEFAULT_FIO_CHUNK_SIZE;
			else if (dev_info->test_app == STORAGE_STRESSAPP_TEST ||
					dev_info->test_app == STORAGE_EXTERNAL_STRESSAPP_TEST ||
					dev_info->test_app == STORAGE_INTERNAL_STRESSAPP_TEST)
				dev_info->chunk_size = STORAGE_DEFAULT_STRESSAPP_CHUNK_SIZE;
			else
				dev_info->chunk_size = STORAGE_DEFAULT_RAW_CHUNK_SIZE;
		}

    	if (str)
    		free(str);
    }

	str = vediag_get_argument(dev_info, "Ext_check");
	if (str && strlen(str) && strtoul(str, NULL, 0))
		test_arg->ext_check_err = strtoul(str, NULL, 0);
	else
		test_arg->ext_check_err = 0;

	if (str)
		free(str);

    /* Store info for test_arg */
	if (info->interface)
		test_arg->interface_name = strdup(info->interface);
	if (dev_info->device)
		test_arg->diskpath = strdup(dev_info->device);
    test_arg->devpath = vepath_alloc_n_get_devpath(dev_info->device);
    if (dev_info->physical_dev)
    	test_arg->phypath = strdup(dev_info->physical_dev);
    test_arg->disk_size = storage_get_size(dev_info, dev_info->device);

    test_arg->apppath = storage_alloc_n_get_apppath(dev_info->test_app);
	if (!strlen(test_arg->apppath))	{
		vediag_err(VEDIAG_STORAGE, dev_info->dbg_info, "Not found test app\n");
		*error = strdup("No test app");
    	ret = 0;
    	goto exit;
	}

	// setup test_arg->testpath for file test if available
	if (dev_info->test_mode == STORAGE_MODE_FILE_TEST) {
		test_arg->testpath = storage_alloc_n_make_testpath_dir(dev_info, dev_info->device);
		if (!test_arg->testpath) {
			vediag_err(VEDIAG_STORAGE, dev_info->dbg_info, "No dir for test\n");
			*error = strdup("No dir for test");
	    	ret = 0;
	    	goto exit;
		}
	}

	if (dev_info->test_mode == STORAGE_MODE_FILE_TEST) {
		sprintf(test_arg->outfile, "%s_%s_%s", STORAGE_FILE_TEST_NAME, test_arg->diskpath + 5,
				test_arg->testpath + strlen(test_arg->testpath) - 4);
		if (!test_arg->testpath) {
			vediag_err(VEDIAG_STORAGE, dev_info->dbg_info, "No dir for test\n");
			*error = strdup("No dir for test");
	    	ret = 0;
	    	goto exit;
		}
	}

	if (dev_info->test_mode == STORAGE_MODE_RAW_TEST && !storage_check_mount_partition(dev_info, test_arg->diskpath, NULL)) {
		vediag_err(VEDIAG_STORAGE, dev_info->dbg_info, "was mounted so not safe for RAW mode testing !!\n");
		*error = strdup("RAW mode not support mounted disk");
		ret = 0;
		goto exit;
	}

	// enable test performance or not (ignore size/chunk size)
	if (info->flags & VEDIAG_TEST_PERFORMANCE) {
		test_arg->test_performance = 0; /* 1 */
	} else {
		test_arg->test_performance = 0;
	}

    dev_info->data = test_arg;

    /* Print info of test device */
    xdebug("Found device %s (physical %s)\n", dev_info->device,
    		dev_info->physical_dev?dev_info->physical_dev:"Unknown");
    xdebug("Info\n"
           "dev_info @%p\n"
           " interface %s\n"
           " cfg_file  %s\n"
           " device    %s\n"
           " physical_dev %s\n"
           " test_type %d\n"
           " test_mode %d\n"
           " test_app %d\n"
           " threads   %d\n"
           " core_nums %d\n"
           " runtime   %d\n"
           " dev_mask  0x%x\n"
           " depend_dev_mask 0x%x\n"
           " offset    0x%llx\n"
           " total_size 0x%llx\n"
           " chunk_size 0x%llx\n"
           " flags     %d\n"
           " data (test_arg) @%p\n"
           "  - interface_name %s\n"
           "  - diskpath %s\n"
           "  - testpath  %s\n"
           "  - devpath  %s\n"
           "  - phypath  %s\n"
           "  - performance %d\n"
           "  - ext_check_err %d\n",
           dev_info,
           dev_info->interface,
           dev_info->cfg_file,
           dev_info->device,
           dev_info->physical_dev,
           dev_info->test_type,
           dev_info->test_mode,
           dev_info->test_app,
           dev_info->threads,
           dev_info->core_nums,
           dev_info->runtime,
           dev_info->dev_mask,
           dev_info->depend_dev_mask,
           dev_info->offset,
           dev_info->total_size,
           dev_info->chunk_size,
           dev_info->flags,
           dev_info->data,
           test_arg->interface_name,
           test_arg->diskpath,
           test_arg->testpath,
           test_arg->devpath,
           test_arg->phypath,
           test_arg->test_performance,
    	   test_arg->ext_check_err);

#ifdef CONFIG_RAS
	/* Clear RAS error in Storage queue */
	double start_time = get_time_sec();
	while (!cbIsEmpty(&ras_cb_stor)) {
		ElemType *e;
		e = calloc(1, sizeof(ElemType));
		if (!e)
			break;

		cbRead(&ras_cb_stor, e);
		free(e);

		/* break if error is bumped too fast*/
		if (get_time_sec() - start_time > 3)
			break;
	}
#endif

exit:
	if (!ret) {
		if (test_arg->interface_name)
			free(test_arg->interface_name);

		if (test_arg->diskpath)
			free(test_arg->diskpath);

		if (test_arg->devpath)
			free(test_arg->devpath);

		if (test_arg->phypath)
			free(test_arg->phypath);

		if (test_arg->apppath)
			free(test_arg->apppath);

		if (test_arg->testpath)
			free(test_arg->testpath);

		if (test_arg)
			free(test_arg);
	}

	return ret;
}

static int storage_test_complete(void *data)
{
	struct vediag_test_info *info = (struct vediag_test_info *) data;
	struct vediag_storage_test_info *test_arg = (struct vediag_storage_test_info *) (info->data);
	if (test_arg){
		/* Remove file/directory test, still keep mount */
		if (test_arg->testpath) {
			storage_remove_path(info, test_arg->outfile);
			storage_remove_path(info, test_arg->testpath);
			free(test_arg->testpath);
		}

		if (test_arg->interface_name)
			free(test_arg->interface_name);
		if (test_arg->diskpath)
			free(test_arg->diskpath);
		if (test_arg->devpath)
			free(test_arg->devpath);
		if (test_arg->phypath)
			free(test_arg->phypath);
		if (test_arg->apppath)
			free(test_arg->apppath);
		free(test_arg);
	}

	return 0;
}

struct vediag_test_device vediag_storage = {
	.type			= VEDIAG_STORAGE,
	.flags			= VEDIAG_SUPPORT_PHY_TEST,
	.name			= "STORAGE",
	.cmd			= "stor",
	.desc			= "STORAGE TEST",
	.default_app	= "RW", /* "fio" */
	.get_test_app	= storage_get_app,
	.create_argument= storage_create_arg,
    .test_start     = storage_test_start,
    .check_dev      = storage_check_dev,
    .test_execute   = storage_test_execute,
    .test_complete  = storage_test_complete,
};

late_initcall(vediag_storage_test_register)
{
	return vediag_test_register(&vediag_storage);
}
