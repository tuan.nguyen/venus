/*
 * string_plus.c
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string_plus.h>
#include <ctype.h>
#include <unistd.h>
#include <common.h>
#include <malloc.h>
#include <circbuf.h>
#include <pthread.h>
#include <fcntl.h>
#include <log.h>
#include <sys/ioctl.h>

/*
 * char *get_word_in_string(char *input, int windex)
 *
 * @input: string input
 * @windex: word index, from 0 ...
 * @return: word
 */
char *get_word_in_string(char *input, char *delimiter, int windex)
{
	//char delimiter[] = " ";
	char *word, *context;
	int inputLength = 0;
	char *inputCopy = NULL;
	int index = windex;

	/* Sanity check */
	if(input == NULL)
		return NULL;

	//vediag_debug(VEDIAG_EVENT, "get_word_in_string", "input: '%s'\n", input);

	inputLength = strlen(input);

	//vediag_debug(VEDIAG_EVENT, "get_word_in_string", "inputLength: %d\n", inputLength);

	inputCopy = (char*) calloc(inputLength + 1, sizeof(char));
	strncpy(inputCopy, input, inputLength);

	word = strtok_r (inputCopy, delimiter, &context);
	//vediag_debug(VEDIAG_EVENT, "get_word_in_string", "0st: '%s'\n", word);
	if(index > 0) {
		for ( ; index--; ) {
			word = strtok_r (NULL, delimiter, &context);
			//vediag_debug(VEDIAG_EVENT, "get_word_in_string", "xst: '%s'\n", word);
			if(word == NULL)
				break;
		}
	}

	if (word != NULL)
		word = strdup(word);

	free(inputCopy);

	return word;
}

/*
 * int check_string_isdigit(char *input)
 *
 * @input: string input
 * @return: 1 if all in digit
 * @return: 0 if others
 */
int check_string_isdigit(char *input, int length)
{
	int i;

	/* Sanity check */
	if((input == NULL) || (strlen(input) < length))
		return 0;

	for ( i = 0; i < length; i++) {
		if(!isdigit(input[i]))
			return 0;
	}

	return 1;
}

/*
 * urandom()
 *
 * Create buffer random pattern
 * from /dev/urandom
 * If not available, use pattern 0x5a
 *
 * @input: buffer input
 * @return: 1 if all in digit
 * @return: 0 if others
 */
int urandom(unsigned char *input, unsigned int length)
{
	int fd = 0;
	size_t randomlen = 0;
	ssize_t result = 0;

	/* Sanity check */
	if ((input == NULL) || (length == 0))
		return -1;

	fd = open("/dev/urandom", O_RDONLY);
	if(fd < 0) {
		memset(input, 0x5a, length);
		return 0;
	}

	while (randomlen < length) 	{
		result = read(fd, input + randomlen, length - randomlen);
		if (result < 0) {
			memset(input, 0x5a, length);
			close(fd);
			return 0;
		}
		randomlen += result;
	}
	close(fd);

	return 0;
}

/*
 * Convert String input to LowerCase
 */
void strlwr(char *p)
{
	for ( ; *p; ++p) *p = tolower(*p);
}
