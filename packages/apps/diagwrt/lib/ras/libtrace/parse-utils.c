#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>

#include <log.h>

#include "event-utils.h"

#define __weak __attribute__((weak))

void __vdie(const char *fmt, va_list ap)
{
	int ret = errno;

	if (errno)
	    xerror("trace-cmd %s\n", strerror(errno));
	else
		ret = -1;

	xerror("  ");
	vxerror(fmt, ap);

	xerror("\n");
	exit(ret);
}

void __die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	__vdie(fmt, ap);
	va_end(ap);
}

void __weak die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	__vdie(fmt, ap);
	va_end(ap);
}

void __vwarning(const char *fmt, va_list ap)
{
	if (errno)
	    xerror("trace-cmd %s\n", strerror(errno));
	errno = 0;

	xerror("  ");
	vxerror(fmt, ap);

	xerror("\n");
}

void __warning(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	__vwarning(fmt, ap);
	va_end(ap);
}

void __weak warning(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	__vwarning(fmt, ap);
	va_end(ap);
}

void __vpr_stat(const char *fmt, va_list ap)
{
	vxdebug(fmt, ap);
	xdebug("\n");
}

void __pr_stat(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	__vpr_stat(fmt, ap);
	va_end(ap);
}

void __weak vpr_stat(const char *fmt, va_list ap)
{
	__vpr_stat(fmt, ap);
}

void __weak pr_stat(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	__vpr_stat(fmt, ap);
	va_end(ap);
}

void __weak *malloc_or_die(unsigned int size)
{
	void *data;

	data = malloc(size);
	if (!data)
		die("malloc");
	return data;
}
