################ ERROR CODE RETURN TO APP ##############
E_NO_ERR 				= 0
E_CMD_NOT_SUPPORT 		= 1
E_UUID_INVALID 			= 2
E_DEV_CONFIGURED 		= 3
E_UNKNOWN 				= 4
E_FILE_ERROR 			= 5
E_KEY_INVALID 			= 6
E_ENCRYPT	 			= 7

S_FAILED          = "failed"
S_OK              = "ok"
S_NOT_SUPPORT     = "notSupport"
S_FILE_NOT_FOUND  = "fileNotFound"
S_DEV_CONFIGURED  = "devConfigured"
S_FILE_ERROR  	  = "fileError"
################ INTERNAL ERROR CODE ###################
EEXCEPTION   = -1
ESYSNOERR    = 0 # Success
ESYSCSVIO    = 1 # CSV IO Error
ESYSCSVNA    = 2 # CSV Not Available Error
ETIMEOUT     = 3 # Error Timeout
ENOAVAILABLE = 4 # Not Available
EUCIIO       = 5 # UCI IO Error
ESYSENCRYPT  = 6 # Encrypt Error
ESYSDECRYPT  = 7 # Decrypt Error
ESYSINVAL    = 8 # Invalid

ESYSFILE     = 10 # File Error


# Flag define
BLE_FLG_NONE        = 0x0000
BLE_FLG_RESTART     = 0x0001
BLE_FLG_REBOOT      = 0x0002