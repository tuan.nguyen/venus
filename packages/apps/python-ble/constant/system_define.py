
import sys, os, logging, threading, Queue

BLE_FOLDER = "/etc/python-ble/"
# BLE Name
BLE_DEFAULT_NAME = 'PhD'

# Bluez for Zinnoinc name


SERVICE_NAME = "org.bluez"
AGENT_IFACE = SERVICE_NAME + '.Agent1'
ADAPTER_IFACE = SERVICE_NAME + ".Adapter1"
DEVICE_IFACE = SERVICE_NAME + ".Device1"
PLAYER_IFACE = SERVICE_NAME + '.MediaPlayer1'

LOG_LEVEL = logging.INFO
#LOG_FILE = "/var/log/syslog"
LOG_LEVEL = logging.DEBUG
LOG_FILE = "/dev/stdout"
LOG_FORMAT = "%(asctime)s <%(filename)s:%(funcName)s:%(lineno)d> %(message)s"
LOG_DATE_FORMAT = "[%Y/%m/%d][%H:%M:%S]"

RFCOMM_COMMAND_UUID                 = "3474d040-11ad-41c6-a147-90a20a387e99"
RFCOMM_STREAMI_UUID                 = "3474d041-11ad-41c6-a147-90a20a387e99"
RFCOMM_COMMAND_SERV_NAME            = "RFCommCommandServer"
RFCOMM_STREAMI_SERV_NAME            = "RFCommStreamiServer"
RFCOMM_SOCKET_SIZE                  = 1024
RFCOMM_SOCKET_COMMAND_PORT          = 1
RFCOMM_SOCKET_STREAMI_PORT          = 2

AGENT_PATH = "/blueagent5/agent"
#"DisplayOnly: Venus(BlueAgent DisplayPasskey invoked: (/org/bluez/hci0/dev_04_D1_3A_C8_60_52, 530152 entered 0) vs Android(Input Pincode)
#"DisplayYesNo: Venus(RequestConfirmation (/org/bluez/hci0/dev_04_D1_3A_C8_60_52, 096098)) vs Android(Press Okay)
#             : Venus(BlueAgent DisplayPasskey invoked: (/org/bluez/hci0/dev_78_D3_2F_52_07_26, 683725 entered 0) vs IOS(Input Pincode)
#"KeyboardDisplay: Venus(RequestConfirmation (/org/bluez/hci0/dev_04_D1_3A_C8_60_52, 701119)) vs Android(Press Okay)"
#                : Venus(RequestPasskey: 123456) vs IOS(failed No popup)s
#"KeyboardOnly: Venus (RequestPasskey: 123456) vs Android (Failed cannot auto pairing)"
#"NoInputNoOutput": Venus (Nothing) vs Android (Successful paired)
CAPABILITY = "NoInputNoOutput"

# "read"                            ---> level1
# "write"                           ---> level1
# "encrypt-read"                    ---> level2
# "encrypt-write"                   ---> level2
# "encrypt-authenticated-read"      ---> level3
# "encrypt-authenticated-write"     ---> level3
# "secure-read" (Server Only)       ---> level4
# "secure-write" (Server Only)      ---> level4
GATT_SECURE_MODE = ['read', 'write']

BLE_PATH_BASE                   = '/org/bluez/zinnoinc'
BLUEZ_SERVICE_NAME              = 'org.bluez'
LE_ADVERTISING_MANAGER_IFACE    = 'org.bluez.LEAdvertisingManager1'
DBUS_OM_IFACE                   = 'org.freedesktop.DBus.ObjectManager'
DBUS_PROP_IFACE                 = 'org.freedesktop.DBus.Properties'
LE_ADVERTISEMENT_IFACE          = 'org.bluez.LEAdvertisement1'

AGENT_MANAGER_IFACE             = 'org.bluez.AgentManager1'
GATT_MANAGER_IFACE              = 'org.bluez.GattManager1'
GATT_SERVICE_IFACE              = 'org.bluez.GattService1'
GATT_CHRC_IFACE                 = 'org.bluez.GattCharacteristic1'
GATT_DESC_IFACE                 = 'org.bluez.GattDescriptor1'

BLE_CONNECTION_NAME             = 'Zinnoinc IoT Connection Service'
BLE_COMMAPOINT_NAME             = 'Zinnoinc IoT Commapoint Service'
BLE_STREAPOINT_NAME             = 'Zinnoinc IoT Streapoint Service'

# Device status String code
BLE_DEVICE_STATUS_INSECURED     = "0000"
BLE_DEVICE_STATUS_SECURED       = "0001"
BLE_DEVICE_STATUS_DISCONNECTED  = "0002"

# CSV file name
BLE_SYSTEM_CSV = BLE_FOLDER + "system_cfg.csv"
BLE_WIFI_CSV   = BLE_FOLDER + "wifi_cfg.csv"

# CSV field name
BLE_DEVICE_STATUS = 'device_status'
# 1: Auto-generation, 0: Fixed
BLE_UUID_AUTO_GENERATION = 0
BLE_PARTNER_UUID         = 'partner_uuid'
BLE_PARTNER_NAME         = 'partner_name'
BLE_CIPHER_KEY           = 'cipher_key'
BLE_CONNECTION_SERV_UUID = 'connection_serv_uuid'
BLE_CONNECTION_SERV_UUID = 'connection_serv_uuid'
BLE_CONNECTION_DESC_UUID = 'connection_desc_uuid'
BLE_CONNECTION_CHAR_UUID = 'connection_char_uuid'

BLE_COMMAPOINT_SERV_UUID = 'commapoint_serv_uuid'
BLE_COMMAPOINT_DESC_UUID = 'commapoint_desc_uuid'
BLE_COMMAPOINT_CHAR_UUID = 'commapoint_char_uuid'

BLE_STREAPOINT_SERV_UUID = 'streapoint_serv_uuid'
BLE_STREAPOINT_DESC_UUID = 'streapoint_desc_uuid'
BLE_STREAPOINT_CHAR_UUID = 'streapoint_char_uuid'

# CSV Wifi field name
BLE_WIFI_ID              = 'wlan'
BLE_WIFI_DISABLED        = 'disabled'
BLE_WIFI_MODE            = 'mode'
BLE_WIFI_SSID            = 'ssid'
BLE_WIFI_KEY             = 'key'
BLE_WIFI_ENCRYPTION      = 'encryption'
BLE_WIFI_IPADDR          = 'ipaddr'
# Text Wifi List Name
BLE_WIFI_FILE_SCAN_RESULT       = '/tmp/wifi_scan_results'
BLE_PID_FILE       				= '/var/run/ble.pid'

# RSA Key
BLE_SYSTEM_RSA_KEY_LENGTH       = 2048
BLE_SYSTEM_RSA_PRIVATE_SEND     = BLE_FOLDER + 'rsa_private_send.pem'
BLE_SYSTEM_RSA_PUBLIC_SEND      = BLE_FOLDER + 'rsa_public_send.pem'
BLE_SYSTEM_RSA_PRIVATE_RECEIVE  = BLE_FOLDER + 'rsa_private_receive.pem'
BLE_SYSTEM_RSA_PUBLIC_RECEIVE   = BLE_FOLDER + 'rsa_public_receive.pem'
BLE_SYSTEM_DFHM_PUBLIC_SEND     = BLE_FOLDER + 'dfhm_public_send.txt'
BLE_SYSTEM_DFHM_PRIVATE_SEND    = BLE_FOLDER + 'dfhm_private_send.txt'
BLE_SYSTEM_DFHM_PUBLIC_RECEIVE  = BLE_FOLDER + 'dfhm_public_receive.txt'
BLE_SYSTEM_DFHM_PRIVATE_RECEIVE = BLE_FOLDER + 'dfhm_private_receive.txt'

# Splitter
BLE_CMD_SPLITTER                = ':'
BLE_CMD_DATA                    = "D"
BLE_RESP_END                    = "E"

# File Transfer Protocol
BLE_FILE_CHUNK_SIZE             = 176 - 16 - len(BLE_CMD_DATA) -len(BLE_CMD_SPLITTER) -1 #iOS maximum MTU is 185
BLE_FILE_END_DATA_SIZE          = 176 - 16 - len(BLE_RESP_END) -2*len(BLE_CMD_SPLITTER) -1 -4 #iOS maximum MTU is 185, 16 bytes IV, 4bytes error code, 
BLE_FILE_RAW_SIZE               = 128 #256 # Data before Encrypt in Byte
BLE_FILE_SYS_CONFIG_CSV         = "CSV"
BLE_FILE_PRIVATE_KEY_SENDER     = "PRS"
BLE_FILE_PUBLIC_KEY_SENDER      = "PBS"
BLE_FILE_PRIVATE_KEY_RECEIVER   = "PRR"
BLE_FILE_PUBLIC_KEY_RECEIVER    = "PBR"
BLE_FILE_GET_WIFI_CONFIG_CSV    = "WGT"
BLE_FILE_SET_WIFI_CONFIG_CSV    = "WST"
BLE_FILE_SEND_DFHM_PUBLICK_KEY      = "DFS"
BLE_FILE_RECEIVE_DFHM_PUBLICK_KEY   = "DFR"
BLE_FILE_READ                   = "FRD"
BLE_FILE_WRITE                  = "FWR"

# Command Transfer Protocol
BLE_CMD_CONTINUE                = "CONT"
BLE_CMD_STOP                    = "STOP"
BLE_CMD_PING                    = "PING"
BLE_CMD_FILE_END                = "ENDF"
BLE_CMD_SEND_MESSAGE            = "ENCS"
BLE_CMD_RECEIVE_MESSAGE         = "ENCR"
#-->PRTS:parter_uuid
#<--ACK:Name
#<--NAK:Name
BLE_CMD_SEND_PARTNER_UUID       = "PRTS"
BLE_CMD_RECEIVE_PARTNER_UUID    = "PRTR"

# Command Connection
BLE_CMD_CONNECTION_CONNECTED    = "CONN"
BLE_CMD_CONNECTION_DISCONNECTED = "DISC"
BLE_CMD_CONNECTION_INSECURED    = "UNCO"
BLE_CMD_CONNECTION_RESTART      = "REST"
BLE_CMD_CONNECTION_REBOOT       = "REBO"

# Respond Transfer Protocol
BLE_RESP_ACK                    = "ACK"
BLE_RESP_NOACK                  = "NAK"

BLE_WAIT_TIMEOUT                = 1000    # Timeout 10 second
BLE_WAIT_INTERVAL               = 0.010   # 10 millisecond

# INDICATOR threshold
BLE_INDICATOR_THRESHOLD         = 5 # 5 second

# PAIR SECURITY threshold
BLE_CONNECTION_SECURITY_THRESHOLD_LOOP        = 1500   # 15 second
BLE_CONNECTION_SECURITY_PAIR_TIME_LOOP        = 800    # Extra delay 8 second
BLE_CONNECTION_SECURITY_RECONNECT_TIME_LOOP   = 12000  # Timeout 120 second after disconnected incase reconnect to retry a packet
BLE_CONNECTION_SECURITY_BLACKLIST_TIME_LOOP   = 3000   # Blacklist timeout 30 second for device not pass Authentication
BLE_CONNECTION_SECURITY_THRESHOLD_INTERVAL    = 0.010 # 10 millisecond

# Zinno BLE Command
BLE_CMD_IDENTIFY                    = "IDT"
BLE_CMD_SELECT_WIFI                 = "SEL"
BLE_CMD_GET_WIFI_LIST               = "GWL"
BLE_CMD_SET_CLAIM_INFO              = "SCI"
BLE_CMD_GET_DEV_STATE               = "GST"
BLE_CMD_CHANGE_DEV_STATE            = "CST"
BLE_CMD_GET_TOKEN                   = "GTO"
BLE_CMD_GET_DEVID                   = "GDI"
BLE_CMD_SET_INFO                    = "SOI"
BLE_CMD_EXCHANGE_KEY                = "XKEY"

BLE_CLAIM_HOME_ID                   = "home"
BLE_CLAIM_USER_ID                   = "user"
BLE_CLAIM_UUID                      = "uuid"
BLE_CLAIM_TOKEN                     = "token"
BLE_CLAIM_SERVER_URL                = "sURL"
BLE_CLAIM_FIRMWARE_URL              = "fURL"
BLE_CLAIM_SSID                      = "ssid"
BLE_CLAIM_PASS                      = "pass"
BLE_CLAIM_AUTH_TYPE                 = "auth"

BLE_DEFAULT_CONFIG_LOCATION         = "/etc/config/"
BLE_DEV_STATE_LOCALTION             = "/etc/alljoyn-onboarding/"

BLE_ONBOARDING_STATE_VALUE_STORE    = "alljoyn-onboarding-state.@onboarding[0].state"
BLE_WIFI_AP_MODE_DISABLED           = "wireless.@wifi-iface[-1].disabled"

BLE_DEVICE_ID                       = "security.@local-access[0].deviceid"
BLE_LOCAL_STATE                     = "security.@local-access[0].state"
BLE_CLOUD_STATE                     = "security.@remote-access[0].state"
BLE_DEV_UUID_STORE                  = "security.@remote-access[0].userUUID"
BLE_DEV_TOKEN_STORE                 = "security.@local-access[0].claimToken"
BLE_DEV_SERVER_URL_STORE            = "security.@remote-access[0].cloud_address"
# BLE_DEV_FIRMWARE_URL_STORE          = "security.@remote-access[0].firmware_address"
BLE_DEV_HOME_ID_STORE          		= "security.@remote-access[0].homeId"
BLE_DEV_SHARE_ID_STORE          	= "security.@remote-access[0].userId"

BLE_DEV_SSID_STORE                  = "alljoyn-onboarding.@onboarding[0].ssid"
BLE_DEV_PASS_STORE                  = "alljoyn-onboarding.@onboarding[0].key"
BLE_DEV_ENCRYPTION_STORE            = "alljoyn-onboarding.@onboarding[0].encryption"

BLE_CONFIGURE_WIFI_CMD              = "/etc/init.d/alljoyn-onboarding configure"
BLE_CONNECT_WIFI_CMD                = "/etc/init.d/alljoyn-onboarding connect"

BLE_DEV_STATE_UNCONFIGURE           = "0"
BLE_DEV_STATE_CONFIGURED            = "1"
BLE_DEV_STATE_VALIDATING            = "2"
BLE_DEV_STATE_VALIDATED             = "3"
BLE_DEV_STATE_REGISTERING           = "4"
BLE_DEV_STATE_RETRY                 = "5"
BLE_DEV_STATE_REGISTERED            = "6"
BLE_DEV_STATE_REGISTER_FAILED       = "7"
BLE_DEV_STATE_CONNECTED             = "8"
