import sys, os, shutil, csv, logging, time
import ConfigParser
from pprint import pprint as Print
import subprocess, shlex
from constant.system_error import *
from constant.system_define import *
from lib.parser.csv_parser import *

sys.path.append(os.path.join(os.path.dirname(__file__), '../../'))

# uci set wireless.@wifi-device[0].disabled=0; uci commit wireless; wifi;sleep 5;udhcpc -qi wlan0
# uci set wireless.@wifi-iface[0].mode="sta"
# uci set wireless.@wifi-iface[0].ssid="verik2"
# uci set wireless.@wifi-iface[0].key="password"
# uci set wireless.@wifi-iface[0].encryption="psk2"
# uci commit wireless
# wifi

# wlan,0
# disabled,0
# mode,sta
# ssid,verik2
# key,password
# encryption,psk2
# ipaddr,192.168.1.2


class UCI_UTILS(object):
    
    def __init__(self):
        self.wlan = '0'
		#Work around driver issue id=0
        self.wlan_id = 0
        self.disabled = '0'
        self.mode = 'sta'
        self.ssid = 'VEriK2'
        self.key = 'VEriKsystems10'
        self.encryption = 'psk2'
        self.ipaddr = ''
        pass

    def create_default(self, csv, wifi_file):
        csv_datas = []
        csv_datas.append(BLE_WIFI_ID + ',0')
        csv_datas.append(BLE_WIFI_DISABLED + ',0')
        csv_datas.append(BLE_WIFI_MODE + ',sta')
        csv_datas.append(BLE_WIFI_SSID + ',VEriK2')
        csv_datas.append(BLE_WIFI_KEY + ',VEriKsystems10')
        csv_datas.append(BLE_WIFI_ENCRYPTION + ',psk2')
        csv_datas.append(BLE_WIFI_IPADDR + ',192.168.1.1')
        csv.create(wifi_file, csv_datas)
    
    def import_config(self, wifi_file):
        csvp = CSV_PARSER();
        self.wlan = csvp.param_get(wifi_file, BLE_WIFI_ID)
        self.disabled = csvp.param_get(wifi_file, BLE_WIFI_DISABLED)
        self.mode = csvp.param_get(wifi_file, BLE_WIFI_MODE)
        self.ssid = csvp.param_get(wifi_file, BLE_WIFI_SSID)
        self.key = csvp.param_get(wifi_file, BLE_WIFI_KEY)
        self.encryption = csvp.param_get(wifi_file, BLE_WIFI_ENCRYPTION)
        # Setting by UCI
        self.set()
        return ESYSNOERR
    
    def export_config(self, wifi_file):
        # Getting 
        self.get()
        
        csvp = CSV_PARSER();
        # Create Template
        self.create_default(csvp, wifi_file)
        # Fill in new value
        self.wlan = csvp.param_update(wifi_file, BLE_WIFI_ID, self.wlan)
        self.disabled = csvp.param_update(wifi_file, BLE_WIFI_DISABLED, self.disabled)
        self.mode = csvp.param_update(wifi_file, BLE_WIFI_MODE, self.mode)
        self.ssid = csvp.param_update(wifi_file, BLE_WIFI_SSID, self.ssid)
        self.key = csvp.param_update(wifi_file, BLE_WIFI_KEY, self.key)
        self.encryption = csvp.param_update(wifi_file, BLE_WIFI_ENCRYPTION, self.encryption)
        self.ipaddr = csvp.param_update(wifi_file, BLE_WIFI_IPADDR, self.ipaddr)
        
        return ESYSNOERR
    
    def read(self, cmd):
        fpipe = os.popen(cmd)
        output = fpipe.read()
        logging.info("UCI read(): %s" %(output))
        return output.rstrip('\n')
    
    def get_ipaddr(self):
        self.ipaddr = self.read('ifconfig wlan0 | grep "inet\ addr" | cut -d: -f2 | cut -d" " -f1')
        logging.info("IP address: %s" %(self.ipaddr))
        return ESYSNOERR
    
    def get(self):
        '''
            Getting Wifi Parameters
        '''
        #cmd = 'ifconfig wlan0 | grep "inet addr:"'
        #cmd = 'ifconfig wlan0'
        #self.read(cmd)
        self.get_ipaddr()
        
        uci_dev_get_pre = 'uci get wireless.@wifi-device['
        cmd = ('%s' %(uci_dev_get_pre)) + ('%s' %(self.wlan_id)) + '].disabled'
        logging.info("UCI: %s" %(cmd))
        self.disabled = self.read(cmd)
        
        uci_iface_get_pre = 'uci get wireless.@wifi-iface['
        cmd = ('%s' %(uci_iface_get_pre)) + ('%s' %(self.wlan_id)) + '].mode'
        logging.info("UCI: %s" %(cmd))
        self.mode = self.read(cmd)
        
        cmd = ('%s' %(uci_iface_get_pre)) + ('%s' %(self.wlan_id)) + '].ssid'
        logging.info("UCI: %s" %(cmd))
        self.ssid = self.read(cmd)
        
        cmd = ('%s' %(uci_iface_get_pre)) + ('%s' %(self.wlan_id)) + '].key'
        logging.info("UCI: %s" %(cmd))
        self.key = self.read(cmd)
        
        cmd = ('%s' %(uci_iface_get_pre)) + ('%s' %(self.wlan_id)) + '].encryption'
        logging.info("UCI: %s" %(cmd))
        self.encryption = self.read(cmd)
        
        return ESYSNOERR
    
    def set(self):
        '''
            Setting Wifi Parameters
        '''
        uci_dev_set_pre = 'uci set wireless.@wifi-device['
        #uci set wireless.@wifi-device[0].disabled=0
        cmd = ('%s' %(uci_dev_set_pre)) + ('%s' %(self.wlan_id)) + '].disabled=' + ('%s' %(self.disabled))
        logging.info("UCI: %s" %(cmd))
        if subprocess.call(shlex.split(cmd)) != ESYSNOERR:
            logging.error("UCI: %s failed!" %(cmd))
            return EUCIIO
        
        #uci set wireless.@wifi-iface[0].mode="sta"
        uci_iface_set_pre = 'uci set wireless.@wifi-iface['
        cmd = ('%s' %(uci_iface_set_pre)) + ('%s' %(self.wlan_id)) + '].mode=' + ('"%s"' %(self.mode))
        logging.info("UCI: %s" %(cmd))
        if subprocess.call(shlex.split(cmd)) != ESYSNOERR:
            logging.error("UCI: %s failed!" %(cmd))
            return EUCIIO
        
        #uci set wireless.@wifi-iface[0].ssid="verik2"
        cmd = ('%s' %(uci_iface_set_pre)) + ('%s' %(self.wlan_id)) + '].ssid=' + ('"%s"' %(self.ssid))
        logging.info("UCI: %s" %(cmd))
        if subprocess.call(shlex.split(cmd)) != ESYSNOERR:
            logging.error("UCI: %s failed!" %(cmd))
            return EUCIIO
        
        #uci set wireless.@wifi-iface[0].key="password"
        cmd = ('%s' %(uci_iface_set_pre)) + ('%s' %(self.wlan_id)) + '].key=' + ('"%s"' %(self.key))
        logging.info("UCI: %s" %(cmd))
        if subprocess.call(shlex.split(cmd)) != ESYSNOERR:
            logging.error("UCI: %s failed!" %(cmd))
            return EUCIIO
        
        #uci set wireless.@wifi-iface[0].encryption="psk2"
        cmd = ('%s' %(uci_iface_set_pre)) + ('%s' %(self.wlan_id)) + '].encryption=' + ('"%s"' %(self.encryption))
        logging.info("UCI: %s" %(cmd))
        if subprocess.call(shlex.split(cmd)) != ESYSNOERR:
            logging.error("UCI: %s failed!" %(cmd))
            return EUCIIO
        
        #uci commit wireless
        cmd = 'uci commit wireless'
        logging.info("UCI: %s" %(cmd))
        if subprocess.call(shlex.split(cmd)) != ESYSNOERR:
            logging.error("UCI: %s failed!" %(cmd))
            return EUCIIO
        
        # wifi
        cmd = 'wifi'
        logging.info("UCI: %s" %(cmd))
        if subprocess.call(shlex.split(cmd)) != ESYSNOERR:
            logging.error("UCI: %s failed!" %(cmd))
            return EUCIIO
        
        time.sleep(5)
        
        #udhcpc -qni wlan0
        cmd = 'udhcpc -qni wlan' + ('%s' %(self.wlan))
        #cmd = 'udhcpc -q -i wlan0'
        logging.info("UCI: %s" %(cmd))
        if subprocess.call(shlex.split(cmd)) != ESYSNOERR:
            logging.error("UCI: %s failed!" %(cmd))
            return EUCIIO
        
        return ESYSNOERR
    
    def uciGet(self, path, config):
        value = ''

        if not config:
            return value

        if not path:
            return value

        uci_cmd = "uci -c %s get %s" %(path,config)
        value=self.read(uci_cmd)

        return value

    def uciSet(self, path, config, mode=1):
        if not config:
            return EUCIIO

        if not path:
            return EUCIIO

        uci_cmd = "uci -c %s set %s" %(path,config)
        if subprocess.call(shlex.split(uci_cmd)) != ESYSNOERR:
            logging.error("UCI: %s failed!" %(uci_cmd))
            return EUCIIO

        if mode == 1:
            fileName=config.split(".")[0]
            uci_cmd = "uci -c %s commit %s" %(path,fileName)
            if subprocess.call(shlex.split(uci_cmd)) != ESYSNOERR:
                logging.error("UCI: %s failed!" %(uci_cmd))
                return EUCIIO

        return ESYSNOERR
    
    def setSecurityInfo(self, uuid, token, sURL, homeId, shareId):
        self.uciSet(BLE_DEFAULT_CONFIG_LOCATION, BLE_DEV_UUID_STORE+"='%s'" %uuid, 0)
        self.uciSet(BLE_DEFAULT_CONFIG_LOCATION, BLE_DEV_TOKEN_STORE+"='%s'" %token, 0)
        self.uciSet(BLE_DEFAULT_CONFIG_LOCATION, BLE_DEV_SERVER_URL_STORE+"='%s'" %sURL, 0)
        # self.uciSet(BLE_DEFAULT_CONFIG_LOCATION, BLE_DEV_FIRMWARE_URL_STORE+"='%s'" %fURL, 0)
        self.uciSet(BLE_DEFAULT_CONFIG_LOCATION, BLE_DEV_HOME_ID_STORE+"='%s'" %homeId, 0)
        if shareId:
            self.uciSet(BLE_DEFAULT_CONFIG_LOCATION, BLE_DEV_SHARE_ID_STORE+"='%s'" %shareId, 0)
        self.uciSet(BLE_DEFAULT_CONFIG_LOCATION, BLE_LOCAL_STATE+"='configured'", 0)
        self.uciSet(BLE_DEFAULT_CONFIG_LOCATION, BLE_CLOUD_STATE+"='configured'", 1)
        return ESYSNOERR

    def getDevState(self):
        cloudState = self.uciGet(BLE_DEFAULT_CONFIG_LOCATION, BLE_CLOUD_STATE)
        if(cloudState == "registered"):
            return BLE_DEV_STATE_REGISTERED
        elif(cloudState == "register_failed"):
            return BLE_DEV_STATE_REGISTER_FAILED
        elif((cloudState == "connected") or (cloudState == "claimed")):
            return BLE_DEV_STATE_CONNECTED
        else:
            onboardingState = self.uciGet(BLE_DEV_STATE_LOCALTION, BLE_ONBOARDING_STATE_VALUE_STORE)
            if(cloudState == "configured") and (onboardingState == BLE_DEV_STATE_VALIDATED):
                return BLE_DEV_STATE_REGISTERING
            return onboardingState