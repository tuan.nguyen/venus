from __future__ import absolute_import, print_function, unicode_literals
import subprocess, shlex
from threading import Thread
from optparse import OptionParser, make_option
import sys
import time

from constant.system_error import *
from constant.system_define import *
from lib.bluetooth.pairing import *

class SECURE_SERVICE(object):
    
    def __init__(self, pair, ):
        self.safety_flag = False
        self.threshold_loop = BLE_CONNECTION_SECURITY_THRESHOLD_LOOP
        self.threshold_interval = BLE_CONNECTION_SECURITY_THRESHOLD_INTERVAL
        
        # Security of Operation
        def security_service_thread(waitsec=BLE_CONNECTION_SECURITY_THRESHOLD_INTERVAL):
            logging.info("Security Thread running...")
            
            # Loop to check Events
            while True:
                # Get flag
                event = pair.getConnectEvent()
                # No pairing income
                if (event == False):
                    # Idle sleep
                    time.sleep(waitsec)
                    continue
                
                # Decrease
                self.threshold_loop -= 1
                if self.threshold_loop == 0:
                    # Timeout
                    self.set_safety(False)
                    # Clear Pairing event
                    pair.clearConnectEvent()
                    
    
        # create security server:
        security_thread = Thread(target= lambda: security_service_thread())
        security_thread.daemon = True
        security_thread.start()
        logging.info("Create Security Thread done!")
    
    def init_params(self):
        self.safety_flag = False
        self.threshold_loop = BLE_CONNECTION_SECURITY_THRESHOLD_LOOP
        self.threshold_interval = BLE_CONNECTION_SECURITY_THRESHOLD_INTERVAL
        
    def set_safety(self, state):
        self.safety_flag = state
    
    def get_safety(self):
        return self.safety_flag
    
    def set_threshold(self, loop):
        self.threshold_loop = loop
    
    def get_threshold(self):
        return self.threshold_loop
    
    