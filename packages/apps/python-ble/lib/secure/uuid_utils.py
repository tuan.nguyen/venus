import sys, os, shutil, csv, logging
import ConfigParser
from pprint import pprint as Print
import uuid

sys.path.append(os.path.join(os.path.dirname(__file__), '../../'))


class UUID_UTILS(object):

    def __init__(self):
        pass

    def uuid128_gen(self):
        '''
            Auto generate UUID128
        '''
        return str(uuid.uuid4())