#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from base64 import b64encode, b64decode
from Crypto import Random
from Crypto.Cipher import AES
from lib.utils.Padding import pad, unpad
import StringIO
from constant.system_error import *
from constant.system_define import *
# from binascii import hexlify
# import logging

class AES_UTILS:

    def __init__(self, key):
        # self.key = b64decode(key)
        self.key = key
        self.block_size = 16
        self.padding_style = 'pkcs7'
    
    def setkey(self, key):
        # self.key = b64decode(key)
        self.key = key

    def encrypt(self, m):
        try:
            iv = Random.new().read(self.block_size)
            padded = pad(m, AES.block_size, style=self.padding_style)
            cipher = AES.new(self.key, AES.MODE_CBC, iv)
            return True, (iv + cipher.encrypt(padded))
            # return True, b64encode(iv + cipher.encrypt(padded))
        except Exception as e:
            logging.error("encrypt() Error: %s" %(e))
            return False, e
        
    def decrypt(self, c):
        try:
            # c = b64decode(c)
            iv = c[:self.block_size]
            cipher = AES.new(self.key, AES.MODE_CBC, iv)
            #padded = cipher.decrypt(c[self.block_size:]).decode("utf8")
            padded = cipher.decrypt(c[self.block_size:])
            # logging.info("padded %s" %hexlify(padded))
            # logging.info("padded len = %s" %len(hexlify(padded)))
            return True, unpad(padded, AES.block_size, style=self.padding_style)
        except Exception as e:
            logging.error("decrypt() Error: %s" %(e))
            return False, e