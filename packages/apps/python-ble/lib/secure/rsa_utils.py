import sys, os, shutil, csv, logging
from Crypto import Random
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from Crypto.Hash import SHA512
import base64
import StringIO
from pprint import pprint as Print

from constant.system_error import *
from constant.system_define import *

sys.path.append(os.path.join(os.path.dirname(__file__), '../../'))


class RSA_UTILS(object):
    
    def __init__(self):
        self.encryptCipher = None
        self.decryptCipher = None
        pass

    def pem_gen(self, bits, private_name, public_name):
        # Random number generator function.
        try:
            rng = Random.new().read
            key = RSA.generate(bits, rng)
            # Write Private key file
            f = open( private_name, 'wb')
            f.write(key.exportKey("PEM"))
            f.close()
            # Write Public key file
            f = open(public_name, 'wb')
            f.write(key.publickey().exportKey("PEM"))
            f.close()
            return True
        except Exception as e:
            logging.error("pem_gen() Error: %s" %(e))
            return False
        
    def keys_gen(self):
        # Generate Sender private/public key.
        #self.pem_gen(BLE_SYSTEM_RSA_KEY_LENGTH, BLE_SYSTEM_RSA_PRIVATE_SEND, BLE_SYSTEM_RSA_PUBLIC_SEND)
        # Generate Receiver private/public key.
        return self.pem_gen(BLE_SYSTEM_RSA_KEY_LENGTH, BLE_SYSTEM_RSA_PRIVATE_RECEIVE, BLE_SYSTEM_RSA_PUBLIC_RECEIVE)
    
    def import_keys(self, private_key_file, public_key_file):
        try:
            # For encrypt
            keyfile = open(public_key_file, "rb").read()
            keyfile = keyfile.replace('-----BEGIN PUBLIC KEY-----', '')
            keyfile = keyfile.replace('-----END PUBLIC KEY-----', '');
            public_key = RSA.importKey(base64.b64decode(keyfile))
            self.encryptCipher = PKCS1_OAEP.new(public_key, hashAlgo=SHA512)
            # For decrypt
            keyfile = open(private_key_file, "rb").read()
            keyfile = keyfile.replace('-----BEGIN RSA PRIVATE KEY-----', '')
            keyfile = keyfile.replace('-----END RSA PRIVATE KEY-----', '');
            private_key = RSA.importKey(base64.b64decode(keyfile))
            self.decryptCipher = PKCS1_OAEP.new(private_key, hashAlgo=SHA512)
            return True
        except Exception as e:
            logging.error("import_keys() Error: %s" %(e))
            return False
        
    def encrypt(self, message):
        try:
            encrypted_data = self.encryptCipher.encrypt(message)
            return True, base64.encodestring(encrypted_data)
        except Exception as e:
            logging.error("encrypt() Error: %s" %(e))
            return False, e
        
    def decrypt(self, ciphertext):
        try:
            base_code = base64.b64decode(ciphertext)
        except Exception as e:
            logging.error("base64.b64decode Error: %s" %(e))
            return False, e
        try:
            return True, self.decryptCipher.decrypt(base_code)
        except Exception as e:
            logging.error("decryptCipher Error: %s" %(e))
            return False, e

    def test(self, private_key, public_key):
        message = 'Welcom RAS 2048 Cipher en-decrypt engine'
        self.import_keys(private_key, public_key)
        logging.info("Test send encrypt:")
        retval, encrypted = self.encrypt(message)
        if retval == True:
            logging.info(encrypted)
        else:
            logging.error(encrypted)
         
        logging.info("Test send decrypt:")
        retval, decrypted = self.decrypt(encrypted)
        if retval == True:
            logging.info("Decrypted Message: %s" %(decrypted))
        else:
            logging.error("Decrypted Error: %s" %(decrypted))
    
        
