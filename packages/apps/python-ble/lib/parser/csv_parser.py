import sys, os, shutil, csv, logging
import ConfigParser
from pprint import pprint as Print
from constant.system_error import *

sys.path.append(os.path.join(os.path.dirname(__file__), '../../'))

'''
    System config
    @variable: name
    @value: value
'''
COL1_NAME = 'variable'
COL2_NAME = 'value'
headers = [COL1_NAME, COL2_NAME]

class CSV_PARSER(object):

    def __init__(self):
        pass

    
    def create(self, file_name, csv_lines):
        '''
            @file_name: csv file name
            @csv_lines: array data line
        '''
        try:
            wfile = open(file_name, "wb")
        except Exception, e:
            logging.error('Open [%s] error' % (file_name))
            logging.error(e)
            return EEXCEPTION
        
        # Write csv
        csvWriter = csv.DictWriter(wfile, fieldnames=headers)
        csvWriter.writeheader()
        
        try:
            # Loop through the lines in the file and get each coordinate
            for row in csv_lines:
                if len(row) == 0:
                    break
                name, value = row.split(',')
                csvWriter.writerow({COL1_NAME:name, COL2_NAME:value})
                    
        except Exception, e:
            logging.error('File %s failed: %s' % (file_name, e))
            return EEXCEPTION

        # Close files
        wfile.close()
        # Successful
        return ESYSNOERR
    
    
    def parse_param(self, file_name):
        '''
            @file_name: csv file name
        '''
        try:
            file = open(file_name, "r")
        except Exception, e:
            logging.error('Open [%s] error' % file_name)
            logging.error(e)
            return None

        csv_param_map = {}
        csvReader = csv.reader(file)

        try:
            header = csvReader.next()
            nameIndex = header.index(COL1_NAME)
            valueIndex = header.index(COL2_NAME)
            
            # Loop through the lines in the file and get each coordinate
            for row in csvReader:
                if len(row) == 0:
                    logging.info('[Line%d, %s] - Null' % (csvReader.line_num, file_name))
                    continue

                name = row[nameIndex]
                value = row[valueIndex]
                
                csv_param_map.update({name:{COL2_NAME:value}})
        except Exception, e:
            logging.error('File %s, line %d: %s' % (file_name, csvReader.line_num, e))
            csv_param_map = None

        file.close()
        return csv_param_map

    
    def param_update(self, file_name, field_name, field_value):
        '''
            @file_name: csv file name
            @field_name: field name
            @field_value: field value
        '''
        try:
            rfile = open(file_name, "rb")
        except Exception, e:
            logging.error('Open [%s] error' % file_name)
            logging.error(e)
            return EEXCEPTION

        try:
            wfile = open('/tmp/tmp-' + file_name.split("/")[-1], "wb")
        except Exception, e:
            logging.error('Open [%s] error' % ('tmp-' + file_name))
            logging.error(e)
            return EEXCEPTION
        
        # Read csv
        csvReader = csv.reader(rfile)
        
        # Write csv
        csvWriter = csv.DictWriter(wfile, fieldnames=headers)
        csvWriter.writeheader()
        
        try:
            header = csvReader.next()
            nameIndex = header.index(COL1_NAME)
            valueIndex = header.index(COL2_NAME)
            
            # Loop through the lines in the file and get each coordinate
            for row in csvReader:
                if len(row) == 0:
                    logging.info('[Line%d, %s] - Null' % (csvReader.line_num, file_name))
                    continue

                name = row[nameIndex]
                value = row[valueIndex]
                if name == field_name:
                    value = field_value
                    logging.info("Modified: %s, %s --> %s" %(name,row[valueIndex],value))
                    csvWriter.writerow({COL1_NAME:name, COL2_NAME:value})
                else:
                    csvWriter.writerow({COL1_NAME:name, COL2_NAME:value})
                    
        except Exception, e:
            logging.error('File %s, line %d: %s' % (file_name, csvReader.line_num, e))
            return EEXCEPTION

        # Close files
        rfile.close()
        wfile.close()
        
        # Rename to original name
        shutil.move('/tmp/tmp-' + file_name.split("/")[-1], file_name)
        # Successful
        return ESYSNOERR
    
    
    def param_get(self, file_name, field_name):
        '''
            @file_name: csv file name
            @field_name: field name
        '''
        try:
            rfile = open(file_name, "rb")
        except Exception, e:
            logging.error('Open [%s] error' % file_name)
            logging.error(e)
            return None

        # Read csv
        csvReader = csv.reader(rfile)
        
        try:
            header = csvReader.next()
            nameIndex = header.index(COL1_NAME)
            valueIndex = header.index(COL2_NAME)
            
            # Loop through the lines in the file and get each coordinate
            for row in csvReader:
                if len(row) == 0:
                    logging.info('[Line%d, %s] - Null' % (csvReader.line_num, file_name))
                    continue

                name = row[nameIndex]
                value = row[valueIndex]
                if name == field_name:
                    logging.info("Found: %s, %s" %(name,value))
                    break
                else:
                    value = None
                    
        except Exception, e:
            logging.error('File %s, line %d: %s' % (file_name, csvReader.line_num, e))
            rfile.close()
            value = None

        # Close files
        rfile.close()
        
        # Successful
        return value