import sys, os, csv, logging
import ConfigParser
from pprint import pprint as Print
from constant.system_error import EEXCEPTION, ESYSNOERR

sys.path.append(os.path.join(os.path.dirname(__file__), '../../'))
from lib.common import *

class INI_PARSER(object):
    """INI_CONFIG_PARSER: parse .ini file"""
    def __init__(self, filename, logger_name=None):
        self.filename = filename
        
        # Init ConfigParser
        self.Config = ConfigParser.ConfigParser()
        try:
            self.Config.read(filename)
        except Exception, e:
            logging.error('Read [%s] file: error!' % filename)
            logging.error(e)
            return None

    # get @option value in @section
    # @type: [bool, str, float, int], default (str)
    # @default: default value
    def get(self, section, option, default=None, type=None):
        if not self.Config.has_section(section) or not self.Config.has_option(section, option):
            return default
        else:
            try:
                if type is int:
                    return self.Config.getint(section, option)
                elif type is bool:
                    return self.Config.getboolean(section, option)
                elif type is float:
                    return self.Config.getfloat(section, option)
                elif type is list:
                    return self.Config.get(section, option).split(',')
                else:
                    return self.Config.get(section, option)
            except Exception, e:
                return default

    def set(self, section, option, value):
        if not self.Config.has_section(section):
            self.Config.add_section(section)
        
        self.Config.set(section, option, str(value))
        # save to a file
        try:
            with open(self.filename, 'w') as configfile:
                self.Config.write(configfile)
        except Exception, e:
            logging.error('Save config: [{section}.{option}.{val}] to {filename} error'.format(section=section,
                option=option,val=value,filename=self.filename))
            logging.error(e)
            return EEXCEPTION
        
        return ESYSNOERR

    