
import time
import sys
import dbus
import dbus.service,exceptions
import dbus.mainloop.glib
import gobject
import logging

from lib.bluetooth import adapters
from optparse import OptionParser

from constant.system_error import *
from constant.system_define import *
from lib.indicator.led_utils import *
from threading import Timer

relevant_ifaces = [ ADAPTER_IFACE, DEVICE_IFACE ]

class BlueAgent(dbus.service.Object):
    pin_code = None
    pass_key = None

    def __init__(self, ble, pin_code, pass_key):
        dbus.service.Object.__init__(self, dbus.SystemBus(), AGENT_PATH)
        self.ble = ble
        self.bus = ble.dbus
        self.mainloop = ble.mainloop
        self.pin_code = pin_code
        self.pass_key = pass_key
        self.indicator = ble.indicator
        self.manager = None #Agent
        # True: connected, False: disconnected
        self.connect_event = False
        self.disconnect_event = True
        self.connect_device = None
        self.pairing_event = False
        # Reconnect name/timeout
        self.reconnect_device = None
        self.reconnect_timeout = 0
        # Blacklist for device not pass Authentication
        self.blacklist_device = None
        self.blacklist_timeout = 0
        # Thread flag
        self.thread_flag = True
        self.thread = None
        
        #logging.basicConfig(filename=LOG_FILE, format=LOG_FORMAT, level=LOG_LEVEL)
        #logging.info("Starting BlueAgent with PIN [{}]".format(self.pin_code))
        #logging.info("Starting BlueAgent with PASSKEY [{}]".format(self.pass_key))
        self.safety_flag = False
        self.threshold_loop = BLE_CONNECTION_SECURITY_THRESHOLD_LOOP
        self.threshold_interval = BLE_CONNECTION_SECURITY_THRESHOLD_INTERVAL
    
        
        # Security of Operation
        def security_connection_service_thread(waitsec=BLE_CONNECTION_SECURITY_THRESHOLD_INTERVAL):
            try:
                logging.info("Security Connection Thread running...")
                
                # Loop to check Events
                while self.thread_flag:
                    # Get flag
                    # No connection income
                    if (self.connect_event == False) or \
                        ((self.connect_event == True) and ((self.safety_flag == True) or (self.pairing_event == True))):
                        
                        # Idle sleep
                        time.sleep(waitsec)
                        
                        # Disconnect process
                        if (self.connect_event == False):
                            #print("Disconnected!")
                            # Check to clear reconnect device name
                            if self.reconnect_timeout > 0:
                                self.reconnect_timeout -= 1
                                #print("Decrease reconnect_timeout %d" %self.reconnect_timeout)
                                if self.reconnect_timeout <= 0:
                                    logging.info("Clear Reconnect name: '%s': '%s'" %(self.reconnect_device, self.get_name(self.reconnect_device)))
                                    self.reconnect_device = None
                            # Check to clear blacklist device name
                            if self.blacklist_timeout > 0:
                                self.blacklist_timeout -= 1
                                #print("Decrease blacklist_timeout %d" %self.blacklist_timeout)
                                if self.blacklist_timeout <= 0:
                                    logging.info("Clear Blacklist name: '%s': '%s'" %(self.blacklist_device, self.get_name(self.blacklist_device)))
                                    self.blacklist_device = None
                        # Goto loop
                        continue
                    
                    # Idle sleep
                    time.sleep(waitsec)
                            
                    # Decrease
                    self.threshold_loop -= 1
                    if self.threshold_loop == 0:
                        logging.info("Connection check Security timeout!\n Device '%s': '%s' (%s,%s,%s,%s)" \
                                     %(self.connect_device, self.get_name(self.connect_device), self.connect_event, self.disconnect_event, self.safety_flag, self.pairing_event))
                        # Save blacklist device
                        self.blacklist_device = self.connect_device
                        logging.info("Save Blacklist name: '%s': '%s'" %(self.blacklist_device, self.get_name(self.blacklist_device)))
                        self.blacklist_timeout = BLE_CONNECTION_SECURITY_BLACKLIST_TIME_LOOP
                        # Timeout
                        self.safety_flag = False
                        # Disconnect
                        self.disconnectDevice(self.connect_device)
            except Exception as e:
                logging.error("Exception on Security Connection Thread: %s" %(e))
                self.mainloop.quit()
            # Disconnect device first
            if self.connect_device:
                self.disconnectDevice(self.connect_device)
            logging.info("--- Host Power off ---")
            ble.host_util.host_setpower('off')
            logging.info("Security Connection Thread Exiting...")
                
                
        # create security server:
        pairing_thread = Thread(target= lambda: security_connection_service_thread(BLE_CONNECTION_SECURITY_THRESHOLD_INTERVAL))
        self.thread = pairing_thread
        pairing_thread.daemon = True
        pairing_thread.start()
        logging.info("Create Security Connection Thread done!")
    
    def initNewConnectParams(self, device):
        if (self.disconnect_event == True):
            self.connect_event = True
            self.disconnect_event = False
        self.threshold_loop += BLE_CONNECTION_SECURITY_THRESHOLD_LOOP
        self.threshold_interval = BLE_CONNECTION_SECURITY_THRESHOLD_INTERVAL
        # Check reconnect device
        #logging.info("Reconnect name: '%s' vs '%s'" %(self.reconnect_device, device))
        if (self.reconnect_device == device):
            logging.info("Reconnect to device '%s': '%s'" %(device, self.get_name(device)))
            self.safety_flag = True
        # Check blacklist device
        #logging.info("Blacklist name: '%s' vs '%s'" %(self.blacklist_device, device))
        if (self.blacklist_device == device):
            logging.info("Blacklist detected '%s': '%s'" %(device, self.get_name(device)))
            # time.sleep(3) #maybe device could be send PRST to active
            self.blacklist_timeout = BLE_CONNECTION_SECURITY_BLACKLIST_TIME_LOOP
            self.connect_event = False
            self.safety_flag = False
            # Disconnect
            self.disconnectDevice(device)
                        
    def extraDelayPairConnection(self):
        self.threshold_loop += BLE_CONNECTION_SECURITY_PAIR_TIME_LOOP
        
    def clearDisconnectParams(self, device):
        # Save reconnect device/timeout
        if self.connect_event == True:
            if self.safety_flag == True:
                self.reconnect_device = device
                self.reconnect_timeout = BLE_CONNECTION_SECURITY_RECONNECT_TIME_LOOP
                logging.info("Save Reconnect name: '%s': '%s'" %(self.reconnect_device, self.get_name(self.reconnect_device)))
            else:
                self.reconnect_device = None
                self.reconnect_timeout = 0
        # Clear parameters
        self.connect_event = False
        self.disconnect_event = True
        self.safety_flag = False
        self.pairing_event = False
        
    def setConnectSafety(self):
        logging.info("Set Safety Connect! (%u)" %(self.threshold_loop))
        self.safety_flag = True
    
    def getConnectSafetyStatus(self):
        return self.safety_flag
    
    def setConnectThreshold(self, loop):
        self.threshold_loop = loop
    
    def getConnectThreshold(self):
        return self.threshold_loop
    
    def getConnectEvent(self):
        return self.connect_event
    
    def clearConnectEvent(self):
        self.connect_event = False
    
    def setThreadFlag(self, state):
        self.thread_flag = state

    def getThread(self):
        return self.thread
    def get_name(self, path):
        name = ""
        try:
            prop = dbus.Interface(self.bus.get_object("org.bluez", path),
                                "org.freedesktop.DBus.Properties")
            name = prop.Get("org.bluez.Device1", "Name")
            logging.info("%s Name: %s" %(path, name))
        except Exception as e:
            logging.error("Exception on get_name(): %s" %(e))
        return name
    
    # Return name of connecting device
    def getConnectName(self):
        return self.get_name(self.connect_device)
    
    @dbus.service.method(AGENT_IFACE, in_signature="os", out_signature="")
    def DisplayPinCode(self, device, pincode):
        # Indicator (LED) active
        # self.indicator.set_state(True)
        logging.info("BlueAgent DisplayPinCode invoked: (%s, %s)" %(device, pincode))

    @dbus.service.method(AGENT_IFACE, in_signature="ouq", out_signature="")
    def DisplayPasskey(self, device, passkey, entered):
        # Indicator (LED) active
        # self.indicator.set_state(True)
        logging.info("BlueAgent DisplayPasskey invoked: (%s, %06u entered %u)" %(device, passkey, entered))

    @dbus.service.method(AGENT_IFACE, in_signature="o", out_signature="s")
    def RequestPinCode(self, device):
        # Indicator (LED) active
        # self.indicator.set_state(True)
        logging.info("BlueAgent is pairing with device [{}]".format(device))
        self.trustDevice(device)
        return self.pin_code

    @dbus.service.method(AGENT_IFACE, in_signature="ou", out_signature="")
    def RequestConfirmation(self, device, passkey):
        """Always confirm"""
        # Indicator (LED) active
        # self.indicator.set_state(True)
        logging.info("RequestConfirmation (%s, %06d)" % (device, passkey))
        #logging.info("BlueAgent is pairing with device [{}]".format(device))
        self.pairing_event = True
        self.trustDevice(device)
        #self.blockDevice(device)
        #time.sleep(10000);
        return

    @dbus.service.method(AGENT_IFACE, in_signature="os", out_signature="")
    def AuthorizeService(self, device, uuid):
        """Always authorize"""
        # Indicator (LED) active
        # self.indicator.set_state(True)
        logging.info("BlueAgent AuthorizeService method invoked: %s - %s" %(device, uuid))
        return

    @dbus.service.method(AGENT_IFACE, in_signature="o", out_signature="u")
    def RequestPasskey(self, device):
        # Indicator (LED) active
        # self.indicator.set_state(True)
        logging.info("RequestPasskey: %s" %(self.pass_key))
        #self.trustDevice(device)
        return self.pass_key

    @dbus.service.method(AGENT_IFACE, in_signature="o", out_signature="")
    def RequestAuthorization(self, device):
        """Always authorize"""
        # Indicator (LED) active
        # self.indicator.set_state(True)
        logging.info("RequestAuthorization (%s)" % (device))
        #logging.info("BlueAgent is authorizing device [{}]".format(self.device))
        return

    @dbus.service.method(AGENT_IFACE, in_signature="", out_signature="")
    def Cancel(self):
        # Indicator (LED) active
        # self.indicator.set_state(True)
        logging.info("BlueAgent pairing request canceled from device [{}]".format(self.device))

    def trustDevice(self, path):
        logging.info("BlueAgent Trusted")
        # Indicator (LED) active
        # self.indicator.set_state(True)
        bus = dbus.SystemBus()
        device_properties = dbus.Interface(bus.get_object(SERVICE_NAME, path), DBUS_PROP_IFACE)
        device_properties.Set(DEVICE_IFACE, "Trusted", True)

    def blockDevice(self, path, state):
        # Indicator (LED) active
        # self.indicator.set_state(True)
        bus = dbus.SystemBus()
        device_properties = dbus.Interface(bus.get_object(SERVICE_NAME, path), DBUS_PROP_IFACE)
        if state == True:
            logging.info("BlueAgent Blocked")
            device_properties.Set(DEVICE_IFACE, "Blocked", True)
        else:
            logging.info("BlueAgent UnBlocked")
            device_properties.Set(DEVICE_IFACE, "Blocked", False)
    
    def disconnectDevice(self, path):
        logging.info("BlueAgent Disconnected: %s" %(path))
        # Indicator (LED) active
        # self.indicator.set_state(True)
        bus = dbus.SystemBus()
        device_methods = dbus.Interface(bus.get_object(SERVICE_NAME, path), DEVICE_IFACE)
        try:
            device_methods.Disconnect()
            return True
        except Exception as e:
            logging.error("disconnectDevice() Error: %s" %(e))
            return False
        
    def properties_changed(self, interface, changed, invalidated, path):
        logging.info("properties_changed() ...")
        # Indicator (LED) active
        # self.indicator.set_state(True)
        # Searching
        iface = interface[interface.rfind(".") + 1:]
        for name, value in changed.iteritems():
            logging.info("{%s.PropertyChanged} [%s] '%s' = '%s'" % (iface, path, name, value))
            if (name == 'Paired'):
                self.connect_device = path
                if (value == 1):
                    logging.info("Add Extra Delay for Pairing: %s" %(path))
                    self.extraDelayPairConnection()
                    break
            elif (name == 'Connected') or (name == 'ServicesResolved'):
                self.connect_device = path
                if (value == 1):
                    logging.info("NewConnection: '%s': '%s'" %(path, self.get_name(path)))
                    self.initNewConnectParams(path)
                else:
                    logging.info("DisConnetion: '%s': '%s'" %(path, self.get_name(path)))
                    self.clearDisconnectParams(path)
                
                # Clear pairing event
                if (self.pairing_event == True):
                    self.pairing_event = False
            # Increase timeout for any update
            self.setConnectThreshold(BLE_CONNECTION_SECURITY_THRESHOLD_LOOP)
            
    def interfaces_added(self, path, interfaces):
        # logging.info("interfaces_added() ...")
        # Indicator (LED) active
        # self.indicator.set_state(True)
        # Searching
        for iface, props in interfaces.iteritems():
            if not(iface in relevant_ifaces):
                continue
            logging.info("{Added %s} [%s]" % (iface, path))
            for name, value in props.iteritems():
                logging.info("      '%s' = '%s'" % (name, value))
                if (name == 'Connected'):
                    self.connect_device = path
                    if (value == 1):
                        logging.info("NewConnection: %s" %(path))
                        self.initNewConnectParams(path)
                    else:
                        logging.info("DisConnetion: %s" %(path))
                        self.clearDisconnectParams(path)
    
    def interfaces_removed(self, path, interfaces):
        logging.info("interfaces_removed() ...")
        # Indicator (LED) active
        # self.indicator.set_state(True)
        # Searching
        for iface in interfaces:
            if not(iface in relevant_ifaces):
                continue
            logging.info("{Removed %s} [%s]" % (iface, path))
        
    def registerAsDefault(self, bus):
        manager = dbus.Interface(bus.get_object(BLUEZ_SERVICE_NAME, "/org/bluez"), AGENT_MANAGER_IFACE)
        manager.RegisterAgent(AGENT_PATH, CAPABILITY)
        manager.RequestDefaultAgent(AGENT_PATH)
        
        bus.add_signal_receiver(self.interfaces_removed,
                                    dbus_interface=DBUS_OM_IFACE,
                                    signal_name="InterfacesRemoved")
        bus.add_signal_receiver(self.interfaces_added,
                                    dbus_interface=DBUS_OM_IFACE,
                                    signal_name='InterfacesAdded')
        bus.add_signal_receiver(self.properties_changed,
                                    dbus_interface=dbus.PROPERTIES_IFACE,
                                    signal_name='PropertiesChanged',
                                    arg0=DEVICE_IFACE,
                                    path_keyword='path')
        return manager

    def startPairing(self):
        #bus = dbus.SystemBus()
        #adapter_path = findAdapter().object_path
        #adapter = dbus.Interface(bus.get_object(SERVICE_NAME, adapter_path), "org.freedesktop.DBus.Properties")
#        adapter.Set(ADAPTER_IFACE, "Discoverable", True)
        logging.info("BlueAgent is waiting to pair with device")
    
def agentPairingServiceRegister(ble):
    try:
        logging.info('Registering Pairing Service...')
        pin_code = "1234"
        pass_key = "123456"
        agent = BlueAgent(ble, pin_code, pass_key)
        agent.manager = agent.registerAsDefault(ble.dbus)
        agent.startPairing()
        return agent
    
    except Exception as e:
        raise("agentPairingServiceRegister(): exception: %s" %(e))
    
def agentUnregister(ble):
    try:
        logging.info('Unregistering Pairing Service...')
        if ble.pair.manager != None:
            ble.pair.manager.UnregisterAgent(AGENT_PATH)
    except Exception as e:
        raise("agentPairingServiceUnregister(): exception: %s" %(e))

def disconnectCurrentDev(ble):
    try:
        logging.info('Disconnect dev...')
        if ble.pair.connect_device != None:
            ble.pair.disconnectDevice(ble.pair.connect_device)
    except Exception as e:
        raise("agentPairingServiceUnregister(): exception: %s" %(e))