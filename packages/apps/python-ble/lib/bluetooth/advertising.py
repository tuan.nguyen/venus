from __future__ import print_function

import dbus
import dbus.exceptions
import dbus.mainloop.glib
import dbus.service
import functools
import logging
import subprocess, shlex
from lib.bluetooth import adapters
from lib.bluetooth import exceptions
from constant.system_error import *
from constant.system_define import *
from lib.parser.csv_parser import *
from lib.secure.uuid_utils import *
from lib.network.uci_utils import *

class Advertisement(dbus.service.Object):
    PATH_BASE = BLE_PATH_BASE + '/advertisement'

    def __init__(self, bus, index, advertising_type, addr, manager):
        self.path = self.PATH_BASE + str(index)
        self.bus = bus
        self.ad_type = advertising_type
        self.service_uuids = None
        self.manufacturer_data = None
        self.solicit_uuids = None
        self.service_data = None
        self.local_name = None
        self.include_tx_power = None
        self.manager = None
        dbus.service.Object.__init__(self, bus, self.path)

    def get_properties(self):
        properties = dict()
        properties['Type'] = self.ad_type
        if self.service_uuids is not None:
            properties['ServiceUUIDs'] = dbus.Array(self.service_uuids,
                                                    signature='s')
        if self.solicit_uuids is not None:
            properties['SolicitUUIDs'] = dbus.Array(self.solicit_uuids,
                                                    signature='s')
        if self.manufacturer_data is not None:
            properties['ManufacturerData'] = dbus.Dictionary(
                self.manufacturer_data, signature='qv')
        if self.service_data is not None:
            properties['ServiceData'] = dbus.Dictionary(self.service_data,
                                                        signature='sv')
        if self.local_name is not None:
            properties['LocalName'] = dbus.String(self.local_name)
        if self.include_tx_power is not None:
            properties['IncludeTxPower'] = dbus.Boolean(self.include_tx_power)
        return {LE_ADVERTISEMENT_IFACE: properties}

    def get_manager(self):
        return self.manager
    def get_path(self):
        return dbus.ObjectPath(self.path)

    def add_service_uuid(self, uuid):
        if not self.service_uuids:
            self.service_uuids = []
        self.service_uuids.append(uuid)

    def add_solicit_uuid(self, uuid):
        if not self.solicit_uuids:
            self.solicit_uuids = []
        self.solicit_uuids.append(uuid)

    def add_manufacturer_data(self, manuf_code, data):
        if not self.manufacturer_data:
            self.manufacturer_data = dbus.Dictionary({}, signature='qv')
        self.manufacturer_data[manuf_code] = dbus.Array(data, signature='y')

    def add_service_data(self, uuid, data):
        if not self.service_data:
            self.service_data = dbus.Dictionary({}, signature='sv')
        self.service_data[uuid] = dbus.Array(data, signature='y')

    def add_local_name(self, name):
        if not self.local_name:
            self.local_name = ""
        self.local_name = dbus.String(name)

    @dbus.service.method(DBUS_PROP_IFACE,
                         in_signature='s',
                         out_signature='a{sv}')
    def GetAll(self, interface):
        logging.debug('GetAll')
        if interface != LE_ADVERTISEMENT_IFACE:
            raise exceptions.InvalidArgsException()
        logging.debug('returning props')
        return self.get_properties()[LE_ADVERTISEMENT_IFACE]

    @dbus.service.method(LE_ADVERTISEMENT_IFACE,
                         in_signature='',
                         out_signature='')
    def Release(self):
        logging.debug('%s: Released!' % self.path)

    def command(self, cmd):
        logging.debug("ADV command: %s" %(cmd))
        return subprocess.call(cmd, shell=True)
    
    # Range [0x20,0x4000]
    def change_adv_duty(self, min_interval, max_interval):
        # Sanity check in range
        if (min_interval < 0x20):
            min_interval = 0x20
        if (max_interval > 0x4000):
            max_interval = 0x4000
        
        retval = self.command("echo 0x%x > /sys/kernel/debug/bluetooth/hci0/adv_min_interval" %(min_interval))
        if retval != 0:
            logging.error("change_adv_duty: set min_interval retval %d" %(retval))
            return retval
        retval = self.command("echo 0x%x > /sys/kernel/debug/bluetooth/hci0/adv_max_interval" %(max_interval))
        if retval != 0:
            logging.error("change_adv_duty: set max_interval retval %d" %(retval))
            return retval
        return 0

class setupAdvertisement(Advertisement):
    def __init__(self, bus, index, uuid, addr, manager):
        Advertisement.__init__(self, bus, index, 'peripheral', addr, manager)
        logging.info('Connection Service UUID print: %s' % (uuid,))
        self.add_service_uuid(uuid)

        serial = subprocess.Popen(['fw_printenv', '-n', 'serial_number'],
                          stdout=subprocess.PIPE).communicate()[0].rstrip()
        self.device_name = BLE_DEFAULT_NAME + (' %s' %serial[-3:])
        self.manager = manager
        # Get device status
        info = []
        devId = UCI_UTILS().uciGet(BLE_DEFAULT_CONFIG_LOCATION, BLE_DEVICE_ID)
        if(devId == "default"):
            devName = subprocess.Popen(['fw_printenv', '-n', 'board_name'],
                          stdout=subprocess.PIPE).communicate()[0].rstrip()
            if(devName == 'PhD'):
                info.append(0x10)
            else:
                info.append(0x11)

            mac=addr.split(":")
            info.extend(int(x, 16) for x in mac)
        else:
            info = [int(devId[i:i+2], 16) for i in range(0, len(devId), 2)]

        devState = UCI_UTILS().getDevState()
        info.extend(int(devState[i:i+2]) for i in range(0, len(devState), 2))

        logging.info("info %s" %info)

        self.add_manufacturer_data(info[1]<<8|info[0], info[2:])
        # logging.info("Device name: %s" %(self.device_name))
        self.add_local_name(self.device_name)
        self.include_tx_power = True
        # Set broadcast duty, fastest speed
        self.change_adv_duty(0x20, 0x20)

def register_ad_cb():
    logging.info('Advertisement registered!')


def register_ad_error_cb(mainloop, error):
    logging.error('Failed to register advertisement: ' + str(error))
    mainloop.quit()

def advertisingUnregister(advertisement):
    try:
        logging.info("advertisingUnregister()")
        ad_manager = advertisement.get_manager()
        ad_manager.UnregisterAdvertisement(advertisement.get_path())
        #advertisement.Release()
    except Exception as e:
        logging.error("Exception on advertisingUnregister(): %s" %(e))

def advertisingMain(ble, uuid, addr):
    mainloop = ble.mainloop
    bus = ble.dbus
    adapter_name = ble.adapter_name
    adapter = adapters.find_adapter(bus, LE_ADVERTISING_MANAGER_IFACE, adapter_name)
    logging.debug('Advertising using adapter: %s' % (adapter,))
    if not adapter:
        raise Exception('LEAdvertisingManager1 interface not found')

    adapter_props = dbus.Interface(bus.get_object(BLUEZ_SERVICE_NAME, adapter),
                                   "org.freedesktop.DBus.Properties")

    adapter_props.Set("org.bluez.Adapter1", "Powered", dbus.Boolean(1))
    adapter_props.Set("org.bluez.Adapter1", "Discoverable", dbus.Boolean(0))

    ad_manager = dbus.Interface(bus.get_object(BLUEZ_SERVICE_NAME, adapter),
                                LE_ADVERTISING_MANAGER_IFACE)

    advertisement = setupAdvertisement(bus, 0, uuid, addr, ad_manager)
    
    ad_manager.RegisterAdvertisement(advertisement.get_path(), {},
                                     reply_handler=register_ad_cb,
                                     error_handler=functools.partial(register_ad_error_cb, mainloop))

    return advertisement