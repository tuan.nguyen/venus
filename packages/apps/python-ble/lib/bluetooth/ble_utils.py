from __future__ import absolute_import, print_function, unicode_literals

from optparse import OptionParser, make_option
import sys
import time
import dbus
from lib.bluetooth import bluezutils

from constant.system_error import *
from constant.system_define import *

class BLE_UTILS:
    def __init__(self):
        self.bus = dbus.SystemBus()
        self.adapter_path = bluezutils.find_adapter().object_path
        self.adapter = dbus.Interface(self.bus.get_object(SERVICE_NAME, self.adapter_path),
                    DBUS_PROP_IFACE)

    def host_getaddr(self):
        addr = self.adapter.Get(ADAPTER_IFACE, "Address")
        logging.info("Host MAC Addr: %s" %(addr))
        return addr
    
    def host_getname(self):
        name = self.adapter.Get(ADAPTER_IFACE, "Name")
        logging.info("Host Name: %s" %(name))
        return name
    
    def host_setpower(self, state):
        if (state == "on"):
            logging.info("Host Power: on")
            value = dbus.Boolean(1)
        else:
            logging.info("Host Power: off")
            value = dbus.Boolean(0)
        self.adapter.Set(ADAPTER_IFACE, "Powered", value)
    
    
    