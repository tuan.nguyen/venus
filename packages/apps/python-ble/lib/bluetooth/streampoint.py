from __future__ import print_function
import dbus
from lib.bluetooth import gatt_server
from constant.system_error import *
from constant.system_define import *
from lib.parser.csv_parser import *
from lib.secure.uuid_utils import *

try:
    from gi.repository import GObject
except ImportError:
    import gobject as GObject


class StreampointService(gatt_server.GattServerService):
    """
    STREAM POINT CHARACTERISTIC
    Streampoint Characteristic Declaration: 0x3803 READ --> Notify Only | 0x0022 | 3474d050-11ad-41c6-a147-90a20a387e99
    Streampoint Descriptor: 0x3804 READ
    """

    def __init__(self, bus, index):
        streapoint_uuid = CSV_PARSER().param_get(BLE_SYSTEM_CSV, BLE_STREAPOINT_SERV_UUID)
        if not streapoint_uuid:
            raise Exception(BLE_STREAPOINT_SERV_UUID + ': not found!')
        
        gatt_server.GattServerService.__init__(self,
                                               'Streampoint',
                                               bus,
                                               index,
                                               streapoint_uuid,
                                               True)
        self.add_characteristic(StreampointCharacteristic(bus, 0, self))


class StreampointCharacteristic(gatt_server.Characteristic):
    """
    Streampoint Characteristic: 3474d051-11ad-41c6-a147-90a20a387e99 READ, WRITE --> Streaming data
    Streampoint Descriptor: 0x4804 READ
    """

    def __init__(self, bus, index, service):
        streapoint_uuid = CSV_PARSER().param_get(BLE_SYSTEM_CSV, BLE_STREAPOINT_CHAR_UUID)
        if not streapoint_uuid:
            raise Exception(BLE_STREAPOINT_CHAR_UUID + ': not found!')
        
        gatt_server.Characteristic.__init__(
            self, bus, index,
            streapoint_uuid,
            GATT_SECURE_MODE,
            service)
        self.value = []
        self.desc = BLE_STREAPOINT_NAME
        self.add_descriptor(StreamPointDescriptor(bus, 0, self))

    def ReadValue(self, options):
        print('StreampointCharacteristic Read: ' + self.desc)
        return self.desc

    def WriteValue(self, value, options):
        print('StreampointCharacteristic Write: ' + repr(value))
        self.value = value


class StreamPointDescriptor(gatt_server.Descriptor):
    """
    Streampoint Descriptor: 0x4804 READ
    """

    def __init__(self, bus, index, characteristic):
        streapoint_uuid = CSV_PARSER().param_get(BLE_SYSTEM_CSV, BLE_STREAPOINT_DESC_UUID)
        if not streapoint_uuid:
            raise Exception(BLE_STREAPOINT_DESC_UUID + ': not found!')
        
        gatt_server.Descriptor.__init__(
            self, bus, index,
            streapoint_uuid,
            GATT_SECURE_MODE[:1],
            characteristic)

    def ReadValue(self, options):
        print('StreamPointDescriptor Read: %s' %(BLE_STREAPOINT_NAME))
        return [ dbus.Byte(x) for x in BLE_STREAPOINT_NAME ]


def streampointAddService(ble):
    strsrv = StreampointService(ble.dbus, ble.service_index)
    ble.gatt.add_service(strsrv)
    ble.service_index += 1

    return strsrv