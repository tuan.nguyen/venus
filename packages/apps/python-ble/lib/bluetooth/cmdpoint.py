from __future__ import print_function
import time
import dbus
import logging
import os.path
from lib.bluetooth import gatt_server
from constant.system_error import *
from constant.system_define import *
from lib.parser.csv_parser import *
from lib.secure.aes_utils import *
from lib.secure.uuid_utils import *
from lib.network.uci_utils import *
from threading import Timer
# from binascii import hexlify
try:
    from gi.repository import GObject
except ImportError:
    import gobject as GObject


class CmdpointService(gatt_server.GattServerService):
    """
    COMMAND POINT CHARACTERISTIC
    Cmdpoint Characteristic Declaration: 0x3803 READ --> Notify Only | 0x0022 | 3474d050-11ad-41c6-a147-90a20a387e99
    Cmdpoint Descriptor: 0x3804 READ
    """

    def __init__(self, bus, index):
        cmdpoint_uuid = CSV_PARSER().param_get(BLE_SYSTEM_CSV, BLE_COMMAPOINT_SERV_UUID)
        if not cmdpoint_uuid:
            raise Exception(BLE_COMMAPOINT_SERV_UUID + ': not found!')
        
        gatt_server.GattServerService.__init__(self,
                                               'Cmdpoint',
                                               bus,
                                               index,
                                               cmdpoint_uuid,
                                               True)
        self.cmd_char = CommandPointCharacteristic(bus, 0, self)
        self.add_characteristic(self.cmd_char)
    
    # Return: [<number args>, <[<arg1>,[arg2],[arg3]...>]
    def getEvent(self, aes):
        argc = 0
        # Check toggle for receive event
        if self.cmd_char.recv_toggle == 0:
        #if self.cmd_char.recv_str == []:
            return argc, []
        # logging.info('Secured getEvent: decrypting...')
        retval, decrypted = aes.decrypt(self.cmd_char.recv_str)
        if retval != True:
            logging.info("getEvent Decrypted Error: %s" %(decrypted))
            self.cmd_char.recv_toggle = 0
            self.cmd_char.recv_str = []
            return argc, []
        # Parse argument
        argv = decrypted.split(":", 1)
        argc = len(argv)
        logging.debug("%d:'%s'" %(argc,argv))
        return argc, argv
    
    def setEvent(self, event, aes):
        logging.info('Secured setEvent: %s' %(event))
        retval,encrypted = aes.encrypt(event)
        if retval == True:
            self.cmd_char.send_str = encrypted
    
    def clearEvent(self, event):
        # logging.info('Secured clearEvent: %s' %(event))
        self.cmd_char.recv_toggle = 0
        self.cmd_char.recv_str = []
    
    def ackEvent(self, aes):
        logging.info('Secured ackEvent: %s' %(BLE_RESP_ACK))
        # Clear toggle
        self.cmd_char.sent_toggle = 0
        retval,encrypted = aes.encrypt(BLE_RESP_ACK)
        if retval == True:
            self.cmd_char.send_str = encrypted
            # Wait to send
            return self.waitToSend(aes)
    
    def contToSend(self, file_name, aes):
        # Wait to receive BLE_CMD_CONTINUE
        i = 0
        while True:
            i += 1
            argc, argv = self.getEvent(aes)
            if argc > 0:
                event = argv[0]
                self.clearEvent(event)
                if event == BLE_CMD_CONTINUE:
                    break
                elif event == BLE_CMD_STOP:
                    logging.info('contToSend [%s] End!' % file_name)
                    self.cmd_char.send_str = []
                    break
                else:
                    logging.error('contToSend: receive un-expected[%s]!' %(event))
                    return ESYSINVAL
            elif i > BLE_WAIT_TIMEOUT:
                logging.error('contToSend [%s] Timeout failed!' % file_name)
                return ETIMEOUT
            # Sleep
            time.sleep(BLE_WAIT_INTERVAL)
        return ESYSNOERR
    
    def waitToSend(self, aes):
        # Wait to send
        i = 0
        while True:
            i += 1
            # Check toggle for send event
            if self.cmd_char.sent_toggle == 1:
                self.cmd_char.sent_toggle = 0
                break
            elif i > BLE_WAIT_TIMEOUT:
                logging.error('waitToSend Timeout failed!')
                return ETIMEOUT
            # Sleep
            time.sleep(BLE_WAIT_INTERVAL)
        return ESYSNOERR
    
    def waitToReceive(self, aes):
        # Wait to receive
        i = 0
        while True:
            i += 1
            # Check toggle for receive event
            if self.cmd_char.recv_toggle == 1:
                self.cmd_char.recv_toggle = 0
                break
            elif i > BLE_WAIT_TIMEOUT:
                logging.error('waitToSend Timeout failed!')
                return ETIMEOUT
            # Sleep
            time.sleep(BLE_WAIT_INTERVAL)
        return ESYSNOERR
    
    def contToReceive(self, aes):
        # Write BLE_CMD_CONTINUE then read data
        # Clear toggle
        self.cmd_char.sent_toggle = 0
        self.setEvent(BLE_CMD_CONTINUE, aes)
        # Wait to send
        return self.waitToSend(aes)

    def sendMessage(self, aes, message):
        retval = ESYSNOERR
        isEnd=False
        for i in range(0, len(message), BLE_FILE_CHUNK_SIZE):
            buff = message[i:i+BLE_FILE_CHUNK_SIZE]
            if len(buff) > BLE_FILE_END_DATA_SIZE:
                retval,encrypted = aes.encrypt(BLE_CMD_DATA+":%s"%(buff))
                if retval == True:
                    # logging.info("encrypted %s" %hexlify(encrypted))
                    # logging.info("encrypted len = %s" %len(hexlify(encrypted)))
                    self.cmd_char.send_str = encrypted
                else:
                    logging.error('Encrypt: failed')
                    retval = ESYSENCRYPT
                    break

                retval = self.contToSend("devKey", aes)
                if retval != ESYSNOERR:
                    retval = ETIMEOUT
                    break
            else:
                retval=self.endReturn(aes, message=buff)
                isEnd=True
        return retval,isEnd

    def recvMessage(self, aes):
        message=''
        try:
            index = 1
            list = []
            # Loop through to get line in the file
            while True:
                retval = self.contToReceive(aes)
                if retval == ESYSNOERR:
                    # Receive data
                    retval = self.waitToReceive(aes)
                    if retval == ESYSNOERR:
                        # logging.info("encrypted %s" %hexlify(self.cmd_char.recv_str))
                        # logging.info("encrypted len = %s" %len(hexlify(self.cmd_char.recv_str)))
                        retval, recv_data = aes.decrypt(self.cmd_char.recv_str)
                        if retval != True:
                            # logging.info('%s' %(recv_data))
                            # logging.info("recv_data len = %s" %len(recv_data));
                            logging.info("Decrypted Error: %s" %(recv_data))
                            break

                        dataSignal = BLE_CMD_DATA+BLE_CMD_SPLITTER
                        endSignal = BLE_RESP_END+BLE_CMD_SPLITTER

                        dataSignalLen = len(dataSignal)
                        endSignalLen = len(endSignal)

                        if recv_data[:endSignalLen] == endSignal:
                            logging.info('End of File')
                            argv = recv_data.split(":", 2);
                            argc = len(argv)
                            if argc == 3:
                                list.append(argv[2])
                            break
                        elif recv_data[:dataSignalLen] == dataSignal:
                            # messgae=messgae.join(str(recv_data[2:]))
                            list.append(str(recv_data[dataSignalLen:]))
                    else:
                        logging.error('waitToReceive: %d' %retval)
                        break
                else:
                    logging.error('contToReceive: %d' %retval)
                    break
                # Continue reading
                index += 1
            message = ''.join(str(x) for x in list)
        except Exception, e:
            logging.error('recvMessage %d: %s' % ( index, e))
        return message

    def sendWifiListFile(self, file_name, aes):
        '''
            @file_name: text file name
        '''
        rc = ESYSNOERR
        isEnd = False
        try:
            file = open(file_name, "rb")
            while True:
                piece = file.read(BLE_FILE_CHUNK_SIZE)# 16byte iv
                if piece == "":
                    break;
                line_str = str(piece)
                # logging.info("line_str %s" %line_str)
                # logging.info("line_str len = %s" %len(line_str))
                rc,isEnd = self.sendMessage(aes, line_str)
            if isEnd == False:
                self.endReturn(aes)
            return rc

        except Exception, e:
            logging.error('Open [%s] error' % file_name)
            logging.error(e)
            self.endReturn(aes, E_FILE_ERROR, S_FILE_ERROR)
            return EEXCEPTION

    def scanWifiTimer(self):
        os.system("/usr/sbin/wifi_scan &")
        return ESYSNOERR

    def sendDevState(self, aes):
        try:
            # Send data
            rc = ESYSNOERR
            isEnd = False

            devState = UCI_UTILS().getDevState()
            rc,isEnd = self.sendMessage(aes, devState)

            if isEnd == False:
                self.endReturn(aes)
            return rc

        except Exception, e:
            logging.error('%s' %(e))
            return EEXCEPTION

    def sendDevToken(self, aes):
        try:
            # Send data
            rc = ESYSNOERR
            isEnd = False

            devToken = UCI_UTILS().uciGet(BLE_DEFAULT_CONFIG_LOCATION, BLE_DEV_TOKEN_STORE)
            rc,isEnd = self.sendMessage(aes, devToken)

            if isEnd == False:
                self.endReturn(aes)
            return rc

        except Exception, e:
            logging.error('%s' %(e))
            return EEXCEPTION

    def sendDevId(self, aes):
        try:
            # Send data
            rc = ESYSNOERR
            isEnd = False

            devId = UCI_UTILS().uciGet(BLE_DEFAULT_CONFIG_LOCATION, BLE_DEVICE_ID)
            rc,isEnd = self.sendMessage(aes, devId)

            if isEnd == False:
                self.endReturn(aes)
            return rc

        except Exception, e:
            logging.error('%s' %(e))
            return EEXCEPTION

    def endReturn(self, aes, error=E_NO_ERR, message=""):
        data = BLE_RESP_END+ ":%s:%s" %(error,message)
        logging.info('endData: %s' %(data))
        retval,encrypted = aes.encrypt(data)
        if retval == True:
            self.cmd_char.sent_toggle = 0
            self.cmd_char.send_str = encrypted
        else:
            logging.error('Encrypt: failed')
            rc = ESYSENCRYPT
            return rc
        return self.waitToSend(aes)

class CommandPointCharacteristic(gatt_server.Characteristic):
    """
    Cmdpoint Characteristic: 3474d050-11ad-41c6-a147-90a20a387e99 READ, WRITE, NOTIFY --> Variable
    Cmdpoint Descriptor: 0x3804 READ
    """

    def __init__(self, bus, index, service):
        cmdpoint_uuid = CSV_PARSER().param_get(BLE_SYSTEM_CSV, BLE_COMMAPOINT_CHAR_UUID)
        if not cmdpoint_uuid:
            raise Exception(BLE_COMMAPOINT_CHAR_UUID + ': not found!')
        
        gatt_server.Characteristic.__init__(
            self, bus, index,
            cmdpoint_uuid,
            GATT_SECURE_MODE,
            service)
        self.recv_toggle = 0
        self.sent_toggle = 0
        self.send_str = []
        self.recv_str = []
        self.desc =  BLE_COMMAPOINT_NAME
        self.cmd_desc = CommandPointDescriptor(bus, 0, self)
        self.add_descriptor(self.cmd_desc)

    def ReadValue(self, options):
        #message = self.send_str
        #self.send_str = []
        if len(self.send_str) < 1:
            return []
        # logging.info('CommandPointCharacteristic Sent:[%d] %s' %(len(self.send_str), self.send_str))
        self.sent_toggle = 1
        message = self.send_str
        self.send_str = []
        return message

    def WriteValue(self, value, options):
        self.recv_str = ''.join([chr(character) for character in value])
        # logging.info("CommandPointCharacteristic Received:[%d] %s" %(len(self.recv_str), self.recv_str))
        self.recv_toggle = 1
    
        
class CommandPointDescriptor(gatt_server.Descriptor):
    """
    Cmdpoint Descriptor: 0x3804 READ
    """

    def __init__(self, bus, index, characteristic):
        cmdpoint_uuid = CSV_PARSER().param_get(BLE_SYSTEM_CSV, BLE_COMMAPOINT_DESC_UUID)
        if not cmdpoint_uuid:
            raise Exception(BLE_COMMAPOINT_DESC_UUID + ': not found!')
        
        gatt_server.Descriptor.__init__(
            self, bus, index,
            cmdpoint_uuid,
            GATT_SECURE_MODE[:1],
            characteristic)
        self.send_str = BLE_COMMAPOINT_NAME
        
    def ReadValue(self, options):
        logging.info('CommandPointDescriptor Sent: ' + self.send_str)
        return self.send_str


def cmdpointAddService(ble):
    cmdsrv = CmdpointService(ble.dbus, ble.service_index)
    ble.gatt.add_service(cmdsrv)
    ble.service_index += 1
    return cmdsrv


class CommandpointThread(threading.Thread):
    wifiTimer = None

    def __init__(self, ble):
        threading.Thread.__init__(self)
        self.ble = ble
        self.devstate = ble.devstate
        self.csvp = ble.csvp
        self.waitsec = BLE_WAIT_INTERVAL
        self.srv = ble.cmdsrv
        # self.indc = ble.indicator
        self.secure = ble.pair
        self.dh = ble.dh
        self.cipher = ble.aes
        self.mainloop = ble.mainloop
        self.thread_flag = True
        self.wifiTimer = Timer(5,self.srv.scanWifiTimer)

    def setClaimInfo(self, data):
        if not data:
            return ENOAVAILABLE

        try:
            homeId=userId=uuid=token=sURL=''
            ssid=''
            auth=''
            wifiKey=''
            uci = UCI_UTILS()
            claimInfo = data.split("\n")
            claimLen = len(claimInfo)
            if(claimLen < 8):
                logging.info("claim info missing")
                return ENOAVAILABLE

            for element in claimInfo:
                if element[:len(BLE_CLAIM_UUID)+1] == BLE_CLAIM_UUID+":":
                    uuid=element[len(BLE_CLAIM_UUID)+1:]
                elif element[:len(BLE_CLAIM_TOKEN)+1] == BLE_CLAIM_TOKEN+":":
                    token=element[len(BLE_CLAIM_TOKEN)+1:]
                elif element[:len(BLE_CLAIM_SERVER_URL)+1] == BLE_CLAIM_SERVER_URL+":":
                    sURL=element[len(BLE_CLAIM_SERVER_URL)+1:]
                # elif element[:len(BLE_CLAIM_FIRMWARE_URL)+1] == BLE_CLAIM_FIRMWARE_URL+":":
                #     fURL=element[len(BLE_CLAIM_FIRMWARE_URL)+1:]
                elif element[:len(BLE_CLAIM_SSID)+1] == BLE_CLAIM_SSID+":":
                    ssid=element[len(BLE_CLAIM_SSID)+1:]
                elif element[:len(BLE_CLAIM_AUTH_TYPE)+1] == BLE_CLAIM_AUTH_TYPE+":":
                    auth=element[len(BLE_CLAIM_AUTH_TYPE)+1:]
                elif element[:len(BLE_CLAIM_PASS)+1] == BLE_CLAIM_PASS+":":
                    wifiKey=element[len(BLE_CLAIM_PASS)+1:]
                elif element[:len(BLE_CLAIM_HOME_ID)+1] == BLE_CLAIM_HOME_ID+":":
                    homeId=element[len(BLE_CLAIM_HOME_ID)+1:]
                elif element[:len(BLE_CLAIM_USER_ID)+1] == BLE_CLAIM_USER_ID+":":
                    userId=element[len(BLE_CLAIM_USER_ID)+1:]

            if (not uuid or not token or not sURL or
                not ssid or not wifiKey or not auth or
                not homeId):
                logging.info("set Claim missing infor")
                return ENOAVAILABLE

            cmd="%s '%s' '%s' '%s' &" %(BLE_CONFIGURE_WIFI_CMD, ssid, auth, wifiKey)
            if subprocess.call(shlex.split(cmd)) != ESYSNOERR:
                logging.error("UCI: %s failed!" %(cmd))
                return ESYSINVAL

            uci.setSecurityInfo(uuid, token, sURL, homeId, userId)

            cmd="%s &" %(BLE_CONNECT_WIFI_CMD)
            os.system(cmd)

            return ESYSNOERR

        except Exception, e:
            logging.error('Set Claim info error %s' % (e))
            return EEXCEPTION

    def setInfo(self, data):
        if not data:
            return ENOAVAILABLE

        try:
            homeId=userId=uuid=token=sURL=''
            uci = UCI_UTILS()
            claimInfo = data.split("\n")
            claimLen = len(claimInfo)
            if(claimLen < 5):
                logging.info("claim info missing")
                return ENOAVAILABLE

            for element in claimInfo:
                if element[:len(BLE_CLAIM_UUID)+1] == BLE_CLAIM_UUID+":":
                    uuid=element[len(BLE_CLAIM_UUID)+1:]
                elif element[:len(BLE_CLAIM_TOKEN)+1] == BLE_CLAIM_TOKEN+":":
                    token=element[len(BLE_CLAIM_TOKEN)+1:]
                elif element[:len(BLE_CLAIM_SERVER_URL)+1] == BLE_CLAIM_SERVER_URL+":":
                    sURL=element[len(BLE_CLAIM_SERVER_URL)+1:]
                # elif element[:len(BLE_CLAIM_FIRMWARE_URL)+1] == BLE_CLAIM_FIRMWARE_URL+":":
                #     fURL=element[len(BLE_CLAIM_FIRMWARE_URL)+1:]
                elif element[:len(BLE_CLAIM_HOME_ID)+1] == BLE_CLAIM_HOME_ID+":":
                    homeId=element[len(BLE_CLAIM_HOME_ID)+1:]
                elif element[:len(BLE_CLAIM_USER_ID)+1] == BLE_CLAIM_USER_ID+":":
                    userId=element[len(BLE_CLAIM_USER_ID)+1:]

            if (not uuid or not token or not sURL or
                not homeId):
                logging.info("set info missing")
                return ENOAVAILABLE

            uci.setSecurityInfo(uuid, token, sURL, homeId, userId)

            cmd="%s &" %(BLE_CONNECT_WIFI_CMD)
            os.system(cmd)

            return ESYSNOERR

        except Exception, e:
            logging.error('Set Claim info error %s' % (e))
            return EEXCEPTION

    def changeDevState(self):
        try:
            cloudState = UCI_UTILS().uciGet(BLE_DEFAULT_CONFIG_LOCATION, BLE_CLOUD_STATE)
            if(cloudState == "registered"):
                retry=10
                while (retry>0) and (cloudState != "claimed"):
                    time.sleep(1)
                    cloudState = UCI_UTILS().uciGet(BLE_DEFAULT_CONFIG_LOCATION, BLE_CLOUD_STATE)
                    retry-=1

            if(cloudState == "claimed"):
                UCI_UTILS().uciSet(BLE_DEFAULT_CONFIG_LOCATION, BLE_WIFI_AP_MODE_DISABLED+"='1'", 1)
                os.system("wifi &");
                os.system("/etc/init.d/ble restart &");
                os._exit(0)
            return ESYSNOERR

        except Exception, e:
            logging.error('change dev state error %s' % (e))
            return EEXCEPTION

    def run(self):
        # Function thread to manage connection
        try:
            logging.info("Secured Commandpoint Thread running...")
            
            secured_message = []
            # Loop to check Events
            while self.thread_flag:
                argc, argv = self.srv.getEvent(self.cipher)
                if argc < 1:
                    # Idle sleep
                    time.sleep(self.waitsec)
                    continue
                event = argv[0]
                # Have event, clear then process it
                self.srv.clearEvent(event)
                
                # Set activity
                # self.indc.set_state(True)
                
                # Check security Pair
                if self.secure.getConnectSafetyStatus() == False:
                    logging.warning("Insecured event: %s, somebody hacking!!! --> skipped!" %(event))
                    continue
                
                # Normal operations
                if event == BLE_CMD_IDENTIFY:
                    if self.srv.endReturn(self.cipher) == ESYSNOERR:
                        os.system("mpg123 /etc/sound/system/identify &")
                        os.system("ubus call venus.system setLed '{\"state\":\"identify\"}'")
                    else:
                        logging.error("END not sent!")
                elif event == BLE_CMD_SELECT_WIFI:
                    if UCI_UTILS().getDevState() != BLE_DEV_STATE_UNCONFIGURE:
                        self.srv.endReturn(self.cipher, E_DEV_CONFIGURED, S_DEV_CONFIGURED)
                        continue

                    mpgId = subprocess.Popen(['pgrep', 'mpg123'], 
                                stdout=subprocess.PIPE).communicate()[0].rstrip()
                    if mpgId:
                        time.sleep(1)
                    if self.srv.endReturn(self.cipher) == ESYSNOERR:
                        os.system("mpg123 /etc/sound/system/SelectWiFiInform &")
                    else:
                        logging.error("ACK not sent!")
                elif event == BLE_CMD_GET_WIFI_LIST:
                    if UCI_UTILS().getDevState() != BLE_DEV_STATE_UNCONFIGURE:
                        self.srv.endReturn(self.cipher, E_DEV_CONFIGURED, S_DEV_CONFIGURED)
                        continue

                    if(self.wifiTimer.is_alive()==0):
                        logging.info("Start scan wifi timer!")
                        self.wifiTimer = Timer(5, self.srv.scanWifiTimer)
                        self.wifiTimer.start()

                    if not os.path.isfile(BLE_WIFI_FILE_SCAN_RESULT) :
                        logging.error("File not existed!")
                        self.srv.endReturn(self.cipher, message=S_FILE_NOT_FOUND)
                        continue
                    if self.srv.sendWifiListFile(BLE_WIFI_FILE_SCAN_RESULT, self.cipher) == ESYSNOERR:
                        logging.info("Sent file: %s done!" %(BLE_WIFI_FILE_SCAN_RESULT))
                    else:
                        logging.info("Sent file: %s failed!" %(BLE_WIFI_FILE_SCAN_RESULT))
                elif event == BLE_CMD_SET_CLAIM_INFO:
                    if UCI_UTILS().getDevState() > BLE_DEV_STATE_VALIDATED:
                        self.srv.endReturn(self.cipher, E_DEV_CONFIGURED, S_DEV_CONFIGURED)
                        continue

                    message = self.srv.recvMessage(self.cipher)
                    if message:
                        logging.info("receive message: %s" %(message))
                        self.setClaimInfo(message)
                elif event == BLE_CMD_SET_INFO:
                    if UCI_UTILS().getDevState() > BLE_DEV_STATE_VALIDATED:
                        self.srv.endReturn(self.cipher, E_DEV_CONFIGURED, S_DEV_CONFIGURED)
                        continue
                    message = self.srv.recvMessage(self.cipher)
                    if message:
                        logging.info("receive message: %s" %(message))
                        self.setInfo(message)
                elif event == BLE_CMD_GET_DEV_STATE:
                        self.srv.sendDevState(self.cipher)

                elif event == BLE_CMD_CHANGE_DEV_STATE:
                    if self.srv.endReturn(self.cipher) == ESYSNOERR:
                        self.changeDevState()
                    else:
                        logging.error("endReturn not sent!")
                elif event == BLE_CMD_GET_TOKEN:
                    if(argc != 2):
                        self.srv.endReturn(self.cipher, E_UUID_INVALID, S_FAILED)
                        continue

                    if UCI_UTILS().getDevState() == BLE_DEV_STATE_CONNECTED:
                        self.srv.endReturn(self.cipher, E_DEV_CONFIGURED, S_DEV_CONFIGURED)
                        continue

                    userUUID = argv[1]
                    currentUUID = UCI_UTILS().uciGet(BLE_DEFAULT_CONFIG_LOCATION, BLE_DEV_UUID_STORE)
                    if(currentUUID != userUUID):
                        self.srv.endReturn(self.cipher, E_UUID_INVALID, S_FAILED)
                        continue
                    self.srv.sendDevToken(self.cipher)

                elif event == BLE_CMD_GET_DEVID:
                        self.srv.sendDevId(self.cipher)
                # elif event == "RST":
                #     self.srv.endReturn(self.cipher)
                #     os.system("reboot")
                # elif event == "TTT":
                #     self.srv.endReturn(self.cipher)
                #     os.system("/etc/init.d/ble restart &")
                else:
                    logging.error("Secured Command [%s] not supported!" %(event))
                    self.srv.endReturn(self.cipher, E_CMD_NOT_SUPPORT, S_NOT_SUPPORT)
        except Exception as e:
            logging.error("Exception on Commandpoint Thread: %s" %(e))
            #self.bucket.put(sys.exc_info())
            self.mainloop.quit()
        logging.info("Secured Commandpoint Thread Exiting...")

    def setThreadFlag(self, state):
        self.thread_flag = state