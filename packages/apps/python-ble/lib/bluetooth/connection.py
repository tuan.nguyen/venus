from __future__ import print_function
import time
import dbus
import logging
import os.path
from lib.bluetooth import gatt_server
from constant.system_error import *
from constant.system_define import *
from lib.parser.csv_parser import *
from lib.secure import diffiehellman
from lib.secure.aes_utils import *

# from binascii import hexlify
from threading import Timer
from lib.bluetooth import advertising
try:
    from gi.repository import GObject
except ImportError:
    import gobject as GObject


class ConnectionService(gatt_server.GattServerService):
    """
    VENUS Service 0x2800 for Primary Service READ 3474d04f-11ad-41c6-a147-90a20a387e99
    Connection Characteristic Declaration: 0x2803 READ --> Notify Only | 0x0022 | 3474d050-11ad-41c6-a147-90a20a387e99
    Connection Descriptor: 0x2804 READ
    """

    def __init__(self, bus, index):
        connection_uuid = CSV_PARSER().param_get(BLE_SYSTEM_CSV, BLE_CONNECTION_SERV_UUID)
        if not connection_uuid:
            raise Exception(BLE_CONNECTION_SERV_UUID + ': not found!')
        
        gatt_server.GattServerService.__init__(self,
                                               'Connection',
                                               bus,
                                               index,
                                               connection_uuid,
                                               True)
        self.cnt_char = ConnectionCharacteristic(bus, 0, self)
        self.add_characteristic(self.cnt_char)
    
    # Return: [<number args>, <[<arg1>,[arg2],[arg3]...>]
    def getEvent(self):
        argc = 0
        # Check toggle for receive event
        if self.cnt_char.recv_toggle == 0:
        #if self.cnt_char.recv_str == []:
            return argc, []
        logging.info('getEvent: %s' %(self.cnt_char.recv_str))
        data = str(self.cnt_char.recv_str)
        # Parse argument
        argv = data.split(":", 1)
        argc = len(argv)
        logging.debug("%d:'%s'" %(argc,argv))
        return argc, argv
    
    def setEvent(self, event):
        logging.info('setEvent: %s' %(event))
        self.cnt_char.send_str = event
    
    def clearEvent(self, event):
        logging.info('clearEvent: %s' %(event))
        self.cnt_char.recv_toggle = 0
        self.cnt_char.recv_str = []
     
    def contToSend(self, file_name):
        # Wait to receive BLE_CMD_CONTINUE
        i = 0
        while True:
            i += 1
            argc, argv = self.getEvent()
            if argc > 0:
                event = argv[0]
                self.clearEvent(event)
                if event == BLE_CMD_CONTINUE:
                    break
                elif event == BLE_CMD_STOP:
                    logging.info('contToSend [%s] End!' % file_name)
                    self.cnt_char.send_str = []
                    break
                else:
                    logging.error('contToSend: receive un-expected[%s]!' %(event))
                    return ESYSINVAL
            elif i > BLE_WAIT_TIMEOUT:
                logging.error('contToSend [%s] Timeout failed!' % file_name)
                return ETIMEOUT
            # Sleep
            time.sleep(BLE_WAIT_INTERVAL)
        return ESYSNOERR
    
    def waitToSend(self):
        # Wait to send
        i = 0
        while True:
            i += 1
            # Check toggle for send event
            if self.cnt_char.sent_toggle == 1:
                # Wait data out of buffer
                #time.sleep(BLE_WAIT_INTERVAL)
                self.cnt_char.sent_toggle = 0
                break
            elif i > BLE_WAIT_TIMEOUT:
                logging.error('waitToSend Timeout failed!')
                return ETIMEOUT
            # Sleep
            time.sleep(BLE_WAIT_INTERVAL)
        return ESYSNOERR

    def sendKey(self, key):
        try:
            for i in range(0, len(key), BLE_FILE_CHUNK_SIZE):
                buff = key[i:i+BLE_FILE_CHUNK_SIZE]
                if len(buff) > BLE_FILE_END_DATA_SIZE:
                    self.cnt_char.send_str = BLE_CMD_DATA+":%s"%(buff)
                    retval = self.contToSend("devKey")
                    if retval != ESYSNOERR:
                        retval = ETIMEOUT
                        break
                else:
                    retval=self.endReturn(message=buff)
            return retval

        except Exception, e:
            logging.error('%s' %(e))

        return retval

    def endReturn(self, error=E_NO_ERR, message=""):
        data = BLE_RESP_END+ ":%s:%s" %(error,message)
        logging.info('endData: %s' %(data))
        # Clear toggle
        self.cnt_char.sent_toggle = 0
        self.cnt_char.send_str = data
        # Wait to send
        return self.waitToSend()

class ConnectionCharacteristic(gatt_server.Characteristic):
    """
    Connection Characteristic Declaration: 0x2803 READ --> Notify Only | 0x0022 | 3474d050-11ad-41c6-a147-90a20a387e99
    Connection Descriptor: 0x2804 READ
    """

    def __init__(self, bus, index, service):
        connection_uuid = CSV_PARSER().param_get(BLE_SYSTEM_CSV, BLE_CONNECTION_CHAR_UUID)
        if not connection_uuid:
            raise Exception(BLE_CONNECTION_CHAR_UUID + ': not found!')
        
        gatt_server.Characteristic.__init__(
            self, bus, index,
            connection_uuid,
            GATT_SECURE_MODE,
            service)
        self.recv_toggle = 0
        self.sent_toggle = 0
        self.send_str = []
        self.recv_str = []
        self.desc = BLE_CONNECTION_NAME
        self.add_descriptor(ConnectionDescriptor(bus, 0, self))

    def ReadValue(self, options):
        #message = self.send_str
        #self.send_str = []
        if len(self.send_str) < 1:
            return []
        logging.info('ConnectionCharacteristic Sent:[%d] %s' %(len(self.send_str), self.send_str))
        self.sent_toggle = 1
        message = self.send_str
        self.send_str = []
        return message
 
    def WriteValue(self, value, options):
        self.recv_str = ''.join([chr(character) for character in value])
        logging.info("ConnectionCharacteristic Received:[%d] %s" %(len(self.recv_str), self.recv_str))
        self.recv_toggle = 1
    
    
class ConnectionDescriptor(gatt_server.Descriptor):
    """
    Connection Descriptor: 0x2804 READ
    """

    def __init__(self, bus, index, characteristic):
        connection_uuid = CSV_PARSER().param_get(BLE_SYSTEM_CSV, BLE_CONNECTION_DESC_UUID)
        if not connection_uuid:
            raise Exception(BLE_CONNECTION_DESC_UUID + ': not found!')
        
        gatt_server.Descriptor.__init__(
            self, bus, index,
            connection_uuid,
            GATT_SECURE_MODE[:1],
            characteristic)

    def ReadValue(self, options):
        logging.info('ConnectionDescriptor Sent: %s' %(BLE_CONNECTION_NAME))
        return [ dbus.Byte(x) for x in BLE_CONNECTION_NAME ]


def interfaces_removed_cb(object_path, interfaces):
    logging.info('Connection Service was removed')
    
    
def interfaces_added_cb(object_path, interfaces):
    logging.info('Connection Service was added')


def connectServerRegister(ble):
    cntsrv = ConnectionService(ble.dbus, ble.service_index)
    ble.gatt.add_service(cntsrv)
    ble.service_index += 1
    return cntsrv    


class ConnectionThread(threading.Thread):

    def __init__(self, ble):
        threading.Thread.__init__(self)
        self.ble = ble
        # self.devstate = ble.devstate
        # self.csvp = ble.csvp
        self.waitsec = BLE_WAIT_INTERVAL
        self.srv = ble.cntsrv
        # self.indc = ble.indicator
        self.secure = ble.pair
        self.dh = ble.dh
        self.shareKey = None
        # self.cipher = ble.aes
        self.mainloop = ble.mainloop
        self.thread_flag = True

    def run(self):
        # Function thread to manage connection
        try:
            logging.info("Insecured Connection Thread running...")
            rdwr_message = []
            # Loop to check Events
            while self.thread_flag:
                argc,argv = self.srv.getEvent()
                if argc < 1:
                    # Idle sleep
                    time.sleep(self.waitsec)
                    continue
                event = argv[0]
                # Have event, clear then process it
                self.srv.clearEvent(event)
                # Check security Connecion
                if self.secure.getConnectSafetyStatus() == False:
                    if self.secure.getConnectThreshold() > 0:
                        if event == BLE_CMD_EXCHANGE_KEY:
                            if(argc != 2):
                                self.srv.endReturn(E_KEY_INVALID, S_FAILED)
                                continue
                            
                            appPublicKey = argv[1]

                            if len(appPublicKey) != 32:
                                self.srv.endReturn(E_KEY_INVALID, S_FAILED)
                                continue

                            self.secure.setConnectSafety()
                            # logging.info("current time = %s" %time.time())
                            # init DH key
                            self.dh = diffiehellman.Private()
                            publicKey = self.dh.get_public()
                            appPublic = diffiehellman.Public(appPublicKey)
                            # create shared key
                            self.shareKey = self.dh.get_shared_key(appPublic)
                            # init aes
                            aes = AES_UTILS(self.shareKey)
                            # encrypt AES default key
                            result,encrypted=aes.encrypt(self.ble.aes.key)
                            # logging.info("current time = %s" %time.time())
                            if result != True:
                                self.srv.endReturn(E_ENCRYPT, S_FAILED)
                            self.srv.sendKey(publicKey.serialize()+encrypted)
                        else:
                            logging.error("Command [%s] not supported!" %(event))
                            self.srv.endReturn(E_CMD_NOT_SUPPORT, S_NOT_SUPPORT)
                    else:
                        logging.warning("Insecured event: %s, somebody hacking!!! --> skipped!" %(event))
                    continue

                if event == BLE_CMD_EXCHANGE_KEY:
                    if(argc != 2):
                        self.srv.endReturn(E_KEY_INVALID, S_FAILED)
                        continue
                    
                    appPublicKey = argv[1]

                    if len(appPublicKey) != 32:
                        self.srv.endReturn(E_KEY_INVALID, S_FAILED)
                        continue

                    self.secure.setConnectSafety()
                    # logging.info("current time = %s" %time.time())
                    # init DH key
                    self.dh = diffiehellman.Private()
                    publicKey = self.dh.get_public()
                    appPublic = diffiehellman.Public(appPublicKey)
                    # create shared key
                    self.shareKey = self.dh.get_shared_key(appPublic)
                    # init aes
                    aes = AES_UTILS(self.shareKey)
                    # encrypt AES default key
                    result,encrypted=aes.encrypt(self.ble.aes.key)
                    # logging.info("current time = %s" %time.time())
                    if result != True:
                        self.srv.endReturn(E_ENCRYPT, S_FAILED)
                    self.srv.sendKey(publicKey.serialize()+encrypted)
                else:
                    logging.error("Command [%s] not supported!" %(event))
                    self.srv.endReturn(E_CMD_NOT_SUPPORT, S_NOT_SUPPORT)
        except Exception as e:
            logging.error("Exception on Connection Thread: %s" %(e))
            #self.bucket.put(sys.exc_info())
            self.mainloop.quit()
        logging.info("Insecured Connection Thread Exiting...")

    def setThreadFlag(self, state):
        self.thread_flag = state