from __future__ import print_function

import dbus
import dbus.exceptions
import dbus.mainloop.glib
import dbus.service
import functools
import logging

from lib.bluetooth import adapters
from lib.bluetooth import exceptions
from constant.system_error import *
from constant.system_define import *
from lib.parser.csv_parser import *
from lib.secure.uuid_utils import *

try:
    from gi.repository import GObject
except ImportError:
    import gobject as GObject

class Application(dbus.service.Object):
    """
    org.bluez.GattApplication1 interface implementation
    """

    def __init__(self, bus):
        self.path = '/'
        self.services = []
        dbus.service.Object.__init__(self, bus, self.path)

    def get_path(self):
        return dbus.ObjectPath(self.path)

    def add_service(self, service):
        self.services.append(service)

    @dbus.service.method(DBUS_OM_IFACE, out_signature='a{oa{sa{sv}}}')
    def GetManagedObjects(self):
        response = {}
        logging.debug('GetManagedObjects')

        for service in self.services:
            response[service.get_path()] = service.get_properties()
            chrcs = service.get_characteristics()
            for chrc in chrcs:
                response[chrc.get_path()] = chrc.get_properties()
                descs = chrc.get_descriptors()
                for desc in descs:
                    response[desc.get_path()] = desc.get_properties()

        return response


class GattServerService(dbus.service.Object):
    """
    org.bluez.GattService1 interface implementation
    """

    def __init__(self, srv, bus, index, uuid, primary):
        self.path = BLE_PATH_BASE + '/' + srv + str(index)
        self.bus = bus
        self.uuid = uuid
        self.primary = primary
        self.characteristics = []
        dbus.service.Object.__init__(self, bus, self.path)
        logging.info('GattServerService: %s, UUID: %s' % (self.path, self.uuid))

    def get_properties(self):
        return {
            GATT_SERVICE_IFACE: {
                'UUID': self.uuid,
                'Primary': self.primary,
                'Characteristics': dbus.Array(
                    self.get_characteristic_paths(),
                    signature='o')
            }
        }

    def get_path(self):
        return dbus.ObjectPath(self.path)

    def add_characteristic(self, characteristic):
        self.characteristics.append(characteristic)

    def get_characteristic_paths(self):
        result = []
        for chrc in self.characteristics:
            result.append(chrc.get_path())
        return result

    def get_characteristics(self):
        return self.characteristics

    @dbus.service.method(DBUS_PROP_IFACE,
                         in_signature='s',
                         out_signature='a{sv}')
    def GetAll(self, interface):
        if interface != GATT_SERVICE_IFACE:
            raise exceptions.InvalidArgsException()

        return self.get_properties()[GATT_SERVICE_IFACE]


class Characteristic(dbus.service.Object):
    """
    org.bluez.GattCharacteristic1 interface implementation
    """

    def __init__(self, bus, index, uuid, flags, service):
        self.path = service.path + '/char' + str(index)
        self.bus = bus
        self.uuid = uuid
        self.service = service
        self.flags = flags
        self.descriptors = []
        dbus.service.Object.__init__(self, bus, self.path)

    def get_properties(self):
        return {
            GATT_CHRC_IFACE: {
                'Service': self.service.get_path(),
                'UUID': self.uuid,
                'Flags': self.flags,
                'Descriptors': dbus.Array(
                    self.get_descriptor_paths(),
                    signature='o')
            }
        }

    def get_path(self):
        return dbus.ObjectPath(self.path)

    def add_descriptor(self, descriptor):
        self.descriptors.append(descriptor)

    def get_descriptor_paths(self):
        result = []
        for desc in self.descriptors:
            result.append(desc.get_path())
        return result

    def get_descriptors(self):
        return self.descriptors

    def get_characteristic_paths(self):
        result = []
        for chrc in self.characteristics:
            result.append(chrc.get_path())
        return result

    def get_characteristics(self):
        return self.characteristics

    @dbus.service.method(DBUS_PROP_IFACE,
                         in_signature='s',
                         out_signature='a{sv}')
    def GetAll(self, interface):
        if interface != GATT_CHRC_IFACE:
            raise exceptions.InvalidArgsException()

        return self.get_properties()[GATT_CHRC_IFACE]

    @dbus.service.method(GATT_CHRC_IFACE,
                         in_signature='a{sv}',
                         out_signature='ay')
    def ReadValue(self, options):
        logging.error('Default ReadValue called, returning error')
        raise exceptions.NotSupportedException()

    @dbus.service.method(GATT_CHRC_IFACE, in_signature='aya{sv}')
    def WriteValue(self, value, options):
        logging.error('Default WriteValue called, returning error')
        raise exceptions.NotSupportedException()

    @dbus.service.method(GATT_CHRC_IFACE)
    def StartNotify(self):
        logging.error('Default StartNotify called, returning error')
        raise exceptions.NotSupportedException()

    @dbus.service.method(GATT_CHRC_IFACE)
    def StopNotify(self):
        logging.error('Default StopNotify called, returning error')
        raise exceptions.NotSupportedException()

    @dbus.service.signal(DBUS_PROP_IFACE,
                         signature='sa{sv}as')
    def PropertiesChanged(self, interface, changed, invalidated):
        pass


class Descriptor(dbus.service.Object):
    """
    org.bluez.GattDescriptor1 interface implementation
    """

    def __init__(self, bus, index, uuid, flags, characteristic):
        self.path = characteristic.path + '/desc' + str(index)
        self.bus = bus
        self.uuid = uuid
        self.flags = flags
        self.chrc = characteristic
        dbus.service.Object.__init__(self, bus, self.path)

    def get_properties(self):
        return {
            GATT_DESC_IFACE: {
                'Characteristic': self.chrc.get_path(),
                'UUID': self.uuid,
                'Flags': self.flags,
            }
        }

    def get_path(self):
        return dbus.ObjectPath(self.path)

    @dbus.service.method(DBUS_PROP_IFACE,
                         in_signature='s',
                         out_signature='a{sv}')
    def GetAll(self, interface):
        if interface != GATT_DESC_IFACE:
            raise exceptions.InvalidArgsException()

        return self.get_properties()[GATT_DESC_IFACE]

    @dbus.service.method(GATT_DESC_IFACE,
                         in_signature='a{sv}',
                         out_signature='ay')
    def ReadValue(self, options):
        logging.error('Default ReadValue called, returning error')
        raise exceptions.NotSupportedException()

    @dbus.service.method(GATT_DESC_IFACE, in_signature='aya{sv}')
    def WriteValue(self, value, options):
        logging.error('Default WriteValue called, returning error')
        raise exceptions.NotSupportedException()


def interfaces_removed_cb(object_path, interfaces):
    logging.info('GATT Service was removed')
    
    
def interfaces_added_cb(object_path, interfaces):
    logging.info('GATT Service was added')


def gattServerInit(ble):
    """
    Init GATT Service Server
    """
    ble.adapterManager = adapters.find_adapter(ble.dbus, GATT_MANAGER_IFACE, ble.adapter_name)
    logging.debug('Using adapter: %s' % (ble.adapterManager,))
    if not ble.adapterManager:
        raise Exception('GattManager1 interface not found')

    service = dbus.Interface(
        ble.dbus.get_object(BLUEZ_SERVICE_NAME, ble.adapterManager),
        GATT_MANAGER_IFACE)

    # Monitor Added/Removed Event
#     logging.info('GATT Service monitor: InterfacesAdded')
#     service.connect_to_signal('InterfacesAdded', interfaces_added_cb)
#     logging.info('GATT Service monitor: InterfacesRemoved')
#     service.connect_to_signal('InterfacesRemoved', interfaces_removed_cb)

    return service


def register_gatt_cb():
    logging.info('GATT Server registered')

def register_gatt_error_cb(mainloop, error):
    logging.error('Failed to register Server: ' + str(error))
    mainloop.quit()

def gattServerUnregister(ble):
    try:
        logging.info('Unregistering GATT Connection...')
        ble.serviceManager.UnregisterApplication(ble.gatt.get_path())
    except Exception as e:
        logging.error("Exception on gattServerUnregister(): %s" %(e))

def gattServerRegister(ble):
    logging.debug('Registering GATT Connection...')

    ble.serviceManager.RegisterApplication(ble.gatt.get_path(), {},
                            reply_handler=register_gatt_cb,
                            error_handler=functools.partial(register_gatt_error_cb, ble.mainloop))
