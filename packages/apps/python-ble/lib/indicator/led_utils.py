from __future__ import absolute_import, print_function, unicode_literals
import subprocess, shlex
from threading import Thread
from optparse import OptionParser, make_option
import sys
import time

from constant.system_error import *
from constant.system_define import *

class LED_UTILS(object):
    
    def __init__(self, mainloop):
        self.mainloop = mainloop
        self.on_flag = False
        self.spin_speed = 0
        self.threshold = BLE_INDICATOR_THRESHOLD
        self.thread_flag = True
        self.thread = None
        
        # Led Indicator of Operation
        def indicator_service_thread(waitsec=1):
            try:
                logging.info("Indicator Thread running...")
                
                check_to_stop = self.get_threshold()
                last_event = False
                self.turn_off()
                # Loop to check Events
                # False --> True: On, -- False in 5s.
                # True --> False: Off, -- False
                #*True --> True: Count down -- False
                # False --> False: Idle
                #*False --> True:
                while self.thread_flag:
                    # Get flag
                    event = self.get_state()    
                    if (event == True and last_event == True):
                        # On count down to off
                        time.sleep(waitsec)
                        if check_to_stop > 0:
                            check_to_stop -= 1
                        else:
                            logging.info("LED ON: count down empty")
                            self.set_state(False)
                        continue
                    elif (event == False and last_event == False):
                        # Idle sleep
                        time.sleep(waitsec)
                        continue
                    # else (event == True and last_event == False) or (event == False and last_event == True):
                    
                    last_event = event
                    check_to_stop = self.get_threshold()
                    if event == True:
                        # On state
                        logging.info("LED spin_on: ON state")
                        if self.spin_on() != 0:
                            logging.error("LED: spin_on failed!")
                            raise Exception("LED IO error")
                        # Clear current state
                        logging.info("LED clear: ON state")
                        self.set_state(False)
                        # Idle sleep
                        time.sleep(self.get_threshold())
                    else:
                        logging.info("LED turn_off: OFF state")
                        if self.turn_off() != 0:
                            logging.error("LED turn_off: failed!")
                            raise Exception("LED IO error")
                        last_event = False
            except Exception as e:
                logging.error("Exception on Indicator Thread: %s" %(e))
                self.mainloop.quit()
            
            logging.info("Indicator Thread Exiting...")
    
        # create indicator server:
        # indicator_thread = Thread(target= lambda: indicator_service_thread())
        # self.thread = indicator_thread
        # indicator_thread.daemon = True
        # indicator_thread.start()
        # logging.info("Create Indicator Thread done!")
        
    def set_state(self, state):
        logging.info("LED set_state: %s" %state)
        self.on_flag = state
        if state == True:
            self.spin_speed += 1
        else:
            self.spin_speed -= 1
    
    def get_state(self):
        return self.on_flag
    
    def set_threshold(self, interval_sec):
        logging.info("LED set_threshold: %d s" %interval_sec)
        self.threshold = interval_sec
    
    def get_threshold(self):
        return self.threshold
    
    def command(self, cmd):
        logging.info("LED command: %s" %(cmd))
        return subprocess.call(cmd, shell=True)
    
    def spin_on(self):
        # Purple spin
        retval = ESYSNOERR
        if self.spin_speed > 2:
            retval = self.command("echo 'P 1500 2 0 0' > /proc/titan_spin")
        elif self.spin_speed > 1:
            retval = self.command("echo 'P 2500 2 0 0' > /proc/titan_spin")
        else:
            retval = self.command("echo 'P 3500 2 0 0' > /proc/titan_spin")
        return retval
    
    def turn_off(self):
        # Turn off
        return self.command("echo '0 P 0 0' > /proc/titan_turn")

    def setThreadFlag(self, state):
        self.thread_flag = state

    def getThread(self):
        return self.thread