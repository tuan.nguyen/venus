from __future__ import print_function
from threading import Thread
import datetime,time
import dbus
import dbus.glib
import dbus.exceptions
import dbus.mainloop.glib,gobject
import dbus.service
import signal
import logging
import sys
import argparse
from lib.bluetooth import bluezutils

try:
    from gi.repository import GObject
except ImportError:
    import gobject as GObject

from constant.system_error import *
from constant.system_define import *
from lib.parser.csv_parser import *
from lib.secure.uuid_utils import *
from lib.secure.rsa_utils import *
from lib.secure.aes_utils import *
from lib.secure.diffiehellman import *
from lib.network.uci_utils import *
from lib.bluetooth.ble_utils import *
from lib.indicator.led_utils import *
from lib.bluetooth import pairing

from lib.bluetooth import advertising
from lib.bluetooth import connection
from lib.bluetooth import cmdpoint
from lib.bluetooth import streampoint
from lib.bluetooth import gatt_server

class BLE_MAIN(object):
    def __init__(self):
        self.adapter_name = None
        self.address = '00:00:00:00:00:00'
        self.adapterManager = None
        self.serviceManager = None
        self.dbus = None
        self.service_index = 0
        self.threads = []
        # Manage Class
        self.host_util = BLE_UTILS()
        self.uutil = UUID_UTILS()
        self.csvp = CSV_PARSER()
        self.indicator = None
        self.pair = None
        self.cntth = None
        self.cmdth = None
        self.gatt = None
        self.cntsrv = None
        self.cmdsrv = None
        self.strsrv = None
        self.advrt = None
        self.mainloop = None
        self.mflag = BLE_FLG_NONE
        # Device
        self.devstate = BLE_DEV_STATE_UNCONFIGURE
        # Security
        self.dh = None
        self.dh_aes_key = None
        self.aes = None

    def setflag(self, flag):
        self.mflag = flag
        logging.info("setflag(): %d" %(self.mflag))

    def getflag(self):
        logging.info("getflag(): %d" %(self.mflag))
        return self.mflag

    def writePidFile(self):
        pid = str(os.getpid())
        f = open(BLE_PID_FILE,'w')
        f.write(pid)
        f.close()

# Function for Handling Connection/Service BLE
def main_device_initialization(ble):
    
    try:
        """
        Create DH Keys
        """
        ble.dh = Private()
        # ble.dh.writefilekey(BLE_SYSTEM_DFHM_PUBLIC_RECEIVE, ble.dh.getpublickey())
        # ble.dh.writefilekey(BLE_SYSTEM_DFHM_PRIVATE_RECEIVE, ble.dh.getprivatekey())
        """
        Create AES with temporary Keys
        """
        # ble.dh_aes_key = ble.csvp.param_get(BLE_SYSTEM_CSV, BLE_CIPHER_KEY)
        # if ble.dh_aes_key == None:
        #     logging.error("CSV %s: not available!" %(BLE_CIPHER_KEY))
        #     dh_aes_key = 'xHDTCFGKFXwKgYQ54oV7ntnpBcPF730uXHWDEUeJllE='
        ble.aes = AES_UTILS(os.urandom(32))

        csvp = ble.csvp
        uutil = ble.uutil
        if BLE_UUID_AUTO_GENERATION == 1:
            logging.info("UUIDs Generated!")
            # Auto Generate Connection UUIDs
            if csvp.param_update(BLE_SYSTEM_CSV, BLE_CONNECTION_SERV_UUID, uutil.uuid128_gen()) != 0:
                logging.error("Update CSV %s: failed!" %(BLE_CONNECTION_SERV_UUID))
                return ESYSCSVIO
            if csvp.param_update(BLE_SYSTEM_CSV, BLE_CONNECTION_DESC_UUID, uutil.uuid128_gen()) != 0:
                logging.error("Update CSV %s: failed!" %(BLE_CONNECTION_DESC_UUID))
                return ESYSCSVIO
            if csvp.param_update(BLE_SYSTEM_CSV, BLE_CONNECTION_CHAR_UUID, uutil.uuid128_gen()) != 0:
                logging.error("Update CSV %s: failed!" %(BLE_CONNECTION_CHAR_UUID))
                return ESYSCSVIO
             
            # Auto Generate Commandpoint UUIDs
            if csvp.param_update(BLE_SYSTEM_CSV, BLE_COMMAPOINT_SERV_UUID, uutil.uuid128_gen()) != 0:
                logging.error("Update CSV %s: failed!" %(BLE_COMMAPOINT_SERV_UUID))
                return ESYSCSVIO
            if csvp.param_update(BLE_SYSTEM_CSV, BLE_COMMAPOINT_DESC_UUID, uutil.uuid128_gen()) != 0:
                logging.error("Update CSV %s: failed!" %(BLE_COMMAPOINT_DESC_UUID))
                return ESYSCSVIO
            if csvp.param_update(BLE_SYSTEM_CSV, BLE_COMMAPOINT_CHAR_UUID, uutil.uuid128_gen()) != 0:
                logging.error("Update CSV %s: failed!" %(BLE_COMMAPOINT_CHAR_UUID))
                return ESYSCSVIO
             
            # # Auto Generate Streampoint UUIDs
            # if csvp.param_update(BLE_SYSTEM_CSV, BLE_STREAPOINT_SERV_UUID, uutil.uuid128_gen()) != 0:
            #     logging.error("Update CSV %s: failed!" %(BLE_STREAPOINT_SERV_UUID))
            #     return ESYSCSVIO
            # if csvp.param_update(BLE_SYSTEM_CSV, BLE_STREAPOINT_DESC_UUID, uutil.uuid128_gen()) != 0:
            #     logging.error("Update CSV %s: failed!" %(BLE_STREAPOINT_DESC_UUID))
            #     return ESYSCSVIO
            # if csvp.param_update(BLE_SYSTEM_CSV, BLE_STREAPOINT_CHAR_UUID, uutil.uuid128_gen()) != 0:
            #     logging.error("Update CSV %s: failed!" %(BLE_STREAPOINT_CHAR_UUID))
            #     return ESYSCSVIO
        else:
            logging.info("UUIDs Fixed!")
        
        """
        Init Advertising Service
        """
        connection_uuid = csvp.param_get(BLE_SYSTEM_CSV, BLE_CONNECTION_SERV_UUID)
        if not connection_uuid:
            logging.error(BLE_CONNECTION_SERV_UUID + ': not found!')
            return ESYSCSVNA

        ble.address = ble.host_util.host_getaddr()
        ble.advrt = advertising.advertisingMain(ble, connection_uuid, ble.address)
    
        """
        Init GATT Server
        """
        ble.gatt = gatt_server.Application(ble.dbus)
 
        """
        Start GATT Connection
        """
        ble.cntsrv = connection.connectServerRegister(ble)

        """
        Add GATT Command Point
        """
        ble.cmdsrv = cmdpoint.cmdpointAddService(ble)
     
        # """
        # # Add GATT Streaming Point
        # # """
        # ble.strsrv = streampoint.streampointAddService(ble)
    
        """
        Register GATT Server
        """
        gatt_server.gattServerRegister(ble)
        
        """
        Register Pair Service
        """
        ble.pair = pairing.agentPairingServiceRegister(ble)
    except Exception as e:
        logging.error("initialize(): exception: %s" %(e))

    # create thread to service connection:
    try:
        ble.cntth = connection.ConnectionThread(ble)
        ble.cntth.daemon = True
        ble.cntth.start()
        ble.threads.append(ble.cntth)
        logging.info("Create Connection Thread done!")
    except Exception as e:
        logging.error("Create Connection Thread failed!: %s" %(e))
        return EEXCEPTION
    
    # create thread to service command:
    try:
        ble.cmdth = cmdpoint.CommandpointThread(ble)
        ble.cmdth.daemon = True
        logging.info("Create Secured Commandpoint Thread done!")
        ble.cmdth.start()
        ble.threads.append(ble.cmdth)
    except Exception as e:
        logging.error("Create Secured Commandpoint Thread failed!: %s" %(e))
        return EEXCEPTION
    
    # Successful Init
    return ESYSNOERR

def main():

    # bluetoothd = subprocess.Popen("ps | grep \"bluetoothd\" | grep -v grep | awk \'{print $1}\'", 
    #                       shell=True, stdout=subprocess.PIPE).communicate()[0].rstrip()
    # if bluetoothd:
    #     cmd="kill %s"%bluetoothd
    #     os.system(cmd)
    #     time.sleep(5)

    # os.system("/usr/bin/bluetoothd -n -C -E -d &");
    # time.sleep(5)

    """
    Init logfile
    """
    logging.basicConfig(filename='/system.log', level=logging.DEBUG,
                        format=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)
    logging.info('')
    logging.info('Started ' + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    # Enable print out
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    # create formatter
    formatter = logging.Formatter(LOG_FORMAT, LOG_DATE_FORMAT)
    ch.setFormatter(formatter)
    root.addHandler(ch)

    """
    Init Main Class
    """
    ble = BLE_MAIN()

    """
    Get arguments input
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--adapter-name', type=str, help='Adapter name', default='')
    args = parser.parse_args()
    ble.adapter_name = args.adapter_name

    """
    Install SIGNAL
    """
    def main_cleanup():
        logging.info("main_cleanup()")
        logging.info("--- Threads Clean-up ---")
        # Notify stop

        # pairing.disconnectCurrentDev(ble)

        if ble.indicator != None:
            ble.indicator.setThreadFlag(False)
        if ble.pair != None:
            ble.pair.setThreadFlag(False)
            pairing.agentUnregister(ble)
        if ble.cntth != None:
            ble.cntth.setThreadFlag(False)
        if ble.cmdth != None:
            ble.cmdth.setThreadFlag(False)
        # Wait to stop completion
        if ble.indicator != None:
            ble.indicator.getThread().join()
        if ble.pair != None:
            ble.pair.getThread().join()
        if ble.cntth != None:
            ble.cntth.join()
        if ble.cmdth != None:
            ble.cmdth.join()

        logging.info("--- Unregister Gatt Server ---")
        if ble.gatt != None:
            gatt_server.gattServerUnregister(ble)
        logging.info("--- Unregister Advertisement ---")
        if ble.advrt != None:
            advertising.advertisingUnregister(ble.advrt)

        # Check to RESTART or REBOOT before really quit
        if (ble.mflag & BLE_FLG_RESTART) == BLE_FLG_RESTART:
            logging.info("Restarting application...")
            #time.sleep(3)
            ble.setflag(BLE_FLG_NONE)
            #ble.mainloop.quit()
            #ble.mainloop = None
            os.execv(sys.executable, ['python'] + sys.argv)
            time.sleep(1)
        elif (ble.mflag & BLE_FLG_REBOOT) == BLE_FLG_REBOOT:
            logging.info("Rebooting system...")
            ble.setflag(BLE_FLG_NONE)
            os.system('reboot')
            time.sleep(1)
        else:
            logging.info("--- Mainloop finished ---")
            ble.mainloop.quit()
            ble.mainloop = None

    signal.signal(signal.SIGTERM, main_cleanup)

    try:

        """
        DBus Main Loop Init
        """
        #bucket = Queue.Queue()
        gobject.threads_init()
        dbus.glib.init_threads()
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        ble.dbus = dbus.SystemBus()
        ble.mainloop = GObject.MainLoop()

        """
        Init global class pointer
        """
        # ble.indicator = LED_UTILS(ble.mainloop)

        """
        Init GATT Service Server
        """
        ble.serviceManager = gatt_server.gattServerInit(ble)
        
        """
        Check Device status:
        """
        ble.devstate =  UCI_UTILS().getDevState()
        if ble.devstate <= BLE_DEV_STATE_VALIDATED:
            logging.info(BLE_DEFAULT_NAME + ': Insecured!')
            # Auto Generate Partner UUIDs
            if ble.csvp.param_update(BLE_SYSTEM_CSV, BLE_PARTNER_UUID, ble.uutil.uuid128_gen()) != 0:
                logging.error("Update CSV %s: failed!" %(BLE_PARTNER_UUID))
                return ESYSCSVIO
            retval = main_device_initialization(ble)
            if retval != ESYSNOERR:
                logging.error(BLE_DEFAULT_NAME + ' Insecured: initialized failed!')
                return EEXCEPTION
        elif ble.devstate > BLE_DEV_STATE_VALIDATED:
            logging.info(BLE_DEFAULT_NAME + ': Secured!')
            retval = main_device_initialization(ble)
            if retval != ESYSNOERR:
                logging.error(BLE_DEFAULT_NAME + ' Connected: initialized failed!')
                return EEXCEPTION
        else:
            logging.info(BLE_DEFAULT_NAME + ': Unknown state!')
    except Exception as e:
        logging.error("maininit(): exception:")
        logging.error(e)
        sys.exit(1)
        
    try:
        """
        Main Loop
        """
        logging.info("Start mainloop()")
        ble.mainloop.run()
        #while True:
        #    time.sleep(0.10)
        #logging.info('Finished')
    except KeyboardInterrupt:
        # ignore exception as it is a valid way to exit the program
        # and skip to finally clause
        pass
    except Exception as e:
        logging.info("mainloop(): exception:")
        logging.error(e)
    finally:
        logging.info("mainloop(): finally:")
        main_cleanup()
            
if __name__ == '__main__':
    main()
