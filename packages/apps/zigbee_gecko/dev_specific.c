#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "timer.h"
#include "VR_define.h"
#include "slog.h"
#include "verik_utils.h"
#include "database.h"
#include "dev_specific.h"
#include "manage_adding.h"

extern sqlite3 *support_devs_db;
extern sqlite3 *zigbee_db;
pthread_mutex_t g_dev_specific_info_listMutex;
device_specific_t g_dev_specific_list;

void init_dev_specific_list()
{
    VR_INIT_LIST_HEAD(&g_dev_specific_list.list);
    pthread_mutex_init(&g_dev_specific_info_listMutex, 0);
}

static int add_new_dev_specific_to_list(void *node_add)
{
    int res = 0;
    pthread_mutex_lock(&g_dev_specific_info_listMutex);
    device_specific_t *input = (device_specific_t *)node_add;
    device_specific_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &(g_dev_specific_list.list), list)
    {
        if(!strcmp(tmp->deviceId, input->deviceId))
        {
            res = 1;
            break;
        }
    }

    if(!res)
    {
        VR_(list_add_tail)(&(input->list), &(g_dev_specific_list.list));
    }

    pthread_mutex_unlock(&g_dev_specific_info_listMutex);
    return res;
}

static device_specific_t *get_dev_specifiec_from_id(char *id)
{
    if(!id)
    {
        return NULL;
    }

    device_specific_t *tmp = NULL;
    VR_(list_for_each_entry)(tmp, &g_dev_specific_list.list, list)
    {
        if(!strcmp(tmp->deviceId, id))
        {
            return tmp;
        }
    }
    return NULL;
}

static void free_dev_specific(device_specific_t *addingNode)
{
    if(!addingNode)
    {
        return;
    }

    SAFE_FREE(addingNode->deviceId);
    SAFE_FREE(addingNode->serialId);
    SAFE_FREE(addingNode->deviceSpecific);
    SAFE_FREE(addingNode);
}

void insert_specific_dev_list(char *deviceId, char *serialId)
{
    if(!deviceId || !serialId)
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(device_specific);
    searching_database("zwave_handler", support_devs_db, zwave_cb, &device_specific,
                        "SELECT DeviceSpecific from SUPPORT_DEVS where SerialID='%s'",
                        serialId);
    if(!device_specific.len)
    {
        goto addingDone;
    }

    device_specific_t *addingNode = NULL;
    addingNode = (device_specific_t *)(malloc)(sizeof(device_specific_t));
    memset(addingNode, 0x00, sizeof(device_specific_t));
    addingNode->deviceSpecific = (char *)malloc(device_specific.len +1);
    strcpy(addingNode->deviceSpecific, device_specific.value);

    addingNode->deviceId = (char *)malloc(strlen(deviceId) + 1);
    strcpy(addingNode->deviceId, deviceId);
    
    addingNode->serialId = (char *)malloc(strlen(serialId) + 1);
    strcpy(addingNode->serialId, serialId);
    
    if(add_new_dev_specific_to_list(addingNode))
    {
        free_dev_specific(addingNode);
    }
addingDone:
    FREE_SEARCH_DATA_VAR(device_specific);
}

/*
    get meterScale into deviceSpecific
*/
void dev_specific_meter_scale(char *devId, int *aCVoltageMultiplier, int *aCVoltageDivisor, int *aCCurrentMultiplier, int *aCCurrentDivisor, int *aCPowerMultiplier, int *aCPowerDivisor)
{
    *aCVoltageMultiplier = 0;   *aCVoltageDivisor = 0;
    *aCCurrentMultiplier = 0;   *aCCurrentDivisor = 0;
    *aCPowerMultiplier = 0;     *aCPowerDivisor = 0;
    if(!devId)
    {
        return;
    }

    device_specific_t *dev = get_dev_specifiec_from_id(devId);
    if(!dev)
    {
        return;
    }

    if(!dev->deviceSpecific)
    {
        return;
    }

    json_object *specificObject = VR_(create_json_object)(dev->deviceSpecific);
    if(!specificObject)
    {
        return;
    }

    CHECK_JSON_OBJECT_EXIST(meterScaleObj, specificObject, ST_METER_SCALE, done);
    CHECK_JSON_OBJECT_EXIST(ACVoltageMultiplierObj, meterScaleObj, ST_AC_VOLTAGE_MULTIPLIER, done);
    CHECK_JSON_OBJECT_EXIST(ACVoltageDivisorObj, meterScaleObj, ST_AC_VOLTAGE_DIVISOR, done);
    CHECK_JSON_OBJECT_EXIST(ACCurrentMultiplier, meterScaleObj, ST_AC_CURRENT_MULTIPLIER, done);
    CHECK_JSON_OBJECT_EXIST(ACCurrentDivisor, meterScaleObj, ST_AC_CURRENT_DIVISOR, done);
    CHECK_JSON_OBJECT_EXIST(ACPowerMultiplier, meterScaleObj, ST_AC_POWER_MULTIPLIER, done);
    CHECK_JSON_OBJECT_EXIST(ACPowerDivisor, meterScaleObj, ST_AC_POWER_DIVISOR, done);
    *aCVoltageMultiplier = json_object_get_int(ACVoltageMultiplierObj);
    *aCVoltageDivisor = json_object_get_int(ACVoltageDivisorObj);
    *aCCurrentMultiplier = json_object_get_int(ACCurrentMultiplier);
    *aCCurrentDivisor = json_object_get_int(ACCurrentDivisor);
    *aCPowerMultiplier = json_object_get_int(ACPowerMultiplier);
    *aCPowerDivisor = json_object_get_int(ACPowerDivisor);

done:
    json_object_put(specificObject);
    return;
}

/*
    get sensorDiv into deviceSpecific
*/
void dev_specific_sensor_div(char *devId, int *sensorScale)
{
    *sensorScale = 0;
    if(!devId)
    {
        return;
    }

    device_specific_t *dev = get_dev_specifiec_from_id(devId);
    if(!dev)
    {
        return;
    }

    if(!dev->deviceSpecific)
    {
        return;
    }

    json_object *specificObject = VR_(create_json_object)(dev->deviceSpecific);
    if(!specificObject)
    {
        return;
    }

    CHECK_JSON_OBJECT_EXIST(sensorDivObj, specificObject, ST_SENSOR_SCALE, done);

    *sensorScale = json_object_get_int(sensorDivObj);

done:
    json_object_put(specificObject);
    return;
}

bool dev_specific_change_channel_report(char *devId, char *changeChannel)
{
    if(!devId)
    {
        return false;
    }

    device_specific_t *dev = get_dev_specifiec_from_id(devId);
    if(!dev)
    {
        return false;
    }

    if(!dev->deviceSpecific)
    {
        return false;
    }

    json_object *specificObject = VR_(create_json_object)(dev->deviceSpecific);
    if(!specificObject)
    {
        return false;
    }

    CHECK_JSON_OBJECT_EXIST(changeChannelObj, specificObject, ST_CHANNEL_REPORT, done);

    const char * temp = json_object_get_string(changeChannelObj);;
    strncpy(changeChannel, temp, strlen(temp));
    json_object_put(specificObject);
    return true;
done:
    json_object_put(specificObject);
    return false;
}

uint8_t get_maximum_user_code(char *devId)
{
    int maxUser = MAXIMUM_USER_CODE_ID_DEFAULT;
    if(!devId)
    {
        return maxUser;
    }

    device_specific_t *dev = get_dev_specifiec_from_id(devId);
    if(!dev)
    {
        return maxUser;
    }

    if(!dev->deviceSpecific)
    {
        return maxUser;
    }

    json_object *specificObject = VR_(create_json_object)(dev->deviceSpecific);
    if(!specificObject)
    {
        return maxUser;
    }

    CHECK_JSON_OBJECT_EXIST(userCodeObj, specificObject, ST_USER_CODE_AUTO_SET, getUserDone);
    CHECK_JSON_OBJECT_EXIST(maxUserObj, userCodeObj, ST_SUPPORTED_USERS, getUserDone);
    maxUser = json_object_get_int(maxUserObj);

getUserDone:
    return maxUser;
}