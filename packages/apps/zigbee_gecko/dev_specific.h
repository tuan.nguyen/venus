#ifndef _DEVICE_SPECIFIC_H_
#define _DEVICE_SPECIFIC_H_
#include <stdbool.h>
#include "VR_list.h"

#define ST_INVERT_STATUS "invertStatus"
#define ST_SINGLE_VALUE  "singleValue"

#define ST_USER_CODE_AUTO_SET  "userCodeAutoSet"

typedef struct _device_specific
{
	char *deviceId;
    char *serialId;
    char *deviceSpecific;
    long timerPooling;
    struct VR_list_head list;
}device_specific_t;

void init_dev_specific_list();
void insert_specific_dev_list(char *deviceId, char *serialId);
void dev_specific_meter_scale(char *devId, int *aCVoltageMultiplier, int *aCVoltageDivisor, int *aCCurrentMultiplier, int *aCCurrentDivisor, int *aCPowerMultiplier, int *aCPowerDivisor);
void dev_specific_sensor_div(char *devId, int *sensorScale);
bool dev_specific_change_channel_report(char *devId, char *changeChannel);
uint8_t get_maximum_user_code(char *devId);

#endif