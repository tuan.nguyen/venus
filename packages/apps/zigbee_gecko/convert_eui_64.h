#ifndef _CONVERT_EUI_64_H_
#define _CONVERT_EUI_64_H_
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define ZIGBEE_TABLE_SIZE 500

typedef struct _convertEUI64Item_s {
   char* appDeviceId;   
   int key;
}convertEUI64Item_t;

convertEUI64Item_t *searchEUI64(char* stringKey);
void insertEUI64(char* stringKey, char *appDeviceId);
bool deleteEUI64(char* stringKey);
void initHashTableEUI64(void);
void freeHashTableEUI64(void);
#endif