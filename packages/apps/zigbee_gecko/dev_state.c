#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "dev_state.h"
#include "VR_define.h"
#include "slog.h"
#include "attribute-id.h"

/*#####################################################################################*/
/*###########################               METER             #########################*/
/*#####################################################################################*/
source_select_t electric_meter[]=
{
    {ZCL_ACTIVE_POWER_ATTRIBUTE_ID,                  ST_METER_W},
    {ZCL_RMS_VOLTAGE_ATTRIBUTE_ID,                   ST_METER_V},
    {ZCL_RMS_CURRENT_ATTRIBUTE_ID,                   ST_METER_A},
};

/*#####################################################################################*/
/*###########################               DOOR LOCK             #########################*/
/*#####################################################################################*/
doorLockAlarmPattern_t doorLockAlarmPattern[]=
{
    {0x0000,                    ST_DOOR_RF_UNLOCKED},
    {0x0002,                    ST_KEYPAD_UNLOCK},
    {0x0001,                    ST_KEYPAD_LOCK},
    {0x020E,                    ST_MANUAL_UNLOCK},
    {0x020D,                    ST_MANUAL_LOCK},
    {0x0102,                    ST_DOOR_RF_UNLOCKED},
    {0x0101,                    ST_DOOR_RF_LOCKED},
    {0x020A,                    ST_AUTO_LOCK},
    {0x0009,                    ST_KEY_UNLOCK},
    {0x0008,                    ST_KEY_LOCK},
    {0x0207,                    ST_ONE_TOUCH_LOCK},
};
static const char* get_name_from_id(uint16_t id, source_select_t table[], int table_len)
{
    int i;

    for(i = 0; i < table_len; i++)
    {
        if(table[i].id == id)
        {
            return table[i].name;
        }
    }

    return ST_UNKNOWN;
}

const char* getAlarmPatternFromEvent(uint16_t event)
{
    int i;

    for(i = 0; i < sizeof(doorLockAlarmPattern)/sizeof(doorLockAlarmPattern_t); i++)
    {
        if(doorLockAlarmPattern[i].event == event)
        {
            return doorLockAlarmPattern[i].alarmPattern;
        }
    }

    return ST_UNKNOWN;
}
//need free output after using
void get_value_bit_mode(uint16_t mask[], uint8_t mask_len, 
                      source_select_t table[], uint8_t table_len,
                      char **output)
{
    int i;

    char *result = (char *)malloc(1);
    result[0] = '\0';

    for(i = 0; i < mask_len; i++)
    {

        SLOGI("id  = 0x%04X\n", mask[i]);
        const char *mode = get_name_from_id(mask[i], table, table_len);
        SLOGI("mode  = %s\n", mode);
        char *new_result = (char*)realloc(result, strlen(result)+strlen(mode)+32);
        if(!new_result)
        {
            SLOGE("Error (re)allocating memory\n");
            *output = result;
            return;
        }

        result = new_result;
        size_t result_len = strlen(result);
        if(!result_len)
        {
            sprintf(result, "%s", mode);
        }
        else
        {
            sprintf(result+result_len, ",%s", mode);
        }
    }

    *output = result;
}

/*return meter name parse from meter ID
output is list of unit in character of meter ID*/
const char *get_meter_scale_from_attribute_mask(uint16_t mask[], uint8_t mask_len, char **output)
{
    get_value_bit_mode(mask, mask_len, electric_meter, sizeof(electric_meter)/sizeof(source_select_t), output);
    return ST_UNKNOWN;
}

int16_t getDataFromAttribute(uint16_t attributeId, uint16_t listAttr[], int16_t listData[], uint8_t len)
{
    uint8_t i;

    for(i = 0; i < len; i++)
    {
        if(listAttr[i]== attributeId)
        {
            return listData[i];
        }
    }

    return 0;    
}