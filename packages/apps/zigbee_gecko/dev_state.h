#ifndef _DEV_STATE_H_
#define _DEV_STATE_H_

typedef struct source_select
{
   uint16_t id;
   const char *name;
}source_select_t;

typedef struct _doorLockAlarmPattern
{
   uint16_t event;
   const char *alarmPattern;
}doorLockAlarmPattern_t;
const char *get_meter_scale_from_attribute_mask(uint16_t mask[], uint8_t mask_len, char **output);
int16_t getDataFromAttribute(uint16_t attributeId, uint16_t listAttr[], int16_t listData[], uint8_t len);
const char* getAlarmPatternFromEvent(uint16_t event);
#endif