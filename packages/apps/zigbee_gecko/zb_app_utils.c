#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <getopt.h>
#include <unistd.h>
#include <json-c/json.h>
#include "zb_app_utils.h"
#include "slog.h"

#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))
#define DEFAULT_HSL_COLLOR 0xFE
int RgbToHsl(uint8_t r_int, uint8_t g_int, uint8_t b_int, uint8_t *h_int, uint8_t *s_int, uint8_t *l_int) // Alpha value is simply passed through
{

    double R, G, B;
    double maxv, minv;
    double h, s, d, l;

    R = r_int;
    G = g_int;
    B = b_int;
    R /= 0xFF;
    G /= 0xFF;
    B /= 0xFF;

    //printf("R:G:B=%f:%f:%f\n",R,G,B);
    maxv = max(max(R, G), B);
    minv = min(min(R, G), B);
    h = 0;
    s = 0;
    d = maxv - minv;
    l = (maxv + minv) / 2;

    if (maxv != minv)
    {
        s = l > 0.5 ? d / (2 - maxv - minv) : d / (maxv + minv);
        if (maxv == R)
        {
            h = (G - B) / d + (G < B ? 6 : 0);
        }
        else if (maxv == G)
        {
            h = (B - R) / d + 2;
        }
        else if (maxv == B)
        {
            h = (R - G) / d + 4;
        }
        h /= 6;
    }

    printf("h:s:l = %f:%f:%f\n", h, s, l);

    *h_int = (unsigned char)(h * DEFAULT_HSL_COLLOR);
    *s_int = (unsigned char)(s * DEFAULT_HSL_COLLOR);
    *l_int = (unsigned char)(l * DEFAULT_HSL_COLLOR);
    return 0;
}
uint32_t htoi(const char *ptr)
{
    uint32_t value = 0;
    if (!ptr)
    {
        return value;
    }

    char ch = *ptr;

    while (ch == ' ' || ch == '\t')
        ch = *(++ptr);

    for (;;)
    {
        if (ch >= '0' && ch <= '9')
            value = (value << 4) + (ch - '0');
        else if (ch >= 'A' && ch <= 'F')
            value = (value << 4) + (ch - 'A' + 10);
        else if (ch >= 'a' && ch <= 'f')
            value = (value << 4) + (ch - 'a' + 10);
        else
        {
            return value;
        }

        ch = *(++ptr);
    }
}

uint64_t htoi64 (const char *ptr) 
{
    uint64_t value = 0;
    char ch = *ptr;
    
    while (ch == ' ' || ch == '\t')
        ch = *(++ptr);

    for (;;) {
        if (ch >= '0' && ch <= '9')
            value = (value << 4) + (ch - '0');
        else if (ch >= 'A' && ch <= 'F')
            value = (value << 4) + (ch - 'A' + 10);
        else if (ch >= 'a' && ch <= 'f')
            value = (value << 4) + (ch - 'a' + 10);
        else
        {
            return value;
        }
            
        ch = *(++ptr);
    }
}

// need free after call
char *get_zigbee_controller_version(void)
{
    char *version = NULL;
    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));
    pzbParam->miscData = malloc(SIZE_256B);
    pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_GET_VERSION_FIRMWARE;
    zigbeeGeckoHandlerSendCommand(pzbParam);
    if(!pzbParam->ret)
    {
        version = strdup(pzbParam->miscData);
    }
    free(pzbParam->miscData);
    free(pzbParam);
    return version;
}

int zigbee_update_firmwre(char *path)
{
    int ret = -1;
    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));
    pzbParam->miscData = (void *)path;
    pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_UPDATE_FIRMWARE;
    zigbeeGeckoHandlerSendCommand(pzbParam);
    if(pzbParam->ret)
    {
        SLOGE("update zigbee controller failed, try again\n");
        sleep(1);
        zigbeeGeckoHandlerSendCommand(pzbParam);
    }
    ret = pzbParam->ret;
    free(pzbParam);
    return ret;
}