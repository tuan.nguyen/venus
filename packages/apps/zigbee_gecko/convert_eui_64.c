#include "convert_eui_64.h"
#include "zb_app_utils.h"

convertEUI64Item_t *hashArrayEUI64[ZIGBEE_TABLE_SIZE];
//key: realId
//appDeviceId: fullserial = endpoint + eui64
int hashCode(int key)
{
    return key % ZIGBEE_TABLE_SIZE;
}

void initHashTableEUI64(void)
{
    int i = 0;
    for (i = 0; i < ZIGBEE_TABLE_SIZE; i++)
    {
        hashArrayEUI64[i] = NULL;
    }
}
/*
   By given key returns a pointer to a DataItem with the same key
   Input: char* stringKey value
   Output: If key found - the pointer to a DataItem else NULL
*/
convertEUI64Item_t *searchEUI64(char* stringKey)
{
    int key = htoi(stringKey);
    //get the hash
    int hashIndex = hashCode(key);
    //check a loop
    int count = 0;
    //move in array until an empty
    while (hashArrayEUI64[hashIndex] != NULL)
    {

        if (hashArrayEUI64[hashIndex]->key == key)
        {
            printf("(Hash-Table) Key (%06X) is found from hash-table\n", key);
            return hashArrayEUI64[hashIndex];
        }

        //go to next cell
        ++hashIndex;

        //wrap around the table
        hashIndex %= ZIGBEE_TABLE_SIZE;
        //check one loop
        if (++count == ZIGBEE_TABLE_SIZE)
        {
            break;
        }
    }

    return NULL;
}

/*
   Adding a DataItem to a hashArray
   Input: char* stringKey value, char* (string) data value
   Output: None
*/
void insertEUI64(char* stringKey, char *appDeviceId)
{
   if(!appDeviceId)
   {
         return;
   }
   
   int key = htoi(stringKey);
   convertEUI64Item_t *tmpItem = searchEUI64(stringKey);

   if (tmpItem)
   {
         printf("(Hash-Table) Duplicate!!!, key: %06X\n", key);
         return;
   }
   convertEUI64Item_t *item = (convertEUI64Item_t *)malloc(sizeof(convertEUI64Item_t));
   item->appDeviceId = strdup(appDeviceId);
   item->key = key;
   //get the hash
   int hashIndex = hashCode(key);
   //check a loop
   int count = 0;
   //move in array until an empty or deleted cell
   while (hashArrayEUI64[hashIndex] != NULL && hashArrayEUI64[hashIndex]->key != -1)
   {
        //go to next cell
        ++hashIndex;

        //wrap around the table
        hashIndex %= ZIGBEE_TABLE_SIZE;
        //check one loop
        if (++count == ZIGBEE_TABLE_SIZE)
        {
           free(item->appDeviceId);
           free(item);
           return;
        }
   }
   printf("(Hash-Table) Key (%06X), \t Data (%s) \t is inserted to hash-table\n", key, appDeviceId);
   hashArrayEUI64[hashIndex] = item;
}

/*
   Deleting a DataItem node from an hash array
   Input: char* stringKey we want to delete
   Output: true if Item is deleted
*/
bool deleteEUI64(char* stringKey)
{
    int key = htoi(stringKey);
    //get the hash
    int hashIndex = hashCode(key);
    //check a loop
    int count = 0;
    //move in array until an empty
    while (hashArrayEUI64[hashIndex] != NULL)
    {

        if (hashArrayEUI64[hashIndex]->key == key)
        {
            printf("(Hash-Table) Key (%06X) is deleted from hash-table\n", key);
            //free item
            free(hashArrayEUI64[hashIndex]->appDeviceId);
            free(hashArrayEUI64[hashIndex]);
            //assign a dummy item at deleted position
            hashArrayEUI64[hashIndex] = NULL;
            return true;
        }

        //go to next cell
        ++hashIndex;

        //wrap around the table
        hashIndex %= ZIGBEE_TABLE_SIZE;
        //check one loop
        if (++count == ZIGBEE_TABLE_SIZE)
        {
            break;
        }
    }
    //can't delete item
    return false;
}

/*
   displaying an hash array
   Input: None
   Output: None
*/
void displayListEUI64()
{
    int i = 0;

    for (i = 0; i < ZIGBEE_TABLE_SIZE; i++)
    {

        if (hashArrayEUI64[i] != NULL)
            printf("(Hash-Table) key(%06X) -> data(%s)\n", hashArrayEUI64[i]->key, hashArrayEUI64[i]->appDeviceId);
        else
            printf("(Hash-Table) key(NULL) -> data(NULL)\n");
    }

    printf("\n");
}

/*
   Freeing an hash array
   Input: None
   Output: None
*/
void freeHashTableEUI64(void)
{
    printf("(Hash-Table) Free Hash Table\n");
    int i = 0;
    for (i = 0; i < ZIGBEE_TABLE_SIZE; i++)
    {
        if (hashArrayEUI64[i] != NULL)
        {
            free(hashArrayEUI64[i]->appDeviceId);
            free(hashArrayEUI64[i]);
        }
    }
}
