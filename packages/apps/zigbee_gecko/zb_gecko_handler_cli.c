#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <getopt.h>
#include <unistd.h>
#include <math.h>

#include <libubus.h>
#include <libubox/blobmsg_json.h>
#include <json-c/json.h>
#include <sys/stat.h> //check file exist ?

#include "utils.h"
#include "gpio_intf.h"
#include "slog.h"
#include "timer.h"
#include "timing.h"

#include "database.h"
#include "group_apis.h"
#include "verik_utils.h"
#include "vr_sound.h"
#include "vr_rest.h"
#include "pmortem.h"
#include "zb_app_utils.h"
#include "action_control.h"
#include "dev_specific.h"

#include "fix-bug-build-code.h"
#include "handle-command.h"
#include "cmd-handler.h"
#include "constants.h"
#include "include/zll-types.h"
#include "interface-zigbee-gecko-handler.h"
#include "pmortem.h"
#include "manage_adding.h"
#include "manage_usercode.h"
#include "attribute-id.h"
#include "dev_state.h"
#include "dev_specific.h"
#include "convert_eui_64.h"

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
/*failed status code*/
#define COMMAND_SUCESS 0
#define COMMAND_FAILED -1
#define GET_CAP_FAILED -2
#define GET_DEV_TYPE_FAILED -3
#define DEV_NOT_FOUND -4
#define DEV_UNACTIVATE -5
#define DEV_NOT_SUPPORT_CLUSTER -8
#define CONFIG_ID_NOT_FOUND -9
#define COMMAND_NOT_SUPPORT              -10
#define USER_CODE_NAME_INVALID           -11
#define USER_CODE_USERID_INVALID         -12

#define CAP_ID_SUPPORTED 1
#define CAP_ID_NOT_SUPPORTED 0
#define CAP_ID_NULL -1

#define ZIGBEE_CONSOLE_PORT_DEFAULT "/dev/ttyO4"
#define GPIO_PIN_CTRL_NUM 48
#define GPIO_PIN_RST_NUM 60
#define GPIO_PIN_BOOTM_NUM 45
#define ENUM_ERROR -1
#define ENUM_SUCCESS 0
#define MAX_OWNER_SIZE 64
#define MAX_SERIAL_SIZE 64
#define MAX_ID_SIZE 32
#define MAX_EP_SIZE 2
#define DEV_ID_SIZE 32
#define MAX_DEVICE_ID_SIZE 32
#define MAX_TMP_SIZE 16
#define MAX_SERIAL_ID_SIZE 256
#define MAX_CAPABILITY_DEVICE_SIZE 256
#define MAX_PROFILEID_SIZE 64
#define MAX_NODE_ID_BINDING_TABLE 256
#define PARENT_ENDPOINT_NUM "0"
#define PARENT_ID_NUM "0"
#define FIRST_CLUSTER 0
#define DEFAULT_PARENT_ENDPOINT_ID 0
#define ENUM_NO_ADDING_DEVICE 0
#define ENUM_ADDING_DEVICE 1
#define FIRST_POS_BIT_OF_HIGH_BYTE 8
#define NULL_ROTATE_NUM 0
#define ENABLE_SOUND_ALARM 1
#define DISABLE_SOUND_ALARM 0

//for Door State Attribute
#define DOOR_STATE_OPEN 0x00
#define DOOR_STATE_CLOSED 0x01
#define DOOR_STATE_ERROR_JAMMED 0x02
#define DOOR_STATE_ERROR_FORDED_OPEN 0x03
#define DOOR_STATE_ERROR_UNSPECIFIED 0x04

//on/off state
#define TURN_ON_VALUE 1
#define TURN_OFF_VALUE 0
#define ZIGBEE_MANUAL_LOCK_VALUE 0x020D
#define ZIGBEE_MANUAL_UNLOCK_VALUE 0x020E
#define DEFAULT_TRANSITION_TIME_LEVEL_WITH_ON_OFF 5 //0.5s
#define DEFAULT_MAX_VALUE_8_BIT 255
#define DEFAULT_MAX_PERCENT 100
#define HIGH_LIMIT_PERCENT 100
#define LOW_LIMIT_PERCENT 1
#define DEFAULT_TRANSITION_TIME_MOVE_TO_HUE_AND_SATURATION 10 //1s for control color
#define MAXIMUM_NODE_ASSSOCIATION_FOR_ONE_DEV 10
#define DEFAULT_INVALID_MANUFACTURER_CODE 0xFFFF
#define DEFAULT_LENGHT_CONFIG_SET_TIME 0x01
#define MANUFACTURER_CODE_MS_OCCUPANCY_SENSING 0xC2DF
#define SPECIFIC_ILLUMINANCE_SENSOR 100
#define INT16_T_DATA_DIV 100
#define INT16_T_DATA_ILLUMINANCE_SENSOR 10000
#define SLEEP_MILISECOND_UNIT 1000
#define BIT_CAPABILITY_POWER_SOURCE 0x04
#define MAX_DISCOVER_ATTRIBUTE 20
#define MAXIMUM_DEVICE_TIME_LIFE (3600 * 24 + 5 * 60) //24h+5mins
#define NO_ATTRIBUTE_ID_READ_CURRENT 3
#define NO_ATTRIBUTE_ID_SUPPORTED_GET 3
#define NO_ATTRIBUTE_ID_READ_POWER 3
#define NO_ATTRIBUTE_ID_READ_VOLT 3
#define DEFAULT_NO_ATTRIBUTE_ID 1
#define DEFAULT_ZIGBEE_SECURE_LEVEL 1
#define KELVIN_TO_COLOR_TEMPERATURE 1000000
//zigbee programmer, update firmware
//#define ZIGBEE_CONTROLLER_VERSION
//    TOSTRING(LASTEST_FIRMWARE_0_VERSION)
//    "." TOSTRING(LASTEST_FIRMWARE_0_MINOR) "." TOSTRING(LASTEST_FIRMWARE_0_PATCH)
#define ZIGBEE_CONTROLLER_VERSION TOSTRING(LASTEST_FIRMWARE_0_VERSION)
#define ZIGBEE_DEFAULT_FIRMWARE_PATH "/lib/firmware/zigbee"
#define ZIGBEE_RECV_FIRMWARE_DEFAULT_NAME "ncp-uart-rts-cts-use-with-serial-uart-btl-6.0.0.gbl"
#define ERROR_CAN_NOT_FOUND_BOOTLOADER 2
#define ERROR_CHECK_VERSION 1
#define MFG_SMARTSENSE_TEMP_HUMIDITY_SENSOR_NEW_FW          0x104E
#define MFG_SMARTSENSE_TEMP_HUMIDITY_SENSOR_OLD_FW          0xC2DF
#define STR_LEN_ZIGBEE_DEVICE_ID_OLD_VER                    0x04
#define STR_LEN_ZIGBEE_DEVICE_ID_NEW_VER                    0x06
#define STR_LEN_ZIGBEE_EUI_64_DEVICE_ID                     18
#define DEFAULT_LENGHT_WRITE_CONFIG 0x01
#define SHIFT_RIGHT_4_BYTE 0x10000
#define SET_PIN_CODE_DUPLICATE_USER_CODE 3
#define USER_ID_ALL_USER_CODE_DELETED 0xFFFF

#define CLUSTER_ID_LENGTH 4
#define ATTRIBUTE_ID_LENGTH 4
#define POS_SERIAL_INTO_APP_DEVICE_ID 2
#define LUMI_DOOR_WINDOWS_SENSOR_SERIAL_ID "Lumi LM-DOOR"

#define CHECK_ITEM_OBJECT_EXIST(_ID, _jumpAddress) \
        convertEUI64Item_t *itemAppId = searchEUI64(_ID); \
        if(itemAppId) \
        { \
            sprintf(_ID, "%s", itemAppId->appDeviceId); \
        } \
        else \
        { \
            goto _jumpAddress; \
        }
typedef enum //mode of zigbee device
{
    MODE_UNKNOW,
    MODE_NONLISTENING,
    MODE_ALWAYSLISTENING,
    MODE_FREQUENTLYLISTENING
}modeDeviceZigbee;

typedef enum //network policy
{
    NETWORK_POLICY_NODEID,
    NETWORK_POLICY_CLASS,
    NETWORK_POLICY_CMD,
    NETWORK_POLICY_DATA0,
    NETWORK_POLICY_DATA1,
    NETWORK_POLICY_DATA2,
    MAX_NUM_NETWORK_POLICY
}networkPolicy;

typedef enum //alarm_policy
{
    ALARM_NODEID,
    ALARM_VALUE
}alarmPolicy;

typedef enum //setbinary_policy
{
    SET_BINARY_NODEID,
    SET_BINARY_CMD,
    SET_BINARY_VALUE,
    MAX_NUM_SET_BINARY
}setbinaryPolicy;

typedef enum //para_node_id_policy
{
    PARA_NODE_ID,
    PARA_NODE_MAX
}paraNodeIdPolicy;

typedef enum //para_set_policy
{
    SET_POLICY_NODE_ID,
    SET_POLICY_VALUE,
    MAX_NUM_SET_POLICY
}paraSetPolicy;

typedef enum //inter pan policy
{
    INTER_PAN_CMD,
    INTER_PAN_DATA0,
    INTER_PAN_DATA1,
    INTER_PAN_DATA2,
    MAX_INTER_PAN_POLICY
}interPanPolicy;

typedef enum //get secure specification
{
    GET_S_SPEC_NODEID,
    GET_S_SPEC_CLASS,
    GET_S_SPEC_CMD,
    GET_S_SPEC_DATA0,
    GET_S_SPEC_DATA1,
    GET_S_SPEC_DATA2,
    MAX_NUM_GET_S_SPEC,
}getSecureSpecificationPolicy;

static struct blob_buf buff;
static struct ubus_context *ctx;
static struct blob_buf buff;
static struct ubus_event_handler ubus_event_listener; //for handle ST_UPDATE_SUPPORTED_DB, ST_POWER_CYCLE
zigbee_gecko_notify_queue_t g_zigbee_notify;
zigbee_dev_info_t g_zigbee_dev_list;
pthread_mutex_t zigbee_dev_info_listMutex;
static pthread_mutex_t CriticalMutexTransmitQueue = PTHREAD_MUTEX_INITIALIZER;

int g_open_network = 0;                  //open network globle variable
static timer_t timerSpeakAdding;            //for add device
pthread_t zigbee_command_process_thread; //handle command in queue

static int g_inform_power_cycle = 0;

sqlite3 *zigbee_db;       //for database
sqlite3 *support_devs_db; //for database

uint64_t zigbeeGeckoControllerSerial; //controller Id

int g_shmid = 0;    //for share memory function
char *g_shm = NULL; //for share memory

/*function for this file*/
zigbee_dev_info_t *_create_zigbee_dev_(char *appDeviceId, char *serialId, char *deviceMode, char *realId, char *endpointNum,
                                       char *parentId, char *childrenId, char *active, char *cloudId, char *capability, int isAdding, int isEU64);
void remove_zigbee_dev(char *ID, uint16_t oldRealDevId16bit);                                   //for remove device
void remove_zigbee_dev_and_hash_table_prepare(char* appDeviceId);
static void zigbee_notify_active_cb(struct uloop_timeout *timeout); //for handle message from device
static int g_sound_alarm_enable = ENABLE_SOUND_ALARM;               //for enable, disable alarm
void alarm_zigbee_report_final(struct ubus_context *ctx, json_object *jobj, char* ID, char *alarm_pattern);                //for process alrm data
static void cancel_adding_device_cb(struct uloop_timeout *timeout); //for cancel adding device
int getOnOffOfDevice(char *nodeid);//for get on/off value of device
static struct uloop_timeout cancel_adding_device = {
    //for cancel adding device, press open network at Zinno App, after 5 min, firmware auto close network
    .cb = cancel_adding_device_cb,
};

typedef struct _rejoin_message
{
    bool rejoinNotify;
    uint16_t oldRealDevId16bit;
    char appDeviceId[MAX_ID_SIZE];
    char newDeviceId16bit[MAX_ID_SIZE];
} rejoin_message_t;

static void alarm_update_zigbee_database_and_voice_inform(struct ubus_context *ctx,json_object *jobj, char *ID);
////////////////////////// For list device //////////////////////////////

/*
* UBUS service
*/
////////////////////////// For Ubus //////////////////////////////

static int open_closenetwork(struct ubus_context *ctx, struct ubus_object *obj,
                             struct ubus_request_data *req, const char *method,
                             struct blob_attr *msg);
static int network(struct ubus_context *ctx, struct ubus_object *obj,
                   struct ubus_request_data *req, const char *method,
                   struct blob_attr *msg);
static int remove_force_device(struct ubus_context *ctx, struct ubus_object *obj,
                               struct ubus_request_data *req, const char *method,
                               struct blob_attr *msg);
static int venus_zigbee_alarm(struct ubus_context *ctx, struct ubus_object *obj,
                              struct ubus_request_data *req, const char *method,
                              struct blob_attr *msg);
static int setbinary(struct ubus_context *ctx, struct ubus_object *obj,
                     struct ubus_request_data *req, const char *method,
                     struct blob_attr *msg);
static int getbinary(struct ubus_context *ctx, struct ubus_object *obj,
                     struct ubus_request_data *req, const char *method,
                     struct blob_attr *msg);
static int setspecification(struct ubus_context *ctx, struct ubus_object *obj,
                            struct ubus_request_data *req, const char *method,
                            struct blob_attr *msg);
static int setsecurespecification(struct ubus_context *ctx, struct ubus_object *obj,
                                  struct ubus_request_data *req, const char *method,
                                  struct blob_attr *msg);
static int getsecurespecification(struct ubus_context *ctx, struct ubus_object *obj,
                                  struct ubus_request_data *req, const char *method,
                                  struct blob_attr *msg);
static int getspecification(struct ubus_context *ctx, struct ubus_object *obj,
                            struct ubus_request_data *req, const char *method,
                            struct blob_attr *msg);
static int identify(struct ubus_context *ctx, struct ubus_object *obj,
                    struct ubus_request_data *req, const char *method,
                    struct blob_attr *msg);
static int changename(struct ubus_context *ctx, struct ubus_object *obj,
                      struct ubus_request_data *req, const char *method,
                      struct blob_attr *msg);
static int reset_stop_add_timer(struct ubus_context *ctx, struct ubus_object *obj,
                                struct ubus_request_data *req, const char *method,
                                struct blob_attr *msg);
static int alexa(struct ubus_context *ctx, struct ubus_object *obj,
                 struct ubus_request_data *req, const char *method,
                 struct blob_attr *msg);
static int zigbee_force_last_status(struct ubus_context *ctx, struct ubus_object *obj,
                                    struct ubus_request_data *req, const char *method,
                                    struct blob_attr *msg);
static int checking_devices_state(struct ubus_context *ctx, struct ubus_object *obj,
                                  struct ubus_request_data *req, const char *method,
                                  struct blob_attr *msg);
static int zll_command(struct ubus_context *ctx, struct ubus_object *obj,
                       struct ubus_request_data *req, const char *method,
                       struct blob_attr *msg);
static int reset_device(struct ubus_context *ctx, struct ubus_object *obj,
                        struct ubus_request_data *req, const char *method,
                        struct blob_attr *msg);

static int setspecification_CRC(struct ubus_context *ctx, struct ubus_object *obj,
                                struct ubus_request_data *req, const char *method,
                                struct blob_attr *msg);

int get_configuration_data_from_id(char *nodeid, uint64_t id, char *configName);

static const struct blobmsg_policy para_node_id_policy[PARA_NODE_MAX] = {
    [PARA_NODE_ID] = {.name = ST_ID, .type = BLOBMSG_TYPE_STRING},
};
static const struct blobmsg_policy set_specification_policy[MAX_NUM_NETWORK_POLICY] = {
    [NETWORK_POLICY_NODEID] = {.name = ST_ID, .type = BLOBMSG_TYPE_STRING},
    [NETWORK_POLICY_CLASS] = {.name = ST_CLASS, .type = BLOBMSG_TYPE_STRING},
    [NETWORK_POLICY_CMD] = {.name = ST_CMD, .type = BLOBMSG_TYPE_STRING},
    [NETWORK_POLICY_DATA0] = {.name = ST_DATA0, .type = BLOBMSG_TYPE_STRING},
    [NETWORK_POLICY_DATA1] = {.name = ST_DATA1, .type = BLOBMSG_TYPE_STRING},
    [NETWORK_POLICY_DATA2] = {.name = ST_DATA2, .type = BLOBMSG_TYPE_STRING},
};

static const struct blobmsg_policy get_secure_specification[MAX_NUM_GET_S_SPEC] = {
    [GET_S_SPEC_NODEID] = {.name = ST_ID, .type = BLOBMSG_TYPE_STRING},
    [GET_S_SPEC_CLASS] = {.name = ST_CLASS, .type = BLOBMSG_TYPE_STRING},
    [GET_S_SPEC_CMD] = {.name = ST_CMD, .type = BLOBMSG_TYPE_STRING},
    [GET_S_SPEC_DATA0] = {.name = ST_DATA0, .type = BLOBMSG_TYPE_STRING},
    [GET_S_SPEC_DATA1] = {.name = ST_DATA1, .type = BLOBMSG_TYPE_STRING},
    [GET_S_SPEC_DATA2] = {.name = ST_DATA2, .type = BLOBMSG_TYPE_STRING},
};

static const struct blobmsg_policy null_policy[0] = {};

static const struct blobmsg_policy alarm_policy[] = {
    [ALARM_NODEID] = {.name = ST_ID, .type = BLOBMSG_TYPE_STRING},
    [ALARM_VALUE] = {.name = ST_VALUE, .type = BLOBMSG_TYPE_STRING},
};

static const struct blobmsg_policy setbinary_policy[MAX_NUM_SET_BINARY] = {
    [SET_BINARY_NODEID] = {.name = ST_ID, .type = BLOBMSG_TYPE_STRING},
    [SET_BINARY_CMD] = {.name = ST_CMD, .type = BLOBMSG_TYPE_STRING},
    [SET_BINARY_VALUE] = {.name = ST_VALUE, .type = BLOBMSG_TYPE_STRING},
};

static const struct blobmsg_policy set_policy[MAX_NUM_SET_POLICY] = {
    [SET_POLICY_NODE_ID] = {.name = ST_ID, .type = BLOBMSG_TYPE_STRING},
    [SET_POLICY_VALUE] = {.name = ST_VALUE, .type = BLOBMSG_TYPE_STRING},
};

static const struct blobmsg_policy inter_pan_policy[MAX_INTER_PAN_POLICY] = {
    [INTER_PAN_CMD] = {.name = ST_CMD, .type = BLOBMSG_TYPE_STRING},
    [INTER_PAN_DATA0] = {.name = ST_DATA0, .type = BLOBMSG_TYPE_STRING},
    [INTER_PAN_DATA1] = {.name = ST_DATA1, .type = BLOBMSG_TYPE_STRING},
    [INTER_PAN_DATA2] = {.name = ST_DATA2, .type = BLOBMSG_TYPE_STRING},
};

static const struct blobmsg_policy specification_CRC[9]={
    [0] = { .name = ST_ID, .type = BLOBMSG_TYPE_STRING },
    [1] = { .name = ST_CLASS, .type = BLOBMSG_TYPE_STRING },
    [2] = { .name = ST_CMD, .type = BLOBMSG_TYPE_STRING },
    [3] = { .name = ST_DATA0, .type = BLOBMSG_TYPE_STRING },
    [4] = { .name = ST_DATA1, .type = BLOBMSG_TYPE_STRING },
    [5] = { .name = ST_DATA2, .type = BLOBMSG_TYPE_STRING },
    [6] = { .name = ST_SRC_ENDPOINT, .type = BLOBMSG_TYPE_STRING },
    [7] = { .name = ST_DES_ENDPOINT, .type = BLOBMSG_TYPE_STRING },
    [8] = { .name = ST_IS_CRC, .type = BLOBMSG_TYPE_STRING },
};

static const struct ubus_method zigbee_methods[] = {
    UBUS_METHOD(ST_OPEN_NETWORK, open_closenetwork, null_policy),
    UBUS_METHOD(ST_CLOSE_NETWORK, open_closenetwork, null_policy),
    UBUS_METHOD(ST_NETWORK, network, set_specification_policy),
    UBUS_METHOD(ST_REMOVE_DEVICE, remove_force_device, para_node_id_policy),
    UBUS_METHOD(ST_VENUS_ALARM, venus_zigbee_alarm, alarm_policy),
    UBUS_METHOD(ST_SET_BINARY, setbinary, setbinary_policy),
    UBUS_METHOD(ST_GET_BINARY, getbinary, para_node_id_policy),
    UBUS_METHOD(ST_WRITE_SPEC, setspecification, set_specification_policy),
    UBUS_METHOD(ST_WRITE_S_SPEC, setsecurespecification, set_specification_policy),
    UBUS_METHOD(ST_WRITE_SPEC_CRC, setspecification_CRC, specification_CRC),
    UBUS_METHOD(ST_READ_SPEC, getspecification, get_secure_specification),
    UBUS_METHOD(ST_READ_S_SPEC, getsecurespecification, get_secure_specification),
    UBUS_METHOD(ST_RESET, reset_device, null_policy),
    UBUS_METHOD(ST_IDENTIFY, identify, set_policy),
    UBUS_METHOD(ST_CHANGE_NAME, changename, set_policy),
    UBUS_METHOD(ST_ALEXA, alexa, set_policy),
    UBUS_METHOD(ST_RESET_STOP_ADD_TIMER, reset_stop_add_timer, null_policy),
    UBUS_METHOD(ST_FORCE_LAST_STATUS, zigbee_force_last_status, null_policy),
    UBUS_METHOD(ST_CHECK_DEV_STATE, checking_devices_state, null_policy),
    //zll command
    UBUS_METHOD(ST_ZIGBEE_LIGHT_LINK, zll_command, inter_pan_policy),
};

static struct ubus_object_type zigbee_object_type =
    UBUS_OBJECT_TYPE(ST_ZIGBEE, zigbee_methods);

static struct ubus_object zigbee_object = {
    .name = ST_ZIGBEE,
    .type = &zigbee_object_type,
    .methods = zigbee_methods,
    .n_methods = ARRAY_SIZE(zigbee_methods),
};

////////////////////////// For action control //////////////////////////////

void init_zigbee_dev_list(void)
{
    VR_INIT_LIST_HEAD(&g_zigbee_dev_list.list);
    pthread_mutex_init(&zigbee_dev_info_listMutex, 0);
}

int add_zigbee_dev_last_list(void *node_add)
{
    int res = 0;
    pthread_mutex_lock(&zigbee_dev_info_listMutex);
    zigbee_dev_info_t *input = (zigbee_dev_info_t *)node_add;
    zigbee_dev_info_t *tmp = NULL;

    VR_(list_for_each_entry)
    (tmp, &(g_zigbee_dev_list.list), list)
    {
        if (tmp->localId == input->localId)
        {
            res = 1;
            break;
        }
    }

    if (!res)
    {
        VR_(list_add_tail)
        (&(input->list), &(g_zigbee_dev_list.list));
    }

    pthread_mutex_unlock(&zigbee_dev_info_listMutex);
    return res;
}

void remove_zigbee_list(char *id)
{
    pthread_mutex_lock(&zigbee_dev_info_listMutex);
    zigbee_dev_info_t *tmp = NULL;
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)
    (pos, q, &g_zigbee_dev_list.list)
    {
        tmp = VR_(list_entry)(pos, zigbee_dev_info_t, list);
        if(STR_LEN_ZIGBEE_DEVICE_ID_NEW_VER == strlen(id))
        {
            if (tmp->localId == htoi(id))
            {
                VR_(list_del)
                (pos);
                free(tmp);
                continue;
            }
        }
        if (!strcmp(tmp->appDeviceId, id))
        {
            VR_(list_del)
            (pos);
            free(tmp);
        }
    }
    pthread_mutex_unlock(&zigbee_dev_info_listMutex);
}

zigbee_dev_info_t *get_zigbee_dev_from_id(char *id)
{
    zigbee_dev_info_t *tmp = NULL;

    VR_(list_for_each_entry)
    (tmp, &(g_zigbee_dev_list.list), list)
    {
        if(STR_LEN_ZIGBEE_DEVICE_ID_NEW_VER == strlen(id))
        {
            if (tmp->localId == htoi(id))
            {
                return tmp;
            }
        }
        if (!strcmp(tmp->appDeviceId, id))
        {
            return tmp;
        }
    }

    return NULL;
}

void printf_dev_info_list()
{
    int i = 0;
    zigbee_dev_info_t *tmp = NULL;

    VR_(list_for_each_entry)(tmp, &g_zigbee_dev_list.list, list)
    {

        printf("#### dev %d ######\n", i);
        printf("localId = %06X\n", tmp->localId);
        printf("serial = %llX\n", tmp->serial);
        printf("active = %d\n", tmp->active);
        printf("endpointNum = %02X\n", tmp->endpointNum);
        printf("parentId = %02X\n", tmp->parentId);
        printf("serialId = %s\n", tmp->serialId);
        printf("cloudId = %s\n", tmp->cloudId);
        printf("childrenId = %s\n", tmp->childrenId?tmp->childrenId: "NULL");
        printf("json_capId = %s\n", tmp->capList?tmp->capList:"NULL");
        printf("status.onoff = %d\n", tmp->status.onOff);
        printf("status.dim = %d\n", tmp->status.dim);
        printf("status.state = %d\n", tmp->status.state);
        printf("status.lastUpdate = %d\n", tmp->status.lastUpdate);
        printf("status.wakeUp = %d\n", tmp->status.wakeUp);
        printf("appDeviceId = %s\n", tmp->appDeviceId);
        i++;
    }
}

////////////////////////// For update status of device //////////////////////////////

void update_zigbee_dev_from_id(char *id, char *input, char *value)
{
    if (!input || !value)
    {
        return;
    }

    zigbee_dev_info_t *tmp = get_zigbee_dev_from_id(id);
    if (!tmp)
    {
        return;
    }

    if (!strcmp(input, ST_ON_OFF))
    {
        tmp->status.onOff = strtol(value, NULL, 0);
    }
    else if (!strcmp(input, ST_DIM))
    {
        tmp->status.dim = strtol(value, NULL, 0);
    }
    else if (!strcmp(input, ST_STATE))
    {
        if (!strcmp(value, ST_ALIVE))
        {
            tmp->status.state = 1;
        }
        else
        {
            tmp->status.state = 0;
        }

    }
    else if (!strcmp(input, ST_LAST_UPDATE))
    {
        tmp->status.lastUpdate = strtoul(value, NULL, 0);
    }
    else if (!strcmp(input, ST_WAKE_UP_NOTIFICATION))
    {
        tmp->status.wakeUp = strtoul(value, NULL, 0);
    }
    else if (!strcmp(input, ST_THERMOSTAT_MODE))
    {
        if (!strcmp(input, ST_OFF))
        {
            tmp->status.onOff = 0;
        }
        else
        {
            tmp->status.onOff = 1;
        }
    }
    else if (!strcmp(input, ST_CHILDREN_ID))
    {
        strncpy(tmp->childrenId, value, sizeof(tmp->childrenId) - 1);
    }
    else if (!strcmp(input, ST_PRIORITY))
    {
        tmp->status.priority = strtol(value, NULL, 0);
    }
}

void update_zigbee_status(char *service_name, sqlite3 *db, db_callback callback,
                          const char *ID, const char *feature, const char *value,
                          const char *mode, int rotate_num)
{
    set_register_database(service_name, db, callback, ID, feature, value, mode, rotate_num);
    update_zigbee_dev_from_id((char *)ID, (char *)feature, (char *)value);
}

void Send_ubus_notify(char *data)
{
    SLOGI("ubus notify data = %s\n", data);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, data);

    if (ctx)
    {
        ubus_notify(ctx, &zigbee_object, ST_ZIGBEE, buff.head, -1);
    }
    SLOGI("end send ubus notify\n");
}

/*remove force device*/
static int remove_force_device(struct ubus_context *ctx, struct ubus_object *obj,
                               struct ubus_request_data *req, const char *method,
                               struct blob_attr *msg)
{
    struct blob_attr *tb[1];
    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    blobmsg_parse(para_node_id_policy, ARRAY_SIZE(para_node_id_policy), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_REMOVE_DEVICE_R));
    if (tb[0])
    {
        pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_NODE_REMOVE;
        pzbParam->destAddr = htoi(blobmsg_data(tb[0]));
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_MISSING_ARGUMENT));
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
        ubus_send_reply(ctx, req, buff.head);
        return COMMAND_SUCESS;
    }

    char node_id[SIZE_32B];
    sprintf(node_id, "%s", (char *)blobmsg_data(tb[0]));

    SLOGI("START send zigbee Command\n");
    zigbeeGeckoHandlerSendCommand(pzbParam);
    SLOGI("END zigbee Command\n");
    SLOGI("pzbParam->ret = %d\n", pzbParam->ret);
    if (pzbParam->ret == 0)
    {
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_REMOVE_DEVICE_R));

        remove_zigbee_dev_and_hash_table_prepare(node_id);

        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(node_id));
    }
    else
    {
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_REMOVE_DEVICE_R));
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(node_id));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
    }

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    if (pzbParam)
    {
        free(pzbParam);
    }
    return COMMAND_SUCESS;
}

/*return message to app*/
void return_json_message(int res, json_object **jobj, char *command, char *commandClass)
{
    if (!jobj || !*jobj)
    {
        return;
    }
    char tmp[SIZE_512B];
    switch (res)
    {
    case DEV_NOT_FOUND:
        json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(*jobj, ST_REASON, json_object_new_string("device not found"));
        break;

    case GET_DEV_TYPE_FAILED:
        json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(*jobj, ST_REASON, json_object_new_string("failed to get DeviceType in database"));
        break;

    case GET_CAP_FAILED:
        json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(*jobj, ST_REASON, json_object_new_string("failed to get Capability in database"));
        break;

    case DEV_NOT_SUPPORT_CLUSTER:
        sprintf(tmp, "this device does not support %s cluster, please list_devices command to check cluster support", commandClass ? commandClass : "NULL");

        json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(*jobj, ST_REASON, json_object_new_string(tmp));
        break;

    case DEV_UNACTIVATE:
        json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(*jobj, ST_REASON, json_object_new_string("device unactivate"));
        break;

    case COMMAND_FAILED:
        json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        break;

    default:
        json_object_object_add(*jobj, ST_STATUS, json_object_new_string(ST_RECEIVED));
        break;
    }
}

/*ubus open/close network method*/
static int open_closenetwork(struct ubus_context *ctx, struct ubus_object *obj,
                             struct ubus_request_data *req, const char *method,
                             struct blob_attr *msg)
{

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));

    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));
    pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_NODE_ADD;

    if (!strcmp(method, ST_OPEN_NETWORK))
    {
        VR_(execute_system)
        ("ubus send zigbee '{\"state\":\"open_network\"}' &");

        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_OPEN_NETWORK_R));
        pzbParam->param1 = ENABLE_OPEN_NETWORK;
        uloop_timeout_set(&cancel_adding_device, ADDING_DEVICE_TIMEOUT * 1000); //5 mins
    }
    else if (!strcmp(method, ST_CLOSE_NETWORK))
    {
        VR_(execute_system)
        ("ubus send zigbee '{\"state\":\"close_network\"}' &");

        uloop_timeout_cancel(&cancel_adding_device);
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CLOSE_NETWORK_R));
        pzbParam->param1 = DISABLE_OPEN_NETWORK;
    }

    zigbeeGeckoHandlerSendCommand(pzbParam);

    if (pzbParam->ret == 0)
    {
        if (!strcmp(method, ST_CLOSE_NETWORK))
        {
            g_open_network = 0;
        }
        else
        {
            g_open_network = 1;
        }
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
    }

    // VR_(usleep)(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    if (pzbParam)
    {
        free(pzbParam);
    }
    return 0;
}

/*ubus network method*/

static int network(struct ubus_context *ctx, struct ubus_object *obj,
                   struct ubus_request_data *req, const char *method,
                   struct blob_attr *msg)
{
    struct blob_attr *tb[6];
    const char *nodeid = ST_UNKNOWN;
    const char *commandCluster = ST_UNKNOWN;
    const char *Command = ST_UNKNOWN;
    const char *data0 = ST_UNKNOWN;
    const char *data1 = ST_UNKNOWN;
    const char *data2 = ST_UNKNOWN;
    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));

    blobmsg_parse(set_specification_policy, ARRAY_SIZE(set_specification_policy), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NETWORK_R));

    if (tb[1] && tb[2] && tb[3])
    {
    }
    else if (tb[1] && tb[2])
    {
        nodeid = blobmsg_data(tb[0]);
        commandCluster = blobmsg_data(tb[1]);
        Command = blobmsg_data(tb[2]);
        if (!strcmp(commandCluster, ST_ADD_DEV))
        {
            if (!strcmp(Command, ST_OPEN))
            {
                pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_NODE_ADD;
                pzbParam->param1 = ENABLE_OPEN_NETWORK;
                uloop_timeout_set(&cancel_adding_device, ADDING_DEVICE_TIMEOUT * 1000); //5 mins
            }
            else if (!strcmp(Command, ST_CLOSE))
            {
                pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_NODE_ADD;
                pzbParam->param1 = DISABLE_OPEN_NETWORK;
                uloop_timeout_cancel(&cancel_adding_device);
            }
        }
        else if (!strcmp(commandCluster, ST_RESET))
        {
            if (!strcmp(Command, ST_RUN))
            {
                pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_NODE_SET_DEFAULT;
            }
        }
        else if (!strcmp(commandCluster, ST_CONTROLLER_INFO))
        {
            if (!strcmp(Command, ST_GET))
            {
                pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_NODE_GET_CONTROLLER_INFO;
            }
        }
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_MISSING_ARGUMENT));

        
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
        ubus_send_reply(ctx, req, buff.head);
        return COMMAND_SUCESS;
    }
    SLOGI("nodeid = %s\n", nodeid);
    SLOGI("commandCluster = %s\n", commandCluster);
    SLOGI("Command = %s\n", Command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);
    SLOGI("data2 = %s\n", data2);

    zigbee_command_response_t response;
    memset(&response, 0x00, sizeof(zigbee_command_response_t));
    strcpy(response.method, ST_NETWORK_R);
    strcpy(response.nodeid, nodeid);
    strcpy(response.commandCluster, commandCluster);
    strcpy(response.command, Command);
    if (tb[3])
    {
        strcpy(response.data0, data0);
    }
    if (tb[4])
    {
        strcpy(response.data1, data1);
    }
    if (tb[5])
    {
        strcpy(response.data2, data2);
    }

    json_object *commandinfo = json_object_new_object();
    json_object_object_add(commandinfo, ST_CLASS, json_object_new_string(commandCluster));
    json_object_object_add(commandinfo, ST_COMMAND, json_object_new_string(Command));
    if (strcmp(data0, ST_UNKNOWN))
        json_object_object_add(commandinfo, ST_DATA0, json_object_new_string(data0));
    if (strcmp(data1, ST_UNKNOWN))
        json_object_object_add(commandinfo, ST_DATA1, json_object_new_string(data1));
    if (strcmp(data2, ST_UNKNOWN))
        json_object_object_add(commandinfo, ST_DATA2, json_object_new_string(data2));
    json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

    insert_to_queue(response, pzbParam);
    //return_json_message(res, &jobj, (char*)Command, (char*)commandCluster);

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    if (pzbParam)
    {
        free(pzbParam);
    }
    return ENUM_SUCCESS;
}

/*ubus venus zigbee alarm method*/
/*disable/enable alarm*/
static int venus_zigbee_alarm(struct ubus_context *ctx, struct ubus_object *obj,
                              struct ubus_request_data *req, const char *method,
                              struct blob_attr *msg)
{
    struct blob_attr *tb[2];
    blobmsg_parse(alarm_policy, ARRAY_SIZE(alarm_policy), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_VENUS_ALARM_R));

    if (tb[0] && tb[1])
    {
        char *id = blobmsg_data(tb[0]);
        const char *value = blobmsg_data(tb[1]);

        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(id));
        json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
        if (strcmp(id, ST_ALL))
        {
            if (!strcmp(value, ST_ON_VALUE))
            {
                g_sound_alarm_enable = ENABLE_SOUND_ALARM;
                set_register_database(_VR_CB_(zigbee), id, ST_SOUND_ALARM_ENABLE, ST_ON_VALUE, ST_REPLACE, NULL_ROTATE_NUM);
            }
            else if (!strcmp(value, ST_OFF_VALUE))
            {
                set_register_database(_VR_CB_(zigbee), id, ST_SOUND_ALARM_ENABLE, ST_OFF_VALUE, ST_REPLACE, NULL_ROTATE_NUM);
                VR_(cancel_alarm)((void *)ctx, id);
            }
            else if(!strcmp(value, ST_TEMP_VALUE))
            {
                VR_(temp_disable_alarm)(ctx, (char*)id);
            }
            else if(!strcmp(value, ST_DISABLE_THIS_TIME))
            {
                VR_(cancel_alarm)((void *)ctx, id);
            }
        }
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_MISSING_ARGUMENT));
    }
    
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return COMMAND_SUCESS;
}
static int init_ubus_service(void)
{
    const char *ubus_socket = NULL;

    uloop_init();

    ctx = ubus_connect(ubus_socket);
    /*
    UBUS initialization. Call this at very beginning before we use UBUS. The path is something like "network.interface".
    To see the available path on SR400ac, type:
    ubus list
    */
    if (!ctx)
    {
        fprintf(stderr, "Failed to connect to ubus\n");
        return ENUM_ERROR;
    }

    ubus_add_uloop(ctx);
    /*
    Activate UBUS; this tells U-Loop to check for ubus events (listens to ubus).
    It is blocking function, meaning that once we call uloop_run, it is waiting for ubus to get something
    */
    return ENUM_SUCCESS;
}

static void free_ubus_service()
{
    if(ctx)
    {
        ubus_free(ctx);
    }
    uloop_done();
    if (buff.buf)
    {
        free(buff.buf);
    }

    if (zigbee_db)
    {
        sqlite3_close(zigbee_db);
    }

    if (support_devs_db)
    {
        sqlite3_close(support_devs_db);
    }
}

/*force status after power cycle**/
static void force_status(void)
{
    pthread_mutex_lock(&zigbee_dev_info_listMutex);
    zigbee_dev_info_t *zigbee_dev = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)
    (pos, q, &g_zigbee_dev_list.list)
    {
        zigbee_dev = VR_(list_entry)(pos, zigbee_dev_info_t, list);
        if (MODE_ALWAYSLISTENING == zigbee_dev->deviceMode && !strcmp(zigbee_dev->VRDeviceType, ST_DIMMABLE_LIGHT))
        {
            SLOGI("deviveMode = %s\n", zigbee_dev->VRDeviceType);
            char deviceId[SIZE_32B];
            if (!zigbee_dev->endpointNum)
            {
                snprintf(deviceId, sizeof(deviceId), "%02X", zigbee_dev->localId);
            }
            else
            {
                snprintf(deviceId, sizeof(deviceId), "%04X", zigbee_dev->localId);
            }
            SLOGI("start set previous status of device %s onOff %d dim %d\n",
                  deviceId, zigbee_dev->status.onOff, zigbee_dev->status.dim);

            zigbee_command_response_t response;
            memset(&response, 0x00, sizeof(zigbee_command_response_t));

            if (zigbee_dev->status.onOff)
            {
                if (zigbee_dev->status.dim)
                {
                    strcpy(response.method, ST_SET_BINARY_R);
                    strcpy(response.nodeid, deviceId);
                    strcpy(response.command, ST_DIM);
                    snprintf(response.value, sizeof(response.value), "%d", zigbee_dev->status.dim);
                    _set_binary_prepare(response);
                }
                else
                {
                    strcpy(response.method, ST_SET_BINARY_R);
                    strcpy(response.nodeid, deviceId);
                    strcpy(response.command, ST_ON_OFF);
                    snprintf(response.value, sizeof(response.value), "%d", zigbee_dev->status.onOff);

                    _set_binary_prepare(response);
                }
            }
            else
            {
                strcpy(response.method, ST_SET_BINARY_R);
                strcpy(response.nodeid, deviceId);
                strcpy(response.command, ST_ON_OFF);
                snprintf(response.value, sizeof(response.value), "%d", zigbee_dev->status.onOff);

                _set_binary_prepare(response);

                /*make sure the next control is set dim*/
                if (zigbee_dev->status.dim)
                {
                    zigbee_dev->status.dimFirst = true;
                }
            }
        }
    }
    pthread_mutex_unlock(&zigbee_dev_info_listMutex);
}

static void handle_ubus_event(struct ubus_context *ctx, struct ubus_event_handler *ev, //not ok
                              const char *type, struct blob_attr *msg)
{
    if (!strcmp(type, ST_UPDATE_SUPPORTED_DB))
    {
        SLOGI("zigbee re-open supported database\n");
        if (support_devs_db)
        {
            sqlite3_close(support_devs_db);
        }

        open_database(SUPPORTED_DEVS_DATABASE, &support_devs_db);
    }
    else if (!strcmp(type, ST_POWER_CYCLE))
    {
        if(!g_inform_power_cycle)
        {
            SLOGI("ZIGBEE POWER CYCLE\n");
            g_inform_power_cycle = 1;
            force_status();
        }
    }
}

static struct uloop_timeout zigbee_polling_zigbee_notify = {
    .cb = zigbee_notify_active_cb,
};
static void zigbee_ubus_service()
{
    int ret;

    ret = ubus_add_object(ctx, &zigbee_object);
    /*
    Add a UBUS object into the list of objects to be queried. We must call this routine before doing ubus_lookup_id.
    */
    if (ret)
        fprintf(stderr, "Failed to add object: %s\n", ubus_strerror(ret));

    memset(&ubus_event_listener, 0, sizeof(ubus_event_listener));
    ubus_event_listener.cb = handle_ubus_event;

    ret = ubus_register_event_handler(ctx, &ubus_event_listener, ST_UPDATE_SUPPORTED_DB);
    if (ret)
    {
        SLOGE("Failed to register event handler %s: %s\n", ST_UPDATE_SUPPORTED_DB, ubus_strerror(ret));
    }

    ret = ubus_register_event_handler(ctx, &ubus_event_listener, ST_POWER_CYCLE);
    if (ret)
    {
        SLOGE("Failed to register event handler %s: %s\n", ST_POWER_CYCLE, ubus_strerror(ret));
    }
    //Set milliseconds from now (e.g, to be executed later after mSecs elapsed)
    //if we want to make a repeatable event, set the timer again by calling uloop_timeout(timeout, <timeout val in msec)
    uloop_timeout_set(&zigbee_polling_zigbee_notify, 1000); //handle message from libzigbee-gecko

    uloop_run();
}

////////////////////////// For add device //////////////////////////////

/*need free cloudId after using*/
static void post_zigbee_dev_resources_to_cloud(char *Owner, char *Serial, char *SerialID,
                                               char *FriendlyName, char *ID, char *capabilityJsonStr,
                                               char **cloudId)
{
    char resource_id[SIZE_256B];
    uuid_make(resource_id, sizeof(resource_id));

    json_object *json_post_resources = json_object_new_object();
    json_object *resources = json_object_new_object();
    JSON_ADD_STRING_SAFE(resources, ST_OWNER, Owner);
    JSON_ADD_STRING_SAFE(resources, ST_SERIAL, Serial);
    JSON_ADD_STRING_SAFE(resources, ST_SERIAL_ID, SerialID);
    JSON_ADD_STRING_SAFE(resources, ST_FRIENDLY_NAME, FriendlyName);
    JSON_ADD_STRING_SAFE(resources, ST_ID, ID);
    JSON_ADD_STRING_SAFE(resources, ST_CAPABILITY, capabilityJsonStr);
    JSON_ADD_STRING_SAFE(resources, ST_TYPE, ST_ZIGBEE);

    json_object_object_add(json_post_resources, ST_CLOUD_RESOURCE, resources);
    json_object_object_add(json_post_resources, ST_ID, json_object_new_string(resource_id));
    JSON_ADD_STRING_SAFE(json_post_resources, ST_CLOUD_LOCAL_ID, ID);
    JSON_ADD_STRING_SAFE(json_post_resources, ST_CLOUD_SERIAL_ID, SerialID);
    JSON_ADD_STRING_SAFE(json_post_resources, ST_NAME, FriendlyName);

    const char *data = json_object_to_json_string(json_post_resources);

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_DEVICE_ID, ID);
    blobmsg_add_string(&buff, ST_DEVICE_TYPE, "zigbee");
    blobmsg_add_string(&buff, ST_DATA, data);
    ubus_send_event(ctx, ST_RESOURCE, buff.head);

    update_resource_id(_VR_CB_(zigbee), ID, resource_id);
    shm_update_data(g_shm, resource_id, ID, "zigbee", SHM_ADD);

    if (cloudId)
    {
        /*
        The strdup() function returns a pointer to a new string which is a
        duplicate of the string s.  Memory for the new string is obtained
        with malloc(3), and can be freed with free(3).
        */
        *cloudId = strdup(resource_id);
    }

    json_object_put(json_post_resources);
}

void convert_cap_to_json(json_object *capability, char *capList, int scheme)
{
    if (!capList || !strlen(capList))
    {
        SLOGE("missing cap List\n");
        return;
    }

    char schemeStr[8];
    sprintf(schemeStr, "%d", scheme);
    json_object *nonSecObj = json_object_new_object();
    json_object_object_add(nonSecObj, ST_SCHEME, json_object_new_string(schemeStr));
    json_object_object_add(nonSecObj, ST_CAP_LIST, json_object_new_string(capList));
    json_object_array_add(capability, nonSecObj);
}

int get_all_configuration_id_from_serial(char *serialId, char configId[][LENGTH_STRING_CONFIG_ID], uint8_t *numConfigId)
{
    if(!configId)
    {
        return -1;
    }

    SEARCH_DATA_INIT_VAR(initConfiguration);
    searching_database("zigbee", support_devs_db, zigbee_cb, &initConfiguration, 
                            "SELECT InitConfiguration from SUPPORT_DEVS where SerialID='%s'", 
                            serialId);
    if(!initConfiguration.len)
    {
        SLOGE("Failed to get initConfiguration of device %s from database\n", serialId);
        FREE_SEARCH_DATA_VAR(initConfiguration);
        return -1;
    }

    json_object *initConfigObj = VR_(create_json_object)(initConfiguration.value);
    if(!initConfigObj)
    {
        SLOGE("Failed to create json object %s\n", initConfiguration.value);
        FREE_SEARCH_DATA_VAR(initConfiguration);
        return -1;
    }

    enum json_type type = json_object_get_type(initConfigObj);
    if(type != json_type_array)
    {
        SLOGE("initConfigObj is not array\n");
        json_object_put(initConfigObj);
        FREE_SEARCH_DATA_VAR(initConfiguration);
        return -1;
    }

    *numConfigId = json_object_array_length(initConfigObj);
    int i, res = 0;
    for(i=0; i < *numConfigId; i++)
    {
        json_object *jvalue = json_object_array_get_idx(initConfigObj, i);
        if(jvalue)
        {

            json_object *configIdObj = json_object_object_get(jvalue, ST_CONFIG_ID);
            if(!configIdObj)
            {
                continue;
            }
            memcpy((uint8_t*)&configId[i], json_object_get_string(configIdObj), strlen(json_object_get_string(configIdObj)));
        }
        else
        {
            SLOGE("jvalue null\n");
            res = -1;
        }
    }

    json_object_put(initConfigObj);
    FREE_SEARCH_DATA_VAR(initConfiguration);
    return res;
}

int get_configuration_data_from_name(char *nodeid, char *name, char *configId, char *configLength)
{
    if(!name || !configId)
    {
        return -1;
    }

    SEARCH_DATA_INIT_VAR(serialId);
    searching_database(_VR_CB_(zigbee), &serialId, 
                        "SELECT serialId from SUB_DEVICES where id='%s'", nodeid);
    if(!serialId.len)
    {
        SLOGE("Failed to get serialId of device %s from database\n", nodeid);
        FREE_SEARCH_DATA_VAR(serialId);
        return -1;
    }

    SEARCH_DATA_INIT_VAR(initConfiguration);
    searching_database("zigbee", support_devs_db, zigbee_cb, &initConfiguration, 
                            "SELECT InitConfiguration from SUPPORT_DEVS where SerialID='%s'", 
                            serialId.value);
    if(!initConfiguration.len)
    {
        SLOGE("Failed to get initConfiguration of device %s from database\n", serialId.value);
        FREE_SEARCH_DATA_VAR(serialId);
        FREE_SEARCH_DATA_VAR(initConfiguration);
        return -1;
    }

    json_object *initConfigObj = VR_(create_json_object)(initConfiguration.value);
    if(!initConfigObj)
    {
        SLOGE("Failed to create json object %s\n", initConfiguration.value);
        FREE_SEARCH_DATA_VAR(serialId);
        FREE_SEARCH_DATA_VAR(initConfiguration);
        return -1;
    }

    enum json_type type = json_object_get_type(initConfigObj);
    if(type != json_type_array)
    {
        SLOGE("initConfigObj is not array\n");
        json_object_put(initConfigObj);
        FREE_SEARCH_DATA_VAR(serialId);
        FREE_SEARCH_DATA_VAR(initConfiguration);
        return -1;
    }

    int arrayLength = json_object_array_length(initConfigObj);
    int i, res=-1;
    for(i=0; i<arrayLength; i++)
    {
        json_object *jvalue = json_object_array_get_idx(initConfigObj, i);
        if(jvalue)
        {
            json_object *configNameObj = json_object_object_get(jvalue, ST_CONFIG_NAME);
            if(!configNameObj)
            {
                continue;
            }

            const char *configName = json_object_get_string(configNameObj);
            if(strcmp(name, configName))
            {
                continue;
            }

            json_object *configIdObj = json_object_object_get(jvalue, ST_CONFIG_ID);
            json_object *configLengthObj = json_object_object_get(jvalue, ST_CONFIG_LENGTH);
            if(!configIdObj || !configLengthObj)
            {
                continue;
            }

            strcpy(configId, json_object_get_string(configIdObj));
            if(configLength)
            {
                strcpy(configLength, json_object_get_string(configLengthObj));
            }
            res = 0;
            break;
        }
        else
        {
            SLOGE("jvalue null\n");
        }
    }

    json_object_put(initConfigObj);
    FREE_SEARCH_DATA_VAR(serialId);
    FREE_SEARCH_DATA_VAR(initConfiguration);
    return res;
}

int get_configuration_data_from_id(char *nodeid, uint64_t id, char *configName)
{
    if(!configName)
    {
        return -1;
    }

    SEARCH_DATA_INIT_VAR(serialId);
    searching_database(_VR_CB_(zigbee), &serialId, 
                        "SELECT serialId from SUB_DEVICES where id='%s'", nodeid);
    if(!serialId.len)
    {
        SLOGE("Failed to get serialId of device %s from database\n", nodeid);
        FREE_SEARCH_DATA_VAR(serialId);
        return -1;
    }
    SEARCH_DATA_INIT_VAR(initConfiguration);
    searching_database("zigbee", support_devs_db, zigbee_cb, &initConfiguration, 
                            "SELECT InitConfiguration from SUPPORT_DEVS where SerialID='%s'", 
                            serialId.value);
    if(!initConfiguration.len)
    {
        SLOGE("Failed to get initConfiguration of device %s from database\n", serialId.value);
        FREE_SEARCH_DATA_VAR(serialId);
        FREE_SEARCH_DATA_VAR(initConfiguration);
        return -1;
    }

    json_object *initConfigObj = VR_(create_json_object)(initConfiguration.value);
    if(!initConfigObj)
    {
        SLOGE("Failed to create json object %s\n", initConfiguration.value);
        FREE_SEARCH_DATA_VAR(serialId);
        FREE_SEARCH_DATA_VAR(initConfiguration);
        return -1;
    }

    enum json_type type = json_object_get_type(initConfigObj);
    if(type != json_type_array)
    {
        SLOGE("initConfigObj is not array\n");
        json_object_put(initConfigObj);
        FREE_SEARCH_DATA_VAR(serialId);
        FREE_SEARCH_DATA_VAR(initConfiguration);
        return -1;
    }

    int arrayLength = json_object_array_length(initConfigObj);
    int i,res=-1;
    for(i=0; i<arrayLength; i++)
    {
        json_object *jvalue = json_object_array_get_idx(initConfigObj, i);
        if(jvalue)
        {
            json_object *configIdObj = json_object_object_get(jvalue, ST_CONFIG_ID);
            if(!configIdObj)
            {
                continue;
            }
            char tempConfigId[SIZE_16B]={0};
            strcpy(tempConfigId, json_object_get_string(configIdObj));

            uint64_t configId = htoi64(&tempConfigId[2]);
            if(configId != id)
            {
                continue;
            }

            json_object *configNameObj = json_object_object_get(jvalue, ST_CONFIG_NAME);
            if(!configNameObj)
            {
                continue;
            }

            strcpy(configName, json_object_get_string(configNameObj));
            res=0;
            break;
        }
    }
    json_object_put(initConfigObj);
    FREE_SEARCH_DATA_VAR(serialId);
    FREE_SEARCH_DATA_VAR(initConfiguration);
    return res;
}

int VR_(set_configuration)(uint16_t deviceId, char *configuration_data, uint8_t endpoint)
{
    SLOGI("device %04X set configuration with data %s\n", 
            deviceId, configuration_data);
    int res = 0;
    char *tok, *save_tok, *pch, *stok;
    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));

    tok = strtok_r(configuration_data, ";", &save_tok);
    while(tok != NULL)
    {
        SLOGI("tok = %s\n", tok);
        char configuration[SIZE_512B], clusterId[SIZE_32B] = {0}, attributeId[SIZE_32B] = {0}; 
        char configId[SIZE_32B], length[SIZE_32B], endPointNumber[SIZE_32B], value[SIZE_32B];
        strncpy(configuration, tok, sizeof(configuration));
        pch = strtok_r(configuration, ",", &stok);
        if(pch != NULL)
        {
            strcpy(configId, pch);
            pch = strtok_r(NULL, ",", &stok);
        }
        memcpy(clusterId, configId, CLUSTER_ID_LENGTH);
        memcpy(attributeId, &configId[CLUSTER_ID_LENGTH], ATTRIBUTE_ID_LENGTH);
        if(pch != NULL)
        {
            strcpy(length, pch);
            pch = strtok_r(NULL, ",", &stok);
        }
        if(pch != NULL)
        {
            strcpy(value, pch);
            pch = strtok_r(NULL, ",", &stok);
        }
        if(pch != NULL)
        {
            strcpy(endPointNumber, pch);
            pch = strtok_r(NULL, ",", &stok);
        }

        SLOGI("clusterId = %s\n", clusterId);
        SLOGI("attributeId = %s\n", attributeId);
        SLOGI("value = %s\n", value);

        pzbParam->zclWriteAttr[0].id = htoi(attributeId);
        pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_WRITE_ATTRIB;
        pzbParam->destAddr = deviceId;
        pzbParam->destEndpoint = htoi(endPointNumber);
        pzbParam->param2 =  htoi(clusterId);  //clusterId
        pzbParam->param3 = HOME_AUTOMATION_PROFILE_ID;        //profileId
        pzbParam->param4 = DEFAULT_INVALID_MANUFACTURER_CODE; //manufacturer code
        pzbParam->param1 = DEFAULT_LENGHT_WRITE_CONFIG;       //attribute Number = 1
        pzbParam->param5 = htoi(value);//dataValue
        zigbeeGeckoHandlerSendCommand(pzbParam);
        if(pzbParam->ret)
        {
            SLOGE("Failed to send config attribute\n");
            res = -1;
            free(pzbParam);
            return res;
        }
        tok = strtok_r(NULL, ";", &save_tok);
    }

    free(pzbParam);
    return res;
}

// set device configurations base on supported database
// format: configId,length,value,endpoint 
int adding_process_set_configuration(char* serialId, uint16_t deviceId, uint8_t endpoint)
{
    int res = 0;
    uint8_t numConfigId;
    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));
    char configId[MAX_CONFIG_ID_FROM_DB][LENGTH_STRING_CONFIG_ID] = {0};
    if(!get_all_configuration_id_from_serial(serialId, configId, &numConfigId))
    {
        pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_MULTI_CONFIG_REPORT;
        pzbParam->destAddr = deviceId;
        pzbParam->destEndpoint = endpoint;
        pzbParam->param1 = numConfigId;//numAttributeId;
        memcpy(&pzbParam->configReportAttributeId[0][0], &configId[0][0], numConfigId * LENGTH_STRING_CONFIG_ID);
        SLOGE("Send config report attribute to deviceId: 0x%04X, endpoint: 0x%02X\n", deviceId, endpoint);
        zigbeeGeckoHandlerSendCommand(pzbParam);
        if(pzbParam->ret)
        {
            SLOGE("Failed to send config report attribute\n");
            timerCancel(&timerSpeakAdding);
            res = -1;
            free(pzbParam);
            return res;
        }
    }
    else
    {
        SLOGE("Not found InitConfig from database\n");
        res = 0;
    }
    SEARCH_DATA_INIT_VAR(configuration_data);
    searching_database("zigbee", support_devs_db, zigbee_cb, &configuration_data, 
                "SELECT Reconfiguration from SUPPORT_DEVS where SerialID='%s'", 
                serialId);
    if(configuration_data.len)
    {
        if(VR_(set_configuration)(deviceId, configuration_data.value, endpoint))
        {
            timerCancel(&timerSpeakAdding);
            res = -1;
        }
    }
    FREE_SEARCH_DATA_VAR(configuration_data);
    free(pzbParam);
    return res;
}

//add device to database and alexa support
static void _zigbee_adding_device(json_object *jobj, char *capabilityJsonStr, char *serial, char * appDeviceId,
                                  char *serialId, char *realDeviceId, char *deviceType, char *endpointNum,
                                  char *parentId, zigbee_adding_info zigbeeGeckoAddInfo)
{
    if (!zigbeeGeckoAddInfo.owner || !serial || !serialId ||
        !realDeviceId)
    {
        SLOGE("missing infor\n");
        return;
    }
    //profileId = realDeviceId
    //parent appDeviceId = endpoint + eui64
    //childrent appDeviceId = endpoint + eui64
    database_actions(_VR_(zigbee), "INSERT INTO SUB_DEVICES"
                                   "('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')"
                                   "VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
                     ST_OWNER, ST_SERIAL, ST_SERIAL_ID, ST_DEVICE_TYPE,
                     ST_DEVICE_MODE, ST_FRIENDLY_NAME, ST_ID, ST_CAPABILITY, ST_PROFILE_ID, ST_ENDPOINT_NUM, ST_PARENT_ID,
                     ST_ACTIVE, ST_ALEXA, ST_TYPE,
                     zigbeeGeckoAddInfo.owner, serial, serialId, deviceType, zigbeeGeckoAddInfo.deviceMode, zigbeeGeckoAddInfo.deviceName,
                     appDeviceId, capabilityJsonStr, realDeviceId, endpointNum, parentId, zigbeeGeckoAddInfo.active, zigbeeGeckoAddInfo.alexa, ST_ZIGBEE);

    json_object *deviceinfo = json_object_new_object();
    json_object_object_add(deviceinfo, ST_OWNER, json_object_new_string(zigbeeGeckoAddInfo.owner));
    json_object_object_add(deviceinfo, ST_SERIAL, json_object_new_string(serial));
    json_object_object_add(deviceinfo, ST_SERIAL_ID, json_object_new_string(serialId));
    json_object_object_add(deviceinfo, ST_DEVICE_TYPE, json_object_new_string(deviceType));
    json_object_object_add(deviceinfo, ST_DEVICE_MODE, json_object_new_string(zigbeeGeckoAddInfo.deviceMode));
    json_object_object_add(deviceinfo, ST_FRIENDLY_NAME, json_object_new_string(zigbeeGeckoAddInfo.deviceName));
    //json_object_object_add(deviceinfo, ST_ID, json_object_new_string(appDeviceId));
    json_object_object_add(deviceinfo, ST_ID, json_object_new_string(appDeviceId));


    /*capabilityJsonStr is in cluster of device*/
    //json_object_object_add(deviceinfo, ST_CAPABILITY, json_object_new_string(capabilityJsonStr));
    json_object *capabilityArrayObj = json_object_new_array();
    convert_cap_to_json(capabilityArrayObj, capabilityJsonStr, DEFAULT_ZIGBEE_SECURE_LEVEL);
    json_object *capabilityObj = VR_(create_json_object)((char *)json_object_to_json_string(capabilityArrayObj));
    if (capabilityObj)
    {
        json_object_object_add(deviceinfo, ST_CAPABILITY, capabilityObj);
    }
    json_object_object_add(deviceinfo, ST_ENDPOINT_NUM, json_object_new_string(endpointNum));
    json_object_object_add(deviceinfo, ST_PARENT_ID, json_object_new_string(parentId));
    json_object_object_add(deviceinfo, ST_TYPE, json_object_new_string(ST_ZIGBEE));

    char *cloudId = NULL;
    post_zigbee_dev_resources_to_cloud(zigbeeGeckoAddInfo.owner, serial, serialId,
                                       zigbeeGeckoAddInfo.deviceName, appDeviceId, capabilityJsonStr,
                                       &cloudId);
    _create_zigbee_dev_(appDeviceId, serialId, zigbeeGeckoAddInfo.deviceMode, realDeviceId,
                        endpointNum, parentId, NULL, zigbeeGeckoAddInfo.active, cloudId, capabilityJsonStr, ENUM_ADDING_DEVICE, STR_LEN_ZIGBEE_EUI_64_DEVICE_ID);

    char *id = (char *)malloc(strlen(appDeviceId) + 1);
    strcpy(id, appDeviceId);
    pthread_t adding_actions_thread_t;
    pthread_create(&adding_actions_thread_t, NULL, (void *)&adding_actions_devices_prepare, id);
    pthread_detach(adding_actions_thread_t);

    JSON_ADD_STRING_SAFE(deviceinfo, ST_CLOUD_RESOURCE, cloudId);
    json_object_object_add(jobj, ST_DEVICE_INFO, deviceinfo);
    Send_ubus_notify((char *)json_object_to_json_string(jobj));
    json_object_object_del(jobj, ST_DEVICE_INFO);
    json_object_put(deviceinfo);
    json_object_put(capabilityObj);
    json_object_put(capabilityArrayObj);
    SAFE_FREE(cloudId);
}

/*add children dev to database*/
void _adding_zigbee_children_devices(int unexpected, json_object *jobj, char *capabilityJsonStr, char *parentSerial,
                                     char *parentSerialId, char *parentRealDeviceId16Bit, char *deviceType, char *endpointNum,
                                     char *parentId, zigbee_adding_info zigbeeGeckoAddInfo, char *childrenId)
{
    if (!zigbeeGeckoAddInfo.owner || !parentSerial || !parentId ||
        !parentSerialId || !zigbeeGeckoAddInfo.deviceName || !childrenId)
    {
        SLOGE("missing infor\n");
        return;
    }

    char childAppDevId[SIZE_128B], realChildDevId[SIZE_128B];
    char childSerial[SIZE_128B] = {0};
    char childSerialID[SIZE_128B] = {0};
    sprintf(childSerialID, "%s-%s", parentSerialId, endpointNum);
    sprintf(childAppDevId, "%s%s", endpointNum, parentSerial);//childAppDevId(App) = endpoint + eui64 = childSerial
    sprintf(realChildDevId, "%s%s", endpointNum, parentRealDeviceId16Bit);//real parentAppDeviceId = endpoint + parentAppDeviceId16bit
    sprintf(childSerial, "%s%s", endpointNum, parentSerial);
    SEARCH_DATA_INIT_VAR(FriendlyName);
    searching_database("zigbee", support_devs_db, zigbee_cb, &FriendlyName,
                       "SELECT FriendlyName from SUPPORT_DEVS where serialId = '%s'",
                       childSerialID);

    if (!FriendlyName.len) /*not supported device*/
    {
        if(!unexpected)
        {
            zigbeeGeckoAddInfo.deviceName = (char *)childSerialID;
        }
        else
        {
            zigbeeGeckoAddInfo.deviceName = ST_UNKNOWN;
        }
    }
    else
    {
        zigbeeGeckoAddInfo.deviceName = FriendlyName.value;
    }
    _zigbee_adding_device(jobj, capabilityJsonStr, childSerial, childAppDevId,
                          childSerialID, realChildDevId, deviceType, endpointNum, parentId,
                          zigbeeGeckoAddInfo);

    size_t childrenLeng = strlen(childrenId);
    if (childrenLeng == 0)
    {
        sprintf(childrenId, "%s", childAppDevId);
    }
    else
    {
        sprintf(childrenId + childrenLeng, ",%s", childAppDevId);
    }
    FREE_SEARCH_DATA_VAR(FriendlyName);
}

int getDeviceModeFromOperatedMode(char* operatedMode)
{
    if (strcmp(operatedMode, "LISTEN") == 0)
        return MODE_ALWAYSLISTENING;
    if (strcmp(operatedMode, "FLISTEN") == 0)
        return MODE_FREQUENTLYLISTENING;
    if (strcmp(operatedMode, "SLEEP") == 0)
        return MODE_NONLISTENING;
    return MODE_UNKNOW;
}

void zigbee_adding_handler(struct ubus_context *ctx, json_object *jobj,
                           NOTIFY_ZIGBEE_TX_BUFFER_T pTxNotify, int *sending_zigbee_notify)
{
    if (!jobj)
    {
        SLOGE("missing jobj\n");
        return;
    }

    int unexpected = 0;
    switch (pTxNotify.newNodeNotify.status)
    {
    // case ADD_NODE_STATUS_LEARN_READY:
    //     json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_READY));
    //     *sending_zigbee_notify = 0;
    //     break;
    // case ADD_NODE_STATUS_FAILED:
    //     json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_ADD_FAILED));
    //     timerCancel(&timerSpeakAdding);
    //     VR_(inform_retry_adding)();
    //     break;
    // case ADD_REMOVE_NODE_TIMEOUT:
    //     json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_TIMEOUT));
    //     timerCancel(&timerSpeakAdding);
    //     VR_(inform_retry_adding)();
    //     break;
    case ADD_NODE_STATUS_INTERVIEW_DEV_FAILED:
    {
        unexpected = 1;
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_UNEXPECTED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_FAILED_TO_INTERVIEW));
    }
    case ADD_NODE_STATUS_DONE:
    {
        json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_ADD_DEV));
        char ID[MAX_ID_SIZE];
        int supported_device = 1;
        zigbee_adding_info zigbeeGeckoInfo;
        char *defaultName = ST_UNKNOWN;
        char childrenId[SIZE_1024B] = {0};
        char owner[SIZE_32B], euiAddr[SIZE_32B], serialId[SIZE_64B];
        char capabilityInCluster[SIZE_256B] = {0};
        char capabilityOutCluster[SIZE_256B] = {0};
        char alexa[MAX_ID_SIZE], active[MAX_ID_SIZE], deviceMode[MAX_ID_SIZE];
        char endPointNum[MAX_ID_SIZE], deviceType[MAX_DEVICE_ID_SIZE];
        char parentRealDevId24Bit[MAX_ID_SIZE];
        char parentAppDeviceId[MAX_ID_SIZE];

        zigbeeGeckoInfo.owner = &owner[0];
        zigbeeGeckoInfo.deviceName = defaultName;
        zigbeeGeckoInfo.deviceMode = &deviceMode[0];
        zigbeeGeckoInfo.active = &active[0];
        zigbeeGeckoInfo.alexa = &alexa[0];

        strcpy(active, ST_YES);
        strcpy(alexa, ST_NO);
        sprintf(ID, "%04X", pTxNotify.newNodeNotify.destAddr);
        sprintf(owner, "%016llX", zigbeeGeckoControllerSerial);
        sprintf(euiAddr, "%016llX", pTxNotify.newNodeNotify.euiAddr);
        sprintf(serialId, "%s", pTxNotify.newNodeNotify.manufacturerName);
        sprintf(serialId + strlen(serialId), " %s", pTxNotify.newNodeNotify.modelIdentifier);
        /*serial: eui64, * serialId: manufacture + model */

        SEARCH_DATA_INIT_VAR(FriendlyName);
        searching_database("zigbee", support_devs_db, zigbee_cb, &FriendlyName,
                           "SELECT FriendlyName from SUPPORT_DEVS where serialId = '%s'",
                           serialId);

        if (!FriendlyName.len) /*not supported device*/
        {
            zigbeeGeckoInfo.deviceName = (char *)serialId;
            supported_device = 0;
            if(!unexpected)
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NOT_SUPPORT));
            }
            else
            {
                zigbeeGeckoInfo.deviceName = ST_UNKNOWN;
            }
        }
        else
        {
            if (!unexpected)
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NEW_DEVICE));
            }
            zigbeeGeckoInfo.deviceName = FriendlyName.value;
            /*set association*/
            /*set configuration*/
            int res = adding_process_set_configuration(serialId, pTxNotify.newNodeNotify.destAddr, pTxNotify.newNodeNotify.clusterList[0].endpoint);
            if(res)
            {
                unexpected = 1;
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_UNEXPECTED));
                json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("failed to set configuration"));
            }
        }
        if (supported_device)
        {
            SEARCH_DATA_INIT_VAR(Alexa);
            searching_database("zigbee", support_devs_db, zigbee_cb, &Alexa,
                               "SELECT alexa from SUPPORT_DEVS where serialId = '%s'",
                               serialId);

            if (Alexa.len)
            {
                strncpy(alexa, Alexa.value, sizeof(alexa) - 1);
            }
            FREE_SEARCH_DATA_VAR(Alexa);
        }

        int i, j;
        for (i = 0; i < pTxNotify.newNodeNotify.numEndpoint; i++)
        {
            if (DEFAULT_PARENT_ENDPOINT_ID == i)
            //parent
            // with:    endpoint = 0, deviceId = endpoint + ID
            //          parentId = 0
            //parent AppDeviceId = eui64
            //children AppDeviceId = endpoint+eui64
            {
                SEARCH_DATA_INIT_VAR(deviceParentMode);
                searching_database("zigbee", support_devs_db, zigbee_cb, &deviceParentMode,
                "SELECT OperatedMode from SUPPORT_DEVS where serialId = '%s'",
                serialId);

                if (deviceParentMode.len)
                {
                    sprintf(deviceMode, "%d", getDeviceModeFromOperatedMode(deviceParentMode.value));
                }
                FREE_SEARCH_DATA_VAR(deviceParentMode);
                sprintf(endPointNum, "%02X", pTxNotify.newNodeNotify.clusterList[i].endpoint);
                sprintf(parentRealDevId24Bit, "%02X%s", pTxNotify.newNodeNotify.clusterList[i].endpoint, ID);
                sprintf(parentAppDeviceId, "%02X%s", pTxNotify.newNodeNotify.clusterList[i].endpoint, euiAddr);
                for (j = 0; j < pTxNotify.newNodeNotify.clusterList[i].inClusterCount; j++)
                {
                    if (FIRST_CLUSTER == j)
                        sprintf(capabilityInCluster + strlen(capabilityInCluster), "%04X",
                                pTxNotify.newNodeNotify.clusterList[i].inClusterList[j]);
                    else
                        sprintf(capabilityInCluster + strlen(capabilityInCluster), ", %04X",
                                pTxNotify.newNodeNotify.clusterList[i].inClusterList[j]);
                }
                sprintf(deviceType, "%04X", pTxNotify.newNodeNotify.clusterList[i].deviceId);
                /*insert into database capability InCluster*/
                database_actions(_VR_(zigbee), "INSERT INTO CAPABILITY"
                                               "(%s,%s,%s,%s)"
                                               "VALUES('%s', '%s', '%s', '%s')",
                                 ST_DEVICE_ID, ST_CAP_LIST, ST_IN_OUT, ST_TYPE,
                                 parentAppDeviceId, capabilityInCluster, "in", ST_ZIGBEE);

                /*use capabilityInCluster push notification to app*/
                _zigbee_adding_device(jobj, capabilityInCluster, euiAddr, parentAppDeviceId,
                                      serialId, parentRealDevId24Bit, deviceType, endPointNum,
                                      PARENT_ID_NUM, zigbeeGeckoInfo);
            }
            else //children if device has more than one enpoint
            // with:    endpoint = clusterList[i].endpoint, deviceId = endpoint+ID
            //          parentId = ID = destAddr
            {
                memset(capabilityInCluster, 0x00, sizeof(capabilityInCluster));
                memset(capabilityOutCluster, 0x00, sizeof(capabilityOutCluster));

                sprintf(endPointNum, "%02X", pTxNotify.newNodeNotify.clusterList[i].endpoint);
                SEARCH_DATA_INIT_VAR(deviceChildrenMode);
                searching_database("zigbee", support_devs_db, zigbee_cb, &deviceChildrenMode,
                "SELECT OperatedMode from SUPPORT_DEVS where serialId = '%s-%s'",
                serialId, endPointNum);

                if (deviceChildrenMode.len)
                {
                    sprintf(deviceMode, "%d", getDeviceModeFromOperatedMode(deviceChildrenMode.value));
                }
                FREE_SEARCH_DATA_VAR(deviceChildrenMode);
                for (j = 0; j < pTxNotify.newNodeNotify.clusterList[i].inClusterCount; j++)
                {
                    if (FIRST_CLUSTER == j)
                        sprintf(capabilityInCluster + strlen(capabilityInCluster), "%04X",
                                pTxNotify.newNodeNotify.clusterList[i].inClusterList[j]);
                    else
                        sprintf(capabilityInCluster + strlen(capabilityInCluster), ", %04X",
                                pTxNotify.newNodeNotify.clusterList[i].inClusterList[j]);
                }
                sprintf(deviceType, "%04X", pTxNotify.newNodeNotify.clusterList[i].deviceId);
                /*insert into database capability InCluster*/
                database_actions(_VR_(zigbee), "INSERT INTO CAPABILITY"
                                               "(%s,%s,%s,%s)"
                                               "VALUES('%s%s', '%s', '%s', '%s')",
                                 ST_DEVICE_ID, ST_CAP_LIST, ST_IN_OUT, ST_TYPE,
                                 endPointNum, euiAddr, capabilityInCluster, "in", ST_ZIGBEE);

                /*use capabilityInCluster push notification to app*/
                _adding_zigbee_children_devices(unexpected, jobj, capabilityInCluster, euiAddr,
                                                serialId, ID, deviceType, endPointNum,
                                                euiAddr, zigbeeGeckoInfo, childrenId);
            }
        }

        if (strlen(childrenId))
        {
            database_actions(_VR_(zigbee), "UPDATE SUB_DEVICES set childrenId='%s' where id='%s'",
                             childrenId, parentAppDeviceId);
            //update_zigbee_dev_from_id(ID, ST_CHILDREN_ID, childrenId);
            /*Tuan Nguyen write this function but he does not use it*/
        }

        timerCancel(&timerSpeakAdding);

        if (supported_device && !unexpected)
        {
            SEARCH_DATA_INIT_VAR(sound_file);
            searching_database("zigbee", support_devs_db, zigbee_cb, &sound_file,
                               "SELECT Sound from SUPPORT_DEVS where serialId = '%s'",
                               serialId);

            if (sound_file.len)
            {
                VR_(inform_device_adding_success)((void *)ctx, sound_file.value);
            }
            FREE_SEARCH_DATA_VAR(sound_file);
        }
        else if (!supported_device && !unexpected)
        {
            VR_(inform_device_not_support)((void *)ctx);
        }
        json_object_object_del(jobj, ST_CONTROLLER_CAPABILITY);
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_ADD_DONE)); //inform finish adding

        FREE_SEARCH_DATA_VAR(FriendlyName);
        break;
    }
    }
}

////////////////////////// For remove device //////////////////////////////
/*remove device from database*/

void remove_zigbee_dev_in_database_and_group(char *ID, zigbee_dev_info_t *zigbee_dev)
{
    if (!ID)
    {
        return;
    }
    /*must call before remove FEATURES*/
    remove_and_update_device_in_group(_VR_CB_(zigbee), g_shm, ID, ST_ZIGBEE, NULL);

    database_actions(_VR_(zigbee), "DELETE from ASSOCIATION_DATA where deviceId='%s'", ID);
    database_actions(_VR_(zigbee), "DELETE from MORE_PROPERTY where deviceId='%s' AND deviceType='%s'",
                     ID, ST_ASSOCIATION);
    database_actions(_VR_(zigbee), "DELETE from USER_CODE where deviceId='%s'", ID);
    database_actions(_VR_(zigbee), "DELETE from MORE_PROPERTY where deviceId='%s' AND deviceType='%s'",
                     ID, ST_USER_CODE);

    database_actions(_VR_(zigbee), "DELETE from METER_DATA where deviceId='%s'", ID);
    database_actions(_VR_(zigbee), "DELETE from MORE_PROPERTY where deviceId='%s' AND deviceType='%s'", 
                            ID, ST_METER);
    database_actions(_VR_(zigbee), "DELETE from SENSOR_DATA where deviceId='%s'", ID);
    database_actions(_VR_(zigbee), "DELETE from MORE_PROPERTY where deviceId='%s' AND deviceType='%s'", 
                            ID, ST_SENSOR_MULTILEVEL);

    database_actions(_VR_(zigbee), "DELETE from SCENES where deviceId='%s'", ID);
    database_actions(_VR_(zigbee), "DELETE from FEATURES where deviceId='%s'", ID);
    database_actions(_VR_(zigbee), "DELETE from CAPABILITY where deviceId='%s'", ID);
    database_actions(_VR_(zigbee), "DELETE from ALARM where deviceId='%s'", ID);
    database_actions(_VR_(zigbee), "DELETE from SUB_DEVICES where id='%s'", ID);
}
/*remove device in cloud*/
void remove_zigbee_dev_in_cloud(char *ID)
{
    if (!ID)
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(resource_id);

    searching_database(_VR_CB_(zigbee), &resource_id,
                       "SELECT owner from DEVICES where deviceId='%s'",
                       ID);

    if (resource_id.len)
    {
        if (!VR_(delete_resources)(resource_id.value, RESOURCES_TIMEOUT))
        {
            database_actions(_VR_(zigbee), "DELETE from DEVICES where deviceId='%s'", ID);
            shm_update_data(g_shm, resource_id.value, NULL, NULL, SHM_DELETE);
        }
    }

    FREE_SEARCH_DATA_VAR(resource_id);
}

static int init_dev_info_db_callback(void *data, int argc, char **argv, char **azColName)
{
    int i;
    //char *serial = NULL;
    char *serialId = NULL;
    char *realId = NULL;
    char *deviceMode = NULL;
    char *id = NULL;
    char *endpointNum = NULL;
    char *parentId = NULL;
    char *childrenId = NULL;
    char *active = NULL;
    char *capList = NULL;
    char ID[MAX_ID_SIZE];
    for (i = 0; i < argc; i++)
    {
        // printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
        if (!strcmp(azColName[i], ST_SERIAL_ID))
        {
            serialId = argv[i];
        }
        if (!strcmp(azColName[i], ST_SERIAL))
        {
            //serial = argv[i];
        }
        if (!strcmp(azColName[i], ST_PROFILE_ID))
        {
            realId = argv[i];
        }
        else if (!strcmp(azColName[i], ST_DEVICE_MODE))
        {
            deviceMode = argv[i];
        }
        else if (!strcmp(azColName[i], ST_ID))
        {
            id = argv[i];
        }
        else if (!strcmp(azColName[i], ST_ENDPOINT_NUM))
        {
            endpointNum = argv[i];
        }
        else if (!strcmp(azColName[i], ST_PARENT_ID))
        {
            parentId = argv[i];
        }
        else if (!strcmp(azColName[i], ST_CHILDREN_ID))
        {
            childrenId = argv[i];
        }
        else if (!strcmp(azColName[i], ST_ACTIVE))
        {
            active = argv[i];
        }
        else if (!strcmp(azColName[i], ST_CAPABILITY))
        {
            capList = argv[i];
        }
    }
    if (id)
    {
        //migrate database, update id 4 digits to 6 digits
        if(STR_LEN_ZIGBEE_DEVICE_ID_OLD_VER == strlen(id))
        {
            sprintf(ID, "%s%s", endpointNum, id);
            SLOGI("migrate database, update deviceId = %s --> %s\n", id, ID);
            database_actions(_VR_(zigbee), "UPDATE SUB_DEVICES SET id='%s' "
                                                   "WHERE id='%s'",
                                     ID, id);
        }
        else
        {
            sprintf(ID, "%s", id);
        }
        if(realId && strlen(realId))//deviceId is eui64
        {
            //sprintf(ID, "%s", realId);
            if (data)
            {
                zigbee_dev_info_t **zigbee_dev = (zigbee_dev_info_t **)data;
                *zigbee_dev = _create_zigbee_dev_(ID, serialId, deviceMode, realId, endpointNum,
                                                  parentId, childrenId, active, NULL,
                                                  capList, ENUM_NO_ADDING_DEVICE, STR_LEN_ZIGBEE_EUI_64_DEVICE_ID);
            }
            else
            {
                _create_zigbee_dev_(ID, serialId, deviceMode, realId, endpointNum,
                                    parentId, childrenId, active, NULL,
                                    capList, ENUM_NO_ADDING_DEVICE, STR_LEN_ZIGBEE_EUI_64_DEVICE_ID);
            }
        }
        else
        {
            if (data)
            {
                zigbee_dev_info_t **zigbee_dev = (zigbee_dev_info_t **)data;
                *zigbee_dev = _create_zigbee_dev_(ID, serialId, deviceMode, ID, endpointNum,
                                                  parentId, childrenId, active, NULL,
                                                  capList, ENUM_NO_ADDING_DEVICE, STR_LEN_ZIGBEE_DEVICE_ID_NEW_VER);
            }
            else
            {
                _create_zigbee_dev_(ID, serialId, deviceMode, ID, endpointNum,
                                    parentId, childrenId, active, NULL,
                                    capList, ENUM_NO_ADDING_DEVICE, STR_LEN_ZIGBEE_DEVICE_ID_NEW_VER);
            }
        }

    }
    return COMMAND_SUCESS;
}

void init_zigbee_dev_info_list(void)
{
    DISABLE_DATABASE_LOG();
    searching_database(_VR_(zigbee), init_dev_info_db_callback, NULL,
                       "SELECT serial,serialId,deviceMode,id,profileId,endpointNum,parentId,childrenId,active,capability "
                       "from SUB_DEVICES where owner='%016llX'",
                       zigbeeGeckoControllerSerial);
    ENABLE_DATABASE_LOG();
}

zigbee_dev_info_t *create_zigbee_dev_from_id(char *id)
{
    zigbee_dev_info_t *zigbee_dev = NULL;
    searching_database(_VR_(zigbee), init_dev_info_db_callback, &zigbee_dev,
                       "SELECT serial,serialId,id,profileId,endpointNum,parentId,childrenId,active,capability "
                       "from SUB_DEVICES where owner='%016llX' AND id='%s'",
                       zigbeeGeckoControllerSerial, id);
    return zigbee_dev;
}
void remove_zigbee_dev_from_id(char *id)
{
    remove_zigbee_list(id);
}

void speak_file_into_database(char *ID)
{
    int alarm = 0;
    char *defaultSound = ST_DEFAULT_SOUND_NAME;
    zigbee_dev_info_t *dev = get_zigbee_dev_from_id(ID);
    /*checking device inform status via venus speaker or not*/
    SEARCH_DATA_INIT_VAR(sound_alarm_enable);
    searching_database(_VR_CB_(zigbee), &sound_alarm_enable,
                       "SELECT register from FEATURES where deviceId='%s' and featureId='%s'",
                       ID, ST_SOUND_ALARM_ENABLE);
    if (!sound_alarm_enable.len)
    {
        set_register_database(_VR_CB_(zigbee), ID, ST_SOUND_ALARM_ENABLE, ST_ON_VALUE, ST_REPLACE, NULL_ROTATE_NUM);
        alarm = 1;
    }
    else if (!strcmp(sound_alarm_enable.value, ST_ON_VALUE))
    {
        alarm = 1;
    }
    FREE_SEARCH_DATA_VAR(sound_alarm_enable);
    if(dev)
    {
        defaultSound = dev->defaultSound;
    }
    VR_(alarm)(NULL, alarm, (char *)ST_HAS_BEEN_RESET, ID, ST_ZIGBEE, defaultSound, NULL);
}

void speak_dev_has_beeen_reset(char *ID)
{
    if(!g_open_network)
    {
        SEARCH_DATA_INIT_VAR(serialId);
        searching_database(_VR_CB_(zigbee), &serialId,
                        "SELECT serialId from SUB_DEVICES where id='%s'",
                        ID);
        if (serialId.len)
        {
            SEARCH_DATA_INIT_VAR(action);
            searching_database("zigbee", support_devs_db, zigbee_cb, &action,
                            "SELECT Actions from SUPPORT_DEVS where serialId = '%s'",
                            serialId.value);

            if (action.len)
            {
                json_object *actionsObject = VR_(create_json_object)(action.value);
                if (!actionsObject)
                {
                    speak_file_into_database(ID);
                    goto speakDone;
                }
                json_object *showUpObj = NULL;
                json_object_object_get_ex(actionsObject, ST_SHOW_UP, &showUpObj);
                if (!showUpObj)
                {
                    json_object_put(actionsObject);
                    speak_file_into_database(ID);
                    goto speakDone;
                }
                const char *showUpValue = json_object_get_string(showUpObj);
                if((strcmp(showUpValue, "false") == 0))
                {
                    json_object_put(showUpObj);
                    json_object_put(actionsObject);
                    goto speakDone;
                }
                else
                {
                    speak_file_into_database(ID);
                    json_object_put(showUpObj);
                    json_object_put(actionsObject);
                }
            }
speakDone:        FREE_SEARCH_DATA_VAR(action);
        }
        else
        {
            SLOGI("not found serialId\n");
        }
        FREE_SEARCH_DATA_VAR(serialId);     
    }
}

/*remove device from SUB_DEVICES, CAPABILITY, cloud, DEVICES database  --> OK
 remove device from feature not start*/
void remove_zigbee_dev(char *ID, uint16_t oldRealDevId16bit)
{
    if (!ID)
    {
        return;
    }
    char oldParentDevId[SIZE_32B];
    zigbee_dev_info_t *zigbee_dev = get_zigbee_dev_from_id(ID);
    if (!zigbee_dev)
    {
        zigbee_dev = create_zigbee_dev_from_id(ID);
    }

    if (!zigbee_dev)//can't found device
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(childrendId);
    searching_database(_VR_CB_(zigbee), &childrendId,
                       "SELECT childrenId from SUB_DEVICES where id='%s'",
                       ID);
    if (childrendId.len) //device has children
    {
        printf("device has children\n");
        // SLOGI("has childrendId\n");
        char *tok, *save_tok;
        tok = strtok_r(childrendId.value, ",", &save_tok);
        while (tok != NULL)
        {
            char childAppDevId[SIZE_32B], oldChildDevId[SIZE_32B];
            strcpy(childAppDevId, tok);
            zigbee_dev_info_t *zigbee_child_dev = get_zigbee_dev_from_id(childAppDevId);
            if (!zigbee_child_dev)
            {
                zigbee_child_dev = create_zigbee_dev_from_id(childAppDevId);
            }
            if (zigbee_child_dev)
            {
                sprintf(oldChildDevId, "%02X%04X", zigbee_child_dev->endpointNum, oldRealDevId16bit);
                deleteEUI64(oldChildDevId);
            }
            // speak_dev_has_beeen_reset(childAppDevId);
            remove_zigbee_dev_in_database_and_group(childAppDevId, zigbee_dev);
            remove_zigbee_dev_in_cloud(childAppDevId);
            remove_zigbee_dev_from_id(childAppDevId); //remove in link list
            VR_(cancel_alarm)(NULL, ID);
            remove_voice_inform_from_id(ST_ZIGBEE, childAppDevId);
            tok = strtok_r(NULL, ",", &save_tok);
        }

        //remove parent
        sprintf(oldParentDevId, "%02X%04X", zigbee_dev->endpointNum, oldRealDevId16bit);
        deleteEUI64(oldParentDevId);

        // speak_dev_has_beeen_reset(ID);
        remove_zigbee_dev_in_database_and_group(ID, zigbee_dev);
        remove_zigbee_dev_in_cloud(ID);
        remove_zigbee_dev_from_id(ID); //remove in link list
        VR_(cancel_alarm)(NULL, ID);
        remove_voice_inform_from_id(ST_ZIGBEE, ID);
    }
    else
    {
        /*dont have children, maybe is parrent(without child) or children*/
        printf("%s\n", "device doesn't have children, maybe is parrent(without child) or children");
        SEARCH_DATA_INIT_VAR(parentIdData);
        searching_database(_VR_CB_(zigbee), &parentIdData,
                           "SELECT parentId from SUB_DEVICES where id='%s'",
                           ID);
        if (parentIdData.len)
        {
            if (!strcmp(parentIdData.value, PARENT_ID_NUM)) // this is parent
            {
                printf("%s\n", "device is parent");
                sprintf(oldParentDevId, "%02X%04X", zigbee_dev->endpointNum, oldRealDevId16bit);
                deleteEUI64(oldParentDevId);

                // speak_dev_has_beeen_reset(ID);
                remove_zigbee_dev_in_database_and_group(ID, zigbee_dev);
                remove_zigbee_dev_in_cloud(ID);
                remove_zigbee_dev_from_id(ID); //remove in link list
                VR_(cancel_alarm)(NULL, ID);
                remove_voice_inform_from_id(ST_ZIGBEE, ID);
            }
            else /*we decided to remove all devices if remove a child*/
            {
                /*this is children device*/
                /*in case parentDev is not exist*/
                printf("%s\n", "this is children device, parentDev != '0'");
                sprintf(oldParentDevId, "%02X%04X", zigbee_dev->endpointNum, oldRealDevId16bit);
                deleteEUI64(oldParentDevId);

                // speak_dev_has_beeen_reset(ID);
                remove_zigbee_dev_in_database_and_group(ID, zigbee_dev);
                remove_zigbee_dev_in_cloud(ID);
                remove_zigbee_dev_from_id(ID); //remove in link list
                remove_zigbee_dev(parentIdData.value, oldRealDevId16bit);
                VR_(cancel_alarm)(NULL, ID);
                remove_voice_inform_from_id(ST_ZIGBEE, ID);
                /*remove all parent of device*/
            }
        }
        else // delete parent, device is not exist in database
        {
            printf("%s\n", "parentIdData is null");
            sprintf(oldParentDevId, "%02X%04X", zigbee_dev->endpointNum, oldRealDevId16bit);
            deleteEUI64(oldParentDevId);

            // speak_dev_has_beeen_reset(ID);
            remove_zigbee_dev_in_database_and_group(ID, zigbee_dev);
            remove_zigbee_dev_in_cloud(ID);
            remove_zigbee_dev_from_id(ID); //remove in link list
            VR_(cancel_alarm)(NULL, ID);
            remove_voice_inform_from_id(ST_ZIGBEE, ID);
        }
        FREE_SEARCH_DATA_VAR(parentIdData);
    }
    FREE_SEARCH_DATA_VAR(childrendId);
}

void remove_device_thread(void *data)
{
    if (!data)
    {
        return;
    }

    rejoin_message_t *rejoin_message = (rejoin_message_t *)data;
    remove_zigbee_dev(rejoin_message->appDeviceId, rejoin_message->oldRealDevId16bit);
    SAFE_FREE(rejoin_message);
}

void zigbee_removing_handler(struct ubus_context *ctx, char* appDeviceId,
                             json_object *jobj, int *sending_zigbee_notify, uint16_t oldRealDevId16bit)
{
    if (!jobj)
    {
        SLOGE("missing jobj\n");
        return;
    }

    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_REMOVE_DEV));
    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_REMOVE_DONE));
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(appDeviceId));
    if(g_open_network)
        VR_(inform_found_device)((void *)ctx);

    if (appDeviceId)
    {
        rejoin_message_t *rejoin_message = (rejoin_message_t *)malloc(sizeof(rejoin_message_t));
        memset(rejoin_message, 0x00, sizeof(rejoin_message_t));

        rejoin_message->oldRealDevId16bit = oldRealDevId16bit;
        memcpy(rejoin_message->appDeviceId, appDeviceId, MAX_ID_SIZE);

        pthread_t remove_device_thread_t;
        pthread_create(&remove_device_thread_t, NULL, (void *)&remove_device_thread, (void *)rejoin_message);
        pthread_detach(remove_device_thread_t);
    }
}

void zigbee_removing_handler_prepare(struct ubus_context *ctx, char* appDeviceId,
                             json_object *jobj, int *sending_zigbee_notify, uint16_t oldRealDevId16bit)
{
    //found device from SUPPORT_DEVS
    if(STR_LEN_ZIGBEE_DEVICE_ID_NEW_VER == strlen(appDeviceId))
    {//deviceId is 6 digits
        zigbee_removing_handler(ctx, appDeviceId, jobj, sending_zigbee_notify, oldRealDevId16bit);
    }
    else
    {
        SEARCH_DATA_INIT_VAR(profileIdExist);
        searching_database(_VR_CB_(zigbee), &profileIdExist,
                        "SELECT profileId from SUB_DEVICES where id='%s'",
                        appDeviceId);
        if(!profileIdExist.len)
        {
            FREE_SEARCH_DATA_VAR(profileIdExist);
            return;
        }
        //update realDeviceId
        zigbee_removing_handler(ctx, appDeviceId, jobj, sending_zigbee_notify, (uint16_t)(htoi(profileIdExist.value)));
        FREE_SEARCH_DATA_VAR(profileIdExist);
    }
}
//update device when receive device-rejoin
/*remove device from SUB_DEVICES, CAPABILITY, cloud, DEVICES database  --> OK
 remove device from feature not start*/
void rejoin_zigbee_dev(bool rejoinNotify, uint16_t oldRealDevId16bit, char* appDeviceId, char* newDeviceId16bit)
{
    if (!appDeviceId)
    {
        return;
    }

    zigbee_dev_info_t *zigbee_dev = get_zigbee_dev_from_id(appDeviceId);
    if (!zigbee_dev)
    {
        zigbee_dev = create_zigbee_dev_from_id(appDeviceId);
    }
    if (!zigbee_dev)
    {
        return;
    }
    SEARCH_DATA_INIT_VAR(childrendId);
    searching_database(_VR_CB_(zigbee), &childrendId,
                       "SELECT childrenId from SUB_DEVICES where id='%s'",
                       appDeviceId);
    if (childrendId.len) //device has children
    {
        printf("device has children\n");
        // SLOGI("has childrendId\n");
        char *tok, *save_tok;
        tok = strtok_r(childrendId.value, ",", &save_tok);
        while (tok != NULL)
        {
            char childAppDevId[SIZE_32B], newChildDevId[SIZE_32B], oldChildDevId[SIZE_32B];
            strcpy(childAppDevId, tok);
            zigbee_dev_info_t *zigbee_child_dev = get_zigbee_dev_from_id(childAppDevId);
            if (!zigbee_child_dev)
            {
                zigbee_child_dev = create_zigbee_dev_from_id(childAppDevId);
            }
            if (!zigbee_child_dev)
            {
                FREE_SEARCH_DATA_VAR(childrendId);
                return;
            }
            sprintf(oldChildDevId, "%02X%04X", zigbee_child_dev->endpointNum, oldRealDevId16bit);
            deleteEUI64(oldChildDevId);
            if(rejoinNotify)
            {
                sprintf(newChildDevId, "%02X%s", zigbee_child_dev->endpointNum, newDeviceId16bit);
                SLOGI("update database, update appDeviceId (%s) to new real device Id: %s\n", childAppDevId, newChildDevId);
                database_actions(_VR_(zigbee), "UPDATE SUB_DEVICES SET profileId='%s' "
                                                "WHERE id='%s'",
                                    newChildDevId, childAppDevId);
                zigbee_child_dev->localId = htoi(newChildDevId);
                memcpy(zigbee_child_dev->appDeviceId, childAppDevId, strlen(childAppDevId));
                insertEUI64(newChildDevId, childAppDevId);
            }
            tok = strtok_r(NULL, ",", &save_tok);
        }

        //replace parent
        char newParentDevId24Bit[SIZE_32B], oldParentDevId[SIZE_32B];
        sprintf(oldParentDevId, "%02X%04X", zigbee_dev->endpointNum, oldRealDevId16bit);
        deleteEUI64(oldParentDevId);
        if(rejoinNotify)
        {
            sprintf(newParentDevId24Bit, "%02X%s", zigbee_dev->endpointNum, newDeviceId16bit);
            SLOGI("update database, update appDeviceId (%s) to new real device Id: %s\n\n", appDeviceId, newParentDevId24Bit);
            database_actions(_VR_(zigbee), "UPDATE SUB_DEVICES SET profileId='%s' "
                                                "WHERE id='%s'",
                                    newParentDevId24Bit, appDeviceId);
            zigbee_dev->localId = htoi(newParentDevId24Bit);
            memcpy(zigbee_dev->appDeviceId, appDeviceId, strlen(appDeviceId));
            insertEUI64(newParentDevId24Bit, appDeviceId);
        }

    }
    else
    {
        /*dont have children, maybe is parrent(without child) or children*/
        printf("%s\n", "device doesn't have children, maybe is parrent(without child) or children");
        SEARCH_DATA_INIT_VAR(parentIdData);
        searching_database(_VR_CB_(zigbee), &parentIdData,
                           "SELECT parentId from SUB_DEVICES where id='%s'",
                           appDeviceId);
        if (parentIdData.len)
        {
            if (!strcmp(parentIdData.value, PARENT_ID_NUM)) // this is parent
            {
                printf("%s\n", "device is parent");
                //replace parent
                char newParentDevId24Bit[SIZE_32B], oldParentDevId[SIZE_32B];
                sprintf(oldParentDevId, "%02X%04X", zigbee_dev->endpointNum, oldRealDevId16bit);
                deleteEUI64(oldParentDevId);
                if(rejoinNotify)
                {
                    sprintf(newParentDevId24Bit, "%02X%s", zigbee_dev->endpointNum, newDeviceId16bit);

                    SLOGI("update database, update appDeviceId (%s) to new real device Id: %s\n\n", appDeviceId, newParentDevId24Bit);
                    database_actions(_VR_(zigbee), "UPDATE SUB_DEVICES SET profileId='%s' "
                                                        "WHERE id='%s'",
                                            newParentDevId24Bit, appDeviceId);
                    zigbee_dev->localId = htoi(newParentDevId24Bit);
                    memcpy(zigbee_dev->appDeviceId, appDeviceId, strlen(appDeviceId));
                    insertEUI64(newParentDevId24Bit, appDeviceId);
                }

            }
            else /*we decided to remove all devices if remove a child*/
            {
                /*this is children device*/
                /*in case parentDev is not exist*/
                printf("%s\n", "this is children device, parentDev != '0'");
                char newParentDevId24Bit[SIZE_32B], oldParentDevId[SIZE_32B];
                sprintf(oldParentDevId, "%02X%04X", zigbee_dev->endpointNum, oldRealDevId16bit);
                deleteEUI64(oldParentDevId);
                if(rejoinNotify)
                {
                    sprintf(newParentDevId24Bit, "%02X%s", zigbee_dev->endpointNum, newDeviceId16bit);
                    SLOGI("update database, update appDeviceId (%s) to new real device Id: %s\n\n", appDeviceId, newParentDevId24Bit);
                    database_actions(_VR_(zigbee), "UPDATE SUB_DEVICES SET profileId='%s' "
                                                        "WHERE id='%s'",
                                            newParentDevId24Bit, appDeviceId);
                    zigbee_dev->localId = htoi(newParentDevId24Bit);
                    memcpy(zigbee_dev->appDeviceId, appDeviceId, strlen(appDeviceId));
                    insertEUI64(newParentDevId24Bit, appDeviceId);
                }

                rejoin_zigbee_dev(rejoinNotify, oldRealDevId16bit, parentIdData.value, newDeviceId16bit);
                /*remove all parent of device*/
            }
        }
        else // delete parent, device is not exist in database
        {
            printf("%s\n", "parentIdData is null");
            char newParentDevId24Bit[SIZE_32B], oldParentDevId[SIZE_32B];
            sprintf(oldParentDevId, "%02X%04X", zigbee_dev->endpointNum, oldRealDevId16bit);
            deleteEUI64(oldParentDevId);
            if(rejoinNotify)
            {
                sprintf(newParentDevId24Bit, "%02X%s", zigbee_dev->endpointNum, newDeviceId16bit);

                SLOGI("update database, update appDeviceId (%s) to new real device Id: %s\n\n", appDeviceId, newParentDevId24Bit);
                database_actions(_VR_(zigbee), "UPDATE SUB_DEVICES SET profileId='%s' "
                                                    "WHERE id='%s'",
                                        newParentDevId24Bit, appDeviceId);
                zigbee_dev->localId = htoi(newParentDevId24Bit);
                memcpy(zigbee_dev->appDeviceId, appDeviceId, strlen(appDeviceId));
                insertEUI64(newParentDevId24Bit, appDeviceId);
            }

        }
        FREE_SEARCH_DATA_VAR(parentIdData);
    }
    FREE_SEARCH_DATA_VAR(childrendId);
}

void remove_zigbee_dev_and_hash_table_prepare(char* appDeviceId)
{
    //found device from SUPPORT_DEVS
    if(STR_LEN_ZIGBEE_DEVICE_ID_NEW_VER == strlen(appDeviceId))
    {//deviceId is 6 digits
        remove_zigbee_dev(appDeviceId, (uint16_t)(htoi(appDeviceId)));
    }
    else
    {
        SEARCH_DATA_INIT_VAR(profileIdExist);
        searching_database(_VR_CB_(zigbee), &profileIdExist,
                        "SELECT profileId from SUB_DEVICES where id='%s'",
                        appDeviceId);
        if(!profileIdExist.len)
        {
            FREE_SEARCH_DATA_VAR(profileIdExist);
            return;
        }
        //update realDeviceId
        remove_zigbee_dev(appDeviceId, (uint16_t)(htoi(profileIdExist.value)));
        FREE_SEARCH_DATA_VAR(profileIdExist);
    }
}

void rejoin_device_thread(void *data)
{
    if (!data)
    {
        return;
    }

    rejoin_message_t *rejoin_message = (rejoin_message_t *)data;
    rejoin_zigbee_dev(rejoin_message->rejoinNotify, rejoin_message->oldRealDevId16bit, rejoin_message->appDeviceId, rejoin_message->newDeviceId16bit);
    SAFE_FREE(rejoin_message);
}

void zigbee_rejoin_handler(bool rejoinNotify, uint16_t oldRealDevId16bit, char* appDeviceId, char* newDeviceId16bit)
{
    if (appDeviceId)
    {
        rejoin_message_t *rejoin_message = (rejoin_message_t *)malloc(sizeof(rejoin_message_t));
        memset(rejoin_message, 0x00, sizeof(rejoin_message_t));

        rejoin_message->rejoinNotify = rejoinNotify;
        rejoin_message->oldRealDevId16bit = oldRealDevId16bit;
        memcpy(rejoin_message->appDeviceId, appDeviceId, MAX_ID_SIZE);
        memcpy(rejoin_message->newDeviceId16bit, newDeviceId16bit, MAX_ID_SIZE);
        pthread_t rejoin_device_thread_t;
        pthread_create(&rejoin_device_thread_t, NULL, (void *)&rejoin_device_thread, (void *)rejoin_message);
        pthread_detach(rejoin_device_thread_t);
    }
}

////////////////////////// For handle message from lib zigbee-gecko //////////////////////////////

static void zigbee_notify_active_cb(struct uloop_timeout *timeout)
{
    NOTIFY_ZIGBEE_TX_BUFFER_T pTxNotify;
    int sending_zigbee_notify = 1;
    int update_last_work = 1;
    char ID[MAX_ID_SIZE]={0};

    if (g_zigbee_notify.notify_index > 0)
    {
        printf("g_zigbee_notify.notify[0].notify_status: %d\n", g_zigbee_notify.notify[0].notify_status);
        char euiAddr[SIZE_32B];

        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));

        if (g_zigbee_notify.notify[0].notify_status == NEWNODE_NOTIFY)
        {
            memcpy((uint8_t *)&pTxNotify.newNodeNotify, g_zigbee_notify.notify[0].notify_message, sizeof(nodeDescriptor));
            sprintf(euiAddr, "%016llX", pTxNotify.newNodeNotify.euiAddr);
            SEARCH_DATA_INIT_VAR(deviceExist);
            searching_database(_VR_CB_(zigbee), &deviceExist,
                               "SELECT id from SUB_DEVICES where serial='%s'",
                               euiAddr);
            if (deviceExist.len)
            { //remove device from SUB_DEVICES
                SLOGE("device Exist into databse\n");

                json_object *jobjRemove = json_object_new_object();
                json_object_object_add(jobjRemove, ST_NOTIFY_TYPE, json_object_new_string(ST_REMOVE_DEV));
                json_object_object_add(jobjRemove, ST_STATUS, json_object_new_string(ST_REMOVE_DONE));
                json_object_object_add(jobjRemove, ST_DEVICE_ID, json_object_new_string(deviceExist.value));
                Send_ubus_notify((char *)json_object_to_json_string(jobjRemove));
                json_object_put(jobjRemove);
                remove_zigbee_dev_and_hash_table_prepare(deviceExist.value);
            }
            FREE_SEARCH_DATA_VAR(deviceExist);
            zigbee_adding_handler(ctx, jobj, pTxNotify, &sending_zigbee_notify);

            update_last_work = 0;
        }
        else if (g_zigbee_notify.notify[0].notify_status == NODELEFT_NOTIFY)
        {
            memcpy((uint8_t *)&pTxNotify.nodeLeftNotify, g_zigbee_notify.notify[0].notify_message, sizeof(nodeLeft));
            sprintf(euiAddr, "%016llX", pTxNotify.nodeLeftNotify.euiAddr);
            SEARCH_DATA_INIT_VAR(deviceExist);
            searching_database(_VR_CB_(zigbee), &deviceExist,
                               "SELECT id from SUB_DEVICES where serial='%s'",
                               euiAddr);
            if (deviceExist.len)
            { //remove device from SUPPORT_DEVS
                /*in this case, we not guarantee status sleepy device after adding will be correct.*/
                zigbee_removing_handler_prepare(ctx, deviceExist.value, jobj, &sending_zigbee_notify, pTxNotify.nodeLeftNotify.destAddr);
            }
            FREE_SEARCH_DATA_VAR(deviceExist);

            update_last_work = 0;
        }
        else if (g_zigbee_notify.notify[0].notify_status == NODE_REJOIN_NOTIFY)
        {
            memcpy((uint8_t *)&pTxNotify.nodeLeftNotify, g_zigbee_notify.notify[0].notify_message, sizeof(nodeLeft));
            sprintf(euiAddr, "%016llX", pTxNotify.nodeLeftNotify.euiAddr);
            sprintf(ID, "%04X", pTxNotify.nodeLeftNotify.destAddr);
            SEARCH_DATA_INIT_VAR(deviceExist);
            searching_database(_VR_CB_(zigbee), &deviceExist,
                               "SELECT id from SUB_DEVICES where serial='%s'",
                               euiAddr);
            if(!deviceExist.len)
            {
                FREE_SEARCH_DATA_VAR(deviceExist);
                goto notSendData;
            }
            //found device from SUPPORT_DEVS
            if(STR_LEN_ZIGBEE_DEVICE_ID_NEW_VER == strlen(deviceExist.value))
            {//deviceId is 6 digits
                if((uint16_t)(htoi(deviceExist.value)) != pTxNotify.nodeLeftNotify.destAddr)
                {
                    //realDeviceId of device is changed
                    zigbee_rejoin_handler(true, (uint16_t)(htoi(deviceExist.value)), deviceExist.value, ID);
                }
            }
            else
            {
                SEARCH_DATA_INIT_VAR(profileIdExist);
                searching_database(_VR_CB_(zigbee), &profileIdExist,
                               "SELECT profileId from SUB_DEVICES where serial='%s'",
                               euiAddr);
                if(!profileIdExist.len)
                {
                    FREE_SEARCH_DATA_VAR(profileIdExist);
                    goto notSendData;
                }
                //update realDeviceId
                if((uint16_t)(htoi(profileIdExist.value)) != pTxNotify.nodeLeftNotify.destAddr)
                {
                    //realDeviceId of device is changed
                    zigbee_rejoin_handler(true, (uint16_t)(htoi(profileIdExist.value)), deviceExist.value, ID);
                }
                FREE_SEARCH_DATA_VAR(profileIdExist);
            }
            FREE_SEARCH_DATA_VAR(deviceExist);

            update_last_work = 0;
        }
        else if (g_zigbee_notify.notify[0].notify_status == REPORT_ATTR_NOTIFY)
        {
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_REPORT_ATTRIBUTE));
            memcpy((uint8_t *)&pTxNotify.reportAttriteNotify, g_zigbee_notify.notify[0].notify_message, sizeof(readAttrData));
            char tmp[MAX_ID_SIZE], tempType[MAX_ID_SIZE], tempData[MAX_TMP_SIZE];
            sprintf(tmp, "%04X", pTxNotify.reportAttriteNotify.devSourceDesc.destAddr);
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(tmp));
            sprintf(tmp, "%04X", pTxNotify.reportAttriteNotify.devSourceDesc.clusterId);
            json_object_object_add(jobj, ST_CLUSTER_ID, json_object_new_string(tmp));
            sprintf(tmp, "%04X", pTxNotify.reportAttriteNotify.attributeID);
            json_object_object_add(jobj, ST_ATTRIBUTE_ID, json_object_new_string(tmp));
            sprintf(tempType, "%02X", pTxNotify.reportAttriteNotify.type);
            json_object_object_add(jobj, ST_DATA_TYPE, json_object_new_string(tempType));
            if (pTxNotify.reportAttriteNotify.type == ZCL_TYPE_CHAR_STRING)
                json_object_object_add(jobj, ST_DATA, json_object_new_string(pTxNotify.reportAttriteNotify.string_data));
            else
            {
                sprintf(tempData, "%08X", pTxNotify.reportAttriteNotify.dataNumber);
                json_object_object_add(jobj, ST_DATA, json_object_new_string(tempData));
            }

            update_last_work = 0;
        }
        else if (g_zigbee_notify.notify[0].notify_status == IAS_NOTIFICATION_NOTIFY)
        {
            memcpy((uint8_t *)&pTxNotify.iasNotificationNotify, g_zigbee_notify.notify[0].notify_message, sizeof(iasNotification));
            sprintf(ID, "%02X%04X", pTxNotify.iasNotificationNotify.devSourceDesc.endpoint, pTxNotify.iasNotificationNotify.devSourceDesc.destAddr);
            CHECK_ITEM_OBJECT_EXIST(ID, notSendData);
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CMD_CLASS_ZPC));
            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ALARM));
            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
            alarm_zigbee_report_final(ctx, jobj, ID, pTxNotify.iasNotificationNotify.zoneStatusNotify);
            remove_dev_adding_actions(ID, jobj);
        }
        else if (g_zigbee_notify.notify[0].notify_status == DOOR_LOCK_RESPONSE_NOTIFY)
        {
            memcpy((uint8_t *)&pTxNotify.doorLockNotify, g_zigbee_notify.notify[0].notify_message, sizeof(doorLockCmdResponse));

            sprintf(ID, "%02X%04X", pTxNotify.doorLockNotify.devSourceDesc.endpoint, pTxNotify.doorLockNotify.devSourceDesc.destAddr);
            CHECK_ITEM_OBJECT_EXIST(ID, notSendData);

            char tmp[SIZE_32B];
            memset(tmp, 0x00, sizeof(tmp));
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CMD_CLASS_ZPC));
            switch (pTxNotify.doorLockNotify.cmd)
            {
                case ZCL_DOOR_LOCK_CMD_SET_PIN_CODE_RESPONSE:
                {
                    char userId[DEV_ID_SIZE];
                    json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_USER_CODE));
                    json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                    char *name = remove_user_code_cmd(ID, pTxNotify.doorLockNotify.setPinCodeRsp.user_id);
                    if (!pTxNotify.doorLockNotify.setPinCodeRsp.status)
                    {
                        sprintf(userId, "%d", pTxNotify.doorLockNotify.setPinCodeRsp.user_id);

                        json_object_object_add(jobj, ST_USER_ID, json_object_new_int(pTxNotify.doorLockNotify.setPinCodeRsp.user_id));
                        json_object_object_add(jobj, ST_USER_ID_STATUS, json_object_new_int(USER_STATUS_OCCUPIED_ENABLED));
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                        json_object_object_add(jobj, ST_ALARM_TYPE_FINAL, json_object_new_string(ST_ADD_NEW_USER));
                        if(!name)
                        {
                            name=strdup(ST_MANUAL);
                        }
                        add_user_code(ID, userId, name);
                    }
                    else if(SET_PIN_CODE_DUPLICATE_USER_CODE == pTxNotify.doorLockNotify.setPinCodeRsp.status)
                    {
                        json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ALARM));
                        json_object_object_add(jobj, ST_ALARM_TYPE_FINAL, json_object_new_string(ST_DUPLICATE_USER_CODE));
                        json_object_object_add(jobj, ST_USER_ID, json_object_new_int(pTxNotify.doorLockNotify.setPinCodeRsp.user_id));
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    }
                    else
                    {
                        //sprintf(userId, "%d", pTxNotify.doorLockNotify.setPinCodeRsp.user_id);
                        json_object_object_add(jobj, ST_USER_ID, json_object_new_int(pTxNotify.doorLockNotify.setPinCodeRsp.user_id));
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    }
                    /*
                    0 = Success
                    1 = General failure
                    2 = Memory full
                    3 = Duplicate Code error
                    */
                    json_object_object_add(jobj, ST_SET_PIN_CODE_RES_STATUS, json_object_new_int(pTxNotify.doorLockNotify.setPinCodeRsp.status));
                    free(name);
                    break;
                }

                case ZCL_DOOR_LOCK_CMD_CLEAR_PIN_CODE_RESPONSE:
                {
                    json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_USER_CODE));
                    json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                    if (!pTxNotify.doorLockNotify.clearPinCodeRsp.status)
                    {
                        //sprintf(tmp, "%04X", pTxNotify.doorLockNotify.clearPinCodeRsp.user_id);
                        json_object_object_add(jobj, ST_USER_ID, json_object_new_int(pTxNotify.doorLockNotify.clearPinCodeRsp.user_id));
                        json_object_object_add(jobj, ST_USER_ID_STATUS, json_object_new_int(USER_STATUS_AVAILABLE));
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                        json_object_object_add(jobj, ST_ALARM_TYPE_FINAL, json_object_new_string(ST_REMOVE_SINGLE_USER));
                        remove_user_code(ID, pTxNotify.doorLockNotify.clearPinCodeRsp.user_id);
                    }
                    else
                    {
                        //sprintf(tmp, "%04X", pTxNotify.doorLockNotify.clearPinCodeRsp.user_id);
                        json_object_object_add(jobj, ST_USER_ID, json_object_new_int(pTxNotify.doorLockNotify.clearPinCodeRsp.user_id));
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    }
                    break;
                }

                case ZCL_DOOR_LOCK_CMD_CLEAR_ALL_PIN_CODE_RESPONSE:
                {
                    json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ALARM));
                    json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                    if (!pTxNotify.doorLockNotify.status)
                    {
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                        json_object_object_add(jobj, ST_ALARM_TYPE_FINAL, json_object_new_string(ST_DELETE_ALL_USERS));
                        remove_all_user_code(ID);
                    }
                    else
                    {
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    }
                    break;
                }

                case ZCL_DOOR_LOCK_CMD_LOCK_DOOR_RESPONSE:
                {
                    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CLOSE_DOOR));
                    if (!pTxNotify.doorLockNotify.status)
                    {
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                    }
                    else
                    {
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    }
                    break;
                }

                case ZCL_DOOR_LOCK_CMD_UNLOCK_DOOR_RESPONSE:
                {
                    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_OPEN_DOOR));
                    if (!pTxNotify.doorLockNotify.status)
                    {
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                    }
                    else
                    {
                        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                    }
                    break;
                }

                case ZCL_DOOR_LOCK_CMD_GET_USER_STATUS_RESPONSE:
                {
                    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_GET_USER_STATUS));
                    char userId[DEV_ID_SIZE] = {0};
                    sprintf(userId, "%d", pTxNotify.doorLockNotify.getUserStatusRsp.user_id);
                    json_object_object_add(jobj, ST_USER_ID, json_object_new_int(pTxNotify.doorLockNotify.getUserStatusRsp.user_id));
                    char *name = remove_user_code_cmd(ID, pTxNotify.doorLockNotify.getUserStatusRsp.user_id);
                    switch (pTxNotify.doorLockNotify.getUserStatusRsp.user_status)
                    {
                        case USER_STATUS_AVAILABLE:
                        {
                            remove_user_code(ID, pTxNotify.doorLockNotify.getUserStatusRsp.user_id);
                            json_object_object_add(jobj, ST_USER_STATUS, json_object_new_string(ST_AVAILABLE));
                            break;
                        }
    
                        case USER_STATUS_OCCUPIED_ENABLED:
                        {
                            if(!name)
                            {
                                name=strdup(ST_MANUAL);
                            }
                            add_user_code(ID, userId, name);
                            json_object_object_add(jobj, ST_USER_STATUS, json_object_new_string(ST_OCCUPIED_ENABLE));
                            break;
                        }
    
                        case USER_STATUS_RESERVED:
                        {
                            remove_user_code(ID, pTxNotify.doorLockNotify.getUserStatusRsp.user_id);
                            json_object_object_add(jobj, ST_USER_STATUS, json_object_new_string(ST_RESERVED));
                            break;
                        }
    
                        case USER_STATUS_OCCUPIED_DISABLED:
                        {
                            if(!name)
                            {
                                name=strdup(ST_MANUAL);
                            }
                            add_user_code(ID, userId, name);
                            json_object_object_add(jobj, ST_USER_STATUS, json_object_new_string(ST_OCCUPIED_DISABLE));
                            break;
                        }
                        default:
                        {
                            json_object_object_add(jobj, ST_USER_STATUS, json_object_new_string(ST_UNKNOWN));
                            break;
                        }
                    }
                    free(name);
                    break;
                }

                case ZCL_DOOR_LOCK_OPERATION_EVENT_NOTIFICATION:
                {
                    /*
                    char codeId[SIZE_64B];
                    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_DOOR_LOCK_OPERATION_EVENT_NOTIFICATION));
                    sprintf(tmp, "%02X", pTxNotify.doorLockNotify.doorLockOperationEv.OperationEvSource);
                    json_object_object_add(jobj, ST_OPERATION_EVENT_SOURCE, json_object_new_string(tmp));
                    sprintf(tmp, "%02X", pTxNotify.doorLockNotify.doorLockOperationEv.OperationEvCode);
                    json_object_object_add(jobj, ST_OPERATION_EVENT_CODE, json_object_new_string(tmp));
                    sprintf(codeId, "%04X", pTxNotify.doorLockNotify.doorLockOperationEv.user_id);
                    json_object_object_add(jobj, ST_USER_ID, json_object_new_string(codeId));
                    sprintf(tmp, "%02X", pTxNotify.doorLockNotify.doorLockOperationEv.pin);
                    json_object_object_add(jobj, ST_PIN, json_object_new_string(tmp));
                    sprintf(tmp, "%08X", pTxNotify.doorLockNotify.doorLockOperationEv.zigbee_local_time);
                    json_object_object_add(jobj, ST_ZIGBEE_LOCAL_TIME, json_object_new_string(tmp));
                    memset(tmp, 0x00, sizeof(tmp));
                    sprintf(tmp, "%02X", pTxNotify.doorLockNotify.doorLockOperationEv.length_data);
                    json_object_object_add(jobj, ST_LENGTH_DATA, json_object_new_string(tmp));
                    json_object_object_add(jobj, ST_DATA,
                                           json_object_new_string(pTxNotify.doorLockNotify.doorLockOperationEv.data));
                                           */
                    uint16_t lockStatus = (pTxNotify.doorLockNotify.doorLockOperationEv.OperationEvSource << FIRST_POS_BIT_OF_HIGH_BYTE) +
                                          pTxNotify.doorLockNotify.doorLockOperationEv.OperationEvCode;
                    const char* alarmPattern;
                    printf("pTxNotify.doorLock.lockStatus = %04X\n", lockStatus);
                    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CMD_CLASS_ZPC));
                    json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ALARM));
                    json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                    json_object_object_add(jobj, ST_USER_ID, json_object_new_int(pTxNotify.doorLockNotify.doorLockOperationEv.user_id));
                    alarmPattern = getAlarmPatternFromEvent(lockStatus);
                    json_object_object_add(jobj, ST_ALARM_TYPE_FINAL, json_object_new_string(alarmPattern));
                    alarm_update_zigbee_database_and_voice_inform(ctx, jobj, ID);
                    /*
                    if (lockStatus == ZIGBEE_MANUAL_UNLOCK_VALUE)
                    {
                        json_object_object_add(jobj, ST_LOCK_STATUS, json_object_new_int(DOOR_OPEN));
                        update_register_device_database(ID, ST_ON_OFF, ST_OPEN_VALUE,
                                                        ST_REPLACE, NULL_ROTATE_NUM,
                                                        MODE_FREQUENTLYLISTENING);
                        update_zigbee_status(_VR_CB_(zigbee), ID, ST_ON_OFF, ST_OPEN_VALUE, ST_REPLACE, NULL_ROTATE_NUM);
                    }
                    else if (lockStatus == ZIGBEE_MANUAL_LOCK_VALUE)
                    {
                        json_object_object_add(jobj, ST_LOCK_STATUS, json_object_new_int(DOOR_CLOSE));
                        update_register_device_database(ID, ST_ON_OFF, ST_CLOSE_VALUE,
                                                        ST_REPLACE, NULL_ROTATE_NUM,
                                                        MODE_FREQUENTLYLISTENING);
                    }
                    */
                    break;
                }
                case ZCL_DOOR_LOCK_PROGRAMMING_EVENT_NOTIFICATION://event when user add/remove user code manual
                {
                    char userId[DEV_ID_SIZE];
                    sprintf(userId, "%d", pTxNotify.doorLockNotify.doorLockOperationEv.user_id);
                    json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ALARM));//notify alarm report to app
                    json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                    json_object_object_add(jobj, ST_USER_ID, json_object_new_int(pTxNotify.doorLockNotify.doorLockOperationEv.user_id));
                    char *name = remove_user_code_cmd(ID, pTxNotify.doorLockNotify.doorLockOperationEv.user_id);
                    switch(pTxNotify.doorLockNotify.doorLockOperationEv.OperationEvCode)
                    {
                        case EVENT_CODES_PIN_CODE_ADDED:
                            json_object_object_add(jobj, ST_ALARM_TYPE_FINAL, json_object_new_string(ST_ADD_NEW_USER));
                            json_object_object_add(jobj, ST_USER_ID_STATUS, json_object_new_int(USER_STATUS_OCCUPIED_ENABLED));
                            if(!name)
                            {
                                name=strdup(ST_MANUAL);
                            }
                            add_user_code(ID, userId, name);
                        break;
                        case EVENT_CODES_PIN_CODE_DELETED:
							if(pTxNotify.doorLockNotify.doorLockOperationEv.user_id != USER_ID_ALL_USER_CODE_DELETED)
							{//remove single user code
								json_object_object_add(jobj, ST_ALARM_TYPE_FINAL, json_object_new_string(ST_REMOVE_SINGLE_USER));
								json_object_object_add(jobj, ST_USER_ID_STATUS, json_object_new_int(USER_STATUS_AVAILABLE));
								remove_user_code(ID, pTxNotify.doorLockNotify.doorLockOperationEv.user_id);
							}
							else//remove all user code
							{
								json_object_object_add(jobj, ST_ALARM_TYPE_FINAL, json_object_new_string(ST_DELETE_ALL_USERS));
								remove_all_user_code(ID);
							}
                        break;
                    }
                    free(name);
                    break;
                }
                case ZCL_DOOR_LOCK_CMD_READ_ATTRIBUTE_RESPONSE:
                {
                    json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_DOOR_LOCK));
                    json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                    uint8_t lockStatus = pTxNotify.doorLockNotify.doorLockAttriResponse.lockStatus;
                    if (DOOR_LOCK_LOCK_STATE_LOCKED == lockStatus)
                    {
                        lockStatus = DOOR_CLOSE;
                        update_zigbee_status(_VR_CB_(zigbee), ID, ST_ON_OFF, ST_CLOSE_VALUE, ST_REPLACE, NULL_ROTATE_NUM);
                    }
                    else if (DOOR_LOCK_LOCK_STATE_UNLOCKED == lockStatus)
                    {
                        lockStatus = DOOR_OPEN;
                        update_zigbee_status(_VR_CB_(zigbee), ID, ST_ON_OFF, ST_OPEN_VALUE, ST_REPLACE, NULL_ROTATE_NUM);
                    }
                    else if (DOOR_LOCK_LOCK_STATE_NOT_FULLY_LOCKED == lockStatus)//Not fully locked alarm
                    {
                        update_zigbee_status(_VR_CB_(zigbee), ID, ST_ON_OFF, ST_OPEN_VALUE, ST_REPLACE, NULL_ROTATE_NUM);
                        lockStatus = DOOR_OPEN;
                        json_object_object_add(jobj, ST_ALARM_TYPE_FINAL, json_object_new_string(ST_STUCKINLOCK));
                        alarm_update_zigbee_database_and_voice_inform(ctx, jobj, ID);
                    }
                    json_object_object_add(jobj, ST_LOCK_STATUS, json_object_new_int(lockStatus));
                    break;
                }
                case ZCL_DOOR_LOCK_CMD_DOOR_STATE_REPORT:
                {
                    //handle lumi door windows sensor
                    zigbee_dev_info_t *tmpDevice = get_zigbee_dev_from_id(ID);
                    if (!tmpDevice)
                    {
                        tmpDevice = create_zigbee_dev_from_id(ID);
                        if (!tmpDevice)
                        {
                            goto notSendData;
                        }
                    }
                    if (!strcmp(tmpDevice->serialId, LUMI_DOOR_WINDOWS_SENSOR_SERIAL_ID))
                    {
                      char tmp[MAX_ID_SIZE];
                        json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_ALARM));
                        json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                        sprintf(tmp, "%02X", pTxNotify.doorLockNotify.doorLockAttriResponse.lockStatus);
                        alarm_zigbee_report_final(ctx, jobj, ID, tmp);
                    }
                    else
                    {
                        json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_DOOR_LOCK));
                        json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                        uint8_t doorState = pTxNotify.doorLockNotify.doorLockAttriResponse.lockStatus;
                        printf("pTxNotify.doorLock.doorState = %02X\n", doorState);
                        json_object_object_add(jobj, ST_DOOR_STATE, json_object_new_int(doorState));
                    }
                    break;
                }
            }
            remove_dev_adding_actions(ID, jobj);
        }
        else if (g_zigbee_notify.notify[0].notify_status == EMBER_ZIGBEE_NETWORK_PANSCAN_NOYIFY)
        {
            memcpy((uint8_t *)&pTxNotify.emberZigbeeNetworkPanScanNotify, g_zigbee_notify.notify[0].notify_message, sizeof(EmberZigbeeNetworkPanScan));
            switch (pTxNotify.emberZigbeeNetworkPanScanNotify.status)
            {
                case WAITING_CHANNEL_SCAN_COMPLETE:
                {
                    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_WAITING_CHANNEL_SCAN_COMPLETE));
                    //zllChannelScanWaitingComplete();
                    break;
                }
                case INTERPAN_CHANNEL_SCAN_COMPLETE:
                {
                    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CHANNEL_SCAN));
                    int i;
                    json_object *jarray = json_object_new_array();
                    for (i = 0; i < pTxNotify.emberZigbeeNetworkPanScanNotify.numberPanId; i++)
                    {
                        char tmp[MAX_ID_SIZE], panId[MAX_ID_SIZE], euiAddr[SIZE_32B];
                        ;
                        json_object *jlist = json_object_new_object();
                        sprintf(tmp, "%02X", pTxNotify.emberZigbeeNetworkPanScanNotify.networkScanHandler[i].channelId);
                        json_object_object_add(jlist, ST_CHANNEL, json_object_new_string(tmp));
                        sprintf(panId, "%04X", pTxNotify.emberZigbeeNetworkPanScanNotify.networkScanHandler[i].panId);
                        json_object_object_add(jlist, ST_PAN_ID, json_object_new_string(panId));
                        sprintf(euiAddr, "%016llX", pTxNotify.emberZigbeeNetworkPanScanNotify.networkScanHandler[i].euiAddr);
                        json_object_object_add(jlist, ST_DES_ADDRESS_64, json_object_new_string(euiAddr));
                        json_object_array_add(jarray, jlist);
                    }
                    json_object_object_add(jobj, ST_CHANNEL_SCAN_LIST, jarray);
                    break;
                }
                default:
                    break;
            }

            update_last_work = 0;
        }
        else if (g_zigbee_notify.notify[0].notify_status == POWER_CONFIGURATION_NOTIFY)
        {
            memcpy((uint8_t *)&pTxNotify.powerConfigurationReport, g_zigbee_notify.notify[0].notify_message, sizeof(powerConfiguration_t));
            sprintf(ID, "%02X%04X", pTxNotify.powerConfigurationReport.endpoint, pTxNotify.powerConfigurationReport.addr);
            CHECK_ITEM_OBJECT_EXIST(ID, notSendData);
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CMD_CLASS_ZPC));
            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_BATTERY));
            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));

            char changeChannel[MAX_EP_SIZE];
            
            if(dev_specific_change_channel_report(ID, changeChannel))
            {
                char tmpParentId[MAX_ID_SIZE];
                sprintf(tmpParentId, "%02X%s", htoi(changeChannel), (char*)&ID[POS_SERIAL_INTO_APP_DEVICE_ID]);
                memset(ID, 0x00, MAX_ID_SIZE);
                sprintf(ID, "%s", tmpParentId);
                json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            }
            if(pTxNotify.powerConfigurationReport.battery_level > DEFAULT_MAX_PERCENT)
                pTxNotify.powerConfigurationReport.battery_level = DEFAULT_MAX_PERCENT;
            json_object_object_add(jobj, ST_BATTERY_LEVEL, json_object_new_int(pTxNotify.powerConfigurationReport.battery_level));

            char tmp[SIZE_32B];
            sprintf(tmp, "%d", pTxNotify.powerConfigurationReport.battery_level);

            set_register_database(_VR_CB_(zigbee), ID, ST_BATTERY_LEVEL, tmp, ST_REPLACE, 0);
            remove_dev_adding_actions(ID, jobj);
        }
        else if (g_zigbee_notify.notify[0].notify_status == TEMPERATURE_MEASUREMENT_NOTIFY)
        {
            memcpy((uint8_t *)&pTxNotify.temperatureMeasurementNotify, g_zigbee_notify.notify[0].notify_message, sizeof(temperatureMeasurement_t));
            sprintf(ID, "%02X%04X", pTxNotify.temperatureMeasurementNotify.endpoint, pTxNotify.temperatureMeasurementNotify.addr);
            CHECK_ITEM_OBJECT_EXIST(ID, notSendData);
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CMD_CLASS_ZPC));
            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SENSOR_MULTILEVEL));
            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
            json_object_object_add(jobj, ST_SENSOR_PATTERN, json_object_new_string(ST_MULTILEVEL_SENSOR_AIR_TEMPERATURE));

            char valueStr[SIZE_32B];
            int sensorDiv;
            char changeChannel[MAX_EP_SIZE];
            dev_specific_sensor_div(ID, &sensorDiv);
            if(dev_specific_change_channel_report(ID, changeChannel))
            {
                char tmpParentId[MAX_ID_SIZE];
                sprintf(tmpParentId, "%02X%s", htoi(changeChannel), (char*)&ID[POS_SERIAL_INTO_APP_DEVICE_ID]);
                memset(ID, 0x00, MAX_ID_SIZE);
                sprintf(ID, "%s", tmpParentId);
                json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            }
            if(sensorDiv)
            {
                sprintf(valueStr, "%.3f", (float)pTxNotify.temperatureMeasurementNotify.temperatureLevel / sensorDiv);
            }
            else
            {
                sprintf(valueStr, "%.3f", (float)pTxNotify.temperatureMeasurementNotify.temperatureLevel / INT16_T_DATA_DIV);
            }
            json_object_object_add(jobj, ST_UNIT, json_object_new_string(ST_MULTILEVEL_SENSOR_LABEL_CELCIUS));
            json_object_object_add(jobj, ST_VALUE, json_object_new_string(valueStr));

            set_sensor_value_database(_VR_CB_(zigbee), ID, ST_MULTILEVEL_SENSOR_AIR_TEMPERATURE,
                                    valueStr, ST_MULTILEVEL_SENSOR_LABEL_CELCIUS);
            remove_dev_adding_actions(ID, jobj);
        }
        else if (g_zigbee_notify.notify[0].notify_status == ZCL_LEVEL_NOTIFY)
        {
            memcpy((uint8_t *)&pTxNotify.zclLevelNotify, g_zigbee_notify.notify[0].notify_message, sizeof(zclLevel_t));
            sprintf(ID, "%02X%04X", pTxNotify.zclLevelNotify.endpoint, pTxNotify.zclLevelNotify.addr);
            CHECK_ITEM_OBJECT_EXIST(ID, notSendData);
            char tmpLevel[MAX_TMP_SIZE];
            sprintf(tmpLevel, "%d",
                    (uint8_t)round((double)pTxNotify.zclLevelNotify.data * DEFAULT_MAX_PERCENT / DEFAULT_MAX_VALUE_8_BIT));

            update_zigbee_status(_VR_CB_(zigbee), ID, ST_DIM, tmpLevel, ST_REPLACE, 0);
            remove_dev_adding_actions(ID, jobj);
            /**get binary**/
            getOnOffOfDevice(ID);
        }

        else if (g_zigbee_notify.notify[0].notify_status == WINDOW_COVERING_NOTIFY)
        {
            memcpy((uint8_t *)&pTxNotify.zclLevelNotify, g_zigbee_notify.notify[0].notify_message, sizeof(zclLevel_t));
            sprintf(ID, "%02X%04X", pTxNotify.zclLevelNotify.endpoint, pTxNotify.zclLevelNotify.addr);
            CHECK_ITEM_OBJECT_EXIST(ID, notSendData);
            char tmpLevel[MAX_TMP_SIZE];
            sprintf(tmpLevel, "%d", pTxNotify.zclLevelNotify.data);

            update_zigbee_status(_VR_CB_(zigbee), ID, ST_DIM, tmpLevel, ST_REPLACE, 0);
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CMD_CLASS_ZPC));
            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SWITCH_MULTILEVEL));
            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
            json_object_object_add(jobj, ST_VALUE, json_object_new_string(tmpLevel));
            remove_dev_adding_actions(ID, jobj);
            if(pTxNotify.zclLevelNotify.data)
            {
                update_zigbee_status(_VR_CB_(zigbee), ID, ST_ON_OFF, ST_OPEN_VALUE, ST_REPLACE, 0);
            }
            else
            {
                update_zigbee_status(_VR_CB_(zigbee), ID, ST_ON_OFF, ST_CLOSE_VALUE, ST_REPLACE, 0);
            }
            /**get binary**/
            //getOnOffOfDevice(ID);
        }

        else if (g_zigbee_notify.notify[0].notify_status == ZCL_ON_OFF_NOTIFY)
        {
            memcpy((uint8_t *)&pTxNotify.zclOnOffNotify, g_zigbee_notify.notify[0].notify_message, sizeof(zclOnOff_t));
            sprintf(ID, "%02X%04X", pTxNotify.zclOnOffNotify.endpoint, pTxNotify.zclOnOffNotify.addr);
            CHECK_ITEM_OBJECT_EXIST(ID, notSendData);
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CMD_CLASS_ZPC));
            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SWITCH_MULTILEVEL));
            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
            char tmp[MAX_TMP_SIZE], tmpLevel[MAX_TMP_SIZE];
            sprintf(tmp, "%d", pTxNotify.zclOnOffNotify.data);
            zigbee_dev_info_t *tmpDevice = get_zigbee_dev_from_id(ID);
            if (!tmpDevice)
            {
                tmpDevice = create_zigbee_dev_from_id(ID);
                if (!tmpDevice)
                {
                    goto notSendData;
                }
            }
            sprintf(tmpLevel, "%d", tmpDevice->status.dim);

            if (pTxNotify.zclOnOffNotify.data && tmpDevice->status.dim)
                json_object_object_add(jobj, ST_VALUE, json_object_new_string(tmpLevel));
            else
                json_object_object_add(jobj, ST_VALUE, json_object_new_string(tmp)); //device Off

            update_zigbee_status(_VR_CB_(zigbee), ID, ST_ON_OFF, tmp, ST_REPLACE, 0);
            remove_dev_adding_actions(ID, jobj);
        }

        else if (g_zigbee_notify.notify[0].notify_status == HUMIDITY_MEASUREMENT_NOTIFY)
        {
            memcpy((uint8_t *)&pTxNotify.humidityMeasurementNotify, g_zigbee_notify.notify[0].notify_message, sizeof(humidityMeasurement_t));
            sprintf(ID, "%02X%04X", pTxNotify.humidityMeasurementNotify.endpoint, pTxNotify.humidityMeasurementNotify.addr);
            CHECK_ITEM_OBJECT_EXIST(ID, notSendData);
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CMD_CLASS_ZPC));
            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SENSOR_MULTILEVEL));
            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
            json_object_object_add(jobj, ST_SENSOR_PATTERN, json_object_new_string(ST_MULTILEVEL_SENSOR_HUMIDITY));

            char valueStr[SIZE_32B];
            int sensorDiv;
            char changeChannel[MAX_EP_SIZE];
            dev_specific_sensor_div(ID, &sensorDiv);
            
            if(dev_specific_change_channel_report(ID, changeChannel))
            {
                char tmpParentId[MAX_ID_SIZE];
                sprintf(tmpParentId, "%02X%s", htoi(changeChannel), (char*)&ID[POS_SERIAL_INTO_APP_DEVICE_ID]);
                memset(ID, 0x00, MAX_ID_SIZE);
                sprintf(ID, "%s", tmpParentId);
                json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            }
            if(sensorDiv)
            {
                sprintf(valueStr, "%.3f", (float)pTxNotify.humidityMeasurementNotify.humidityLevel / sensorDiv);
            }
            else
            {
                sprintf(valueStr, "%.3f", (float)pTxNotify.humidityMeasurementNotify.humidityLevel / INT16_T_DATA_DIV);
            }

            json_object_object_add(jobj, ST_UNIT, json_object_new_string(ST_MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE));
            json_object_object_add(jobj, ST_VALUE, json_object_new_string(valueStr));

            set_sensor_value_database(_VR_CB_(zigbee), ID, ST_MULTILEVEL_SENSOR_HUMIDITY,
                                    valueStr, ST_MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE);
            remove_dev_adding_actions(ID, jobj);
        }
        else if (g_zigbee_notify.notify[0].notify_status == ILLUMINANCE_MEASUREMENT_NOTIFY)
        {
            /*MeasuredValue = 10,000 x log10 Illuminance + 1*/
            /*humidity and illuminance have the same format*/
            memcpy((uint8_t *)&pTxNotify.humidityMeasurementNotify, g_zigbee_notify.notify[0].notify_message, sizeof(humidityMeasurement_t));
            sprintf(ID, "%02X%04X", pTxNotify.humidityMeasurementNotify.endpoint, pTxNotify.humidityMeasurementNotify.addr);
            CHECK_ITEM_OBJECT_EXIST(ID, notSendData);
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CMD_CLASS_ZPC));
            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_SENSOR_MULTILEVEL));
            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
            json_object_object_add(jobj, ST_SENSOR_PATTERN, json_object_new_string(ST_MULTILEVEL_SENSOR_LUMINANCE));

            int sensorDiv;
            char changeChannel[MAX_EP_SIZE];
            dev_specific_sensor_div(ID, &sensorDiv);
            
            if(dev_specific_change_channel_report(ID, changeChannel))
            {
                char tmpParentId[MAX_ID_SIZE];
                sprintf(tmpParentId, "%02X%s", htoi(changeChannel), (char*)&ID[POS_SERIAL_INTO_APP_DEVICE_ID]);
                memset(ID, 0x00, MAX_ID_SIZE);
                sprintf(ID, "%s", tmpParentId);
                json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            }
            float measuredValue = (float)pTxNotify.humidityMeasurementNotify.humidityLevel;
            float illuminanceValue = pow(10, ((measuredValue - 1) / INT16_T_DATA_ILLUMINANCE_SENSOR));
            char valueStr[SIZE_32B];
            if(sensorDiv)
            {
                sprintf(valueStr, "%.3f", illuminanceValue * SPECIFIC_ILLUMINANCE_SENSOR / sensorDiv);
            }
            else
            {
                sprintf(valueStr, "%.3f", illuminanceValue * SPECIFIC_ILLUMINANCE_SENSOR / INT16_T_DATA_DIV);
            }

            json_object_object_add(jobj, ST_UNIT, json_object_new_string(ST_MULTILEVEL_SENSOR_LABEL_LUX));
            json_object_object_add(jobj, ST_VALUE, json_object_new_string(valueStr));

            set_sensor_value_database(_VR_CB_(zigbee), ID, ST_MULTILEVEL_SENSOR_LUMINANCE,
                                    valueStr, ST_MULTILEVEL_SENSOR_LABEL_LUX);
            remove_dev_adding_actions(ID, jobj);
        }
        else if (g_zigbee_notify.notify[0].notify_status == ELECTRIC_MEASUREMENT_NOTIFY)
        {
            memcpy((uint8_t *)&pTxNotify.zclElectricMeasurementNotify, g_zigbee_notify.notify[0].notify_message, sizeof(zclElectricMeasurement_t));
            char *meterSupport = NULL;                    
            char valueStr[SIZE_32B];
            int16_t mValue, mMul, mDiv;
            int aCVoltageMultiplier, aCVoltageDivisor;
            int aCCurrentMultiplier, aCCurrentDivisor;
            int aCPowerMultiplier, aCPowerDivisor;
            float realValue;
            sprintf(ID, "%02X%04X", pTxNotify.zclElectricMeasurementNotify.endpoint, pTxNotify.zclElectricMeasurementNotify.addr);
            CHECK_ITEM_OBJECT_EXIST(ID, notSendData);
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CMD_CLASS_ZPC));
            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_METER));
            json_object_object_add(jobj, ST_METER_PATTERN, json_object_new_string(ST_ELECTRIC_METER));

            char changeChannel[MAX_EP_SIZE];
            
            if(dev_specific_change_channel_report(ID, changeChannel))
            {
                char tmpParentId[MAX_ID_SIZE];
                sprintf(tmpParentId, "%02X%s", htoi(changeChannel), (char*)&ID[POS_SERIAL_INTO_APP_DEVICE_ID]);
                memset(ID, 0x00, MAX_ID_SIZE);
                sprintf(ID, "%s", tmpParentId);
                json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            }
            switch(pTxNotify.zclElectricMeasurementNotify.state)
            {
                case ZCL_ELECTRIC_SUPPORT_GET:
                    json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_SUPPORTED_REPORT));

                    get_meter_scale_from_attribute_mask(pTxNotify.zclElectricMeasurementNotify.listAttribute,
                                                       pTxNotify.zclElectricMeasurementNotify.noAttributeSupported,
                                                       &meterSupport);
                    JSON_ADD_STRING_SAFE(jobj, ST_METER_SUPPORT, meterSupport);
                    
                    if(meterSupport)
                    {
                        set_property_database(_VR_CB_(zigbee), ID, ST_METER, ST_ELECTRIC_METER, ST_METER_SUPPORT, meterSupport);
                    }
                    SAFE_FREE(meterSupport);
                    break;
                case ZCL_ELECTRIC_CURRENT:
                    json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                    mValue = getDataFromAttribute(ZCL_RMS_CURRENT_ATTRIBUTE_ID,
                                                pTxNotify.zclElectricMeasurementNotify.listAttribute, 
                                                pTxNotify.zclElectricMeasurementNotify.data, NO_ATTRIBUTE_ID_READ_CURRENT);
                    dev_specific_meter_scale(ID, &aCVoltageMultiplier, &aCVoltageDivisor,
                                            &aCCurrentMultiplier, &aCCurrentDivisor,
                                            &aCPowerMultiplier, &aCPowerDivisor);
                    if(aCCurrentDivisor)
                    {
                        realValue = (float)((float)mValue * aCCurrentMultiplier / aCCurrentDivisor);
                    }
                    else
                    {
                        mMul = getDataFromAttribute(ZCL_AC_CURRENT_MULTIPLIER_ATTRIBUTE_ID,
                                                pTxNotify.zclElectricMeasurementNotify.listAttribute, 
                                                pTxNotify.zclElectricMeasurementNotify.data, NO_ATTRIBUTE_ID_READ_CURRENT);
                        mDiv = getDataFromAttribute(ZCL_AC_CURRENT_DIVISOR_ATTRIBUTE_ID,
                                                pTxNotify.zclElectricMeasurementNotify.listAttribute, 
                                                pTxNotify.zclElectricMeasurementNotify.data, NO_ATTRIBUTE_ID_READ_CURRENT);
                        if(mMul && mDiv)
                        {
                            realValue = (float)((float)mValue * mMul / mDiv);
                        }
                        else
                        {
                            realValue = (float)((float)mValue);
                        }
                    }

                    sprintf(valueStr, "%.3f", realValue);
                    json_object_object_add(jobj, ST_UNIT, json_object_new_string(ST_METER_A));
                    json_object_object_add(jobj, ST_VALUE, json_object_new_string(valueStr));
                    break;
                case ZCL_ELECTRIC_VOLT:
                    json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                    mValue = getDataFromAttribute(ZCL_RMS_VOLTAGE_ATTRIBUTE_ID,
                                                pTxNotify.zclElectricMeasurementNotify.listAttribute, 
                                                pTxNotify.zclElectricMeasurementNotify.data, NO_ATTRIBUTE_ID_READ_VOLT);
                    dev_specific_meter_scale(ID, &aCVoltageMultiplier, &aCVoltageDivisor,
                                            &aCCurrentMultiplier, &aCCurrentDivisor,
                                            &aCPowerMultiplier, &aCPowerDivisor);
                    if(aCVoltageDivisor)
                    {
                        realValue = (float)((float)mValue * aCVoltageMultiplier / aCVoltageDivisor);
                    }
                    else
                    {
                        mMul = getDataFromAttribute(ZCL_AC_VOLTAGE_MULTIPLIER_ATTRIBUTE_ID,
                                                    pTxNotify.zclElectricMeasurementNotify.listAttribute, 
                                                    pTxNotify.zclElectricMeasurementNotify.data, NO_ATTRIBUTE_ID_READ_VOLT);
                        mDiv = getDataFromAttribute(ZCL_AC_VOLTAGE_DIVISOR_ATTRIBUTE_ID,
                                                    pTxNotify.zclElectricMeasurementNotify.listAttribute, 
                                                    pTxNotify.zclElectricMeasurementNotify.data, NO_ATTRIBUTE_ID_READ_VOLT);
                        if(mMul && mDiv)
                        {
                            realValue = (float)((float)mValue * mMul / mDiv);
                        }
                        else
                        {
                            realValue = (float)((float)mValue);
                        } 
                    }
                    sprintf(valueStr, "%.3f", realValue);
                    json_object_object_add(jobj, ST_UNIT, json_object_new_string(ST_METER_V));
                    json_object_object_add(jobj, ST_VALUE, json_object_new_string(valueStr));
                    break;
                case ZCL_ELECTRIC_POWER:
                    json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));
                    mValue = getDataFromAttribute(ZCL_ACTIVE_POWER_ATTRIBUTE_ID,
                                                pTxNotify.zclElectricMeasurementNotify.listAttribute, 
                                                pTxNotify.zclElectricMeasurementNotify.data, NO_ATTRIBUTE_ID_READ_POWER);
                    dev_specific_meter_scale(ID, &aCVoltageMultiplier, &aCVoltageDivisor,
                                            &aCCurrentMultiplier, &aCCurrentDivisor,
                                            &aCPowerMultiplier, &aCPowerDivisor);
                    if(aCPowerDivisor)
                    {
                        realValue = (float)((float)mValue * aCPowerMultiplier / aCPowerDivisor);
                    }
                    else
                    {
                        mMul = getDataFromAttribute(ZCL_AC_POWER_MULTIPLIER_ATTRIBUTE_ID,
                                                    pTxNotify.zclElectricMeasurementNotify.listAttribute, 
                                                    pTxNotify.zclElectricMeasurementNotify.data, NO_ATTRIBUTE_ID_READ_POWER);
                        mDiv = getDataFromAttribute(ZCL_AC_POWER_DIVISOR_ATTRIBUTE_ID,
                                                    pTxNotify.zclElectricMeasurementNotify.listAttribute, 
                                                    pTxNotify.zclElectricMeasurementNotify.data, NO_ATTRIBUTE_ID_READ_POWER);
                        if(mMul && mDiv)
                        {
                            realValue = (float)((float)mValue * mMul / mDiv);
                        }
                        else
                        {
                            realValue = (float)((float)mValue);
                        }
                    }

                    sprintf(valueStr, "%.3f", realValue);
                    json_object_object_add(jobj, ST_UNIT, json_object_new_string(ST_METER_W));
                    json_object_object_add(jobj, ST_VALUE, json_object_new_string(valueStr));
                    break;
            }
            remove_dev_adding_actions(ID, jobj);
        }
        else if (g_zigbee_notify.notify[0].notify_status == DEVICE_ANNOUNCE_NOTIFY)
        {
            memcpy((uint8_t *)&pTxNotify.deviceAnnounceNotify, g_zigbee_notify.notify[0].notify_message, sizeof(deviceAnnounce_t));
            char euiAddr[SIZE_32B];
            sprintf(euiAddr, "%016llX", pTxNotify.deviceAnnounceNotify.euiAddr);
            SEARCH_DATA_INIT_VAR(deviceExist);
            searching_database(_VR_CB_(zigbee), &deviceExist,
                               "SELECT id from SUB_DEVICES where serial='%s'",
                               euiAddr);
            if (deviceExist.len)
            { //remove device from SUB_DEVICES
                SLOGE("device Exist into databse --> get binary of device\n");

                /**get binary**/
                zigbee_command_response_t response;
                memset(&response, 0x00, sizeof(zigbee_command_response_t));
                strcpy(response.method, ST_GET_BINARY_R);
                strcpy(response.nodeid, deviceExist.value);
                _get_binary_prepare(response);
            }
            FREE_SEARCH_DATA_VAR(deviceExist);
            sending_zigbee_notify = 0;
            //Send_ubus_notify((char *)json_object_to_json_string(jobj));
        }

        else if (g_zigbee_notify.notify[0].notify_status == CONFIG_REPORT_NOTIFY)
        {
            memcpy((uint8_t *)&pTxNotify.configReportNotify, g_zigbee_notify.notify[0].notify_message, sizeof(configReport_t));

            sprintf(ID, "%02X%04X", pTxNotify.configReportNotify.devSourceDesc.endpoint, pTxNotify.configReportNotify.devSourceDesc.destAddr);
            CHECK_ITEM_OBJECT_EXIST(ID, notSendData);
            json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
            json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_CMD_CLASS_ZPC));
            json_object_object_add(jobj, ST_CMD_CLASS, json_object_new_string(ST_CONFIGURATION));
            json_object_object_add(jobj, ST_CMD, json_object_new_string(ST_REPORT));

            uint64_t configId;
            int realData = pTxNotify.configReportNotify.realData;
            configId = (uint64_t)pTxNotify.configReportNotify.attributeId +
                (uint64_t)pTxNotify.configReportNotify.devSourceDesc.clusterId * SHIFT_RIGHT_4_BYTE;

            json_object_object_add(jobj, ST_CONFIGURATION_VALUE, json_object_new_int(realData));
            
            char paramName[SIZE_32B] ={0};
            if(!get_configuration_data_from_id(ID, configId, paramName))
            {
                if(strlen(paramName))
                {
                    char paramValue[SIZE_32B] ={0};
                    sprintf(paramValue, "%d", realData);
                    json_object_object_add(jobj, ST_CONFIG_NAME, json_object_new_string(paramName));
                    set_register_database(_VR_CB_(zigbee), ID, paramName, paramValue, ST_REPLACE, 0);
                }
            }
            Send_ubus_notify((char *)json_object_to_json_string(jobj));
        }

        else if (g_zigbee_notify.notify[0].notify_status == ZIGBEE_COMMAND_PROCESS_NOTIFY)
        {
            Send_ubus_notify((char *)g_zigbee_notify.notify[0].notify_message);
            sending_zigbee_notify = 0;
        }
        /*move notify[1] --> notify[0], alway handle notify[0]*/

        if(sending_zigbee_notify)
        {
            /*update device last time working*/
            if(update_last_work)
            {
                char timestampt[SIZE_32B];
                sprintf(timestampt, "%u", (unsigned)time(NULL));

                update_zigbee_status(_VR_CB_(zigbee), ID, ST_LAST_UPDATE, timestampt, ST_REPLACE, 0);
                update_zigbee_status(_VR_CB_(zigbee), ID, ST_STATE, ST_ALIVE, ST_REPLACE, 0);
                json_object_object_add(jobj, ST_CLOUD_TIME_STAMP, json_object_new_string(timestampt));
            }

            Send_ubus_notify((char*)json_object_to_json_string(jobj));
        }

    notSendData:
        pthread_mutex_lock(&CriticalMutexTransmitQueue);
        g_zigbee_notify.notify_index--;
        memcpy((uint8_t *)&g_zigbee_notify.notify[0], (uint8_t *)&g_zigbee_notify.notify[1], g_zigbee_notify.notify_index * sizeof(notify_t));
        pthread_mutex_unlock(&CriticalMutexTransmitQueue);
        json_object_put(jobj);
    }

    uloop_timeout_set(timeout, 100); //repeat
}

////////////////////////// For cancel adding device //////////////////////////////
static void cancel_adding_device_cb(struct uloop_timeout *timeout)
{
    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));
    pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_NODE_ADD;
    pzbParam->param1 = DISABLE_OPEN_NETWORK;
    zigbeeGeckoHandlerSendCommand(pzbParam);
    free(pzbParam);

    VR_(execute_system)
    ("ubus send zigbee '{\"state\":\"close_network\"}' &");

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CLOSE_NETWORK_R));
    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

    Send_ubus_notify((char *)json_object_to_json_string(jobj));
    json_object_put(jobj);
}

////////////////////////// For zigbee gekco controller //////////////////////////////

int zigbee_controller_enable(void)
{
    /*
     * Enable GPIO pins
     */
    if (ENUM_ERROR == GPIOExport(GPIO_PIN_RST_NUM) || ENUM_ERROR == GPIOExport(GPIO_PIN_BOOTM_NUM))
        return ENUM_ERROR;

    /*
     * Set GPIO directions
     */
    if (ENUM_ERROR == GPIODirection(GPIO_PIN_RST_NUM, OUT) || ENUM_ERROR == GPIODirection(GPIO_PIN_BOOTM_NUM, OUT))
        return ENUM_ERROR;

    /* 
     * Write data
     */
    if (ENUM_ERROR == GPIOWrite(GPIO_PIN_BOOTM_NUM, 1))
    {
        SLOGW("Can't write to BOOTMODE PIN:%d\n", GPIO_PIN_BOOTM_NUM);
        return ENUM_ERROR;
    }
    if (ENUM_ERROR == GPIOWrite(GPIO_PIN_RST_NUM, 0))
    {
        SLOGW("Can't write to RST PIN:%d\n", GPIO_PIN_RST_NUM);
        return ENUM_ERROR;
    }
    sleep(1);
    if (ENUM_ERROR == GPIOWrite(GPIO_PIN_RST_NUM, 1))
    {
        SLOGW("Can't write to RST PIN:%d\n", GPIO_PIN_RST_NUM);
        return ENUM_ERROR;
    }
    return ENUM_SUCCESS;
}

void zigbee_controller_reset()
{
    /*
     * Disable GPIO pins
     */
    if (ENUM_ERROR == GPIOUnexport(GPIO_PIN_RST_NUM) || ENUM_ERROR == GPIOUnexport(GPIO_PIN_BOOTM_NUM))
        return;
}

////////////////////////// For handle database: seaching, action //////////////////////////////

static void update_zigbee_controll_info(void)
{
    zigbeeControllerInfo(&zigbeeGeckoControllerSerial);
    /*
    char *sql = "CREATE TABLE Friends(Id INTEGER PRIMARY KEY, Name TEXT);"
    "INSERT INTO Friends(Name) VALUES ('Tom');"
    "INSERT INTO Friends(Name) VALUES ('Rebecca');"
    "INSERT INTO Friends(Name) VALUES ('Jim');"
    "INSERT INTO Friends(Name) VALUES ('Roger');"
    "INSERT INTO Friends(Name) VALUES ('Robert');";
    */
    SEARCH_DATA_INIT_VAR(zigbee_controller_id);
    //_VR_CB_(zigbee) zigbee, zigbee_db, zigbee_cb
    searching_database(_VR_CB_(zigbee), &zigbee_controller_id,
                       "SELECT udn from CONTROL_DEVS where udn='%016llX'",
                       zigbeeGeckoControllerSerial);

    if (!zigbee_controller_id.len)
    {
        database_actions(_VR_(zigbee), "INSERT INTO %s VALUES('%s','%s','%016llX', '%s')",
                         "CONTROL_DEVS (realName, friendlyName, udn, type)",
                         "zigbee_controller", "zigbee_venus",
                         zigbeeGeckoControllerSerial, ST_ZIGBEE);
    }
    else
    {
        char zigbeeGeckoControllerId[SIZE_64B];
        sprintf(zigbeeGeckoControllerId, "%016llX", zigbeeGeckoControllerSerial);
        if (strcmp(zigbeeGeckoControllerId, zigbee_controller_id.value))
        {
            database_actions(_VR_(zigbee), "UPDATE CONTROL_DEVS SET udn='%s' where realName='%s'",
                             zigbeeGeckoControllerId, "zigbee_controller");
        }
    }
    FREE_SEARCH_DATA_VAR(zigbee_controller_id);
}

////////////////////////// For reset controller //////////////////////////////
void remove_zigbee_dev_lists()
{
    pthread_mutex_lock(&zigbee_dev_info_listMutex);
    zigbee_dev_info_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)
    (pos, q, &g_zigbee_dev_list.list)
    {
        tmp = VR_(list_entry)(pos, zigbee_dev_info_t, list);

        VR_(list_del)
        (pos);
        free(tmp);
    }
    pthread_mutex_unlock(&zigbee_dev_info_listMutex);
}

void printf_zigbee_dev_info_list()
{
    int i = 0;
    zigbee_dev_info_t *tmp = NULL;

    VR_(list_for_each_entry)
    (tmp, &g_zigbee_dev_list.list, list)
    {
        printf("#### dev %d ######\n", i);
        printf("localId = %04X\n", tmp->localId);
        printf("active = %d\n", tmp->active);
        printf("endpointNum = %02X\n", tmp->endpointNum);
        printf("parentId = %02X\n", tmp->parentId);
        printf("serialId = %s\n", tmp->serialId);
        printf("cloudId = %s\n", tmp->cloudId);
        printf("childrenId = %s\n", tmp->childrenId ? tmp->childrenId : "NULL");
        printf("capList = %s\n", tmp->capList ? tmp->capList : "NULL");
        printf("status.onoff = %d\n", tmp->status.onOff);
        printf("status.dim = %d\n", tmp->status.dim);
        printf("status.state = %d\n", tmp->status.state);
        printf("status.lastUpdate = %d\n", tmp->status.lastUpdate);
        printf("status.wakeUp = %d\n", tmp->status.wakeUp);
        i++;
    }
}

static int reset_zigbee_remove_cloud_cb(void *data, int argc, char **argv, char **azColName)
{
    if (argc == 0)
    {
        return COMMAND_SUCESS;
    }

    char *resource_id = argv[0];
    if (resource_id && strlen(resource_id))
    {
        if (!VR_(delete_resources)(resource_id, RESOURCES_TIMEOUT))
        {
            database_actions(_VR_(zigbee), "DELETE from DEVICES where owner='%s'", resource_id);
            shm_update_data(g_shm, resource_id, NULL, NULL, SHM_DELETE);
        }
    }

    return COMMAND_SUCESS;
}

void reset_zigbee_remove_cloud()
{
    searching_database("zigbee", zigbee_db, reset_zigbee_remove_cloud_cb, NULL,
                       "SELECT owner from DEVICES where deviceType='%s'", ST_ZIGBEE);
}

void reset_zigbee_thread()
{
    remove_zigbee_dev_lists();
    printf_zigbee_dev_info_list();
    reset_zigbee_remove_cloud();

    database_actions(_VR_(zigbee), "DELETE from CONTROL_DEVS where udn='%016llX'", zigbeeGeckoControllerSerial);
    database_actions(_VR_(zigbee), "DELETE from SUB_DEVICES where type='zigbee'");
    database_actions(_VR_(zigbee), "DELETE from FEATURES where type='zigbee_handler'");
    database_actions(_VR_(zigbee), "DELETE from METER_DATA where type='zigbee_handler'");
    database_actions(_VR_(zigbee), "DELETE from ASSOCIATION_DATA where type='zigbee_handler'");
    database_actions(_VR_(zigbee), "DELETE from USER_CODE where type='zigbee_handler'");
    database_actions(_VR_(zigbee), "DELETE from SENSOR_DATA where type='zigbee_handler'");
    database_actions(_VR_(zigbee), "DELETE from THERMOSTAT_SETPOINT where type='zigbee_handler'");
    database_actions(_VR_(zigbee), "DELETE from MORE_PROPERTY where type='zigbee_handler'");
    database_actions(_VR_(zigbee), "DELETE from CAPABILITY where type='zigbee'");
    database_actions(_VR_(zigbee), "DELETE from SCENES where type='zigbee'");
    database_actions(_VR_(zigbee), "DELETE from ALARM where type='zigbee_handler'");
    freeHashTableEUI64();
    update_zigbee_controll_info();
}

static int reset_device(struct ubus_context *ctx, struct ubus_object *obj,
                        struct ubus_request_data *req, const char *method,
                        struct blob_attr *msg)
{
    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));

    SLOGI("in reset_device \n");
    json_object *jobj = json_object_new_object();

    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("ZIGBEE RESET\n");
    system("echo \"ZIGBEE RESET\" > /ZIGBEE.reset");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");
    printf("#####################################\n");

    SLOGI("START Send ZIGBEE Command\n");
    pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_NODE_SET_DEFAULT;
    zigbeeGeckoHandlerSendCommand(pzbParam);
    SLOGI("END ZIGBEE Command\n");
    if (pzbParam->ret == 0)
    {
        pthread_t reset_zigbee_thread_t;
        pthread_create(&reset_zigbee_thread_t, NULL, (void *)&reset_zigbee_thread, NULL);
        pthread_detach(reset_zigbee_thread_t);

        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RESET_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    }
    else
    {
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_RESET_R));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string("Action timeout (try again)"));
    }

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    if (pzbParam)
    {
        free(pzbParam);
    }

    return 0;
}

/*return full controller information*/
static void return_controller_infor(json_object *jobj, ZigbeeGeckoHandlerParam *pzbParam) //not ok
{
    if (!jobj || !pzbParam)
    {
        return;
    }

    uint8_t i, j;

    char ID[MAX_ID_SIZE], tmp[MAX_ID_SIZE], euiAddr[SIZE_32B];
    sprintf(ID, "%04X", pzbParam->controllerInfo.destAddr);
    sprintf(euiAddr, "%016llX", pzbParam->controllerInfo.euiAddr);

    json_object_object_add(jobj, ST_CONTROLLER_EUI64_ADDRESS, json_object_new_string(euiAddr));
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ID));
    json_object_object_add(jobj, ST_NODE_MODE, json_object_new_int(MODE_ALWAYSLISTENING));

    json_object_object_add(jobj, ST_NO_ENDPOINT, json_object_new_int(pzbParam->controllerInfo.numEndpoint));

    json_object *jarray_multichanel_capability = json_object_new_array();

    for (i = 0; i < pzbParam->controllerInfo.numEndpoint; i++)
    {
        json_object *jobj_multichannel_capability = json_object_new_object();
        json_object_object_add(jobj_multichannel_capability, ST_ENDPOINT, json_object_new_int(pzbParam->controllerInfo.clusterList[i].endpoint));
        sprintf(tmp, "%04X", pzbParam->controllerInfo.clusterList[i].profileId);
        json_object_object_add(jobj_multichannel_capability, ST_PROFILE_ID, json_object_new_string(tmp));
        sprintf(tmp, "%04X", pzbParam->controllerInfo.clusterList[i].deviceId);
        json_object_object_add(jobj_multichannel_capability, ST_DEVICE_ID, json_object_new_string(tmp));
        json_object *jarray_endpoint_in_cluster = json_object_new_array();
        for (j = 0; j < pzbParam->controllerInfo.clusterList[i].inClusterCount; j++)
        {
            sprintf(tmp, "%04X", pzbParam->controllerInfo.clusterList[i].inClusterList[j]);
            json_object_array_add(jarray_endpoint_in_cluster, json_object_new_string(tmp));
        }
        json_object_object_add(jobj_multichannel_capability, ST_IN_CLUSTER_LIST, jarray_endpoint_in_cluster);

        json_object *jarray_endpoint_out_cluster = json_object_new_array();
        for (j = 0; j < pzbParam->controllerInfo.clusterList[i].outClusterCount; j++)
        {
            sprintf(tmp, "%04X", pzbParam->controllerInfo.clusterList[i].outClusterList[j]);
            json_object_array_add(jarray_endpoint_out_cluster, json_object_new_string(tmp));
        }
        json_object_object_add(jobj_multichannel_capability, ST_OUT_CLUSTER_LIST, jarray_endpoint_out_cluster);

        json_object_array_add(jarray_multichanel_capability, jobj_multichannel_capability);
    }
    json_object_object_add(jobj, ST_MULTICHANNEL_CAPABILITY, jarray_multichanel_capability);
}

////////////////////////// For handle command in queue //////////////////////////////
char g_last_node_id[SIZE_32B] = {0};

void zigbee_command_process(void *data)
{
    SLOGI("Start zigbee command process\n");
    while (1)
    {
        int queueId = NO_COMMAND;
        int send_zigbee_command = 1;
        //char str_value[MAX_ID_SIZE];
        queueId = decide_queue_to_execute();

        if (queueId == NO_COMMAND)
        {
            //SLOGW("no zigbee gecko command in queue");
            usleep(100000);
            continue;
        }

        zigbee_queue_priority_t *queueInfo = get_queue_from_queueId(queueId);
        if (!queueInfo)
        {
            SLOGW("not found queue id %d\n", queueId);
            usleep(SLEEP_MILISECOND_UNIT);
            continue;
        }

        if (queueInfo->queue.command_index <= 0)
        {
            SLOGW("no command in queue %d, command_index = %d\n", queueId, queueInfo->queue.command_index);
            usleep(SLEEP_MILISECOND_UNIT);
            continue;
        }

        zigbee_command_queue_t *commandInQ = get_and_delete_first_element_in_queue(&queueInfo->queue, &queueInfo->queueMutex);
        if (!commandInQ)
        {
            SLOGW("commandInQ not found\n");
            usleep(SLEEP_MILISECOND_UNIT);
            continue;
        }

        // SLOGI("execute queue with Id = %d\n", queueId);

        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));

        ZigbeeGeckoHandlerParam *pzbParam = commandInQ->pzbParam;
        char *commandClass = commandInQ->response.commandCluster;
        char *command = commandInQ->response.command;
        char *value = commandInQ->response.value;
        char *nodeid = commandInQ->response.nodeid;
        char *method = commandInQ->response.method;

        /*if two commands in the sequence of one device, slow down it.*/
        if (!strcmp(nodeid, g_last_node_id))
        {
            usleep(50000);
        }
        else
        {
            strncpy(g_last_node_id, nodeid, sizeof(g_last_node_id) - 1);
        }

        SLOGI("pzbParam->ret = 0x%02X\n", pzbParam->ret);
        SLOGI("pzbParam->command = 0x%02X\n", pzbParam->command);
        SLOGI("pzbParam->param1 = 0x%02X\n", pzbParam->param1);
        SLOGI("pzbParam->param2 = 0x%04X\n", pzbParam->param2);
        SLOGI("pzbParam->param3 = 0x%04X\n", pzbParam->param3);
        SLOGI("pzbParam->param4 = 0x%016llX\n", pzbParam->param4);
        SLOGI("pzbParam->param5 = 0x%016llX\n", pzbParam->param5);
        SLOGI("pzbParam->destAddr = 0x%04X\n", pzbParam->destAddr);
        SLOGI("pzbParam->srcEndpoint = 0x%02X\n", pzbParam->srcEndpoint);
        SLOGI("pzbParam->destEndpoint = 0x%02X\n", pzbParam->destEndpoint);

        if (send_zigbee_command)
        {
            // SLOGI("start send zigbee command\n");
            zigbeeGeckoHandlerSendCommand(pzbParam);
            // SLOGI("end send zigbee command\n");
        }
        /*
        if (DEAD_PRIORITY != queueId && pzbParam->ret == -1)
        {
            pzbParam->param4 = 0;//reset timeout value
            insert_to_a_queue(DEAD_PRIORITY, commandInQ->response, commandInQ->pzbParam);
        }
        */
        JSON_ADD_STRING_SAFE(jobj, ST_METHOD, method);
        JSON_ADD_STRING_SAFE(jobj, ST_DEVICE_ID, nodeid);

        SLOGI("method = %s\n", method);

        update_dev_priority(nodeid, pzbParam);

        char timestampt[SIZE_32B];
        sprintf(timestampt, "%u", (unsigned)time(NULL));
        if (pzbParam->ret == COMMAND_SUCESS)
        {
            update_zigbee_status(_VR_CB_(zigbee), nodeid, ST_LAST_UPDATE, timestampt, ST_REPLACE, 0);
            update_zigbee_status(_VR_CB_(zigbee), nodeid, ST_STATE, ST_ALIVE, ST_REPLACE, 0);
            json_object_object_add(jobj, ST_CLOUD_TIME_STAMP, json_object_new_string(timestampt));
        }
        else if (pzbParam->ret == COMMAND_FAILED)
        {
            if(!g_open_network)
            {
                update_zigbee_status(_VR_CB_(zigbee), nodeid, ST_STATE, ST_DEAD, ST_REPLACE, 0);
                json_object_object_add(jobj, ST_CLOUD_TIME_STAMP, json_object_new_string(timestampt));
            }
        }

        if (!strcmp(method, ST_NETWORK_R))
        {
            char *commandCluster = commandInQ->response.commandCluster;
            char *data0 = commandInQ->response.data0;
            char *data1 = commandInQ->response.data1;
            char *data2 = commandInQ->response.data2;

            json_object *commandinfo = json_object_new_object();
            JSON_ADD_STRING_SAFE(commandinfo, ST_CLASS, commandCluster);
            JSON_ADD_STRING_SAFE(commandinfo, ST_COMMAND, command);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA0, data0);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA1, data1);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA2, data2);
            json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

            if (pzbParam->ret == COMMAND_SUCESS)
            {
                if (!strcmp(commandCluster, ST_RESET))
                {
                    pthread_t reset_zigbee_thread_t;
                    pthread_create(&reset_zigbee_thread_t, NULL, (void *)&reset_zigbee_thread, NULL);
                    pthread_detach(reset_zigbee_thread_t);

                    return_controller_infor(jobj, pzbParam);
                }
                else if (!strcmp(commandCluster, ST_CONTROLLER_INFO))
                {
                    return_controller_infor(jobj, pzbParam);
                }
                else if (!strcmp(commandCluster, ST_REMOVE_FAILED_NODE))
                {
                    //char nodeID[3];
                    //sprintf(nodeID, "%02X", pzbParam->controllerInfo.node_id);
                    //json_object_object_add(jobj, ST_REMOVED_NODE_ID, json_object_new_string(nodeID));
                }

                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }
            else if (pzbParam->ret == COMMAND_FAILED)
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }
            else
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }

            char *notify_message = (char *)json_object_to_json_string(jobj);
            pushNotificationZigbeeToHandler(ZIGBEE_COMMAND_PROCESS_NOTIFY,
                                            (uint8_t *)notify_message,
                                            strlen(notify_message) + 1);
        }
        else if (!strcmp(method, ST_SET_BINARY_R)) //ok
        {
            JSON_ADD_STRING_SAFE(jobj, ST_COMMAND, command);
            JSON_ADD_STRING_SAFE(jobj, ST_VALUE, value);

            if (pzbParam->ret == COMMAND_SUCESS)
            {
                if (!strcasecmp(command, ST_ON_OFF)) //light + doorlock
                {
                    update_zigbee_status(_VR_CB_(zigbee), nodeid, ST_ON_OFF, value, ST_REPLACE, 0);
                    if (!strcmp(commandClass, ST_DOOR_LOCK))
                    {
                                            /*work around for making dev->status.onOff is difference with notify*/
                        if(!strcmp(value, ST_OPEN_VALUE))
                        {
                            update_zigbee_dev_from_id((char*) nodeid, ST_ON_OFF, ST_OPENING_VALUE);
                        }
                        else if(!strcmp(value, ST_CLOSE_VALUE))
                        {
                            update_zigbee_dev_from_id((char*) nodeid, ST_ON_OFF, ST_CLOSING_VALUE);
                        }
                    }
                }
                else if (!strcasecmp(command, ST_DIM))
                {
                    update_zigbee_status(_VR_CB_(zigbee), nodeid, ST_ON_OFF, ST_OPEN_VALUE, ST_REPLACE, 0);
                    update_zigbee_status(_VR_CB_(zigbee), nodeid, ST_DIM, value, ST_REPLACE, 0);
                }
                JSON_ADD_STRING_SAFE(jobj, ST_STATUS, ST_SUCCESSFUL);
            }
            else if (pzbParam->ret == COMMAND_FAILED)
            {
                if (!g_open_network)
                {
                    JSON_ADD_STRING_SAFE(jobj, ST_STATUS, ST_FAILED);
                }
                else
                {
                    JSON_ADD_STRING_SAFE(jobj, ST_STATUS, ST_NETWORK_STILL_OPEN);
                }
                JSON_ADD_STRING_SAFE(jobj, ST_REASON, ST_TIMEOUT);
            }
            else
            {
                JSON_ADD_STRING_SAFE(jobj, ST_STATUS, ST_FAILED);
                JSON_ADD_STRING_SAFE(jobj, ST_REASON, "node is not in network");
            }
            remove_dev_adding_actions(nodeid, jobj);
            char *notify_message = (char *)json_object_to_json_string(jobj);
            pushNotificationZigbeeToHandler(ZIGBEE_COMMAND_PROCESS_NOTIFY,
                                            (uint8_t *)notify_message,
                                            strlen(notify_message) + 1);
        }
        else if (!strcmp(method, ST_GET_BINARY_R))
        {
            if (pzbParam->ret == COMMAND_SUCESS)
            {
                JSON_ADD_STRING_SAFE(jobj, ST_COMMAND, command);
                JSON_ADD_STRING_SAFE(jobj, ST_STATUS, ST_SUCCESSFUL);
            }
            else if (pzbParam->ret == NO_ACK_FROM_DEV)
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_EMBER_DELIVERY_FAILED));
            }
            else
            {
                if (!g_open_network)
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                }
                else
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NETWORK_STILL_OPEN));
                }

                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }
            //remove_dev_adding_actions(nodeid, jobj);
            char *notify_message = (char *)json_object_to_json_string(jobj);
            pushNotificationZigbeeToHandler(ZIGBEE_COMMAND_PROCESS_NOTIFY,
                                            (uint8_t *)notify_message,
                                            strlen(notify_message) + 1);
        }
        else if (!strcmp(method, ST_WRITE_SPEC_R))
        {
            char *commandCluster = commandInQ->response.commandCluster;
            char *data0 = commandInQ->response.data0;
            char *data1 = commandInQ->response.data1;
            char *data2 = commandInQ->response.data2;

            json_object *commandinfo = json_object_new_object();
            JSON_ADD_STRING_SAFE(commandinfo, ST_CLASS, commandCluster);
            JSON_ADD_STRING_SAFE(commandinfo, ST_COMMAND, command);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA0, data0);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA1, data1);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA2, data2);
            json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

            if (pzbParam->ret == COMMAND_SUCESS)
            {
                if (!strcmp(commandCluster, ST_ASSOCIATION))
                {
                    if (!strcasecmp(command, ST_SET))
                    {
                        /*data0 targetAddrB*/
                        set_ass_node_list_database(_VR_CB_(zigbee), nodeid, ST_ONOFF_GROUP,
                                                   data0, ST_UPDATE, MAXIMUM_NODE_ASSSOCIATION_FOR_ONE_DEV);
                    }
                    else if (!strcasecmp(command, ST_REMOVE))
                    {
                        remove_ass_node_list_database(_VR_CB_(zigbee), nodeid, ST_ONOFF_GROUP, data0);
                    }
                }
                else if (!strcmp(commandCluster, ST_CONFIGURATION))
                {
                    if(!data0 || !data1)
                    {
                        goto done;
                    }

                    char *paramName = data0;
                    char *paramValue = data1;
                    set_register_database(_VR_CB_(zigbee), nodeid, paramName, paramValue, ST_REPLACE, 0);
                }
                else if (!strcmp(commandCluster, ST_WAKE_UP))
                {
                    // set_data_database(_VR_CB_(zigbee), nodeid, ST_WAKE_UP, data1, data0, ST_REPLACE, 0);
                }
                else if (!strcmp(commandCluster, ST_SWITCH_COLOR))
                {
                    char value[SIZE_256B];
                    sprintf(value, "%s:%s:%s", data0, data1, data2);
                    set_register_database(_VR_CB_(zigbee), nodeid, ST_SWITCH_COLOR, value, ST_REPLACE, 0);
                }
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }
            else if (pzbParam->ret == COMMAND_FAILED)
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }
            else
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }

            char *notify_message = (char *)json_object_to_json_string(jobj);
            pushNotificationZigbeeToHandler(ZIGBEE_COMMAND_PROCESS_NOTIFY,
                                            (uint8_t *)notify_message,
                                            strlen(notify_message) + 1);
        }
        else if (!strcmp(method, ST_WRITE_S_SPEC_R))
        {
            char *commandCluster = commandInQ->response.commandCluster;
            char *data0 = commandInQ->response.data0;
            char *data1 = commandInQ->response.data1;
            char *data2 = commandInQ->response.data2;

            json_object *commandinfo = json_object_new_object();
            JSON_ADD_STRING_SAFE(commandinfo, ST_CLASS, commandCluster);
            JSON_ADD_STRING_SAFE(commandinfo, ST_COMMAND, command);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA0, data0);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA1, data1);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA2, data2);
            json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

            if (pzbParam->ret == COMMAND_SUCESS)
            {
                if (!strcmp(commandCluster, ST_USER_CODE)) // data0: codeId, data1: remove/add, data2: {"pass":"","name":"name"}
                {                                          /*insert database when gateway receive response from dev*/
                    // data0: codeId, data1: remove/add, data2: {"pass":"","name":"name"}
                    if(!data0 || !data1)
                    {
                        goto done;
                    }

                    char *userIdStr = data0;
                    int userId = strtol(userIdStr, NULL, 0);
                    int sendUserId = pzbParam->reqZclDoorLockReq.setPinCode.user_id;

                    if(!data2)
                    {
                        goto done;
                    }

                    json_object *userCodeObj = VR_(create_json_object)(data2);
                    if(!userCodeObj)
                    {
                        goto done;
                    }

                    json_object *nameObj = NULL;
                    if(!json_object_object_get_ex(userCodeObj, ST_NAME, &nameObj))
                    {
                        json_object_put(userCodeObj);
                        goto done;
                    }

                    if(userId != sendUserId)
                    {
                        userId = sendUserId;
                        char validUserId[SIZE_32B];
                        snprintf(validUserId, sizeof(validUserId), "%d", userId);
                        json_object_object_add(jobj, ST_VALID_USER_ID, json_object_new_string(validUserId));
                    }
                }

                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }
            else
            {
                if (!g_open_network)
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                }
                else
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NETWORK_STILL_OPEN));
                }
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }

            char *notify_message = (char *)json_object_to_json_string(jobj);
            pushNotificationZigbeeToHandler(ZIGBEE_COMMAND_PROCESS_NOTIFY,
                                            (uint8_t *)notify_message,
                                            strlen(notify_message) + 1);
        }
        else if (!strcmp(method, ST_READ_S_SPEC_R))
        {
            char *commandCluster = commandInQ->response.commandCluster;
            char *data0 = commandInQ->response.data0;

            json_object *commandinfo = json_object_new_object();
            JSON_ADD_STRING_SAFE(commandinfo, ST_CLASS, commandCluster);
            JSON_ADD_STRING_SAFE(commandinfo, ST_COMMAND, command);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA0, data0);
            json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);
            if (pzbParam->ret == COMMAND_SUCESS)
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

                if (!strcasecmp(commandCluster, ST_BATTERY)) //ST_READ_SPEC_R battery
                {
                }
                else if (!strcasecmp(commandCluster, ST_CLUSTER_ID))
                {
                    if (!strcasecmp(command, ST_DISCOVER))
                    {
                        char temp[MAX_ID_SIZE], tempDesc[MAX_ID_SIZE];
                        sprintf(temp, "%04X", pzbParam->discData.devSourceDesc.clusterId);
                        json_object_object_add(jobj, ST_CLUSTER_ID, json_object_new_string(temp));

                        if (pzbParam->discData.discoveryComplete)
                            json_object_object_add(jobj, ST_DISCOVER_COMPLETE, json_object_new_string(ST_TRUE));
                        else
                            json_object_object_add(jobj, ST_DISCOVER_COMPLETE, json_object_new_string(ST_FALSE));
                        sprintf(tempDesc, "%02X", pzbParam->discData.numAttributeInfo);
                        json_object_object_add(jobj, ST_NO_ATTRIBUTE_INFO, json_object_new_string(tempDesc));
                        int i;
                        json_object *jarray_attribute_info = json_object_new_array();
                        for (i = 0; i < pzbParam->discData.numAttributeInfo; i++)
                        {
                            json_object *jlist = json_object_new_object();
                            sprintf(temp, "%04X", pzbParam->discData.discAttrInfo[i].attributeID);
                            sprintf(tempDesc, "%02X", pzbParam->discData.discAttrInfo[i].attributeDataType);
                            json_object_object_add(jlist, ST_ATTRIBUTE_ID, json_object_new_string(temp));
                            json_object_object_add(jlist, ST_DATA_TYPE, json_object_new_string(tempDesc));
                            json_object_array_add(jarray_attribute_info, jlist);
                        }
                        json_object_object_add(jobj, ST_ATTRIBUTE_INFO_LIST, jarray_attribute_info);
                        //set_register_database(_VR_CB_(zigbee), nodeid, ST_MULTILEVEL_SENSOR_AIR_TEMPERATURE, temp_value, ST_REPLACE, NULL_ROTATE_NUM);
                    }
                }
                else if (!strcasecmp(commandCluster, ST_ELECTRIC_METER))
                {
                    if (!strcasecmp(command, ST_REAL_RMS_CURRENT))
                    {
                        if (pzbParam->rData[0].noAttributeData == NO_ATTRIBUTE_ID_READ_CURRENT)
                        {
                            /*
                            ATTRIB ID: 0508, STATUS: 00 type: UNSIGNED INT, DATA: 0021                      
                            ATTRIB ID: 0602, STATUS: 00 type: UNSIGNED INT, DATA: 0001                      
                            ATTRIB ID: 0603, STATUS: 00 type: UNSIGNED INT, DATA: 03E8  
                            */

                            float rMSCurrent = (float)((float)pzbParam->rData[0].uint16_t_data * pzbParam->rData[1].uint16_t_data / pzbParam->rData[2].uint16_t_data);
                            char valueStr[SIZE_32B];
                            sprintf(valueStr, "%.3f", rMSCurrent);
                            json_object_object_add(jobj, ST_REAL_RMS_CURRENT, json_object_new_string(valueStr));
                        }
                    }
                    else if (!strcasecmp(command, ST_RMS_VOLTAGE))
                    {
                        if (pzbParam->rData[0].noAttributeData == NO_ATTRIBUTE_ID_READ_VOLT)
                        {
                            char valueStr[MAX_ID_SIZE];
                            sprintf(valueStr, "%d", pzbParam->rData[0].uint16_t_data);
                            json_object_object_add(jobj, ST_RMS_VOLTAGE, json_object_new_string(valueStr));
                        }
                    }
                    else if (!strcasecmp(command, ST_ACTIVE_POWER))
                    {
                        if (pzbParam->rData[0].noAttributeData == NO_ATTRIBUTE_ID_READ_CURRENT)
                        {
                            /**
                            ATTRIB ID: 050B, STATUS: 00 type: SIGNED INT, DATA: 0036                        
                            ATTRIB ID: 0604, STATUS: 00 type: UNSIGNED INT, DATA: 0001                      
                            ATTRIB ID: 0605, STATUS: 00 type: UNSIGNED INT, DATA: 000A   */
                            float rMSPower = (float)((float)pzbParam->rData[0].int16_t_data * pzbParam->rData[1].uint16_t_data / pzbParam->rData[2].uint16_t_data);
                            char valueStr[SIZE_32B];
                            sprintf(valueStr, "%.3f", rMSPower);
                            json_object_object_add(jobj, ST_REAL_ACTIVE_POWER, json_object_new_string(valueStr));
                        }
                    }
                }
            }
            else
            {
                if (!g_open_network)
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                }
                else
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NETWORK_STILL_OPEN));
                }
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }
            //remove_dev_adding_actions(nodeid, jobj);
            char *notify_message = (char *)json_object_to_json_string(jobj);
            pushNotificationZigbeeToHandler(ZIGBEE_COMMAND_PROCESS_NOTIFY,
                                            (uint8_t *)notify_message,
                                            strlen(notify_message) + 1);
        }
        else if (!strcmp(method, ST_READ_SPEC_R))
        {
            char *commandCluster = commandInQ->response.commandCluster;
            char *data0 = commandInQ->response.data0;

            json_object *commandinfo = json_object_new_object();
            JSON_ADD_STRING_SAFE(commandinfo, ST_CLASS, commandCluster);
            JSON_ADD_STRING_SAFE(commandinfo, ST_COMMAND, command);
            JSON_ADD_STRING_SAFE(commandinfo, ST_DATA0, data0);
            json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

            if (pzbParam->ret == COMMAND_SUCESS)
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
                if (!strcasecmp(commandCluster, ST_ASSOCIATION)) //doorlock
                {
                    if (!strcasecmp(command, ST_ON_OFF))
                    {
                        json_object *nodefollow = json_object_new_array();
                        char new_value[MAX_NODE_ID_BINDING_TABLE];
                        uint8_t i;
                        memset(new_value, 0x00, sizeof(new_value));
                        for (i = 0; i < pzbParam->param1; i++) //pzbParam->param1: rbt_number_of_element
                        {
                            SEARCH_DATA_INIT_VAR(tmp);
                            searching_database(_VR_CB_(zigbee), &tmp,
                                               "SELECT id from SUB_DEVICES where serial='%016llX'", pzbParam->rbt[i].euiDestAddr);
                            if (tmp.len)
                            {
                                json_object *jstring = json_object_new_string(tmp.value);
                                json_object_array_add(nodefollow, jstring);

                                if (!strlen(new_value))
                                {
                                    sprintf(new_value, "%s", tmp.value);
                                }
                                else
                                {
                                    sprintf(new_value + strlen(new_value), ",%s", tmp.value);
                                }
                            }
                            FREE_SEARCH_DATA_VAR(tmp);
                        }
                        json_object_object_add(jobj, ST_NODE_FOLLOW, nodefollow);
                        set_register_database(_VR_CB_(zigbee), nodeid, ST_ONOFF_GROUP, new_value, ST_REPLACE, NULL_ROTATE_NUM);
                    }
                }
                else if (!strcasecmp(commandCluster, ST_SENSOR_MULTILEVEL))
                {

                }
                else if (!strcasecmp(commandCluster, ST_CONFIGURATION)) // get time configuration of sensor
                {
                    if (!strcasecmp(command, ST_TIME))
                    {
                        if (pzbParam->rData[0].type == ZCL_TYPE_U16)
                        {
                            char time_value[SIZE_32B];
                            sprintf(time_value, "%d", pzbParam->rData[0].uint16_t_data);
                            json_object_object_add(jobj, ST_VALUE, json_object_new_string(time_value));
                            set_register_database(_VR_CB_(zigbee), nodeid, ST_TIME, time_value, ST_REPLACE, NULL_ROTATE_NUM);
                        }
                    }
                }
                else if (!strcasecmp(commandCluster, ST_BATTERY)) //ST_READ_S_SPEC_R
                {
                }
            }
            else
            {
                if (!g_open_network)
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                }
                else
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NETWORK_STILL_OPEN));
                }
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }
            //remove_dev_adding_actions(nodeid, jobj);
            char *notify_message = (char *)json_object_to_json_string(jobj);
            pushNotificationZigbeeToHandler(ZIGBEE_COMMAND_PROCESS_NOTIFY,
                                            (uint8_t *)notify_message,
                                            strlen(notify_message) + 1);
        }
        else if (!strcmp(method, ST_IDENTIFY_R))
        {
            JSON_ADD_STRING_SAFE(jobj, ST_VALUE, value);
            if (pzbParam->ret == COMMAND_SUCESS)
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
            }
            else if (pzbParam->ret == DEV_NOT_SUPPORT_CLUSTER)
            {
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                json_object_object_add(jobj, ST_REASON, json_object_new_string("does not support identify function"));
            }
            else
            {
                if (!g_open_network)
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
                }
                else
                {
                    json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_NETWORK_STILL_OPEN));
                }
                json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_TIMEOUT));
            }
            char *notify_message = (char *)json_object_to_json_string(jobj);
            pushNotificationZigbeeToHandler(ZIGBEE_COMMAND_PROCESS_NOTIFY,
                                            (uint8_t *)notify_message,
                                            strlen(notify_message) + 1);
        }
        else if (!strcmp(method, ST_ZIGBEE_LIGHT_LINK_R))
        {
            char *data0 = commandInQ->response.data0;
            char *data1 = commandInQ->response.data1;
            char *data2 = commandInQ->response.data2;

            JSON_ADD_STRING_SAFE(jobj, ST_COMMAND, command);
            JSON_ADD_STRING_SAFE(jobj, ST_DATA0, data0);
            JSON_ADD_STRING_SAFE(jobj, ST_DATA1, data1);
            JSON_ADD_STRING_SAFE(jobj, ST_DATA2, data2);

            if (pzbParam->ret == COMMAND_SUCESS)
            {
                JSON_ADD_STRING_SAFE(jobj, ST_STATUS, ST_SUCCESSFUL);
            }
            else
            {
                JSON_ADD_STRING_SAFE(jobj, ST_STATUS, ST_FAILED);
                JSON_ADD_STRING_SAFE(jobj, ST_REASON, "no find zll device");
            }
            remove_dev_adding_actions(nodeid, jobj);
            char *notify_message = (char *)json_object_to_json_string(jobj);
            pushNotificationZigbeeToHandler(ZIGBEE_COMMAND_PROCESS_NOTIFY,
                                            (uint8_t *)notify_message,
                                            strlen(notify_message) + 1);
        }
done:
        free_queue_element(commandInQ);
        json_object_put(jobj);

        usleep(100);
    }
}

/*status of device callback function*/
static int get_zigbee_dev_status_callback(void *data, int argc, char **argv, char **azColName)
{
    if (argc < 1)
    {
        return COMMAND_SUCESS;
    }

    char *feature = argv[0];
    char *value = argv[1];
    zigbee_dev_status_t *tmp = (zigbee_dev_status_t *)data;

    if (!tmp)
    {
        return COMMAND_SUCESS;
    }

    if (!strcmp(feature, ST_ON_OFF))
    {
        tmp->onOff = strtol(value, NULL, 0);
    }
    else if (!strcmp(feature, ST_DIM))
    {
        tmp->dim = strtol(value, NULL, 0);
    }
    else if (!strcmp(feature, ST_STATE))
    {
        if (!strcmp(value, ST_ALIVE))
        {
            tmp->state = 1;
        }
        else
        {
            tmp->state = 0;
        }
    }
    else if (!strcmp(feature, ST_LAST_UPDATE))
    {
        tmp->lastUpdate = strtoul(value, NULL, 0);
    }
    else if (!strcmp(feature, ST_WAKE_UP_NOTIFICATION))
    {
        tmp->wakeUp = strtoul(value, NULL, 0);
    }
    else if (!strcmp(feature, ST_PRIORITY))
    {
        tmp->priority = strtoul(value, NULL, 0);
    }

    return COMMAND_SUCESS;
}

static int get_origin_info(void *data, int argc, char **argv, char **azColName)
{
    if(!data)
    {
        return 0;
    }

    if(argc < 2)
    {
        return 0;
    }

    int i;
    char *sound = NULL;
    char *deviceType = NULL;

    for(i=0; i<argc;i++)
    {
        if(!strcmp(azColName[i], "Sound"))
        {
            sound = argv[i];
        }
        else if(!strcmp(azColName[i], "DeviceType"))
        {
            deviceType = argv[i];
        }
    }

    if(!sound || !deviceType)
    {
        return 0;
    }

    zigbee_dev_info_t *zigbee_dev = (zigbee_dev_info_t *)data;
    strncpy(zigbee_dev->VRDeviceType, deviceType, sizeof(zigbee_dev->VRDeviceType)-1);
    strncpy(zigbee_dev->defaultSound, sound, sizeof(zigbee_dev->defaultSound)-1);

    return 0;
}

/*create new zigbee dev*/
zigbee_dev_info_t *_create_zigbee_dev_(char *appDeviceId, char *serialId, char *deviceMode, char *realId, char *endpointNum,
                                       char *parentId, char *childrenId, char *active, char *cloudId,
                                       char *capability, int isAdding, int isEU64)
{
    if (!realId)
    {
        return NULL;
    }

    zigbee_dev_info_t *zigbee_dev = (zigbee_dev_info_t *)(malloc)(sizeof(zigbee_dev_info_t));
    memset(zigbee_dev, 0x00, sizeof(zigbee_dev_info_t));
    zigbee_dev->localId = htoi(realId);
    zigbee_dev->deviceMode = htoi(deviceMode);

    zigbee_dev->endpointNum = htoi(endpointNum);
    zigbee_dev->parentId = htoi(parentId);
    if (serialId)
    {
        strncpy(zigbee_dev->serialId, serialId, sizeof(zigbee_dev->serialId) - 1);

        searching_database("zigbee_handler", support_devs_db, get_origin_info, zigbee_dev, 
                            "SELECT Sound,DeviceType from SUPPORT_DEVS where SerialID='%s'", 
                            serialId);
    }

    if (childrenId)
    {
        strncpy(zigbee_dev->childrenId, childrenId, sizeof(zigbee_dev->childrenId) - 1);
    }

    if (appDeviceId)
    {
        strncpy(zigbee_dev->appDeviceId, appDeviceId, sizeof(zigbee_dev->appDeviceId) - 1);
    }

    if (!strcmp(active, ST_YES))
    {
        zigbee_dev->active = 1;
    }
    else
    {
        zigbee_dev->active = 0;
    }

    if (cloudId)
    {
        strncpy(zigbee_dev->cloudId, cloudId, sizeof(zigbee_dev->cloudId) - 1);
    }
    else
    {
        SEARCH_DATA_INIT_VAR(cloudId_db);
        searching_database(_VR_CB_(zigbee), &cloudId_db,
                           "SELECT owner "
                           "from DEVICES where deviceId='%s' and deviceType='%s'",
                           appDeviceId, ST_ZIGBEE);
        if (cloudId_db.len)
        {
            strncpy(zigbee_dev->cloudId, cloudId_db.value, sizeof(zigbee_dev->cloudId) - 1);
        }
        FREE_SEARCH_DATA_VAR(cloudId_db);
    }

    if (capability)
    {
        strncpy(zigbee_dev->capList, capability, sizeof(zigbee_dev->capList) - 1);
    }
    zigbee_dev->status.priority = INIT_PRIORITY_NUM;

    if (!isAdding)
    {
        /*get status of device*/
        searching_database(_VR_(zigbee), get_zigbee_dev_status_callback, &(zigbee_dev->status),
                           "SELECT featureId,register from FEATURES where deviceId='%s'"
                           "AND featureId IN ('%s', '%s', '%s', '%s', '%s', '%s')",
                           appDeviceId, ST_STATE,
                           ST_WAKE_UP_NOTIFICATION, ST_LAST_UPDATE, ST_ON_OFF, ST_DIM, ST_PRIORITY);
    }

    int res = add_zigbee_dev_last_list((void *)zigbee_dev);
    if (res)
    {
        SAFE_FREE(zigbee_dev);
    }

    insert_specific_dev_list(appDeviceId, serialId);
    if (appDeviceId)
    {
        insertEUI64(realId, appDeviceId);
    }

    return zigbee_dev;
}

static void alarm_voice_inform(struct ubus_context *ctx, char *ID, char *alarmTypeFinal)
{
    if(!ID || !alarmTypeFinal)
    {
        return;
    }

    if(g_sound_alarm_enable)
    {
        int alarm = 0;
        char *defaultSound = ST_DEFAULT_SOUND_NAME;
        zigbee_dev_info_t *dev = get_zigbee_dev_from_id(ID);
        /*checking device inform status via venus speaker or not*/
        SEARCH_DATA_INIT_VAR(sound_alarm_enable);
        searching_database(_VR_CB_(zigbee), &sound_alarm_enable,
                           "SELECT register from FEATURES where deviceId='%s' and featureId='%s'",
                           ID, ST_SOUND_ALARM_ENABLE);
        if (!sound_alarm_enable.len)
        {
            set_register_database(_VR_CB_(zigbee), ID, ST_SOUND_ALARM_ENABLE, ST_ON_VALUE, ST_REPLACE, NULL_ROTATE_NUM);
            alarm = 1;
        }
        else if (!strcmp(sound_alarm_enable.value, ST_ON_VALUE))
        {
            alarm = 1;
        }
        FREE_SEARCH_DATA_VAR(sound_alarm_enable);

        if(dev)
        {
            defaultSound = dev->defaultSound;
        }

        VR_(alarm)(ctx, alarm, (char *)alarmTypeFinal, ID, ST_ZIGBEE, defaultSound, NULL);
    }
    else
    {
        SLOGI("disable all alarms\n");
    }

    return;
}

static void alarm_update_database(char *ID, const char *alarmTypeFinal)
{
    int i,j;
    group_alarm_type_t alarm_info;
    alarm_info = get_group_alarm(alarmTypeFinal);
    if (!alarm_info.groupAlarm)
    {
        return;
    }
    /* write database*/
    set_alarm_value_database(_VR_CB_(zigbee), ID, alarm_info.groupAlarm, alarm_info.alarmTypeFinal);

    /* Check active*/
    for (i = 0; i < alarm_info.alarm_array_active_len; i++)
    {
        char *alarmTypeFinal_DB = get_alarm_value_database(_VR_CB_(zigbee), ID, alarm_info.alarmActive[i].groupAlarm);
        if (!alarmTypeFinal_DB)
        {
            continue;
        }

        for (j = 0; j < alarm_info.alarmActive[i].number_alarm; j++)
        {
            if (!strcmp(alarm_info.alarmActive[i].alarmTypeFinal[j], alarmTypeFinal_DB))
            {
                remove_alarm_database(_VR_(zigbee), ID, alarm_info.alarmActive[i].groupAlarm);
            }
        }
        free(alarmTypeFinal_DB);
    }
}

////////////////////////// For handle alarm //////////////////////////////

static void alarm_update_zigbee_database_and_voice_inform(struct ubus_context *ctx, json_object *jobj, char *ID)
{
    if (!jobj || !ID)
    {
        return;
    }

    CHECK_JSON_OBJECT_EXIST(alarmTypeFinalObj, jobj, ST_ALARM_TYPE_FINAL, done);
    const char *alarmTypeFinal = json_object_get_string(alarmTypeFinalObj);

    if (!strcmp(alarmTypeFinal, ST_UNKNOWN))
    {
        return;
    }

    int res = get_door_status(alarmTypeFinal);
    if (res == DOOR_RF_LOCK_VALUE)
    {
        update_zigbee_status(_VR_CB_(zigbee), ID, ST_ON_OFF, ST_CLOSE_VALUE, ST_REPLACE, NULL_ROTATE_NUM);
    }
    else if (res == DOOR_RF_UNLOCK_VALUE)
    {
        update_zigbee_status(_VR_CB_(zigbee), ID, ST_ON_OFF, ST_OPEN_VALUE, ST_REPLACE, NULL_ROTATE_NUM);
    }
    alarm_update_database(ID, alarmTypeFinal);
    alarm_voice_inform(ctx, ID, (char*)alarmTypeFinal);
done:
    return;
}

void alarm_zigbee_report_final(struct ubus_context *ctx, json_object *jobj, char* ID, char *alarm_pattern)
{
    if (!jobj || !alarm_pattern)
    {
        return;
    }

    SLOGI("alarm_zigbee_report_final.alarm_pattern = %s\n", alarm_pattern);
    SEARCH_DATA_INIT_VAR(serialId);
    searching_database(_VR_CB_(zigbee), &serialId,
                       "SELECT serialId from SUB_DEVICES where id='%s'",
                       ID);
    if (serialId.len)
    {
        SEARCH_DATA_INIT_VAR(alarm_data);
        searching_database("zigbee", support_devs_db, zigbee_cb, &alarm_data,
                           "SELECT AlarmData from SUPPORT_DEVS where SerialID='%s'",
                           serialId.value);
        if (alarm_data.len)
        {
            json_object *jobj_alarm = json_tokener_parse(alarm_data.value);
            if (jobj_alarm)
            {
                const char *alarmTypeFinal = json_object_get_string(
                    json_object_object_get(jobj_alarm, alarm_pattern));
                if (alarmTypeFinal && strlen(alarmTypeFinal))
                {
                    json_object_object_add(jobj, ST_ALARM_TYPE_FINAL, json_object_new_string(alarmTypeFinal));
                }
                else
                {
                    SLOGI("not found alarm pattern in json data\n");
                }
                json_object_put(jobj_alarm);
            }
            else
            {
                SLOGI("failed to paser alarm data\n");
            }
        }
        FREE_SEARCH_DATA_VAR(alarm_data);
    }
    else
    {
        SLOGI("not found serialId\n");
    }
    FREE_SEARCH_DATA_VAR(serialId);
    alarm_update_zigbee_database_and_voice_inform(ctx, jobj, ID);
    //Send_ubus_notify((char *)json_object_to_json_string(jobj));
}

////////////////////////// For handle signal from system //////////////////////////////

void signal_handler(int signo)
{
    SLOGI("signo = %d\n", signo);
    if (signo == SIGINT || signo == SIGTERM || signo == SIGSEGV)
    {
        if (signo == SIGSEGV)
        {
            uint32_t *up = (uint32_t *)&signo;

            pmortem_connect_and_send(up, 8 * 1024);
            fprintf(stderr, "SIGSEGV [%d].  Best guess fault address: %08x, ra: %08x, sig return: %p\n",
                    signo, up[8], up[72], __builtin_return_address(0));
        }
        if(zigbee_command_process_thread)
        {
            pthread_cancel(zigbee_command_process_thread);
        }
        zigbeeGeckoDestruct();
        freeHashTableEUI64();
        free_ubus_service();
        uloop_end();
        exit(1);
    }
}

////////////////////////// For handle set binary //////////////////////////////

static void _zigbee_init_set_onoff(zigbee_command_response_t *response,
                                   ZigbeeGeckoHandlerParam *pzbParam, zigbee_dev_info_t *dev, int input_value)
{
    if (!pzbParam)
    {
        return;
    }
    zclOnOffReq reqZclOnOffReq;
    pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_ON_OFF_REQ;
    pzbParam->destAddr = dev->localId;
    pzbParam->destEndpoint = dev->endpointNum;
    pzbParam->reqZclOnOffReq = reqZclOnOffReq;
    strcpy(response->command, ST_ON_OFF);
    if (input_value > 0)
    {
        pzbParam->param1 = ZCL_ON_OFF_CMD_ON;
    }
    else
    {
        pzbParam->param1 = ZCL_ON_OFF_CMD_OFF;
    }
}

static void _zigbee_init_set_dim(zigbee_command_response_t *response,
                                 ZigbeeGeckoHandlerParam *pzbParam, zigbee_dev_info_t *dev, int input_value)
{
    if (!pzbParam)
    {
        return;
    }
    if (input_value > HIGH_LIMIT_PERCENT)
        input_value = HIGH_LIMIT_PERCENT;
    if (input_value < LOW_LIMIT_PERCENT)
        input_value = LOW_LIMIT_PERCENT;

    int level = (input_value * DEFAULT_MAX_VALUE_8_BIT / DEFAULT_MAX_PERCENT); //convert 0 --> 100% to 0 --> 255;
    zclLevelReq reqZclLevelReq;

    reqZclLevelReq.move_to_level.level = (uint8_t)level;
    reqZclLevelReq.move_to_level.trans_time = DEFAULT_TRANSITION_TIME_LEVEL_WITH_ON_OFF;
    pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_LEVEL_REQ;
    pzbParam->destAddr = dev->localId;
    pzbParam->destEndpoint = dev->endpointNum;
    pzbParam->reqZclLevelReq = reqZclLevelReq;
    pzbParam->param1 = ZCL_LEVEL_CMD_MOVE_TO_LEVEL_WITH_ON_OFF;
    strcpy(response->command, ST_DIM);
    sprintf(response->value, "%d", input_value);
}

static void _zigbee_init_set_go_to_lift_percentage(zigbee_command_response_t *response,
                                 ZigbeeGeckoHandlerParam *pzbParam, zigbee_dev_info_t *dev, int input_value)
{
    if (!pzbParam)
    {
        return;
    }
    if (input_value > HIGH_LIMIT_PERCENT)
        input_value = HIGH_LIMIT_PERCENT;
    if (input_value < LOW_LIMIT_PERCENT)
        input_value = LOW_LIMIT_PERCENT;

    //int level = (input_value * DEFAULT_MAX_VALUE_8_BIT / DEFAULT_MAX_PERCENT); //convert 0 --> 100% to 0 --> 255;
    zclWindowCoveringReq_t redZclWindowCovering;

    redZclWindowCovering.go_to_lift_percentage.percentage_lift_value = (uint8_t)input_value;
    pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_WINDOW_COVERING_REQ;
    pzbParam->destAddr = dev->localId;
    pzbParam->destEndpoint = dev->endpointNum;
    pzbParam->redZclWindowCovering = redZclWindowCovering;
    pzbParam->param1 = ZCL_WINDOW_COVERING_CMD_GO_TO_LIFT_PERCENTAGE;
    strcpy(response->command, ST_DIM);
    sprintf(response->value, "%d", input_value);
}

/*check capId of device*/
int check_zigbee_dev_is_support_cap(char *nodeid, char *capList, char *capId)
{
    if (!capList)
    {
        return CAP_ID_NULL;
    }

    printf("capList: %s\n", capList);
    printf("capId = %s\n", capId);
    char *ch = strstr(capList, capId);
    if (ch)
        return CAP_ID_SUPPORTED;
    else
        return CAP_ID_NOT_SUPPORTED;
}
/*set binary, prepare for insert to queue*/
int _set_binary_prepare(zigbee_command_response_t response)
{
    int res = COMMAND_SUCESS;
    char *nodeid = &response.nodeid[0];
    char *command = &response.command[0];
    char *value = &response.value[0];

    zigbee_dev_info_t *dev = get_zigbee_dev_from_id(nodeid);
    if (!dev)
    {
        dev = create_zigbee_dev_from_id(nodeid);
        if (!dev)
        {
            return DEV_NOT_FOUND;
        }
    }

    if (!dev->active)
    {
        return DEV_UNACTIVATE;
    }

    if (!strlen(dev->serialId))
    {
        SEARCH_DATA_INIT_VAR(serialId);
        searching_database(_VR_CB_(zigbee), &serialId,
                           "SELECT serialId from SUB_DEVICES where id='%s'", nodeid);
        strncpy(dev->serialId, serialId.value, sizeof(dev->serialId) - 1);
        FREE_SEARCH_DATA_VAR(serialId);
    }

    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));
    SEARCH_DATA_INIT_VAR(tmp); //declare tmp
    searching_database(_VR_CB_(zigbee), &tmp,
                       "SELECT deviceType from SUB_DEVICES where id='%s'",
                       nodeid);

    if (tmp.len)
    {
        if (!strncmp(tmp.value, DOOR_LOCK_DEV_TYPE, sizeof(DOOR_LOCK_DEV_TYPE))) //door lock
        {

            if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, DOOR_LOCK_CAP_ID))
            {
                strcpy(response.commandCluster, ST_DOOR_LOCK);
                zclLevelReq reqZclLevelReq;
                pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_DOOR_LOCK_REQ;
                pzbParam->destAddr = dev->localId;
                pzbParam->destEndpoint = dev->endpointNum;
                pzbParam->reqZclLevelReq = reqZclLevelReq;
                if (strtol(value, NULL, 0) == DOOR_OPEN)
                {
                    pzbParam->param1 = ZCL_DOOR_LOCK_CMD_UNLOCK_DOOR;
                }
                else
                {
                    pzbParam->param1 = ZCL_DOOR_LOCK_CMD_LOCK_DOOR;
                }
            }
            else
            {
                res = DEV_NOT_SUPPORT_CLUSTER;
                goto finish_set_binary_prepare;
            }
        }
        else //light
        {
            if (!strcmp(command, ST_ON_OFF))
            {
                /*set dim fisrt after power cycle*/
                if (dev->status.dimFirst) //on off use switch multi level
                {
                    dev->status.dimFirst = false;
                    if (dev->status.dim) ////on off use switch multi level
                    {
                        uint8_t input_value = dev->status.dim;
                        if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, DIM_CAP_ID))
                        {
                            //dev->status.dim alway > 0
                            if (!dev->status.onOff || !dev->status.dim) //Status of device is off --> first we must turn on device
                            {
                                if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, ON_OFF_CAP_ID))
                                {
                                    _zigbee_init_set_onoff(&response, pzbParam, dev, TURN_ON_VALUE);
                                    pzbParam->ret = COMMAND_FAILED;
                                    insert_to_queue(response, pzbParam);
                                }
                            }
                            if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, DIM_CAP_ID))
                            {
                                _zigbee_init_set_dim(&response, pzbParam, dev, input_value);
                            }
                            else
                            {
                                res = DEV_NOT_SUPPORT_CLUSTER;
                                goto finish_set_binary_prepare;
                            }
                        }
                    }
                    else
                    {
                        if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, ON_OFF_CAP_ID))
                            _zigbee_init_set_onoff(&response, pzbParam, dev, htoi(value));
                        else
                        {
                            res = DEV_NOT_SUPPORT_CLUSTER;
                            goto finish_set_binary_prepare;
                        }
                    }
                }
                else
                {
                    if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, ON_OFF_CAP_ID))
                        _zigbee_init_set_onoff(&response, pzbParam, dev, htoi(value));
                    else
                    {
                        res = DEV_NOT_SUPPORT_CLUSTER;
                        goto finish_set_binary_prepare;
                    }
                }
            }
            else if (!strcmp(command, ST_DIM))
            {
                if (dev->status.dimFirst) //for POWER CYCLE
                {
                    dev->status.dimFirst = false;
                }
                //dev->status.dim alway > 0
                if (!dev->status.onOff || !dev->status.dim) //Status of device is off --> first we must turn on device
                {
                    if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, ON_OFF_CAP_ID))
                    {
                        _zigbee_init_set_onoff(&response, pzbParam, dev, TURN_ON_VALUE);
                        pzbParam->ret = COMMAND_FAILED;
                        insert_to_queue(response, pzbParam);
                    }
                }
                if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, DIM_CAP_ID))
                {
                    _zigbee_init_set_dim(&response, pzbParam, dev, strtol(value, NULL, 0));
                }
                else if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, WINDOW_COVERING_CAP_ID))
                {
                    _zigbee_init_set_go_to_lift_percentage(&response, pzbParam, dev, strtol(value, NULL, 0));
                }
                else
                {
                    res = DEV_NOT_SUPPORT_CLUSTER;
                    goto finish_set_binary_prepare;
                }
            }
        }
    }
    else
    {
        res = GET_DEV_TYPE_FAILED;
    }
    pzbParam->ret = COMMAND_FAILED;
    insert_to_queue(response, pzbParam);
finish_set_binary_prepare:
    FREE_SEARCH_DATA_VAR(tmp);

    SAFE_FREE(pzbParam);
    return res;
}

/*ubus set binary method*/
static int setbinary(struct ubus_context *ctx, struct ubus_object *obj,
                     struct ubus_request_data *req, const char *method,
                     struct blob_attr *msg)
{
    int res = 0;
    struct blob_attr *tb[3];
    const char *nodeid = ST_UNKNOWN;
    const char *command = ST_UNKNOWN;
    const char *value = ST_UNKNOWN;

    blobmsg_parse(setbinary_policy, ARRAY_SIZE(setbinary_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0] && tb[1] && tb[2])
    {
        nodeid = blobmsg_data(tb[0]);
        command = blobmsg_data(tb[1]);
        value = blobmsg_data(tb[2]);
    }
    else
    {
        return COMMAND_SUCESS;
    }

    SLOGI("nodeid = %s\n", nodeid);
    SLOGI("command = %s\n", command);
    SLOGI("value = %s\n", value);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SET_BINARY_R));
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));
    json_object_object_add(jobj, ST_COMMAND, json_object_new_string(command));
    json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));

    zigbee_command_response_t response;
    memset(&response, 0x00, sizeof(zigbee_command_response_t));
    strcpy(response.method, ST_SET_BINARY_R);
    strcpy(response.nodeid, nodeid);
    strcpy(response.command, command);
    strcpy(response.value, value);

    res = _set_binary_prepare(response);
    return_json_message(res, &jobj, (char *)command, NULL);

    
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return COMMAND_SUCESS;
}

/*Table 2-9. Data Types*/
void zclReadAttribPrepare(zigbee_command_response_t *response, char *command,
                          ZigbeeGeckoHandlerParam *pzbParam, uint16_t destAddr,
                          uint8_t endPointNumber, uint16_t clustId, uint16_t profileId,
                          uint16_t manufacturerCode, uint16_t attributId)
{
    //readAttrData rData;
    pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_READ_ATTRIB;
    pzbParam->destAddr = destAddr;
    pzbParam->destEndpoint = endPointNumber;
    pzbParam->param1 = DEFAULT_NO_ATTRIBUTE_ID;    //number attribute
    pzbParam->param2 = clustId;                    //clusterId
    pzbParam->param3 = profileId;                  //profileId
    pzbParam->param4 = (uint16_t)manufacturerCode; //manufacturerCode
    //pzbParam->param5 = attributId;                 //attributeId
    pzbParam->param6[0] = attributId; //attributeId
    //pzbParam->rData = rData;
    strcpy(response->command, command);
}

void zclReadAttribPrepareV2(zigbee_command_response_t *response, char *command,
                            ZigbeeGeckoHandlerParam *pzbParam, uint16_t destAddr,
                            uint8_t endPointNumber, uint16_t clustId, uint16_t profileId,
                            uint16_t manufacturerCode, uint16_t *attributId, uint8_t noAttribute)
{
    //readAttrData rData[MAX_NO_ZCL_READ_ATTRIBUTE];
    pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_READ_ATTRIB;
    pzbParam->destAddr = destAddr;
    pzbParam->destEndpoint = endPointNumber;
    pzbParam->param1 = noAttribute;                //number attribute
    pzbParam->param2 = clustId;                    //clusterId
    pzbParam->param3 = profileId;                  //profileId
    pzbParam->param4 = (uint16_t)manufacturerCode; //manufacturerCode
    memcpy((uint16_t *)pzbParam->param6, (uint16_t *)attributId, noAttribute * sizeof(uint16_t));
    //pzbParam->rData = rData;
    strcpy(response->command, command);
}

void zclDiscoverAttribPrepare(zigbee_command_response_t *response, char *command,
                              ZigbeeGeckoHandlerParam *pzbParam, uint16_t destAddr,
                              uint8_t endPointNumber, uint16_t clustId, uint16_t profileId,
                              uint16_t manufacturerCode, uint16_t attributId, uint8_t maxAttributeId)
{
    discAttrResData discData;
    pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_DISCOVER_ATTRIB;
    pzbParam->destAddr = destAddr;
    pzbParam->destEndpoint = endPointNumber;
    pzbParam->param2 = clustId;                    //clusterId
    pzbParam->param3 = profileId;                  //profileId
    pzbParam->param4 = (uint16_t)manufacturerCode; //manufacturerCode
    pzbParam->param5 = attributId;                 //attributeId
    pzbParam->param1 = maxAttributeId;             //maxAttributeId
    pzbParam->discData = discData;                 //discover Atrribute response data
    strcpy(response->command, command);
}

/*
void zclConfigReportAttribPrepare(zigbee_command_response_t *response, char *command,
                            ZigbeeGeckoHandlerParam *pzbParam, uint16_t destAddr,
                            uint8_t endPointNumber, uint16_t clustId, uint8_t type,
                            uint16_t attributId, int minIntervalReport, int maxIntervalReport, uint64_t reportableChange)
{
    pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_CONFIG_REPORT;
    pzbParam->destAddr = destAddr;
    pzbParam->destEndpoint = endPointNumber;
    pzbParam->param2 = clustId;                 //clusterId
    pzbParam->param1 = type;                    //type
    pzbParam->param3 = attributId;              //attributId
    pzbParam->param6[0] = minIntervalReport;    //minIntervalReport
    pzbParam->param4 = maxIntervalReport;       //maxIntervalReport
    pzbParam->param5 = reportableChange;       //reportableChange
    strcpy(response->command, command);
}
*/

void zclMultiConfigReportAttribPrepare(zigbee_command_response_t *response, char *command,
                            ZigbeeGeckoHandlerParam *pzbParam, uint16_t destAddr,
                            uint8_t endPointNumber, uint8_t numConfigId,
                            char* clusterAndAttribute)
{
    pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_MULTI_CONFIG_REPORT;
    pzbParam->destAddr = destAddr;
    pzbParam->destEndpoint = endPointNumber;
    pzbParam->param1 = numConfigId;//numAttributeId;
    memcpy(&pzbParam->configReportAttributeId[0][0], clusterAndAttribute, strlen(clusterAndAttribute));
    strcpy(response->command, command);
}
/*get binary, prepare for insert to queue*/
int _get_binary_prepare(zigbee_command_response_t response)
{
    //get door lock state, on_off, dim
    int res = COMMAND_SUCESS;
    char *nodeid = &response.nodeid[0];
    SLOGI("in _get_binary_prepare nodeid = %s\n", nodeid);
    zigbee_dev_info_t *dev = get_zigbee_dev_from_id(nodeid);
    if (!dev)
    {
        dev = create_zigbee_dev_from_id(nodeid);
        if (!dev)
        {
            return DEV_NOT_FOUND;
        }
    }

    if (!dev->active)
    {
        return DEV_UNACTIVATE;
    }

    if (!strlen(dev->serialId))
    {
        SEARCH_DATA_INIT_VAR(serialId);
        searching_database(_VR_CB_(zigbee), &serialId,
                           "SELECT serialId from SUB_DEVICES where id='%s'", nodeid);
        strncpy(dev->serialId, serialId.value, sizeof(dev->serialId) - 1);
        FREE_SEARCH_DATA_VAR(serialId);
    }

    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));
    SEARCH_DATA_INIT_VAR(tmp); //declare tmp
    searching_database(_VR_CB_(zigbee), &tmp,
                       "SELECT deviceType from SUB_DEVICES where id='%s'",
                       nodeid);

    if (tmp.len)
    {
        if (!strncmp(tmp.value, DOOR_LOCK_DEV_TYPE, sizeof(DOOR_LOCK_DEV_TYPE))) //door lock
        {

            if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, DOOR_LOCK_CAP_ID))
            {
                zclReadAttribPrepare(&response, ST_DOOR_LOCK,
                                     pzbParam, dev->localId, dev->endpointNum, ZCL_DOOR_LOCK_CLUSTER_ID,
                                     HOME_AUTOMATION_PROFILE_ID, DEFAULT_MANUFACTURER_CODE,
                                     ZCL_DOOR_LOCK_ATTRIB_LOCK_STATE);
            }
            else
            {
                res = DEV_NOT_SUPPORT_CLUSTER;
                goto finish_get_binary_prepare;
            }
        }
        else //light
        {
            if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, DIM_CAP_ID))
            {
                zclReadAttribPrepare(&response, ST_DIM,
                                     pzbParam, dev->localId, dev->endpointNum, ZCL_LEVEL_CONTROL_CLUSTER_ID,
                                     HOME_AUTOMATION_PROFILE_ID, DEFAULT_MANUFACTURER_CODE,
                                     ZCL_LEVEL_ATTRIB_CURR_LEVEL);
                insert_to_queue(response, pzbParam);
            }
            if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, ON_OFF_CAP_ID))
            {
                zclReadAttribPrepare(&response, ST_ON_OFF,
                                     pzbParam, dev->localId, dev->endpointNum, ZCL_ON_OFF_CLUSTER_ID,
                                     HOME_AUTOMATION_PROFILE_ID, DEFAULT_MANUFACTURER_CODE,
                                     ZCL_ON_OFF_ATTRIB);
                pzbParam->ret = COMMAND_FAILED;
            }
        }
    }
    else
    {
        res = GET_DEV_TYPE_FAILED;
    }
    pzbParam->ret = COMMAND_FAILED;
    insert_to_queue(response, pzbParam);
finish_get_binary_prepare:
    FREE_SEARCH_DATA_VAR(tmp);

    SAFE_FREE(pzbParam);
    return res;
}

/*get binary, prepare for insert to queue*/
int getOnOffOfDevice(char *nodeid)
{
    zigbee_command_response_t response;
    memset(&response, 0x00, sizeof(zigbee_command_response_t));
    //get door lock state, on_off, dim
    SLOGI("in get on Off value, nodeid = %s\n", nodeid);
    zigbee_dev_info_t *dev = get_zigbee_dev_from_id(nodeid);
    if (!dev)
    {
        dev = create_zigbee_dev_from_id(nodeid);
        if (!dev)
        {
            return DEV_NOT_FOUND;
        }
    }

    if (!dev->active)
    {
        return DEV_UNACTIVATE;
    }

    if (!strlen(dev->serialId))
    {
        SEARCH_DATA_INIT_VAR(serialId);
        searching_database(_VR_CB_(zigbee), &serialId,
                           "SELECT serialId from SUB_DEVICES where id='%s'", nodeid);
        strncpy(dev->serialId, serialId.value, sizeof(dev->serialId) - 1);
        FREE_SEARCH_DATA_VAR(serialId);
    }

    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));
    SEARCH_DATA_INIT_VAR(tmp); //declare tmp
    searching_database(_VR_CB_(zigbee), &tmp,
                       "SELECT deviceType from SUB_DEVICES where id='%s'",
                       nodeid);

    if (tmp.len)
    {
        if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, ON_OFF_CAP_ID))
        {
            zclReadAttribPrepare(&response, ST_ON_OFF,
                                 pzbParam, dev->localId, dev->endpointNum, ZCL_ON_OFF_CLUSTER_ID,
                                 HOME_AUTOMATION_PROFILE_ID, DEFAULT_MANUFACTURER_CODE,
                                 ZCL_ON_OFF_ATTRIB);
            pzbParam->ret = COMMAND_FAILED;
        }    
    }
    pzbParam->ret = COMMAND_FAILED;
    insert_to_queue(response, pzbParam);
    FREE_SEARCH_DATA_VAR(tmp);

    SAFE_FREE(pzbParam);
    return 0;
}

static int getbinary(struct ubus_context *ctx, struct ubus_object *obj,
                     struct ubus_request_data *req, const char *method,
                     struct blob_attr *msg)
{
    int res = 0;
    struct blob_attr *tb[PARA_NODE_MAX];
    const char *nodeid = ST_UNKNOWN;
    blobmsg_parse(para_node_id_policy, ARRAY_SIZE(para_node_id_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[PARA_NODE_ID])
    {
        nodeid = blobmsg_data(tb[PARA_NODE_ID]);
    }
    else
    {
        return COMMAND_SUCESS;
    }

    SLOGI("nodeid = %s\n", nodeid);

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_GET_BINARY_R));
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));

    zigbee_command_response_t response;
    memset(&response, 0x00, sizeof(zigbee_command_response_t));
    strcpy(response.method, ST_GET_BINARY_R);
    strcpy(response.nodeid, nodeid);

    res = _get_binary_prepare(response);
    return_json_message(res, &jobj, NULL, NULL);

    
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return COMMAND_SUCESS;
}

/*set specification, association, configuration, switch color*/
int _setSpecificationPrepare(zigbee_command_response_t response)
{
    int res = 0;
    char *nodeid = &response.nodeid[0];
    char *CommandClass = &response.commandCluster[0];
    char *Command = &response.command[0];
    char *data0 = &response.data0[0];
    char *data1 = &response.data1[0];
    char *data2 = &response.data2[0];

    zigbee_dev_info_t *dev = get_zigbee_dev_from_id(nodeid);
    if (!dev)
    {
        dev = create_zigbee_dev_from_id(nodeid);
        if (!dev)
        {
            return DEV_NOT_FOUND;
        }
    }

    if (!dev->active)
    {
        return DEV_UNACTIVATE;
    }
    if (!dev->serial)
    {
        SEARCH_DATA_INIT_VAR(serialA);
        searching_database(_VR_CB_(zigbee), &serialA,
                           "SELECT serial from SUB_DEVICES where id='%s'", nodeid);
        dev->serial = htoi64(serialA.value);
        FREE_SEARCH_DATA_VAR(serialA);
    }
    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));

    if (!strcmp(CommandClass, ST_SWITCH_COLOR))
    {
        if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, SWITH_COLOR_CAP_ID))
        {
            json_object *color = VR_(create_json_object)(data0);
            if (!color)
            {
                SLOGI("color %s is not json format\n", data0);
                goto setSpecPreDone;
            }

            json_object_object_foreach(color, key, value)
            {
                if (!strcmp(key, RGB_COLOR))
                {
                    int color = json_object_get_int(value);
                    zclColorControlReq reqZclColorControlReq;
                    pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_COLOR_CONTROL_REQ;
                    pzbParam->destAddr = dev->localId;
                    pzbParam->destEndpoint = dev->endpointNum;
                    pzbParam->param1 = ZCL_COLOR_CONTROL_CMD_MOVE_TO_HUE_AND_SATURATION; //color command
                    uint8_t Hue, Saturation, Lightness = 0;
                    uint8_t R = (color >> 16) & 0xFF;
                    uint8_t G = (color >> 8) & 0xFF;
                    uint8_t B = color;

                    RgbToHsl(R, G, B, &Hue, &Saturation, &Lightness);
                    printf("Hue:Saturation:Lightness = %02X:%02X:%02X\n", Hue, Saturation, Lightness);

                    reqZclColorControlReq.move_to_hue_and_saturation.hue = Hue;
                    reqZclColorControlReq.move_to_hue_and_saturation.saturation = Saturation;
                    reqZclColorControlReq.move_to_hue_and_saturation.transition_time = DEFAULT_TRANSITION_TIME_MOVE_TO_HUE_AND_SATURATION;
                    pzbParam->reqZclColorControlReq = reqZclColorControlReq;
                    pzbParam->ret = COMMAND_FAILED;
                    insert_to_queue(response, pzbParam);
                }
                else if (!strcmp(key, TEMP_COLOR))
                {
                    //int cw,ww;
                    //dev_specific_temperature_color(nodeid, json_object_get_int(value), &ww, &cw);
                    zclColorControlReq reqZclColorControlReq;
                    pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_COLOR_CONTROL_REQ;
                    pzbParam->destAddr = dev->localId;
                    pzbParam->destEndpoint = dev->endpointNum;
                    pzbParam->param1 = ZCL_COLOR_CONTROL_CMD_MOVE_TO_COLOR_TEMPERATURE; //color temperature command
                    reqZclColorControlReq.move_to_color_temperature.color_temperature = (uint16_t)((float)KELVIN_TO_COLOR_TEMPERATURE / json_object_get_int(value));
                    reqZclColorControlReq.move_to_color_temperature.transition_time = DEFAULT_TRANSITION_TIME_MOVE_TO_HUE_AND_SATURATION;
                    pzbParam->reqZclColorControlReq = reqZclColorControlReq;
                    pzbParam->ret = COMMAND_FAILED;
                    insert_to_queue(response, pzbParam);
                }
            }

            json_object_put(color);
        }
        else
        {
            res = DEV_NOT_SUPPORT_CLUSTER;
            goto setSpecPreDone;
        }
    }
    else if (!strcmp(CommandClass, ST_THERMOSTAT_SETPOINT))
    {
        //To do in the future
    }
    else if (!strcmp(CommandClass, ST_WAKE_UP))
    {
        //To do in the future
    }
    else if (!strcmp(CommandClass, ST_THERMOSTAT_FAN_MODE))
    {
        //To do in the future
    }
    else if (!strcmp(CommandClass, ST_THERMOSTAT_MODE))
    {
        //To do in the future
    }
    else if (!strcmp(CommandClass, ST_CONFIGURATION))
    {
        if(!data0 || !data1)
        {
            goto setSpecPreDone;
        }
        char tempConfigId[SIZE_16B]={0}, configLength[MAX_ID_SIZE]={0};
        char configId[SIZE_16B]={0};
        if(!get_configuration_data_from_name((char*)nodeid, (char*)data0, tempConfigId, configLength))
        {
            if(!(*tempConfigId) || !(*configLength))
            {
                res = CONFIG_ID_NOT_FOUND;
                goto setSpecPreDone;
            }
            strncpy(configId, &tempConfigId[2], strlen(tempConfigId) - 2);
            int paramValue = strtol(data1, NULL, 0);//value
            //configId: "0x0101002323"
            //          byte[2..5] clusterId
            //          byte[6..9] attributeId
            char    clusterId[CLUSTER_ID_SIZE + NULL_CHAR_LENGTH_SIZE] = {0},//one byte for null char
                    attributeId[ATTRIBUTE_ID_SIZE + NULL_CHAR_LENGTH_SIZE] = {0};

            memcpy(clusterId, &configId[0], CLUSTER_ID_SIZE);
            memcpy(attributeId, &configId[CLUSTER_ID_SIZE], ATTRIBUTE_ID_SIZE);
            uint16_t clusterId16 = htoi(clusterId);
            uint16_t attributeId16 = htoi(attributeId);

            printf("Set specification, Write config: clusterId: 0x%04X, attributeId: 0x%04X\n", 
                clusterId16, attributeId16);

            if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, clusterId))
            {
                pzbParam->zclWriteAttr[0].id = attributeId16;
                pzbParam->zclWriteAttr[0].type = htoi(configLength);//save length of string data
                pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_WRITE_ATTRIB;
                pzbParam->destAddr = dev->localId;
                pzbParam->destEndpoint = dev->endpointNum;
                pzbParam->param2 =  clusterId16;  //clusterId
                pzbParam->param3 = HOME_AUTOMATION_PROFILE_ID;        //profileId
                pzbParam->param4 = DEFAULT_INVALID_MANUFACTURER_CODE; //manufacturer code
                pzbParam->param1 = DEFAULT_LENGHT_WRITE_CONFIG;       //attribute Number = 1
                pzbParam->param5 = paramValue;//dataValue
            }
            else
            {
                res = DEV_NOT_SUPPORT_CLUSTER;
                goto setSpecPreDone;
            }
        }
        else
        {
            res = CONFIG_ID_NOT_FOUND;
            goto setSpecPreDone;
        }
    }
    else if (!strcmp(Command, ST_SET) && !strcmp(CommandClass, ST_DOOR_LOCK))
    {
        if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, DOOR_LOCK_CAP_ID))
        {
            zclDoorLockReq reqZclDoorLockReq;
            pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_DOOR_LOCK_REQ;
            pzbParam->destAddr = dev->localId;
            pzbParam->destEndpoint = dev->endpointNum;
            pzbParam->reqZclDoorLockReq = reqZclDoorLockReq;
            if (!strcasecmp(data0, ST_OPEN))
            {
                pzbParam->param1 = ZCL_DOOR_LOCK_CMD_UNLOCK_DOOR;
            }
            else if (!strcasecmp(data0, ST_CLOSE))
            {
                pzbParam->param1 = ZCL_DOOR_LOCK_CMD_LOCK_DOOR;
            }
        }
        else
        {
            res = DEV_NOT_SUPPORT_CLUSTER;
            goto setSpecPreDone;
        }
    }
    else if (!strcmp(CommandClass, ST_USER_CODE))
    {
        if (!data0 || !data1)
        {
            SLOGE("missing data when set usercode\n");
            goto setSpecPreDone;
        }
        if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, DOOR_LOCK_CAP_ID))
        {
            char *userId = data0;
            char *actionStr = data1;

            int action = strtol(actionStr, NULL, 0);

            pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_DOOR_LOCK_REQ;
            pzbParam->destAddr = dev->localId;
            pzbParam->destEndpoint = dev->endpointNum;
            zclDoorLockReq reqZclDoorLockReq;
            if (action == DOORLOCK_USERCODE_ADD)
            {
                if (!data2)
                {
                    goto setSpecPreDone;
                }
                json_object *userCodeObj = VR_(create_json_object)(data2);
                if (!userCodeObj)
                {
                    goto setSpecPreDone;
                }

                CHECK_JSON_OBJECT_EXIST(passObj, userCodeObj, ST_PASS, setSpecPreDone);
                CHECK_JSON_OBJECT_EXIST(nameObj, userCodeObj, ST_NAME, setSpecPreDone);
                const char *pass = json_object_get_string(passObj);
                const char *name = json_object_get_string(nameObj);

                uint16_t userIdValue = strtol(userId, NULL, 0);
                uint16_t maxUsers = get_maximum_user_code(nodeid);

                if(userIdValue >= maxUsers)//start from 0.
                {
                    res = USER_CODE_USERID_INVALID;
                    goto setSpecPreDone;
                }

                int state = check_user_code_info(nodeid, userIdValue, name);
                if(USER_CODE_SAME_NAME == state)
                {
                    res = USER_CODE_NAME_INVALID;
                    goto setSpecPreDone;
                }

                if(USER_CODE_SAME_USER_ID == state)
                {
                    userIdValue = get_user_id_valid(nodeid, maxUsers);
                    if(userIdValue >= maxUsers)
                    {
                        res = USER_CODE_USERID_INVALID;
                        goto setSpecPreDone;
                    }
                }

                reqZclDoorLockReq.setPinCode.user_id = userIdValue;
                reqZclDoorLockReq.setPinCode.user_status = USER_STATUS_OCCUPIED_ENABLED;
                reqZclDoorLockReq.setPinCode.user_type = USER_TYPE_UNRESTRICTED_USER;
                reqZclDoorLockReq.setPinCode.doorLockUserCodeLength = strlen(pass);
                memcpy(reqZclDoorLockReq.setPinCode.doorLockUserCode, pass, reqZclDoorLockReq.setPinCode.doorLockUserCodeLength);
                pzbParam->reqZclDoorLockReq = reqZclDoorLockReq;
                pzbParam->param1 = ZCL_DOOR_LOCK_CMD_SET_PIN_CODE;
                add_user_code_cmd(ADD_USER_CODE_CMD, nodeid, userIdValue, (char*)name);
                json_object_put(userCodeObj);
            }
            else if (action == DOORLOCK_USERCODE_DELETE)
            {
                if (!data2)
                {
                    goto setSpecPreDone;
                }
                reqZclDoorLockReq.clearPinCode.user_id = (uint16_t)strtol(data0, NULL, 0);
                pzbParam->reqZclDoorLockReq = reqZclDoorLockReq;
                /*when remove user, we dont need doorLockUserCode*/
                pzbParam->param1 = ZCL_DOOR_LOCK_CMD_CLEAR_PIN_CODE;
            }
            else if (action == DOORLOCK_USERCODE_DELETE_ALL)
            {
                pzbParam->reqZclDoorLockReq = reqZclDoorLockReq;
                pzbParam->param1 = ZCL_DOOR_LOCK_CMD_CLEAR_ALL_PIN_CODE;
            }
        }
        else
        {
            res = DEV_NOT_SUPPORT_CLUSTER;
            goto setSpecPreDone;
        }
    }
    else if (!strcmp(CommandClass, ST_SWITCH_MULTILEVEL))
    {
        if (!data0)
        {
            SLOGE("missing data when set window covering\n");
            goto setSpecPreDone;
        }
        if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, WINDOW_COVERING_CAP_ID))
        {
            pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_WINDOW_COVERING_REQ;
            pzbParam->destAddr = dev->localId;
            pzbParam->destEndpoint = dev->endpointNum;
            zclWindowCoveringReq_t redZclWindowCovering;
            if (!strcmp(Command, ST_START_LEVEL_CHANGE))
            {
                json_object *startLevelChangeObj = VR_(create_json_object)(data0);
                if (!startLevelChangeObj)
                {
                    goto setSpecPreDone;
                }

                CHECK_JSON_OBJECT_EXIST(upDownObj, startLevelChangeObj, ST_START_LEVEL_CHANGE_UP_DOWN, setSpecPreDone);
                int upDown = json_object_get_int(upDownObj);
                //up: 0, down: 1
                pzbParam->redZclWindowCovering = redZclWindowCovering;
                if(!upDown)
                {
                    pzbParam->param1 = ZCL_WINDOW_COVERING_CMD_UP_OPEN;
                }
                else
                {
                    pzbParam->param1 = ZCL_WINDOW_COVERING_CMD_DOWN_CLOSE;
                }
            }
            else if (!strcmp(Command, ST_STOP_LEVEL_CHANGE))
            {
                pzbParam->redZclWindowCovering = redZclWindowCovering;
                pzbParam->param1 = ZCL_WINDOW_COVERING_CMD_STOP;
            }
        }
        else
        {
            res = DEV_NOT_SUPPORT_CLUSTER;
            goto setSpecPreDone;
        }
    }
    else if (!strcmp(CommandClass, ST_CONFIG_REPORT_ATTRIBUTE))
    {
        /*
        0101 clusterId
        data0: type
        data1: attribute
        data2: minAttribute
        3600: maxAttribute
        command: reportableChange
        zclConfigReportAttribPrepare(&response, ST_CONFIG_REPORT_ATTRIBUTE,
                                 pzbParam, dev->localId, dev->endpointNum, 0x0101,
                                 htoi(data0), htoi(data1), htoi(data2), 3600, htoi64(Command));
                                 */
        //new version
        //data0: numAttribute
        //data1: cluster+attribute
        zclMultiConfigReportAttribPrepare(&response, ST_CONFIG_REPORT_ATTRIBUTE,
                                     pzbParam, dev->localId, dev->endpointNum, htoi(data0), data1);

    }
    else if (!strcmp(CommandClass, ST_BARRIER_OPERATOR))
    {
        //To do in the future
    }
    else if (!strcmp(CommandClass, ST_CLUSTER_ID))
    {
        if (!strcasecmp(Command, ST_WRITE_ATTRIBUTE))
        {
            /*  data0: clusterId
            data1: start AtrributeId
            data2: value*/
            pzbParam->zclWriteAttr[0].id = htoi(data1);
            //pzbParam->zclWriteAttr[0].type = htoi(data2);//save length of string data
            pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_WRITE_ATTRIB;
            pzbParam->destAddr = dev->localId;
            pzbParam->destEndpoint = dev->endpointNum;
            pzbParam->param2 =  htoi(data0);  //clusterId
            pzbParam->param3 = HOME_AUTOMATION_PROFILE_ID;        //profileId
            pzbParam->param4 = DEFAULT_INVALID_MANUFACTURER_CODE; //manufacturer code
            pzbParam->param1 = DEFAULT_LENGHT_WRITE_CONFIG;       //attribute Number = 1
            pzbParam->param5 = htoi(data2);//dataValue
        }
    }
    else if (!strcmp(CommandClass, ST_ASSOCIATION))
    {
        /*
        binding deviceA --> deviceB
        nodeId: uint16_t targetAddrA -->uint64_t srcAddrA
        data0: command Add/Remove
        data1: uint32_t targetAddrB, may be, data0 is childrenId 020BCD
        data2: uint16_t clustId
        */
        //uint32_t targetAddr = (uint32_t)strtol(data1, NULL, 0);
        uint16_t clustId = htoi(data2);
        zigbee_dev_info_t *devB = get_zigbee_dev_from_id(data1);
        if (!devB)
        {
            devB = create_zigbee_dev_from_id(data0);
            if (!devB)
            {
                return DEV_NOT_FOUND;
            }
        }

        if (!devB->active)
        {
            return DEV_UNACTIVATE;
        }

        if (!devB->serial)
        {
            SEARCH_DATA_INIT_VAR(serialB);
            searching_database(_VR_CB_(zigbee), &serialB,
                               "SELECT serial from SUB_DEVICES where id='%s'", data0);
            devB->serial = htoi64(serialB.value);
            FREE_SEARCH_DATA_VAR(serialB);
        }
        if (!strcasecmp(Command, ST_ON_OFF))
        {
            if (!strcasecmp(data0, ST_ADD))
            {
                pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_CREATE_BINDING_ON_REMOTE;
                pzbParam->destAddr = devB->localId; //uint16_t targetAddr
                pzbParam->param4 = devB->serial;    //uint64_t destAddr
                pzbParam->param3 = clustId;         //clusterId
                pzbParam->param5 = dev->serial;     //uint64_t srcAddr
                pzbParam->srcEndpoint = dev->endpointNum;
                pzbParam->destEndpoint = devB->endpointNum;
            }
            else if (!strcasecmp(data0, ST_REMOVE))
            {
                pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_DELETE_BINDING_ON_REMOTE;
                pzbParam->destAddr = devB->localId; //uint16_t targetAddr
                pzbParam->param4 = devB->serial;    //uint64_t destAddr
                pzbParam->param3 = clustId;         //clusterId
                pzbParam->param5 = dev->serial;     //uint64_t srcAddr
                pzbParam->srcEndpoint = dev->endpointNum;
                pzbParam->destEndpoint = devB->endpointNum;
            }
        }
    }

    pzbParam->ret = COMMAND_FAILED;
    insert_to_queue(response, pzbParam);

setSpecPreDone:
    SAFE_FREE(pzbParam);
    return res;
}

static int setspecification(struct ubus_context *ctx, struct ubus_object *obj,
                            struct ubus_request_data *req, const char *method,
                            struct blob_attr *msg)
{
    int res = 0;

    struct blob_attr *tb[6];
    const char *nodeid = ST_UNKNOWN;
    const char *CommandClass = ST_UNKNOWN;
    const char *Command = ST_UNKNOWN;
    const char *data0 = ST_UNKNOWN;
    const char *data1 = ST_UNKNOWN;
    const char *data2 = ST_UNKNOWN;

    blobmsg_parse(set_specification_policy, ARRAY_SIZE(set_specification_policy), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WRITE_SPEC_R));

    if (tb[0] && tb[1] && tb[2])
    {
        nodeid = blobmsg_data(tb[0]);
        CommandClass = blobmsg_data(tb[1]);
        Command = blobmsg_data(tb[2]);
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        if (!tb[0])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: nodeid"));
        }
        else if (!tb[1])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: CommandClass"));
        }
        else if (!tb[2])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: Command"));
        }

        
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
        ubus_send_reply(ctx, req, buff.head);
        return COMMAND_SUCESS;
    }

    if (tb[3])
    {
        data0 = blobmsg_data(tb[3]);
    }

    if (tb[4])
    {
        data1 = blobmsg_data(tb[4]);
    }

    if (tb[5])
    {
        data2 = blobmsg_data(tb[5]);
    }

    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));

    json_object *commandinfo = json_object_new_object();
    json_object_object_add(commandinfo, ST_CLASS, json_object_new_string(CommandClass));
    json_object_object_add(commandinfo, ST_COMMAND, json_object_new_string(Command));
    if (tb[3])
    {
        json_object_object_add(commandinfo, ST_DATA0, json_object_new_string(data0));
    }
    if (tb[4])
    {
        json_object_object_add(commandinfo, ST_DATA1, json_object_new_string(data1));
    }
    if (tb[5])
    {
        json_object_object_add(commandinfo, ST_DATA2, json_object_new_string(blobmsg_data(tb[5])));
    }
    json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

    SLOGI("nodeid = %s\n", nodeid);
    SLOGI("CommandClass = %s\n", CommandClass);
    SLOGI("Command = %s\n", Command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);
    SLOGI("data2 = %s\n", data2);

    zigbee_command_response_t response;
    memset(&response, 0x00, sizeof(zigbee_command_response_t));
    strcpy(response.method, ST_WRITE_SPEC_R);
    strcpy(response.nodeid, nodeid);
    strcpy(response.commandCluster, CommandClass);
    strcpy(response.command, Command);
    if (tb[3])
    {
        strcpy(response.data0, data0);
    }
    if (tb[4])
    {
        strcpy(response.data1, data1);
    }
    if (tb[5])
    {
        strcpy(response.data2, blobmsg_data(tb[5]));
    }

    res = _setSpecificationPrepare(response);
    return_json_message(res, &jobj, (char *)Command, (char *)CommandClass);

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return COMMAND_SUCESS;
}

static int setsecurespecification(struct ubus_context *ctx, struct ubus_object *obj,
                                  struct ubus_request_data *req, const char *method,
                                  struct blob_attr *msg)
{
    //user_code add/remove removeAll
    int res = 0;

    struct blob_attr *tb[6];
    const char *nodeid = ST_UNKNOWN;
    const char *CommandClass = ST_UNKNOWN;
    const char *Command = ST_UNKNOWN;
    const char *data0 = ST_UNKNOWN;
    const char *data1 = ST_UNKNOWN;
    const char *data2 = ST_UNKNOWN;

    blobmsg_parse(set_specification_policy, ARRAY_SIZE(set_specification_policy), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WRITE_S_SPEC_R));

    if (tb[0] && tb[1] && tb[2])
    {
        nodeid = blobmsg_data(tb[0]);
        CommandClass = blobmsg_data(tb[1]);
        Command = blobmsg_data(tb[2]);
        if (tb[3])
        {
            data0 = blobmsg_data(tb[3]);
        }
        if (tb[4])
        {
            data1 = blobmsg_data(tb[4]);
        }
        if (tb[5])
        {
            data2 = blobmsg_data(tb[5]);
        }
    }
    else
    {
        
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_MISSING_ARGUMENT);
        ubus_send_reply(ctx, req, buff.head);
        return COMMAND_SUCESS;
    }

    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));

    json_object *commandinfo = json_object_new_object();
    json_object_object_add(commandinfo, ST_CLASS, json_object_new_string(CommandClass));
    json_object_object_add(commandinfo, ST_COMMAND, json_object_new_string(Command));
    if (tb[3])
    {
        json_object_object_add(commandinfo, ST_DATA0, json_object_new_string(data0));
    }
    if (tb[4])
    {
        json_object_object_add(commandinfo, ST_DATA1, json_object_new_string(data1));
    }
    if (tb[5])
    {
        json_object_object_add(commandinfo, ST_DATA2, json_object_new_string(blobmsg_data(tb[5])));
    }
    json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

    SLOGI("nodeid = %s\n", nodeid);
    SLOGI("CommandClass = %s\n", CommandClass);
    SLOGI("Command = %s\n", Command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);
    SLOGI("data2 = %s\n", data2);

    zigbee_command_response_t response;
    memset(&response, 0x00, sizeof(zigbee_command_response_t));
    strcpy(response.method, ST_WRITE_S_SPEC_R);
    strcpy(response.nodeid, nodeid);
    strcpy(response.commandCluster, CommandClass);
    strcpy(response.command, Command);
    if (tb[3])
    {
        strcpy(response.data0, data0);
    }
    if (tb[4])
    {
        strcpy(response.data1, data1);
    }
    if (tb[5])
    {
        strcpy(response.data2, data2);
    }

    res = _setSpecificationPrepare(response);
    return_json_message(res, &jobj, (char *)Command, (char *)CommandClass);

    
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);
    return COMMAND_SUCESS;
}

/*set specification, association, configuration, switch color*/
int _get_specification_prepare(zigbee_command_response_t response)
{
    int res = 0;
    char *nodeid = &response.nodeid[0];
    char *CommandClass = &response.commandCluster[0];
    char *Command = &response.command[0];
    char *data0 = &response.data0[0];
    char *data1 = &response.data1[0];
    char *data2 = &response.data2[0];

    zigbee_dev_info_t *dev = get_zigbee_dev_from_id(nodeid);
    if (!dev)
    {
        dev = create_zigbee_dev_from_id(nodeid);
        if (!dev)
        {
            return DEV_NOT_FOUND;
        }
    }

    if (!dev->active)
    {
        return DEV_UNACTIVATE;
    }
    if (!dev->serial)
    {
        SEARCH_DATA_INIT_VAR(serialA);
        searching_database(_VR_CB_(zigbee), &serialA,
                           "SELECT serial from SUB_DEVICES where id='%s'", nodeid);
        dev->serial = htoi64(serialA.value);
        FREE_SEARCH_DATA_VAR(serialA);
    }
    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));

    if (!strcmp(CommandClass, ST_BATTERY))
    {
        if (!strcasecmp(Command, ST_GET))
        {
            if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, MULTILEVEL_SENSOR_AIR_TEMP_CAP_ID))
            {
                zclReadAttribPrepare(&response, ST_GET,
                                     pzbParam, dev->localId, dev->endpointNum, ZCL_POWER_CONFIG_CLUSTER_ID,
                                     HOME_AUTOMATION_PROFILE_ID, DEFAULT_MANUFACTURER_CODE,
                                     ZCL_POWER_CONFIGURATION_BATTERY_VOLTAGE);
            }
            else if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, BATTERY_PERCENT_CAP_ID))
            {
                zclReadAttribPrepare(&response, ST_GET,
                                     pzbParam, dev->localId, dev->endpointNum, ZCL_POWER_CONFIG_CLUSTER_ID,
                                     HOME_AUTOMATION_PROFILE_ID, DEFAULT_MANUFACTURER_CODE,
                                     ZCL_POWER_CONFIGURATION_BATTERY_PERCENTAGE_REMAINING);
            }
            else
            {
                res = DEV_NOT_SUPPORT_CLUSTER;
                goto getSpecPreDone;
            }
        }
    }
    else if (!strcmp(CommandClass, ST_METER))
    {
        /*zigbee smartThing Outled
          attributeId           Name                     Type
            0x0000          MeasurementType             32-bit BitMap
            0x0300          ACFrequency             unsigned 16-bit integer
            0x0505          RMSVoltage              unsigned 16-bit integer
            0x0508          RMSCurrent              Unsigned 16-bit integer
            0x050B          ActivePower             Signed 16-bit integer
            Represents the single phase or Phase A, current demand of active power delivered or received at the premises, in Watts (W). 
            Positive values indicate power delivered to the premises where negative values indicatepower received from the premises.
            0x0600          ACVoltageMultiplier     Unsigned 16-bit integer
            0x0601          ACVoltageDivisor        Unsigned 16-bit integer
            0x0602          ACCurrentMultiplier     Unsigned 16-bit integer
            0x0603          ACCurrentDivisor        Unsigned 16-bit integer
            0x0604          ACPowerMultiplier       Unsigned 16-bit integer
            0x0605          ACPowerDivisor          Unsigned 16-bit integer
            if RMSCurrent = 33, ACCurrentMultiplier = 1
                ACCurrentDivisor = 1000
                --> Ireal = RMSCurrent * ACCurrentMultiplier / ACCurrentDivisor = 0.033 (A)
        */
        if (!strcasecmp(Command, ST_SUPPORTED_GET))
        {
            if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, ELECTRICAL_MEASUREMENT_CAP_ID))
            {
                stateElectricMeasurement(ZCL_ELECTRIC_SUPPORT_GET);
                uint16_t attribList[NO_ATTRIBUTE_ID_SUPPORTED_GET] = {ZCL_RMS_CURRENT_ATTRIBUTE_ID,
                                                                     ZCL_RMS_VOLTAGE_ATTRIBUTE_ID, ZCL_ACTIVE_POWER_ATTRIBUTE_ID};
                zclReadAttribPrepareV2(&response, ST_SUPPORTED_GET,
                                       pzbParam, dev->localId, dev->endpointNum, ZCL_ELECTRICAL_MEASUREMENT_CLUSTER_ID,
                                       HOME_AUTOMATION_PROFILE_ID, DEFAULT_MANUFACTURER_CODE,
                                       attribList, NO_ATTRIBUTE_ID_READ_CURRENT);
            }
            else
            {
                res = DEV_NOT_SUPPORT_CLUSTER;
                goto getSpecPreDone;
            }
        }
        else if (!strcasecmp(Command, ST_GET))
        {
            if (!strcasecmp(data1, ST_METER_A))
            {
                if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, ELECTRICAL_MEASUREMENT_CAP_ID))
                {
                    stateElectricMeasurement(ZCL_ELECTRIC_CURRENT);
                    uint16_t attribList[NO_ATTRIBUTE_ID_READ_CURRENT] = {ZCL_RMS_CURRENT_ATTRIBUTE_ID,
                                                                         ZCL_AC_CURRENT_MULTIPLIER_ATTRIBUTE_ID, ZCL_AC_CURRENT_DIVISOR_ATTRIBUTE_ID};
                    zclReadAttribPrepareV2(&response, ST_REAL_RMS_CURRENT,
                                           pzbParam, dev->localId, dev->endpointNum, ZCL_ELECTRICAL_MEASUREMENT_CLUSTER_ID,
                                           HOME_AUTOMATION_PROFILE_ID, DEFAULT_MANUFACTURER_CODE,
                                           attribList, NO_ATTRIBUTE_ID_READ_CURRENT);
                }
                else
                {
                    res = DEV_NOT_SUPPORT_CLUSTER;
                    goto getSpecPreDone;
                }    
            }
            else if (!strcasecmp(data1, ST_METER_V))
            {
                if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, ELECTRICAL_MEASUREMENT_CAP_ID))
                {
                    stateElectricMeasurement(ZCL_ELECTRIC_VOLT);
                    uint16_t attribList[NO_ATTRIBUTE_ID_READ_VOLT] =    {ZCL_RMS_VOLTAGE_ATTRIBUTE_ID, 
                                                                        ZCL_AC_VOLTAGE_MULTIPLIER_ATTRIBUTE_ID, ZCL_AC_VOLTAGE_DIVISOR_ATTRIBUTE_ID};
                    zclReadAttribPrepareV2(&response, ST_RMS_VOLTAGE,
                                           pzbParam, dev->localId, dev->endpointNum, ZCL_ELECTRICAL_MEASUREMENT_CLUSTER_ID,
                                           HOME_AUTOMATION_PROFILE_ID, DEFAULT_MANUFACTURER_CODE,
                                           attribList, NO_ATTRIBUTE_ID_READ_VOLT);
                }
                else
                {
                    res = DEV_NOT_SUPPORT_CLUSTER;
                    goto getSpecPreDone;
                }
            }
            else if (!strcasecmp(data1, ST_METER_W))
            {
                if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, ELECTRICAL_MEASUREMENT_CAP_ID))
                {
                    stateElectricMeasurement(ZCL_ELECTRIC_POWER);
                    uint16_t attribList[NO_ATTRIBUTE_ID_READ_POWER] = {ZCL_ACTIVE_POWER_ATTRIBUTE_ID,
                                                                       ZCL_AC_POWER_MULTIPLIER_ATTRIBUTE_ID, ZCL_AC_POWER_DIVISOR_ATTRIBUTE_ID};
                    zclReadAttribPrepareV2(&response, ST_ACTIVE_POWER,
                                           pzbParam, dev->localId, dev->endpointNum, ZCL_ELECTRICAL_MEASUREMENT_CLUSTER_ID,
                                           HOME_AUTOMATION_PROFILE_ID, DEFAULT_MANUFACTURER_CODE,
                                           attribList, NO_ATTRIBUTE_ID_READ_POWER);
                }
                else
                {
                    res = DEV_NOT_SUPPORT_CLUSTER;
                    goto getSpecPreDone;
                }
            }

        }
    }
    else if (!strcmp(CommandClass, ST_CLUSTER_ID))
    {
        if (!strcasecmp(Command, ST_READ_ATTRIBUTE_ID))
        {
            /*  data0: clusterId
        data1: start AtrributeId*/
            zclReadAttribPrepare(&response, ST_READ_ATTRIBUTE_ID,
                                 pzbParam, dev->localId, dev->endpointNum, htoi(data0),
                                 HOME_AUTOMATION_PROFILE_ID, htoi(data2),
                                 htoi(data1));
        }
        else if (!strcasecmp(Command, ST_DISCOVER))
        {
            /*  data0: clusterId
                data1: start AtrributeId*/
            zclDiscoverAttribPrepare(&response, ST_DISCOVER,
                                     pzbParam, dev->localId, dev->endpointNum, htoi(data0),
                                     HOME_AUTOMATION_PROFILE_ID, htoi(data2),
                                     htoi(data1), MAX_DISCOVER_ATTRIBUTE);
        }
    }
    else if (!strcmp(CommandClass, ST_DOOR_LOCK))
    {
        if (!strcasecmp(Command, ST_STATUS))
        {
            if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, DOOR_LOCK_CAP_ID))
            {
                zclReadAttribPrepare(&response, ST_STATUS,
                                     pzbParam, dev->localId, dev->endpointNum, ZCL_DOOR_LOCK_CLUSTER_ID,
                                     HOME_AUTOMATION_PROFILE_ID, DEFAULT_MANUFACTURER_CODE,
                                     ZCL_DOOR_LOCK_ATTRIB_LOCK_STATE);
            }
            else
            {
                res = DEV_NOT_SUPPORT_CLUSTER;
                goto getSpecPreDone;
            }
        }
    }
    else if (!strcmp(CommandClass, ST_USER_CODE))
    {
        if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, DOOR_LOCK_CAP_ID))
        {
            pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_DOOR_LOCK_REQ;
            pzbParam->destAddr = dev->localId;
            pzbParam->destEndpoint = dev->endpointNum;
            zclDoorLockReq reqZclDoorLockReq;

            if (!strcasecmp(Command, ST_GET))
            {
                reqZclDoorLockReq.get_user_status.user_id = (uint16_t)strtol(data0, NULL, 10);
                pzbParam->reqZclDoorLockReq = reqZclDoorLockReq;
                pzbParam->param1 = ZCL_DOOR_LOCK_CMD_GET_USER_STATUS;
            }
        }
        else
        {
            res = DEV_NOT_SUPPORT_CLUSTER;
            goto getSpecPreDone;
        }
    }
    else if (!strcmp(CommandClass, ST_SENSOR_MULTILEVEL))
    {
        if (!strcasecmp(data0, ST_MULTILEVEL_SENSOR_AIR_TEMPERATURE))
        {
            if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, MULTILEVEL_SENSOR_AIR_TEMP_CAP_ID))
            {
                zclReadAttribPrepare(&response, ST_GET,
                                     pzbParam, dev->localId, dev->endpointNum, ZCL_TEMP_MEASUREMENT_CLUSTER_ID,
                                     HOME_AUTOMATION_PROFILE_ID, DEFAULT_MANUFACTURER_CODE,
                                     ZCL_TEMPERATURE_MEASUREMENT_MEASURED_VALUE);
            }
            else
            {
                res = DEV_NOT_SUPPORT_CLUSTER;
                goto getSpecPreDone;
            }
        }
        else if (!strcasecmp(data0, ST_MULTILEVEL_SENSOR_HUMIDITY))
        {
            if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, MULTILEVEL_SENSOR_HUMIDITY_CAP_ID))
            {
                zclReadAttribPrepare(&response, ST_GET,
                                     pzbParam, dev->localId, dev->endpointNum, ZCL_RELATIVE_HUMIDITY_MEASUREMENT_CLUSTER_ID,
                                     HOME_AUTOMATION_PROFILE_ID, DEFAULT_MANUFACTURER_CODE,
                                     ZCL_HUMIDITY_MEASUREMENT_MEASURED_VALUE);
            }
            else if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, MS_MULTILEVEL_SENSOR_HUMIDITY_CAP_ID))
            {
                zclReadAttribPrepare(&response, ST_MULTILEVEL_SENSOR_HUMIDITY,
                                     pzbParam, dev->localId, dev->endpointNum, ZCL_MS_HUMIDITY_MEASUREMENT_CLUST_ID,
                                     HOME_AUTOMATION_PROFILE_ID, MFG_SMARTSENSE_TEMP_HUMIDITY_SENSOR_OLD_FW,
                                     ZCL_MS_HUMIDITY_MEASUREMENT_MEASURED_VALUE);
            }
            else
            {
                res = DEV_NOT_SUPPORT_CLUSTER;
                goto getSpecPreDone;
            }
        }
        else if (!strcasecmp(data0, ST_MULTILEVEL_SENSOR_LUMINANCE))
        {
            if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, MULTILEVEL_SENSOR_LUMINANCE_CAP_ID))
            {
                zclReadAttribPrepare(&response, ST_GET,
                                     pzbParam, dev->localId, dev->endpointNum, ZCL_ILLUM_MEASUREMENT_CLUSTER_ID,
                                     HOME_AUTOMATION_PROFILE_ID, DEFAULT_MANUFACTURER_CODE,
                                     ZCL_ILLUMINANCE_MEASUREMENT_ATTRIB_MEASURED_VALUE);
            }
            else
            {
                res = DEV_NOT_SUPPORT_CLUSTER;
                goto getSpecPreDone;
            }
        }
    }
    else if (!strcmp(CommandClass, ST_CONFIGURATION))
    {
        char tempConfigId[SIZE_16B]={0}, configLength[MAX_ID_SIZE]={0};
        char configId[SIZE_16B]={0};
        if(!get_configuration_data_from_name((char*)nodeid, (char*)data0, tempConfigId, configLength))
        {
            if(!(*tempConfigId) || !(*configLength))
            {
                res = CONFIG_ID_NOT_FOUND;
                goto getSpecPreDone;
            }
            strncpy(configId, &tempConfigId[2], strlen(tempConfigId) - 2);
            //configId: "0x0101002323"
            //          byte[2..5] clusterId
            //          byte[6..9] attributeId
            //          byte[10..11] dataType
            char    clusterId[CLUSTER_ID_SIZE + NULL_CHAR_LENGTH_SIZE] = {0},//one byte for null char
                    attributeId[ATTRIBUTE_ID_SIZE + NULL_CHAR_LENGTH_SIZE] = {0};
            memcpy(clusterId, &configId[0], CLUSTER_ID_SIZE);
            memcpy(attributeId, &configId[CLUSTER_ID_SIZE], ATTRIBUTE_ID_SIZE);
            uint16_t clusterId16 = htoi(clusterId);
            uint16_t attributeId16 = htoi(attributeId);

            printf("Get specification, get config: clusterId: 0x%04X, attributeId: 0x%04X\n",
                clusterId16, attributeId16);

            if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, clusterId))
            {
                zclReadAttribPrepare(&response, ST_CONFIGURATION,
                     pzbParam, dev->localId, dev->endpointNum, clusterId16,
                     HOME_AUTOMATION_PROFILE_ID, DEFAULT_MANUFACTURER_CODE,
                     attributeId16);
            }
            else
            {
                res = DEV_NOT_SUPPORT_CLUSTER;
                goto getSpecPreDone;
            }
        }
        else
        {
            res = CONFIG_ID_NOT_FOUND;
            goto getSpecPreDone;
        }
    }
    else if (!strcmp(CommandClass, ST_ASSOCIATION))
    {
        if (!strcasecmp(Command, ST_ON_OFF))
        {
            pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_GET_BINDING_TABLE;
            pzbParam->destAddr = dev->localId;
            //remoteBindingNode rbt[MAX_NO_ELEMENTS_REMOTE_BINDING_TABLE];
            //pzbParam->rbt = rbt;
        }
    }
    pzbParam->ret = COMMAND_FAILED;
    insert_to_queue(response, pzbParam);

getSpecPreDone:
    SAFE_FREE(pzbParam);
    return res;
}

static int getsecurespecification(struct ubus_context *ctx, struct ubus_object *obj,
                                  struct ubus_request_data *req, const char *method,
                                  struct blob_attr *msg)
{
    /*user_code get status of userId, get battery percent, doorLock get status**/
    int res = 0;
    struct blob_attr *tb[MAX_NUM_GET_S_SPEC];
    const char *nodeid = ST_UNKNOWN;
    const char *CommandClass = ST_UNKNOWN;
    const char *Command = ST_UNKNOWN;
    const char *data0 = ST_UNKNOWN;
    const char *data1 = ST_UNKNOWN;
    const char *data2 = ST_UNKNOWN;

    blobmsg_parse(get_secure_specification, ARRAY_SIZE(get_secure_specification), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_READ_S_SPEC_R));

    if (tb[0] && tb[1] && tb[2])
    {
        nodeid = blobmsg_data(tb[0]);
        CommandClass = blobmsg_data(tb[1]);
        Command = blobmsg_data(tb[2]);
        if (tb[3])
        {
            data0 = blobmsg_data(tb[3]);
        }
        if (tb[4])
        {
            data1 = blobmsg_data(tb[4]);
        }
        if (tb[5])
        {
            data2 = blobmsg_data(tb[5]);
        }
    }
    else
    {
        
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_MISSING_ARGUMENT);
        ubus_send_reply(ctx, req, buff.head);
        return COMMAND_SUCESS;
    }

    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));

    json_object *commandinfo = json_object_new_object();
    json_object_object_add(commandinfo, ST_CLASS, json_object_new_string(CommandClass));
    json_object_object_add(commandinfo, ST_COMMAND, json_object_new_string(Command));
    if (tb[3])
    {
        json_object_object_add(commandinfo, ST_DATA0, json_object_new_string(data0));
    }
    if (tb[4])
    {
        json_object_object_add(commandinfo, ST_DATA1, json_object_new_string(data1));
    }
    if (tb[5])
    {
        json_object_object_add(commandinfo, ST_DATA2, json_object_new_string(data2));
    }
    json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

    SLOGI("getsecurespecification\n");
    SLOGI("nodeid = %s\n", nodeid);
    SLOGI("CommandClass = %s\n", CommandClass);
    SLOGI("Command = %s\n", Command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);
    SLOGI("data2 = %s\n", data2);

    zigbee_command_response_t response;
    memset(&response, 0x00, sizeof(zigbee_command_response_t));
    strcpy(response.method, ST_READ_S_SPEC_R);
    strcpy(response.nodeid, nodeid);
    strcpy(response.commandCluster, CommandClass);
    strcpy(response.command, Command);
    if (tb[3])
    {
        strcpy(response.data0, data0);
    }
    if (tb[4])
    {
        strcpy(response.data1, data1);
    }
    if (tb[5])
    {
        strcpy(response.data2, data2);
    }
    res = _get_specification_prepare(response);
    return_json_message(res, &jobj, (char *)Command, (char *)CommandClass);

    
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);
    return COMMAND_SUCESS;
}

static int getspecification(struct ubus_context *ctx, struct ubus_object *obj,
                            struct ubus_request_data *req, const char *method,
                            struct blob_attr *msg)
{
    /*get binding table, get sensor air temperature, humidity, time configuration
    ZCL_POWER_CONFIGURATION_BATTERY_VOLTAGE**/
    int res = 0;
    struct blob_attr *tb[MAX_NUM_GET_S_SPEC];
    const char *nodeid = ST_UNKNOWN;
    const char *CommandClass = ST_UNKNOWN;
    const char *Command = ST_UNKNOWN;
    const char *data0 = ST_UNKNOWN;
    const char *data1 = ST_UNKNOWN;
    const char *data2 = ST_UNKNOWN;

    blobmsg_parse(get_secure_specification, ARRAY_SIZE(get_secure_specification), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_READ_SPEC_R));

    if (tb[0] && tb[1] && tb[2])
    {
        nodeid = blobmsg_data(tb[0]);
        CommandClass = blobmsg_data(tb[1]);
        Command = blobmsg_data(tb[2]);
        if (tb[3])
        {
            data0 = blobmsg_data(tb[3]);
        }
        if (tb[4])
        {
            data1 = blobmsg_data(tb[4]);
        }
        if (tb[5])
        {
            data2 = blobmsg_data(tb[5]);
        }
    }
    else
    {
        // usleep(SLEEP_TIME);
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_MISSING_ARGUMENT);
        ubus_send_reply(ctx, req, buff.head);
        return COMMAND_SUCESS;
    }

    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));

    json_object *commandinfo = json_object_new_object();
    json_object_object_add(commandinfo, ST_CLASS, json_object_new_string(CommandClass));
    json_object_object_add(commandinfo, ST_COMMAND, json_object_new_string(Command));
    if (tb[3])
    {
        json_object_object_add(commandinfo, ST_DATA0, json_object_new_string(data0));
    }
    if (tb[4])
    {
        json_object_object_add(commandinfo, ST_DATA1, json_object_new_string(data1));
    }
    if (tb[5])
    {
        json_object_object_add(commandinfo, ST_DATA2, json_object_new_string(data2));
    }
    json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

    SLOGI("getspecification\n");
    SLOGI("nodeid = %s\n", nodeid);
    SLOGI("CommandClass = %s\n", CommandClass);
    SLOGI("Command = %s\n", Command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);
    SLOGI("data2 = %s\n", data2);

    zigbee_command_response_t response;
    memset(&response, 0x00, sizeof(zigbee_command_response_t));
    strcpy(response.method, ST_READ_SPEC_R);
    strcpy(response.nodeid, nodeid);
    strcpy(response.commandCluster, CommandClass);
    strcpy(response.command, Command);
    if (tb[3])
    {
        strcpy(response.data0, data0);
    }
    if (tb[4])
    {
        strcpy(response.data1, data1);
    }
    if (tb[5])
    {
        strcpy(response.data2, data2);
    }

    res = _get_specification_prepare(response);
    return_json_message(res, &jobj, (char *)Command, (char *)CommandClass);

    // usleep(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);
    return COMMAND_SUCESS;
}
/**identify device*/
static int identify_prepare(zigbee_command_response_t response)
{
    int res = COMMAND_SUCESS;
    char *nodeid = &response.nodeid[0];
    char *value = &response.value[0];

    zigbee_dev_info_t *dev = get_zigbee_dev_from_id(nodeid);
    if (!dev)
    {
        dev = create_zigbee_dev_from_id(nodeid);
        if (!dev)
        {
            return DEV_NOT_FOUND;
        }
    }

    if (!dev->active)
    {
        return DEV_UNACTIVATE;
    }
    if (!dev->serial)
    {
        SEARCH_DATA_INIT_VAR(serialA);
        searching_database(_VR_CB_(zigbee), &serialA,
                           "SELECT serial from SUB_DEVICES where id='%s'", nodeid);
        dev->serial = htoi64(serialA.value);
        FREE_SEARCH_DATA_VAR(serialA);
    }
    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));
    //send command
    if (check_zigbee_dev_is_support_cap(nodeid, dev->capList, IDENTIFY_CAP_ID))
    {
        pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZCL_ID_REQ;
        pzbParam->destAddr = dev->localId;
        pzbParam->destEndpoint = dev->endpointNum;
        zclIdReq reqZclIdReq;
        reqZclIdReq.timeout = (uint16_t)strtol(value, NULL, 0);
        pzbParam->reqZclIdReq = reqZclIdReq;
        pzbParam->param1 = ZCL_ID_CMD_ID;
    }
    else
    {
        pzbParam->ret = DEV_NOT_SUPPORT_CLUSTER;
        res = DEV_NOT_SUPPORT_CLUSTER;
    }

    pzbParam->ret = COMMAND_FAILED;
    insert_to_queue(response, pzbParam);

    SAFE_FREE(pzbParam);
    return res;
}
static int identify(struct ubus_context *ctx, struct ubus_object *obj,
                    struct ubus_request_data *req, const char *method,
                    struct blob_attr *msg)
{
    int res = COMMAND_SUCESS;
    struct blob_attr *tb[2];
    const char *nodeid = ST_UNKNOWN;
    const char *value = ST_UNKNOWN;
    SLOGI("in identify\n");

    blobmsg_parse(set_policy, ARRAY_SIZE(set_policy), tb, blob_data(msg), blob_len(msg));
    if (tb[0])
    {
        ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
        memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));

        nodeid = blobmsg_data(tb[0]);
        if (tb[1])
            value = blobmsg_data(tb[1]);

        char tmp[256];
        memset(tmp, 0x00, sizeof(tmp));

        json_object *jobj = json_object_new_object();
        json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
        json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_IDENTIFY_R));
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));
        if (tb[1])
            json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));

        zigbee_command_response_t response;
        memset(&response, 0x00, sizeof(zigbee_command_response_t));
        strcpy(response.method, ST_IDENTIFY_R);
        strcpy(response.nodeid, nodeid);
        if (tb[1])
            strcpy(response.value, value);
        res = identify_prepare(response);
        return_json_message(res, &jobj, NULL, NULL);
        //json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_RECEIVED));

        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
        ubus_send_reply(ctx, req, buff.head);

        json_object_put(jobj);

        if (pzbParam)
        {
            free(pzbParam);
        }

        return res;
    }
    else
    {
        return COMMAND_FAILED;
    }
}


static int setspecification_CRC(struct ubus_context *ctx, struct ubus_object *obj,
                                struct ubus_request_data *req, const char *method,
                                struct blob_attr *msg)
{
    int res = 0;
    //config_associate_command *config_associate_cmd = NULL;

    struct blob_attr *tb[9];
    const char *nodeid = ST_UNKNOWN;
    const char *CommandClass = ST_UNKNOWN;
    const char *Command = ST_UNKNOWN;
    const char *data0 = ST_UNKNOWN;
    const char *data1 = ST_UNKNOWN;
    const char *data2 = ST_UNKNOWN;
    const char *data3 = ST_UNKNOWN;
    const char *data4 = ST_UNKNOWN;
    const char *data5 = ST_UNKNOWN;

    blobmsg_parse(specification_CRC, ARRAY_SIZE(specification_CRC), tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZWAVE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_WRITE_SPEC_CRC_R));

    if (tb[0] && tb[1] && tb[2] && tb[3] && tb[6] && tb[7] && tb[8])
    {
        nodeid = blobmsg_data(tb[0]);
        CommandClass = blobmsg_data(tb[1]);
        Command = blobmsg_data(tb[2]);
        data0 = blobmsg_data(tb[3]);
        if(tb[4])
        {
            data2 = blobmsg_data(tb[4]);
        }
        data3 = blobmsg_data(tb[6]);
        data4 = blobmsg_data(tb[7]);
        data5 = blobmsg_data(tb[8]);

        SLOGI("nodeid = %s\n", nodeid);
        SLOGI("CommandClass = %s\n", CommandClass);
        SLOGI("Command = %s\n", Command);
        SLOGI("data0 = %s\n", data0);//value
        SLOGI("data3 = %s\n", data3);//source_endpoint
        SLOGI("data4 = %s\n", data4);//destination_enpoint
        SLOGI("data5 = %s\n", data5);//is_CRC
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        if (!tb[0])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: NodeID"));
        }
        else if (!tb[1])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: CommandClass"));
        }
        else if (!tb[2])
        {
            json_object_object_add(jobj, ST_REASON, json_object_new_string("Missing Argument: Command"));
        }

        // VR_(usleep)(SLEEP_TIME);
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
        ubus_send_reply(ctx, req, buff.head);
        return 0;
    }

    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(nodeid));

    json_object *commandinfo = json_object_new_object();
    json_object_object_add(commandinfo, ST_CMD_CLASS, json_object_new_string(CommandClass));
    json_object_object_add(commandinfo, ST_CMD, json_object_new_string(Command));
    if (tb[3])
    {
        json_object_object_add(commandinfo, ST_DATA0, json_object_new_string(data0));
    }
    if (tb[4])
    {
        json_object_object_add(commandinfo, ST_DATA1, json_object_new_string(data1));
    }
    if (tb[5])
    {
        json_object_object_add(commandinfo, ST_DATA2, json_object_new_string(blobmsg_data(tb[5])));
    }
    if (tb[6])
    {
        json_object_object_add(commandinfo, "data3", json_object_new_string(blobmsg_data(tb[6])));
    }
    if (tb[7])
    {
        json_object_object_add(commandinfo, "data4", json_object_new_string(blobmsg_data(tb[7])));
    }
    if (tb[8])
    {
        json_object_object_add(commandinfo, "data5", json_object_new_string(blobmsg_data(tb[8])));
    }
    json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

    zigbee_command_response_t response;
    memset(&response, 0x00, sizeof(zigbee_command_response_t));
    strcpy(response.method, ST_WRITE_SPEC_CRC_R);
    strcpy(response.nodeid, nodeid);
    strcpy(response.commandCluster, CommandClass);
    strcpy(response.command, Command);
    if(tb[3])
    {
        //data0 is json format
        strcpy(response.data0, data0);
    }
    if(tb[4])
    {
        strcpy(response.data1, data1);
    }
    if(tb[5])
    {
        strcpy(response.data2, data2);
    }
    if(tb[6])
    {
        strcpy(response.data3, data3);
    }
    if(tb[7])
    {
        strcpy(response.data4, data4);
    }
    if(tb[8])
    {
        strcpy(response.data5, data5);
    }

    res = _setSpecificationPrepare(response);

    return_json_message(res, &jobj, (char*)Command, (char*)CommandClass);

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

/*change name of device into database*/
static int changename(struct ubus_context *ctx, struct ubus_object *obj,
                      struct ubus_request_data *req, const char *method,
                      struct blob_attr *msg)
{
    struct blob_attr *tb[2];
    const char *id = ST_UNKNOWN;
    const char *value = ST_UNKNOWN;
    SLOGI("in changename\n");

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_CHANGE_NAME_R));

    blobmsg_parse(set_policy, ARRAY_SIZE(set_policy), tb, blob_data(msg), blob_len(msg));
    if (tb[0] && tb[1])
    {
        id = blobmsg_data(tb[0]);
        value = blobmsg_data(tb[1]);
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(id));
        json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
        SEARCH_DATA_INIT_VAR(deviceExist);
        searching_database(_VR_CB_(zigbee), &deviceExist,
                           "SELECT id from SUB_DEVICES where id='%s'",
                           id);
        if (deviceExist.len)
        {
            database_actions(_VR_(zigbee), "UPDATE SUB_DEVICES SET friendlyName='%s' "
                                           "WHERE id='%s'",
                             value, id);
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
        }
        else
            json_object_object_add(jobj, ST_STATUS, json_object_new_string("device not found"));
        FREE_SEARCH_DATA_VAR(deviceExist);
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_MISSING_ARGUMENT));
    }

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

/*change alexa supported of dev into database*/
static int alexa(struct ubus_context *ctx, struct ubus_object *obj,
                 struct ubus_request_data *req, const char *method,
                 struct blob_attr *msg)
{
    struct blob_attr *tb[2];
    const char *id = ST_UNKNOWN;
    const char *value = ST_UNKNOWN;
    SLOGI("in alexa\n");

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_ALEXA_R));

    blobmsg_parse(set_policy, ARRAY_SIZE(set_policy), tb, blob_data(msg), blob_len(msg));
    if (tb[0] && tb[1])
    {
        id = blobmsg_data(tb[0]);
        value = blobmsg_data(tb[1]);
        json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(id));
        json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));

        SEARCH_DATA_INIT_VAR(deviceExist);
        searching_database(_VR_CB_(zigbee), &deviceExist,
                           "SELECT id from SUB_DEVICES where id='%s'",
                           id);
        if (deviceExist.len)
        {
            if (!strncasecmp(value, ST_ENABLE, strlen(ST_ENABLE)))
            {
                SEARCH_DATA_INIT_VAR(tmp);
                searching_database(_VR_CB_(zigbee), &tmp,
                                   "SELECT alexa from SUB_DEVICES where id='%s'", id);
                if (tmp.len && !strncasecmp(tmp.value, "NO", 2))
                {
                    database_actions(_VR_(zigbee), "UPDATE SUB_DEVICES SET alexa='%s' "
                                                   "WHERE id='%s'",
                                     "0,0", id);
                }
                FREE_SEARCH_DATA_VAR(tmp);
            }
            else if (!strncasecmp(value, ST_DISABLE, strlen(ST_DISABLE)))
            {
                database_actions(_VR_(zigbee), "UPDATE SUB_DEVICES SET alexa='%s' "
                                               "WHERE id='%s'",
                                 "NO", id);
            }
            json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
        }
        else
            json_object_object_add(jobj, ST_STATUS, json_object_new_string("device not found"));
        FREE_SEARCH_DATA_VAR(deviceExist);
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_MISSING_ARGUMENT));
    }

    // usleep(SLEEP_TIME);
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    return 0;
}

/*reset cancel adding device timer*/
static int reset_stop_add_timer(struct ubus_context *ctx, struct ubus_object *obj,
                                struct ubus_request_data *req, const char *method,
                                struct blob_attr *msg)
{
    SLOGI("in reset_stop_add_timer \n");
    uloop_timeout_set(&cancel_adding_device, ADDING_DEVICE_TIMEOUT * 1000); //5 mins

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
    ubus_send_reply(ctx, req, buff.head);

    return 0;
}

static int zigbee_force_last_status(struct ubus_context *ctx, struct ubus_object *obj,
                                    struct ubus_request_data *req, const char *method,
                                    struct blob_attr *msg) //not ok
{
    SLOGI("in zigbee_force_last_status \n");
    /*get all light and switch (GENERIC_TYPE_SWITCH_BINARY, GENERIC_TYPE_SWITCH_MULTILEVEL)*/

    force_status();
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
    ubus_send_reply(ctx, req, buff.head);

    return 0;
}

/*check state of device*/
static int checking_devices_state(struct ubus_context *ctx, struct ubus_object *obj, //not ok
                                  struct ubus_request_data *req, const char *method,
                                  struct blob_attr *msg)
{
    pthread_mutex_lock(&zigbee_dev_info_listMutex);
    zigbee_dev_info_t *zigbee_dev = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)
    (pos, q, &g_zigbee_dev_list.list)
    {
        zigbee_dev = VR_(list_entry)(pos, zigbee_dev_info_t, list);
        if (zigbee_dev->deviceMode == MODE_NONLISTENING)
        {
            char deviceId[SIZE_32B];
            snprintf(deviceId, sizeof(deviceId) - 1, "%02X", zigbee_dev->localId);
            /* not have WAKE_UP_NOTIFICATION:
            if (zigbee_dev->status.wakeUp == 0)
            {
                char timeStamp[SIZE_32B];
                sprintf(timeStamp, "%u", (unsigned)time(NULL));
                update_zigbee_status(_VR_CB_(zigbee), deviceId, ST_WAKE_UP_NOTIFICATION, timeStamp, ST_REPLACE, 0);
                // tmp->status.wakeUp = (unsigned)time(NULL);
                continue;
            }
            */

            unsigned int time_now = (unsigned)time(NULL);
            unsigned int tmp = time_now - zigbee_dev->status.wakeUp;
            printf("time_now = %u\n", time_now);
            printf("tmp = %u\n", tmp);
            if (tmp > MAXIMUM_DEVICE_TIME_LIFE)
            {
                tmp = time_now - zigbee_dev->status.lastUpdate;
                if (tmp > MAXIMUM_DEVICE_TIME_LIFE)
                {
                    update_zigbee_status(_VR_CB_(zigbee), deviceId, ST_STATE, ST_DEAD, ST_REPLACE, 0);
                    json_object *jobj = json_object_new_object();
                    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
                    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_NOTIFY));
                    json_object_object_add(jobj, ST_NOTIFY_TYPE, json_object_new_string(ST_DEVICE_STATE));
                    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceId));
                    json_object_object_add(jobj, ST_STATE, json_object_new_string(ST_DEAD));

                    char last_update[SIZE_256B];
                    snprintf(last_update, sizeof(last_update) - 1, "%u", zigbee_dev->status.lastUpdate);
                    json_object_object_add(jobj, ST_LAST_UPDATE, json_object_new_string(last_update));
                    Send_ubus_notify((char *)json_object_to_json_string(jobj));
                    json_object_put(jobj);
                }
            }
        }
    }
    pthread_mutex_unlock(&zigbee_dev_info_listMutex);

    
    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, ST_SUCCESSFUL);
    ubus_send_reply(ctx, req, buff.head);

    return 0;
}

//zll command
static int zll_command(struct ubus_context *ctx, struct ubus_object *obj,
                       struct ubus_request_data *req, const char *method,
                       struct blob_attr *msg)
{
    struct blob_attr *tb[MAX_INTER_PAN_POLICY];
    const char *Command = ST_UNKNOWN;
    const char *data0 = ST_UNKNOWN;
    const char *data1 = ST_UNKNOWN;
    const char *data2 = ST_UNKNOWN;

    blobmsg_parse(inter_pan_policy, ARRAY_SIZE(inter_pan_policy), tb, blob_data(msg), blob_len(msg));

    ZigbeeGeckoHandlerParam *pzbParam = (ZigbeeGeckoHandlerParam *)malloc(sizeof(ZigbeeGeckoHandlerParam));
    memset(pzbParam, 0x00, sizeof(ZigbeeGeckoHandlerParam));
    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_ZIGBEE));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_ZIGBEE_LIGHT_LINK_R));

    if (tb[0] && tb[1])
    {

        Command = blobmsg_data(tb[0]);
        data0 = blobmsg_data(tb[1]);
        if (!strcmp(Command, ST_CHANNEL_SCAN))
        {
            pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZLL_CHANNEL_SCAN;
            pzbParam->param1 = (uint8_t)htoi(data0); //scan_duration
        }
        else if (!strcmp(Command, ST_CHANGE_CHANNEL))
        {
            pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZLL_CHANGE_CHANNEL;
            pzbParam->param1 = (uint8_t)htoi(data0); //channelId
        }
        else if (!strcmp(Command, ST_SCAN_REQUEST_AND_IDENTIFY))
        {
            data1 = blobmsg_data(tb[2]);
            data2 = blobmsg_data(tb[3]);

            pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZLL_IDENTIFY;
            pzbParam->param1 = (uint8_t)htoi(data0);    //channelId
            pzbParam->param2 = (uint16_t)htoi(data1);   //panId
            pzbParam->param4 = (uint64_t)htoi64(data2); //eui64Addr/ PanId64
        }
        else if (!strcmp(Command, ST_SCAN_REQUEST_AND_RESET))
        {
            data1 = blobmsg_data(tb[2]);
            data2 = blobmsg_data(tb[3]);
            pzbParam->command = ZIGBEE_GECKO_COMMAND_SPECIFIC_ZLL_RESET;
            pzbParam->param1 = (uint8_t)htoi(data0);    //channelId
            pzbParam->param2 = (uint16_t)htoi(data1);   //panId
            pzbParam->param4 = (uint64_t)htoi64(data2); //eui64Addr/ PanId64

            json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SCAN_REQUEST_AND_RESET));
        }
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_FAILED));
        json_object_object_add(jobj, ST_REASON, json_object_new_string(ST_MISSING_ARGUMENT));

        // usleep(SLEEP_TIME);
        blob_buf_init(&buff, 0);
        blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
        ubus_send_reply(ctx, req, buff.head);
        return COMMAND_SUCESS;
    }
    SLOGI("Command = %s\n", Command);
    SLOGI("data0 = %s\n", data0);
    SLOGI("data1 = %s\n", data1);
    SLOGI("data2 = %s\n", data2);

    zigbee_command_response_t response;
    memset(&response, 0x00, sizeof(zigbee_command_response_t));
    strcpy(response.method, ST_ZIGBEE_LIGHT_LINK_R);
    strcpy(response.command, Command);
    if (tb[1])
    {
        strcpy(response.data0, data0);
    }
    if (tb[2])
    {
        strcpy(response.data1, data1);
    }
    if (tb[3])
    {
        strcpy(response.data2, data2);
    }
    json_object *commandinfo = json_object_new_object();
    json_object_object_add(commandinfo, ST_COMMAND, json_object_new_string(Command));
    if (strcmp(data0, ST_UNKNOWN))
        json_object_object_add(commandinfo, ST_DATA0, json_object_new_string(data0));
    if (strcmp(data1, ST_UNKNOWN))
        json_object_object_add(commandinfo, ST_DATA1, json_object_new_string(data1));
    if (strcmp(data2, ST_UNKNOWN))
        json_object_object_add(commandinfo, ST_DATA2, json_object_new_string(data1));
    json_object_object_add(jobj, ST_COMMAND_INFO, commandinfo);

    insert_to_queue(response, pzbParam);

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, ST_UBUS_MESSAGE_RETURN_KEY, json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);

    json_object_put(jobj);

    if (pzbParam)
    {
        free(pzbParam);
    }
    return ENUM_SUCCESS;
}

// >= 0 no need update.
int is_zigbee_controller_updated(void)
{
    int ret = 1;
    char *version = get_zigbee_controller_version();
    if (!version)
    {
        SLOGE("cant get version from zigbee controller\n");
        return ret;
    }

    SLOGI("zigbee controller version %s\n", version);
    SLOGI("newest version %s\n", ZIGBEE_CONTROLLER_VERSION);

    ret = VR_(version_cmp)(version, ZIGBEE_CONTROLLER_VERSION);
    free(version);
    return ret;
}

int get_firmware(void)
{
    int ret;
    SLOGI("start update zigbee controller firmware %s\n", ZIGBEE_CONTROLLER_VERSION);
    char *firmwarePath;
    firmwarePath = malloc(strlen(ZIGBEE_DEFAULT_FIRMWARE_PATH) + strlen(ZIGBEE_RECV_FIRMWARE_DEFAULT_NAME) + 3);
    sprintf(firmwarePath, "%s/%s", ZIGBEE_DEFAULT_FIRMWARE_PATH, ZIGBEE_RECV_FIRMWARE_DEFAULT_NAME);
    printf("firmwarePath: %s\n", firmwarePath);
    ret = zigbee_update_firmwre(firmwarePath);
    if (0 != ret)
    {
        VR_(execute_system)
        ("ubus send zigbee '{\"state\":\"uart_failed\"}' &");
        free(firmwarePath);
        return -1;
    }
    free(firmwarePath);

    uint8_t status =  zigbeeInitialize();
    return status;
}

////////////////////////// main function //////////////////////////////
int main(int argc, char *argv[])
{
    int c;
    char *pdevID = NULL;
    gLogLevel = 2;

    SLOG_Init(SLOG_STDOUT, NULL);
    set_thread_name("zigbee-gecko-service");

    char cmd[256];
    struct sigaction action;
    memset(cmd, 0x00, sizeof(cmd));
    sprintf(cmd, "%s", "pgrep zigbee | head -n -1 | cut -c-5 | xargs kill -9 &>/dev/null");
    system(cmd);
    if (ENUM_ERROR == zigbee_controller_enable())
    {
        zigbee_controller_reset();
        return ENUM_ERROR;
    }
    while (1)
    {
        static struct option long_options[] =
            {
                /* These options don’t set a flag.
                 We distinguish them by their indices. */
                {"help", no_argument, 0, 'h'},
                {"port", required_argument, 0, 'p'},
                {"loglevel", required_argument, 0, 'l'},
                {0, 0, 0, 0}};
        /* getopt_long stores the option index here. */
        int option_index = 0;

        c = getopt_long(argc, argv, "hp:l",
                        long_options, &option_index);

        /* Detect the end of the options. */
        if (c == ENUM_ERROR)
            break;

        switch (c)
        {
        case 'h':
            printf("Usage: zigbee-gecko-handler-cli [options]\n");
            printf("Options:\n");
            printf("--help(-h): print help information\n");
            printf("--port(-p): specify the dedicated port to control zigbee-gecko controller.\n");
            printf("Without options, the default port is used to connect to zigbee-gecko controller\n");
            exit(EXIT_SUCCESS);
        case 'p':
            pdevID = (char *)malloc(strlen(optarg));
            strncpy(pdevID, optarg, strlen(optarg));
            break;
        case 'l':
            gLogLevel = 99;
            break;
        default:
            printf("Invalid option, please use --help or -h to know how to use\n");
            exit(EXIT_SUCCESS);
            break;
        }
    }

    if (pdevID == NULL)
    {
        pdevID = (char *)malloc(strlen(ZIGBEE_CONSOLE_PORT_DEFAULT) + 1); //+1 for char null
        strncpy(pdevID, ZIGBEE_CONSOLE_PORT_DEFAULT, strlen(ZIGBEE_CONSOLE_PORT_DEFAULT) + 1);
    }

    sleep(2);
    /*for more info http://man7.org/linux/man-pages/man2/sigaction.2.html*/
    /*http://en.cppreference.com/w/c/program/SIG_types*/
    /*https://www.gnu.org/software/libc/manual/html_node/Flags-for-Sigaction.html*/
    //handle signal from system
    memset(&action, 0, sizeof(action));
    action.sa_flags = (SA_NOCLDSTOP | SA_NOCLDWAIT | SA_RESTART);
    action.sa_handler = signal_handler;
    sigaction(SIGSEGV, &action, NULL); //invalid memory access (segmentation fault)
    sigaction(SIGTERM, &action, NULL); //termination request, sent to the program
    sigaction(SIGINT, &action, NULL);  //external interrupt, usually initiated by the user

    //timerInit(); // sleep function will not work after init timer.
    int ret = zigbeeGeckoInitialize(pdevID, &g_zigbee_notify);

    if (ENUM_ERROR == ret) //uart failed
    {
        SLOGE("FAILED TO INITIALIZE UART\n");
        VR_(execute_system)
        ("ubus send zigbee '{\"state\":\"uart_failed\"}' &");
        return ENUM_ERROR;
    }
    else if (ERROR_CHECK_VERSION == ret) //firmware failed
    {
        if (is_zigbee_controller_updated() < 0)
        {
            SLOGE("Need update firmware for zigbee controller\n");
            if ((ret = get_firmware()) != 0)
            {
                SLOGE("FAILED TO INITIALIZE UART\n");
                VR_(execute_system)
                ("ubus send zigbee '{\"state\":\"uart_failed\"}' &");
                return ret;
            }

        }
    }
    else if (ret)
    {
        if ((ret = get_firmware()) != 0)
        {
            SLOGE("FAILED TO INITIALIZE UART\n");
            VR_(execute_system)
            ("ubus send zigbee '{\"state\":\"uart_failed\"}' &");
            return ret;
        }
    }
    /*database save info of device*/
    open_database(DEVICES_DATABASE, &zigbee_db);
    open_database(SUPPORTED_DEVS_DATABASE, &support_devs_db);

    update_zigbee_controll_info();
    /*handle command in queue*/
    init_zigbee_dev_list();
    init_dev_adding_actions_list(); //for manage_adding
    init_dev_specific_list();
    init_queue_table();
    init_user_code_list();

    pthread_create(&zigbee_command_process_thread, NULL, (void *)&zigbee_command_process, NULL);
    pthread_detach(zigbee_command_process_thread);
    pthread_attr_t tattr;
    pthread_attr_init(&tattr);
    pthread_attr_setstacksize(&tattr, 17000);
    pthread_t manage_user_code_thread_t;
    pthread_create(&manage_user_code_thread_t, &tattr, (void *)&manage_user_code_pending_thread, NULL);
    pthread_detach(manage_user_code_thread_t);

    if (shm_init(&g_shm, &g_shmid))
    {
        SLOGI("Failed to init share memory\n");
        return COMMAND_SUCESS;
    }
    // send close when restart service
    VR_(execute_system)
    ("ubus send zigbee '{\"state\":\"close_network\"}' &");
    initHashTableEUI64();
    init_zigbee_dev_info_list();
    init_ubus_service();
    //printf_zigbee_dev_info_list();
    zigbee_ubus_service();
    free_ubus_service();

    zigbee_controller_reset();

    if (pdevID)
    {
        free(pdevID);
    }

    printf("################## END zigbee-gecko ####################\n");
    return ENUM_SUCCESS;
}