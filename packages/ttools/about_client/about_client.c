/******************************************************************************
 * Copyright AllSeen Alliance. All rights reserved.
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ******************************************************************************/

#include <alljoyn_c/AboutData.h>
#include <alljoyn_c/AboutListener.h>
#include <alljoyn_c/AboutObj.h>
#include <alljoyn_c/AboutObjectDescription.h>
#include <alljoyn_c/AboutProxy.h>
#include <alljoyn_c/BusAttachment.h>
#include <alljoyn_c/Init.h>

#include <signal.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include <openssl/bio.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/buffer.h>
#include <json/json.h>

#define CONTROL_INTF "com.verik.bus.VENUS_CONTROL"
#define MANAGE_INTF "com.verik.bus.VENUS_MANAGE"

/*
 * Please note that we don't have error handling logic here to make the sample
 * code easier to read.  This is for demonstration purpose only; actual
 * programs should check the return values of all AllJoyn API calls.
 */

static volatile sig_atomic_t s_interrupt = QCC_FALSE;

static const char* INTERFACE_NAME = "*";

static alljoyn_authlistener g_authListener;

static void CDECL_CALL sig_int_handler(int sig)
{
    QCC_UNUSED(sig);
    s_interrupt = QCC_TRUE;
};

alljoyn_busattachment g_bus;

static char cmd[256] = {NULL};
static char id[256] = {NULL};
static char value[256]= {NULL};
static char type[256] = {NULL};
static char name[256] = {NULL};
static char new_name[256] = {NULL};
static char action[256] = {NULL};
static char data2[256] = {NULL};

static char source_endpoint[256] = {NULL};
static char destination_endpoint[256] = {NULL};
static char is_CRC[256] = {NULL};

char userUUID[256];
char hubID[256];

enum arguments {
    d = 0,  //device id
    c,      //config file
    v,      //value
    l,      //log
    t,      //type of device
    o,      //time out
    ci,      //capability id
    cv,       //capability value
    ct,         //capability time
    n,
    a,
    s,
    y,
    w,
    z
};

struct opts {
    /* the string  */
  const char *str;
    /* the command */
  int optnum;

} opts;

struct opts opt_list[] = {
    {"-d",              d},
    {"-t",              t},
    {"-v",              v},
    {"--value",         v},
    {"-n",              n},
    {"-c",              c},
    {"-a",              a},
    {"-s",              s},

    {"-y",              y},
    {"-w",              w},
    {"-z",              z},
};

struct commandlist {
    const char *command;
};

struct commandlist command_list[]= {
    "help",
    "scan",
    "listDevices",
    "setBinary",
    "addDevices",
    "getBinary",
    "firmwareActions",
    "setRule",
    "getRule",
    "ruleActions",
    "readSpec",
    "writeSpec",
    "readSSpec",
    "writeSSpec",
    "removeDevice",
    "openCloseNetwork",
    "getSubdevs",
    "reset",
    "identify",
    "changeName",
    "alexa",
    "setTime",
    "getTime",
    "autoConf",
    "rediscover",
    "createGroup",
    "groupActions",
    "groupControl",
    "network",
    "readSpecCRC",
    "writeSpecCRC",
    "requestAccess",
    "denyAccess",
    "shareAccess",
    "listUsers",
    "sendReport",
    "wifiSettings",
    "ruleManual",
    "getName",
    "listGroups",
    "venusAlarm",
    "sceneActivation",
    "sceneActions",
};

int commands_check(char *cmd)
{
    int i;
    int numcmds = sizeof(command_list)/sizeof(struct commandlist);
    for(i=0; i<numcmds; i++)
    {
        if(!strcmp(command_list[i].command, cmd))
        {
            return 1;
        }
    }
    return 0;
}

void help(void) {
    printf("Alljoyn Client App version 0.1\n\n");
    printf("usage: ./client command argv\n\n");

    printf("    Available Commands:\n");
    printf("\tlistDevices -t <device type>\t\t\t\t\tShow the list of devices discovered and\n"
                "\t\t\t\t\t\ttheir basic info.\n");

    printf("\tsetBinary -t <device type> -d <device ID> -c <command> -v <setvalue> -n <subdevID> \tSet status of device.\n"
                        "\t\t\t    --value <setvalue>\n"
            );
    printf("\tgetBinary -t <device type> -d <device ID> -v <subdevID>\tGet status of device.\n"
            );
    printf("\taddDevices -t <device type> -d <device ID> -a <bulbid> \tAdd devices\n"
            );
    printf("\tfirmwareActions -a <action> -v <version> \taction: get_current, get_latest, update\n"
            );
    printf("\tsetRule -t <rule type> -n <rule name> -d <conditions> -v <actions>\t set rule\n"
                    "\t\t<conditions>  :  12:31;1,2,3,4,5\n"
                    "\t\t<actions>     :  upnp;controllee_59E;1 or zigbee;33D5;1;01;0104\n"  
            );
    printf("\tgetRule -t all \tget rule\n"
            );
    printf("\truleActions -a <action> -t <rule type> -n <rule name> -c <new name> -d <new conditions> -v <new actions> \ttake actions\n"
            );
    printf("\t\truleActions -a activate/deactivate/delete -t time/d2d -n \"rule 1\" \n"
            );
    printf("\t\truleActions -a update -t time/d2d -n \"rule 1\" -c \"rule 2\" -d \"15:03;noloop\" -v \"zigbee;33D5;1;01;0104\"\n"
            );

    printf("\treadSpec -t <device type> -d <device ID> -c <class> -v <command> -n <type> -a <scale> -s <force>\n"
                "\t   zwave devices\n"
                "\t\t class SENSOR_MULTILEVEL: type: TEMP, HUMI, UV, LUMI scale: 2A\n"
                "\t\t class BATTERY: no need type\n"
                "\t\t class ASSOCIATION: type: Group number\n"
                "\t\t class CONFIGURATION: type: 03 timer\n"
                "\t\t class METER type: ENERGY/POWER\n"
                "\t   zigbee devices\n"
                "\t\t class SENSOR: cmd: TEMP/HUMI\n"
                "\t\t class ASSOCIATION: cmd: ONOFF\n"
                "\t\t class CONFIGURATION: cmd: TIME\n"
                "\t\t class BATTERY: \n"
            );

    printf("\twriteSpec -t <device type> -d <device ID> -c <class> -v <command> -n <data0> -a <data1> -s <data2>\n"
                "\t   zwave devices\n"
                "\t\t class COLOR_CONTROL: data0: RED, data2: BLUE, data1: GREEN\n"
                "\t\t class ASSOCIATION: SET/REMOVE data0: Group number, data1: associated node id\n"
                "\t\t class CONFIGURATION: data0{configid}, data1{data}\n"
                "\t   zigbee devices\n"
                "\t\t class ASSOCIATION: cmd: ONOFF/REPORT data0:ADD/REMOVE data1:assid/zoneid\n"
                "\t\t class CONFIGURATION: cmd: TIME data0: 0000->FFFF\n"
                "\t\t class COLOR_CONTROL: cmd: SET data0: hue, data1: satuaration\n"
            );

    printf("\treadSSpec -t <device type> -d <device ID> -c <class> -v <command> -n <data0>\n"
                "\t   zwave devices\n"
                "\t\t class USER_CODE: GET data0: passcode_address{01->14} \n"
                "\t\t class USER_CODE: GET_NUM: get maximum slot passcode support, no need data0\n"
                "\t\t class BATTERY: no need data0\n"
                "\t\t class CONFIGURATION: data0: register id\n"
                "\t\t class DOOR_LOCK: GET\n"
                "\t   zigbee devices\n"
                "\t\t class USER_CODE: STATUS data0: userid \n"
                "\t\t class DOOR_LOCK: STATUS \n"
            );

    printf("\twriteSSpec -t <device type> -d <device ID> -c <class> -v <command> -n <data0> -a <data1> -s <data2>\n"
                "\t   zwave devices\n"
                "\t\t class DOOR_LOCK: data0: open/close\n"
                "\t\t class USER_CODE: data0: passcode_address{01->14}, data1: is_status{1}, data2: passcode\n"
                "\t\t class CONFIGURATION: data0: config id, data1: data, data2: force\n"
                "\t   zigbee devices\n"
                "\t\t class DOOR_LOCK: data0: open/close\n"
                "\t\t class USER_CODE: ADD data0: userid, data1: passcode\n"
                "\t\t class USER_CODE: REMOVE data0: userid\n"
                "\t\t class USER_CODE: REMOVE_ALL \n"
            );
    printf("\topenCloseNetwork -t <device type> -d <device ID> -a <open/close>\n");
    printf("\tgetSubdevs -t <device type> -d <device ID> -a <ALL_LIST/SCAN_LIST/PAIRED_LIST/UNPAIRED_LIST>\n");
    printf("\tremoveDevice -t <device type> -d <device ID> -a <bulbid>\n");

    printf("\tidentify -t <device type> -d <device ID> -v <time> -n <subdevID> \tidentify\n");
    printf("\tchangeName -t <device type> -d <device ID> -v <name> -n <subdevID> \tchange name\n");
    printf("\talexa -t <device type> -d <device ID> -v <enable/disable> -n <subdevID> \talexa support\n");
    printf("\tsetTime -t <time zone> -v <day and time>  \tset time\n");
    printf("\tgetTime  \tget time\n");
    printf("\tautoConf -t <device type> -v <ssid> \tauto config wifi\n");
    printf("\tcreateGroup -t<userid> -d<appid> -c <group name> -v <deviceid1> -n <devicetype1> -a <deviceid2> -s <devicetype2> \tcreate group\n");
    printf("\tgroupActions -a <action:add/remove> -t <userid> -d <groupid> -c <deviceid> -v <devicetype> \n");
    printf("\tgroupActions -a <action:changename> -t <userid> -d <groupid> -c <newname> \n");
    printf("\tgroupControl -n <userid> -d <groupid> -a <command:ON_OFF/DIM> -v <value>\n");
    printf("\tnetwork -t <device type> -d <device ID> -c <class> -v <command> -n <data0> -a <data1> -s <data2>\n");
    printf("\treadSpecCRC -t <device type> -d <device ID> -c <class> -v <command> -n <data0> -a <data1> -s <data2> "
                             "-y <source_endpoint> - w <destination_endpoint> -z<is_CRC> \n");
    printf("\twriteSpecCRC -t <device type> -d <device ID> -c <class> -v <command> -n <data0> -a <data1> -s <data2> "
                             "-y <source_endpoint> - w <destination_endpoint> -z<is_CRC> \n");
    printf("\twifiSettings -t <userid> -c <mode> -v <ssid> -n <password> -a <encryption> -s <channel>\n"
                "\t\t   mode:scan/get/ap/sta\n"
            );
    printf("\tlistUsers -t <userId> \tlist_users\n");
    printf("\tshareAccess -t <UserId1> -d <UserId2> -n <shareID> -v <UserName> -s <expiration> \tshare_access\n");
    printf("\tdenyAccess -t <UserId1> -d <UserId2> \tdeny_access\n");
    printf("\trequestAccess -t <userID>\trequest_access\n");
    printf("\tvenusAlarm  -a<userID> -t <device type> -d <device ID>  -v <value>\tdisable alarm\n");
    printf("\tgetName -t <device type> -d <device ID> -v <subdevID>\tGet name of device.\n"
            );
    printf("\n");
}

int check_opt(char *argv, struct opts opt_list[], int numopt)
{
    int j;
    for(j=0; j< numopt; j++)
    {
        if(strcasecmp(argv, opt_list[j].str ) == 0)
        {
            return opt_list[j].optnum;
        }
    }
    return -1;
}

json_object* create_json_object(char *string)
{
    struct json_tokener* tok = NULL;
    json_object *jobj = NULL;
    if (NULL == string)
    {
        return NULL;
    }

    tok = json_tokener_new();
    if (tok)
    {
        jobj = json_tokener_parse_ex(tok, string, -1);
        if(json_tokener_success == (enum json_tokener_error)tok->err && jobj)
        {
            //printf ("Object created successfully.\n");
        }
        else
        {
            jobj = NULL;
            //printf ("Object creation failed.\n");
        }
        json_tokener_free(tok);
    }
    return jobj;
}

void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}

int decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key,
  unsigned char *iv, unsigned char *plaintext)
{
    EVP_CIPHER_CTX *ctx;

    int len;

    int plaintext_len;

    /* Create and initialise the context */
    if(!(ctx = EVP_CIPHER_CTX_new())) handleErrors();

    /* Initialise the decryption operation. IMPORTANT - ensure you use a key
    * and IV size appropriate for your cipher
    * In this example we are using 256 bit AES (i.e. a 256 bit key). The
    * IV size for *most* modes is the same as the block size. For AES this
    * is 128 bits */
    if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
    handleErrors();

    /* Provide the message to be decrypted, and obtain the plaintext output.
    * EVP_DecryptUpdate can be called multiple times if necessary
    */
    if(1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
    handleErrors();
    plaintext_len = len;

    /* Finalise the decryption. Further plaintext bytes may be written at
    * this stage.
    */
    if(1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len)) handleErrors();
    plaintext_len += len;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return plaintext_len;
}

int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
  unsigned char *iv, unsigned char *ciphertext)
{
    EVP_CIPHER_CTX *ctx;

    int len;

    int ciphertext_len;

    /* Create and initialise the context */
    if(!(ctx = EVP_CIPHER_CTX_new())) handleErrors();

    /* Initialise the encryption operation. IMPORTANT - ensure you use a key
    * and IV size appropriate for your cipher
    * In this example we are using 256 bit AES (i.e. a 256 bit key). The
    * IV size for *most* modes is the same as the block size. For AES this
    * is 128 bits */
    if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
    handleErrors();

    /* Provide the message to be encrypted, and obtain the encrypted output.
    * EVP_EncryptUpdate can be called multiple times if necessary
    */
    if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
    handleErrors();
    ciphertext_len = len;

    /* Finalise the encryption. Further ciphertext bytes may be written at
    * this stage.
    */
    if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len)) handleErrors();
    ciphertext_len += len;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return ciphertext_len;
}

/**
 * Create an 128 bit key and IV using the supplied key_data. salt can be added for taste.
 * Fills in the encryption and decryption ctx objects and returns 0 on success
 **/
char* base64Encode(unsigned char *str, int len)
{
    BIO *bio, *b64;
    BUF_MEM *buf_encode;
    unsigned char *encStr;

    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new(BIO_s_mem());
    b64 = BIO_push(b64, bio);
    BIO_write(b64, str, len);
    BIO_flush(b64);

    BIO_get_mem_ptr(b64, &buf_encode);

    encStr = (unsigned char*)calloc(1, buf_encode->length);
    strncpy((char*)encStr, buf_encode->data, buf_encode->length);
    encStr[buf_encode->length] = '\0';

    BIO_free_all(b64);
    return (char *)encStr;
}

int base64Decode(char *str, char *decStr, int *decode_len)
{
    char outBuf[256];
    int len = 0;
    BIO *bio, *b64;

    memset(outBuf,0,256);
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_mem_buf(str, strlen(str));
    bio = BIO_push(b64, bio);
    len = BIO_read(b64, &outBuf, 256);
    *decode_len = len;
    memcpy(decStr, outBuf, len);

    BIO_free_all(b64);
    return 0;
}

int psk_encrypt(char *hubID, char *userid, char *psk)
{
    /* Set up the key and iv. Do I need to say to not hard code these in a
    * real application? :-)
    */

    /* A 256 bit key */
    unsigned char *key = (unsigned char *)userid;

    /* A 128 bit IV */
    unsigned char *iv = (unsigned char *)hubID;

    /* Message to be encrypted */
    // unsigned char *plaintext = (unsigned char *)psk;

    /* Buffer for ciphertext. Ensure the buffer is long enough for the
    * ciphertext which may be longer than the plaintext, dependant on the
    * algorithm and mode
    */
    // unsigned char ciphertext[256];

    // int ciphertext_len;

    // /* Initialise the library */
    // ERR_load_crypto_strings();
    // OpenSSL_add_all_algorithms();
    // OPENSSL_config(NULL);

    // /* Encrypt the plaintext */
    // ciphertext_len = encrypt (plaintext, strlen ((char *)plaintext), key, iv,
    //                         ciphertext);

    // /* Do something useful with the ciphertext here */
    // printf("Ciphertext is:\n");
    // BIO_dump_fp (stdout, (const char *)ciphertext, ciphertext_len);
    // char *final = base64Encode(ciphertext, ciphertext_len);
    // printf("final = %s\n", final);
    // strcpy(final_b64, final);
    // if(final)
    // {
    //     free(final);
    // }
    int b64_decode_length;
    unsigned char b64_outBuf[256];
    base64Decode(psk, (char *)b64_outBuf, &b64_decode_length);

    printf("b64_decode_length %d\n", b64_decode_length);
    BIO_dump_fp (stdout, (const char *)b64_outBuf, b64_decode_length);

    /* Buffer for the decrypted text */
    unsigned char decryptedtext[256];
    int decryptedtext_len;
    /* Decrypt the ciphertext */
    decryptedtext_len = decrypt(b64_outBuf, b64_decode_length, key, iv, decryptedtext);
    decryptedtext[decryptedtext_len] = '\0';
    /* Show the decrypted text */
    printf("Decrypted text is:\n");
    printf("%s\n", decryptedtext);

    /* Clean up */
    EVP_cleanup();
    ERR_free_strings();

    return 0;
}

/**
 * Print out the fields found in the AboutData. Only fields with known signatures
 * are printed out.  All others will be treated as an unknown field.
 */
static void printAboutData(alljoyn_aboutdata aboutData, const char* language, int tabNum) {

    size_t count = alljoyn_aboutdata_getfields(aboutData, NULL, 0);
    size_t i = 0;
    int j = 0;
    size_t k = 0;
    const char** fields = (const char**) malloc(sizeof(char*) * count);
    alljoyn_aboutdata_getfields(aboutData, fields, count);
    for (i = 0; i < count; ++i) {
        for (j = 0; j < tabNum; ++j) {
            printf("\t");
        }
        printf("Key: %s", fields[i]);

        alljoyn_msgarg tmp = alljoyn_msgarg_create();
        alljoyn_aboutdata_getfield(aboutData, fields[i], &tmp, language);
        printf("\t");
        char signature[256] = { 0 };
        alljoyn_msgarg_signature(tmp, signature, 16);
        if (!strncmp(signature, "s", 1)) {
            const char* tmp_s;
            alljoyn_msgarg_get(tmp, "s", &tmp_s);
            printf("%s", tmp_s);
        } else if (!strncmp(signature, "as", 2)) {
            size_t las;
            alljoyn_msgarg as_arg = alljoyn_msgarg_create();
            alljoyn_msgarg_get(tmp, "as", &las, &as_arg);

            for (size_t j = 0; j < las; ++j) {
                const char* tmp_s;
                alljoyn_msgarg_get(alljoyn_msgarg_array_element(as_arg, j), "s", &tmp_s);
                printf("%s ", tmp_s);
            }
        } else if (!strncmp(signature, "ay", 2)) {
            size_t lay;
            uint8_t* pay;
            alljoyn_msgarg_get(tmp, "ay", &lay, &pay);
            for (k = 0; k < lay; ++k) {
                printf("%02x ", pay[k]);
            }
        } else {
            printf("User Defined Value\tSignature: %s", signature);
        }
        printf("\n");
    }
    free((void*)fields);
}

typedef struct my_about_listener_t {
    alljoyn_sessionlistener sessionlistener;
    alljoyn_aboutlistener aboutlistener;
}my_about_listener;

static void alljoyn_sessionlistener_connect_lost_cb(const void* context,
                                                    alljoyn_sessionid sessionId,
                                                    alljoyn_sessionlostreason reason)
{
    QCC_UNUSED(context);
    printf("SessionLost sessionId = %u, Reason = %d\n", sessionId, reason);
}

static alljoyn_sessionlistener create_my_alljoyn_sessionlistener()
{
    alljoyn_sessionlistener_callbacks* callbacks =
        (alljoyn_sessionlistener_callbacks*)
        malloc(sizeof(alljoyn_sessionlistener_callbacks));
    callbacks->session_member_added = NULL;
    callbacks->session_member_removed = NULL;
    callbacks->session_lost = alljoyn_sessionlistener_connect_lost_cb;

    return alljoyn_sessionlistener_create(callbacks, NULL);
}

void busobject_object_registered(const void* context)
{
    printf("ObjectRegistered has been called\n");
}

void testsignal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    QStatus status;
    alljoyn_msgarg outArg;
    printf("srcPath = %s\n", srcPath);

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    printf("sessionId = %u\n", sessionId);
    char* testsignal_handle_client;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &testsignal_handle_client);
    printf("%s : \n%s\n",member->name, testsignal_handle_client);

    if(member->name && !strcmp(member->name, "request_access"))
    {
        json_object *jobj = create_json_object(testsignal_handle_client);
        if(jobj)
        {
            json_object *psk = json_object_object_get(jobj, "psk_encrypt");
            if(psk)
            {
                char *psk_encrypt_st = json_object_get_string(psk);
                printf("psk_encrypt_st = %s\n", psk_encrypt_st);
                printf("hubID = %s\n", hubID);
                printf("userUUID = %s\n", userUUID);
                char id[256];
                sprintf(id, "%s99", hubID);
                psk_encrypt(id, userUUID, psk_encrypt_st);
            }
        }
    }

    // status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &testsignal_handle_client);
    // if (ER_OK != status) {
    //     printf("Error reading alljoyn_message: value\n");
    //     return;
    // }
    // printf("testsignal_handle_client = %s\n", testsignal_handle_client);

    // outArg = alljoyn_msgarg_create_and_set("s", "reply_signal");
    // status  = alljoyn_busobject_signal(testObj, NULL, sessionId, *member, outArg , 1, (unsigned char) 0, 0, NULL);
    // alljoyn_msgarg_destroy(outArg);
    //s_interrupt = QCC_TRUE;
}

void notifysignal_handle(const alljoyn_interfacedescription_member* member, const char* srcPath, alljoyn_message msg)
{
    QStatus status;
    alljoyn_msgarg outArg;
    printf("srcPath = %s\n", srcPath);

    alljoyn_sessionid sessionId = alljoyn_message_getsessionid(msg);

    printf("sessionId = %u\n", sessionId);
    char* notifysignal_handle_client;
    status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 0), "s", &notifysignal_handle_client);
    printf("receive notify %s : \n %s\n", member->name, notifysignal_handle_client);

    // status = alljoyn_msgarg_get(alljoyn_message_getarg(msg, 1), "s", &testsignal_handle_client);
    // if (ER_OK != status) {
    //     printf("Error reading alljoyn_message: value\n");
    //     return;
    // }
    // printf("testsignal_handle_client = %s\n", testsignal_handle_client);

    // outArg = alljoyn_msgarg_create_and_set("s", "reply_signal");
    // status  = alljoyn_busobject_signal(testObj, NULL, sessionId, *member, outArg , 1, (unsigned char) 0, 0, NULL);
    // alljoyn_msgarg_destroy(outArg);
    //s_interrupt = QCC_TRUE;
}

QCC_BOOL AJ_CALL request_credentials(const void* context, const char* authMechanism, const char* authPeer, uint16_t authCount,
                                     const char* userName, uint16_t credMask, alljoyn_credentials credentials)
{
    printf("request_credentials for authenticating %s using mechanism %s\n", authPeer, authMechanism);
    if (!strcmp(authMechanism, "ALLJOYN_ECDHE_PSK")) 
    {
        printf("userName = %s\n", userName);
        printf("authCount = %u\n", authCount);
        printf("credMask = %u\n", credMask);
        alljoyn_credentials_setpassword(credentials, "7EBE828837EF096870979676781A3FFE");
        // alljoyn_credentials_setpassword(credentials, "faaa0af3dd3f1e0379da046a3ab6ca45");
        alljoyn_credentials_setexpiration(credentials, 10);
        return QCC_TRUE;
    }
    return QCC_FALSE;
}

void AJ_CALL authentication_complete(const void* context, const char* authMechanism, const char* peerName, QCC_BOOL success)
{
    printf("authentication_complete %s %s\n", authMechanism, success == QCC_TRUE ? "successful" : "failed");
}

void AJ_CALL authlistener_securityviolation (const void *context, QStatus status, const alljoyn_message msg)
{
    printf("authlistener_securityviolation\n");
}

QCC_BOOL AJ_CALL authlistener_verifycredentials (const void *context, const char *authMechanism, const char *peerName, const alljoyn_credentials credentials)
{
    printf("authlistener_verifycredentials\n");
}

static void announced_cb(const void* context,
                         const char* busName,
                         uint16_t version,
                         alljoyn_sessionport port,
                         const alljoyn_msgarg objectDescriptionArg,
                         const alljoyn_msgarg aboutDataArg)
{
    my_about_listener* mylistener = (my_about_listener*) context;
    alljoyn_aboutobjectdescription objectDescription = alljoyn_aboutobjectdescription_create();
    alljoyn_aboutobjectdescription_createfrommsgarg(objectDescription, objectDescriptionArg);
    printf("*********************************************************************************\n");
    printf("Announce signal discovered\n");
    printf("\tFrom bus %s\n", busName);
    printf("\tAbout version %hu\n", version);
    printf("\tSessionPort %hu\n", port);
    printf("\tObjectDescription:\n");

    alljoyn_aboutobjectdescription aod = alljoyn_aboutobjectdescription_create();
    alljoyn_aboutobjectdescription_createfrommsgarg(aod, objectDescriptionArg);

    size_t path_num = alljoyn_aboutobjectdescription_getpaths(aod, NULL, 0);
    const char** paths = (const char**) malloc(sizeof(const char*) * path_num);
    alljoyn_aboutobjectdescription_getpaths(aod, paths, path_num);

    for (size_t i = 0; i < path_num; ++i) {
        printf("\t\t%s\n", paths[i]);
        size_t intf_num = alljoyn_aboutobjectdescription_getinterfaces(aod, paths[i], NULL, 0);
        const char** intfs = (const char**) malloc(sizeof(const char*) * intf_num);
        alljoyn_aboutobjectdescription_getinterfaces(aod, paths[i], intfs, intf_num);

        for (size_t j = 0; j < intf_num; ++j) {
            printf("\t\t\t%s\n", intfs[j]);
        }
        free((void*)intfs);
        intfs = NULL;
    }
    free((void*) paths);
    paths = NULL;

    printf("\tAboutData:\n");
    alljoyn_aboutdata aboutData = alljoyn_aboutdata_create_full(aboutDataArg, "en");
    printAboutData(aboutData, NULL, 2);
    printf("*********************************************************************************\n");

    char *deviceId = NULL;
    alljoyn_aboutdata_getdeviceid(aboutData, &deviceId);
    printf("deviceId = %s\n", deviceId);
    QStatus status;
    if (g_bus != NULL) {
        //if(strcmp(deviceId, "93c06771-c725-48c2-b1ff-544a161942cf"))
            //if(strcmp(deviceId, "107cec79c9abb4"))
            // if(strcmp(deviceId, "11ec24b8163f93"))
            // if(strcmp(deviceId, "11ec24b81653c9"))
            if(strcmp(deviceId, "3e1428b61566480e8aec97dd00d50f00"))
            // if(strcmp(deviceId, "11ec24b8169652"))
            
            //if(strcmp(deviceId, "93c06771-c725-48c2-b1ff-5c313ee31350"))
        //if(strcmp(deviceId, "93c06771-c725-48c2-b1ff-5c313ee32ac3"))
            
            goto failed;
        strcpy(hubID, deviceId);
        alljoyn_sessionid sessionId;
        alljoyn_sessionopts sessionOpts =
            alljoyn_sessionopts_create(ALLJOYN_TRAFFIC_TYPE_MESSAGES,
                                       QCC_FALSE, ALLJOYN_PROXIMITY_ANY,
                                       ALLJOYN_TRANSPORT_ANY);

        alljoyn_busattachment_enableconcurrentcallbacks(g_bus);
        status =
            alljoyn_busattachment_joinsession(g_bus, busName, port,
                                              mylistener->sessionlistener,
                                              &sessionId, sessionOpts);
        if (ER_OK == status && 0 != sessionId) {
            // alljoyn_aboutproxy aboutProxy =
            //     alljoyn_aboutproxy_create(g_bus, busName, sessionId);
            // alljoyn_msgarg objArg = alljoyn_msgarg_create();
            // alljoyn_aboutproxy_getobjectdescription(aboutProxy, objArg);
            // printf("*********************************************************************************\n");
            // printf("AboutProxy.GetObjectDescription:\n");
            // alljoyn_aboutobjectdescription aod2 =
            //     alljoyn_aboutobjectdescription_create();
            // alljoyn_aboutobjectdescription_createfrommsgarg(aod2, objArg);

            // path_num = alljoyn_aboutobjectdescription_getpaths(aod2, NULL, 0);
            // paths = (const char**) malloc(sizeof(const char*) * path_num);
            // alljoyn_aboutobjectdescription_getpaths(aod2, paths, path_num);

            // for (size_t i = 0; i < path_num; ++i) {
            //     printf("\t\t%s\n", paths[i]);
            //     size_t intf_num =
            //         alljoyn_aboutobjectdescription_getinterfaces(aod2,
            //                                                      paths[i],
            //                                                      NULL, 0);
            //     const char** intfs =
            //         (const char**) malloc(sizeof(const char*) * intf_num);
            //     alljoyn_aboutobjectdescription_getinterfaces(aod2, paths[i],
            //                                                  intfs, intf_num);

            //     for (size_t j = 0; j < intf_num; ++j) {
            //         printf("\t\t\t%s\n", intfs[j]);
            //     }
            //     free((void*)intfs);
            //     intfs = NULL;
            // }
            // free((void*) paths);
            // paths = NULL;

            // alljoyn_msgarg aArg = alljoyn_msgarg_create();
            // alljoyn_aboutproxy_getaboutdata(aboutProxy, "en", aArg);
            // printf("*********************************************************************************\n");
            // printf("AboutProxy.GetAboutData: (Default Language)\n");
            // aboutData = alljoyn_aboutdata_create("en");
            // alljoyn_aboutdata_createfrommsgarg(aboutData, aArg, "en");
            //printAboutData(aboutData, NULL, 1);
            

            // size_t lang_num =
            //     alljoyn_aboutdata_getsupportedlanguages(aboutData, NULL, 0);
            // if (lang_num > 1) {
            //     const char** langs =
            //         (const char**) malloc(sizeof(char*) * lang_num);
            //     alljoyn_aboutdata_getsupportedlanguages(aboutData,
            //                                             langs,
            //                                             lang_num);
            //     char* defaultLanguage;
            //     alljoyn_aboutdata_getdefaultlanguage(aboutData,
            //                                          &defaultLanguage);
            //     /*
            //      * Print out the AboutData for every language but the
            //      * default it has already been printed.
            //      */
            //     for (size_t i = 0; i < lang_num; ++i) {
            //         if (strcmp(defaultLanguage, langs[i]) != 0) {
            //             status = alljoyn_aboutproxy_getaboutdata(aboutProxy,
            //                                                      langs[i],
            //                                                      aArg);
            //             if (ER_OK == status) {
            //                 alljoyn_aboutdata_createfrommsgarg(aboutData,
            //                                                    aArg,
            //                                                    langs[i]);
            //                 printAboutData(aboutData, langs[i], 1);
            //             }
            //         }
            //     }
            //     free((void*)langs);
            //     langs = NULL;
            //     uint16_t ver;
            //     alljoyn_aboutproxy_getversion(aboutProxy, &ver);
            //     printf("*********************************************************************************\n");
            //     printf("AboutProxy.GetVersion %hd\n", ver);
            //     printf("*********************************************************************************\n");

                const char* path;
                alljoyn_aboutobjectdescription_getinterfacepaths(objectDescription,
                                                                 CONTROL_INTF,
                                                                 &path, 2);
                // alljoyn_aboutobjectdescription_getinterfacepaths(objectDescription,
                //                                                  MANAGE_INTF,
                //                                                  &path, 2);
                printf("++++++++++++++++++++++ path = %s\n", path);

                // if(deviceId)
                // {
                //     int i;
                //     printf("length device id = %d\n", strlen(deviceId));
                //     for(i=0; i<strlen(deviceId); i++)
                //     {
                //         printf("%c\n", deviceId[i]);
                //     }
                // }                

                if(path && strlen(path) > 2 )
                //if(path && strlen(path) > 2 && !strcasecmp(deviceId, "93c06771-c725-48c2-b1ff-5C313EE32901"))
                //if(path && strlen(path) > 2 && !strcmp(deviceId, "93c06771-c725-48c2-b1ff-12345test"))
                {
                    alljoyn_proxybusobject proxyObject = alljoyn_proxybusobject_create(g_bus, busName,
                                                 path, sessionId);
                    alljoyn_interfacedescription exampleIntf = NULL;
                    status = alljoyn_proxybusobject_introspectremoteobject(proxyObject);
                    if (status != ER_OK) {
                        printf("Failed to introspect remote object.\n");
                    }

                    exampleIntf = alljoyn_proxybusobject_getinterface(proxyObject, CONTROL_INTF);
                    // exampleIntf = alljoyn_proxybusobject_getinterface(proxyObject, MANAGE_INTF);
                    status = alljoyn_proxybusobject_secureconnection(proxyObject, QCC_TRUE); 

                    // exampleIntf = alljoyn_proxybusobject_getinterface(proxyObject, MANAGE_INTF);
                   
                   alljoyn_busobject_callbacks busObjCbs = {
                        NULL,
                        NULL,
                        &busobject_object_registered,
                        NULL
                    };
                    QCC_BOOL foundMember = QCC_FALSE;
                    alljoyn_busobject testObj = alljoyn_busobject_create(path, QCC_FALSE, &busObjCbs, NULL);

                    alljoyn_busobject_addinterface(testObj, exampleIntf);

                    alljoyn_busattachment_registerbusobject(g_bus, testObj);
                    alljoyn_interfacedescription_member testsignal_member;
                    alljoyn_interfacedescription_member notifysignal_member;

                    foundMember = alljoyn_interfacedescription_getmember(exampleIntf, "notify", &notifysignal_member);
                    assert(foundMember == QCC_TRUE);
                    if (!foundMember)
                    {
                        printf("Failed to get notifysignal_member of interface\n");
                    }
                    else
                    {
                        printf("succeeded to notifysignal_member member of interface\n");
                    }

                    printf("name = %s\n", notifysignal_member.name);
                    printf("signature = %s\n", notifysignal_member.signature);
                    printf("argNames = %s\n", notifysignal_member.argNames);
                    alljoyn_busattachment_registersignalhandler(g_bus, notifysignal_handle, notifysignal_member, NULL);

                    foundMember = QCC_FALSE;
                    foundMember = alljoyn_interfacedescription_getmember(exampleIntf, cmd, &testsignal_member);
                    assert(foundMember == QCC_TRUE);
                    if (!foundMember)
                    {
                        printf("Failed to get testsignal member of interface\n");
                    }
                    else
                    {
                        printf("succeeded to get testsignal member of interface\n");
                    }

                    printf("name = %s\n", testsignal_member.name);
                    printf("signature = %s\n", testsignal_member.signature);
                    printf("argNames = %s\n", testsignal_member.argNames);

                    alljoyn_busattachment_registersignalhandler(g_bus, testsignal_handle, testsignal_member, NULL);
                    alljoyn_msgarg outArg;
                    size_t numArgs;
                    if(!strcmp(cmd, "setBinary"))
                    {
                        outArg = alljoyn_msgarg_array_create(5);
                        numArgs = 5;
                        if(strlen(name) )
                            alljoyn_msgarg_array_set(outArg, &numArgs, "sssss", type, id, new_name, value, name);
                        else 
                            alljoyn_msgarg_array_set(outArg, &numArgs, "sssss", type, id, new_name, value, NULL);
                    }
                    else
                    {
                        if(!strcmp(cmd, "getBinary") || !strcmp(cmd, "getName"))
                        {
                            outArg = alljoyn_msgarg_array_create(3);
                            numArgs = 3;
                            if(!strcmp(type, "zigbee"))
                                alljoyn_msgarg_array_set(outArg, &numArgs, "sss", type, id, NULL);
                            else
                            {
                                if(strlen(value))
                                {
                                    alljoyn_msgarg_array_set(outArg, &numArgs, "sss", type, id, value);
                                }
                                else
                                {
                                    alljoyn_msgarg_array_set(outArg, &numArgs, "sss", type, id, NULL);
                                }
                            } 
                        }
                        else
                        if(!strcmp(cmd, "setRule"))
                        {
                            outArg = alljoyn_msgarg_array_create(4);
                            numArgs = 4;
                            alljoyn_msgarg_array_set(outArg, &numArgs, "ssss", type, name, id, value);
                        }
                        else
                        if(!strcmp(cmd, "ruleActions"))
                        {
                            outArg = alljoyn_msgarg_array_create(6);
                            numArgs = 6;
                            if(!strcmp(action, "update"))
                            {
                                alljoyn_msgarg_array_set(outArg, &numArgs, "ssssss", action, type, name, new_name, id, value); 
                            }
                            else
                            {
                                alljoyn_msgarg_array_set(outArg, &numArgs, "ssssss", action, type, name, NULL, NULL, NULL); 
                            }
                        }
                        else
                        if(!strcmp(cmd, "readSpec"))
                        {
                            outArg = alljoyn_msgarg_array_create(7);
                            numArgs = 7;
                            if(!strcmp(new_name,"meter"))
                            {
                                alljoyn_msgarg_array_set(outArg, &numArgs, "sssssss", type, id, new_name, value, name, NULL, NULL);
                            }
                            else
                            {
                                alljoyn_msgarg_array_set(outArg, &numArgs, "sssssss", type, id, new_name, value, name, action, data2);
                            }
                        }
                        else
                        if(!strcmp(cmd, "writeSpec") || !strcmp(cmd, "writeSSpec") || !strcmp(cmd, "network"))
                        {
                            outArg = alljoyn_msgarg_array_create(7);
                            numArgs = 7;
                            alljoyn_msgarg_array_set(outArg, &numArgs, "sssssss", type, id, new_name, value, name, action, data2);
                        }
                        else
                        if(!strcmp(cmd, "readSSpec"))
                        {
                            outArg = alljoyn_msgarg_array_create(5);
                            numArgs = 5;
                            alljoyn_msgarg_array_set(outArg, &numArgs, "sssss", type, id, new_name, value, name);
                        }
                        else
                        if(!strcmp(cmd, "openCloseNetwork") || !strcmp(cmd, "getSubdevs"))
                        {
                            outArg = alljoyn_msgarg_array_create(3);
                            numArgs = 3;
                            if(!strcmp(type,"upnp"))
                                alljoyn_msgarg_array_set(outArg, &numArgs, "sss", type, id, action);
                            else 
                                alljoyn_msgarg_array_set(outArg, &numArgs, "sss", type, NULL, action);
                        }
                        else
                        if(!strcmp(cmd, "removeDevice"))
                        {
                            outArg = alljoyn_msgarg_array_create(3);
                            numArgs = 3;
                            if(!strcmp(type,"upnp"))
                                alljoyn_msgarg_array_set(outArg, &numArgs, "sss", type, id, action);
                            else 
                                alljoyn_msgarg_array_set(outArg, &numArgs, "sss", type, id, NULL);
                        }
                        else
                        if(!strcmp(cmd, "addDevices"))
                        {
                            outArg = alljoyn_msgarg_array_create(3);
                            numArgs = 3;
                            if(!strcmp(type,"upnp"))
                                alljoyn_msgarg_array_set(outArg, &numArgs, "sss", type, id, action);
                            else if(!strcmp(type,"zwave"))
                                alljoyn_msgarg_array_set(outArg, &numArgs, "sss", type, NULL, NULL);
                        }
                        else
                        if(!strcmp(cmd, "identify") || !strcmp(cmd, "changeName") || !strcmp(cmd, "alexa"))
                        {
                            outArg = alljoyn_msgarg_array_create(4);
                            numArgs = 4;
                            if(strlen)
                            if(strlen(name))
                                alljoyn_msgarg_array_set(outArg, &numArgs, "ssss", type, id, value, name);
                            else 
                                alljoyn_msgarg_array_set(outArg, &numArgs, "ssss", type, id, value, NULL);
                        }
                        else
                        if(!strcmp(cmd, "setTime") || !strcmp(cmd, "autoConf") || !strcmp(cmd, "rediscover"))
                        {  
                            outArg = alljoyn_msgarg_array_create(2);
                            numArgs = 2;
                            alljoyn_msgarg_array_set(outArg, &numArgs, "ss", type, value);
                        }
                        else
                        if(!strcmp(cmd, "getTime"))
                        {  
                            outArg = alljoyn_msgarg_array_create(1);
                            numArgs = 1;
                            alljoyn_msgarg_array_set(outArg, &numArgs, "s", NULL);
                        }
                        else
                        if(!strcmp(cmd, "wifiSettings"))
                        {
                            outArg = alljoyn_msgarg_array_create(6);
                            numArgs = 6;
                            alljoyn_msgarg_array_set(outArg, &numArgs, "ssssss", type, new_name, value, name, action, data2);
                                                                                // -t    -c        -v     -n    -a      -s
                        }
                        else
                        if(!strcmp(cmd, "createGroup"))
                        {
                            numArgs = 4;
                            outArg = alljoyn_msgarg_array_create(numArgs);
                            alljoyn_msgarg_array_set(outArg, &numArgs, "ssss", type, id, new_name, value);
                                                                            // -t   -d     -c        -v 
                        }
                        else
                        if(!strcmp(cmd, "groupActions"))
                            {
                            outArg = alljoyn_msgarg_array_create(5);
                            numArgs = 5;
                            if(!strcmp(action, "delete"))
                                alljoyn_msgarg_array_set(outArg, &numArgs, "sssss", action, type, id, NULL, NULL);
                                                                                    // -a    -t    -d    
                            else
                            {
                                alljoyn_msgarg_array_set(outArg, &numArgs, "sssss", action, type, id, new_name, value);
                                                                                    // -a    -t    -d   -c    -v 
                            }
                        }
                        else
                        if(!strcmp(cmd, "firmwareActions"))
                        {  
                            outArg = alljoyn_msgarg_array_create(2);
                            numArgs = 2;
                            alljoyn_msgarg_array_set(outArg, &numArgs, "ss", action, value);
                        }
                        else
                        if(!strcmp(cmd, "groupControl"))
                        {  
                            outArg = alljoyn_msgarg_array_create(4);
                            numArgs = 4;
                            alljoyn_msgarg_array_set(outArg, &numArgs, "ssss", name, id, action, value);
                                                                            // -n    -d   -a    -v 
                        }
                        else
                        if(!strcmp(cmd, "readSpecCRC") || !strcmp(cmd, "writeSpecCRC"))
                        {  
                            outArg = alljoyn_msgarg_array_create(10);
                            numArgs = 10;
                            alljoyn_msgarg_array_set(outArg, &numArgs, "ssssssssss", type, id, new_name, value, name, action, data2,
                                                            source_endpoint, destination_endpoint, is_CRC);
                        }

                        else
                        if(!strcmp(cmd, "denyAccess") || !strcmp(cmd, "sendReport"))
                        {  
                            outArg = alljoyn_msgarg_array_create(2);
                            numArgs = 2;
                            alljoyn_msgarg_array_set(outArg, &numArgs, "ss", type, id);
                        }
                        else
                        if(!strcmp(cmd, "shareAccess") || !strcmp(cmd, "sceneActions"))
                        {  
                            outArg = alljoyn_msgarg_array_create(5);
                            numArgs = 5;
                            alljoyn_msgarg_array_set(outArg, &numArgs, "sssss", type, id, name, value, data2);//-t -d -n -v -s
                        }
                        else
                        if(!strcmp(cmd, "ruleManual"))
                        {  
                            numArgs = 3;
                            outArg = alljoyn_msgarg_array_create(numArgs);
                            alljoyn_msgarg_array_set(outArg, &numArgs, "ss", id, action);//-d -a
                        }
                        else
                        if(!strcmp(cmd, "venusAlarm") || !strcmp(cmd, "sceneActivation"))
                        {
                            outArg = alljoyn_msgarg_array_create(4);
                            numArgs = 4;
                            alljoyn_msgarg_array_set(outArg, &numArgs, "ssss", action, type, id, value);// -t -a -d -v
                        }
                        else
                        if(!strcmp(cmd, "requestAccess"))
                        {
                            outArg = alljoyn_msgarg_array_create(1);
                            numArgs = 1;
                            strcpy(userUUID, type);
                            alljoyn_msgarg_array_set(outArg, &numArgs, "s", type);
                        }
                        else
                        if(!strcmp(cmd, "reset"))
                        {
                            numArgs = 2;
                            outArg = alljoyn_msgarg_array_create(numArgs);
                            strcpy(userUUID, type);
                            alljoyn_msgarg_array_set(outArg, &numArgs, "ss", type, action);
                        }
                        else    
                        {  
                            outArg = alljoyn_msgarg_array_create(1);
                            numArgs = 1;
                            alljoyn_msgarg_array_set(outArg, &numArgs, "s", type);
                        }
                    }
                    
                    
                    // alljoyn_msgarg outArg = alljoyn_msgarg_create();
                    // size_t numArgs = 5;
                    //  // alljoyn_msgarg_array_set(outArg, &numArgs, "sssss", "zigbee", "2B5D", "0", "01", "0104");
                    // //alljoyn_msgarg_array_set(outArg, &numArgs, "sssss", "upnp", "controllee_59E", "0", NULL, NULL);
                    //  printf("Send signal\n");
                    //  char* fruits[5] =  { NULL };
                    //  int i;
                    //  for(i = 0; i< 5; i++)
                    //  {
                    //     printf("fruits[%d] = %s\n", i, fruits[i]);
                    //  }

                    // char *msg0 = "upnp";char *msg1 = "controllee_59E";char *msg2 = "0";char *msg3 = "A"; char *msg4 = "A";
                    // fruits[0]=msg0; fruits[1]=msg1; fruits[2]=msg2; fruits[3]=msg3; fruits[4]=msg4;

                    // for(i = 0; i< 5; i++)
                    //  {
                    //     printf("fruits[%d] = %s\n", i, fruits[i]);
                    //  }
                    //  alljoyn_msgarg_set(outArg, "as", 5, fruits);
                    //  printf("tubb\n");
                    int repeat = 1;
                    while(repeat > 0)
                    {
                        status = alljoyn_busobject_signal(testObj, NULL, sessionId, testsignal_member, outArg , numArgs, 5000, 0, NULL);
                        if (status != ER_OK) {
                            printf("Failed to call testsignal_member.\n");
                            return;
                        }
                        repeat--;
                    }

                    // if(!strcmp(cmd, "set_binary"))
                    // {
                    //     outArg = alljoyn_msgarg_array_create(5);
                    //     numArgs = 5;
                    //     if(strlen(name) )
                    //         alljoyn_msgarg_array_set(outArg, &numArgs, "sssss", type, id, new_name, value, name);
                    //     else 
                    //         alljoyn_msgarg_array_set(outArg, &numArgs, "sssss", type, id, new_name, value, NULL);
                    // }

                    alljoyn_msgarg_destroy(outArg);

                    // printf("sessionId = %u\n", sessionId);
                    // alljoyn_proxybusobject proxyObject =alljoyn_proxybusobject_create(g_bus, busName,
                    //                              path, sessionId);

                    // status = alljoyn_proxybusobject_introspectremoteobject(proxyObject);
                    // if (status != ER_OK) {
                    //     printf("Failed to introspect remote object.\n");
                    // }

                    // alljoyn_msgarg arg = alljoyn_msgarg_create_and_set("s", "all");
                    // alljoyn_message replyMsg = alljoyn_message_create(g_bus);
                    // status= alljoyn_proxybusobject_methodcall(proxyObject,
                    //                               "com.verik.bus.VENUS_BOARD",
                    //                               "list_devices", arg,
                    //                               1, replyMsg,
                    //                               25000, 0);

                    // if (status != ER_OK) {
                    //     printf("Failed to call Echo method.\n");
                    //     return;
                    // }

                    // char* echoReply;
                    // alljoyn_msgarg reply_msgarg =
                    //     alljoyn_message_getarg(replyMsg, 0);
                    // status = alljoyn_msgarg_get(reply_msgarg, "s", &echoReply);
                    // if (status != ER_OK) {
                    //     printf("Failed to read Echo method reply.\n");
                    // }
                    // printf("getbinary method reply: %s\n", echoReply);
                    // alljoyn_message_destroy(replyMsg);
                    // alljoyn_msgarg_destroy(arg);
                    alljoyn_proxybusobject_destroy(proxyObject);

                    // s_interrupt = QCC_TRUE;
                }
                


                
               

                
            //}

            //alljoyn_msgarg_destroy(aArg);
            //alljoyn_aboutobjectdescription_destroy(aod2);
            //alljoyn_msgarg_destroy(objArg);
            //alljoyn_aboutproxy_destroy(aboutProxy);
        }
        alljoyn_sessionopts_destroy(sessionOpts);
    } else {
        printf("BusAttachment is NULL\n");
    }
failed:
    alljoyn_aboutdata_destroy(aboutData);
    alljoyn_aboutobjectdescription_destroy(aod);
    alljoyn_aboutobjectdescription_destroy(objectDescription);

}

static my_about_listener* create_my_alljoyn_aboutlistener()
{
    my_about_listener* result =
        (my_about_listener*) malloc(sizeof(my_about_listener));
    alljoyn_aboutlistener_callback* callback =
        (alljoyn_aboutlistener_callback*)
        malloc(sizeof(alljoyn_aboutlistener_callback));
    callback->about_listener_announced = announced_cb;
    result->aboutlistener = alljoyn_aboutlistener_create(callback, result);
    result->sessionlistener = create_my_alljoyn_sessionlistener();
    return result;
}

static void destroy_my_alljoyn_aboutlistener(my_about_listener* listener)
{
    if (listener) {
        if (listener->aboutlistener) {
            alljoyn_aboutlistener_destroy(listener->aboutlistener);
            listener->aboutlistener = NULL;
        }
        if (listener->sessionlistener) {
            alljoyn_sessionlistener_destroy(listener->sessionlistener);
            listener->sessionlistener = NULL;
        }
        free(listener);
        listener = NULL;
    }
}

int CDECL_CALL main(int argc, char** argv)
{
    int rec, optnum, i;
    int numopt = sizeof(opt_list)/sizeof(opts);


    if(argc == 1)
    {
        help();
        return 1;
    }
    rec = commands_check(argv[1]);

    if(!rec)
    {
        printf("%s : Wrong command\n", argv[1]);
        return 1;
    }
    strncpy(cmd, argv[1], sizeof(cmd)-1);
    if (!strcmp(argv[1],"help")) 
    {
        help();
        return 1;
    }

    for(i=2; i < argc; i++)
    {
        optnum = check_opt(argv[i], opt_list, numopt);
        switch(optnum)
        {
            case d:
            {
                if(i+1 < argc)
                {
                    if(check_opt(argv[i+1], opt_list, numopt) != -1) 
                    {
                        printf("need value for argument -d\n");
                        return 1;
                    }
                    else
                    {
                        strncpy(id, argv[i+1], sizeof(id)-1);
                        i++;
                    }    
                }
                else
                {
                    printf("need value for argument -d\n");
                    return 1;
                }
                break;
            }
            case t:
            {
                if(i+1 < argc)
                {
                    if(check_opt(argv[i+1], opt_list, numopt) != -1) 
                    {
                        printf("need value for argument -t\n");
                        return 1;
                    }
                    else
                    {
                        strncpy(type, argv[i+1], sizeof(type)-1);
                        i++;
                    }    
                }
                else
                {
                    printf("need value for argument -t\n");
                    return 1;
                }
                break;
            }
            case v:
            {
                if(i+1 < argc)
                {
                    if(check_opt(argv[i+1], opt_list, numopt) != -1) 
                    {
                        printf("need value for argument -v\n");
                        return 1;
                    }
                    else
                    {
                        strncpy(value, argv[i+1], sizeof(value)-1);
                        i++;
                    }    
                }
                else
                {
                    printf("need value for argument -v\n");
                    return 1;
                }
                break;
            }

            case n:
            {
                if(i+1 < argc)
                {
                    if(check_opt(argv[i+1], opt_list, numopt) != -1) 
                    {
                        printf("need value for argument -n\n");
                        return 1;
                    }
                    else
                    {
                        strncpy(name, argv[i+1], sizeof(value)-1);
                        i++;
                    }    
                }
                else
                {
                    printf("need value for argument -n\n");
                    return 1;
                }
                break;
            }

            case c:
            {
                if(i+1 < argc)
                {
                    if(check_opt(argv[i+1], opt_list, numopt) != -1) 
                    {
                        printf("need value for argument -c\n");
                        return 1;
                    }
                    else
                    {
                        strncpy(new_name, argv[i+1], sizeof(value)-1);
                        i++;
                    }    
                }
                else
                {
                    printf("need value for argument -c\n");
                    return 1;
                }
                break;
            }

            case a:
            {
                if(i+1 < argc)
                {
                    if(check_opt(argv[i+1], opt_list, numopt) != -1) 
                    {
                        printf("need value for argument -a\n");
                        return 1;
                    }
                    else
                    {
                        strncpy(action, argv[i+1], sizeof(value)-1);
                        i++;
                    }    
                }
                else
                {
                    printf("need value for argument -a\n");
                    return 1;
                }
                break;
            }

            case s:
            {
                if(i+1 < argc)
                {
                    if(check_opt(argv[i+1], opt_list, numopt) != -1) 
                    {
                        printf("need value for argument -s\n");
                        return 1;
                    }
                    else
                    {
                        strncpy(data2, argv[i+1], sizeof(value)-1);
                        i++;
                    }    
                }
                else
                {
                    printf("need value for argument -s\n");
                    return 1;
                }
                break;
            }

            case y:
            {
                if(i+1 < argc)
                {
                    if(check_opt(argv[i+1], opt_list, numopt) != -1) 
                    {
                        printf("need value for argument -y\n");
                        return 1;
                    }
                    else
                    {
                        strncpy(source_endpoint, argv[i+1], sizeof(value)-1);
                        i++;
                    }    
                }
                else
                {
                    printf("need value for argument -y\n");
                    return 1;
                }
                break;
            }

            case w:
            {
                if(i+1 < argc)
                {
                    if(check_opt(argv[i+1], opt_list, numopt) != -1) 
                    {
                        printf("need value for argument -w\n");
                        return 1;
                    }
                    else
                    {
                        strncpy(destination_endpoint, argv[i+1], sizeof(value)-1);
                        i++;
                    }    
                }
                else
                {
                    printf("need value for argument -w\n");
                    return 1;
                }
                break;
            }

            case z:
            {
                if(i+1 < argc)
                {
                    if(check_opt(argv[i+1], opt_list, numopt) != -1) 
                    {
                        printf("need value for argument -z\n");
                        return 1;
                    }
                    else
                    {
                        strncpy(is_CRC, argv[i+1], sizeof(value)-1);
                        i++;
                    }    
                }
                else
                {
                    printf("need value for argument -z\n");
                    return 1;
                }
                break;
            }

            default:
            {
                if(argv[i][0] == '-') 
                {
                    printf("not support argument %s\n", argv[i]);
                    return 1;
                }
            }
        }
    }
    printf("cmd = %s\n", cmd);
    printf("type = %s\n", type); //-t
    printf("id = %s\n", id); //-d
    printf("value = %s\n", value); //-v
    printf("name = %s\n", name); //-n
    printf("new_name = %s\n",  new_name); //-c
    printf("action = %s\n", action); //-a
    printf("data2 = %s\n", data2); //-s

    printf("source_endpoint = %s\n",  source_endpoint); //-y
    printf("destination_endpoint = %s\n", destination_endpoint); //-w
    printf("is_CRC = %s\n", is_CRC); //-z

    /* Install SIGINT handler so Ctrl + C deallocates memory properly */
    signal(SIGINT, sig_int_handler);

    QStatus status = alljoyn_init();
    if (ER_OK != status) {
        printf("alljoyn_init failed (%s)\n", QCC_StatusText(status));
        return 1;
    }
    status = alljoyn_routerinit();
    if (ER_OK != status) {
        printf("alljoyn_routerinit failed (%s)\n", QCC_StatusText(status));
        alljoyn_shutdown();
        return 1;
    }

    g_bus = alljoyn_busattachment_create("AboutServiceTest", QCC_TRUE);

    status = alljoyn_busattachment_start(g_bus);
    if (ER_OK == status) {
        printf("BusAttachment started.\n");
    } else {
        printf("FAILED to start BusAttachment (%s)\n",
               QCC_StatusText(status));
        return 1;
    }

    if (ER_OK == status) {
        alljoyn_authlistener_callbacks callbacks = {
            request_credentials,
            authlistener_verifycredentials,
            authlistener_securityviolation,
            authentication_complete
        };
        g_authListener = alljoyn_authlistener_create(&callbacks, NULL);

        /*
         * alljoyn_busattachment_enablepeersecurity function is called by
         * applications that want to use authentication and encryption. This
         * function call must be made after alljoyn_busattachment_start and
         * before calling alljoyn_busattachment_connect.
         *
         * In most situations a per-application keystore file is generated.
         * However, this code specifies the location of the keystore file and
         * the isShared parameter is being set to QCC_TRUE. The resulting
         * keystore file can be used by multiple applications.
         */
        status = alljoyn_busattachment_enablepeersecurity(g_bus, "ALLJOYN_SRP_KEYX ALLJOYN_ECDHE_PSK", g_authListener,
                                                          "/.alljoyn_keystore/central.ks", QCC_TRUE);
        if (ER_OK != status) {
            printf("alljoyn_busattachment_enablepeersecurity failed (%s)\n", QCC_StatusText(status));
        } else {
            printf("alljoyn_busattachment_enablepeersecurity Successful\n");
        }
    }

    status = alljoyn_busattachment_connect(g_bus, NULL);
    if (ER_OK == status) {
        printf("BusAttachment connect succeeded. BusName %s\n",
               alljoyn_busattachment_getuniquename(g_bus));
    } else {
        printf("FAILED to connect to router node (%s)\n",
               QCC_StatusText(status));
        return 1;
    }

    my_about_listener* listener = create_my_alljoyn_aboutlistener();
    alljoyn_busattachment_registeraboutlistener(g_bus, listener->aboutlistener);


    const char* interfaces[] = { INTERFACE_NAME };
    alljoyn_busattachment_whoimplements_interfaces(g_bus, interfaces,
                                                   sizeof(interfaces) / sizeof(interfaces[0]));
    

    if (ER_OK == status) {
        printf("WhoImplements called.\n");
    } else {
        printf("WhoImplements call FAILED with status %s\n", QCC_StatusText(status));
        return 1;
    }

    /* Perform the service asynchronously until the user signals for an exit */
    if (ER_OK == status) {
        while (s_interrupt == QCC_FALSE) {
#ifdef _WIN32
            Sleep(100);
#else
            usleep(100 * 1000);
#endif
        }
    }
    printf("end\n");
    destroy_my_alljoyn_aboutlistener(listener);

    alljoyn_busattachment_stop(g_bus);
    alljoyn_busattachment_join(g_bus);

    alljoyn_busattachment_destroy(g_bus);


    alljoyn_routershutdown();

    alljoyn_shutdown();
    return 0;
}
