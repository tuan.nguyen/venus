#!/bin/sh

# This is supposed to be run on openwrt

# Written by Nguyen Huu Tuong Vinh, 2014
# Based on http://wiki.openwrt.org/doc/recipes/guest-wlan

# Disable lan interface
uci set network.lan.disabled=1

# Configure Venus AP Wi-Fi
uci delete wireless.@wifi-iface[0]
uci delete wireless.venus_ap
uci set wireless.venus_ap=wifi-iface
uci set wireless.venus_ap.device=radio0
uci set wireless.venus_ap.network=ap
uci set wireless.venus_ap.mode=ap
uci set wireless.venus_ap.ssid=VENUS_AP
uci set wireless.venus_ap.encryption=none

# Enable wifi interface
uci set wireless.radio0.disabled=0
uci delete wireless.radio0.htmode

# Configure AP network
uci delete network.ap
uci set network.ap=interface
uci set network.ap.proto=static
uci set network.ap.ipaddr=10.0.0.1
uci set network.ap.netmask=255.255.255.0

 
# Configure DHCP for AP network
uci delete dhcp.ap
uci set dhcp.ap=dhcp
uci set dhcp.ap.interface=ap
uci set dhcp.ap.start=50
uci set dhcp.ap.limit=200
uci set dhcp.ap.leasetime=1h

uci commit
