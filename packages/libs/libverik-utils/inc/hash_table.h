#ifdef __cplusplus
extern "C"{
#endif 

#ifndef _HASH_TABLE_H_
#define _HASH_TABLE_H_

#define _GNU_SOURCE

#include <search.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


typedef struct table_t 
{
    struct hsearch_data htab;
    size_t size;
} table_t;
#define TABLE_T_INITIALIZER                                                  \
(table_t)                                                                    \
{                                                                            \
    .htab = (struct hsearch_data){ 0 }, .size = 0                            \
}

table_t* table_create(size_t size);
void table_destroy(table_t* table);
int table_add(table_t* table, char* key, void* data);
void* table_get(table_t* table, char* key);




#endif
#ifdef __cplusplus
} /* extern "C" */
#endif
