#ifdef __cplusplus
extern "C"{
#endif 

#ifndef _TIMER_H_
#define _TIMER_H_

#include <stdio.h>
#include <sys/time.h>
#include <signal.h>
#include <time.h>
#include <stdlib.h>

#define TIMER_FOREVER               0
#define TIMER_ONETIME               1

#define TIMER_CANCEL_FREE_DATA      0
#define TIMER_CANCEL_RETAIN_DATA    1

#define TIMER_NO_ERROR              0
#define TIMER_ERROR_DUPLICATE       1
#define TIMER_ERROR_ALLOC           2
#define TIMER_ERROR_SETUP_SIGNAL    3
#define TIMER_ERROR_NO_TIMER        4
#define TIMER_ERROR_CREATE_TIMER    5
#define TIMER_ERROR_SET_TIME        6



    

int     timerInit(void);
int     timerDelete(void);
int     timerStart(timer_t *timerID, void(*handler)(void *), void *data, int expireMS, int attribute);
int     timerCancel(timer_t *timerID);
int     timerCancelOptional(timer_t *timerID, int attribute);


#endif
#ifdef __cplusplus
} /* extern "C" */
#endif
