#ifndef _TIMING_H_ 
#define _TIMING_H_

#include <stdio.h>
#include <time.h>
#include <unistd.h>

#define VR_(_str) VR_##_str

typedef struct timespec timing_t;

typedef struct _sTiming
{
	timing_t startTime;
    timing_t stopTime;
	timing_t elapsedTime;
} sTiming;

int timingGetClockSystem(sTiming *timer);
int timingGetClockSystemStop(sTiming *timer);

double timingGetElapsedMSec(sTiming *timer);
void VR_(usleep)(useconds_t usec);

#endif