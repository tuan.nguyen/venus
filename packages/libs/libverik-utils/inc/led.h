#ifdef __cplusplus
extern "C"{
#endif 

#ifndef _LED_H_
#define _LED_H_
	
#include "VR_define.h"

#define ST_CURRENT_STATE "currentState"
#define ST_IN_QUEUE      "inQueue"

void VR_(led_show)(char *led_shm);
void VR_(leds_turn_back_state)(void);
void VR_(set_led_state)(char *led_shm, char *new_led_state);
int led_shm_init(char **shm, int *shmid, char *key_path_name);

#endif
#ifdef __cplusplus
} /* extern "C" */
#endif