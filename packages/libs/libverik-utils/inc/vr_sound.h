#ifdef __cplusplus
extern "C"{
#endif 

#ifndef _VR_SOUND_H_
#define _VR_SOUND_H_

#include <libubus.h>
#include "VR_define.h"
#include "VR_list.h"

#define ST_REPEAT           "repeat"
#define ST_SOUND            "sound"
#define ST_LED_AND_SOUND    "ledAndSound"
#define ST_DELAY_TIME       "delayTime"
#define ST_FILE             "file"
#define ST_SYSTEM           "system"
#define ST_DISABLE_TIME     "disableTime"

#define MAX_VOLUME_VALUE                31 //possible value 39
#define SYSTEM_VOLUME_VALUE             22 //possible value 39
#define ST_VOLUME                       "volume"

#define PLAY_SOUND_CMD                  1
#define STOP_SOUND_CMD                  0
#define TEMPORARY_DISABLE_SOUND_CMD     2
#define DISABLE_ONE_TIME_SOUND_CMD      3
#define DISABLE_ALL_SOUND_CMD           4

#define GET_SOUND_ASYNCHONOUS 1
#define GET_SOUND_SYNCHONOUS 0

#define ST_DEFAULT_SOUND_NAME "device"

typedef struct _command_alarm
{
    char *deviceId;
    char *file;
    int repeat;
    int delayTime;

    struct VR_list_head list;
} command_alarm;

typedef struct _play_
{
    char *deviceId;
    char *file;
    uint8_t playTime;//we done have file play too long

    struct VR_list_head list;
} play_t;

typedef struct _thread_alarm
{
    char *deviceId;
    char *file;
    int repeat;
    int delayTime;

    int temporary_disable_time;
    pthread_t pthreadID;
    uint16_t speak_loop_enable;

    struct VR_list_head list;
} thread_alarm;

int VR_(get_media_file)(char *text, char *save_path);
void get_sound_temperature_threshold(char *value);
void create_all_sound_files(char *deviceId, char *deviceType, char *deviceName, int mode);

int create_link(int enable, char *alarm_type, char *deviceId, char *deviceType,
                char *defaultName, char *path, size_t inputLengh, int *delayTime, json_object *data);

void VR_(alarm)(struct ubus_context *ctx, int mode, char *alarm_type,
                char *deviceId, char *deviceType, char *defaultName, json_object *data);

void VR_(cancel_alarm)(struct ubus_context *ctx, char *deviceId);
void VR_(temp_disable_alarm)(struct ubus_context *ctx, char *deviceId);
void VR_(cancel_all_alarms)(struct ubus_context *ctx);

void VR_(inform_found_device)(void *data);
void VR_(inform_adding_device)(void *data);
void VR_(inform_device_not_support)(void *data);
void VR_(inform_retry_adding)(void *data);
void VR_(introduce_system)(void *data);
void VR_(inform_receive_config)(void *data);
void VR_(inform_wifi_wrong_pass)(void *data);
void VR_(inform_config_failed)(void *data);
void VR_(inform_system_ready)(void *data);
void VR_(inform_cloud_disconnected)(void *data);
void VR_(inform_cloud_connected)(void *data);
void VR_(inform_router_lost_connection)(void *data);
void VR_(inform_became_access_point)(void *data);
void VR_(inform_setup_mode_ready)(void *data);
void VR_(inform_entry_setup_mode)(void *data);
void VR_(inform_exit_setup_mode)(void *data);
void VR_(inform_reseting)(void *data);
void VR_(inform_wifi_connecting)(void *data);
void VR_(inform_register_failed)(void *data);
void VR_(inform_register_connect_error)(void *data);
void VR_(inform_unclaimed)(void *data);
void VR_(inform_firmware_updating)(void *data);
void VR_(inform_firmware_updated)(void *data);

void VR_(inform_device_adding_success)(void *data, char *device_name);

/* APIs FOR PROCESS HANDLE PLAY SOUND */
void init_sound_manage_list(void);
void insert_alarm_command(char *deviceId, int repeat, int delayTime, char *file);
void handle_command_sound(void);
void play_sound(void);
void cancel_alarm_thread(char *deviceId);
void temporary_disable_alarm_thread(char *deviceId, int disableTime);
void disable_all_alarms_thread(void);
int VR_(set_volume)(int value);
int VR_(get_volume)();
void inform_volume_change(void);
void remove_voice_inform_from_id(char *type, char *devId);

#endif

#ifdef __cplusplus
}
#endif