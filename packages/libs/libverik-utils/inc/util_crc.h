#ifndef _UTIL_CRC_H_
#define _UTIL_CRC_H_
#include <stdint.h>
#define POLY 0x1021
uint16_t ccittCrc16(uint16_t crc,uint8_t *pDataAddr,int bDataLen);
void chksumCrc32gentab(uint32_t* crc_tab);


#endif