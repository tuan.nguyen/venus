#ifdef __cplusplus
extern "C"{
#endif 

#ifndef _VERIK_SEC_H_
#define _VERIK_SEC_H_

#include "VR_define.h"

int psk_encrypt(char *userid, char *psk, char *final_b64);
char* base64Encode(unsigned char *str, int len);

#endif

#ifdef __cplusplus
}
#endif