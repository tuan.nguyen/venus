#ifdef __cplusplus
extern "C"{
#endif 

#ifndef _GPIO_INTF_H_
#define _GPIO_INTF_H_

#define IN  0
#define OUT 1
 
#define LOW  0
#define HIGH 1

// Public APIs
int GPIOExport(int pin);
int GPIOUnexport(int pin);
int GPIODirection(int pin, int dir);
int GPIOSet(int pin, int dir);
int GPIORead(int pin);
int GPIOWrite(int pin, int value);

#endif

#ifdef __cplusplus
}
#endif