#ifndef _RULE_H_
#define _RULE_H_

#define RULE_TRIGGER_EVENT_DOMAIN   "https://%s/api/rules/%s/trigger"

void rule_report_timeline(char *ruleId);
int trigger_rule_via_shell_script(char *ruleId, char *actions);

#endif