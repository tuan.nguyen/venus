#ifdef __cplusplus
extern "C"{
#endif 

#ifndef _DATABASE_H_
#define _DATABASE_H_
#include <sqlite3.h>

#define _VR_str(str) #str
#define _VR_CB_(name) _VR_str(name##_handler), name##_db, name##_cb
#define _VR_(name) _VR_str(name##_handler), name##_db

#define SEARCH_DATA_INIT_VAR(_NAME) search_data _NAME; init_seach_data(&_NAME);
#define FREE_SEARCH_DATA_VAR(_NAME) \
        do { \
            if(_NAME.value){ \
                free(_NAME.value);\
            }\
        }while(0)

#define DISABLE_DATABASE_LOG() remove_database_log();
#define ENABLE_DATABASE_LOG() enable_database_log();

typedef struct {
    size_t len;
    char *value;
} search_data;

typedef struct {
    const char *service_name;
    int max_retry;  /* Max retry times. */
    int sleep_ms;   /* Time to sleep before retry again. */
} busy_handler_attr;

typedef struct {
    void *object;
    void *db;
} status_data;

typedef int (*db_callback)(void *, int,  char **, char **);

int open_database(char *Database_Location, sqlite3 **data);
void close_database(sqlite3 **db);
int database_actions(const char* service_name, sqlite3 *db, const char* format, ...);
void searching_database(const char* service_name, sqlite3 *db, db_callback callback, void* result, const char* format, ...);
void set_register_database(char* service_name, sqlite3 *db, db_callback callback, 
                        const char *ID, const char* feature, const char* value,
                        const char* mode, int rotate_num);
void remove_register_database(char* service_name, sqlite3 *db, db_callback callback, 
                            const char *ID, const char* feature, const char* value);

// //support association
void set_ass_node_list_database(char* service_name, sqlite3 *db, db_callback callback, 
                                char *ID, char *groupId, char* nodeList, 
                                const char* mode, int rotate_num);

void remove_ass_node_list_database(char* service_name, sqlite3 *db, db_callback callback,
                                char *ID, char* groupId, char* nodeList);

void remove_all_ass_node_database(char* service_name, sqlite3 *db, 
                                char* deviceId, char *groupId);

////////////////////// meter //////////////////
void set_meter_value_database(char* service_name, sqlite3 *db, db_callback callback, 
                                char *deviceId, const char *meterType, char* meterValue, 
                                const char *meterUnit);
/*need free value after using*/
char *get_meter_value_database(char* service_name, sqlite3 *db, db_callback callback, 
                                char *deviceId, const char *meterType, const char *meterUnit);

void reset_all_meter_value_database(char* service_name, sqlite3 *db, char *deviceId);

/////////////// sensor multilevel //////////////////////
void set_sensor_value_database(char* service_name, sqlite3 *db, db_callback callback, 
                                char *deviceId, const char *sensorType, char* sensorValue, 
                                const char *sensorUnit);
/////////////// thermostat setpoint //////////////////////
void set_thermostat_setpoint_value_database(char* service_name, sqlite3 *db, db_callback callback, 
                                char *deviceId, const char *thermostatMode, char *value, 
                                const char *unit);

/////////////// usercode ////////////////////////////////
void set_door_user_code_database(char* service_name, sqlite3 *db, db_callback callback,
                                 char* deviceId, char* userId, char* name);

void remove_door_user_code_database(char* service_name, sqlite3 *db, 
                               char* deviceId, char* codeId);

void remove_all_user_code_database(char* service_name, sqlite3 *db, char* deviceId);

/////////////// scenes ////////////////////////////////
void update_sence_activate_database(char* service_name, sqlite3 *db,
                                char* deviceId, char* buttonId, unsigned int timeActive);

void update_senceId_database(char* service_name, sqlite3 *db,
                            char* deviceId, char* buttonId, char *sceneId);

void set_scene_button_database(char* service_name, sqlite3 *db, db_callback callback,
                             char* deviceId, char* groupId, char* buttonId, char *sceneId);
////////////////////////////
void set_property_database(char* service_name, sqlite3 *db, db_callback callback, 
                            const char *deviceId, const char *deviceType, char *propertyId,
                            const char *key, char *value);

void saving_thermostat_floatformat_database(char* service_name, sqlite3 *db, db_callback callback, 
                                    const char *ID, const char* unitName, char* unitFormat);

void set_alarm_value_database(char* service_name, sqlite3 *db, db_callback callback, 
                                char *deviceId, const char *groupAlarm, const char* alarmTypeFinal);

char *get_alarm_value_database(char* service_name, sqlite3 *db, db_callback callback, 
                                char *deviceId, const char *groupAlarm);

void remove_alarm_database(char* service_name, sqlite3 *db,
                                    char* deviceId, const char *groupAlarm);

int check_single_quote(char *input, char **output);
void init_seach_data(search_data *data);
int get_last_data_cb(void *data, int argc, char **argv, char **azColName);
int upnp_cb(void *data, int argc, char **argv, char **azColName);
int zigbee_cb(void *data, int argc, char **argv, char **azColName);
int zwave_cb(void *data, int argc, char **argv, char **azColName);
int group_get_last_data_cb(void *data, int argc, char **argv, char **azColName);
void remove_database_log(void);
void enable_database_log(void);

void update_resource_id(char* service_name, sqlite3 *db, db_callback callback, char *deviceId, char *cloudId);

#endif
#ifdef __cplusplus
} /* extern "C" */
#endif