/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
#ifndef _EEPROM_H_
#define _EEPROM_H_

#define I2C_NODE    "/dev/i2c-"
#define BUFSIZE         512
#define EEPROM_BUS      0
#define EEPROM_ADDRESS  0x50

int read_eeprom(__uint16_t reg_addr, unsigned char *buf, int size_to_read);

#endif