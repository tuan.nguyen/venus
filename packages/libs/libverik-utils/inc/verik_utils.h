#ifdef __cplusplus
extern "C"{
#endif 

#ifndef _VERIK_UTILS_H_
#define _VERIK_UTILS_H_

#include <json-c/json.h>
#include "VR_define.h"

#define SHM_ADD      0
#define SHM_DELETE   1
#define SHM_SIZE     1024*1024
#define KEY_PATH_NAME "/bin/system_handler"

#define ST_LED_CURRENT                  "ledCurrent"
#define LED_CURRENT_MAX_VALUE           150
#define LED_CURRENT_DEFAULT_VALUE       60

typedef struct firmware_info
{
    const char *board_name;
    int version;
    const char *product_name;
    const char *token_product;
    const char *dev_name;
    const char *token_dev;
}firmware_info_t;

typedef struct _alarm_effect_type
{
	const char *groupAlarm;
	int number_alarm;
	const char *const*alarmTypeFinal;
}alarm_effect_type_t;

typedef struct _group_alarm_type
{
	const char *groupAlarm;
	const char *alarmTypeFinal;
	int alarm_array_active_len;
	alarm_effect_type_t *alarmActive;
}group_alarm_type_t;

group_alarm_type_t get_group_alarm(const char *alarmTypeFinal);
int get_data_firmware(char *name, int version, firmware_info_t *output);
void VR_(install_rule)(char* rule_name, char* conditions, char* actions);
int VR_(generate_32bit)(void);
void VR_(generate_128bit) (char *result);
char* VR_(read_option)(char *config_path, const char* option);
int VR_(write_option)(char *config_path, const char* format, ...);
int uci_do_add(const char* package, const char* section);
void VR_(execute_system)(const char* format, ...);
int VR_(execute_cmd_with_lock)(const char *filename, const char *argv[],const char *envp[]);
int VR_(execute_cmd_without_lock)(const char *filename, const char *argv[],const char *envp[]);
void VR_(deny_with_timer)(char *userId, time_t expiration);
void VR_(remove_deny_timer)(char *userId);
json_object* VR_(create_json_object)(char *string);

unsigned int VR_(str_to_time)(char *humantime, const char* format, int *milisecond);

// int VR_(is_wireless_AP_mode)(void);
int VR_(is_wireless_up)(char *mode);

int sendMsgtoRemoteService(char *msg);
int sendMsgtoLocalService(char *method, char *msg);

void VR_(run_command)(char *command, char *result, int result_length);
int VR_(httpUploadFile)(char *file_path);
void string_to_hash(const unsigned char *input, unsigned int length, char *output, size_t outLength);

int shm_init(char **shm, int *shmid);
void shm_write_data(char *shm, char *data);
void shm_read_data(char *shm, char *data, size_t data_length);
void shm_update_data(char *shm, char *cloudid, char *deviceid, char *devicetype, int mode);

char *VR_(get_localid_from_cloudid)(char* shm, char *cloud_id, char *type);
char *VR_(get_cloudid_from_localid)(char* shm, char *local_id);

void VR_(convert_upper_to_lower)(char *input, char *output);

// assert(versionCmp("1.2.3" , "1.2.3" ) == 0);
//     assert(versionCmp("1.2.3" , "1.2.4" )  < 0);
//     assert(versionCmp("1.2.4" , "1.2.3" )  > 0);
//     assert(versionCmp("10.2.4", "9.2.3" )  > 0);
//     assert(versionCmp("9.2.4",  "10.2.3")  < 0);
//     /* Trailing 0 ignored. */
//     assert(versionCmp("01", "1") == 0);
//     /* Any single space delimiter is OK. */
// assert(versionCmp("1a2", "1b2") == 0);
int VR_(version_cmp)( char *pc1, char *pc2);
int VR_(get_file_infor)(char *path);

int VR_(get_rule_infor)(char *shm, char *ruleDocument, char **state, char **ruleId, 
                        char **ruleName, char **ruleType, char **condition, char **actions,
                        int64_t *timeStamp, char **enabled);

void get_feature_device_support(json_object *capabilityObj, char *deviceType, int *association,
                         int *meter, int *sensorMultilevel, int *userCode, int *thermostatSetpoint);

int get_processId_from_name(char *pro_name);
int get_number_process_from_name(char *process_name);

void get_hub_name(char *result, size_t result_length);
void get_hub_id(char *result, size_t result_length);
void get_hub_ssid(char *result, size_t result_length);
void get_hub_bssid(char *result, size_t result_length);
void get_hub_state(char **state, char **message);

uint64_t get_timestamp_in_microseconds(void);

unsigned int get_system_up_time(void);
void inform_pubsub_post_failed(char *deviceId, char *deviceType, char *data);
void get_log_location(char *location, size_t length);
size_t get_file_length(char *file);
void clean_file_content(char *file);

int get_door_status(const char *state);
int get_door_lock_state(const char *state);
int ping_command(char *host, uint8_t timeout);
int router_connect_status(char *interface);
int get_cpu_system(void);

void uuid_make(char *uuid, size_t length);
const char* get_ubus_service(const char *sevice_name);

unsigned int get_led_current(void);
void set_led_current(unsigned int value);

#endif

#ifdef __cplusplus
}
#endif