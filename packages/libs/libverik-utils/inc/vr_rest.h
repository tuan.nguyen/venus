#ifdef __cplusplus
extern "C"{
#endif 

#ifndef _VR_REST_H_
#define _VR_REST_H_

#include <stdint.h>
#include "VR_define.h"
#include "VR_list.h"

#define HUB_META_DATA "https://%s/api/devices/%s/dmeta"

#define HTTP_POST_REQUEST 		0
#define HTTP_GET_REQUEST  		1
#define HTTP_PUT_REQUEST  		2
#define HTTP_PATCH_REQUEST  	3
#define HTTP_DELETE_REQUEST 	4

#define RESOURCES_TIMEOUT   7L /*7s*/
#define DEFAULT_TIMEOUT     120L /*2mins*/
// #define DOWNLOAD_TIMEOUT 600L /*5mins*/

typedef int (*download_progess_cb)(void *,double , double , double , double );
typedef int (*request_data_cb)(char *, void *);

typedef struct curl_request_queue_
{
	uint8_t request_type;
    char *url;
    char *data;
    char *headers;

    request_data_cb data_cb;
    struct VR_list_head list;
}curl_request_queue_t;

int VR_(get_request)(char *address, char *headerIn, char *data,
					 char **headerOut, char **response, long timeout);
int VR_(post_request)(char *address, char *headerIn, char *data,
					 char **header, char **response, long timeout);
int VR_(put_request)(char *address, char *headerIn, char *data,
					 char **header, char **response, long timeout);
int VR_(patch_request)(char *address, char *headerIn, char *data,
					 char **header, char **response, long timeout);
int VR_(delete_request)(char *address, char *headerIn, char **header,
					 char **response, long timeout);

int VR_(download_file)(char *url, const char *filename, const char *user_password,
                       download_progess_cb download_cb, void *data);

int VR_(post_resources)(char *data, char *id_return, size_t length, long timeout);
int VR_(delete_resources)(char *resource_id, long timeout);

int VR_(check_internet_connection)(char *address);
int VR_(upload_file)(char *address, char *file);

int VR_(update_hub_infor)(char *bssid, char *ssid);

int tls_init(void);
int tls_cleanup(void);

void init_curl_request_list(void);
void adding_curl_request_list(uint8_t request_type, char *url, char *headers, char *data, request_data_cb data_cb);
void request_thread(void);
void stop_request_thread(void);

#endif

#ifdef __cplusplus
}
#endif