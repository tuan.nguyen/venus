#ifdef __cplusplus
extern "C"{
#endif 

#ifndef _VR_DEFINE_H_
#define _VR_DEFINE_H_

#define VR_(_str) VR_##_str
#define SAFE_FREE(_A) \
    do { \
        if (_A) { \
            free(_A); \
            _A=NULL; \
        } \
    } while (0)

#define JSON_RETURN(jobj,key,value) \
    do \
    {  \
        if(value) \
        { \
            json_object_object_add(jobj, key, json_object_new_string(value)); \
            free(value); \
            value = NULL; \
        } \
        else \
        { \
            json_object_object_add(jobj, key, json_object_new_string("")); \
        } \
    } \
    while(0)

#define JSON_ADD_STRING_SAFE(jobj,key,value) \
    do \
    {  \
        if(value && key) \
        { \
            json_object_object_add(jobj, key, json_object_new_string(value)); \
        } \
    } \
    while(0)

#define CHECK_JSON_OBJECT_EXIST(_varName, _jobj, _key, _jumpAddress) \
        json_object *_varName = NULL; \
        if(!json_object_object_get_ex(_jobj, _key, &_varName)) \
        { \
            SLOGE("%s not found\n", _key);\
            goto _jumpAddress;\
        }

#define SAFE_STRCPY(_OUTPUT,_INPUT) \
        do \
        { \
            strlcpy(_OUTPUT, _INPUT, sizeof(_OUTPUT)); \
        } \
        while(0)

#define GET_DATA_FROM_TOKEN_STRING(_TOK,_OUTPUT,_DELIM,_SAVE_TOK) \
        do \
        { \
            if(_TOK) \
            { \
                _OUTPUT = strdup(_TOK); \
                _TOK = strtok_r(NULL, _DELIM, &_SAVE_TOK); \
            } \
        } \
        while(0)

#define TITAN_BOARD "Titan"
#define PHD_BOARD   "PhD"

#define PHD_PATTERN_ID "10"
#define TITAN_PATTERN_ID "11"
#define PHD_SSID_PATTERN "PhD"
#define TITAN_SSID_PATTERN "Titan"

#define CERTIFICATE_PATH "/etc/ssl/certs/"
#define CERTIFICATE_FILE "/etc/ssl/certs/ca-certificates.crt"

#define UCI_RULE_ETAG          "security.@etag[0].rule"
#define UCI_RESOURCE_ETAG          "security.@etag[0].resource"

#define UCI_SYSTEM_TIMEZONE         "system.@system[0].timezone"
#define UCI_FIREWALL_MASQ           "firewall.@zone[-1].masq"

#define UCI_ALARM_ENABLE          "security.@venus-alarm[0].enable"

#define UCI_ALLJOYN_ONBOARDING_STATE        "alljoyn-onboarding-state.@onboarding[0].state"
#define UCI_ALLJOYN_ONBOARDING_ERROR_MSG    "alljoyn-onboarding-state.@onboarding[0].lasterrormsg"

#define UCI_ALLJOYN_ONBOARDING_SSID         "alljoyn-onboarding.@onboarding[0].ssid"
#define UCI_ALLJOYN_ONBOARDING_ENCRYPTION   "alljoyn-onboarding.@onboarding[0].encryption"
#define UCI_ALLJOYN_ONBOARDING_KEY          "alljoyn-onboarding.@onboarding[0].key"

#define UCI_HUB_DEFAULT_NAME                "alljoyn-onboarding.@onboarding[0].apssid"
#define UCI_ALLJOYN_ONBOARDING_APENCRYPTION "alljoyn-onboarding.@onboarding[0].apencryption"
#define UCI_ALLJOYN_ONBOARDING_APKEY        "alljoyn-onboarding.@onboarding[0].apkey"

#define UCI_FIRMWARE_POWER_CYCLE            "firmware.@power-cycle[0].value"
#define UCI_FIRMWARE_POWER_CYCLE_TIME       "firmware.@power-cycle[0].time"
#define UCI_FIRMWARE_CURRENT_VER            "firmware.@firmware-info[0].current"
#define UCI_FIRMWARE_LATEST_VER             "firmware.@firmware-info[0].latest"
#define UCI_FIRMWARE_DOWNLOAD_URL           "firmware.@firmware-info[0].url"
#define UCI_FIRMWARE_CHECKSUM               "firmware.@firmware-info[0].checksum"
#define UCI_FIRMWARE_PRE_VER                "security.@firmware-pre[0].version"

#define DEFAULT_DATABASE_ID                   "phd"
#define DEFAULT_DATABASE_TOKEN                "b9ee5abcfb2f4f838930de5c372cc7a6"
#define UCI_FIRMWARE_DATABASE_ID               "firmware.@supported-database-info[0].id"
#define UCI_FIRMWARE_DATABASE_ID_DEV           "firmware.@supported-database-info[0].idDev"
#define UCI_FIRMWARE_DATABASE_TOKEN            "firmware.@supported-database-info[0].token"
#define UCI_FIRMWARE_DATABASE_TOKEN_DEV        "firmware.@supported-database-info[0].tokenDev"
#define UCI_FIRMWARE_DATABASE_CURRENT_VER      "firmware.@supported-database-info[0].current"
#define UCI_FIRMWARE_DATABASE_LATEST_VER       "firmware.@supported-database-info[0].latest"
#define UCI_FIRMWARE_DATABASE_DOWNLOAD_URL     "firmware.@supported-database-info[0].url"
#define UCI_FIRMWARE_DATABASE_CHECKSUM         "firmware.@supported-database-info[0].checksum"


#define UCI_HUB_NAME                  "security.@access-point[0].ssid"
#define UCI_HUB_ENCRYPTION                  "security.@access-point[0].encryption"
#define UCI_HUB_KEY                  "security.@access-point[0].key"
#define UCI_HUB_CHANNEL                  "security.@access-point[0].channel"

#define UCI_LOCAL_STATE               "security.@local-access[0].state"
#define UCI_LOCAL_ONBOARDING_PSK      "security.@local-access[0].onboarding"
#define UCI_LOCAL_HUB_ID              "security.@local-access[0].deviceid"
#define UCI_LOCAL_PSK                 "security.@local-access[0].psk"
#define UCI_LOCAL_CLAIM_TOKEN         "security.@local-access[0].claimToken"
#define UCI_LOCAL_SETUP_MODE          "security.@local-access[0].setupMode"

#define UCI_CLOUD_ADDRESS                  "security.@remote-access[0].cloud_address"
#define UCI_CLOUD_FIRMWARE_ADDRESS         "security.@remote-access[0].firmware_address"
#define UCI_CLOUD_TOKEN                  "security.@remote-access[0].token"
#define UCI_CLOUD_STATE                  "security.@remote-access[0].state"
#define UCI_CLOUD_HUB_UUID                  "security.@remote-access[0].hubUUID"
#define UCI_CLOUD_USER_UUID                 "security.@remote-access[0].userUUID"
#define UCI_CLOUD_HOME_ID                   "security.@remote-access[0].homeId"
#define UCI_CLOUD_USER_ID                   "security.@remote-access[0].userId"
#define UCI_CLOUD_MQTT_ADDRESS                 "security.@remote-access[0].mqtt_address"

#define UCI_CLOUD_MQTT_USER                 "security.@remote-access[0].username"
#define UCI_CLOUD_MQTT_PASS                 "security.@remote-access[0].password"
#define UCI_CLOUD_UPDATE_SSID                 "security.@remote-access[0].update_ssid"

#define UCI_LOCATION_LATITUDE                  "security.@location[0].latitude"
#define UCI_LOCATION_LONGITUDE                 "security.@location[0].longitude"

#define UCI_WIFI_STA_MODE                "wireless.@wifi-iface[0].mode"
#define UCI_WIFI_STA_DISABLED            "wireless.@wifi-iface[0].disabled"
#define UCI_WIFI_AP_MAC_ADDRESS          "wireless.@wifi-iface[-1].macaddr"
#define UCI_WIFI_AP_MODE_DISABLED        "wireless.@wifi-iface[-1].disabled"

#define UCI_SOUND_VOLUME                 "security.@local-access[0].volume"
#define UCI_LED_CURRENT                 "security.@local-access[0].ledCurrent"

#define DEFAULT_LOG_LOCATION "/system.log"
#define LOG_SIZE_ROTATE      100*1024*1024//MB

#define GET_TEMP_COMMAND        "cat /sys/bus/i2c/devices/0-0049/temp1_input"
#define GET_WHITE_LED_COMMAND   "cat /sys/class/leds/TOP_white_led/brightness"
#define GET_IP_ADDRESS_COMMAND  "ifconfig wlan0 | awk '/inet addr:/{ print substr($2,6)}'"
#define GET_MASK_ADDRESS_COMMAND "ifconfig wlan0 | awk '/Mask:/{ print substr($4,6)}'"   
#define GET_MAC_ADDRESS_COMMAND "cat /sys/class/net/wlan0/address"
#define GET_BOARD_NAME_COMMAND  "fw_printenv -n board_name | sed -e 's/ //g'"
#define GET_BOARD_REV_COMMAND   "fw_printenv -n board_rev | sed -e 's/ //g'"
#define GET_BOARD_VER_COMMAND   "fw_printenv -n board_version | sed -e 's/ //g'"
#define GET_BOARD_SER_COMMAND   "fw_printenv -n serial_number | sed -e 's/ //g'"
#define GET_LOG_LOCATION_COMMAND   "fw_printenv -n log | sed -e 's/ //g'"
#define GET_STA_STATUS_COMMAND   "wpa_cli -i wlan0 status 2>/dev/null | grep wpa_state | cut -d= -f2"
#define GET_CPU_SYSTEM_COMMAND   "top -b -n 1 | grep ^CPU | awk '{print $4}' |  sed -e 's/%//g'"

#define RULE_DATABASE "/etc/database/rule.db"
#define DEVICES_DATABASE "/etc/database/devicelist.db"
#define TEMP_DEVICES_DATABASE "/tmp/%u_devicelist.db"
#define USERS_DATABASE "/etc/database/users.db"

#define WIFI_INFO_DATABASE "/etc/wifiInfor.db" //no need to reuse
#define SUPPORTED_DEVS_DATABASE "/etc/supported_devices.db" //no need to reuse

#define SUPPORTED_DEVS_DATABASE_NAME "supported_devices.db" //no need to reuse

#define SOUND_FILE "/tmp/sound.tar.gz" //no need to reuse
#define SOUND_SERVER_NAME "sound.tar.gz" //no need to reuse

#define ADDING_DEVICE_TIMEOUT 5*60 //second

#define SIZE_16B                16
#define SIZE_32B                32
#define SIZE_64B                64
#define SIZE_128B               128
#define SIZE_256B               256
#define SIZE_512B               512
#define SIZE_1024B              1024

typedef struct _scene_infor_
{
    char deviceId[SIZE_32B];
    char buttonId[SIZE_32B];
}scene_infor;

// #define ST_HW_REV_A             "Rev.00.0A"
// #define ST_HW_REV_E             "Rev.00.0E"
#define HW_REV_E                48484869
// #define ST_HW_REV_F             "Rev.00.0F"

//FILE PATH COMMAND
#define ST_EXCUTE_LED             "/etc/led_control.sh"

#define ST_ALLJOYN                "alljoyn"
#define ST_ZWAVE                  "zwave"
#define ST_ZIGBEE                 "zigbee"
#define ST_PUBSUB                 "pubsub"
#define ST_UPNP                   "upnp"
#define ST_VENUS_SERVICE          "venus"

#define ST_ALLJOYN_HANDLER        "alljoyn_handler"
#define ST_ZWAVE_HANDLER          "zwave_handler"
#define ST_ZIGBEE_HANDLER         "zigbee_handler"
#define ST_PUBSUB_HANDLER         "pubsub_handler"
#define ST_UPNP_HANDLER           "upnp_handler"
#define ST_VENUS_HANDLER          "venus_handler"

#define ST_NOTIFY               "notify"
#define ST_NOTIFY_TYPE          "notifyType"

#define ST_SET_TIME             "setTime"
#define ST_SET_TIME_R           "setTimeR"

#define ST_GET_TIME             "getTime"
#define ST_GET_TIME_R           "getTimeR"

#define ST_CHANGE_STATE         "changeState"

#define ST_GET_STATE            "getState"
#define ST_GET_STATE_R          "getStateR"

#define ST_AUTO_CONF            "autoConf"
#define ST_AUTO_CONF_R          "autoConfR"

#define ST_ADD_DEVICES          "addDevices"
#define ST_ADD_DEVICES_R        "addDevicesR"

#define ST_LIST_DEVICES         "listDevices"
#define ST_LIST_DEVICES_R       "listDevicesR"

#define ST_REMOVE_DEVICE        "removeDevice"
#define ST_REMOVE_DEVICE_R      "removeDeviceR"

#define ST_RESET                "reset"
#define ST_RESET_R              "resetR"

#define ST_OPEN_CLOSE_NETWORK   "openCloseNetwork" 
#define ST_OPEN_CLOSE_NETWORK_R   "openCloseNetworkR" 

#define ST_OPEN_NETWORK         "openNetwork" 
#define ST_OPEN_NETWORK_R       "openNetworkR"

#define ST_CLOSE_NETWORK         "closeNetwork" 
#define ST_CLOSE_NETWORK_R       "closeNetworkR"

#define ST_GET_SUB_DEVS          "getSubDevs"
#define ST_GET_SUB_DEVS_R        "getSubDevsR"

#define ST_GET_BINARY           "getBinary"
#define ST_GET_BINARY_R         "getBinaryR"

#define ST_SET_BINARY           "setBinary" 
#define ST_SET_BINARY_R         "setBinaryR"

#define ST_CHANGE_NAME          "changeName"
#define ST_CHANGE_NAME_R        "changeNameR"

#define ST_ALEXA                "alexa"
#define ST_ALEXA_R              "alexaR"

#define ST_IDENTIFY             "identify"
#define ST_IDENTIFY_R           "identifyR"

#define ST_FIRMWARE_ACTIONS     "firmwareActions"
#define ST_FIRMWARE_ACTIONS_R   "firmwareActionsR"

#define ST_SET_RULE             "setRule"
#define ST_SET_RULE_R           "setRuleR"

#define ST_GET_RULE             "getRule"
#define ST_GET_RULE_R           "getRuleR"

#define ST_RULE_ACTIONS             "ruleActions"
#define ST_RULE_ACTIONS_R           "ruleActionsR"

#define ST_READ_SPEC            "readSpec"
#define ST_READ_SPEC_R          "readSpecR"

#define ST_WRITE_SPEC           "writeSpec"
#define ST_WRITE_SPEC_R         "writeSpecR"

#define ST_READ_S_SPEC          "readSSpec"
#define ST_READ_S_SPEC_R        "readSSpecR"

#define ST_WRITE_S_SPEC         "writeSSpec"
#define ST_WRITE_S_SPEC_R       "writeSSpecR"

#define ST_REDISCOVER           "rediscover"
#define ST_REDISCOVER_R         "rediscoverR"

#define ST_CREATE_GROUP         "createGroup"
#define ST_CREATE_GROUP_R       "createGroupR"

#define ST_GROUP_ACTIONS        "groupActions"
#define ST_GROUP_ACTIONS_R      "groupActionsR"

#define ST_GROUP_CONTROL        "groupControl"
#define ST_GROUP_CONTROL_R      "groupControlR"

#define ST_NETWORK              "network"
#define ST_NETWORK_R            "networkR"

#define ST_READ_SPEC_CRC        "read_spec_CRC"
#define ST_READ_SPEC_CRC_R      "read_spec_CRCR"

#define ST_WRITE_SPEC_CRC       "writeSpecCRC"
#define ST_WRITE_SPEC_CRC_R     "writeSpecCRCR"

#define ST_REQUEST_ACCESS       "requestAccess"
#define ST_REQUEST_ACCESS_R     "requestAccessR"

#define ST_REQUEST_ACCESS_2     "requestAccess2"

#define ST_DENY_ACCESS          "denyAccess"
#define ST_DENY_ACCESS_R        "denyAccessR"

#define ST_LIST_USERS           "listUsers"
#define ST_LIST_USERS_R         "listUsersR"

#define ST_SHARE_ACCESS         "shareAccess"
#define ST_SHARE_ACCESS_R       "shareAccessR"

#define ST_OB_REQUEST_ACCESS         "obRequestAccess"
#define ST_OB_REQUEST_ACCESS_R       "obRequestAccessR"

#define ST_PUBLISH              "publish"
#define ST_PUBLISH_R            "publishR"

#define ST_GET_NAME             "getName"
#define ST_GET_NAME_R           "getNameR"

#define ST_RULE_MANUAL           "ruleManual"
#define ST_RULE_MANUAL_R         "ruleManualR"

#define ST_LIST_GROUPS           "listGroups"
#define ST_LIST_GROUPS_R         "listGroupsR"

#define ST_LIST_GROUPS           "listGroups"
#define ST_LIST_GROUPS_R         "listGroupsR"

#define ST_SEND_REPORT           "sendReport"
#define ST_SEND_REPORT_R         "sendReportR"

#define ST_WIFI_SETTINGS         "wifiSettings"
#define ST_WIFI_SETTINGS_R       "wifiSettingsR"

#define ST_GET_STATUS            "getStatus"
#define ST_GET_STATUS_R          "getStatusR"

#define ST_SETUP                 "setup"
#define ST_SETUP_R               "setupR"

#define ST_PSK_ENCRYPT           "pskEncrypt"
#define ST_PSK_CHANGED           "pskChanged"

#define ST_VENUS_SERVICE         "venus"

// alljoyn ubus method def/// using in rule, parser wifi, hue bridge, pubsub
#define ST_AUTO_SCAN_RESULT      "autoScanResult" //using in parser_iwlist_wifi_scan
#define ST_NETWORK_UBUS          "network" 
#define ST_WEMO_SSID             "wemoSsid"
#define ST_SERVICE               "service"
#define ST_SERVICE               "service"
#define ST_SET_GROUP_BINARY      "setGroupBinary"
#define ST_SEND_NOTIFY           "sendNotify" //using in system_handler.init
#define ST_SEND_PUBLISH          "sendPublish"
#define ST_SET_MODE              "setMode"

// pubsub send cloud report
#define ST_CLOUD_ON_OFF          "onoff" //using in parser_iwlist_wifi_scan
#define ST_SHADOW                "shadow" 
#define ST_CLOUD_DIM             "dim"
#define ST_CLOUD_SET_TEMP        "setTemp"
#define ST_CLOUD_ADD_DEVICE      "addDevice"
#define ST_CLOUD_EM_HEAT         "emHeat"
#define ST_CLOUD_SMOKE           "smoke"
#define ST_FALSE                 "false"
#define ST_TRUE                  "true"
#define ST_CLOUD_CO              "co"
#define ST_GARAGE_SENSOR         "garageSensor"
#define ST_CLOUD_CURRENT_TEMP    "currentTemp"
#define ST_CLOUD_RESOURCE        "resource"
#define ST_CLOUD_USER_HOME       "userhome"
#define ST_RESOURCE              "resource"
#define ST_MISSING               "missing"
#define ST_LOW_BATTERY           "lowBattery"
#define ST_OVERHEAT              "overheat"
#define ST_UNDERHEAT             "underheat"
#define ST_TEMPERATURE_THRESH_HOLD "thresholdTemp"
#define ST_MIN_CELSIUS           "minC"
#define ST_MIN_FAHRENHEIT        "minF"
#define ST_MAX_CELSIUS           "maxC"
#define ST_MAX_FAHRENHEIT        "maxF"

#define ST_CLOUD_USER_HOME       "userhome"
#define ST_CLOUD_RESOURCE_GROUP  "resourcegroup"
#define ST_CLOUD_CURRENT_RESOURCE_ID       "currentResourceID"

#define GARAGE_MISSING           101
#define GARAGE_LOW_BATTERY       102

#define SYNC_TRUE                1
#define SYNC_FALSE               0

#if 0
#define ST_CLOUD_LOCAL_ID        "localID"
#define ST_CLOUD_SERIAL_ID       "serialID"
#else
#define ST_CLOUD_LOCAL_ID        "localId"
#define ST_CLOUD_SERIAL_ID       "serialId"
#endif
#define RESOURCE_EXIST_ERROR     400
#define NOT_USE_ETAG             0
#define USE_ETAG                 1

#define ST_DIMMABLE_LIGHT             "DimmableLight"
#define ST_GARAGE_DOOR_CONTROLLER     "GarageDoorController"
#define ST_POWER_CYCLE                "powerCycle"
// zwave ubus method
#define ST_CHECK_DEV_STATE            "checkDevState"
#define ST_UPDATE_NETWORK             "updateNetwork"
#define ST_UPDATE_NETWORK_R           "updateNetworkR"
#define ST_VENUS_ALARM                "venusAlarm"
#define ST_VENUS_ALARM_V2             "venusAlarmV2"

#define ST_VENUS_ALARM_R              "venusAlarmR"
#define ST_SCENE_ACTIVATION_R         "sceneActivationR"
#define ST_SCENE_ACTIONS              "sceneActions"
#define ST_SCENE_ACTIONS_R            "sceneActionsR"
#define ST_PAIR                       "pair"
#define ST_UNPAIR                     "unpair"

#define ST_RESET_STOP_ADD_TIMER       "resetStopAddTimer"
#define ST_FORCE_LAST_STATUS          "forceLastStatus"
#define ST_TIME_ZONE                  "timeZone"

///for firmware actions//
#define ST_GET_LATEST                   "getLatest"
#define ST_GET_CURRENT                  "getCurrent"
#define ST_UPDATE_SUPPORT_DATABASE      "updateSupportDatabase"
#define ST_UPDATE                       "update" 
#define ST_UPDATING                     "updating" 
#define ST_DOWNLOADING                  "downloading"
#define ST_LATEST_VER                   "latestVersion"
#define ST_CURRENT_VER                  "currentVersion"
#define ST_SP_CURRENT_VER               "spCurrentVer"
#define ST_WHITE_LED_MOTION             "whiteLedMotion"
#define ST_ECHO_SUPPORT                 "echoSupport"
#define ST_SSID                         "ssid"
#define ST_ENCRYPTION                   "encryption"
#define ST_AP_MODE                      "apMode"
#define ST_STA_MODE                     "staMode"
#define ST_LATITUDE                     "latitude" 
#define ST_LONGTITUDE                   "longitude" 
#define ST_WHITE_LEDS                   "whiteLeds" 
#define ST_LOCATION                     "location" 
#define ST_BRANCH                       "branch" 
#define ST_BUILD_DATE                   "buildDate" 
#define ST_UPDATE_DATE                  "updateDate" 
#define ST_VER_INFO                     "versionInfo"
#define ST_ENABLE                       "enable"
#define ST_UPDATE_SUCCESS               "UPDATE FIRMWARE SUCCESS"
#define ST_WRONG_URL                    "WRONG URL"
#define ST_UNTAR_FAILED                 "UNTAR FAILED"
#define ST_FIRMWARE_FAILED_DOWNLOAD     "could not resolve host, please check your internet connection"
#define ST_FIRMWARE_WRONG_CHECKSUM      "firmware checksum is wrong, please retry again"
#define ST_FIRMWARE_MISSING_CHECKSUM    "firmware checksum is missing, please retry again"
#define ST_FIRMWARE_MISSING_VERSION     "firmware version is missing, please retry again"

/// for rule ///
#define ST_RULE                         "rule"
#define ST_RULE_ID                      "ruleId"
#define ST_RULE_TYPE                    "ruleType"
#define ST_RULE_NAME                    "ruleName"
#define ST_CONDITIONS                   "conditions"
#define ST_ACTIONS                      "actions"
#define ST_NEW_CONDITIONS               "newConditions"
#define ST_NEW_ACTIONS                  "newActions"
#define ST_NEW_NAME                     "newName"
#define ST_TIME                         "time"
#define ST_MANUAL                       "manual"
#define ST_DEV_TO_DEV                   "d2d"
#define ST_AUTO_OFF                     "autoOff"
#define ST_ACTIVE_TIME                  "activeTime"
#define ST_CUSTOM                       "custom"
#define ST_NO_LOOP                      "noLoop"
#define ST_RULE_LIST                    "ruleList"
#define ST_RULE_ACTION                  "ruleAction"
#define ST_RULE_TYPE                    "ruleType"
#define ST_DELETE                       "delete"
#define ST_ACTIVATE                     "activate"
#define ST_DEACTIVATE                   "deactivate"
#define ST_AUTO_OFF_ACTIVATE            "autoOffActivate"

#define ST_CLOUD_RULE_CONDITION          "condition"
#define ST_CLOUD_RULE_ACTION             "action"
#define ST_CLOUD_RULE_ID                 "id"
#define ST_CLOUD_RULE_STATE              "state"
#define ST_CLOUD_RULE_DOC                "doc"
#define ST_CLOUD_DATA                    "data"
#define ST_CLOUD_TIME_STAMP              "timestamp"
#define ST_CLOUD_ACCEPTED                "accepted"
#define ST_CLOUD_ENABLED                 "enabled"
#define ST_CLOUD_VERB                    "verb"
#define ST_CLOUD_MODEL                   "model"
#define ST_CLOUD_ISCLAIMED               "isClaimed"
#define ST_TIMER                         "timer"
#define ST_LOOP                          "loop"
#define ST_CREATE                        "create"

/// reset ///
#define ST_HARD                         "hard"
#define ST_RESET_TYPE                   "resetType"
#define ST_REBOOT                       "reboot"
#define ST_ADMIN                        "admin"

// for group///
#define ST_GROUP_LIST                   "groupList"
#define ST_USER_ID                      "userId"
#define ST_APP_ID                       "appId"
#define ST_GROUP_ID                     "groupId" 
#define ST_GROUP_NAME                   "groupName"
#define ST_DEVICE_ID                    "deviceId" 
#define ST_DEVICE_TYPE                  "deviceType"
#define ST_DEVICE_ID1                   "deviceId1"
#define ST_DEVICE_TYPE1                 "deviceType1"
#define ST_DEVICE_ID2                   "deviceId2"
#define ST_DEVICE_TYPE2                 "deviceType2"
#define ST_ADD                          "add"
#define ST_COMMAND                      "command"
#if 0
#define ST_RESOURCE_UUIDS               "resourceUUIDs"
#define ST_RESOURCE_UUID                "resourceUUID"
#else
#define ST_RESOURCE_UUIDS               "resourceIds"
#define ST_RESOURCE_UUID                "resourceId"
#endif
#define ST_GROUP_DEV                    "groupDev"
#define ST_MODIFY                       "modify"
#define ST_RESOURCE_GROUP_RESOURCE      "resourcegroupresource"

// for share user//
#define ST_ADMIN_ID             "adminId"
#define ST_SHARE_ID             "shareId"
#define ST_USER_NAME            "username"
#define ST_EXPIRATION           "expiration"
#define ST_SHARE_TIME           "shareTime"
#define ST_ROLE                 "role"
#define ST_DENY                 "deny"
#define ST_MESSAGE              "message"
#define ST_ACCESS_CHANGED       "accessChanged"
#define ST_USER                 "user"
#define ST_USER_LIST            "userList"
#define ST_OB_ENCRYPT           "obEncrypt"

//for wifi_settings//
#define ST_DISCONNECTED         "disconnected"
#define ST_CONNECTED            "connected"
#define ST_CLAIMED              "claimed"
#define ST_STA                  "sta"
#define ST_AP                   "ap"
#define ST_SCAN                 "scan"
#define ST_MODE                 "mode"
#define ST_AUTH                 "auth"
#define ST_SCAN_LIST            "scanList"
#define ST_GET                  "get"
#define ST_KEY                  "key"
#define ST_PASSWORD             "password"
#define ST_CHANNEL              "channel"

/// ubus define ///
#define ST_UBUS_MESSAGE_RETURN_KEY      "msg" 
#define ST_MOTION                       "motion" 
#define ST_DEVICES_LIST                 "devicesList"
#define ST_HUBS_LIST                    "hubsList"
#define ST_COMMAND_INFO                 "commandInfo" 

#define ST_SRC_ENDPOINT              "srcEndpoint"
#define ST_DES_ENDPOINT              "desEndpoint"
#define ST_IS_CRC                    "isCRC"
#define ST_LIST_TYPE                 "listType"

#define ST_ID                   "id"
#define ST_VALUE                "value"
#define ST_TOPIC                "topic"
#define ST_SUBDEVID             "subDevId"
#define ST_CLASS                "class"
#define ST_CMD                  "cmd"
#define ST_DATA0                "data0"
#define ST_DATA1                "data1"
#define ST_DATA2                "data2"
#define ST_DATA3                "data3"
#define ST_DATA4                "data4"
#define ST_DATA5                "data5"
#define ST_RESET_TYPE           "resetType"

#define ST_ALL                  "all"
#define ST_TYPE                 "type"
#define ST_ACTION               "action"
#define ST_VERSION              "version"
#define ST_METHOD               "method"
#define ST_STATUS               "status" 
#define ST_STATE                "state" 

#define ST_REASON               "reason" 
#define ST_FAILED               "failed" 
#define ST_SUCCESSFUL           "successful" 

// for upnp
#define ST_REPLACE                "replace"
#define ST_REPLACE_DEV            "replaceDev" 
#define ST_ACTION_TYPE            "actionType" 
#define ST_ACTION_COMPLETE        "actionComplete"
#define ST_STATUS_CHANGE          "statusChange"
#define ST_CAPABILITY_ID          "capabilityId"
#define ST_FRIENDLY_NAME          "friendlyName"
#define ST_SUB_DEV_LIST           "subDevList"
#define ST_SUB_DEV                "subDev"
#define ST_NORMAL_DEV             "normalDev"
#define ST_DEVICE_INFO            "deviceInfo"
#define ST_CONFIGURING            "configuring"
#define ST_WEMO_LIST              "wemoList"
#define ST_BINARY_STATE_RESPONSE           "binaryState"
#define ST_BINARY_STATE_WEMO_DOC           "BinaryState"
#define ST_GET_NAME                  "getName"
#define ST_REAL_NAME                 "realName"
#define ST_UDN                       "udn"
#define ST_URL                       "url"
#define ST_GROUP                     "group"
#define ST_ADD_SUB_DEVS              "addSubDevs"
#define ST_GET_SUB_DEV_STATUS        "getSubDevStatus"
#define ST_REMOVE_SUB_DEV            "removeSubDev"

//for add new device 
#define ST_FOUND_DEVICE                 "foundDevice"
#define ST_REMOVE_AUTO_NODE_ZPC         "removeAutoNodeZpc"
#define ST_ADD_AUTO_NODE_ZPC            "addAutoNodeZpc"
#define ST_READY                        "ready"
#define ST_NODE_FOUND                   "nodeFound"
#define ST_REMOVING_SLAVE               "removingSlave"
#define ST_REMOVING_CONTROLLER          "removingController"
#define ST_REMOVE_DONE                  "removeDone"
// #define ST_REMOVE_NODE_ZPC              "removeNodeZpc"
#define ST_REMOVING                     "removing"
#define ST_REMOVE_FAILED                "removeFailed"
#define ST_ADDING_DEVICE                "addingDevice"
#define ST_ADDING_FAILED                "addingFailed"
#define ST_ADDING_SLAVE                 "addingSlave"
#define ST_ADDING_CONTROLLER            "addingController"
#define ST_ADD_FAILED                   "addFailed"
#define ST_FAILED_TO_INTERVIEW          "failToInterviewDevice"
#define ST_FAILED_TO_ADD_SECURE         "failToAddSecure"
#define ST_TIMEOUT                      "timeout"
#define ST_NOT_SUPPORT                  "notSupport"
#define ST_SERIAL_ID                    "serialId"
#define ST_SERIAL                       "serial"
#define ST_NEW_DEVICE                   "newDevice"
#define ST_OWNER                        "owner"
#define ST_CAPABILITY                   "capability"
#define ST_EP_MEMBERS                   "endpointMembers"
#define ST_PROFILE_ID                   "profileId"
#define ST_ENDPOINT_NUM                 "endpointNum"
#define ST_CHANGING_CONTROLLER          "changingController"
#define ST_CHANGE_DONE                  "changeDone"
#define ST_UPDATE_SUC_ID                "updateSucId"
#define ST_SUC_ID                       "sucId"
#define ST_ADD_DEV                      "addDev"
#define ST_ADDING                       "adding"
#define ST_POOLING                      "pooling"
// #define ST_ADD_NODE_ZPC                 "addNodeZPC"
#define ST_ADD_DONE                     "addDone"
#define ST_REMOVE_DEV                   "removeDev"
#define ST_PARENT_ID                    "parentId"

#define ST_OLD_ID                   "oldId"
#define ST_NEW_ID                   "newId"
#define ST_DEVICE_UPDATE_ID         "deviceUpdateId"
#define ST_DEVICE_STATE             "deviceState"
#define ST_ALIVE                    "alive"
#define ST_DEAD                     "dead"
#define ST_WAITTING                 "waitting"
#define ST_RUNNING                  "running"
#define ST_INACTIVE                 "inactive"
#define ST_ACTIVE                   "active"
#define ST_DEVICE                   "device"

//zwave alarm
#define ST_ON_OFF               "onOff"
#define ST_DIM                  "dim"
#define ST_SET_TEMP             "setTemp"
#define ST_REMOVE               "remove"
#define ST_CMD_CLASS_ZPC        "cmdClassZpc"
#define ST_DEVICE_ENDPOINT      "deviceEndpoint"
#define ST_CMD_CLASS            "cmdClass"
#define ST_ALARM_STATUS         "alarmStatus"
#define ST_ALARM_TYPE           "alarmType"
#define ST_ALARM_TYPE_FINAL     "alarmTypeFinal"
#define ST_ALARM_MEANING        "alarmMeaning"

#define ST_ALARM_LEVEL                  "alarmLevel"
#define ST_SENSOR_SRC_ID                "sensorSrcId"
#define ST_ZWAVE_ALARM_STATUS           "zwaveAlarmStatus"
#define ST_ZWAVE_ALARM_TYPE             "zwaveAlarmType"
#define ST_ZWAVE_ALARM_EVENT            "zwaveAlarmEvent"
#define ST_NO_EVENT_PARAMS              "noEventParams"
#define ST_EVENT_PARAM                  "eventParam"
#define ST_SEQUENCE                     "sequence"
#define ST_SEQUENCE_NUMBER              "sequenceNumber"
#define ST_SUPPORTED_REPORT             "supportedReport"
#define ST_V1_ALARM                     "v1Alarm"
#define ST_NO_BIT_MASKS                 "noBitMasks"
#define ST_BIT_MASK                     "bitMask"
#define ST_EVENT_SUPPORTED_REPORT       "eventSupportedReport"
#define ST_NOTIFICATION_TYPE            "notificationType"

#define ST_SRC_ID                    "srcId"
#define ST_SENSOR_TYPE               "sensorType"
#define ST_SENSOR_PATTERN            "sensorPattern"
#define ST_SENSOR_STATE              "sensorState"
#define ST_SECONDS                   "seconds"
#define ST_SENSOR_VALUE              "sensorValue"

#define ST_ZWAVE_PLUS_VERSION            "zwavePlusVersion"
#define ST_ROLE_TYPE                     "roleType"
#define ST_NODE_TYPE                     "nodeType"
#define ST_INSTALLER_ICON_TYPE           "installerIconType"
#define ST_USER_ICON_TYPE                "userIconType"

#define ST_MAX_NODES_SUPPORTED           "maxNodesSupported"
#define ST_REPORT_TO_FOLLOW              "reportToFollow"
#define ST_NO_NODES                      "noNodes"
#define ST_NO_MULTI_CHANNEL_NODE         "noMultiChannelNode"
#define ST_MULTI_CHANNEL_NODE            "multiChannelNode"
#define ST_BIT_ADDRESS                   "bitAddress"
#define ST_NODE_LIST                     "nodeList"
#define ST_NODE_MODE_LIST                "nodeModeList"
#define ST_GROUPINGS_REPORT              "groupingsReport"
#define ST_SUPPORTED_GROUPS              "supportedGroups"

#define ST_GROUP_NAME_REPORT             "groupNameReport"
#define ST_NAME_LEN                      "nameLen"
#define ST_NAME                          "name"
#define ST_COMMAND_LIST_REPORT           "commandListReport"
#define ST_LIST_LEN                      "listLen"
#define ST_CMD_LIST                      "cmdList"
#define ST_GROUP_INFO_REPORT             "groupInfoReport"
#define ST_LIST_MODE                     "listMode"
#define ST_DYNAMIC_INFO                  "dynamicInfo"
#define ST_GROUP_COUNT                   "groupCount"
#define ST_PROFILE                       "profile"
#define ST_EVENT_CODE                    "eventCode"
#define ST_GROUP_INFO                    "groupInfo"

#define ST_BUSY                          "busy"
#define ST_WAIT_TIME                     "waitTime"
#define ST_REJECTED_REQUEST              "rejectedRequest"

#define ST_SUPPORTED_SCALE_REPORT        "supportedScaleReport"
#define ST_SCALE_BIT_MASK                "scaleBitMask"
#define ST_SUPPORTED_SENSOR_REPORT       "supportedSensorReport"
#define ST_PRECISION                     "precision"
#define ST_SCALE                         "scale"
#define ST_SCALE_2                       "scale2"
#define ST_SIZE                          "size"

#define ST_CAPABILITIES_REPORT           "capabilitiesReport"
#define ST_SET_POINT_TYPE                "setpointType"
#define ST_SET_POINT_PATTERN             "setpointPattern"
#define ST_SET_POINT_VALUE               "setpointValue"
#define ST_MIN_PRECISION                 "minPrecision"
#define ST_MIN_SCALE                     "minScale"
#define ST_MIN_SIZE                      "minSize"
#define ST_MIN_VALUE                     "minValue"
#define ST_MAX_PRECISION                 "maxPrecision"
#define ST_MAX_SCALE                     "maxScale"
#define ST_MAX_SIZE                      "maxSize"
#define ST_MAX_VALUE                     "maxValue"

#define ST_SET_BACK_TYPE                 "setbackType"
#define ST_SET_BACK_STATE                "setbackState"

#define ST_LOGGING_REPORT                "loggingReport"
#define ST_NO_USAGES                     "noUsages"
#define ST_OPERATING_STATE_LOG_TYPE      "operatingStateLogType"
#define ST_USAGE_TODAY_HOURS             "usageTodayHours"
#define ST_USAGE_TODAY_MINUTES           "usageTodayMinutes"
#define ST_USAGE_YESTERDAY_HOURS         "usageYesterdayHours"
#define ST_USAGE_YESTERDAY_MINUTES       "usageYesterdayMinutes"
#define ST_USAGE_LIST                    "usageList"
#define ST_OPERATING_STATE               "operatingState"
#define ST_NO_MANUFACTURER_DATA          "noManufacturerData"
#define ST_MANUFACTURER_DATA             "manufacturerData"

#define ST_OFF                           "off"
#define ST_FAN_MODE                      "fanMode"
#define ST_FAN_OPERATING_STATE           "fanOperatingState"

#define ST_TARGET_VALUE                  "targetValue"
#define ST_DURATION                      "duration"

#define ST_BATTERY_LEVEL                 "batteryLevel"
#define ST_YEAR                          "year"
#define ST_MONTH                         "month"
#define ST_DAY                           "day"
#define ST_HOUR_UTC                      "hourUtc"
#define ST_MINUTE_UTC                    "minuteUtc"
#define ST_SECOND_UTC                    "secondUtc"

#define ST_RTC_FAILURE                   "rtcFailure"
#define ST_HOUR_LOCAL_TIME               "hourLocalTime"
#define ST_MINUTE_LOCAL_TIME             "minuteLocalTime"
#define ST_SECOND_LOCAL_TIME             "secondLocalTime"
#define ST_DATE_REPORT                   "dateReport"
#define ST_OFFSET_REPORT                 "offsetReport"
#define ST_SIGN_TZO                      "signTzo"
#define ST_HOUR_TZO                      "hourTzo"
#define ST_MINUTE_TZO                    "minuteTzo"
#define ST_SIGN_OFFSET_DST               "signOffsetDst"
#define ST_MINUTE_OFFSET_DST             "minuteOffsetDst"
#define ST_MONTH_START_DST               "monthStartDst"
#define ST_DAY_START_DST                 "dayStartDst"
#define ST_HOUR_START_DST                "hourStartDst"
#define ST_MONTH_END_DST                 "monthEndDst"
#define ST_DAY_END_DST                   "dayEndDst"
#define ST_HOUR_END_DST                  "hourEndDst"

#define ST_CAPABILITY_REPORT             "capabilityReport"
#define ST_COLOR                         "color"
#define ST_TEMPERATURE_COLOR             "temperatureColor"
#define ST_MAX                           "max"
#define ST_MIN                           "min"
#define ST_COLOR_COMPONENT_MASK          "colorComponentMask"
#define ST_COLOR_COMPONENT_ID            "colorComponentId"
#define ST_COLORFUL_MODE                 "colorfulMode"

#define ST_PRIMARY_SWITCH_TYPE           "primarySwitchType"
#define ST_SECONDARY_SWITCH_TYPE         "secondarySwitchType"

#define ST_METER_TYPE                    "meterType"
#define ST_METER_PATTERN                 "meterPattern"
#define ST_METER_VALUE                   "meterValue"
#define ST_DELTA_TIME                    "deltaTime"
#define ST_PREVIOUS_METER_VALUE          "previousMeterValue"
#define ST_METER_RESET                   "meterReset"
#define ST_MORE_SCALE_TYPES              "moreScaleTypes"
#define ST_SCALE_SUPPORTED               "scaleSupported"
#define ST_RATE_TYPE                     "rateType"

#define ST_MANUFACTURER_ID               "manufacturerId"
#define ST_PRODUCT_TYPE_ID               "productTypeId"
#define ST_PRODUCT_ID                    "productId"
#define ST_DEVICE_SPECIFIC_REPORT        "deviceSpecificReport"
#define ST_DEVICE_ID_TYPE                "deviceIdType"
#define ST_DEVICE_ID_DATA_FORMAT         "deviceIdDataFormat"
#define ST_DEVICE_ID_DATA_LENGTH         "deviceIdDataLength"
#define ST_DEVICE_ID_DATA                "deviceIdData"

#define ST_END_POINT_REPORT              "endPointReport"
#define ST_DYNAMIC                       "dynamic"
#define ST_IDENTICAL                     "identical"
#define ST_NO_ENDPOINT                   "noEndpoint"
#define ST_NO_AGG_ENDPOINT               "noAggEndpoint"
#define ST_INDIVIDUAL_END_POINTS         "individualEndPoints"
#define ST_AGGREGATED_END_POINTS         "aggregatedEndPoints"
#define ST_ENDPOINT                      "endpoint"
#define ST_GENERIC_DEVICE_CLASS          "genericDeviceClass"
#define ST_SPECIFIC_DEVICE_CLASS         "specificDeviceClass"
#define ST_NO_CMD_CLASS                  "noCmdClass"
#define ST_CMD_CLASS_LIST                "cmdClassList"
#define ST_END_POINT_FIND_REPORT         "endPointFindReport"
#define ST_ENDPOINT_LIST                 "endpointList"
#define ST_AGGREGATED_MEMBERS_REPORT     "aggregatedMembersReport"
#define ST_AGGREGATED_END_POINT          "aggregatedEndPoint"

#define ST_TEST_NODE_REPORT              "testNodeReport"
#define ST_TEST_NODE_ID                  "testNodeId"
#define ST_STATUS_OF_OPERATION           "statusOfOperation"
#define ST_TEST_FRAME_ACK_COUNT          "testFrameAckCount"

#define ST_ZW_LIB_TYPE                   "zwLibType"
#define ST_ZW_PROTOCOL_VERSION           "zwProtocolVersion"
#define ST_ZW_PROTOCOL_SUB_VERSION       "zwProtocolSubVersion"
#define ST_APP_VERSION                   "appVersion"
#define ST_APP_SUB_VERSION               "appSubVersion"
#define ST_FIRMWARE_0_VERSION            "firmware0Version"
#define ST_FIRMWARE0_SUB_VERSION         "firmware0SubVersion"
#define ST_HARDWARE_VERSION              "hardwareVersion"
#define ST_NO_FIRMWARE_TARGETS           "noFirmwareTargets"
#define ST_FIRMWARE_VERSION              "firmwareVersion"
#define ST_FIRMWARE_SUB_VERSION          "firmwareSubVersion"
#define ST_FW_TARGET_LIST                "fwTargetList"
#define ST_COMMAND_CLASS_REPORT          "commandClassReport"
#define ST_REQUEST_CMD_CLASS             "requestCmdClass"
#define ST_CMD_CLASS_VERSION             "cmdClassVersion"

#define ST_PROTECTION_STATE              "protectionState"
#define ST_LOCAL_PROTECTION_STATE        "localProtectionState"
#define ST_RF_PROTECTION_STATE           "rfProtectionState"
#define ST_EXCLUSIVE_CONTROL             "exclusiveControl"
#define ST_EC_REPORT                     "ecReport"
#define ST_TIMEOUT_REPORT                "timeoutReport"
#define ST_NODE_ID                       "nodeId"

#define ST_PARAMETER_NUMBER              "parameterNumber"
#define ST_CONFIGURATION_VALUE           "configurationValue"
#define ST_BULK_REPORT                   "bulkReport"
#define ST_PARAMETER_OFFSET              "parameterOffset"
#define ST_NO_PARAMS                     "noParams"
#define ST_DEFAULT                       "default"
#define ST_HANDSHAKE                     "handshake"
#define ST_PARAMS_LIST                   "paramsList"
#define ST_NAME_REPORT                   "nameReport"
#define ST_INFO_REPORT                   "infoReport"
#define ST_INFO_LEN                      "infoLen"
#define ST_INFO                          "info"
#define ST_PROPERTIES_REPORT             "propertiesReport"
#define ST_FORMAT                        "format"
#define ST_DEFAULT_VALUE                 "defaultValue"
#define ST_NEXT_PARAMETER_NUMBER         "nextParameterNumber"
#define ST_DOOR_LOCK_MODE                "doorLockMode"
#define ST_OUTSIDE_DOOR_HANDLES_MODE     "outsideDoorHandlesMode"
#define ST_INSIDE_DOOR_HANDLES_MODE      "insideDoorHandlesMode"
#define ST_DOOR_CONDITION                "doorCondition"
#define ST_LOCK_TIMEOUT_MINUTES          "lockTimeoutMinutes"
#define ST_LOCK_TIMEOUT_SECONDS          "lockTimeoutSeconds"
#define ST_TARGET_DOOR_LOCK_MODE         "targetDoorLockMode"
#define ST_CONFIGURATION_REPORT          "configurationReport"
#define ST_OPERATION_TYPE                "operationType"

#define ST_TYPE_SUPPORTED_REPORT         "typeSupportedReport"
#define ST_NO_SLOTS_WEEK_DAY             "noSlotsWeekDay"
#define ST_NO_SLOTS_YEAR_DAY             "noSlotsYearDay"
#define ST_NO_SLOTS_DAILY_REPEATING      "noSlotsDailyRepeating"
#define ST_WEEK_DAY_REPORT               "weekDayReport"
#define ST_SCHEDULE_SLOT_ID              "scheduleSlotId"
#define ST_DAY_OF_WEEK                   "dayOfWeek"
#define ST_START_HOUR                    "startHour"
#define ST_START_MINUTE                  "startMinute"
#define ST_STOP_HOUR                     "stopHour"
#define ST_STOP_MINUTE                   "stopMinute"
#define ST_YEAR_DAY_REPORT               "yearDayReport"
#define ST_START_YEAR                    "startYear"
#define ST_START_MONTH                   "startMonth"
#define ST_START_DAY                     "startDay"
#define ST_STOP_YEAR                     "stopYear"
#define ST_STOP_MONTH                    "stopMonth"
#define ST_STOP_DAY                      "stopDay"
#define ST_TIME_OFFSET_REPORT            "timeOffsetReport"
#define ST_DAILY_REPEATING_REPORT        "dailyRepeatingReport"
#define ST_WEEK_DAY_BIT_MASK             "weekDayBitMask"
#define ST_DURATION_HOUR                 "durationHour"
#define ST_DURATION_MINUTE               "durationMinute"

#define ST_SIGNAL_SUPPORTED_REPORT       "signalSupportedReport"
#define ST_SIGNAL_SUPPORTED              "signalSupport"
#define ST_SIGNAL_REPORT                 "signalReport"
#define ST_SUBSYSTEM_TYPE                "subsystemType"
#define ST_SUBSYSTEM_NAME                "subsystemName"
#define ST_SUBSYSTEM_STATE               "subsystemState"

#define ST_SYSTEM_INFO_REPORT            "systemInfoReport"
#define ST_MASTER_VALVE                  "masterValve"
#define ST_TOTAL_NO_VALVES               "totalNoValves"
#define ST_TOTAL_NO_VALVE_TABLES         "totalNoValveTables"
#define ST_VALVE_TABLE_MAX_SIZE          "valveTableMaxSize"
#define ST_SYSTEM_STATUS_REPORT          "systemStatusReport"
#define ST_SYSTEM_VOLTAGE                "systemVoltage"
#define ST_SENSOR_STATUS                 "sensorStatus"
#define ST_FLOW_PRECISION                "flowPrecision"
#define ST_FLOW_SCALE                    "flowScale"
#define ST_FLOW_SIZE                     "flowSize"
#define ST_FLOW_VALUE                    "flowValue"
#define ST_PRESSURE_PRECISION            "pressurePrecision"
#define ST_PRESSURE_SCALE                "pressureScale"
#define ST_PRESSURE_SIZE                 "pressureSize"
#define ST_PRESSURE_VALUE                "pressureValue"
#define ST_SHUTOFF_DURATION              "shutoffDuration"
#define ST_SYSTEM_ERROR_STATUS           "systemErrorStatus"
#define ST_VALVE_ID                      "valveId"
#define ST_SYSTEM_CONFIG_REPORT          "systemConfigReport"
#define ST_MASTER_VALVE_DELAY            "masterValveDelay"
#define ST_HIGH_PRESSURE_THRESHOLD_PRECISION            "highPressureThresholdPrecision"
#define ST_HIGH_PRESSURE_THRESHOLD_SCALE                "highPressureThresholdScale"
#define ST_HIGH_PRESSURE_THRESHOLD_SIZE                 "highPressureThresholdSize"
#define ST_HIGH_PRESSURE_THRESHOLD_VALUE                "highPressureThresholdValue"
#define ST_LOW_PRESSURE_THRESHOLD_PRECISION             "lowPressureThresholdPrecision"
#define ST_LOW_PRESSURE_THRESHOLD_SCALE                 "lowPressureThresholdScale"
#define ST_LOW_PRESSURE_THRESHOLD_SIZE                  "lowPressureThresholdSize"
#define ST_LOW_PRESSURE_THRESHOLD_VALUE                 "lowPressureThresholdValue"
#define ST_SENSOR_POLARITY               "sensorPolarity"
#define ST_VALVE_INFO_REPORT             "valveInfoReport"
#define ST_NOMINAL_CURRENT               "nominalCurrent"
#define ST_VALVE_ERROR_STATUS            "valveErrorStatus"
#define ST_VALVE_CONFIG_REPORT           "valveConfigReport"
#define ST_NOMINAL_CURRENT_HIGH_THRESHOLD               "nominalCurrentHighThreshold"
#define ST_NOMINAL_CURRENT_LOW_THRESHOLD                "nominalCurrentLowThreshold"
#define ST_MAXIMUM_FLOW_PRECISION                       "maximumFlowPrecision"
#define ST_MAXIMUM_FLOW_SCALE                           "maximumFlowScale"
#define ST_MAXIMUM_FLOW_SIZE                            "maximumFlowSize"
#define ST_MAXIMUM_FLOW_VALUE                           "maximumFlowValue"
#define ST_FLOW_HIGH_THRESHOLD_PRECISION                "flowHighThresholdPrecision"
#define ST_FLOW_HIGH_THRESHOLD_SCALE                    "flowHighThresholdScale"
#define ST_FLOW_HIGH_THRESHOLD_SIZE                     "flowHighThresholdSize"
#define ST_FLOW_HIGH_THRESHOLD_VALUE                    "flowHighThresholdValue"
#define ST_FLOW_LOW_THRESHOLD_PRECISION                 "flowLowThresholdPrecision"
#define ST_FLOW_LOW_THRESHOLD_SCALE                     "flowLowThresholdScale"
#define ST_FLOW_LOW_THRESHOLD_SIZE                      "flowLowThresholdSize"
#define ST_FLOW_LOW_THRESHOLD_VALUE                     "flowLowThresholdValue"
#define ST_SENSOR_USAGE                                 "sensorUsage"
#define ST_VALVE_TABLE_REPORT            "valveTableReport"
#define ST_VALVE_TABLE_ID                "valveTableId"
#define ST_NO_VALVES                     "noValves"
#define ST_VALVE_LIST                    "valveList"

#define ST_NO_REPORTS                    "noReports"
#define ST_REPORT_NO                     "reportNo"

#define ST_USERS_NUMBER_REPORT           "usersNumberReport"
#define ST_SUPPORTED_USERS               "supportedUsers"
#define ST_NO_USER_CODE                  "noUserCode"

#define ST_BYTE_REPLACE_MULTI             "byteReplaceMulti"
#define ST_PATTERN                        "pattern"
#define ST_BYTE_INDEX                     "byteIndex"
#define ST_REPLACE_TO                     "replaceTo"
#define ST_MEANING                        "meaning"

#define ST_INTERVAL_TIME                 "intervalTime"
#define ST_INTERVAL_REPORT               "intervalReport"
#define ST_INTERVAL_CAPABILITIES_REPORT  "intervalCapabilitiesReport"
#define ST_MIN_WK_INTERVAL_SECONDS       "minWkIntervalSeconds"
#define ST_MAX_WK_INTERVAL_SECONDS       "maxWkIntervalSeconds"
#define ST_DEFAULT_WK_INTERVAL_SECONDS   "defaultWkIntervalSeconds"
#define ST_WK_INTERVAL_STEP_SECONDS      "wkIntervalStepSeconds"

#define ST_PARAM_ID                         "paramId"
#define ST_NO_PARAM_MASKS                   "noParamMasks"
#define ST_PARAM_MASK                       "paramMask"

#define ST_WAKE_UP_NOTIFICATION          "wakeUpNotification"
#define ST_LAST_UPDATE                   "lastUpdate"
#define ST_FILE_NAME                     "fileName"

#define ST_TEMPERATURE_LOW_THRESHOLD     "lowTemperatureThreshold"
#define ST_TEMPERATURE_HIGH_THRESHOLD    "highTemperatureThreshold"

//zwave class
#define ST_ALARM                            "alarm"
#define ST_NOTIFICATION                     "notification"
#define ST_REPORT                           "report"
#define ST_SENSOR_ALARM                     "sensorAlarm"
#define ST_SENSOR_BINARY                    "sensorBinary"
#define ST_SWITCH_ALL                       "switchAll"
#define ST_DEVICE_RESET_LOCALLY             "deviceResetLocally"
#define ST_ZWAVE_PLUS_INFO                  "zwavePlusInfo"
#define ST_ASSOCIATION                      "association"
#define ST_ASSOCIATION_GRP_INFO             "associationGrpInfo"
#define ST_APPLICATION_STATUS               "applicationStatus"
#define ST_SENSOR_MULTILEVEL                "sensorMultilevel"
#define ST_THERMOSTAT_SETPOINT              "thermostatSetpoint"
#define ST_THERMOSTAT_SETBACK               "thermostatSetback"
#define ST_THERMOSTAT_OPERATING_STATE       "thermostatOperatingState"
#define ST_THERMOSTAT_MODE                  "thermostatMode"
#define ST_THERMOSTAT_PREVIOUS_MODE         "thermostatPreviousMode"
#define ST_THERMOSTAT_FAN_MODE              "thermostatFanMode"
#define ST_THERMOSTAT_FAN_STATE             "thermostatFanState"
#define ST_BASIC                            "basic"
#define ST_BATTERY                          "battery"
#define ST_SWITCH_BINARY                    "switchBinary"
#define ST_TIME_PARAMETERS                  "timeParameters"
#define ST_SWITCH_COLOR                     "switchColor"
#define ST_SWITCH_MULTILEVEL                "switchMultilevel"
#define ST_METER                            "meter"
#define ST_MANUFACTURER_SPECIFIC            "manufacturerSpecific"
#define ST_MULTI_CHANNEL                    "multiChannel"
#define ST_POWER_LEVEL                      "powerLevel"
#define ST_PROTECTION                       "protection"
#define ST_CONFIGURATION                    "configuration"
#define ST_DOOR_LOCK                        "doorLock"
#define ST_SCHEDULE_ENTRY_LOCK              "scheduleEntryLock"
#define ST_BARRIER_OPERATOR                 "barrierOperator"
#define ST_IRRIGATION                       "irrigation"
#define ST_SENSOR_CONFIGURATION             "sensorConfiguration"
#define ST_TRIGGER_VALUE                    "triggerValue"
#define ST_SIMPLE_AV_CONTROL                "simpleAvControl"
#define ST_USER_CODE                        "userCode"
#define ST_WAKE_UP                          "wakeUp"
#define ST_WINDOW_COVERING                  "windowCovering"
#define ST_CONTROLLER_REPLICATION           "controllerReplication"
#define ST_CRC_16_ENCAP                     "crc16Encap"
#define ST_MULTI_CHANNEL_ASSOCIATION        "multiChannelAssociation"
#define ST_MULTI_CHANNEL_V3                 "multiChannelV3"
#define ST_SILENCE_ALARM                    "silenceAlarm"
#define ST_NO_OPERATION                     "noOperation"
#define ST_DOOR_LOCK_LOGGING                "doorLockLogging"
#define ST_ANTITHEFT                        "antitheft"
#define ST_CENTRAL_SCENE                    "centralScene"
#define ST_LOCK                             "lock"
#define ST_FIRMWARE_UPDATE_MD               "firmwareUpdateMd"
#define ST_LANGUAGE                         "language"
#define ST_TIME                             "time"
#define ST_MULTI_CMD                        "multiCmd"
#define ST_CLOCK                            "clock"
#define ST_RECORDS_SUPPORTED_REPORT         "recordsSupportedReport"
#define ST_MAX_RECORDS_STORED               "maxRecordsStored"
#define ST_RECORD_REPORT                    "recordReport"
#define ST_RECORD_NUMBER                    "recordNumber"
#define ST_TIMESTAMP_YEAR                   "timestampYear"
#define ST_TIMESTAMP_MONTH                  "timestampMonth"
#define ST_TIMESTAMP_DAY                    "timestampDay"
#define ST_RECORD_STATUS                    "recordStatus"
#define ST_TIMESTAMP_HOUR_LOCAL_TIME        "timestampHourLocalTime"
#define ST_TIMESTAMP_MINUTE_LOCAL_TIME      "timestampMinuteLocalTime"
#define ST_TIMESTAMP_SECOND_LOCAL_TIME      "timestampSecondLocalTime"
#define ST_EVENT_TYPE                       "eventType"
#define ST_USER_IDENTIFIER                  "userIdentifier"
#define ST_USER_CODE_LENGTH                 "userCodeLength"
#define ST_VALID_USER_ID                    "validUserId"

///////////////END ZWAVE CLASS////////////////

#define ST_MY_ID                            "myId"
#define ST_LEARN_MODE                       "learnMode"
#define ST_STARTED                          "started"
#define ST_REMOVED                          "removed"
#define ST_HOMEID                           "homeId"
#define ST_CONTROLLER_CAPABILITY            "controllerCapability"
#define ST_LEARN_FAILED                     "learnFailed"
#define ST_ADD_NON_SECURE_DONE              "addNonSecureDone"
#define ST_NETWORK_CAPABILITY               "networkCapability"
#define ST_NETWORK_SECURITY                 "networkSecurity"
#define ST_NODE_MODE                        "nodeMode"
#define ST_TYPE_BASIC                       "typeBasic"
#define ST_TYPE_GENERIC                     "typeGeneric"
#define ST_TYPE_SPECIFIC                    "typeSpecific"
#define ST_ADD_SECURE_STARTED               "addSecureStarted"
#define ST_ADD_SECURE_DONE                  "addSecureDone"
#define ST_ADD_SECURE_FAILED                "addSecureFailed"
#define ST_REPLACE_TIMEOUT                  "replaceTimeout"
#define ST_REPLACE_TIMEOUT_WAITING_NIF      "replaceTimeoutWaitingNIF"
#define ST_REPLACE_FAILED                   "replaceFailed"
#define ST_REPLACE_DONE                     "replaceDone"
#define ST_NON_SECURE_CAPABILITY            "nonSecureCapability"
#define ST_NODE_SECURE_SCHEME               "nodeSecureScheme"
#define ST_NODE_FLAGS                       "nodeFlags"
#define ST_MY_NODE_FLAGS                    "myNodeFlags"//my node scheme for learn mode
#define ST_INCLUDING_SECURE_SCHEME          "includingSecureScheme"
#define ST_SECURE_CAPABILITY                "secureCapability"
#define ST_SECURE_2_CAPABILITY              "secure2Capability"
#define ST_MULTICHANNEL_CAPABILITY          "multichannelCapability"
#define ST_MULTICHANNEL_AGG_CAPABILITY      "multichannelAggCapability"
#define ST_IS_ZWAVE_PLUS                    "isZWavePlus"
#define ST_SET_SPECIFICATION_IN_QUEUE       "setSpecificationInQueue"
#define ST_CONTROLLER_CHANGE_ZPC            "controllerChangeZpc"
#define ST_SUPPORTED_GET_SENSOR             "supportedGetSensor"
#define ST_SUPPORTED_GET_SCALE              "supportedGetScale"
#define ST_SUPPORTED_GET                    "supportedGet"
#define ST_GET_SECURE_KEY                   "getSecureKey"

#define ST_SET                              "set"
#define ST_VER                              "ver"
#define ST_ON                               "on"
#define ST_OFF                              "off"
#define ST_STOP_LEVEL_CHANGE                "stopLevelChange"
#define ST_START_LEVEL_CHANGE               "startLevelChange"
#define ST_END_POINT_FIND                   "endpointFind"
#define ST_END_POINT_GET                    "endpointGet"
#define ST_CAPABILITY_GET                   "capabilityGet"
#define ST_AGGREGATED_MEMBERS_GET           "aggregatedMembersGet"
#define ST_GROUPINGS_GET                    "groupingsGet"
#define ST_SPECIFIC_GROUP_GET               "specificGroupGet"
#define ST_DEVICE_SPECIFIC_GET              "deviceSpecificGet"
#define ST_GROUPINGS_REPORT                 "groupingsReport"
#define ST_SPECIFIC_GROUP_REPORT            "specificGroupReport"
#define ST_GROUP_NAME_GET                   "groupNameGet"
#define ST_GROUP_INFO_GET                   "groupInfoGet"
#define ST_GROUP_COMMAND_LIST_GET           "groupCommandListGet"
#define ST_BULK_SET                         "bulkSet"
#define ST_BULK_GET                         "bulkGet"
#define ST_NAME_GET                         "nameGet"
#define ST_INFO_GET                         "infoGet"
#define ST_PROPERTIES_GET                   "propertiesGet"
#define ST_CONFIGURATION_GET                "configurationGet"
#define ST_CONFIGURATION_SET                "configurationSet"
#define ST_USERS_NUMBER_GET                 "usersNumberGet"
#define ST_CAPABILITIES_GET                 "capabilitiesGet"
#define ST_INTERVAL_GET                     "intervalGet"
#define ST_INTERVAL_SET                     "intervalSet"
#define ST_INTERVAL_CAPABILITIES_GET        "intervalCapabilitiesGet"
#define ST_NO_MORE_INFORMATION              "noMoreInformation"
#define ST_LOGGING_GET                      "loggingGet"
#define ST_LOGGING_SUPPORTED_GET            "loggingSupportedGet"
#define ST_SIGNAL_SUPPORTED_GET             "signalSupportedGet"
#define ST_SIGNAL_SET                       "signalSet"
#define ST_SIGNAL_GET                       "signalGet"
#define ST_SYSTEM_CONFIG_SET                "systemConfigSet"
#define ST_SYSTEM_CONFIG_GET                "systemConfigGet"
#define ST_SYSTEM_INFO_GET                  "systemInfoGet"
#define ST_SYSTEM_STATUS_GET                "systemStatusGet"
#define ST_VALVE_INFO_GET                   "valveInfoGet"
#define ST_VALVE_CONFIG_GET                 "valveConfigGet"
#define ST_VALVE_CONFIG_SET                 "valveConfigSet"
#define ST_VALVE_RUN                        "valveRun"
#define ST_VALVE_TABLE_SET                  "valveTableSet"
#define ST_VALVE_TABLE_GET                  "valveTableGet"
#define ST_VALVE_TABLE_RUN                  "valveTableRun"
#define ST_SYSTEM_SHUTOFF                   "systemShutoff"
#define ST_COMMAND_CLASS_GET                "commandClassGet"
#define ST_REJECT_REQUEST                   "rejectRequest"
#define ST_EVENT_SUPPORTED_GET              "eventSupportedGet"
#define ST_TEST_NODE_GET                    "testNodeGet"
#define ST_TEST_NODE_SET                    "testNodeSet"
#define ST_EC_GET                           "ecGet"
#define ST_EC_SET                           "ecSet" 
#define ST_TIMEOUT_SET                      "timeoutSet"
#define ST_TIMEOUT_GET                      "timeoutGet"
#define ST_ENABLE_SET                       "enableSet"
#define ST_ENABLE_ALL_SET                   "enableAllSet"
#define ST_TYPE_SUPPORTED_GET               "typeSupportedGet"
#define ST_WEEK_DAY_SET                     "weekDaySet"
#define ST_WEEK_DAY_GET                     "weekDayGet"
#define ST_YEAR_DAY_SET                     "yearDaySet"
#define ST_YEAR_DAY_GET                     "yearDayGet"
#define ST_TIME_OFFSET_GET                  "timeOffsetGet"
#define ST_TIME_OFFSET_SET                  "timeOffsetSet"
#define ST_DAILY_REPEATING_GET              "dailyRepeatingGet"
#define ST_DAILY_REPEATING_SET              "dailyRepeatingSet"
#define ST_OFFSET_SET                       "offsetSet"
#define ST_OFFSET_GET                       "offsetGet"
#define ST_DATE_GET                         "dateGet"

#define ST_OVERLOAD_PROTECT                 "overloadProtect"
#define ST_LOCK_CONFIGURATION               "lockConfiguration"
#define ST_REPORT_GROUP                     "reportGroup"
#define ST_ONOFF_GROUP                      "onOffGroup"
#define ST_CONFIG_REPORT_GROUP              "configReportGroup"
#define ST_CONFIG_REPORT_GROUP_2            "configReportGroup2"
#define ST_CONFIG_REPORT_GROUP_3            "configReportGroup3"
#define ST_ENABLE_MOTION                    "enableMotion"
#define ST_REPORT_SENSOR                    "reportSensor"
#define ST_TIME_AUTO_REPORT                 "timeAutoReport"
#define ST_ENABLE_ONOFF                     "enableOnOff"

#define ST_BEEPER                           "beeper"
#define ST_VACATION                         "vacation"
#define ST_LOCK_AND_LEAVE                   "lockAndLeave"
#define ST_PIN_LENGTH                       "pinCodeLength"
#define ST_METER_REPORT_GROUP               "meterReportGroup"
#define ST_SOUND_VOLUME                     "soundVolume"
#define ST_SET_DIMMER                       "setDimmer"
#define ST_ONOFF_LED                        "onOffLed"
#define ST_PIR_SENSOR                       "pirSensor"
#define ST_TAMPER_ALARM                     "tamperAlarm"
#define ST_ALARM_GENERIC                    "alarmGeneric"
#define ST_DETERMINES                       "determines"
#define ST_SET_REPORT                       "setReport"
#define ST_BINARY_REPORT                    "binaryReport"
#define ST_BASIC_SET                        "basicSet"
#define ST_ALARM_BOARDCAST                  "alarmBoardcast"
#define ST_DELAY_ALARM                      "delayAlarm"
#define ST_ONOFF_GROUP1                     "onOffGroup1"
#define ST_ONOFF_GROUP2                     "onOffGroup2"
#define ST_ONOFF_GROUP3                     "onOffGroup3"
#define ST_LOW_TEMP_TRIGGER                 "lowTempTrigger"
#define ST_HIGH_TEMP_TRIGGER                "highTempTrigger"
#define ST_LOW_BATTERY_REPORT_GROUP         "lowBatteryReportGroup"
#define ST_TEMP_TRIGGER_ON                  "tempTriggerOn"
#define ST_TEMP_TRIGGER_OFF                 "tempTriggerOff"
#define ST_HUMI_TRIGGER_ON                  "humiTriggerOn"
#define ST_HUMI_TRIGGER_OFF                 "humiTriggerOff"
#define ST_AUTO_REPORT_TIME                 "autoReportTime"
#define ST_AUTO_REPORT_TEMP_CHANGE          "autoReportTempChange"
#define ST_AUTO_REPORT_HUMI_CHANGE          "autoReportHumiChange"
#define ST_CURRENT_OVERLOAD_PROTECTION      "currentOverloadProtection"
#define ST_RE_POWER_STATUS                  "rePowerStatus"
#define ST_RGB_LED_TESTING                  "rgbLedTesting"
#define ST_ENABLE_SEND_NOTIFICATION         "enableSendNotification"
#define ST_CONFIG_RGB_LED_MODE              "configRgbLedMode"
#define ST_CHANGE_COLOR_RGB_LED             "changeColorRgbLed"
#define ST_CHANGE_BRIGHTNESS_RGB_LED        "changeBrightnessRgbLed"
#define ST_CLEAR_ALARM                      "clearAlarm"
#define ST_USER_CODE_TYPE_01                "userCodeType01"
#define ST_USER_CODE_TYPE_02                "userCodeType02"
#define ST_USER_CODE_TYPE_03                "userCodeType03"
#define ST_USER_CODE_TYPE_04                "userCodeType04"
#define ST_USER_CODE_TYPE_05                "userCodeType05"
#define ST_USER_CODE_TYPE_06                "userCodeType06"
#define ST_USER_CODE_TYPE_07                "userCodeType07"
#define ST_USER_CODE_TYPE_08                "userCodeType08"
#define ST_USER_CODE_TYPE_09                "userCodeType09"
#define ST_USER_CODE_TYPE_10                "userCodeType10"
#define ST_USER_CODE_TYPE_11                "userCodeType11"
#define ST_USER_CODE_TYPE_12                "userCodeType12"
#define ST_USER_CODE_TYPE_13                "userCodeType13"
#define ST_USER_CODE_TYPE_14                "userCodeType14"
#define ST_USER_CODE_TYPE_15                "userCodeType15"
#define ST_USER_CODE_TYPE_16                "userCodeType16"
#define ST_USER_CODE_TYPE_17                "userCodeType17"
#define ST_USER_CODE_TYPE_18                "userCodeType18"
#define ST_USER_CODE_TYPE_19                "userCodeType19"
#define ST_USER_CODE_TYPE_20                "userCodeType20"
#define ST_USER_CODE_TYPE_21                "userCodeType21"
#define ST_USER_CODE_TYPE_22                "userCodeType22"
#define ST_USER_CODE_TYPE_23                "userCodeType23"
#define ST_USER_CODE_TYPE_24                "userCodeType24"
#define ST_USER_CODE_TYPE_25                "userCodeType25"
#define ST_USER_CODE_TYPE_26                "userCodeType26"
#define ST_USER_CODE_TYPE_27                "userCodeType27"
#define ST_USER_CODE_TYPE_28                "userCodeType28"
#define ST_USER_CODE_TYPE_29                "userCodeType29"
#define ST_USER_CODE_TYPE_30                "userCodeType30"
#define ST_DIP_SWITCHES_STATE               "dipSwitchesState"
#define ST_RESET_TO_FACTORY                 "resetToFactory"
#define ST_IR_CODE_LEARNING                 "irCodeLearning"
#define ST_LEARNING_STATUS                  "learningStatus"
#define ST_BUILT_IN_IR_CODE                 "builtInIrCode"
#define ST_EXTERNAL_EMITTER_POWER           "externalEmitterPower"
#define ST_SURROUND_IR_CONTROL              "surroundIrControl"
#define ST_SWING_CONTROL                    "swingControl"
#define ST_LEARNT_LOCATION_STATUS           "learntLocationStatus"
#define ST_RECORDS_SUPPORTED_GET            "recordsSupportedGet"
#define ST_RECORD_GET                       "recordGet"
#define ST_ENCAP                            "encap"


/// for zwave compliant testing application ////
#define ST_SET_SUC_MODE                     "setSucMode"
#define ST_SUC                              "suc"
#define ST_SIS                              "sis"
#define ST_CONTROLLER_CHANGE                "controllerChange"
#define ST_REQ_NET_UPDATE                   "reqNetUpdate"
#define ST_REPLACE_FAILED_NODE              "replaceFailedNode"
#define ST_NETWORK_TEST                     "networkTest"
#define ST_IS_FAILED_NODE                   "isFailedNode"
#define ST_CONTROLLER_INFO                  "controllerInfo"
#define ST_REMOVE_FAILED_NODE               "removeFailedNode"
#define ST_LEARNING_MODE                    "learningMode"
#define ST_MISSING_ARGUMENT                 "Missing Argument"
#define ST_REMOVED_NODE_ID                  "removedNodeId"
#define ST_NOT_REMOVED                      "notRemoved"
#define ST_NOT_STARTED                      "notStarted"
#define ST_NO_VALID_ROUTE                   "noValidRoute"
#define ST_RUN                              "run"
#define ST_LANGUAGE_REPORT                  "languageReport"
#define ST_LANGUAGE_1                       "language1"
#define ST_LANGUAGE_2                       "language2"
#define ST_LANGUAGE_3                       "language3"
#define ST_COUNTRY                          "country"
#define ST_USER_ID_STATUS                   "userIdStatus"
#define ST_WEEKDAY                          "weekday"
#define ST_HOUR                             "hour"
#define ST_MINUTE                           "minute"
#define ST_PROTECTION_STATUS                "protectionStatus"
#define ST_NO_ANTI_THEFT_HINT               "noAntiTheftHint"
#define ST_HINT_LIST                        "hintList"
#define ST_SCENE_ACTIVATION                 "sceneActivation"
#define ST_SCENE_ACTUATOR_CONF              "sceneActuatorConf"
#define ST_SCENE_CONTROLLER_CONF            "sceneControllerConf"
#define ST_CENTRAL_SCENE                    "centralScene"
#define ST_DIMMING_DURATION                 "dimmingDuration"
#define ST_SCENE_ID                         "sceneId"
#define ST_LEVEL                            "level"
#define ST_HAIL                             "hail"
#define ST_INDICATOR                        "indicator"
#define ST_NODE_NAME_SET                    "nodeNameSet"
#define ST_NODE_NAME_GET                    "nodeNameGet"
#define ST_NODE_NAMING                      "nodeNaming"
#define ST_NODE_LOCATION_SET                "nodeLocationSet"
#define ST_NODE_LOCATION_GET                "nodeLocationGet"
#define ST_NODE_LOCATION_REPORT             "nodeLocationReport"
#define ST_NO_NODE_LOCATION                 "noNodeLocation"
#define ST_NODE_NAME_REPORT                 "nodeNameReport"
#define ST_NO_NODE_NAME                     "noNodeName"
#define ST_CHAR_PRESENTATION                "charPresentation"
#define ST_NODE_NAME_LIST                   "nodeNamelist"
#define ST_NODE_LOCATION_LIST               "nodeLocationlist"
#define ST_MANUFACTURER_PROPRIETARY         "manufacturerProprietary"
#define ST_FIRMWARE_MD_GET                  "firmwareMdGet"
#define ST_FIRMWARE_UPDATE_MD_REQUEST_GET   "firmwareUpdateMdRequestGet"
#define ST_FIRMWARE_UPDATE_MD_REQUEST_REPORT "firmwareUpdateMdRequestReport"
#define ST_FIRMWARE_UPDATE_MD_GET           "firmwareUpdateMdGet"
#define ST_FIRMWARE_UPDATE_MD_REPORT        "firmwareUpdateMdReport"
#define ST_FIRMWARE_MD_REPORT               "firmwareMdReport"
#define ST_FIRMWARE_UPDATE_MD_STATUS_REPORT "firmwareUpdateMdStatusReport"
#define ST_FIRMWARE_ID                      "firmwareId"
#define ST_CHECKSUM                         "checksum"
#define ST_FIRMWARE_0_ID                    "firmware0Id"
#define ST_FIRMWARE_0_CHECKSUM              "firmware0Checksum"
#define ST_FIRMWARE_UPGRADABLE              "firmwareUpgradable"
#define ST_NO_FIRMWARE_TARGETS              "noFirmwareTargets"
#define ST_MAX_FRAGMENT_SIZE                "maxFragmentSize"
#define ST_FIRMWARE_ID_LIST                 "firmwareIdList"
#define ST_REPORT_NUMBER                    "reportNumber"
#define ST_WAIT_TIME                        "waitTime"
#define ST_LAST                             "last"
#define ST_NO_DATA                          "noData"
#define ST_DATA_LIST                        "dataList"
#define ST_ZWAVE_CONTROLLER_BACKUP_RESTORE  "zwaveControllerBackupRestore"
#define ST_BACKUP                           "backup"
#define ST_RESTORE                          "restore"
#define ST_FLAG_CHEME                       "flagScheme"
#define ST_S2_KEY_GRANT                     "s2KeyGrant"
#define ST_SECURITY_KEYS                    "securityKeys"
#define ST_REQUEST_CSA                      "requestClientSideAuthentication"
#define ST_S2_PUBLIC_KEY_CHALLENGE          "s2PublicKeyChallenge"
#define ST_S2_PUBLIC_KEY_SHOW_UP            "s2PublicKeyShowUp"
#define ST_S2_HIGH_MY_DSK                   "s2HighMyDSK"
#define ST_S2_LOW_MY_DSK                    "s2LowMyDSK"
#define ST_S2_MY_DSK                        "s2MyDSK"
#define ST_S2_MY_DSK_HIGHLIGHT_LEN          "s2MyDSKHighlightLen"
#define ST_S2_2_BYTE_SUGGEST                "s2ByteDSKSuggest"
#define ST_S2_BOOTSTRAPPING_KEX_REPORT      "s2BootstrappingKexReport"
#define ST_S2_BOOTSTRAPPING_CHALLENGE_RESPONSE      "s2BootstrappingChallengeRespone"
#define ST_PRIORITY_ROUTE                           "priorityRoute"
#define ST_SUPERVISION                              "supervision"
#define ST_SUPERVISION_REPORT                       "supervisionReport"
#define ST_MORE_STATUS_UPDATES                      "moreStatusUpdates"
#define ST_CMD_CLASS_ENCAP                          "cmdClassEncap"
#define ST_CMD_ENCAP                                "cmdEncap"
#define ST_REMOTE_ASSOCIATION_ACTIVATE              "remoteAssociationActivate"
#define ST_REMOTE_ASSOCIATION                       "remoteAssociation"
#define ST_LOCAL_GROUPING_ID                        "localGroupingId"
#define ST_REMOTE_NODE_ID                           "remoteNodeId"
#define ST_REMOTE_GROUPING_ID                       "remoteGroupingId"
#define ST_SEND_MY_NIF                              "sendMyNIF"
#define ST_RETURN                                   "return"
#define ST_NETWORK_INFO                             "networkInfo"
#define ST_INCLUSION_CONTROLLER                     "inclusionController"
#define ST_FIRMWARE_UPDATE_START                    "firmwareUpdateStart"
#define ST_FIRMWARE_UPDATE_STOP_BY_USER             "firmwareUpdateStopByUser"
#define ST_FIRMWARE_UPDATE_TIMOUT_STATE             "firmwareUpdateTimeoutState"
#define ST_FIRMWARE_UPDATE_SUCCESS                  "firmwareUpdateSuccess"
#define ST_FIRMWARE_UPDATE_FAIL                     "firmwareUpdateFail"
#define ST_FIRMWARE_UPDATE_FAIL_TO_OPEN_FILE        "firmwareUpdateFailToOpenFile"
#define ST_FIRMWARE_UPDATE_DEVICE_NOT_RESPONE       "firmwareUpdateDeviceNotRespone"
#define ST_WAITING_FIRMWARE_UPDATE                  "waitingFirmwareUpdate"
#define ST_INDICATOR_0_VALUE                        "indicator0Value"
#define ST_INDICATOR_OBJECT_COUNT                   "indicatorObjectCount"
#define ST_INDICATOR_ID                             "indicatorId"
#define ST_NEXT_INDICATOR_ID                        "nextIndicatorId"
#define ST_PROPERTY_ID                              "propertyId"
#define ST_VALUE                                    "value"
#define ST_TEMPORARY_TIME                           "temporaryTime"
#define ST_INDICATOR_OBJECT_LIST                    "indicatorObjectList"
#define ST_PROPERTY_SUPPORTED_BIT_MASK_LENGTH       "propertySupportedBitMaskLength"
#define ST_PROPERTY_SUPPORTED_BIT_MASK_LIST         "propertySupportedBitMaskList"
#define ST_TRIGGER_LEVEL_SET                        "triggerLevelSet"
#define ST_TRIGGER_LEVEL_GET                        "triggerLevelGet"
#define ST_UPDATE_FIRMWARE_PROGRAMMER               "updateFirmwareProgrammer"
#define ST_GET_FIRMWARE_PROGRAMMER_VERSION          "getFirmwareProgrammerVersion"
#define ST_DELAY                                    "delay"


///////////////////
/// for rule////
#define ST_METHOD_NOTIFY                        "\"method\": \"notify\""
#define ST_METHOD_SETBINARY_R                   "\"method\": \"setBinaryR\""
#define ST_METHOD_ACTION_COMPLETE               "\"method\": \"actionComplete\""
#define ST_ACTION_TYPE_SETBINARY                "\"actionType\": \"setBinary\""
#define ST_NOTIFY_TYPE_ALARM                    "\"notifyType\": \"alarm\""
#define ST_NOTIFY_TYPE_BINARY_STATE             "\"notifyType\": \"binaryState\""
#define ST_CMD_CLASS_ALARM                      "\"cmdClass\": \"alarm\""
#define ST_CMD_CLASS_SENSOR_ALARM               "\"cmdClass\": \"sensorAlarm\""
#define ST_CMD_CLASS_SENSOR_BINARY              "\"cmdClass\": \"sensorBinary\""
#define ST_CMD_CLASS_NOTIFICATION               "\"cmdClass\": \"notification\""
#define ST_CMD_CLASS_BARRIER_OPERATOR           "\"cmdClass\": \"barrierOperator\""
#define ST_CMD_CLASS_BASIC                      "\"cmdClass\": \"basic\""
#define ST_CMD_CLASS_SWITCH_BINARY              "\"cmdClass\": \"switchBinary\""
#define ST_CMD_CLASS_SWITCH_ALL                 "\"cmdClass\": \"switchAll\""
#define ST_CMD_CLASS_SWITCH_MULTILEVEL          "\"cmdClass\": \"switchMultilevel\""
#define ST_CMD_CLASS_DOORLOCK                   "\"cmdClass\": \"doorLock\""
#define ST_COMMAND_ON_OFF                       "\"command\": \"onOff\""
#define ST_COMMAND_DIM                          "\"command\": \"dim\""

// Alarm partern 
#define ST_SOUND_ALARM_ENABLE           "alarmEnable"
#define ST_TEMPORARY_ALARM_TIME         "alarmTemporary"
#define ST_WRONG_PIN_CODE               "wrongPinCode"
#define ST_GROUP_ALARM                  "groupAlarm"
#define ST_BATTERY                      "battery"
#define ST_BARRIER                      "barrier"
#define ST_TEMPERATURE                  "temperature"
#define ST_WATER                        "water"
#define ST_WATER_FLOW                   "waterFlow"
#define ST_SMOKE                        "smoke"
#define ST_CO                           "CO"
#define ST_DOOR_SENSOR                  "doorSensor"

#define ST_POWER_OVER_LOAD_DETECT       "overLoadDetected"
#define ST_POWER_MANAGEMENT_IDLE        "powerManagementIdle"
#define ST_BATTERY_IDLE                 "batteryIdle"
#define ST_BATTERY_WARNING              "lowBatteryWarning"
#define ST_BATTERY_REPLACE              "replaceBatteryNow"
#define ST_HARDWARE_FAILURE             "hardwareFailure"
#define ST_BARRIER_SENSOR_NOT_DETECTED  "barrierSensorNotDetected"
#define ST_BARRIER_SENSOR_BATTERY_WARNING  "barrierSensorLowBatteryWarning"
#define ST_BARRIER_ACCESS_CONTROL_IDLE  "accessControlIdle"
#define ST_BARRIER_UNATTENDED_DISABLED  "barrierUnattendedDisabled"
#define ST_BARRIER_MOTOR_EXCEEDED       "barrierMotorExceeded"
#define ST_BARRIER_OPERATION_EXCEEDED   "barrierOperationExceeded"
#define ST_BARRIER_PHYSICAL_EXCEEDED    "barrierPhysicalExceeded"
#define ST_VOLTAGE_DROP                 "voltageDrop"
#define ST_POWER_APPLY                  "powerApplied"
#define ST_TAMPER                       "tamperingProductCoverRemoved"
#define ST_TAMPER_CLEAR                 "tamperingProductCoverIdle"
#define ST_DOOR_OPEN                    "doorOpen"
#define ST_DOOR_CLOSED                  "doorClosed"
#define ST_GARA_OPEN                    "garaOpen"
#define ST_GARA_OPENING                 "garaOpening"
#define ST_GARA_CLOSED                  "garaClosed"
#define ST_GARA_CLOSING                 "garaClosing"
#define ST_MOTION_DETECTED              "motionDetected"
#define ST_MOTION_CLEARED               "motionIdle"
#define ST_MOTION_DISABLED              "motionDisabled"
#define ST_TEMPERATURE_HIGH             "overheatDetected"
#define ST_TEMPERATURE_LOW              "underheatDetected"
#define ST_TEMPERATURE_OK               "heatIdle"
#define ST_HAS_BEEN_RESET               "hasBeenReset"
#define ST_WATER_LEAKS                  "waterLeakDetected"
#define ST_WATER_CLEAR                  "waterIdle"
#define ST_WATER_FLOW_IDLE              "waterFlowAlarmNoData"
#define ST_WATER_FLOW_LOW               "waterFlowAlarmLowThreshold"
#define ST_WATER_FLOW_HIGH              "waterFlowAlarmHighThreshold"
#define ST_SMOKE_DETECTED               "smokeDetected"
#define ST_SMOKE_CLEARED                "smokeIdle"
#define ST_CO_DETECTED                  "CODetected"
#define ST_CO_CLEARED                   "COIdle"
#define ST_MAIFUNCTION                  "barrierMalfunction"
#define ST_WRONG_PIN_3TIMES             "manualTryAccessLimited"
#define ST_STUCKINLOCK                  "lockJammed"
#define ST_TEST_START                   "smokeAlarmTest"
#define ST_TEST_END                     "smokeIdle"
#define ST_WATER_VALVE_OPEN             "waterValveOpen"
#define ST_WATER_VALVE_CLOSED           "waterValveClosed"

//DeviceType
#define ST_WATER_VALVE                  "WaterValve"

#define DOOR_RF_UNLOCK_VALUE            1
#define DOOR_RF_LOCK_VALUE              2
#define DOOR_KEYPAD_UNLOCK_VALUE        3
#define DOOR_KEYPAD_LOCK_VALUE          4
#define DOOR_MANUAL_UNLOCK_VALUE        5
#define DOOR_MANUAL_LOCK_VALUE          6
#define DOOR_AUTO_LOCK_VALUE            7
#define DOOR_ONE_TOUCH_LOCK_VALUE       8

#define ST_DOOR_RF_UNLOCKED             "rfUnlock"
#define ST_DOOR_RF_LOCKED               "rfLock"
#define ST_KEYPAD_UNLOCK                "keypadUnlock"
#define ST_KEYPAD_LOCK                  "keypadLock"
#define ST_MANUAL_UNLOCK                "manualUnlock"
#define ST_MANUAL_LOCK                  "manualLock"
#define ST_KEY_UNLOCK                   "keyUnlock"
#define ST_KEY_LOCK                     "keyLock"
// #define ST_AUTO_LOCK                    "manualLockAndLeave"
#define ST_AUTO_LOCK                    "autoLocked"
#define ST_ONE_TOUCH_LOCK               "oneTouchLock"

#define ST_ADD_NEW_USER                 "userCodeAdded"
#define ST_REMOVE_SINGLE_USER           "singleUserCodeDeleted"
#define ST_DELETE_ALL_USERS             "allUserCodeDeleted"
#define ST_DUPLICATE_USER_CODE          "duplicateUserCode"

#define MAXIMUM_USER_CODE_ID_DEFAULT        40
///////////////////FIRMWARE UPDATE /////////////////
#define FIRMWARE_NAME           "openwrt-omap-ext4.img.tar.gz"
#define FIRMWARE_USER_PASS      "venus_firmware:v3nu5_f!rmw@r3"
#define PRODUCT_FIRMWARE_ADDR    "api.zinnoinc.com"
#define DEV_FIRMWARE_ADDR        "staging-api.zinnoinc.com"
#define FIRMWARE_ADDRESS        "https://fw.zinnoinc.com"
#define IS_UPDATE_LINK          "/api/products/%s/isupdated?currentVersion=%s&uuid=%s"
#define TEXT_TO_SPEECH_LINK     "/api/products/%s/t2s"  

#define CLOUD_DOMAIN                "api.zinnoinc.com"
#define CLOUD_ADDRESS               "https://%s"
#define CLOUD_REGISTER_ADDRESS      "https://%s/api/devices"

/*#####################################################################################*/
/*###########################           THERMOSTAT           #########################*/
/*#####################################################################################*/

#define ST_THERMOSTAT_SETPOINT_FORMAT "thermostatSetpointFormat"
// thermostat fan mode
#define ST_AUTO_LOW                 "autoLow"
#define ST_LOW                      "low"
#define ST_AUTO_HIGH                "autoHigh"
#define ST_HIGH                     "high"
#define ST_AUTO_MEDIUM              "autoMedium"
#define ST_MEDIUM                   "medium"
#define ST_CIRCULATION              "circulation"
#define ST_HUMI_CIRCULATION         "humiCirculation"
#define ST_LEFT_RIGHT               "leftRight"
#define ST_UP_DOWN                  "upDown"
#define ST_QUIET                    "quiet"
#define THERMOSTAT_FAN_MODE_AUTO_LOW_VALUE              0x00
#define THERMOSTAT_FAN_MODE_LOW_VALUE                   0x01
#define THERMOSTAT_FAN_MODE_AUTO_HIGH_VALUE             0x02
#define THERMOSTAT_FAN_MODE_HIGH_VALUE                  0x03
#define THERMOSTAT_FAN_MODE_AUTO_MEDIUM_VALUE           0x04
#define THERMOSTAT_FAN_MODE_MEDIUM_VALUE                0x05
#define THERMOSTAT_FAN_MODE_CIRCULATION_VALUE           0x06
#define THERMOSTAT_FAN_MODE_HUMI_CIRCULATION_VALUE      0x07
#define THERMOSTAT_FAN_MODE_LEFT_RIGHT_VALUE            0x08
#define THERMOSTAT_FAN_MODE_UP_DOWN_VALUE               0x09
#define THERMOSTAT_FAN_MODE_QUIET_VALUE                 0x0A
// thermostat fan state
#define ST_RUNNING_LOW              "runningLow"
#define ST_RUNNING_HIGH             "runningHigh"
#define ST_RUNNING_MEDIUM           "runningMedium"
#define THERMOSTAT_FAN_STATE_IDLE_VALUE                          0x00
#define THERMOSTAT_FAN_STATE_RUNNING_LOW_VALUE                   0x01
#define THERMOSTAT_FAN_STATE_RUNNING_HIGH_VALUE                  0x02
#define THERMOSTAT_FAN_STATE_RUNNING_MEDIUM_VALUE                0x03
#define THERMOSTAT_FAN_STATE_CIRCULATION_STATE_VALUE             0x04
#define THERMOSTAT_FAN_STATE_HUMI_CIRCULATION_STATE_VALUE        0x05
#define THERMOSTAT_FAN_STATE_LEFT_RIGHT_STATE_VALUE              0x06
#define THERMOSTAT_FAN_STATE_UP_DOWN_STATE_VALUE                 0x07
#define THERMOSTAT_FAN_STATE_QUIET_STATE_VALUE                   0x08
// thermostat mode
#define ST_AUTO                     "auto"
#define ST_HEAT                     "heat"
#define ST_COOL                     "cool"
#define ST_RESUME                   "resume"
#define ST_FAN_ONLY                 "fanOnly"
#define ST_FURNACE                  "furnace"
#define ST_DRY_AIR                  "dryAir"
#define ST_MOIST_AIR                "moistAir"
#define ST_AUTO_CHANGE_OVER         "autoChangeOver"
#define ST_ENERGY_SAVE_HEAT         "energySaveHeat"
#define ST_ENERGY_SAVE_COOL         "energySaveCool"
#define ST_AWAY                     "away"
#define ST_FULL_POWER               "fullPower"
#define ST_AUXILIARY                "auxiliary"

#define THERMOSTAT_MODE_OFF_VALUE                   0x00
#define THERMOSTAT_MODE_HEAT_VALUE                  0x01
#define THERMOSTAT_MODE_COOL_VALUE                  0x02
#define THERMOSTAT_MODE_AUTO_VALUE                  0x03
#define THERMOSTAT_MODE_AUXILIARY_HEAT_VALUE        0x04
#define THERMOSTAT_MODE_RESUME_VALUE                0x05
#define THERMOSTAT_MODE_FAN_ONLY_SETMODE_VALUE      0x06
#define THERMOSTAT_MODE_FURNACE_VALUE               0x07
#define THERMOSTAT_MODE_DRY_AIR_VALUE               0x08
#define THERMOSTAT_MODE_MOIST_VALUE                 0x09
#define THERMOSTAT_MODE_AUTO_CHANGE_OVER_VALUE      0x0A
#define THERMOSTAT_MODE_ENERGY_SAVE_HEAT_VALUE      0x0B
#define THERMOSTAT_MODE_ENERGY_SAVE_COOL_VALUE      0x0C
#define THERMOSTAT_MODE_AWAY_VALUE                  0x0D
#define THERMOSTAT_MODE_RESERVED_VALUE              0x0E
#define THERMOSTAT_MODE_FULL_POWER_VALUE            0x0F
#define THERMOSTAT_MODE_MANUFACTURER_SPECIFIC_VALUE 0x1F
////////// end thermostat mode /////////
// thermostat operating 
#define ST_IDLE                     "idle"
#define ST_HEATING                  "heating"
#define ST_COOLING                  "cooling"
#define ST_PENDING_HEAT             "pendingHeat"
#define ST_PENDING_COOL             "pendingCool"
#define ST_VENT_ECONOMIZER          "ventEconomizer"
#define ST_AUX_HEATING              "AuxHeating"
#define ST_2ND_STAGE_HEATING        "2ndStageHeating"
#define ST_2ND_STAGE_COOLING        "2ndStageCooling"
#define ST_2ND_STAGE_AUX_HEAT       "2ndAuxHeat"
#define ST_3RD_STAGE_AUX_HEAT       "3rdAuxHeat"
#define THERMOSTAT_OPERATING_IDLE_VALUE                  0x00
#define THERMOSTAT_OPERATING_HEATING_VALUE               0x01
#define THERMOSTAT_OPERATING_COOLING_VALUE               0x02
#define THERMOSTAT_OPERATING_FAN_ONLY_OPERATE_VALUE      0x03
#define THERMOSTAT_OPERATING_PENDING_HEAT_VALUE          0x04
#define THERMOSTAT_OPERATING_PENDING_COOL_VALUE          0x05
#define THERMOSTAT_OPERATING_VENT_ECONOMIZER_VALUE       0x06
#define THERMOSTAT_OPERATING_AUX_HEATING                 0x07
#define THERMOSTAT_OPERATING_2ND_STAGE_HEATING           0x08
#define THERMOSTAT_OPERATING_2ND_STAGE_COOLING           0x09
#define THERMOSTAT_OPERATING_2ND_STAGE_AUX_HEAT          0x0A
#define THERMOSTAT_OPERATING_3RD_STAGE_AUX_HEAT          0x0B
////// thermostat setpoint/////////
#define ST_ENERGY_SAVE_HEATING       "energySaveHeating"
#define ST_ENERGY_SAVE_COOLING       "energySaveCooling"
#define ST_AWAY_HEATING              "awayHeating"
#define ST_AWAY_COOLING              "awayCooling"
#define THERMOSTAT_SETPOINT_HEATING_VALUE               0x01
#define THERMOSTAT_SETPOINT_COOLING_VALUE               0x02
#define THERMOSTAT_SETPOINT_FUMANCE_VALUE               0x07
#define THERMOSTAT_SETPOINT_DRY_AIR_VALUE               0x08
#define THERMOSTAT_SETPOINT_MOIST_AIR_VALUE             0x09
#define THERMOSTAT_SETPOINT_AUTO_CHANGE_OVER_VALUE      0x0A
#define THERMOSTAT_SETPOINT_ENERGY_SAVE_HEATING_VALUE   0x0B
#define THERMOSTAT_SETPOINT_ENERGY_SAVE_COOLING_VALUE   0x0C
#define THERMOSTAT_SETPOINT_ALWAY_HEATING_VALUE         0x0D
#define THERMOSTAT_SETPOINT_ALWAY_COOLING_VALUE         0x0E
#define THERMOSTAT_SETPOINT_FULL_POWER_VALUE            0x0F

/*#####################################################################################*/
/*###########################        SENSOR MULTILEVEL        #########################*/
/*#####################################################################################*/
/*multilevel sensor ID*/
#define MULTILEVEL_SENSOR_RESERVED                                    0x00
#define MULTILEVEL_SENSOR_AIR_TEMPERATURE                             0x01
#define MULTILEVEL_SENSOR_GENERAL_PURPOSE                             0x02
#define MULTILEVEL_SENSOR_LUMINANCE                                   0x03
#define MULTILEVEL_SENSOR_POWER                                       0x04
#define MULTILEVEL_SENSOR_HUMIDITY                                    0x05
#define MULTILEVEL_SENSOR_VELOCITY                                    0x06
#define MULTILEVEL_SENSOR_DIRECTION                                   0x07
#define MULTILEVEL_SENSOR_ATMOSPHERIC_PRESSURE                        0x08
#define MULTILEVEL_SENSOR_BAROMETRIC_PRESSURE                         0x09
#define MULTILEVEL_SENSOR_SOLAR_RADIATION                             0x0A
#define MULTILEVEL_SENSOR_DEW_POINT                                   0x0B
#define MULTILEVEL_SENSOR_RAIN_RATE                                   0x0C
#define MULTILEVEL_SENSOR_TIDE_LEVEL                                  0x0D
#define MULTILEVEL_SENSOR_WEIGHT                                      0x0E
#define MULTILEVEL_SENSOR_VOLTAGE                                     0x0F
#define MULTILEVEL_SENSOR_CURRENT                                     0x10
#define MULTILEVEL_SENSOR_CARBON_DIOXIDE_CO2_LEVEL                    0x11
#define MULTILEVEL_SENSOR_AIR_FLOW                                    0x12
#define MULTILEVEL_SENSOR_TANK_CAPACITY                               0x13
#define MULTILEVEL_SENSOR_DISTANCE                                    0x14
#define MULTILEVEL_SENSOR_ANGLE_POSITION                              0x15
#define MULTILEVEL_SENSOR_ROTATION                                    0x16
#define MULTILEVEL_SENSOR_WATER_TEMPERATURE                           0x17
#define MULTILEVEL_SENSOR_SOIL_TEMPERATURE                            0x18
#define MULTILEVEL_SENSOR_SEISMIC_INTENSITY                           0x19
#define MULTILEVEL_SENSOR_SEISMIC_MAGNITUDE                           0x1A
#define MULTILEVEL_SENSOR_ULTRAVIOLET                                 0x1B
#define MULTILEVEL_SENSOR_ELECTRICAL_RESISTIVITY                      0x1C
#define MULTILEVEL_SENSOR_ELECTRICAL_CONDUCTIVITY                     0x1D
#define MULTILEVEL_SENSOR_LOUNDNESS                                   0x1E
#define MULTILEVEL_SENSOR_MOISTURE                                    0x1F
#define MULTILEVEL_SENSOR_FREQUENCY                                   0x20
#define MULTILEVEL_SENSOR_TIME                                        0x21
#define MULTILEVEL_SENSOR_TARGET_TEMPERATURE                          0x22
#define MULTILEVEL_SENSOR_PARTICULATE_MATTER_25                       0x23
#define MULTILEVEL_SENSOR_FORMALDEHYDE_CH2O_LEVEL                     0x24
#define MULTILEVEL_SENSOR_RADON_CONCERNTRATION                        0x25
#define MULTILEVEL_SENSOR_METHANE_DENSITY                             0x26
#define MULTILEVEL_SENSOR_VOLATILE_ORGANIC_COMPOUND_LEVEL             0x27
#define MULTILEVEL_SENSOR_CARBON_MONOXIDE_LEVEL                       0x28
#define MULTILEVEL_SENSOR_SOIL_HUMIDITY                               0x29
#define MULTILEVEL_SENSOR_SOIL_REACTIVITY                             0x2A
#define MULTILEVEL_SENSOR_SOIL_SALINITY                               0x2B
#define MULTILEVEL_SENSOR_HEART_RATE                                  0x2C
#define MULTILEVEL_SENSOR_BLOOD_PRESSURE                              0x2D
#define MULTILEVEL_SENSOR_MUSCLE_MASS                                 0x2E
#define MULTILEVEL_SENSOR_FAT_MASS                                    0x2F
#define MULTILEVEL_SENSOR_BONE_MASS                                   0x30
#define MULTILEVEL_SENSOR_TOTAL_BODY_WATER                            0x31
#define MULTILEVEL_SENSOR_BASIS_METABOLIC_RATE                        0x32
#define MULTILEVEL_SENSOR_BODY_MASS                                   0x33
#define MULTILEVEL_SENSOR_ACCELERATION_X_AXIS                         0x34
#define MULTILEVEL_SENSOR_ACCELERATION_Y_AXIS                         0x35
#define MULTILEVEL_SENSOR_ACCELERATION_Z_AXIS                         0x36
#define MULTILEVEL_SENSOR_SMOKE_DENSITY                               0x37
#define MULTILEVEL_SENSOR_WATER_FLOW                                  0x38
#define MULTILEVEL_SENSOR_WATER_PRESSURE                              0x39
#define MULTILEVEL_SENSOR_RF_SIGNAL_STRENGTH                          0x3A
#define MULTILEVEL_SENSOR_PARTICULATE_MATTER_10                       0x3B
#define MULTILEVEL_SENSOR_RESPIRATORY_RATE                            0x3C
#define MULTILEVEL_SENSOR_RELATIVE_MODULATION_LEVEL                   0x3D
#define MULTILEVEL_SENSOR_BOILER_WATER_TEMPERATURE                    0x3E
#define MULTILEVEL_SENSOR_DOMESTIC_HOT_WATER                          0x3F
#define MULTILEVEL_SENSOR_OUTSIDE_TEMPERATURE                         0x40
#define MULTILEVEL_SENSOR_EXHAUST_TEMPERATURE                         0x41

/*sensor type name*/
#define ST_MULTILEVEL_SENSOR_RESERVED                                 "unknown"
#define ST_MULTILEVEL_SENSOR_AIR_TEMPERATURE                          "airTemperature"
#define ST_MULTILEVEL_SENSOR_GENERAL_PURPOSE                          "generalPurpose"
#define ST_MULTILEVEL_SENSOR_LUMINANCE                                "luminance"
#define ST_MULTILEVEL_SENSOR_POWER                                    "power"
#define ST_MULTILEVEL_SENSOR_HUMIDITY                                 "humidity"
#define ST_MULTILEVEL_SENSOR_VELOCITY                                 "velocity"
#define ST_MULTILEVEL_SENSOR_DIRECTION                                "direction"
#define ST_MULTILEVEL_SENSOR_ATMOSPHERIC_PRESSURE                     "atmosphericPressure"
#define ST_MULTILEVEL_SENSOR_BAROMETRIC_PRESSURE                      "barometricPressure"
#define ST_MULTILEVEL_SENSOR_SOLAR_RADIATION                          "solarRadiation"
#define ST_MULTILEVEL_SENSOR_DEW_POINT                                "dewPoint"
#define ST_MULTILEVEL_SENSOR_RAIN_RATE                                "rainRate"
#define ST_MULTILEVEL_SENSOR_TIDE_LEVEL                               "tideLevel"
#define ST_MULTILEVEL_SENSOR_WEIGHT                                   "weight"
#define ST_MULTILEVEL_SENSOR_VOLTAGE                                  "voltage"
#define ST_MULTILEVEL_SENSOR_CURRENT                                  "current"
#define ST_MULTILEVEL_SENSOR_CARBON_DIOXIDE_CO2_LEVEL                 "carbonDioxideCO2level"
#define ST_MULTILEVEL_SENSOR_AIR_FLOW                                 "airFlow"
#define ST_MULTILEVEL_SENSOR_TANK_CAPACITY                            "tankCapacity"
#define ST_MULTILEVEL_SENSOR_DISTANCE                                 "distance"
#define ST_MULTILEVEL_SENSOR_ANGLE_POSITION                           "anglePosition"
#define ST_MULTILEVEL_SENSOR_ROTATION                                 "rotation"
#define ST_MULTILEVEL_SENSOR_WATER_TEMPERATURE                        "waterTemperature"
#define ST_MULTILEVEL_SENSOR_SOIL_TEMPERATURE                         "soilTemperature"
#define ST_MULTILEVEL_SENSOR_SEISMIC_INTENSITY                        "seismicIntensity"
#define ST_MULTILEVEL_SENSOR_SEISMIC_MAGNITUDE                        "seismicMagnitude"
#define ST_MULTILEVEL_SENSOR_ULTRAVIOLET                              "ultraviolet"
#define ST_MULTILEVEL_SENSOR_ELECTRICAL_RESISTIVITY                   "electricalResistivity"
#define ST_MULTILEVEL_SENSOR_ELECTRICAL_CONDUCTIVITY                  "electricalConductivity"
#define ST_MULTILEVEL_SENSOR_LOUNDNESS                                "loundness"
#define ST_MULTILEVEL_SENSOR_MOISTURE                                 "moisture"
#define ST_MULTILEVEL_SENSOR_FREQUENCY                                "frequency"
#define ST_MULTILEVEL_SENSOR_TIME                                     "time"
#define ST_MULTILEVEL_SENSOR_TARGET_TEMPERATURE                       "targetTemperature"
#define ST_MULTILEVEL_SENSOR_PARTICULATE_MATTER_25                    "particulateMatter2.5"
#define ST_MULTILEVEL_SENSOR_FORMALDEHYDE_CH2O_LEVEL                  "formaldehydeCH20level"
#define ST_MULTILEVEL_SENSOR_RADON_CONCERNTRATION                     "radonConcentration"
#define ST_MULTILEVEL_SENSOR_METHANE_DENSITY                          "methane(CH4)Density"
#define ST_MULTILEVEL_SENSOR_VOLATILE_ORGANIC_COMPOUND_LEVEL          "volatileOrganicCompoundLevel"
#define ST_MULTILEVEL_SENSOR_CARBON_MONOXIDE_LEVEL                    "carbonMonoxide(CO)Level"
#define ST_MULTILEVEL_SENSOR_SOIL_HUMIDITY                            "soilHumidity"
#define ST_MULTILEVEL_SENSOR_SOIL_REACTIVITY                          "soilReactivity"
#define ST_MULTILEVEL_SENSOR_SOIL_SALINITY                            "soilSalinity"
#define ST_MULTILEVEL_SENSOR_HEART_RATE                               "heartRate"
#define ST_MULTILEVEL_SENSOR_BLOOD_PRESSURE                           "bloodPressure"
#define ST_MULTILEVEL_SENSOR_MUSCLE_MASS                              "muscleMass"
#define ST_MULTILEVEL_SENSOR_FAT_MASS                                 "fatMass"
#define ST_MULTILEVEL_SENSOR_BONE_MASS                                "boneMass"
#define ST_MULTILEVEL_SENSOR_TOTAL_BODY_WATER                         "totalBodyWater(TBW)"
#define ST_MULTILEVEL_SENSOR_BASIS_METABOLIC_RATE                     "basisMetabolicRate(BMR)"
#define ST_MULTILEVEL_SENSOR_BODY_MASS                                "bodyMassIndex(BMI)"
#define ST_MULTILEVEL_SENSOR_ACCELERATION_X_AXIS                      "acceleration_X_Axis"
#define ST_MULTILEVEL_SENSOR_ACCELERATION_Y_AXIS                      "acceleration_Y_Axis"
#define ST_MULTILEVEL_SENSOR_ACCELERATION_Z_AXIS                      "acceleration_Z_Axis"
#define ST_MULTILEVEL_SENSOR_SMOKE_DENSITY                            "smokeDensity"
#define ST_MULTILEVEL_SENSOR_WATER_FLOW                               "waterFlow"
#define ST_MULTILEVEL_SENSOR_WATER_PRESSURE                           "waterPressure"
#define ST_MULTILEVEL_SENSOR_RF_SIGNAL_STRENGTH                       "rfsignalStrength"
#define ST_MULTILEVEL_SENSOR_PARTICULATE_MATTER_10                    "particulateMatter10"
#define ST_MULTILEVEL_SENSOR_RESPIRATORY_RATE                         "respiratoryRate"
#define ST_MULTILEVEL_SENSOR_RELATIVE_MODULATION_LEVEL                "relativeModulationLevel"
#define ST_MULTILEVEL_SENSOR_BOILER_WATER_TEMPERATURE                 "boilerWaterTemperature"
#define ST_MULTILEVEL_SENSOR_DOMESTIC_HOT_WATER                       "domesticHotWaterTemperature"
#define ST_MULTILEVEL_SENSOR_OUTSIDE_TEMPERATURE                      "outsideTemperature"
#define ST_MULTILEVEL_SENSOR_EXHAUST_TEMPERATURE                      "exhaustTemperature"

/*sensor unit*/
#define MULTILEVEL_SENSOR_LABEL_CELCIUS                               0x00
#define MULTILEVEL_SENSOR_LABEL_FAHRENHEIT                            0x01
#define MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE                      0x00
#define MULTILEVEL_SENSOR_LABEL_DIMENSIONLESS_VALUE                   0x01
#define MULTILEVEL_SENSOR_LABEL_LUX                                   0x01
#define MULTILEVEL_SENSOR_LABEL_WATT                                  0x00
#define MULTILEVEL_SENSOR_LABEL_BTU                                   0x01
#define MULTILEVEL_SENSOR_LABEL_ABSOLUTE                              0x01
#define MULTILEVEL_SENSOR_LABEL_MS                                    0x00
#define MULTILEVEL_SENSOR_LABEL_MPH                                   0x01
#define MULTILEVEL_SENSOR_LABEL_DEGREES                               0x00
#define MULTILEVEL_SENSOR_LABEL_KILOPASCAL                            0x00
#define MULTILEVEL_SENSOR_LABEL_INCHES_OF_MERCURY                     0x01
#define MULTILEVEL_SENSOR_LABEL_WATT_PER_SQUARE                       0x00
#define MULTILEVEL_SENSOR_LABEL_MILLIMETER                            0x00
#define MULTILEVEL_SENSOR_LABEL_INCHES_PER_HOUR                       0x01
#define MULTILEVEL_SENSOR_LABEL_METER                                 0x00
#define MULTILEVEL_SENSOR_LABEL_FEET                                  0x01
#define MULTILEVEL_SENSOR_LABEL_KILOGRAM                              0x00
#define MULTILEVEL_SENSOR_LABEL_POUNDS                                0x01
#define MULTILEVEL_SENSOR_LABEL_VOLT                                  0x00
#define MULTILEVEL_SENSOR_LABEL_MILLIVOLT                             0x01
#define MULTILEVEL_SENSOR_LABEL_AMPERE                                0x00
#define MULTILEVEL_SENSOR_LABEL_MILLIAMPERE                           0x01
#define MULTILEVEL_SENSOR_LABEL_PARTS_MILLION                         0x00
#define MULTILEVEL_SENSOR_LABEL_CUBIC_METER_PER_HOUR                  0x00
#define MULTILEVEL_SENSOR_LABEL_CUBIC_FEET_PER_MINUTE                 0x01
#define MULTILEVEL_SENSOR_LABEL_LITER                                 0x00
#define MULTILEVEL_SENSOR_LABEL_CUBIC_METER                           0x01
#define MULTILEVEL_SENSOR_LABEL_GALLONS                               0x02
#define MULTILEVEL_SENSOR_LABEL_CENTIMETER                            0x01
#define MULTILEVEL_SENSOR_LABEL_DISTANCE_FEET                         0x02
#define MULTILEVEL_SENSOR_LABEL_DEGREES_RELATIVE_NORTH_1              0x01
#define MULTILEVEL_SENSOR_LABEL_DEGREES_RELATIVE_NORTH_2              0x02
#define MULTILEVEL_SENSOR_LABEL_REVOLUTIONS_PER_MINUTE                0x00
#define MULTILEVEL_SENSOR_LABEL_HERTZ                                 0x01
#define MULTILEVEL_SENSOR_LABEL_MERCALLI                              0x00
#define MULTILEVEL_SENSOR_LABEL_EUROPEAN_MACROSEISMIC                 0x01
#define MULTILEVEL_SENSOR_LABEL_LIEDU                                 0x02
#define MULTILEVEL_SENSOR_LABEL_SHINDO                                0x03
#define MULTILEVEL_SENSOR_LABEL_LOCAL                                 0x00
#define MULTILEVEL_SENSOR_LABEL_MOMENT                                0x01
#define MULTILEVEL_SENSOR_LABEL_SURFACE_WAVE                          0x02
#define MULTILEVEL_SENSOR_LABEL_BODY_WAVE                             0x03
#define MULTILEVEL_SENSOR_LABEL_UV_INDEX                              0x00
#define MULTILEVEL_SENSOR_LABEL_OHM_METER                             0x00
#define MULTILEVEL_SENSOR_LABEL_SIEMENS_PER_METER                     0x00
#define MULTILEVEL_SENSOR_LABEL_DECIBEL                               0x00
#define MULTILEVEL_SENSOR_LABEL_WEIGHTED_DECIBEL                      0x01
#define MULTILEVEL_SENSOR_LABEL_VOLUME_WATER_CONTENT                  0x01
#define MULTILEVEL_SENSOR_LABEL_IMPEDANCE                             0x02
#define MULTILEVEL_SENSOR_LABEL_WATER_ACTIVITY                        0x03
#define MULTILEVEL_SENSOR_LABEL_FREQUENCY_HERTZ                       0x00
#define MULTILEVEL_SENSOR_LABEL_KILOHERTZ                             0x01
#define MULTILEVEL_SENSOR_LABEL_SECOND                                0x00
#define MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER                  0x00
#define MULTILEVEL_SENSOR_LABEL_MICROGRAM_PER_CUBIC_METER             0x01
#define MULTILEVEL_SENSOR_LABEL_BECQUEREL_PER_CUBIC_METER             0x00
#define MULTILEVEL_SENSOR_LABEL_PICOCURIES_PER_LITER                  0x01
#define MULTILEVEL_SENSOR_LABEL_ORGANIC_PARTS_MILLION                 0x01
#define MULTILEVEL_SENSOR_LABEL_ACIDITY                               0x00
#define MULTILEVEL_SENSOR_LABEL_BEATS_PER_MINUTE                      0x00
#define MULTILEVEL_SENSOR_LABEL_SYSTOLIC                              0x00
#define MULTILEVEL_SENSOR_LABEL_DIASTOLIC                             0x01
#define MULTILEVEL_SENSOR_LABEL_JOULE                                 0x00
#define MULTILEVEL_SENSOR_LABEL_BMI_INDEX                             0x00
#define MULTILEVEL_SENSOR_LABEL_METER_PER_SQUARE_SECOND               0x00
#define MULTILEVEL_SENSOR_LABEL_LITER_PER_HOUR                        0x00
#define MULTILEVEL_SENSOR_LABEL_RSSI                                  0x00
#define MULTILEVEL_SENSOR_LABEL_DBM                                   0x01
#define MULTILEVEL_SENSOR_LABEL_BREATHS_PER_MINUTE                    0x00

/*sensor unit name*/
#define ST_MULTILEVEL_SENSOR_LABEL_CELCIUS                            "celsius"
#define ST_MULTILEVEL_SENSOR_LABEL_FAHRENHEIT                         "fahrenheit"
#define ST_MULTILEVEL_SENSOR_LABEL_PERCENTAGE_VALUE                   "percentageValue(%)"
#define ST_MULTILEVEL_SENSOR_LABEL_DIMENSIONLESS_VALUE                "dimensionlessValue"
#define ST_MULTILEVEL_SENSOR_LABEL_LUX                                "lux"   
#define ST_MULTILEVEL_SENSOR_LABEL_WATT                               "watt(W)"
#define ST_MULTILEVEL_SENSOR_LABEL_BTU                                "btu/h" 
#define ST_MULTILEVEL_SENSOR_LABEL_ABSOLUTE                           "absoluteHumidity(g/m3)"
#define ST_MULTILEVEL_SENSOR_LABEL_MS                                 "m/s"
#define ST_MULTILEVEL_SENSOR_LABEL_MPH                                "mph"
#define ST_MULTILEVEL_SENSOR_LABEL_DEGREES                            "degrees"
#define ST_MULTILEVEL_SENSOR_LABEL_KILOPASCAL                         "kilipascal(kPa)"
#define ST_MULTILEVEL_SENSOR_LABEL_INCHES_OF_MERCURY                  "inchesOfMercury"
#define ST_MULTILEVEL_SENSOR_LABEL_WATT_PER_SQUARE                    "wattPerSquareMeter(W/m2)"
#define ST_MULTILEVEL_SENSOR_LABEL_MILLIMETER                         "millimeter/Hour(mm/h)"
#define ST_MULTILEVEL_SENSOR_LABEL_INCHES_PER_HOUR                    "inchesPerHour(in/h)"
#define ST_MULTILEVEL_SENSOR_LABEL_METER                              "meter(m)"
#define ST_MULTILEVEL_SENSOR_LABEL_FEET                               "feet(ft)"
#define ST_MULTILEVEL_SENSOR_LABEL_KILOGRAM                           "kilogram(kg)"
#define ST_MULTILEVEL_SENSOR_LABEL_POUNDS                             "pounds(lb)"
#define ST_MULTILEVEL_SENSOR_LABEL_VOLT                               "Volt(V)"
#define ST_MULTILEVEL_SENSOR_LABEL_MILLIVOLT                          "milliVolt(mV)"
#define ST_MULTILEVEL_SENSOR_LABEL_AMPERE                             "ampere(A)"
#define ST_MULTILEVEL_SENSOR_LABEL_MILLIAMPERE                        "milliampere(mA)"
#define ST_MULTILEVEL_SENSOR_LABEL_PARTS_MILLION                      "parts/million(ppm)"
#define ST_MULTILEVEL_SENSOR_LABEL_CUBIC_METER_PER_HOUR               "cubicMeterPerHour(m3/n)"
#define ST_MULTILEVEL_SENSOR_LABEL_CUBIC_FEET_PER_MINUTE              "cubicFeetPerMinute(cfm)"
#define ST_MULTILEVEL_SENSOR_LABEL_LITER                              "liter(I)"
#define ST_MULTILEVEL_SENSOR_LABEL_CUBIC_METER                        "cubicMeter(m3)"
#define ST_MULTILEVEL_SENSOR_LABEL_GALLONS                            "gallons"
#define ST_MULTILEVEL_SENSOR_LABEL_CENTIMETER                         "centimeter(cm)"
#define ST_MULTILEVEL_SENSOR_LABEL_DEGREES_RELATIVE_NORTH             "degrees"
#define ST_MULTILEVEL_SENSOR_LABEL_REVOLUTIONS_PER_MINUTE             "revolutionsPerMinute(rpm)"
#define ST_MULTILEVEL_SENSOR_LABEL_HERTZ                              "hertz(Hz)"
#define ST_MULTILEVEL_SENSOR_LABEL_MERCALLI                           "mercalli"
#define ST_MULTILEVEL_SENSOR_LABEL_EUROPEAN_MACROSEISMIC              "europeanMacroseismic"
#define ST_MULTILEVEL_SENSOR_LABEL_LIEDU                              "liedu"
#define ST_MULTILEVEL_SENSOR_LABEL_SHINDO                             "shindo"
#define ST_MULTILEVEL_SENSOR_LABEL_LOCAL                              "local"
#define ST_MULTILEVEL_SENSOR_LABEL_MOMENT                             "moment"
#define ST_MULTILEVEL_SENSOR_LABEL_SURFACE_WAVE                       "surfaceWave"
#define ST_MULTILEVEL_SENSOR_LABEL_BODY_WAVE                          "bodyWave"
#define ST_MULTILEVEL_SENSOR_LABEL_UV_INDEX                           "uv"
#define ST_MULTILEVEL_SENSOR_LABEL_OHM_METER                          "ohmMeter(Ohm)"
#define ST_MULTILEVEL_SENSOR_LABEL_SIEMENS_PER_METER                  "siemensPerMeter(S/m)"
#define ST_MULTILEVEL_SENSOR_LABEL_DECIBEL                            "decibel(dB)"
#define ST_MULTILEVEL_SENSOR_LABEL_WEIGHTED_DECIBEL                   "A-weightedDecibel(dBA)"
#define ST_MULTILEVEL_SENSOR_LABEL_VOLUME_WATER_CONTENT               "volumeWaterContent(m3/m3)"
#define ST_MULTILEVEL_SENSOR_LABEL_IMPEDANCE                          "impedance(kOhm)"
#define ST_MULTILEVEL_SENSOR_LABEL_WATER_ACTIVITY                     "waterActivity(aw)"
#define ST_MULTILEVEL_SENSOR_LABEL_KILOHERTZ                          "kilohertz(kHz)"
#define ST_MULTILEVEL_SENSOR_LABEL_SECOND                             "second(s)"
#define ST_MULTILEVEL_SENSOR_LABEL_MOLE_PER_CUBIC_METER               "molePerCubicMeter(mol/m3)"
#define ST_MULTILEVEL_SENSOR_LABEL_MICROGRAM_PER_CUBIC_METER          "microgramPerCubicMeter(ug/m3)"
#define ST_MULTILEVEL_SENSOR_LABEL_BECQUEREL_PER_CUBIC_METER          "becquerelPerCubicMeter(bq/m3)"
#define ST_MULTILEVEL_SENSOR_LABEL_PICOCURIES_PER_LITER               "picocuriesPerLiter(pCI/l)"
#define ST_MULTILEVEL_SENSOR_LABEL_ACIDITY                            "acidity(pH)"
#define ST_MULTILEVEL_SENSOR_LABEL_BEATS_PER_MINUTE                   "beatsPerMinute(bpm)"
#define ST_MULTILEVEL_SENSOR_LABEL_SYSTOLIC                           "systolic(mmHg)"
#define ST_MULTILEVEL_SENSOR_LABEL_DIASTOLIC                          "diastolic(mmHg)"
#define ST_MULTILEVEL_SENSOR_LABEL_JOULE                              "joule(J)"
#define ST_MULTILEVEL_SENSOR_LABEL_BMI_INDEX                          "BMIindex"
#define ST_MULTILEVEL_SENSOR_LABEL_METER_PER_SQUARE_SECOND            "meterPerSquareSecond(m/s2)"
#define ST_MULTILEVEL_SENSOR_LABEL_LITER_PER_HOUR                     "literPerHour(l/h)"
#define ST_MULTILEVEL_SENSOR_LABEL_RSSI                               "RSSI(%)"
#define ST_MULTILEVEL_SENSOR_LABEL_DBM                                "dBm"
#define ST_MULTILEVEL_SENSOR_LABEL_BREATHS_PER_MINUTE                 "breathsPerMinute(bpm)"

/*#####################################################################################*/
/*###########################               METER             #########################*/
/*#####################################################################################*/

//         //////// zwave define /////
// #define ST_ENERGY               "energy"
#define ST_POWER                "power"
#define ST_ELECTRIC                 "electric"
// #define ST_VERSION_UPCASE            "version"

#define ST_RECEIVED                 "received"
#define ST_METER_SUPPORT            "meterSupport"
#define ST_CELSIUS                  "celsius"
#define ST_FAHRENHEIT               "fahrenheit"
// // #define ST_CURRENT_STATE         "currentState"
// #define ST_PARAMETER                "parameter"
// #define ST_MAXNODE                  "maxnode"
#define ST_NODE_FOLLOW              "nodefollow"
#define ST_UNIT                     "unit"
#define ST_PERCENTAGE               "percentage"
#define ST_TEMP                     "temp"
/*meter ID*/
#define ELECTRIC_METER                  0x01
#define GAS_METER                       0x02
#define WATER_METER                     0x03
#define HEATING_METER                   0x04
#define COOLING_METER                   0x05
/*meter type*/
#define ST_ELECTRIC_METER                  "electric"
#define ST_GAS_METER                       "gas"
#define ST_WATER_METER                     "water"
#define ST_HEATING_METER                   "heating"
#define ST_COOLING_METER                   "cooling"

/*meter unit ID*/
#define METER_KWH                       0x00
#define METER_KVAH                      0x01
#define METER_W                         0x02
#define METER_PULSE_COUNT               0x03
#define METER_V                         0x04
#define METER_A                         0x05
#define METER_POWER_FACTOR              0x06
#define METER_MST                       0x07
#define METER_KVAR                      0x08
#define METER_KVARH                     0x09
#define METER_CUBIC_METERS              0x00
#define METER_CUBIC_FEET                0x01
#define METER_US_GALLONS                0x02

/*meter unit name*/
#define ST_METER_KWH                    "kWh"
#define ST_METER_KVAH                   "kVAh"
#define ST_METER_W                      "W"
#define ST_METER_PULSE_COUNT            "pulseCount"
#define ST_METER_V                      "V"
#define ST_METER_A                      "A"
#define ST_METER_POWER_FACTOR           "powerFactor"
#define ST_METER_KVAR                   "kVar"
#define ST_METER_KVARH                  "kVarh"
#define ST_METER_CUBIC_METERS           "cubicMeters"
#define ST_METER_CUBIC_FEET             "cubicFeet"
#define ST_METER_US_GALLONS             "USgallons"

// Supported Key Attributes
#define ST_KEY_PRESSED_1_TIME       "keyPressed1Time"
#define ST_KEY_RELEASED             "keyReleased"
#define ST_KEY_HELD_DOWN            "keyHeldDown"
#define ST_KEY_PRESSED_2_TIME       "keyPressed2Time"
#define ST_KEY_PRESSED_3_TIME       "keyPressed3Time"
#define ST_KEY_PRESSED_4_TIME       "keyPressed4Time"
#define ST_KEY_PRESSED_5_TIME       "keyPressed5Time"



#define SUPPORTED_KEY_ATTRIBUTES_PRESS_1_TIME       0x00
#define SUPPORTED_KEY_ATTRIBUTES_RELEASED           0x01
#define SUPPORTED_KEY_ATTRIBUTES_HELD_DOWN          0x02
#define SUPPORTED_KEY_ATTRIBUTES_PRESS_2_TIME       0x03
#define SUPPORTED_KEY_ATTRIBUTES_PRESS_3_TIME       0x04
#define SUPPORTED_KEY_ATTRIBUTES_PRESS_4_TIME       0x05
#define SUPPORTED_KEY_ATTRIBUTES_PRESS_5_TIME       0x06
/*#####################################################################################*/

#define ST_NORMAL_POWER         "normalPower"
#define ST_OPEN                 "open"
#define ST_CLOSE                "close"
#define ST_NUMBER_USER          "numberUser"
#define ST_DISABLE              "disable"
#define ST_WAIT_NOTIFY          "waitNotify"
#define ST_SENSOR               "sensor"
#define ST_REMOVE_ALL           "removeAll"
#define ST_PERCENT              "percent"
#define ST_DOOR_STATE           "doorState"
#define ST_SET_PINCODE          "setPincode"
#define ST_GET_PINCODE          "getPincode"
#define ST_USER_STATUS          "userStatus"
#define ST_USER_TYPE            "userType"
#define ST_REMOVE_PINCODE       "removePincode"
#define ST_REMOVE_ALL_PINCODE   "removeAllPincode"
#define ST_CLOSE_DOOR           "closeDoor"
#define ST_OPEN_DOOR            "openDoor"
#define ST_GET_USER_STATUS      "getUserStatus"
#define ST_AVAILABLE            "available"
#define ST_OCCUPIED_ENABLE      "occupiedEnable"
#define ST_OCCUPIED_DISABLE     "occupiedDisable"
#define ST_RESERVED             "reserved"
#define ST_UNKNOWN              "unknown"
#define ST_ACCELEROMETER_DATA   "accelerometerData"
#define ST_AXIS_X               "axisX"
#define ST_AXIS_Y               "axisY"
#define ST_AXIS_Z               "axisZ"
#define ST_TEMP_BATTERY         "tempBattery"
#define ST_PASSCODE             "passcode"
#define ST_DEVICE_LEAVE         "deviceLeave"
#define ST_LOCK_STATUS          "lockStatus"
#define ST_BARRIER_STATUS       "barrierStatus"

#define ST_ON_VALUE             "1"
#define ST_OFF_VALUE            "0"
#define ST_TEMP_VALUE           "2"
#define ST_DISABLE_THIS_TIME    "3"

#define ST_CLOSE_VALUE          "0"
#define ST_OPEN_VALUE           "1"
#define ST_OPENING_VALUE        "2"
#define ST_CLOSING_VALUE        "3"

#define ST_CONFIG_NAME          "configName"
#define ST_CONFIG_ID            "configId"
#define ST_CONFIG_LENGTH        "configLength"

// LEDs state access point
#define ST_AP_mode                                      "AP_mode" 
#define ST_AP_config_mode                               "AP_config_mode" 
#define ST_AP_switch_mode                               "AP_switch_mode"
#define ST_STA_connecting                               "STA_connecting" 
#define ST_STA_connected_unsecure_not_internet          "STA_connected_unsecure_not_internet" 
#define ST_STA_connected_unsecure_has_internet          "STA_connected_unsecure_has_internet" 
#define ST_STA_connected_secure_not_internet            "STA_connected_secure_not_internet" 
#define ST_STA_connected_secure_has_internet            "STA_connected_secure_has_internet" 
#define ST_Firmware_updating                            "Firmware_updating" 
#define ST_Firmware_updated                             "Firmware_updated" 
#define ST_Received_secure_command                      "Received_secure_command" 
#define ST_Done_secure_command                          "Done_secure_command" 
#define ST_Open_network                                 "Open_network" 
#define ST_Close_network                                "Close_network"
#define ST_Error                                        "Error"
#define ST_Builder_Default                              "Builder_Default"
#define ST_Default                                      "Default"
#define ST_Reset                                        "Reset"
#define ST_Stop_reset                                   "Stop_reset"
#define ST_Switch_AP_mode                               "Switch_AP_mode"
#define ST_Stop_switch_AP_mode                          "Stop_switch_AP_mode"
#define ST_Reset_done                                   "Reset_done"

//// for uppdate supported database
#define ST_UPDATE_SUPPORTED_DB                          "updateSupportedDb"
#define ST_NETWORK_STILL_OPEN                           "networkIsOpen"
#define ST_IP_ADDRESS                                   "ipAddress"
#define ST_MAC_ADDRESS                                  "macAddress"
#define ST_BSSID                                        "BSSID"
#define ST_BSSID_LOWER_CASE                             "bssid"

////
#define ST_DEVICE_MODE                                  "deviceMode"
#define ST_DEVICE_NAME                                  "deviceName"
#define ST_CHILDREN_ID                                  "childrenId"
#define ST_YES                                          "yes"
#define ST_NO                                           "no"
#define ST_SCHEME                                       "scheme"
#define ST_CAP_LIST                                     "capList"
#define ST_IN_OUT                                       "inOut"
#define ST_OPTION                                       "option"
#define ST_ENDPOINT_MEM                                 "endpointMem"
#define ST_PRIORITY                                     "priority"

/// support styx ///
#define ST_PING                                         "ping"
#define ST_STORE                                        "store"
#define ST_STORE_R                                      "store_R"
#define ST_PLUTO                                        "pluto"
#define ST_START                                        "start"


#define ST_HEALTH_TEST_START                            "healthTestStart"
#define ST_HEALTH_TEST_STOP                             "healthTestStop"
#define ST_MAINTENANCE_START                            "maintenanceStart"
#define ST_MAINTENANCE_STOP                             "maintenanceStop"
#define ST_REDISCOVERY_START                            "rediscoveryStart"
#define ST_REDISCOVERY_STOP                             "rediscoveryStop"
#define ST_DUMP_NEIGHBORS                               "dumpNeighbors"
#define ST_RSSI_MAP                                     "rssiMap"
#define ST_PING_ALL_NODE                                "pingAllNodes"
#define ST_PING_ALL_NODE_START                          "pingAllNodesStart"
#define ST_PING_ALL_NODE_STOP                           "pingAllNodesStop"
#define ST_NETWORK_HEALTH                               "networkHealth"
#define ST_STOP                                         "stop"
#define ST_HUB_UUID                                     "hubUUID"
#define ST_TRIGGER                                      "trigger"
#define ST_SEND_TRIGGER                                 "sendTrigger"

// define for database///
#define ST_DATA                                         "data"
#define ST_FEATURE_ID                                   "featureId"
#define ST_REGISTER                                     "register"
/////////ASSOCIATION SUPPORT/////////////
#define ST_ASSOCIATION_MAX_GROUP_SUPPORT                "associationMaxGroupSupport"
#define ST_MAX_NODE_SUPPORT                             "maxNodeSupport"
#define ST_CLASS_SUPPORT                                "classSupport"
#define ST_NODE_LIST                                    "nodeList"
#define ST_NEIGHBORS                                    "neighbors"
#define ST_TEST_COUNT                                   "testCount"
#define ST_PER                                          "PER"
#define ST_ROUTE_CHANGE                                 "routeChange"
#define ST_ROUTE_COUNT                                  "routeCount"
#define ST_ROUTES                                       "routes"
#define ST_ROUTE_CONFIG                                 "routeConfig"
#define ST_40K                                          "40K"
#define ST_100K                                         "100K"
#define ST_9600                                         "9.6K"
#define ST_NETWORK_HEALTH_VALUE                         "networkHealthValue"
#define ST_NETWORK_HEALTH_SYMBOL                        "networkHealthSymbol"

#define ST_FAILED_NODE_LIST                             "failedNodeList"
#define ST_ZW_GROUP_ID                                  "group_id"
#define ST_ZW_NODE_LIST                                 "node_list"
#define ST_THERMOSTAT_MODE_SUPPORT                      "thermostatModeSupport"
#define ST_THERMOSTAT_SETPOINT_SUPPORT                  "thermostatSetpointSupport"
#define ST_SENSOR_SUPPORT                               "sensorSupport"
#define ST_SENSOR_UNIT_SUPPORT                          "sensorUnitSupport"
////////////////// ZWAVE ADDING GET DEVICE INFOR PROCESS ///////////////
#define ST_UNEXPECTED                                   "unexpected"
#define ST_SYNC_ACTION                                  "sync_action"
#define ST_UPDATE_STATUS                                "updateStatus"
#define ST_SIGNAL                                       "signal"
#define GET_BINARY                          4001
#define SET_BINARY                          4002
#define GET_BATTERY                         4003
#define GET_TEMP                            4004
#define GET_HUMIDITY                        4005
#define GET_LUMINANCE                       4006
#define GET_UV                              4007
#define GET_METER                           4008
#define GET_THERMOSTAT_MODE                 4009
#define GET_THERMOSTAT_SET_POINT            4010
#define GET_THERMOSTAT_OPERATING            4011
#define GET_THERMOSTAT_FAN_MODE             4012
#define GET_THERMOSTAT_FAN_OPERATING        4013
#define GET_NAME                            4014
#define CLEAN_USERCODE                      4027
#define GET_STATUS_DIM_OFF                  4028
#define GET_CHILDREN_STATUS                 4029
//////////////////////////////////////////////////////////////
#define USER_CODE_ERROR         0xFE
#define USER_CODE_RESERVED      0x02
#define USER_CODE_AVAILABLE     0x00
#define USER_CODE_SUCCESS       0x01
////////////////////////////////////////////////////////////////
#define ST_PASS                                         "pass"
#define ST_METER_UNIT                                   "meterUnit"
#define ST_SENSOR_UNIT                                  "sensorUnit"
////////////////////// SCENES //////////////////////////////////
#define ST_TIME_ACTIVE                                  "timeActive"
#define ST_BUTTON_ID                                    "buttonId"
#define ST_SCENE_ID                                     "sceneId"
#define ST_SUPPORTED_SCENES                             "supportedScenes"
#define ST_SLOW_REFRESH                                 "slowRefresh"
#define ST_SUPPORTED_KEY_ATTRIBUTES                     "supportedKeyAttributes"
#define ST_SUPPORTED_KEY_ATTRIBUTES_FOR_SCENE           "supportedKeyAttributesForScene"
#define ST_KEY_ATTRIBUTES                               "keyAttributes"
#define ST_SCENE_NUMBER                                 "sceneNumber"
#define ST_KEY_ATTRIBUTES_SUPPORTED                     "keyAttributesSupported"

////////////////////////////////////////////////////////////////
/*Zigbee Gecko 3.0*/
#define ST_DOOR_LOCK_OPERATION_EVENT_NOTIFICATION   "doorLockOperationEventNotification"
#define ST_OPERATION_EVENT_SOURCE                   "operationEventSource"
#define ST_OPERATION_EVENT_CODE                     "operationEventCode"
#define ST_PIN                                      "pin"
#define ST_ZIGBEE_LOCAL_TIME                        "zigbeeLocalTime"
#define ST_LENGTH_DATA                              "lengthData"
#define ST_REPORT_ATTR                              "reportAttr"
#define ST_PAN_SCAN                                 "panScan"
#define ST_INTER_PAN_INDENTIFY                      "interPanIndentify"
#define ST_INTER_PAN_RESET_DEV                      "interPanResetDev"
#define ST_CHANGE_CHANNEL                           "changeChannel"
#define ST_CHANNEL_SCAN                             "channelScan"
#define ST_CONTROLLER_CHANNEL                       "controllerChannel"
#define ST_TOUCHLINK_SCAN_RESPONSE                  "touchlinkScanResponse"
#define ST_DES_ADDRESS_64                           "desAddress64"
#define ST_CHANNEL_SCAN_LIST                        "channelScanList"
#define ST_PAN_ID                                   "panId"
#define ST_SCAN_REQUEST_AND_IDENTIFY                "scanRequestAndIdentify"
#define ST_SCAN_REQUEST_AND_RESET                   "scanRequestAndReset"
#define ST_WAITING_CHANNEL_SCAN_COMPLETE            "waitingChannelScanComplete"
#define DOOR_LOCK_DEV_TYPE                          "000A"
#define DOOR_LOCK_CAP_ID                            "0101"
#define ON_OFF_CAP_ID                               "0006"
#define DIM_CAP_ID                                  "0008"
#define SWITH_COLOR_CAP_ID                          "0300"
#define IDENTIFY_CAP_ID                             "0003"
#define OCCUPANCY_SENSING_CAP_ID                    "0406"
#define MS_OCCUPANCY_SENSING_CAP_ID                 "FC46"
#define BATTERY_PERCENT_CAP_ID                      "0001"
#define MULTILEVEL_SENSOR_LUMINANCE_CAP_ID          "0400"
#define MULTILEVEL_SENSOR_AIR_TEMP_CAP_ID           "0402"
#define MULTILEVEL_SENSOR_HUMIDITY_CAP_ID           "0405"
#define MS_MULTILEVEL_SENSOR_HUMIDITY_CAP_ID        "FC45"
#define ELECTRICAL_MEASUREMENT_CAP_ID               "0B04"
#define WINDOW_COVERING_CAP_ID                      "0102"
#define ST_EMBER_DELIVERY_FAILED                    "emberDeliveryFailed"
#define ST_BATTERY_VOLTAGE_CONFIGURATION            "batteryVoltageConfiguration"
#define ST_CONTROLLER_EUI64_ADDRESS                 "controllerEui64Address"
#define ST_IN_CLUSTER_LIST                          "inClusterList"
#define ST_OUT_CLUSTER_LIST                         "outClusterList"
#define ST_ZIGBEE_LIGHT_LINK                        "zigbeeLightLink"
#define ST_ZIGBEE_LIGHT_LINK_R                      "zigbeeLightLinkR"
#define ST_DISCOVER                                 "discover"
#define ST_DISCOVER_COMPLETE                        "discoverComplete"
#define ST_CLUSTER_ID                               "clusterId"
#define ST_NO_ATTRIBUTE_INFO                        "noAttributeInfo"
#define ST_ATTRIBUTE_ID                             "attributeId"
#define ST_DATA_TYPE                                "dataType"
#define ST_ATTRIBUTE_INFO_LIST                      "attributeInfoList"
#define ST_REPORT_ATTRIBUTE                         "reportAttribute"
#define ST_REAL_RMS_CURRENT                         "realRMSCurrent"
#define ST_RMS_VOLTAGE                              "rMSVoltage"
#define ST_ACTIVE_POWER                             "activePower"
#define ST_REAL_ACTIVE_POWER                        "realActivePower"
#define ST_READ_ATTRIBUTE_ID                        "readAttributeId"
#define ST_CONFIG_ATTRIBUTE_REPORT                  "configReportInterval"
#define ST_SENSOR_SCALE                             "sensorScale"
#define ST_CHANNEL_REPORT                           "channelReport"
#define ZONE_STATUS_ALARM1                          "0001"
#define ZONE_STATUS_NO_ALARM1                       "0000"
#define ZONE_STATUS_ALARM2                          "0101"
#define ZONE_STATUS_NO_ALARM2                       "0100"
#define ZONE_STATUS_TAMPER                          "0201"
#define ZONE_STATUS_NO_TAMPER                       "0200"
#define ZONE_STATUS_LOW_BATTERY                     "0301"
#define ZONE_STATUS_BATTERY_OK                      "0300"
#define ST_SHOW_UP                                  "showUp"
#define ST_INTERVAL                                 "interval"
#define ST_CONFIG_REPORT_ATTRIBUTE                  "configReportAttribute"
#define ST_SET_PIN_CODE_RES_STATUS                  "setPinCodeResStatus"
#define ST_START_LEVEL_CHANGE_UP_DOWN               "up_down"
#define ST_WRITE_ATTRIBUTE                          "writeAttribute"

//-----------meter scale----------------------------------
#define ST_METER_SCALE                              "meterScale"
#define ST_AC_VOLTAGE_MULTIPLIER                    "ACVoltageMultiplier"
#define ST_AC_VOLTAGE_DIVISOR                       "ACVoltageDivisor"
#define ST_AC_CURRENT_MULTIPLIER                    "ACCurrentMultiplier"
#define ST_AC_CURRENT_DIVISOR                       "ACCurrentDivisor"
#define ST_AC_POWER_MULTIPLIER                      "ACPowerMultiplier"
#define ST_AC_POWER_DIVISOR                         "ACPowerDivisor"

#define ST_CLEAR_ALL                                "2"
#define ALARM_ZIGBEE_DEVICE                         1
#define ALARM_ZWAVE_DEVICE                          2

#endif
#ifdef __cplusplus
} /* extern "C" */
#endif
