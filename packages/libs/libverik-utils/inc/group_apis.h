#ifdef __cplusplus
extern "C"{
#endif 

#ifndef _GROUP_APIS_H_
#define _GROUP_APIS_H_

#include "VR_define.h"

#define GROUP_CREATE_DOMAIN   "https://%s/api/devices/%s/genrg"
#define GROUP_MODIFY_DOMAIN   "https://%s/api/resourcegroups/%s"
#define GROUP_NAME_DUPPLICATE   2
#define GROUP_NAME_DUPPLICATE_CODE   "ER_DUP_ENTRY"
#define GROUP_ETAG_ADDR   	  "security.@etag[0].group"

typedef int (*group_callback)(void *, char *, void*, int);

typedef struct _group_info_t 
{
    char *groupId;
    char *groupName;
    char *groupDev;
    char *userId;
	char *appId;
} group_info;

// void update_device_groupid(char *devicetype, sqlite3 *db, db_callback callback, char *deviceid, char *groupid);
// void remove_device_groupid(char *devicetype, sqlite3 *db, char *deviceid);
void group_control_process(json_object * jobj, void* data, group_callback callback);

int remove_and_update_device_in_group(char *service, sqlite3 *db, db_callback callback, 
                                    char *g_shm, char *deviceid, char *devicetype, 
                                    char *newdeviceid);

int VR_(cloud_delete_group)(char *shm, char *group_id);
int VR_(cloud_modify_group)(char *shm, char *group_id, char *group_name, char *group_dev);
int VR_(cloud_create_group)(char *shm, char *group_name, char *group_dev, char *id_return, size_t id_length);

int cloud_request_delete_group(char *group_id);
void VR_(update_group_Etag)();
int VR_(check_group_dev_valid)(char *groupDev);
#endif

#ifdef __cplusplus
}
#endif