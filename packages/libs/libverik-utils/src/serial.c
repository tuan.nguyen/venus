#include "serial.h"

int set_interface_attribs(int fd, int speed, int parity,uint8_t stop_bit)
{
    struct termios tty;
    memset (&tty, 0, sizeof tty);
    if (tcgetattr (fd, &tty) != 0)
    {
        printf("ERROR %d from tcgetattr\n", errno);
        return -1;
    }

    cfsetospeed (&tty, speed);
    cfsetispeed (&tty, speed);

    tcflush(fd, TCIOFLUSH); //FLUSH IO

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;                     // 8-bit
    tty.c_iflag &= ~IGNBRK;                                                 // disable break processing
    tty.c_iflag &= ~(INLCR | ICRNL);
    tty.c_lflag = 0;                                                        // no signaling chars, no echo,
    // no canonical processing
    tty.c_oflag = 0;                                                        // no remapping, no delays
    tty.c_cc[VMIN]  = 0;                                                    // read doesn't block
    tty.c_cc[VTIME] = 5;                                                    // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY);                             // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);                                        // ignore modem controls,
    // enable reading
    tty.c_cflag &= ~(PARENB | PARODD);                                  // shut off parity
    tty.c_cflag |= parity;
    if (stop_bit==1)
    {
        tty.c_cflag &=~ CSTOPB;
    }else
    {
        tty.c_cflag |= CSTOPB;
    }
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr (fd, TCSANOW, &tty) != 0)
    {
        printf  ("ERROR %d from tcsetattr\n", errno);
        return -1;
    }
    return fd;
}


int putchar_t(int fd, uint8_t c)
{
    char buf[2];
    buf[0] = c;
    return  write(fd, buf, 1);
}


int putbuffer_t(int fd, uint8_t *buff, int len)
{
    return write(fd, buff, len);

}

int getchar_t(int fd, uint8_t *c)
{
    uint8_t buff[1];
    int rc = read(fd, buff, 1);
    *c = buff[0];
    return rc;
}

int getbuffer_t(int fd, uint8_t *buff, int len)
{
    int numRead=0;
    int response_length = 0;
    int retry=10;
    //int i;
    while (retry-->0)
    {
        numRead=read(fd,&buff[response_length],1);
        if (numRead<=0)
        {
            usleep(1000);
            retry--;
        }else
        {
            response_length = response_length + numRead;
        }
        if (response_length==len) break;
    }
    return response_length;

}

int free_uart(int fd)
{
    return close(fd);
}

ssize_t readln(int fd, void *buffer, size_t n)
{
    ssize_t numRead;
    size_t totRead;
    char *buf;
    char ch;
    if (n <= 0 || buffer == NULL) {
        return -1;
    }
    buf = buffer;//Allow further pointer arithmetic
    totRead = 0;
    for (;;) {
        numRead = read(fd, &ch, 1);
        if (numRead <= 0) { //Error or EOF
            break;
        } else { //One character read
/*            if (ch == '\n') {
                break;
            }*/
            if (totRead < n - 1) { //Discard > (n - 1) bytes
                totRead++;
                *buf++ = ch;
            }
        }
    }
    *buf = '\0';
    return totRead;
}

