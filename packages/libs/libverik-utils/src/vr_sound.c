#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdint.h>
#include <json-c/json.h>
#include <curl/curl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <libubox/blobmsg_json.h>
#include <mpg123.h>

#include "verik_utils.h"
#include "vr_sound.h"
#include "vr_rest.h"
#include "slog.h"

#define MAXIMUM_PATH SIZE_512B

#define SET_VOLUME_COMMAND              "amixer --quiet set Headphone %d"
#define UPDATE_ALSA_STATE_COMMAND       "alsactl store --file /etc/asound.state"
#define GET_VOLUME_COMMAND              "amixer get Headphone | grep 'Right:' | awk -F' ' '{print $3}'"

#define ST_TEXT "text"
#define ST_LINK "link"
#define ST_DEFAULT_NEW_NAME "new_name"

#define MAXIMUM_MPG123_PROCESS 1
#define TEMPORARY_DISABLE_TIME 180 //seconds
#define DELAY_PLAY             500000 //mirco seconds

#define FOLDER_SOUND_STORED         "/etc/devVoiceInform/" //etc/devVoiceInform/deviceId
#define DEFAULT_NAME_FOLDER         "/etc/sound/default_names/"
#define DEFAULT_TEMPERATURE_FOLDER  "/etc/sound/temperature/"
#define SYSTEM_SOUND_FOLDER         "/etc/sound/system/"
#define ALARM_SOUND_FOLDER          SYSTEM_SOUND_FOLDER"alarm_event/"

#define DEVICE_NAME_CHARACTOR       'n'
#define SYSTEM_DEFAULT_CHARACTOR    'o'
#define TEMPERATURE_NAME_CHARACTOR  't'
#define UNIT_NAME_CHARACTOR         'u'

/*ALARM*/
#define SOUND_BEEP                      ALARM_SOUND_FOLDER"beep"
#define SOUND_BEEP_2                    SOUND_BEEP" "SOUND_BEEP
#define SOUND_BEEP_3                    SOUND_BEEP" "SOUND_BEEP" "SOUND_BEEP

#define SOUND_IS_OPENED                 ALARM_SOUND_FOLDER"isOpened"
#define SOUND_IS_OPENING                ALARM_SOUND_FOLDER"isOpening"
#define SOUND_IS_CLOSED                 ALARM_SOUND_FOLDER"isClosed"
#define SOUND_IS_CLOSING                ALARM_SOUND_FOLDER"isClosing"
#define SOUND_IS_UNLOCKED               ALARM_SOUND_FOLDER"isUnlocked"
#define SOUND_IS_LOCKED                 ALARM_SOUND_FOLDER"isLocked"
#define SOUND_IS_STUCK                  ALARM_SOUND_FOLDER"isStuck"
#define SOUND_IS_DETECTED               ALARM_SOUND_FOLDER"isDetected"
#define SOUND_IS_MALFUNCTION            ALARM_SOUND_FOLDER"isMalfunction"
#define SOUND_IS_HIGH                   ALARM_SOUND_FOLDER"isHigh"

#define SOUND_DETECTS_WATER             ALARM_SOUND_FOLDER"detectsWater"
#define SOUND_CO_FROM                   ALARM_SOUND_FOLDER"COfrom"
#define SOUND_SMOKE_FROM                ALARM_SOUND_FOLDER"smokeFrom"
#define SOUND_MOTION_FROM               ALARM_SOUND_FOLDER"motionFrom"
#define SOUND_WRONG_USER_CODE           ALARM_SOUND_FOLDER"wrongUserCode"
#define SOUND_CHECK_AND_TRY             ALARM_SOUND_FOLDER"checkAndTry"
#define SOUND_PLEASE_EXIT               ALARM_SOUND_FOLDER"pleaseExit"
#define SOUND_HAS_BEEN_RESET            ALARM_SOUND_FOLDER"hasBeenReset"
#define SOUND_WATER_FLOW_FROM           ALARM_SOUND_FOLDER"waterFlowFrom"
#define TEMPERATURE_IS_ABOVE_SOUND      ALARM_SOUND_FOLDER"TemperatureIsAbove"
#define TEMPERATURE_IS_BELOW_SOUND      ALARM_SOUND_FOLDER"TemperatureIsBelow"

/*ADDING INFORM*/
#define SOUND_ADDING                    SYSTEM_SOUND_FOLDER"adding"
#define SOUND_SUCCESS                   SYSTEM_SOUND_FOLDER"success"

/*OLD SYSTEM*/
#define FOUND_DEVICE_SOUND              "/etc/sound/system/found_device"
#define ADDING_DEVICE_SOUND             "/etc/sound/system/The_device_is_being_added"
#define NOT_SUPPORT_SOUND               "/etc/sound/system/This_device_is_not_supported"
#define TRY_AGAIN_SOUND                 "/etc/sound/system/Try_to_add_again"
// #define NETWORK_IS_OPENING_SOUND        "/etc/sound/system/Network_Is_Opening"
// #define HELLO_SOUND                     "/etc/sound/system/Hello"
#define FIRST_TIME_BOOTUP_SOUND         "/etc/sound/system/First_Time_Bootup"
#define RECEIVED_WIFI_CONFIG_SOUND      "/etc/sound/system/Received_Wifi_Config"
#define WIFI_CONFIG_UNAUTHORIZED_SOUND  "/etc/sound/system/Wifi_Unauthorized"
#define FIRST_TIME_CONFIG_FAILED_SOUND  "/etc/sound/system/First_Time_Config_Failed"
#define WIFI_CONNECTING_SOUND           "/etc/sound/system/Wifi_Connecting"
#define READY_SOUND                     "/etc/sound/system/Ready"
#define CANT_CONNECT_CLOUD_SOUND        "/etc/sound/system/Cant_Connect_Cloud"
#define CONNECTED_CLOUD_SOUND           "/etc/sound/system/Cloud_Connected"
#define ROUTER_LOST_CONNECT_SOUND       "/etc/sound/system/Router_Lost_Connect"
#define BECAME_TO_AP_SOUND              "/etc/sound/system/Became_To_Ap"
#define REGISTER_FAILED_SOUND           "/etc/sound/system/Register_Failed"
#define REGISTER_CONNECT_ERROR_SOUND    "/etc/sound/system/RegisterFailedConnectError"
// #define ALEXA_SERVICE_OFF_SOUND         "/etc/sound/system/Alexa_Service_Off"
#define SETUP_MODE_SOUND                "/etc/sound/system/Setup_Mode"
#define GOING_SETUP_MODE_SOUND          "/etc/sound/system/Going_SetUp_Mode"
#define QUIT_SETUP_MODE_SOUND           "/etc/sound/system/Quit_SetUp_Mode"
#define UNCLAIMED_SOUND                 "/etc/sound/system/Unclaimed_Inform"
#define RESETING_SOUND                  "/etc/sound/system/Reseting"
#define TEMPERATURE_IS_HIGH_SOUND       "/etc/sound/system/TemperatureHigh"
#define TEMPERATURE_IS_LOW_SOUND        "/etc/sound/system/TemperatureLow"
#define FIRMWARE_UPDATING               "/etc/sound/system/FirmwareUpdating"
#define FIRMWARE_UPDATED                "/etc/sound/system/FirmwareUpdated"

typedef struct __alarm_sound_struct_
{
    const char *alarmType;
    int repeat;/*0: loop infinity*/
    int delayTime;/*second*/
    const char *format;
    const char *const *infrom;
}alarm_sound_struct;

/*
o:define
n:deviceName
*/
alarm_sound_struct alarmSounds[] =
{
    {ST_DOOR_OPEN,              1,    0,   "ono",       (const char *const[]){SOUND_BEEP, DEFAULT_NAME_FOLDER, SOUND_IS_OPENED}},
    {ST_DOOR_CLOSED,            1,    0,   "ono",       (const char *const[]){SOUND_BEEP, DEFAULT_NAME_FOLDER, SOUND_IS_CLOSED}},
    {ST_GARA_OPEN,              1,    0,   "ono",       (const char *const[]){SOUND_BEEP_2, DEFAULT_NAME_FOLDER, SOUND_IS_OPENED}},
    {ST_GARA_OPENING,           1,    0,   "ono",       (const char *const[]){SOUND_BEEP_2, DEFAULT_NAME_FOLDER, SOUND_IS_OPENING}},
    {ST_GARA_CLOSED,            1,    0,   "ono",       (const char *const[]){SOUND_BEEP_2, DEFAULT_NAME_FOLDER, SOUND_IS_CLOSED}},
    {ST_GARA_CLOSING,           1,    0,   "ono",       (const char *const[]){SOUND_BEEP_2, DEFAULT_NAME_FOLDER, SOUND_IS_CLOSING}},
    {ST_DOOR_RF_UNLOCKED,       1,    0,   "ono",       (const char *const[]){SOUND_BEEP_2, DEFAULT_NAME_FOLDER, SOUND_IS_UNLOCKED}},
    {ST_MANUAL_UNLOCK,          1,    0,   "ono",       (const char *const[]){SOUND_BEEP_2, DEFAULT_NAME_FOLDER, SOUND_IS_UNLOCKED}},
    {ST_KEYPAD_UNLOCK,          1,    0,   "ono",       (const char *const[]){SOUND_BEEP_2, DEFAULT_NAME_FOLDER, SOUND_IS_UNLOCKED}},
    {ST_AUTO_LOCK,              1,    0,   "ono",       (const char *const[]){SOUND_BEEP_2, DEFAULT_NAME_FOLDER, SOUND_IS_LOCKED}},
    {ST_ONE_TOUCH_LOCK,         1,    0,   "ono",       (const char *const[]){SOUND_BEEP_2, DEFAULT_NAME_FOLDER, SOUND_IS_LOCKED}},
    {ST_DOOR_RF_LOCKED,         1,    0,   "ono",       (const char *const[]){SOUND_BEEP_2, DEFAULT_NAME_FOLDER, SOUND_IS_LOCKED}},
    {ST_MANUAL_LOCK,            1,    0,   "ono",       (const char *const[]){SOUND_BEEP_2, DEFAULT_NAME_FOLDER, SOUND_IS_LOCKED}},
    {ST_KEYPAD_LOCK,            1,    0,   "ono",       (const char *const[]){SOUND_BEEP_2, DEFAULT_NAME_FOLDER, SOUND_IS_LOCKED}},
    {ST_STUCKINLOCK,            0,    5,   "ono",       (const char *const[]){SOUND_BEEP_3, DEFAULT_NAME_FOLDER, SOUND_IS_STUCK}},
    {ST_WRONG_PIN_3TIMES,       1,    0,   "oono",      (const char *const[]){SOUND_BEEP_3, SOUND_WRONG_USER_CODE, DEFAULT_NAME_FOLDER, SOUND_CHECK_AND_TRY}},
    {ST_MAIFUNCTION,            3,    10,  "onoo",      (const char *const[]){SOUND_BEEP_3, DEFAULT_NAME_FOLDER, SOUND_IS_MALFUNCTION, SOUND_CHECK_AND_TRY}},
    {ST_SMOKE_DETECTED,         0,    8,   "oonoo",     (const char *const[]){SOUND_BEEP_3, SOUND_SMOKE_FROM, DEFAULT_NAME_FOLDER, SOUND_IS_DETECTED, SOUND_PLEASE_EXIT}},
    {ST_CO_DETECTED,            0,    8,   "oonoo",     (const char *const[]){SOUND_BEEP_3, SOUND_CO_FROM, DEFAULT_NAME_FOLDER, SOUND_IS_DETECTED, SOUND_PLEASE_EXIT}},
    {ST_WATER_LEAKS,            0,    10,  "ono",       (const char *const[]){SOUND_BEEP_3, DEFAULT_NAME_FOLDER, SOUND_DETECTS_WATER}},
    {ST_MOTION_DETECTED,        1,    0,   "oono",      (const char *const[]){SOUND_BEEP, SOUND_MOTION_FROM, DEFAULT_NAME_FOLDER, SOUND_IS_DETECTED}},
    {ST_TEMPERATURE_LOW,        1,    0,   "ontu",      (const char *const[]){SOUND_BEEP_3, DEFAULT_NAME_FOLDER, TEMPERATURE_IS_BELOW_SOUND, DEFAULT_TEMPERATURE_FOLDER}},
    {ST_TEMPERATURE_HIGH,       1,    0,   "ontu",      (const char *const[]){SOUND_BEEP_3, DEFAULT_NAME_FOLDER, TEMPERATURE_IS_ABOVE_SOUND, DEFAULT_TEMPERATURE_FOLDER}},
    {ST_HAS_BEEN_RESET,         1,    0,   "ono",       (const char *const[]){SOUND_BEEP_3, DEFAULT_NAME_FOLDER, SOUND_HAS_BEEN_RESET}},
    {ST_WATER_FLOW_HIGH,        1,    0,   "oono",      (const char *const[]){SOUND_BEEP_3, SOUND_WATER_FLOW_FROM, DEFAULT_NAME_FOLDER, SOUND_IS_HIGH}},
    {ST_WATER_VALVE_OPEN,       1,    0,   "ono",       (const char *const[]){SOUND_BEEP_2, DEFAULT_NAME_FOLDER, SOUND_IS_OPENED}},
    {ST_WATER_VALVE_CLOSED,     1,    0,   "ono",       (const char *const[]){SOUND_BEEP_2, DEFAULT_NAME_FOLDER, SOUND_IS_CLOSED}},
};

alarm_sound_struct alarmSoundDisable[] =
{
    {ST_DOOR_OPEN,              1,    0,        "o",          (const char *const[]){SOUND_BEEP}},
    {ST_DOOR_CLOSED,            1,    0,        "o",          (const char *const[]){SOUND_BEEP}},
    {ST_GARA_OPEN,              1,    0,        "o",          (const char *const[]){SOUND_BEEP_2}},
    {ST_GARA_OPENING,           1,    0,        "o",          (const char *const[]){SOUND_BEEP_2}},
    {ST_GARA_CLOSED,            1,    0,        "o",          (const char *const[]){SOUND_BEEP_2}},
    {ST_GARA_CLOSING,           1,    0,        "o",          (const char *const[]){SOUND_BEEP_2}},
    {ST_DOOR_RF_UNLOCKED,       1,    0,        "o",          (const char *const[]){SOUND_BEEP_2}},
    {ST_MANUAL_UNLOCK,          1,    0,        "o",          (const char *const[]){SOUND_BEEP_2}},
    {ST_KEYPAD_UNLOCK,          1,    0,        "o",          (const char *const[]){SOUND_BEEP_2}},
    {ST_AUTO_LOCK,              1,    0,        "o",          (const char *const[]){SOUND_BEEP_2}},
    {ST_ONE_TOUCH_LOCK,         1,    0,        "o",          (const char *const[]){SOUND_BEEP_2}},
    {ST_DOOR_RF_LOCKED,         1,    0,        "o",          (const char *const[]){SOUND_BEEP_2}},
    {ST_MANUAL_LOCK,            1,    0,        "o",          (const char *const[]){SOUND_BEEP_2}},
    {ST_KEYPAD_LOCK,            1,    0,        "o",          (const char *const[]){SOUND_BEEP_2}},
    {ST_WRONG_PIN_3TIMES,       1,    0,        "o",          (const char *const[]){SOUND_BEEP_3}},
    {ST_MAIFUNCTION,            1,    0,        "o",          (const char *const[]){SOUND_BEEP_3}},
    {ST_STUCKINLOCK,            0,    5,   "ono",       (const char *const[]){SOUND_BEEP_3, DEFAULT_NAME_FOLDER, SOUND_IS_STUCK}},
    {ST_SMOKE_DETECTED,         0,    8,   "oonoo",     (const char *const[]){SOUND_BEEP_3, SOUND_SMOKE_FROM, DEFAULT_NAME_FOLDER, SOUND_IS_DETECTED, SOUND_PLEASE_EXIT}},
    {ST_CO_DETECTED,            0,    8,   "oonoo",     (const char *const[]){SOUND_BEEP_3, SOUND_CO_FROM, DEFAULT_NAME_FOLDER, SOUND_IS_DETECTED, SOUND_PLEASE_EXIT}},
    {ST_WATER_LEAKS,            0,    10,  "ono",       (const char *const[]){SOUND_BEEP_3, DEFAULT_NAME_FOLDER, SOUND_DETECTS_WATER}},
    {ST_WATER_VALVE_OPEN,       1,    0,        "o",          (const char *const[]){SOUND_BEEP}},
    {ST_WATER_VALVE_CLOSED,     1,    0,        "o",          (const char *const[]){SOUND_BEEP}},
};

int VR_(get_media_file)(char *text, char *save_path)
{
    if(!text && !save_path)
    {
        return -1;
    }

    SLOGI("text = %s\n", text);
    SLOGI("save_path = %s\n", save_path);
    int ret;
    char address[SIZE_1024B], headerIn[SIZE_1024B]={0};
    char *firmware_address = NULL;
    firmware_info_t firmware_data;
    // char device_name[SIZE_32B];
    // char device_rev[SIZE_32B];

    // VR_(run_command)(GET_BOARD_NAME_COMMAND, device_name, sizeof(device_name));
    // VR_(run_command)(GET_BOARD_REV_COMMAND, device_rev, sizeof(device_rev));

    get_data_firmware(PHD_BOARD, HW_REV_E, &firmware_data);

    firmware_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);

    if(!firmware_address || !strcmp(firmware_address, PRODUCT_FIRMWARE_ADDR))
    {
        sprintf(address, "%s"TEXT_TO_SPEECH_LINK, FIRMWARE_ADDRESS, firmware_data.product_name);
        snprintf(headerIn, sizeof(headerIn), "authorization: %s", firmware_data.token_product);
    }
    else
    {
        sprintf(address, "%s"TEXT_TO_SPEECH_LINK, FIRMWARE_ADDRESS, firmware_data.dev_name);
        snprintf(headerIn, sizeof(headerIn), "authorization: %s", firmware_data.token_dev);
    }
    char *result = NULL;
    json_object *post_data = json_object_new_object();
    json_object_object_add(post_data, ST_TEXT, json_object_new_string(text));
    ret = VR_(post_request)(address, headerIn, (char *)json_object_to_json_string(post_data),
                            NULL, (void *)&result, DEFAULT_TIMEOUT);

    if(!ret)
    {
        json_object* jobj = VR_(create_json_object)(result);
        if(!jobj)
        {
            ret = 1;
            SLOGE("not json object: %s\n", result);
            goto done;
        }

        json_object *linkObj = NULL;
        if(json_object_object_get_ex(jobj, ST_LINK, &linkObj))
        {
            const char *link = json_object_get_string(linkObj);
            ret = VR_(download_file)((char*) link, save_path, NULL, NULL, NULL);
        }
        else
        {
            ret = 1;
            SLOGE("link not found: %s\n", result);
        }
    }

done:
    free(result);

    if(firmware_address)
    {
        free(firmware_address);
    }
    json_object_put(post_data);

    return ret;
}

static void create_all_sound_files_thread(void *json_data)
{
    if(!json_data)
    {
        SLOGE("json_data is NULL\n");
        return;
    }

    int ret = 0;
    char folderPath[SIZE_512B];
    json_object *jobj = (json_object *)json_data;
    CHECK_JSON_OBJECT_EXIST(deviceIdObj, jobj, ST_DEVICE_ID, done);
    CHECK_JSON_OBJECT_EXIST(deviceTypeObj, jobj, ST_DEVICE_TYPE, done);
    CHECK_JSON_OBJECT_EXIST(deviceNameObj, jobj, ST_DEVICE_NAME, done);

    const char *deviceId = json_object_get_string(deviceIdObj);
    const char *devicdType = json_object_get_string(deviceTypeObj);
    const char *deviceName = json_object_get_string(deviceNameObj);

    snprintf(folderPath, sizeof(folderPath), FOLDER_SOUND_STORED"/%s", devicdType);
    if(-1 == VR_(get_file_infor)(folderPath))
    {
        VR_(execute_system)("mkdir %s", folderPath);
    }

    snprintf(folderPath, sizeof(folderPath), FOLDER_SOUND_STORED"/%s/%s", devicdType, deviceId);

    if(-1 == VR_(get_file_infor)(folderPath))
    {
        VR_(execute_system)("mkdir %s", folderPath);
    }
    else
    {
        VR_(execute_system)("rm -rf %s/*", folderPath);
    }

    char filePath[SIZE_256B];
    snprintf(filePath, sizeof(filePath), "%s/%s", folderPath, ST_DEFAULT_NEW_NAME);
    ret = VR_(get_media_file)((char*)deviceName, filePath);
    if(ret)
    {
        SLOGE("failed to get media file deviceId %s with name %s", deviceId, deviceName);
        goto done;
    }

done:
    json_object_put(jobj);
    return;
}

void create_all_sound_files(char *deviceId,
                            char *deviceType,
                            char *deviceName,
                            int mode)
{
    if(!deviceId || !deviceName || !deviceType)
    {
        SLOGE("missing device infor\n");
        return;
    }

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceId));
    json_object_object_add(jobj, ST_DEVICE_NAME, json_object_new_string(deviceName));
    json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(deviceType));

    if(mode == GET_SOUND_ASYNCHONOUS)
    {
        pthread_t getSoundFilesThread_t = 0;
        pthread_create(&getSoundFilesThread_t, NULL, (void *)&create_all_sound_files_thread, (void *)jobj);
        pthread_detach(getSoundFilesThread_t);
    }
    else if(mode == GET_SOUND_SYNCHONOUS)
    {
        create_all_sound_files_thread((void *)jobj);
    }
}

static void get_sound_temperature_threshold_thread(void *data)
{
    char *value = (char *)data;
    if(value)
    {
        char folderPath[SIZE_512B];
        snprintf(folderPath, sizeof(folderPath), DEFAULT_TEMPERATURE_FOLDER"/%s", value);
        int res = VR_(get_file_infor)(folderPath);
        if(res != S_IFREG) /*not found temperature*/
        {
            int ret = VR_(get_media_file)(value, folderPath);
            if(ret)
            {
                SLOGE("failed to get media file temperature %s\n", value);
            }
        }
        free(value);
    }
}

void get_sound_temperature_threshold(char *value)
{
    if(!value)
    {
        SLOGE("Temperature input invalid\n");
        return;
    }

    size_t length = strlen(value)+1; 
    char *input_value = (char*)malloc(length);
    strlcpy(input_value, value, length);
    pthread_t getSoundFilesThread_t = 0;
    pthread_create(&getSoundFilesThread_t, NULL, (void *)&get_sound_temperature_threshold_thread, (void *)input_value);
    pthread_detach(getSoundFilesThread_t);
}

static void remove_process(char *process_name)
{
    FILE *fp;
    char buff[SIZE_32B]={0}, cmd[SIZE_256B];
    snprintf(cmd, sizeof(cmd), "pgrep %s", process_name);
    fp = popen(cmd, "r");
    if(fp)
    {
        while(fgets(buff, SIZE_32B, fp) != NULL)
        {
            snprintf(cmd, sizeof(cmd), "kill -9 %s", buff);
            VR_(execute_system)(cmd);
        }
        pclose(fp);
    }
}

static void sleep_with_detect_cancel(uint16_t* enable, int sleep_time)
{
    while(*enable && sleep_time > 0)
    {
        sleep(1);
        sleep_time--;
    }
}

int create_link(int enable, char *alarm_type, char *deviceId, char *deviceType,
                char *defaultName, char *path, size_t inputLengh, int *delayTime, json_object *data)
{
    size_t i, length = 0;
    alarm_sound_struct *sounds;

    if(enable)
    {
        sounds = &alarmSounds[0];
        length = sizeof(alarmSounds)/sizeof(alarm_sound_struct);
    }
    else
    {
        sounds = &alarmSoundDisable[0];
        length = sizeof(alarmSoundDisable)/sizeof(alarm_sound_struct);
    }

    for(i=0; i<length; i++)
    {
        if(!strcmp(alarm_type, sounds[i].alarmType))
        {
            break;
        }
    }

    if(i >= length)
    {
        return -1;
    }

    sounds = &sounds[i];
    length = strlen(sounds->format);
    int unitDisable = 0;

    for(i=0; i<length; i++)
    {
        if(sounds->format[i] == DEVICE_NAME_CHARACTOR)
        {
            char filePath[SIZE_256B];
            snprintf(filePath, sizeof(filePath), FOLDER_SOUND_STORED"/%s/%s/%s", deviceType, deviceId, ST_DEFAULT_NEW_NAME);
            int res = VR_(get_file_infor)(filePath);
            if(res == S_IFREG)/*found new name*/
            {
                snprintf(path+strlen(path), inputLengh-strlen(path), "%s ", filePath);
            }
            else /*using default name*/
            {
                snprintf(filePath, sizeof(filePath), DEFAULT_NAME_FOLDER"/%s", defaultName);
                int res = VR_(get_file_infor)(filePath);
                if(res == S_IFREG)
                {
                    snprintf(path+strlen(path), inputLengh-strlen(path), "%s%s ", DEFAULT_NAME_FOLDER, defaultName);
                }
                else
                {
                    snprintf(path+strlen(path), inputLengh-strlen(path), "%s%s ", DEFAULT_NAME_FOLDER, ST_DEFAULT_SOUND_NAME);
                }
            }
        }
        else if(sounds->format[i] == TEMPERATURE_NAME_CHARACTOR)
        {
            int mode = 0;
            if(!strcmp(sounds->alarmType, ST_TEMPERATURE_HIGH))
            {
                mode = 1;
            }
            
            json_object *valueObj = NULL;
            json_object_object_get_ex(data, ST_TEMPERATURE_THRESH_HOLD, &valueObj);
            if(valueObj)
            {
                const char *valThreshold = json_object_get_string(valueObj);
                char filePath[SIZE_256B];
                snprintf(filePath, sizeof(filePath), DEFAULT_TEMPERATURE_FOLDER"/%s", valThreshold);
                int res = VR_(get_file_infor)(filePath);
                if(res == S_IFREG)/*found sound temperature*/
                {
                    snprintf(path+strlen(path), inputLengh-strlen(path), "%s %s ", sounds->infrom[i], filePath);
                }
                else
                {
                    unitDisable = 1;
                    if(!mode)
                    {
                        snprintf(path+strlen(path), inputLengh-strlen(path), "%s ", TEMPERATURE_IS_LOW_SOUND);
                    }
                    else
                    {
                        snprintf(path+strlen(path), inputLengh-strlen(path), "%s ", TEMPERATURE_IS_HIGH_SOUND);
                    }

                    /* Get sound temperature default */
                    get_sound_temperature_threshold((char *)valThreshold);
                }
            }
            else
            {
                unitDisable = 1;
                if(!mode)
                {
                    snprintf(path+strlen(path), inputLengh-strlen(path), "%s ", TEMPERATURE_IS_LOW_SOUND);
                }
                else
                {
                    snprintf(path+strlen(path), inputLengh-strlen(path), "%s ", TEMPERATURE_IS_HIGH_SOUND);
                }               
            }
        }
        else if(sounds->format[i] == UNIT_NAME_CHARACTOR)
        {
            if(!unitDisable)
            {
                json_object *unitObj = NULL;
                json_object_object_get_ex(data, ST_UNIT, &unitObj);
                if(unitObj)
                {
                    const char *unit = json_object_get_string(unitObj);
                    char filePath[SIZE_256B];
                    snprintf(filePath, sizeof(filePath), "%s%s", sounds->infrom[i], unit);
                    int res = VR_(get_file_infor)(filePath);
                    if(res == S_IFREG)/*found sound unit*/
                    {
                        snprintf(path+strlen(path), inputLengh-strlen(path), "%s ", filePath);
                    }
                }
            }
        }
        else
        {
            snprintf(path+strlen(path), inputLengh-strlen(path), "%s ", sounds->infrom[i]);
        }
    }

    *delayTime = sounds->delayTime;
    return sounds->repeat;
}

static void send_event(struct ubus_context *ctx, json_object *jobj)
{
    /*need static here*/
    static struct blob_buf buff;
    blob_buf_init(&buff, 0);
    blobmsg_add_object(&buff, jobj);
    ubus_send_event(ctx, ST_LED_AND_SOUND, buff.head);
    blob_buf_free(&buff);
}

static void VR_(speak)(void *ctx, char *deviceId,
                       int repeat, int delayTime, char *file)
{
    if(!deviceId || !file)
    {
        return;
    }

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_SOUND));
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceId));
    json_object_object_add(jobj, ST_CMD, json_object_new_int(PLAY_SOUND_CMD));
    json_object_object_add(jobj, ST_REPEAT, json_object_new_int(repeat));
    json_object_object_add(jobj, ST_DELAY_TIME, json_object_new_int(delayTime));
    json_object_object_add(jobj, ST_FILE, json_object_new_string(file));

    if(ctx)
    {
        send_event((struct ubus_context *)ctx, jobj);
    }
    else
    {
        const char *msg = json_object_to_json_string(jobj);
        size_t cmd_len = strlen(msg)+SIZE_256B;
        char *cmd = (char *)malloc(cmd_len);
        memset(cmd, 0, cmd_len);

        snprintf(cmd, cmd_len, "ubus send %s '%s'", ST_LED_AND_SOUND, json_object_to_json_string(jobj));
        VR_(execute_system)(cmd);
        free(cmd);
    }
    json_object_put(jobj);
}

void VR_(cancel_alarm)(struct ubus_context *ctx, char *deviceId)
{
    if(!deviceId)
    {
        return;
    }

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_SOUND));
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceId));
    json_object_object_add(jobj, ST_CMD, json_object_new_int(STOP_SOUND_CMD));

    if(ctx)
    {
        send_event(ctx, jobj);
    }
    else
    {
        const char *msg = json_object_to_json_string(jobj);
        size_t cmd_len = strlen(msg)+SIZE_256B;
        char *cmd = (char *)malloc(cmd_len);
        memset(cmd, 0, cmd_len);

        snprintf(cmd, cmd_len, "ubus send %s '%s'", ST_LED_AND_SOUND, json_object_to_json_string(jobj));
        VR_(execute_system)(cmd);
        free(cmd);
    }

    json_object_put(jobj);
    return;
}

void VR_(temp_disable_alarm)(struct ubus_context *ctx, char *deviceId)
{
    if(!deviceId)
    {
        return;
    }

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_SOUND));
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceId));
    json_object_object_add(jobj, ST_CMD, json_object_new_int(TEMPORARY_DISABLE_SOUND_CMD));
    json_object_object_add(jobj, ST_DISABLE_TIME, json_object_new_int(TEMPORARY_DISABLE_TIME));

    if(ctx)
    {
        send_event(ctx, jobj);
    }
    else
    {
        const char *msg = json_object_to_json_string(jobj);
        size_t cmd_len = strlen(msg)+SIZE_256B;
        char *cmd = (char *)malloc(cmd_len);
        memset(cmd, 0, cmd_len);

        snprintf(cmd, cmd_len, "ubus send %s '%s'", ST_LED_AND_SOUND, json_object_to_json_string(jobj));
        VR_(execute_system)(cmd);
        free(cmd);
    }

    json_object_put(jobj);
    return;
}

void VR_(cancel_all_alarms)(struct ubus_context *ctx)
{
    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string(ST_SOUND));
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ST_SYSTEM));
    json_object_object_add(jobj, ST_CMD, json_object_new_int(DISABLE_ALL_SOUND_CMD));

    if(ctx)
    {
        send_event(ctx, jobj);
    }
    else
    {
        const char *msg = json_object_to_json_string(jobj);
        size_t cmd_len = strlen(msg)+SIZE_256B;
        char *cmd = (char *)malloc(cmd_len);
        memset(cmd, 0, cmd_len);

        snprintf(cmd, cmd_len, "ubus send %s '%s'", ST_LED_AND_SOUND, json_object_to_json_string(jobj));
        VR_(execute_system)(cmd);
        free(cmd);
    }

    json_object_put(jobj);
    return;
}

void VR_(alarm)(struct ubus_context *ctx, int mode, char *alarm_type,
                char *deviceId, char *deviceType, char *defaultName, json_object *data)
{
    if(!alarm_type || !deviceId)
    {
        return;
    }

    SLOGI("alarm_type = %s\n", alarm_type);

    if(!strcmp(alarm_type, ST_TEST_END) ||
        !strcmp(alarm_type, ST_CO_CLEARED) ||
        !strcmp(alarm_type, ST_SMOKE_CLEARED) ||
        !strcmp(alarm_type, ST_WATER_CLEAR)
            )
    {
        VR_(cancel_alarm)(ctx, deviceId);
        return;
    }

    int res = get_door_status(alarm_type);
    if( (res == DOOR_RF_LOCK_VALUE)
        || (res == DOOR_RF_UNLOCK_VALUE)
        )
    {
        VR_(cancel_alarm)(ctx, deviceId);
    }

    char path[MAXIMUM_PATH] = {0};
    int delayTime = 0;
    int repeat = create_link(mode, alarm_type, deviceId, deviceType,
                    defaultName, path, sizeof(path), &delayTime, data);

    if(!strlen(path) || (repeat<0))
    {
        return;
    }

    VR_(speak)((void*)ctx, deviceId, repeat, delayTime, path);
}

void VR_(inform_found_device)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, FOUND_DEVICE_SOUND);
}

void VR_(inform_adding_device)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, ADDING_DEVICE_SOUND);
}

void VR_(inform_device_not_support)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, NOT_SUPPORT_SOUND);
}

void VR_(inform_retry_adding)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, TRY_AGAIN_SOUND);
}

void VR_(introduce_system)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, FIRST_TIME_BOOTUP_SOUND);
}

void VR_(inform_receive_config)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, RECEIVED_WIFI_CONFIG_SOUND);
}

void VR_(inform_wifi_wrong_pass)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, WIFI_CONFIG_UNAUTHORIZED_SOUND);
}

void VR_(inform_config_failed)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, FIRST_TIME_CONFIG_FAILED_SOUND);
}

void VR_(inform_system_ready)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, READY_SOUND);
}

void VR_(inform_cloud_disconnected)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, CANT_CONNECT_CLOUD_SOUND);
}

void VR_(inform_cloud_connected)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, CONNECTED_CLOUD_SOUND);
}

void VR_(inform_router_lost_connection)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, ROUTER_LOST_CONNECT_SOUND);
}

void VR_(inform_became_access_point)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, BECAME_TO_AP_SOUND);
}

void VR_(inform_setup_mode_ready)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, SETUP_MODE_SOUND);
}

void VR_(inform_wifi_connecting)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, WIFI_CONNECTING_SOUND);
}

void VR_(inform_entry_setup_mode)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, GOING_SETUP_MODE_SOUND);
}

void VR_(inform_exit_setup_mode)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, QUIT_SETUP_MODE_SOUND);
}

void VR_(inform_reseting)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, RESETING_SOUND);
}

void VR_(inform_register_failed)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, REGISTER_FAILED_SOUND);
}

void VR_(inform_register_connect_error)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, REGISTER_CONNECT_ERROR_SOUND);
}

void VR_(inform_unclaimed)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, UNCLAIMED_SOUND);
}

void VR_(inform_firmware_updating)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, FIRMWARE_UPDATING);
}

void VR_(inform_firmware_updated)(void *data)
{
    VR_(speak)(data, ST_SYSTEM, 1, 0, FIRMWARE_UPDATED);
}

void VR_(inform_device_adding_success)(void *data, char *device_name)
{
    if(!device_name)
    {
        return;
    }

    char path[SIZE_512B];
    snprintf(path, sizeof(path), DEFAULT_NAME_FOLDER"/%s", device_name);
    int res = VR_(get_file_infor)(path);
    if(res == S_IFREG)
    {
        snprintf(path, sizeof(path), "%s %s%s %s", 
                SOUND_ADDING, 
                DEFAULT_NAME_FOLDER, 
                device_name, 
                SOUND_SUCCESS);
    }
    else
    {
        snprintf(path, sizeof(path), "%s %s%s %s", 
                SOUND_ADDING, 
                DEFAULT_NAME_FOLDER, 
                ST_DEFAULT_SOUND_NAME, 
                SOUND_SUCCESS);
    }

    VR_(speak)(data, ST_SYSTEM, 1, 0, path);
}

static int set_system_volume(int value)
{
    if((value < 0) || (value > MAX_VOLUME_VALUE))
    {
        return 0;
    }

    VR_(execute_system)(SET_VOLUME_COMMAND, value);
    return 1;
}

/*###############  APIs use for a process to manage sound play ##############*/

static int g_user_volume = -1;
static int g_playing = 0;
static int g_play_control = 0;
/*receiving command from another process*/

static int handle_command = 1;
static command_alarm g_command_alarm;
static pthread_mutex_t alarmCommandListMutex;

/*queue play*/
static int handle_play = 1;
static play_t g_play_alarm;
static pthread_mutex_t playListMutex;

/*handle loop play*/
static thread_alarm g_thread_alarm;
static pthread_mutex_t threadListMutex;

static void init_command_alarm(void)
{
    VR_INIT_LIST_HEAD(&g_command_alarm.list);
    pthread_mutex_init(&alarmCommandListMutex, 0);
}

static void init_play_list(void)
{
    VR_INIT_LIST_HEAD(&g_play_alarm.list);
    pthread_mutex_init(&playListMutex, 0);
}

static void init_thread_list(void)
{
    VR_INIT_LIST_HEAD(&g_thread_alarm.list);
    pthread_mutex_init(&threadListMutex, 0);
}

void init_sound_manage_list(void)
{
    init_command_alarm();
    init_play_list();
    init_thread_list();
}

static void free_alarm_command(command_alarm *tmp)
{
    if(!tmp)
    {
        return;
    }

    SAFE_FREE(tmp->deviceId);
    SAFE_FREE(tmp->file);
    SAFE_FREE(tmp);
}

static void free_play(play_t *tmp)
{
    if(!tmp)
    {
        return;
    }

    SAFE_FREE(tmp->file);
    SAFE_FREE(tmp->deviceId);
    SAFE_FREE(tmp);
}

static void add_new_alarm_command(void *alarm)
{
    pthread_mutex_lock(&alarmCommandListMutex);
    command_alarm *input = (command_alarm *)alarm;
    command_alarm *tmp = NULL;
    struct VR_list_head *pos, *q;

    VR_(list_for_each_safe)(pos, q, &g_command_alarm.list)
    {
        tmp = VR_(list_entry)(pos, command_alarm, list);
        if(!strcmp(tmp->deviceId, input->deviceId) &&
            !strcmp(tmp->file, input->file))
        {
            SLOGI("device %s duplicate command %s\n", input->deviceId, input->file);
            if(pos != g_command_alarm.list.next)
            {
                SLOGI("not current\n");
                VR_(list_del)(pos);
                free_alarm_command(tmp);
                break;
            }
            else
            {
                free_alarm_command(input);
                pthread_mutex_unlock(&alarmCommandListMutex);
                return;
            }
        }
    }

    VR_(list_add_tail)(&(input->list), &(g_command_alarm.list));
    pthread_mutex_unlock(&alarmCommandListMutex);
    return;
}

static void add_new_play(void *play)
{
    pthread_mutex_lock(&playListMutex);
    play_t *input = (play_t *)play;
    play_t *tmp = NULL;
    struct VR_list_head *pos, *q;

    VR_(list_for_each_safe)(pos, q, &g_play_alarm.list)
    {
        tmp = VR_(list_entry)(pos, play_t, list);
        if(!strcmp(tmp->file, input->file) &&
            !strcmp(tmp->deviceId, input->deviceId))
        {
            SLOGI("remove duplicate play %s\n", input->file);
            if(pos != g_play_alarm.list.next)
            {
                SLOGI("not current\n");
                VR_(list_del)(pos);
                free_play(tmp);
                break;
            }
            else
            {
                free_play(input);
                pthread_mutex_unlock(&playListMutex);
                return;
            }
        }
    }

    VR_(list_add_tail)(&(input->list), &(g_play_alarm.list));
    pthread_mutex_unlock(&playListMutex);
    return;
}

static thread_alarm *get_thread_alarm(char *deviceId, char *file)
{
    if(!deviceId || !file)
    {
        return NULL;
    }

    int found = 0;
    pthread_mutex_lock(&threadListMutex);
    thread_alarm *tmp = NULL;
    struct VR_list_head *pos, *q;

    VR_(list_for_each_safe)(pos, q, &g_thread_alarm.list)
    {
        tmp = VR_(list_entry)(pos, thread_alarm, list);
        if(!strcmp(tmp->deviceId, deviceId) &&
            !strcmp(tmp->file, file))
        {
            found = 1;
            break;
        }
    }

    pthread_mutex_unlock(&threadListMutex);
    return found?tmp:NULL;
}

void insert_alarm_command(char *deviceId, int repeat, int delayTime, char *file)
{
    if(!deviceId || !file)
    {
        SLOGE("missing device id or file to play\n");
        return;
    }

    command_alarm *new_alarm = (command_alarm *)malloc(sizeof(command_alarm));
    memset(new_alarm, 0x00, sizeof(command_alarm));

    new_alarm->deviceId = strdup(deviceId);
    new_alarm->file = strdup(file);
    new_alarm->repeat = repeat;
    new_alarm->delayTime = delayTime;

    add_new_alarm_command((void *)new_alarm);
}

static void insert_play(char *file, char *deviceId, int playTime)
{
    if(!file || !deviceId)
    {
        SLOGE("missing file to play\n");
        return;
    }

    play_t *new_play = (play_t *)malloc(sizeof(play_t));
    memset(new_play, 0x00, sizeof(play_t));

    new_play->file = strdup(file);
    new_play->deviceId = strdup(deviceId);
    new_play->playTime = (uint8_t)playTime;

    add_new_play((void *)new_play);
}

static void estimate_duration_clean(mpg123_handle *mh)
{
    // close_output(ao);
    // close_output_module(ao);
    /* It's really to late for error checks here;-) */
    mpg123_close(mh);
    mpg123_delete(mh);
    mpg123_exit();
}

static int estimate_duration(char *infile)
{
    if(!infile)
    {
        return 0;
    }

    int err = MPG123_OK;
    mpg123_handle *mh = NULL;

    err = mpg123_init();
    if(err != MPG123_OK || (mh = mpg123_new(NULL, &err)) == NULL)
    {
        SLOGE("Basic setup goes wrong: %s\n", mpg123_plain_strerror(err));
        estimate_duration_clean(mh);
        return -1;
    }

    float total_time = 0;
    size_t file_length = 0;
    size_t current_rate = 0;

    char *tmp = strdup(infile);
    char *tok = strtok (tmp, " ");
    while(tok)
    {
        if(mpg123_open(mh, tok) != MPG123_OK)
        {
            SLOGE("Trouble with mpg123: %s\n", mpg123_strerror(mh) );
            /*try with next*/
            tok = strtok(NULL, " ");
            continue;
        }

        struct mpg123_frameinfo mi;
        mpg123_info(mh, &mi);

        if(current_rate != mi.rate)
        {
            if(current_rate)
            {
                total_time += (float)file_length/current_rate;
            }

            file_length = 0;
            current_rate = mi.rate;
        }

        file_length += mpg123_length(mh);

        tok = strtok(NULL, " ");
    }

    if(current_rate)
    {
        total_time += (float)file_length/current_rate;
    }

    estimate_duration_clean(mh);
    free(tmp);
    return (int)(total_time+0.5);
}

static void speak_loop_v2(void *data)
{
    if(!data)
    {
        return;
    }

    thread_alarm *thread = (thread_alarm*)data;

    if(!thread->file)
    {
        goto done;
    }

    int play_time = estimate_duration(thread->file);
    /*for repeat feature*/
    if(thread->repeat)
    {
        while(thread->repeat)
        {
            insert_play(thread->file, thread->deviceId, play_time);
            sleep_with_detect_cancel(&thread->speak_loop_enable, thread->delayTime+play_time);
            thread->repeat--;
        }

        cancel_alarm_thread(thread->deviceId);
        goto done;
    }

    /*for loop feature*/
    while(thread->speak_loop_enable)
    {
        insert_play(thread->file, thread->deviceId, play_time);
        /*should detect duration and adding sleep here*/

        sleep_with_detect_cancel(&thread->speak_loop_enable, thread->delayTime+play_time);

        if(thread->temporary_disable_time)
        {
            sleep_with_detect_cancel(&thread->speak_loop_enable,
                                     thread->temporary_disable_time);
            thread->temporary_disable_time = 0;
        }
    }

done:
    pthread_detach(thread->pthreadID);
    SAFE_FREE(thread->deviceId);
    SAFE_FREE(thread->file);
    SAFE_FREE(thread);
    return ;
}

static void insert_new_thread(char *deviceId, int repeat, int delayTime, char *file)
{
    if(!deviceId || !file)
    {
        SLOGE("missing device id or file to play\n");
        return;
    }

    thread_alarm *thread = get_thread_alarm(deviceId, file);
    if(thread)
    {
        printf("found old thread\n");
        /*thread exist, reset value*/
        thread->speak_loop_enable = 1;
        thread->temporary_disable_time = 0;
        return;
    }

    /*create new thread*/
    pthread_t loop_until_cancel;
    thread_alarm *new_thread = (thread_alarm*)malloc(sizeof(thread_alarm));
    memset(new_thread, 0x00, sizeof(thread_alarm));

    new_thread->file = strdup(file);
    new_thread->deviceId = strdup(deviceId);
    new_thread->repeat = repeat;
    new_thread->delayTime = delayTime;
    new_thread->speak_loop_enable = 1;
    new_thread->temporary_disable_time = 0;

    pthread_create(&loop_until_cancel, NULL, (void *)&speak_loop_v2, (void*)new_thread);
    /*run pthread_create to get pthreadID */
    new_thread->pthreadID = loop_until_cancel;

    pthread_mutex_lock(&threadListMutex);
    VR_(list_add_tail)(&(new_thread->list), &(g_thread_alarm.list));
    pthread_mutex_unlock(&threadListMutex);
}


static command_alarm *get_command(void)
{
    command_alarm *command = VR_list_entry(g_command_alarm.list.next, command_alarm, list);

    // pthread_mutex_lock(&alarmCommandListMutex);
    // VR_(list_del)(g_command_alarm.list.next);
    // pthread_mutex_unlock(&alarmCommandListMutex);

    return command;
}

static play_t *get_play(void)
{
    play_t *play = VR_list_entry(g_play_alarm.list.next, play_t, list);

    // pthread_mutex_lock(&playListMutex);
    // VR_(list_del)(g_play_alarm.list.next);
    // pthread_mutex_unlock(&playListMutex);

    return play;
}

static int get_system_volume()
{
    char value[SIZE_32B]={0};

    VR_(run_command)(GET_VOLUME_COMMAND, value, sizeof(value));
    return strtol(value, NULL, 10);
}

int VR_(set_volume)(int value)
{
    if((value < 0) || (value > MAX_VOLUME_VALUE))
    {
        return 0;
    }

    int current_value = VR_(get_volume)();
    if(current_value != value)
    {
        set_system_volume(value);
        VR_(write_option)(NULL, UCI_SOUND_VOLUME"=%d", value);

        /*this value for handle sound process only*/
        g_user_volume = value;
    }

    return 1;
}

int VR_(get_volume)()
{
    char *value = VR_(read_option)(NULL, UCI_SOUND_VOLUME);

    int volume = SYSTEM_VOLUME_VALUE;
    if(value)
    {
        volume = strtol(value, NULL, 10);
    }

    /*this value for handle sound process only*/
    g_user_volume = volume;

    SAFE_FREE(value);
    return volume;
}

static int speak(char *deviceId, char *text, uint8_t playTime)
{
    if(!text || !deviceId)
    {
        return 0;
    }

    int volume = SYSTEM_VOLUME_VALUE;
    // if(strcmp(deviceId, ST_SYSTEM))
    // {
        /*not system sound*/
        if(-1 == g_user_volume)
        {
            volume = VR_(get_volume)();
        }
        else
        {
            volume = g_user_volume;
        }
    // }

    if(volume != get_system_volume())
    {
        set_system_volume(volume);
    }

    if(get_processId_from_name("radiomon") != -1)
    {
        return 0;
    }

    int retry = 3;
    int cpu_system = get_cpu_system();
    while(cpu_system > 80 && retry > 0 )
    {
        cpu_system = get_cpu_system();
        SLOGI("retry %d cpu_system = %d%%\n", 4-retry, cpu_system);
        usleep(DELAY_PLAY);
        retry--;
    }

    int num_process = get_number_process_from_name("mpg123");
    if(num_process > MAXIMUM_MPG123_PROCESS)
    {
        SLOGI("mpg23 got maximum allow\n");
        remove_process("mpg123");
    }

    g_playing = 1;
    char *command = (char *)malloc(strlen(text)+SIZE_256B);
    sprintf(command, "nice -n -19 mpg123 %s &", text);
    VR_(execute_system)(command);
    free(command);
    if(playTime > 0)
    {
        sleep(playTime);
    }
    g_playing = 0;
    return 0;
}

void handle_command_sound(void)
{
    while(handle_command)
    {
        if(VR_list_empty(&g_command_alarm.list))
        {
            usleep(2000);
            continue;
        }

        command_alarm *tmp = get_command();
        if(!tmp)
        {
            usleep(2000);
            continue;
        }

        if(tmp->repeat == 1)
        {
            insert_play(tmp->file, tmp->deviceId, estimate_duration(tmp->file));
        }
        else if(tmp->repeat >= 0)
        {
            insert_new_thread(tmp->deviceId, tmp->repeat, tmp->delayTime, tmp->file);
        }

        pthread_mutex_lock(&alarmCommandListMutex);
        VR_(list_del)(g_command_alarm.list.next);
        pthread_mutex_unlock(&alarmCommandListMutex);

        free_alarm_command(tmp);

        usleep(1000);
    }
}

void play_sound(void)
{
    while(handle_play)
    {
        if(g_play_control)
        {
            usleep(2000);
            continue;
        }

        if(VR_list_empty(&g_play_alarm.list))
        {
            usleep(2000);
            continue;
        }

        play_t *tmp = get_play();
        if(!tmp)
        {
            usleep(2000);
            continue;
        }

        speak(tmp->deviceId, tmp->file, tmp->playTime);

        pthread_mutex_lock(&playListMutex);
        VR_(list_del)(g_play_alarm.list.next);
        pthread_mutex_unlock(&playListMutex);

        free_play(tmp);

        usleep(DELAY_PLAY);
    }
}

void cancel_alarm_thread(char *deviceId)
{
    pthread_mutex_lock(&threadListMutex);
    thread_alarm *tmp = NULL;
    struct VR_list_head *pos, *q;
    
    VR_(list_for_each_safe)(pos, q, &g_thread_alarm.list)
    {
        tmp = VR_(list_entry)(pos, thread_alarm, list);
        if(tmp->deviceId && !strcmp(tmp->deviceId, deviceId))
        {
            VR_(list_del)(pos);
            tmp->speak_loop_enable = 0;
            // break; maybe 1 devices have more than 1 alarm.
        }
    }

    pthread_mutex_unlock(&threadListMutex);
    return;
}

void temporary_disable_alarm_thread(char *deviceId, int disableTime)
{
    pthread_mutex_lock(&threadListMutex);
    thread_alarm *tmp = NULL;
    struct VR_list_head *pos, *q;

    VR_(list_for_each_safe)(pos, q, &g_thread_alarm.list)
    {
        tmp = VR_(list_entry)(pos, thread_alarm, list);
        if(!strcmp(tmp->deviceId, deviceId))
        {
            tmp->temporary_disable_time = disableTime;
            // break; maybe 1 device have more than 1 alarm.
        }
    }

    pthread_mutex_unlock(&threadListMutex);
    return;
}

void disable_all_alarms_thread(void)
{
    pthread_mutex_lock(&threadListMutex);
    thread_alarm *tmp = NULL;
    struct VR_list_head *pos, *q;

    VR_(list_for_each_safe)(pos, q, &g_thread_alarm.list)
    {
        tmp = VR_(list_entry)(pos, thread_alarm, list);
        VR_(list_del)(pos);
        tmp->speak_loop_enable = 0;
    }

    pthread_mutex_unlock(&threadListMutex);
    return;
}

/*this api shoud be called by process that handle sound play thread.*/
void inform_volume_change(void)
{
    if(g_playing)
    {
        return;
    }

    //pause play
    g_play_control = 1;

    VR_(execute_system)("nice -n -19 mpg123 /etc/sound/system/ping &");

    //unpause play
    g_play_control = 0;

    return;
}

void remove_voice_inform_from_id(char *type, char *devId)
{
    if(!devId || !type)
    {
        return;
    }

    char pathDir[SIZE_256B];
    snprintf(pathDir, sizeof(pathDir), FOLDER_SOUND_STORED"/%s/%s", type, devId);
    int res = VR_(get_file_infor)(pathDir);
    if(res == S_IFDIR)/*found directory name*/
    {
        VR_(execute_system)("rm -rf %s", pathDir);
    }
}
