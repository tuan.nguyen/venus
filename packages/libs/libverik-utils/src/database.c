#define _GNU_SOURCE
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "verik_utils.h"
#include "database.h"
#include "VR_define.h"
#include "slog.h"

static int showLog = 1;

void remove_database_log(void)
{
    showLog = 0;
}

void enable_database_log(void)
{
    showLog = 1;
}

void init_seach_data(search_data *data)
{
    data->len = 0;
    data->value = malloc(data->len+1);
    if (data->value == NULL) 
    {
        fprintf(stderr, "malloc() failed\n");
        exit(EXIT_FAILURE);
    }
    data->value[0] = '\0';
}

int get_last_data_cb(void *data, int argc, char **argv, char **azColName)
{
    int i;
    for(i=0; i<argc; i++)
    {
        if(showLog)
        {
            SLOGI("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
        }
        
        if(!argv[i] || !data)
        {
            if(showLog)
            {
                SLOGE("data or input invalid\n");
            }
            return 0;
        }

        search_data *tmp_data = (search_data *)data;
        size_t new_len = strlen(argv[i]);
        if(!new_len)
        {
            tmp_data->len = 1;
            return 0;
        }

        tmp_data->value = realloc(tmp_data->value, new_len+1);
        if (tmp_data->value == NULL) 
        {
            fprintf(stderr, "realloc() failed\n");
            exit(EXIT_FAILURE);
        }
        memcpy(tmp_data->value, argv[i], new_len);
        tmp_data->value[new_len] = '\0';
        tmp_data->len = new_len;

        return 0;
    }
    return 0;
}

int zwave_cb(void *data, int argc, char **argv, char **azColName)
{
    return get_last_data_cb(data, argc, argv, azColName);
}

int zigbee_cb(void *data, int argc, char **argv, char **azColName)
{
    return get_last_data_cb(data, argc, argv, azColName);
}

int upnp_cb(void *data, int argc, char **argv, char **azColName)
{
    return get_last_data_cb(data, argc, argv, azColName);
}

int group_get_last_data_cb(void *data, int argc, char **argv, char **azColName)
{
    return get_last_data_cb(data, argc, argv, azColName);
}

static int busy_handler(void *data, int retry)
{
    busy_handler_attr* attr = (busy_handler_attr*)data;

    if (retry < attr->max_retry) {
        /* Sleep a while and retry again. */
        SLOGI("%s hits SQLITE_BUSY %d times, retry again.\n",
                attr->service_name, retry);
        sqlite3_sleep(attr->sleep_ms);
        /* Return non-zero to let caller retry again. */
        return 1;
    }
    /* Return zero to let caller return SQLITE_BUSY immediately. */
    SLOGI("Error: %s had retried %d times, exit.\n",
            attr->service_name, retry);
    // int processId = get_processId_from_name((char *)attr->service_name);
    // if(processId == -1)
    // {
    //     VR_(execute_system)("killall %s", attr->service_name);
    // }
    // else
    // {
    //     VR_(execute_system)("kill -9 %d", processId);
    // }
    return 0;
}

int open_database(char *Database_Location, sqlite3 **data)
{
    int  rc;
    /* Open database */
    rc = sqlite3_open(Database_Location, data);
    if( rc )
    {
        SLOGE("Can't open database: %s\n", sqlite3_errmsg(*data));
        return -1;
    }
    else
    {
        SLOGI("Opened database successfully\n");
    }

    return rc; 
}

void close_database(sqlite3 **db)
{
    sqlite3_close(*db);
}

void searching_database(const char* service_name, sqlite3 *db, db_callback callback, void *result, const char* format, ...)
{
    int rc;
    char *zErrMsg = 0;
    char *sql = NULL;
    // char sql[MAX_SQL_QUERRY];
    // memset(sql, 0x00, sizeof(sql));

    va_list argptr;
    va_start(argptr, format);
    rc = vasprintf(&sql, format, argptr);
    va_end(argptr);

    if(rc < 0)
    {
        SAFE_FREE(sql);
        return ;
    }

    busy_handler_attr bh_attr;

    bh_attr.service_name = service_name;  /* Max retry times */
    bh_attr.max_retry = 100;  /* Max retry times */
    bh_attr.sleep_ms = 100;   /* Sleep 100ms before each retry */
    sqlite3_busy_handler(db, busy_handler, &bh_attr);

    rc = sqlite3_exec(db, sql, callback, (void*)result, &zErrMsg);
    if( rc != SQLITE_OK )
    {
        SLOGE("SQL error: %s\n", zErrMsg);
        if(zErrMsg)
        {
            sqlite3_free(zErrMsg);
        }
        printf("\n\n\n###########################################\n");
        SLOGI("rc = %d\n", rc);
        SLOGI("sql = %s\n", sql);
        // if(rc == 21)
        // {
        //     char cmd[256];
        //     sprintf("/etc/start_services.sh start_service %s >> /system.log", service_name);
        //     system(cmd);
        // }
        // printf("###########################################\n\n\n");
        // if(rc == SQLITE_LOCKED || rc == SQLITE_BUSY)
        // {
        //     while(rc != SQLITE_OK)
        //     {
        //         rc = sqlite3_exec(db, sql, callback, (void*)result, &zErrMsg);
        //         printf("rc = %d\n", rc);
        //         if( rc != SQLITE_OK )
        //         {
        //             SLOGE("SQL error: %s\n", zErrMsg);
        //             if(zErrMsg)
        //             {
        //                 sqlite3_free(zErrMsg);
        //             }
        //         }
        //         else
        //         {
        //             SLOGI("%s SUCCESS\n", sql);
        //         }
        //         usleep(1000);
        //     }
        // }
    }
    else
    {
        if(showLog)
        {
            SLOGI("%s SUCCESS\n", sql);
        }
    }

    SAFE_FREE(sql);
}

int database_actions(const char* service_name, sqlite3 *db, const char* format, ...)
{
    // char sql[MAX_SQL_QUERRY];
    // memset(sql, 0x00, sizeof(sql));
    int rc;
    char *sql = NULL;

    va_list argptr;
    va_start(argptr, format);
    rc = vasprintf(&sql, format, argptr);
    va_end(argptr);

    if(rc < 0)
    {
        SAFE_FREE(sql);
        return rc;
    }

    sqlite3_mutex_enter(sqlite3_db_mutex(db));

    sqlite3_stmt* stmt;
    busy_handler_attr bh_attr;

    bh_attr.service_name = service_name;  /* Max retry times */
    bh_attr.max_retry = 100;  /* Max retry times */
    bh_attr.sleep_ms = 100;   /* Sleep 100ms before each retry */
    sqlite3_busy_handler(db, busy_handler, &bh_attr);
    //int rc = sqlite3_exec(db, sql, NULL, 0, &zErrMsg);
    rc = sqlite3_prepare_v2(db, sql, (int)strlen(sql), &stmt, NULL);
    if (rc != SQLITE_OK)
    {
        SLOGI("error sqlite3_prepare_v2\n");
        goto cleanup;
    }
    rc = sqlite3_exec(db, "BEGIN TRANSACTION", NULL, 0, NULL);
    if (rc != SQLITE_OK)
    {
        goto cleanup;
    }
        
    int txn_begin = 1; /* Mark that explicit txn began. */
    rc = sqlite3_step(stmt);
    sqlite3_reset(stmt);
    if (rc != SQLITE_DONE) 
    {
        /* We can not return here. Rollback is required. */
        goto cleanup;
    }

    /* Commit if no errors. */
    rc = sqlite3_exec(db, "COMMIT TRANSACTION", NULL, 0, NULL);
    if (rc != SQLITE_OK)
    {
        goto cleanup;
    }

cleanup:
    /* Error handle. */
    if (rc != SQLITE_OK && rc != SQLITE_DONE) {
        SLOGI("%s ERROR: %s. ERRCODE: %d.\n txn_begin = %d\n", sql, sqlite3_errmsg(db), rc, txn_begin);
        /* Rollback if explict txn had begined. */
        if (txn_begin)
        {
            sqlite3_exec(db, "ROLLBACK TRANSACTION", NULL, 0, NULL);
        }
    }

    /* Final cleanup. */
    sqlite3_finalize(stmt);

    //sqlite3_close(db);
    if(!rc)
    {
        if(showLog)
        {
            SLOGI("%s SUCCESS\n", sql);
        }
    }

    SAFE_FREE(sql);
    sqlite3_mutex_leave(sqlite3_db_mutex(db));
    return rc;
}

int countChars( char* s, char c )
{
    return *s == '\0'
              ? 0
              : countChars( s + 1, c ) + (*s == c);
}

void update_resource_id(char* service_name, sqlite3 *db, db_callback callback, char *deviceId, char *cloudId)
{
    if(!deviceId || !cloudId)
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(owner);
    searching_database(service_name, db, callback, &owner, "SELECT owner from DEVICES where deviceId='%s'", deviceId);

    if(!owner.len)
    {
        const char *service = get_ubus_service(service_name);
        database_actions(service_name, db, "INSERT INTO DEVICES(deviceId,deviceType,owner)"
                            "VALUES('%s','%s','%s')", deviceId, service, cloudId);
    }
    else if(strcmp(owner.value, cloudId))
    {
        database_actions(service_name, db, "UPDATE DEVICES set owner='%s' where deviceId='%s'", cloudId, deviceId);
    }
    FREE_SEARCH_DATA_VAR(owner);
}


/*####################### support association #######################*/
void set_ass_node_list_database(char* service_name, sqlite3 *db, db_callback callback, 
                                char *ID, char *groupId, char* nodeList, 
                                const char* mode, int rotate_num)
{
    if(!nodeList || !strlen(nodeList))
    {
        return;
    }

    int rotate = rotate_num;

    SEARCH_DATA_INIT_VAR(tmp);
    char newNodeList[SIZE_1024B];

    searching_database(service_name, db, callback, &tmp,
                        "SELECT nodeList from ASSOCIATION_DATA where "
                        "%s='%s' AND %s='%s'",
                        ST_DEVICE_ID, ID,
                        ST_GROUP_ID, groupId);
    if(tmp.len)
    {
        if(!strcmp(mode,ST_UPDATE))
        {
            int current_number_comma = 0;
            current_number_comma = countChars(tmp.value, ',');

            SEARCH_DATA_INIT_VAR(maxNodeSupport);
            searching_database(service_name, db, callback, &maxNodeSupport,
                                "SELECT value from MORE_PROPERTY where deviceId='%s' "
                                "AND deviceType='association' AND propertyId='%s' AND "
                                "key='%s'",
                                ID, groupId, ST_MAX_NODE_SUPPORT);

            if(maxNodeSupport.len)
            {
                rotate = strtol(maxNodeSupport.value, NULL, 0);
            }

            FREE_SEARCH_DATA_VAR(maxNodeSupport);

            if(current_number_comma >= rotate-1)
            {
                snprintf(newNodeList, sizeof(newNodeList), "%s", tmp.value); //do not insert if maximum.
            }
            else
            {
                char all_value[SIZE_1024B];
                snprintf(all_value, sizeof(all_value), "%s,%s", tmp.value, nodeList);

                char *save_tok;
                char *ch = strtok_r(all_value, ",", &save_tok);
                int i;
                for(i=0; (i < rotate) && ch; i++)
                {
                    if(!i)
                    {
                        sprintf(newNodeList, "%s", ch);
                    }
                    else
                    {
                        sprintf(newNodeList+strlen(newNodeList), ",%s", ch);
                    }
                    ch = strtok_r(NULL, ",", &save_tok);
                }
            }
            // else
            // {
            //     if(current_number_comma >= rotate-1)
            //     {
            //         char *ch = NULL;
            //         ch = strtok(tmp.value, ",");
            //         new_number_comma = countChars((char*)value, ',');
            //         while(new_number_comma > 0 && ch) //remove old data
            //         {
            //             new_number_comma--;
            //             ch = strtok(NULL, ",");
            //         }
            //         strcpy(new_value, ch);
            //         sprintf(new_value+strlen(new_value), ",%s", value);
            //     }
            //     else
            //     {
            //         char all_value[SIZE_1024B];
            //         sprintf(all_value, "%s,%s", tmp.value, value);
            //         char *ch = strtok(all_value, ",");
            //         while(rotate > 0 && ch) //remove old data
            //         {
            //             ch = strtok(NULL, ",");
            //             rotate--;
            //         }
            //         strcpy(new_value, ch);
            //     }
            // }
        }
        else if(!strcmp(mode,ST_REPLACE))
        {
            snprintf(newNodeList, sizeof(newNodeList), "%s", nodeList);
        }

        if(strcmp(newNodeList, tmp.value))
        {
            database_actions(service_name, db, "UPDATE ASSOCIATION_DATA SET nodeList='%s' "
                                "WHERE deviceId='%s' AND groupId='%s'", 
                                newNodeList, ID, groupId);
        }
    }
    else
    {
        database_actions(service_name, db, "INSERT INTO ASSOCIATION_DATA (deviceId,groupId,nodeList,type) "
                                        "VALUES('%s','%s','%s','%s')", 
                                        ID, groupId, nodeList, service_name);
    }
    FREE_SEARCH_DATA_VAR(tmp);
}

void remove_ass_node_list_database(char* service_name, sqlite3 *db, db_callback callback, 
                                char *ID, char* groupId, char* nodeList)
{
    if(!nodeList || !strlen(nodeList))
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(tmp);
    char newNodeList[SIZE_1024B] ={0},*ch=NULL, *save_tok;

    searching_database(service_name, db, callback, &tmp,
                    "SELECT nodeList from ASSOCIATION_DATA where "
                    "%s='%s' AND %s='%s'",
                    ST_DEVICE_ID, ID,
                    ST_GROUP_ID, groupId);

    if(tmp.len)
    {
        ch = strtok_r(tmp.value, ",", &save_tok);
        while( ch != NULL ) 
        {
            if(!strstr(nodeList, ch)) //if it does not exist in value,
            {
                if(strlen(newNodeList))
                {
                    sprintf(newNodeList+strlen(newNodeList),",%s", ch);
                }
                else
                {
                    SAFE_STRCPY(newNodeList, ch);
                }
            }
            ch = strtok_r(NULL, ",", &save_tok);
        }
        database_actions(service_name, db, "UPDATE ASSOCIATION_DATA SET nodeList='%s' "
                                "WHERE deviceId='%s' AND groupId='%s'", 
                                newNodeList, ID, groupId);
    }
    FREE_SEARCH_DATA_VAR(tmp);
}

void remove_all_ass_node_database(char* service_name, sqlite3 *db, 
                                char* deviceId, char *groupId)
{
    if(!deviceId || !groupId)
    {
        SLOGE("missing deviceId || groupId\n");
        return;
    }

     database_actions(service_name, db, 
                        "DELETE from ASSOCIATION_DATA where %s='%s' AND %s='%s'",
                        ST_DEVICE_ID, deviceId, ST_GROUP_ID, groupId);
}

/*####################### support meter #######################*/
void set_meter_value_database(char* service_name, sqlite3 *db, db_callback callback, 
                                char *deviceId, const char *meterType, char* meterValue, 
                                const char *meterUnit)
{
    if(!meterValue || !strlen(meterValue))
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(tmp);

    searching_database(service_name, db, callback, &tmp,
                        "SELECT meterValue from METER_DATA where "
                        "%s='%s' AND %s='%s' AND %s='%s'",
                        ST_DEVICE_ID, deviceId,
                        ST_METER_TYPE, meterType,
                        ST_METER_UNIT, meterUnit);
    if(tmp.len)
    {
        if(strcmp(meterValue, tmp.value))
        {
            database_actions(service_name, db, "UPDATE METER_DATA SET meterValue='%s' "
                                "WHERE deviceId='%s' AND meterType='%s' AND meterUnit='%s'", 
                                meterValue, deviceId, meterType, meterUnit);
        }
    }
    else
    {
        database_actions(service_name, db, "INSERT INTO METER_DATA (deviceId,meterType,meterValue,meterUnit,type) "
                                        "VALUES('%s','%s','%s','%s','%s')", 
                                        deviceId, meterType, meterValue, meterUnit, service_name);
    }
    FREE_SEARCH_DATA_VAR(tmp);
}

char *get_meter_value_database(char* service_name, sqlite3 *db, db_callback callback, 
                                char *deviceId, const char *meterType, const char *meterUnit)
{
    char *meterValue = NULL;
    SEARCH_DATA_INIT_VAR(meterCurrentValue);
    searching_database(service_name, db, callback, &meterCurrentValue, 
                        "SELECT meterValue from METER_DATA where %s='%s' and %s='%s' and %s='%s'", 
                        ST_DEVICE_ID, deviceId, ST_METER_TYPE, meterType, ST_METER_UNIT, meterUnit);
    if(meterCurrentValue.len)
    {
        meterValue = strdup(meterCurrentValue.value);
    }
    FREE_SEARCH_DATA_VAR(meterCurrentValue);

    return meterValue;
}

void reset_all_meter_value_database(char* service_name, sqlite3 *db, char *deviceId)
{
    if(!deviceId)
    {
        return;
    }

    database_actions(service_name, db,
                    "DELETE from METER_DATA where %s='%s'", ST_DEVICE_ID, deviceId);
}

/*####################### support sensor #######################*/
void set_sensor_value_database(char* service_name, sqlite3 *db, db_callback callback, 
                                char *deviceId, const char *sensorType, char* sensorValue, 
                                const char *sensorUnit)
{
    if(!sensorValue || !strlen(sensorValue))
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(tmp);

    searching_database(service_name, db, callback, &tmp,
                        "SELECT sensorValue from SENSOR_DATA where "
                        "%s='%s' AND %s='%s'",
                        ST_DEVICE_ID, deviceId,
                        ST_SENSOR_TYPE, sensorType);
    if(tmp.len)
    {
        if(strcmp(sensorValue, tmp.value))
        {
            database_actions(service_name, db, 
                                "UPDATE SENSOR_DATA SET sensorValue='%s', sensorUnit='%s' "
                                "WHERE deviceId='%s' AND sensorType='%s'", 
                                sensorValue, sensorUnit, deviceId, sensorType);
        }
    }
    else
    {
        database_actions(service_name, db, "INSERT INTO SENSOR_DATA (deviceId,sensorType,sensorValue,sensorUnit,type) "
                                        "VALUES('%s','%s','%s','%s','%s')", 
                                        deviceId, sensorType, sensorValue, sensorUnit, service_name);
    }
    FREE_SEARCH_DATA_VAR(tmp);
}

/*####################### support doorlock #######################*/
void set_door_user_code_database(char* service_name, sqlite3 *db, db_callback callback,
                                 char* deviceId, char* userId, char* name)
{
    if(!deviceId || !userId || !name)
    {
        SLOGE("missing deviceId or userId or name\n");
        return;
    }

    SEARCH_DATA_INIT_VAR(tmp);

    searching_database(service_name, db, callback, &tmp,
                        "SELECT %s from USER_CODE where "
                        "%s='%s' AND %s='%s'", ST_PASS,
                        ST_DEVICE_ID, deviceId,
                        ST_USER_ID, userId);

    if(tmp.len)
    {
        if(strcmp(name, tmp.value))
        {
            database_actions(service_name, db, 
                                "UPDATE USER_CODE SET %s='%s' "
                                "WHERE %s='%s' AND %s='%s'", 
                                ST_PASS, name, ST_DEVICE_ID, 
                                deviceId, ST_USER_ID, userId);
        }
    }
    else
    {
        database_actions(service_name, db, 
                        "INSERT INTO USER_CODE (%s,%s,%s,%s) "
                        "VALUES('%s','%s','%s','%s')",
                        ST_DEVICE_ID, ST_USER_ID, ST_PASS, ST_TYPE,
                        deviceId, userId, name, service_name);
    }
}

void remove_door_user_code_database(char* service_name, sqlite3 *db,
                                    char* deviceId, char* userId)
{
    if(!deviceId || !userId)
    {
        SLOGE("missing deviceId or userId\n");
        return;
    }

    database_actions(service_name, db, "DELETE FROM USER_CODE WHERE "
                                        "%s='%s' AND %s='%s'",
                                        ST_DEVICE_ID, deviceId, 
                                        ST_USER_ID, userId);
}

void remove_all_user_code_database(char* service_name, sqlite3 *db, char* deviceId)
{
    if(!deviceId)
    {
        SLOGE("missing deviceId \n");
        return;
    }

    database_actions(service_name, db, "DELETE FROM USER_CODE WHERE "
                                        "%s='%s'",
                                        ST_DEVICE_ID, deviceId);
}

/*####################### support sence device #######################*/
void update_sence_activate_database(char* service_name, sqlite3 *db,
                                char* deviceId, char* buttonId, unsigned int timeActive)
{
    if(!deviceId || !buttonId)
    {
        SLOGE("missing deviceId or buttonId\n");
        return;
    }

    /*alway update no need check exist or not because we dont have groupId here*/
    database_actions(service_name, db, 
                    "UPDATE SCENES set %s='%u' where "
                    "%s='%s' AND %s='%s'",
                    ST_TIME_ACTIVE, timeActive, 
                    ST_DEVICE_ID, deviceId,
                    ST_BUTTON_ID, buttonId);
}

void set_scene_button_database(char* service_name, sqlite3 *db, db_callback callback,
                             char* deviceId, char* groupId, char* buttonId, char *sceneId)
{
    if(!deviceId || !buttonId || !groupId)
    {
        SLOGE("missing deviceId or buttonId or groupId\n");
        return;
    }

    SEARCH_DATA_INIT_VAR(currentButtonId);

    searching_database(service_name, db, callback, &currentButtonId,
                        "SELECT %s from SCENES where "
                        "%s='%s' AND %s='%s'",
                        ST_BUTTON_ID,
                        ST_DEVICE_ID, deviceId,
                        ST_GROUP_ID, groupId);
    if(currentButtonId.len)
    {
        char *sql;
        if(sceneId)
        {
            sql = "UPDATE SCENES set %s='%s',%s='%s' where %s='%s' AND %s='%s'";
            database_actions(service_name, db, sql,
                    ST_BUTTON_ID, buttonId, 
                    ST_SCENE_ID, sceneId, 
                    ST_DEVICE_ID, deviceId,
                    ST_GROUP_ID, groupId);
        }
        else
        {
            sql = "UPDATE SCENES set %s='%s' where %s='%s' AND %s='%s'";
            database_actions(service_name, db, sql,
                    ST_BUTTON_ID, buttonId,
                    ST_DEVICE_ID, deviceId,
                    ST_GROUP_ID, groupId);
        }
        
    }
    else
    {
        char *sql;
        if(sceneId)
        {
            sql = "INSERT INTO SCENES (%s,%s,%s,%s,type) VALUES('%s','%s','%s','%s','zwave')";
            database_actions(service_name, db, sql,
                    ST_DEVICE_ID, ST_GROUP_ID, ST_BUTTON_ID, ST_SCENE_ID,
                    deviceId, groupId, buttonId, sceneId);
        }
        else
        {
            sql = "INSERT INTO SCENES (%s,%s,%s,type) VALUES('%s','%s','%s','zwave')";
            database_actions(service_name, db, sql,
                    ST_DEVICE_ID, ST_GROUP_ID, ST_BUTTON_ID,
                    deviceId, groupId, buttonId);
        }
    }

    FREE_SEARCH_DATA_VAR(currentButtonId);
}

void update_senceId_database(char* service_name, sqlite3 *db,
                            char* deviceId, char* buttonId, char *sceneId)
{
    if(!deviceId || !buttonId || !sceneId)
    {
        SLOGE("missing deviceId or buttonId or sceneId\n");
        return;
    }

    /*alway update no need check exist or not because we dont have groupId here*/
    database_actions(service_name, db, 
                    "UPDATE SCENES set %s='%s' where "
                    "%s='%s' AND %s='%s'",
                    ST_SCENE_ID, sceneId, 
                    ST_DEVICE_ID, deviceId,
                    ST_BUTTON_ID, buttonId);
}

/*####################### thermostat setpoint ##########################*/
void set_thermostat_setpoint_value_database(char* service_name, sqlite3 *db, db_callback callback, 
                                char *deviceId, const char *thermostatMode, char *value, 
                                const char *unit)
{
    if(!value || !strlen(value))
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(tmp);

    searching_database(service_name, db, callback, &tmp,
                        "SELECT value from THERMOSTAT_SETPOINT where "
                        "%s='%s' AND %s='%s'",
                        ST_DEVICE_ID, deviceId,
                        ST_MODE, thermostatMode);
    if(tmp.len)
    {
        if(strcmp(value, tmp.value))
        {
            database_actions(service_name, db, 
                                "UPDATE THERMOSTAT_SETPOINT SET value='%s', unit='%s' "
                                "WHERE deviceId='%s' AND mode='%s' ", 
                                value, unit, deviceId, thermostatMode);
        }
    }
    else
    {
        database_actions(service_name, db, "INSERT INTO THERMOSTAT_SETPOINT (deviceId,mode,value,unit,type) "
                                        "VALUES('%s','%s','%s','%s','%s')", 
                                        deviceId, thermostatMode, value, unit, service_name);
    }
    FREE_SEARCH_DATA_VAR(tmp);
}
/*####################### support common device #######################*/
void set_property_database(char* service_name, sqlite3 *db, db_callback callback, 
                            const char *deviceId, const char *deviceType, char *propertyId,
                            const char *key, char *value)
{
    if(!value || !strlen(value))
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(tmp);
    char new_value[SIZE_1024B*2];
    memset(new_value, 0x00, sizeof(new_value));

    searching_database(service_name, db, callback, &tmp,
                        "SELECT value from MORE_PROPERTY where deviceId='%s' AND "
                        "deviceType='%s' AND propertyId='%s' AND key='%s'", 
                        deviceId, deviceType, propertyId, key);
    if(tmp.len)
    {
        if(strcmp(tmp.value, value))
        {
            database_actions(service_name, db, "UPDATE MORE_PROPERTY SET value='%s' "
                             "WHERE deviceId='%s' AND deviceType='%s' AND propertyId='%s' AND key='%s'", 
                             deviceId, deviceType, propertyId, key);
        }
    }
    else
    {
        database_actions(service_name, db, "INSERT INTO MORE_PROPERTY (deviceId,deviceType,propertyId,key,value,type) "
                            "VALUES('%s','%s','%s','%s','%s','%s')", 
                            deviceId, deviceType, propertyId, key, value, service_name);
    }
    FREE_SEARCH_DATA_VAR(tmp);
}

void set_register_database(char* service_name, sqlite3 *db, db_callback callback, 
                        const char *ID, const char* feature, const char* value,
                        const char* mode, int rotate_num)
{
    if(!value || !strlen(value))
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(tmp);
    char new_value[2048];
    memset(new_value, 0x00, sizeof(new_value));

    searching_database(service_name, db, callback, &tmp,
                        "SELECT register from FEATURES where deviceId='%s' AND featureId='%s';", 
                        ID, feature);
    if(tmp.len)
    {
        if(!strcmp(mode,ST_UPDATE))
        {
            int number_comma = countChars(tmp.value, ',');
            if(number_comma >= rotate_num-1)
            {
                char *ch = strchr(tmp.value, ',');
                SAFE_STRCPY(new_value, ch+1);
                snprintf(new_value+strlen(new_value), sizeof(new_value)-strlen(new_value), ",%s", value);
            }
            else
            {
                snprintf(new_value, sizeof(new_value), "%s,%s", tmp.value, value);
            }
        }
        else if(!strcmp(mode,ST_REPLACE))
        {
            snprintf(new_value, sizeof(new_value), "%s", value);
        }

        if(strcmp(new_value, tmp.value))
        {
            database_actions(service_name, db, "UPDATE FEATURES SET register='%s' "
                            "WHERE deviceId='%s' AND featureId='%s';", 
                                new_value, ID, feature);
        }
    }
    else
    {
        database_actions(service_name, db, "INSERT INTO FEATURES (deviceId,featureId,register,type) "
                            "VALUES('%s','%s','%s','%s')", 
                            ID, feature, value, service_name);
    }
    FREE_SEARCH_DATA_VAR(tmp);
}

void saving_thermostat_floatformat_database(char* service_name, sqlite3 *db, db_callback callback, 
                                    const char *ID, const char* unitName, char* unitFormat)
{
    SEARCH_DATA_INIT_VAR(tmp);
    searching_database(service_name, db, callback, &tmp,
                        "SELECT register from FEATURES where deviceId='%s' AND featureId='%s';", 
                        ID, ST_THERMOSTAT_SETPOINT_FORMAT);
    if(tmp.len)
    {
        json_object *jobj = VR_(create_json_object)(tmp.value);
        if(!jobj)
        {
            json_object *newOject = json_object_new_object();
            JSON_ADD_STRING_SAFE(newOject, unitName, unitFormat);
            database_actions(service_name, db, "UPDATE FEATURES SET register='%s' "
                            "WHERE deviceId='%s' AND featureId='%s';", 
                            json_object_to_json_string(newOject), 
                            ID, ST_THERMOSTAT_SETPOINT_FORMAT);
            json_object_put(newOject);
        }
        else
        {
            JSON_ADD_STRING_SAFE(jobj, unitName, unitFormat);
            database_actions(service_name, db, "UPDATE FEATURES SET register='%s' "
                            "WHERE deviceId='%s' AND featureId='%s';", 
                            json_object_to_json_string(jobj), 
                            ID, ST_THERMOSTAT_SETPOINT_FORMAT);
            json_object_put(jobj);
        }
    }
    else
    {
        json_object *newOject = json_object_new_object();
        JSON_ADD_STRING_SAFE(newOject, unitName, unitFormat);
        database_actions(service_name, db, "INSERT INTO FEATURES (deviceId,featureId,register,type) "
                        "VALUES('%s','%s','%s','%s')", 
                        ID, ST_THERMOSTAT_SETPOINT_FORMAT, 
                        json_object_to_json_string(newOject), service_name);
        json_object_put(newOject);
    }
    FREE_SEARCH_DATA_VAR(tmp);
}

/*####################### support alarm #######################*/
void set_alarm_value_database(char* service_name, sqlite3 *db, db_callback callback,
                                char *deviceId, const char *groupAlarm, const char* alarmTypeFinal)
{
    if(!groupAlarm || !alarmTypeFinal)
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(tmp);

    searching_database(service_name, db, callback, &tmp,
                        "SELECT alarmTypeFinal from ALARM where "
                        "%s='%s' AND %s='%s'",
                        ST_DEVICE_ID, deviceId, ST_GROUP_ALARM, groupAlarm);
    if(tmp.len)
    {
        if(strcmp(alarmTypeFinal, tmp.value))
        {
            database_actions(service_name, db, "UPDATE ALARM SET alarmTypeFinal='%s' "
                                "WHERE deviceId='%s' AND groupAlarm='%s' ",
                                alarmTypeFinal, deviceId, groupAlarm);
        }
    }
    else
    {
        database_actions(service_name, db, "INSERT INTO ALARM (deviceId,groupAlarm,alarmTypeFinal,type) "
                                        "VALUES('%s','%s','%s','%s')",
                                        deviceId, groupAlarm, alarmTypeFinal, service_name);
    }
    FREE_SEARCH_DATA_VAR(tmp);
}

char *get_alarm_value_database(char* service_name, sqlite3 *db, db_callback callback,
                                char *deviceId, const char *groupAlarm)
{
    char *alarmTypeFinal = NULL;
    SEARCH_DATA_INIT_VAR(alarmCurrentValue);
    searching_database(service_name, db, callback, &alarmCurrentValue,
                        "SELECT alarmTypeFinal from ALARM where %s='%s' and %s='%s'",
                        ST_DEVICE_ID, deviceId, ST_GROUP_ALARM, groupAlarm);
    if(alarmCurrentValue.len)
    {
        alarmTypeFinal = strdup(alarmCurrentValue.value);
    }
    FREE_SEARCH_DATA_VAR(alarmCurrentValue);

    return alarmTypeFinal;
}

void remove_alarm_database(char* service_name, sqlite3 *db,
                                    char* deviceId, const char *groupAlarm)
{
    if(!deviceId || !groupAlarm)
    {
        SLOGE("missing deviceId or groupAlarm\n");
        return;
    }

    database_actions(service_name, db, "DELETE FROM ALARM WHERE "
                                        "%s='%s' AND %s='%s'",
                                        ST_DEVICE_ID, deviceId,
                                        ST_GROUP_ALARM, groupAlarm);
}

void remove_register_database(char* service_name, sqlite3 *db, db_callback callback, 
                            const char *ID, const char* feature, const char* value)
{
    if(!value || !strlen(value))
    {
        return;
    }

    SEARCH_DATA_INIT_VAR(tmp);
    char new_value[SIZE_1024B], *ch, *save_tok;
    memset(new_value, 0x00, sizeof(new_value));

    searching_database(service_name, db, callback, &tmp,
                        "SELECT register from FEATURES where deviceId='%s' AND featureId='%s';", 
                        ID, feature);
    if(tmp.len && strstr(tmp.value, value))
    {
        ch = strtok_r(tmp.value, ",", &save_tok);
        while( ch != NULL ) 
        {
            printf( " %s\n", ch );
            if(strcmp(ch, value))
            {
                if(strlen(new_value))
                {
                    sprintf(new_value+strlen(new_value),",%s", ch);
                }
                else
                {
                    SAFE_STRCPY(new_value, ch);
                }
            }
            ch = strtok_r(NULL, ",", &save_tok);
        }
        database_actions(service_name, db, "UPDATE FEATURES SET register='%s' "
                            "WHERE deviceId='%s' AND featureId='%s';", 
                                new_value, ID, feature);
    }
    FREE_SEARCH_DATA_VAR(tmp);
}

int check_single_quote(char *input, char **output)
{
    int i;
    if(!input)
    {
        *output = NULL;
        return 0;
    }

    if(strchr(input, '\''))
    {
        int k = 0;
        size_t input_length = strlen(input);
        char *tmp = (char *)malloc(2*input_length);

        for (i=0; i< input_length; i++)
        {
            tmp[k++] = input[i];
            if(input[i] == '\'')
            {
                tmp[k++] = input[i];
            }
        }
        tmp[k] = '\0';
        *output = tmp;
        return 1;
    }
    else
    {
        *output = NULL;
        return 0;
    }
}