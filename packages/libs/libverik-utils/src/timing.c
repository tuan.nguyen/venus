#include "timing.h"

//get clock of system unit: ms
int timingGetClockSystem(sTiming *timer)
{
	return clock_gettime(CLOCK_MONOTONIC, &timer->startTime);
}

//stop get clock of system
int timingGetClockSystemStop(sTiming *timer)
{
	int ret;
	ret=clock_gettime(CLOCK_MONOTONIC, &timer->stopTime);

	if ((timer->stopTime.tv_nsec-timer->startTime.tv_nsec)<0) 
	{
		timer->elapsedTime.tv_sec = timer->stopTime.tv_sec-timer->startTime.tv_sec-1;
		timer->elapsedTime.tv_nsec = 1000000000+timer->stopTime.tv_nsec-timer->startTime.tv_nsec;
	} else 
	{
		timer->elapsedTime.tv_sec = timer->stopTime.tv_sec-timer->startTime.tv_sec;
		timer->elapsedTime.tv_nsec = timer->stopTime.tv_nsec-timer->startTime.tv_nsec;
	}
	return ret;
}

//get time (ms) from timingGetClockSystem to current
double timingGetElapsedMSec(sTiming *timer)
{

	clock_gettime(CLOCK_MONOTONIC, &timer->stopTime);

	if ((timer->stopTime.tv_nsec-timer->startTime.tv_nsec)<0) 
	{
		timer->elapsedTime.tv_sec = timer->stopTime.tv_sec-timer->startTime.tv_sec-1;
		timer->elapsedTime.tv_nsec = 1000000000+timer->stopTime.tv_nsec-timer->startTime.tv_nsec;
	} else 
	{
		timer->elapsedTime.tv_sec = timer->stopTime.tv_sec-timer->startTime.tv_sec;
		timer->elapsedTime.tv_nsec = timer->stopTime.tv_nsec-timer->startTime.tv_nsec;
	}

	return timer->elapsedTime.tv_sec*1000 + timer->elapsedTime.tv_nsec / 1000000.0;
}

//get time (us) from timingGetClockSystem to current
static double timingGetElapsedUSec(sTiming *timer)
{
	if(!timer)
	{
		return -1;
	}

	clock_gettime(CLOCK_MONOTONIC, &timer->stopTime);

	if ((timer->stopTime.tv_nsec-timer->startTime.tv_nsec)<0) 
	{
		timer->elapsedTime.tv_sec = timer->stopTime.tv_sec-timer->startTime.tv_sec-1;
		timer->elapsedTime.tv_nsec = 1000000000+timer->stopTime.tv_nsec-timer->startTime.tv_nsec;
	} 
	else 
	{
		timer->elapsedTime.tv_sec = timer->stopTime.tv_sec-timer->startTime.tv_sec;
		timer->elapsedTime.tv_nsec = timer->stopTime.tv_nsec-timer->startTime.tv_nsec;
	}

	return timer->elapsedTime.tv_sec*1000000 + timer->elapsedTime.tv_nsec / 1000.0;
}


void VR_(usleep)(useconds_t usec)
{
	sTiming txTiming;
	timingGetClockSystem(&txTiming);
	while(timingGetElapsedUSec(&txTiming) < usec)
	{
		usleep(100);
	}
}