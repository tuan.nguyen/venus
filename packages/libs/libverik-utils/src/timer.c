#include "timer.h"

typedef struct timerUnit
{
    timer_t *timerID;
    void    (*handler)(void *data);
    void    *data;
    int     attribute;
    struct timerUnit* next;
}timerUnit_t;

typedef struct __timerList
{
    struct timerUnit *head;
    struct timerUnit *tail;
    int size;
}timerList_t;

timerList_t gTimerList;

struct sigevent         te;
struct itimerspec       its;
struct sigaction        sa;
static  void timerHandler( int sig, siginfo_t *si, void *uc );


/*=========================== timerInit ====================================
** Function description
**      Init timer 
**      
** Side effects:
**
**--------------------------------------------------------------------------*/

int timerInit(void)
{
    gTimerList.head = NULL;
    gTimerList.tail = NULL;
    gTimerList.size = 0;

    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = timerHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGRTMIN, &sa, NULL) == -1)
    {
        fprintf(stderr, " Failed to setup signal handling for.\n");
        return TIMER_ERROR_SETUP_SIGNAL;
    }

    // Set and enable alarm 
    te.sigev_notify = SIGEV_SIGNAL;
    te.sigev_signo = SIGRTMIN;
    return TIMER_NO_ERROR;
}


/*=========================== timerStart ====================================
** Function description
**      Start the timer, call the callback after expireMS.
**      
** Side effects:
**
**--------------------------------------------------------------------------*/


int timerStart(timer_t *timerID, void(*handler)(void *), void *data, int expireMS, int attribute)
{
    int size;

    timerUnit_t *current = gTimerList.head;

    size = gTimerList.size;

    while(size-- > 0)
    {
        if(current->timerID == timerID)
        {
            printf("duplicate timer\n");
            timerCancel((current->timerID));
            break;
        }
        current = current->next;
    }

    // Set up signal handler. 
    te.sigev_value.sival_ptr = timerID;
    if (timer_create(CLOCK_REALTIME, &te, timerID))
    {
        fprintf(stderr,"Cannot create new timmer\n");
        return TIMER_ERROR_CREATE_TIMER;
    }

    its.it_interval.tv_sec = expireMS / 1000;
    its.it_interval.tv_nsec =(expireMS * 1000000)- (expireMS / 1000) * 1000000000;

    its.it_value.tv_sec = its.it_interval.tv_sec;
    its.it_value.tv_nsec =its.it_interval.tv_nsec;

    if (attribute == TIMER_ONETIME) 
    {
        its.it_interval.tv_sec = 0;
        its.it_interval.tv_nsec = 0;
    } 
    if (timer_settime(*timerID, 0, &its, NULL))
    {
        timer_delete(*timerID);
        fprintf(stderr,"Cannot set time new timer\n");
        return TIMER_ERROR_SET_TIME;
    }

    timerUnit_t *lTimer = (timerUnit_t*)malloc(sizeof(timerUnit_t));
    if(lTimer == NULL)
    {
        timer_delete(*timerID);
        fprintf(stderr,"Cannot alloc new timer\n");
        return TIMER_ERROR_ALLOC;
    }

    lTimer->timerID = timerID;
    lTimer->handler = handler;
    lTimer->data = data;
    lTimer->attribute = attribute;
    lTimer->next = NULL;

    if (gTimerList.head == NULL)
    {
        gTimerList.head = gTimerList.tail = lTimer;
    }
    else
    {
        gTimerList.tail->next = lTimer;
        gTimerList.tail = lTimer;
    }
    gTimerList.size++;
    
    return TIMER_NO_ERROR;
}

static void timerHandler( int sig, siginfo_t *si, void *uc )
{
    timer_t *tidp;
    tidp = si->si_value.sival_ptr;
    timerUnit_t *current = gTimerList.head;

    void (*handler)(void*) = NULL;
    void *data = NULL;
    while(current != NULL)
    {
        if (current->timerID == tidp)
        {
            if (current->handler != NULL)
            {
                handler = current->handler;
                data = current->data;
            }

            if (current->attribute == TIMER_ONETIME)
            {
                timerCancel((current->timerID));
            }

            if (handler != NULL) handler(data);

            return;
        }

        if(current) current = current->next;
    }
  
}

/*=========================== timerCancel ====================================
** Function description
**      Cancel the timer before timeout, ltimerID will be zero after cancel.
**      
** Side effects:
**
**--------------------------------------------------------------------------*/

int  timerCancel(timer_t* ltimerID)
{
    if (ltimerID == 0)
    {
        printf("TIMER_ERROR_NO_TIMER\n");
        return TIMER_ERROR_NO_TIMER;
    }

    timerUnit_t *previous = NULL;
    timerUnit_t *current = gTimerList.head;
    while(current != NULL)
    {
        if (ltimerID == current->timerID)
        {
            if (gTimerList.head == current) gTimerList.head = current->next;
            if (gTimerList.tail == current) gTimerList.tail = previous;

            if (previous != NULL) previous->next = current->next;
            if (*ltimerID) timer_delete(*ltimerID);
            free(current);
            current = NULL;
            gTimerList.size--;
            *ltimerID = 0;
            return TIMER_NO_ERROR;
        }
        previous = current;
        current = current->next;
    } 

    return TIMER_ERROR_NO_TIMER;  
}

/*====================== timerCancelOptional ================================
** Function description
**      Cancel the timer before timeout, ltimerID will be zero after cancel.
**      
** Side effects:
**
**--------------------------------------------------------------------------*/

int  timerCancelOptional(timer_t *ltimerID, int attribute)
{
    if (ltimerID == 0)
    {
        return TIMER_ERROR_NO_TIMER;
    }

    timerUnit_t *previous = NULL;
    timerUnit_t *current = gTimerList.head;

    while(current != NULL)
    {
        if (ltimerID == current->timerID)
        {
            if (gTimerList.head == current) gTimerList.head = current->next;
            if (gTimerList.tail == current) gTimerList.tail = previous;

            if (previous != NULL) previous->next=current->next;
            if (*ltimerID) timer_delete(*ltimerID);

            if (TIMER_CANCEL_FREE_DATA == attribute)
            {
                if (current->data)
                {
                    free(current->data);
                }
            }
            free(current);
            current = NULL;
            gTimerList.size--;
            *ltimerID = 0;
            return TIMER_NO_ERROR;
        }
        previous = current;
        current = current->next;
    } 
    return TIMER_ERROR_NO_TIMER;  

}


static int removePopTimerFromList(void)
{
    timerUnit_t *current = gTimerList.head;
    if (current != NULL)
    {
        gTimerList.head = current->next;
        if (*(current->timerID)) timer_delete(*(current->timerID));
        free(current);
        gTimerList.size--;
    }
    return TIMER_NO_ERROR;
}

/*========================== timerDelete ====================================
** Function description
**      Delete all of timers
**      
** Side effects:
**
**--------------------------------------------------------------------------*/

int timerDelete(void)
{
    while(gTimerList.head != NULL)
    {
        removePopTimerFromList();
    }
    return TIMER_NO_ERROR;
}