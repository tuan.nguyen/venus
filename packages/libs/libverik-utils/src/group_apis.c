#include <json-c/json.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "database.h"
#include "verik_utils.h"
#include "group_apis.h"
#include "slog.h"
#include "vr_rest.h"

#define NUMBER_DEVS_IN_GROUP 256

// /*printing the value corresponding to boolean, double, integer and strings*/
// void print_json_value(json_object *jobj){
//   enum json_type type;
//   type = json_object_get_type(jobj); /*Getting the type of the json object*/
//   switch (type) {
//     case json_type_boolean: printf("json_type_boolean\n");
//                          printf("value: %s\n", json_object_get_boolean(jobj)? "true": "false");
//                          break;
//     case json_type_double: printf("json_type_double\n");
//                         printf("value: %lf\n", json_object_get_double(jobj));
//                          break;
//     case json_type_int: printf("json_type_int\n");
//                         printf("value: %d\n", json_object_get_int(jobj));
//                          break;
//     case json_type_string: printf("json_type_string\n");
//                          printf("value: %s\n", json_object_get_string(jobj));
//                          break;
//   }

// }

/*Parsing the json object
  update groupDev database {"zwave":["A1", "A2"], "zigbee":[]}
  with action is add or remove.
*/
// static void group_action_process(json_object *jobj, char *devicetype, char *deviceid, char *action) 
// {
//     int found = 0;
//     enum json_type type;
//     json_object_object_foreach(jobj, key, val) 
//     { /*Passing through every array element*/
//         if(!strcmp(key, devicetype))
//         {
//             found = 1;
//             type = json_object_get_type(val);
//             if(json_type_array != type)
//             {
//                 continue;
//             }

//             json_object *jarray = jobj;
//             jarray = json_object_object_get(jobj, key);
//             if(!strcmp(action, ST_ADD))
//             {
//                 json_object_array_add(jarray, json_object_new_string(deviceid));
//             }
//             else
//             {
//                 int arraylen = json_object_array_length(jarray);
//                 if(arraylen <= 1)
//                 {
//                     json_object_object_del(jobj, key);
//                 }
//                 else
//                 {
//                     int i;
//                     json_object * jvalue;
//                     json_object *deviceIDlist = json_object_new_array();
//                     for (i=0; i< arraylen; i++)
//                     {
//                         jvalue = json_object_array_get_idx(jarray, i);
//                         if(strcmp(json_object_get_string(jvalue), deviceid))
//                         {
//                             json_object_array_add(deviceIDlist, json_object_new_string(json_object_get_string(jvalue)));
//                         }
//                         //printf("value: %s\n", json_object_get_string(jvalue));

//                         //printf("%s\n", json_object_to_json_string(deviceIDlist));
//                     }
//                     json_object_object_del(jobj, key);
//                     json_object_object_add(jobj, devicetype, deviceIDlist);
//                 }
//             }
//         }
//     }

//     if(!found)
//     {
//         if(!strcmp(action, ST_ADD))
//         {
//             json_object *deviceIDlist = json_object_new_array();
//             json_object_array_add(deviceIDlist, json_object_new_string(deviceid));
//             json_object_object_add(jobj, devicetype, deviceIDlist);
//         }
//     }
// }

//update groupId device property in FEATURE table
void update_device_groupid(char *devicetype, sqlite3 *db, db_callback callback, char *deviceid, char *groupid)
{
    char service[strlen(devicetype)+16];
    sprintf(service, "%s_handler", devicetype);

    set_register_database(service, db, callback, deviceid, ST_GROUP_ID, groupid, ST_REPLACE, 0);
}

//remove groupid in feature table
void remove_device_groupid(char *devicetype, sqlite3 *db, char *deviceid)
{
    char service[strlen(devicetype)+16];
    sprintf(service, "%s_handler", devicetype);

    database_actions(service, db, "DELETE from FEATURES where deviceId ='%s' and featureid='%s'", deviceid, ST_GROUP_ID);
}

/*Parsing the json object*/
void group_control_process(json_object * jobj, void* data, group_callback callback) 
{
    enum json_type type;
    json_object_object_foreach(jobj, key, val) 
    { /*Passing through every array element*/
        type = json_object_get_type(val);
        switch (type) 
        {
            case json_type_null:
            case json_type_boolean: 
            case json_type_double: 
            case json_type_int: 
            case json_type_string:
            case json_type_object:
                break;
            case json_type_array:
            {
                int i, arraylen;
                json_object * jvalue;
                json_object *jarray = jobj;
                jarray = json_object_object_get(jobj, key);
                arraylen = json_object_array_length(jarray);
                if(arraylen == 0)
                {
                    continue;
                }

                char devicelist[64*NUMBER_DEVS_IN_GROUP]={0};
                for (i=0; i< arraylen; i++)
                {
                    jvalue = json_object_array_get_idx(jarray, i);
                    if(i==0)
                    {
                        SAFE_STRCPY(devicelist, json_object_get_string(jvalue));
                    }
                    else
                    {
                        sprintf(devicelist+strlen(devicelist), ",%s", json_object_get_string(jvalue));
                    }
                }
                callback(data, key, (void*)devicelist, arraylen);
            
                break;
            } 
        }
    }
}

int remove_and_update_device_in_group(char *service, sqlite3 *db, db_callback callback, 
                                    char *shm, char *deviceid, char *devicetype, 
                                    char *newdeviceid)
{
//     // check device is in group or not
//     SEARCH_DATA_INIT_VAR(groupid);
//     searching_database(service, db, callback, &groupid,  
//                     "SELECT register from FEATURES where deviceId='%s' AND featureid='%s'", deviceid, ST_GROUP_ID);
//     if(!groupid.len)
//     {
//         goto done;   
//     }

//     SEARCH_DATA_INIT_VAR(device);
//     searching_database(service, db, callback, &device, 
//                         "SELECT device from GROUPS where groupId='%s'",
//                         groupid.value);

//     if(!device.len)
//     {
//         FREE_SEARCH_DATA_VAR(device);
//         goto done;
//     }

//     json_object * jobj = json_tokener_parse(device.value);
//     if(jobj)
//     {
//         /*update groupDev in group table*/
//         group_action_process(jobj, devicetype, deviceid, ST_REMOVE);

//         if(newdeviceid)/*replace with another device*/
//         {
//             group_action_process(jobj, devicetype, deviceid, ST_REMOVE);
//             update_device_groupid(devicetype, db, callback, newdeviceid, groupid.value);
//             group_action_process(jobj, devicetype, newdeviceid, ST_ADD);
//         }

//         const char *groupDev = json_object_to_json_string(jobj);

//         if(strlen(groupDev) < 4)
//         {
//             database_actions(service, db,
//                     "DELETE from GROUPS where groupId='%s'", groupid.value);

//             database_actions(service, db, "DELETE FROM DEVICES where deviceId='%s'", groupid.value);

//             char *cloud_group_id = VR_(get_cloudid_from_localid)(shm, groupid.value);
//             if(cloud_group_id)
//             {
//                 shm_update_data(shm, cloud_group_id, NULL, NULL, SHM_DELETE);
//             }
//             VR_(cloud_delete_group)(shm, groupid.value);

//             SAFE_FREE(cloud_group_id);
//         }
//         else
//         {
//             database_actions(service, db,
//                     "UPDATE GROUPS set device='%s' where groupId='%s'",
//                     groupDev, groupid.value);

//             SEARCH_DATA_INIT_VAR(name);
//             searching_database(service, db, callback, &name, 
//                                 "SELECT groupName from GROUPS where groupId='%s'",
//                                 groupid.value);

//             if(name.len)
//             {
//                 VR_(cloud_modify_group)(shm, groupid.value, name.value, (char *)groupDev);
//             }

//             FREE_SEARCH_DATA_VAR(name);
//         }

//         json_object_put(jobj);
//     }
    
//     FREE_SEARCH_DATA_VAR(device);
// done:
//     FREE_SEARCH_DATA_VAR(groupid);
    return 0;
}

/*group_dev is a local format with data {"zwave":["01","01"], "zigbee":"["ABCD"]"}
  cloud_group_dev is format cloud could understand: ["uuid1","uuid2"], need free after using
*/
static char *convert_group_dev_to_cloud_format(char *shm, char *group_dev)
{
    char *cloud_group_dev = NULL;

    if(!shm || !group_dev)
    {
        SLOGE("input invalid\n");
        return cloud_group_dev;
    }

    json_object *groupObj = VR_(create_json_object)(group_dev);
    if(!groupObj)
    {
        SLOGE("group_dev %s is not json object\n", group_dev);
        return cloud_group_dev;
    }

    int arraylen = 0;
    json_object *cloudArrayObj = json_object_new_array();

    enum json_type type;
    json_object_object_foreach(groupObj, deviceType, deviceList)
    {
        deviceType = deviceType;
        type = json_object_get_type(deviceList);
        if(json_type_array != type)
        {
            continue;
        }

        int i;
        json_object * jvalue;
        // json_object *jarray = groupObj;
        // jarray = json_object_object_get(groupObj, deviceType);
        arraylen = json_object_array_length(deviceList);

        for (i=0; i< arraylen; i++)
        {
            jvalue = json_object_array_get_idx(deviceList, i);
            const char *localId = json_object_get_string(jvalue);
            char *cloudId = VR_(get_cloudid_from_localid)(shm, (char*)localId);
            if(cloudId)
            {
                json_object_array_add(cloudArrayObj, json_object_new_string(cloudId));
            }
            SAFE_FREE(cloudId);
        }
    }

    arraylen = json_object_array_length(cloudArrayObj);

    if(arraylen>0)
    {
        cloud_group_dev = strdup(json_object_to_json_string(cloudArrayObj));
    }

    json_object_put(groupObj);
    json_object_put(cloudArrayObj);

    return cloud_group_dev;
}

static int cloud_request_create_group(char *group_name, char *group_dev, 
                                    char *id_return, size_t id_length)
{
    int ret = -1;
    char address[SIZE_1024B];

    if(!group_name || !strlen(group_name)
        || !group_dev || !strlen(group_dev)
        )
    {
        SLOGI("group_name %s invalid\n", group_name);
        return ret;
    }

    json_object *cloudDev = VR_(create_json_object)(group_dev);
    if(!cloudDev)
    {
        SLOGE("group_dev %s is not json format\n", group_dev);
        return ret;
    }

    char *token = VR_(read_option)(NULL, UCI_CLOUD_TOKEN);
    if(!token)
    {
        SLOGI("Token not found\n");
        return ret;
    }

    if(!strcmp(token, "default"))
    {
        SLOGI("Token invalid\n");
        free(token);
        return ret;
    }

    char *hubUUID = VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
    if(!hubUUID)
    {
        SLOGI("hubUUID not found\n");
        free(token);
        return ret;
    }

    char headerIn[SIZE_1024B]={0};
    snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);

    char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    if(!cloud_address)
    {
        snprintf(address, sizeof(address), GROUP_CREATE_DOMAIN, CLOUD_DOMAIN, hubUUID);
    }
    else
    {
        snprintf(address, sizeof(address), GROUP_CREATE_DOMAIN, cloud_address, hubUUID);
        SAFE_FREE(cloud_address);
    }

    json_object *postData = json_object_new_object();
    json_object_object_add(postData, ST_NAME, json_object_new_string(group_name));
    json_object_object_add(postData, ST_RESOURCE_UUIDS, cloudDev);

    char *result = NULL;
    ret = VR_(post_request)(address, headerIn, (char*)json_object_to_json_string(postData),
                            NULL, &result, DEFAULT_TIMEOUT);
    if(!ret)
    {
        SLOGI("######## RESPONSE = %s\n", result);
        if(!result || !strcmp(result, "{}"))
        {
            ret = -1;
            goto done;
        }

        json_object* jobj = VR_(create_json_object)(result);
        if(!jobj)
        {
            SLOGI("is not json format\n");
            ret = -1;
            goto done;
        }
        
        json_object *error = json_object_object_get(jobj, "error");
        if(error)
        {
            SLOGI("##### ERROR MESSAGE #### = %s\n", 
                    json_object_to_json_string(json_object_object_get(error, "message")));

            const char *code = json_object_get_string(json_object_object_get(error, "code"));
            if(!strcmp(GROUP_NAME_DUPPLICATE_CODE, code))
            {
                ret = GROUP_NAME_DUPPLICATE;
            }
            else
            {
                ret = -1;
            }
            json_object_put(jobj);
            goto done;
        }

        json_object *id = json_object_object_get(jobj, "id");
        if(id && id_return)
        {
            strlcpy(id_return, json_object_get_string(id), id_length);
        }

        json_object_put(jobj);
    }

done:
    json_object_put(postData);
    SAFE_FREE(result);
    SAFE_FREE(hubUUID);
    SAFE_FREE(token);

    return ret;
}

int VR_(cloud_create_group)(char *shm, char *group_name, char *group_dev, 
                            char *id_return, size_t id_length)
{
    int ret = -1;
    char *cloudDev = convert_group_dev_to_cloud_format(shm, group_dev);
    if(cloudDev)
    {
        ret = cloud_request_create_group(group_name, cloudDev, id_return, id_length);
        free(cloudDev);
    }
    return ret;
}

static int cloud_request_modify_group(char *group_id, char *group_name, char *group_dev)
{
    int ret = -1;
    char address[SIZE_1024B];

    if(!group_name || !strlen(group_name)
        || !group_dev || !strlen(group_dev)
        || !group_id || !strlen(group_id)
        )
    {
        SLOGI("group_name %s invalid\n", group_name);
        return ret;
    }

    json_object *cloudDev = VR_(create_json_object)(group_dev);
    if(!cloudDev)
    {
        SLOGE("group_dev %s is not json format\n", group_dev);
        return ret;
    }

    char *token = VR_(read_option)(NULL, UCI_CLOUD_TOKEN);
    if(!token)
    {
        SLOGI("Token not found\n");
        return ret;
    }

    if(!strcmp(token, "default"))
    {
        SLOGI("Token invalid\n");
        free(token);
        return ret;
    }

    char headerIn[SIZE_1024B]={0};
    snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);

    char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    if(!cloud_address)
    {
        snprintf(address, sizeof(address), GROUP_MODIFY_DOMAIN, CLOUD_DOMAIN, group_id);
    }
    else
    {
        snprintf(address, sizeof(address), GROUP_MODIFY_DOMAIN, cloud_address, group_id);
        SAFE_FREE(cloud_address);
    }

    json_object *postData = json_object_new_object();
    json_object_object_add(postData, ST_NAME, json_object_new_string(group_name));
    json_object_object_add(postData, ST_RESOURCE_UUIDS, cloudDev);

    char *result = NULL;
    ret = VR_(put_request)(address, headerIn, (char*)json_object_to_json_string(postData),
                           NULL, &result, DEFAULT_TIMEOUT);
    if(!ret)
    {
        SLOGI("######## RESPONSE = %s\n", result);
        if(!result)
        {
            ret = -1;
            goto done;
        }

        json_object* jobj = VR_(create_json_object)(result);
        if(!jobj)
        {
            SLOGI("is not json format\n");
            ret = -1;
            goto done;
        }
        
        json_object *error = json_object_object_get(jobj, "error");
        if(error)
        {
            SLOGI("##### ERROR MESSAGE #### = %s\n", 
                    json_object_to_json_string(json_object_object_get(error, "message")));
            const char *code = json_object_get_string(json_object_object_get(error, "code"));
            if(!strcmp(GROUP_NAME_DUPPLICATE_CODE, code))
            {
                ret = GROUP_NAME_DUPPLICATE;
            }
            else
            {
                ret = -1;
            }
        }
        json_object_put(jobj);
    }

done:
    json_object_put(postData);
    SAFE_FREE(result);
    SAFE_FREE(token);

    return ret;
}

int cloud_request_delete_group(char *group_id)
{
    int ret = -1;
    char address[SIZE_1024B];

    if(!group_id || !strlen(group_id))
    {
        SLOGI("group_id %s invalid\n", group_id);
        return ret;
    }

    char *token = VR_(read_option)(NULL, UCI_CLOUD_TOKEN);
    if(!token)
    {
        SLOGI("Token not found\n");
        return ret;
    }

    if(!strcmp(token, "default"))
    {
        SLOGI("Token invalid\n");
        free(token);
        return ret;
    }

    char headerIn[SIZE_1024B]={0};
    snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);

    char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    if(!cloud_address)
    {
        snprintf(address, sizeof(address), GROUP_MODIFY_DOMAIN, CLOUD_DOMAIN, group_id);
    }
    else
    {
        snprintf(address, sizeof(address), GROUP_MODIFY_DOMAIN, cloud_address, group_id);
        SAFE_FREE(cloud_address);
    }

    char *result = NULL;
    ret = VR_(delete_request)(address, headerIn, NULL, &result, DEFAULT_TIMEOUT);
    if(!ret)
    {
        SLOGI("######## RESPONSE = %s\n", result);
        if(!result)
        {
            ret = -1;
            goto done;
        }

        json_object* jobj = VR_(create_json_object)(result);
        if(!jobj)
        {
            SLOGI("is not json format\n");
            ret = -1;
            goto done;
        }
        
        json_object *error = json_object_object_get(jobj, "error");
        if(error)
        {
            SLOGI("##### ERROR MESSAGE #### = %s\n", 
                    json_object_to_json_string(json_object_object_get(error, "message")));
            json_object_put(jobj);
            ret = -1;
            goto done;
        }

        json_object_put(jobj);
    }

done:
    SAFE_FREE(result);
    SAFE_FREE(token);

    return ret;
}

int VR_(cloud_modify_group)(char *shm, char *group_id, char *group_name, char *group_dev)
{
    int ret = -1;
    char *cloud_dev = convert_group_dev_to_cloud_format(shm, group_dev);
    char *cloud_group_id = VR_(get_cloudid_from_localid)(shm, group_id);
    if(cloud_dev && cloud_group_id)
    {
        ret = cloud_request_modify_group(cloud_group_id, group_name, cloud_dev);
    }
    SAFE_FREE(cloud_dev);
    SAFE_FREE(cloud_group_id);
    return ret;
}

int VR_(cloud_delete_group)(char *shm, char *group_id)
{
    int ret = -1;
    char *cloud_group_id = VR_(get_cloudid_from_localid)(shm, group_id);
    if(cloud_group_id)
    {
        ret = cloud_request_delete_group(cloud_group_id);
        if(!ret)
        {
            shm_update_data(shm, cloud_group_id, NULL, NULL, SHM_DELETE);
        }
    }
    SAFE_FREE(cloud_group_id);
    return ret;
}

void VR_(update_group_Etag)(void)
{
    VR_(write_option)(NULL, GROUP_ETAG_ADDR"=1");
}

int VR_(check_group_dev_valid)(char *groupDev)
{
    int res = 0;
    if(!groupDev)
    {
        return res;
    }

    json_object *jobj = VR_(create_json_object)(groupDev);
    if(!jobj)
    {
        return res;
    }

    enum json_type type;
    json_object_object_foreach(jobj, deviceType, deviceList)
    {
        deviceType = deviceType;
        type = json_object_get_type(deviceList);
        if(json_type_array != type)
        {
            res = 0;
            break;
        }

        int arraylen = json_object_array_length(deviceList);
        if(arraylen > 0)
        {
            res = 1;
        }
    }
    json_object_put(jobj);
    return res;
}