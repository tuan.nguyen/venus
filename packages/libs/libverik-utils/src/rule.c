#include <pthread.h>
#include <stdint.h>
#include <json-c/json.h>
#include <string.h>
#include <stdio.h>

#include "slog.h"
#include "VR_define.h"
#include "verik_utils.h"
#include "vr_rest.h"
#include "database.h"
#include "rule.h"

static void rule_trigger_report(char *ruleId, uint64_t timeStamp)
{
    if(!ruleId)
    {
        SLOGE("ruleId not found\n");
        return;
    }

    char *token = VR_(read_option)(NULL, UCI_CLOUD_TOKEN);
    if(!token)
    {
        SLOGI("Token not found\n");
        return;
    }

    if(!strcmp(token, "default"))
    {
        SLOGI("Token invalid\n");
        free(token);
        return;
    }

    char address[SIZE_1024B], headerIn[SIZE_1024B];
    snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);
    // int64_t timestamp = strtoull(timeStamp, NULL, 10);

    json_object *ruleTrigger = json_object_new_object();
    json_object_object_add(ruleTrigger, ST_CLOUD_TIME_STAMP, json_object_new_int64(timeStamp));

    char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    if(!cloud_address)
    {
        snprintf(address, sizeof(address), RULE_TRIGGER_EVENT_DOMAIN, CLOUD_DOMAIN, ruleId);
    }
    else
    {
        snprintf(address, sizeof(address), RULE_TRIGGER_EVENT_DOMAIN, cloud_address, ruleId);
        free(cloud_address);
    }

    // char *result = NULL;
    // res = VR_(post_request)(address, NULL, (char*)json_object_to_json_string(ruleTrigger), NULL, &result);
    adding_curl_request_list(HTTP_POST_REQUEST, address, headerIn, (char*)json_object_to_json_string(ruleTrigger), NULL);

    // SLOGI("######## RESPONSE = %s\n", result);
    json_object_put(ruleTrigger);
    // SAFE_FREE(result);
    SAFE_FREE(token);

    return;
}

// static void trigger_report_cb(void *data)
// {
//     char *ruleId = (char*)data;
//     if(!ruleId)
//     {
//         return;
//     }
//     SLOGI("start report rule \n");
//     uint64_t timeStamp = (unsigned)time(NULL);
//     rule_trigger_report(ruleId, timeStamp);
//     SAFE_FREE(ruleId);
// }

void rule_report_timeline(char *ruleId)
{
    if(!ruleId)
    {
        return;
    }

    uint64_t timeStamp = (unsigned)time(NULL);
    rule_trigger_report(ruleId, timeStamp);

    // size_t length = strlen(ruleId);
    // char *ruleIdCb = (char*)malloc(length+1);
    // strcpy(ruleIdCb, ruleId);

    // pthread_t trigger_report;
    // pthread_create(&trigger_report, NULL, (void *)&trigger_report_cb, ruleIdCb);
    // pthread_detach(trigger_report);
}

int trigger_rule_via_shell_script(char *ruleId, char *actions)
{
    if(!ruleId || !actions)
    {
        SLOGE("ruleId || actions not found\n");
        return -1;
    }

    char *command = (char *)malloc(2*strlen(actions) + SIZE_1024B);
    sprintf(command, "/etc/rule_control.sh \"%s\" \"dontcare\" \"%s\" &", ruleId, actions);
    VR_(execute_system)(command);
    free(command);
    return 0;
}