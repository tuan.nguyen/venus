#include <stdio.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/stat.h>
#include <openssl/crypto.h>

#include <unistd.h>
#include "verik_utils.h"
#include "vr_rest.h"
#include "slog.h"

// typedef size_t (*writefunc_cb)(void *, size_t , size_t , void *);

static uint8_t curl_request_thread_enable = 1;

static curl_request_queue_t g_curl_request;
static pthread_mutex_t g_curl_list_queueMutex;

struct string {
    char *ptr;
    size_t len;
};

struct CRYPTO_dynlock_value
{
    pthread_mutex_t mutex;
};

/**
 * OpenSSL allocate and initialize dynamic crypto lock.
 *
 * @param    file    source file name
 * @param    line    source file line number
 */
static struct CRYPTO_dynlock_value *dyn_create_function(const char *file, int line)
{
    struct CRYPTO_dynlock_value *value;

    value = (struct CRYPTO_dynlock_value *)
        malloc(sizeof(struct CRYPTO_dynlock_value));
    if (!value) {
        goto err;
    }
    pthread_mutex_init(&value->mutex, NULL);

    return value;

  err:
    return (NULL);
}

/**
 * OpenSSL dynamic locking function.
 *
 * @param    mode    lock mode
 * @param    l        lock structure pointer
 * @param    file    source file name
 * @param    line    source file line number
 * @return    none
 */
static void dyn_lock_function(int mode, struct CRYPTO_dynlock_value *l,
                              const char *file, int line)
{
    if (mode & CRYPTO_LOCK) {
        pthread_mutex_lock(&l->mutex);
    } else {
        pthread_mutex_unlock(&l->mutex);
    }
}

/**
 * OpenSSL destroy dynamic crypto lock.
 *
 * @param    l        lock structure pointer
 * @param    file    source file name
 * @param    line    source file line number
 * @return    none
 */

static void dyn_destroy_function(struct CRYPTO_dynlock_value *l,
                                 const char *file, int line)
{
    pthread_mutex_destroy(&l->mutex);
    free(l);
}

/**
 * Initialize TLS library.
 *
 * @return    0 on success, -1 on error
 */
int tls_init(void)
{
    /* dynamic locks callbacks */
    CRYPTO_set_dynlock_create_callback(dyn_create_function);
    CRYPTO_set_dynlock_lock_callback(dyn_lock_function);
    CRYPTO_set_dynlock_destroy_callback(dyn_destroy_function);

    return (0);
}

/**
 * Cleanup TLS library.
 *
 * @return    0
 */
int tls_cleanup(void)
{
    CRYPTO_set_dynlock_create_callback(NULL);
    CRYPTO_set_dynlock_lock_callback(NULL);
    CRYPTO_set_dynlock_destroy_callback(NULL);

    return (0);
} 

static void init_string(struct string *s) 
{
    s->len = 0;
    s->ptr = malloc(s->len+1);
    if (s->ptr == NULL) 
    {
        fprintf(stderr, "malloc() failed\n");
        exit(EXIT_FAILURE);
    }
    s->ptr[0] = '\0';
}

static size_t writefunc_header(void *ptr, size_t size, size_t nmemb, void *output)
{
    struct string *s = (struct string *)output;
    size_t new_len = s->len + size*nmemb;
    s->ptr = realloc(s->ptr, new_len+1);
    if (s->ptr == NULL) 
    {
        fprintf(stderr, "realloc() failed\n");
        exit(EXIT_FAILURE);
    }
    memcpy(s->ptr+s->len, ptr, size*nmemb);
    s->ptr[new_len] = '\0';
    s->len = new_len;

    return size*nmemb;
};

static size_t writefunc_response(void *ptr, size_t size, size_t nmemb, void *output)
{
    struct string *s = (struct string *)output;
    size_t new_len = s->len + size*nmemb;
    s->ptr = realloc(s->ptr, new_len+1);
    if (s->ptr == NULL) 
    {
        fprintf(stderr, "realloc() failed\n");
        exit(EXIT_FAILURE);
    }
    memcpy(s->ptr+s->len, ptr, size*nmemb);
    s->ptr[new_len] = '\0';
    s->len = new_len;

    return size*nmemb;
};

static int _get_request(CURL *curl_t, char *address, char *headerIn, char *data,
                        void *headerOut, void *response, long timeout)
{
    CURL *curl = curl_t;
    CURLcode res = CURLE_FAILED_INIT;

    if(!curl)
    {
        curl_global_init(CURL_GLOBAL_SSL);
        curl = curl_easy_init();
        if(!curl)
        {
            curl_global_cleanup();
            return res;
        }
    }

    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "Accept: application/json");
    if(headerIn)
    {
        headers = curl_slist_append(headers, headerIn);
    }
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_URL, address);

    if(headerOut)
    {
        curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, writefunc_header);
        curl_easy_setopt(curl, CURLOPT_WRITEHEADER, headerOut);
    }

    if(response)
    {
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc_response);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, response);
    }

    if(data)
    {
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
    }
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout);
    curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
    curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "PEM");
    curl_easy_setopt(curl, CURLOPT_CAINFO, CERTIFICATE_FILE);//for openssl
    curl_easy_setopt(curl, CURLOPT_CAPATH, CERTIFICATE_PATH);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
    curl_easy_setopt(curl, CURLOPT_NOSIGNAL,1L);
    // curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);//debug
    /* Perform the request, res will get the return code */ 
    res = curl_easy_perform(curl);
    /* Check for errors */ 
    if(res != CURLE_OK)
    {
        SLOGE("curl_easy_perform() failed: %d: %s\n",
              res, curl_easy_strerror(res));
    }

    if(!curl_t)
    {
        curl_easy_cleanup(curl);
        curl_global_cleanup();
    }

    if(headers)
    {
        curl_slist_free_all(headers);
    }
    return res;
}

/*
need free response, header after using
*/
int VR_(get_request)(char *address, char *headerIn, char *data,
                     char **headerOut, char **response, long timeout)
{
    if(!address || !response)
    {
        SLOGE("address or response is null\n");
        return -1;
    }

    SLOGI("get_request infor: address %s, data %s\n", address, data);
    int res;
    struct string httpHeader;
    init_string(&httpHeader);
    struct string httpResponse;
    init_string(&httpResponse);

    res = _get_request(NULL, address, headerIn, data,
                      (void *)&httpHeader, (void *)&httpResponse, timeout);

    if(headerOut)
    {  
        *headerOut = httpHeader.ptr;
    }
    else
    {
        free(httpHeader.ptr);
    }

    if(response)
    {
        *response = httpResponse.ptr;
    }
    else
    {
        free(httpResponse.ptr);
    }
    return res;
}

static int _post_request(CURL *curl_t, char *address, char *headerIn, char *data,
                         void *headerOut, void *response, long timeout)
{
    CURL *curl = curl_t;
    CURLcode res = CURLE_FAILED_INIT;

    if(!curl)
    {
        curl_global_init(CURL_GLOBAL_SSL);
        curl = curl_easy_init();
        if(!curl)
        {
            curl_global_cleanup();
            return res;
        }
    }

    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "Accept: application/json");
    if(headerIn)
    {
        headers = curl_slist_append(headers, headerIn);
    }

    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_URL, address);

    if(headerOut)
    {
        curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, writefunc_header);
        curl_easy_setopt(curl, CURLOPT_WRITEHEADER, headerOut);
    }

    if(response)
    {
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc_response);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, response);
    }
    
    if(data)
    {
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
    }
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout);
    curl_easy_setopt(curl, CURLOPT_POST, 1);
    curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "PEM");
    curl_easy_setopt(curl, CURLOPT_CAINFO, CERTIFICATE_FILE);//for openssl
    curl_easy_setopt(curl, CURLOPT_CAPATH, CERTIFICATE_PATH);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
    curl_easy_setopt(curl, CURLOPT_NOSIGNAL,1L);
    // curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);//debug
    /* Perform the request, res will get the return code */
    res = curl_easy_perform(curl);
    /* Check for errors */
    if(res != CURLE_OK)
    {
        SLOGE("curl_easy_perform() failed: %d: %s\n",
              res, curl_easy_strerror(res));
    }

    if(!curl_t)
    {
        curl_easy_cleanup(curl);
        curl_global_cleanup();
    }

    if(headers)
    {
        curl_slist_free_all(headers);
    }
    return res;
}

/*
need free result after using
*/
int VR_(post_request)(char *address, char *headerIn, char *data,
                      char **header, char **response, long timeout)
{
    if(!address || !response)
    {
        SLOGE("address or response is null\n");
        return -1;
    }

    SLOGI("post_request infor: address %s, data %s\n", address, data);
    int res;
    struct string httpHeader;
    init_string(&httpHeader);
    struct string httpResponse;
    init_string(&httpResponse);

    res = _post_request(NULL, address, headerIn, data,
                        (void *)&httpHeader, (void *)&httpResponse, timeout);
    if(header)
    {  
        *header = httpHeader.ptr;
    }
    else
    {
        free(httpHeader.ptr);
    }

    if(response)
    {
        *response = httpResponse.ptr;
    }
    else
    {
        free(httpResponse.ptr);
    }
    return res;
}

static int _put_request(CURL *curl_t, char *address, char *headerIn,
                        char *data, void *headerOut, void *response, long timeout)
{
    CURL *curl = curl_t;
    CURLcode res = CURLE_FAILED_INIT;

    if(!curl)
    {
        curl_global_init(CURL_GLOBAL_SSL);
        curl = curl_easy_init();
        if(!curl)
        {
            curl_global_cleanup();
            return res;
        }
    }

    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "Accept: application/json");
    if(headerIn)
    {
        headers = curl_slist_append(headers, headerIn);
    }
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_URL, address);

    if(headerOut)
    {
        curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, writefunc_header);
        curl_easy_setopt(curl, CURLOPT_WRITEHEADER, headerOut);
    }

    if(response)
    {
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc_response);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, response);
    }    

    if(data)
    {
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
    }
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout);
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "PEM");
    curl_easy_setopt(curl, CURLOPT_CAINFO, CERTIFICATE_FILE);//for openssl
    curl_easy_setopt(curl, CURLOPT_CAPATH, CERTIFICATE_PATH);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
    curl_easy_setopt(curl, CURLOPT_NOSIGNAL,1L);
    // curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);//debug
    /* Perform the request, res will get the return code */ 
    res = curl_easy_perform(curl);
    /* Check for errors */ 
    if(res != CURLE_OK)
    {
        SLOGE("curl_easy_perform() failed: %d: %s\n",
              res, curl_easy_strerror(res));
    }

    if(!curl_t)
    {
        curl_easy_cleanup(curl);
        curl_global_cleanup();
    }

    if(headers)
    {
        curl_slist_free_all(headers);
    }
    return res;
}

/*
need free result after using
*/
int VR_(put_request)(char *address, char *headerIn, char *data,
                     char **header, char **response, long timeout)
{
    if(!address || !response)
    {
        SLOGE("address or response is null\n");
        return -1;
    }

    SLOGI("put_request infor: address %s, data %s\n", address, data);
    int res;
    struct string httpHeader;
    init_string(&httpHeader);
    struct string httpResponse;
    init_string(&httpResponse);

    res = _put_request(NULL, address, headerIn, data,
                       (void *)&httpHeader, (void *)&httpResponse, timeout);
    if(header)
    {  
        *header = httpHeader.ptr;
    }
    else
    {
        free(httpHeader.ptr);
    }

    if(response)
    {
        *response = httpResponse.ptr;
    }
    else
    {
        free(httpResponse.ptr);
    }

    return res;
}

static int _patch_request(CURL *curl_t, char *address, char *headerIn,
                          char *data, void *headerOut, void *response, long timeout)
{
    CURL *curl = curl_t;
    CURLcode res = CURLE_FAILED_INIT;

    if(!curl)
    {
        curl_global_init(CURL_GLOBAL_SSL);
        curl = curl_easy_init();
        if(!curl)
        {
            curl_global_cleanup();
            return res;
        }
    }

    struct curl_slist *headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "Accept: application/json");
    if(headerIn)
    {
        headers = curl_slist_append(headers, headerIn);
    }
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    curl_easy_setopt(curl, CURLOPT_URL, address);

    if(headerOut)
    {
        curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, writefunc_header);
        curl_easy_setopt(curl, CURLOPT_WRITEHEADER, headerOut);
    }

    if(response)
    {
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc_response);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, response);
    }

    if(data)
    {
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
    }
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout);
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PATCH");
    curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "PEM");
    curl_easy_setopt(curl, CURLOPT_CAINFO, CERTIFICATE_FILE);//for openssl
    curl_easy_setopt(curl, CURLOPT_CAPATH, CERTIFICATE_PATH);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
    curl_easy_setopt(curl, CURLOPT_NOSIGNAL,1L);
    // curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);//debug
    /* Perform the request, res will get the return code */ 
    res = curl_easy_perform(curl);
    /* Check for errors */ 
    if(res != CURLE_OK)
    {
        SLOGE("curl_easy_perform() failed: %d: %s\n",
              res, curl_easy_strerror(res));
    }

    if(!curl_t)
    {
        curl_easy_cleanup(curl);
        curl_global_cleanup();
    }

    if(headers)
    {
        curl_slist_free_all(headers);
    }
    return res;
}

/*
need free result after using
*/
int VR_(patch_request)(char *address, char *headerIn, char *data,
                       char **header, char **response, long timeout)
{
    if(!address || !response)
    {
        SLOGE("address or response is null\n");
        return -1;
    }

    SLOGI("patch_request infor: address %s, data %s\n", address, data);
    int res;
    struct string httpHeader;
    init_string(&httpHeader);
    struct string httpResponse;
    init_string(&httpResponse);

    res = _patch_request(NULL, address, headerIn, data,
                         (void *)&httpHeader, (void *)&httpResponse, timeout);
    if(header)
    {  
        *header = httpHeader.ptr;
    }
    else
    {
        free(httpHeader.ptr);
    }

    if(response)
    {
        *response = httpResponse.ptr;
    }
    else
    {
        free(httpResponse.ptr);
    }

    return res;
}

static int _delete_request(CURL *curl_t, char *address, char *headerIn,
                           void *headerOut, void *response, long timeout)
{
    CURL *curl = curl_t;
    CURLcode res = CURLE_FAILED_INIT;

    if(!curl)
    {
        curl_global_init(CURL_GLOBAL_SSL);
        curl = curl_easy_init();
        if(!curl)
        {
            curl_global_cleanup();
            return res;
        }
    }

    struct curl_slist *headers = NULL;
    curl_easy_setopt(curl, CURLOPT_URL, address);
    if(headerIn)
    {
        headers = curl_slist_append(headers, headerIn);
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    }

    if(headerOut)
    {
        curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, writefunc_header);
        curl_easy_setopt(curl, CURLOPT_WRITEHEADER, headerOut);
    }

    if(response)
    {
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc_response);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, response);
    }
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, timeout);

    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "PEM");
    curl_easy_setopt(curl, CURLOPT_CAINFO, CERTIFICATE_FILE);//for openssl
    curl_easy_setopt(curl, CURLOPT_CAPATH, CERTIFICATE_PATH);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
    curl_easy_setopt(curl, CURLOPT_NOSIGNAL,1L);
    // curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);//debug
    /* Perform the request, res will get the return code */ 
    res = curl_easy_perform(curl);
    /* Check for errors */ 
    if(res != CURLE_OK)
    {
        SLOGE("curl_easy_perform() failed: %d: %s\n",
              res, curl_easy_strerror(res));
    }

    if(!curl_t)
    {
        curl_easy_cleanup(curl);
        curl_global_cleanup();
    }

    if(headers)
    {
        curl_slist_free_all(headers);
    }
    return res;
}

/*
need free result after using
*/
int VR_(delete_request)(char *address, char *headerIn, char **header,
                        char **response, long timeout)
{
    if(!address || !response)
    {
        SLOGE("address or response is null\n");
        return -1;
    }

    SLOGI("delete_request infor: address %s\n", address);
    int res;
    struct string httpHeader;
    init_string(&httpHeader);
    struct string httpResponse;
    init_string(&httpResponse);

    res = _delete_request(NULL, address, headerIn,
                         (void *)&httpHeader, (void *)&httpResponse, timeout);
    if(header)
    {  
        *header = httpHeader.ptr;
    }
    else
    {
        free(httpHeader.ptr);
    }

    if(response)
    {
        *response = httpResponse.ptr;
    }
    else
    {
        free(httpResponse.ptr);
    }

    return res;
}

int VR_(check_internet_connection)(char *address)
{
    CURL *curl;
    CURLcode res = CURLE_FAILED_INIT;
    curl_global_init(CURL_GLOBAL_SSL);
    curl = curl_easy_init();
    if(curl) 
    {
        struct curl_slist *headers = NULL;
        headers = curl_slist_append(headers, "Content-Type: application/json");
        headers = curl_slist_append(headers, "Accept: application/json");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
        curl_easy_setopt(curl, CURLOPT_URL, address);
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, DEFAULT_TIMEOUT);

        curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "PEM");
        curl_easy_setopt(curl, CURLOPT_CAINFO, CERTIFICATE_FILE);//for openssl
        curl_easy_setopt(curl, CURLOPT_CAPATH, CERTIFICATE_PATH);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
        // curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);//debug
        curl_easy_setopt(curl, CURLOPT_NOSIGNAL,1L);
        /* Perform the request, res will get the return code */ 
        res = curl_easy_perform(curl);
        /* Check for errors */ 
        if(res != CURLE_OK)
        {
            SLOGE("curl_easy_perform() failed: %d: %s\n",
                  res, curl_easy_strerror(res));
        }

        curl_easy_cleanup(curl);
    }
    curl_global_cleanup();
    SLOGI("check_internet_connection = %d\n", res);
    return res;
}

struct FtpFile {
    const char *filename;
    FILE *stream;
};

static size_t my_fwrite(void *buffer, size_t size, size_t nmemb, void *stream)
{
    struct FtpFile *out=(struct FtpFile *)stream;
    if(out && !out->stream) 
    {
        /* open file for writing */
        out->stream=fopen(out->filename, "wb");
        if(!out->stream)
        {
            return -1; /* failure, can't open file to write */
        }
        
    }
    return fwrite(buffer, size, nmemb, out->stream);
}

int VR_(download_file)(char *url, const char *filename, const char *user_password,
                       download_progess_cb download_cb, void *data)
{
    CURL *curl;
    CURLcode res = CURLE_BAD_DOWNLOAD_RESUME;
    struct FtpFile ftpfile={
        filename, /* name to store the file as if successful */
        NULL
    };

    curl_global_init(CURL_GLOBAL_DEFAULT);

    curl = curl_easy_init();
    if(curl) 
    {
        if(download_cb && data)
        {
            curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, download_cb);
            curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, data);
        }

// #if LIBCURL_VERSION_NUM >= 0x072000
//         curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, xferinfo);
//         curl_easy_setopt(curl, CURLOPT_XFERINFODATA, &prog);
// #endif
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
        if(user_password)
        {
            curl_easy_setopt(curl, CURLOPT_USERPWD, user_password);
        }

        // curl_easy_setopt(curl, CURLOPT_TIMEOUT, DOWNLOAD_TIMEOUT);
        /* abort if slower than 30 bytes/sec during 120 seconds */
        curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, DEFAULT_TIMEOUT);
        curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 30L);

        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, my_fwrite);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ftpfile);
        curl_easy_setopt(curl, CURLOPT_CAINFO, CERTIFICATE_FILE);//for openssl
        curl_easy_setopt(curl, CURLOPT_CAPATH, CERTIFICATE_PATH);
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "curl/7.53.1");
        curl_easy_setopt(curl, CURLOPT_NOSIGNAL,1L);
        // curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "PEM");
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        // curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

        res = curl_easy_perform(curl);

        curl_easy_cleanup(curl);
    }

    if(ftpfile.stream)
    {
        fclose(ftpfile.stream); /* close the local file */
    }
    
    curl_global_cleanup();
    return res;
}

/*post resource to cloud with timeout(s)*/
int VR_(post_resources)(char *data, char *id_return, size_t length, long timeout)
{
    int ret;
    char address[SIZE_1024B];

    if(!data || !strlen(data))
    {
        SLOGI("data %s invalid\n", data);
        return -1;
    }

    char *token = VR_(read_option)(NULL, UCI_CLOUD_TOKEN);
    if(!token)
    {
        SLOGI("Token not found\n");
        return -1;
    }

    if(!strcmp(token, "default"))
    {
        SLOGI("Token invalid\n");
        free(token);
        return -1;
    }

    char *hubUUID = VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
    if(!hubUUID)
    {
        SLOGI("hubUUID not found\n");
        free(token);
        return -1;
    }

    char headerIn[SIZE_1024B]={0};
    snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);

    char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    if(!cloud_address)
    {
        snprintf(address, sizeof(address), CLOUD_REGISTER_ADDRESS"/%s/resources", CLOUD_DOMAIN, hubUUID);
    }
    else
    {
        snprintf(address, sizeof(address), CLOUD_REGISTER_ADDRESS"/%s/resources", cloud_address, hubUUID);
        free(cloud_address);
    }

    /*alway update resource etag when having a post resource request*/
    VR_(write_option)(NULL, UCI_RESOURCE_ETAG"=1");

    char *result = NULL;
    ret = VR_(post_request)(address, headerIn, data, NULL, &result, timeout);
    
    if(ret || !result)
    {
        goto post_resources_done;
    }

    SLOGI("######## RESPONSE = %s\n", result);
    if(!strcmp(result, "{}"))
    {
        goto post_resources_done;
    }

    json_object* jobj = VR_(create_json_object)(result);
    if(!jobj)
    {
        goto post_resources_done;
    }

    json_object *error = json_object_object_get(jobj, "error");
    if(error)
    {
        json_object *message_obj = NULL;
        if(json_object_object_get_ex(error, ST_MESSAGE, &message_obj))
        {
            const char *message = json_object_to_json_string(message_obj);
            SLOGI("##### ERROR MESSAGE #### = %s\n", message);
            ret = -1;
        }

        json_object *status_obj = NULL;
        if(json_object_object_get_ex(error, ST_STATUS, &status_obj))
        {
            int status = json_object_get_int(status_obj);
            SLOGI("##### STATUS MESSAGE #### = %d\n", status);
            if(status == 400)
            {
                json_object *cur_resource_obj = NULL;
                if(json_object_object_get_ex(error, ST_CLOUD_CURRENT_RESOURCE_ID, &cur_resource_obj))
                {
                    const char *cur_resource = json_object_get_string(cur_resource_obj);
                    if(cur_resource && id_return)
                    {
                        strncpy(id_return, cur_resource, length-1);
                        id_return[length-1]='\0';
                    }
                }
                ret = 0;
            }
        }
        
        json_object_put(jobj);
        goto post_resources_done;
    }

    json_object *id = json_object_object_get(jobj, "id");
    if(id && id_return)
    {
        strncpy(id_return, json_object_get_string(id), length-1);
        id_return[length-1]='\0';
    }

    json_object_put(jobj);

post_resources_done:
    SAFE_FREE(result);
    SAFE_FREE(hubUUID);
    SAFE_FREE(token);

    return ret;
}

int VR_(delete_resources)(char *resource_id, long timeout)
{
    int ret;
    char address[SIZE_1024B];

    char *token = VR_(read_option)(NULL, UCI_CLOUD_TOKEN);
    if(!token)
    {
        SLOGI("Token not found\n");
        return -1;
    }

    if(!strcmp(token, "default"))
    {
        SLOGI("Token invalid\n");
        free(token);
        return -1;
    }

    char *hubUUID = VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
    if(!hubUUID)
    {
        SLOGI("hubUUID not found\n");
        free(token);
        return -1;
    }

    char headerIn[SIZE_1024B]={0};
    snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);

    char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    if(!cloud_address)
    {
        snprintf(address,sizeof(address), CLOUD_REGISTER_ADDRESS"/%s/resources/%s", CLOUD_DOMAIN,
                    hubUUID, resource_id);
    }
    else
    {
        snprintf(address,sizeof(address), CLOUD_REGISTER_ADDRESS"/%s/resources/%s", cloud_address,
                    hubUUID, resource_id);
        free(cloud_address);
    }

    char *result = NULL;
    ret = VR_(delete_request)(address, headerIn, NULL, &result, timeout);

    if(ret)
    {
        goto delete_resources_done;
    }

    SLOGI("######## RESPONSE = %s\n", result);
    if(!strcmp(result, "{}"))
    {
        goto delete_resources_done;
    }

    json_object* jobj = VR_(create_json_object)(result);
    if(!jobj)
    {
        goto delete_resources_done;
    }

    json_object *error = json_object_object_get(jobj, "error");
    if(error)
    {
        SLOGI("##### ERROR MESSAGE #### = %s\n", 
                json_object_to_json_string(json_object_object_get(error, "message")));

        if(!strcmp(json_object_to_json_string(json_object_object_get(error, "status")),
                "404"))
        {
            ret = 0;
        }
        else
        {
            ret = -1;
        }
    }
    json_object_put(jobj);

delete_resources_done:
    SAFE_FREE(result);
    SAFE_FREE(hubUUID);
    SAFE_FREE(token);
    return ret;
}

int VR_(upload_file)(char *address, char *file)
{
    FILE *fd;
    CURL *curl;
    CURLcode res;
    struct stat file_info;
    double speed_upload, total_time;

    fd = fopen(file, "rb");
    if(!fd)
    {
        return 1;
    }

    if(fstat(fileno(fd), &file_info) != 0)
    {
        return 1;
    }

    curl = curl_easy_init();
    if(curl) 
    {
        curl_easy_setopt(curl, CURLOPT_URL, address);
        curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
        curl_easy_setopt(curl, CURLOPT_READDATA, fd);
        curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE,
                         (curl_off_t)file_info.st_size);

        /* abort if slower than 30 bytes/sec during 120 seconds */
        curl_easy_setopt(curl, CURLOPT_LOW_SPEED_TIME, DEFAULT_TIMEOUT);
        curl_easy_setopt(curl, CURLOPT_LOW_SPEED_LIMIT, 30L);

        curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "PEM");
        curl_easy_setopt(curl, CURLOPT_CAPATH, "/etc/certs/");
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
        // curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

        res = curl_easy_perform(curl);
        if(res != CURLE_OK) 
        {
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                        curl_easy_strerror(res));
        }
        else 
        {
            curl_easy_getinfo(curl, CURLINFO_SPEED_UPLOAD, &speed_upload);
            curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &total_time);

            fprintf(stderr, "Speed: %.3f bytes/sec during %.3f seconds\n",
                        speed_upload, total_time);
        }
        curl_easy_cleanup(curl);
    }
    fclose(fd);
    return 0;
}

int VR_(update_hub_infor)(char *bssid, char *ssid)
{
    int ret;
    char address[SIZE_1024B];

    char *token = VR_(read_option)(NULL, UCI_CLOUD_TOKEN);
    if(!token)
    {
        SLOGI("Token not found\n");
        return -1;
    }

    if(!strcmp(token, "default"))
    {
        SLOGI("Token invalid\n");
        free(token);
        return -1;
    }

    char *hubUUID = VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
    if(!hubUUID)
    {
        SLOGI("hubUUID not found\n");
        free(token);
        return -1;
    }

    char headerIn[SIZE_1024B]={0};
    snprintf(headerIn, sizeof(headerIn), "authorization: %s", token);

    char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
    if(!cloud_address)
    {
        snprintf(address,sizeof(address), HUB_META_DATA, CLOUD_DOMAIN, hubUUID);
    }
    else
    {
        snprintf(address,sizeof(address), HUB_META_DATA, cloud_address, hubUUID);
        free(cloud_address);
    }

    json_object *data = json_object_new_object();
    JSON_ADD_STRING_SAFE(data, ST_SSID, ssid);
    JSON_ADD_STRING_SAFE(data, ST_BSSID_LOWER_CASE, bssid);

    char *result = NULL;
    ret = VR_(put_request)(address, headerIn, (char*)json_object_to_json_string(data),
                           NULL, &result, DEFAULT_TIMEOUT);
    if(!ret)
    {
        SLOGI("######## RESPONSE = %s\n", result);
        if(strcmp(result, "{}"))
        {
            json_object* jobj = VR_(create_json_object)(result);
            if(jobj)
            {
                json_object *error = json_object_object_get(jobj, "error");
                if(error)
                {
                    SLOGI("##### ERROR MESSAGE #### = %s\n", 
                            json_object_to_json_string(json_object_object_get(error, "message")));

                    if(!strcmp(json_object_to_json_string(json_object_object_get(error, "status")),
                            "404"))
                    {
                        ret = 0;
                    }
                    else
                    {
                        ret = -1;
                    }
                }
                json_object_put(jobj);
            }
        }
    }

    free(result);
    free(hubUUID);
    free(token);
    json_object_put(data);
    return ret;
}

void init_curl_request_list(void)
{
    VR_INIT_LIST_HEAD(&g_curl_request.list);
    pthread_mutex_init(&g_curl_list_queueMutex, 0);
}

void adding_curl_request_list(uint8_t request_type, char *url, char *headers, char *data, request_data_cb data_cb)
{
    if(!url)
    {
        SLOGE("request %d missing url\n", request_type);
        return;
    }

    curl_request_queue_t *new = (curl_request_queue_t*)malloc(sizeof(curl_request_queue_t));
    memset(new, 0x00, sizeof(curl_request_queue_t));

    new->request_type = request_type;
    new->url = strdup(url);
    if(headers)
    {
        new->headers = strdup(headers);
    }

    if(data)
    {
        new->data = strdup(data);
    }

    if(data_cb)
    {
        new->data_cb = data_cb;
    }

    VR_(list_add_tail)(&(new->list), &(g_curl_request.list));
}

static curl_request_queue_t *get_first_element_in_queue(void)
{
    curl_request_queue_t *first = VR_list_entry(g_curl_request.list.next, curl_request_queue_t, list);

    pthread_mutex_lock(&g_curl_list_queueMutex);
    VR_(list_del)(g_curl_request.list.next);
    pthread_mutex_unlock(&g_curl_list_queueMutex);

    return first;
}

static void free_request(curl_request_queue_t *request)
{
    if(request)
    {
        SAFE_FREE(request->headers);
        SAFE_FREE(request->url);
        SAFE_FREE(request->data);
        SAFE_FREE(request);
    }
}

void request_thread(void)
{
    int rc = 0;
    CURL *curl = curl_easy_init();

    while(curl_request_thread_enable)
    {
        if(VR_list_empty(&g_curl_request.list))
        {
            usleep(2000);
            continue;
        }

        curl_request_queue_t *request = get_first_element_in_queue();
        if(!request)
        {
            usleep(2000);
            continue;
        }

        struct string httpResponse;
        init_string(&httpResponse);
        switch(request->request_type)
        {
            case HTTP_POST_REQUEST:
                rc = _post_request(curl, request->url, request->headers, request->data, NULL, (void *)&httpResponse, DEFAULT_TIMEOUT);
                if(rc)
                {
                    SLOGE("post request to %s error rc = %d\n", request->url, rc);
                }
                break;

            case HTTP_GET_REQUEST:
                rc = _get_request(curl, request->url, request->headers, request->data, NULL, (void *)&httpResponse, DEFAULT_TIMEOUT);
                if(rc)
                {
                    SLOGE("get request to %s error rc = %d\n", request->url, rc);
                }
                break;

            case HTTP_PUT_REQUEST:
                rc = _put_request(curl, request->url, request->headers, request->data, NULL, (void *)&httpResponse, DEFAULT_TIMEOUT);
                if(rc)
                {
                    SLOGE("put request to %s error rc = %d\n", request->url, rc);
                }
                break;

            case HTTP_PATCH_REQUEST:
                rc = _patch_request(curl, request->url, request->headers, request->data, NULL, (void *)&httpResponse, DEFAULT_TIMEOUT);
                if(rc)
                {
                    SLOGE("patch request to %s error rc = %d\n", request->url, rc);
                }
                break;

            case HTTP_DELETE_REQUEST:
                rc = _delete_request(curl, request->url, request->headers, NULL, (void *)&httpResponse, DEFAULT_TIMEOUT);
                if(rc)
                {
                    SLOGE("delete request to %s error rc = %d\n", request->url, rc);
                }
                break;
        }

        if(httpResponse.ptr)
        {
            SLOGI("httpResponse.ptr = %s\n", httpResponse.ptr);
        }

        if(request->data_cb)
        {
            request->data_cb(httpResponse.ptr, (void*)request);
        }

        free(httpResponse.ptr);

        free_request(request);
        usleep(1000);
    }
}

void stop_request_thread(void)
{
    curl_request_thread_enable = 0;
}