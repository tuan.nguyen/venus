/**
 * Compile with -D_GNU_SOURCE 
 * Note: we're using static keys here and we don't plan to add
 *       new items to the table. In such case using a perfect
 *       hash generator (e.g, gperf) would clearly outperform
 *       this simple example w/ the cost of first running gperf
 *       (and other tradeoff that must be considered)
 * 
 * Note: it's up to you to manage those resources contained in
 *       the hash table. Also, hsearch does NOT provide a 
 *       REMOVE action, only ENTER and FIND. If you with to 
 *       improve it with remove semantics you'll probably need
 *       to deal with it in the data-management side, 
 *       remembering that the table will keep only growing in
 *       space (there's no UPDATE action as well)
 */

#include "hash_table.h"

table_t* table_create(size_t size)
{
    table_t* table = malloc(sizeof(*table));

    *table = TABLE_T_INITIALIZER;
    hcreate_r(size, &table->htab);

    return table;
}

void table_destroy(table_t* table)
{
    hdestroy_r(&table->htab);
    free(table);
    table = NULL;
}

int table_add(table_t* table, char* key, void* data)
{
    unsigned n = 0;
    ENTRY e, *ep;

    e.key = key;
    e.data = data;
    n = hsearch_r(e, ENTER, &ep, &table->htab);

    return n;
}

void* table_get(table_t* table, char* key)
{
    unsigned n = 0;
    ENTRY e, *ep;

    e.key = key;
    n = hsearch_r(e, FIND, &ep, &table->htab);

    if (!n)
    return NULL;

    return ep->data;
}
