#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <time.h>
#include <uci.h>
#include <curl/curl.h>
#include <ctype.h>
#include <sys/stat.h>
#include <openssl/bio.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/buffer.h>
#include <openssl/sha.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <errno.h>

#include <linux/unistd.h>       /* for _syscallX macros/related stuff */
#include <linux/kernel.h>       /* for struct sysinfo */
#include <sys/sysinfo.h>

#include "verik_utils.h"
#include "verik_sec.h"
#include "slog.h"

#define LOGGER_URL "fwlogs.veriksystems.com"
#define LOGGER_URL_PORT 8000

#define COMMAND_CLASS_ASSOCIATION                                                        0x85
#define COMMAND_CLASS_METER                                                              0x32
#define COMMAND_CLASS_SENSOR_MULTILEVEL                                                  0x31
#define COMMAND_CLASS_USER_CODE                                                          0x63
#define COMMAND_CLASS_THERMOSTAT_SETPOINT                                                0x43

firmware_info_t firmware_table[]=
{
    {PHD_BOARD, HW_REV_E, "phd", "6a7a7b8426bc4d25a92d16ee4233e151", "phd-staging", "6a217b90364a49e9b70a737287108f3d"},
};

/*#####################################################################################*/
/*###########################        ALARM NOTIFICATION        #########################*/
/*#####################################################################################*/

alarm_effect_type_t table_keypad[] =
{
    {ST_WRONG_PIN_CODE, 1, (const char *const[]){ST_WRONG_PIN_3TIMES}},
};

group_alarm_type_t group_alarm_type_table[]=
{
    {ST_TAMPER_ALARM    ,ST_TAMPER              ,0 ,NULL},
    {ST_TAMPER_ALARM    ,ST_TAMPER_CLEAR        ,0 ,NULL},
    
    {ST_DOOR_LOCK       ,ST_DOOR_RF_UNLOCKED    ,0 ,NULL},
    {ST_DOOR_LOCK       ,ST_DOOR_RF_LOCKED      ,0 ,NULL},
    {ST_DOOR_LOCK       ,ST_STUCKINLOCK         ,0 ,NULL},
    {ST_DOOR_LOCK       ,ST_KEYPAD_UNLOCK       ,sizeof(table_keypad)/sizeof(alarm_effect_type_t), table_keypad},
    {ST_DOOR_LOCK       ,ST_KEYPAD_LOCK         ,sizeof(table_keypad)/sizeof(alarm_effect_type_t), table_keypad},
    {ST_DOOR_LOCK       ,ST_MANUAL_UNLOCK       ,0 ,NULL},
    {ST_DOOR_LOCK       ,ST_MANUAL_LOCK         ,0 ,NULL},
    {ST_DOOR_LOCK       ,ST_AUTO_LOCK           ,0 ,NULL},
    {ST_DOOR_LOCK       ,ST_ONE_TOUCH_LOCK      ,0 ,NULL},

    {ST_WRONG_PIN_CODE  ,ST_WRONG_PIN_3TIMES    ,0 ,NULL},

    {ST_DOOR_SENSOR     ,ST_DOOR_OPEN           ,0 ,NULL},
    {ST_DOOR_SENSOR     ,ST_DOOR_CLOSED         ,0 ,NULL},

    {ST_POWER           ,ST_POWER_OVER_LOAD_DETECT          ,0 ,NULL},
    {ST_POWER           ,ST_POWER_MANAGEMENT_IDLE           ,0 ,NULL},

    {ST_BATTERY         ,ST_BATTERY_IDLE        ,0 ,NULL},
    {ST_BATTERY         ,ST_BATTERY_WARNING     ,0 ,NULL},
    {ST_BATTERY         ,ST_BATTERY_REPLACE     ,0 ,NULL},

    {ST_BARRIER         ,ST_HARDWARE_FAILURE                ,0 ,NULL},
    {ST_BARRIER         ,ST_BARRIER_SENSOR_NOT_DETECTED     ,0 ,NULL},
    {ST_BARRIER         ,ST_BARRIER_SENSOR_BATTERY_WARNING  ,0 ,NULL},
    {ST_BARRIER         ,ST_BARRIER_ACCESS_CONTROL_IDLE     ,0 ,NULL},
    {ST_BARRIER         ,ST_BARRIER_UNATTENDED_DISABLED     ,0 ,NULL},
    {ST_BARRIER         ,ST_BARRIER_MOTOR_EXCEEDED          ,0 ,NULL},
    {ST_BARRIER         ,ST_BARRIER_OPERATION_EXCEEDED      ,0 ,NULL},
    {ST_BARRIER         ,ST_BARRIER_PHYSICAL_EXCEEDED       ,0 ,NULL},
    {ST_BARRIER         ,ST_MAIFUNCTION                     ,0 ,NULL},

    {ST_MOTION          ,ST_MOTION_DETECTED     ,0 ,NULL},
    {ST_MOTION          ,ST_MOTION_CLEARED      ,0 ,NULL},
    {ST_MOTION          ,ST_MOTION_DISABLED     ,0 ,NULL},

    {ST_TEMPERATURE     ,ST_TEMPERATURE_HIGH    ,0 ,NULL},
    {ST_TEMPERATURE     ,ST_TEMPERATURE_LOW     ,0 ,NULL},
    {ST_TEMPERATURE     ,ST_TEMPERATURE_OK      ,0 ,NULL},

    {ST_WATER           ,ST_WATER_LEAKS         ,0 ,NULL},
    {ST_WATER           ,ST_WATER_CLEAR         ,0 ,NULL},
    {ST_WATER           ,ST_WATER_FLOW_IDLE     ,0 ,NULL},
    {ST_WATER           ,ST_WATER_FLOW_LOW      ,0 ,NULL},
    {ST_WATER           ,ST_WATER_FLOW_HIGH     ,0 ,NULL},

    {ST_SMOKE           ,ST_SMOKE_DETECTED      ,0 ,NULL},
    {ST_SMOKE           ,ST_SMOKE_CLEARED       ,0 ,NULL},
    {ST_SMOKE           ,ST_TEST_START          ,0 ,NULL},

    {ST_ELECTRIC        ,ST_VOLTAGE_DROP        ,0 ,NULL},
    {ST_ELECTRIC        ,ST_POWER_APPLY         ,0 ,NULL},

    {ST_CO              ,ST_CO_DETECTED         ,0 ,NULL},
    {ST_CO              ,ST_CO_CLEARED          ,0 ,NULL},
};

static int get_data_from_name(char *name, int version, firmware_info_t table[],
                                            int table_len, firmware_info_t *output)
{
    int i;
    for(i =0; i < table_len; i++)
    {
        if(!strcmp(name, table[i].board_name) && (version == table[i].version))
        {
            *output = table[i];
            return 1;
        }
    }
    /*Set default board venus 2*/
    *output = table[0];
    return -1;
}

int get_data_firmware(char *name, int version, firmware_info_t *output)
{
    // return get_data_from_name(name, version, firmware_table, sizeof(firmware_table)/sizeof(firmware_info_t), output);
    return get_data_from_name(name, version, firmware_table, sizeof(firmware_table)/sizeof(firmware_info_t), output);
}

static group_alarm_type_t get_group_alarm_from_alarmType(const char *alarmTypeFinal, group_alarm_type_t table[], int table_len)
{
    int i;
    group_alarm_type_t result;
    result.groupAlarm = NULL;
    result.alarmTypeFinal = NULL;
    result.alarm_array_active_len = 0;
    result.alarmActive = NULL;
    for(i =0; i < table_len; i++)
    {
        if(!strcmp(alarmTypeFinal, table[i].alarmTypeFinal))
        {
            result = table[i];
        }
    }
    return result;
}

group_alarm_type_t get_group_alarm(const char *alarmTypeFinal)
{
    return get_group_alarm_from_alarmType(alarmTypeFinal, group_alarm_type_table, sizeof(group_alarm_type_table)/sizeof(group_alarm_type_t));
}

void VR_(install_rule)(char* rule_id, char* conditions, char* actions)
{
    char *tok, *save_tok, minutes[8], hour[8], dayloop[32];

    size_t mallocLength = strlen(conditions) + 1;
    char *tmp =(char *)malloc(mallocLength);
    strlcpy(tmp, conditions, mallocLength);

    SLOGI("tmp = %s", tmp);
    tok = strtok_r(tmp,":;", &save_tok);
    if(tok != NULL)
    {
        strncpy(hour, tok, sizeof(hour)-1);
        tok = strtok_r(NULL, ":;", &save_tok);
    }
    if(tok != NULL)
    {
        strncpy(minutes, tok, sizeof(minutes)-1);
        tok = strtok_r(NULL, ":;", &save_tok);
    }
    if(tok != NULL)
    {
        strncpy(dayloop, tok, sizeof(dayloop)-1);    
    }
    SLOGI("minutes = %s\n", minutes);
    SLOGI("hour = %s\n", hour);
    SLOGI("dayloop = %s\n", dayloop);

    char *command = (char *)malloc(strlen(rule_id) + strlen(conditions) + 2*strlen(actions) + SIZE_256B);

    if(!strcmp(dayloop,ST_NO_LOOP) || !strcmp(dayloop,ST_AUTO_OFF))
    {
        sprintf(command, "sed -i -e '$a\\%s %s * * * /etc/rule_control.sh ", minutes, hour);
    }
    else
    {
        sprintf(command, "sed -i -e '$a\\%s %s * * %s /etc/rule_control.sh ", minutes, hour, dayloop);
    }
    sprintf(command+strlen(command), "\"%s\" \"%s\" \"%s\"", rule_id, dayloop, actions);

    sprintf(command+strlen(command), ";' /etc/crontabs/root");
    SLOGI("command %s\n", command);
    VR_(execute_system)(command);
    free(tmp);
    // free(action);
    free(command);
}

static int convert_rule_condition(char *shm, char *ruleType, 
                                json_object *object, char **condition)
{
    int ret = -1;
    if(!ruleType)
    {
        return ret;
    }

    if(!strcmp(ruleType, ST_MANUAL))
    {   
        *condition = strdup("dontCare");
        ret = 0;
        goto convert_done;
    }

    if(!object)
    {
        SLOGE("object is NULL\n");
        return ret;
    }

    if(!strcmp(ruleType, ST_DEV_TO_DEV))
    {
        CHECK_JSON_OBJECT_EXIST(deviceObj, object, ST_DEVICE, convert_done);
        const char *hubUUID = json_object_get_string(deviceObj);
        char *curHubUUID = VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);
        if(curHubUUID && strcmp(hubUUID, curHubUUID))
        {
            SLOGI("this condition %s is not for this hub\n", json_object_to_json_string(object));
            ret = 0;
            *condition = strdup("dontCare");
            free(curHubUUID);
            curHubUUID = NULL;
            goto convert_done;
        }

        SAFE_FREE(curHubUUID);
    }
    
    CHECK_JSON_OBJECT_EXIST(dataObj, object, ST_CLOUD_DATA, convert_done);
    enum json_type object_type = json_object_get_type(dataObj);
    if(json_type_array != object_type)
    {
        SLOGE("data is not json array\n");
        goto convert_done;
    }

    json_object *firstData = json_object_array_get_idx(dataObj, 0);

    if(!strcmp(ruleType, ST_TIME))
    {
        char timeFormat[SIZE_64B];
        CHECK_JSON_OBJECT_EXIST(timeObj, firstData, ST_TIME, convert_done);
        CHECK_JSON_OBJECT_EXIST(loopObj, firstData, ST_LOOP, convert_done);

        const char *timeStr = json_object_get_string(timeObj);
        const char *loop = json_object_get_string(loopObj);
        snprintf(timeFormat, sizeof(timeFormat), "%s;%s", timeStr, loop);
        *condition = strdup(timeFormat);
    }
    else if(!strcmp(ruleType, ST_DEV_TO_DEV))
    {
        char dev2devFormat[SIZE_256B];
        CHECK_JSON_OBJECT_EXIST(resourceObj, object, ST_CLOUD_RESOURCE, convert_done);
        CHECK_JSON_OBJECT_EXIST(valueObj, firstData, ST_VALUE, convert_done);

        const char *resource = json_object_get_string(resourceObj);
        const char *value = json_object_get_string(valueObj);

        char *localId = VR_(get_localid_from_cloudid)(shm, (char*)resource, NULL);
        if(!localId)
        {
            // goto convert_done;
        }

        json_object *userIdObj = NULL;
        const char *userId = NULL;
        if(json_object_object_get_ex(firstData, ST_USER_ID, &userIdObj))
        {
            userId = json_object_get_string(userIdObj); 
            snprintf(dev2devFormat, sizeof(dev2devFormat),  "%s;%s;%s", localId?localId:"", value, userId);
        }
        else
        {
            snprintf(dev2devFormat, sizeof(dev2devFormat), "%s;%s", localId?localId:"", value);
        }

        *condition = strdup(dev2devFormat);
        SAFE_FREE(localId);
    }
    else if(!strcmp(ruleType, ST_LOCATION))
    {
        CHECK_JSON_OBJECT_EXIST(valueObj, firstData, ST_VALUE, convert_done);
        const char *value = json_object_get_string(valueObj);

        *condition = strdup(value);
    }

    ret = 0;
convert_done:
    return ret;    
}

static int convert_rule_actions(char *shm, json_object *actionsObj, char **actions)
{
    int ret = -1;
    if(!actionsObj)
    {
        SLOGE("actionsObj is NULL\n");
        return ret;
    }

    enum json_type object_type = json_object_get_type(actionsObj);
    if(json_type_array != object_type)
    {
        SLOGE("actions is not json array\n");
        goto convert_done;
    }

    int i;
    int actionsNumArray = json_object_array_length(actionsObj);
    size_t actionsObjLength = strlen(json_object_to_json_string(actionsObj));

    char *actionsR = (char*)malloc(actionsObjLength*2+1);
    memset(actionsR, 0x00, actionsObjLength*2+1);
    *actions = actionsR;

    char *curHubUUID = VR_(read_option)(NULL, UCI_CLOUD_HUB_UUID);

    for (i=0; i < actionsNumArray; i++)
    {
        json_object *actionObj = json_object_array_get_idx(actionsObj, i);
        CHECK_JSON_OBJECT_EXIST(deviceObj, actionObj, ST_DEVICE, convert_done);
        
        const char *hubUUID = json_object_get_string(deviceObj);
        if(!strcmp(hubUUID, curHubUUID))
        {
            CHECK_JSON_OBJECT_EXIST(resourceObj, actionObj, ST_CLOUD_RESOURCE, convert_done);
            CHECK_JSON_OBJECT_EXIST(datasObj, actionObj, ST_CLOUD_DATA, convert_done);

            char *localId = NULL;
            char deviceType[SIZE_64B] = {0};
            const char *resourceId = json_object_get_string(resourceObj);
            enum json_type object_type = json_object_get_type(datasObj);
            if(json_type_array != object_type)
            {
                SLOGE("data is not json array\n");
                goto convert_done;
            }

            localId = VR_(get_localid_from_cloudid)(shm, (char*)resourceId, deviceType);
            if(!localId || !strlen(deviceType))
            {
                SLOGE("not found local id with resource %s\n", resourceId);
                SAFE_FREE(localId);
                continue;
            }

            int j;
            int dataNumArray = json_object_array_length(datasObj);
            for (j=0; j < dataNumArray; j++)
            {
                size_t actionsLength = strlen(actionsR);
                json_object *dataObj = json_object_array_get_idx(datasObj, j);
                CHECK_JSON_OBJECT_EXIST(commandObj, dataObj, ST_COMMAND, convert_done);
                CHECK_JSON_OBJECT_EXIST(valueObj, dataObj, ST_VALUE, convert_done);

                const char *command = json_object_get_string(commandObj);
                const char *value = json_object_get_string(valueObj);
                if(!strcmp(command, ST_SET_TEMP))
                {
                    CHECK_JSON_OBJECT_EXIST(modeObj, dataObj, ST_MODE, convert_done);
                    CHECK_JSON_OBJECT_EXIST(unitObj, dataObj, ST_UNIT, convert_done);
                    const char *mode = json_object_get_string(modeObj);
                    const char *unit = json_object_get_string(unitObj);

                    if(!actionsLength)
                    {
                        sprintf(actionsR, "%s;%s;%s;%s;%s;%s;%s", hubUUID, deviceType, localId,
                                                            command, mode, value, unit);
                    }
                    else
                    {
                        sprintf(actionsR+actionsLength, "&&%s;%s;%s;%s;%s;%s;%s", 
                                                        hubUUID, deviceType, localId,
                                                        command, mode, value, unit);
                    }
                }
                else
                {
                    if(!actionsLength)
                    {
                        sprintf(actionsR, "%s;%s;%s;%s;%s", hubUUID, deviceType, localId,
                                                            command, value);
                    }
                    else
                    {
                        sprintf(actionsR+actionsLength, "&&%s;%s;%s;%s;%s", 
                                                        hubUUID, deviceType, localId,
                                                        command, value);
                    }
                }
            }

            SAFE_FREE(localId);
        }
        else
        {
            /*only need hubUUID when actions for another hub*/
            size_t actionsLength = strlen(actionsR);
            if(!actionsLength)
            {
                sprintf(actionsR, "%s", hubUUID);
            }
            else if(!strstr(actionsR, hubUUID))
            {
                sprintf(actionsR+actionsLength, "&&%s", hubUUID);
            }
        }
    }

    SAFE_FREE(curHubUUID);
    ret = 0;
convert_done:
    return ret;
}

/*RULE DOCUMENT*/
/*
"verb":"create/update/delete",
"ruleName":"name",
"type":"name",  //device, time, manual, geo : ruleType
"ruleID":"id",
"doc":{
    "condition": [
        {
            "type": "device",
            "device": "hubUUID",           
            "resource": "resourceID",
            "data": [
                { "command": "onoff", "value": "1" }
            ]
        }
    ],
    "action": [
        {
            "type": "device",
            "device": "hubUUID",
            "resource": "resourceID",
            "data": [
                { "command": "setMode", "value": "cool" },
                { "command": "setTemp", "mode": "cool", "value": “30” }
            ]
        }
    ]
}
*/
/*return rule information, need free after using*/
int VR_(get_rule_infor)(char *shm, char *ruleDocument, char **state, char **ruleId, 
                        char **ruleName, char **ruleType, char **condition, char **actions,
                        int64_t *timeStamp, char **enabled)
{
    int ret = -1;
    if(!ruleDocument)
    {
        SLOGE("ruleDocument is NULL invalid\n");
        return ret;
    }

    json_object *jobj = VR_(create_json_object)(ruleDocument);
    if(!jobj)
    {
        SLOGE("ruleDocument is not json format\n"
               "#################\n"
               "%s\n"
               "#################\n", ruleDocument);
        return ret;
    }

    CHECK_JSON_OBJECT_EXIST(stateObj, jobj, ST_CLOUD_RULE_STATE, get_rule_info_done);
    CHECK_JSON_OBJECT_EXIST(ruleIdObj, jobj, ST_CLOUD_RULE_ID, get_rule_info_done);
    CHECK_JSON_OBJECT_EXIST(timeStampObj, jobj, ST_CLOUD_TIME_STAMP, get_rule_info_done);
    CHECK_JSON_OBJECT_EXIST(enableObj, jobj, ST_CLOUD_ENABLED, get_rule_info_done);

    *state = strdup(json_object_get_string(stateObj));
    *ruleId = strdup(json_object_get_string(ruleIdObj));
    *timeStamp = json_object_get_int64(timeStampObj);
    json_bool enabledValue = json_object_get_boolean(enableObj);
    if(enabledValue)
    {
        *enabled = strdup(ST_ACTIVE);
    }
    else
    {
        *enabled = strdup(ST_INACTIVE);
    }

    // if(!strcmp(*state, ST_DELETE))
    // {
    //     goto get_rule_info_done;
    // }

    CHECK_JSON_OBJECT_EXIST(ruleNameObj, jobj, ST_NAME, get_rule_info_done);
    CHECK_JSON_OBJECT_EXIST(docObj, jobj, ST_CLOUD_RULE_DOC, get_rule_info_done);
    CHECK_JSON_OBJECT_EXIST(ruleTypeObj, docObj, ST_TYPE, get_rule_info_done);

    CHECK_JSON_OBJECT_EXIST(conditionObj, docObj, ST_CLOUD_RULE_CONDITION, get_rule_info_done);
    CHECK_JSON_OBJECT_EXIST(actionsObj, docObj, ST_CLOUD_RULE_ACTION, get_rule_info_done);

    enum json_type object_type = json_object_get_type(conditionObj);
    if(json_type_array != object_type)
    {
        SLOGE("condition is not json array\n");
        goto get_rule_info_done;
    }

    object_type = json_object_get_type(actionsObj);
    if(json_type_array != object_type)
    {
        SLOGE("actions is not json array\n");
        goto get_rule_info_done;
    }

    /*get rule type in condition[0], will be fixed*/
    json_object *firstCondition = json_object_array_get_idx(conditionObj, 0);

    
    *ruleName = strdup(json_object_get_string(ruleNameObj));
    *ruleType = strdup(json_object_get_string(ruleTypeObj));
    
    // *condition = strdup(json_object_to_json_string(conditionObj));
    // *actions = strdup(json_object_to_json_string(actionsObj));
    
    ret = convert_rule_condition(shm, *ruleType, firstCondition, condition);
    if(ret)
    {
        SLOGE("condition format error\n");
        goto get_rule_info_done;
    }
    ret = convert_rule_actions(shm, actionsObj, actions);
    if(ret)
    {
        SLOGE("actions format error %s\n", json_object_to_json_string(actionsObj));
        goto get_rule_info_done;
    }

get_rule_info_done:
    json_object_put(jobj);
    return ret;
}

int VR_(generate_32bit)(void)
{
    uint32_t x;
    struct timeval time; 
    gettimeofday(&time,NULL);

    // microsecond has 1 000 000
    // Assuming you did not need quite that accuracy
    // Also do not assume the system clock has that accuracy.
    srand((time.tv_sec * 1000) + (time.tv_usec / 1000));
    x = rand() & 0xff;
    x |= (rand() & 0xff) << 8;
    x |= (rand() & 0xff) << 16;
    x |= (rand() & 0xff) << 24;
    return x;
}

void VR_(generate_128bit) (char *result)
{
    int i;
    uint64_t x;
    struct timeval time; 
    gettimeofday(&time,NULL);

    srand((time.tv_sec * 1000) + (time.tv_usec / 1000));
    for(i = 0; i < 2; i++)
    {
        x = rand();
        x = (x<<32) | rand();

        if(!i)
        {
            sprintf(result, "%016llX", x);
        }
        else
        {
            sprintf(result+strlen(result), "%016llX", x);
        }
    }
}

char* VR_(read_option)(char *config_path, const char* option)
{
    struct uci_context *ctx;
    struct uci_ptr ptr;
    int ret;
    char* data = NULL;
    unsigned int sep = 0;
    struct uci_element *e;

    ctx = uci_alloc_context();
    if (!ctx)
    {
        SLOGI("Failed to alloc uci_context.\n");
        return data;
    }

    if(config_path)
    {
        uci_set_confdir(ctx, config_path);
    }

    char *input = strdup(option);
    ret = uci_lookup_ptr(ctx, &ptr, input, 1);

    if(ret == UCI_OK)
    {
        if(!(ptr.flags & UCI_LOOKUP_COMPLETE))
        {
            goto done;
        }

        if(!ptr.o)
        {
            goto done;
        }

        switch(ptr.o->type) 
        {
            case UCI_TYPE_STRING:
                data = strdup(ptr.o->v.string);
                break;
            case UCI_TYPE_LIST:
                uci_foreach_element(&ptr.o->v.list, e) {
                    if(e->name){
                        sep += strlen(e->name);
                    }
                }

                data = malloc(sep + 1 + SIZE_256B);
                data[0] = '\0';/*for strlen(data) in first time*/
                sep = 0;
                uci_foreach_element(&ptr.o->v.list, e) {
                    sprintf(data + strlen(data),"%s%s", (sep?" ":""), e->name);
                    sep = 1;
                }
                break;
            default:
        		SLOGI("Failed to get the option value.\n");
                break;
        }
    }
done:
    uci_free_context(ctx);
    free(input);
    return data;
}

int uci_do_add(const char* package, const char* section)
{
    struct uci_ptr ptr;
    struct uci_context *ctx = uci_alloc_context();
    int ret;

    ret = uci_load(ctx, package, &(ptr.p));
    if (ret != UCI_OK)
        goto done;

    ret = uci_add_section(ctx, ptr.p, section, &(ptr.s));
    if (ret != UCI_OK)
        goto done;

    ret = uci_commit(ctx, &ptr.p, false);

done:
    uci_free_context(ctx);

    return ret;
}

void VR_(execute_system)(const char* format, ...)
{
    int rc = 0;
    char *system_cmd = NULL;
    va_list argptr;
    va_start(argptr, format);
    rc = vasprintf(&system_cmd, format, argptr);
    va_end(argptr);

    if(rc < 0)
    {
        SAFE_FREE(system_cmd);
        return;
    }

    SLOGI("%s\n", system_cmd);
    system(system_cmd);
    SAFE_FREE(system_cmd);
}

int VR_(execute_cmd_with_lock)(const char *filename, const char *argv[],
                    const char *envp[])
{
    pid_t pid;
    int status;
    pid_t ret = -1;

    pid = fork();
    if (!pid)
    {
        SLOGI("%s\n", filename);
        if((ret=execve(filename, (char *const*)argv, (char *const*)envp)) == -1)
        {
            SLOGE("Command not found\n");
            /*  Termination of a program
                Exit Code Number:127 meaning command not found
            */
            _Exit(127);
        }
    }
    else
    {
        if (pid < 0)
        {
            SLOGE("Error to create process %s\n", strerror(errno));
            return -1;
        }
        else
        {
            while ((ret = waitpid(pid, &status, 0)) == -1)
            {
                if (errno != EINTR)
                {
                    SLOGE("Failed run command error_code: %d\n", WEXITSTATUS(status));
                    break;
                }
            }

            if ((ret != -1) && (!WIFEXITED(status))) {
                return -1;
            }
            return WEXITSTATUS(status);
        }
    }

    return ret;
}

int VR_(execute_cmd_without_lock)(const char *filename, const char *argv[],
                    const char *envp[])
{
    pid_t pid;
    pid_t ret = -1;

    pid = fork();
    if (!pid)
    {
        if((ret=execve(filename, (char *const*)argv, (char *const*)envp)) == -1)
        {
            SLOGE("Command not found\n");
            /*  Termination of a program
                Exit Code Number:127 meaning command not found
            */
            _Exit(127);
        }
    }
    else if (pid < 0)
    {
        SLOGE("Error to create process %s\n", strerror(errno));
        return -1;
    }

    return ret;
}

void VR_(remove_deny_timer)(char *userId)
{
    char command[SIZE_256B];
    snprintf(command, sizeof(command), "sed -e \"/\"%s\"/d\" -i /etc/crontabs/root", userId);
    VR_(execute_system)(command);
}

void VR_(deny_with_timer)(char *userId, time_t expiration)
{
    char command[SIZE_512B];
    struct tm *localTime;
    time_t deny_time = (unsigned)time(NULL) + expiration;
    localTime = localtime(&deny_time);

    snprintf(command, sizeof(command),
                "sed -i -e '$a\\%d %d %d %d * ubus call alljoyn denyAccess '\\\''{\"userId\":\"%s\"}'\\\' /etc/crontabs/root",
                localTime->tm_min,
                localTime->tm_hour,
                localTime->tm_mday,
                localTime->tm_mon + 1,
                userId);
    VR_(execute_system)(command);
}

// json_object *jobj = NULL;
// const char *mystring = NULL;
// int stringlen = 0;
// enum json_tokener_error jerr;
// do {
//         mystring = ...  // get JSON string, e.g. read from file, etc...
//         stringlen = strlen(mystring);
//         jobj = json_tokener_parse_ex(tok, mystring, stringlen);
// } while ((jerr = json_tokener_get_error(tok)) == json_tokener_continue);
// if (jerr != json_tokener_success)
// {
//         fprintf(stderr, "Error: %s\n", json_tokener_error_desc(jerr));
//         // Handle errors, as appropriate for your application.
// }
// if (tok->char_offset < stringlen) // XXX shouldn't access internal fields
// {
//         // Handle extra characters after parsed object as desired.
//         // e.g. issue an error, parse another object from that point, etc...
// }
// // Success, use jobj here.


json_object* VR_(create_json_object)(char *string)
{
    struct json_tokener* tok = NULL;
    json_object *jobj = NULL;
    enum json_tokener_error jerr;
    int stringlen = 0;
    if (NULL == string)
    {
        return NULL;
    }

    tok = json_tokener_new();
    if (tok)
    {
        stringlen = strlen(string);
        if(!stringlen)
        {
            json_tokener_free(tok);
            return NULL;
        }

        jobj = json_tokener_parse_ex(tok, string, stringlen);

        jerr = json_tokener_get_error(tok);
        if (jerr != json_tokener_success)
        {
            jobj = NULL;
            // printf ("Object creation failed.\n");
        }

        if (tok->char_offset < stringlen) // XXX shouldn't access internal fields
        {
            // Handle extra characters after parsed object as desired.
            // e.g. issue an error, parse another object from that point, etc...
            if(jobj)
            {
                json_object_put(jobj);
            }
            jobj = NULL;

            // printf ("wrong string len.\n");
        }

        json_tokener_free(tok);
    }
    return jobj;
}

int VR_(write_option)(char *config_path, const char* format, ...)
{
    int rc = -1;
    

    char *path = NULL;

    va_list argptr;
    va_start(argptr, format);
    rc = vasprintf(&path, format, argptr);
    va_end(argptr);

    if(rc < 0)
    {
        SAFE_FREE(path);
        return rc;
    }

    struct uci_ptr ptr;
    struct uci_context *ctx = uci_alloc_context();

    if(!ctx) 
    {
        SLOGI("Failed to alloc uci_context.\n");
        SAFE_FREE(path);
        return -1;
    }

    if(config_path)
    {
        uci_set_confdir(ctx, config_path);
    }

    if ((uci_lookup_ptr(ctx, &ptr, path, true) != UCI_OK)) 
    {
        SLOGI("Failed to alloc lookup ptr.\n");
        rc = -1;
        goto done;
    }

    if (uci_set(ctx, &ptr) != UCI_OK)
    {
        SLOGI("Failed to set.\n");
        rc = -1;
        goto done;
    }    
    
    if (uci_commit(ctx, &ptr.p, false) != UCI_OK)
    {
        SLOGI("Failed to commit.\n");
        rc = -1;
        goto done;
    }

done:
    uci_free_context(ctx);
    SAFE_FREE(path);
    return rc;
}

static void send_message_to_service(char *from, char *to, char *method, char *message)
{
    if(!from || !to || !method || !message)
    {
        return;
    }

    json_object *messageObj = VR_(create_json_object)(message);
    if(!messageObj)
    {
        SLOGE("message %s is not json format\n", message);
        return;
    }

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_SERVICE, json_object_new_string(from));
    json_object_object_add(jobj, ST_MESSAGE, messageObj);

    VR_(execute_system)("ubus call %s %s '%s'", to, method, json_object_to_json_string(jobj));
    json_object_put(jobj);
}

int sendMsgtoRemoteService(char *msg)
{
    send_message_to_service(ST_ALLJOYN, ST_PUBSUB, ST_NOTIFY, msg);
    return 0;
}

int sendMsgtoLocalService(char *method, char *msg)
{
    send_message_to_service(ST_PUBSUB, ST_ALLJOYN, method, msg);
    return 0;
}

unsigned int VR_(str_to_time)(char *humantime, const char* format, int *milisecond)
{
    if(humantime)
    {
        struct tm ts;
        time_t t_of_day;

        memset(&ts, 0, sizeof(struct tm));
        sscanf(humantime, format, &ts.tm_year, &ts.tm_mon,
               &ts.tm_mday, &ts.tm_hour, &ts.tm_min, &ts.tm_sec, milisecond);
        ts.tm_year -= 1900;
        ts.tm_mon -= 1;
        if (ts.tm_year < 0 || ts.tm_mon < 0 || ts.tm_mon > 11 ||
            ts.tm_mday < 0 || ts.tm_mday > 31 || ts.tm_hour > 23 ||
            ts.tm_min > 59 || ts.tm_sec > 61)
            ts.tm_mday = 0;

        t_of_day = mktime(&ts);

        return (unsigned int)t_of_day;
    }
    else
    {
        *milisecond = 0;
        return 0;
    }
}

// 1 OK, orther error
int VR_(is_wireless_up)(char *mode)
{
    int ret = 0;
    if(!mode)
    {
        return ret;
    }

    // char get_status_command[SIZE_256B];
    char check_enable_command[SIZE_256B];
    memset(check_enable_command, 0x00, sizeof(check_enable_command)-1);

    if(!strcmp(mode, ST_STA_MODE))
    {
        strncpy(check_enable_command, UCI_WIFI_STA_DISABLED, sizeof(check_enable_command)-1);
        // strncpy(get_status_command, "ifstatus cereswifi", sizeof(get_status_command)-1);
    }
    else if(!strcmp(mode, ST_AP_MODE))
    {
        strncpy(check_enable_command, UCI_WIFI_AP_MODE_DISABLED, sizeof(check_enable_command)-1);
        // strncpy(get_status_command, "ifstatus lan", sizeof(get_status_command)-1);
    }
    else
    {
        return ret;
    }

    char *enabled = VR_(read_option)(NULL, check_enable_command);
    if(!enabled)
    {
        return ret;
    }

    if(!strcmp(enabled, "0"))
    {
        ret = 1;
        // char status[SIZE_1024B*2];
        // VR_(run_command)(get_status_command, status, sizeof(status));
        // json_object *jobj = VR_(create_json_object)(status);
        // if(!jobj)
        // {
        //     goto done;
        // }

        // CHECK_JSON_OBJECT_EXIST(upObj, jobj, "up", done);
        // int up = json_object_get_boolean(upObj);

        // if(!up)
        // {
        //     goto done;
        // }

        // CHECK_JSON_OBJECT_EXIST(availableObj, jobj, "available", done);
        // int available = json_object_get_boolean(availableObj);
        // if(available)
        // {
        //     ret = 1;
        // }
    }

// done:
    free(enabled);
    // printf("################### %s ret = %d\n", mode, ret);
    return ret;
}

void VR_(run_command)(char *command, char *result, int result_length)
{
    //char *systemCmd = "cat /sys/class/ieee80211/phy0/macaddress";
    if(!result)
    {
        return;
    }
    
    memset(result, 0x00, result_length);
    FILE *fp;
    fp = popen(command, "r");
    if(fp)
    {
        fgets(result, result_length, fp);
        char *pos;
        if ((pos=strchr(result, '\n')) != NULL)
        {
            *pos = '\0';
        }
        pclose(fp);
    }
}

//return code
//  0: Sucessful 
// -1: The file_path is invalid 
// -2: could not connect to server 
// -3: unknown 
/***********************************************************************/
int VR_(httpUploadFile)(char *file_path)
{
    CURL *curl;
    CURLcode res;
    int rc = 0;
    char url[SIZE_1024B];
    memset(url, 0, SIZE_1024B);

    struct curl_httppost *formpost=NULL;
    struct curl_httppost *lastptr=NULL;

    curl_global_init(CURL_GLOBAL_ALL);

    curl_formadd(&formpost,
               &lastptr,
               CURLFORM_COPYNAME, "file",
               CURLFORM_FILE, file_path,
               CURLFORM_END);

    char *macAddress = VR_(read_option)(NULL, UCI_WIFI_AP_MAC_ADDRESS);

    if(!macAddress)
    {
        curl_formfree(formpost);
        return -3;
    }

    snprintf(url, sizeof(url), "http://%s:%d/logger/create/%s/", LOGGER_URL, LOGGER_URL_PORT, macAddress);
    SLOGI("log url = %s\n", url);
    curl = curl_easy_init();
    if(curl) 
    {
        curl_easy_setopt(curl, CURLOPT_URL, url);

        curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
        res = curl_easy_perform(curl);
        if(res != CURLE_OK)
        {
            if (res == CURLE_READ_ERROR)
            {
                rc = -1;
            }
            else if (res == CURLE_COULDNT_CONNECT)
            {
                rc = -2;
            }
            else 
            {
                rc = -3;
            }

            SLOGE("curl_easy_perform() failed: %s. Error code: %d \n",
                    curl_easy_strerror(res), res);
        }
        curl_easy_cleanup(curl);
        curl_formfree(formpost);
    }
    else
    {
        rc = -3;
    } 
    
    SAFE_FREE(macAddress);
    return rc;
}

void string_to_hash(const unsigned char *input, unsigned int length, char *output, size_t outLength)
{
    if(input)
    {
        unsigned char sha[SHA_DIGEST_LENGTH];
        SHA1((const unsigned char *)input, length, sha);

        char *final = base64Encode(sha, SHA_DIGEST_LENGTH);
        strlcpy(output, final, outLength);
        free(final);
    }
}

int shm_init(char **shm, int *shmid)
{
    key_t key;
    int id = 0;
    char *share_m = NULL;
    if ((key = ftok(KEY_PATH_NAME, 'R')) == -1) /*Here the file must exist */ 
    {
        perror("ftok");
        return -1;
    }

    /*
     * Create the segment.
     */
    if ((id = shmget(key, SHM_SIZE, IPC_CREAT | 0666)) < 0) 
    {
        perror("shmget");
        return -1;
    }

    /*
     * Now we attach the segment to our data space.
     */
    if ((share_m = shmat(id, 0, 0)) == (char *) -1) 
    {
        perror("shmat");
        return -1;
    }

    *shmid = id;
    *shm = share_m;
    return 0;
}

void shm_write_data(char *shm, char *data)
{
    if(shm && data)
    {
        strncpy(shm, data, SHM_SIZE-1);
    }
}

void shm_read_data(char *shm, char *data, size_t data_length)
{
    if(shm && data)
    {
        strncpy(data, shm, data_length);
    }
}

// 0 add 1 Remove
void shm_update_data(char *shm, char *cloudid, char *deviceid, char *devicetype, int mode)
{
    if(!shm || !cloudid)
    {
        return;
    }

    int data_length = strlen(shm)+1;
    char *database = (char*)malloc(data_length);
    shm_read_data(shm, database, data_length);
    SLOGI("current share memory = %s\n", database);

    json_object * jobj = NULL;
    jobj = VR_(create_json_object)((char*)database);

    if (!jobj)
    {
        free(database);
        return;
    }

    /*alway delete first*/
    json_object *cloud_obj = NULL;
    if(json_object_object_get_ex(jobj, cloudid, &cloud_obj))
    {
        json_object_object_del(jobj, cloudid);
    }

    if(mode == SHM_ADD)
    {
        json_object *cloud_dev = json_object_new_object();
        JSON_ADD_STRING_SAFE(cloud_dev, ST_ID, deviceid);
        JSON_ADD_STRING_SAFE(cloud_dev, ST_TYPE, devicetype);
        json_object_object_add(jobj, cloudid, cloud_dev);
    }

    char *share_memory = (char *)json_object_to_json_string(jobj);
    SLOGI("new share memory: %s\n", share_memory);
    shm_write_data(shm, share_memory);
    json_object_put(jobj);
    free(database);
}

char *VR_(get_localid_from_cloudid)(char* shm, char *cloud_id, char *type)
{
    if(!shm)
    {
        return NULL;
    }

    char *id_return = NULL;
    int data_length = strlen(shm)+1;
    char *database = (char*)malloc(data_length);
    shm_read_data(shm, database, data_length);
    // SLOGI("share memory = %s\n", database);

    json_object * jobj = NULL;
    jobj = VR_(create_json_object)((char*)database);

    if (!jobj)
    {
        free(database);
        return NULL;
    }

    /* scan json object */
    json_object_object_foreach(jobj, cloudid, cloudobj)
    {
        if(!strcmp(cloudid, cloud_id))
        {
            json_object_object_foreach(cloudobj, devinf, devdata)
            {
                if(!strcmp(devinf, ST_ID))
                {
                    id_return = strdup(json_object_get_string(devdata));
                }

                if(!strcmp(devinf, ST_TYPE) && type)
                {
                    strcpy(type, json_object_get_string(devdata));
                }
            }

            json_object_put(jobj);
            free(database);
            return id_return;
        }
    }
    json_object_put(jobj);
    free(database);
    return NULL;
}

char *VR_(get_cloudid_from_localid)(char* shm, char *local_id)
{
    if(!shm)
    {
        return NULL;
    }

    char *id_return = NULL;
    int data_length = strlen(shm)+1;
    char *database = (char*)malloc(data_length);
    shm_read_data(shm, database, data_length);
    // SLOGI("share memory = %s\n", database);

    json_object * jobj = NULL;
    jobj = VR_(create_json_object)((char*)database);

    if (!jobj)
    {
        free(database);
        return NULL;
    }

    /* scan json object */
    json_object_object_foreach(jobj, cloudid, cloudobj)
    {
        json_object_object_foreach(cloudobj, devinf, devdata)
        {
            if(!strcmp(devinf, ST_ID))
            {
                const char *id = json_object_get_string(devdata);
                if(!strcmp(id, local_id))
                {
                    id_return = strdup(cloudid);
                    json_object_put(jobj);
                    free(database);
                    return id_return;
                }
            }
        }
    }
    json_object_put(jobj);
    free(database);
    return NULL;
}

void VR_(convert_upper_to_lower)(char *input, char *output)
{
    if(!input)
    {
        return;
    }
    for(;*input; input++)
    {
        *output = tolower(*input);
        output++;
    }
}

// assert(version_cmp("1.2.3" , "1.2.3" ) == 0);
// assert(version_cmp("1.2.3" , "1.2.4" )  < 0);
// assert(version_cmp("1.2.4" , "1.2.3" )  > 0);
// assert(version_cmp("10.2.4", "9.2.3" )  > 0);
// assert(version_cmp("9.2.4",  "10.2.3")  < 0);
// /* Trailing 0 ignored. */
// assert(version_cmp("01", "1") == 0);
// /* Any single space delimiter is OK. */
// assert(version_cmp("1a2", "1b2") == 0);
int VR_(version_cmp)( char *pc1, char *pc2)
{
    if(!pc1 || !pc2)
    {
        return 0;
    }
    
    if(!strstr(pc1, "."))
    {
        return -1;
    }

    int result = 0;
    while (result == 0)
    {
        char* tail1;
        char* tail2;
        unsigned long ver1 = strtoul( pc1, &tail1, 10 );
        unsigned long ver2 = strtoul( pc2, &tail2, 10 );
        if (ver1 < ver2)
        {
            result = -1;
        }
        else if (ver1 > ver2)
        {
            result = +1;
        }
        else 
        {
            pc1 = tail1;
            pc2 = tail2;
            if (*pc1 == '\0' && *pc2 == '\0')
            {
                break;
            }
            else if (*pc1 == '\0')
            {
                result = -1;
            }
            else if (*pc2 == '\0')
            {
                result = +1;
            }
            else
            {
                pc1++;
                pc2++;
            }
        }
    }
    return result;
}

/*-2 file does not exist
  
*/
int VR_(get_file_infor)(char *path)
{
    int ret = -1;
    struct stat st={0};
    if(stat(path, &st) == -1)
    {
        return ret;
    }

    ret = st.st_mode & S_IFMT;
    // switch(st.st_mode & S_IFMT)
    // {
    //     case S_IFBLK: 
    //     {
    //         SLOGI( "%s: this is Block device\n", path); 
    //         break;
    //     }

    //     case S_IFCHR: 
    //     {
    //         SLOGI( "%s: this is Character device\n", path); 
    //         break;
    //     }

    //     case S_IFDIR: 
    //     {
    //         SLOGI( "%s: this is Directory.\n", path); 
    //         break;
            
    //     }

    //     case S_IFIFO: 
    //     {
    //         SLOGI( "%s: this is FIFO/pipe \n", path); 
    //         break;
    //     }

    //     case S_IFLNK:
    //     {
    //         SLOGI( "%s: this is Symlink \n", path); 
    //         break;
    //     } 
            
    //     case S_IFREG: 
    //     {
    //         SLOGI( "%s: this is Regular file.\n", path); 
    //         break;
    //     }
    //     case S_IFSOCK: 
    //     {
    //         SLOGI( "%s: this is Socket\n", path); 
    //         break;
    //     }
    //     default: 
    //     {
    //         SLOGI("%s: unknown\n", path); 
    //         break;
    //     }
    // }
    return ret;
}

void get_feature_device_support(json_object *capabilityObj, char *deviceType, int *association,
                         int *meter, int *sensorMultilevel, int *userCode, int *thermostatSetpoint)
{
    if(!capabilityObj)
    {
        SLOGE("capability not found\n");
        return;
    }

    if(!strcmp(deviceType, ST_UPNP))
    {
        return;
    }

    int i;
    char assCap[8]={0};
    char meterCap[8]={0};
    char sensorCap[8]={0};
    char userCodeCap[8]={0};
    char thermostatSetpointCap[8]={0};

    if(!strcmp(deviceType,ST_ZWAVE))
    {
        sprintf(assCap, "%d", COMMAND_CLASS_ASSOCIATION);
        sprintf(meterCap, "%d", COMMAND_CLASS_METER);
        sprintf(sensorCap, "%d", COMMAND_CLASS_SENSOR_MULTILEVEL);
        sprintf(userCodeCap, "%d", COMMAND_CLASS_USER_CODE);
        sprintf(thermostatSetpointCap, "%d", COMMAND_CLASS_THERMOSTAT_SETPOINT);
    }

    enum json_type type = json_object_get_type(capabilityObj);
    if(json_type_array == type)
    {
        int arraylen = json_object_array_length(capabilityObj);
        for (i=0; i<arraylen; i++)
        {
            json_object *jvalue = json_object_array_get_idx(capabilityObj, i);
            json_object *capList = NULL;
            json_object_object_get_ex(jvalue, ST_CAP_LIST, &capList);
            if(capList)
            {
                const char *capListStr = json_object_get_string(capList);
                if(strstr(capListStr, assCap) && association)
                {
                    *association = 1;
                }

                if(strstr(capListStr, meterCap) && meter)
                {
                    *meter = 1;
                }

                if(strstr(capListStr, sensorCap) && sensorMultilevel)
                {
                    *sensorMultilevel = 1;
                }

                if(strstr(capListStr, userCodeCap) && userCode)
                {
                    *userCode = 1;
                }

                if(strstr(capListStr, thermostatSetpointCap) && thermostatSetpoint)
                {
                    *thermostatSetpoint = 1;
                }
            }
        }
    }
    else if(json_type_object == type)
    {
        json_object_object_foreach(capabilityObj, capListStr, sheme_obj)
        {
            sheme_obj = sheme_obj;
            if(strstr(capListStr, assCap) && association)
            {
                *association = 1;
            }

            if(strstr(capListStr, meterCap) && meter)
            {
                *meter = 1;
            }

            if(strstr(capListStr, sensorCap) && sensorMultilevel)
            {
                *sensorMultilevel = 1;
            }

            if(strstr(capListStr, userCodeCap) && userCode)
            {
                *userCode = 1;
            }

            if(strstr(capListStr, thermostatSetpointCap) && thermostatSetpoint)
            {
                *thermostatSetpoint = 1;
            }
        }
    }
}

int get_processId_from_name(char *pro_name)
{
    FILE *fp;
    char buff[32]={0}, cmd[256];
    snprintf(cmd, sizeof(cmd), "pgrep %s", pro_name);

    fp = popen(cmd, "r");
    if(fp)
    {
        fgets(buff, 32, fp);
        pclose(fp);
    }

    if(strlen(buff))
    {
        char *pos;
        if ((pos=strchr(buff, '\n')) != NULL)
            *pos = '\0';
        return atoi(buff);
    }  
    else 
        return -1;
}

int get_number_process_from_name(char *process_name)
{
    int i = 0;
    FILE *fp;
    char buff[SIZE_32B]={0}, cmd[SIZE_256B];
    snprintf(cmd, sizeof(cmd), "pgrep %s", process_name);
    fp = popen(cmd, "r");
    if(fp)
    {
        while(fgets(buff, SIZE_32B, fp) != NULL)
        {
            i++;
        }
        pclose(fp);
    }

    return i;
}

static void generate_mac(char *output, size_t length)
{
    char mac_address[SIZE_32B];
    memset(mac_address, 0x00, sizeof(mac_address));
    VR_(run_command)(GET_MAC_ADDRESS_COMMAND, mac_address, sizeof(mac_address));

    int byte1=0, byte2=0, byte3=0, byte4=0, byte5=0, byte6=0;
    char *tok = strtok(mac_address, ":");
    if(tok)
    {
        byte1=strtol(tok, NULL, 16);
        tok=strtok(NULL, ":");
    }

    if(tok)
    {
        byte2=strtol(tok, NULL, 16);
        tok=strtok(NULL, ":");
    }

    if(tok)
    {
        byte3=strtol(tok, NULL, 16);
        tok=strtok(NULL, ":");
    }

    if(tok)
    {
        byte4=strtol(tok, NULL, 16);
        tok=strtok(NULL, ":");
    }

    if(tok)
    {
        byte5=strtol(tok, NULL, 16);
        tok=strtok(NULL, ":");
    }

    if(tok)
    {
        byte6=strtol(tok, NULL, 16);
        tok=strtok(NULL, ":");
    }

    if(byte6<255)
    {
        byte6=byte6^1;
    }
    else
    {
        byte5++;
        if(byte5>255)
        {
            byte5=0;
        }
        byte6=0;
    }

    snprintf(output, length, "%02X:%02X:%02X:%02X:%02X:%02X",
            byte1, byte2, byte3, byte4, byte5, byte6);
}

void get_hub_name(char *result, size_t result_length)
{
    if(!result || !result_length)
    {
        return;
    }

    char *hub_name = (char *)VR_(read_option)(NULL, UCI_HUB_NAME);
    if(!hub_name)
    {
        hub_name = (char *)VR_(read_option)(NULL, UCI_HUB_DEFAULT_NAME);
        if(!hub_name)
        {
            hub_name = (char *)VR_(read_option)(NULL, UCI_LOCAL_HUB_ID);
        }
    }

    if(!hub_name)
    {
        char device_id[SIZE_64B], board_name[SIZE_64B];
        memset(device_id, 0x00, sizeof(device_id));

        VR_(run_command)(GET_BOARD_NAME_COMMAND, board_name, sizeof(board_name));
        if(!strncmp(board_name, TITAN_BOARD, strlen(TITAN_BOARD)))
        {
            strcpy(device_id, TITAN_PATTERN_ID);
        }
        else
        {
            strcpy(device_id, PHD_PATTERN_ID);
        }

        char *board_ser = VR_(read_option)(NULL, GET_BOARD_SER_COMMAND);
        if(!board_ser)
        {
            board_ser = strdup("000");
        }

        size_t serial_length = strlen(board_ser);
        sprintf(device_id+strlen(device_id)," %s", board_ser+serial_length-3);
        hub_name=strdup(device_id);

        SAFE_FREE(board_ser);
    }

    strncpy(result, hub_name, result_length-1);
    result[result_length-1]='\0';

    SAFE_FREE(hub_name);
    return;
}

void get_hub_id(char *result, size_t result_length)
{
    if(!result || !result_length)
    {
        return;
    }

    char *hub_id = (char *)VR_(read_option)(NULL, UCI_LOCAL_HUB_ID);
    if(!hub_id || !strcmp(hub_id, "default"))
    {
        char device_id[SIZE_64B], board_name[SIZE_64B];
        memset(device_id, 0x00, sizeof(device_id));

        VR_(run_command)(GET_BOARD_NAME_COMMAND, board_name, sizeof(board_name));
        if(!strncmp(board_name, TITAN_BOARD, strlen(TITAN_BOARD)))
        {
            strncpy(device_id, TITAN_PATTERN_ID, sizeof(device_id)-1);
        }
        else
        {
            strncpy(device_id, PHD_PATTERN_ID, sizeof(device_id)-1);
        }

        char *lan_address = VR_(read_option)(NULL, UCI_WIFI_AP_MAC_ADDRESS);
        if(!lan_address)
        {
            char mac_gen[SIZE_32B];
            generate_mac(mac_gen, sizeof(mac_gen));
            lan_address = strdup(mac_gen);
        }

        size_t k = strlen(device_id);
        size_t i;
        for(i=0; i< strlen(lan_address);i++)
        {
            if(lan_address[i] != ':')
            {
                device_id[k++] = lan_address[i];
            }
        }
        device_id[k] = '\0';

        SAFE_FREE(lan_address);
        SAFE_FREE(hub_id);

        hub_id=strdup(device_id);
        VR_(write_option)(NULL, UCI_LOCAL_HUB_ID"=%s", hub_id);
    }

    strncpy(result, hub_id, result_length-1);
    result[result_length-1]='\0';

    SAFE_FREE(hub_id);

    return;
}

void get_hub_bssid(char *result, size_t result_length)
{
    if(!result || !result_length)
    {
        return;
    }

    char *lan_address = VR_(read_option)(NULL, UCI_WIFI_AP_MAC_ADDRESS);
    if(!lan_address)
    {
        char mac_gen[SIZE_32B];
        generate_mac(mac_gen, sizeof(mac_gen));
        lan_address = strdup(mac_gen);
    }

    strncpy(result, lan_address, result_length-1);
    result[result_length-1]='\0';

    SAFE_FREE(lan_address);
    return;
}

void get_hub_ssid(char *result, size_t result_length)
{
    if(!result || !result_length)
    {
        return;
    }

    char *ssid = VR_(read_option)(NULL, UCI_ALLJOYN_ONBOARDING_SSID);
    if(ssid)
    {
        strncpy(result, ssid, result_length-1);
        result[result_length-1]='\0';
    }
    else
    {
        result[0]= '\0';
    }

    SAFE_FREE(ssid);
    return;
}
/*
0: unconfigure
1: configured
2: connecting
3: connected
4: registering
5: error
6: registered
7: register_failed
*/
void get_hub_state(char **state, char **message)
{
    char *cloud_state = VR_(read_option)(NULL, UCI_CLOUD_STATE);
    if(!cloud_state)
    {
        *state = VR_(read_option)("/etc/alljoyn-onboarding", UCI_ALLJOYN_ONBOARDING_STATE);
        *message = VR_(read_option)("/etc/alljoyn-onboarding", UCI_ALLJOYN_ONBOARDING_ERROR_MSG);
        return;
    }

    if(!strcmp(cloud_state, "registered") || !strcmp(cloud_state, "claimed"))
    {
        *state = strdup("6");
        *message = strdup(cloud_state);
    }
    else if(!strcmp(cloud_state, "register_failed"))
    {
        *state = strdup("7");
        *message = strdup(cloud_state);
    }
    else if(!strcmp(cloud_state, "connected"))
    {
        *state = strdup("8");
        *message = strdup(cloud_state);
    }
    else if(!strcmp(cloud_state, "configured"))
    {
        char *onboarding = VR_(read_option)("/etc/alljoyn-onboarding", UCI_ALLJOYN_ONBOARDING_STATE);
        if(onboarding)
        {
            if(!strcmp(onboarding, "3"))
            {
                *state = strdup("4");
                *message = strdup("Registering");
            }
            else
            {
                *state = strdup(onboarding);
                *message = VR_(read_option)("/etc/alljoyn-onboarding", UCI_ALLJOYN_ONBOARDING_ERROR_MSG);
            }
            free(onboarding);
        }
    }
    else
    {
        *state = VR_(read_option)("/etc/alljoyn-onboarding", UCI_ALLJOYN_ONBOARDING_STATE);
        *message = VR_(read_option)("/etc/alljoyn-onboarding", UCI_ALLJOYN_ONBOARDING_ERROR_MSG);
    }

    SAFE_FREE(cloud_state);
    return;
}

uint64_t get_timestamp_in_microseconds(void)
{
    struct timeval tv;
    gettimeofday(&tv,NULL);

    return tv.tv_sec*(uint64_t)1000000+tv.tv_usec;;
}

unsigned int get_system_up_time(void)
{
    struct sysinfo s_info;
    int error = sysinfo(&s_info);
    if(error != 0)
    {
        printf("code error = %d\n", error);
    }
    return s_info.uptime;
}

void inform_pubsub_post_failed(char *deviceId, char *deviceType, char *data)
{
    if(!deviceId || !deviceType || !data)
    {
        return;
    }

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(deviceId));
    json_object_object_add(jobj, ST_DEVICE_TYPE, json_object_new_string(deviceType));
    json_object_object_add(jobj, ST_DATA, json_object_new_string(data));

    const char *msg = json_object_to_json_string(jobj);
    size_t cmd_len = strlen(msg)+SIZE_256B;
    char *cmd = (char *)malloc(cmd_len);
    memset(cmd, 0, cmd_len);

    snprintf(cmd, cmd_len, "ubus send resource '%s'", json_object_to_json_string(jobj));
    VR_(execute_system)(cmd);
    json_object_put(jobj);
    free(cmd);
}

void get_log_location(char *location, size_t length)
{
    char log_location[SIZE_128B]={0};
    VR_(run_command)(GET_LOG_LOCATION_COMMAND, log_location, sizeof(log_location));
    if(!strlen(log_location))
    {
        strncpy(location, DEFAULT_LOG_LOCATION, length-1);
    }
    else
    {
        strncpy(location, log_location, length-1);
    }

    location[length]='\0';
}

size_t get_file_length(char *file)
{
    if(!file)
    {
        return 0;
    }

    FILE *fp = fopen(file, "r");

    if(!fp)
    {
        return 0;
    }

    fseek(fp, 0, SEEK_END);
    size_t fileLen = ftell(fp);

    fclose(fp);
    return fileLen;
}

void clean_file_content(char *file)
{
    if(!file)
    {
        return;
    }

    FILE *fp = fopen(file, "w");

    if(!fp)
    {
        return;
    }

    fclose(fp);
    return;
}

/*
0:unlock
1:lock
*/
typedef struct _mapping
{
    const char *name;
    int id;
} mapping_t;

mapping_t doorLockStateMap[]=
{
    {ST_DOOR_RF_UNLOCKED,       DOOR_RF_UNLOCK_VALUE},
    {ST_DOOR_RF_LOCKED,         DOOR_RF_LOCK_VALUE},
    {ST_KEYPAD_UNLOCK,          DOOR_KEYPAD_UNLOCK_VALUE},
    {ST_KEYPAD_LOCK,            DOOR_KEYPAD_LOCK_VALUE},
    {ST_MANUAL_UNLOCK,          DOOR_MANUAL_UNLOCK_VALUE},
    {ST_MANUAL_LOCK,            DOOR_MANUAL_LOCK_VALUE},
    {ST_AUTO_LOCK,              DOOR_AUTO_LOCK_VALUE},
    {ST_ONE_TOUCH_LOCK,         DOOR_ONE_TOUCH_LOCK_VALUE},
};

int get_door_lock_state(const char *state)
{
    if(!state)
    {
        return 0;
    }

    int i;
    for(i = 0; i<sizeof(doorLockStateMap)/sizeof(mapping_t); i++)
    {
        if(!strcmp(doorLockStateMap[i].name, state))
        {
            return doorLockStateMap[i].id;
        }
    }

    return 0;
}

int get_door_status(const char *state)
{
    int res = 0;
    if(!state)
    {
        return res;
    }

    int doorState = get_door_lock_state(state);
    switch(doorState)
    {
        case DOOR_RF_UNLOCK_VALUE:
        case DOOR_KEYPAD_UNLOCK_VALUE:
        case DOOR_MANUAL_UNLOCK_VALUE:
                res = DOOR_RF_UNLOCK_VALUE;
            break;

        case DOOR_RF_LOCK_VALUE:
        case DOOR_KEYPAD_LOCK_VALUE:
        case DOOR_MANUAL_LOCK_VALUE:
        case DOOR_AUTO_LOCK_VALUE:
        case DOOR_ONE_TOUCH_LOCK_VALUE:
                res = DOOR_RF_LOCK_VALUE;
            break;
    }

    return res;
}

typedef struct _service_map
{
    const char *service_name;
    const char *service;
} service_map_t;

service_map_t services[]=
{
    {ST_ALLJOYN_HANDLER,       ST_ALLJOYN},
    {ST_ZWAVE_HANDLER,         ST_ZWAVE},
    {ST_ZIGBEE_HANDLER,        ST_ZIGBEE},
    {ST_PUBSUB_HANDLER,        ST_PUBSUB},
    {ST_UPNP_HANDLER,          ST_UPNP},
    {ST_VENUS_HANDLER,         ST_VENUS_SERVICE},
};

const char* get_ubus_service(const char *service_name)
{
    if(!service_name)
    {
        return ST_UNKNOWN;
    }

    int i;
    for(i=0; i < sizeof(services)/sizeof(service_map_t); i++)
    {
        if(!strcmp(services[i].service_name, service_name))
        {
            return services[i].service;
        }
    }

    return ST_UNKNOWN;
}

#define PING_COMMAND   "/etc/setup_mode.sh ping %s %d"
/*return 0 is ok*/
int ping_command(char *host, uint8_t timeout)
{
    if(!host)
    {
        return 0;
    }

    char cmd[SIZE_256B], ping_result[SIZE_32B];
    snprintf(cmd, sizeof(cmd), PING_COMMAND, host, timeout);

    memset(ping_result, 0x00, sizeof(ping_result));
    VR_(run_command)(cmd, ping_result, sizeof(ping_result));

    if(strstr(ping_result, "failed"))
    {
        return 1;
    }

    return 0;
}

#define ROUTER_CONNECT_COMMAND   "/etc/setup_mode.sh isConnected %s"
/*return 0 is connected*/
int router_connect_status(char *interface)
{
    if(!interface)
    {
        return 0;
    }

    char cmd[SIZE_256B], state[SIZE_32B];
    snprintf(cmd, sizeof(cmd), ROUTER_CONNECT_COMMAND, interface);

    memset(state, 0x00, sizeof(state));
    VR_(run_command)(cmd, state, sizeof(state));

    if(strstr(state, "failed"))
    {
        return 1;
    }

    return 0;
}

/*return CPU system in percent*/
int get_cpu_system(void)
{
    char value[SIZE_32B]={0};

    VR_(run_command)(GET_CPU_SYSTEM_COMMAND, value, sizeof(value));
    return strtol(value, NULL, 10);
}

unsigned int get_led_current(void)
{
    unsigned int value = LED_CURRENT_DEFAULT_VALUE;
    char *valueStr = VR_(read_option)(NULL, UCI_LED_CURRENT);
    if(!valueStr)
    {
        return value;
    }

    value = strtol(valueStr, NULL, 0);
    return value;
}

void set_led_current(unsigned int value)
{
    if(value > LED_CURRENT_MAX_VALUE)
    {
        value = LED_CURRENT_MAX_VALUE;
    }

    // int i;
    // unsigned int curent = get_led_current();
    // if(curent < value)
    // {
    //     for(i = curent; i < value; i+=10)
    //     {
    //         VR_(execute_system)("echo '0 0 %d' > /proc/titan_set_current", i);
    //     }
    // }
    // VR_(execute_system)("echo '0 0 %d' > /proc/titan_set_current", value);
    VR_(write_option)(NULL, UCI_LED_CURRENT"=%d", value);
    return;
}
