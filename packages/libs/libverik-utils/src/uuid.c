// $Id: Uuid.c $
// $Date: 2012-09-09 07:47Z $
// $Revision: 1.0 $
// $Author: dai $

// *************************** COPYRIGHT NOTICE ******************************
// This code was originally written by David Ireland and is copyright
// (C) 2012 DI Management Services Pty Ltd <www.di-mgt.com.au>.
// Provided "as is". No warranties. Use at your own risk. You must make your
// own assessment of its accuracy and suitability for your own purposes.
// It is not to be altered or distributed, except as part of an application.
// You are free to use it in any application, provided this copyright notice
// is left unchanged.
// ************************ END OF COPYRIGHT NOTICE **************************

// This module uses functions from the CryptoSys (tm) PKI Toolkit available from
// <www.cryptosys.net/pki/>.
// Include a reference to `diCrSysPKINet.dll` in your project.

// REFERENCE:
// RFC 4122 "A Universally Unique IDentifier (UUID) URN Namespace", P. Leach et al,
// July 2005, <http://www.ietf.org/rfc/rfc4122.txt>.

#include <stdio.h>
#include <string.h>
#include <openssl/rand.h>

#define NBYTES 16

void uuid_make(char *uuid, size_t length)
{
    //                                           12345678 9012 3456 7890 123456789012
    // Returns a 36-character string in the form XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
    // where "X" is an "upper-case" hexadecimal digit [0-9A-F].
    // Use the LCase function if you want lower-case letters.

    unsigned char abData[NBYTES];
    char strHex[2*NBYTES+1];

    // 1. Generate 16 random bytes = 128 bits
    RAND_bytes(abData, NBYTES);

    // 2. Adjust certain bits according to RFC 4122 section 4.4.
    // This just means do the following
    // (a) set the high nibble of the 7th byte equal to 4 and
    // (b) set the two most significant bits of the 9th byte to 10'B,
    //     so the high nibble will be one of {8,9,A,B}.
    abData[6] = 0x40 | (abData[6] & 0xf);
    abData[8] = 0x80 | (abData[8] & 0x3f);

    // convert hex to string
    char *ptr = &strHex[0];
    int i;
    for(i=0; i<NBYTES; i++)
    {
        ptr += sprintf(ptr, "%02X", abData[i]);
    }

    // convert uppercase to lowercase.
    for(i=0;i<=strlen(strHex);i++)
    {
        if(strHex[i]>=65 && strHex[i]<=92)
        {
            strHex[i]=strHex[i]+32;
        }
    }

    strncpy(uuid, strHex, length-1);
    uuid[length-1]='\0';
    return;
}


// int main(void)
// {
//   char uuid[33];
//   int i;
//   for (i = 0; i < 10; i++)
//   {
//     UUID_Make(uuid, sizeof(uuid));
//     printf("%s\n", uuid);
//   }
//   return 0;
// }

