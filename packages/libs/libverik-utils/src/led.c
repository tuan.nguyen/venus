#include <pthread.h>
#include <stdint.h>
#include <json-c/json.h>
#include <string.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#include "verik_utils.h"
#include "slog.h"
#include "led.h"
#include "timer.h"
#include "vr_rest.h"
#include "ping.h"

#define EFFECT_IMMEDIATE 100

static timer_t g_timer = 0;
static char fist_state_in_queue[SIZE_256B];

typedef struct __led_state_priority
{
    const char *led_state;
    int priority;
    int number_effect;
    const char *const*effect_state;
}led_state_priority_t;


typedef struct __led_state_short_action
{
    const char *led_state;
    int priority;
    int take_time; //milisecond
    int number_effect;
    const char * const*effect_state;
}led_state_short_action_t;

led_state_priority_t LED_priority[]=
{
    {ST_Builder_Default,                       1,   0, (const char *const[]){NULL}},
    {ST_Default,                               2,   1, (const char *const[]){ST_Builder_Default}},
    {ST_AP_mode,                               247, 4, (const char *const[]){ST_STA_connected_unsecure_not_internet,ST_STA_connected_secure_not_internet,ST_STA_connecting,ST_Switch_AP_mode}},
    {ST_AP_config_mode,                        248, 4, (const char *const[]){ST_STA_connected_unsecure_not_internet,ST_STA_connected_secure_not_internet,ST_STA_connecting,ST_Switch_AP_mode}},
    {ST_AP_switch_mode,                        248, 4, (const char *const[]){ST_STA_connected_unsecure_not_internet,ST_STA_connected_secure_not_internet,ST_STA_connecting,ST_Switch_AP_mode}},
    {ST_STA_connecting,                        250, 7, (const char *const[]){ST_AP_mode,ST_AP_config_mode,ST_STA_connected_unsecure_has_internet,ST_STA_connected_unsecure_not_internet,ST_STA_connected_secure_not_internet,ST_Switch_AP_mode,ST_AP_switch_mode}},
    {ST_STA_connected_unsecure_not_internet,   251, 1, (const char *const[]){ST_STA_connecting}},
    {ST_STA_connected_secure_not_internet,     252, 1, (const char *const[]){ST_STA_connecting}},
    {ST_Firmware_updating,                     253, 0, (const char *const[]){NULL}},
    {ST_Open_network,                          254, 0, (const char *const[]){NULL}},
    {ST_Error,                                 255, 0, (const char *const[]){NULL}},
    {ST_Switch_AP_mode,                       1001, 0, (const char *const[]){NULL}},
    {ST_Reset,                                1001, 0, (const char *const[]){NULL}},
    {ST_Reset_done,                           1003, 1, (const char *const[]){ST_Reset}},
};

led_state_short_action_t LED_shot_actions[]=
{
    {ST_Stop_switch_AP_mode,                  1000,     500,  1, (const char *const[]){ST_Switch_AP_mode}},
    {ST_STA_connected_unsecure_has_internet,  1000,  7000, 3, (const char *const[]){ST_AP_mode,ST_STA_connected_unsecure_not_internet,ST_STA_connecting}},
    {ST_STA_connected_secure_has_internet,    1000,  7000, 3, (const char *const[]){ST_AP_mode,ST_STA_connected_secure_not_internet,ST_STA_connecting}},
    {ST_Firmware_updated,                     1000,  3000, 1, (const char *const[]){ST_Firmware_updating}},
    {ST_Received_secure_command,              1000,  3000, 1, (const char *const[]){ST_Done_secure_command}},
    {ST_Done_secure_command,                  1000,  1500, 0, (const char *const[]){NULL}},
    {ST_Close_network,                        1000,  1000, 1, (const char *const[]){ST_Open_network}},
    {ST_Stop_reset,                           1002,  500,  1, (const char *const[]){ST_Reset}},
    {ST_IDENTIFY,                             1002,  5000,  0, (const char *const[]){NULL}},
};

int led_shm_init(char **shm, int *shmid, char *key_path_name)
{
    key_t key;
    int id = 0;
    char *share_m = NULL;
    if ((key = ftok(key_path_name, 'R')) == -1) /*Here the file must exist */ 
    {
        perror("ftok");
        return -1;
    }

    /*
     * Create the segment.
     */
    if ((id = shmget(key, SHM_SIZE, IPC_CREAT | 0666)) < 0) 
    {
        perror("shmget");
        return -1;
    }

    /*
     * Now we attach the segment to our data space.
     */
    if ((share_m = shmat(id, 0, 0)) == (char *) -1) 
    {
        perror("shmat");
        return -1;
    }

    *shmid = id;
    *shm = share_m;
    return 0;
}

// void shm_write_data(char *shm, char *data)
// {
//     if(shm && data)
//     {
//         strncpy(shm, data, SHM_SIZE-1);
//     }
// }

// void shm_read_data(char *shm, char *data, size_t data_length)
// {
//     if(shm && data)
//     {
//         strncpy(data, shm, data_length);
//     }
// }

char *VR_(get_current_led_state)(char* shm)
{
    if(!shm)
    {
        return NULL;
    }

    char *state = NULL;
    int data_length = strlen(shm)+1;
    char *led_share_data = (char*)malloc(data_length);
    shm_read_data(shm, led_share_data, data_length);
    SLOGI("share memory = %s\n", led_share_data);

    json_object * jobj = NULL;
    jobj = VR_(create_json_object)((char*)led_share_data);

    if (!jobj)
    {
        free(led_share_data);
        return state;
    }

    json_object *current_led_state_obj = NULL;

    if(json_object_object_get_ex(jobj, ST_CURRENT_STATE, &current_led_state_obj))
    {
        const char *currentState = json_object_get_string(current_led_state_obj);
        state = strdup(currentState);
    }

    json_object_put(jobj);
    free(led_share_data);
    return state;
}

int VR_(check_state_in_queue)(char *shm, char *led_state)
{
    if(!shm)
    {
        return 0;
    }

    int res = 0;
    int data_length = strlen(shm)+1;
    char *led_share_data = (char*)malloc(data_length);
    shm_read_data(shm, led_share_data, data_length);
    SLOGI("share memory = %s\n", led_share_data);

    json_object * jobj = NULL;
    jobj = VR_(create_json_object)((char*)led_share_data);

    if (!jobj)
    {
        free(led_share_data);
        return res;
    }

    json_object *in_queue_obj = NULL;

    if(json_object_object_get_ex(jobj, ST_IN_QUEUE, &in_queue_obj))
    {
        enum json_type type = json_object_get_type(in_queue_obj);
        if(type == json_type_array)
        {
            int i, arrayLen = 0 ;
            json_object *jvalue;

            arrayLen = json_object_array_length(in_queue_obj);
            for (i=0; i < arrayLen; i++)
            {
                jvalue = json_object_array_get_idx(in_queue_obj, i);
                if(!strcmp(json_object_get_string(jvalue), led_state))
                {
                    res = 1;
                    break;
                }
            }
        }
    }

    json_object_put(jobj);
    free(led_share_data);
    return res;
}

char *VR_(get_first_in_queue)(char *shm)
{
    if(!shm)
    {
        return NULL;
    }

    char *input = NULL;
    int data_length = strlen(shm)+1;
    char *led_share_data = (char*)malloc(data_length);
    shm_read_data(shm, led_share_data, data_length);
    SLOGI("share memory = %s\n", led_share_data);

    json_object * jobj = NULL;
    jobj = VR_(create_json_object)((char*)led_share_data);

    if (!jobj)
    {
        free(led_share_data);
        return input;
    }

    json_object *in_queue_obj = NULL;

    if(json_object_object_get_ex(jobj, ST_IN_QUEUE, &in_queue_obj))
    {
        enum json_type type = json_object_get_type(in_queue_obj);
        if(type == json_type_array)
        {
            json_object * jvalue;
            jvalue = json_object_array_get_idx(in_queue_obj, 0);
            if(jvalue)
            {
                input = strdup(json_object_get_string(jvalue));
            }
        }
    }

    json_object_put(jobj);
    free(led_share_data);
    return input;
}


int VR_(check_state_is_shot_action)(char *led_state, int *take_time, int *number_effect)
{
    int i, res = 0;
    for (i = 0; i < sizeof(LED_shot_actions)/sizeof(led_state_short_action_t); i++)
    {
        if(!strcmp(LED_shot_actions[i].led_state, led_state))
        {
            *take_time = LED_shot_actions[i].take_time;
            *number_effect = LED_shot_actions[i].number_effect;
            res = 1;
            break;
        }
    }
    return res;
}

static int get_led_priority(char *led_state)
{
    int i, res = 0;
    for (i = 0; i < sizeof(LED_priority)/sizeof(led_state_priority_t); i++)
    {
        if(!strcmp(LED_priority[i].led_state, led_state))
        {
            res = LED_priority[i].priority;
            break;
        }
    }
    return res;
}

static int get_led_priority_short_action(char *led_state)
{
    int i, res = 0;
    for (i = 0; i < sizeof(LED_shot_actions)/sizeof(led_state_short_action_t); i++)
    {
        if(!strcmp(LED_shot_actions[i].led_state, led_state))
        {
            res = LED_shot_actions[i].priority;
            break;
        }
    }
    return res;
}

static int is_effect_state(char *led_state, char *state_be_effected)
{
    int i, res = 0;
    for (i = 0; i < sizeof(LED_priority)/sizeof(led_state_priority_t); i++)
    {
        if(!strcmp(LED_priority[i].led_state, led_state))
        {
            int j;
            for(j = 0; j < LED_priority[i].number_effect; j++)
            {
                if(!strcmp(LED_priority[i].effect_state[j], state_be_effected))
                {
                    res = 1;
                    break;
                }
            }
            break;
        }
    }
    return res;
}

static int is_effect_state_short(char *led_state, char *state_be_effected)
{
    int i, res = 0;
    for (i = 0; i < sizeof(LED_shot_actions)/sizeof(led_state_short_action_t); i++)
    {
        if(!strcmp(LED_shot_actions[i].led_state, led_state))
        {
            int j;
            for(j = 0; j < LED_shot_actions[i].number_effect; j++)
            {
                if(!strcmp(LED_shot_actions[i].effect_state[j], state_be_effected))
                {
                    res = 1;
                    break;
                }
            }
            break;
        }
    }
    return res;
}

static int insert_new_state_to_queue(char *shm, char *led_state)
{
    printf("led_state = %s\n", led_state);
    if(!shm)
    {
        return 0;
    }

    int execute = 0;
    int data_length = strlen(shm)+1;
    char *led_share_data = (char*)malloc(data_length);
    shm_read_data(shm, led_share_data, data_length);
    SLOGI("share memory = %s\n", led_share_data);

    json_object * jobj = NULL;
    jobj = VR_(create_json_object)((char*)led_share_data);

    if (!jobj)
    {
        free(led_share_data);
        return execute;
    }

    json_object *in_queue_obj = NULL;

    if(json_object_object_get_ex(jobj, ST_IN_QUEUE, &in_queue_obj))
    {
        enum json_type type = json_object_get_type(in_queue_obj);
        if(type == json_type_array)
        {
            int i, arrayLen = 0, newLedStatePriority = 0, addedNewState = 0;
            json_object * jvalue;

            json_object *newQueue = json_object_new_array();
            arrayLen = json_object_array_length(in_queue_obj);
            if(!arrayLen)
            {
                execute = 1;
                json_object_array_add(newQueue, json_object_new_string(led_state));
            }
            else
            {
                newLedStatePriority = get_led_priority(led_state);
                printf("newLedStatePriority = %d\n", newLedStatePriority);

                for (i = 0; i < arrayLen; i++)
                {
                    jvalue = json_object_array_get_idx(in_queue_obj, i);
                    if(!jvalue)
                    {
                        continue;
                    }
                    const char *stateInQueue = json_object_get_string(jvalue);
                    printf("stateInQueue = %s\n", stateInQueue);
                    if(is_effect_state(led_state, (char*)stateInQueue))
                    {
                        if(arrayLen == (i+1)) //top
                        {
                            json_object_array_add(newQueue, json_object_new_string(led_state));
                        }
                        continue;
                    }
                    else
                    {
                        int priorityInQueue = get_led_priority((char*)stateInQueue);
                        printf("priorityInQueue = %d\n", priorityInQueue);
                        if(newLedStatePriority > priorityInQueue)
                        {
                            if(!addedNewState) //mid
                            {
                                json_object_array_add(newQueue, json_object_new_string(led_state));
                                json_object_array_add(newQueue, json_object_new_string(stateInQueue));
                                addedNewState = 1;
                            }
                            else
                            {
                                json_object_array_add(newQueue, json_object_new_string(stateInQueue));
                            }
                        }
                        else
                        {
                            json_object_array_add(newQueue, json_object_new_string(stateInQueue));
                            if(arrayLen == (i+1)) //bot
                            {
                                json_object_array_add(newQueue, json_object_new_string(led_state));
                            }
                            continue;
                        }
                    }
                }

                jvalue = json_object_array_get_idx(newQueue, 0);
                if(jvalue)
                {
                    if(!strcmp(json_object_get_string(jvalue), led_state))
                    {
                        execute = 1;
                    }
                }
            }

            json_object_object_del(jobj, ST_IN_QUEUE);
            json_object_object_add(jobj, ST_IN_QUEUE, newQueue);
            if(execute)
            {
                json_object_object_del(jobj, ST_CURRENT_STATE);
                json_object_object_add(jobj, ST_CURRENT_STATE, json_object_new_string(led_state));
            }

            char *share_memory = (char *)json_object_to_json_string(jobj);
            SLOGI("new share_memory: %s\n", share_memory);
            shm_write_data(shm, share_memory);
        }
    }

    json_object_put(jobj);
    free(led_share_data);
    return execute;
}

static int update_current_state(char *shm, char *led_state)
{
    if(!shm)
    {
        return 0;
    }

    int data_length = strlen(shm)+1;
    char *led_share_data = (char*)malloc(data_length);
    shm_read_data(shm, led_share_data, data_length);
    SLOGI("share memory = %s\n", led_share_data);

    json_object * jobj = NULL;
    jobj = VR_(create_json_object)((char*)led_share_data);

    if (!jobj)
    {
        free(led_share_data);
        return 0;
    }

    json_object *current_led_state_obj = NULL;

    if(json_object_object_get_ex(jobj, ST_CURRENT_STATE, &current_led_state_obj))
    {
        if(current_led_state_obj)
        {
            if(strcmp(json_object_get_string(current_led_state_obj), led_state))
            {
                json_object_object_del(jobj, ST_CURRENT_STATE);
                json_object_object_add(jobj, ST_CURRENT_STATE, json_object_new_string(led_state));

                char *share_memory = (char *)json_object_to_json_string(jobj);
                SLOGI("new share_memory: %s\n", share_memory);
                shm_write_data(shm, share_memory);
            }
        }
    }

    json_object_put(jobj);
    free(led_share_data);
    return 0;
}

static int remove_state_to_queue(char *shm, char *led_state)
{
    if(!shm)
    {
        return 0;
    }

    int execute = 0;
    int data_length = strlen(shm)+1;
    char *led_share_data = (char*)malloc(data_length);
    shm_read_data(shm, led_share_data, data_length);
    SLOGI("share memory = %s\n", led_share_data);

    json_object * jobj = NULL;
    jobj = VR_(create_json_object)((char*)led_share_data);

    if (!jobj)
    {
        free(led_share_data);
        return execute;
    }

    json_object *in_queue_obj = NULL;

    if(json_object_object_get_ex(jobj, ST_IN_QUEUE, &in_queue_obj))
    {
        enum json_type type = json_object_get_type(in_queue_obj);
        if(type == json_type_array)
        {
            int i, arrayLen = 0;
            json_object * jvalue;

            json_object *newQueue = json_object_new_array();
            arrayLen = json_object_array_length(in_queue_obj);

            for (i=0; i < arrayLen; i++)
            {
                jvalue = json_object_array_get_idx(in_queue_obj, i);
                const char *stateInQueue = json_object_get_string(jvalue);

                if(is_effect_state_short(led_state, (char*)stateInQueue))
                {
                    continue;
                }
                else
                {
                    json_object_array_add(newQueue, json_object_new_string(stateInQueue));
                }
            }

            json_object_object_del(jobj, ST_IN_QUEUE);
            json_object_object_add(jobj, ST_IN_QUEUE, newQueue);

            char *share_memory = (char *)json_object_to_json_string(jobj);
            SLOGI("new share_memory: %s\n", share_memory);
            shm_write_data(shm, share_memory);
        }
    }

    json_object_put(jobj);
    free(led_share_data);
    return execute;
}

void VR_(leds_turn_back_state)(void)
{
    // //check ap or sta mode
    // char *led_state = VR_(read_option)(NULL,"security.@led-state[0].state");
    // if(led_state)
    // {
    //     if(!strcmp(led_state, ST_STA_connected_secure_not_internet) ||
    //         !strcmp(led_state, ST_STA_connected_unsecure_not_internet) ||
    //         !strcmp(led_state, ST_AP_mode) || 
    //         !strcmp(led_state, ST_Error)
    //         )
    //     {
    //         char cmd[SIZE_256B];
    //         sprintf(cmd, "/etc/led_control.sh %s &", led_state);
    //         VR_(execute_system)(cmd);
    //     }

    //     free(led_state);
    // }
    // else
    // {
    //     int wifi_mode = VR_(get_wifi_mode)();
    //     SLOGI("wifi_mode = %d\n", wifi_mode);
    //     if(!wifi_mode) //STA mode
    //     {
    //         char *local_state = VR_(read_option)(NULL,"security.@local-access[0].state");
    //         char *remote_state = VR_(read_option)(NULL,"security.@remote-access[0].state");
    //         if(!local_state || !remote_state)
    //         {
    //             return;
    //         }

    //         if(!strcmp(local_state, "available"))
    //         {
    //             if(VR_(check_internet_connection)("www.google.com"))
    //             {
    //                 VR_(set_led_state)(ST_STA_connected_unsecure_not_internet);
    //             }
    //         }
    //         else if(!strcmp(remote_state, "connected") || !strcmp(remote_state, "configured"))
    //         {
    //             char cloud_addr[SIZE_256B];
    //             char *cloud_address = (char*)VR_(read_option)(NULL,"security.@remote-access[0].cloud_address");
    //             if(!cloud_address)
    //             {
    //                 sprintf(cloud_addr, CLOUD_ADDRESS, CLOUD_DOMAIN);
    //             }
    //             else
    //             {
    //                 sprintf(cloud_addr, CLOUD_ADDRESS, cloud_address);
    //                 free(cloud_address);
    //             }

    //             if(VR_(check_internet_connection)(cloud_addr))
    //             {
    //                 VR_(set_led_state)(ST_STA_connected_secure_not_internet);
    //             }
    //         }

    //         free(local_state);
    //         free(remote_state);
    //     }
    //     else if(wifi_mode == 1) //AP mode
    //     {
    //         VR_(set_led_state)(ST_AP_mode);
    //     }
    //     else
    //     {
    //         VR_(set_led_state)("Error");
    //     }
    // }
}

void execute_timer(void * data)
{
    const char *argv[] = { ST_EXCUTE_LED, fist_state_in_queue, NULL };
    VR_(execute_cmd_without_lock)(ST_EXCUTE_LED, argv, NULL);
}

void VR_(set_led_state)(char *led_shm, char *new_led_state)
{
    SLOGI("SET LED WITH STATE %s\n", new_led_state);

    char *current_led_state = VR_(get_current_led_state)(led_shm);
    if(current_led_state)
    {
        if(strcmp(current_led_state, new_led_state))//diff
        {
            int execute_state = 0;
            int take_time = 0, number_effect = 0;
            int is_short_action = VR_(check_state_is_shot_action)(new_led_state, &take_time, &number_effect);
            if(!is_short_action)
            {
                //check priority and decide state execute or not
                printf("start insert defaut mode\n");

                int is_in_queue = VR_(check_state_in_queue)(led_shm, new_led_state);
                if(!is_in_queue)
                {
                    SLOGI("state not in queue\n");
                    execute_state = insert_new_state_to_queue(led_shm, new_led_state);
                    if(execute_state)
                    {
                        strncpy(fist_state_in_queue, new_led_state, sizeof(fist_state_in_queue)-1);
                    }
                }
                else
                {
                    SLOGI("state in queue\n");
                    // char *privious_state = VR_(get_first_in_queue)(led_shm);
                    // if(privious_state && !strcmp(privious_state, new_led_state))
                    // {
                    //     execute_state = 1;
                    // }
                }
            }
            else
            {
                //remove all dependence in queue
                SLOGI("take_time = %d\n, number_effect = %d\n", take_time, number_effect);
                if(number_effect)
                {
                    remove_state_to_queue(led_shm, new_led_state);
                }

                char *privious_state = VR_(get_first_in_queue)(led_shm);
                if(privious_state && strlen(privious_state))
                {
                    SLOGI("privious_state = %s\n", privious_state);
                    int priorityInQueue = get_led_priority((char*)privious_state);
                    int priorityShortAction = get_led_priority_short_action((char*)new_led_state);
                    
                    if(priorityShortAction > priorityInQueue)
                    {
                        execute_state = 1;
                    }
                    
                    strncpy(fist_state_in_queue, privious_state, sizeof(fist_state_in_queue)-1);
                    update_current_state(led_shm, privious_state);
                    if(g_timer)
                    {
                        timerCancel(&g_timer);
                    }

                    if(execute_state)
                    {
                        timerStart(&g_timer, execute_timer, NULL, take_time, TIMER_ONETIME);
                    }
                    free(privious_state);
                }
                else
                {
                    update_current_state(led_shm, "done");
                }
            }

            if(execute_state)
            {
                const char *argv[] = { ST_EXCUTE_LED, new_led_state, NULL };
                VR_(execute_cmd_without_lock)(ST_EXCUTE_LED, argv, NULL);
            }
        }
        free(current_led_state);
    }
    else
    {
        if(strcmp(new_led_state, ST_Done_secure_command) &&
            strcmp(new_led_state, ST_Received_secure_command) &&
            strcmp(new_led_state, ST_Close_network) &&
            strcmp(new_led_state, ST_Open_network)
            )
        {
            VR_(write_option)(NULL,"security.@led-state[0].state=%s", new_led_state);
        }

        const char *argv[] = { ST_EXCUTE_LED, new_led_state, NULL };
        VR_(execute_cmd_without_lock)(ST_EXCUTE_LED, argv, NULL);
    }
}

void VR_(led_show)(char *led_shm)
{
    //check ap or sta mode
    int sta_mode = VR_(is_wireless_up)(ST_STA_MODE);
    if(sta_mode) //STA mode
    {
        char *local_state = VR_(read_option)(NULL, UCI_LOCAL_STATE);
        char *remote_state = VR_(read_option)(NULL, UCI_CLOUD_STATE);
        if(!local_state || !remote_state)
        {
            return;
        }

        if(!strcmp(local_state, "available"))
        {
            int res = ping4_api("8.8.8.8");
            int ping_retry = 3;
            while(res != 1 && ping_retry)
            {
                res = ping4_api("8.8.8.8");
                ping_retry--;
            }

            if(1 == res)
            {
                VR_(set_led_state)(led_shm, ST_STA_connected_unsecure_has_internet);
            }
            else
            {
                VR_(set_led_state)(led_shm, ST_STA_connected_unsecure_not_internet);
            }
        }
        else if(!strcmp(remote_state, "connected") || !strcmp(local_state, "connected"))
        {
            char cloud_addr[SIZE_256B];
            char *cloud_address = (char*)VR_(read_option)(NULL, UCI_CLOUD_ADDRESS);
            if(!cloud_address)
            {
                sprintf(cloud_addr, CLOUD_ADDRESS, CLOUD_DOMAIN);
            }
            else
            {
                sprintf(cloud_addr, CLOUD_ADDRESS, cloud_address);
                free(cloud_address);
            }

            if(!VR_(check_internet_connection)(cloud_addr))
            {
                VR_(set_led_state)(led_shm, ST_STA_connected_secure_has_internet);
            }
            else
            {
                VR_(set_led_state)(led_shm, ST_STA_connected_secure_not_internet);
            }
        }

        free(local_state);
        free(remote_state);
    }
    else if(!sta_mode) //AP mode
    {
        VR_(set_led_state)(led_shm, ST_AP_mode);
    }
    else
    {
        VR_(set_led_state)(led_shm, "Error");
    }
}