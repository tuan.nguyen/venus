#include "util_crc.h"

uint16_t ccittCrc16(uint16_t crc,uint8_t *pDataAddr,int bDataLen)
{
    uint8_t WorkData;
    uint8_t bitMask;
    uint8_t NewBit;
    while(bDataLen--) 
    {
        WorkData=*pDataAddr++;
        for(bitMask=0x80;bitMask!=0;bitMask>>=1) 
        {
            
            NewBit=((WorkData&bitMask)!=0)^((crc&0x8000)!=0);
            crc<<=1;
            if (NewBit)
            {
                crc^=POLY;
            }
        }
        
    }
    return crc;
}
void chksumCrc32gentab(uint32_t* crc_tab)
{
    uint32_t crc, poly;
    int i, j;

    poly = 0xEDB88320;
    for (i = 0; i < 256; i++)
    {
        crc = (uint32_t)i;
        for (j = 8; j > 0; j--)
        {
            if ((crc & (uint32_t)1) > 0)
            {
                crc = (crc >> 1) ^ poly;
            }
            else
            {
                crc >>= 1;
            }
        }
        crc_tab[i] = crc;
    }
    return ;
}

