/****************************************************************************
 *
 * This file is part of SLOG (Simple Logger for C and C++).
 *
 * Copyright (c) 2013 SLOG authors and contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 ****************************************************************************/

#include "slog.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <pthread.h>

#define PTHREAD_NAME_SIZE (16)

#if defined(_MSC_VER)
  /* Visual Studio */
  #include <io.h>
  #define write _write
  #define snprintf(buf, size, format, ...) _snprintf_s(buf, size, _TRUNCATE, format, ##__VA_ARGS__)
  #define vsnprintf(buf, size, format, args) vsnprintf_s(buf, size, _TRUNCATE, format, args)
  #pragma warning( disable : 4996 )  /* disable some VS warnings, e.g. 'fopen': This function or variable may be unsafe. */
#else
  #include <unistd.h>
#endif


/************************************************************
 * Static data
 ************************************************************/

static int s_initialized = 0;
static int s_consoleFD;         /**< Console file descriptor. 0 - no, 1 - stdout, 2 - stderr. */
static FILE *s_logFile = NULL;  /**< File for logging. */

static SLOGLevel s_consoleLevel = SLOG_INFO;
static SLOGLevel s_fileLevel = SLOG_DEBUG;

//static char thread_name[256];

/************************************************************
 * Function implementation
 ************************************************************/

void SLOG_Init(SLOGConsole console, const char *logFile)
{
  if(!s_initialized)
  {
    switch(console)
    {
    case SLOG_STDOUT:
      s_consoleFD = 1;
      break;
    case SLOG_STDERR:
      s_consoleFD = 2;
      break;
    default:
      s_consoleFD = 0;
    }

    if(logFile)
    {
      s_logFile = fopen(logFile, "wt");
      /* ignoring any errors */
    }

    s_initialized = 1;
  }
}

/** @brief Library deinitialization.
 *
 * This function should be called before program exit only once. */
void SLOG_Deinit()
{
  if(s_logFile)
  {
    fclose(s_logFile);
    s_logFile = NULL;
  }
}


void SLOG_SetLevel(SLOGLevel console, SLOGLevel file)
{
  s_consoleLevel = console;
  s_fileLevel = file;
}


// /** Get level name for including into the log. */
// static const char* getLevelName(SLOGLevel level)
// {
//   switch(level)
//   {
//   case SLOG_DEBUG:      return "DEBUG";
//   case SLOG_INFO:       return "INFO";
//   case SLOG_WARNING:    return "WARNING";
//   case SLOG_ERROR:      return "ERROR";
//   }
//   return "";
// }

/** @file Utilities for managing thread names.
 * This uses thread-local storage (TLS) to store a thread's name.
 */

static pthread_key_t tname_key; /**< TLS key for name store */

/** token used by tpthread_once() single execution mechanism to
 * ensure something only happens once.  In this case, we're making
 * sure that our TLS key is created only once
 */
static pthread_once_t tname_key_once = PTHREAD_ONCE_INIT; 

/** @brief TLS destructor.
 * This gets called when a thread exits to clean up the space
 * allocated to hold its' name.
 * @param data Anonymous pointer provided tot he destructor.  It holds
 * whatever was passed to pthread_setspecific.  In our case that's the
 * buffer for reading the thread name.
 */
static void tname_destructor(void *data) { 
  free( data );
}

/** @brief Key creator callback.
 * This function is called via pthread_once to create the TLS entry
 * and to register the destructor function.
 */
static void make_tname_key() {
    (void) pthread_key_create(&tname_key, tname_destructor);
}

/** @brief Get existing or create new buffer.
 * Functions that need access to the name buffer use this so they
 * don't have to manage the creation of the buffer.
 * @return PTHREAD_NAME_SIZE sized character array.
 */
static char *get_tname_buff() {
  char *ptr;

  (void) pthread_once(&tname_key_once, make_tname_key);
  if ((ptr = pthread_getspecific(tname_key)) == NULL) {
    ptr = (char *)calloc( PTHREAD_NAME_SIZE + 1, 1 );
    if( ptr )
      (void) pthread_setspecific(tname_key, ptr);
  } /*else {
    DBG( "Found buffer for tid %lu: \"%s\"\n", 
         (unsigned long int)pthread_self(), ptr );
  } */
  return (char *)ptr;
}

void set_thread_name( const char* name ) {
  char *buff = get_tname_buff();
  if( buff ) {
    strncpy( buff, name, PTHREAD_NAME_SIZE );
  }
}

const char *get_thread_name( void ) {
  char *buff = get_tname_buff();
  return buff ? buff : "UNNAMED";
}

void SLOG_LogMessage(SLOGLevel level, const char *format, ...)
{
  int logToConsole = (s_consoleFD != 0) && (level >= s_consoleLevel);
  int logToFile = (s_logFile != NULL) && (level >= s_fileLevel);

  if(logToConsole || logToFile)
  {
    struct tm *localTime;
    struct timeval tv;
    va_list args;
    char buf[1024*20];
    size_t size = 0;
    ssize_t res = 0;
    size_t current_point = 0;

    gettimeofday(&tv,NULL);
    localTime = localtime(&tv.tv_sec);

    /* print time, level and tag */
    size += snprintf(buf + size, sizeof(buf) - size, "[%4d/%02d/%02d][%02d:%02d:%02d.%06d] ",
                     localTime->tm_year + 1900, localTime->tm_mon + 1, localTime->tm_mday,
                     localTime->tm_hour, localTime->tm_min, localTime->tm_sec, (int)tv.tv_usec);

    /* print message */
    va_start(args, format);
    size += vsnprintf(buf + size, sizeof(buf) - size, format, args);
    va_end(args);

    if(size >= (int)sizeof(buf))
    {
      /* there were not enough space in the buf */
      size = sizeof(buf) - 1;
    }

    buf[size] = 0;

    if(logToConsole)
    {
      while((res=write(s_consoleFD, &buf[current_point], size)) != -1)
      {
        if(res == size)
        {
          break;
        }
        else
        {
          size = size - res;
          current_point = current_point + res;
        }
      }
    }

    if(logToFile)
    {
      fputs(buf, s_logFile);
      fflush(s_logFile);
    }
  }
}
