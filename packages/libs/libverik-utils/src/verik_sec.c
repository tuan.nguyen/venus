#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <time.h>
#include <uci.h>
#include <json-c/json.h>
// #include <curl/curl.h>

#include <openssl/bio.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/buffer.h>

#include "verik_utils.h"
#include "slog.h"

void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}

int decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key,
  unsigned char *iv, unsigned char *plaintext)
{
    EVP_CIPHER_CTX *ctx;

    int len;

    int plaintext_len;

    /* Create and initialise the context */
    if(!(ctx = EVP_CIPHER_CTX_new())) handleErrors();

    /* Initialise the decryption operation. IMPORTANT - ensure you use a key
    * and IV size appropriate for your cipher
    * In this example we are using 256 bit AES (i.e. a 256 bit key). The
    * IV size for *most* modes is the same as the block size. For AES this
    * is 128 bits */
    if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
    handleErrors();

    /* Provide the message to be decrypted, and obtain the plaintext output.
    * EVP_DecryptUpdate can be called multiple times if necessary
    */
    if(1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len))
    handleErrors();
    plaintext_len = len;

    /* Finalise the decryption. Further plaintext bytes may be written at
    * this stage.
    */
    if(1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len)) handleErrors();
    plaintext_len += len;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return plaintext_len;
}

int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
  unsigned char *iv, unsigned char *ciphertext)
{
    EVP_CIPHER_CTX *ctx;

    int len;

    int ciphertext_len;

    /* Create and initialise the context */
    if(!(ctx = EVP_CIPHER_CTX_new())) handleErrors();

    /* Initialise the encryption operation. IMPORTANT - ensure you use a key
    * and IV size appropriate for your cipher
    * In this example we are using 256 bit AES (i.e. a 256 bit key). The
    * IV size for *most* modes is the same as the block size. For AES this
    * is 128 bits */
    if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv))
    handleErrors();

    /* Provide the message to be encrypted, and obtain the encrypted output.
    * EVP_EncryptUpdate can be called multiple times if necessary
    */
    if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
    handleErrors();
    ciphertext_len = len;

    /* Finalise the encryption. Further ciphertext bytes may be written at
    * this stage.
    */
    if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len)) handleErrors();
    ciphertext_len += len;

    /* Clean up */
    EVP_CIPHER_CTX_free(ctx);

    return ciphertext_len;
}

/**
 * Create an 128 bit key and IV using the supplied key_data. salt can be added for taste.
 * Fills in the encryption and decryption ctx objects and returns 0 on success
 **/
char* base64Encode(unsigned char *str, int len)
{
    BIO *bio, *b64;
    BUF_MEM *buf_encode;
    unsigned char *encStr;

    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new(BIO_s_mem());
    b64 = BIO_push(b64, bio);
    BIO_write(b64, str, len);
    BIO_flush(b64);

    BIO_get_mem_ptr(b64, &buf_encode);

    encStr = (unsigned char*)calloc(1, buf_encode->length+1);
    strncpy((char*)encStr, buf_encode->data, buf_encode->length);
    encStr[buf_encode->length] = '\0';

    BIO_free_all(b64);
    return (char *)encStr;
}

int base64Decode(char *str, char *decStr, int *decode_len)
{
    char outBuf[SIZE_256B];
    int len = 0;
    BIO *bio, *b64;

    memset(outBuf,0,SIZE_256B);
    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new_mem_buf(str, strlen(str));
    bio = BIO_push(b64, bio);
    len = BIO_read(b64, &outBuf, SIZE_256B);
    *decode_len = len;
    memcpy(decStr, outBuf, len);

    BIO_free_all(b64);
    return 0;
}

int psk_encrypt(char *userid, char *psk, char *final_b64)
{
    char hubID[SIZE_32B];
    get_hub_id(hubID, sizeof(hubID));
    strcat(hubID,"99");

    printf("hubID = %s\n", hubID);

    /* Set up the key and iv. Do I need to say to not hard code these in a
    * real application? :-)
    */

    /* A 256 bit key */
    unsigned char *key = (unsigned char *)userid;

    /* A 128 bit IV */
    unsigned char *iv = (unsigned char *)hubID;

    /* Message to be encrypted */
    unsigned char *plaintext = (unsigned char *)psk;

    /* Buffer for ciphertext. Ensure the buffer is long enough for the
    * ciphertext which may be longer than the plaintext, dependant on the
    * algorithm and mode
    */
    unsigned char ciphertext[SIZE_256B];

    int ciphertext_len;

    /* Initialise the library */
    ERR_load_crypto_strings();
    OpenSSL_add_all_algorithms();
    OPENSSL_config(NULL);

    /* Encrypt the plaintext */
    ciphertext_len = encrypt (plaintext, strlen ((char *)plaintext), key, iv,
                            ciphertext);

    /* Do something useful with the ciphertext here */
    printf("Ciphertext is:\n");
    BIO_dump_fp (stdout, (const char *)ciphertext, ciphertext_len);
    char *final = base64Encode(ciphertext, ciphertext_len);
    printf("final = %s\n", final);
    strcpy(final_b64, final);
    if(final)
    {
        free(final);
    }
    int b64_decode_length;
    unsigned char b64_outBuf[SIZE_256B];
    base64Decode(final_b64, (char *)b64_outBuf, &b64_decode_length);

    printf("b64_decode_length %d\n", b64_decode_length);
    BIO_dump_fp (stdout, (const char *)b64_outBuf, b64_decode_length);

    /* Buffer for the decrypted text */
    unsigned char decryptedtext[SIZE_256B];
    int decryptedtext_len;
    /* Decrypt the ciphertext */
    decryptedtext_len = decrypt(b64_outBuf, b64_decode_length, key, iv, decryptedtext);
    decryptedtext[decryptedtext_len] = '\0';
    /* Show the decrypted text */
    printf("Decrypted text is:\n");
    printf("%s\n", decryptedtext);

    /* Clean up */
    EVP_cleanup();
    ERR_free_strings();

    return ciphertext_len;
}
