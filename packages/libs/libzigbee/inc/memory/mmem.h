#ifndef __MMEM_H__
#define __MMEM_H__

/*---------------------------------------------------------------------------*/
/**
 * \brief      Get a pointer to the managed memory
 * \param m    A pointer to the struct mmem 
 * \return     A pointer to the memory block, or NULL if memory could
 *             not be allocated. 
 * \author     Adam Dunkels
 *
 *             This macro is used to get a pointer to a memory block
 *             allocated with mmem_alloc().
 *
 * \hideinitializer
 */
#define MMEM_PTR(m) (struct mmem *)(m)->ptr

struct mmem 
{
  struct mmem *next;
  unsigned int size;
  void *ptr;
};

/* XXX: tagga minne med "interrupt usage", vilke g�r att man �r
   speciellt varsam under free(). */

int  mmem_alloc(struct mmem *m, unsigned int size);
void mmem_free(struct mmem *);
void mmem_init(void);

#endif