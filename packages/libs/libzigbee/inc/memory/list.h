#ifndef __LIST_H__
#define __LIST_H__

#define LIST_CONCAT2(s1, s2) s1##s2
#define LIST_CONCAT(s1, s2) LIST_CONCAT2(s1, s2)

#define LIST(name) \
 		static void *LIST_CONCAT(name,_list) = NULL; \
		static list_t name = (list_t)&LIST_CONCAT(name,_list)

#define LIST_STRUCT(name) \
		void *LIST_CONCAT(name,_list); \
		list_t name

#define LIST_STRUCT_INIT(struct_ptr, name)                          \
do {                                                                \
   (struct_ptr)->name = &((struct_ptr)->LIST_CONCAT(name,_list));   \
   (struct_ptr)->LIST_CONCAT(name,_list) = NULL;                    \
   zigbee_list_init((struct_ptr)->name);                                   \
} while(0)

/**
* The linked list type.
* */
typedef void ** list_t;

void   zigbee_list_init(list_t list);
void * zigbee_list_head(list_t list);
void * zigbee_list_tail(list_t list);
void * zigbee_list_pop (list_t list);
void   zigbee_list_push(list_t list, void *item);

void * zigbee_list_chop(list_t list);

void   zigbee_list_add(list_t list, void *item);
void   zigbee_list_remove(list_t list, void *item);

int    zigbee_list_length(list_t list);

void   zigbee_list_copy(list_t dest, list_t src);

void   zigbee_list_insert(list_t list, void *previtem, void *newitem);

void * zigbee_list_item_next(void *item);

#endif 