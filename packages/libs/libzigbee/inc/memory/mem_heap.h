#ifndef MEM_HEAP_H
#define MEM_HEAP_H

#include "mmem.h"
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#define MAX_MEM_PTR_POOL 30     ///< Define the number of mem pointers here

/**************************************************************************/
/*!
    Memory pointer structure - This is the structure that will go into all
    the tables and lists, and is a generic handle to the underlying memory.
    You will need to access this structure via the pre-defined handle names.
*/
/**************************************************************************/
typedef struct _mem_ptr_t
{
    struct _mem_ptr_t *next;    ///< Next pointer in memory
    bool    alloc;              ///< Allocated
    struct  mmem mmem_ptr;      ///< The actual pointer to the pointer to the mem block
} mem_ptr_t;

void		 mem_heap_init();
mem_ptr_t *  mem_heap_alloc(uint8_t size);
void 		 mem_heap_free(mem_ptr_t *mem_ptr);

#endif // MEM_