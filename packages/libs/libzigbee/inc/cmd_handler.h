#ifndef _ZIGBBE_APIs_H_
#define _ZIGBBE_APIs_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include "zcl/zcl_constants.h"
#include "zcl/cluster_id.h"
#include "zcl/attribute_id.h"
#include "zcl/zcl_zll_commissioning.h"
#include "zcl/zcl_color_control.h"
#include "zcl/zcl_ms_acceleration.h"
#include "zcl/zcl_shade_configuration.h"
#include "zcl/zcl_door_lock.h"
#include "zcl/zcl_ias_zone.h"
#include "zcl/zcl_temperature_measurement.h"
#include "zcl/zcl_illuminance_measurement.h"
#include "zcl/zcl_illuminance_level_sensing.h"
#include "zcl/zcl_humidity_measurement.h"
#include "zcl/zcl_ms_humidity_measurement.h"
#include "zcl/zcl_occupancy_sensing.h"
#include "zcl/zcl_ms_occupancy_sensing.h"
#include "zcl/zcl_level.h"
#include "zcl/zcl_thermostat.h"
#include "zcl/zcl_window_covering.h"
#include "zcl/zcl_pump_configuration_and_control.h"
#include "zcl/zcl_on_off.h"
#include "zcl/zcl_grp.h"
#include "zcl/zcl_scenes.h"
#include "zcl/zcl_id.h"
#include "zcl/zcl_basic.h"
#include "zcl/zcl_power_configuration.h"
#include "zcl/zcl.h"
#include "zcl/misc/dev_dbg.h"

#define MAX_PARMS 				         30
#define NULLCHAR                 0
#define LF                     '\n'
#define CR                     '\r'

#define XX                      0
#define XXXX                    1
#define nn                      2
#define ss                      3
#define bb                      4
#define cc                      5
#define _PANID_                   6
#define _EPANID_                  7
#define _channel_                       8
#define _password_                  9
#define _EUI64_                   10

#define BUF_LINE                          512
#define FIRMWARE_REVISION_R309

#define S_REGISTER_Prompt_Enable_2        0x0F

#define S_REGISTER_ENDPOINTS_CURRENT      0x40
#define S_REGISTER_ENDPOINTS_DEFAULT      0x41
#define S_REGISTER_CLUSTER_ID_CURRENT     0x42
#define S_REGISTER_CLUSTER_ID_DEFAULT     0x43
#define S_REGISTER_PROFILE_ID_CURRENT     0x44
#define S_REGISTER_PROFILE_ID_DEFAULT     0x45
#define SLEEP_MILISECOND_UNIT 1000

//for door lock state
#define DOOR_OPEN 1
#define DOOR_CLOSE 0

extern int debug_level;
#define LOG_DBG (1)
#define LOG(level, ...)  \
do {  \
if (level <= debug_level) { \
  printf("%s:%d:", __FILE__, __LINE__); \
  printf(__VA_ARGS__); \
  printf("\n"); \
} \
} while (0)

typedef struct command_t 
{
  const char *name;
  int cmdnum;
}command;

enum cmdname {
  Rx_OK = 0,
  Rx_ERROR,
  Rx_JPAN,
  Rx_FFD,
  Rx_SED,
  Rx_N,
  Rx_ActEpDesc,
  Rx_SimpleDesc,
  Rx_ProfileID,
  Rx_DeviceID,
  Rx_UCASTB,
  Rx_SEQ,
  Rx_ACK,
  Rx_NACK,
  Rx,
  Rx_AddrResp,
  Rx_PANSCAN,
  Rx_INTERPAN,
  Rx_InCluster,
  Rx_OutCluster,
  Rx_NEWNODE,
  Rx_NODELEFT,
  Rx_Bind,
  Rx_Unbind,
  Rx_No,
  Rx_Length,
  R309C
};



typedef struct __msg_receive 
{
    int number_parms;
    char data[BUF_LINE];
    char *parms[MAX_PARMS];

}msg_receive ;





int zigbeeInitialize(char* pdevID,zigbee_notify_queue_t* notify);
int zigbee_detruct(int fd);
int zigbee_controller_info(uint64_t* euiAddr);
int zigbeeOpenCloseNetwork(uint8_t mode);
int zigbeeTelegesisReset(int fd);
int zigbeeRemoveNode(int fd, uint16_t desNodeID);
int zigbeeGetRemoteBindingTable(int fd, uint16_t desNodeID, remoteBindingNode *rbt,uint8_t* rbt_noe);
int zigbee_read_color(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint8_t *hue, uint8_t * saturation);
int zigbee_request_eui64(int fd, uint64_t desEui64ID, uint16_t desNodeID);


int appZclReadAttrib(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t clust_id, uint16_t ProfileID, uint16_t manu_code, uint16_t* attrib_list,uint8_t attrib_num, readAttrData* rData);
int appZclWriteAttrib(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t clust_id, uint16_t ProfileID, uint16_t manu_code, zclAttribute* attrib_list, uint8_t attrib_num);
int app_zcl_config_rpt(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t clust_id, uint16_t ProfileID, uint16_t manu_code, uint16_t attrib_id, uint16_t min_intv, uint16_t max_intv, uint32_t change);
int appZclOnOffReq(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID,uint8_t cmd,zclOnOffReq req);
int appZclDoorLockReq(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID,uint8_t cmd,zclDoorLockReq req);
int appZclLevelReq(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID,uint8_t cmd, zclLevelReq req);
int app_zcl_ias_zone_req(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID,uint8_t cmd, zclIasZoneReq req);
int appCreateBindingOnRemote(int fd, uint16_t target_addr, uint64_t destAddr, uint16_t clust_id, 
  uint64_t src_addr, uint8_t srcEndpoint, uint8_t destEndpoint);
int appDeleteBindingOnRemote(int fd, uint16_t target_addr, uint64_t destAddr, uint16_t clust_id, 
  uint64_t src_addr, uint8_t srcEndpoint, uint8_t destEndpoint);
int appZclIdReq(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID,uint8_t cmd,zclIdReq req);
int app_zcl_zll_commissioning_req(int fd, uint64_t destAddr, uint16_t dest_PAN, uint16_t ProfileID,uint8_t cmd, zclZllCommissioningReq req);
int appZclColorControlReq(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID,uint8_t cmd,zclColorControlReq req);
int app_zcl_grp_req(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID,uint8_t cmd,zcl_grp_req_t req, grp_cmd_response *res);

int app_multi_zcl_level_req(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID,uint8_t cmd, zclLevelReq req);
int app_multi_zcl_on_off_req(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID,uint8_t cmd);

int app_touchlink_scan(int fd);
void pushNotificationZigbeeToHandler(uint8_t bStatus, uint8_t* pData, uint32_t len);


#endif