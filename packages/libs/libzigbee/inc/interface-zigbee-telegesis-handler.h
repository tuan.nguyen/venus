#ifndef __INTERFACE_ZIGBEE_TELEGESIS_HANDLER_H__
#define __INTERFACE_ZIGBEE_TELEGESIS_HANDLER_H__

#include "cmd_handler.h"


#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_NODE_ADD                              0x01
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_NODE_REMOVE                           0x02
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_NODE_GET_CONTROLLER_INFO              0x03
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_NODE_SET_DEFAULT                      0x04
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_GET_BINDING_TABLE                     0x05
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_CREATE_BINDING_ON_REMOTE              0x06
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_DELETE_BINDING_ON_REMOTE              0x07
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_ON_OFF_REQ                        0x08
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_DOOR_LOCK_REQ                     0x09
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_LEVEL_REQ                         0x0A
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_IAS_ZONE_REQ                      0x0B
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_COLOR_CONTROL_REQ                 0x0C
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_ID_REQ                            0x0D
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_ZLL_COMMISSIONING_REQ             0x0E
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_READ_ATTRIB                       0x0F
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_WRITE_ATTRIB                      0x10
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_DISCOVER_ATTRIB                   0x11

#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZLL_CHANNEL_SCAN                      0x12
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZLL_IDENTIFY                          0x13
#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZLL_RESET                             0x14


#define ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_NODE_REQUEST_SPECIFICATION            0xFF

#define MAX_NO_ELEMENTS_REMOTE_BINDING_TABLE                                16
#define MAX_NO_ZCL_WRITE_ATTRIBUTE                                          5
#define HOME_AUTOMATION_PROFILE_ID                                          0x0104
#define DEFAULT_MANUFACTURER_CODE                                           0xFFFF
#define MAX_NO_ZCL_READ_ATTRIBUTE                                           5

/*Define Command*/
//----------------------------------------------------------------


//system
#define DEFAULT_RETRY_SEND_CMD 1
#define STATE_IDLE 0x01
#define STATE_WORKING 0x02
//default
#define BROADCAST_DES       0xFFFF
#define DEFAULT_SRC_ENDPOINT 1
#define DEFAULT_DEST_ENDPOINT 1
#define DEFAULT_CLUSTER 0x0000
#define DEFAULT_ATTRIBUTE 0x0000
//network
#define ENABLE_OPEN_NETWORK 0x01
#define DISABLE_OPEN_NETWORK 0x00
//status of command
#define NO_RESPONSE_FROM_DEV 0xF1
#define ENUM_NO_CMD_RESPONSE 0xF2
#define ENUM_NO_ZCL_CMD_RES 0xF3
#define NO_ACK_FROM_DEV     0xF4
//zcl cli
#define EMBER_CHANGE_CHANNEL_TIMEOUT 0xF5
#define EMBER_OPEN_NWK_TIMEOUT 0xF6
#define WAITING_FOR_DEVICE_RES_TIMEOUT 0xF7
#define ENUM_NO_ZDO_CMD_RES 0xF8
#define ZDO_BINDING_ALL_TABLE 0xF9

//timer waiting for 
#define WAITING_FOR_DEVICE_RESPONSE 10000

typedef struct tagZWParam_s {
    uint8_t command;
    int     ret;
    uint8_t param1;
    uint16_t param2;
    uint16_t param3;
    uint64_t param4;//euiAddr
    uint64_t param5;//euiAddr
    uint16_t destAddr;
    uint8_t srcEndpoint;
    uint8_t destEndpoint;
    uint16_t param6[MAX_NO_ZCL_READ_ATTRIBUTE];//atribute list, read multi attribute
    union
    {
        remoteBindingNode rbt[MAX_NO_ELEMENTS_REMOTE_BINDING_TABLE];
        zclOnOffReq reqZclOnOffReq;
        zclDoorLockReq reqZclDoorLockReq;
        zclLevelReq reqZclLevelReq;
        zclIasZoneReq reqZclIasZoneReq;
        zclColorControlReq reqZclColorControlReq;
        zclIdReq reqZclIdReq;
        zclZllCommissioningReq reqZclZllCommissioningReq;
        readAttrData rData[MAX_NO_ZCL_READ_ATTRIBUTE];
        zclAttribute zclWriteAttr[MAX_NO_ZCL_WRITE_ATTRIBUTE];
        nodeDescriptor controllerInfo;
        discAttrResData discData;
    };
} ZigbeeTelegesisHandlerParam;//interface with with zigbee-telegesis-handler
int zigbeeTelegesisHandlerSendCommand(ZigbeeTelegesisHandlerParam *pzbParam);
int zigbeeTelegesiInitialize(char *pdevID, zigbee_notify_queue_t *notify);

#endif //__INTERFACE_ZIGBEE_TELEGESIS_HANDLER_H__