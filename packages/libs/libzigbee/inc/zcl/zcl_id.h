#ifndef ZCL_ID_H
#define ZCL_ID_H

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"
#include "zcl_gen.h"
/*! attribute list size - this will be one larger than the actual list size
    to hold the end marker
*/

#define ZCL_ID_ATTRIB_LIST_SZ               2

/**************************************************************************/
/*!
    Constants for this cluster
*/
/**************************************************************************/
typedef enum _zcl_id_enums_t
{
    ZCL_ID_ATTRIB               = 0x00,
    
    ZCL_ID_CMD_ID               = 0x00,
    ZCL_ID_CMD_ID_QUERY         = 0x01,

    ZCL_ID_CMD_ID_QUERY_RESP    = 0x00,

    ZCL_ID_ACTION_ID_ON         = 0x00,
    ZCL_ID_ACTION_ID_OFF        = 0x01
} zcl_id_enums_t;

/**************************************************************************/
/*!
    This is the data for the attribute.
*/
/**************************************************************************/
typedef struct _zcl_id_data_t
{
    uint16_t id_time;
} zcl_id_data_t;

/**************************************************************************/
/*!
    This struct holds the actual attribute list as well as the attribute
    data.
*/
/**************************************************************************/
typedef struct _zcl_id_attrib_list_t
{
    zclAttribute    list[ZCL_ID_ATTRIB_LIST_SZ];
    zcl_id_data_t   data;
} zcl_id_attrib_list_t;


typedef struct _zclIdReq
{
  uint16_t timeout;
} zclIdReq;
/**************************************************************************/
/*!

*/
/**************************************************************************/


// prototypes
void    zcl_id_init(zcl_id_attrib_list_t *attrib_list);
uint8_t zcl_id_gen_req(uint8_t *data, zcl_hdr_t *hdr, zclIdReq *req);
void    zcl_id_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);

#endif
