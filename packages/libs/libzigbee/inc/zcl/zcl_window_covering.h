
#ifndef ZCL_WINDOW_COVERING_H
#define ZCL_WINDOW_COVERING_H
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"
#include "zcl_gen.h"

#define ZCL_WINDOW_COVERING_ATTRIB_LIST_SZ    21              ///< Window covering attribute list size
#define ZCL_WINDOW_COVERING_INTERMEDIATE_SETPOINT_MAX_LEN         32



typedef enum _zcl_window_covering_enum_t
{
    // Window covering information
    ZCL_WINDOW_COVERING_ATTRIB_WINDOW_COVERING_TYPE                      = 0x0000,        
    ZCL_WINDOW_COVERING_ATTRIB_PHYSICAL_CLOSED_LIMIT_LIFT                = 0x0001,       
    ZCL_WINDOW_COVERING_ATTRIB_PHYSICAL_CLOSED_LIMIT_TILT                = 0x0002, 
    ZCL_WINDOW_COVERING_ATTRIB_CURRENT_POSITION_LIFT                     = 0x0003,
    ZCL_WINDOW_COVERING_ATTRIB_CURRENT_POSITION_TILT                     = 0x0004,
    ZCL_WINDOW_COVERING_ATTRIB_NUMBER_OF_ACTUATIONS_LIFT                 = 0x0005,
    ZCL_WINDOW_COVERING_ATTRIB_NUMBER_OF_ACTUATIONS_TILT                 = 0x0006,
    ZCL_WINDOW_COVERING_ATTRIB_CONFIG_STATUS                             = 0x0007,
    ZCL_WINDOW_COVERING_ATTRIB_CURRENT_POSITION_LIFT_PERCENTAGE          = 0x0008,
    ZCL_WINDOW_COVERING_ATTRIB_CURRENT_POSITION_TILT_PERCENTAGE          = 0x0009,

    // Window covering settings
    ZCL_WINDOW_COVERING_ATTRIB_INSTALLED_OPEN_LIMIT_LIFT                 = 0x0010,        
    ZCL_WINDOW_COVERING_ATTRIB_INSTALLED_CLOSED_LIMIT_LIFT               = 0x0011,       
    ZCL_WINDOW_COVERING_ATTRIB_INSTALLED_OPEN_LIMIT_TILT                 = 0x0012,        
    ZCL_WINDOW_COVERING_ATTRIB_INSTALLED_CLOSED_LIMIT_TILT               = 0x0013,       
    ZCL_WINDOW_COVERING_ATTRIB_VELOCITY_LIFT_SECOND                      = 0x0014,       
    ZCL_WINDOW_COVERING_ATTRIB_ACCELERATION_TIME_LIFT                    = 0x0015,       
    ZCL_WINDOW_COVERING_ATTRIB_DECELERATION_TIME_LIFT                    = 0x0016,       
    ZCL_WINDOW_COVERING_ATTRIB_MODE                                      = 0x0017,       
    ZCL_WINDOW_COVERING_ATTRIB_INTERMEDIATE_SETPOINT_LIFT                = 0x0018,       
    ZCL_WINDOW_COVERING_ATTRIB_INTERMEDIATE_SETPOINT_TILT                = 0x0019,       

    //Window covering cmd
    ZCL_WINDOW_COVERING_CMD_UP_OPEN                                      = 0x00,
    ZCL_WINDOW_COVERING_CMD_DOWN_CLOSE                                   = 0x01,
    ZCL_WINDOW_COVERING_CMD_STOP                                         = 0x02,
    ZCL_WINDOW_COVERING_CMD_GO_TO_LIFT_VALUE                             = 0x04,
    ZCL_WINDOW_COVERING_CMD_GO_TO_LIFT_PERCENTAGE                        = 0x05,
    ZCL_WINDOW_COVERING_CMD_GO_TO_TILT_VALUE                             = 0x07,
    ZCL_WINDOW_COVERING_CMD_GO_TO_TILT_PERCENTAGE                        = 0x08,
    
 }zcl_window_covering_enum_t;

typedef struct _zcl_window_covering_data_t
{
    uint8_t         window_covering_type;
    uint16_t        physical_closed_limit_lift;
    uint16_t        physical_closed_limit_tilt;
    uint16_t        current_position_lift;
    uint16_t        current_position_tilt;
    uint16_t        number_of_actuations_lift;
    uint16_t        number_of_actuations_tilt;
    uint8_t         config_status;
    uint8_t         current_position_lift_percentage;
    uint8_t         current_position_tilt_percentage;
    uint16_t        installed_open_limit_lift;
    uint16_t        installed_closed_limit_lift;
    uint16_t        installed_open_limit_tilt;
    uint16_t        installed_closed_limit_tilt;
    uint16_t        velocity_lift_second;
    uint16_t        acceleration_time_lift;
    uint16_t        deceleration_time_lift;
    uint8_t         mode;
    char            intermediate_setpoint_lift[ZCL_WINDOW_COVERING_INTERMEDIATE_SETPOINT_MAX_LEN];
    char            intermediate_setpoint_tilt[ZCL_WINDOW_COVERING_INTERMEDIATE_SETPOINT_MAX_LEN];


} zcl_window_covering_data_t;

typedef struct _zcl_window_covering_go_to_lift_value_req_t
{
  uint16_t          lift_value;
} zcl_window_covering_go_to_lift_value_req_t;

typedef struct _zcl_window_covering_go_to_lift_percentage_req_t
{
  uint8_t          percentage_lift_value;
} zcl_window_covering_go_to_lift_percentage_req_t;

typedef struct _zcl_window_covering_go_to_tilt_value_req_t
{
  uint16_t          tilt_value;
} zcl_window_covering_go_to_tilt_value_req_t;

typedef struct _zcl_window_covering_go_to_tilt_percentage_req_t
{
  uint8_t          percentage_tilt_value;
} zcl_window_covering_go_to_tilt_percentage_req_t;


typedef struct _zcl_window_covering_req_t
{
    zcl_window_covering_go_to_lift_value_req_t          go_to_lift_value;
    zcl_window_covering_go_to_lift_percentage_req_t     go_to_lift_percentage;
    zcl_window_covering_go_to_tilt_value_req_t          go_to_tilt_value;
    zcl_window_covering_go_to_tilt_percentage_req_t     go_to_tilt_percentage;
    
} zcl_window_covering_req_t;


/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_window_covering_attrib_list_t
{
    zclAttribute                list[ZCL_WINDOW_COVERING_ATTRIB_LIST_SZ];
    zcl_window_covering_data_t  data;
} zcl_window_covering_attrib_list_t;




void    zcl_window_covering_init(zcl_window_covering_attrib_list_t *attrib_list);
uint8_t zcl_window_covering_gen_req(uint8_t *data, zcl_hdr_t *hdr, zcl_window_covering_req_t *req);
void    zcl_window_covering_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);

#endif