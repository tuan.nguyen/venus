
#ifndef ZCL_POLL_CONTROL_H
#define ZCL_POLL_CONTROL_H
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"
#include "zcl_gen.h"

#define ZCL_POLL_CONTROL_ATTRIB_LIST_SZ    8              ///< Poll control attribute list size


typedef enum _zcl_poll_control_enum_t
{
    // Poll control attrib id
    ZCL_POLL_CONTROL_ATTRIB_CHECK_IN_INTERVAL                      = 0x0000,        
    ZCL_POLL_CONTROL_ATTRIB_LONG_POLL_INTERVAL                     = 0x0001,       
    ZCL_POLL_CONTROL_ATTRIB_SHORT_POLL_INTERVAL                    = 0x0002, 
    ZCL_POLL_CONTROL_ATTRIB_FAST_POLL_INTERVAL                     = 0x0003,
    ZCL_POLL_CONTROL_ATTRIB_CHECK_IN_INTERVAL_MIN                  = 0x0004,
    ZCL_POLL_CONTROL_ATTRIB_LONG_POLL_INTERVAL_MIN                 = 0x0005,
    ZCL_POLL_CONTROL_ATTRIB_FAST_POLL_INTERVAL_MAX                 = 0x0006,
   
    // Poll control cmd received
    ZCL_POLL_CONTROL_CMD_CHECK_IN_RESPONSE                         = 0x00,
    ZCL_POLL_CONTROL_CMD_FAST_POLL_STOP                            = 0x01,
    ZCL_POLL_CONTROL_CMD_SET_LONG_POLL_INTERVAL                    = 0x02,
    ZCL_POLL_CONTROL_CMD_SET_SHORT_POLL_INTERVAL                   = 0x03,

    //Poll control cmd generated 
    ZCL_POLL_CONTROL_CMD_CHECK_IN                                  = 0x00,


 }zcl_poll_control_enum_t;

typedef struct _zcl_poll_control_data_t
{
    uint32_t        check_in_interval;
    uint32_t        long_poll_interval;
    uint16_t        short_poll_interval;
    uint16_t        fast_poll_timeout;
    uint32_t        check_in_interval_min;
    uint32_t        long_poll_interval_min;
    uint16_t        fast_poll_timeout_max;
} zcl_poll_control_data_t;


typedef struct _zcl_poll_control_check_in_response_req_t
{
    bool        start_fast_polling;
    uint16_t    fast_poll_timeout;
} zcl_poll_control_check_in_response_req_t;

typedef struct _zcl_poll_control_set_long_poll_interval_req_t
{
    uint32_t    new_long_poll_interval;
} zcl_poll_control_set_long_poll_interval_req_t;

typedef struct _zcl_poll_control_set_short_poll_interval_req_t
{
    uint16_t    new_short_poll_interval;
} zcl_poll_control_set_short_poll_interval_req_t;


typedef struct _zcl_poll_control_req_t
{
    zcl_poll_control_check_in_response_req_t            check_in_response;
    zcl_poll_control_set_long_poll_interval_req_t       set_long_poll_interval;
    zcl_poll_control_set_short_poll_interval_req_t      set_short_poll_interval;
  
} zcl_poll_control_req_t;


/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_poll_control_attrib_list_t
{
    zclAttribute                list[ZCL_POLL_CONTROL_ATTRIB_LIST_SZ];
    zcl_poll_control_data_t     data;
} zcl_poll_control_attrib_list_t;




void    zcl_poll_control_init(zcl_poll_control_attrib_list_t *attrib_list);
uint8_t zcl_poll_control_gen_req(uint8_t *data, zcl_hdr_t *hdr, zcl_poll_control_req_t *req);
void    zcl_poll_control_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);

#endif