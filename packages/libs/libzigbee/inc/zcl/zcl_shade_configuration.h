
#ifndef ZCL_SHADE_CONFIGURATION_H
#define ZCL_SHADE_CONFIGURATION_H
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"
#include "zcl_gen.h"

#define ZCL_SHADE_CONFIGURATION_ATTRIB_LIST_SZ    6              ///< Thermostat attribute list size




typedef enum _zcl_shade_configuration_enum_t
{
    // Shade Information attrib id
    ZCL_SHADE_CONFIGURATION_ATTRIB_PHYSICAL_CLOSED_LIMIT        = 0x0000,        
    ZCL_SHADE_CONFIGURATION_ATTRIB_MOTOR_STEP_SIZE              = 0x0001,       
    ZCL_SHADE_CONFIGURATION_ATTRIB_STATUS                       = 0x0002, 
    
    // Shade Setting attrib id
    ZCL_SHADE_CONFIGURATION_ATTRIB_CLOSED_LIMIT                 = 0x0010,        
    ZCL_SHADE_CONFIGURATION_ATTRIB_MODE                         = 0x0011,       
   

 }zcl_shade_configuration_enum_t;

typedef struct _zcl_shade_configuration_data_t
{
    uint16_t        physical_closed_limit;
    uint8_t         motor_step_size;
    uint8_t         status;
    
    uint16_t        closed_limit;
    uint8_t         mode;
    
} zcl_shade_configuration_data_t;




/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_shade_configuration_attrib_list_t
{
    zclAttribute                      list[ZCL_SHADE_CONFIGURATION_ATTRIB_LIST_SZ];
    zcl_shade_configuration_data_t    data;
} zcl_shade_configuration_attrib_list_t;




void    zcl_shade_configuration_init(zcl_shade_configuration_attrib_list_t *attrib_list);
void    zcl_shade_configuration_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);

#endif