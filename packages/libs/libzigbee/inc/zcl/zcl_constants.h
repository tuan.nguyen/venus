/*******************************************************************
    Copyright (C) 2009 FreakLabs
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    1. Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
    3. Neither the name of the the copyright holder nor the names of its contributors
       may be used to endorse or promote products derived from this software
       without specific prior written permission.
    4. This software is subject to the additional restrictions placed on the
       Zigbee Specification's Terms of Use.

    THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS'' AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
    OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
    SUCH DAMAGE.

    Originally written by Christopher Wang aka Akiba.
    Please post support questions to the FreakLabs forum.

*******************************************************************/
/*!
    \file constants.h
    \ingroup misc
    \brief FreakZ stack constants

    This file contains many of the constants used in the stack.
*/
/*******************************************************************/
#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>


#define CLOCK_SECOND    32

/* Define whether the sim environment is being used or not */
#define SIM_ENV 1

/* Zigbee 2006 APS Defined Constants */
/* Max frame retries at the APS level */
#define APS_MAX_FRAME_RETRIES   3
/*
 * Define amount of time to wait in seconds for an ACK before timeout.
 * Curr value is dummy value. Here is the real value: 0.05 * (2 * nwkcMaxDepth)
 */
#define APS_ACK_WAIT_DURATION       2

/* Zigbee 2007 NWK Defined constants */
#define NWK_COORDINATOR_CAPABLE     true        ///< Device is capable of being coordinator
#define NWK_DEFAULT_SECURITY_LEVEL  ENC_MIC_64  ///< Default security level
#define NWK_DISCOVERY_RETRY_LIMIT   0x03        ///< Number of times to retry network discovery
#define NWK_MIN_HDR_OVERHEAD        0x08        ///< Minimum num bytes for NWK header
//0x2AF8 - add one second to the actual
// value to reflect uncertainty in the timer
#define NWK_RTE_DISC_TIME           0x7D0       ///< Time allowed for route discovery before operation is ended
#define NWK_BRC_JITTER              0x40        ///< Max value of broadcast jitter
#define NWK_PASSIVE_ACK_TIMEOUT     1000        ///< Timeout value for passive acks during broadcast
#define NWK_MAX_BRC_RETRIES         0x03        ///< Number of broadcast retries
#define NWK_RREQ_RETRIES            0x03        ///< Number of route request retries
#define NWK_RREQ_RETRY_INTERVAL     0xFE        ///< Time in between route request retries
#define NWK_MIN_RREQ_JITTER         0x01        ///< Min time of rreq jitter
#define NWK_MAX_RREQ_JITTER         0x40        ///< Max time of rreq jitter

// 802.15.4 MAC Defined constants
// Tick value is 1 msec so all time values are converted to units of msec

/*! 802.15.4 defined slot duration. Used to calculate other values in MAC layer.
60 symbols = 960 usec ~ 1 msec*/
#define aBaseSlotDuration           1
#define aBaseSuperframeDuration     (aBaseSlotDuration * aNumSuperframeSlots)  ///< Length of superframe
#define aGTSDescPersistenceTime     4                   ///< Unused in FreakZ
#define aMaxBeaconOverhead          75                  ///< Unused in FreakZ
#define aMaxBeaconPayloadLength     (aMaxPHYPacketSize - aMaxBeaconOverhead) ///< Max payload length for beacons
#define aMaxLostBeacons             4                   ///< Unused in FreakZ
#define aMaxMACPayloadSize          (aMaxPHYPacketSize - aMinMPDUOverhead)      ///< Max payload length for MAC frames
#define aMaxMPDUUnsecuredOverhead   25  ///< Unused in FreakZ
#define aMaxSIFSFrameSize           18  ///< Unused in FreakZ
#define aMinCAPLength               7   ///< 440 symbols = 7040 usec ~7 msec
#define aMinMPDUOverhead            9   ///< Min MAC header size
#define aNumSuperframeSlots         16  ///< Unused in FreakZ
#define aUnitBackoffPeriod          1   ///< 20 symbols = 320 usec...round to 1 msec
#define aMinLIFSPeriod              40  ///< Unused in FreakZ
#define aminSIFSPeriod              12  ///< Unused in FreakZ
#define aMacAckWaitDuration                     2   ///< 54 symbols = 864 usec ~ 1 msec...round to 2 msec
#define aMacMaxFrameTotalWaitTime   20  ///< 1220 symbols = 19500 usec ~ 20 msec
#define aMacResponseWaitTime        32  ///< default value = 32 * aBaseSlotDuration ~ 32 msec
#define aMacMaxFrameRetries         3   ///< Number of frame retries before we fail the transmission
#define aMaxCsmaBackoffs            5   ///< Max number of CSMA backoffs before we fail the transmission
#define aMinBE                      3   ///< Minimum backoff exponent for calculating CSMA backoff time

// 802.15.4 PHY Defined constants
#define aMaxPHYPacketSize           127 ///< Actual size of 802.15.4 frame
#define aPhyChannelPage             0   ///< Unused in FreakZ
//#define aPhyChannelsSupported       0x07FFF800  // channels supported by 802.15.4
#define aPhyChannelsSupported       0x00000800  ///< Channels supported by 802.15.4
#define aPhySymbolsPerOctet         2   ///< Takes 2 symbols to produce a byte @ 2.4 GHz OQPSK
#define aPhySHRDuration             10  ///< Each SHR is 5 bytes or 10 symbols (4 bytes Preamble + 1 byte SFD) or 1 tick. Use the value of 2 for safety.
/// Max frame duration for an 802.15.4 frame
#define aPhyMaxFrameDuration        (aPhySHRDuration + ((aMaxPHYPacketSize + 1) * aPhySymbolsPerOctet))

// max data payload sizes
#define MAX_MAC_PAYLOAD             118     ///< Max MAC payload size 127 - 9
#define MAX_NWK_PAYLOAD             110     ///< Max NWK payload size 127 - 9 - 8
#define MAX_APS_PAYLOAD             99      ///< Max Application payload size 127 - 9 - 8 - 10 (currently 1 less...need to find out why)
#define DEFAULT_SCAN_DURATION       10      ///< Default time to wait before timing out a network or energy scan

// Stack constants - These parameters are defined in the Zigbee Profile Spec
// TODO: Move these to a separate file
#define ZIGBEE_PROTOCOL_ID          0           ///< Protocol ID for this stack
#define ZIGBEE_STACK_PROFILE        1       ///< Stack profile
#define ZIGBEE_PROF_ID              1       ///< Profile ID
#define ZIGBEE_PROTOCOL_VERSION     2       ///< Zigbee protocol version
#define ZIGBEE_NWK_SECURITY_LEVEL   5       ///< Network security level
#define ZIGBEE_MAX_DEPTH            3       ///< Max network depth
#define ZIGBEE_MAX_CHILDREN         4       ///< Max child devices
#define ZIGBEE_MAX_ROUTERS          4       ///< Max router devices
#define ZIGBEE_MAX_NWK_BRC_RECORDS  10      ///< Max number of broadcast records
#define ZIGBEE_PASSIVE_ACK_TIMEOUT  CLOCK_SECOND ///< Passive ack timeout ~ 1 sec
#define ZIGBEE_DEVICE_TYPE          NWK_ROUTER  ///< Default device type
#define ZIGBEE_BRC_EXPIRY           3           ///< Time in seconds before you expire a broadcast entry
#define ZIGBEE_DEFAULT_RADIUS       6       ///< Radius value for frames
#define ZIGBEE_POLL_INTERVAL        8       ///< Polling interval
#define ZIGBEE_CONFIRM_INTERVAL     5       ///< Confirmation timeout
#define ZIGBEE_INVALID_HANDLE       0       ///< Invalid handle value
#define ZIGBEE_INVALID_EP           241     ///< Invalid endpoint value

// descriptors
#define ZIGBEE_PRIM_TRUST           0           ///< Primary trust center
#define ZIGBEE_BACKUP_TRUST         0       ///< Backup trust center
#define ZIGBEE_PRIM_BINDING         0       ///< Primary binding cache
#define ZIGBEE_BACKUP_BINDING       0       ///< Backup binding cache
#define ZIGBEE_PRIM_DISCOVERY       0       ///< Primary discovery cache
#define ZIGBEE_BACKUP_DISCOVERY     0       ///< Backup discovery cache
#define ZIGBEE_NWK_MANAGER          0       ///< Network manager
#define ZIGBEE_MAX_USER_DESC_MSG    20      ///< Max string size for user descriptor

// Capability Info
#define ZIGBEE_MAINS_POWERED        1                    ///< Mains powered - must be 1 if its a router.
#define ZIGBEE_RX_ON_WHEN_IDLE      ZIGBEE_MAINS_POWERED    ///< Rx on when idle - rx will be on when idle if its mains powered. otherwise not.
#define ZIGBEE_HIGH_SECURITY        0                    ///< High security is currently not supported

// Node descriptor
#define ZIGBEE_COMPLEX_DESC_AVAIL   0           ///< Complex descriptor present
#define ZIGBEE_USER_DESC_AVAIL      0       ///< User descriptor present
#define ZIGBEE_MANUF_CODE           0       ///< Manufacturer code
#define ZIGBEE_MAX_BUFSIZE          80      ///< Maximum buffer size
#define ZIGBEE_SERVER_MASK          0       ///< Server mask
#define ZIGBEE_MAX_IN_XFER          MAX_APS_PAYLOAD ///< Node descr - max size of in apdu payload
#define ZIGBEE_MAX_OUT_XFER         MAX_APS_PAYLOAD ///< Node descr - max size of out apdu payload
#define ZIGBEE_DESC_CAPABILITY      0           ///< Capabilities

#define ZIGBEE_MAX_ENDPOINTS        10          ///< Max endpoints for this device
#define ZIGBEE_MIN_BUFS_NEEDED      1           ///< Minimum number of frame buffers required

/* app layer rx defines */
/* Mark the end of a cluster */
#define END_MARKER_CLUST    0xFFFC

/* Invalid network address value */
#define INVALID_NWK_ADDR    0xFFFF
/* Invalid extended address value */
#define INVALID_EXT_ADDR    0xFFFFFFFFFFFFFFFFULL


#define     ZCL_STACK_PROF_HA                       0x0104               //profileId of Home Automation
#define     ZCL_STACK_PROF_LL                       0xC05E               //profileId of Light Link

#define     NEWNODE_NOTIFY                          0x01
#define     REPORT_ATTR_NOTIFY                      0x02
#define     NODELEFT_NOTIFY                         0x03
#define     IAS_NOTIFICATION_NOTIFY                 0x04
#define     IAS_ZONE_REQUEST_NOTIFY                 0x05 
#define     DOOR_LOCK_RESPONSE_NOTIFY               0x06
#define     MS_ACCELERATION_MSG_NOTIFY              0x07
#define     ID_QUERY_RESPONSE_NOTIFY                0x08
#define     ZLL_COMMISSIONING_RESPONSE_NOTIFY       0x09
#define     GRP_RESPONSE_NOTIFY                     0x0A
#define     UPDATE_NETWORK_ID_NOTIFY                0x0B
#define     SCENES_RESPONSE_NOTIFY                  0x0C
#define     POLL_CONTROL_NOTIFY                     0x0D
#define     ON_OFF_RESPONSE_NOTIFY                  0x0E
#define     INTERVIEW_DEVICE_FAILED                 0x0F
#define     EMBER_ZIGBEE_NETWORK_PANSCAN_NOYIFY     0x10
#define     ZIGBEE_COMMAND_PROCESS_NOTIFY           0xFF
#define     POWER_CONFIGURATION_NOTIFY              0x11
#define     TEMPERATURE_MEASUREMENT_NOTIFY          0x12
#define     HUMIDITY_MEASUREMENT_NOTIFY             0x13
#define     ZCL_LEVEL_NOTIFY                        0x14
#define     ZCL_ON_OFF_NOTIFY                       0x15

#define     MAX_DOOR_LOCK_OPERATION_EVENT           32
#define     MAX_DEVICE_ID_LENGTH                    5
#define     MAX_TMP_LENGTH                          32
#define     MAX_IN_CLUSTER_LIST                     10
#define     MAX_OUT_CLUSTER_LIST                    10
#define     MAX_ZIGBEE_CHANNEL                      32


#define     MAX_VIEW_GROUP_NAME                     64
#define     MAX_ATTRIBUTE_STRING_DATA               64
#define     MAX_GROUP_LIST_MEMBERSHIP               32
#define     MAX_DEVID                               10
#define     MAX_CLUSTER                             10
#define     MAX_clusterList                     25
#define     MAX_NOTIFY_MESSAGE                      1024
#define     MAX_NOTIFY                              100
#define     MAX_REGISTER_ARR_DATE                   8
#define     MAX_CMD_REGISTER_LIST                   256
#define     MAX_BYTE_EUI_ADDR                       8
#define     MAX_MANUFACTURER_NAME                   32
#define     MAX_MODEL_IDENTIFIER                    32
#define     MAX_ALARM_PATTERN                       5
#define     MAX_PIN_CODE_LENGTH                     8
#define     MAX_IN_CLUSTER_LIST                     10
#define     MAX_OUT_CLUSTER_LIST                    10
#define     MAX_ATTRIBUTE_DATA_INFO                 10

// states from add zigbee device
#define ADD_NODE_STATUS_LEARN_READY          0x01
#define ADD_NODE_STATUS_DONE                 0x06
#define ADD_NODE_STATUS_FAILED               0x07
#define ADD_NODE_STATUS_ADD_SECURE_FAILED    0xFD
#define ADD_NODE_STATUS_INTERVIEW_DEV_FAILED 0xFF

enum ZDO_CLUSTER_ENUMS
{
    // zdo_clusters
    NWK_ADDR_REQ_CLUST                  = 0x0000,   ///< Network address request cluster ID
    IEEE_ADDR_REQ_CLUST                 = 0x0001,   ///< IEEE address request cluster ID
    NODE_DESC_REQ_CLUST                 = 0x0002,   ///< Node descriptor request cluster ID
    PWR_DESC_REQ_CLUST                  = 0x0003,   ///< Power descriptor request cluster ID
    SIMPLE_DESC_REQ_CLUST               = 0x0004,   ///< Simple descriptor request cluster ID
    ACTIVE_EP_REQ_CLUST                 = 0x0005,   ///< Active endpoint list request cluster ID
    MATCH_DESC_REQ_CLUST                = 0x0006,   ///< Match descriptor request cluster ID
    DEV_ANNCE_REQ_CLUST                 = 0x0013,   ///< End device announce request cluster ID
    END_DEV_BIND_REQ_CLUST              = 0x0020,   ///< End device vind request clust ID
    BIND_REQ_CLUST                      = 0x0021,   ///< Bind request clust ID
    UNBIND_REQ_CLUST                    = 0x0022,   ///< Unbind request clust ID
    NWK_DISC_REQ_CLUST                  = 0x0030,   ///< Network discovery request cluster ID
    NWK_LQI_REQ_CLUST                   = 0x0031,   ///< Network LQI request cluster ID
    NWK_LEAVE_REQ_CLUST                 = 0x0034,   ///< Network leave request cluster ID
    NWK_PERMIT_JOIN_REQ_CLUST           = 0x0036,   ///< Network permit join request cluster ID
    NWK_UPDATE_REQ_CLUST                = 0x0038,   ///< Network update request cluster ID

    NWK_ADDR_RESP_CLUST                 = 0x8000,   ///< Network address response cluster ID
    IEEE_ADDR_RESP_CLUST                = 0x8001,   ///< IEEE address response cluster ID
    NODE_DESC_RESP_CLUST                = 0x8002,   ///< Node descriptor response cluster ID
    PWR_DESC_RESP_CLUST                 = 0x8003,   ///< Power descriptor response cluster ID
    SIMPLE_DESC_RESP_CLUST              = 0x8004,   ///< Simple descriptor response cluster ID
    ACTIVE_EP_RESP_CLUST                = 0x8005,   ///< Active endpoint list response cluster ID
    MATCH_DESC_RESP_CLUST               = 0x8006,   ///< Match descriptor response cluster ID
    END_DEV_BIND_RESP_CLUST             = 0x8020,   ///< End device bind response clust ID
    BIND_RESP_CLUST                     = 0x8021,   ///< Bind response clust ID
    UNBIND_RESP_CLUST                   = 0x8022,   ///< Unbind response clust ID
    NWK_DISC_RESP_CLUST                 = 0x8030,   ///< Network discovery response cluster ID
    NWK_LQI_RESP_CLUST                  = 0x8031,   ///< Network LQI response cluster ID
    NWK_LEAVE_RESP_CLUST                = 0x8034,   ///< Network Leave response cluster ID
    NWK_UPDATE_RESP_CLUST               = 0x8038,   ///< Network update response cluster ID
    NWK_PERMIT_JOIN_RESP_CLUST          = 0x8036,   ///< Network permit join response cluster ID

    // response sizes
    NWK_ADDR_RESP_BASE_SIZE             = 12,       ///< Network address response base size in bytes
    NODE_DESC_RESP_BASE_SIZE            = 17,       ///< Node descriptor response base size in bytes
    PWR_DESC_RESP_BASE_SIZE             = 6,        ///< Power descriptor response base size in bytes

    ZDO_SINGLE_DEV_RESP                 = 0x0,      ///< Zigbee NWK address request only for target device
    ZDO_EXT_DEV_RESP                    = 0x1       ///< Zigbee NWK address request for target and all children
};

typedef struct
{
    uint16_t clust_id;   ///< Cluster ID
    void (*handler)(uint8_t *data, uint8_t len, uint16_t src_addr, uint8_t srcEndpoint, uint16_t clust);  ///< Cluster handler function pointer
} zdo_handler_t;

typedef struct
{
    uint16_t nwk_addr;       ///< Network address of end device
    uint64_t ext_addr;       ///< Extended address of end device
    uint8_t  capab_info;     ///< Capability info of end device
} dev_annce_req_t;

typedef struct _zdo_req_t
{
    uint8_t seq;             ///< Sequence ID of request (the response will later have same ID)
    uint16_t clust;          ///< Cluster ID of the request
    union               ///< Union of requests for the ZDO
    {
        // nwk_addr_req_t          nwk_addr;       ///< Network address request
        // ieee_addr_req_t         ieee_addr;      ///< IEEE address request
        // node_desc_req_t         node_desc;      ///< Node descriptor request
        // pwr_desc_req_t          pwr_desc;       ///< Power descriptor request
        // active_endpoint_req_t         active_endpoint;      ///< Active endpoint list request
        // simpleDesc_req_t       simpleDesc;    ///< Simple descriptor request
        // match_desc_req_t        match_desc;     ///< Match descriptor request
        dev_annce_req_t         dev_annce;      ///< End device announce request
        // nwk_disc_req_t          nwk_disc;       ///< Network discovery request
        // nwk_lqi_req_t           nwk_lqi;        ///< Network LQI request
        // nwk_leave_req_t         leave;          ///< Network Leave request
        // nwk_permit_join_req_t   permit_join;    ///< Network permit join request
        // nwk_update_req_t        nwk_update;     ///< Network update request
        // bind_req_t              bind;           ///< Bind request
        // unbind_req_t            unbind;         ///< Unbind request
        // end_dev_bind_req_t      ed_bind;        ///< End device bind request
    } type;                                     ///< Type qualifier for the ZDO request
} zdo_req_t;

typedef struct _remoteBindingNode_
{
    uint64_t eui_srcAddr;
    uint8_t  srcEndpoint;
    uint16_t cluster_id;
    uint64_t euiDestAddr;
    uint8_t  destEndpoint;
}remoteBindingNode;

typedef struct _pan_scan_node
{
    uint8_t     channel;
    uint16_t    pid;
    uint64_t    endpointid;
    uint8_t     zigbee_stack;
    uint8_t     is_allow_joining; 
}pan_scan_node;

typedef struct _sourceDesc_
{
    uint16_t    destAddr;
    uint8_t     endpoint;
    uint16_t    profileId;
    uint16_t    clusterId;

} sourceDesc;

typedef struct _readAttrData_ 
{
    sourceDesc devSourceDesc;
    uint16_t attributeID;
    uint8_t type;
    uint32_t dataNumber;
    char     dataString[MAX_ATTRIBUTE_STRING_DATA];
    uint8_t noAttributeData;
    union
    {
        bool        bool_data;
        uint8_t     uint8_t_data;
        uint16_t    uint16_t_data;
        uint32_t    uint32_t_data;
        char        string_data[MAX_ATTRIBUTE_STRING_DATA];
        int16_t     int16_t_data;
    };
    
}readAttrData;

typedef struct _discAttributeInfo_ 
{
    uint16_t attributeID;
    uint8_t attributeDataType;
}discAttributeInfo;

typedef struct _discAttrResData_ 
{
    sourceDesc devSourceDesc;
    uint8_t discoveryComplete;
    uint8_t numAttributeInfo;
    discAttributeInfo discAttrInfo[MAX_ATTRIBUTE_DATA_INFO];
    
}discAttrResData;

typedef struct _iasNotification
{
    sourceDesc devSourceDesc;
    
    char alarm_pattern[MAX_ALARM_PATTERN];
}iasNotification;

typedef struct _zcl_door_lock_get_pin_code_rsp_t
{
  uint16_t  user_id;
  uint8_t   user_status;   // e.g. USER_STATUS_AVAILABLE
  uint8_t   user_type;   // e.g. USER_TYPE_UNRESTRICTED_USER
  uint8_t   pin_length;
  uint8_t   p_pin[MAX_PIN_CODE_LENGTH];   // variable length string
} zcl_door_lock_get_pin_code_rsp_t;

typedef struct _zcl_door_lock_get_user_status_rsp_t
{
  uint16_t user_id;
  uint8_t  user_status;   // e.g. USER_STATUS_AVAILABLE
} zcl_door_lock_get_user_status_rsp_t;

typedef struct _zcl_door_lock_set_pin_code_rsp_t
{
  uint16_t user_id;
  uint8_t  status;   // e.g. USER_STATUS_AVAILABLE
  uint8_t  pin_length;
  uint8_t  p_pin[MAX_PIN_CODE_LENGTH];
} zcl_door_lock_set_pin_code_rsp_t;


typedef struct _zcl_door_lock_clear_pin_code_rsp_t
{
  uint16_t user_id;
  uint8_t  status;   // e.g. USER_STATUS_AVAILABLE
  uint8_t  pin_length;
  uint8_t  p_pin[MAX_PIN_CODE_LENGTH];
} zcl_door_lock_clear_pin_code_rsp_t;

typedef struct _doorLockOperationEv_t
{
  uint8_t OperationEvSource;
  uint8_t OperationEvCode;
  uint16_t user_id;
  uint8_t pin;
  uint32_t zigbee_local_time;
  uint8_t length_data;  
  char data[MAX_DOOR_LOCK_OPERATION_EVENT];
  
} doorLockOperationEv_t;

typedef struct _doorLockReport_t
{
    uint8_t type;
    uint16_t lockStatus;
    uint16_t addr;
  
} doorLockReport_t;

typedef struct _doorLockCmdResponse
{
    sourceDesc devSourceDesc;
    uint8_t cmd;
    union
    {
        uint8_t                             status;
        zcl_door_lock_set_pin_code_rsp_t    set_pin_code_rsp;
        zcl_door_lock_clear_pin_code_rsp_t  clear_pin_code_rsp;
        zcl_door_lock_get_pin_code_rsp_t    get_pin_code_rsp;
        zcl_door_lock_get_user_status_rsp_t get_user_status_rsp;
        doorLockOperationEv_t               doorLockOperationEv;
        doorLockReport_t                    doorLockAttriResponse;
    };
} doorLockCmdResponse;


typedef struct _zcl_scenes_add_scene_rsp_t
{
    uint8_t   status;
    uint16_t  group_id;
    uint8_t   scene_id;
} zcl_scenes_add_scene_rsp_t;

typedef struct _zcl_scenes_view_scene_rsp_t
{
    uint8_t   status;
    uint16_t  group_id;
    uint8_t   scene_id;
    uint16_t  trans_time;                         ///< Transition time for this scene
    char      name[16];       ///< Scene name
    char      ext_field[32];       ///< Scene name

} zcl_scenes_view_scene_rsp_t;


typedef struct _zcl_scenes_remove_scene_rsp_t
{
    uint8_t   status;
    uint16_t  group_id;
    uint8_t   scene_id;
    
} zcl_scenes_remove_scene_rsp_t;

typedef struct _zcl_scenes_remove_all_scene_rsp_t
{
    uint8_t   status;
    uint16_t  group_id;
    
} zcl_scenes_remove_all_scene_rsp_t;

typedef struct _zcl_scenes_store_scene_rsp_t
{
    uint8_t   status;
    uint16_t  group_id;
    uint8_t   scene_id;
    
} zcl_scenes_store_scene_rsp_t;

typedef struct _zcl_scenes_get_scene_membership_rsp_t
{
    uint8_t   status;
    uint8_t   capacity;
    uint16_t  group_id;
    uint8_t   scene_count;
    uint8_t   scene_list[16];
} zcl_scenes_get_scene_membership_rsp_t;


typedef struct _scenes_cmd_response
{
    sourceDesc devSourceDesc;
    uint8_t cmd;
    union
    {
        zcl_scenes_add_scene_rsp_t              add_scene_rsp;
        zcl_scenes_view_scene_rsp_t             view_scene_rsp;
        zcl_scenes_remove_scene_rsp_t           remove_scene_rsp;
        zcl_scenes_remove_all_scene_rsp_t       remove_all_scene_rsp;
        zcl_scenes_store_scene_rsp_t            store_scene_rsp;
        zcl_scenes_get_scene_membership_rsp_t   get_scene_membership_rsp;
    };
} scenes_cmd_response;

typedef struct _zcl_on_off_smartenit_getrelays_rsp_t
{
    uint32_t   relay_pattern;

} zcl_on_off_smartenit_getrelays_rsp_t;

typedef struct _zcl_on_off_smartenit_getmode_rsp_t
{
    uint8_t   relay_mode;
} zcl_on_off_smartenit_getmode_rsp_t;

typedef struct _zcl_on_off_smartenit_get_timer_rsp_t
{
    uint8_t          timer_number;
    uint8_t          timer_cnt;
    uint16_t         timer_list[16];
} zcl_on_off_smartenit_get_timer_rsp_t;

typedef struct _zcl_on_off_smartenit_pump_get_config_rsp_t
{
    uint32_t          pump_config;
} zcl_on_off_smartenit_pump_get_config_rsp_t;

typedef struct _zcl_on_off_smartenit_get_name_rsp_t
{
    uint8_t         relay_number;
    uint8_t         relay_name_len;
    char            relay_name[32];
} zcl_on_off_smartenit_get_name_rsp_t;


typedef struct _on_off_cmd_response
{
    sourceDesc devSourceDesc;
    uint8_t cmd;
    union
    {
        zcl_on_off_smartenit_getrelays_rsp_t        smartenit_getrelays_rsp;
        zcl_on_off_smartenit_getmode_rsp_t          smartenit_getmode_rsp;
        zcl_on_off_smartenit_get_timer_rsp_t        smartenit_get_timer_rsp;
        zcl_on_off_smartenit_pump_get_config_rsp_t  smartenit_pump_get_config_rsp;
        zcl_on_off_smartenit_get_name_rsp_t         smartenit_get_name_rsp;
    };
} on_off_cmd_response;


typedef struct _poll_control_command
{
    sourceDesc devSourceDesc;
    uint8_t cmd;
} poll_control_command;

typedef struct _zcl_grp_add_group_rsp_t
{
  uint8_t   status;
  uint16_t  group_id;   
} zcl_grp_add_group_rsp_t;

typedef struct _zcl_grp_view_group_rsp_t
{
  uint8_t   status;
  uint16_t  group_id;   
  uint8_t   group_name[64];
} zcl_grp_view_group_rsp_t;

typedef struct _zcl_grp_get_group_membership_rsp_t
{
  uint8_t   capacity;
  uint8_t   group_count;   
  uint16_t  group_list[32];
} zcl_grp_get_group_membership_rsp_t;

typedef struct _zcl_grp_remove_group_rsp_t
{
  uint8_t   status;
  uint16_t  group_id;   
} zcl_grp_remove_group_rsp_t;

typedef struct _grp_cmd_response
{
    sourceDesc devSourceDesc;
    uint8_t cmd;
    union
    {
        zcl_grp_add_group_rsp_t             add_group_rsp;
        zcl_grp_view_group_rsp_t            view_group_rsp;
        zcl_grp_get_group_membership_rsp_t  get_group_membership_rsp;
        zcl_grp_remove_group_rsp_t          remove_group_rsp;
    };
} grp_cmd_response;


typedef struct _zcl_zll_commissioning_scan_rsp_t
{
    uint32_t    inter_pan_transaction_id;
    uint8_t     rssi_correction;
    uint8_t     zigbee_information;
    uint8_t     zll_information;
    uint16_t    key_bitmask;
    uint32_t    response_identifier;
    uint64_t    extended_pan_identifier;
    uint8_t     network_update_identifier;
    uint8_t     logical_channel;
    uint16_t    pan_identifier;
    uint16_t    network_address;
    uint8_t     number_of_sub_devices;
    uint8_t     total_group_identifiers;
    uint8_t     endpoint_identifier;
    uint16_t    profile_identifier;
    uint16_t    device_identifier;
    uint8_t     version;
    uint8_t     group_identifier_count;
}zcl_zll_commissioning_scan_rsp_t;

typedef struct _zcl_ms_acceleration_temperature_battery_msg_t
{
    int16_t  temperature;
    uint8_t  battery;   
} zcl_ms_acceleration_temperature_battery_msg_t;

typedef struct _zcl_ms_acceleration_acceleration_msg_t
{
    int16_t  axis_x;
    int16_t  axis_y;
    int16_t  axis_z;   
} zcl_ms_acceleration_acceleration_msg_t;

typedef struct _ms_acceleration_cmd_msg
{
    sourceDesc devSourceDesc;
    uint8_t cmd;
    union
    {
        zcl_ms_acceleration_temperature_battery_msg_t   temperature_battery_msg;
        zcl_ms_acceleration_acceleration_msg_t          acceleration_msg;
    };
} ms_acceleration_cmd_msg;


typedef struct _zll_destAddr
{
    uint16_t profile;
    uint16_t option;
    union
    {
        uint16_t short_addr;
        uint64_t long_addr;
    };
}zll_destAddr;

typedef struct _zll_commissioning_cmd_rsp_t
{
    zll_destAddr zll_sourceDesc;
    uint8_t cmd;
    union
    {
        zcl_zll_commissioning_scan_rsp_t                scan_response;
    };
} zll_commissioning_cmd_rsp_t;


typedef struct _ias_zone_request
{
    sourceDesc devSourceDesc;
    uint16_t zone_type;
    uint16_t manufacturer_code;
}ias_zone_request;

typedef struct _id_query_response
{
    sourceDesc devSourceDesc;
    uint16_t timeout;
}id_query_response;


/**
 * @brief A list of clusters received during a service discovery attempt.
 * This will be returned for a simple descriptor request.
 */
#define     MAX_IN_CLUSTER_LIST                     10
#define     MAX_OUT_CLUSTER_LIST                    10

typedef struct {
  uint8_t inClusterCount;
  uint16_t inClusterList[MAX_IN_CLUSTER_LIST];
  uint8_t outClusterCount;
  uint16_t outClusterList[MAX_OUT_CLUSTER_LIST];
  uint16_t profileId;
  uint16_t deviceId;
  uint8_t endpoint;
} simpleDesc;

typedef struct _nodeDescriptor_
{
    uint8_t     status;
    uint64_t    euiAddr;
    uint16_t    destAddr;
    uint8_t     numEndpoint;
    uint8_t     capability;
    char     manufacturerName[MAX_MANUFACTURER_NAME];
    char     modelIdentifier[MAX_MODEL_IDENTIFIER];    
    simpleDesc clusterList[MAX_clusterList];
}nodeDescriptor;

typedef struct _nodeLeft_
{
    uint64_t    euiAddr;
    uint16_t    destAddr;
}nodeLeft;

typedef struct _deviceAnnounce_
{
    uint16_t    destAddr;
    uint64_t    euiAddr;
    uint8_t     capability;
}deviceAnnounce_t;

typedef struct _emberZigbeeNetworkScan 
{
    uint8_t channelId;
    uint16_t panId;
    uint64_t    euiAddr;
}EmberZigbeeNetworkScan;

typedef struct _emberZigbeeNetworkPanScan 
{
    uint8_t numberPanId;
    uint8_t status;
    EmberZigbeeNetworkScan  networkScanHandler[MAX_ZIGBEE_CHANNEL];
}EmberZigbeeNetworkPanScan;

typedef struct _powerConfiguration
{
    uint8_t type;
    uint8_t battery_level;
    uint16_t addr;
}powerConfiguration_t;

typedef struct _temperatureMeasurement
{
    uint8_t type;
    uint16_t temperatureLevel;
    uint16_t addr;
}temperatureMeasurement_t;

typedef struct _humidityMeasurement
{
    uint8_t type;
    uint16_t humidityLevel;
    uint16_t addr;
}humidityMeasurement_t;

typedef struct _zclLevel
{
    uint8_t type;
    uint8_t data;
    uint16_t addr;
}zclLevel_t;

typedef struct _zclOnOff
{
    uint8_t type;
    uint8_t data;
    uint16_t addr;
}zclOnOff_t;

typedef union _notify_tx_buffer_
{
    nodeDescriptor              newNodeNotify;
    readAttrData                readAttributeNotify;
    readAttrData                reportAttriteNotify;            
    nodeLeft                    nodeLeftNotify;
    nodeLeft                    node_update_id_notify;     
    iasNotification             iasNotificationNotify;
    ias_zone_request            ias_zone_request_notify;
    doorLockCmdResponse         doorLockNotify;
    ms_acceleration_cmd_msg     ms_acceleration_cmd_msg_notify;
    id_query_response           id_query_response_notify;
    zll_commissioning_cmd_rsp_t zll_commissioning_response_notify;
    grp_cmd_response            grp_cmd_response_notify;
    poll_control_command        poll_control_cmd_notify;
    scenes_cmd_response         scenes_cmd_response_notify;
    EmberZigbeeNetworkPanScan   emberZigbeeNetworkPanScanNotify;
    powerConfiguration_t        powerConfigurationReport;
    temperatureMeasurement_t    temperatureMeasurementNotify;
    humidityMeasurement_t       humidityMeasurementNotify;
    zclLevel_t                  zclLevelNotify;
    zclOnOff_t                  zclOnOffNotify;
} NOTIFY_ZIGBEE_TX_BUFFER_T;


typedef struct notify
{
    uint8_t notify_status;
    uint8_t notify_message[MAX_NOTIFY_MESSAGE];
}notify_t;

typedef struct notify_queue
{
    notify_t notify[MAX_NOTIFY];
    uint8_t  notify_index;
}zigbee_notify_queue_t; 

typedef struct _cmdRegister_
{
    uint16_t nodeId;
    uint8_t cmdClass;
    uint8_t cmd;
    int data0;
    int data1;
    int data2;
    uint8_t  arrayLength;
    uint8_t  arrayData[MAX_REGISTER_ARR_DATE]; 
}cmdRegister;

extern cmdRegister cmdRegisterList[MAX_CMD_REGISTER_LIST];



#endif // CONSTANTS_H
