#ifndef ZCL_POWER_CONFIGURATION_H
#define ZCL_POWER_CONFIGURATION_H
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"

#define ZCL_POWER_CONFIGURATION_ATTRIB_LIST_SZ    16              ///< Power Configuration attribute list size


#define BATTERY_SIZE_NO_BATTERY                                     0x00
#define BATTERY_SIZE_BUILT_IN                                       0x01
#define BATTERY_SIZE_OTHER                                          0x02
#define BATTERY_SIZE_AA                                             0x03
#define BATTERY_SIZE_AAA                                            0x04
#define BATTERY_SIZE_C                                              0x05
#define BATTERY_SIZE_D                                              0x06
#define BATTERY_SIZE_UNKNOWN                                        0xFF


typedef enum _zcl_power_configuration_enum_t
{
    // attrib id
    ZCL_POWER_CONFIGURATION_MAINS_VOLTAGE                       = 0x0000,       ///< MainsVoltage
    ZCL_POWER_CONFIGURATION_MAINS_FREQUENCY                     = 0x0001,       ///< MainsFrequency

    ZCL_POWER_CONFIGURATION_MAINS_ALARM_MASK                    = 0x0010,       ///< MainsAlarmMask
    ZCL_POWER_CONFIGURATION_MAINS_VOLTAGE_MIN_THRESHOLD         = 0x0011,       ///< MainsVoltageMinThreshold
    ZCL_POWER_CONFIGURATION_MAINS_VOLTAGE_MAX_THRESHOLD         = 0x0012,       ///< MainsVoltageMaxThreshold
    ZCL_POWER_CONFIGURATION_MAINS_VOLTAGE_DWELL_TRIP_POINT      = 0x0013,       ///< MainsVoltageDwellTripPoint

    ZCL_POWER_CONFIGURATION_BATTERY_VOLTAGE                     = 0x0020,       ///<BatteryVoltage
    ZCL_POWER_CONFIGURATION_BATTERY_PERCENTAGE_REMAINING        = 0x0021,       ///<BatteryPercentageRemaining

    ZCL_POWER_CONFIGURATION_BATTERY_MANUFACTURER                = 0x0030,       ///<BatteryManufacturer
    ZCL_POWER_CONFIGURATION_BATTERY_SIZE                        = 0x0031,       ///<BatterySize
    ZCL_POWER_CONFIGURATION_BATTERY_AHR_RATING                  = 0x0032,       ///<BatteryAHrRating
    ZCL_POWER_CONFIGURATION_BATTERY_QUANTITY                    = 0x0033,       ///<BatteryQuantity
    ZCL_POWER_CONFIGURATION_BATTERY_RATED_VOLTAGE               = 0x0034,       ///<BatteryRatedVoltage
    ZCL_POWER_CONFIGURATION_BATTERY_ALARM_MASK                  = 0x0035,       ///<BatteryAlarmMask
    ZCL_POWER_CONFIGURATION_BATTERY_VOLTAGE_MIN_THRESHOLD       = 0x0036        ///<BatteryVoltageMinThreshold

   
} zcl_power_configuration_enum_t;

typedef struct _zcl_power_configuration_data_t
{
    uint16_t 	mains_voltage;                                                 
    uint8_t 	mains_frequency;                         
    uint8_t 	mains_alarm_mask;                       
    uint16_t 	mains_voltage_min_threshold;
    uint16_t    mains_voltage_max_threshold;                         
    uint16_t    mains_voltage_dwell_trip_point;
    uint8_t     battery_voltage;
    uint8_t     battery_percentage_remaining;
    char        battery_manufacture[ZCL_MAX_STR_SZ];
    uint8_t     battery_size;
    uint16_t    battery_ahr_rating;
    uint8_t     battery_quantity;
    uint8_t     battery_rated_voltage;
    uint8_t     battery_alarm_mask;
    uint8_t     battery_voltage_min_threshold;                          
} zcl_power_configuration_data_t;

typedef struct _zcl_power_configuration_attrib_list_t
{
    zclAttribute list[ZCL_POWER_CONFIGURATION_ATTRIB_LIST_SZ];
    zcl_power_configuration_data_t data;
} zcl_power_configuration_attrib_list_t;

void zcl_power_configuration_init(zcl_power_configuration_attrib_list_t *attrib_list);
void zcl_power_configuration_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);
#endif