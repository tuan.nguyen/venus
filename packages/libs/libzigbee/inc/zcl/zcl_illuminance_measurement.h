
#ifndef ZCL_ILLUMINANCE_MEASUREMENT_H
#define ZCL_ILLUMINANCE_MEASUREMENT_H
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"
#include "zcl_gen.h"

#define ZCL_ILLUMINANCE_MEASUREMENT_ATTRIB_LIST_SZ    6              ///<  attribute list size




typedef enum _zcl_illuminance_measurement_enum_t
{
    // illuminance measurement information
    ZCL_ILLUMINANCE_MEASUREMENT_ATTRIB_MEASURED_VALUE               = 0x0000,        
    ZCL_ILLUMINANCE_MEASUREMENT_ATTRIB_MIN_MEASURED_VALUE           = 0x0001,       
    ZCL_ILLUMINANCE_MEASUREMENT_ATTRIB_MAX_MEASURED_VALUE           = 0x0002, 
    ZCL_ILLUMINANCE_MEASUREMENT_ATTRIB_TOLERANCE                    = 0x0003, 
    ZCL_ILLUMINANCE_MEASUREMENT_ATTRIB_LIGHT_SENSOR_TYPE            = 0x0004, 
    
 }zcl_illuminance_measurement_enum_t;

typedef struct _zcl_illuminance_measurement_data_t
{
    uint16_t        measured_value;
    uint16_t        min_measured_value;
    uint16_t        max_measured_value;
    uint16_t        tolerance;
    uint8_t         light_sensor_type;

} zcl_illuminance_measurement_data_t;




/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_illuminance_measurement_attrib_list_t
{
    zclAttribute                      list[ZCL_ILLUMINANCE_MEASUREMENT_ATTRIB_LIST_SZ];
    zcl_illuminance_measurement_data_t    data;
} zcl_illuminance_measurement_attrib_list_t;




void    zcl_illuminance_measurement_init(zcl_illuminance_measurement_attrib_list_t *attrib_list);
void    zcl_illuminance_measurement_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);

#endif