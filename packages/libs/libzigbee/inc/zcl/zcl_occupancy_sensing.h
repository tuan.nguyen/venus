
#ifndef ZCL_OCCUPANCY_SENSING_H
#define ZCL_OCCUPANCY_SENSING_H
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"

#define ZCL_OCCUPANCY_SENSING_ATTRIB_LIST_SZ                  9///< Basic attribute list size

#define OCCUPANCY_SENSOR_TYPE_PIR                             0x00
#define OCCUPANCY_SENSOR_TYPE_ULTRASONIC                      0x01
#define OCCUPANCY_SENSOR_TYPE_PIR_AND_ULTRASONIC              0x02

typedef enum _zcl_occupancy_enum_t
{
    // attrib Occupancy sensor information
    ZCL_OCCUPANCY_SENSING_ATTRIB_OCCUPANCY                                    = 0x0000,///< Occupancy
    ZCL_OCCUPANCY_SENSING_ATTRIB_OCCUPANCY_SENSOR_TYPE                        = 0x0001,///< OccupancySensorType

    // attrib PIR Configuration Set
    ZCL_OCCUPANCY_SENSING_ATTRIB_PIR_OCCUPIED_TO_UNOCCUPIED_DELAY             = 0x0010,//PIROccupiedToUnoccupiedDelay
    ZCL_OCCUPANCY_SENSING_ATTRIB_PIR_UNOCCUPIED_TO_OCCUPIED_DELAY             = 0x0011,//PIRUnoccupiedToOccupiedDelay
    ZCL_OCCUPANCY_SENSING_ATTRIB_PIR_UNOCCUPIED_TO_OCCUPIED_THRESHOLD         = 0x0012,//PIRUnoccupiedToOccupiedDelay

    // attrib Ultrasonic Configuration Set
    ZCL_OCCUPANCY_SENSING_ATTRIB_ULTRASONIC_OCCUPIED_TO_UNOCCUPIED_DELAY      = 0x0020,//UltrasonicOccupiedToUnoccupiedDelay
    ZCL_OCCUPANCY_SENSING_ATTRIB_ULTRASONIC_UNOCCUPIED_TO_OCCUPIED_DELAY      = 0x0021,//UltrasonicUnoccupiedToOccupiedDelay
    ZCL_OCCUPANCY_SENSING_ATTRIB_ULTRASONIC_UNOCCUPIED_TO_OCCUPIED_THRESHOLD  = 0x0022//UltrasonicUnoccupiedToOccupiedDelay

} zcl_occupancy_enum_t;

typedef struct _zcl_occupancy_sensing_data_t
{
    uint8_t 	occupancy;                                                 
    uint8_t 	occupancy_sensor_type;
    uint16_t    pir_occupied_to_unoccupied_delay;
    uint16_t    pir_unoccupied_to_occupied_delay;
    uint16_t    pir_unoccupied_to_occupied_threshold;
    uint16_t    ultrasonic_occupied_to_unoccupied_delay;
    uint16_t    ultrasonic_unoccupied_to_occupied_delay;
    uint16_t    ultrasonic_unoccupied_to_occupied_threshold;
} zcl_occupancy_sensing_data_t;

typedef struct _zcl_occupancy_sensing_attrib_list_t
{
    zclAttribute list[ZCL_OCCUPANCY_SENSING_ATTRIB_LIST_SZ];
    zcl_occupancy_sensing_data_t data;
} zcl_occupancy_sensing_attrib_list_t;


void zcl_occupancy_sensing_init(zcl_occupancy_sensing_attrib_list_t *attrib_list);
void zcl_occupancy_sensing_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);
#endif