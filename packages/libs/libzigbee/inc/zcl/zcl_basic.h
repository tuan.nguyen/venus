#ifndef ZCL_BASIC_H
#define ZCL_BASIC_H

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"

// test data ... change this later
#define ZCL_BASIC_ATTRIB_LIST_SZ    13              ///< Basic attribute list size
#define ZCL_VER_NUM                 0xA3            ///< Version number
#define ZCL_APP_VER_NUM             0x55            ///< Application version number
#define ZCL_STACK_VER_NUM           0x12            ///< Stack version number
#define ZCL_HW_VER_NUM              0x34            ///< Hardware version number
#define ZCL_MANUF_ID                "VEriK"         ///< Manufacturer Name
#define ZCL_MODEL_ID                "Venus"         ///< Model Name
#define ZCL_DATE_CODE               "2015-12-10"    ///< Date code
#define ZCL_LOC_DESC                "Company"       ///< Location description...wtf is this used for?

/**************************************************************************/
/*!
    These are the enumerated values specific to the ZCL Basic cluster.
*/
/**************************************************************************/
typedef enum _zcl_basic_enum_t
{
    // attrib id
    ZCL_BASIC_VER           = 0x0000,       ///< Version number attrib ID
    ZCL_BASIC_APP_VER       = 0x0001,       ///< App version attrib ID
    ZCL_BASIC_STACK_VER     = 0x0002,       ///< Stack version attrib ID
    ZCL_BASIC_HW_VER        = 0x0003,       ///< Hardware version attrib ID
    ZCL_BASIC_MANUF_NAME    = 0x0004,       ///< Manufacturer name attrib ID
    ZCL_BASIC_MODEL_ID      = 0x0005,       ///< Model name attrib ID
    ZCL_BASIC_DATE_CODE     = 0x0006,       ///< Date code attrib ID
    ZCL_BASIC_PWR_SRC       = 0x0007,       ///< Power source attrib ID

    ZCL_BASIC_LOC_DESC      = 0x0010,       ///< Location description attrib ID
    ZCL_BASIC_PHYS_ENV      = 0x0011,       ///< Physical environment attrib ID
    ZCL_BASIC_DEV_ENB       = 0x0012,       ///< Device enable attrib ID
    ZCL_BASIC_ALARM_MSK     = 0x0013,       ///< Alarm mask attrib ID

    // cmd id
    ZCL_BASIC_CMD_RESET     = 0x00,         ///< Command reset command ID

    // pwr src enum
    ZCL_BASIC_PWR_ENUM_UNKNOWN                      = 0x00,     ///< Unknown power source
    ZCL_BASIC_PWR_ENUM_MAINS_SINGLE                 = 0x01,     ///< Mains powered single phase
    ZCL_BASIC_PWR_ENUM_MAINS_TRIPLE                 = 0x02,     ///< Mains powered three phase
    ZCL_BASIC_PWR_ENUM_BATTERY                      = 0x03,     ///< Battery powered
    ZCL_BASIC_PWR_ENUM_DC_SRC                       = 0x04,     ///< DC powered
    ZCL_BASIC_PWR_ENUM_EMERGENCY_MAINS_CONST_PWR    = 0x05,     ///< Emergency mains constant powered
    ZCL_BASIC_PWR_ENUM_EMERGENCY_MAINS_TRANS_SW     = 0x06,     ///< Emergency mains and transfer switch

    // phys env
    ZCL_BASIC_PHYS_ENV_UNSPECD  = 0x00,         ///< Physical environment unspecified
    ZCL_BASIC_PHYS_ENV_UNKNOWN  = 0xff          ///< Physical environment unknown
} zcl_basic_enum_t;

/**************************************************************************/
/*!
    This is the structure that holds all a particular instance of the ZCL basic
    cluster.
*/
/**************************************************************************/
typedef struct _zcl_basic_data_t
{
    uint8_t zcl_ver;                                                 ///< ZCL version attribute
    uint8_t app_ver;                         ///< App version attribute
    uint8_t stack_ver;                       ///< Stack version attribute
    uint8_t hw_ver;                          ///< Hardware version attribute
    uint8_t manuf_id[ZCL_MAX_STR_SZ];        ///< Manufacturer name attribute
    uint8_t model_id[ZCL_MAX_STR_SZ];        ///< Model name attribute
    uint8_t date_code[ZCL_MAX_STR_SZ/2];     ///< Date code attribute - defined to be a max of 16 chars
    uint8_t pwr_src;                         ///< power source attribute

    uint8_t loc_desc[ZCL_MAX_STR_SZ/2];      ///< Location description attribute
    uint8_t phys_env;                        ///< Physical environment attribute
    bool dev_enb;                       ///< Device enabled attribute
    uint8_t alarm_msk;                       ///< Alarm mask attribute
} zcl_basic_data_t;

/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_basic_attrib_list_t
{
    zclAttribute list[ZCL_BASIC_ATTRIB_LIST_SZ];
    zcl_basic_data_t data;
} zcl_basic_attrib_list_t;

void zcl_basic_init(zcl_basic_attrib_list_t *attrib_list);
void zcl_basic_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);

#endif