#ifndef _ZCL_H_
#define _ZCL_H_
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "zcl_constants.h"
#include "../memory/mem_heap.h"

#define ZCL_END_MARKER          0xFFFF          ///< Used to define the end of a list or table.
#define ZCL_MAX_DISC_ATTRIBS    20              ///< Maximum discovered attributes.
#define ZCL_MAX_STR_SZ          32              ///< Maximum string size.
#define ZCL_MAX_PAYLOAD_SIZE    80              ///< Maximum payload size.
#define ZCL_MAX_RPT_SZ          20              ///< Maximum report size.
#define ZCL_HDR_SZ              3               ///< ZCL Header is 3 bytes

// mem pointer macros
#define ZCL_RPT(m)        ((zcl_rpt_entry_t *)MMEM_PTR(&m->mmem_ptr))   ///< De-reference the mem ptr and cast it as a zcl report entry



#define  ZCL_SMARTENIT_MFG_CODE                             0x1075



/**************************************************************************/
/*!
        Enumeration of the attribute access values.
*/
/**************************************************************************/
typedef enum _zcl_attrib_access_enum_t
{
    ZCL_ACCESS_READ_ONLY    = 0x00,     ///< Attribute is read only
    ZCL_ACCESS_READ_WRITE   = 0x01      ///< Attribute is read/write
} zcl_attrib_access_enum_t;

// Enumeration of the ZCL cluster IDs
typedef enum _zcl_clust_id_t
{
    ZCL_BASIC_CLUST_ID                                      = 0x0000,   ///< Basic cluster ID
    ZCL_PWR_CFG_CLUST_ID                                    = 0x0001,   ///< Power configuration cluster ID
    ZCL_DEV_TMP_CFG_CLUST_ID                                = 0x0002,   ///< Device temperature cluster ID
    ZCL_IDENTIFY_CLUST_ID                                   = 0x0003,   ///< Identify cluster ID
    ZCL_GRP_CLUST_ID                                        = 0x0004,   ///< Group cluster ID
    ZCL_SCENES_CLUST_ID                                     = 0x0005,   ///< Scenes cluster ID
    ZCL_ON_OFF_CLUST_ID                                     = 0x0006,   ///< On/Off cluster ID
    ZCL_LEVEL_CLUST_ID                                      = 0x0008,   ///< Level Control cluster ID
    ZCL_TEMPERATURE_MEASUREMENT_CLUST_ID                    = 0x0402,   ///< Temperature Measurement cluster ID
    ZCL_HUMIDITY_MEASUREMENT_CLUST_ID                       = 0x0405,   ///< Humidity Measurement cluster ID
    ZCL_MS_HUMIDITY_MEASUREMENT_CLUST_ID                    = 0xFC45,   ///< Manufacturer Specific Humidity Measurement
    ZCL_OCCUPANCY_SENSING_CLUST_ID                          = 0x0406,   ///< Occupancy sensing cluster ID
    ZCL_MS_OCCUPANCY_SENSING_CLUST_ID                       = 0xFC46,   ///< Manufacturer Specific Occupancy sensing cluster ID
    ZCL_IAS_ZONE_CLUST_ID                                   = 0x0500,   ///< IAS Zone cluster ID
    ZCL_SHADE_CONFIGURATION_CLUST_ID                        = 0x0100,   ///< Shade Configuration cluster ID
    ZCL_DOOR_LOCK_CLUST_ID                                  = 0x0101,   ///< Door Lock cluster ID
    ZCL_COLOR_CONTROL_CLUST_ID                              = 0x0300,   ///< Color Control cluster ID
    ZCL_MS_ACCELERATION_CLUST_ID                            = 0xFC03,   ///< Manufacturer Specific Acceleration cluster ID
    ZCL_ILLUMINANCE_MEASUREMENT_CLUST_ID                    = 0x0400,   ///< Illuminance Measurement cluster ID
    ZCL_ILLUMINANCE_LEVEL_SENSING_CLUST_ID                  = 0x0401,   ///< Illuminance Level Sensing cluster ID
    ZCL_THERMOSTAT_CLUST_ID                                 = 0x0201,   ///< Thermostat cluster ID
    ZCL_PUMP_CONFIGURATION_AND_CONTROL_CLUST_ID             = 0x0200,   ///< Pump Configuraton and Control cluster ID
    ZCL_THERMOSTAT_USER_INTERFACE_CONFIGURATION_CLUST_ID    = 0x0204,   ///< Thermostat User Interface Configuration cluster ID
    ZCL_WINDOW_COVERING_CLUST_ID                            = 0x0102,   ///< Window Covering cluster ID
    ZCL_TIME_CLUST_ID                                       = 0x000A,   ///< Time cluster ID
    ZCL_POLL_CONTROL_CLUST_ID                               = 0x0020,   ///< Poll Control cluster ID

} zcl_clust_id_t;

typedef enum _zcl_gen_cmd_t
{
    ZCL_CMD_READ_ATTRIB             = 0x00,      ///< Read attributes foundation command ID
    ZCL_CMD_READ_ATTRIB_RESP        = 0x01,      ///< Read attributes response foundation command ID
    ZCL_CMD_WRITE_ATTRIB            = 0x02,      ///< Write attributes foundation command ID
    ZCL_CMD_WRITE_ATTRIB_RESP       = 0x03,      ///< Write attributes response foundation command ID
    ZCL_CMD_WRITE_ATTRIB_UNDIV      = 0x04,      ///< Write attributes undivided foundation command ID
    ZCL_CMD_WRITE_ATTRIB_NO_RESP    = 0x05,      ///< Write attributes no response foundation command ID
    ZCL_CMD_CONFIG_REPORT           = 0x06,      ///< Configure reporting foundation command ID
    ZCL_CMD_CONFIG_REPORT_RESP      = 0x07,      ///< Configure reporting response foundation command ID
    ZCL_CMD_READ_REPORT_CFG         = 0x08,      ///< Read reporting config foundation command ID
    ZCL_CMD_READ_REPORT_CFG_RESP    = 0x09,      ///< Read reporting config response foundation command ID
    ZCL_CMD_REPORT_ATTRIB           = 0x0a,      ///< Report attribute foundation command ID
    ZCL_CMD_DEFAULT_RESP            = 0x0b,      ///< Default response foundation command ID
    ZCL_CMD_DISC_ATTRIB             = 0x0c,      ///< Discover attributes foundation command ID
    ZCL_CMD_DISC_ATTRIB_RESP        = 0x0d       ///< Discover attributes response foundation command ID
} zcl_gen_cmd_t;

typedef enum _zcl_frm_enums_t
{
    ZCL_FRM_TYPE_OFF    = 0,                        ///< Frame type offset  
    ZCL_MANUF_SPEC_OFF  = 2,                        ///< Manuf specification offset
    ZCL_DIR_OFF         = 3,                        ///< Direction offset
    ZCL_DIS_DEF_OFF     = 4,                        ///< Disable default response offset
                                                   
    ZCL_FRM_TYPE_MASK   = (3<<ZCL_FRM_TYPE_OFF),    ///< Frame type mask
    ZCL_MANUF_SPEC_MASK = (1<<ZCL_MANUF_SPEC_OFF),  ///< Manufacturing spec mask
    ZCL_DIR_MASK        = (1<<ZCL_DIR_OFF),         ///< Direction mask
    ZCL_DIS_DEF_MASK    = (1<<ZCL_DIS_DEF_OFF),     ///< Disable default response mask
                                                   
    ZCL_FRM_TYPE_GENERAL    = 0,                    ///< Frame type - general
    ZCL_FRM_TYPE_CLUST_SPEC = 1,                    ///< Frame type - cluster specific
                                                
    ZCL_FRM_DIR_TO_SRVR     = 0,                    ///< Direction - To server
    ZCL_FRM_DIR_TO_CLI      = 1,                    ///< Direction - To client
                                                   
    ZCL_RPT_DIR_SEND_RPT    = 0                     ///< Report Direction - Send report
} zcl_frm_enums_t;

//Enumeration of the ZCL status values.
typedef enum _zcl_status_enum_t
{
    ZCL_STATUS_SUCCESS                  = 0x00,     ///< ZCL Success
    ZCL_STATUS_FAIL                     = 0x01,     ///< ZCL Fail
    ZCL_STATUS_MALFORMED_CMD            = 0x80,     ///< Malformed command
    ZCL_STATUS_UNSUP_CLUST_CMD          = 0x81,     ///< Unsupported cluster command
    ZCL_STATUS_UNSUP_GEN_CMD            = 0x82,     ///< Unsupported general command
    ZCL_STATUS_UNSUP_MANUF_CLUST_CMD    = 0x83,     ///< Unsupported manuf-specific clust command
    ZCL_STATUS_UNSUP_MANUF_GEN_CMD      = 0x84,     ///< Unsupported manuf-specific general command
    ZCL_STATUS_INVALID_FIELD            = 0x85,     ///< Invalid field
    ZCL_STATUS_UNSUP_ATTRIB             = 0x86,     ///< Unsupported attribute
    ZCL_STATUS_INVALID_VALUE            = 0x87,     ///< Invalid value
    ZCL_STATUS_READ_ONLY                = 0x88,     ///< Read only
    ZCL_STATUS_INSUFF_SPACE             = 0x89,     ///< Insufficient space
    ZCL_STATUS_DUPE_EXISTS              = 0x8a,     ///< Duplicate exists
    ZCL_STATUS_NOT_FOUND                = 0x8b,     ///< Not found
    ZCL_STATUS_UNREPORTABLE_ATTRIB      = 0x8c,     ///< Unreportable attribute
    ZCL_STATUS_INVALID_TYPE             = 0x8d,     ///< Invalid type
    ZCL_STATUS_HW_FAIL                  = 0xc0,     ///< Hardware failure
    ZCL_STATUS_SW_FAIL                  = 0xc1,     ///< Software failure
    ZCL_STATUS_CALIB_ERR                = 0xc2,     ///< Calibration error
    ZCL_STATUS_DISC_COMPLETE            = 0x01,     ///< Discovery complete
    ZCL_STATUS_DISC_INCOMPLETE          = 0x00      ///< Discovery incomplete
} zcl_status_enum_t;

//enumeration of the ZCL attribute data type values
typedef enum _zclAttributeype_enum_t
{
    ZCL_TYPE_NULL           = 0x00,     ///< Null data type
    ZCL_TYPE_8BIT           = 0x08,     ///< 8-bit value data type
    ZCL_TYPE_16BIT          = 0x09,     ///< 16-bit value data type
    ZCL_TYPE_32BIT          = 0x0b,     ///< 32-bit value data type
    ZCL_TYPE_BOOL           = 0x10,     ///< Boolean data type
    ZCL_TYPE_8BITMAP        = 0x18,     ///< 8-bit bitmap data type
    ZCL_TYPE_16BITMAP       = 0x19,     ///< 16-bit bitmap data type
    ZCL_TYPE_32BITMAP       = 0x1b,     ///< 32-bit bitmap data type
    ZCL_TYPE_U8             = 0x20,     ///< Unsigned 8-bit value data type
    ZCL_TYPE_U16            = 0x21,     ///< Unsigned 16-bit value data type
    ZCL_TYPE_U24            = 0x22,     ///< Unsigned 24-bit value data type
    ZCL_TYPE_S8             = 0x28,     ///< Signed 8-bit value data type
    ZCL_TYPE_S16            = 0x29,     ///< Signed 16-bit value data
    ZCL_TYPE_U32            = 0x23,     ///< Unsigned 32-bit value data type
    ZCL_TYPE_8BIT_ENUM      = 0x30,
    ZCL_TYPE_16BIT_ENUM     = 0x31,
    ZCL_TYPE_BYTE_ARRAY     = 0x41,     ///< Byte array data type
    ZCL_TYPE_CHAR_STRING    = 0x42,     ///< Charactery string (array) data type
    ZCL_TYPE_IEEE_ADDR      = 0xf0,     ///< IEEE address (U64) type
    ZCL_TYPE_INVALID        = 0xff      ///< Invalid data type
} zclAttributeype_enum_t;



//ZCL frame control field
typedef struct _zcl_frm_ctrl_t
{
	uint8_t 			frm_type; 				//ZCL frame type
	bool 				manuf_spec;				//Manufacturer specific frame
	bool 				dir;					//Direction
	bool 				dis_def_resp;			//Disable default response
}zcl_frm_ctrl_t;

//ZCL frame header structure
typedef struct _zcl_hdr_t
{
	zcl_frm_ctrl_t 		frm_ctrl; 				//Frame control field structure
	uint8_t 			fcf;					//Frame control field - single byte
	uint16_t 			manuf_code;				//Manufacturer code
	uint8_t				seq_num;				//sequence number
	uint8_t				cmd;					//command id
	uint8_t				*payload;				//payload
	uint8_t				payload_len;			//payload length
}zcl_hdr_t;

//ZCL attribute structure
typedef struct _zclAttribute
{
    uint16_t         id;             ///< Attrib ID
    uint8_t          type;           ///< Attrib data type
    uint8_t          access;         ///< Attrib data access privileges
    void             *data;          ///< Ptr to data
    mem_ptr_t   *rpt;           ///< Placeholder in case this attrib requires a report
} zclAttribute;

//ZCL cluster structure
typedef struct _zcl_clust_t
{
    uint8_t endpoint;                      ///< Endpoint that cluster belongs to
    uint16_t clust_id;               ///< Cluster ID
    zclAttribute *attrib_list;  ///< Cluster attribute list

    /// Cluster rx handler callback. All frames targeting this cluster will go here to get processed
    void (*rx_handler)(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, struct _zcl_clust_t *clust, zcl_hdr_t *hdr);

    /// Cluster action handler. After going to the rx handler, any application specific actions can be defined
    /// in the action handler
    void (*action_handler)(uint8_t action, void *data);
} zcl_clust_t;

void 				zcl_init();
uint8_t 			zcl_get_seq_num();
uint8_t 			zcl_gen_read_attrib(uint8_t *data, zcl_hdr_t *hdr, uint16_t *attrib_list, uint8_t attrib_num);
uint8_t             zcl_gen_write_attrib(uint8_t *data, zcl_hdr_t *hdr, zclAttribute *attrib_list, uint8_t attrib_num);
uint8_t             zcl_gen_config_rpt(uint8_t *data, zcl_hdr_t *hdr, zcl_clust_t *clust, uint16_t attrib_id, uint16_t min_intv, uint16_t max_intv, uint32_t change);
zcl_clust_t         *zcl_find_clust(zcl_clust_t **clust_list, uint16_t clust_id);
void 				zcl_parse_hdr(uint8_t *data, uint8_t len, zcl_hdr_t *hdr);
void                zcl_set_clust(zcl_clust_t *clust, uint8_t endpoint, uint16_t clust_id, zclAttribute *attrib_list,
                                    void (*rx_handler)(uint8_t *, uint8_t * ,uint16_t, uint8_t, uint16_t, struct _zcl_clust_t *, zcl_hdr_t *),
                                    void (*action_handler)(uint8_t, void *));

void                zcl_set_attrib(zclAttribute *attrib, uint16_t id, uint8_t type, uint8_t access, void *data);
zclAttribute        *zcl_find_attrib(zclAttribute *attrib_list, uint16_t attrib_id);
void                zcl_cmd_handler(uint8_t *resp, uint8_t *resp_len, uint16_t src_addr, uint8_t srcEndpoint, uint16_t prof_id, zcl_clust_t *clust, zcl_hdr_t *hdr);
void                zcl_set_string_attrib(uint8_t *attrib_data, uint8_t *val, uint8_t max_sz);



#endif