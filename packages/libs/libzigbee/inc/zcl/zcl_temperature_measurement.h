
#ifndef ZCL_TEMPERATURE_MEASUREMENT_H
#define ZCL_TEMPERATURE_MEASUREMENT_H
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"

#define ZCL_TEMPERATURE_MEASUREMENT_ATTRIB_LIST_SZ    5              ///< Basic attribute list size

typedef enum _zcl_temperature_measurement_enum_t
{
    // attrib id
    ZCL_TEMPERATURE_MEASUREMENT_MEASURED_VALUE          = 0x0000,       ///< MeasuredValue
    ZCL_TEMPERATURE_MEASUREMENT_MIN_MEASURED_VALUE      = 0x0001,       ///< MinMeasuredValue
    ZCL_TEMPERATURE_MEASUREMENT_MAX_MEASURED_VALUE      = 0x0002,       ///< MaxMeasuredValue
    ZCL_TEMPERATURE_MEASUREMENT_TOLERANCE               = 0x0003       ///< Tolerance

   
} zcl_temperature_measurement_enum_t;

typedef struct _zcl_temperature_measurement_data_t
{
    int16_t 	mesured_value;                                                 
    int16_t 	min_measured_value;                         
    int16_t 	max_mesured_value;                       
    uint16_t 	tolerance;                          
} zcl_temperature_measurement_data_t;

typedef struct _zcl_temperature_measurement_attrib_list_t
{
    zclAttribute list[ZCL_TEMPERATURE_MEASUREMENT_ATTRIB_LIST_SZ];
    zcl_temperature_measurement_data_t data;
} zcl_temperature_measurement_attrib_list_t;

void zcl_temperature_measurement_init(zcl_temperature_measurement_attrib_list_t *attrib_list);
void zcl_temperature_measurement_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);
#endif