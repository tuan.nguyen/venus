#ifndef _ZCL_GEN_H_
#define _ZCL_GEN_H_
#include "zcl.h"
uint8_t zcl_gen_hdr(uint8_t *data, zcl_hdr_t *hdr);
uint8_t zcl_gen_def_resp(uint8_t *resp, uint8_t status, zcl_hdr_t *hdr);

#endif