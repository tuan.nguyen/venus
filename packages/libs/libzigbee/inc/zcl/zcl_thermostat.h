
#ifndef ZCL_THERMOSTAT_H
#define ZCL_THERMOSTAT_H
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"
#include "zcl_gen.h"

#define ZCL_THERMOSTAT_ATTRIB_LIST_SZ    24              ///< Thermostat attribute list size




typedef enum _zcl_thermostat_enum_t
{
    // Thermostat Information attrib id
    ZCL_THERMOSTAT_ATTRIB_LOCAL_TEMPERATURE                     = 0x0000,        
    ZCL_THERMOSTAT_ATTRIB_OUTDOOR_TEMPERATURE                   = 0x0001,       
    ZCL_THERMOSTAT_ATTRIB_OCCUPANCY                             = 0x0002, 
    ZCL_THERMOSTAT_ATTRIB_ABS_MIN_HEAT_SETPOINT_LIMIT           = 0x0003,
    ZCL_THERMOSTAT_ATTRIB_ABS_MAX_HEAT_SETPOINT_LIMIT           = 0x0004,
    ZCL_THERMOSTAT_ATTRIB_ABS_MIN_COOL_SETPOINT_LIMIT           = 0x0005,
    ZCL_THERMOSTAT_ATTRIB_ABS_MAX_COOL_SETPOINT_LIMIT           = 0x0006,
    ZCL_THERMOSTAT_ATTRIB_PI_COOLING_DEMAND                     = 0x0007,
    ZCL_THERMOSTAT_ATTRIB_PI_HEATING_DEMAND                     = 0x0008,

    // Thermostat Setting attrib id
    ZCL_THERMOSTAT_ATTRIB_LOCAL_TEMPERATURE_CALIBRATION         = 0x0010,        
    ZCL_THERMOSTAT_ATTRIB_OCCUPIED_COOLING_SETPOINT             = 0x0011,       
    ZCL_THERMOSTAT_ATTRIB_OCCUPIED_HEATING_SETPOINT             = 0x0012, 
    ZCL_THERMOSTAT_ATTRIB_UNOCCUPIED_COOLING_SETPOINT           = 0x0013,
    ZCL_THERMOSTAT_ATTRIB_UNOCCUPIED_HEATING_SETPOINT           = 0x0014,
    ZCL_THERMOSTAT_ATTRIB_MIN_HEAT_SETPOINT_LIMIT               = 0x0015,
    ZCL_THERMOSTAT_ATTRIB_MAX_HEAT_SETPOINT_LIMIT               = 0x0016,
    ZCL_THERMOSTAT_ATTRIB_MIN_COOL_SETPOINT_LIMIT               = 0x0017,
    ZCL_THERMOSTAT_ATTRIB_MAX_COOL_SETPOINT_LIMIT               = 0x0018,
    ZCL_THERMOSTAT_ATTRIB_MIN_SETPOINT_DEAD_BAND                = 0x0019,
    ZCL_THERMOSTAT_ATTRIB_REMOTE_SENSING                        = 0x001A,
    ZCL_THERMOSTAT_ATTRIB_CONTROL_SEQUENCE_OF_OPERATION         = 0x001B,
    ZCL_THERMOSTAT_ATTRIB_SYSTEM_MODE                           = 0x001C,
    ZCL_THERMOSTAT_ATTRIB_ALARM_MASK                            = 0x001D,
   

    // Thermostat cmd received
    ZCL_THERMOSTAT_CMD_SETPOINT_RAISE_LOWER                         = 0x00,
    


 }zcl_thermostat_enum_t;

typedef struct _zcl_thermostat_data_t
{
    int16_t         local_temperature;
    int16_t         outdoor_temperature;
    uint8_t         occupancy;
    int16_t         abs_min_heat_setpoint_limit;
    int16_t         abs_max_heat_setpoint_limit;
    int16_t         abs_min_cool_setpoint_limit;
    int16_t         abs_max_cool_setpoint_limit;
    uint8_t         pi_cooling_demand;
    uint8_t         pi_heating_demand;

    int8_t          local_temperature_calibration;
    int16_t         occupied_cooling_setpoint;
    int16_t         occupied_heating_setpoint;
    int16_t         unoccupied_cooling_setpoint;
    int16_t         unoccupied_heating_setpoint;
    int16_t         min_heat_setpoint_limit;
    int16_t         max_heat_setpoint_limit;
    int16_t         min_cool_setpoint_limit;
    int16_t         max_cool_setpoint_limit;
    int8_t          min_setpoint_dead_band;
    uint8_t         remote_sensing;
    uint8_t         control_sequence_of_operation;
    uint8_t         system_mode;
    uint8_t         alarm_mask;

} zcl_thermostat_data_t;

typedef struct _zcl_thermostat_setpoint_req_t
{
  uint8_t           mode;   
  int8_t            amount;      
} zcl_thermostat_setpoint_req_t;



typedef struct _zcl_thermostat_req_t
{
    zcl_thermostat_setpoint_req_t        setpoint;

} zcl_thermostat_req_t;


/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_thermostat_attrib_list_t
{
    zclAttribute            list[ZCL_THERMOSTAT_ATTRIB_LIST_SZ];
    zcl_thermostat_data_t    data;
} zcl_thermostat_attrib_list_t;




void    zcl_thermostat_init(zcl_thermostat_attrib_list_t *attrib_list);
uint8_t zcl_thermostat_gen_req(uint8_t *data, zcl_hdr_t *hdr, zcl_thermostat_req_t *req);
void    zcl_thermostat_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);

#endif