#ifndef ZCL_ZLL_COMMISSIONING_H
#define ZCL_ZLL_COMMISSIONING_H

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"
#include "zcl_gen.h"
/*! attribute list size - this will be one larger than the actual list size
    to hold the end marker
*/

#define ZCL_ZLL_COMMISSIONING_ATTRIB_LIST_SZ    1
#define OPTION_ADDRESS_64_BIT                   0x02

/**************************************************************************/
/*!
    Constants for this cluster
*/
/**************************************************************************/
typedef enum _zcl_zll_commissioning_enums_t
{
    //Received Touchlink cmd
    ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_SCAN_REQUEST                             = 0x00,
    ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_DEVICE_INFORMATION_REQUEST               = 0x02,
    ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_DEVICE_IDENTIFY_REQUEST                  = 0x06,
    ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_RESET_TO_FACTORY_NEW_REQUEST             = 0x07,
    ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_NETWORK_START_REQUEST                    = 0x10,
    ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_NETWORK_JOIN_ROUTER_REQUEST              = 0x12,
    ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_NETWORK_JOIN_END_DEVICE_REQUEST          = 0x14,
    ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_NETWORK_UPDATE_REQUEST                   = 0x16,

    //Received Utility cmd
    ZCL_ZLL_COMMISSIONING_CMD_UTILITY_GET_GROUP_IDENTIFIERS_REQUEST              = 0x41, 
    ZCL_ZLL_COMMISSIONING_CMD_UTILITY_GET_ENDPOINT_LIST_REQUEST                  = 0x42,

    //Generated Touchlink cmd
    ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_SCAN_RESPONSE                            = 0x01,
    ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_DEVICE_INFORMATION_RESPONSE              = 0x03,
    ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_NETWORK_START_REPONSE                    = 0x11,
    ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_NETWORK_JOIN_ROUTER_REPONSE              = 0x13,
    ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_NETWORK_JOIN_END_DEVICE_REPONSE          = 0x15,

    //Generated Utility cmd
    ZCL_ZLL_COMMISSIONING_CMD_UTILITY_ENDPOINT_INFORMATION                       = 0x40,
    ZCL_ZLL_COMMISSIONING_CMD_UTILITY_GET_GROUP_IDENTIFIERS_REPONSE              = 0x41, 
    ZCL_ZLL_COMMISSIONING_CMD_UTILITY_GET_ENDPOINT_LIST_REPONSE                  = 0x42


    
} zcl_zll_commissioning_enums_t;

/**************************************************************************/
/*!
    This is the data for the attribute.
*/
/**************************************************************************/


/**************************************************************************/
/*!
    This struct holds the actual attribute list as well as the attribute
    data.
*/
/**************************************************************************/
typedef struct _zcl_zll_commissioning_attrib_list_t
{
    zclAttribute    list[ZCL_ZLL_COMMISSIONING_ATTRIB_LIST_SZ];
} zcl_zll_commissioning_attrib_list_t;

typedef struct _zcl_zll_commissioning_scan_req_t
{
    uint32_t  inter_pan_transaction_id;
    uint8_t   zigbee_information;   
    uint8_t   zll_information;
} zcl_zll_commissioning_scan_req_t;

typedef struct _zcl_zll_commissioning_device_information_req_t
{
    uint32_t  inter_pan_transaction_id;
    uint8_t   start_index;   
} zcl_zll_commissioning_device_information_req_t;

typedef struct _zcl_zll_commissioning_identify_req_t
{
    uint32_t  inter_pan_transaction_id;
    uint16_t  identify_duration;   
} zcl_zll_commissioning_identify_req_t;

typedef struct _zcl_zll_commissioning_reset_to_factory_new_req_t
{
    uint32_t  inter_pan_transaction_id;
} zcl_zll_commissioning_reset_to_factory_new_req_t;

typedef struct _zclZllCommissioningReq
{
  zcl_zll_commissioning_scan_req_t                  scan_request;
  zcl_zll_commissioning_device_information_req_t    device_information_request;
  zcl_zll_commissioning_identify_req_t              identify_request;
  zcl_zll_commissioning_reset_to_factory_new_req_t  reset_to_factory_new_request;
} zclZllCommissioningReq;
/**************************************************************************/
/*!

*/
/**************************************************************************/


// prototypes
void    zcl_zll_commissioning_init(zcl_zll_commissioning_attrib_list_t *attrib_list);
uint8_t zcl_zll_commissioning_gen_req(uint8_t *data, zcl_hdr_t *hdr, zclZllCommissioningReq *req);
void    zcl_zll_commissioning_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t option, uint64_t addr, uint16_t profile, zcl_hdr_t *hdr);

#endif
