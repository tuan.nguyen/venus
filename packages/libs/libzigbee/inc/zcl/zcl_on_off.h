#ifndef _ZCL_ON_OFF_H_
#define _ZCL_ON_OFF_H_
#include <stdint.h>
#include <stdbool.h>
#include "zcl_gen.h"
#define ZCL_ON_OFF_ATTRIB_LIST_SZ 2

/**************************************************************************/
/*!
        On/Off enumerated values.
*/
/**************************************************************************/
typedef enum _zcl_on_off_enum_t
{
        ZCL_ON_OFF_ATTRIB       							= 0x0000,       ///< On/Off attribute ID

        ZCL_ON_OFF_CMD_OFF      							= 0x00,         ///< Command ID to turn attrib status to off
        ZCL_ON_OFF_CMD_ON       							= 0x01,         ///< Command ID to turn attrib status to on
        ZCL_ON_OFF_CMD_TOGGLE   							= 0x02,         ///< Command ID to toggle attrib status

        ZCL_ON_OFF_ATTRIB_MFG_SMARTENIT_MODE 				= 0x0101,		///< On/Off attribute ID mode register
        ZCL_ON_OFF_ATTRIB_MFG_SMARTENIT_RELAY_STATUS 		= 0x0102,		///< On/Off attribute ID bitmap of the valve status
        ZCL_ON_OFF_ATTRIB_MFG_SMARTENIT_RELAY_PERCENT 		= 0x0106,		///< On/Off attribute ID percent adjustment to apply to timers

        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_OFF	    		= 0x10, 		///< Command ID to turn a valve off
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_ON		    	= 0x11, 		///< Command ID to turn a valve on
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_TOGGLE			= 0x12,			///< Command ID to toggle a valve (change to opposite state)
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SETRELAYS		= 0x13,			///< Command ID to set a pattern on relays
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_GETRELAYS		= 0x14,			///< Command ID to get a pattern on relays
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SETMODE			= 0x15,			///< Command ID to set the mode register
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_GETMODE			= 0x16,			///< Command ID to get the mode register
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SKIPFW			= 0x17,			///< Command ID to skip the next valve
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SKIPBK			= 0x18,			///< Command ID to skip the previous valve
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_PROG_ONOFF		= 0x19,			///< Command ID to start/stop a specific program sequence
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SETTIMERS		= 0x1C,			///< Command ID to set a timer bank
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_GETTIMERS		= 0x1D,			///< Command ID to get a timer bank
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_PUMP_SET_CONFIGURATION = 0x1E,
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_PUMP_GET_PUMP 			= 0x1F,  
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SETNAME			= 0x20,			///< Command ID to set a relay name string
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_GETNAME			= 0x21,			///< Command ID to get a relay name string

        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_GETRELAYS_RSP 	= 0x31,
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_GETMODE_RSP 		= 0x30,
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_GETTIMERS_RSP 	= 0x32,
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_PUMP_GET_RSP 			= 0x33,
        ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_GET_NAME_RSP 	= 0x34,

} zcl_on_off_enum_t;

/**************************************************************************/
/*!
        This struct holds the actual attribute value.
*/
/**************************************************************************/
typedef struct _zcl_on_off_data_t
{
    bool on_off;                                                ///< On/off attribute status
} zcl_on_off_data_t;

/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_on_off_attrib_list_t
{
    zclAttribute list[ZCL_ON_OFF_ATTRIB_LIST_SZ];
    zcl_on_off_data_t data;
} zcl_on_off_attrib_list_t;


typedef struct _zcl_on_off_relay_off_req_t
{
  	uint8_t          relay_number;
} zcl_on_off_relay_off_req_t;


typedef struct _zcl_on_off_relay_on_req_t
{
  	uint8_t          relay_number;
} zcl_on_off_relay_on_req_t;


typedef struct _zcl_on_off_relay_toggle_req_t
{
 	uint8_t          relay_number;
} zcl_on_off_relay_toggle_req_t;


typedef struct _zcl_on_off_relay_setrelays_req_t
{
   	uint32_t          relay_pattern;
} zcl_on_off_relay_setrelays_req_t;


typedef struct _zcl_on_off_relay_setmode_req_t
{
  	uint8_t          relay_mode;
	// Value to be put in Relay Mode attribute:
	// Bits 3:0 –Active program 1-7. A value of zero indicates
	// the default timer values are in use (no program is
	// active)
	// Bit 4 -
	// Set when the unit is in Diagnostics mode
	// Bit 5 -
	// Set when the timers are enabled
	// Bit 6 -
	// Set when the unit in in irrigation mode. This
	// will also set the timers enabled bit.
	// Bit 7 - Set when the unit is disabled (not accepting
	// commands other than enable)

} zcl_on_off_relay_setmode_req_t;


typedef struct _zcl_on_off_relay_prog_onoff_req_t
{
  	uint8_t          program_number;
} zcl_on_off_relay_prog_onoff_req_t;

typedef struct _zcl_on_off_relay_set_timer_values_req_t
{
  	uint8_t          timer_number;
  	uint8_t 		 timer_cnt;
  	uint16_t 		 timer_list[16];		
} zcl_on_off_relay_set_timer_values_req_t;

typedef struct _zcl_on_off_relay_get_timer_values_req_t
{
  	uint8_t          timer_number;
} zcl_on_off_relay_get_timer_values_req_t;

typedef struct _zcl_on_off_pump_set_configuration_req_t
{
  	uint32_t          pump_config;
} zcl_on_off_pump_set_configuration_req_t;


typedef struct _zcl_on_off_relay_set_name_req_t
{
  	uint8_t         relay_number;
  	uint8_t         relay_name_len;
  	char 		 	relay_name[32];
} zcl_on_off_relay_set_name_req_t;

typedef struct _zcl_on_off_relay_get_name_req_t
{
  	uint8_t         relay_number;
} zcl_on_off_relay_get_name_req_t;


typedef struct _zclOnOffReq
{
	zcl_on_off_relay_off_req_t 				relay_off;
	zcl_on_off_relay_on_req_t 				relay_on;
	zcl_on_off_relay_toggle_req_t 			relay_toggle;
	zcl_on_off_relay_setrelays_req_t 		relay_setrelays;
	zcl_on_off_relay_setmode_req_t 			relay_setmode;
	zcl_on_off_relay_prog_onoff_req_t 		relay_prog_onoff;
    zcl_on_off_relay_set_timer_values_req_t relay_set_timer_values;
	zcl_on_off_relay_get_timer_values_req_t relay_get_timer_values;
    zcl_on_off_pump_set_configuration_req_t pump_set_configuration;
    zcl_on_off_relay_set_name_req_t         relay_set_name;
	zcl_on_off_relay_get_name_req_t 		relay_get_name;		

} zclOnOffReq;

//interface
uint8_t 	zcl_on_off_gen_req(uint8_t *data, zcl_hdr_t *hdr, zclOnOffReq *req);
void 		zcl_on_off_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);
void 		zcl_on_off_init(zcl_on_off_attrib_list_t *attrib_list);


#endif

