
#ifndef ZCL_MS_ACCELERATION_H
#define ZCL_MS_ACCELERATION_H
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"
#include "zcl_gen.h"

#define ZCL_MS_ACCELERATION_ATTRIB_LIST_SZ    1              ///< Door lock attribute list size




typedef enum _zcl_ms_acceleration_enum_t
{
   
    // Manufacturer specific Acceleration cmd received
    ZCL_MS_ACCELERATION_CMD_ACCELERATION_MSG                         = 0x05,
    ZCL_MS_ACCELERATION_CMD_TEMPERATURE_BATTERY_MSG                  = 0x07,


 }zcl_ms_acceleration_enum_t;


typedef struct _zcl_ms_acceleration_attrib_list_t
{
    zclAttribute            list[ZCL_MS_ACCELERATION_ATTRIB_LIST_SZ];
} zcl_ms_acceleration_attrib_list_t;




void    zcl_ms_acceleration_init(zcl_ms_acceleration_attrib_list_t *attrib_list);
uint8_t zcl_ms_acceleration_gen_req(uint8_t *data, zcl_hdr_t *hdr);
void    zcl_ms_acceleration_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);

#endif