
#ifndef ZCL_DOOR_LOCK_H
#define ZCL_DOOR_LOCK_H
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"
#include "zcl_gen.h"

#define ZCL_DOOR_LOCK_ATTRIB_LIST_SZ    8              ///< Door lock attribute list size

/*** User Status Value enums ***/
#define USER_STATUS_AVAILABLE                                   0x00
#define USER_STATUS_OCCUPIED_ENABLED                            0x01
#define USER_STATUS_RESERVED                                    0x02
#define USER_STATUS_OCCUPIED_DISABLED                           0x03


/*** User Type Value enums ***/
#define USER_TYPE_UNRESTRICTED_USER                             0x00 // default
#define USER_TYPE_YEAR_DAY_SCHEDULE_USER                        0x01
#define USER_TYPE_WEEK_DAY_SCHEDULE_USER                        0x02
#define USER_TYPE_MASTER_USER                                   0x03

#define DOOR_LOCK_LOCK_STATE_NOT_FULLY_LOCKED                   0x00
#define DOOR_LOCK_LOCK_STATE_LOCKED                             0x01
#define DOOR_LOCK_LOCK_STATE_UNLOCKED                           0x02
#define DOOR_LOCK_LOCK_STATE_UNKNOWN                            0xFF


typedef enum _zcl_door_lock_enum_t
{
    // Door lock attrib id
    ZCL_DOOR_LOCK_ATTRIB_LOCK_STATE                      = 0x0000,        
    ZCL_DOOR_LOCK_ATTRIB_LOCK_TYPE                       = 0x0001,       
    ZCL_DOOR_LOCK_ATTRIB_ACTUATOR_ENABLED                = 0x0002, 
    ZCL_DOOR_LOCK_ATTRIB_DOOR_STATE                      = 0x0003,
    ZCL_DOOR_LOCK_ATTRIB_DOOR_OPEN_EVENTS                = 0x0004,
    ZCL_DOOR_LOCK_ATTRIB_DOOR_CLOSED_EVENTS              = 0x0005,
    ZCL_DOOR_LOCK_ATTRIB_OPEN_PERIOD                     = 0x0006,
   

    // Door lock cmd received
    ZCL_DOOR_LOCK_CMD_LOCK_DOOR                         = 0x00,
    ZCL_DOOR_LOCK_CMD_UNLOCK_DOOR                       = 0x01,
    ZCL_DOOR_LOCK_CMD_TOGGLE_DOOR                       = 0x02,
    ZCL_DOOR_LOCK_CMD_UNLOCK_WITH_TIMEOUT               = 0x03,
    ZCL_DOOR_LOCK_CMD_GET_LOCK_RECORD                   = 0x04,
    ZCL_DOOR_LOCK_CMD_SET_PIN_CODE                      = 0x05,
    ZCL_DOOR_LOCK_CMD_GET_PIN_CODE                      = 0x06,
    ZCL_DOOR_LOCK_CMD_CLEAR_PIN_CODE                    = 0x07,
    ZCL_DOOR_LOCK_CMD_CLEAR_ALL_PIN_CODE                = 0x08,
    ZCL_DOOR_LOCK_CMD_SET_USER_STATUS                   = 0x09,
    ZCL_DOOR_LOCK_CMD_GET_USER_STATUS                   = 0x0A,



    //ias zone cmd generated 
    ZCL_DOOR_LOCK_CMD_LOCK_DOOR_RESPONSE                = 0x00,
    ZCL_DOOR_LOCK_CMD_UNLOCK_DOOR_RESPONSE              = 0x01,
    ZCL_DOOR_LOCK_CMD_TOGGLE_DOOR_RESPONSE              = 0x02,
    ZCL_DOOR_LOCK_CMD_UNLOCK_WITH_TIMEOUT_RESPONSE      = 0x03,
    ZCL_DOOR_LOCK_CMD_GET_LOCK_RECORD_RESPONSE          = 0x04,
    ZCL_DOOR_LOCK_CMD_SET_PIN_CODE_RESPONSE             = 0x05,
    ZCL_DOOR_LOCK_CMD_GET_PIN_CODE_RESPONSE             = 0x06,
    ZCL_DOOR_LOCK_CMD_CLEAR_PIN_CODE_RESPONSE           = 0x07,
    ZCL_DOOR_LOCK_CMD_CLEAR_ALL_PIN_CODE_RESPONSE       = 0x08,
    ZCL_DOOR_LOCK_CMD_SET_USER_STATUS_RESPONSE          = 0x09,
    ZCL_DOOR_LOCK_CMD_GET_USER_STATUS_RESPONSE          = 0x0A,
    ZCL_DOOR_LOCK_CMD_READ_ATTRIBUTE_RESPONSE           = 0x0B,
        // Door lock notification
    ZCL_DOOR_LOCK_OPERATION_EVENT_NOTIFICATION          = 0x20,
    ZCL_DOOR_LOCK_PROGRAMMING_EVENT_NOTIFICATION        = 0x21,
    ZCL_DOOR_LOCK_CMD_UNKNOWN                           = 0xFF


 }zcl_door_lock_enum_t;

typedef struct _zcl_door_lock_data_t
{
    uint8_t         lock_state;
    uint8_t         lock_type;
    bool            actuator_enabled;
    uint8_t         door_state;
    uint32_t        door_open_events;
    uint32_t        door_close_events;
    uint16_t        door_period;
} zcl_door_lock_data_t;

typedef struct _zcl_door_lock_set_pin_code_req_t
{
  uint16_t          user_id;
  uint8_t           user_status;   // e.g. USER_STATUS_AVAILABLE
  uint8_t           user_type;     // e.g. USER_TYPE_UNRESTRICTED_USER
  uint8_t           pin_length;
  uint8_t           p_pin[8];      // variable length string
} zcl_door_lock_set_pin_code_req_t;

typedef struct _zcl_door_lock_clear_pin_code_req_t
{
  uint16_t          user_id;
  uint8_t           pin_length;
  uint8_t           p_pin[8];      // variable length string
} zcl_door_lock_clear_pin_code_req_t;

typedef struct _zcl_door_lock_user_id_req_t
{
  uint16_t user_id;
} zcl_door_lock_user_id_req_t;

typedef struct _zcl_door_lock_set_user_status_req_t
{
  uint16_t  user_id;
  uint8_t   user_status;   // e.g. USER_STATUS_AVAILABLE
} zcl_door_lock_set_user_status_req_t;


typedef struct _zclDoorLockReq
{
    zcl_door_lock_set_pin_code_req_t        set_pin_code;
    zcl_door_lock_user_id_req_t             get_pin_code;
    zcl_door_lock_clear_pin_code_req_t      clear_pin_code; 
    zcl_door_lock_set_user_status_req_t     set_user_status;
    zcl_door_lock_user_id_req_t             get_user_status;
} zclDoorLockReq;


/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_door_lock_attrib_list_t
{
    zclAttribute            list[ZCL_DOOR_LOCK_ATTRIB_LIST_SZ];
    zcl_door_lock_data_t    data;
} zcl_door_lock_attrib_list_t;




void    zcl_door_lock_init(zcl_door_lock_attrib_list_t *attrib_list);
uint8_t zcl_door_lock_gen_req(uint8_t *data, zcl_hdr_t *hdr, zclDoorLockReq *req);
void    zcl_door_lock_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);

#endif