
#ifndef ZCL_ILLUMINANCE_LEVEL_SENSING_H
#define ZCL_ILLUMINANCE_LEVEL_SENSING_H
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"
#include "zcl_gen.h"

#define ZCL_ILLUMINANCE_LEVEL_SENSING_ATTRIB_LIST_SZ    4              ///<  attribute list size




typedef enum _zcl_illuminance_level_sensing_enum_t
{
    // illuminance level sensing information
    ZCL_ILLUMINANCE_LEVEL_SENSING_ATTRIB_LEVEL_STATUS               = 0x0000,        
    ZCL_ILLUMINANCE_LEVEL_SENSING_ATTRIB_LIGHT_SENSOR_TYPE          = 0x0001,

    // illuminance level sensing settings
    ZCL_ILLUMINANCE_LEVEL_SENSING_ATTRIB_ILLUMINANCE_TARGET_LEVEL   = 0x0010,        
    
 }zcl_illuminance_level_sensing_enum_t;

typedef struct _zcl_illuminance_level_sensing_data_t
{
    uint16_t        level_status;
    uint16_t        light_sensor_type;
    uint16_t        illuminance_target_level;

} zcl_illuminance_level_sensing_data_t;




/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_illuminance_level_sensing_attrib_list_t
{
    zclAttribute                           list[ZCL_ILLUMINANCE_LEVEL_SENSING_ATTRIB_LIST_SZ];
    zcl_illuminance_level_sensing_data_t   data;
} zcl_illuminance_level_sensing_attrib_list_t;



void    zcl_illuminance_level_sensing_init(zcl_illuminance_level_sensing_attrib_list_t *attrib_list);
void    zcl_illuminance_level_sensing_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);

#endif