
#ifndef ZCL_GRP_H
#define ZCL_GRP_H

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"
#include "zcl_gen.h"

#define ZCL_GRP_ATTRIB_LIST_SZ  2
#define ZCL_GRP_MAX_LIST_SZ     10
#define ZCL_GRP_NAME_SUPPORT    0x00

/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef enum _zcl_grp_enums_t
{
    ZCL_GRP_ATTRIB_NAME_SUPPORT  = 0x00,

    ZCL_GRP_CMD_ADD_GRP         = 0x00,
    ZCL_GRP_CMD_VIEW_GRP        = 0x01,
    ZCL_GRP_CMD_GET_MEMB        = 0x02,
    ZCL_GRP_CMD_REM_GRP         = 0x03,
    ZCL_GRP_CMD_REM_ALL         = 0x04,
    ZCL_GRP_CMD_ADD_IF_ID       = 0x05,

    ZCL_GRP_CMD_ADD_GRP_RESP    = 0x00,
    ZCL_GRP_CMD_VIEW_GRP_RESP   = 0x01,
    ZCL_GRP_CMD_GET_MEMB_RESP   = 0x02,
    ZCL_GRP_CMD_REM_GRP_RESP    = 0x03,

    ZCL_GRP_CAPACITY_AVAIL      = 0xfe
} zcl_grp_enums_t;

/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_grp_data_t
{
    uint8_t name_supp;
} zcl_grp_data_t;

/**************************************************************************/
/*!

*/
/**************************************************************************/


typedef struct _zcl_grp_add_group_req_t
{
  uint16_t  group_id;
  uint8_t   len_name;
  uint8_t   group_name[64];   
} zcl_grp_add_group_req_t;

typedef struct _zcl_grp_view_group_req_t
{
  uint16_t  group_id;   
} zcl_grp_view_group_req_t;

typedef struct _zcl_grp_get_group_membership_req_t
{
  uint8_t  group_count;
  uint16_t group_list[32];   
} zcl_grp_get_group_membership_req_t;

typedef struct _zcl_grp_remove_group_req_t
{
  uint16_t  group_id;   
} zcl_grp_remove_group_req_t;

typedef struct _zcl_grp_add_group_if_identifying_req_t
{
  uint16_t  group_id;
  uint8_t   len_name;
  uint8_t   group_name[64];   
} zcl_grp_add_group_if_identifying_req_t;

typedef struct _zcl_grp_req_t
{
    zcl_grp_add_group_req_t                 add_group;
    zcl_grp_view_group_req_t                view_group;
    zcl_grp_get_group_membership_req_t      get_group_membership; 
    zcl_grp_remove_group_req_t              remove_group;
    zcl_grp_add_group_if_identifying_req_t  add_group_if_identifying;
} zcl_grp_req_t;


typedef struct _zcl_grp_attrib_list_t
{
    zclAttribute list[ZCL_GRP_ATTRIB_LIST_SZ];
    zcl_grp_data_t data;
} zcl_grp_attrib_list_t;

void    zcl_grp_init(zcl_grp_attrib_list_t *attrib_list);
uint8_t zcl_grp_gen_req(uint8_t *data, zcl_hdr_t *hdr, zcl_grp_req_t *req);
void    zcl_grp_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);

#endif
