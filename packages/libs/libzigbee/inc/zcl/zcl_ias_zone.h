
#ifndef ZCL_IAS_ZONE_H
#define ZCL_IAS_ZONE_H
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"
#include "zcl_gen.h"

#define ZCL_IAS_ZONE_ATTRIB_LIST_SZ    5              ///< IAS Zone attribute list size


#define ZONE_STATUS_ALARM1_BIT_MARK                         0x01
#define ZONE_STATUS_ALARM2_BIT_MARK                         0x02
#define ZONE_STATUS_TAMPER_BIT_MARK                         0x04
#define ZONE_STATUS_BATTERY_BIT_MARK                        0x08
#define ZONE_STATUS_SUPERVISION_REPORTS_BIT_MARK            0x10
#define ZONE_STATUS_RESTORE_REPORTS_BIT_MARK                0x20
#define ZONE_STATUS_TROUBLE_BIT_MARK                        0x40
#define ZONE_STATUS_AC_MAINS_BIT_MARK                       0x80

#define ZONE_TYPE_STANDARD_CIE                              0x0000
#define ZONE_TYPE_MOTION_SENSOR                             0x000D
#define ZONE_TYPE_CONTACT_SWITCH                            0x0015
#define ZONE_TYPE_FIRE_SENSOR                               0x0028
#define ZONE_TYPE_WATER_SENSOR                              0x002A
#define ZONE_TYPE_GAS_SENSOR                                0x002B
#define ZONE_TYPE_PERSONAL_EMERGENCY_DEVICE                 0x002C
#define ZONE_TYPE_VIBRATION_MOVEMENT_SENSOR                 0x002D
#define ZONE_TYPE_REMOTE_CONTROL                            0x010F
#define ZONE_TYPE_KEYFOB                                    0x0115
#define ZONE_TYPE_KEYPAD                                    0x021D
#define ZONE_TYPE_STANDARD_WARNING_DEVICE                   0x0225
#define ZONE_TYPE_INVALID_TYPE                              0xFFFF

typedef enum _zcl_ias_zone_enum_t
{
    // Zone information attrib id
    ZCL_IAS_ZONE_ATTRIB_ZONE_STATE                      = 0x0000,        
    ZCL_IAS_ZONE_ATTRIB_ZONE_TYPE                       = 0x0001,       
    ZCL_IAS_ZONE_ATTRIB_ZONE_STATUS                     = 0x0002,       
    // Zone settings attrib id
    ZCL_IAS_ZONE_ATTRIB_IAS_CIE_ADDRESS                 = 0x0010,

    // ias zone cmd received
    ZCL_IAS_ZONE_CMD_ZONE_ENROLL_RESPONSE               = 0x00,

    //ias zone cmd generated 
    ZCL_IAS_ZONE_CMD_ZONE_STATUS_CHANGE_NOTIFICATION    = 0x00,
    ZCL_IAS_ZONE_CMD_ZONE_ENROLL_REQUEST                = 0x01

} zcl_ias_zone_enum_t;

typedef struct _zcl_ias_zone_data_t
{
    uint8_t         zone_state;
    uint16_t        zone_type;
    uint16_t        zone_status;
    uint64_t        ias_cie_address;
} zcl_ias_zone_data_t;

/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_ias_zone_attrib_list_t
{
    zclAttribute            list[ZCL_IAS_ZONE_ATTRIB_LIST_SZ];
    zcl_ias_zone_data_t    data;
} zcl_ias_zone_attrib_list_t;


typedef enum _enroll_response_code_enum_t
{
    ZCL_IAS_ZONE_ENROLL_RESPONSE_CODE_SUCCESS           = 0x00,
    ZCL_IAS_ZONE_ENROLL_RESPONSE_CODE_NOT_SUPPORTED     = 0x01,
    ZCL_IAS_ZONE_ENROLL_RESPONSE_CODE_NO_ENROLL_PERMIT  = 0x02,
    ZCL_IAS_ZONE_ENROLL_RESPONSE_CODE_TOO_MANY_ZONES    = 0x03
}enroll_response_code_enum_t;

typedef struct _zcl_ias_zone_zone_enroll_response_t
{
    enroll_response_code_enum_t enroll_response_code;
    uint8_t                     zone_id;
}zcl_ias_zone_zone_enroll_response_t;

typedef struct _zcl_ias_zone_zone_status_change_notification_t
{
    uint16_t                    zone_status;
    uint8_t                     extended_status;
}zcl_ias_zone_zone_status_change_notification_t;

typedef struct _zcl_ias_zone_zone_enroll_request_t
{
    uint16_t                    zone_type;
    uint16_t                    manufacturer_code;
}zcl_ias_zone_zone_enroll_request_t;

typedef struct _zclIasZoneReq 
{
    zcl_ias_zone_zone_enroll_response_t             zone_enroll_response;
    zcl_ias_zone_zone_status_change_notification_t  zone_status_change_notification;
    zcl_ias_zone_zone_enroll_request_t              zone_enroll_request;
}zclIasZoneReq;


void    zcl_ias_zone_init(zcl_ias_zone_attrib_list_t *attrib_list);
uint8_t zcl_ias_zone_gen_req(uint8_t *data, zcl_hdr_t *hdr, zclIasZoneReq *req);
void    zcl_ias_zone_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);

#endif