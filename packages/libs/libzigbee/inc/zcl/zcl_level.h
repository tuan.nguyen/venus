#ifndef ZCL_LEVEL_H
#define ZCL_LEVEL_H
#include "zcl.h"
#include "../memory/list.h"
#include "zcl_gen.h"

#define ZCL_LEVEL_ATTRIB_LIST_SZ 5
#define ZCL_LEVEL_TMR(m)   ((zcl_level_tmr_t *)MMEM_PTR(&m->mmem_ptr))      ///< De-reference the mem ptr and cast it as an zcl level timer value

/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef enum zcl_level_enums_t
{
    ZCL_LEVEL_ATTRIB_CURR_LEVEL             = 0x0000,
    ZCL_LEVEL_ATTRIB_ON_OFF_TRANS_TIME      = 0x0010,
    ZCL_LEVEL_ATTRIB_ON_LEVEL               = 0x0011,

    ZCL_LEVEL_CMD_MOVE_TO_LEVEL             = 0x00,
    ZCL_LEVEL_CMD_MOVE                      = 0x01,
    ZCL_LEVEL_CMD_STEP                      = 0x02,
    ZCL_LEVEL_CMD_STOP                      = 0x03,
    ZCL_LEVEL_CMD_MOVE_TO_LEVEL_WITH_ON_OFF = 0x04,
    ZCL_LEVEL_CMD_MOVE_WITH_ON_OFF          = 0x05,
    ZCL_LEVEL_CMD_STEP_WITH_ON_OFF          = 0x06,
    ZCL_LEVEL_CMD_STOP_WITH_ON_OFF          = 0x07,

    ZCL_LEVEL_MODE_UP                       = 0x00,
    ZCL_LEVEL_MODE_DOWN                     = 0x01,

    ZCL_LEVEL_ACTION_REFRESH                = 0x00,
    ZCL_LEVEL_ACTION_MAX_LEVEL              = 0x01,
    ZCL_LEVEL_ACTION_MIN_LEVEL              = 0x02
} zcl_level_enums_t;

/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct zcl_level_data_t
{
    uint8_t  curr_level;
    uint16_t on_off_trans_time;
    uint8_t  on_level;
} zcl_level_data_t;

/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_level_attrib_list_t
{
    zclAttribute        list[ZCL_LEVEL_ATTRIB_LIST_SZ];
    zcl_level_data_t    data;
} zcl_level_attrib_list_t;

/**************************************************************************/
/*!
*/
/**************************************************************************/
typedef struct _zcl_level_tmr_t
{
    uint8_t *level;                          ///< pointer to level attribute
    uint8_t step;                            ///< step size
    uint16_t acc;                            ///< accumulator used to track roundoff error
    uint8_t rem;                            ///< remainder term used to handle roundoff error
    uint16_t rem_time;                       ///< time remaining for this operation
    bool dir;                           ///< direction of level change
    bool with_on_off;                   ///< integration with on/off attribute
    void (*action_handler)(uint8_t, void *); ///< action handler for this operation
} zcl_level_tmr_t;

////////////////////////////////////////////////////////////////////////////
// client side requests
////////////////////////////////////////////////////////////////////////////

/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_level_move_to_level_req_t
{
    uint8_t level;
    uint16_t trans_time;
} zcl_level_move_to_level_req_t;

/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_level_move_req_t
{
    uint8_t dir;
    uint16_t rate;
} zcl_level_move_req_t;

/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_level_step_req_t
{
    uint8_t dir;
    uint8_t step_size;
    uint16_t trans_time;
} zcl_level_step_req_t;

/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zclLevelReq
{
    zcl_level_move_to_level_req_t   move_to_level;
    zcl_level_move_req_t            move;
    zcl_level_step_req_t            step;
} zclLevelReq;

// prototypes
void zcl_level_init(zcl_level_attrib_list_t *attrib_list);
uint8_t zcl_level_gen_req(uint8_t *data, zcl_hdr_t *hdr, zclLevelReq *req);
void zcl_level_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);
void zcl_level_tmr_add(uint8_t *level, uint16_t rem_time, uint8_t step, uint8_t rem, bool dir, bool on_off, void (*action_handler)(uint8_t, void *));
void zcl_level_tmr_periodic(void *ptr);
#endif
