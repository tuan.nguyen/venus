#ifndef ZCL_SCENES_H
#define ZCL_SCENES_H

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"
#include "zcl_gen.h"

#define ZCL_SCENES_ATTRIB_LIST_SZ       7
#define ZCL_SCENES_NAME_MAX_LEN         16
#define ZCL_SCENES_EXT_MAX_LEN          13
#define ZCL_SCENES_NAME_SUPPORT         0x80
#define ZCL_SCENES_GET_MEMB_CAPACITY    0xfe

typedef enum _zcl_scenes_enum_t
{
    ZCL_SCENES_ATTRIB_SCENE_COUNT       = 0x0000,
    ZCL_SCENES_ATTRIB_CURR_SCENE        = 0x0001,
    ZCL_SCENES_ATTRIB_CURR_GROUP        = 0x0002,
    ZCL_SCENES_ATTRIB_SCENE_VALID       = 0x0003,
    ZCL_SCENES_ATTRIB_NAME_SUPP         = 0x0004,
    ZCL_SCENES_ATTRIB_LAST_CONFIG_BY    = 0x0005,

    ZCL_SCENES_CMD_ADD                  = 0x00,
    ZCL_SCENES_CMD_VIEW                 = 0x01,
    ZCL_SCENES_CMD_REM                  = 0x02,
    ZCL_SCENES_CMD_REM_ALL              = 0x03,
    ZCL_SCENES_CMD_STORE                = 0x04,
    ZCL_SCENES_CMD_RECALL               = 0x05,
    ZCL_SCENES_CMD_GET_MEMB             = 0x06,

    ZCL_SCENES_CMD_ADD_RESP             = 0x00,
    ZCL_SCENES_CMD_VIEW_RESP            = 0x01,
    ZCL_SCENES_CMD_REM_RESP             = 0x02,
    ZCL_SCENES_CMD_REM_ALL_RESP         = 0x03,
    ZCL_SCENES_CMD_STORE_RESP           = 0x04,
    ZCL_SCENES_CMD_GET_MEMB_RESP        = 0x06,

    ZCL_SCENES_ACTION_STORE             = 0x01,
    ZCL_SCENES_ACTION_RECALL            = 0x02
} zcl_scenes_enum_T;

typedef struct _zcl_scenes_data_t
{
    uint8_t      scene_cnt;
    uint8_t      curr_scene;
    uint16_t     curr_grp;
    bool   		 scene_valid;
    uint8_t      name_supp;
    uint64_t     last_cfg_by;
} zcl_scenes_data_t;

typedef struct _zcl_scenes_attrib_list_t
{
    zclAttribute list[ZCL_SCENES_ATTRIB_LIST_SZ];
    zcl_scenes_data_t data;
} zcl_scenes_attrib_list_t;

typedef struct _zcl_scenes_req_t
{
    uint8_t  endpoint;
    uint16_t grp_id;                             ///< Scene group ID
    uint8_t  id;                                 ///< Scene ID
    char     name[ZCL_SCENES_NAME_MAX_LEN];       ///< Scene name
    uint16_t trans_time;                         ///< Transition time for this scene
    uint8_t  ext_len;                            ///< Extension fields length
    char     ext_field[ZCL_SCENES_EXT_MAX_LEN];  ///< Extension fields
} zcl_scenes_req_t;

void    zcl_scenes_init(zcl_scenes_attrib_list_t *attrib_list);
uint8_t zcl_scenes_gen_req(uint8_t *data, zcl_hdr_t *hdr, zcl_scenes_req_t *req);
void    zcl_scenes_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);

#endif