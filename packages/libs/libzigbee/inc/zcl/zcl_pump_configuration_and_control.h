
#ifndef ZCL_PUMP_CONFIG_AND_CONTROL_H
#define ZCL_PUMP_CONFIG_AND_CONTROL_H
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"
#include "zcl_gen.h"

#define ZCL_PUMP_CONFIGURATION_AND_CONTROL_ATTRIB_LIST_SZ    25              ///< Pump configuration and control attribute list size




typedef enum _zcl_pump_configuration_and_control_enum_t
{
    // Pump Information attrib id
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_PRESSURE                       = 0x0000,        
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_SPEED                          = 0x0001,       
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_FLOW                           = 0x0002, 
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MIN_CONST_PRESSURE                 = 0x0003,
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_CONST_PRESSURE                 = 0x0004,
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MIN_COMP_PRESSURE                  = 0x0005,
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_COMP_PRESSURE                  = 0x0006,
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MIN_CONST_SPEED                    = 0x0007,
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_CONST_SPEED                    = 0x0008,
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MIN_CONST_FLOW                     = 0x0009,
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_CONST_FLOW                     = 0x000A,
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MIN_CONST_TEMP                     = 0x000B,
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_CONST_TEMP                     = 0x000C,

    // Pump Dynamic Information attrib id
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_PUMP_STATUS                       = 0x0010,        
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_EFFECTIVE_OPERATION_MODE          = 0x0011,       
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_EFFECTIVE_CONTROL_MODE            = 0x0012, 
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_CAPACITY                          = 0x0013,
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_SPEED                             = 0x0014,
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_LIFETIME_RUNNING_HOURS            = 0x0015,
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_POWER                             = 0x0016,
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_LIFETIME_ENERGY_CONSUMED          = 0x0017,

    // Pump Settings attrib id
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_OPERATION_MODE                    = 0x0020,        
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_CONTROL_MODE                      = 0x0021,       
    ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_ALARM_MASK                        = 0x0022, 
   


 }zcl_pump_configuration_and_control_enum_t;

typedef struct _zcl_pump_configuration_and_control_data_t
{
    int16_t         max_pressure;
    uint16_t        max_speed;
    uint16_t        max_flow;
    int16_t         min_const_pressure;
    int16_t         max_const_pressure;
    int16_t         min_comp_pressure;
    int16_t         max_comp_pressure;
    uint16_t        min_const_speed;
    uint16_t        max_const_speed;
    uint16_t        min_const_flow;
    uint16_t        max_const_flow;
    int16_t         min_const_temp;
    int16_t         max_const_temp;

    uint16_t        pump_status;
    uint8_t         effective_operation_mode;
    uint8_t         effective_control_mode;
    int16_t         capacity;
    uint16_t        speed;
    uint32_t        lifetime_running_hours;
    uint32_t        power;
    uint32_t        lifetime_energy_consumed;

    uint8_t         operation_mode;
    uint8_t         control_mode;
    uint16_t        alarm_mask;

} zcl_pump_configuration_and_control_data_t;




/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_pump_configuration_and_control_attrib_list_t
{
    zclAttribute            list[ZCL_PUMP_CONFIGURATION_AND_CONTROL_ATTRIB_LIST_SZ];
    zcl_pump_configuration_and_control_data_t    data;
} zcl_pump_configuration_and_control_attrib_list_t;


void    zcl_pump_configuration_and_control_init(zcl_pump_configuration_and_control_attrib_list_t *attrib_list);
void    zcl_pump_configuration_and_control_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);

#endif