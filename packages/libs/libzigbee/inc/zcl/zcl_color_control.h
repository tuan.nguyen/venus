
#ifndef ZCL_COLOR_CONTROL_H
#define ZCL_COLOR_CONTROL_H
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "zcl.h"
#include "zcl_gen.h"

#define ZCL_COLOR_CONTROL_ATTRIB_LIST_SZ    10              ///< Door lock attribute list size

/***  Move To Hue Cmd payload: direction field values  ***/
#define COLOR_CONTROL_MOVE_TO_HUE_DIRECTION_SHORTEST_DISTANCE                 0x00
#define COLOR_CONTROL_MOVE_TO_HUE_DIRECTION_LONGEST_DISTANCE                  0x01
#define COLOR_CONTROL_MOVE_TO_HUE_DIRECTION_UP                                0x02
#define COLOR_CONTROL_MOVE_TO_HUE_DIRECTION_DOWN                              0x03
  /***  Move Hue Cmd payload: moveMode field values   ***/
#define COLOR_CONTROL_MOVE_HUE_STOP                                           0x00
#define COLOR_CONTROL_MOVE_HUE_UP                                             0x01
#define COLOR_CONTROL_MOVE_HUE_DOWN                                           0x03

 /***  Steep Hue Cmd payload: stepMode field values ***/
#define COLOR_CONTROL_STEP_HUE_UP                                             0x01
#define COLOR_CONTROL_STEP_HUE_DOWN                                           0x03
  /***  Move Saturation Cmd payload: moveMode field values ***/
#define COLOR_CONTROL_MOVE_SATURATION_STOP                                    0x00
#define COLOR_CONTROL_MOVE_SATURATION_UP                                      0x01
#define COLOR_CONTROL_MOVE_SATURATION_DOWN                                    0x03
  /***  Steep Saturation Cmd payload: stepMode field values ***/
#define COLOR_CONTROL_STEP_SATURATION_UP                                      0x01
#define COLOR_CONTROL_STEP_SATURATION_DOWN                                    0x03

typedef enum _zcl_color_control_enum_t
{
    // Door lock attrib id
    ZCL_COLOR_CONTROL_ATTRIB_CURRENT_HUE                              = 0x0000,        
    ZCL_COLOR_CONTROL_ATTRIB_CURRENT_SATURATION                       = 0x0001,       
    ZCL_COLOR_CONTROL_ATTRIB_REMAINING_TIME                           = 0x0002, 
    ZCL_COLOR_CONTROL_ATTRIB_CURRENT_X                                = 0x0003,
    ZCL_COLOR_CONTROL_ATTRIB_CURRENT_Y                                = 0x0004,
    ZCL_COLOR_CONTROL_ATTRIB_DRIFT_COMPENSATION                       = 0x0005,
    ZCL_COLOR_CONTROL_ATTRIB_COMPENSATION_TEXT                        = 0x0006,
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_TEMPERATURE                        = 0x0007,
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_MODE                               = 0x0008,
   
    ZCL_COLOR_CONTROL_ATTRIB_NUM_PRIMARIES                            = 0x0010,
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_1_X                              = 0x0011,
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_1_Y                              = 0x0012,
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_1_INTENSITY                      = 0x0013,
  // 0x0014 is reserved
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_2_X                              = 0x0015,
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_2_Y                              = 0x0016,
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_2_INTENSITY                      = 0x0017,
  // 0x0018 is reserved
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_3_X                              = 0x0019,
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_3_Y                              = 0x001a,
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_3_INTENSITY                      = 0x001b,

 // Additional Defined Primaries Information attribute set
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_4_X                              = 0x0020,
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_4_Y                              = 0x0021,
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_4_INTENSITY                      = 0x0022,
  // 0x0023 is reserved
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_5_X                              = 0x0024,
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_5_Y                              = 0x0025,
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_5_INTENSITY                      = 0x0026,
  // 0x0027 is reserved
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_6_X                              = 0x0028,
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_6_Y                              = 0x0029,
    ZCL_COLOR_CONTROL_ATTRIB_PRIMARY_6_INTENSITY                      = 0x002a,

  // Defined Color Points Settings attribute set
    ZCL_COLOR_CONTROL_ATTRIB_WHITE_POINT_X                            = 0x0030,
    ZCL_COLOR_CONTROL_ATTRIB_WHITE_POINT_Y                            = 0x0031,
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_POINT_R_X                          = 0x0032,
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_POINT_R_Y                          = 0x0033,
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_POINT_R_INTENSITY                  = 0x0034,
  // 0x0035 is reserved
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_POINT_G_X                          = 0x0036,
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_POINT_G_Y                          = 0x0037,
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_POINT_B_INTENSITY                  = 0x0038,
  // 0x0039 is reserved
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_POINT_B_X                          = 0x003a,
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_POINT_B_Y                          = 0x003b,
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_POINT_G_INTENSITY                  = 0x003c,
  // 0x003d is reserved
    ZCL_COLOR_CONTROL_ATTRIB_ENHANCED_CURRENT_HUE                     = 0x4000,
    ZCL_COLOR_CONTROL_ATTRIB_ENHANCED_COLOR_MODE                      = 0x4001,
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_LOOP_ACTIVE                        = 0x4002,
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_LOOP_DIRECTION                     = 0x4003,
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_LOOP_TIME                          = 0x4004,
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_LOOP_START_ENHANCED_HUE            = 0x4005,
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_LOOP_STORED_ENHANCED_HUE           = 0x4006,
    ZCL_COLOR_CONTROL_ATTRIB_COLOR_CAPABILITIES                       = 0x400a,

    //Color control cmd received 
    ZCL_COLOR_CONTROL_CMD_MOVE_TO_HUE                                 = 0x00,
    ZCL_COLOR_CONTROL_CMD_MOVE_HUE                                    = 0x01,
    ZCL_COLOR_CONTROL_CMD_STEP_HUE                                    = 0x02,
    ZCL_COLOR_CONTROL_CMD_MOVE_TO_SATURATION                          = 0x03,
    ZCL_COLOR_CONTROL_CMD_MOVE_SATURATION                             = 0x04,
    ZCL_COLOR_CONTROL_CMD_STEP_SATURATION                             = 0x05,
    ZCL_COLOR_CONTROL_CMD_MOVE_TO_HUE_AND_SATURATION                  = 0x06,
    ZCL_COLOR_CONTROL_CMD_MOVE_TO_COLOR                               = 0x07,
    ZCL_COLOR_CONTROL_CMD_MOVE_COLOR                                  = 0x08,
    ZCL_COLOR_CONTROL_CMD_STEP_COLOR                                  = 0x09,
    ZCL_COLOR_CONTROL_CMD_MOVE_TO_COLOR_TEMPERATURE                   = 0x0a


 }zcl_color_control_enum_t;

typedef struct _zcl_color_control_data_t
{
    uint8_t         current_hue;
    uint8_t         current_saturation;
    uint16_t        remaining_time;
    uint16_t        current_x;
    uint16_t        current_y;
    uint8_t         drift_compensation;
    uint8_t         compensation_text[ZCL_MAX_STR_SZ];
    uint16_t        color_temperature;
    uint8_t         color_mode;
} zcl_color_control_data_t;

typedef struct _zcl_color_control_move_to_hue_req_t
{
    uint8_t           hue;   
    uint8_t           direction;     
    uint16_t          transition_time;
} zcl_color_control_move_to_hue_req_t;

typedef struct _zcl_color_control_move_hue_req_t
{
    uint8_t           move_mode;   
    uint8_t           rate;     
} zcl_color_control_move_hue_req_t;

typedef struct _zcl_color_control_step_hue_req_t
{
    uint8_t           step_mode;   
    uint8_t           step_size;
    uint8_t           transition_time;     
} zcl_color_control_step_hue_req_t;

typedef struct _zcl_color_control_move_to_saturation_req_t
{
    uint8_t           saturation;   
    uint16_t          transition_time;     
} zcl_color_control_move_to_saturation_req_t;

typedef struct _zcl_color_control_move_saturation_req_t
{
    uint8_t           move_mode;   
    uint8_t           rate;     
} zcl_color_control_move_saturation_req_t;

typedef struct _zcl_color_control_step_saturation_req_t
{
    uint8_t           step_mode;   
    uint8_t           step_size;
    uint8_t           transition_time;     
} zcl_color_control_step_saturation_req_t;

typedef struct _zcl_color_control_move_to_hue_and_saturation_req_t
{
    uint8_t           hue;   
    uint8_t           saturation;
    uint16_t          transition_time;     
} zcl_color_control_move_to_hue_and_saturation_req_t;

typedef struct _zcl_color_control_move_to_color_req_t
{
    uint16_t          color_x;   
    uint16_t          color_y;
    uint16_t          transition_time;     
} zcl_color_control_move_to_color_req_t;

typedef struct _zcl_color_control_move_color_req_t
{
    int16_t           rate_x;   
    int16_t           rate_y;
    
} zcl_color_control_move_color_req_t;

typedef struct _zcl_color_control_step_color_req_t
{
    int16_t           step_x;   
    int16_t           step_y;
    uint16_t          transition_time;
} zcl_color_control_step_color_req_t;

typedef struct _zcl_color_control_move_to_color_temperature_req_t
{
    uint16_t          color_temperature;   
    uint16_t          transition_time;
} zcl_color_control_move_to_color_temperature_req_t;


typedef struct _zclColorControlReq
{
    zcl_color_control_move_to_hue_req_t                 move_to_hue;
    zcl_color_control_move_hue_req_t                    move_hue;
    zcl_color_control_step_hue_req_t                    step_hue; 
    zcl_color_control_move_to_saturation_req_t          move_to_saturation;
    zcl_color_control_move_saturation_req_t             move_saturation;
    zcl_color_control_step_saturation_req_t             step_saturation;
    zcl_color_control_move_to_hue_and_saturation_req_t  move_to_hue_and_saturation;
    zcl_color_control_move_to_color_req_t               move_to_color;
    zcl_color_control_move_color_req_t                  move_color;
    zcl_color_control_step_color_req_t                  step_color;
    zcl_color_control_move_to_color_temperature_req_t   move_to_color_temperature;

} zclColorControlReq;

/**************************************************************************/
/*!

*/
/**************************************************************************/
typedef struct _zcl_color_control_attrib_list_t
{
    zclAttribute                list[ZCL_COLOR_CONTROL_ATTRIB_LIST_SZ];
    zcl_color_control_data_t    data;
} zcl_color_control_attrib_list_t;


void    zcl_color_control_init(zcl_color_control_attrib_list_t *attrib_list);
uint8_t zcl_color_control_gen_req(uint8_t *data, zcl_hdr_t *hdr, zclColorControlReq *req);
void    zcl_color_control_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr);

#endif