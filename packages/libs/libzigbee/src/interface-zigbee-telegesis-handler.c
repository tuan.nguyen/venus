#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "interface-zigbee-telegesis-handler.h"
#include "cmd_handler.h"

/*========================================================================================================
=================================== Handle Command from Zb telegesis handler =================================
==========================================================================================================*/
int fileDescripter;
int zigbeeTelegesiInitialize(char *pdevID, zigbee_notify_queue_t *notify)
{
    fileDescripter = zigbeeInitialize(pdevID, notify);
    return fileDescripter;
}
int zigbeeTelegesisHandlerSendCommand(ZigbeeTelegesisHandlerParam *pzbParam)
{
    switch (pzbParam->command)
    {
    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_NODE_ADD: //ok
    {
        /* pzbParam->param1: 0 --> close network
                ** pzbParam->param1: #0 --> open network
                */
        pzbParam->ret = zigbeeOpenCloseNetwork(pzbParam->param1);
        break;
    }

    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_NODE_REMOVE:
    {
        /* pzbParam->destAddr: destinationNodeId
                */
        pzbParam->ret = zigbeeRemoveNode(fileDescripter, pzbParam->destAddr);
        break;
    }

    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_NODE_GET_CONTROLLER_INFO: //ok
    {
        /* return full information of controller (simpleDesc) to pzbParam->controllerInfo
                */
        //pzbParam->ret = zigbeeControllerFullInfo(&pzbParam->controllerInfo);
        break;
    }

    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_NODE_SET_DEFAULT: //ok
    {
        /* none parameter
                */
        //pzbParam->ret = zigbeeTelegesisReset(&pzbParam->controllerInfo);
        pzbParam->ret = zigbeeTelegesisReset(fileDescripter); //telefesis
        break;
    }

    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_GET_BINDING_TABLE:
    {
        /*
                pzbParam->destAddr: desNodeID
                pzbParam->rbt: remoteBindingNode *rbt
                pzbParam->param1: uint8_t *numberRemoteBindingTable
                */
        pzbParam->ret = zigbeeGetRemoteBindingTable(fileDescripter, pzbParam->destAddr, pzbParam->rbt, &pzbParam->param1);
        break;
    }

    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_CREATE_BINDING_ON_REMOTE:
    {
        /*
                (uint16_t targetAddr,  uint64_t destAddr, uint16_t clustId, uint64_t srcAddr, 
                uint8_t srcEndpoint, uint8_t destEndpoint)
                */
        pzbParam->ret = appCreateBindingOnRemote(fileDescripter, pzbParam->destAddr, pzbParam->param4,
                                                 pzbParam->param3, pzbParam->param5, pzbParam->srcEndpoint, pzbParam->destEndpoint);
        break;
    }

    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_DELETE_BINDING_ON_REMOTE:
    {
        /*
                (uint16_t targetAddr,  uint64_t destAddr, uint16_t clustId, uint64_t srcAddr, 
                uint8_t srcEndpoint, uint8_t destEndpoint)
                */
        pzbParam->ret = appDeleteBindingOnRemote(fileDescripter, pzbParam->destAddr, pzbParam->param4,
                                                 pzbParam->param3, pzbParam->param5, pzbParam->srcEndpoint, pzbParam->destEndpoint);
        break;
    }
    /*
                uint16_t destAddr,      uint8_t destEndpoint,   uint8_t param1, union req;
                uint16_t destAddr,      uint8_t endPointNumber, uint8_t cmd,    zclOnOffReq req;
                */
    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_ON_OFF_REQ:
    {
        pzbParam->ret = appZclOnOffReq(fileDescripter, pzbParam->destAddr, pzbParam->destEndpoint,
                                       ZCL_STACK_PROF_HA, pzbParam->param1, pzbParam->reqZclOnOffReq);
        break;
    }
    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_DOOR_LOCK_REQ:
    {
        pzbParam->ret = appZclDoorLockReq(fileDescripter, pzbParam->destAddr, pzbParam->destEndpoint, ZCL_STACK_PROF_HA, pzbParam->param1, pzbParam->reqZclDoorLockReq);
        break;
    }
    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_LEVEL_REQ:
    {
        pzbParam->ret = appZclLevelReq(fileDescripter, pzbParam->destAddr, pzbParam->destEndpoint, ZCL_STACK_PROF_HA, pzbParam->param1, pzbParam->reqZclLevelReq);
        break;
    }
    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_IAS_ZONE_REQ:
    {
        //pzbParam->ret = appZclIasZoneReq(pzbParam->destAddr, pzbParam->destEndpoint, pzbParam->param1, pzbParam->reqZclIasZoneReq);
        break;
    }
    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_COLOR_CONTROL_REQ:
    {
        pzbParam->ret = appZclColorControlReq(fileDescripter, pzbParam->destAddr, pzbParam->destEndpoint,
                                              ZCL_STACK_PROF_HA, pzbParam->param1, pzbParam->reqZclColorControlReq);
        break;
    }
    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_ID_REQ:
    {
        pzbParam->ret = appZclIdReq(fileDescripter, pzbParam->destAddr, pzbParam->destEndpoint, ZCL_STACK_PROF_HA, pzbParam->param1, pzbParam->reqZclIdReq);
        break;
    }
    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_ZLL_COMMISSIONING_REQ:
    { /* 
            param4: destAddrEUI64
            param2: destPAN
            param3: profileId
            param1: cmd
            reqZclZllCommissioningReq: req
            int appZclZllCommissioningReq(uint64_t destAddr, uint16_t destPAN, 
            uint16_t profileId, uint8_t cmd, zclZllCommissioningReq req);
            */

        //pzbParam->ret = appZclZllCommissioningReq(pzbParam->param4, pzbParam->param2, pzbParam->param3,
        //                                          pzbParam->param1, pzbParam->reqZclZllCommissioningReq);
        break;
    }
    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_READ_ATTRIB:
    {
        /*
            pzbParam->destAddr:         destAddr
            pzbParam->destEndpoint:     endPointNumber
            pzbParam->param2:           clustId
            pzbParam->param3:           profileId
            (uint16_t)pzbParam->param4: manufacturerCode
            pzbParam->param6:  attributeId
            pzbParam->param1            number Attribute
            pzbParam->rData:  rData

            int appZclReadAttrib(uint16_t destAddr, uint8_t endPointNumber, uint16_t clustId, 
                uint16_t profileId, uint16_t manufacturerCode, uint16_t attributeId, readAttrData *rData);
            */
        pzbParam->ret = appZclReadAttrib(fileDescripter, pzbParam->destAddr, pzbParam->destEndpoint, pzbParam->param2,
                                         pzbParam->param3, (uint16_t)pzbParam->param4,
                                         (uint16_t *)pzbParam->param6, pzbParam->param1, pzbParam->rData);
        break;
    }

    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_DISCOVER_ATTRIB:
    {
        /*
            pzbParam->destAddr:         destAddr
            pzbParam->destEndpoint:     endPointNumber
            pzbParam->param2:           clustId
            pzbParam->param3:           profileId
            (uint16_t)pzbParam->param4: manufacturerCode
            pzbParam->param5:  attributeId
            pzbParam->param1:  maxAttributeId

            int appZclDiscoverAttrib(uint16_t destAddr, uint8_t endPointNumber, uint16_t clustId, 
                uint16_t profileId, uint16_t manufacturerCode, uint16_t attributeId, uint8_t maxAttributeId)
            */
        //pzbParam->ret = appZclDiscoverAttrib(pzbParam->destAddr, pzbParam->destEndpoint, pzbParam->param2,
        //                                 pzbParam->param3, (uint16_t)pzbParam->param4,
        //                                 (uint16_t)pzbParam->param5, pzbParam->param1, &pzbParam->discData);
        break;
    }

    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZCL_WRITE_ATTRIB:
    {
        /*
            pzbParam->destAddr:         destAddr
            pzbParam->destEndpoint:     endPointNumber
            pzbParam->param2:           clustId
            pzbParam->param3:           profileId
            (uint16_t)pzbParam->param4: manufacturerCode
            &pzbParam->zclWriteAttr:  *attribList
            pzbParam->param1:  attribNumber
            int appZclWriteAttrib(uint16_t destAddr, uint8_t endPointNumber, uint16_t clustId, 
                uint16_t profileId, uint16_t manufacturerCode, zclAttribute *attribList, uint8_t attribNumber);
            */
        pzbParam->ret = appZclWriteAttrib(fileDescripter, pzbParam->destAddr, pzbParam->destEndpoint, pzbParam->param2,
                                          pzbParam->param3, (uint16_t)pzbParam->param4, pzbParam->zclWriteAttr, pzbParam->param1);
        break;
    }

        /************ZLL command*************************************/

    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZLL_CHANNEL_SCAN:
    {
        /* pzbParam->param1: SCAN duration
                */
        //pzbParam->ret = zllChannelScan(pzbParam->param1);
        break;
    }

    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZLL_IDENTIFY:
    {
        /* pzbParam->destAddr: destinationNodeId
                */
        //pzbParam->ret = interPANScanRequestAndIdentify(pzbParam->param1, pzbParam->param2, pzbParam->param4);
        break;
    }

    case ZIGBEE_TELEGESIS_COMMAND_SPECIFIC_ZLL_RESET:
    {
        /* pzbParam->destAddr: destinationNodeId
                */
        //pzbParam->ret = interPANScanRequestAndReset(pzbParam->param1, pzbParam->param2, pzbParam->param4);
        break;
    }

    default:
    {
        break;
    }
    }

    return 0;
}