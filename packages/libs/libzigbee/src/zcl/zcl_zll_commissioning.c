#include "zcl_zll_commissioning.h"

/**************************************************************************/
/*!
        Init the on/off cluster.
*/
/**************************************************************************/
void zcl_zll_commisioning_init(zcl_zll_commissioning_attrib_list_t *attrib_list)
{
   
    zcl_set_attrib(&attrib_list->list[0], ZCL_END_MARKER, 0, 0, NULL);
}


/**************************************************************************/
/*!
        Handle the incoming frames targeted to the on/off cluster. This function
        processes the on/off cluster commands and sets the status to on, off, or
        toggles the value. The action handler will also handle any user defined
        actions that is related to the status.
*/
/**************************************************************************/
void zcl_zll_commissioning_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t option, uint64_t addr, uint16_t profile, zcl_hdr_t *hdr)
{
    uint8_t  *data_ptr;
    zll_commissioning_cmd_rsp_t zll_commissioning_response;
    
    memset((char*)&zll_commissioning_response,0,sizeof(zll_commissioning_cmd_rsp_t));

    zll_commissioning_response.zll_sourceDesc.profile=profile;
    zll_commissioning_response.zll_sourceDesc.option=option;

    if (option==OPTION_ADDRESS_64_BIT)
    {
        zll_commissioning_response.zll_sourceDesc.long_addr=addr;
    }else
    {
        zll_commissioning_response.zll_sourceDesc.short_addr=(uint16_t)addr;
    }

    data_ptr = hdr->payload;
    switch (hdr->cmd)
    {
        case ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_SCAN_RESPONSE:
        {
            zll_commissioning_response.cmd=ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_SCAN_RESPONSE;
            zll_commissioning_response.scan_response.inter_pan_transaction_id=*(uint32_t *)data_ptr;
            data_ptr += sizeof(uint32_t);
            zll_commissioning_response.scan_response.rssi_correction=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            zll_commissioning_response.scan_response.zigbee_information=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            zll_commissioning_response.scan_response.zll_information=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            zll_commissioning_response.scan_response.key_bitmask=*(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            zll_commissioning_response.scan_response.response_identifier=*(uint32_t *)data_ptr;
            data_ptr += sizeof(uint32_t);
            zll_commissioning_response.scan_response.extended_pan_identifier=*(uint64_t *)data_ptr;
            data_ptr += sizeof(uint64_t);
            zll_commissioning_response.scan_response.network_update_identifier=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            zll_commissioning_response.scan_response.logical_channel=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            zll_commissioning_response.scan_response.pan_identifier=*(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            zll_commissioning_response.scan_response.network_address=*(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            zll_commissioning_response.scan_response.number_of_sub_devices=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            zll_commissioning_response.scan_response.total_group_identifiers=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            if (zll_commissioning_response.scan_response.number_of_sub_devices==1)
            {
                zll_commissioning_response.scan_response.endpoint_identifier=*(uint8_t *)data_ptr;
                data_ptr += sizeof(uint8_t);
                zll_commissioning_response.scan_response.profile_identifier=*(uint16_t *)data_ptr;
                data_ptr += sizeof(uint16_t);
                zll_commissioning_response.scan_response.device_identifier=*(uint16_t *)data_ptr;
                data_ptr += sizeof(uint16_t);
                zll_commissioning_response.scan_response.version=*(uint8_t *)data_ptr;
                data_ptr += sizeof(uint8_t);
                zll_commissioning_response.scan_response.group_identifier_count=*(uint8_t *)data_ptr;
                data_ptr += sizeof(uint8_t);
            }
           
            break;
        }
        
        default:
            printf("ZCL_ZLL_COMMISSIONING_CMD_UNKNOWN\n");
            *resp_len=0;
            return;
    }

    *resp_len=0;
    resp[0]=ZLL_COMMISSIONING_RESPONSE_NOTIFY;
    *resp_len=1;
    memcpy(&resp[1],(uint8_t*)&zll_commissioning_response,sizeof(zll_commissioning_cmd_rsp_t));
    *resp_len=*resp_len+sizeof(zll_commissioning_cmd_rsp_t);
    return;
}



uint8_t zcl_zll_commissioning_gen_req(uint8_t *data, zcl_hdr_t *hdr, zclZllCommissioningReq *req)
{
    uint8_t len, *data_ptr;

    data_ptr = data;

    // gen the header
    len = zcl_gen_hdr(data_ptr, hdr);
    data_ptr += len;
    printf("zcl_zll_commissioning_gen_req\n");

    switch (hdr->cmd)
    {
        case ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_SCAN_REQUEST:
            printf("ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_SCAN_REQUEST\n");
            *(uint32_t *)data_ptr = req->scan_request.inter_pan_transaction_id;
            data_ptr += sizeof(uint32_t);
            *data_ptr++ = req->scan_request.zigbee_information;
            *data_ptr++ = req->scan_request.zll_information;
            break;
        case ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_DEVICE_IDENTIFY_REQUEST:
            *(uint32_t *)data_ptr = req->identify_request.inter_pan_transaction_id;
            data_ptr += sizeof(uint32_t);
            *(uint16_t *)data_ptr = req->identify_request.identify_duration;
            data_ptr += sizeof(uint16_t);
            break;
        case ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_RESET_TO_FACTORY_NEW_REQUEST:
            *(uint32_t *)data_ptr = req->reset_to_factory_new_request.inter_pan_transaction_id;
            data_ptr += sizeof(uint32_t);
            break;
    }

    return data_ptr - data;
}