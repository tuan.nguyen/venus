#include "zcl_shade_configuration.h"

void zcl_shade_configuration_init(zcl_shade_configuration_attrib_list_t *attrib_list)
{
    // init the data values first
    memset(attrib_list, 0, sizeof(zcl_shade_configuration_attrib_list_t));
   
    // init the attribute fields
    zcl_set_attrib(&attrib_list->list[0],     ZCL_SHADE_CONFIGURATION_ATTRIB_PHYSICAL_CLOSED_LIMIT,         ZCL_TYPE_U16,            ZCL_ACCESS_READ_ONLY,  &attrib_list->data.physical_closed_limit);
    zcl_set_attrib(&attrib_list->list[1],     ZCL_SHADE_CONFIGURATION_ATTRIB_MOTOR_STEP_SIZE,               ZCL_TYPE_U8,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.motor_step_size);
    zcl_set_attrib(&attrib_list->list[2],     ZCL_SHADE_CONFIGURATION_ATTRIB_STATUS,                        ZCL_TYPE_8BITMAP,        ZCL_ACCESS_READ_WRITE, &attrib_list->data.status);
    zcl_set_attrib(&attrib_list->list[3],     ZCL_SHADE_CONFIGURATION_ATTRIB_CLOSED_LIMIT,                  ZCL_TYPE_U16,            ZCL_ACCESS_READ_WRITE, &attrib_list->data.closed_limit);
    zcl_set_attrib(&attrib_list->list[4],     ZCL_SHADE_CONFIGURATION_ATTRIB_MODE,                          ZCL_TYPE_8BIT_ENUM,      ZCL_ACCESS_READ_WRITE, &attrib_list->data.mode);

    //attrib end marker    
    zcl_set_attrib(&attrib_list->list[5], ZCL_END_MARKER, 0, 0, NULL);
   
}

void zcl_shade_configuration_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
    
}