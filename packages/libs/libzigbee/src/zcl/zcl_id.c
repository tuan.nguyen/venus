#include "zcl_id.h"

void zcl_id_init(zcl_id_attrib_list_t *attrib_list)
{
      // init the data values first
    attrib_list->data.id_time = 0;

    // init the attribute fields
    zcl_set_attrib(&attrib_list->list[0], ZCL_ID_ATTRIB, ZCL_TYPE_U16, ZCL_ACCESS_READ_WRITE, &attrib_list->data.id_time);
    zcl_set_attrib(&attrib_list->list[1], ZCL_END_MARKER, 0, 0, NULL);

}

void zcl_id_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
	uint8_t  *data_ptr;
    id_query_response id_query_rsp;
    memset((char*)&id_query_rsp,0,sizeof(id_query_response));

    id_query_rsp.devSourceDesc.destAddr=addr;
    id_query_rsp.devSourceDesc.endpoint=endpoint;
    id_query_rsp.devSourceDesc.profileId=profile;
    id_query_rsp.devSourceDesc.clusterId=clust->clust_id;
    data_ptr = hdr->payload;

    switch (hdr->cmd)
    {
        case ZCL_ID_CMD_ID_QUERY_RESP:
        {   
            id_query_rsp.timeout=*(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            break;  
        }

        default:
            *resp_len=0;
            return;
    }

    *resp_len=0;
    resp[0]=ID_QUERY_RESPONSE_NOTIFY;
    *resp_len=1;
    memcpy(&resp[1],(uint8_t*)&id_query_rsp,sizeof(id_query_response));
    *resp_len=*resp_len+sizeof(id_query_response);

}

uint8_t zcl_id_gen_req(uint8_t *data, zcl_hdr_t *hdr, zclIdReq *req)
{
    uint8_t len, *data_ptr;

    len = zcl_gen_hdr(data, hdr);
    data_ptr = data + len;

    if (hdr->cmd == ZCL_ID_CMD_ID)
    {
        *(uint16_t *)data_ptr = req->timeout;
        data_ptr += sizeof(uint16_t);
    }
    return data_ptr - data;
}
