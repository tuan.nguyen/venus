#include "zcl_grp.h"

void zcl_grp_init(zcl_grp_attrib_list_t *attrib_list)
{


    // init the attribute fields
    zcl_set_attrib(&attrib_list->list[0], ZCL_GRP_ATTRIB_NAME_SUPPORT, ZCL_TYPE_8BITMAP, ZCL_ACCESS_READ_ONLY, &attrib_list->data.name_supp);
    zcl_set_attrib(&attrib_list->list[1], ZCL_END_MARKER, 0, 0, NULL);

}

void zcl_grp_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
	uint8_t  *data_ptr;
    grp_cmd_response  grp_cmd_rsp;
    memset((char*)&grp_cmd_rsp,0,sizeof(grp_cmd_response));

    grp_cmd_rsp.devSourceDesc.destAddr=addr;
    grp_cmd_rsp.devSourceDesc.endpoint=endpoint;
    grp_cmd_rsp.devSourceDesc.profileId=profile;
    grp_cmd_rsp.devSourceDesc.clusterId=clust->clust_id;
    data_ptr = hdr->payload;

    switch (hdr->cmd)
    {
        case ZCL_GRP_CMD_ADD_GRP_RESP:
        {   
            grp_cmd_rsp.cmd=ZCL_GRP_CMD_ADD_GRP_RESP;
            grp_cmd_rsp.add_group_rsp.status=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            grp_cmd_rsp.add_group_rsp.group_id=*(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            break;  
        }

        case ZCL_GRP_CMD_VIEW_GRP_RESP:
        {   
            grp_cmd_rsp.cmd=ZCL_GRP_CMD_VIEW_GRP_RESP;
            grp_cmd_rsp.view_group_rsp.status=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            grp_cmd_rsp.view_group_rsp.group_id=*(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            uint8_t len=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            if ((len>0)&&(len<=64))
            {
                memcpy(&grp_cmd_rsp.view_group_rsp.group_name,data_ptr,len);
                data_ptr +=len;
            }
            break;  
        }

        case ZCL_GRP_CMD_GET_MEMB_RESP:
        {   
            grp_cmd_rsp.cmd=ZCL_GRP_CMD_GET_MEMB_RESP;
            grp_cmd_rsp.get_group_membership_rsp.capacity=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            grp_cmd_rsp.get_group_membership_rsp.group_count=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            
            if ((grp_cmd_rsp.get_group_membership_rsp.group_count>0)&&(grp_cmd_rsp.get_group_membership_rsp.group_count<=32))
            {
                uint8_t i;
                for (i=0;i<grp_cmd_rsp.get_group_membership_rsp.group_count;i++)
                {
                    grp_cmd_rsp.get_group_membership_rsp.group_list[i]=*(uint16_t *)data_ptr;
                    data_ptr += sizeof(uint16_t);
                }
            }
            break;  
        }

        case ZCL_GRP_CMD_REM_GRP_RESP:
        {   
            grp_cmd_rsp.cmd=ZCL_GRP_CMD_REM_GRP_RESP;
            grp_cmd_rsp.remove_group_rsp.status=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            grp_cmd_rsp.remove_group_rsp.group_id=*(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            break;  
        }

        default:
            *resp_len=0;
            return;
    }

    *resp_len=0;
    resp[0]=GRP_RESPONSE_NOTIFY;
    *resp_len=1;
    memcpy(&resp[1],(uint8_t*)&grp_cmd_rsp,sizeof(grp_cmd_response));
    *resp_len=*resp_len+sizeof(grp_cmd_response);

}

uint8_t zcl_grp_gen_req(uint8_t *data, zcl_hdr_t *hdr, zcl_grp_req_t *req)
{
    uint8_t len, *data_ptr;

    len = zcl_gen_hdr(data, hdr);
    data_ptr = data + len;

    switch (hdr->cmd)
    {
        case ZCL_GRP_CMD_ADD_GRP:
            *(uint16_t *)data_ptr = req->add_group.group_id;
            data_ptr += sizeof(uint16_t);
            if ((req->add_group.len_name>0)&&(req->add_group.len_name<=64))
            {
                *data_ptr++ = req->add_group.len_name;
                memcpy(data_ptr,req->add_group.group_name,req->add_group.len_name);
                data_ptr += req->add_group.len_name;
            }
            break;
        
        case ZCL_GRP_CMD_VIEW_GRP:
            *(uint16_t *)data_ptr = req->view_group.group_id;
            data_ptr += sizeof(uint16_t);
            break;

        case ZCL_GRP_CMD_GET_MEMB:
            *data_ptr++ = req->get_group_membership.group_count;
            if ((req->get_group_membership.group_count>0) && (req->get_group_membership.group_count<=32))
            {
                uint8_t i;
                for (i=0;i<req->get_group_membership.group_count;i++)
                {
                    *(uint16_t *)data_ptr = req->get_group_membership.group_list[i];
                    data_ptr += sizeof(uint16_t);
                }
            }
            break;

        case ZCL_GRP_CMD_REM_GRP:
            *(uint16_t *)data_ptr = req->remove_group.group_id;
            data_ptr += sizeof(uint16_t);
            break;

        case ZCL_GRP_CMD_ADD_IF_ID:
            *(uint16_t *)data_ptr = req->add_group_if_identifying.group_id;
            data_ptr += sizeof(uint16_t);
            if ((req->add_group_if_identifying.len_name>0)&&(req->add_group_if_identifying.len_name<=64))
            {
                *data_ptr++ = req->add_group_if_identifying.len_name;
                memcpy(data_ptr,req->add_group_if_identifying.group_name,req->add_group_if_identifying.len_name);
                data_ptr += req->add_group_if_identifying.len_name;
            }
            break;

        default:
            break;
    }
    // return the len
    return data_ptr - data;
}
