#include "zcl_illuminance_level_sensing.h"

void zcl_illuminance_level_sensing_init(zcl_illuminance_level_sensing_attrib_list_t *attrib_list)
{
    // init the data values first
    memset(attrib_list, 0, sizeof(zcl_illuminance_level_sensing_attrib_list_t));
   
    // init the attribute fields
    zcl_set_attrib(&attrib_list->list[0],     ZCL_ILLUMINANCE_LEVEL_SENSING_ATTRIB_LEVEL_STATUS,             ZCL_TYPE_8BIT_ENUM,       ZCL_ACCESS_READ_ONLY,   &attrib_list->data.level_status);
    zcl_set_attrib(&attrib_list->list[1],     ZCL_ILLUMINANCE_LEVEL_SENSING_ATTRIB_LIGHT_SENSOR_TYPE,        ZCL_TYPE_8BIT_ENUM,       ZCL_ACCESS_READ_ONLY,   &attrib_list->data.light_sensor_type);
    zcl_set_attrib(&attrib_list->list[2],     ZCL_ILLUMINANCE_LEVEL_SENSING_ATTRIB_ILLUMINANCE_TARGET_LEVEL, ZCL_TYPE_U16,             ZCL_ACCESS_READ_WRITE,  &attrib_list->data.illuminance_target_level);


    //attrib end marker    
    zcl_set_attrib(&attrib_list->list[4], ZCL_END_MARKER, 0, 0, NULL);
   
}

void zcl_illuminance_level_sensing_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
	
}