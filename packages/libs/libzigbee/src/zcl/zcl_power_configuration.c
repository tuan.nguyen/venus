#include "zcl_power_configuration.h"

#define BATTERY_PERCENTAGE_THRESHOLD_X10 25 //2.5v * 10
#define MAX_VOLTAGE_PIN_CR2_X10 30          //3.0v * 10
#define MAX_BATTERY_PERCENTAGE 100

void zcl_power_configuration_init(zcl_power_configuration_attrib_list_t *attrib_list)
{
    // init the data values first
    memset(attrib_list, 0, sizeof(zcl_power_configuration_attrib_list_t));

    // init the attribute fields
    zcl_set_attrib(&attrib_list->list[0], ZCL_POWER_CONFIGURATION_MAINS_VOLTAGE, ZCL_TYPE_U16, ZCL_ACCESS_READ_ONLY, &attrib_list->data.mains_voltage);
    zcl_set_attrib(&attrib_list->list[1], ZCL_POWER_CONFIGURATION_MAINS_FREQUENCY, ZCL_TYPE_U8, ZCL_ACCESS_READ_ONLY, &attrib_list->data.mains_frequency);
    zcl_set_attrib(&attrib_list->list[2], ZCL_POWER_CONFIGURATION_MAINS_ALARM_MASK, ZCL_TYPE_8BITMAP, ZCL_ACCESS_READ_WRITE, &attrib_list->data.mains_alarm_mask);
    zcl_set_attrib(&attrib_list->list[3], ZCL_POWER_CONFIGURATION_MAINS_VOLTAGE_MIN_THRESHOLD, ZCL_TYPE_U16, ZCL_ACCESS_READ_WRITE, &attrib_list->data.mains_voltage_min_threshold);
    zcl_set_attrib(&attrib_list->list[4], ZCL_POWER_CONFIGURATION_MAINS_VOLTAGE_MAX_THRESHOLD, ZCL_TYPE_U16, ZCL_ACCESS_READ_WRITE, &attrib_list->data.mains_voltage_max_threshold);
    zcl_set_attrib(&attrib_list->list[5], ZCL_POWER_CONFIGURATION_MAINS_VOLTAGE_DWELL_TRIP_POINT, ZCL_TYPE_U16, ZCL_ACCESS_READ_WRITE, &attrib_list->data.mains_voltage_dwell_trip_point);
    zcl_set_attrib(&attrib_list->list[6], ZCL_POWER_CONFIGURATION_BATTERY_VOLTAGE, ZCL_TYPE_U8, ZCL_ACCESS_READ_ONLY, &attrib_list->data.battery_voltage);
    zcl_set_attrib(&attrib_list->list[7], ZCL_POWER_CONFIGURATION_BATTERY_PERCENTAGE_REMAINING, ZCL_TYPE_U8, ZCL_ACCESS_READ_ONLY, &attrib_list->data.battery_percentage_remaining);
    //zcl_set_attrib(&attrib_list->list[7],     ZCL_POWER_CONFIGURATION_BATTERY_MANUFACTURER,            ZCL_TYPE_U16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.tolerance);
    zcl_set_attrib(&attrib_list->list[9], ZCL_POWER_CONFIGURATION_BATTERY_SIZE, ZCL_TYPE_8BIT_ENUM, ZCL_ACCESS_READ_WRITE, &attrib_list->data.battery_size);
    zcl_set_attrib(&attrib_list->list[10], ZCL_POWER_CONFIGURATION_BATTERY_AHR_RATING, ZCL_TYPE_U16, ZCL_ACCESS_READ_WRITE, &attrib_list->data.battery_ahr_rating);
    zcl_set_attrib(&attrib_list->list[11], ZCL_POWER_CONFIGURATION_BATTERY_QUANTITY, ZCL_TYPE_U8, ZCL_ACCESS_READ_WRITE, &attrib_list->data.battery_quantity);
    zcl_set_attrib(&attrib_list->list[12], ZCL_POWER_CONFIGURATION_BATTERY_RATED_VOLTAGE, ZCL_TYPE_U8, ZCL_ACCESS_READ_WRITE, &attrib_list->data.battery_rated_voltage);
    zcl_set_attrib(&attrib_list->list[13], ZCL_POWER_CONFIGURATION_BATTERY_ALARM_MASK, ZCL_TYPE_8BITMAP, ZCL_ACCESS_READ_WRITE, &attrib_list->data.battery_alarm_mask);
    zcl_set_attrib(&attrib_list->list[14], ZCL_POWER_CONFIGURATION_BATTERY_VOLTAGE_MIN_THRESHOLD, ZCL_TYPE_U8, ZCL_ACCESS_READ_WRITE, &attrib_list->data.battery_voltage_min_threshold);

    //attrib end marker
    zcl_set_attrib(&attrib_list->list[14], ZCL_END_MARKER, 0, 0, NULL);
}

void zcl_power_configuration_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
    powerConfiguration_t powerConfigurationReport;

    memset((char *)&powerConfigurationReport, 0, sizeof(powerConfiguration_t));

    uint8_t *dataPointer, status;
    uint16_t attrib_id;
    dataPointer = hdr->payload;
    attrib_id = *(uint16_t *)dataPointer;
    dataPointer += sizeof(uint16_t);
    status = *dataPointer++;

    if (status == ZCL_STATUS_SUCCESS)
    {
        powerConfigurationReport.addr = addr;
        powerConfigurationReport.type = *dataPointer++;
        if (ZCL_POWER_CONFIGURATION_BATTERY_PERCENTAGE_REMAINING == attrib_id)
            powerConfigurationReport.battery_level = *dataPointer++;
        else if (ZCL_POWER_CONFIGURATION_BATTERY_VOLTAGE == attrib_id)
        {
            uint8_t data = *dataPointer++;
            powerConfigurationReport.battery_level = (data - BATTERY_PERCENTAGE_THRESHOLD_X10) *
                                                     MAX_BATTERY_PERCENTAGE / (MAX_VOLTAGE_PIN_CR2_X10 - BATTERY_PERCENTAGE_THRESHOLD_X10);
        }
    }

    *resp_len = 0;
    resp[0] = POWER_CONFIGURATION_NOTIFY;
    *resp_len = 1;
    memcpy(&resp[1], (uint8_t *)&powerConfigurationReport, sizeof(powerConfiguration_t));
    *resp_len = *resp_len + sizeof(powerConfiguration_t);
}