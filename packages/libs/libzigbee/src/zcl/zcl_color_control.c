#include "zcl_color_control.h"

/**************************************************************************/
/*!
        Init the on/off cluster.
*/
/**************************************************************************/
void zcl_color_control_init(zcl_color_control_attrib_list_t *attrib_list)
{
    // init the data
    memset(&attrib_list->data, 0, sizeof(zcl_color_control_data_t));


    // init the attribs
    zcl_set_attrib(&attrib_list->list[0], ZCL_COLOR_CONTROL_ATTRIB_CURRENT_HUE,             ZCL_TYPE_U8,         ZCL_ACCESS_READ_ONLY, &attrib_list->data.current_hue);
    zcl_set_attrib(&attrib_list->list[1], ZCL_COLOR_CONTROL_ATTRIB_CURRENT_SATURATION,      ZCL_TYPE_U8,         ZCL_ACCESS_READ_ONLY, &attrib_list->data.current_saturation);
    zcl_set_attrib(&attrib_list->list[2], ZCL_COLOR_CONTROL_ATTRIB_REMAINING_TIME,          ZCL_TYPE_U16,        ZCL_ACCESS_READ_ONLY, &attrib_list->data.remaining_time);
    zcl_set_attrib(&attrib_list->list[3], ZCL_COLOR_CONTROL_ATTRIB_CURRENT_X,               ZCL_TYPE_U16,        ZCL_ACCESS_READ_ONLY, &attrib_list->data.current_x);
    zcl_set_attrib(&attrib_list->list[4], ZCL_COLOR_CONTROL_ATTRIB_CURRENT_Y,               ZCL_TYPE_U16,        ZCL_ACCESS_READ_ONLY, &attrib_list->data.current_y);
    zcl_set_attrib(&attrib_list->list[5], ZCL_COLOR_CONTROL_ATTRIB_DRIFT_COMPENSATION,      ZCL_TYPE_8BIT_ENUM,  ZCL_ACCESS_READ_ONLY, &attrib_list->data.drift_compensation);
    zcl_set_attrib(&attrib_list->list[7], ZCL_COLOR_CONTROL_ATTRIB_COLOR_TEMPERATURE,       ZCL_TYPE_U16,        ZCL_ACCESS_READ_ONLY, &attrib_list->data.color_temperature);
    zcl_set_attrib(&attrib_list->list[8], ZCL_COLOR_CONTROL_ATTRIB_COLOR_MODE,              ZCL_TYPE_8BIT_ENUM,  ZCL_ACCESS_READ_ONLY, &attrib_list->data.color_mode);
   
    zcl_set_attrib(&attrib_list->list[9], ZCL_END_MARKER, 0, 0, NULL);
}


/**************************************************************************/
/*!
        Handle the incoming frames targeted to the on/off cluster. This function
        processes the on/off cluster commands and sets the status to on, off, or
        toggles the value. The action handler will also handle any user defined
        actions that is related to the status.
*/
/**************************************************************************/
void zcl_color_control_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
    

}



uint8_t zcl_color_control_gen_req(uint8_t *data, zcl_hdr_t *hdr, zclColorControlReq *req)
{
    uint8_t len, *data_ptr;

    data_ptr = data;

    // gen the header
    len = zcl_gen_hdr(data_ptr, hdr);
    data_ptr += len;

    switch (hdr->cmd)
    {
        case ZCL_COLOR_CONTROL_CMD_MOVE_TO_HUE:
            *data_ptr++ = req->move_to_hue.hue;
            *data_ptr++ = req->move_to_hue.direction;
            *(uint16_t *)data_ptr = req->move_to_hue.transition_time;
            data_ptr += sizeof(uint16_t);
            break;
        case ZCL_COLOR_CONTROL_CMD_MOVE_HUE:
            *data_ptr++ = req->move_hue.move_mode;
            *data_ptr++ = req->move_hue.rate;
            break;
        case ZCL_COLOR_CONTROL_CMD_STEP_HUE:
            *data_ptr++ = req->step_hue.step_mode;
            *data_ptr++ = req->step_hue.step_size;
            *data_ptr++ = req->step_hue.transition_time;
            break;
        case ZCL_COLOR_CONTROL_CMD_MOVE_TO_SATURATION:
            *data_ptr++ = req->move_to_saturation.saturation;
            *(uint16_t *)data_ptr = req->move_to_saturation.transition_time;
            data_ptr += sizeof(uint16_t);
            break;
        case ZCL_COLOR_CONTROL_CMD_MOVE_SATURATION:
            *data_ptr++ = req->move_saturation.move_mode;
            *data_ptr++ = req->move_saturation.rate;
            break;
        case ZCL_COLOR_CONTROL_CMD_STEP_SATURATION:
            *data_ptr++ = req->step_saturation.step_mode;
            *data_ptr++ = req->step_saturation.step_size;
            *data_ptr++ = req->step_saturation.transition_time;
            break;

        case ZCL_COLOR_CONTROL_CMD_MOVE_TO_HUE_AND_SATURATION:
            *data_ptr++ = req->move_to_hue_and_saturation.hue;
            *data_ptr++ = req->move_to_hue_and_saturation.saturation;
            *(uint16_t *)data_ptr = req->move_to_hue_and_saturation.transition_time;
            data_ptr += sizeof(uint16_t);
            break;

        case ZCL_COLOR_CONTROL_CMD_MOVE_TO_COLOR:
            *(uint16_t *)data_ptr = req->move_to_color.color_x;
            data_ptr += sizeof(uint16_t);
            *(uint16_t *)data_ptr = req->move_to_color.color_y;
            data_ptr += sizeof(uint16_t);
            *(uint16_t *)data_ptr = req->move_to_color.transition_time;
            data_ptr += sizeof(uint16_t);
            break;

        case ZCL_COLOR_CONTROL_CMD_MOVE_COLOR:
            *(int16_t *)data_ptr = req->move_color.rate_x;
            data_ptr += sizeof(int16_t);
            *(int16_t *)data_ptr = req->move_color.rate_y;
            data_ptr += sizeof(int16_t);
            break;

        case ZCL_COLOR_CONTROL_CMD_STEP_COLOR:
            *(int16_t *)data_ptr = req->step_color.step_x;
            data_ptr += sizeof(int16_t);
            *(int16_t *)data_ptr = req->step_color.step_y;
            data_ptr += sizeof(int16_t);
            *(uint16_t *)data_ptr = req->step_color.transition_time;
            data_ptr += sizeof(uint16_t);
            break;
        case ZCL_COLOR_CONTROL_CMD_MOVE_TO_COLOR_TEMPERATURE:
            *(uint16_t *)data_ptr = req->move_to_color_temperature.color_temperature;
            data_ptr += sizeof(uint16_t);
            *(uint16_t *)data_ptr = req->move_to_color_temperature.transition_time;
            data_ptr += sizeof(uint16_t);
            break;

        default:
            break;
    }
    // return the len
    return data_ptr - data;
}