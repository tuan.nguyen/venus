#include "zcl_door_lock.h"
#include "cmd_handler.h"

//for door lock state
#define DOOR_OPEN_VALUE 0x02
#define DOOR_CLOSE_VALUE 0x01

#define MINIMUM_LENGTH_READ_ATTRI_RESPONSE 0x03 //attributeId (2 byte), status (1 byte), type (1 byte) data (n byte)

/**************************************************************************/
/*!
        Init the on/off cluster.
*/
/**************************************************************************/
void zcl_door_lock_init(zcl_door_lock_attrib_list_t *attrib_list)
{
    // init the data
    attrib_list->data.lock_state = 0;
    attrib_list->data.lock_type = 0;
    attrib_list->data.actuator_enabled = 0;
    attrib_list->data.door_state = 0;
    attrib_list->data.door_open_events = 0;
    attrib_list->data.door_period = 0;

    // init the attribs
    zcl_set_attrib(&attrib_list->list[0], ZCL_DOOR_LOCK_ATTRIB_LOCK_STATE, ZCL_TYPE_8BIT_ENUM, ZCL_ACCESS_READ_ONLY, &attrib_list->data.lock_state);
    zcl_set_attrib(&attrib_list->list[1], ZCL_DOOR_LOCK_ATTRIB_LOCK_TYPE, ZCL_TYPE_8BIT, ZCL_ACCESS_READ_ONLY, &attrib_list->data.lock_type);
    zcl_set_attrib(&attrib_list->list[2], ZCL_DOOR_LOCK_ATTRIB_ACTUATOR_ENABLED, ZCL_TYPE_BOOL, ZCL_ACCESS_READ_ONLY, &attrib_list->data.actuator_enabled);
    zcl_set_attrib(&attrib_list->list[3], ZCL_DOOR_LOCK_ATTRIB_DOOR_STATE, ZCL_TYPE_8BIT, ZCL_ACCESS_READ_ONLY, &attrib_list->data.door_state);
    zcl_set_attrib(&attrib_list->list[4], ZCL_DOOR_LOCK_ATTRIB_DOOR_OPEN_EVENTS, ZCL_TYPE_U32, ZCL_ACCESS_READ_WRITE, &attrib_list->data.door_open_events);
    zcl_set_attrib(&attrib_list->list[5], ZCL_DOOR_LOCK_ATTRIB_DOOR_CLOSED_EVENTS, ZCL_TYPE_U32, ZCL_ACCESS_READ_WRITE, &attrib_list->data.door_close_events);
    zcl_set_attrib(&attrib_list->list[6], ZCL_DOOR_LOCK_ATTRIB_OPEN_PERIOD, ZCL_TYPE_U16, ZCL_ACCESS_READ_WRITE, &attrib_list->data.door_period);

    zcl_set_attrib(&attrib_list->list[7], ZCL_END_MARKER, 0, 0, NULL);
}

/**************************************************************************/
/*!
        Handle the incoming frames targeted to the on/off cluster. This function
        processes the on/off cluster commands and sets the status to on, off, or
        toggles the value. The action handler will also handle any user defined
        actions that is related to the status.
*/
/**************************************************************************/
void zcl_door_lock_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
    uint8_t *data_ptr;
    doorLockCmdResponse door_lock_cmd_rsp;

    memset((char *)&door_lock_cmd_rsp, 0, sizeof(doorLockCmdResponse));

    door_lock_cmd_rsp.devSourceDesc.destAddr = addr;
    door_lock_cmd_rsp.devSourceDesc.endpoint = endpoint;
    door_lock_cmd_rsp.devSourceDesc.profileId = profile;
    door_lock_cmd_rsp.devSourceDesc.clusterId = clust->clust_id;
    data_ptr = hdr->payload;
    if (hdr->frm_ctrl.frm_type == ZCL_FRM_TYPE_GENERAL)
    {
        uint8_t *dataPointer, status;
        //uint16_t attrib_id;
        dataPointer = hdr->payload;
        //attrib_id = *(uint16_t *)dataPointer;
        dataPointer += sizeof(uint16_t);
        status = *dataPointer++;

        if (status == ZCL_STATUS_SUCCESS)
        {
            door_lock_cmd_rsp.cmd = ZCL_DOOR_LOCK_CMD_READ_ATTRIBUTE_RESPONSE;
            door_lock_cmd_rsp.doorLockAttriResponse.addr = addr;
            door_lock_cmd_rsp.doorLockAttriResponse.type = *dataPointer++;
            if(DOOR_CLOSE_VALUE == *(uint8_t *)dataPointer)
            {
                door_lock_cmd_rsp.doorLockAttriResponse.lockStatus = DOOR_CLOSE;
            }
            else if(DOOR_OPEN_VALUE == *(uint8_t *)dataPointer)
            {
                door_lock_cmd_rsp.doorLockAttriResponse.lockStatus = DOOR_OPEN;
            }
        }
    }
    else
    {
        switch (hdr->cmd)
        {
        case ZCL_DOOR_LOCK_CMD_SET_PIN_CODE_RESPONSE:
        {
            printf("ZCL_DOOR_LOCK_CMD_SET_PIN_CODE_RESPONSE\n");
            door_lock_cmd_rsp.cmd = ZCL_DOOR_LOCK_CMD_SET_PIN_CODE_RESPONSE;
            door_lock_cmd_rsp.set_pin_code_rsp.status = *(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            if (cmdRegisterList[hdr->seq_num].nodeId == addr)
            {
                door_lock_cmd_rsp.set_pin_code_rsp.user_id = (uint16_t)cmdRegisterList[hdr->seq_num].data0;
                door_lock_cmd_rsp.set_pin_code_rsp.pin_length = cmdRegisterList[hdr->seq_num].arrayLength;
                memcpy(door_lock_cmd_rsp.set_pin_code_rsp.p_pin, cmdRegisterList[hdr->seq_num].arrayData, cmdRegisterList[hdr->seq_num].arrayLength);
                //cmdRegisterList[hdr->seq_num].nodeId=0;
                memset((uint8_t *)&cmdRegisterList[hdr->seq_num], 0x00, sizeof(cmdRegister));
            }
            break;
        }
        case ZCL_DOOR_LOCK_CMD_GET_PIN_CODE_RESPONSE:
        {
            printf("ZCL_DOOR_LOCK_CMD_GET_PIN_CODE_RESPONSE\n");
            door_lock_cmd_rsp.cmd = ZCL_DOOR_LOCK_CMD_GET_PIN_CODE_RESPONSE;
            door_lock_cmd_rsp.get_pin_code_rsp.user_id = *(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            door_lock_cmd_rsp.get_pin_code_rsp.user_status = *(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            door_lock_cmd_rsp.get_pin_code_rsp.user_type = *(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            door_lock_cmd_rsp.get_pin_code_rsp.pin_length = *(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            memcpy((uint8_t *)&door_lock_cmd_rsp.get_pin_code_rsp.p_pin, data_ptr, door_lock_cmd_rsp.get_pin_code_rsp.pin_length);
            data_ptr += door_lock_cmd_rsp.get_pin_code_rsp.pin_length;
            break;
        }
        case ZCL_DOOR_LOCK_CMD_CLEAR_PIN_CODE_RESPONSE:
            printf("ZCL_DOOR_LOCK_CMD_CLEAR_PIN_CODE_RESPONSE\n");
            door_lock_cmd_rsp.cmd = ZCL_DOOR_LOCK_CMD_CLEAR_PIN_CODE_RESPONSE;
            door_lock_cmd_rsp.clear_pin_code_rsp.status = *(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            if (cmdRegisterList[hdr->seq_num].nodeId == addr)
            {
                door_lock_cmd_rsp.clear_pin_code_rsp.user_id = (uint16_t)cmdRegisterList[hdr->seq_num].data0;
                door_lock_cmd_rsp.clear_pin_code_rsp.pin_length = cmdRegisterList[hdr->seq_num].arrayLength;
                memcpy(door_lock_cmd_rsp.clear_pin_code_rsp.p_pin, cmdRegisterList[hdr->seq_num].arrayData, cmdRegisterList[hdr->seq_num].arrayLength);
                //cmdRegisterList[hdr->seq_num].nodeId=0;
                memset((uint8_t *)&cmdRegisterList[hdr->seq_num], 0x00, sizeof(cmdRegister));
            }

            break;
        case ZCL_DOOR_LOCK_CMD_CLEAR_ALL_PIN_CODE_RESPONSE:
        {
            printf("ZCL_DOOR_LOCK_CMD_CLEAR_ALL_PIN_CODE_RESPONSE\n");
            door_lock_cmd_rsp.cmd = ZCL_DOOR_LOCK_CMD_CLEAR_ALL_PIN_CODE_RESPONSE;
            door_lock_cmd_rsp.status = *(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            break;
        }
        case ZCL_DOOR_LOCK_CMD_SET_USER_STATUS_RESPONSE:
        {
            printf("ZCL_DOOR_LOCK_CMD_SET_USER_STATUS_RESPONSE\n");
            door_lock_cmd_rsp.cmd = ZCL_DOOR_LOCK_CMD_SET_USER_STATUS_RESPONSE;
            door_lock_cmd_rsp.status = *(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            break;
        }
        case ZCL_DOOR_LOCK_CMD_GET_USER_STATUS_RESPONSE:
        {
            printf("ZCL_DOOR_LOCK_CMD_GET_USER_STATUS_RESPONSE\n");
            door_lock_cmd_rsp.cmd = ZCL_DOOR_LOCK_CMD_GET_USER_STATUS_RESPONSE;
            door_lock_cmd_rsp.get_user_status_rsp.user_id = *(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            door_lock_cmd_rsp.get_user_status_rsp.user_status = *(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            break;
        }
        case ZCL_DOOR_LOCK_CMD_LOCK_DOOR_RESPONSE:
        {
            printf("ZCL_DOOR_LOCK_CMD_LOCK_DOOR_RESPONSE\n");
            door_lock_cmd_rsp.cmd = ZCL_DOOR_LOCK_CMD_LOCK_DOOR_RESPONSE;
            door_lock_cmd_rsp.status = *(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            break;
        }
        case ZCL_DOOR_LOCK_CMD_UNLOCK_DOOR_RESPONSE:
        {
            printf("ZCL_DOOR_LOCK_CMD_UNLOCK_DOOR_RESPONSE\n");
            door_lock_cmd_rsp.cmd = ZCL_DOOR_LOCK_CMD_UNLOCK_DOOR_RESPONSE;
            door_lock_cmd_rsp.status = *(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            break;
        }
        default:
            printf("ZCL_DOOR_LOCK_CMD_UNKNOWN\n");
            door_lock_cmd_rsp.cmd = ZCL_DOOR_LOCK_CMD_UNKNOWN;
            break;
        }
    }

    *resp_len = 0;
    resp[0] = DOOR_LOCK_RESPONSE_NOTIFY;
    *resp_len = 1;
    memcpy(&resp[1], (uint8_t *)&door_lock_cmd_rsp, sizeof(doorLockCmdResponse));
    *resp_len = *resp_len + sizeof(doorLockCmdResponse);
}

uint8_t zcl_door_lock_gen_req(uint8_t *data, zcl_hdr_t *hdr, zclDoorLockReq *req)
{
    uint8_t len, *data_ptr;

    data_ptr = data;

    // gen the header
    len = zcl_gen_hdr(data_ptr, hdr);
    data_ptr += len;

    switch (hdr->cmd)
    {
    case ZCL_DOOR_LOCK_CMD_SET_PIN_CODE:
        *(uint16_t *)data_ptr = req->set_pin_code.user_id;
        data_ptr += sizeof(uint16_t);
        *data_ptr++ = req->set_pin_code.user_status;
        *data_ptr++ = req->set_pin_code.user_type;
        *data_ptr++ = req->set_pin_code.pin_length;
        memcpy(data_ptr, req->set_pin_code.p_pin, req->set_pin_code.pin_length);
        data_ptr += req->set_pin_code.pin_length;
        break;
    case ZCL_DOOR_LOCK_CMD_GET_PIN_CODE:
        *(uint16_t *)data_ptr = req->get_pin_code.user_id;
        data_ptr += sizeof(uint16_t);
        break;
    case ZCL_DOOR_LOCK_CMD_CLEAR_PIN_CODE:
        *(uint16_t *)data_ptr = req->clear_pin_code.user_id;
        data_ptr += sizeof(uint16_t);
        break;
    case ZCL_DOOR_LOCK_CMD_SET_USER_STATUS:
        *(uint16_t *)data_ptr = req->set_user_status.user_id;
        data_ptr += sizeof(uint16_t);
        *data_ptr++ = req->set_user_status.user_status;
        break;
    case ZCL_DOOR_LOCK_CMD_GET_USER_STATUS:
        *(uint16_t *)data_ptr = req->get_user_status.user_id;
        data_ptr += sizeof(uint16_t);
        break;

    default:
        break;
    }
    // return the len
    return data_ptr - data;
}