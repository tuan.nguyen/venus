#include "zcl_illuminance_measurement.h"

void zcl_illuminance_measurement_init(zcl_illuminance_measurement_attrib_list_t *attrib_list)
{
    // init the data values first
    memset(attrib_list, 0, sizeof(zcl_illuminance_measurement_attrib_list_t));
   
    // init the attribute fields
    zcl_set_attrib(&attrib_list->list[0],     ZCL_ILLUMINANCE_MEASUREMENT_ATTRIB_MEASURED_VALUE,          ZCL_TYPE_U16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.measured_value);
    zcl_set_attrib(&attrib_list->list[1],     ZCL_ILLUMINANCE_MEASUREMENT_ATTRIB_MIN_MEASURED_VALUE,      ZCL_TYPE_U16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.min_measured_value);
    zcl_set_attrib(&attrib_list->list[2],     ZCL_ILLUMINANCE_MEASUREMENT_ATTRIB_MAX_MEASURED_VALUE,      ZCL_TYPE_U16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.max_measured_value);
    zcl_set_attrib(&attrib_list->list[3],     ZCL_ILLUMINANCE_MEASUREMENT_ATTRIB_TOLERANCE,     		  ZCL_TYPE_U16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.tolerance);
    zcl_set_attrib(&attrib_list->list[4],     ZCL_ILLUMINANCE_MEASUREMENT_ATTRIB_LIGHT_SENSOR_TYPE,       ZCL_TYPE_8BIT_ENUM,       ZCL_ACCESS_READ_ONLY,  &attrib_list->data.light_sensor_type);


    //attrib end marker    
    zcl_set_attrib(&attrib_list->list[5], ZCL_END_MARKER, 0, 0, NULL);
   
}

void zcl_illuminance_measurement_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
	
}