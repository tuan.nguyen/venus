#ifndef _DEV_DBG_H_
#define _DEV_DBG_H_
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "../zcl.h" 
#include "../zcl_on_off.h"
void debug_dump_zcl_frm(uint8_t *data, zcl_hdr_t *hdr, uint16_t clust);
void debug_dump_buf(const uint8_t *buf, uint8_t len);
char *debug_dump_zcl_status(uint8_t status);

#endif