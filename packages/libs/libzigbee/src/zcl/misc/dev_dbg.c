/*******************************************************************
    Copyright (C) 2009 FreakLabs
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

    1. Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
    3. Neither the name of the the copyright holder nor the names of its contributors
       may be used to endorse or promote products derived from this software
       without specific prior written permission.
    4. This software is subject to the additional restrictions placed on the
       Zigbee Specification's Terms of Use.

    THIS SOFTWARE IS PROVIDED BY THE THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS'' AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
    OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
    SUCH DAMAGE.

    Originally written by Christopher Wang aka Akiba.
    Please post support questions to the FreakLabs forum.

*******************************************************************/
/*!
    \file debug.c
    \ingroup misc
    \brief Functions for debugging

    Decode and print various frame formats for debugging purposes.
    Bascially, a set of various debug utilities that I cobbled together.
*/
#include "dev_dbg.h"

//#include "zcl_id.h"
//#include "zcl_level.h"

/* Print out the contents of a frame buffer */
void debug_dump_buf(const uint8_t *buf, uint8_t len)
{
//#if (DEBUG_BUF == 1)
	uint8_t i;

	printf("\n");
	printf("DBG_DUMP_BUF: ");

	for (i=0; i<len; i++)
		printf("%02x ", buf[i]);

	printf("\n");
//#endif
}

/* Decode the MAC header and print out the fields */

/* Dump the specified cluster list for the endpoint */
/*
void debug_dump_clust_list(clust_list_t *clust_list)
{
//#if (DEBUG_ZDO == 1)
	uint8_t i;

	printf("DUMP_CLUST_LIST: CLUST_CNT           = %02X.\n",
		      clust_list->clust_cnt);

	for (i = 0; i < clust_list->clust_cnt; i++)
		printf("DUMP_CLUST_LIST: CLUST_LIST[%d]       = %04X.\n",
			      i, clust_list->clust_list[i]);
//#endif
}
*/

static uint8_t debug_dump_attrib_data(uint8_t type, uint8_t *data)
{
	uint8_t size, str_buf[128], *data_ptr = data;
	switch (type)
	{
	case ZCL_TYPE_U8:
		printf("TYPE: %s, ", "UNSIGNED CHAR");
		printf("DATA: %02X", *data_ptr);
		data_ptr++;
		break;
	case ZCL_TYPE_U16:
		printf("TYPE: %s, ", "UNSIGNED INT");
		printf("DATA: %04X", *(uint16_t *)data_ptr);
		data_ptr += sizeof(uint16_t);
		break;
	case ZCL_TYPE_U32:
		printf("TYPE: %s, ", "UNSIGNED WORD");
		printf("DATA: %08X", *(uint32_t *)data_ptr);
		data_ptr += sizeof(uint32_t);
		break;
	case ZCL_TYPE_BOOL:
		printf("TYPE: %s, ", "BOOL");
		printf("DATA: %s", *data_ptr ? "TRUE" : "FALSE");
		data_ptr++;
		break;
	case ZCL_TYPE_CHAR_STRING:
		printf("TYPE: %s, ", "CHAR STRING");

		// byte 0 is the length of the string
		size = *(uint8_t *)data_ptr++;

		// start the copy from byte 1
		memcpy(str_buf, data_ptr, size);
		data_ptr += size;
		str_buf[size] = 0;

		printf("DATA: %s", str_buf);
		break;
	case ZCL_TYPE_S16:
		printf("TYPE: %s, ", "SIGNED INT");
		printf("DATA: %04d", *(int16_t *)data_ptr);
		data_ptr += sizeof(int16_t);
	default:
		break;
	}
	return data_ptr - data;
}
char *debug_dump_zcl_status(uint8_t status)
{
//#if (DEBUG_ZCL == 1)
	switch (status)
	{
	case ZCL_STATUS_SUCCESS:
		return "ZCL SUCCESS";
	case ZCL_STATUS_FAIL:
		return "ZCL FAIL";
	case ZCL_STATUS_MALFORMED_CMD:
		return "ZCL MALFORMED CMD";
	case ZCL_STATUS_UNSUP_CLUST_CMD:
		return "ZCL UNSUP CLUST CMD";
	case ZCL_STATUS_UNSUP_GEN_CMD:
		return "ZCL UNSUP GEN CMD";
	case ZCL_STATUS_UNSUP_MANUF_CLUST_CMD:
		return "ZCL UNSUP MANUF CLUST CMD";
	case ZCL_STATUS_UNSUP_MANUF_GEN_CMD:
		return "ZCL UNSUP MANUF GEN CMD";
	case ZCL_STATUS_INVALID_FIELD:
		return "ZCL INVALID FIELD";
	case ZCL_STATUS_UNSUP_ATTRIB:
		return "ZCL UNSUP ATTRIB";
	case ZCL_STATUS_INVALID_VALUE:
		return "ZCL INVALID VALUE";
	case ZCL_STATUS_READ_ONLY:
		return "ZCL READ ONLY";
	case ZCL_STATUS_INSUFF_SPACE:
		return "ZCL INSUFF SPACE";
	case ZCL_STATUS_DUPE_EXISTS:
		return "ZCL DUPE EXISTS";
	case ZCL_STATUS_NOT_FOUND:
		return "ZCL NOT FOUND";
	case ZCL_STATUS_UNREPORTABLE_ATTRIB:
		return "ZCL UNREPORTABLE ATTRIB";
	case ZCL_STATUS_INVALID_TYPE:
		return "ZCL INVALID TYPE";
	case ZCL_STATUS_HW_FAIL:
		return "ZCL HW FAIL";
	case ZCL_STATUS_SW_FAIL:
		return "ZCL SW FAIL";
	case ZCL_STATUS_CALIB_ERR:
		return "ZCL CALIB ERR";
	default:
		return "ZCL UNKNOWN STATUS";
	}
//#endif
	return NULL;
}
static void debug_dump_attrib_type(uint8_t type)
{
	switch (type)
	{
	case ZCL_TYPE_U8:
		printf(", TYPE: %s", "UNSIGNED CHAR");
		break;
	case ZCL_TYPE_U16:
		printf(", TYPE: %s", "UNSIGNED INT");
		break;
	case ZCL_TYPE_U32:
		printf(", TYPE: %s", "UNSIGNED WORD");
		break;
	case ZCL_TYPE_BOOL:
		printf(", TYPE: %s", "BOOL");
		break;
	case ZCL_TYPE_CHAR_STRING:
		printf(", TYPE: %s", "CHAR STRING");
		break;
	default:
		break;
	}
}

static char *debug_dump_zcl_cmd(uint16_t clust, uint8_t cmd_id)
{
	switch (clust)
	{
		/*
	case ZCL_IDENTIFY_CLUST_ID:
		if (cmd_id == ZCL_ID_CMD_ID)
	*/
			return "IDENTIFY";
	case ZCL_ON_OFF_CLUST_ID:
		switch (cmd_id)
		{
		case ZCL_ON_OFF_CMD_OFF:
			return "OFF";
		case ZCL_ON_OFF_CMD_ON:
			return "ON";
		case ZCL_ON_OFF_CMD_TOGGLE:
			return "TOGGLE";
		}
	}
	return NULL;
}


/* Decode and dump the ZCL frame */
void debug_dump_zcl_frm(uint8_t *data, zcl_hdr_t *hdr, uint16_t clust)
{
//#if (DEBUG_ZCL == 1)
	uint8_t i, *data_ptr, status, type, len;
	uint16_t attrib_id;

	printf("\n");
	printf("DUMP_ZCL_FRM: FRM TYPE       = %s.\n",
		  hdr->frm_ctrl.frm_type        ? "SPECIFIC" : "GENERAL");
	printf("DUMP_ZCL_FRM: MANUF SPECIFIC = %s.\n",
		  hdr->frm_ctrl.manuf_spec      ? "TRUE" : "FALSE");
	printf("DUMP_ZCL_FRM: DIRECT         = %s.\n",
		  hdr->frm_ctrl.dir             ? "TO CLIENT" : "TO SERVER");
	printf("DUMP_ZCL_FRM: DIS DEF RESP   = %s.\n",
		  hdr->frm_ctrl.dis_def_resp    ? "TRUE" : "FALSE");

	if (hdr->frm_ctrl.manuf_spec)
		printf("DUMP_ZCL_FRM: MANUF CODE     = %4X.\n",hdr->manuf_code);

	printf("DUMP_ZCL_FRM: SEQ NUM        = %02X.\n", hdr->seq_num);

	if (hdr->frm_ctrl.frm_type == ZCL_FRM_TYPE_GENERAL)
	{
		switch (hdr->cmd)
		{
		case ZCL_CMD_READ_ATTRIB:
			printf("DUMP_ZCL_FRM: CMD            = %s.\n", "READ ATTRIBUTES");
			printf("DUMP_ZCL_FRM: ATTRIBS: ");

			data_ptr = hdr->payload;
			for (i=0; i<(hdr->payload_len)/2; i++)
			{
				printf("%04X ", *(uint16_t *)data_ptr);
				data_ptr += sizeof(uint16_t);
			}
			printf("\n");
			break;
		case ZCL_CMD_READ_ATTRIB_RESP:
			printf("DUMP_ZCL_FRM: CMD            = %s.\n", "READ ATTRIBUTE RESPONSE");

			data_ptr = hdr->payload;
			while ((data_ptr - hdr->payload) < hdr->payload_len)
			{
				printf("ATTRIB ID: %04X", *(uint16_t *)data_ptr);
				data_ptr += sizeof(uint16_t);

				status = *data_ptr++;
				printf(", STATUS: %02X", status);

				if (status == ZCL_STATUS_SUCCESS)
				{
					/*
					 * since the data is variable, there is
					 * a special function to handle dumping it.
					 * just pass in the type and the data ptr.
					 * then increment the data ptr when we're done.
					 */
					type = *data_ptr++;
					len = debug_dump_attrib_data(type, data_ptr);
					data_ptr += len;
				}
				printf("\n");
			}
			break;
		case ZCL_CMD_WRITE_ATTRIB:
			printf("DUMP_ZCL_FRM: CMD            = %s.\n", "WRITE ATTRIBUTES");

			data_ptr = hdr->payload;
			while ((data_ptr - hdr->payload) <  hdr->payload_len)
			{
				printf("ATTRIB ID: %04X, ", *(uint16_t *)data_ptr);
				data_ptr += sizeof(uint16_t);

				type = *data_ptr++;
				len = debug_dump_attrib_data(type, data_ptr);
				data_ptr += len;
				printf("\n");
			}
			break;
		case ZCL_CMD_WRITE_ATTRIB_RESP:
			printf("DUMP_ZCL_FRM: CMD            = %s.\n", "WRITE ATTRIBUTE RESP");

			data_ptr = hdr->payload;
			while ((data_ptr - hdr->payload) < hdr->payload_len)
			{
				status = *data_ptr++;
				attrib_id = *(uint16_t *)data_ptr;
				data_ptr += sizeof(uint16_t);
				printf("Attrib ID: %04X, ", attrib_id);
				printf("DUMP_ZCL_FRM: STATUS         = %s.\n",
					  debug_dump_zcl_status(status));
			}
			break;
		case ZCL_CMD_DISC_ATTRIB:
			printf("DUMP_ZCL_FRM: CMD            = %s.\n", "DISC ATTRIBUTES");

			data_ptr = hdr->payload;
			printf("DUMP_ZCL_FRM: START ID       = %04X.\n", *(uint16_t *)data_ptr);
			data_ptr += sizeof(uint16_t);
			printf("DUMP_ZCL_FRM: NUM OF ATTRIBS = %02X.\n", *data_ptr);
			data_ptr++;
			break;
		case ZCL_CMD_DISC_ATTRIB_RESP:
			printf("DUMP_ZCL_FRM: CMD            = %s.\n", "DISC ATTRIBUTES RESP");

			data_ptr = hdr->payload;
			printf("DUMP_ZCL_FRM: DISC COMPLETE  = %s.\n", *data_ptr ? "TRUE" : "FALSE");
			data_ptr++;

			while ((data_ptr - hdr->payload) < hdr->payload_len)
			{
				attrib_id = *(uint16_t *)data_ptr;
				data_ptr += sizeof(uint16_t);
				type = *data_ptr++;

				printf("Attrib ID: %04X, ", attrib_id);
				debug_dump_attrib_type(type);
				printf("\n");
			}
			break;
		case ZCL_CMD_CONFIG_REPORT_RESP:
			printf("DUMP_ZCL_FRM: CMD            = %s.\n", "CONFIG ATTRIBUTES RESP");
			data_ptr = hdr->payload;

			status      = *data_ptr++;
			attrib_id   = *(uint16_t *)data_ptr;
			data_ptr += sizeof(uint16_t);
			printf("DUMP_ZCL_FRM: ATTRIB ID = %04X.\n ", attrib_id);
			printf("DIR                          = %s.\n ",
				  *data_ptr ? "RECEIVE REPORT" : "SEND REPORT");
			printf("DUMP_ZCL_FRM: CMD            = %s.\n", debug_dump_zcl_status(status));
			break;
		case ZCL_CMD_REPORT_ATTRIB:
			printf("DUMP_ZCL_FRM: CMD            = %s.\n", "REPORT ATTRIBUTES");
			data_ptr = hdr->payload;

			attrib_id = *(uint16_t *)data_ptr;
			data_ptr += sizeof(uint16_t);
			type = *data_ptr++;

			printf("DUMP_ZCL_FRM: ATTRIB ID = %04X, ", attrib_id);
			len = debug_dump_attrib_data(type, data_ptr);
			data_ptr += len;
			printf("\n");
			break;

		case ZCL_CMD_DEFAULT_RESP:
			data_ptr = hdr->payload;
			printf("DUMP_ZCL_FRM: CMD            = %s.\n", "DEFAULT RESPONSE");
			printf("DUMP_ZCL_FRM: CMD ID         = %s, ",
				  debug_dump_zcl_cmd(clust, *data_ptr));
			data_ptr++;
			printf("DUMP_ZCL_FRM: STATUS         = %s, ", debug_dump_zcl_status(*data_ptr));
			break;
		}
	} else {
		switch (clust)
		{
		/*
		case ZCL_IDENTIFY_CLUST_ID:
			if (hdr->frm_ctrl.dir == ZCL_FRM_DIR_TO_SRVR)
			{
				switch (hdr->cmd)
				{
				case ZCL_ID_CMD_ID:
					data_ptr = hdr->payload;
					printf("DUMP_ZCL_FRM: CMD            = %s.\n", "IDENTIFY");
					printf("DUMP_ZCL_FRM: TIMEOUT        = %04X.\n",
						  *(uint16_t *)data_ptr);
					break;
				case ZCL_ID_CMD_ID_QUERY:
					printf("DUMP_ZCL_FRM: CMD            = %s.\n", "IDENTIFY QUERY");
				break;
				}
			} else {
				if (hdr->cmd == ZCL_ID_CMD_ID_QUERY_RESP)
				{
					data_ptr = hdr->payload;
					printf("DUMP_ZCL_FRM: CMD            = IDENTIFY QUERY RESP.\n");
					printf("DUMP_ZCL_FRM: TIME REMAINING = %04X.\n",
						  *(uint16_t *)data_ptr);
				}
			}
			break;
		*/	
		case ZCL_ON_OFF_CLUST_ID:
			switch (hdr->cmd)
			{
			case ZCL_ON_OFF_CMD_OFF:
				printf("DUMP_ZCL_FRM: CMD            = %s.\n", "OFF");
				break;
			case ZCL_ON_OFF_CMD_ON:
				printf("DUMP_ZCL_FRM: CMD            = %s.\n", "ON");
				break;
			case ZCL_ON_OFF_CMD_TOGGLE:
				printf("DUMP_ZCL_FRM: CMD            = %s.\n", "TOGGLE");
				break;
			}
			break;
		/*
		case ZCL_LEVEL_CLUST_ID:
			data_ptr = hdr->payload;
			switch (hdr->cmd)
			{
			case ZCL_LEVEL_CMD_MOVE_TO_LEVEL:
				printf("DUMP_ZCL_FRM: CMD            = %s.\n", "MOVE TO LEVEL");
				printf("DUMP_ZCL_FRM: LEVEL          = %02X.\n", *data_ptr);
				data_ptr++;
				printf("DUMP_ZCL_FRM: TRANS TIME     = %04X.\n", *(uint16_t *)data_ptr);
				break;
			case ZCL_LEVEL_CMD_MOVE:
				printf("DUMP_ZCL_FRM: CMD            = %s.\n", "MOVE");
				printf("DUMP_ZCL_FRM: DIR            = %s.\n",
					  *data_ptr ? "DOWN" : "UP");
				data_ptr++;
				printf("DUMP_ZCL_FRM: RATE           = %02X.\n", *data_ptr);
				break;
			case ZCL_LEVEL_CMD_STEP:
				printf("DUMP_ZCL_FRM: CMD            = %s.\n", "MOVE TO LEVEL");
				printf("DUMP_ZCL_FRM: STEP           = %02X.\n", *data_ptr);
				data_ptr++;
				printf("DUMP_ZCL_FRM: STEP SIZE      = %02X.\n", *data_ptr);
				data_ptr++;
				printf("DUMP_ZCL_FRM: TRANS TIME     = %04X.\n", *(uint16_t *)data_ptr);
				break;
			case ZCL_LEVEL_CMD_STOP:
				printf("DUMP_ZCL_FRM: CMD            = %s.\n", "STOP");
				break;
			}
			break;
		*/
		}
	}
//#endif
}
