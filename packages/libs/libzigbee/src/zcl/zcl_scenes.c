#include "zcl_scenes.h"
void zcl_scenes_init(zcl_scenes_attrib_list_t *attrib_list)
{
	memset(&attrib_list->data, 0, sizeof(zcl_scenes_data_t));
    attrib_list->data.name_supp = ZCL_SCENES_NAME_SUPPORT;

    // init the attribute fields
    zcl_set_attrib(&attrib_list->list[0], ZCL_SCENES_ATTRIB_SCENE_COUNT,    ZCL_TYPE_U8,        ZCL_ACCESS_READ_ONLY, &attrib_list->data.scene_cnt);
    zcl_set_attrib(&attrib_list->list[1], ZCL_SCENES_ATTRIB_CURR_SCENE,     ZCL_TYPE_U8,        ZCL_ACCESS_READ_ONLY, &attrib_list->data.curr_scene);
    zcl_set_attrib(&attrib_list->list[2], ZCL_SCENES_ATTRIB_CURR_GROUP,     ZCL_TYPE_U16,       ZCL_ACCESS_READ_ONLY, &attrib_list->data.curr_grp);
    zcl_set_attrib(&attrib_list->list[3], ZCL_SCENES_ATTRIB_SCENE_VALID,    ZCL_TYPE_BOOL,      ZCL_ACCESS_READ_ONLY, &attrib_list->data.scene_valid);
    zcl_set_attrib(&attrib_list->list[4], ZCL_SCENES_ATTRIB_NAME_SUPP,      ZCL_TYPE_8BITMAP,   ZCL_ACCESS_READ_ONLY, &attrib_list->data.name_supp);
    zcl_set_attrib(&attrib_list->list[5], ZCL_SCENES_ATTRIB_LAST_CONFIG_BY, ZCL_TYPE_IEEE_ADDR, ZCL_ACCESS_READ_ONLY, &attrib_list->data.last_cfg_by);
    zcl_set_attrib(&attrib_list->list[6], ZCL_END_MARKER,                   0,                  0,                    NULL);

}


uint8_t zcl_scenes_gen_req(uint8_t *data, zcl_hdr_t *hdr, zcl_scenes_req_t *req)
{
	uint8_t len, *data_ptr;

    // gen the zcl hdr first
    data_ptr = data;
    len = zcl_gen_hdr(data_ptr, hdr);
    data_ptr += len;

    switch (hdr->cmd)
    {
        case ZCL_SCENES_CMD_ADD:
            // fill out the add scene payload
            *(uint16_t *)data_ptr = req->grp_id;
            data_ptr += sizeof(uint16_t);
            *data_ptr++ = req->id;
            *(uint16_t *)data_ptr = req->trans_time;
            data_ptr += sizeof(uint16_t);

            // copy in the name
            len = (uint8_t)(strlen(req->name) > ZCL_SCENES_NAME_MAX_LEN) ? ZCL_SCENES_NAME_MAX_LEN : (uint8_t)strlen(req->name);
            *data_ptr++ = len;
            memcpy(data_ptr, req->name, len);
            data_ptr += len;

            // copy in the extension fields
            memcpy(data_ptr, req->ext_field, req->ext_len);
            data_ptr += req->ext_len;
            break;

        case ZCL_SCENES_CMD_VIEW:
        case ZCL_SCENES_CMD_REM:
        case ZCL_SCENES_CMD_STORE:
        case ZCL_SCENES_CMD_RECALL:
            *(uint16_t *)data_ptr = req->grp_id;
            data_ptr += sizeof(uint16_t);
            *data_ptr++ = req->id;
            break;

        case ZCL_SCENES_CMD_REM_ALL:
        case ZCL_SCENES_CMD_GET_MEMB:
            *(uint16_t *)data_ptr = req->grp_id;
            data_ptr += sizeof(uint16_t);
            break;
    }
    return data_ptr - data;
}

void    zcl_scenes_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
    uint8_t  *data_ptr;
    scenes_cmd_response scenes_cmd_rsp;
    
    memset((char*)&scenes_cmd_rsp,0,sizeof(scenes_cmd_response));

    scenes_cmd_rsp.devSourceDesc.destAddr=addr;
    scenes_cmd_rsp.devSourceDesc.endpoint=endpoint;
    scenes_cmd_rsp.devSourceDesc.profileId=profile;
    scenes_cmd_rsp.devSourceDesc.clusterId=clust->clust_id;

    data_ptr = hdr->payload;
    switch (hdr->cmd)
    {
        case ZCL_SCENES_CMD_ADD_RESP:
        {
            scenes_cmd_rsp.cmd=ZCL_SCENES_CMD_ADD_RESP;
            scenes_cmd_rsp.add_scene_rsp.status=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            scenes_cmd_rsp.add_scene_rsp.group_id=*(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            scenes_cmd_rsp.add_scene_rsp.scene_id=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            break;
        }
        case ZCL_SCENES_CMD_VIEW_RESP:
        {
            scenes_cmd_rsp.cmd=ZCL_SCENES_CMD_VIEW_RESP;
            scenes_cmd_rsp.view_scene_rsp.status=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            scenes_cmd_rsp.view_scene_rsp.group_id=*(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            scenes_cmd_rsp.view_scene_rsp.scene_id=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            if (hdr->payload_len>4)
            {
                scenes_cmd_rsp.view_scene_rsp.trans_time=*(uint16_t *)data_ptr;
                data_ptr += sizeof(uint16_t);
            }
            if (hdr->payload_len>6)
            {
                uint8_t len=*(uint8_t *)data_ptr;
                data_ptr += sizeof(uint8_t);
                if ((len>0)&&(len<=ZCL_SCENES_NAME_MAX_LEN))
                {
                    memcpy(scenes_cmd_rsp.view_scene_rsp.name,data_ptr,len);
                    data_ptr +=len;
                }

                memcpy(scenes_cmd_rsp.view_scene_rsp.ext_field,data_ptr,(hdr->payload_len-6-len));
                data_ptr +=(hdr->payload_len-6-len);
            }
            break;
        }
        case ZCL_SCENES_CMD_REM_RESP:
        {
            scenes_cmd_rsp.cmd=ZCL_SCENES_CMD_REM_RESP;
            scenes_cmd_rsp.remove_scene_rsp.status=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            scenes_cmd_rsp.remove_scene_rsp.group_id=*(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            scenes_cmd_rsp.remove_scene_rsp.scene_id=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);

            break;
        }
        case ZCL_SCENES_CMD_REM_ALL_RESP:
        {
            scenes_cmd_rsp.cmd=ZCL_SCENES_CMD_REM_ALL_RESP;
            scenes_cmd_rsp.remove_all_scene_rsp.status=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            scenes_cmd_rsp.remove_all_scene_rsp.group_id=*(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            break;
        }
        case ZCL_SCENES_CMD_STORE_RESP:
        {
            scenes_cmd_rsp.cmd=ZCL_SCENES_CMD_STORE_RESP;
            scenes_cmd_rsp.store_scene_rsp.status=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            scenes_cmd_rsp.store_scene_rsp.group_id=*(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            scenes_cmd_rsp.store_scene_rsp.scene_id=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);

            break;
        }
        case ZCL_SCENES_CMD_GET_MEMB_RESP:
        {
            scenes_cmd_rsp.cmd=ZCL_SCENES_CMD_GET_MEMB_RESP;
            scenes_cmd_rsp.get_scene_membership_rsp.status=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            scenes_cmd_rsp.get_scene_membership_rsp.capacity=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            scenes_cmd_rsp.get_scene_membership_rsp.group_id=*(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            if (hdr->payload_len>4)
            {
                scenes_cmd_rsp.get_scene_membership_rsp.scene_count=*(uint8_t *)data_ptr;
                data_ptr += sizeof(uint8_t);
                memcpy(scenes_cmd_rsp.get_scene_membership_rsp.scene_list,data_ptr,scenes_cmd_rsp.get_scene_membership_rsp.scene_count);
                data_ptr +=scenes_cmd_rsp.get_scene_membership_rsp.scene_count;

            } 
            break;
        }
        default:
            printf("ZCL_SCENES_CMD_UNKNOWN\n");
            break;
    }

    *resp_len=0;
    resp[0]=SCENES_RESPONSE_NOTIFY;
    *resp_len=1;
    memcpy(&resp[1],(uint8_t*)&scenes_cmd_rsp,sizeof(scenes_cmd_response));
    *resp_len=*resp_len+sizeof(scenes_cmd_response);
    
}


