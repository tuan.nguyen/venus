#include "zcl.h"
#include "zcl_gen.h"
static uint8_t seq_num;

void zcl_init()
{
    seq_num = 0;
}

uint8_t zcl_get_seq_num()
{
    seq_num++;
    if(!seq_num)
    {
        seq_num++;
    }
    return seq_num;
}

uint8_t zcl_get_attrib_size(zclAttribute *attrib)
{
    switch (attrib->type)
    {
    case ZCL_TYPE_8BIT:
    case ZCL_TYPE_U8:
        return sizeof(uint8_t);

    case ZCL_TYPE_16BIT:
    case ZCL_TYPE_U16:
    case ZCL_TYPE_S16:
        return sizeof(uint16_t);

    case ZCL_TYPE_32BIT:
    case ZCL_TYPE_U32:
        return sizeof(uint32_t);

    case ZCL_TYPE_IEEE_ADDR:
        return sizeof(uint64_t);

    case ZCL_TYPE_BOOL:
        return sizeof(bool);

    case ZCL_TYPE_CHAR_STRING:
        // the first byte of the string attrib has the size. so we need
        // to return the value of the first byte + 1 to account for using
        // a byte to hold the string size
        return ((*(uint8_t *)attrib->data) + 1);
    default:
        return 0xff;
    }
}
/**************************************************************************/
/*!
    Generate the ZCL Read Attributes command. An empty array is passed in
    and it gets filled out in the function. An attribute ID list and the number
        of attributes is required to be passed into the function. Only the ID from
        the attrib list is used and are used to specify the attributes that are to be read.
        The command data is contained in the zcl header that's passed in. When
        finished, the function will return the length of the data.
*/
/**************************************************************************/
uint8_t zcl_gen_read_attrib(uint8_t *data, zcl_hdr_t *hdr, uint16_t *attrib_list, uint8_t attrib_num)
{
    uint8_t i, len, *data_ptr;

    // gen the zcl frame header
    len = zcl_gen_hdr(data, hdr);
    data_ptr = data + len;

    // gen the payload
    for (i=0; i<attrib_num; i++)
    {
        *(uint16_t *)data_ptr = attrib_list[i];
        data_ptr += sizeof(uint16_t);
    }

    return (data_ptr - data);
}


/**************************************************************************/
/*!
    Generate the ZCL Write attributes command. The length of the write attribute
        command frame gets passed back to the caller. An attrib list and number of
        attributes needs to be passed into the function. The attrib IDs and values will
        be used from the attrib list and will specify the attrib ID to be written to
        as well as the value to write.
*/
/**************************************************************************/
uint8_t zcl_gen_write_attrib(uint8_t *data, zcl_hdr_t *hdr, zclAttribute *attrib_list, uint8_t attrib_num)
{
    uint8_t i, len, attrib_len, *data_ptr;
    zclAttribute *attrib;

    // gen the zcl frame header
    len = zcl_gen_hdr(data, hdr);
    data_ptr = data + len;

    for (i=0; i<attrib_num; i++)
    {
        attrib = &attrib_list[i];

        // copy the attrib id and the attrib data type
        *(uint16_t *)data_ptr = attrib->id;
        data_ptr += sizeof(uint16_t);
        *data_ptr++ = attrib->type;

        // get the len of the attribute and then copy it into the data ptr
        attrib_len = zcl_get_attrib_size(attrib);
        memcpy(data_ptr, attrib->data, attrib_len);
        data_ptr += attrib_len;
    }

    return (data_ptr - data);
}

/**************************************************************************/
/*!
        Generate a configure report command frame. This frame is used to configure an
        attribute for reporting. The attribute ID, min/max interval, the change value
        must be specified. The min/max interval are the intervals that will be used
        to specify the reporting time. Only the max interval is used for the reporting
        interval so it's recommended to set the min and max interval to the same value.
        The change value is the amount the value needs to change in order to send the report.
        A value of 0 for the change signifies that the report will be sent regardless
        if the value changed or not.
*/
/**************************************************************************/
uint8_t zcl_gen_config_rpt(uint8_t *data, zcl_hdr_t *hdr, zcl_clust_t *clust, uint16_t attrib_id, uint16_t min_intv, uint16_t max_intv, uint32_t change)
{
    uint8_t len, *data_ptr;
    zclAttribute *attrib;

    // gen the zcl frm hdr
    len = zcl_gen_hdr(data, hdr);
    data_ptr = data + len;

    if ((attrib = zcl_find_attrib(clust->attrib_list, attrib_id)) == NULL)
    {
        return 0;
    }

    *data_ptr++         = ZCL_RPT_DIR_SEND_RPT;
    *(uint16_t *)data_ptr    = attrib_id;
    data_ptr += sizeof(uint16_t);
    *data_ptr++         = attrib->type;
    *(uint16_t *)data_ptr    = min_intv;
    data_ptr += sizeof(uint16_t);
    *(uint16_t *)data_ptr    = max_intv;
    data_ptr += sizeof(uint16_t);

    // report on level change - currently not supported
    //switch (attrib->type)
    //{
    //case ZCL_TYPE_8BIT:
    //case ZCL_TYPE_U8:
    //    *data_ptr++ = change;
    //    break;
    //
    //case ZCL_TYPE_16BIT:
    //case ZCL_TYPE_U16:
    //    *(U16 *)data_ptr = change;
    //    data_ptr += sizeof(U16);
    //    break;
    //
    //case ZCL_TYPE_32BIT:
    //case ZCL_TYPE_U32:
    //    *(U32 *)data_ptr = change;
    //    data_ptr += sizeof(U32);
    //    break;
    //
    //default:
    //    *data_ptr++ = 0;
    //    break;
    //}
    return (data_ptr - data);
}

/**************************************************************************/
//used to find a cluster from a list of clusters based on the cluster ID.
/**************************************************************************/
zcl_clust_t *zcl_find_clust(zcl_clust_t **clust_list, uint16_t clust_id)
{
    uint8_t i;

    if (clust_list)
    {
        for (i=0; clust_list[i]->clust_id != ZCL_END_MARKER; i++)
        //for (i=0; i< ZCL_END_MARKER; i++)
        {
            if (clust_list[i]->clust_id == clust_id)
            {
                return clust_list[i];
            }
        }
    }
    printf("Not support this cluster\n");
    return NULL;
}
/**************************************************************************/
//parse the ZCL into structure
/**************************************************************************/
void zcl_parse_hdr(uint8_t *data, uint8_t len, zcl_hdr_t *hdr)
{
    uint8_t *data_ptr;

    if (data)
    {
        data_ptr = data;

        // get the fcf for further decoding
        hdr->fcf = *data_ptr++;

        // decode the fcf and fill out the frm ctrl structure
        hdr->frm_ctrl.frm_type      = (hdr->fcf & ZCL_FRM_TYPE_MASK)    >> ZCL_FRM_TYPE_OFF;
        hdr->frm_ctrl.manuf_spec    = (hdr->fcf & ZCL_MANUF_SPEC_MASK)  >> ZCL_MANUF_SPEC_OFF;
        hdr->frm_ctrl.dir           = (hdr->fcf & ZCL_DIR_MASK)         >> ZCL_DIR_OFF;
        hdr->frm_ctrl.dis_def_resp  = (hdr->fcf & ZCL_DIS_DEF_MASK)     >> ZCL_DIS_DEF_OFF;

        // get the rest of the fields
        if (hdr->frm_ctrl.manuf_spec)
        {
            hdr->manuf_code = *(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
        }

        hdr->seq_num        = *data_ptr++;
        hdr->cmd            = *data_ptr++;
        hdr->payload        = data_ptr;
        hdr->payload_len    = len - (data_ptr - data);
    }
}

void zcl_set_clust(zcl_clust_t *clust, uint8_t endpoint, uint16_t clust_id, zclAttribute *attrib_list,
                void (*rx_handler)(uint8_t *, uint8_t *, uint16_t, uint8_t, uint16_t, struct _zcl_clust_t *, zcl_hdr_t *),
                void (*action_handler)(uint8_t, void *))
{
    clust->endpoint               = endpoint;
    clust->clust_id         = clust_id;
    clust->attrib_list      = attrib_list;
    clust->rx_handler       = rx_handler;
    clust->action_handler   = action_handler;
}

void zcl_set_attrib(zclAttribute *attrib, uint16_t id, uint8_t type, uint8_t access, void *data)
{
    attrib->id        = id;
    attrib->type      = type;
    attrib->access    = access;
    attrib->data      = data;
    attrib->rpt       = NULL;
}


/**************************************************************************/
/*!
        Used to find the attribute from a list of attributes based on the
        attribute ID.
*/
/**************************************************************************/
zclAttribute *zcl_find_attrib(zclAttribute *attrib_list, uint16_t attrib_id)
{
    uint8_t i;

    if (attrib_list)
    {
        for (i=0; attrib_list[i].id != ZCL_END_MARKER; i++)
        {
            if (attrib_list[i].id == attrib_id)
            {
                return &attrib_list[i];
            }
        }
    }
    return NULL;
}
/**************************************************************************/
/*!

*/
/**************************************************************************/
void zcl_set_string_attrib(uint8_t *attrib_data, uint8_t *val, uint8_t max_sz)
{
    uint8_t len;

    if ((len = strlen((char *)val)) < max_sz)
    {
        memcpy(attrib_data, &len, 1);
        memcpy(attrib_data + 1, val, len);
    }
}

/**************************************************************************/
/*!
        This is the main ZCL command handler. When a ZCL frame arrives, it first
        comes here and then gets decoded and sent to the correct function for
        handling.
*/
/**************************************************************************/
void zcl_cmd_handler(uint8_t *resp, uint8_t *resp_len, uint16_t src_addr, uint8_t srcEndpoint, uint16_t prof_id, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
    // make sure that the buffers and clusters are present
    if ((clust == NULL) || (hdr == NULL))
    {
        return;
    }

    // decode the cmd and route it to the correct function
    //LOG(LOG_DBG,"decode zcl_cmd_handler\n");
    switch (hdr->cmd)
    {
        case ZCL_CMD_READ_ATTRIB:
            //LOG(LOG_DBG,"ZCL_CMD_READ_ATTRIB\n");
            //*resp_len = zcl_cmd_read_attrib(resp, src_addr, srcEndpoint, clust, hdr);
            break;

        case ZCL_CMD_WRITE_ATTRIB:
            //LOG(LOG_DBG,"ZCL_CMD_WRITE_ATTRIB\n");
            //*resp_len = zcl_cmd_write_attrib(resp, src_addr, srcEndpoint, clust, hdr);
            break;

        case ZCL_CMD_WRITE_ATTRIB_NO_RESP:
            //LOG(LOG_DBG,"ZCL_CMD_WRITE_ATTRIB_NO_RESP\n");
            //zcl_cmd_write_attrib(resp, src_addr, srcEndpoint, clust, hdr);
            //*resp_len = 0;
            break;

        case ZCL_CMD_DISC_ATTRIB:
            //LOG(LOG_DBG,"ZCL_CMD_DISC_ATTRIB\n");
            //*resp_len = zcl_cmd_disc_attrib(resp, src_addr, srcEndpoint, clust, hdr);
            break;

        case ZCL_CMD_CONFIG_REPORT:
            //LOG(LOG_DBG,"ZCL_CMD_CONFIG_REPORT\n");
            //*resp_len = zcl_cmd_cfg_rpt(resp, src_addr, srcEndpoint, clust, prof_id, hdr);
            break;
    }
    return;
}

