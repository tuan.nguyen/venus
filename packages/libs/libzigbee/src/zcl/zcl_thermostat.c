#include "zcl_thermostat.h"

/**************************************************************************/
/*!
        Init the on/off cluster.
*/
/**************************************************************************/
void zcl_thermostat_init(zcl_thermostat_attrib_list_t *attrib_list)
{
    // init the data
    memset(&attrib_list->data, 0, sizeof(zcl_thermostat_data_t));


    // init the attribs
    zcl_set_attrib(&attrib_list->list[0],   ZCL_THERMOSTAT_ATTRIB_LOCAL_TEMPERATURE,             ZCL_TYPE_S16,         ZCL_ACCESS_READ_ONLY, &attrib_list->data.local_temperature);
    zcl_set_attrib(&attrib_list->list[1],   ZCL_THERMOSTAT_ATTRIB_OUTDOOR_TEMPERATURE,           ZCL_TYPE_S16,         ZCL_ACCESS_READ_ONLY, &attrib_list->data.outdoor_temperature);
    zcl_set_attrib(&attrib_list->list[2],   ZCL_THERMOSTAT_ATTRIB_OCCUPANCY,                     ZCL_TYPE_8BITMAP,     ZCL_ACCESS_READ_ONLY, &attrib_list->data.occupancy);
    zcl_set_attrib(&attrib_list->list[3],   ZCL_THERMOSTAT_ATTRIB_ABS_MIN_HEAT_SETPOINT_LIMIT,   ZCL_TYPE_S16,         ZCL_ACCESS_READ_ONLY, &attrib_list->data.abs_min_heat_setpoint_limit);
    zcl_set_attrib(&attrib_list->list[4],   ZCL_THERMOSTAT_ATTRIB_ABS_MAX_HEAT_SETPOINT_LIMIT,   ZCL_TYPE_S16,         ZCL_ACCESS_READ_ONLY, &attrib_list->data.abs_max_heat_setpoint_limit);
    zcl_set_attrib(&attrib_list->list[5],   ZCL_THERMOSTAT_ATTRIB_ABS_MIN_COOL_SETPOINT_LIMIT,   ZCL_TYPE_S16,         ZCL_ACCESS_READ_ONLY, &attrib_list->data.abs_min_cool_setpoint_limit);
    zcl_set_attrib(&attrib_list->list[6],   ZCL_THERMOSTAT_ATTRIB_ABS_MAX_COOL_SETPOINT_LIMIT,   ZCL_TYPE_S16,         ZCL_ACCESS_READ_ONLY, &attrib_list->data.abs_max_cool_setpoint_limit);
    zcl_set_attrib(&attrib_list->list[7],   ZCL_THERMOSTAT_ATTRIB_PI_COOLING_DEMAND,             ZCL_TYPE_U8,          ZCL_ACCESS_READ_ONLY, &attrib_list->data.pi_cooling_demand);
    zcl_set_attrib(&attrib_list->list[8],   ZCL_THERMOSTAT_ATTRIB_PI_HEATING_DEMAND,             ZCL_TYPE_U8,          ZCL_ACCESS_READ_ONLY, &attrib_list->data.pi_heating_demand);

    zcl_set_attrib(&attrib_list->list[9],   ZCL_THERMOSTAT_ATTRIB_LOCAL_TEMPERATURE_CALIBRATION, ZCL_TYPE_S8,          ZCL_ACCESS_READ_WRITE, &attrib_list->data.local_temperature_calibration);
    zcl_set_attrib(&attrib_list->list[10],  ZCL_THERMOSTAT_ATTRIB_OCCUPIED_COOLING_SETPOINT,     ZCL_TYPE_S16,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.occupied_cooling_setpoint);
    zcl_set_attrib(&attrib_list->list[11],  ZCL_THERMOSTAT_ATTRIB_OCCUPIED_HEATING_SETPOINT,     ZCL_TYPE_S16,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.occupied_heating_setpoint);
    zcl_set_attrib(&attrib_list->list[12],  ZCL_THERMOSTAT_ATTRIB_UNOCCUPIED_COOLING_SETPOINT,   ZCL_TYPE_S16,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.unoccupied_cooling_setpoint);
    zcl_set_attrib(&attrib_list->list[13],  ZCL_THERMOSTAT_ATTRIB_UNOCCUPIED_HEATING_SETPOINT,   ZCL_TYPE_S16,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.unoccupied_heating_setpoint);
    zcl_set_attrib(&attrib_list->list[14],  ZCL_THERMOSTAT_ATTRIB_MIN_HEAT_SETPOINT_LIMIT,       ZCL_TYPE_S16,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.min_heat_setpoint_limit);
    zcl_set_attrib(&attrib_list->list[15],  ZCL_THERMOSTAT_ATTRIB_MAX_HEAT_SETPOINT_LIMIT,       ZCL_TYPE_S16,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.max_heat_setpoint_limit);
    zcl_set_attrib(&attrib_list->list[16],  ZCL_THERMOSTAT_ATTRIB_MIN_COOL_SETPOINT_LIMIT,       ZCL_TYPE_S16,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.min_cool_setpoint_limit);
    zcl_set_attrib(&attrib_list->list[17],  ZCL_THERMOSTAT_ATTRIB_MAX_COOL_SETPOINT_LIMIT,       ZCL_TYPE_S16,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.max_cool_setpoint_limit);
    zcl_set_attrib(&attrib_list->list[18],  ZCL_THERMOSTAT_ATTRIB_MIN_SETPOINT_DEAD_BAND,        ZCL_TYPE_S8,          ZCL_ACCESS_READ_WRITE, &attrib_list->data.min_setpoint_dead_band);
    zcl_set_attrib(&attrib_list->list[19],  ZCL_THERMOSTAT_ATTRIB_REMOTE_SENSING,                ZCL_TYPE_8BITMAP,     ZCL_ACCESS_READ_WRITE, &attrib_list->data.remote_sensing);
    zcl_set_attrib(&attrib_list->list[20],  ZCL_THERMOSTAT_ATTRIB_CONTROL_SEQUENCE_OF_OPERATION, ZCL_TYPE_8BIT_ENUM,   ZCL_ACCESS_READ_WRITE, &attrib_list->data.control_sequence_of_operation);
    zcl_set_attrib(&attrib_list->list[21],  ZCL_THERMOSTAT_ATTRIB_SYSTEM_MODE,                   ZCL_TYPE_8BIT_ENUM,   ZCL_ACCESS_READ_WRITE, &attrib_list->data.system_mode);
    zcl_set_attrib(&attrib_list->list[22],  ZCL_THERMOSTAT_ATTRIB_ALARM_MASK,                    ZCL_TYPE_8BITMAP,     ZCL_ACCESS_READ_ONLY,  &attrib_list->data.alarm_mask);

   
    zcl_set_attrib(&attrib_list->list[23], ZCL_END_MARKER, 0, 0, NULL);
}


/**************************************************************************/
/*!
        Handle the incoming frames targeted to the on/off cluster. This function
        processes the on/off cluster commands and sets the status to on, off, or
        toggles the value. The action handler will also handle any user defined
        actions that is related to the status.
*/
/**************************************************************************/
void zcl_thermostat_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
    

}



uint8_t zcl_thermostat_gen_req(uint8_t *data, zcl_hdr_t *hdr, zcl_thermostat_req_t *req)
{
    uint8_t len, *data_ptr;

    data_ptr = data;

    // gen the header
    len = zcl_gen_hdr(data_ptr, hdr);
    data_ptr += len;

    switch (hdr->cmd)
    {
        case ZCL_THERMOSTAT_CMD_SETPOINT_RAISE_LOWER:
            *data_ptr++ = req->setpoint.mode;
            *data_ptr++ = req->setpoint.amount;
            break;
        
        default:
            break;
    }
    // return the len
    return data_ptr - data;
}