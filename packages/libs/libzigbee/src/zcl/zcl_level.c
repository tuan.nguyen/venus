#include "zcl_level.h"

// function prototypes
static mem_ptr_t *zcl_level_tmr_alloc();
static void zcl_level_tmr_free(mem_ptr_t *mem_ptr);

/**************************************************************************/
/*!
    This is the timer list that will be used to keep track of the remaining
    time required to move a dimmer command to the required level.
*/
/**************************************************************************/
LIST(level_tmr_list);

//static struct ctimer level_tmr;     // this is the timer for the slow clock

/**************************************************************************/
/*!

*/
/**************************************************************************/
void zcl_level_init(zcl_level_attrib_list_t *attrib_list)
{
    // init the attrib data values first
    memset(&attrib_list->data, 0, sizeof(zcl_level_data_t));
    attrib_list->data.on_level = 0xfe;

    zcl_set_attrib(&attrib_list->list[0], ZCL_LEVEL_ATTRIB_CURR_LEVEL, ZCL_TYPE_U8, ZCL_ACCESS_READ_ONLY, &attrib_list->data.curr_level);
    zcl_set_attrib(&attrib_list->list[2], ZCL_LEVEL_ATTRIB_ON_OFF_TRANS_TIME, ZCL_TYPE_U16, ZCL_ACCESS_READ_WRITE, &attrib_list->data.on_off_trans_time);
    zcl_set_attrib(&attrib_list->list[3], ZCL_LEVEL_ATTRIB_ON_LEVEL, ZCL_TYPE_U8, ZCL_ACCESS_READ_WRITE, &attrib_list->data.on_level);
    zcl_set_attrib(&attrib_list->list[4], ZCL_END_MARKER, 0, 0, NULL);

    zigbee_list_init(level_tmr_list);
    zcl_level_tmr_periodic(NULL);
}

/**************************************************************************/
/*!

*/
/**************************************************************************/
void zcl_level_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
    zclLevel_t zclLevelReport;

    memset((char *)&zclLevelReport, 0, sizeof(zclLevel_t));

    uint8_t *dataPointer, status;
    //uint16_t attrib_id;
    dataPointer = hdr->payload;
    //attrib_id = *(uint16_t *)dataPointer;
    dataPointer += sizeof(uint16_t);
    status = *dataPointer++;

    if (status == ZCL_STATUS_SUCCESS)
    {
        zclLevelReport.addr = addr;
        zclLevelReport.type = *dataPointer++;
        zclLevelReport.data = *(uint8_t *)dataPointer;
    }
    *resp_len = 0;
    resp[0] = ZCL_LEVEL_NOTIFY;
    *resp_len = 1;
    memcpy(&resp[1], (uint8_t *)&zclLevelReport, sizeof(zclLevel_t));
    *resp_len = *resp_len + sizeof(zclLevel_t);
}

/**************************************************************************/
/*!

*/
/**************************************************************************/
uint8_t zcl_level_gen_req(uint8_t *data, zcl_hdr_t *hdr, zclLevelReq *req)
{
    uint8_t len, *data_ptr;

    len = zcl_gen_hdr(data, hdr);
    data_ptr = data + len;

    switch (hdr->cmd)
    {
    case ZCL_LEVEL_CMD_MOVE_TO_LEVEL:
    case ZCL_LEVEL_CMD_MOVE_TO_LEVEL_WITH_ON_OFF:
        *data_ptr++ = req->move_to_level.level;
        *(uint16_t *)data_ptr = req->move_to_level.trans_time;
        data_ptr += sizeof(uint16_t);
        break;

    case ZCL_LEVEL_CMD_MOVE:
        *data_ptr++ = req->move.dir;
        *(uint16_t *)data_ptr = req->move.rate;
        data_ptr += sizeof(uint16_t);
        break;

    case ZCL_LEVEL_CMD_STEP:
        *data_ptr++ = req->step.dir;
        *data_ptr++ = req->step.step_size;
        *(uint16_t *)data_ptr = req->step.trans_time;
        data_ptr += sizeof(uint16_t);
        break;

    default:
        break;
    }
    return data_ptr - data;
}

/**************************************************************************/
/*!

*/
/**************************************************************************/
static mem_ptr_t *zcl_level_tmr_alloc()
{
    mem_ptr_t *mem_ptr;

    if ((mem_ptr = mem_heap_alloc(sizeof(zcl_level_tmr_t))) != NULL)
    {
        zigbee_list_add(level_tmr_list, mem_ptr);
    }

    return mem_ptr;
}

/**************************************************************************/
/*!

*/
/**************************************************************************/
static void zcl_level_tmr_free(mem_ptr_t *mem_ptr)
{
    if (mem_ptr)
    {
        zigbee_list_remove(level_tmr_list, mem_ptr);
        mem_heap_free(mem_ptr);
    }
}

/**************************************************************************/
/*!

*/
/**************************************************************************/

/**************************************************************************/
/*!

*/
/**************************************************************************/
void zcl_level_tmr_add(uint8_t *level, uint16_t rem_time, uint8_t step, uint8_t rem, bool dir, bool on_off, void (*action_handler)(uint8_t, void *))
{
    mem_ptr_t *mem_ptr;

    if ((mem_ptr = zcl_level_tmr_alloc()) != NULL)
    {
        ZCL_LEVEL_TMR(mem_ptr)->level = level;
        ZCL_LEVEL_TMR(mem_ptr)->rem_time = rem_time;
        ZCL_LEVEL_TMR(mem_ptr)->rem = rem;
        ZCL_LEVEL_TMR(mem_ptr)->step = step;
        ZCL_LEVEL_TMR(mem_ptr)->dir = dir;
        ZCL_LEVEL_TMR(mem_ptr)->with_on_off = on_off;
        ZCL_LEVEL_TMR(mem_ptr)->action_handler = action_handler;
    }
}

/**************************************************************************/
/*!

*/
/**************************************************************************/
void zcl_level_tmr_periodic(void *ptr)
{
    mem_ptr_t *mem_ptr;
    uint8_t level, step, rem;
    uint16_t rem_time;
    bool dir;

    for (mem_ptr = zigbee_list_head(level_tmr_list); mem_ptr != NULL; mem_ptr = mem_ptr->next)
    {
        level = *ZCL_LEVEL_TMR(mem_ptr)->level;
        step = ZCL_LEVEL_TMR(mem_ptr)->step;
        rem_time = ZCL_LEVEL_TMR(mem_ptr)->rem_time;
        dir = ZCL_LEVEL_TMR(mem_ptr)->dir;
        rem = ZCL_LEVEL_TMR(mem_ptr)->rem;

        if (rem_time == 0)
        {
            // adjust the level value so that any small offset will be adjusted in the last step.
            *ZCL_LEVEL_TMR(mem_ptr)->level = level;

            // handle any actions by sending a refresh command to the action handler
            ZCL_LEVEL_TMR(mem_ptr)->action_handler(ZCL_LEVEL_ACTION_REFRESH, mem_ptr);

            // the time for this operation has expired. free the timer.
            zcl_level_tmr_free(mem_ptr);
        }
        else if ((level == 0xff) && (dir == ZCL_LEVEL_MODE_UP))
        {
            // we've reached the max level. inform the action handler in case any action is
            // to be taken. then free the timer.
            ZCL_LEVEL_TMR(mem_ptr)->action_handler(ZCL_LEVEL_ACTION_MAX_LEVEL, mem_ptr);
            zcl_level_tmr_free(mem_ptr);
        }
        else if ((level == 0x00) && (dir == ZCL_LEVEL_MODE_DOWN))
        {
            // we've reached the min level. inform the action handler in case any action is
            // to be taken. then free the timer.
            ZCL_LEVEL_TMR(mem_ptr)->action_handler(ZCL_LEVEL_ACTION_MIN_LEVEL, mem_ptr);
            zcl_level_tmr_free(mem_ptr);
        }
        else
        {
            // now adjust the level. we need to handle the roundoff error first, otherwise we run
            // the risk of overflowing.

            // this part is to handle the roundoff error accumulation.
            // add the remainder term to the accumulator to accumulate the roundoff error.
            // when it exceeds 0x7fff, then we will add one to the level. Just a note, the accumulator
            // is the fractional portion of the step size and it is normalized so that 0x8000 = 1. so
            // when we are > 0x7fff, then we've overflowed and need to adjust the step by 1 and subtract
            // 1 from the accumulator.
            ZCL_LEVEL_TMR(mem_ptr)->acc += rem;
            if (ZCL_LEVEL_TMR(mem_ptr)->acc > 0xff)
            {
                // subtract 1 from the accumulator
                ZCL_LEVEL_TMR(mem_ptr)->acc -= 0x100;

                // adjust the step size
                level = dir ? level - 1 : level + 1;
            }

            // limit the max and min values so that we don't overflow
            if (dir)
            {
                // we're moving downwards. if the level is less than the step size, then we
                // will set the level to 0. otherwise, just decrement the step size.
                *ZCL_LEVEL_TMR(mem_ptr)->level = (level < step) ? 0 : (level - step);
            }
            else
            {
                // we're moving upwards. if the step size is greater than the headroom between 0xff
                // and the current level, then we need to limit it to the max value.
                *ZCL_LEVEL_TMR(mem_ptr)->level = ((0xff - level) < step) ? 0xff : (level + step);
            }

            // decrement the remaining time
            ZCL_LEVEL_TMR(mem_ptr)->rem_time--;

            // handle any actions by sending a refresh command to the action handler
            ZCL_LEVEL_TMR(mem_ptr)->action_handler(ZCL_LEVEL_ACTION_REFRESH, mem_ptr);
        }
    }
    //ctimer_set(&level_tmr, CLOCK_SECOND/10, zcl_level_tmr_periodic, NULL);
}
