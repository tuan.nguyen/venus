#include "zcl_ms_humidity_measurement.h"

void zcl_ms_humidity_measurement_init(zcl_ms_humidity_measurement_attrib_list_t *attrib_list)
{
    // init the data values first
    memset(attrib_list, 0, sizeof(zcl_ms_humidity_measurement_attrib_list_t));
   
    // init the attribute fields
    zcl_set_attrib(&attrib_list->list[0],     ZCL_MS_HUMIDITY_MEASUREMENT_MEASURED_VALUE,          ZCL_TYPE_U16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.measured_value);
    zcl_set_attrib(&attrib_list->list[1],     ZCL_MS_HUMIDITY_MEASUREMENT_MIN_MEASURED_VALUE,      ZCL_TYPE_U16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.min_measured_value);
    zcl_set_attrib(&attrib_list->list[2],     ZCL_MS_HUMIDITY_MEASUREMENT_MAX_MEASURED_VALUE,      ZCL_TYPE_U16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.max_mesured_value);
    zcl_set_attrib(&attrib_list->list[3],     ZCL_MS_HUMIDITY_MEASUREMENT_TOLERANCE,     		   ZCL_TYPE_U16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.tolerance);

    //attrib end marker    
    zcl_set_attrib(&attrib_list->list[4], ZCL_END_MARKER, 0, 0, NULL);
   
}

void zcl_ms_humidity_measurement_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
	
}