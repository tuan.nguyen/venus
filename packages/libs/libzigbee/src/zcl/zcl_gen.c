#include "zcl_gen.h"
uint8_t zcl_gen_fcf(zcl_hdr_t *hdr)
{
	uint8_t fcf = 0;

    fcf |= (hdr->frm_ctrl.frm_type & 0x3);
    fcf |= (hdr->frm_ctrl.manuf_spec & 0x1)     << ZCL_MANUF_SPEC_OFF;
    fcf |= (hdr->frm_ctrl.dir & 0x1)            << ZCL_DIR_OFF;
    fcf |= (hdr->frm_ctrl.dis_def_resp & 0x1)   << ZCL_DIS_DEF_OFF;
    return fcf;
}

uint8_t zcl_gen_hdr(uint8_t *data, zcl_hdr_t *hdr)
{
	uint8_t *data_ptr;
	data_ptr=data;
	*data_ptr++=zcl_gen_fcf(hdr);
	if (hdr->frm_ctrl.manuf_spec)
    {
        *(uint16_t *)data_ptr = hdr->manuf_code;
        data_ptr += sizeof(uint16_t);
    }

    *data_ptr++ = hdr->seq_num;
    *data_ptr++ = hdr->cmd;

    return (data_ptr - data);
}

/**************************************************************************/
/*!
    Send the default response. This is the response when no other response
    is specified and will just mirror the command and the status. It can be
    suppressed by specifying to disable the default response in the incoming
    header.
*/
/**************************************************************************/
uint8_t zcl_gen_def_resp(uint8_t *resp, uint8_t status, zcl_hdr_t *hdr)
{
    uint8_t *resp_ptr, len;
    zcl_hdr_t resp_hdr;

    resp_ptr = resp;

    resp_hdr.frm_ctrl.dir            = ZCL_FRM_DIR_TO_CLI;
    resp_hdr.frm_ctrl.dis_def_resp   = true;
    resp_hdr.frm_ctrl.frm_type       = ZCL_FRM_TYPE_GENERAL;
    resp_hdr.frm_ctrl.manuf_spec     = false;
    resp_hdr.cmd                     = ZCL_CMD_DEFAULT_RESP;
    resp_hdr.seq_num                 = hdr->seq_num;

    len = zcl_gen_hdr(resp_ptr, &resp_hdr);
    resp_ptr += len;

    // payload is here. 1 byte for cmd, 1 byte for status
    *resp_ptr++ = hdr->cmd;
    *resp_ptr++ = status;

    return resp_ptr - resp;
}
