#include "zcl_on_off.h"

/**************************************************************************/
/*!
        Init the on/off cluster.
*/
/**************************************************************************/
void zcl_on_off_init(zcl_on_off_attrib_list_t *attrib_list)
{
    // init the data
    attrib_list->data.on_off = false;

    // init the attribs
    zcl_set_attrib(&attrib_list->list[0], ZCL_ON_OFF_ATTRIB, ZCL_TYPE_BOOL, ZCL_ACCESS_READ_WRITE, &attrib_list->data.on_off);
    zcl_set_attrib(&attrib_list->list[1], ZCL_END_MARKER, 0, 0, NULL);
}


/**************************************************************************/
/*!
        Handle the incoming frames targeted to the on/off cluster. This function
        processes the on/off cluster commands and sets the status to on, off, or
        toggles the value. The action handler will also handle any user defined
        actions that is related to the status.
*/
/**************************************************************************/
void zcl_on_off_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
    zclOnOff_t zclOnOffReport;

    memset((char *)&zclOnOffReport, 0, sizeof(zclOnOff_t));

    uint8_t *dataPointer, status;
    //uint16_t attrib_id;
    dataPointer = hdr->payload;
    //attrib_id = *(uint16_t *)dataPointer;
    dataPointer += sizeof(uint16_t);
    status = *dataPointer++;

    if (status == ZCL_STATUS_SUCCESS)
    {
        zclOnOffReport.addr = addr;
        zclOnOffReport.type = *dataPointer++;
        zclOnOffReport.data = *(uint8_t *)dataPointer;
    }
    *resp_len = 0;
    resp[0] = ZCL_ON_OFF_NOTIFY;
    *resp_len = 1;
    memcpy(&resp[1], (uint8_t *)&zclOnOffReport, sizeof(zclOnOff_t));
    *resp_len = *resp_len + sizeof(zclOnOff_t);
}



uint8_t zcl_on_off_gen_req(uint8_t *data, zcl_hdr_t *hdr, zclOnOffReq *req)
{
    uint8_t len, *data_ptr;

    data_ptr = data;

    // gen the header
    len = zcl_gen_hdr(data_ptr, hdr);
    data_ptr += len;

    switch (hdr->cmd)
    {
        case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_OFF:
            *data_ptr++ = req->relay_off.relay_number;
            break;

        case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_ON:
            *data_ptr++ = req->relay_on.relay_number;
            break;

        case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_TOGGLE:
            *data_ptr++ = req->relay_toggle.relay_number;
            break;
        
        case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SETRELAYS:
            *(uint32_t *)data_ptr = req->relay_setrelays.relay_pattern;
            data_ptr += sizeof(uint32_t);
            break;

        case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SETMODE:
            *data_ptr++ = req->relay_setmode.relay_mode;
            break;

        case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_PROG_ONOFF:
            *data_ptr++ = req->relay_prog_onoff.program_number;
            break;

        case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SETTIMERS:
            *data_ptr++ = req->relay_set_timer_values.timer_number;
            *data_ptr++ = req->relay_set_timer_values.timer_cnt;
            memcpy(data_ptr,req->relay_set_timer_values.timer_list,req->relay_set_timer_values.timer_cnt*sizeof(uint16_t));
            data_ptr +=req->relay_set_timer_values.timer_cnt*sizeof(uint16_t);
            break;

        case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_GETTIMERS:
            *data_ptr++ = req->relay_get_timer_values.timer_number;
            break;

        case ZCL_ON_OFF_CMD_MFG_SMARTENIT_PUMP_SET_CONFIGURATION:
            *(uint32_t *)data_ptr = req->pump_set_configuration.pump_config;
            data_ptr += sizeof(uint32_t);
            break;

        case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SETNAME:
            *data_ptr++ = req->relay_set_name.relay_number;
            *data_ptr++ = req->relay_set_name.relay_name_len;
            memcpy(data_ptr,req->relay_set_name.relay_name,req->relay_set_name.relay_name_len);
            break;

        case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_GETNAME:
            *data_ptr++ = req->relay_get_name.relay_number;
            break;
    }


    // return the len
    return data_ptr - data;
}