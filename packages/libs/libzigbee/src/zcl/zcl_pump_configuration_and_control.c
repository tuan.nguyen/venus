#include "zcl_pump_configuration_and_control.h"

/**************************************************************************/
/*!
        Init the on/off cluster.
*/
/**************************************************************************/
void zcl_pump_configuration_and_control_init(zcl_pump_configuration_and_control_attrib_list_t *attrib_list)
{
    // init the data
    memset(&attrib_list->data, 0, sizeof(zcl_pump_configuration_and_control_data_t));


    // init the attribs
    zcl_set_attrib(&attrib_list->list[0],   ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_PRESSURE,             ZCL_TYPE_S16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.max_pressure);
    zcl_set_attrib(&attrib_list->list[1],   ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_SPEED,                ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.max_speed);
    zcl_set_attrib(&attrib_list->list[2],   ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_FLOW,                 ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.max_flow);
    zcl_set_attrib(&attrib_list->list[3],   ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MIN_CONST_PRESSURE,       ZCL_TYPE_S16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.min_const_pressure);
    zcl_set_attrib(&attrib_list->list[4],   ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_CONST_PRESSURE,       ZCL_TYPE_S16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.max_const_pressure);
    zcl_set_attrib(&attrib_list->list[5],   ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MIN_COMP_PRESSURE,        ZCL_TYPE_S16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.min_comp_pressure);
    zcl_set_attrib(&attrib_list->list[6],   ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_COMP_PRESSURE,        ZCL_TYPE_S16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.max_comp_pressure);
    zcl_set_attrib(&attrib_list->list[7],   ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MIN_CONST_SPEED,          ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.min_const_speed);
    zcl_set_attrib(&attrib_list->list[8],   ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_CONST_SPEED,          ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.max_const_speed);
    zcl_set_attrib(&attrib_list->list[9],   ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MIN_CONST_FLOW,           ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.min_const_flow);
    zcl_set_attrib(&attrib_list->list[10],  ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_CONST_FLOW,           ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.max_const_flow);
    zcl_set_attrib(&attrib_list->list[11],  ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MIN_CONST_TEMP,           ZCL_TYPE_S16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.min_const_temp);
    zcl_set_attrib(&attrib_list->list[12],  ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_MAX_CONST_TEMP,           ZCL_TYPE_S16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.max_const_temp);

    zcl_set_attrib(&attrib_list->list[13],  ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_PUMP_STATUS,              ZCL_TYPE_16BITMAP,    ZCL_ACCESS_READ_WRITE, &attrib_list->data.pump_status);
    zcl_set_attrib(&attrib_list->list[14],  ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_EFFECTIVE_OPERATION_MODE, ZCL_TYPE_8BIT_ENUM,   ZCL_ACCESS_READ_WRITE, &attrib_list->data.effective_operation_mode);
    zcl_set_attrib(&attrib_list->list[15],  ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_EFFECTIVE_CONTROL_MODE,   ZCL_TYPE_8BIT_ENUM,   ZCL_ACCESS_READ_WRITE, &attrib_list->data.effective_control_mode);
    zcl_set_attrib(&attrib_list->list[16],  ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_CAPACITY,                 ZCL_TYPE_S16,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.capacity);
    zcl_set_attrib(&attrib_list->list[17],  ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_SPEED,                    ZCL_TYPE_U16,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.speed);
    zcl_set_attrib(&attrib_list->list[18],  ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_LIFETIME_RUNNING_HOURS,   ZCL_TYPE_U24,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.lifetime_running_hours);
    zcl_set_attrib(&attrib_list->list[19],  ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_POWER,                    ZCL_TYPE_U24,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.power);
    zcl_set_attrib(&attrib_list->list[20],  ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_LIFETIME_ENERGY_CONSUMED, ZCL_TYPE_U32,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.lifetime_energy_consumed);

    zcl_set_attrib(&attrib_list->list[21],  ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_OPERATION_MODE,           ZCL_TYPE_8BIT_ENUM,   ZCL_ACCESS_READ_WRITE, &attrib_list->data.operation_mode);
    zcl_set_attrib(&attrib_list->list[22],  ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_CONTROL_MODE,             ZCL_TYPE_8BIT_ENUM,   ZCL_ACCESS_READ_WRITE, &attrib_list->data.control_mode);
    zcl_set_attrib(&attrib_list->list[23],  ZCL_PUMP_CONFIG_AND_CONTROL_ATTRIB_ALARM_MASK,               ZCL_TYPE_16BITMAP,    ZCL_ACCESS_READ_ONLY,  &attrib_list->data.alarm_mask);

   
    zcl_set_attrib(&attrib_list->list[24], ZCL_END_MARKER, 0, 0, NULL);
}


/**************************************************************************/
/*!
        Handle the incoming frames targeted to the on/off cluster. This function
        processes the on/off cluster commands and sets the status to on, off, or
        toggles the value. The action handler will also handle any user defined
        actions that is related to the status.
*/
/**************************************************************************/
void zcl_pump_configuration_and_control_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
    

}



