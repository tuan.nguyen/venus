#include "zcl_temperature_measurement.h"

void zcl_temperature_measurement_init(zcl_temperature_measurement_attrib_list_t *attrib_list)
{
    // init the data values first
    memset(attrib_list, 0, sizeof(zcl_temperature_measurement_attrib_list_t));
   
    // init the attribute fields
    zcl_set_attrib(&attrib_list->list[0],     ZCL_TEMPERATURE_MEASUREMENT_MEASURED_VALUE,          ZCL_TYPE_S16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.mesured_value);
    zcl_set_attrib(&attrib_list->list[1],     ZCL_TEMPERATURE_MEASUREMENT_MIN_MEASURED_VALUE,      ZCL_TYPE_S16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.min_measured_value);
    zcl_set_attrib(&attrib_list->list[2],     ZCL_TEMPERATURE_MEASUREMENT_MAX_MEASURED_VALUE,      ZCL_TYPE_S16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.max_mesured_value);
    zcl_set_attrib(&attrib_list->list[3],     ZCL_TEMPERATURE_MEASUREMENT_TOLERANCE,     		   ZCL_TYPE_U16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.tolerance);
    
    //attrib end marker    
    zcl_set_attrib(&attrib_list->list[4], ZCL_END_MARKER, 0, 0, NULL);

}

void zcl_temperature_measurement_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
	temperatureMeasurement_t temperatureMeasurementReport;

    memset((char *)&temperatureMeasurementReport, 0, sizeof(temperatureMeasurement_t));

    uint8_t *dataPointer, status;
    //uint16_t attrib_id;
    dataPointer = hdr->payload;
    //attrib_id = *(uint16_t *)dataPointer;
    dataPointer += sizeof(uint16_t);
    status = *dataPointer++;

    if (status == ZCL_STATUS_SUCCESS)
    {
        temperatureMeasurementReport.addr = addr;
        temperatureMeasurementReport.type = *dataPointer++;
        temperatureMeasurementReport.temperatureLevel = *(uint16_t *)dataPointer;
    }
    *resp_len = 0;
    resp[0] = TEMPERATURE_MEASUREMENT_NOTIFY;
    *resp_len = 1;
    memcpy(&resp[1], (uint8_t *)&temperatureMeasurementReport, sizeof(temperatureMeasurement_t));
    *resp_len = *resp_len + sizeof(temperatureMeasurement_t);
}
