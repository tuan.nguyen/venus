#include "zcl_ias_zone.h"

void  zcl_ias_zone_init(zcl_ias_zone_attrib_list_t *attrib_list)
{
    // init the attrib data values first
    memset(&attrib_list->data, 0, sizeof(zcl_ias_zone_data_t));

    zcl_set_attrib(&attrib_list->list[0], ZCL_IAS_ZONE_ATTRIB_ZONE_STATE,          	ZCL_TYPE_U8,    	ZCL_ACCESS_READ_ONLY,   &attrib_list->data.zone_state);
    zcl_set_attrib(&attrib_list->list[2], ZCL_IAS_ZONE_ATTRIB_ZONE_TYPE,   			ZCL_TYPE_U16,   	ZCL_ACCESS_READ_ONLY,   &attrib_list->data.zone_type);
    zcl_set_attrib(&attrib_list->list[3], ZCL_IAS_ZONE_ATTRIB_ZONE_STATUS,          ZCL_TYPE_U16,   	ZCL_ACCESS_READ_ONLY,   &attrib_list->data.zone_status);
    zcl_set_attrib(&attrib_list->list[3], ZCL_IAS_ZONE_ATTRIB_IAS_CIE_ADDRESS,      ZCL_TYPE_IEEE_ADDR, ZCL_ACCESS_READ_WRITE,  &attrib_list->data.ias_cie_address);
    zcl_set_attrib(&attrib_list->list[4], ZCL_END_MARKER, 0, 0, NULL);

}

uint8_t zcl_ias_zone_gen_req(uint8_t *data, zcl_hdr_t *hdr, zclIasZoneReq *req)
{
	uint8_t len, *data_ptr;

    len = zcl_gen_hdr(data, hdr);
    data_ptr = data + len;

    switch (hdr->cmd)
    {
    	case ZCL_IAS_ZONE_CMD_ZONE_ENROLL_RESPONSE:
        	*data_ptr++ = req->zone_enroll_response.enroll_response_code;
        	*data_ptr++ = req->zone_enroll_response.zone_id;
        	break;

    	default:
        	break;
    }
    return data_ptr - data;
}

void  zcl_ias_zone_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
	uint8_t  *data_ptr;
	switch (hdr->cmd)
    {
    	case ZCL_IAS_ZONE_CMD_ZONE_STATUS_CHANGE_NOTIFICATION:
        {
    		//LOG(LOG_DBG,"Zone Change notification\n");
            //uint16_t zone_status;
            //uint8_t  extended_status;
            //debug_dump_buf(hdr->payload,hdr->payload_len);
            data_ptr = hdr->payload;
            //zone_status=*(uint16_t *)data_ptr;
            //data_ptr += sizeof(uint16_t);
            //LOG(LOG_DBG,"Zone status: %04X\n",zone_status);
            //extended_status=*(uint8_t *)data_ptr;
            //data_ptr += sizeof(uint8_t);
            //LOG(LOG_DBG,"Extended status: %02X\n", extended_status);

            iasNotification ias_notify;
            ias_notify.devSourceDesc.destAddr=addr;
            ias_notify.devSourceDesc.endpoint=endpoint;
            ias_notify.devSourceDesc.profileId=profile;
            ias_notify.devSourceDesc.clusterId=clust->clust_id;

            // ias_notify.alarm1               = (zone_status&ZONE_STATUS_ALARM1_BIT_MARK)?              true:false;
            // ias_notify.alarm2               = (zone_status&ZONE_STATUS_ALARM2_BIT_MARK)?              true:false;
            // ias_notify.tamper               = (zone_status&ZONE_STATUS_TAMPER_BIT_MARK)?              true:false;
            // ias_notify.battery              = (zone_status&ZONE_STATUS_BATTERY_BIT_MARK)?             true:false;
            // ias_notify.supervision_reports  = (zone_status&ZONE_STATUS_SUPERVISION_REPORTS_BIT_MARK)? true:false;
            // ias_notify.restore_reports      = (zone_status&ZONE_STATUS_RESTORE_REPORTS_BIT_MARK)?     true:false;
            // ias_notify.trouble              = (zone_status&ZONE_STATUS_TROUBLE_BIT_MARK)?             true:false;
            // ias_notify.ac_mains             = (zone_status&ZONE_STATUS_AC_MAINS_BIT_MARK)?            true:false;
            sprintf(ias_notify.alarm_pattern, "%02X", data_ptr[0]);
            sprintf(ias_notify.alarm_pattern, "%s%02X", ias_notify.alarm_pattern, data_ptr[1]);
            *resp_len=0;
            resp[0]=IAS_NOTIFICATION_NOTIFY;
            *resp_len=1;
            memcpy(&resp[1],(uint8_t*)&ias_notify,sizeof(iasNotification));
            *resp_len=*resp_len+sizeof(iasNotification);

    		break;
        }

    	case ZCL_IAS_ZONE_CMD_ZONE_ENROLL_REQUEST:
        {
    		//LOG(LOG_DBG,"Zone enroll request\n");
            //debug_dump_buf(hdr->payload,hdr->payload_len);
            uint16_t zone_type;
            uint16_t manufacturer_code;
            data_ptr = hdr->payload;
            zone_type=*(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            //LOG(LOG_DBG,"Zone type: %04X\n", zone_type);
            manufacturer_code=*(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            //LOG(LOG_DBG,"Manufacturer code: %04X\n",manufacturer_code);

            ias_zone_request ias_request_notify;
            ias_request_notify.devSourceDesc.destAddr=addr;
            ias_request_notify.devSourceDesc.endpoint=endpoint;
            ias_request_notify.devSourceDesc.profileId=profile;
            ias_request_notify.devSourceDesc.clusterId=clust->clust_id;

            ias_request_notify.zone_type=zone_type;
            ias_request_notify.manufacturer_code=manufacturer_code;

            *resp_len=0;
            resp[0]=IAS_ZONE_REQUEST_NOTIFY;
            *resp_len=1;
            memcpy(&resp[1],(uint8_t*)&ias_request_notify,sizeof(ias_zone_request));
            *resp_len=*resp_len+sizeof(ias_zone_request);
    		break;
        }

    	default:
    		break;
    }
}
