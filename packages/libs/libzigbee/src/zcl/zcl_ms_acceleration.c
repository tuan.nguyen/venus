#include "zcl_ms_acceleration.h"

/**************************************************************************/
/*!
        Init the on/off cluster.
*/
/**************************************************************************/
void zcl_ms_acceleration_init(zcl_ms_acceleration_attrib_list_t *attrib_list)
{
   
    zcl_set_attrib(&attrib_list->list[0], ZCL_END_MARKER, 0, 0, NULL);
}


/**************************************************************************/
/*!
        Handle the incoming frames targeted to the on/off cluster. This function
        processes the on/off cluster commands and sets the status to on, off, or
        toggles the value. The action handler will also handle any user defined
        actions that is related to the status.
*/
/**************************************************************************/
void zcl_ms_acceleration_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
    uint8_t  *data_ptr;
    ms_acceleration_cmd_msg acceleration_message;
    
    memset((char*)&acceleration_message,0,sizeof(ms_acceleration_cmd_msg));

    acceleration_message.devSourceDesc.destAddr=addr;
    acceleration_message.devSourceDesc.endpoint=endpoint;
    acceleration_message.devSourceDesc.profileId=profile;
    acceleration_message.devSourceDesc.clusterId=clust->clust_id;
    data_ptr = hdr->payload;
    switch (hdr->cmd)
    {
        case ZCL_MS_ACCELERATION_CMD_ACCELERATION_MSG:
        {
            acceleration_message.cmd=ZCL_MS_ACCELERATION_CMD_ACCELERATION_MSG;
            acceleration_message.acceleration_msg.axis_x=*(int16_t *)data_ptr;
            data_ptr += sizeof(int16_t);
            acceleration_message.acceleration_msg.axis_y=*(int16_t *)data_ptr;
            data_ptr += sizeof(int16_t);
            acceleration_message.acceleration_msg.axis_z=*(int16_t *)data_ptr;
            data_ptr += sizeof(int16_t);
            break;
        }
        case ZCL_MS_ACCELERATION_CMD_TEMPERATURE_BATTERY_MSG:
        {
            uint8_t temp_battery;
            int16_t temp_temperature;
            acceleration_message.cmd=ZCL_MS_ACCELERATION_CMD_TEMPERATURE_BATTERY_MSG;
            temp_temperature=*(int16_t *)data_ptr;
            data_ptr += sizeof(int16_t);
            acceleration_message.temperature_battery_msg.temperature=temp_temperature*10;
            temp_battery=*(uint8_t *)data_ptr;
            data_ptr += sizeof(uint8_t);
            acceleration_message.temperature_battery_msg.battery=(temp_battery>>2);
            break;
        }
        default:
            printf("ZCL_MS_ACCELERATION_CMD_UNKNOWN\n");
            *resp_len=0;
            return;
    }

    *resp_len=0;
    resp[0]=MS_ACCELERATION_MSG_NOTIFY;
    *resp_len=1;
    memcpy(&resp[1],(uint8_t*)&acceleration_message,sizeof(ms_acceleration_cmd_msg));
    *resp_len=*resp_len+sizeof(ms_acceleration_cmd_msg);
    return;
}



uint8_t zcl_ms_acceleration_gen_req(uint8_t *data, zcl_hdr_t *hdr)
{
    return 0;
}