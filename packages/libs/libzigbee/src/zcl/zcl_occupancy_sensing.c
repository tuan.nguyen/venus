#include "zcl_occupancy_sensing.h"

void zcl_occupancy_sensing_init(zcl_occupancy_sensing_attrib_list_t *attrib_list)
{
    // init the data values first
    memset(attrib_list, 0, sizeof(zcl_occupancy_sensing_attrib_list_t));
   
    // init the attribute fields
    zcl_set_attrib(&attrib_list->list[0],     ZCL_OCCUPANCY_SENSING_ATTRIB_OCCUPANCY,                                       ZCL_TYPE_8BITMAP,    ZCL_ACCESS_READ_ONLY,   &attrib_list->data.occupancy);
    zcl_set_attrib(&attrib_list->list[1],     ZCL_OCCUPANCY_SENSING_ATTRIB_OCCUPANCY_SENSOR_TYPE,                           ZCL_TYPE_8BIT_ENUM,  ZCL_ACCESS_READ_ONLY,   &attrib_list->data.occupancy_sensor_type);
    zcl_set_attrib(&attrib_list->list[2],     ZCL_OCCUPANCY_SENSING_ATTRIB_PIR_OCCUPIED_TO_UNOCCUPIED_DELAY,                ZCL_TYPE_U16,        ZCL_ACCESS_READ_WRITE,  &attrib_list->data.pir_occupied_to_unoccupied_delay);
    zcl_set_attrib(&attrib_list->list[3],     ZCL_OCCUPANCY_SENSING_ATTRIB_PIR_UNOCCUPIED_TO_OCCUPIED_DELAY,     		    ZCL_TYPE_U16,        ZCL_ACCESS_READ_WRITE,  &attrib_list->data.pir_unoccupied_to_occupied_delay);
    zcl_set_attrib(&attrib_list->list[4],     ZCL_OCCUPANCY_SENSING_ATTRIB_PIR_UNOCCUPIED_TO_OCCUPIED_THRESHOLD,            ZCL_TYPE_U16,        ZCL_ACCESS_READ_WRITE,  &attrib_list->data.pir_unoccupied_to_occupied_threshold);
    zcl_set_attrib(&attrib_list->list[5],     ZCL_OCCUPANCY_SENSING_ATTRIB_ULTRASONIC_OCCUPIED_TO_UNOCCUPIED_DELAY,         ZCL_TYPE_U16,        ZCL_ACCESS_READ_WRITE,  &attrib_list->data.ultrasonic_occupied_to_unoccupied_delay);
    zcl_set_attrib(&attrib_list->list[6],     ZCL_OCCUPANCY_SENSING_ATTRIB_ULTRASONIC_UNOCCUPIED_TO_OCCUPIED_DELAY,         ZCL_TYPE_U16,        ZCL_ACCESS_READ_WRITE,  &attrib_list->data.ultrasonic_unoccupied_to_occupied_delay);
    zcl_set_attrib(&attrib_list->list[7],     ZCL_OCCUPANCY_SENSING_ATTRIB_ULTRASONIC_UNOCCUPIED_TO_OCCUPIED_THRESHOLD,     ZCL_TYPE_U16,        ZCL_ACCESS_READ_WRITE,  &attrib_list->data.ultrasonic_unoccupied_to_occupied_threshold);

    //attrib end marker    
    zcl_set_attrib(&attrib_list->list[8], ZCL_END_MARKER, 0, 0, NULL);

}

void zcl_occupancy_sensing_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
	
}
