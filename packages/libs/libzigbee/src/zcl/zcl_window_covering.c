#include "zcl_window_covering.h"

/**************************************************************************/
/*!
        Init the on/off cluster.
*/
/**************************************************************************/
void zcl_window_covering_init(zcl_window_covering_attrib_list_t *attrib_list)
{
    // init the data
    memset(&attrib_list->data, 0, sizeof(zcl_window_covering_data_t));


    // init the attribs
    zcl_set_attrib(&attrib_list->list[0],   ZCL_WINDOW_COVERING_ATTRIB_WINDOW_COVERING_TYPE,             ZCL_TYPE_8BIT_ENUM,   ZCL_ACCESS_READ_ONLY, &attrib_list->data.window_covering_type);
    zcl_set_attrib(&attrib_list->list[1],   ZCL_WINDOW_COVERING_ATTRIB_PHYSICAL_CLOSED_LIMIT_LIFT,       ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY, &attrib_list->data.physical_closed_limit_lift);
    zcl_set_attrib(&attrib_list->list[2],   ZCL_WINDOW_COVERING_ATTRIB_PHYSICAL_CLOSED_LIMIT_TILT,       ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY, &attrib_list->data.physical_closed_limit_tilt);
    zcl_set_attrib(&attrib_list->list[3],   ZCL_WINDOW_COVERING_ATTRIB_CURRENT_POSITION_LIFT,            ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY, &attrib_list->data.current_position_lift);
    zcl_set_attrib(&attrib_list->list[4],   ZCL_WINDOW_COVERING_ATTRIB_CURRENT_POSITION_TILT,            ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY, &attrib_list->data.current_position_tilt);
    zcl_set_attrib(&attrib_list->list[5],   ZCL_WINDOW_COVERING_ATTRIB_NUMBER_OF_ACTUATIONS_LIFT,        ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY, &attrib_list->data.number_of_actuations_lift);
    zcl_set_attrib(&attrib_list->list[6],   ZCL_WINDOW_COVERING_ATTRIB_NUMBER_OF_ACTUATIONS_TILT,        ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY, &attrib_list->data.number_of_actuations_tilt);
    zcl_set_attrib(&attrib_list->list[7],   ZCL_WINDOW_COVERING_ATTRIB_CONFIG_STATUS,                    ZCL_TYPE_8BITMAP,     ZCL_ACCESS_READ_ONLY, &attrib_list->data.config_status);
    zcl_set_attrib(&attrib_list->list[8],   ZCL_WINDOW_COVERING_ATTRIB_CURRENT_POSITION_LIFT_PERCENTAGE, ZCL_TYPE_U8,          ZCL_ACCESS_READ_ONLY, &attrib_list->data.current_position_lift_percentage);
    zcl_set_attrib(&attrib_list->list[9],   ZCL_WINDOW_COVERING_ATTRIB_CURRENT_POSITION_TILT_PERCENTAGE, ZCL_TYPE_U8,          ZCL_ACCESS_READ_ONLY, &attrib_list->data.current_position_tilt_percentage);

    zcl_set_attrib(&attrib_list->list[10],  ZCL_WINDOW_COVERING_ATTRIB_INSTALLED_OPEN_LIMIT_LIFT,        ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.installed_open_limit_lift);
    zcl_set_attrib(&attrib_list->list[11],  ZCL_WINDOW_COVERING_ATTRIB_INSTALLED_CLOSED_LIMIT_LIFT,      ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.installed_closed_limit_lift);
    zcl_set_attrib(&attrib_list->list[12],  ZCL_WINDOW_COVERING_ATTRIB_INSTALLED_OPEN_LIMIT_TILT,        ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.installed_open_limit_tilt);
    zcl_set_attrib(&attrib_list->list[13],  ZCL_WINDOW_COVERING_ATTRIB_INSTALLED_CLOSED_LIMIT_TILT,      ZCL_TYPE_U16,         ZCL_ACCESS_READ_ONLY,  &attrib_list->data.installed_closed_limit_tilt);
    zcl_set_attrib(&attrib_list->list[14],  ZCL_WINDOW_COVERING_ATTRIB_VELOCITY_LIFT_SECOND,             ZCL_TYPE_U16,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.velocity_lift_second);
    zcl_set_attrib(&attrib_list->list[15],  ZCL_WINDOW_COVERING_ATTRIB_ACCELERATION_TIME_LIFT,           ZCL_TYPE_U16,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.acceleration_time_lift);
    zcl_set_attrib(&attrib_list->list[16],  ZCL_WINDOW_COVERING_ATTRIB_DECELERATION_TIME_LIFT,           ZCL_TYPE_U16,         ZCL_ACCESS_READ_WRITE, &attrib_list->data.deceleration_time_lift);
    zcl_set_attrib(&attrib_list->list[17],  ZCL_WINDOW_COVERING_ATTRIB_MODE,                             ZCL_TYPE_8BITMAP,     ZCL_ACCESS_READ_WRITE, &attrib_list->data.mode);
    zcl_set_attrib(&attrib_list->list[18],  ZCL_WINDOW_COVERING_ATTRIB_INTERMEDIATE_SETPOINT_LIFT,       ZCL_TYPE_CHAR_STRING, ZCL_ACCESS_READ_WRITE, attrib_list->data.intermediate_setpoint_lift);
    zcl_set_attrib(&attrib_list->list[19],  ZCL_WINDOW_COVERING_ATTRIB_INTERMEDIATE_SETPOINT_TILT,       ZCL_TYPE_CHAR_STRING, ZCL_ACCESS_READ_WRITE, attrib_list->data.intermediate_setpoint_tilt);

   
    zcl_set_attrib(&attrib_list->list[20], ZCL_END_MARKER, 0, 0, NULL);
}


/**************************************************************************/
/*!
        Handle the incoming frames targeted to the on/off cluster. This function
        processes the on/off cluster commands and sets the status to on, off, or
        toggles the value. The action handler will also handle any user defined
        actions that is related to the status.
*/
/**************************************************************************/
void zcl_window_covering_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
    

}



uint8_t zcl_window_covering_gen_req(uint8_t *data, zcl_hdr_t *hdr, zcl_window_covering_req_t *req)
{
    uint8_t len, *data_ptr;

    data_ptr = data;

    // gen the header
    len = zcl_gen_hdr(data_ptr, hdr);
    data_ptr += len;

    switch (hdr->cmd)
    {
        case ZCL_WINDOW_COVERING_CMD_GO_TO_LIFT_VALUE:
            *(uint16_t *)data_ptr = req->go_to_lift_value.lift_value;
            data_ptr += sizeof(uint16_t);
            break;
        case ZCL_WINDOW_COVERING_CMD_GO_TO_LIFT_PERCENTAGE:
            *data_ptr++ = req->go_to_lift_percentage.percentage_lift_value;
            break;
        case ZCL_WINDOW_COVERING_CMD_GO_TO_TILT_VALUE:
            *(uint16_t *)data_ptr = req->go_to_tilt_value.tilt_value;
            data_ptr += sizeof(uint16_t);
            break;
        case ZCL_WINDOW_COVERING_CMD_GO_TO_TILT_PERCENTAGE:
            *data_ptr++ = req->go_to_tilt_percentage.percentage_tilt_value;
            break;
        
        default:
            break;
    }
    // return the len
    return data_ptr - data;
}