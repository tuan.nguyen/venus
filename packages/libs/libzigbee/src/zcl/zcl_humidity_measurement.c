#include "zcl_humidity_measurement.h"

void zcl_humidity_measurement_init(zcl_humidity_measurement_attrib_list_t *attrib_list)
{
    // init the data values first
    memset(attrib_list, 0, sizeof(zcl_humidity_measurement_attrib_list_t));
   
    // init the attribute fields
    zcl_set_attrib(&attrib_list->list[0],     ZCL_HUMIDITY_MEASUREMENT_MEASURED_VALUE,          ZCL_TYPE_U16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.measured_value);
    zcl_set_attrib(&attrib_list->list[1],     ZCL_HUMIDITY_MEASUREMENT_MIN_MEASURED_VALUE,      ZCL_TYPE_U16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.min_measured_value);
    zcl_set_attrib(&attrib_list->list[2],     ZCL_HUMIDITY_MEASUREMENT_MAX_MEASURED_VALUE,      ZCL_TYPE_U16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.max_mesured_value);
    zcl_set_attrib(&attrib_list->list[3],     ZCL_HUMIDITY_MEASUREMENT_TOLERANCE,     		    ZCL_TYPE_U16,             ZCL_ACCESS_READ_ONLY,  &attrib_list->data.tolerance);

    //attrib end marker    
    zcl_set_attrib(&attrib_list->list[4], ZCL_END_MARKER, 0, 0, NULL);
   
}

void zcl_humidity_measurement_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
	humidityMeasurement_t humidityMeasurementReport;

    memset((char *)&humidityMeasurementReport, 0, sizeof(humidityMeasurement_t));

    uint8_t *dataPointer, status;
    //uint16_t attrib_id;
    dataPointer = hdr->payload;
    //attrib_id = *(uint16_t *)dataPointer;
    dataPointer += sizeof(uint16_t);
    status = *dataPointer++;

    if (status == ZCL_STATUS_SUCCESS)
    {
        humidityMeasurementReport.addr = addr;
        humidityMeasurementReport.type = *dataPointer++;
        humidityMeasurementReport.humidityLevel = *(uint16_t *)dataPointer;
    }
    *resp_len = 0;
    resp[0] = HUMIDITY_MEASUREMENT_NOTIFY;
    *resp_len = 1;
    memcpy(&resp[1], (uint8_t *)&humidityMeasurementReport, sizeof(humidityMeasurement_t));
    *resp_len = *resp_len + sizeof(humidityMeasurement_t);
}