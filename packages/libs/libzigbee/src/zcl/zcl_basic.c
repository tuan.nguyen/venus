#include "zcl_basic.h"

/**************************************************************************/
/*!
    Set the ZCL Basic Cluster attributes to their default values. These can
    be changed here or when you init the endpoint. The basic clusters are
        mostly read-only and just provide information about the device. The only
        action that is specified for them is to reset them to their default values.
*/
/**************************************************************************/
void zcl_basic_init(zcl_basic_attrib_list_t *attrib_list)
{
    char *manuf_id  = ZCL_MANUF_ID,
         *model_id  = ZCL_MODEL_ID,
         *date_code = ZCL_DATE_CODE,
         *loc_desc  = ZCL_LOC_DESC;

    // init the data values first
    memset(attrib_list, 0, sizeof(zcl_basic_attrib_list_t));
    attrib_list->data.zcl_ver     = ZCL_VER_NUM;
    attrib_list->data.app_ver     = ZCL_APP_VER_NUM;
    attrib_list->data.stack_ver   = ZCL_STACK_VER_NUM;
    attrib_list->data.hw_ver      = ZCL_HW_VER_NUM;
    attrib_list->data.pwr_src     = ZCL_BASIC_PWR_ENUM_MAINS_SINGLE;
    attrib_list->data.phys_env    = ZCL_BASIC_PHYS_ENV_UNSPECD;
    attrib_list->data.dev_enb     = true;
    attrib_list->data.alarm_msk   = 0;

    // init the string data. these need special processing.
    zcl_set_string_attrib(attrib_list->data.manuf_id, (uint8_t *)manuf_id, ZCL_MAX_STR_SZ);
    zcl_set_string_attrib(attrib_list->data.model_id, (uint8_t *)model_id, ZCL_MAX_STR_SZ);
    zcl_set_string_attrib(attrib_list->data.date_code, (uint8_t *)date_code, ZCL_MAX_STR_SZ/2);
    zcl_set_string_attrib(attrib_list->data.loc_desc, (uint8_t *)loc_desc, ZCL_MAX_STR_SZ/2);

    // init the attribute fields
    zcl_set_attrib(&attrib_list->list[0],     ZCL_BASIC_VER,          ZCL_TYPE_U8,             ZCL_ACCESS_READ_WRITE,  &attrib_list->data.zcl_ver);
    zcl_set_attrib(&attrib_list->list[1],     ZCL_BASIC_APP_VER,      ZCL_TYPE_U8,             ZCL_ACCESS_READ_WRITE,  &attrib_list->data.app_ver);
    zcl_set_attrib(&attrib_list->list[2],     ZCL_BASIC_STACK_VER,    ZCL_TYPE_U8,             ZCL_ACCESS_READ_WRITE,  &attrib_list->data.stack_ver);
    zcl_set_attrib(&attrib_list->list[3],     ZCL_BASIC_HW_VER,       ZCL_TYPE_U8,             ZCL_ACCESS_READ_WRITE,  &attrib_list->data.hw_ver);
    zcl_set_attrib(&attrib_list->list[4],     ZCL_BASIC_MANUF_NAME,   ZCL_TYPE_CHAR_STRING,    ZCL_ACCESS_READ_ONLY,    attrib_list->data.manuf_id);
    zcl_set_attrib(&attrib_list->list[5],     ZCL_BASIC_MODEL_ID,     ZCL_TYPE_CHAR_STRING,    ZCL_ACCESS_READ_ONLY,    attrib_list->data.model_id);
    zcl_set_attrib(&attrib_list->list[6],     ZCL_BASIC_DATE_CODE,    ZCL_TYPE_CHAR_STRING,    ZCL_ACCESS_READ_ONLY,    attrib_list->data.date_code);
    zcl_set_attrib(&attrib_list->list[7],     ZCL_BASIC_PWR_SRC,      ZCL_TYPE_U8,             ZCL_ACCESS_READ_ONLY,   &attrib_list->data.pwr_src);
    zcl_set_attrib(&attrib_list->list[8],     ZCL_BASIC_LOC_DESC,     ZCL_TYPE_CHAR_STRING,    ZCL_ACCESS_READ_WRITE,   attrib_list->data.loc_desc);
    zcl_set_attrib(&attrib_list->list[9],     ZCL_BASIC_PHYS_ENV,     ZCL_TYPE_U8,             ZCL_ACCESS_READ_WRITE,  &attrib_list->data.phys_env);
    zcl_set_attrib(&attrib_list->list[10],    ZCL_BASIC_DEV_ENB,      ZCL_TYPE_BOOL,           ZCL_ACCESS_READ_WRITE,  &attrib_list->data.dev_enb);
    zcl_set_attrib(&attrib_list->list[11],    ZCL_BASIC_ALARM_MSK,    ZCL_TYPE_U8,             ZCL_ACCESS_READ_WRITE,  &attrib_list->data.alarm_msk);
    zcl_set_attrib(&attrib_list->list[12],    ZCL_END_MARKER,         0,                       0,                      NULL);
}

/**************************************************************************/
/*!
        This is the rx handler for the ZCL basic cluster.

        TODO: Implement ZCL reset command if needed.
*/
/**************************************************************************/
void zcl_basic_rx_handler(uint8_t *resp, uint8_t *resp_len, uint16_t addr, uint8_t endpoint, uint16_t profile, zcl_clust_t *clust, zcl_hdr_t *hdr)
{
    if (hdr->cmd == ZCL_BASIC_CMD_RESET)
    {
        // do nothing...not sure how I want to handle a reset to factory defaults yet...
    }
    return;
}