#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>

#include "serial.h"
#include "cmd_handler.h"
#include "timer.h"
#include "interface-zigbee-telegesis-handler.h"

#define ZCL_MAX_BUF_SZ 128
#define ZCL_MAX_ATTRIBS 16
#define ZCL_EP 1
#define REMOTE_BINDING_MAX_NODE 32
#define PAN_SCAN_MAX_NODE 32

//#define LINUXx86_64

#define ZIGBEE_CONSOLE_PORT_DEFAULT "/dev/ttyUSB0"
#define GPIO_PIN_CTRL_NUM 48
#define GPIO_PIN_RST_NUM 60
#define GPIO_PIN_BOOTM_NUM 45
#define TIMEOUT_WAITING_ACK 25000
#define TIMEOUT_WAITING_SEQ 1000
#define WAITING_TRANSMIT_MULTINODE_BINARY 100000
#define WAITING_ZIGBEE_DETRUCT_COMPLETE 500000
#define WAITING_ZIGBEE_INIT_COMPLETE 1

#define ZB_JOIN_LIST "/etc/database/devicelist.db"

typedef enum //mode of zigbee device
{
    MODE_UNKNOW,
    MODE_NONLISTENING,
    MODE_ALWAYSLISTENING,
    MODE_FREQUENTLYLISTENING
}modeDeviceZigbee;
//static int zigbee_bulb_id = 1;
static remoteBindingNode remote_binding_table[REMOTE_BINDING_MAX_NODE];
static pan_scan_node pan_scan_list[PAN_SCAN_MAX_NODE];
static uint8_t remote_binding_table_index;
static uint8_t remote_binding_table_number_of_element;
static uint8_t pan_scan_list_index;
static uint8_t current_channel = 0;
static uint8_t find_out_touch_link_node = 0;
static uint8_t get_serial_number = 0;
static uint8_t get_table = 0;
static int rx_ack_num = 0;
static int rx_nack_num = 0;
static int rx_seq_num = 0;
static uint8_t rx_bind_status = 0xff;
static uint8_t rx_unbind_status = 0xff;
static uint8_t simple_endpoint_index = 0;
static uint8_t rx_get_euiAddr = 0;
static pthread_mutex_t CriticalMutexNotification = PTHREAD_MUTEX_INITIALIZER;
uint64_t controller_euiAddr;
static zigbee_notify_queue_t *g_notify_buf;
static grp_cmd_response *g_grp_rsp;

pthread_t thread_reader, thread_reader_pipe;

uint64_t htoi64(const char *ptr);
uint64_t GetParm(char **params, uint8_t parm, uint8_t base);
void *command_handling_loop(void *unused);
char *GetStringOfStatusCode(uint8_t statusCode);

void write_to_pipe(int file, char *data, int length);
void read_from_pipe(int file);
void app_zcl_rx_handler(uint8_t *data, uint8_t len, uint16_t src_addr, uint8_t srcEndpoint, uint16_t profile_id, uint16_t clust_id);
void zll_rx_handler(uint8_t *data, uint8_t len, uint8_t msg_type, uint16_t option, uint16_t pan_id, uint64_t src_addr, uint16_t profile_id, uint16_t clust_id);
void zdo_disc_dev_annce_req_handler(uint8_t *data, uint8_t len, uint16_t src_addr, uint8_t srcEndpoint, uint16_t clust);
void zdo_rx_handler(uint8_t *data, uint8_t len, uint16_t src_addr, uint8_t srcEndpoint, uint16_t clust_id);

bool bWaitForGroupResponseData = false;
int iWaitForGetInforNewNode = -1;
bool bGetSimpleDesc = false;
bool gOpenNetwork = false;
bool bWaitForReadAttributeData = false;
cmdRegister cmdRegisterList[256];

static readAttrData lastAttrData, get_readAttrData_cond;
nodeDescriptor lastNewNode;
static nodeLeft last_nodeLeft;

static void test_zcl_on_off_action_handler(uint8_t action, void *data);
static void test_zcl_level_action_handler(uint8_t action, void *data);

// Basic Cluster
static zcl_basic_attrib_list_t basic_attrib_list;
static zcl_clust_t basic_clust;

// Power Configuration Cluster
static zcl_power_configuration_attrib_list_t power_configuration_attrib_list;
static zcl_clust_t power_configuration_clust;

// ID Cluster
static zcl_id_attrib_list_t id_attrib_list;
static zcl_clust_t id_clust;

// Group Cluster
static zcl_grp_attrib_list_t grp_attrib_list;
static zcl_clust_t grp_clust;

// Scenes Cluster
static zcl_scenes_attrib_list_t scenes_attrib_list;
static zcl_clust_t scenes_clust;

//On-Off Cluster
static zcl_on_off_attrib_list_t on_off_attrib_list;
static zcl_clust_t on_off_clust;

//Level Cluster
static zcl_level_attrib_list_t level_attrib_list;
static zcl_clust_t level_clust;

//Thermostat Cluster
static zcl_thermostat_attrib_list_t thermostat_attrib_list;
static zcl_clust_t thermostat_clust;

//Window Covering Cluster
static zcl_window_covering_attrib_list_t window_covering_attrib_list;
static zcl_clust_t window_covering_clust;

//Pump configuration and control Cluster
static zcl_pump_configuration_and_control_attrib_list_t pump_configuration_and_control_attrib_list;
static zcl_clust_t pump_configuration_and_control_clust;

//Temperature measurement Cluster
static zcl_temperature_measurement_attrib_list_t temperature_measurement_attrib_list;
static zcl_clust_t temperature_measurement_clust;

//Illuminance measurement Cluster
static zcl_illuminance_measurement_attrib_list_t illuminance_measurement_attrib_list;
static zcl_clust_t illuminance_measurement_clust;

//Illuminance level sensing Cluster
static zcl_illuminance_level_sensing_attrib_list_t illuminance_level_sensing_attrib_list;
static zcl_clust_t illuminance_level_sensing_clust;

//Humidity measurement Cluster
static zcl_humidity_measurement_attrib_list_t humidity_measurement_attrib_list;
static zcl_clust_t humidity_measurement_clust;

//Manufacturer Specific Humidity measurement Cluster
static zcl_ms_humidity_measurement_attrib_list_t ms_humidity_measurement_attrib_list;
static zcl_clust_t ms_humidity_measurement_clust;

//Occupancy sensing Cluster
static zcl_occupancy_sensing_attrib_list_t occupancy_sensing_attrib_list;
static zcl_clust_t occupancy_sensing_clust;

//Manufacturer Specific Occupancy sensing Cluster
static zcl_ms_occupancy_sensing_attrib_list_t ms_occupancy_sensing_attrib_list;
static zcl_clust_t ms_occupancy_sensing_clust;

//IAS Zone Cluster
static zcl_ias_zone_attrib_list_t zcl_ias_zone_attrib_list;
static zcl_clust_t ias_zone_clust;

//Shade Configuration Cluster
static zcl_shade_configuration_attrib_list_t zcl_shade_configuration_attrib_list;
static zcl_clust_t shade_configuration_clust;

//Door Lock Cluster
static zcl_door_lock_attrib_list_t zcl_door_lock_attrib_list;
static zcl_clust_t door_lock_clust;

//Color Control Cluster
static zcl_color_control_attrib_list_t zcl_color_control_attrib_list;
static zcl_clust_t color_control_clust;

//Manufacturer Specific Acceleration Cluster
static zcl_ms_acceleration_attrib_list_t zcl_ms_acceleration_attrib_list;
static zcl_clust_t ms_acceleration_clust;

static zcl_clust_t end_maker_clust;

static zcl_clust_t *zcl_clust_list[] =
    {
        &basic_clust,
        &power_configuration_clust,
        &grp_clust,
        &scenes_clust,
        &on_off_clust,
        &id_clust,
        &level_clust,
        &thermostat_clust,
        &window_covering_clust,
        &pump_configuration_and_control_clust,
        &temperature_measurement_clust,
        &illuminance_measurement_clust,
        &illuminance_level_sensing_clust,
        &humidity_measurement_clust,
        &ms_humidity_measurement_clust,
        &occupancy_sensing_clust,
        &ms_occupancy_sensing_clust,
        &ias_zone_clust,
        &shade_configuration_clust,
        &door_lock_clust,
        &color_control_clust,
        &ms_acceleration_clust,
        &end_maker_clust};

int debug_level = 0;

int fd;
int uart_buffer[2];
static int g_command_done;

int waitFor_cmdRx_N = 0;
int waitFor_cmdRx_JPAN = 0;

static char g_blub_status;
static uint8_t g_network_info = 0;
static uint8_t g_addr_info = 0;
static uint8_t g_get_version = 0;

static zdo_handler_t zdo_handler_tbl[] =
    {
        /* requests */
        {DEV_ANNCE_REQ_CLUST, zdo_disc_dev_annce_req_handler},

        {END_MARKER_CLUST, NULL}};

void sig_handler(int signo)
{
    if (signo == SIGINT)
    {
        printf("handle SIGINT\n");
        free_uart(fd);
    }
    exit(0);
}
static timer_t timerNodeLeft = 0;
static int timerNodeLeftStatus = -1;

void timer_nodeleft_handler(void *data)
{
    printf("-----------------timer_nodeleft_handler-------------- \n");
    timerCancel(&timerNodeLeft);
    LOG(LOG_DBG, "notify node left to cli\n");
    //notify to cli
    pthread_mutex_lock(&CriticalMutexNotification);

    g_notify_buf->notify[g_notify_buf->notify_index].notify_status = NODELEFT_NOTIFY;
    memcpy(g_notify_buf->notify[g_notify_buf->notify_index].notify_message, (char *)&last_nodeLeft, sizeof(nodeLeft));
    g_notify_buf->notify_index = g_notify_buf->notify_index + 1;

    pthread_mutex_unlock(&CriticalMutexNotification);
    timerNodeLeftStatus = -1;
}

bool check_devSourceDesc(const sourceDesc *src, const sourceDesc *dest)
{
    if ((src->destAddr == dest->destAddr) &&
        (src->endpoint == dest->endpoint) &&
        (src->clusterId == dest->clusterId) &&
        (src->profileId == dest->profileId))
    {
        return true;
    }
    return false;
}

static uint8_t nibbleFromChar(char c)
{
    if (c >= '0' && c <= '9')
        return c - '0';
    if (c >= 'a' && c <= 'f')
        return c - 'a' + 10;
    if (c >= 'A' && c <= 'F')
        return c - 'A' + 10;
    return 255;
}

/* Convert a string of characters rendpointresenting a hex buffer into a series of bytes of that real value */
int hexStringToBytes(char *inhex, uint8_t *pData, uint8_t *pData_len)
{
    uint8_t *p;
    int i;

    *pData_len = strlen(inhex) / 2;
    for (i = 0, p = (uint8_t *)inhex; i < *pData_len; i++)
    {
        pData[i] = (nibbleFromChar(*p) << 4) | nibbleFromChar(*(p + 1));
        p += 2;
    }
    return 0;
}
command commands[] = {
    {"OK", Rx_OK},
    {"ERROR", Rx_ERROR},
    {"JPAN", Rx_JPAN},
    {"FFD", Rx_FFD},
    {"SED", Rx_SED},
    {"+N", Rx_N},
    {"ActEpDesc", Rx_ActEpDesc},
    {"SimpleDesc", Rx_SimpleDesc},
    {"ProfileID", Rx_ProfileID},
    {"DeviceID", Rx_DeviceID},
    {">", Rx_UCASTB},
    {"SEQ", Rx_SEQ},
    {"ACK", Rx_ACK},
    {"NACK", Rx_NACK},
    {"RX", Rx},
    {"AddrResp", Rx_AddrResp},
    {"+PANSCAN", Rx_PANSCAN},
    {"INTERPAN", Rx_INTERPAN},
    {"InCluster", Rx_InCluster},
    {"OutCluster", Rx_OutCluster},
    {"NEWNODE", Rx_NEWNODE},
    {"NODELEFT", Rx_NODELEFT},
    {"Bind", Rx_Bind},
    {"Unbind", Rx_Unbind},
    {"No. ", Rx_No},
    {"Length", Rx_Length},
    {"R309C", R309C}};
const int N_COMMANDS = sizeof(commands) / sizeof(command);

static int cmdTx_GetEndPointNumber(int fd, uint64_t Eui64, uint16_t NodeID)
{
    char atcmdBuffer[BUF_LINE];

    LOG(LOG_DBG, "cmdTx_GetEndPointNumber\n");
#ifdef LINUXx86_64
    sprintf(atcmdBuffer, "AT+ACTEPDESC:%016lX,%04X\r\n", Eui64, NodeID);
#else
    sprintf(atcmdBuffer, "AT+ACTEPDESC:%016llX,%04X\r\n", Eui64, NodeID);
#endif
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));
    return 0;
}

static int cmdTx_CreateBindingOnRemoteDevice(int fd, uint16_t target_addr, uint64_t src_addr, uint8_t srcEndpoint, uint16_t clust_id, uint64_t destAddr, uint8_t destEndpoint)
{
    char atcmdBuffer[BUF_LINE];

    LOG(LOG_DBG, "cmdTx_CreateBindingOnRemoteDevice\n");
#ifdef LINUXx86_64
    sprintf(atcmdBuffer, "AT+BIND:%04X,3,%016lX,%02X,%04X,%016lX,%02X\r\n", target_addr, src_addr, srcEndpoint, clust_id, destAddr, destEndpoint);
#else
    sprintf(atcmdBuffer, "AT+BIND:%04X,3,%016llX,%02X,%04X,%016llX,%02X\r\n", target_addr, src_addr, srcEndpoint, clust_id, destAddr, destEndpoint);
#endif
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));
    return 0;
}

static int cmdTx_DeleteBindingOnRemoteDevice(int fd, uint16_t target_addr, uint64_t src_addr, uint8_t srcEndpoint, uint16_t clust_id, uint64_t destAddr, uint8_t destEndpoint)
{
    char atcmdBuffer[BUF_LINE];

    LOG(LOG_DBG, "cmdTx_DeleteBindingOnRemoteDevice\n");
#ifdef LINUXx86_64
    sprintf(atcmdBuffer, "AT+UNBIND:%04X,3,%016lX,%02X,%04X,%016lX,%02X\r\n", target_addr, src_addr, srcEndpoint, clust_id, destAddr, destEndpoint);
#else
    sprintf(atcmdBuffer, "AT+UNBIND:%04X,3,%016llX,%02X,%04X,%016llX,%02X\r\n", target_addr, src_addr, srcEndpoint, clust_id, destAddr, destEndpoint);
#endif
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));
    return 0;
}

static int cmdTx_GetProfileId(int fd, uint64_t Eui64, uint16_t NodeID, uint8_t EndPoint_num)
{
    char atcmdBuffer[BUF_LINE];

    LOG(LOG_DBG, "cmdTx_GetProfileId\n");
#ifdef LINUXx86_64
    sprintf(atcmdBuffer, "AT+SIMPLEDESC:%016lX,%04X,%02X\r\n", Eui64, NodeID, EndPoint_num);
#else
    sprintf(atcmdBuffer, "AT+SIMPLEDESC:%016llX,%04X,%02X\r\n", Eui64, NodeID, EndPoint_num);
#endif
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));
    return 0;
}

static int cmdTx_RequestNodeEui64(int fd, uint64_t Eui64, uint16_t NodeID)
{
    char atcmdBuffer[BUF_LINE];

    LOG(LOG_DBG, "cmdTx_RequestNodeEui64\n");
#ifdef LINUXx86_64
    sprintf(atcmdBuffer, "AT+EUIREQ:%016lX,%04X\r\n", Eui64, NodeID);
#else
    sprintf(atcmdBuffer, "AT+EUIREQ:%016llX,%04X\r\n", Eui64, NodeID);
#endif
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));
    return 0;
}

void DispatchRX(uint16_t NWK_addr, uint16_t profileId, uint8_t desEndpoint, uint8_t scrEndpoint, uint16_t clusterId,
                uint8_t *pData, uint8_t pData_len)
{
    int i;
    LOG(LOG_DBG, "NWK_addr:%04X\n", NWK_addr);
    LOG(LOG_DBG, "profileId:%04X\n", profileId);
    LOG(LOG_DBG, "desEndpoint:%02X\n", desEndpoint);
    LOG(LOG_DBG, "scrEndpoint:%02X\n", scrEndpoint);
    LOG(LOG_DBG, "clusterId:%04X\n", clusterId);
    LOG(LOG_DBG, "begin");
    for (i = 0; i < pData_len; i++)
    {
        LOG(LOG_DBG, "%02X ", pData[i]);
    }
    LOG(LOG_DBG, "end");
}

int CtrlPointEvenHandler(int command, int fd, msg_receive msg)
{
    switch (command)
    {
    case Rx_OK:
    {
        LOG(LOG_DBG, "Rx_OK\n");
        g_command_done = 1;
        break;
    }
    case Rx_ERROR:
    {
        LOG(LOG_DBG, "Rx_ERROR\n");
        //printf("\nERROR string:%s \n",GetStringOfStatusCode(statusCode));
        break;
    }
    case Rx_JPAN:
    {
        LOG(LOG_DBG, "Rx_JPAN\n");

        break;
    }
    case Rx_FFD:
    {
        if (msg.number_parms < 2)
        {
            return -1;
        }

        uint16_t ffd_new_destAddr = GetParm(msg.parms, 1, XXXX);
        uint64_t ffd_eui64 = GetParm(msg.parms, 0, _EUI64_);
        if (iWaitForGetInforNewNode == 0)
        {
            if (ffd_new_destAddr == lastNewNode.destAddr)
            {//FFD:<EUI64>,<NWK addr>[,<RSSI>,<LQI>] A router announcing itself
                LOG(LOG_DBG, "Rx_FFD - NEWNODE\n");
                lastNewNode.capability = MODE_ALWAYSLISTENING;
                cmdTx_GetEndPointNumber(fd, lastNewNode.euiAddr, lastNewNode.destAddr);
                iWaitForGetInforNewNode = 1;
            }
        }
        else
        {
            //notify to cli
            nodeLeft last_node_update;
            last_node_update.euiAddr = ffd_eui64;
            last_node_update.destAddr = ffd_new_destAddr;

            pthread_mutex_lock(&CriticalMutexNotification);

            g_notify_buf->notify[g_notify_buf->notify_index].notify_status = UPDATE_NETWORK_ID_NOTIFY;
            memcpy(g_notify_buf->notify[g_notify_buf->notify_index].notify_message, (char *)&last_node_update, sizeof(nodeLeft));
            g_notify_buf->notify_index = g_notify_buf->notify_index + 1;

            pthread_mutex_unlock(&CriticalMutexNotification);
        }
        break;
    }

    case Rx_SED:
    {
        if (msg.number_parms < 2)
        {
            return -1;
        }

        uint16_t sed_new_destAddr = GetParm(msg.parms, 1, XXXX);
        uint64_t sed_eui64 = GetParm(msg.parms, 0, _EUI64_);

        if (iWaitForGetInforNewNode == 0)
        {
            if (sed_new_destAddr == lastNewNode.destAddr)
            {//SED:<EUI64>,<NWK addr>[,<RSSI>,<LQI>] A sleepy end device announcing itself
                LOG(LOG_DBG, "Rx_SED - NEWNODE\n");
                lastNewNode.capability = MODE_NONLISTENING;
                cmdTx_GetEndPointNumber(fd, lastNewNode.euiAddr, lastNewNode.destAddr);
                iWaitForGetInforNewNode = 1;
            }
        }
        else
        {
            //notify to cli

            nodeLeft last_node_update;
            last_node_update.euiAddr = sed_eui64;
            last_node_update.destAddr = sed_new_destAddr;

            if ((last_nodeLeft.euiAddr == last_node_update.euiAddr) && (last_node_update.destAddr == last_nodeLeft.destAddr) && (timerNodeLeftStatus == 1))
            {
                if (timerNodeLeft)
                {
                    timerCancel(&timerNodeLeft);
                }
                timerNodeLeftStatus = -1;
                last_nodeLeft.destAddr = 0;
            }
            else
            {

                pthread_mutex_lock(&CriticalMutexNotification);

                g_notify_buf->notify[g_notify_buf->notify_index].notify_status = UPDATE_NETWORK_ID_NOTIFY;
                memcpy(g_notify_buf->notify[g_notify_buf->notify_index].notify_message, (char *)&last_node_update, sizeof(nodeLeft));
                g_notify_buf->notify_index = g_notify_buf->notify_index + 1;

                pthread_mutex_unlock(&CriticalMutexNotification);
            }
        }
        break;
    }
    case Rx_N:
    {
        LOG(LOG_DBG, "Rx_N\n");
        g_network_info = msg.number_parms;
        if (msg.number_parms < 2)
        {
            current_channel = 0;
        }
        else
        {
            current_channel = strtol(msg.parms[1], NULL, 0);
        }
        break;
    }
    case Rx_ActEpDesc:
    {
        LOG(LOG_DBG, "Rx_ActEpDesc\n");
        uint16_t destAddr;
        int i;
        uint8_t EndPoint_num;
        if (iWaitForGetInforNewNode)
        {
            if (msg.number_parms > 2)
            {
                destAddr = (uint16_t)htoi64(msg.parms[0]);
                if (lastNewNode.destAddr == destAddr)
                {
                    lastNewNode.numEndpoint = msg.number_parms - 2;

                    for (i = 0; i < lastNewNode.numEndpoint; i++)
                    {
                        EndPoint_num = GetParm(msg.parms, i + 2, XX);
                        lastNewNode.clusterList[i].endpoint = EndPoint_num;
                    }
                    simple_endpoint_index = 0;
                    cmdTx_GetProfileId(fd, lastNewNode.euiAddr, lastNewNode.destAddr, lastNewNode.clusterList[simple_endpoint_index].endpoint);
                }
            }
        }
        break;
    }
    case Rx_SimpleDesc:
    {
        LOG(LOG_DBG, "RX_SimpleDesc\n");
        if (msg.number_parms > 1)
        {
            if (iWaitForGetInforNewNode)
            {
                uint8_t err = GetParm(msg.parms, 1, XXXX);
                uint16_t destAddr = GetParm(msg.parms, 0, XXXX);
                LOG(LOG_DBG, "err:%d", err);
                LOG(LOG_DBG, "destAddr:%04X", destAddr);
                if ((err == 0) && (lastNewNode.destAddr == destAddr))
                {
                    LOG(LOG_DBG, "bGetSimpleDesc");
                    bGetSimpleDesc = true;
                }
            }
        }
        break;
    }
    case Rx_ProfileID:
    {
        LOG(LOG_DBG, "Rx_ProfileID\n");

        if (msg.number_parms > 0)
        {
            if (bGetSimpleDesc)
            {
                lastNewNode.clusterList[simple_endpoint_index].profileId = GetParm(msg.parms, 0, XXXX);
            }
        }
        break;
    }
    case Rx_DeviceID:
    {
        LOG(LOG_DBG, "Rx_DeviceID\n");

        if (msg.number_parms > 0)
        {
            if (bGetSimpleDesc)
            {
                char temp[5];
                memcpy(&temp[0], msg.parms[0], 4);
                lastNewNode.clusterList[simple_endpoint_index].deviceId = htoi64(temp);
            }
        }
        break;
    }

    case Rx_UCASTB:
    {
        LOG(LOG_DBG, "RX_>>\n");
        g_command_done = 1;
        break;
    }

    case Rx_SEQ:
    {
        LOG(LOG_DBG, "Rx_SEQ\n");
        rx_seq_num = (uint8_t)htoi64(msg.parms[0]);
        break;
    }

    case Rx_ACK:
    {
        rx_ack_num = htoi64(msg.parms[0]);
        break;
    }
    case Rx_NACK:
    {
        rx_nack_num = htoi64(msg.parms[0]);
        break;
    }
    case Rx_NEWNODE:
    {
        LOG(LOG_DBG, "Rx_NEWNODE\n");
        if (msg.number_parms < 1)
            return -1;

        iWaitForGetInforNewNode = 0;
        memset((uint8_t *)&lastNewNode, 0, sizeof(nodeDescriptor));
        //lastNewNode.euiAddr = GetParm(msg.parms, 1, _EUI64_);
        printf("msg.parms: 0x%016llX\n", htoi64(msg.parms[1]));
        lastNewNode.euiAddr = htoi64(msg.parms[1]);
        lastNewNode.destAddr = GetParm(msg.parms, 0, XXXX);
        LOG(LOG_DBG, "Device Unsecured joined, destAddr, 0x%04X (%d), euiAddr: 0x%016llX (%016lld)",
            lastNewNode.destAddr, lastNewNode.destAddr, lastNewNode.euiAddr, lastNewNode.euiAddr);
        break;
    }
    case Rx_NODELEFT:
    {
        if(!gOpenNetwork)
        {
            break;
        }
        LOG(LOG_DBG, "Rx_NODELEFT\n");
        if (msg.number_parms < 1)
            return -1;
        nodeLeft current_nodeLeft;
        current_nodeLeft.euiAddr = GetParm(msg.parms, 1, _EUI64_);
        current_nodeLeft.destAddr = GetParm(msg.parms, 0, XXXX);
        if ((current_nodeLeft.euiAddr == last_nodeLeft.euiAddr) && (current_nodeLeft.destAddr == last_nodeLeft.destAddr))
        {
            LOG(LOG_DBG, "Rx_NODELEFT: Duplicate\n");
        }
        else
        {
            if (timerNodeLeftStatus == 1)
            {
                int timeout = 500;
                while (timeout-- > 0)
                {
                    if (timerNodeLeftStatus == 0)
                        break;
                    usleep(SLEEP_MILISECOND_UNIT);
                }
            }
            last_nodeLeft.euiAddr = current_nodeLeft.euiAddr;
            last_nodeLeft.destAddr = current_nodeLeft.destAddr;
            LOG(LOG_DBG, "timerStart\n");
            timerStart(&timerNodeLeft, timer_nodeleft_handler, NULL, 500, TIMER_ONETIME);
            timerNodeLeftStatus = 1;
        }
        break;
    }

    case Rx_Bind:
    {
        if (msg.number_parms > 1)
        {
            rx_bind_status = (uint8_t)htoi64(msg.parms[1]);
        }
        break;
    }

    case Rx_Unbind:
    {
        if (msg.number_parms > 1)
        {
            rx_unbind_status = (uint8_t)htoi64(msg.parms[1]);
        }
        break;
    }

    case Rx:
    {
        LOG(LOG_DBG, "RX\n");
        LOG(LOG_DBG, "number of params:%d\n", msg.number_parms);
        if (msg.number_parms > 7)
        {
            //uint64_t s_eui64;
            uint16_t s_nwk_addr;
            uint16_t s_profileId;
            //uint8_t  d_endpoint;
            uint8_t s_endpoint;
            uint16_t s_clusterId;
            //uint8_t  s_length;

            uint8_t pData_len;
            uint8_t pData[256];

            //s_eui64=htoi64(msg.parms[0]);
            s_nwk_addr = (uint16_t)htoi64(msg.parms[1]);
            s_profileId = (uint16_t)htoi64(msg.parms[2]);
            //d_endpoint=(uint8_t)htoi64(msg.parms[3]);
            s_endpoint = (uint8_t)htoi64(msg.parms[4]);
            s_clusterId = (uint16_t)htoi64(msg.parms[5]);
            //s_length=(uint8_t)htoi64(msg.parms[6]);

            LOG(LOG_DBG, "pData:%s\n", msg.parms[7]);
            hexStringToBytes(msg.parms[7], pData, &pData_len);
            //DispatchRX(s_nwk_addr,s_profileId,d_endpoint,s_endpoint,s_clusterId,pData,pData_len);
            if (s_profileId)
            {
                app_zcl_rx_handler(pData, pData_len, s_nwk_addr, s_endpoint, s_profileId, s_clusterId);
            }
            else
            {
                zdo_rx_handler(pData, pData_len, s_nwk_addr, s_endpoint, s_clusterId);
            }
        }
        else if (msg.number_parms == 7)
        {
            uint16_t s_nwk_addr;
            uint16_t s_profileId;
            //uint8_t  d_endpoint;
            uint8_t s_endpoint;
            uint16_t s_clusterId;
            //uint8_t  s_length;

            uint8_t pData_len;
            uint8_t pData[256];

            s_nwk_addr = (uint16_t)htoi64(msg.parms[0]);
            s_profileId = (uint16_t)htoi64(msg.parms[1]);
            //d_endpoint=(uint8_t)htoi64(msg.parms[2]);
            s_endpoint = (uint8_t)htoi64(msg.parms[3]);
            s_clusterId = (uint16_t)htoi64(msg.parms[4]);
            //s_length=(uint8_t)htoi64(msg.parms[5]);

            LOG(LOG_DBG, "pData:%s\n", msg.parms[6]);
            hexStringToBytes(msg.parms[6], pData, &pData_len);
            if (s_profileId)
            {
                app_zcl_rx_handler(pData, pData_len, s_nwk_addr, s_endpoint, s_profileId, s_clusterId);
            }
            else
            {
                zdo_rx_handler(pData, pData_len, s_nwk_addr, s_endpoint, s_clusterId);
            }
        }
        else
        {
            LOG(LOG_DBG, "RX next code\n");
        }
        break;
    }

    case Rx_PANSCAN:
    {
        LOG(LOG_DBG, "Rx_PANSCAN\n");
        LOG(LOG_DBG, "number of params:%d\n", msg.number_parms);
        if (msg.number_parms > 4)
        {
            pan_scan_list[pan_scan_list_index].channel = (uint8_t)strtol(msg.parms[0], NULL, 0);
            pan_scan_list[pan_scan_list_index].pid = (uint16_t)htoi64(msg.parms[1]);
            pan_scan_list[pan_scan_list_index].endpointid = (uint64_t)htoi64(msg.parms[2]);
            pan_scan_list[pan_scan_list_index].zigbee_stack = (uint8_t)htoi64(msg.parms[3]);
            pan_scan_list[pan_scan_list_index].is_allow_joining = (uint8_t)htoi64(msg.parms[4]);
            pan_scan_list_index++;
        }
    }
    break;

    case Rx_AddrResp:
    {
        LOG(LOG_DBG, "Rx_AddrResp\n");
        LOG(LOG_DBG, "number of params:%d\n", msg.number_parms);
        if (msg.number_parms > 1)
        {
            uint8_t err_code = (uint8_t)strtol(msg.parms[0], NULL, 0);
            LOG(LOG_DBG, "err_code:%02X", err_code);
            if (err_code == 0)
            {
                g_addr_info = 1;
            }
        }
    }
    break;

    case Rx_INTERPAN:
    {
        LOG(LOG_DBG, "INTERPAN\n");
        LOG(LOG_DBG, "number of params:%d\n", msg.number_parms);
        if (msg.number_parms > 8)
        {

            uint16_t inter_profile;
            uint16_t inter_clust;
            uint8_t inter_msg_type;
            uint16_t inter_option;
            //uint16_t inter_group;
            uint16_t inter_pan;
            uint64_t inter_src_addr;

            uint8_t inter_pData_len;
            uint8_t inter_pData[256];

            inter_profile = (uint16_t)htoi64(msg.parms[0]);
            inter_clust = (uint16_t)htoi64(msg.parms[1]);
            inter_msg_type = (uint8_t)htoi64(msg.parms[2]);
            inter_option = (uint16_t)htoi64(msg.parms[3]);
            //inter_group=(uint16_t)htoi64(msg.parms[4]);
            inter_pan = (uint16_t)htoi64(msg.parms[5]);
            inter_src_addr = htoi64(msg.parms[6]);

            LOG(LOG_DBG, "Inter pData:%s\n", msg.parms[8]);
            hexStringToBytes(msg.parms[8], inter_pData, &inter_pData_len);
            //app_zcl_rx_handler(pData,pData_len,s_nwk_addr,s_endpoint,s_profileId,s_clusterId);
            zll_rx_handler(inter_pData, inter_pData_len, inter_msg_type, inter_option, inter_pan, inter_src_addr, inter_profile, inter_clust);
        }
        else if (msg.number_parms == 8)
        {
            uint16_t inter_profile;
            uint16_t inter_clust;
            uint8_t inter_msg_type;
            uint16_t inter_option;
            uint16_t inter_pan;
            uint64_t inter_src_addr;

            uint8_t inter_pData_len;
            uint8_t inter_pData[256];

            inter_profile = (uint16_t)htoi64(msg.parms[0]);
            inter_clust = (uint16_t)htoi64(msg.parms[1]);
            inter_msg_type = (uint8_t)htoi64(msg.parms[2]);
            inter_option = (uint16_t)htoi64(msg.parms[3]);
            inter_pan = (uint16_t)htoi64(msg.parms[4]);
            inter_src_addr = htoi64(msg.parms[5]);

            LOG(LOG_DBG, "Inter pData:%s\n", msg.parms[7]);
            hexStringToBytes(msg.parms[7], inter_pData, &inter_pData_len);
            zll_rx_handler(inter_pData, inter_pData_len, inter_msg_type, inter_option, inter_pan, inter_src_addr, inter_profile, inter_clust);
        }
        else
        {
            LOG(LOG_DBG, "RX next code\n");
        }

        break;
    }
    case Rx_InCluster:
    {
        LOG(LOG_DBG, "RX_InCluster\n");
        //printf("msg.data = %s\n", msg.data);
        int i;
        //printf("msg.number_parms = %d\n", msg.number_parms);
        //if(msg.number_parms > 0)
        //{
        if (bGetSimpleDesc)
        {
            if (msg.number_parms > 0)
            {
                lastNewNode.clusterList[simple_endpoint_index].inClusterCount = msg.number_parms;
                for (i = 0; i < msg.number_parms; i++)
                {
                    lastNewNode.clusterList[simple_endpoint_index].inClusterList[i] = GetParm(msg.parms, i, XXXX);
                }
            }
            else
            {
                lastNewNode.clusterList[simple_endpoint_index].inClusterCount = 0;
            }
        }
        break;
    }

    case Rx_OutCluster:
    {
        LOG(LOG_DBG, "RX_OutCluster\n");
        //printf("msg.data = %s\n", msg.data);
        int i;
        //printf("msg.number_parms = %d\n", msg.number_parms);
        if (bGetSimpleDesc)
        {
            if (msg.number_parms > 0)
            {
                lastNewNode.clusterList[simple_endpoint_index].outClusterCount = msg.number_parms;
                for (i = 0; i < msg.number_parms; i++)
                {
                    lastNewNode.clusterList[simple_endpoint_index].outClusterList[i] = GetParm(msg.parms, i, XXXX);
                }
            }
            else
            {
                lastNewNode.clusterList[simple_endpoint_index].outClusterCount = 0;
            }
            LOG(LOG_DBG, "RX_OutCluster:number of outClusterList:%d\n", lastNewNode.clusterList[simple_endpoint_index].outClusterCount);

            if (simple_endpoint_index < lastNewNode.numEndpoint - 1)
            {
                LOG(LOG_DBG, "get next endpoint\n");
                simple_endpoint_index++;
                cmdTx_GetProfileId(fd, lastNewNode.euiAddr, lastNewNode.destAddr, lastNewNode.clusterList[simple_endpoint_index].endpoint);
            }
            else
            {
                LOG(LOG_DBG, "notify new node.................\n");
                lastNewNode.status = ADD_NODE_STATUS_DONE;
                pushNotificationZigbeeToHandler(NEWNODE_NOTIFY, (uint8_t *)&lastNewNode, sizeof(nodeDescriptor));
                bGetSimpleDesc = false;
                iWaitForGetInforNewNode = -1;
            }
        }
        break;
    }
    case Rx_No:
    {
        printf("\"msgnum1:%s\"\n", msg.parms[0]);
        if (!strcmp("     SrcAddr      ", (char *)msg.parms[0]))
        {
            get_table = 1;
            remote_binding_table_index = 0;
        }

        break;
    }
    case Rx_Length:
    {
        remote_binding_table_number_of_element = 0;
        if (msg.number_parms > 0)
        {
            remote_binding_table_number_of_element = (uint8_t)GetParm(msg.parms, 0, XX);
        }

        break;
    }
    case R309C:
    {
        g_get_version = 1;
        get_serial_number = 1;
        break;
    }
    }
    return 0;
}

uint64_t htoi64(const char *ptr)
{
    uint64_t value = 0;
    char ch = *ptr;

    while (ch == ' ' || ch == '\t')
        ch = *(++ptr);

    for (;;)
    {
        if (ch >= '0' && ch <= '9')
            value = (value << 4) + (ch - '0');
        else if (ch >= 'A' && ch <= 'F')
            value = (value << 4) + (ch - 'A' + 10);
        else if (ch >= 'a' && ch <= 'f')
            value = (value << 4) + (ch - 'a' + 10);
        else
        {
            return value;
        }

        ch = *(++ptr);
    }
}

uint64_t GetParm(char **parms, uint8_t parm, uint8_t base)
{
    switch (base)
    {
    case XX:
        return (htoi64(parms[parm]));
    case XXXX:
        return (htoi64(parms[parm]));
    case nn:
        break;
    case ss:
        break;
    case bb:
        break;
    case cc:
        break;
    case _PANID_:
        return (htoi64(parms[parm]));
    case _EPANID_:
        break;
    case _channel_:
        return (atoi(parms[parm]));
    case _password_:
        break;
    case _EUI64_:
        return (htoi64(parms[parm]));
        break;
    }
    return 0;
}

int SplitCommand(char *cmd_buffer, char **parms)
{
    int i, p;
    int number_parms = 0;
    for (i = 0; i < MAX_PARMS; i++)
    {
        parms[i] = NULL;
    }

    for (i = 0, p = 0; cmd_buffer[i] != NULLCHAR; i++)
    {
        if ((cmd_buffer[i] == ':') || (cmd_buffer[i] == ',') || (cmd_buffer[i] == '=') || (cmd_buffer[i] == '|'))
        {
            cmd_buffer[i] = NULLCHAR;
            parms[p++] = &cmd_buffer[i] + 1;
            number_parms++;
        }
    }

    return number_parms;
}

void ProcessCommand(int fd, void *bypass)
{
    int cmd;
    msg_receive msg = *((msg_receive *)bypass);
    if (get_serial_number)
    {
        char result[BUF_LINE];
        memset(result, 0x00, sizeof(result));
        controller_euiAddr = htoi64(msg.data);
        rx_get_euiAddr = 0;
        get_serial_number = 0;
    }
    else if (get_table)
    {
        printf("msg.data: %s\n", msg.data);
        msg.number_parms = SplitCommand(msg.data, msg.parms);
        if (!strcmp("ACK", (char *)msg.data))
        {
            if (remote_binding_table_index == remote_binding_table_number_of_element)
            {
                rx_ack_num = (uint8_t)GetParm(msg.parms, 0, XX);
                get_table = 0;
            }
        }

        printf("msg.number_parms: %d\n", msg.number_parms);
        if (msg.number_parms == 5)
        {
            remote_binding_table[remote_binding_table_index].eui_srcAddr = GetParm(msg.parms, 0, _EUI64_);
            remote_binding_table[remote_binding_table_index].srcEndpoint = (uint8_t)GetParm(msg.parms, 1, XX);
            remote_binding_table[remote_binding_table_index].cluster_id = (uint16_t)GetParm(msg.parms, 2, XXXX);
            remote_binding_table[remote_binding_table_index].euiDestAddr = GetParm(msg.parms, 3, _EUI64_);
            remote_binding_table[remote_binding_table_index].destEndpoint = (uint8_t)GetParm(msg.parms, 4, XX);
            remote_binding_table_index++;
        }
    }
    else
    {
        msg.number_parms = SplitCommand(msg.data, msg.parms);
        /*    int i;
        LOG(LOG_DBG, "ProcessCommand msg.number_parms = %d\n", msg.number_parms);
        for(i=0; i< msg.number_parms; i++)
        {
            LOG(LOG_DBG, "ProcessCommand msg.parms[%d] = %s\n", i, msg.parms[i]);
        }
        LOG(LOG_DBG, "ProcessCommand msg.data = %s\n", msg.data);*/
        for (cmd = 0; cmd < N_COMMANDS; cmd++)
        {
            if (!strcmp(commands[cmd].name, (char *)msg.data))
            {
                CtrlPointEvenHandler(cmd, fd, msg);
            }
        }
    }
}

void *command_handling_pipe_loop(void *unused)
{
    char c_array[1];
    char c;
    int fd = *((int *)unused);
    int cmd_buffer_index = 0, num_hex_input = 0, i;
    char cmd_buffer_pipe[BUF_LINE];
    LOG(LOG_DBG, "command_handling_pipe_loop fd = %04X\n", fd);
    while (1)
    {
        if (read(uart_buffer[0], c_array, 1) > 0)
        {
            c = c_array[0];
            if (c == LF && num_hex_input == 0)
            {
                msg_receive msg;
                LOG(LOG_DBG, "cmd_buffer_pipe = %s\n", cmd_buffer_pipe);
                memset(&msg, 0x00, sizeof(msg));
                for (i = 0; i < cmd_buffer_index; i++)
                {
                    msg.data[i] = cmd_buffer_pipe[i];
                }
                cmd_buffer_index = 0;
                cmd_buffer_pipe[cmd_buffer_index] = NULLCHAR;
                ProcessCommand(fd, &msg);
            }
            else
            {
                if (c == CR && num_hex_input == 0)
                    ;
                else
                {
                    cmd_buffer_pipe[cmd_buffer_index++] = c;
                    cmd_buffer_pipe[cmd_buffer_index] = NULLCHAR;
                }
                /*
                if(num_hex_input > 0)
                    num_hex_input--;
                */
            }
        }
        else
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
    }
}

void *command_handling_loop(void *unused)
{
    char c_array[1];
    char c;
    int index = 0, num_hex_input = 0;
    char cmd_buffer[BUF_LINE];
    int fd = *((int *)unused);
    LOG(LOG_DBG, "command_handling_loop fd = %04X\n", fd);
    int __catch__ = 0;
    while (1)
    {

        if (read(fd, c_array, 1) > 0)
        {
            //printf("c_array[0] = %x\n", c_array[0]);
            c = c_array[0];
            cmd_buffer[index++] = c;
            cmd_buffer[index] = NULLCHAR;

            if (c == ':' && strstr(cmd_buffer, "RX") && index > 3)
            {
                char length_hex[3];
                length_hex[0] = cmd_buffer[index - 3];
                length_hex[1] = cmd_buffer[index - 2];
                length_hex[2] = NULLCHAR;
                num_hex_input = atoi(length_hex);
            }

            if (c == LF && num_hex_input == 0)
            {

                if (strlen(cmd_buffer) > 2)
                {
                    LOG(LOG_DBG, "cmd_buffer = %s\n", cmd_buffer);
                    if (strstr(cmd_buffer, "AT+UCASTB") || strstr(cmd_buffer, "AT+MCASTB"))
                    {
                        __catch__ = 1;
                    }
                    write_to_pipe(uart_buffer[1], cmd_buffer, index);
                }
                index = 0;
                cmd_buffer[index] = NULLCHAR;

                //read_from_pipe(uart_buffer[0]);
                //printf("msg.data = %s\n", msg.data);
            }
            else
            {
                if ((c == '>') && __catch__)
                {
                    __catch__ = 0;
                    cmd_buffer[index++] = CR;
                    cmd_buffer[index++] = LF;
                    cmd_buffer[index] = NULLCHAR;
                    //printf("c= 0x3e cmd_buffer = %s\n", cmd_buffer);
                    write_to_pipe(uart_buffer[1], cmd_buffer, index);
                    index = 0;
                    cmd_buffer[index] = NULLCHAR;
                }

                if (num_hex_input > 0)
                    num_hex_input--;
            }
        }
        else
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
    }
}

int cmdTx_NetworkInformation(int fd)
{
    LOG(LOG_DBG, "cmdTx_NetworkInformation\n");
    int timeout = TIMEOUT_WAITING_ACK;
    char atcmdBuffer[BUF_LINE];
    sprintf(atcmdBuffer, "AT+N?\r\n");
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));

    g_network_info = 0;
    while ((!g_network_info) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }

    return g_network_info;
}

int cmdTx_ControllerInfor(int fd)
{
    LOG(LOG_DBG, "cmdTx_ControllerInfor\n");
    int timeout = 3000;
    char atcmdBuffer[BUF_LINE];
    sprintf(atcmdBuffer, "ATI\r\n");
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));
    g_get_version = 0;
    while ((!g_get_version) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }

    return 0;
}

int cmdTx_EstablishPersonalAreaNetwork(int fd)
{
    LOG(LOG_DBG, "cmdTx_EstablishPersonalAreaNetwork\n");
    char atcmdBuffer[BUF_LINE];

    sprintf(atcmdBuffer, "AT+EN\r\n");
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));

    return 0;
}

int cmdTx_S4x_Registers(int fd, uint8_t reg, uint16_t value)
{
    LOG(LOG_DBG, "cmdTx_S4x_Registers\n");

    char atcmdBuffer[BUF_LINE];
    int timeout = 2000;

    sprintf(atcmdBuffer, "ATS%02X=%04X\r\n", reg, value);
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));

    g_command_done = 0;
    while ((!g_command_done) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }

    return 0;
}

int cmdTx_Sxx_HA_Registers(int fd)
{
    LOG(LOG_DBG, "cmdTx_Sxx_HA_Registers\n");

    char atcmdBuffer[BUF_LINE];
    int timeout = 2000;

    sprintf(atcmdBuffer, "ATS00=6319\r\n");
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));

    g_command_done = 0;
    while ((!g_command_done) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }

    sprintf(atcmdBuffer, "ATS0A=0934;password\r\n");
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));

    g_command_done = 0;
    while ((!g_command_done) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }

    sprintf(atcmdBuffer, "ATS09=5A6967426565416C6C69616E63653039;password\r\n");
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));

    g_command_done = 0;
    while ((!g_command_done) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }

    sprintf(atcmdBuffer, "ATS48=0104\r\n");
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));

    g_command_done = 0;
    while ((!g_command_done) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }

    sprintf(atcmdBuffer, "ATS4C=0500\r\n");
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));

    g_command_done = 0;
    while ((!g_command_done) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }

    sprintf(atcmdBuffer, "ATS0F=1914\r\n");
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));

    g_command_done = 0;
    while ((!g_command_done) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }

    return 0;
}

int cmdTx_TransmitInterPANBinaryData(int fd, uint8_t addr_mode, uint64_t destAddr, uint16_t dest_PAN,
                                     uint16_t profile_id, uint16_t clust_id, uint8_t *pData, uint8_t pDataLen)
{
    LOG(LOG_DBG, "cmdTx_TransmitInterPANBinaryData\n");
    char payload[BUF_LINE] = "";
    int i;
    char atcmdBuffer[BUF_LINE];

    memset((char *)&payload, 0x00, BUF_LINE);

    for (i = 0; i < pDataLen; i++)
    {
        snprintf(&payload[strlen(payload)], BUF_LINE - strlen(payload), "%02X", pData[i]);
    }

    if (addr_mode == 0)
    {
        sprintf(atcmdBuffer, "AT+INTERPAN:00,%04X,%04X,%04X,%04X,%s\r\n", (uint16_t)destAddr, dest_PAN, profile_id, clust_id, payload);
        putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));
    }
    else if (addr_mode == 1)
    {
        sprintf(atcmdBuffer, "AT+INTERPAN:01,%04X,%04X,%04X,%04X,%s\r\n", (uint16_t)destAddr, dest_PAN, profile_id, clust_id, payload);
        putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));
    }
    else if (addr_mode == 2)
    {
#ifdef LINUXx86_64
        sprintf(atcmdBuffer, "AT+INTERPAN:02,%016lX,%04X,%04X,%04X,%s\r\n", destAddr, dest_PAN, profile_id, clust_id, payload);
#else
        sprintf(atcmdBuffer, "AT+INTERPAN:02,%016llX,%04X,%04X,%04X,%s\r\n", destAddr, dest_PAN, profile_id, clust_id, payload);
#endif
        putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));
    }
    return 0;
}

int cmdTx_TransmitUnicastBinaryData(int fd, uint16_t desNodeID, uint8_t *pData, uint8_t pDataLen)
{
    char atcmdBuffer[BUF_LINE];
    int timeout = 2000;

    LOG(LOG_DBG, "cmdTx_TransmitUnicastBinaryData\n");
    sprintf(atcmdBuffer, "AT+UCASTB:%02X,%04X\r", pDataLen, desNodeID);
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));
    g_command_done = 0;
    while ((!g_command_done) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: TransmitUnicast but no response from ZigBee module\n");
        return -1;
    }

    putbuffer_t(fd, pData, pDataLen);

    g_command_done = 0;
    timeout = 2000;
    while ((!g_command_done) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }

    return 0;
}

int cmdTx_TransmitMulticastBinaryData(int fd, uint16_t desGroupID, uint8_t *pData, uint8_t pDataLen)
{
    char atcmdBuffer[BUF_LINE];
    int timeout = 2000;

    LOG(LOG_DBG, "cmdTx_TransmitMulticastBinaryData\n");
    //sprintf(atcmdBuffer,"AT+MCASTB:%02X,00,%04X\r",pDataLen,desGroupID);
    sprintf(atcmdBuffer, "AT+MCASTB:%02X,%s,%04X\r", pDataLen, "00", desGroupID);
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));
    g_command_done = 0;
    while ((!g_command_done) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }

    putbuffer_t(fd, pData, pDataLen);

    g_command_done = 0;
    timeout = 2000;
    while ((!g_command_done) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }

    return 0;
}

int cmdTx_TransmitMultiNodeBinaryData(int fd, uint16_t *pNode, uint8_t nNode, uint8_t *m_endPoint, uint8_t *pData, uint8_t pDataLen)
{
    char atcmdBuffer[BUF_LINE];
    int timeout = 2000;
    int n = 0;
    uint16_t EndPoint;
    LOG(LOG_DBG, "cmdTx_TransmitMultiNodeBinaryData\n");
    for (n = 0; n < nNode; n++)
    {
        EndPoint = ((m_endPoint[n]) | 0x0100);
        cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
        sprintf(atcmdBuffer, "AT+UCASTB:%02X,%04X\r", pDataLen, pNode[n]);
        putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));
        g_command_done = 0;
        while ((!g_command_done) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("Fatal error: No response from ZigBee module\n");
            return -1;
        }

        putbuffer_t(fd, pData, pDataLen);

        g_command_done = 0;
        timeout = 2000;
        while ((!g_command_done) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("Fatal error: No response from ZigBee module\n");
            return -1;
        }
        usleep(WAITING_TRANSMIT_MULTINODE_BINARY);
    }
    return 0;
}

int cmdTx_RemoveNode(int fd, uint16_t desNodeID)
{
    char atcmdBuffer[BUF_LINE];
    int timeout = 2000;

    LOG(LOG_DBG, "cmdTx_RemoveNode\n");
    sprintf(atcmdBuffer, "AT+DASSR:%04X\r", desNodeID);
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));

    g_command_done = 0;
    while ((!g_command_done) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }

    return 0;
}

int cmdTx_Reset(int fd)
{
    char atcmdBuffer[BUF_LINE];
    int timeout = 2000;

    LOG(LOG_DBG, "cmdTx_Reset\n");
    sprintf(atcmdBuffer, "AT&F\r");
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));

    g_command_done = 0;
    while ((!g_command_done) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }

    return 0;
}

int cmdTx_SetOnOff(int fd, uint16_t desNodeID, uint8_t EndPoint_num, uint16_t ProfileID, char *value)
{
    uint16_t EndPoint = (((EndPoint_num)) | 0x0100);
    uint8_t pData[3] = {0x01, 0x01, 0x02};

    cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, 0x0006);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);
    if (!strcmp(value, "0"))
    {
        pData[2] = 0x00;
    }
    else if (!strcmp(value, "1"))
    {
        pData[2] = 0x01;
    }

    return cmdTx_TransmitUnicastBinaryData(fd, desNodeID, pData, 3);
}

int cmdTx_SetDimmer(int fd, uint16_t desNodeID, uint8_t EndPoint_num, uint16_t ProfileID, char *value)
{
    uint16_t EndPoint = (((EndPoint_num)) | 0x0100);
    uint8_t pData[6] = {0x01, 0x01, 0x04, 0x00, 0x00, 0x00};

    cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, 0x0008);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);
    pData[3] = (uint8_t)GetParm(&value, 0, XX);
    //printf("sizeof(pData) %d pData[3] = %02X", sizeof(pData), pData[3]);
    return cmdTx_TransmitUnicastBinaryData(fd, desNodeID, pData, sizeof(pData));
}

int cmdTx_GetStatus(int fd, uint16_t desNodeID, uint8_t EndPoint_num, uint16_t ProfileID)
{
    uint16_t EndPoint = (((EndPoint_num)) | 0x0100);
    uint8_t pData[5] = {0x00, 0x01, 0x00, 0x00, 0x00};

    //cmdTx_S4x_Registers(fd, S_REGISTER_Prompt_Enable_2, 0x0104);
    cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, 0x0006);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);
    g_blub_status = 0x00;
    if (!cmdTx_TransmitUnicastBinaryData(fd, desNodeID, pData, sizeof(pData)))
    {
        int timeout = 2000;
        while ((!g_blub_status) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("Fatal error: No response from ZigBee module\n");
            return 0;
        }

        return 1;
    }
    else
    {
        printf("Fatal error: No response from ZigBee module\n");
        return 0;
    }
}

void write_to_pipe(int file, char *data, int index)
{
    write(file, data, index); // remove the last NULL in data
}

void read_from_pipe(int file)
{
    char tmp[BUF_LINE];
    ssize_t nbytes;
    do
    {
        memset(tmp, 0x00, sizeof(tmp));
        nbytes = readln(file, tmp, BUF_LINE);
        if (nbytes > 0)
        {
            printf("tmp %s\n", tmp);
        }
    } while (nbytes > 0);
}

void app_zcl_init()
{
    zcl_init();
    /* init the clusters with the proper data */
    zcl_set_clust(&basic_clust,
                  ZCL_EP,
                  ZCL_BASIC_CLUST_ID,
                  basic_attrib_list.list,
                  zcl_basic_rx_handler,
                  NULL);

    zcl_set_clust(&power_configuration_clust,
                  ZCL_EP,
                  ZCL_PWR_CFG_CLUST_ID,
                  power_configuration_attrib_list.list,
                  zcl_power_configuration_rx_handler,
                  NULL);

    zcl_set_clust(&on_off_clust,
                  ZCL_EP,
                  ZCL_ON_OFF_CLUST_ID,
                  on_off_attrib_list.list,
                  zcl_on_off_rx_handler,
                  test_zcl_on_off_action_handler);

    zcl_set_clust(&id_clust,
                  ZCL_EP,
                  ZCL_IDENTIFY_CLUST_ID,
                  id_attrib_list.list,
                  zcl_id_rx_handler,
                  NULL);

    zcl_set_clust(&grp_clust,
                  ZCL_EP,
                  ZCL_GRP_CLUST_ID,
                  grp_attrib_list.list,
                  zcl_grp_rx_handler,
                  NULL);

    zcl_set_clust(&scenes_clust,
                  ZCL_EP,
                  ZCL_SCENES_CLUST_ID,
                  scenes_attrib_list.list,
                  zcl_scenes_rx_handler,
                  NULL);

    zcl_set_clust(&thermostat_clust,
                  ZCL_EP,
                  ZCL_THERMOSTAT_CLUST_ID,
                  thermostat_attrib_list.list,
                  zcl_thermostat_rx_handler,
                  NULL);

    zcl_set_clust(&window_covering_clust,
                  ZCL_EP,
                  ZCL_WINDOW_COVERING_CLUST_ID,
                  window_covering_attrib_list.list,
                  zcl_window_covering_rx_handler,
                  NULL);

    zcl_set_clust(&pump_configuration_and_control_clust,
                  ZCL_EP,
                  ZCL_PUMP_CONFIGURATION_AND_CONTROL_CLUST_ID,
                  pump_configuration_and_control_attrib_list.list,
                  zcl_pump_configuration_and_control_rx_handler,
                  NULL);

    zcl_set_clust(&level_clust,
                  ZCL_EP,
                  ZCL_LEVEL_CLUST_ID,
                  level_attrib_list.list,
                  zcl_level_rx_handler,
                  test_zcl_level_action_handler);

    zcl_set_clust(&temperature_measurement_clust,
                  ZCL_EP,
                  ZCL_TEMPERATURE_MEASUREMENT_CLUST_ID,
                  temperature_measurement_attrib_list.list,
                  zcl_temperature_measurement_rx_handler,
                  NULL);

    zcl_set_clust(&illuminance_measurement_clust,
                  ZCL_EP,
                  ZCL_ILLUMINANCE_MEASUREMENT_CLUST_ID,
                  illuminance_measurement_attrib_list.list,
                  zcl_illuminance_measurement_rx_handler,
                  NULL);

    zcl_set_clust(&illuminance_level_sensing_clust,
                  ZCL_EP,
                  ZCL_ILLUMINANCE_LEVEL_SENSING_CLUST_ID,
                  illuminance_level_sensing_attrib_list.list,
                  zcl_illuminance_level_sensing_rx_handler,
                  NULL);

    zcl_set_clust(&humidity_measurement_clust,
                  ZCL_EP,
                  ZCL_HUMIDITY_MEASUREMENT_CLUST_ID,
                  humidity_measurement_attrib_list.list,
                  zcl_humidity_measurement_rx_handler,
                  NULL);

    zcl_set_clust(&ms_humidity_measurement_clust,
                  ZCL_EP,
                  ZCL_MS_HUMIDITY_MEASUREMENT_CLUST_ID,
                  ms_humidity_measurement_attrib_list.list,
                  zcl_ms_humidity_measurement_rx_handler,
                  NULL);

    zcl_set_clust(&occupancy_sensing_clust,
                  ZCL_EP,
                  ZCL_OCCUPANCY_SENSING_CLUST_ID,
                  occupancy_sensing_attrib_list.list,
                  zcl_occupancy_sensing_rx_handler,
                  NULL);

    zcl_set_clust(&ms_occupancy_sensing_clust,
                  ZCL_EP,
                  ZCL_MS_OCCUPANCY_SENSING_CLUST_ID,
                  ms_occupancy_sensing_attrib_list.list,
                  zcl_ms_occupancy_sensing_rx_handler,
                  NULL);

    zcl_set_clust(&ias_zone_clust,
                  ZCL_EP,
                  ZCL_IAS_ZONE_CLUST_ID,
                  zcl_ias_zone_attrib_list.list,
                  zcl_ias_zone_rx_handler,
                  NULL);

    zcl_set_clust(&shade_configuration_clust,
                  ZCL_EP,
                  ZCL_SHADE_CONFIGURATION_CLUST_ID,
                  zcl_shade_configuration_attrib_list.list,
                  zcl_shade_configuration_rx_handler,
                  NULL);

    zcl_set_clust(&door_lock_clust,
                  ZCL_EP,
                  ZCL_DOOR_LOCK_CLUST_ID,
                  zcl_door_lock_attrib_list.list,
                  zcl_door_lock_rx_handler,
                  NULL);

    zcl_set_clust(&color_control_clust,
                  ZCL_EP,
                  ZCL_COLOR_CONTROL_CLUST_ID,
                  zcl_color_control_attrib_list.list,
                  zcl_color_control_rx_handler,
                  NULL);

    zcl_set_clust(&ms_acceleration_clust,
                  ZCL_EP,
                  ZCL_MS_ACCELERATION_CLUST_ID,
                  zcl_ms_acceleration_attrib_list.list,
                  zcl_ms_acceleration_rx_handler,
                  NULL);

    zcl_set_clust(&end_maker_clust,
                  0,
                  ZCL_END_MARKER,
                  NULL,
                  NULL,
                  NULL);

    /* init the attributes */
    zcl_basic_init(&basic_attrib_list);
    zcl_power_configuration_init(&power_configuration_attrib_list);
    zcl_on_off_init(&on_off_attrib_list);
    zcl_id_init(&id_attrib_list);
    zcl_grp_init(&grp_attrib_list);
    zcl_scenes_init(&scenes_attrib_list);
    zcl_level_init(&level_attrib_list);
    zcl_thermostat_init(&thermostat_attrib_list);
    zcl_window_covering_init(&window_covering_attrib_list);
    zcl_pump_configuration_and_control_init(&pump_configuration_and_control_attrib_list);
    zcl_temperature_measurement_init(&temperature_measurement_attrib_list);
    zcl_illuminance_measurement_init(&illuminance_measurement_attrib_list);
    zcl_illuminance_level_sensing_init(&illuminance_level_sensing_attrib_list);
    zcl_humidity_measurement_init(&humidity_measurement_attrib_list);
    zcl_ms_humidity_measurement_init(&ms_humidity_measurement_attrib_list);
    zcl_occupancy_sensing_init(&occupancy_sensing_attrib_list);
    zcl_ms_occupancy_sensing_init(&ms_occupancy_sensing_attrib_list);
    zcl_ias_zone_init(&zcl_ias_zone_attrib_list);
    zcl_shade_configuration_init(&zcl_shade_configuration_attrib_list);
    zcl_door_lock_init(&zcl_door_lock_attrib_list);
    zcl_color_control_init(&zcl_color_control_attrib_list);
}

static void remove_blank(char *text)
{
    int length, c, d;
    char *start;
    c = d = 0;
    length = strlen(text);
    start = (char *)malloc(length + 1);

    if (start == NULL)
    {
        exit(EXIT_FAILURE);
    }

    while (*(text + c) != '\0')
    {
        if (*(text + c) == ' ')
        {
            int temp = c + 1;
            if (*(text + temp) != '\0')
            {
                while (*(text + temp) == ' ' && *(text + temp) != '\0')
                {
                    if (*(text + temp) == ' ')
                    {
                        c++;
                    }
                    temp++;
                }
            }
        }
        *(start + d) = *(text + c);
        c++;
        d++;
    }

    *(start + d) = '\0';
    memcpy(text, start, length + 1);
    free(start);
    return;
}

static uint8_t parse_attrib_data(uint8_t type, uint8_t *data, uint16_t attrib)
{
    LOG(LOG_DBG, "parse_attrib_data");
    uint8_t size, str_buf[128], *data_ptr = data;
    switch (type)
    {
    case ZCL_TYPE_8BIT_ENUM:
        LOG(LOG_DBG, "TYPE: %s, ", "8BIT ENUMERATION");
        LOG(LOG_DBG, "DATA: 0x%02X", *data_ptr);
        bWaitForReadAttributeData = false;
        break;
    case ZCL_TYPE_U8:
        LOG(LOG_DBG, "TYPE: %s, ", "UNSIGNED CHAR");
        LOG(LOG_DBG, "DATA: 0x%02X", *data_ptr);
        bWaitForReadAttributeData = false;
        break;
    case ZCL_TYPE_U16:
        LOG(LOG_DBG, "TYPE: %s, ", "UNSIGNED INT");
        LOG(LOG_DBG, "DATA: 0x%04X", *(uint16_t *)data_ptr);
        bWaitForReadAttributeData = false;
        break;
    case ZCL_TYPE_U32:
        LOG(LOG_DBG, "TYPE: %s, ", "UNSIGNED WORD");
        LOG(LOG_DBG, "DATA: 0x%08X", *(uint32_t *)data_ptr);
        bWaitForReadAttributeData = false;
        break;
    case ZCL_TYPE_BOOL:
        LOG(LOG_DBG, "TYPE: %s, ", "BOOL");
        LOG(LOG_DBG, "DATA: %s", *data_ptr ? "TRUE" : "FALSE");
        bWaitForReadAttributeData = false;
        break;
    case ZCL_TYPE_CHAR_STRING:
        // byte 0 is the length of the string
        size = *(uint8_t *)data_ptr++;
        LOG(LOG_DBG, "size :%0d\n", size);
        // start the copy from byte 1
        memcpy(str_buf, data_ptr, size);
        data_ptr += size;
        str_buf[size] = 0;
        if ((bWaitForReadAttributeData) &&
            (check_devSourceDesc(&get_readAttrData_cond.devSourceDesc, &lastAttrData.devSourceDesc)) &&
            (get_readAttrData_cond.attributeID == attrib))
        {
            lastAttrData.type = ZCL_TYPE_CHAR_STRING;
            memcpy((uint8_t *)&lastAttrData.string_data, str_buf, size + 1);
            remove_blank((char *)&lastAttrData.string_data);
            LOG(LOG_DBG, "lastAttrData.string_data:%s\n", lastAttrData.string_data);
            bWaitForReadAttributeData = false;
        }
        else
        {
            //notify
        }
        LOG(LOG_DBG, "TYPE: %s, ", "CHAR STRING");
        LOG(LOG_DBG, "DATA: %s", str_buf);
        break;

    case ZCL_TYPE_S16:
        lastAttrData.type = ZCL_TYPE_S16;
        lastAttrData.int16_t_data = *(int16_t *)data_ptr;
        //notify to cli
        pthread_mutex_lock(&CriticalMutexNotification);
        //g_notify->notify_status=REPORT_ATTR_NOTIFY;
        //memcpy((char*)g_notify->notify_message,(char*)&lastAttrData,sizeof(readAttrData));

        g_notify_buf->notify[g_notify_buf->notify_index].notify_status = REPORT_ATTR_NOTIFY;
        memcpy(g_notify_buf->notify[g_notify_buf->notify_index].notify_message, (char *)&lastAttrData, sizeof(readAttrData));
        g_notify_buf->notify_index = g_notify_buf->notify_index + 1;

        pthread_mutex_unlock(&CriticalMutexNotification);
        LOG(LOG_DBG, "TYPE: %s, ", "SIGNED  INT16");
        LOG(LOG_DBG, "DATA: %d", *(int16_t *)data_ptr);
        bWaitForReadAttributeData = false;
        break;

    default:
        break;
    }

    return data_ptr - data;
}

/* Decode and dump the ZCL frame */
void parse_zcl_frm(uint8_t *data, zcl_hdr_t *hdr, uint16_t clust)
{
    uint8_t i, *data_ptr, status, type, len;
    uint16_t attrib_id;

    //show infor of frame header
    LOG(LOG_DBG, "\n");
    LOG(LOG_DBG, "DUMP_ZCL_FRM: FRM TYPE       = %s.\n",
        hdr->frm_ctrl.frm_type ? "SPECIFIC" : "GENERAL");
    LOG(LOG_DBG, "DUMP_ZCL_FRM: MANUF SPECIFIC = %s.\n",
        hdr->frm_ctrl.manuf_spec ? "TRUE" : "FALSE");
    LOG(LOG_DBG, "DUMP_ZCL_FRM: DIRECT         = %s.\n",
        hdr->frm_ctrl.dir ? "TO CLIENT" : "TO SERVER");
    LOG(LOG_DBG, "DUMP_ZCL_FRM: DIS DEF RESP   = %s.\n",
        hdr->frm_ctrl.dis_def_resp ? "TRUE" : "FALSE");

    if (hdr->frm_ctrl.manuf_spec)
        LOG(LOG_DBG, "DUMP_ZCL_FRM: MANUF CODE     = %4X.\n", hdr->manuf_code);

    LOG(LOG_DBG, "DUMP_ZCL_FRM: SEQ NUM        = %02X.\n", hdr->seq_num);

    if (hdr->frm_ctrl.frm_type == ZCL_FRM_TYPE_GENERAL)
    {
        switch (hdr->cmd)
        {
        case ZCL_CMD_READ_ATTRIB:
            LOG(LOG_DBG, "DUMP_ZCL_FRM: CMD            = %s.\n", "READ ATTRIBUTES");
            LOG(LOG_DBG, "DUMP_ZCL_FRM: ATTRIBS: ");

            data_ptr = hdr->payload;
            for (i = 0; i < (hdr->payload_len) / 2; i++)
            {
                LOG(LOG_DBG, "%04X ", *(uint16_t *)data_ptr);
                data_ptr += sizeof(uint16_t);
            }
            LOG(LOG_DBG, "\n");
            break;
        case ZCL_CMD_READ_ATTRIB_RESP:
            LOG(LOG_DBG, "DUMP_ZCL_FRM: CMD            = %s.\n", "READ ATTRIBUTE RESPONSE");

            data_ptr = hdr->payload;
            while ((data_ptr - hdr->payload) < hdr->payload_len)
            {
                attrib_id = *(uint16_t *)data_ptr;
                data_ptr += sizeof(uint16_t);

                LOG(LOG_DBG, "ATTRIB ID: %04X", attrib_id);
                //lastAttrData.attributeID = attrib_id;

                status = *data_ptr++;
                LOG(LOG_DBG, ", STATUS: %02X", status);

                if (status == ZCL_STATUS_SUCCESS)
                {
                    /*
                     * since the data is variable, there is
                     * a special function to handle dumping it.
                     * just pass in the type and the data ptr.
                     * then increment the data ptr when we're done.
                     */
                    type = *data_ptr++;
                    len = parse_attrib_data(type, data_ptr, attrib_id);
                    data_ptr += len;
                }
                LOG(LOG_DBG, "\n");
            }
            break;
        case ZCL_CMD_WRITE_ATTRIB_RESP:
            LOG(LOG_DBG, "DUMP_ZCL_FRM: CMD            = %s.\n", "WRITE ATTRIBUTE RESP");

            data_ptr = hdr->payload;
            while ((data_ptr - hdr->payload) < hdr->payload_len)
            {
                status = *data_ptr++;
                attrib_id = *(uint16_t *)data_ptr;
                data_ptr += sizeof(uint16_t);
                LOG(LOG_DBG, "Attrib ID: %04X, ", attrib_id);
                LOG(LOG_DBG, "DUMP_ZCL_FRM: STATUS         = %s.\n",
                    debug_dump_zcl_status(status));
            }
            break;
        case ZCL_CMD_REPORT_ATTRIB:
            LOG(LOG_DBG, "DUMP_ZCL_FRM: CMD            = %s.\n", "REPORT ATTRIBUTES");
            data_ptr = hdr->payload;

            attrib_id = *(uint16_t *)data_ptr;
            data_ptr += sizeof(uint16_t);
            type = *data_ptr++;
            LOG(LOG_DBG, "DUMP_ZCL_FRM: ATTRIB ID = %04X, ", attrib_id);
            lastAttrData.attributeID = attrib_id;
            len = parse_attrib_data(type, data_ptr, attrib_id);
            data_ptr += len;
            LOG(LOG_DBG, "\n");
            break;

            //     case ZCL_CMD_DEFAULT_RESP:
            //         data_ptr = hdr->payload;
            //         LOG(LOG_DBG,"DUMP_ZCL_FRM: CMD            = %s.\n", "DEFAULT RESPONSE");
            //         LOG(LOG_DBG,"DUMP_ZCL_FRM: CMD ID         = %s, ",
            //               debug_dump_zcl_cmd(clust, *data_ptr));
            //         data_ptr++;
            //         LOG(LOG_DBG,"DUMP_ZCL_FRM: STATUS         = %s, ", debug_dump_zcl_status(*data_ptr));
            //         break;
            //     }
        }
    }
}

int zdo_parse_req(uint16_t addr, uint8_t *data_in, uint16_t clust, zdo_req_t *req)
{
    uint8_t *data = data_in;

    req->clust = clust;
    req->seq = *data++;

    switch (clust)
    {
    case DEV_ANNCE_REQ_CLUST:
        req->type.dev_annce.nwk_addr = *(uint16_t *)data;
        data += sizeof(uint16_t);
        req->type.dev_annce.ext_addr = *(uint64_t *)data;
        data += sizeof(uint64_t);
        req->type.dev_annce.capab_info = *data++;
        break;
    default:
        return -1;
    }
    //debug_dump_zdo_request(req);
    return 0;
}
void zdo_disc_dev_annce_req_handler(uint8_t *data, uint8_t len, uint16_t src_addr, uint8_t srcEndpoint, uint16_t clust)
{
    zdo_req_t req;

    /* parse the request */
    zdo_parse_req(src_addr, data, clust, &req);

    switch (req.clust)
    {
    case DEV_ANNCE_REQ_CLUST:
    {
        LOG(LOG_DBG, "DUMP_ZDO_REQ: REQ            = %s.\n", "DEV_ANNCE_REQ_CLUST DISC REQ");
        LOG(LOG_DBG, "DUMP_ZDO_REQ: NETWORK ADDR   = %04X.\n", req.type.dev_annce.nwk_addr);
#ifdef LINUXx86_64
        LOG(LOG_DBG, "DUMP_ZDO_REQ: EXT ADDR       = %016lX.\n", req.type.dev_annce.ext_addr);
#else
        LOG(LOG_DBG, "DUMP_ZDO_REQ: EXT ADDR       = %016llX.\n", req.type.dev_annce.ext_addr);
#endif
        LOG(LOG_DBG, "DUMP_ZDO_REQ: CAPAB INFO     = %02X.\n", req.type.dev_annce.capab_info);
        if (iWaitForGetInforNewNode == 0)
        {
            if (req.type.dev_annce.nwk_addr == lastNewNode.destAddr)
            {
                LOG(LOG_DBG, "Rx_[%02X] - NEWNODE\n", req.type.dev_annce.capab_info);
                cmdTx_GetEndPointNumber(fd, lastNewNode.euiAddr, lastNewNode.destAddr);
                iWaitForGetInforNewNode = 1;
            }
        }
    }
    }
}

void zdo_rx_handler(uint8_t *data, uint8_t len, uint16_t src_addr, uint8_t srcEndpoint, uint16_t clust_id)
{
    uint8_t i;

    for (i = 0; zdo_handler_tbl[i].clust_id != END_MARKER_CLUST; i++)
    {
        if ((zdo_handler_tbl[i].clust_id == clust_id) && zdo_handler_tbl[i].handler)
            zdo_handler_tbl[i].handler(data, len, src_addr, srcEndpoint, clust_id);
    }
}

void zll_rx_handler(uint8_t *data, uint8_t len, uint8_t msg_type, uint16_t option, uint16_t pan_id, uint64_t src_addr, uint16_t profile_id, uint16_t clust_id)
{
    zcl_hdr_t hdr;
    uint8_t resp_len, resp[ZCL_MAX_PAYLOAD_SIZE];
    resp_len = 0;

    zcl_parse_hdr(data, len, &hdr);
    LOG(LOG_DBG, "zcl_parse_hdr\n");
    debug_dump_zcl_frm(data, &hdr, clust_id);
    parse_zcl_frm(data, &hdr, clust_id);
    LOG(LOG_DBG, "hdr\n");

    zcl_zll_commissioning_rx_handler(resp, &resp_len, option, src_addr, profile_id, &hdr);

    if (resp_len)
    {
        //notify to cli
        LOG(LOG_DBG, "notify response to cli\n");
        find_out_touch_link_node = 1;
        pthread_mutex_lock(&CriticalMutexNotification);

        memcpy((uint8_t *)&g_notify_buf->notify[g_notify_buf->notify_index], resp, resp_len);
        g_notify_buf->notify_index = g_notify_buf->notify_index + 1;

        pthread_mutex_unlock(&CriticalMutexNotification);
    }
}

void app_zcl_rx_handler(uint8_t *data, uint8_t len, uint16_t src_addr, uint8_t srcEndpoint, uint16_t profile_id, uint16_t clust_id)
{
    zcl_clust_t *clust;
    zcl_hdr_t hdr;
    uint8_t resp_len, resp[ZCL_MAX_PAYLOAD_SIZE];
    resp_len = 0;
    if ((clust = zcl_find_clust(zcl_clust_list, clust_id)) == NULL)
    {
        /* non-existent cluster */
        LOG(LOG_DBG, "TEST_ZCL: Non existent cluster.\n");
        return;
    }
    //get report data for notification
    memset((uint8_t *)&lastAttrData, 0, sizeof(readAttrData));
    lastAttrData.devSourceDesc.destAddr = src_addr;
    lastAttrData.devSourceDesc.endpoint = srcEndpoint;
    lastAttrData.devSourceDesc.profileId = profile_id;
    lastAttrData.devSourceDesc.clusterId = clust_id;

    zcl_parse_hdr(data, len, &hdr);
    LOG(LOG_DBG, "zcl_parse_hdr from device: 0x%04X\n", src_addr);
    debug_dump_zcl_frm(data, &hdr, clust_id);
    parse_zcl_frm(data, &hdr, clust_id);

    if (clust != NULL)
    {
        if (clust->rx_handler)
        {
            LOG(LOG_DBG, "cl rx handler");
            clust->rx_handler(resp, &resp_len, src_addr, srcEndpoint, profile_id, clust, &hdr);
        }
    }

    /*
     * if resp_len is zero, that probably means we got a
     * write attrib no response command. in that case, don't
     * return a response.
     */

    if (resp_len)
    {
        //notify to cli
        if (clust_id != ZCL_GRP_CLUST_ID)
        {
            LOG(LOG_DBG, "notify response to cli\n");

            pthread_mutex_lock(&CriticalMutexNotification);

            memcpy((uint8_t *)&g_notify_buf->notify[g_notify_buf->notify_index], resp, resp_len);
            g_notify_buf->notify_index = g_notify_buf->notify_index + 1;

            pthread_mutex_unlock(&CriticalMutexNotification);
        }
        else
        {
            if (bWaitForGroupResponseData)
            {
                if (g_grp_rsp->devSourceDesc.destAddr == src_addr)
                {
                    LOG(LOG_DBG, "Get group rsp \n");
                    memcpy((uint8_t *)g_grp_rsp, &resp[1], resp_len - 1);
                    bWaitForGroupResponseData = false;
                }
            }
        }
    }
}

void test_zcl_on_off_action_handler(uint8_t action, void *data)
{
    switch (action)
    {
    case ZCL_ON_OFF_CMD_OFF:
        LOG(LOG_DBG, "ON OFF ACTION HANDLER: OFF.\n");
        break;
    case ZCL_ON_OFF_CMD_ON:
        LOG(LOG_DBG, "ON OFF ACTION HANDLER: ON.\n");
        break;
    case ZCL_ON_OFF_CMD_TOGGLE:
        LOG(LOG_DBG, "ON OFF ACTION HANDLER: TOGGLE.\n");
        break;
    }
    LOG(LOG_DBG, "ON OFF ACTION HANDLER: Current value of on_off attrib: %02X.\n",
        on_off_attrib_list.data.on_off);
    return;
}
void test_zcl_level_action_handler(uint8_t action, void *data)
{
    uint8_t on_off;
    mem_ptr_t *mem_ptr = data;

    on_off = ZCL_LEVEL_TMR(mem_ptr)->with_on_off;

    switch (action)
    {
    case ZCL_LEVEL_ACTION_MAX_LEVEL:
        LOG(LOG_DBG, "LEVEL ACTION HANDLER: MAX LEVEL REACHED.\n");
        break;
    case ZCL_LEVEL_ACTION_MIN_LEVEL:
        //DBG_PRINT_SIMONLY("LEVEL ACTION HANDLER: MIN LEVEL REACHED.\n");

        /*
         * if the with_on_off command is issued,
         * then switch the on_off attribute
         */
        if (on_off)
        {
            on_off_attrib_list.data.on_off = 0;
            LOG(LOG_DBG, "LEVEL ACTION HANDLER: ON OFF ATTRIBUTE TURNED OFF.\n");
        }
        break;
    case ZCL_LEVEL_ACTION_REFRESH:
        LOG(LOG_DBG, "LEVEL ACTION HANDLER: CURRENT LEVEL = %02X.\n",
            level_attrib_list.data.curr_level);
        if (on_off && (on_off_attrib_list.data.on_off == 0))
        {
            on_off_attrib_list.data.on_off = 1;
            LOG(LOG_DBG, "LEVEL ACTION HANDLER: ON OFF ATTRIBUTE TURNED ON.\n");
        }
        break;
    }
}
int app_zcl_ias_zone_req(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID, uint8_t cmd, zclIasZoneReq req)
{
    uint16_t EndPoint = (((EndPoint_num)) | 0x0100);
    uint8_t len, data_out[ZCL_MAX_BUF_SZ];
    zcl_hdr_t hdr;

    hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
    hdr.frm_ctrl.dis_def_resp = false;
    hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
    hdr.frm_ctrl.manuf_spec = false;
    hdr.cmd = cmd;
    hdr.seq_num = zcl_get_seq_num();

    len = zcl_ias_zone_gen_req(data_out, &hdr, &req);

    debug_dump_buf(data_out, len);

    cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, ZCL_IAS_ZONE_CLUST_ID);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);

    rx_seq_num = -1;
    rx_ack_num = -1;
    if (!cmdTx_TransmitUnicastBinaryData(fd, destAddr, data_out, len))
    {
        int timeout = TIMEOUT_WAITING_SEQ;
        while ((rx_seq_num == -1) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No response from Zigbee controller\n");
            return -2;
        }

        timeout = TIMEOUT_WAITING_ACK;

        while ((rx_ack_num != rx_seq_num) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No ACK from Zigbee device\n");
            return -2;
        }
        return 0;
    }
    else
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }
    return 0;
}

int app_zcl_zll_commissioning_req(int fd, uint64_t destAddr, uint16_t dest_PAN, uint16_t ProfileID, uint8_t cmd, zclZllCommissioningReq req)
{
    uint8_t len, data_out[ZCL_MAX_BUF_SZ];
    zcl_hdr_t hdr;
    switch (cmd)
    {
    case ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_SCAN_REQUEST:
    case ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_DEVICE_IDENTIFY_REQUEST:
    case ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_RESET_TO_FACTORY_NEW_REQUEST:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = cmd;
        hdr.seq_num = zcl_get_seq_num();

        len = zcl_zll_commissioning_gen_req(data_out, &hdr, &req);
        break;
    default:
        printf("zll commissioning cluster not support this command\n");
        return 2;
    }
    debug_dump_buf(data_out, len);
    if (cmd == ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_SCAN_REQUEST)
    {
        cmdTx_TransmitInterPANBinaryData(fd, 1, (uint16_t)destAddr, dest_PAN, ProfileID, 0x1000, data_out, len);
    }
    else if ((cmd == ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_DEVICE_IDENTIFY_REQUEST) || (cmd == ZCL_ZLL_COMMISSIONING_CMD_TOUCHLINK_RESET_TO_FACTORY_NEW_REQUEST))
    {
        cmdTx_TransmitInterPANBinaryData(fd, 2, destAddr, dest_PAN, ProfileID, 0x1000, data_out, len);
    }

    return 0;
}

int appZclLevelReq(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID, uint8_t cmd, zclLevelReq req)
{
    uint16_t EndPoint = (((EndPoint_num)) | 0x0100);
    uint8_t len, data_out[ZCL_MAX_BUF_SZ];
    zcl_hdr_t hdr;
    switch (cmd)
    {
    case ZCL_LEVEL_CMD_STOP:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = ZCL_LEVEL_CMD_STOP;
        hdr.seq_num = zcl_get_seq_num();

        len = zcl_level_gen_req(data_out, &hdr, NULL);
        break;

    case ZCL_LEVEL_CMD_STEP:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = ZCL_LEVEL_CMD_STEP;
        hdr.seq_num = zcl_get_seq_num();

        //req.step.dir       = dir;
        //req.step.step_size  = step_size;
        //req.step.trans_time = trans_time;

        len = zcl_level_gen_req(data_out, &hdr, &req);
        break;

    case ZCL_LEVEL_CMD_MOVE:

        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = ZCL_LEVEL_CMD_MOVE;
        hdr.seq_num = zcl_get_seq_num();

        //req.move.dir    = dir;
        //req.move.rate   = rate;

        len = zcl_level_gen_req(data_out, &hdr, &req);
        break;

    case ZCL_LEVEL_CMD_MOVE_TO_LEVEL:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = ZCL_LEVEL_CMD_MOVE_TO_LEVEL;
        hdr.seq_num = zcl_get_seq_num();

        //req.move_to_level.level         = level;
        //req.move_to_level.trans_time    = trans_time;

        len = zcl_level_gen_req(data_out, &hdr, &req);
        break;

    case ZCL_LEVEL_CMD_MOVE_TO_LEVEL_WITH_ON_OFF:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = ZCL_LEVEL_CMD_MOVE_TO_LEVEL_WITH_ON_OFF;
        hdr.seq_num = zcl_get_seq_num();

        //req.move_to_level.level         = level;
        //req.move_to_level.trans_time    = trans_time;

        len = zcl_level_gen_req(data_out, &hdr, &req);
        break;

    default:
        printf("level cluster not support this command\n");
        return 2;
    }

    debug_dump_buf(data_out, len);

    cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, ZCL_LEVEL_CLUST_ID);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);

    rx_seq_num = -1;
    rx_ack_num = -1;
    if (!cmdTx_TransmitUnicastBinaryData(fd, destAddr, data_out, len))
    {
        int timeout = TIMEOUT_WAITING_SEQ;
        while ((rx_seq_num == -1) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No response from Zigbee controller\n");
            return -2;
        }

        timeout = 7000;

        while ((rx_ack_num != rx_seq_num) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No ACK from Zigbee device\n");
            return -2;
        }
        return 0;
    }
    else
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }
    return 0;
}

int app_multi_zcl_level_req(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID, uint8_t cmd, zclLevelReq req)
{
    uint16_t EndPoint = (((EndPoint_num)) | 0x0100);
    uint8_t len, data_out[ZCL_MAX_BUF_SZ];
    zcl_hdr_t hdr;
    switch (cmd)
    {
    case ZCL_LEVEL_CMD_STOP:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = ZCL_LEVEL_CMD_STOP;
        hdr.seq_num = zcl_get_seq_num();

        len = zcl_level_gen_req(data_out, &hdr, NULL);
        break;

    case ZCL_LEVEL_CMD_STEP:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = ZCL_LEVEL_CMD_STEP;
        hdr.seq_num = zcl_get_seq_num();

        //req.step.dir       = dir;
        //req.step.step_size  = step_size;
        //req.step.trans_time = trans_time;

        len = zcl_level_gen_req(data_out, &hdr, &req);
        break;

    case ZCL_LEVEL_CMD_MOVE:

        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = ZCL_LEVEL_CMD_MOVE;
        hdr.seq_num = zcl_get_seq_num();

        //req.move.dir    = dir;
        //req.move.rate   = rate;

        len = zcl_level_gen_req(data_out, &hdr, &req);
        break;

    case ZCL_LEVEL_CMD_MOVE_TO_LEVEL:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = ZCL_LEVEL_CMD_MOVE_TO_LEVEL;
        hdr.seq_num = zcl_get_seq_num();

        //req.move_to_level.level         = level;
        //req.move_to_level.trans_time    = trans_time;

        len = zcl_level_gen_req(data_out, &hdr, &req);
        break;

    case ZCL_LEVEL_CMD_MOVE_TO_LEVEL_WITH_ON_OFF:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = ZCL_LEVEL_CMD_MOVE_TO_LEVEL_WITH_ON_OFF;
        hdr.seq_num = zcl_get_seq_num();

        //req.move_to_level.level         = level;
        //req.move_to_level.trans_time    = trans_time;

        len = zcl_level_gen_req(data_out, &hdr, &req);
        break;

    default:
        printf("level cluster not support this command\n");
        return 2;
    }

    debug_dump_buf(data_out, len);

    cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, ZCL_LEVEL_CLUST_ID);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);

    if (!cmdTx_TransmitMulticastBinaryData(fd, destAddr, data_out, len))
    {
        return 0;
    }
    else
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }
    return 0;
}

int appZclOnOffReq(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID, uint8_t cmd, zclOnOffReq req)
{
    uint16_t EndPoint = (((EndPoint_num)) | 0x0100);
    uint8_t len, data_out[ZCL_MAX_BUF_SZ];
    zcl_hdr_t hdr;
    switch (cmd)
    {
    case ZCL_ON_OFF_CMD_OFF:
    case ZCL_ON_OFF_CMD_ON:
    case ZCL_ON_OFF_CMD_TOGGLE:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = cmd;
        hdr.seq_num = zcl_get_seq_num();
        len = zcl_on_off_gen_req(data_out, &hdr, NULL);
        break;
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_GETRELAYS:
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_GETMODE:
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SKIPFW:
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SKIPBK:
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_PUMP_GET_PUMP:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = true;
        hdr.manuf_code = ZCL_SMARTENIT_MFG_CODE;
        hdr.cmd = cmd;
        hdr.seq_num = zcl_get_seq_num();
        len = zcl_on_off_gen_req(data_out, &hdr, NULL);
        break;
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_OFF:
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_ON:
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_TOGGLE:
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SETRELAYS:
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SETMODE:
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_PROG_ONOFF:
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SETTIMERS:
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_GETTIMERS:
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_PUMP_SET_CONFIGURATION:
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_SETNAME:
    case ZCL_ON_OFF_CMD_MFG_SMARTENIT_RELAY_GETNAME:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = true;
        hdr.manuf_code = ZCL_SMARTENIT_MFG_CODE;
        hdr.cmd = cmd;
        hdr.seq_num = zcl_get_seq_num();
        len = zcl_on_off_gen_req(data_out, &hdr, &req);
        break;

    default:
        printf("on off cluster not support this command\n");
        return 2;
    }

    debug_dump_buf(data_out, len);
    cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, ZCL_ON_OFF_CLUST_ID);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);
    rx_seq_num = -1;
    rx_ack_num = -1;
    if (!cmdTx_TransmitUnicastBinaryData(fd, destAddr, data_out, len))
    {
        int timeout = TIMEOUT_WAITING_SEQ;
        while ((rx_seq_num == -1) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No response from Zigbee controller\n");
            return -2;
        }

        timeout = TIMEOUT_WAITING_ACK;

        while ((rx_ack_num != rx_seq_num) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No ACK from Zigbee device\n");
            return -2;
        }
        return 0;
    }
    else
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }
    return 0;
}

int app_multi_zcl_on_off_req(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID, uint8_t cmd)
{
    uint16_t EndPoint = (((EndPoint_num)) | 0x0100);
    uint8_t len, data_out[ZCL_MAX_BUF_SZ];
    zcl_hdr_t hdr;

    hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
    hdr.frm_ctrl.dis_def_resp = true;
    hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
    hdr.frm_ctrl.manuf_spec = false;
    hdr.cmd = cmd;
    hdr.seq_num = zcl_get_seq_num();

    len = zcl_on_off_gen_req(data_out, &hdr, NULL);
    debug_dump_buf(data_out, len);
    cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, ZCL_ON_OFF_CLUST_ID);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);

    if (!cmdTx_TransmitMulticastBinaryData(fd, destAddr, data_out, len))
    {
        return 0;
    }
    else
    {
        printf("Fatal error: 2 No response from ZigBee module\n");
        return -1;
    }
    return 0;
}
int app_multinode_zcl_on_off_req(int fd, uint16_t *destAddr, uint8_t no_node, uint8_t *m_endPoint, uint16_t ProfileID, uint8_t cmd)
{
    //uint16_t EndPoint = (((EndPoint_num))|0x0100);
    uint8_t len, data_out[ZCL_MAX_BUF_SZ];
    zcl_hdr_t hdr;

    hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
    hdr.frm_ctrl.dis_def_resp = true;
    hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
    hdr.frm_ctrl.manuf_spec = false;
    hdr.cmd = cmd;
    hdr.seq_num = zcl_get_seq_num();

    len = zcl_on_off_gen_req(data_out, &hdr, NULL);
    debug_dump_buf(data_out, len);
    //cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT,EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, ZCL_ON_OFF_CLUST_ID);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);

    if (!cmdTx_TransmitMultiNodeBinaryData(fd, destAddr, no_node, m_endPoint, data_out, len))
    {
        return 0;
    }
    else
    {
        printf("Fatal error: 2 No response from ZigBee module\n");
        return -1;
    }
    return 0;
}

int app_multinode_zcl_level_req(int fd, uint16_t *destAddr, uint8_t no_node, uint8_t *m_endPoint, uint16_t ProfileID, uint8_t cmd, zclLevelReq req)
{

    uint8_t len, data_out[ZCL_MAX_BUF_SZ];
    zcl_hdr_t hdr;
    switch (cmd)
    {
    case ZCL_LEVEL_CMD_STOP:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = ZCL_LEVEL_CMD_STOP;
        hdr.seq_num = zcl_get_seq_num();

        len = zcl_level_gen_req(data_out, &hdr, NULL);
        break;

    case ZCL_LEVEL_CMD_STEP:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = ZCL_LEVEL_CMD_STEP;
        hdr.seq_num = zcl_get_seq_num();

        //req.step.dir       = dir;
        //req.step.step_size  = step_size;
        //req.step.trans_time = trans_time;

        len = zcl_level_gen_req(data_out, &hdr, &req);
        break;

    case ZCL_LEVEL_CMD_MOVE:

        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = ZCL_LEVEL_CMD_MOVE;
        hdr.seq_num = zcl_get_seq_num();

        //req.move.dir    = dir;
        //req.move.rate   = rate;

        len = zcl_level_gen_req(data_out, &hdr, &req);
        break;

    case ZCL_LEVEL_CMD_MOVE_TO_LEVEL:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = ZCL_LEVEL_CMD_MOVE_TO_LEVEL;
        hdr.seq_num = zcl_get_seq_num();

        //req.move_to_level.level         = level;
        //req.move_to_level.trans_time    = trans_time;

        len = zcl_level_gen_req(data_out, &hdr, &req);
        break;

    case ZCL_LEVEL_CMD_MOVE_TO_LEVEL_WITH_ON_OFF:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = ZCL_LEVEL_CMD_MOVE_TO_LEVEL_WITH_ON_OFF;
        hdr.seq_num = zcl_get_seq_num();

        //req.move_to_level.level         = level;
        //req.move_to_level.trans_time    = trans_time;

        len = zcl_level_gen_req(data_out, &hdr, &req);
        break;

    default:
        printf("level cluster not support this command\n");
        return 2;
    }

    debug_dump_buf(data_out, len);

    //cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT,EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, ZCL_LEVEL_CLUST_ID);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);

    if (!cmdTx_TransmitMultiNodeBinaryData(fd, destAddr, no_node, m_endPoint, data_out, len))
    {
        return 0;
    }
    else
    {
        printf("Fatal error: 2 No response from ZigBee module\n");
        return -1;
    }
    return 0;
}
int appZclDoorLockReq(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID, uint8_t cmd, zclDoorLockReq req)
{
    uint16_t EndPoint = (((EndPoint_num)) | 0x0100);
    uint8_t len, data_out[ZCL_MAX_BUF_SZ];
    zcl_hdr_t hdr;
    uint8_t seq = 0;
    switch (cmd)
    {
    case ZCL_DOOR_LOCK_CMD_LOCK_DOOR:
    case ZCL_DOOR_LOCK_CMD_UNLOCK_DOOR:
    case ZCL_DOOR_LOCK_CMD_CLEAR_ALL_PIN_CODE:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = cmd;
        hdr.seq_num = zcl_get_seq_num();
        len = zcl_door_lock_gen_req(data_out, &hdr, NULL);
        break;
    case ZCL_DOOR_LOCK_CMD_SET_PIN_CODE:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = cmd;
        seq = zcl_get_seq_num();
        //write to cmdRegisterList
        cmdRegisterList[seq].nodeId = destAddr;
        cmdRegisterList[seq].data0 = req.set_pin_code.user_id;
        cmdRegisterList[seq].arrayLength = req.set_pin_code.pin_length;
        memcpy(cmdRegisterList[seq].arrayData, req.set_pin_code.p_pin, req.set_pin_code.pin_length);

        hdr.seq_num = seq;
        len = zcl_door_lock_gen_req(data_out, &hdr, &req);
        break;
    case ZCL_DOOR_LOCK_CMD_CLEAR_PIN_CODE:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = cmd;
        seq = zcl_get_seq_num();
        //write to cmdRegisterList
        cmdRegisterList[seq].nodeId = destAddr;
        cmdRegisterList[seq].data0 = req.clear_pin_code.user_id;
        cmdRegisterList[seq].arrayLength = req.clear_pin_code.pin_length;
        memcpy(cmdRegisterList[seq].arrayData, req.clear_pin_code.p_pin, req.clear_pin_code.pin_length);

        hdr.seq_num = seq;
        len = zcl_door_lock_gen_req(data_out, &hdr, &req);
        break;

    case ZCL_DOOR_LOCK_CMD_GET_PIN_CODE:
    case ZCL_DOOR_LOCK_CMD_SET_USER_STATUS:
    case ZCL_DOOR_LOCK_CMD_GET_USER_STATUS:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = cmd;
        hdr.seq_num = zcl_get_seq_num();
        len = zcl_door_lock_gen_req(data_out, &hdr, &req);
        break;

    default:
        printf("door lock cluster not support this command\n");
        return 2;
    }
    debug_dump_buf(data_out, len);
    cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, ZCL_DOOR_LOCK_CLUST_ID);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);
    rx_seq_num = -1;
    rx_ack_num = -1;
    if (!cmdTx_TransmitUnicastBinaryData(fd, destAddr, data_out, len))
    {
        int timeout = TIMEOUT_WAITING_SEQ;
        while ((rx_seq_num == -1) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No response from Zigbee controller\n");
            return -2;
        }

        timeout = 8000;

        while ((rx_ack_num != rx_seq_num) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No ACK from Zigbee device\n");
            return -2;
        }
        return 0;
    }
    else
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }
    return 0;
}

int app_zcl_grp_req(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID, uint8_t cmd, zcl_grp_req_t req, grp_cmd_response *res)
{
    uint16_t EndPoint = (((EndPoint_num)) | 0x0100);
    uint8_t len, data_out[ZCL_MAX_BUF_SZ];

    memset((uint8_t *)res, 0, sizeof(grp_cmd_response));
    res->devSourceDesc.destAddr = destAddr;
    res->devSourceDesc.endpoint = EndPoint_num;
    res->devSourceDesc.profileId = ProfileID;
    res->devSourceDesc.clusterId = ZCL_GRP_CLUST_ID;

    g_grp_rsp = res;

    zcl_hdr_t hdr;
    switch (cmd)
    {
    case ZCL_GRP_CMD_REM_ALL:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = cmd;
        hdr.seq_num = zcl_get_seq_num();
        len = zcl_grp_gen_req(data_out, &hdr, NULL);
        break;
    case ZCL_GRP_CMD_ADD_GRP:
    case ZCL_GRP_CMD_VIEW_GRP:
    case ZCL_GRP_CMD_GET_MEMB:
    case ZCL_GRP_CMD_REM_GRP:
    case ZCL_GRP_CMD_ADD_IF_ID:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = cmd;
        hdr.seq_num = zcl_get_seq_num();
        len = zcl_grp_gen_req(data_out, &hdr, &req);
        break;

    default:
        printf("group cluster not support this command\n");
        return 2;
    }
    debug_dump_buf(data_out, len);
    cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, ZCL_GRP_CLUST_ID);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);
    rx_seq_num = -1;
    rx_ack_num = -1;
    if (!cmdTx_TransmitUnicastBinaryData(fd, destAddr, data_out, len))
    {
        bWaitForGroupResponseData = true;
        int timeout = TIMEOUT_WAITING_SEQ;
        while ((rx_seq_num == -1) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No response from Zigbee controller\n");
            return -2;
        }

        timeout = TIMEOUT_WAITING_ACK;

        while ((rx_ack_num != rx_seq_num) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No ACK from Zigbee device\n");
            return -2;
        }

        timeout = 2000;

        while ((bWaitForGroupResponseData) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No group response from Zigbee device\n");
            return -3;
        }

        return 0;
    }
    else
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }
    return 0;
}
int appZclColorControlReq(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID, uint8_t cmd, zclColorControlReq req)
{
    uint16_t EndPoint = (((EndPoint_num)) | 0x0100);
    uint8_t len, data_out[ZCL_MAX_BUF_SZ];
    zcl_hdr_t hdr;
    switch (cmd)
    {
    case ZCL_COLOR_CONTROL_CMD_MOVE_TO_HUE:
    case ZCL_COLOR_CONTROL_CMD_MOVE_HUE:
    case ZCL_COLOR_CONTROL_CMD_STEP_HUE:
    case ZCL_COLOR_CONTROL_CMD_MOVE_TO_SATURATION:
    case ZCL_COLOR_CONTROL_CMD_MOVE_SATURATION:
    case ZCL_COLOR_CONTROL_CMD_STEP_SATURATION:
    case ZCL_COLOR_CONTROL_CMD_MOVE_TO_HUE_AND_SATURATION:
    case ZCL_COLOR_CONTROL_CMD_MOVE_TO_COLOR:
    case ZCL_COLOR_CONTROL_CMD_MOVE_COLOR:
    case ZCL_COLOR_CONTROL_CMD_STEP_COLOR:
    case ZCL_COLOR_CONTROL_CMD_MOVE_TO_COLOR_TEMPERATURE:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = cmd;
        hdr.seq_num = zcl_get_seq_num();
        len = zcl_color_control_gen_req(data_out, &hdr, &req);
        break;

    default:
        printf("color control cluster not support this command\n");
        return 2;
    }
    debug_dump_buf(data_out, len);
    cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, ZCL_COLOR_CONTROL_CLUST_ID);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);
    rx_seq_num = -1;
    rx_ack_num = -1;
    if (!cmdTx_TransmitUnicastBinaryData(fd, destAddr, data_out, len))
    {
        int timeout = TIMEOUT_WAITING_SEQ;
        while ((rx_seq_num == -1) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No response from Zigbee controller\n");
            return -2;
        }

        timeout = TIMEOUT_WAITING_ACK;

        while ((rx_ack_num != rx_seq_num) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No ACK from Zigbee device\n");
            return -2;
        }
        return 0;
    }
    else
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }
    return 0;
}

int appZclIdReq(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t ProfileID, uint8_t cmd, zclIdReq req)
{
    uint16_t EndPoint = (((EndPoint_num)) | 0x0100);
    uint8_t len, data_out[ZCL_MAX_BUF_SZ];
    zcl_hdr_t hdr;
    switch (cmd)
    {
    case ZCL_ID_CMD_ID_QUERY:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = cmd;
        hdr.seq_num = zcl_get_seq_num();
        len = zcl_id_gen_req(data_out, &hdr, NULL);
        break;
    case ZCL_ID_CMD_ID:
        hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
        hdr.frm_ctrl.dis_def_resp = true;
        hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_CLUST_SPEC;
        hdr.frm_ctrl.manuf_spec = false;
        hdr.cmd = cmd;
        hdr.seq_num = zcl_get_seq_num();
        len = zcl_id_gen_req(data_out, &hdr, &req);
        break;

    default:
        printf("Identify cluster not support this command\n");
        return 2;
    }
    debug_dump_buf(data_out, len);
    cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, ZCL_IDENTIFY_CLUST_ID);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);

    rx_seq_num = -1;
    rx_ack_num = -1;
    if (!cmdTx_TransmitUnicastBinaryData(fd, destAddr, data_out, len))
    {
        int timeout = TIMEOUT_WAITING_SEQ;
        while ((rx_seq_num == -1) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No response from Zigbee controller\n");
            return -2;
        }

        timeout = TIMEOUT_WAITING_ACK;

        while ((rx_ack_num != rx_seq_num) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No ACK from Zigbee device\n");
            return -2;
        }
        return 0;
    }
    else
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }
    return 0;
}

int appZclWriteAttrib(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t clust_id, uint16_t ProfileID, uint16_t manu_code, zclAttribute *attrib_list, uint8_t attrib_num)
{
    uint16_t EndPoint = (((EndPoint_num)) | 0x0100);
    uint8_t len, data_out[ZCL_MAX_BUF_SZ];
    zcl_hdr_t hdr;

    memset(&hdr, 0, sizeof(zcl_hdr_t));
    hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_GENERAL;
    hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
    if (manu_code == 0xFFFF)
    {
        hdr.frm_ctrl.manuf_spec = false;
    }
    else
    {
        hdr.frm_ctrl.manuf_spec = true;
        hdr.manuf_code = manu_code;
    }
    hdr.frm_ctrl.dis_def_resp = false;
    hdr.seq_num = zcl_get_seq_num();
    hdr.cmd = ZCL_CMD_WRITE_ATTRIB;
    len = zcl_gen_write_attrib(data_out, &hdr, attrib_list, attrib_num);

    debug_dump_buf(data_out, len);

    cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, clust_id);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);

    rx_seq_num = -1;
    rx_ack_num = -1;
    if (!cmdTx_TransmitUnicastBinaryData(fd, destAddr, data_out, len))
    {
        int timeout = TIMEOUT_WAITING_SEQ;
        while ((rx_seq_num == -1) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No response from Zigbee controller\n");
            return -2;
        }

        timeout = 7000;

        while ((rx_ack_num != rx_seq_num) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No ACK from Zigbee device\n");
            return -2;
        }
        return 0;
    }
    else
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }
    return 0;
}

int app_zcl_config_rpt(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t clust_id, uint16_t ProfileID, uint16_t manu_code, uint16_t attrib_id, uint16_t min_intv, uint16_t max_intv, uint32_t change)
{
    uint16_t EndPoint = (((EndPoint_num)) | 0x0100);
    uint8_t len, data_out[ZCL_MAX_BUF_SZ];
    zcl_clust_t *clust;

    zcl_hdr_t hdr;

    if ((clust = zcl_find_clust(zcl_clust_list, clust_id)) == NULL)
    {
        /* non-existent cluster */
        LOG(LOG_DBG, "TEST_ZCL: Non existent cluster.\n");
        return -1;
    }
    memset(&hdr, 0, sizeof(zcl_hdr_t));
    hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_GENERAL;
    hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
    if (manu_code == 0xFFFF)
    {
        hdr.frm_ctrl.manuf_spec = false;
    }
    else
    {
        hdr.frm_ctrl.manuf_spec = true;
        hdr.manuf_code = manu_code;
    }
    hdr.frm_ctrl.dis_def_resp = false;
    hdr.seq_num = zcl_get_seq_num();
    hdr.cmd = ZCL_CMD_CONFIG_REPORT;
    len = zcl_gen_config_rpt(data_out, &hdr, clust, attrib_id, min_intv, max_intv, change);

    debug_dump_buf(data_out, len);

    cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, clust_id);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);

    rx_seq_num = -1;
    rx_ack_num = -1;
    if (!cmdTx_TransmitUnicastBinaryData(fd, destAddr, data_out, len))
    {
        int timeout = TIMEOUT_WAITING_SEQ;
        while ((rx_seq_num == -1) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No response from Zigbee controller\n");
            return -2;
        }

        timeout = 7000;

        while ((rx_ack_num != rx_seq_num) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No ACK from Zigbee device\n");
            return -2;
        }
        return 0;
    }
    else
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }
    return 0;
}

int appZclReadAttrib(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint16_t clust_id, uint16_t ProfileID, uint16_t manu_code, uint16_t *attrib_list, uint8_t attrib_num, readAttrData *rData)
{
    uint16_t EndPoint = (((EndPoint_num)) | 0x0100);
    uint8_t len, data_out[ZCL_MAX_BUF_SZ];
    zcl_hdr_t hdr;
    memset((uint8_t *)rData, 0, sizeof(readAttrData));
    rData->devSourceDesc.destAddr = destAddr;
    rData->devSourceDesc.endpoint = EndPoint_num;
    rData->devSourceDesc.profileId = ProfileID;
    rData->devSourceDesc.clusterId = clust_id;

    get_readAttrData_cond.devSourceDesc.destAddr = destAddr;
    get_readAttrData_cond.devSourceDesc.endpoint = EndPoint_num;
    get_readAttrData_cond.devSourceDesc.profileId = ProfileID;
    get_readAttrData_cond.devSourceDesc.clusterId = clust_id;
    get_readAttrData_cond.attributeID = attrib_list[0];

    memset(&hdr, 0, sizeof(hdr));
    hdr.frm_ctrl.frm_type = ZCL_FRM_TYPE_GENERAL;
    hdr.frm_ctrl.dir = ZCL_FRM_DIR_TO_SRVR;
    if (manu_code == 0xFFFF)
    {
        hdr.frm_ctrl.manuf_spec = false;
    }
    else
    {
        hdr.frm_ctrl.manuf_spec = true;
        hdr.manuf_code = manu_code;
    }
    hdr.frm_ctrl.dis_def_resp = false;
    hdr.seq_num = zcl_get_seq_num();
    hdr.cmd = ZCL_CMD_READ_ATTRIB;
    len = zcl_gen_read_attrib(data_out, &hdr, attrib_list, attrib_num);

    debug_dump_buf(data_out, len);

    cmdTx_S4x_Registers(fd, S_REGISTER_ENDPOINTS_CURRENT, EndPoint);
    cmdTx_S4x_Registers(fd, S_REGISTER_CLUSTER_ID_CURRENT, clust_id);
    cmdTx_S4x_Registers(fd, S_REGISTER_PROFILE_ID_CURRENT, ProfileID);
    rx_ack_num = -1;
    rx_seq_num = -1;
    rx_nack_num = -1;
    bWaitForReadAttributeData = true;
    if (!cmdTx_TransmitUnicastBinaryData(fd, destAddr, data_out, len))
    {
        int timeout = TIMEOUT_WAITING_SEQ;
        while ((rx_seq_num == -1) && (timeout-- > 0))
        {
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No response from Zigbee controller\n");
            return -2;
        }

        timeout = TIMEOUT_WAITING_ACK;

        while ((rx_ack_num != rx_seq_num) && (timeout-- > 0))
        {
            if (rx_nack_num == rx_seq_num)
            {
                printf("NACK from Zigbee device\n");
                return -2;
            }
            usleep(SLEEP_MILISECOND_UNIT);
        }
        if (timeout <= 0)
        {
            printf("No ACK from Zigbee device\n");
            return -2;
        }
        if (gOpenNetwork)
        {
            LOG(LOG_DBG, "Read Attribute, waiting response");
            while ((bWaitForReadAttributeData == true) && (timeout-- > 0))
            {
                usleep(SLEEP_MILISECOND_UNIT);
            }
            if (timeout <= 0)
            {
                printf("No ACK from Zigbee device\n");
                return -2;
            }
            memcpy((uint8_t *)rData, (uint8_t *)&lastAttrData, sizeof(readAttrData));
        }
        return 0;
    }
    else
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }
}

int zigbee_read_color(int fd, uint16_t destAddr, uint8_t EndPoint_num, uint8_t *hue, uint8_t *saturation)
{
    int ret;
    readAttrData rData;
    uint16_t attrib_list[] = {ZCL_COLOR_CONTROL_ATTRIB_CURRENT_HUE};
    ret = appZclReadAttrib(fd, destAddr, EndPoint_num, ZCL_COLOR_CONTROL_CLUST_ID, ZCL_STACK_PROF_HA, 0xFFFF, attrib_list, 1, &rData);
    if (ret)
    {
        return ret;
    }

    *hue = rData.uint8_t_data;

    attrib_list[0] = ZCL_COLOR_CONTROL_ATTRIB_CURRENT_SATURATION;
    ret = appZclReadAttrib(fd, destAddr, EndPoint_num, ZCL_COLOR_CONTROL_CLUST_ID, ZCL_STACK_PROF_HA, 0xFFFF, attrib_list, 1, &rData);
    if (ret)
    {
        return ret;
    }

    *saturation = rData.uint8_t_data;
    return 0;
}

int appCreateBindingOnRemote(int fd, uint16_t target_addr, uint64_t destAddr, uint16_t clust_id, uint64_t src_addr, uint8_t srcEndpoint, uint8_t destEndpoint)

{
    cmdTx_CreateBindingOnRemoteDevice(fd, target_addr, src_addr, srcEndpoint, clust_id, destAddr, destEndpoint);
    rx_bind_status = 0xff;

    int timeout = TIMEOUT_WAITING_ACK;
    while ((rx_bind_status == 0xff) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("No ACK from Zigbee device\n");
        return -2;
    }
    else
    {
        return rx_bind_status;
    }

    return 0;
}

int appDeleteBindingOnRemote(int fd, uint16_t target_addr, uint64_t destAddr, uint16_t clust_id,
                             uint64_t src_addr, uint8_t srcEndpoint, uint8_t destEndpoint)
{
    cmdTx_DeleteBindingOnRemoteDevice(fd, target_addr, src_addr, srcEndpoint, clust_id, destAddr, destEndpoint);
    rx_unbind_status = 0xff;

    int timeout = TIMEOUT_WAITING_ACK;
    while ((rx_unbind_status == 0xff) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("No ACK from Zigbee device\n");
        return -2;
    }
    else
    {
        return rx_unbind_status;
    }

    return 0;
}

int zigbeeTelegesisReset(int fd)
{
    return cmdTx_Reset(fd);
}

int zigbeeRemoveNode(int fd, uint16_t desNodeID)
{
    return cmdTx_RemoveNode(fd, desNodeID);
}

int zigbee_request_eui64(int fd, uint64_t desEui64ID, uint16_t desNodeID)
{
    int timeout = 2000;

    cmdTx_RequestNodeEui64(fd, desEui64ID, desNodeID);
    g_command_done = 0;
    while ((!g_command_done) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }

    timeout = 3000;
    g_addr_info = 0;
    while ((!g_addr_info) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }
    return 0;
}

int zigbeeGetRemoteBindingTable(int fd, uint16_t desNodeID, remoteBindingNode *rbt, uint8_t *rbt_noe)
{
    char atcmdBuffer[BUF_LINE];
    int timeout;
    memset((uint8_t *)&remote_binding_table, 0x00, sizeof(remoteBindingNode) * REMOTE_BINDING_MAX_NODE);
    remote_binding_table_index = 0;
    LOG(LOG_DBG, "cmdTx_Btable\n");
    sprintf(atcmdBuffer, "AT+BTABLE:00,%04X\r", desNodeID);
    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));

    rx_ack_num = -1;
    rx_seq_num = -1;
    timeout = TIMEOUT_WAITING_SEQ;
    while ((rx_seq_num == -1) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("No response from Zigbee controller\n");
        return -2;
    }

    timeout = 8000;
    while ((rx_ack_num != rx_seq_num) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        get_table = 0;
        *rbt_noe = 0;
        return -1;
    }

    printf("remote_binding_table_index:%02X\n", remote_binding_table_index);
    *rbt_noe = remote_binding_table_index;
    memcpy((uint8_t *)rbt, (uint8_t *)&remote_binding_table, remote_binding_table_index * sizeof(remoteBindingNode));

    return 0;
}

int zigbee_controller_info(uint64_t *euiAddr)
{
    cmdTx_ControllerInfor(fd);
    rx_get_euiAddr = 1;
    int timeout = 2000;

    while ((rx_get_euiAddr) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("No ACK from Zigbee device\n");
        rx_get_euiAddr = 0;
        return -2;
    }
    else
    {
        *euiAddr = controller_euiAddr;
        return 0;
    }
}
int zigbeeOpenCloseNetwork(uint8_t mode)
{
    char atcmdBuffer[BUF_LINE];
    int timeout = 2000;

    switch (mode)
    {
    case ENABLE_OPEN_NETWORK:
        gOpenNetwork = true;
        sprintf(atcmdBuffer, "ATS0A5=0;password\r\n");
        break;

    case DISABLE_OPEN_NETWORK:
        gOpenNetwork = false;
        sprintf(atcmdBuffer, "ATS0A5=1;password\r\n");
        break;
    default:
        return 1;
    }

    putbuffer_t(fd, (uint8_t *)atcmdBuffer, (uint8_t)strlen(atcmdBuffer));
    g_command_done = 0;
    while ((!g_command_done) && (timeout-- > 0))
    {
        usleep(SLEEP_MILISECOND_UNIT);
    }
    if (timeout <= 0)
    {
        printf("Fatal error: No response from ZigBee module\n");
        return -1;
    }
    return 0;
}


static int init_uart(char *dev)
{
    int n;
    int fd = open(dev, O_RDWR | O_NDELAY | O_NOCTTY);
    if (fd >= 0)
    {
        // Cancel the O_NDELAY flag.
        n = fcntl(fd, F_GETFL, 0);
        fcntl(fd, F_SETFL, n & ~O_NDELAY);
    }

    if (fd < 0)
    {
        printf("error %d opening %s: %s\n", errno, dev, strerror(errno));
        return -1;
    }
    return set_interface_attribs(fd, B19200, 0, 1);
}

int zigbee_detruct(int fd)
{
    timerDelete();
    pthread_cancel(thread_reader);
    pthread_cancel(thread_reader_pipe);
    usleep(WAITING_ZIGBEE_DETRUCT_COMPLETE);
    close(fd);
    return 0;
}
int zigbeeInitialize(char *pdevID, zigbee_notify_queue_t *notify)
{
    int ret;
    app_zcl_init();
    timerInit();
    g_notify_buf = notify;
    memset((char *)g_notify_buf, 0, sizeof(zigbee_notify_queue_t));
    memset((uint8_t *)&cmdRegisterList, 0, 256 * sizeof(cmdRegister));
    memset((uint8_t *)&last_nodeLeft, 0, sizeof(nodeLeft));

    if (pdevID == NULL)
    {
        fd = init_uart(ZIGBEE_CONSOLE_PORT_DEFAULT);
    }
    else
    {
        fd = init_uart(pdevID);
    }

    if (fd < 0)
    {
        if (pdevID == NULL)
            printf("error %d opening %s: %s\n", errno, ZIGBEE_CONSOLE_PORT_DEFAULT, strerror(errno));
        else
            printf("error %d opening %s: %s\n", errno, pdevID, strerror(errno));
        return 0;
    }

    if (signal(SIGINT, sig_handler) == SIG_ERR)
        printf("\ncan't catch SIGINT\n");

    if (pipe(uart_buffer))
    {
        printf("Pipe failed.\n");
        return 0;
    }
    fcntl(uart_buffer[0], F_SETFL, fcntl(uart_buffer[0], F_GETFL) | O_NONBLOCK); //set read non block

    ret = pthread_create(&thread_reader, NULL, command_handling_loop, &fd);
    if (ret)
    {
        printf("Cannot create reader thread: %s.\n", strerror(ret));
        exit(1);
    }

    ret = pthread_create(&thread_reader_pipe, NULL, command_handling_pipe_loop, &fd);
    if (ret)
    {
        printf("Cannot create reader thread: %s.\n", strerror(ret));
        exit(1);
    }

    sleep(WAITING_ZIGBEE_INIT_COMPLETE);

    ret = cmdTx_ControllerInfor(fd);
    if (ret != 0)
    {
        return -1;
    }

    ret = cmdTx_NetworkInformation(fd);
    if (ret < 2)
    {
        cmdTx_Sxx_HA_Registers(fd);
        cmdTx_EstablishPersonalAreaNetwork(fd);
    }

    return fd;
}

void pushNotificationZigbeeToHandler(uint8_t bStatus, uint8_t *pData, uint32_t len)
{
    pthread_mutex_lock(&CriticalMutexNotification);

    g_notify_buf->notify[g_notify_buf->notify_index].notify_status = bStatus;
    memcpy(g_notify_buf->notify[g_notify_buf->notify_index].notify_message, pData, len);
    g_notify_buf->notify_index = g_notify_buf->notify_index + 1;

    pthread_mutex_unlock(&CriticalMutexNotification);
}

char *GetStringOfStatusCode(uint8_t statusCode)
{
    switch (statusCode)
    {
#ifdef FIRMWARE_REVISION_FIOD //Five In One Device
    case 0x00:
        return "Everything OK - Success";
    case 0x01:
        return "Fatal Error";
    case 0x02:
        return "Unknown command";
    case 0x04:
        return "Invalid S-Register";
    case 0x05:
        return "Invalid parameter";
    case 0x06:
        return "Recipient could not be reached";
    case 0x07:
        return "Message was not acknowledged";
    case 0x0A:
        return "Message could not be sent";
    case 0x0C:
        return "Too many characters";
    case 0x0D:
        return "License Problem";
    case 0x0E:
        return "PAN could not be established because duplicate PAN ID was detected";
    case 0x0F:
        return "Fatal error initialising the network";
    case 0x10:
        return "Error bootloading";
    case 0x12:
        return "Fatal error initialising the stack";
    case 0x18:
        return "Node has run out of Buffers";
    case 0x19:
        return "Trying to write read-only register";
    case 0x20:
        return "Invalid password";
    case 0x25:
        return "Cannot form network";
    case 0x27:
        return "No network found";
    case 0x28:
        return "Operation cannot be completed if node is part of a PAN";
    case 0x2C:
        return "Error leaving the PAN";
    case 0x2D:
        return "Error scanning for PANs";
    case 0x33:
        return "No response from the remote boot loader";
    case 0x39:
        return "MAC transmit queue is full";
    case 0x6C:
        return "Invalid binding index";
    case 0x66:
        return "Message is not been sent successfully";
    case 0x70:
        return "Invalid operation";
    case 0x74:
        return "Message too long";
    case 0x86:
        return "Unsupported Attribute";
    case 0x8D:
        return "Incorrect attribute type";
    case 0x91:
        return "Operation only possible if joined to a PAN";
    case 0x93:
        return "Node is not part of a Network";
    case 0x94:
        return "Cannot join network";
    case 0x96:
        return "Mobile End Device Move to new Parent Failed";
    case 0x98:
        return "Cannot join ZigBee 2006 Network as Router";
    case 0xA3:
        return "Invalid Endpoint";
    case 0xA6:
        return "Error in trying to encrypt at APS level. No link key entry in the table for the destination";
    case 0xAB:
        return "Trying to join, but no beacons could be heard";
    case 0xAC:
        return "Network key was sent in the clear when trying to join secured";
    case 0xAD:
        return "Did not receive Network Key";
    case 0xAE:
        return "No Link Key received";
    case 0xAF:
        return "Preconfigured Key Required";
    case 0xB1:
        return "Not attached to a meter";
    case 0xB2:
        return "ESI end point not known";
    case 0xC5:
        return "NWK Already Present";
    case 0xC7:
        return "NWK Table Full";
    case 0xC8:
        return "NWK Unknown Device";
    default:
        return "Initial status";
#endif //Five In One Device

#ifdef FIRMWARE_REVISION_R309
    case 0x00:
        return "Everything OK - Success";
    case 0x01:
        return "Couldn’t poll Parent because of Timeout";
    case 0x02:
        return "Unknown command";
    case 0x04:
        return "Invalid S-Register";
    case 0x05:
        return "Invalid parameter";
    case 0x06:
        return "Recipient could not be reached";
    case 0x07:
        return "Message was not acknowledged";
    case 0x0A:
        return "Message could not be sent";
    case 0x0B:
        return "Local node is not sink";
    case 0x0C:
        return "Too many characters";
    case 0x0D:
        return "License Problem";
    case 0x0E:
        return "Background Scan in Progress (Please wait and try again)";
    case 0x0F:
        return "Fatal error initialising the network";
    case 0x10:
        return "Error bootloading";
    case 0x12:
        return "Fatal error initialising the stack";
    case 0x18:
        return "Node has run out of Buffers";
    case 0x19:
        return "Trying to write read-only register";
    case 0x1A:
        return "Data Mode Refused by Remote Node";
    case 0x20:
        return "Invalid password";
    case 0x25:
        return "Cannot form network";
    case 0x27:
        return "No network found";
    case 0x28:
        return "Operation cannot be completed if node is part of a PAN";
    case 0x2C:
        return "Error leaving the PAN";
    case 0x2D:
        return "Error scanning for PANs";
    case 0x33:
        return "No response from the remote bootloader";
    case 0x34:
        return "Target did not respond during cloning";
    case 0x35:
        return "Timeout occurred during xCASTB";
    case 0x39:
        return "MAC transmit queue is full";
    case 0x6C:
        return "Invalid binding index";
    case 0x70:
        return "Invalid operation";
    case 0x72:
        return "More than 10 unicast messages were in flight at the same time";
    case 0x74:
        return "Message too long";
    case 0x80:
        return "ZDP Invalid Request Type";
    case 0x81:
        return "ZDP Device not Found";
    case 0x82:
        return "ZDP Invalid Endpoint";
    case 0x83:
        return "ZDP Not Active";
    case 0x84:
        return "ZDP Not Supported";
    case 0x85:
        return "ZDP Timeout";
    case 0x86:
        return "ZDP No Match";
    case 0x87:
        return "ZDP Table Full";
    case 0x88:
        return "ZDP No Entry";
    case 0x89:
        return "ZDP No Descriptor";
    default:
        return "Initial status";
#endif
    }
}
