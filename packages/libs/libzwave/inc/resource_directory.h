
#ifndef RESOURCEDIRECTORY_H
#define RESOURCEDIRECTORY_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "zw_serialapi.h"

#define MODE_FLAGS_DELETED 0x0100
#define MODE_FLAGS_FAILED  0x0200
#define MODE_FLAGS_LOWBAT  0x0400
#define INVALUE_LAST_VALUE 101
#define NODE_MANUFACTURER_SIZE 0x06
#define CONVERT_UINT8_TO_UINT16 256

typedef enum 
{
    //STATUS_ADDING,
    STATUS_CREATED,
    //STATUS_PROBE_PROTOCOL_INFO,
    STATUS_PROBE_NODE_INFO,
    STATUS_PROBE_PRODUCT_ID,
    STATUS_ENUMERATE_ENDPOINTS,
    STATUS_SET_WAKE_UP_INTERVAL,
    STATUS_ASSIGN_RETURN_ROUTE,
    STATUS_PROBE_WAKE_UP_INTERVAL,
    STATUS_PROBE_ENDPOINTS,
    STATUS_MDNS_PROBE,
    STATUS_MDNS_EP_PROBE,
    STATUS_DONE,
    STATUS_PROBE_FAIL,
    STATUS_FAILING,
} rd_node_state_t;

typedef enum 
{
    MODE_PROBING,
    MODE_NONLISTENING,
    MODE_ALWAYSLISTENING,
    MODE_FREQUENTLYLISTENING,
    MODE_MAILBOX,
    MODE_REMOVE_FORCE,
} rd_node_mode_t;


enum 
{
    CC_SUPPORTED = 0,
    CC_NOT_SUPPORTED,
    CC_NOT_PROBED,
} ;


typedef struct rd_node_database_entry 
{

    uint32_t wakeUp_interval;
    uint32_t lastAwake;
    uint32_t lastUpdate; //(0->100%)

    uint8_t isSupportedCmdClass; /* BIT0 Inclusion Controller CC
                                    BIT1 Transport Service CC */

    uint8_t nodeid;
    uint8_t security_flags;

    /*uint32_t homeID;*/

    rd_node_mode_t mode;
    rd_node_state_t state;

    uint16_t manufacturerID;
    uint16_t productType;
    uint16_t productID;

    uint8_t nodeTypeGeneric; // Is this a controller, routing slave ... etc
    NODE_TYPE nodeType;   /* Basic, Generic and Specific Device Types */
    uint8_t refCnt;//size of struct

} rd_node_database_entry_t;

typedef struct rd_node_list
{
    uint8_t nodeId;
    rd_node_mode_t mode;
} rd_node_list_t;
void rd_remove_node(uint8_t node);

void rd_probe_resume();

void rd_node_probe_update(rd_node_database_entry_t* n);

void rd_register_new_node(uint8_t node,uint8_t bAdding);


/**
 * Get nodedatabase entry from data store. free_node_dbe, MUST
 * be called when the node entry if no longer needed.
 */
rd_node_database_entry_t* rd_get_node_dbe(uint8_t nodeid);

/**
 * MUST be called when a node entry is no longer needed.
 */
void rd_free_node_dbe(rd_node_database_entry_t* n);

void rd_mark_node_deleted(uint8_t nodeid); 

int rd_init(uint8_t lock, uint8_t* node_id_list, uint8_t* node_list_size);
int GetNodeList(uint8_t* noNode, rd_node_list_t* nodeList);
uint8_t
rd_node_exists(uint8_t node);

void
rd_probe_lock(uint8_t enable);

void    rd_exit();
void    rd_destroy();

uint8_t
GetCacheEntryFlag(uint8_t nodeid);
uint8_t
GetCacheEntryNodeTypeGeneric(uint8_t nodeid);
NODE_TYPE
GetCacheEntryNodeType(uint8_t nodeid);
uint8_t
GetCacheEntryNodeMode(uint8_t nodeid);
uint8_t
SetCacheEntryNodeMode(uint8_t nodeid, uint8_t value);
uint8_t
SetSupportedInclusionControllerCmdClass(uint8_t nodeid, bool value);
uint8_t
GetSupportedInclusionControllerCmdClass(uint8_t nodeid);
uint8_t
SetCacheEntryFlagMasked(uint8_t nodeid, uint8_t value, uint8_t mask);

uint8_t
SetSupportedTransportServiceCmdClass(uint8_t nodeid, bool value);
uint8_t
GetSupportedTransportServiceCmdClass(uint8_t nodeid);

uint8_t
count_array_bits(uint8_t* a, uint8_t length);

uint8_t
GetLastValueSwitchMultilevel(uint8_t nodeid);

uint8_t
SetLastValueSwitchMultilevel(uint8_t nodeid, uint8_t value);

uint16_t
GetNodeManufactureId(uint8_t nodeid);
uint16_t
GetNodeProductType(uint8_t nodeid);
uint16_t
GetNodeProductID(uint8_t nodeid);
uint8_t
SetNodeManufacture(uint8_t nodeid, uint8_t manu[NODE_MANUFACTURER_SIZE]);
void getNoDeviceMode(uint8_t* noListening, uint8_t* noNoneListening);
#endif