/****************************************************************************
 *
 * Z-Wave, the wireless language.
 *
 * Copyright (c) 2001-2011
 * Sigma Designs, Inc.
 * All Rights Reserved
 *
 *---------------------------------------------------------------------------
 *
 * Description:      Serial API 
 *
 * Last Changed By:  $Author$: 
 * Revision:         $Rev$: 
 * Last Changed:     $Date$: 
 *
 ****************************************************************************/

/****************************************************************************/
/*                              INCLUDE FILES                               */
/****************************************************************************/


/****************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                       */
/****************************************************************************/


#ifndef _ZW_SERIALAPI_H_
#define _ZW_SERIALAPI_H_

/* Serialapi serial communication definitions */
#define SOF 0x01  /* Start Of Frame */
#define ACK 0x06  /* Acknowledge successfull frame reception */
#define NAK 0x15  /* Not Acknowledge successfull frame reception - please retransmit... */
#define SYN 0x16  /* HOST Sync request */
#define CAN 0x18  /* Frame received (from host) was dropped - waiting for ACK */

/* Frame types */
#define REQUEST                                         0x00
#define RESPONSE                                        0x01

/* Flags used in SERIAL_API_GET_INIT_DATA */
#define GET_INIT_DATA_FLAG_SLAVE_API                    0x01
#define GET_INIT_DATA_FLAG_TIMER_SUPPORT                0x02
#define GET_INIT_DATA_FLAG_CONTROLLER_STATUS            0x04 /* Obsolete. USE next */
#define GET_INIT_DATA_FLAG_SECONDARY_CTRL               0x04
#define GET_INIT_DATA_FLAG_IS_SUC                       0x08


/* Function IDs - Definitions */
#define FUNC_ID_SERIAL_API_GET_INIT_DATA                0x02
#define FUNC_ID_SERIAL_API_APPL_NODE_INFORMATION        0x03
#define FUNC_ID_APPLICATION_COMMAND_HANDLER             0x04
#define FUNC_ID_ZW_GET_CONTROLLER_CAPABILITIES          0x05

/* SERIAL API ver 4 added - START */
#define FUNC_ID_SERIAL_API_SET_TIMEOUTS                 0x06
#define FUNC_ID_SERIAL_API_GET_CAPABILITIES             0x07
#define FUNC_ID_SERIAL_API_SOFT_RESET                   0x08
/* SERIAL API ver 4 added - END */

#define FUNC_ID_ZW_GET_PROTOCOL_VERSION                 0x09

/* Function ID for startup message */
#define FUNC_ID_SERIALAPI_STARTED                       0x0A

#define FUNC_ID_ZW_SET_RF_RECEIVE_MODE                  0x10
#define FUNC_ID_ZW_SET_SLEEP_MODE                       0x11
#define FUNC_ID_ZW_SEND_NODE_INFORMATION                0x12
#define FUNC_ID_ZW_SEND_DATA                            0x13
#define FUNC_ID_ZW_SEND_DATA_MULTI                      0x14
#define FUNC_ID_ZW_GET_VERSION                          0x15

/* SERIAL API ver 4 added - START */
#define FUNC_ID_ZW_SEND_DATA_ABORT                      0x16
#define FUNC_ID_ZW_RF_POWER_LEVEL_SET                   0x17
#define FUNC_ID_ZW_SEND_DATA_META                       0x18
/* SERIAL API ver 4 added - END */

#define FUNC_ID_ZW_SEND_DATA_MR                         0x19
#define FUNC_ID_ZW_SEND_DATA_META_MR                    0x1A
#define FUNC_ID_ZW_SET_ROUTING_INFO                     0x1B

#define FUNC_ID_ZW_GET_RANDOM                           0x1C
#define FUNC_ID_ZW_RANDOM                               0x1D
#define FUNC_ID_ZW_RF_POWER_LEVEL_REDISCOVERY_SET       0x1E
/* ZW030x only */
#define FUNC_ID_APPLICATION_RF_NOTIFY                   0x1F
/* ZW030x only end */

#define FUNC_ID_MEMORY_GET_ID                           0x20
#define FUNC_ID_MEMORY_GET_BYTE                         0x21
#define FUNC_ID_MEMORY_PUT_BYTE                         0x22
#define FUNC_ID_MEMORY_GET_BUFFER                       0x23
#define FUNC_ID_MEMORY_PUT_BUFFER                       0x24
/* SERIAL API ver 5 added - START */
#define FUNC_ID_SERIAL_API_GET_APPL_HOST_MEMORY_OFFSET  0x25
#define FUNC_ID_DEBUG_OUTPUT                            0x26
/* SERIAL API ver 5 added - END */
#define FUNC_ID_AUTO_PROGRAMMING                        0x27

#define FUNC_ID_NVR_GET_VALUE                           0x28

#define FUNC_ID_NVM_GET_ID                              0x29
#define FUNC_ID_NVM_EXT_READ_LONG_BUFFER                0x2A
#define FUNC_ID_NVM_EXT_WRITE_LONG_BUFFER               0x2B
#define FUNC_ID_NVM_EXT_READ_LONG_BYTE                  0x2C
#define FUNC_ID_NVM_EXT_WRITE_LONG_BYTE                 0x2D
#define FUNC_ID_NVM_BACKUP_RESTORE                      0x2E

#define FUNC_ID_ZW_NVR_GET_APP_VALUE                    0x2F
#define FUNC_ID_CLOCK_SET                               0x30
#define FUNC_ID_CLOCK_GET                               0x31
#define FUNC_ID_CLOCK_CMP                               0x32
#define FUNC_ID_RTC_TIMER_CREATE                        0x33
#define FUNC_ID_RTC_TIMER_READ                          0x34
#define FUNC_ID_RTC_TIMER_DELETE                        0x35
#define FUNC_ID_RTC_TIMER_CALL                          0x36

#define FUNC_ID_CLEAR_TX_TIMERS                         0x37
#define FUNC_ID_GET_TX_TIMERS                           0x38

#define FUNC_ID_ZW_GET_BACKGROUND_RSSI                  0x3B
#define FUNC_ID_ZW_REMOVE_NODE_ID_FROM_NETWORK          0x3F

#define FUNC_ID_ZW_SET_LEARN_NODE_STATE                 0x40
#define FUNC_ID_ZW_GET_NODE_PROTOCOL_INFO               0x41
#define FUNC_ID_ZW_SET_DEFAULT                          0x42
#define FUNC_ID_ZW_NEW_CONTROLLER                       0x43
#define FUNC_ID_ZW_REPLICATION_COMMAND_COMPLETE         0x44
#define FUNC_ID_ZW_REPLICATION_SEND_DATA                0x45
#define FUNC_ID_ZW_ASSIGN_RETURN_ROUTE                  0x46
#define FUNC_ID_ZW_DELETE_RETURN_ROUTE                  0x47
#define FUNC_ID_ZW_REQUEST_NODE_NEIGHBOR_UPDATE         0x48
#define FUNC_ID_ZW_APPLICATION_UPDATE                   0x49

/*Obsolete use ZW_APPLICATION_UPDATE */
#define FUNC_ID_ZW_APPLICATION_CONTROLLER_UPDATE        0x49

#define FUNC_ID_ZW_ADD_NODE_TO_NETWORK                  0x4A
#define FUNC_ID_ZW_REMOVE_NODE_FROM_NETWORK             0x4B
#define FUNC_ID_ZW_CREATE_NEW_PRIMARY                   0x4C
#define FUNC_ID_ZW_CONTROLLER_CHANGE                    0x4D

#define FUNC_ID_ZW_REQUEST_NODE_NEIGHBOR_UPDATE_MR      0x4E
#define FUNC_ID_ZW_ASSIGN_RETURN_ROUTE_MR               0x4F

/* Slave only */
#define FUNC_ID_ZW_SET_LEARN_MODE                       0x50
/* Slave only end */

#define FUNC_ID_ZW_ASSIGN_SUC_RETURN_ROUTE              0x51
#define FUNC_ID_ZW_ENABLE_SUC                           0x52
#define FUNC_ID_ZW_REQUEST_NETWORK_UPDATE               0x53
#define FUNC_ID_ZW_SET_SUC_NODE_ID                      0x54
#define FUNC_ID_ZW_DELETE_SUC_RETURN_ROUTE              0x55
#define FUNC_ID_ZW_GET_SUC_NODE_ID                      0x56
#define FUNC_ID_ZW_SEND_SUC_ID                          0x57

#define FUNC_ID_ZW_ASSIGN_SUC_RETURN_ROUTE_MR           0x58
#define FUNC_ID_ZW_REDISCOVERY_NEEDED                   0x59

#define FUNC_ID_ZW_REQUEST_NODE_NEIGHBOR_UPDATE_OPTION  0x5A

/* Slave only */
#define FUNC_ID_ZW_SUPPORT9600_ONLY                     0x5B
/* Slave only end */

/* Enhanced/Routing Slave only */
#define FUNC_ID_ZW_REQUEST_NEW_ROUTE_DESTINATIONS       0x5C
#define FUNC_ID_ZW_IS_NODE_WITHIN_DIRECT_RANGE          0x5D
/* Enhanced/Routing Slave only end */

#define FUNC_ID_ZW_EXPLORE_REQUEST_INCLUSION            0x5E
#define FUNC_ID_ZW_EXPLORE_REQUEST_EXCLUSION            0x5F

#define FUNC_ID_ZW_REQUEST_NODE_INFO                    0x60
#define FUNC_ID_ZW_REMOVE_FAILED_NODE_ID                0x61
#define FUNC_ID_ZW_IS_FAILED_NODE_ID                    0x62
#define FUNC_ID_ZW_REPLACE_FAILED_NODE                  0x63

#define FUNC_ID_TIMER_START                             0x70
#define FUNC_ID_TIMER_RESTART                           0x71
#define FUNC_ID_TIMER_CANCEL                            0x72
#define FUNC_ID_TIMER_CALL                              0x73

/* Installer API */
#define FUNC_ID_GET_ROUTING_TABLE_LINE                  0x80
#define FUNC_ID_GET_TX_COUNTER                          0x81
#define FUNC_ID_RESET_TX_COUNTER                        0x82
#define FUNC_ID_STORE_NODEINFO                          0x83
#define FUNC_ID_STORE_HOMEID                            0x84
/* Installer API only end */

#define FUNC_ID_LOCK_ROUTE_RESPONSE                     0x90

#define FUNC_ID_ZW_GET_LAST_WORKING_ROUTE               0x92
#define FUNC_ID_ZW_SET_LAST_WORKING_ROUTE               0x93

#ifdef ZW_CONTROLLER_SINGLE
#define FUNC_ID_SERIAL_API_TEST                         0x95
#endif

/* ZW_CONTROLLER_BRIDGE only START */
#define FUNC_ID_SERIAL_API_APPL_SLAVE_NODE_INFORMATION  0xA0
#define FUNC_ID_APPLICATION_SLAVE_COMMAND_HANDLER       0xA1
#define FUNC_ID_ZW_SEND_SLAVE_NODE_INFORMATION          0xA2
#define FUNC_ID_ZW_SEND_SLAVE_DATA                      0xA3
#define FUNC_ID_ZW_SET_SLAVE_LEARN_MODE                 0xA4
#define FUNC_ID_ZW_GET_VIRTUAL_NODES                    0xA5
#define FUNC_ID_ZW_IS_VIRTUAL_NODE                      0xA6
#define FUNC_ID_ZW_SEND_SLAVE_DATA_MR                   0xA7


#define VIRTUAL_SLAVE_LEARN_MODE_DISABLE                0x00
#define VIRTUAL_SLAVE_LEARN_MODE_ENABLE                 0x01
#define VIRTUAL_SLAVE_LEARN_MODE_ADD                    0x02
#define VIRTUAL_SLAVE_LEARN_MODE_REMOVE                 0x03

#define ASSIGN_COMPLETE                                 0x00
#define ASSIGN_NODEID_DONE                              0x01
#define ASSIGN_RANGE_INFO_UPDATE                        0x02


/* DevKit 4.5x added - obsoletes FUNC_ID_APPLICATION_SLAVE_COMMAND_HANDLER and */
/* FUNC_ID_APPLICATION_COMMAND_HANDLER for the Controller Bridge applications as */
/* this handles both cases - only for 4.5x based Controller Bridge applications */
#define FUNC_ID_APPLICATION_COMMAND_HANDLER_BRIDGE      0xA8
/* DevKit 4.5x added - Adds sourceNodeID to the parameter list */
#define FUNC_ID_ZW_SEND_DATA_BRIDGE                     0xA9
#define FUNC_ID_ZW_SEND_DATA_META_BRIDGE                0xAA
#define FUNC_ID_ZW_SEND_DATA_MULTI_BRIDGE               0xAB
/* ZW_CONTROLLER_BRIDGE only END */

#define FUNC_ID_PWR_SETSTOPMODE                         0xB0    // ZW102 only
#define FUNC_ID_PWR_CLK_PD                              0xB1    // ZW102 only
#define FUNC_ID_PWR_CLK_PUP                             0xB2    // ZW102 only
#define FUNC_ID_PWR_SELECT_CLK                          0xB3    // ZW102 only
#define FUNC_ID_ZW_SET_WUT_TIMEOUT                      0xB4    // ZW201 only
#define FUNC_ID_ZW_IS_WUT_KICKED                        0xB5    // ZW201 only

#define FUNC_ID_ZW_WATCHDOG_ENABLE                      0xB6
#define FUNC_ID_ZW_WATCHDOG_DISABLE                     0xB7
#define FUNC_ID_ZW_WATCHDOG_KICK                        0xB8
#define FUNC_ID_ZW_SET_EXT_INT_LEVEL                    0xB9    // ZW201 only

#define FUNC_ID_ZW_RF_POWER_LEVEL_GET                   0xBA
#define FUNC_ID_ZW_GET_NEIGHBOR_COUNT                   0xBB
#define FUNC_ID_ZW_ARE_NODES_NEIGHBOURS                 0xBC

#define FUNC_ID_ZW_TYPE_LIBRARY                         0xBD
#define FUNC_ID_ZW_SEND_TEST_FRAME                      0xBE
#define FUNC_ID_ZW_GET_PROTOCOL_STATUS                  0xBF

#define FUNC_ID_ZW_SET_PROMISCUOUS_MODE                 0xD0
/* SERIAL API ver 5 added - START */
#define FUNC_ID_PROMISCUOUS_APPLICATION_COMMAND_HANDLER 0xD1
/* SERIAL API ver 5 added - END */

#define FUNC_ID_ZW_WATCHDOG_START                       0xD2
#define FUNC_ID_ZW_WATCHDOG_STOP                        0xD3

#define FUNC_ID_ZW_SET_ROUTING_MAX                      0xD4
#define OBSOLETE_FUNC_ID_ZW_GET_ROUTING_MAX             0xD5

/* Allocated for Power Management */
#define FUNC_ID_SERIAL_API_POWER_MANAGEMENT             0xEE
#define FUNC_ID_SERIAL_API_READY                        0xEF

/* Allocated for proprietary serial API commands */
#define FUNC_ID_PROPRIETARY_0                           0xF0
#define FUNC_ID_PROPRIETARY_1                           0xF1
#define FUNC_ID_PROPRIETARY_2                           0xF2
#define FUNC_ID_PROPRIETARY_3                           0xF3
#define FUNC_ID_PROPRIETARY_4                           0xF4
#define FUNC_ID_PROPRIETARY_5                           0xF5
#define FUNC_ID_PROPRIETARY_6                           0xF6
#define FUNC_ID_PROPRIETARY_7                           0xF7
#define FUNC_ID_PROPRIETARY_8                           0xF8
#define FUNC_ID_PROPRIETARY_9                           0xF9
#define FUNC_ID_PROPRIETARY_A                           0xFA
#define FUNC_ID_PROPRIETARY_B                           0xFB
#define FUNC_ID_PROPRIETARY_C                           0xFC
#define FUNC_ID_PROPRIETARY_D                           0xFD
#define FUNC_ID_PROPRIETARY_E                           0xFE


/* Illegal function ID */
#define FUNC_ID_UNKNOWN                                 0xFF


// Defines
#define CMDBUF_CMDCLASS_OFFSET 0
#define CMDBUF_CMD_OFFSET 1
#define CMDBUF_PARM1_OFFSET 2
#define CMDBUF_PARM2_OFFSET 3
#define CMDBUF_PARM3_OFFSET 4
#define CMDBUF_PARM4_OFFSET 5
#define CMDBUF_PARM5_OFFSET 6
#define CMDBUF_PARM6_OFFSET 7

/* Flags used by capabilities byte in SerialAPI_GetInitData */
#define GET_INIT_DATA_FLAG_SLAVE_API        0x01
#define GET_INIT_DATA_FLAG_TIMER_SUPPORT    0x02
#define GET_INIT_DATA_FLAG_SECONDARY_CTRL   0x04
#define GET_INIT_DATA_FLAG_IS_SUC           0x08

/* Defined libraries - Received when calling ZW_Version */
#define ZW_LIB_CONTROLLER_STATIC  0x01
#define ZW_LIB_CONTROLLER         0x02
#define ZW_LIB_SLAVE_ENHANCED     0x03
#define ZW_LIB_SLAVE              0x04
#define ZW_LIB_INSTALLER          0x05
#define ZW_LIB_SLAVE_ROUTING      0x06
#define ZW_LIB_CONTROLLER_BRIDGE  0x07
#define ZW_LIB_DUT                0x08

/* Defines for ZW_GetControllerCapabilities */
#define CONTROLLER_CAPABILITIES_IS_SECONDARY                 0x01
#define CONTROLLER_CAPABILITIES_ON_OTHER_NETWORK             0x02
#define CONTROLLER_CAPABILITIES_NODEID_SERVER_PRESENT        0x04
#define CONTROLLER_CAPABILITIES_IS_REAL_PRIMARY              0x08
#define CONTROLLER_CAPABILITIES_IS_SUC                       0x10
#define CONTROLLER_CAPABILITIES_NO_NODES_INCUDED             0x20

#define ZW_SUC_FUNC_BASIC_SUC                                0x00
#define ZW_SUC_FUNC_NODEID_SERVER                            0x01

/*remote status mask defines*/
#define REMOTE_STATUS_SLAVE_BIT        0x01  /*1 indicates slave*/

/* SetLearnNodeState parameter */
/*#define LEARN_NODE_STATE_OFF    0
#define LEARN_NODE_STATE_NEW    1
#define LEARN_NODE_STATE_UPDATE 2
#define LEARN_NODE_STATE_DELETE 3
*/

// Mode parameters to ZW_AddNodeToNetwork
#define ADD_NODE_ANY         0x01
#define ADD_NODE_CONTROLLER  0x02
#define ADD_NODE_SLAVE       0x03
#define ADD_NODE_EXISTING    0x04
#define ADD_NODE_STOP        0x05
#define ADD_NODE_STOP_FAILED 0x06

#define ADD_NODE_MODE_MASK                   0x0F
#define ADD_NODE_OPTION_NORMAL_POWER         0x80
#define ADD_NODE_OPTION_NETWORK_WIDE         0x40

// Callback states from ZW_AddNodeToNetwork
#define ADD_NODE_STATUS_LEARN_READY          0x01
#define ADD_NODE_STATUS_NODE_FOUND           0x02
#define ADD_NODE_STATUS_ADDING_SLAVE         0x03
#define ADD_NODE_STATUS_ADDING_CONTROLLER    0x04
#define ADD_NODE_STATUS_PROTOCOL_DONE        0x05
#define ADD_NODE_STATUS_DONE                 0x06
#define ADD_NODE_STATUS_FAILED               0x07
#define ADD_NODE_HOME_ID                     0x08
#define ADD_NODE_SMART_START                 0x09
#define ADD_NODE_STATUS_ADD_SECURE_FAILED    0xFD
#define ADD_NODE_STATUS_INTERVIEW_DEV_FAILED 0xFF

#define ADD_SECURE_NODE_STATUS_LEARN_READY           0x10
#define ADD_SECURE_NODE_STATUS_NODE_FOUND            0x11
#define ADD_SECURE_NODE_STATUS_ADDING_SLAVE          0x12
#define ADD_SECURE_NODE_STATUS_ADDING_CONTROLLER     0x13
#define ADD_SECURE_NODE_STATUS_PROTOCOL_DONE         0x14
#define ADD_SECURE_NODE_STATUS_DONE                  0x15
#define ADD_SECURE_NODE_STATUS_FAILED                0x16
#define ADD_SECURE_2_NODE_STATUS_DONE                0x17
#define ADD_SECURE_2_NODE_STATUS_FAILED              0x18

#define NODE_ADD_KEYS_SET_EX_ACCEPT_BIT              0x01
#define NODE_ADD_KEYS_SET_EX_CSA_BIT                 0x02

#define INCLUDED_SECURE_NODE_STATUS_LEARN_READY      0x01
#define INCLUDED_SECURE_NODE_STATUS_LEARN_FAILED     0x02
#define INCLUDED_SECURE_NODE_STATUS_LEARN_DONE       0x03


#define ZW_SET_LEARN_MODE_CLASSIC                     0x01
#define ZW_SET_LEARN_MODE_NWI                         0x02
#define ZW_SET_LEARN_MODE_DISABLE                     0x00


#define REPLACE_FAILED_NODE_TIMEOUT                   0x01
#define REPLACE_FAILED_NODE_DONE                      0x02
#define REPLACE_FAILED_NODE_FAILED                    0x03
#define REPLACE_FAILED_NODE_TIMEOUT_WAITING_NIF       0x04


#define ADD_REMOVE_NODE_TIMEOUT                       0xFE
/*Status to supportCommandClassInclusionController*/
#define INCLUSION_CONTROLLER_START                    0x01
#define INCLUSION_CONTROLLER_TIMEOUT                  0x02
/*Status to controlFirmwareUpdateNotify*/
#define FIRMWARE_UPDATE_START                         0x01
#define FIRMWARE_UPDATE_STOP_BY_USER                  0x02
#define FIRMWARE_UPDATE_TIMOUT_STATE                  0x03
#define FIRMWARE_UPDATE_SUCCESS                       0x04
#define FIRMWARE_UPDATE_FAIL                          0x05
#define FIRMWARE_UPDATE_FAIL_TO_OPEN_FILE             0x06
#define FIRMWARE_UPDATE_DEVICE_NOT_RESPONE            0x07
#define WAITING_FIRMWARE_UPDATE                       0x08


#define CONTROLLER_CHANGE_START       ADD_NODE_CONTROLLER
#define CONTROLLER_CHANGE_STOP        ADD_NODE_STOP
#define CONTROLLER_CHANGE_STOP_FAILED ADD_NODE_STOP_FAILED

// Mode parameters to ZW_RemoveNodeFromNetwork 
#define REMOVE_NODE_ANY                        ADD_NODE_ANY
#define REMOVE_NODE_CONTROLLER                 ADD_NODE_CONTROLLER
#define REMOVE_NODE_SLAVE                      ADD_NODE_SLAVE
#define REMOVE_NODE_STOP                       ADD_NODE_STOP
#define REMOVE_NODE_OPTION_NETWORK_WIDE        ADD_NODE_OPTION_NETWORK_WIDE


// Callback states to ZW_RemoveNodeFrom
#define REMOVE_NODE_STATUS_LEARN_READY         ADD_NODE_STATUS_LEARN_READY
#define REMOVE_NODE_STATUS_NODE_FOUND          ADD_NODE_STATUS_NODE_FOUND
#define REMOVE_NODE_STATUS_REMOVING_SLAVE      ADD_NODE_STATUS_ADDING_SLAVE
#define REMOVE_NODE_STATUS_REMOVING_CONTROLLER ADD_NODE_STATUS_ADDING_CONTROLLER
#define REMOVE_NODE_STATUS_DONE                ADD_NODE_STATUS_DONE
#define REMOVE_NODE_STATUS_FAILED              ADD_NODE_STATUS_FAILED
#define REMOVE_NODE_STATUS_BUSY                ADD_NODE_SMART_START



/* ZW_RemoveFailedNode and ZW_ReplaceFailedNode return value definitions */
#define  NOT_PRIMARY_CONTROLLER             1 /* The removing process was */
                                              /* aborted because the controller */
                                              /* is not the primary one */

#define  NO_CALLBACK_FUNCTION               2 /* The removing process was */
                                              /* aborted because no call back */
                                              /* function is used */
#define  FAILED_NODE_NOT_FOUND              3 /* The removing process aborted */
                                              /* because the node was found */
#define  FAILED_NODE_REMOVE_PROCESS_BUSY    4 /* The removing process is busy */
#define  FAILED_NODE_REMOVE_FAIL            5 /* The removing process could not */
                                              /* be started */

#define ZW_FAILED_NODE_REMOVE_STARTED       0 /* The removing/replacing failed node process started */
#define ZW_NOT_PRIMARY_CONTROLLER           (1 << NOT_PRIMARY_CONTROLLER)
#define ZW_NO_CALLBACK_FUNCTION             (1 << NO_CALLBACK_FUNCTION)
#define ZW_FAILED_NODE_NOT_FOUND            (1 << FAILED_NODE_NOT_FOUND)
#define ZW_FAILED_NODE_REMOVE_PROCESS_BUSY  (1 << FAILED_NODE_REMOVE_PROCESS_BUSY)
#define ZW_FAILED_NODE_REMOVE_FAIL          (1 << FAILED_NODE_REMOVE_FAIL)


/* ZW_RemoveFailedNode and ZW_ReplaceFailedNode callback status definitions */
#define ZW_NODE_OK                          0 /* The node is working properly (removed from the failed nodes list ) */

/* ZW_RemoveFailedNode callback status definitions */
#define ZW_FAILED_NODE_REMOVED              1 /* The failed node was removed from the failed nodes list */
#define ZW_FAILED_NODE_NOT_REMOVED          2 /* The failed node was not removed from the failing nodes list */

/* ZW_ReplaceFailedNode callback status definitions */
#define ZW_FAILED_NODE_REPLACE              3 /* The failed node are ready to be replaced and controller */
                                              /* is ready to add new node with nodeID of the failed node */
#define ZW_FAILED_NODE_REPLACE_DONE         4 /* The failed node has been replaced */
#define ZW_FAILED_NODE_REPLACE_FAILED       5 /* The failed node has not been replaced */

#define ZW_PRIORITY_ROUTE_ZW_NO_VALID 		0x00 /*There is no route defined for the target
												NodeID. In this case, the Repeater and
												Speed fields MUST be set to 0x00 and
												ignored by a receiving node.*/
#define ZW_PRIORITY_ROUTE_ZW_LWR 			0x01 /*The returned route is a last working route.
												The Last Working route is the last
												successful route used between the sender
												and receiver. */
#define ZW_PRIORITY_ROUTE_ZW_NLWR 			0x02 /*The returned route is a next to last working route.
												It is a route which was Last Working Route
												and has been replaced by a new route.*/
#define ZW_PRIORITY_ROUTE_APP_PR 			0x10 /* The returned has been determined by the
												application */


// SetLearnMode callback status
#define LEARN_MODE_STARTED                 ADD_NODE_STATUS_LEARN_READY
#define LEARN_MODE_DONE                    ADD_NODE_STATUS_DONE
#define LEARN_MODE_FAILED                  ADD_NODE_STATUS_FAILED
#define LEARN_MODE_DELETED                 0x80

// SetLearnNodeState callback status 
#define LEARN_STATE_ROUTING_PENDING    0x21
#define LEARN_STATE_DONE               0x22
#define LEARN_STATE_FAIL               0x20

// idleLearnState callback status
#define UPDATE_STATE_NODE_INFO_FOREIGN_HOMEID_RECEIVED  0x85
#define UPDATE_STATE_NODE_INFO_RECEIVED                 0x84
#define UPDATE_STATE_NODE_INFO_REQ_DONE                 0x82
#define UPDATE_STATE_NODE_INFO_REQ_FAILED               0x81
#define UPDATE_STATE_ROUTING_PENDING                    0x80
#define UPDATE_STATE_NEW_ID_ASSIGNED                    0x40
#define UPDATE_STATE_DELETE_DONE                        0x20
#define UPDATE_STATE_SUC_ID                             0x10


/* ZW_NewController parameter */
#define NEW_CTRL_STATE_SEND             0x01
#define NEW_CTRL_STATE_RECEIVE          0x02
#define NEW_CTRL_STATE_STOP_OK          0x03
#define NEW_CTRL_STATE_STOP_FAIL        0x04
#define NEW_CTRL_STATE_ABORT            0x05
#define NEW_CTRL_STATE_CHANGE           0x06
#define NEW_CTRL_STATE_DELETE           0x07
#define NEW_CTRL_STATE_MAKE_NEW         0x08

// NewController callback status 
#define NEW_CONTROLLER_FAILED           0x00
#define NEW_CONTROLLER_DONE             0x01
#define NEW_CONTROLLER_LEARNED          0x02
#define NEW_CONTROLLER_CHANGE_DONE      0x03
#define NEW_CONTROLLER_DELETED          0x04
#define NEW_CONTROLLER_MAKE_NEW_DONE    0x05

/* Listening bit in NODEINFO capability */
#define NODEINFO_LISTENING_MASK     0x80
#define NODEINFO_LISTENING_SUPPORT  0x80
/* Routing bit in the NODEINFO capability byte */
#define NODEINFO_ROUTING_SUPPORT            0x40

/* Beam wakeup mode type bits in the NODEINFO security byte */
#define NODEINFO_ZWAVE_SENSOR_MODE_WAKEUP_1000   0x40
#define NODEINFO_ZWAVE_SENSOR_MODE_WAKEUP_250    0x20

/* ZW_RequestNetworkUpdate callback values*/
#define ZW_SUC_UPDATE_DONE      0x00
#define ZW_SUC_UPDATE_ABORT     0x01
#define ZW_SUC_UPDATE_WAIT      0x02

#define ZW_SUC_UPDATE_DISABLED  0x03
#define ZW_SUC_UPDATE_OVERFLOW  0x04

#define ZW_SUC_SET_SUCCEEDED    0x05
#define ZW_SUC_SET_FAILED       0x06

// Received frame status flags
// Received Z-Wave frame status flags - as received in ApplicationCommandHandler
#define RECEIVE_STATUS_ROUTED_BUSY  	0x01
// Received Z-Wave frame status flag telling that received frame was sent with low power
#define RECEIVE_STATUS_LOW_POWER    	0x02
// Received Z-Wave frame status mask used to mask Z-Wave frame type flag bits out
#define RECEIVE_STATUS_TYPE_MASK    	0x1C
// Receíved Z-wave Received frame is singlecast frame - (status == xxxx00xx)
#define RECEIVE_STATUS_TYPE_SINGLE  	0x00
// Receíved Z-wave Received frame is broadcast frame - (status == xxxx01xx)
#define RECEIVE_STATUS_TYPE_BROAD   	0x04
// Receíved Z-wave Received frame is multicast frame - (status == xxxx10xx)
#define RECEIVE_STATUS_TYPE_MULTI   	0x08
// Received Z-wave Received frame is explorer frame - (statux == xxx100xx)
#define RECEIVE_STATUS_TYPE_EXPLORE 	0x10
// Received frame is not send to me (useful only in promiscuous mode)
#define RECEIVE_STATUS_FOREIGN_FRAME  	0x40
/**
 * Received frame is from a foreign homeid.
 *
 * Used by ApplicationControllerUpdate() with status UPDATE_STATE_INCLUDED_NODE_INFO_RECEIVED.
 * Currently undocumented in the from embedded SDK.
 */
#define RECEIVE_STATUS_FOREIGN_HOMEID  			  0x80
#define UPDATE_STATE_INCLUDED_NODE_INFO_RECEIVED  0x86


typedef union _convertFloatByteAr_
{
    float fVal;
    unsigned char baVal[4];
} convertFloatByteAr;

typedef struct _NODE_CAPABILITY_
{
   uint8_t noCapability;
   uint8_t aCapability[32];
} NODE_CAPABILITY;

typedef struct _NODE_TYPE_
{
   uint8_t basic;
   uint8_t generic;
   uint8_t specific;
} NODE_TYPE;

typedef struct _APPL_NODE_TYPE_
{
   uint8_t generic;
   uint8_t specific;
} APPL_NODE_TYPE;

/* Node info stored within the non-volatile memory */
/* This are the first (protocol part) payload bytes from the Node Infomation frame */
typedef struct _NODEINFO_
{
   uint8_t  capability;     /* Network capabilities */
   uint8_t  security;       /* Network security */
   uint8_t  reserved;
   NODE_TYPE nodeType;   /* Basic, Generic and Specific Device Types */
} NODEINFO;

typedef struct _CLOCK_
{
   uint8_t  weekday;  /* Weekday 1:monday...7:sunday   */
   uint8_t  hour;     /* Hour 0...23                   */
   uint8_t  minute;   /* Minute 0...59                 */
} CLOCK;

typedef struct _RTC_TIMER_
{
   uint8_t  status;         /* Timer element status                                  */
   CLOCK timeOn;         /* Weekday, all days, work days or weekend callback time */
   CLOCK timeOff;        /*  , Hour callback time                                 */
                         /*  , Minute callback time                               */
   uint8_t  repeats;        /* Number of callback times (-1: forever)                */
   void (*func)(uint8_t status, uint8_t parm);   /* Timer function address              */
   uint8_t  parm;           /* Parameter that is returned to the timer function      */
} RTC_TIMER;    

/* RTC_TIMER status */
#define RTC_STATUS_FIRED    0x40    /* Set when the "on" timer callback function is called */
                                    /* Cleared when the "off" timer callback function is called */
#define RTC_STATUS_APPL_MASK 0x0F   /* Bits reserved for the Application programs */
                                    /*   The appl. bits are stored when the timer is created */                

/* day definitions */
#define RTC_ALLDAYS   0
#define RTC_MONDAY    1
#define RTC_TUESDAY   2
#define RTC_WEDNESDAY 3
#define RTC_THURSDAY  4
#define RTC_FRIDAY    5 
#define RTC_SATURDAY  6
#define RTC_SUNDAY    7
#define RTC_WORKDAYS  8
#define RTC_WEEKEND   9


/* From ZW_transport.h --- begin */
/* Transmit frame option flags */
#define TRANSMIT_OPTION_ACK         0x01   /* request acknowledge from destination node */
#define TRANSMIT_OPTION_LOW_POWER   0x02   /* transmit at low output power level (1/3 of normal RF range)*/  
#define TRANSMIT_OPTION_AUTO_ROUTE  0x04   /* request retransmission via repeater nodes */
/* do not use response route - Even if available */
#define TRANSMIT_OPTION_NO_ROUTE      0x10

/* Use explore frame if needed */
#define TRANSMIT_OPTION_EXPLORE       0x20

/* Transmit frame option flag which are valid when sending explore frames  */
#define TRANSMIT_EXPLORE_OPTION_ACK         TRANSMIT_OPTION_ACK
#define TRANSMIT_EXPLORE_OPTION_LOW_POWER   TRANSMIT_OPTION_LOW_POWER

/* Received frame status flags */
#define RECEIVE_STATUS_ROUTED_BUSY    0x01
#define RECEIVE_STATUS_LOW_POWER      0x02  /* received at low output power level */
#define RECEIVE_STATUS_TYPE_MASK      0x1C  /* Mask for masking out the received frametype bits */
#define RECEIVE_STATUS_TYPE_SINGLE    0x00  /* Received frame is singlecast frame (rxOptions == xxxx00xx) */
#define RECEIVE_STATUS_TYPE_BROAD     0x04  /* Received frame is broadcast frame  (rxOptions == xxxx01xx) */
#define RECEIVE_STATUS_TYPE_MULTI     0x08  /* Received frame is multiecast frame (rxOptions == xxxx10xx) */
#define RECEIVE_STATUS_FOREIGN_FRAME  0x40  /* Received frame is not send to me (useful only in promiscuous mode) */
#define RECEIVE_STATUS_TYPE_EXPLORE   0x10  /* Received frame is an explore frame */


/* Predefined Node ID's */
#define NODE_BROADCAST    0xFF    /* broadcast */

/* Transmit complete codes */
#define TRANSMIT_COMPLETE_OK      0x00  
#define TRANSMIT_COMPLETE_NO_ACK  0x01  /* retransmission error */
#define TRANSMIT_COMPLETE_FAIL    0x02  /* transmit error */ 

/* Rediscovery complete codes */
#define REDISCOVERY_DONE_OK       0x00  
#define REDISCOVERY_DONE_FAIL  	  0x01  
#define REDISCOVERY_STOP     	  0x02  
#define REDISCOVERY_NOT_RUNNING   0x03  

/*#ifdef ZW_CONTROLLER*/
/* Assign route transmit complete but no routes was found */
#define TRANSMIT_COMPLETE_NOROUTE 0x04  /* no route found in assignroute */
                                        /* therefore nothing was transmitted */
/*#endif*/
#define TRANSMIT_COMPLETE_HOP_0_FAIL  0x05
#define TRANSMIT_COMPLETE_HOP_1_FAIL  0x06
#define TRANSMIT_COMPLETE_HOP_2_FAIL  0x07
#define TRANSMIT_COMPLETE_HOP_3_FAIL  0x08
#define TRANSMIT_COMPLETE_HOP_4_FAIL  0x09

/* Max hops in route */
#define TRANSMIT_ROUTED_ATTEMPT   0x08

/* From ZW_transport.h --- end */

#define ZW_MAX_NODES                    232
#define ZW_MAX_REPEATERS                4
#define ZW_MAX_NODEMASK_LENGTH          (ZW_MAX_NODES/8)

#define ZW_MAX_NODES_IN_MULTICAST       64     /* Maximum number of nodes in a multicast */
/* Max hops in route */
#define MAX_REPEATERS      				4
#define MAX_CHANNELS 					3       /* 3 channels is maximum in Z-Wave */


/* HOST Power Management defines */

/* Power Management Command definitions. */
#define PM_PIN_UP_CONFIGURATION_CMD           0x01
#define PM_MODE_CONFIGURATION_CMD             0x02
#define PM_POWERUP_ZWAVE_CONFIGURATION_CMD    0x03
#define PM_POWERUP_TIMER_CONFIGURATION_CMD    0x04
#define PM_POWERUP_EXTERNAL_CONFIGURATION_CMD 0x05
#define PM_SET_POWER_MODE_CMD                 0x06
#define PM_GET_STATUS                         0x07

/* HOST Power Up reason definitions */
#define PM_WAKEUP_REASON_NONE                 0x00
#define PM_WAKEUP_REASON_EXTERNAL             0x01
#define PM_WAKEUP_REASON_RF_ALL               0x02
#define PM_WAKEUP_REASON_RF_ALL_NO_BROADCAST  0x03
#define PM_WAKEUP_REASON_RF_MASK              0x04
#define PM_WAKEUP_REASON_TIMER_SECONDS        0x05
#define PM_WAKEUP_REASON_TIMER_MINUTES        0x06

/* Pin definitions */
#define PM_PHYSICAL_PIN_P00         0x00
#define PM_PHYSICAL_PIN_P01         0x01
#define PM_PHYSICAL_PIN_P02         0x02
#define PM_PHYSICAL_PIN_P03         0x03
#define PM_PHYSICAL_PIN_P04         0x04
#define PM_PHYSICAL_PIN_P05         0x05
#define PM_PHYSICAL_PIN_P06         0x06
#define PM_PHYSICAL_PIN_P07         0x07
#define PM_PHYSICAL_PIN_P10         0x10
#define PM_PHYSICAL_PIN_P11         0x11
#define PM_PHYSICAL_PIN_P12         0x12
#define PM_PHYSICAL_PIN_P13         0x13
#define PM_PHYSICAL_PIN_P14         0x14
#define PM_PHYSICAL_PIN_P15         0x15
#define PM_PHYSICAL_PIN_P16         0x16
#define PM_PHYSICAL_PIN_P17         0x17
#define PM_PHYSICAL_PIN_P22         0x22
#define PM_PHYSICAL_PIN_P23         0x23
#define PM_PHYSICAL_PIN_P24         0x24
#define PM_PHYSICAL_PIN_P30         0x30
#define PM_PHYSICAL_PIN_P31         0x31
#define PM_PHYSICAL_PIN_P32         0x32
#define PM_PHYSICAL_PIN_P33         0x33
#define PM_PHYSICAL_PIN_P34         0x34
#define PM_PHYSICAL_PIN_P35         0x35
#define PM_PHYSICAL_PIN_P36         0x36
#define PM_PHYSICAL_PIN_P37         0x37
#define PM_PHYSICAL_PIN_MAX         PM_PHYSICAL_PIN_P37
#define PM_PHYSICAL_PIN_UNDEFINED   0xFF

#define PM_IO_PIN_MAX               4

/* Power Management RF Wakeup modes */
#define PM_WAKEUP_UNDEFINED         0x00
#define PM_WAKEUP_ALL               0x01
#define PM_WAKEUP_ALL_NO_BROADCAST  0x02
#define PM_WAKEUP_MASK              0x03
#define PM_WAKEUP_MODE_MAX          PM_WAKEUP_MASK

#define PM_WAKEUP_MAX_BYTES         8
#define PM_MASK_DONTCARE            0

/* Power Management Timer Resolution definitions */
#define PM_TIMER_UNDEFINED          0x00
#define PM_TIMER_SECONDS            0x01
#define PM_TIMER_MINUTES            0x02
#define PM_TIMER_MODE_MAX           PM_TIMER_MINUTES

/* Z-Wave RF speed definitions */
#define ZW_RF_SPEED_NONE                        0x00
#define ZW_RF_SPEED_9600                        0x01
#define ZW_RF_SPEED_40K                         0x02
#define ZW_RF_SPEED_100K                        0x03
#define ZW_RF_SPEED_MASK                        0x07

/* ZW_GetLastWorkingRoute and ZW_SetLastWorkingRoute Route structure definitions */
#define ROUTECACHE_LINE_CONF_SIZE               1
#define ROUTECACHE_LINE_SIZE                    (ZW_MAX_REPEATERS + ROUTECACHE_LINE_CONF_SIZE)

/* LastWorkingRoute index definitions */
#define ROUTECACHE_LINE_REPEATER_1_INDEX        0
#define ROUTECACHE_LINE_REPEATER_2_INDEX        1
#define ROUTECACHE_LINE_REPEATER_3_INDEX        2
#define ROUTECACHE_LINE_REPEATER_4_INDEX        3
#define ROUTECACHE_LINE_CONF_INDEX              4

/* ZW_GetLastWorkingRoute and ZW_SetLastWorkingRoute speed definitions */
#define ZW_LAST_WORKING_ROUTE_SPEED_AUTO        ZW_RF_SPEED_NONE
#define ZW_LAST_WORKING_ROUTE_SPEED_9600        ZW_RF_SPEED_9600
#define ZW_LAST_WORKING_ROUTE_SPEED_40K         ZW_RF_SPEED_40K
#define ZW_LAST_WORKING_ROUTE_SPEED_100K        ZW_RF_SPEED_100K


/* RSSI defines used in ZW_SendData callback and ZW_GetCurrentNoiseLevel */
#define RSSI_NOT_AVAILABLE 						127   /* RSSI measurement not available */
#define RSSI_MAX_POWER_SATURATED 				126   /* Receiver saturated. RSSI too high to measure precisely. */
#define RSSI_BELOW_SENSITIVITY 					125   /* No signal detected. The RSSI is too low to measure precisely. */
#define RSSI_RESERVED_START    					11    /* All values above and including RSSI_RESERVED_START are reserved,
                                        except those defined above.*/




/* Additional tx status information. Delivered with ZW_Senddata() callback. */
typedef struct 
{
	uint8_t bRepeaters;         /* Repeaters in route, zero for direct range */
	/* rssi_values per hop for direct and routed frames.
	* Contains repeaters + 1 value pairs. Each pair contains an
	* incoming and an outgoing rssi value. */
	uint8_t rssi_values[MAX_REPEATERS + 1];
	uint16_t transmitTicks;
	uint8_t ackChannel;
	uint8_t lastTxChannel;
	uint8_t routeSchemeState;
	uint8_t repeaters[4];
	uint8_t routeSpeed;
	uint8_t routeTries;
	uint8_t lastFailedLinkFrom;
	uint8_t lastFailedLinkTo;
} TX_STATUS_TYPE;


#endif