#ifdef __cplusplus
extern "C"{
#endif 
#ifndef _ZWAVE_API_
#define _ZWAVE_API_

#include <stdint.h>
#include <stdbool.h>
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"
#include "S2.h"
#include "S2_wrap.h"
#include "resource_directory.h"
#include "transport_service2_external.h"


#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

#define LASTEST_FIRMWARE_0_VERSION                                  6
#define LASTEST_FIRMWARE_0_SUB_VERSION                              02
#define LASTEST_HARDWARE_VERSION                                    6 /*RevF*/


#define COMMAND_CLASS_SPECIFIC_NODE_ADD                             0x01
#define COMMAND_CLASS_SPECIFIC_NODE_REMOVE                          0x02
#define COMMAND_CLASS_SPECIFIC_NODE_GET_LIST                        0x03


#define COMMAND_CLASS_SPECIFIC_NODE_SET_DEFAULT                     0x04

#define COMMAND_CLASS_SPECIFIC_NETWORK_TEST_CONNECTION              0x05
#define COMMAND_CLASS_SPECIFIC_NETWORK_DUMP_NEIGHBORS               0x06

#define COMMAND_CLASS_SPECIFIC_NODE_SET_LEARN_MODE                  0x07
#define COMMAND_CLASS_SPECIFIC_NODE_SET_SUC_SIS_MODE                0x08

#define COMMAND_CLASS_SPECIFIC_NODE_REQUEST_UPDATE                  0x09
#define COMMAND_CLASS_SPECIFIC_NODE_SET_SLAVE_LEARN_MODE            0x0A


#define COMMAND_CLASS_SPECIFIC_NODE_REMOVE_FORCE                    0x0B
#define COMMAND_CLASS_SPECIFIC_NODE_CONTROLLER_CHANGE               0x0C

#define COMMAND_CLASS_SPECIFIC_NODE_POLLING_BASIC_TEST              0x0D
#define COMMAND_CLASS_SPECIFIC_NODE_REPLACE_FAILED_NODE             0x0E
#define COMMAND_CLASS_SPECIFIC_NODE_IS_FAILED_NODE                  0x0F
#define COMMAND_CLASS_SPECIFIC_NODE_ADD_NODE_ZPC                    0x10
#define COMMAND_CLASS_SPECIFIC_NODE_ADD_NODE_ZPC_AUTO               0x11
#define COMMAND_CLASS_SPECIFIC_NODE_REMOVE_NODE_ZPC                 0x12
#define COMMAND_CLASS_SPECIFIC_NODE_REMOVE_NODE_ZPC_AUTO            0x13
#define COMMAND_CLASS_SPECIFIC_NODE_GET_CONTROLLER_INFO_ZPC         0x14

#define COMMAND_CLASS_SPECIFIC_CONTROLLER_BACKUP                    0x15
#define COMMAND_CLASS_SPECIFIC_CONTROLLER_RESTORE                   0x16
#define COMMAND_CLASS_SPECIFIC_S2_BOOTSTRAPPING_KEX_REPORT          0x17
#define COMMAND_CLASS_SPECIFIC_S2_BOOTSTRAPPING_CHALLENGE_RESPONSE  0x18
#define COMMAND_CLASS_SPECIFIC_REQUEST_NIF                          0x19
#define COMMAND_CLASS_SPECIFIC_SEND_MY_NIF                          0x20
#define COMMAND_CLASS_SPECIFIC_FIRMWARE_UPDATE_MD                   0x21
#define COMMAND_CLASS_SPECIFIC_GET_SECURE_KEY                       0x22
#define COMMAND_CLASS_SPECIFIC_UPDATE_FIRMWARE                      0x23
#define COMMAND_CLASS_SPECIFIC_GET_VERSION_FIRMWARE                 0x24
#define COMMAND_CLASS_SPECIFIC_CHANGE_CHANNEL_TV                    0x25
#define COMMAND_CLASS_SPECIFIC_PRIORITY_ROUTE                       0x26


#define COMMAND_CLASS_SPECIFIC_NODE_REQUEST_SPECIFICATION_ZPC       0xFF


#define COMMAND_IN_NODE                                             0x01
#define COMMAND_IN_GROUP                                            0x02

#define UPDATE_SUC_NOTIFY                                           0x01
#define CONTROLLER_CHANGE_ZPC_NOTIFY                                0x02
#define LEARN_MODE_NOTIFY                                           0x03
#define CONTROLLER_CAPABILITY_NOTIFY                                0x04
#define ADD_NODE_ZPC_NOTIFY                                         0x05
#define ADD_NODE_ZPC_NOTIFY_AUTO                                    0x06
#define REMOVE_NODE_ZPC_NOTIFY                                      0x07
#define REMOVE_NODE_ZPC_NOTIFY_AUTO                                 0x08
#define REPLACE_FAILED_NODE_ZPC_NOTIFY                              0x09
#define CMD_CLASS_ZPC_NOTIFY                                        0x0A
#define CMD_REQ_IN_QUEUE_ZPC_NOTIFY                                 0x0B

#define S2_KEY_GRANT_NOTIFY                                         0x0C
#define S2_PUBLIC_KEY_CHALLENGE_NOTIFY                              0x0D
#define S2_PUBLIC_KEY_SHOW_UP_NOTIFY                                0x0E
#define INCLUSION_CONTROLLER_NOTIFY                                 0x0F
#define UPDATE_NETWORK_NOTIFY                                       0x10
#define FIRMWARE_UPDATE_NOTIFY                                      0x11
#define NODE_LIST_NOTIFY                                            0x12
#define PING_FAILED_NODE_NOTIFY                                     0x13
#define RE_DISCOVERY_NOTIFY                                         0x14
#define NETWORK_HEALTH_NOTIFY                                       0x15

#define S2_EVT_MAX_BUFFER_SIZE                                      128 
/*< Max event buffer in s2 inclusion model. Buffer is uint32_t to ensure proper alignment therefor size is divided by 4.*/

#define APP_SET_LEARN_MODE_START                                    0x01
#define APP_SET_LEARN_MODE_REMOVED                                  0x02
#define APP_SET_LEARN_MODE_TIMEOUT                                  0x03
#define APP_SET_LEARN_MODE_FAILED                                   0x04
#define APP_SET_LEARN_MODE_INCLUDED_NON_SECURE                      0x05
#define APP_SET_LEARN_MODE_INCLUDED_SECURE_READY                    0x06
#define APP_SET_LEARN_MODE_INCLUDED_SECURE_DONE                     0x07
#define APP_SET_LEARN_MODE_INCLUDED_SECURE_FAILED                   0x08
#define ZWAVE_COMMAND_PROCESS_NOTIFY                                0xFF

#define MAX_BUFFER_LENGTH                                           230
#define MAX_MAGIC_CODE                                              10
#define MAX_NODE_NAME_CHAR                                          16
#define MAX_ENDPOINT_MEMBER                                         20
#define MAX_SUPPORTED_KEY_ATTRIBUTES_FOR_SCENE                      16
#define MAX_NUMBER_OF_BIT_MASK_BYTES_ATTRIBUTES                     3


#define LENGTH_DSK_SSA_IN_DECIMAL_DIGIT                             5
#define LENGTH_DSK_CSA_IN_DECIMAL_DIGIT                             10
#define CONVERT_CHANNEL_ID_TO_TV_CMD                                6
#define INDEX_BUTTON_COMMAND_ZW_TV                                  2
#define CONVERT_ASCII_TO_INTERGER                                   '0'

#define COMMAND_HEALTH_TEST                                         1
#define COMMAND_MAINTENANCE                                         2                                    
#define COMMAND_REDISCOVERY                                         3
#define COMMAND_DUMP_NEIGHBORS                                      4
#define COMMAND_PING_ALL_NODE                                       5
#define COMMAND_RSSI_MAP                                            6




typedef struct _ZWAVE_OWNER_ID_
{
    uint32_t HomeID;
    uint8_t  MyNodeID;
}ZW_OwnerID;

typedef struct _MANUFACTURE_INFO_
{
    uint8_t manufacturerID[2];
    uint8_t productTypeID[2];
    uint8_t productID[2];
}MANUFACTURE_INFO;

typedef struct _NODE_INFO_
{
    uint8_t   mode;  /* MODE_NONLISTENING,
                        MODE_ALWAYSLISTENING,
                        MODE_FREQUENTLYLISTENING, */

    NODE_TYPE nodeType;   /* Basic, Generic and Specific Device Types */
} NODE_INFO;

typedef struct _multi_channel_agg_capability_
{
    uint8_t             endpoint;
    uint8_t             generic_device_class;
    uint8_t             specific_device_class;
    uint8_t             no_memberof;
    uint8_t             ep_member[MAX_ENDPOINT_MEMBER];
    NODE_CAPABILITY     endpoint_capability;
}MULTI_CHANNEL_AGG_CAPABILITY;

typedef struct _multi_channel_capability_
{
    uint8_t             endpoint;
    uint8_t             generic_device_class;
    uint8_t             specific_device_class;
    uint8_t             no_memberof;
    uint8_t             ep_member[MAX_ENDPOINT_MEMBER];
    NODE_CAPABILITY     endpoint_capability;
}MULTI_CHANNEL_CAPABILITY;

typedef struct _alarm_report_v1
{
    uint8_t alarm_type;
    uint8_t alarm_level;
}alarm_report_v1_t;

typedef struct _alarm_report_v2
{
    uint8_t alarm_type;
    uint8_t alarm_level;
    uint8_t zensor_src_id;
    uint8_t zwave_alarm_status;
    uint8_t zwave_alarm_type;
    uint8_t zwave_alarm_event;
    uint8_t no_event_params:5;
    uint8_t event_param[32];
}alarm_report_v2_t;

typedef struct _notification_report
{
    uint8_t alarm_type;
    uint8_t alarm_level;
    uint8_t zwave_alarm_status;
    uint8_t zwave_alarm_type;
    uint8_t zwave_alarm_event;
    uint8_t sequence:1;
    uint8_t no_event_params:5;
    uint8_t event_param[32];
    uint8_t sequence_number;
}notification_report_t;

typedef struct _alarm_supported_report
{
    uint8_t v1_alarm:1;
    uint8_t no_bit_masks:5;
    uint8_t bit_mask[32];
}alarm_supported_report_t;

typedef struct _event_supported_report
{
    uint8_t notification_type;
    uint8_t no_bit_masks:5;
    uint8_t bit_mask[32];
}event_supported_report_t;


typedef struct _sensor_alarm_report
{
    uint8_t     src_id;
    uint8_t     sensor_type;
    uint8_t     sensor_state;
    uint16_t    seconds;
}sensor_alarm_report_t;

typedef struct _sensor_alarm_supported_report
{
    uint8_t no_bit_masks:5;
    uint8_t bit_mask[32];
}sensor_alarm_supported_report_t;

typedef struct _sensor_binary_report_v1
{
    uint8_t     sensor_value;
}sensor_binary_report_v1_t;

typedef struct _sensor_binary_report_v2
{
    uint8_t     sensor_value;
    uint8_t     sensor_type;
}sensor_binary_report_v2_t;

typedef struct _sensor_binary_supported_report
{
    uint8_t no_bit_masks:5;
    uint8_t bit_mask[32];
}sensor_binary_supported_report_t;


typedef struct _switch_all_report
{
    uint8_t mode;
}switch_all_report_t;

typedef struct _association_report
{
    uint8_t groud_id;
    uint8_t max_nodes_supported;
    uint8_t report_to_follow;
    uint8_t no_nodes;
    uint8_t node_list[MAX_BUFFER_LENGTH];
}association_report_t;

typedef struct _association_groupings_report
{
    uint8_t supported_groups;

}association_groupings_report_t;

typedef struct _association_specific_group_report
{
    uint8_t group;

}association_specific_group_report_t;

typedef struct _multi_channel_association_report
{
    uint8_t groud_id;
    uint8_t max_nodes_supported;
    uint8_t report_to_follow;
    uint8_t no_nodes;
    uint8_t node_list[30];
    uint8_t no_multi_channel_node;
    uint8_t multi_channel_node_list[30];
    uint8_t bit_address_list[30];
    uint8_t endpoint_list[30];
}multi_channel_association_report_t;

typedef struct _multi_channel_association_groupings_report
{
    uint8_t supported_groups;

}multi_channel_association_groupings_report_t;

typedef struct _association_group_name_report
{
    uint8_t group_id;
    uint8_t name_len;
    char    name[MAX_BUFFER_LENGTH];
    
}association_group_name_report_t;

typedef struct _group_info
{
    uint8_t group_id;
    uint8_t mode;
    uint16_t profile;
    uint16_t event_code;    
}group_info_t;

typedef struct _association_group_info_report
{
    uint8_t list_mode:1;
    uint8_t dynamic_info:1;
    uint8_t group_count:6;
    group_info_t group_info[32]; 
    
}association_group_info_report_t;

typedef struct _cmd_list
{
    uint8_t cmd_class;
    uint8_t cmd;    
}cmd_list_t;

typedef struct _association_group_cmd_list_report
{
    uint8_t     group_id;
    uint8_t     list_len:7;
    cmd_list_t  cmd_list[128];  
}association_group_cmd_list_report_t;

typedef struct _application_busy
{
    uint8_t status;
    uint8_t wait_time;
}application_busy_t;

typedef struct _application_reject_request
{
    uint8_t status;
}application_reject_request_t;

typedef struct _barrier_operator_report
{
    uint8_t state;
}barrier_operator_report_t;

typedef struct _barrier_operator_signal_supported_report
{
    uint8_t no_bit_masks;
    uint8_t bit_mask[MAX_BUFFER_LENGTH];
}barrier_operator_signal_supported_report_t;

typedef struct _barrier_operator_signal_report
{
    uint8_t subsystem_type;
    uint8_t subsystem_state;
}barrier_operator_signal_report_t;

typedef struct _basic_report_v1
{
    uint8_t value;
}basic_report_v1_t;

typedef struct _basic_set
{
    uint8_t value;
}basic_set_t;

typedef struct _basic_report_v2
{
    uint8_t value;
    uint8_t target_value;
    uint8_t duration;
}basic_report_v2_t;

typedef struct _battery_report
{
    uint8_t battery_level;
}battery_report_t;

typedef struct _switch_binary_report_v1
{
    uint8_t value;
}switch_binary_report_v1_t;

typedef struct _switch_binary_report_v2
{
    uint8_t value;
    uint8_t target_value;
    uint8_t duration;
}switch_binary_report_v2_t;

typedef struct _color_control_supported_report
{
    uint16_t color_component_mask;
}color_control_supported_report_t;

typedef struct _color_control_report_v1
{
    uint8_t color_component_id;
    uint8_t value;
}color_control_report_v1_t;

typedef struct _color_control_report_v3
{
    uint8_t color_component_id;
    uint8_t value;
    uint8_t target_value;
    uint8_t duration;
}color_control_report_v3_t;

typedef struct _configuration_report
{
    uint8_t parameter_number;
    uint8_t size:3;
    int configuration_value;
}configuration_report_t;

typedef struct _configuration_bulk_report
{
    uint16_t parameter_offset;
    uint8_t  no_params;
    uint8_t  report_to_follow;
    uint8_t  default_value:1;
    uint8_t  handshake:1;
    uint8_t  size:3;
    int      params_list[32];
}configuration_bulk_report_t;

typedef struct _configuration_name_report
{
    uint16_t parameter_number;
    uint8_t  report_to_follow;
    uint8_t  name_len;
    char  name[MAX_BUFFER_LENGTH];
}configuration_name_report_t;

typedef struct _configuration_info_report
{
    uint16_t parameter_number;
    uint8_t  report_to_follow;
    uint8_t  info_len;
    char     info[MAX_BUFFER_LENGTH];
}configuration_info_report_t;

typedef struct _configuration_properties_report
{
    uint16_t parameter_number;
    uint8_t  format:3;
    uint8_t  size:3;
    int      min_value;
    int      max_value;
    int      default_value;
    uint16_t next_parameter_number;
}configuration_properties_report_t;

typedef struct _door_lock_operation_report_v1
{
    uint8_t door_lock_mode;
    uint8_t outside_door_handles_mode:4;
    uint8_t inside_door_handles_mode:4;
    uint8_t door_condition;
    uint8_t lock_timeout_minutes;
    uint8_t lock_timeout_seconds;
}door_lock_operation_report_v1_t;

typedef struct _door_lock_operation_report_v3
{
    uint8_t door_lock_mode;
    uint8_t outside_door_handles_mode:4;
    uint8_t inside_door_handles_mode:4;
    uint8_t door_condition;
    uint8_t lock_timeout_minutes;
    uint8_t lock_timeout_seconds;
    uint8_t target_door_lock_mode;
    uint8_t duration;
}door_lock_operation_report_v3_t;

typedef struct _door_lock_configuration_report
{
    uint8_t operation_type;
    uint8_t outside_door_handles_mode:4;
    uint8_t inside_door_handles_mode:4;
    uint8_t lock_timeout_minutes;
    uint8_t lock_timeout_seconds;
}door_lock_configuration_report_t;

typedef struct _door_lock_logging_record_report
{
    uint8_t record_number;
    uint16_t timestamp_year;
    uint8_t timestamp_month;
    uint8_t timestamp_day;
    uint8_t record_status:3;
    uint8_t timestamp_hour_local_time;
    uint8_t timestamp_minute_local_time;
    uint8_t timestamp_second_local_time;
    uint8_t event_type;
    uint8_t user_identifier;
    uint8_t user_code_length;
    uint8_t user_code[20];//Minimum code length is 4 and maximum 10 ASCII digits
}door_lock_logging_record_report_t;

typedef struct _door_lock_logging_records_supported_report
{
    uint8_t max_records_stored;
}door_lock_logging_records_supported_report_t;

typedef struct _irrigation_system_info_report
{
    uint8_t master_valve:1;
    uint8_t total_no_valves;
    uint8_t total_no_valve_tables;
    uint8_t valve_table_max_size;
}irrigation_system_info_report_t;

typedef struct _irrigation_system_status_report
{
    uint8_t system_voltage;
    uint8_t sensor_status;
    uint8_t flow_precision:3;
    uint8_t flow_scale:2;
    uint8_t flow_size:3;
    int     flow_value;
    uint8_t pressure_precision:3;
    uint8_t pressure_scale:2;
    uint8_t pressure_size:3;
    int     pressure_value;
    uint8_t shutoff_duration;
    uint8_t system_error_status;
    uint8_t master_valve:1;
    uint8_t valve_id;
}irrigation_system_status_report_t;



typedef struct _irrigation_system_config_report
{
    uint8_t master_valve_delay;
    uint8_t HPT_precision:3; 
    uint8_t HPT_scale:2;
    uint8_t HPT_size:3;
    int     HPT_value;
    uint8_t LPT_precision:3;
    uint8_t LPT_scale:2;
    uint8_t LPT_size:3;
    int     LPT_value;
    int     sensor_polarity;

}irrigation_system_config_report_t;

typedef struct _irrigation_valve_info_report
{
    uint8_t connected:1;
    uint8_t master_valve:1;
    uint8_t valve_id;
    uint8_t nominal_current;
    uint8_t valve_error_status;
}irrigation_valve_info_report_t;


typedef struct _irrigation_valve_config_report
{
    uint8_t master_valve:1;
    uint8_t valve_id;
    uint8_t nominal_current_high_threshold;
    uint8_t nominal_current_low_threshold;
    uint8_t maximum_flow_precision:3;
    uint8_t maximum_flow_scale:2;
    uint8_t maximum_flow_size:3;
    int     maximum_flow_value;
    uint8_t FHT_precision:3;   //Flow High Threshold
    uint8_t FHT_scale:2;
    uint8_t FHT_size:3;
    int     FHT_value;
    uint8_t FLT_precision:3;   //Flow Low Threshold
    uint8_t FLT_scale:2;
    uint8_t FLT_size:3;
    int     FLT_value;
    uint8_t sensor_usage;

}irrigation_valve_config_report_t;

typedef struct _valve_list
{
    uint8_t     valve_id;
    uint16_t    duration;

}valve_list_t;

typedef struct _irrigation_valve_table_report
{
    uint8_t         valve_table_id;
    uint8_t         no_valves;
    valve_list_t    valve_list[32];

}irrigation_valve_table_report_t;

typedef struct _manufacturer_specific_report
{
    uint16_t        manufacturer_id;
    uint16_t        product_type_id;
    uint16_t        product_id;
}manufacturer_specific_report_t;

typedef struct _device_specific_report
{
    uint8_t         device_id_type:3;
    uint8_t         device_id_data_format:3;
    uint8_t         device_id_data_length:5;
    uint8_t         device_id_data[32];
}device_specific_report_t;

typedef struct _meter_report_v1
{
    uint8_t         meter_type;
    uint8_t         precision:3;
    uint8_t         scale:2;
    uint8_t         size:3;
    int             meter_value;
}meter_report_v1_t;

typedef struct _meter_supported_report_v2
{
    uint8_t         meter_reset:1;
    uint8_t         meter_type:5;
    uint8_t         scale_supported;
}meter_supported_report_v2_t;

typedef struct _meter_supported_report_v4
{
    uint8_t         meter_reset:1;
    uint8_t         rate_type:2;
    uint8_t         meter_type:5;
    uint8_t         more_scale_types:1;
    uint8_t         no_bit_masks;
    uint8_t         bit_mask[MAX_BUFFER_LENGTH];
}meter_supported_report_v4_t;

typedef struct _meter_report_v2
{
    uint8_t         rate_type:2;
    uint8_t         meter_type:5;
    uint8_t         precision:3;
    uint8_t         scale:3;
    uint8_t         size:3;
    int             meter_value;
    uint16_t        delta_time;
    int             previous_meter_value;
}meter_report_v2_t;

typedef struct _meter_report_v4
{
    uint8_t         rate_type:2;
    uint8_t         meter_type:5;
    uint8_t         precision:3;
    uint8_t         scale:3;
    uint8_t         size:3;
    int             meter_value;
    uint16_t        delta_time;
    int             previous_meter_value;
    uint8_t         scale2;
}meter_report_v4_t;

typedef struct _multi_channel_report
{
    uint8_t         dynamic:1;
    uint8_t         identical:1;
    uint8_t         no_endpoint:7;
}multi_channel_report_t;

typedef struct _multi_channel_capability_report
{
    uint8_t         dynamic:1;
    uint8_t         endpoint:7;
    uint8_t         generic_device_class;
    uint8_t         specific_device_class;
    uint8_t         no_cmd_class;
    uint8_t         cmd_class_list[128];
}multi_channel_capability_report_t;

typedef struct _multi_channel_endpoint_find_report
{
    uint8_t         report_to_follow;
    uint8_t         generic_device_class;
    uint8_t         specific_device_class;
    uint8_t         no_endpoint;
    uint8_t         endpoint_list[128];
}multi_channel_endpoint_find_report_t;

typedef struct _multi_channel_endpoint_report_v4
{
    uint8_t         dynamic:1;
    uint8_t         identical:1;
    uint8_t         individual_end_points:7;
    uint8_t         aggregated_end_points:7;
}multi_channel_endpoint_report_v4_t;

typedef struct _multi_channel_aggregated_members_report_v4
{
    uint8_t         aggregated_end_point:7;
    uint8_t         no_bit_masks;
    uint8_t         bit_mask[128];
}multi_channel_aggregated_members_report_v4_t;

typedef struct _switch_multilevel_report_v1
{
    uint8_t         value;
}switch_multilevel_report_v1_t;

typedef struct _switch_multilevel_report_v3
{
    uint8_t         value;
    uint8_t         target_value;
    uint8_t         duration;
}switch_multilevel_report_v3_t;

typedef struct _switch_multilevel_set_v1
{
    uint8_t         value;
}switch_multilevel_set_v1_t;

typedef struct _switch_multilevel_set_v2
{
    uint8_t         value;
    uint8_t         duration;
}switch_multilevel_set_v2_t;

typedef struct _switch_multilevel_set_v3
{
    uint8_t         value;
    uint8_t         target_value;
    uint8_t         duration;
}switch_multilevel_set_v3_t;

typedef struct _switch_multilevel_supported_report
{
    uint8_t         primary_switch_type:5;
    uint8_t         secondary_switch_type:5;
}switch_multilevel_supported_report_t;


typedef struct _powerlevel_report
{
    uint8_t         powerlevel;
    uint8_t         timeout;
}powerlevel_report_t;

typedef struct _powerlevel_test_node_report
{
    uint8_t         test_node_id;
    uint8_t         status_of_operation;
    uint16_t        test_frame_ack_count;
}powerlevel_test_node_report_t;

typedef struct _protection_report_v1
{
    uint8_t         protection_state;
}protection_report_v1_t;

typedef struct _protection_report_v2
{
    uint8_t         local_protection_state;
    uint8_t         rf_protection_state;
}protection_report_v2_t;

typedef struct _protection_supported_report
{
    uint8_t         exclusive_control:1;
    uint8_t         timeout:1;
    uint16_t        local_protection_state;
    uint16_t        rf_protection_state;
}protection_supported_report_t;

typedef struct _protection_ec_report
{
    uint8_t         node_id;
}protection_ec_report_t;

typedef struct _protection_timeout_report
{
    uint8_t         timeout;
}protection_timeout_report_t;

typedef struct _schedule_entry_type_supported_report_v1
{
    uint8_t         no_slots_week_day;
    uint8_t         no_slots_year_day;
}schedule_entry_type_supported_report_v1_t;

typedef struct _schedule_entry_type_supported_report_v3
{
    uint8_t         no_slots_week_day;
    uint8_t         no_slots_year_day;
    uint8_t         no_slots_daily_repeating;
}schedule_entry_type_supported_report_v3_t;

typedef struct _schedule_entry_lock_week_day_report
{
    uint8_t         user_id;
    uint8_t         schedule_slot_id;
    uint8_t         day_of_week;
    uint8_t         start_hour;
    uint8_t         start_minute;
    uint8_t         stop_hour;
    uint8_t         stop_minute;
}schedule_entry_lock_week_day_report_t;

typedef struct _schedule_entry_lock_year_day_report
{
    uint8_t         user_id;
    uint8_t         schedule_slot_id;
    uint8_t         start_year;
    uint8_t         start_month;
    uint8_t         start_day;
    uint8_t         start_hour;
    uint8_t         start_minute;
    uint8_t         stop_year;
    uint8_t         stop_month;
    uint8_t         stop_day;
    uint8_t         stop_hour;
    uint8_t         stop_minute;
}schedule_entry_lock_year_day_report_t;

typedef struct _schedule_entry_lock_time_offset_report
{
    uint8_t         sign_TZO:1;
    uint8_t         hour_TZO:7;
    uint8_t         minute_TZO;
    uint8_t         sign_offset_DST:1;
    uint8_t         minute_offset_DST:7;
}schedule_entry_lock_time_offset_report_t;

typedef struct _schedule_entry_lock_daily_repeating_report
{
    uint8_t         user_id;
    uint8_t         schedule_slot_id;
    uint8_t         week_day_bitmask;
    uint8_t         start_hour;
    uint8_t         start_minute;
    uint8_t         duration_hour;
    uint8_t         duration_minute;
}schedule_entry_lock_daily_repeating_report_t;

typedef struct _sensor_trigger_level_report
{
    uint8_t         sensor_type;
    uint8_t         precision:3;
    uint8_t         scale:2;
    uint8_t         size:3;
    int             trigger_value;      
}sensor_trigger_level_report_t;

typedef struct _sensor_multilevel_report
{
    uint8_t         sensor_type;
    uint8_t         precision:3;
    uint8_t         scale:2;
    uint8_t         size:3;
    int             sensor_value;       
}sensor_multilevel_report_t;

typedef struct _sensor_multilevel_supported_report
{
    uint8_t         no_bit_masks;
    uint8_t         bit_mask[MAX_BUFFER_LENGTH];
}sensor_multilevel_supported_report_t;

typedef struct _sensor_multilevel_supported_scale_report
{
    uint8_t         sensor_type;
    uint8_t         scale_bit_mask:4;
}sensor_multilevel_supported_scale_report_t;

typedef struct _simple_av_control_report
{
    uint8_t         no_reports;     
}simple_av_control_report_t;

typedef struct _simple_av_control_supported_report
{
    uint8_t         report_no;
    uint8_t         no_bit_masks;
    uint8_t         bit_mask[MAX_BUFFER_LENGTH];        
}simple_av_control_supported_report_t;

typedef struct _thermostat_fan_mode_report_v1
{
    uint8_t         fan_mode:4;
}thermostat_fan_mode_report_v1_t;

typedef struct _thermostat_fan_mode_report_v2
{
    uint8_t         off:1;
    uint8_t         fan_mode:4;
}thermostat_fan_mode_report_v2_t;

typedef struct _thermostat_fan_mode_supported_report
{
    uint8_t         no_bit_masks;
    uint8_t         bit_mask[MAX_BUFFER_LENGTH];
}thermostat_fan_mode_supported_report_t;

typedef struct _thermostat_mode_supported_report
{
    uint8_t         no_bit_masks;
    uint8_t         bit_mask[MAX_BUFFER_LENGTH];
}thermostat_mode_supported_report_t;

typedef struct _thermostat_fan_state_report
{
    uint8_t         fan_operating_state:4;
}thermostat_fan_state_report_t;

typedef struct _thermostat_mode_report_v1
{
    uint8_t         mode:5;
}thermostat_mode_report_v1_t;

typedef struct _thermostat_mode_report_v3
{
    uint8_t         mode:5;
    uint8_t         no_manufacturer_data:3;
    uint8_t         manufacturer_data[8];
}thermostat_mode_report_v3_t;

typedef struct _thermostat_operating_state_report
{
    uint8_t         operating_state;
}thermostat_operating_state_report_t;

typedef struct _thermostat_operating_logging_supported_report
{
    uint8_t         no_bit_masks;
    uint8_t         bit_mask[MAX_BUFFER_LENGTH];
}thermostat_operating_logging_supported_report_t;

typedef struct _usage_list
{
    uint8_t         operating_state_log_type:4;
    uint8_t         usage_today_hours;
    uint8_t         usage_today_minutes;
    uint8_t         usage_yesterday_hours;
    uint8_t         usage_yesterday_minutes;
}usage_list_t;

typedef struct _thermostat_operating_logging_report
{
    uint8_t         report_to_follow;
    uint8_t         no_usages;
    usage_list_t    usage_list[50];
}thermostat_operating_logging_report_t;

typedef struct _thermostat_setback_report
{
    uint8_t         setback_type:2;
    uint8_t         setback_state;
}thermostat_setback_report_t;

typedef struct _thermostat_setpoint_report
{
    uint8_t         setpoint_type:4;
    uint8_t         precision:3;
    uint8_t         scale:2;
    uint8_t         size:3;
    int             value;      
}thermostat_setpoint_report_t;

typedef struct _thermostat_setpoint_supported_report
{
    uint8_t         no_bit_masks;
    uint8_t         bit_mask[MAX_BUFFER_LENGTH];
}thermostat_setpoint_supported_report_t;

typedef struct _thermostat_setpoint_capabilities_report
{
    uint8_t         setpoint_type:4;
    uint8_t         min_precision:3;
    uint8_t         min_scale:2;
    uint8_t         min_size:3;
    int             min_value; 
    uint8_t         max_precision:3;
    uint8_t         max_scale:2;
    uint8_t         max_size:3;
    int             max_value; 
}thermostat_setpoint_capabilities_report_t;

typedef struct _time_report
{
    uint8_t         RTC_failure:1;
    uint8_t         hour_local_time:5;
    uint8_t         minute_local_time;
    uint8_t         second_local_time;
}time_report_t;

typedef struct _time_date_report
{
    uint16_t        year;
    uint8_t         month;
    uint8_t         day;
}time_date_report_t;

typedef struct _time_offset_report
{
    uint8_t         sign_TZO:1;
    uint8_t         hour_TZO:7;
    uint8_t         minute_TZO;
    uint8_t         sign_offset_DST:1;
    uint8_t         minute_offset_DST:7;
    uint8_t         month_start_DST;
    uint8_t         day_start_DST;
    uint8_t         hour_start_DST;
    uint8_t         month_end_DST;
    uint8_t         day_end_DST;
    uint8_t         hour_end_DST;
}time_offset_report_t;

typedef struct _time_parameters_report
{
    uint16_t        year;
    uint8_t         month;
    uint8_t         day;
    uint8_t         hour_UTC;
    uint8_t         minute_UTC;
    uint8_t         second_UTC;
}time_parameters_report_t;

typedef struct _user_code_report
{
    uint8_t         user_id;
    uint8_t         user_id_status;
    uint8_t         no_user_code;
    uint8_t         user_code[16];
}user_code_report_t;

typedef struct _users_number_report
{
    uint8_t         supported_users;
}users_number_report_t;

typedef struct _version_report_v1
{
    uint8_t         zw_lib_type;
    uint8_t         zw_protocol_version;
    uint8_t         zw_protocol_sub_version;
    uint8_t         app_version;
    uint8_t         app_sub_version;
}version_report_v1_t;

typedef struct _fw_target
{
    uint8_t         firmware_version;
    uint8_t         firmware_sub_version;
}fw_target_t;

typedef struct _version_report_v2
{
    uint8_t         zw_lib_type;
    uint8_t         zw_protocol_version;
    uint8_t         zw_protocol_sub_version;
    uint8_t         firmware0_version;
    uint8_t         firmware0_sub_version;
    uint8_t         hardware_version;
    uint8_t         no_firmware_targets;
    fw_target_t     fw_target_list[16];
}version_report_v2_t;

typedef struct _version_cmd_class_report
{
    uint8_t         request_cmd_class;
    uint8_t         cmd_class_version;
}version_cmd_class_report_t;

typedef struct _wake_up_interval_report
{
    int             seconds;
    uint8_t         node_id;
}wake_up_interval_report_t;

typedef struct _wake_up_interval_capabilities_report
{
    int             min_wk_interval_seconds;
    int             max_wk_interval_seconds;
    int             default_wk_interval_seconds;
    int             wk_interval_step_seconds;
}wake_up_interval_capabilities_report_t;

typedef struct _window_covering_supported_report
{
    uint8_t         no_param_masks;
    uint8_t         param_mask[MAX_BUFFER_LENGTH];
}window_covering_supported_report_t;

typedef struct _window_covering_report
{
    uint8_t         param_id;
    uint8_t         value;
    uint8_t         target_value;
    uint8_t         duration;
}window_covering_report_t;

typedef struct _zwaveplus_info_report
{
    uint8_t         zwaveplus_version;
    uint8_t         role_type;
    uint8_t         node_type;
    uint16_t        installer_icon_type;
    uint16_t        user_icon_type;
}zwaveplus_info_report_t;

typedef struct _cmd_req_in_queue_notify
{
    uint8_t source_endpoint;
    uint8_t dest_endpoint;
    uint8_t source_nodeId;
    uint8_t dest_nodeId;
    char    str_data[1024];
    uint8_t status;
}cmd_req_in_queue_notify_t;


typedef union _meta_data_t
{
    char alarm_pattern[29];
}meta_data_t;

typedef struct _language_report
{
    uint8_t language_1;
    uint8_t language_2;
    uint8_t language_3;
    uint16_t country;
}language_report_t;

typedef struct _clock_report
{
    uint8_t     weekday:3;
    uint8_t     hour:5;
    uint8_t     minute;
}clock_report_t;

typedef struct _antitheft_report
{
    uint8_t     protection_status;
    uint16_t    manufacturer_id;
    //Version 2 limits the Magic Code and Anti-theft Hint maximum bytes to 10
    uint8_t     no_anti_theft_hint;
    uint8_t     hint_list[MAX_MAGIC_CODE];

}antitheft_report_t;

typedef struct _scene_actuator_conf_report
{
    uint8_t     scene_id;
    uint8_t     level;
    uint8_t     dimming_duration;
}scene_actuator_conf_report_t;

typedef struct _scene_controller_conf_report
{
    uint8_t     group_id;
    uint8_t     scene_id;
    uint8_t     dimming_duration;
}scene_controller_conf_report_t;

typedef struct _scene_activation_set
{
    uint8_t     scene_id;
    uint8_t     dimming_duration;
}scene_activation_set_t;

typedef struct _central_scene_supported_report
{
    uint8_t     supported_scenes;//This field indicates the maximum number of scenes supported by the requested device
}central_scene_supported_report_t;

typedef struct _central_scene_supported_report_v2
{
    uint8_t     supported_scenes;//This field indicates the maximum number of scenes supported by the requested device
    uint8_t identical:1;
    uint8_t slow_refresh:1;
    uint8_t no_bit_masks;    
    //This field advertises the size of each “Supported Key Attributes” field measured in bytes.
    //The value MUST be in the range 1..3
    uint8_t supported_key_attributes[MAX_SUPPORTED_KEY_ATTRIBUTES_FOR_SCENE][MAX_NUMBER_OF_BIT_MASK_BYTES_ATTRIBUTES];//supported_key_attributes maximum 24bit

}central_scene_supported_report_v2_t;

typedef struct _central_scene_notification
{
    uint8_t     sequence_number;
    uint8_t     key_attributes;
    uint8_t slow_refresh:1;
    /*
Key Attribute Description Version
0x00 Key Pressed 1 time 1
0x01 Key Released 1
0x02 Key Held Down 1
0x03 Key Pressed 2 times 2
0x04 Key Pressed 3 times 2
0x05 Key Pressed 4 times 2
0x06 Key Pressed 5 times 2
    */
    uint8_t     scene_number;
}central_scene_notification_t;

typedef struct _central_scene_configuration_report 
{
    uint8_t slow_refresh:1;//This flag is used to advertise the use of the Slow Refresh capability.
    //the value 1 MUST indicate that the scene launching node MUST use Slow Refresh.
    /*
    If the Slow Refresh field is 1:
    A new Key Held Down notification MUST be sent every 55 seconds until the key is released.
    The Sequence Number field MUST be updated at each notification refresh.
    If not receiving a new Key Held Down notification within 60 seconds after the most recent Key
    Held Down notification, a receiving node MUST respond as if it received a Key Release
    notification
    */
}central_scene_configuration_report_t;

typedef struct _remote_association_configuration_report
{
    uint8_t     local_grouping_id;
    uint8_t     remote_node_id;
    uint8_t     remote_grouping_id;
}remote_association_configuration_report_t;

typedef struct _indicator_report
{
    uint8_t value;
}indicator_report_t;

typedef struct _indicator_support_report
{
    uint8_t indicator_id;
    uint8_t next_indicator_id;
    uint8_t property_supported_bit_mask_length:5;
    uint8_t property_supported_bit_mask_list[32];
}indicator_support_report_t;
typedef struct _indicator_object
{
    uint8_t indicator_id;
    uint8_t property_id;
    uint8_t value;
}indicator_object_t;

typedef struct _indicator_report_v2
{
    uint8_t indicator_0_value;
    uint8_t indicator_object_count:5;
    indicator_object_t indicator_object[32];//5 bit
}indicator_report_v2_t;

typedef struct _node_name_report
{
    uint8_t char_presentation:3;
    uint8_t no_node_name;
    uint8_t node_name_list[MAX_NODE_NAME_CHAR];
}node_name_report_t;

typedef struct _node_location_report
{
    uint8_t char_presentation:3;
    uint8_t no_node_location;
    uint8_t node_location_list[MAX_NODE_NAME_CHAR];
}node_location_report_t;

typedef struct _firmware_md_report
{
    uint16_t    manufacturer_id;
    uint16_t    firmware_id;
    uint16_t    checksum;
}firmware_md_report_t;

typedef struct _firmware_md_report_v3
{
    uint16_t    manufacturer_id;
    uint16_t    firmware0_id;
    uint16_t    firmware0_checksum;
    uint8_t     firmware_upgradable;
    uint8_t     no_firmware_targets;
    uint16_t    max_fragment_size;
    uint16_t    firmware_id_list[MAX_BUFFER_LENGTH];
}firmware_md_report_v3_t;

typedef struct _firmware_update_md_request_report
{
    uint8_t status;
}firmware_update_md_request_report_t;

typedef struct _firmware_update_md_report
{
    uint8_t     last:1;
    uint16_t    report_number;
    uint8_t     no_data;
    uint8_t     data_list[MAX_BUFFER_LENGTH];
}firmware_update_md_report_t;
typedef struct _firmware_update_md_get
{
    uint8_t     no_reports;
    uint16_t    report_number;
}firmware_update_md_get_t;

typedef struct _firmware_update_md_report_v2
{
    uint8_t     last:1;
    uint16_t    report_number;
    uint8_t     no_data;
    uint8_t     data_list[MAX_BUFFER_LENGTH];
    uint16_t    checksum;
}firmware_update_md_report_v2_t;

typedef struct _firmware_update_md_status_report
{
    uint8_t status;
}firmware_update_md_status_report_t;

typedef struct _firmware_update_md_status_report_v3
{
    uint8_t     status;
    uint16_t    wait_time;
}firmware_update_md_status_report_v3_t;
//handle notify Supervision
typedef struct _supervision_report//only version 1
{
    uint8_t     more_status_updates:1;
    uint8_t     class;
    uint8_t     cmd;
    uint8_t     session_id;//for app send supervision get
    uint8_t     status;
    uint8_t     duration;
    bool        manual_supervision;
}supervision_report_t;

typedef struct cmd_class_notify
{
    uint8_t     source_endpoint;
    uint8_t     dest_endpoint;
    uint8_t     source_nodeId;
    uint8_t     dest_nodeId;
    uint8_t     node_type;
    uint8_t     node_is_listening;
    uint8_t     cmd_class;
    uint8_t     cmd;
    uint8_t     version;
    uint8_t     pollingReport;
    meta_data_t meta_data;  
    union
    {
        alarm_report_v1_t                                   alarm_report_v1;
        alarm_report_v2_t                                   alarm_report_v2;
        alarm_supported_report_t                            alarm_supported_report;
        notification_report_t                               notification_report;
        event_supported_report_t                            event_supported_report;
        sensor_alarm_report_t                               sensor_alarm_report;
        sensor_alarm_supported_report_t                     sensor_alarm_supported_report;
        sensor_binary_report_v1_t                           sensor_binary_report_v1;
        sensor_binary_report_v2_t                           sensor_binary_report_v2;
        sensor_binary_supported_report_t                    sensor_binary_supported_report;
        switch_all_report_t                                 switch_all_report;
        association_report_t                                association_report;
        association_groupings_report_t                      association_groupings_report;
        association_specific_group_report_t                 association_specific_group_report;
        multi_channel_association_report_t                  multi_channel_association_report;
        multi_channel_association_groupings_report_t        multi_channel_association_groupings_report;
        association_group_info_report_t                     association_group_info_report;
        association_group_name_report_t                     association_group_name_report;
        association_group_cmd_list_report_t                 association_group_cmd_list_report;
        application_busy_t                                  application_busy;
        application_reject_request_t                        application_reject_request;
        barrier_operator_report_t                           barrier_operator_report;
        barrier_operator_signal_supported_report_t          barrier_operator_signal_supported_report;
        barrier_operator_signal_report_t                    barrier_operator_signal_report;
        basic_report_v1_t                                   basic_report_v1;
        basic_report_v2_t                                   basic_report_v2;
        battery_report_t                                    battery_report;
        switch_binary_report_v1_t                           switch_binary_report_v1;
        switch_binary_report_v2_t                           switch_binary_report_v2;
        color_control_supported_report_t                    color_control_supported_report;
        color_control_report_v1_t                           color_control_report_v1;
        color_control_report_v3_t                           color_control_report_v3;
        configuration_report_t                              configuration_report;
        configuration_bulk_report_t                         configuration_bulk_report;
        configuration_name_report_t                         configuration_name_report;
        configuration_info_report_t                         configuration_info_report;
        configuration_properties_report_t                   configuration_properties_report;
        door_lock_operation_report_v1_t                     door_lock_operation_report_v1;
        door_lock_operation_report_v3_t                     door_lock_operation_report_v3;
        door_lock_configuration_report_t                    door_lock_configuration_report;
        door_lock_logging_record_report_t                   door_lock_logging_record_report;
        door_lock_logging_records_supported_report_t        door_lock_logging_records_supported_report;
        irrigation_system_info_report_t                     irrigation_system_info_report;
        irrigation_system_status_report_t                   irrigation_system_status_report;
        irrigation_system_config_report_t                   irrigation_system_config_report;
        irrigation_valve_info_report_t                      irrigation_valve_info_report;
        irrigation_valve_config_report_t                    irrigation_valve_config_report;
        irrigation_valve_table_report_t                     irrigation_valve_table_report;
        manufacturer_specific_report_t                      manufacturer_specific_report;
        device_specific_report_t                            device_specific_report;
        meter_report_v1_t                                   meter_report_v1;
        meter_supported_report_v2_t                         meter_supported_report_v2;
        meter_supported_report_v4_t                         meter_supported_report_v4;
        meter_report_v2_t                                   meter_report_v2;
        meter_report_v4_t                                   meter_report_v4;
        sensor_multilevel_report_t                          sensor_multilevel_report;
        sensor_multilevel_supported_report_t                sensor_multilevel_supported_report;
        sensor_multilevel_supported_scale_report_t          sensor_multilevel_supported_scale_report;
        switch_multilevel_report_v1_t                       switch_multilevel_report_v1;
        switch_multilevel_report_v3_t                       switch_multilevel_report_v3;
        switch_multilevel_set_v1_t                          switch_multilevel_set_v1;
        switch_multilevel_set_v2_t                          switch_multilevel_set_v2;
        switch_multilevel_set_v3_t                          switch_multilevel_set_v3;
        switch_multilevel_supported_report_t                switch_multilevel_supported_report;
        multi_channel_report_t                              multi_channel_report;
        multi_channel_capability_report_t                   multi_channel_capability_report;
        multi_channel_endpoint_find_report_t                multi_channel_endpoint_find_report;
        multi_channel_endpoint_report_v4_t                  multi_channel_endpoint_report_v4;
        multi_channel_aggregated_members_report_v4_t        multi_channel_aggregated_members_report_v4;
        powerlevel_report_t                                 powerlevel_report;
        powerlevel_test_node_report_t                       powerlevel_test_node_report;
        protection_report_v1_t                              protection_report_v1;
        protection_report_v2_t                              protection_report_v2;
        protection_supported_report_t                       protection_supported_report;
        protection_ec_report_t                              protection_ec_report;
        protection_timeout_report_t                         protection_timeout_report;
        schedule_entry_type_supported_report_v1_t           schedule_entry_type_supported_report_v1;
        schedule_entry_type_supported_report_v3_t           schedule_entry_type_supported_report_v3;
        schedule_entry_lock_week_day_report_t               schedule_entry_lock_week_day_report;
        schedule_entry_lock_year_day_report_t               schedule_entry_lock_year_day_report;
        schedule_entry_lock_time_offset_report_t            schedule_entry_lock_time_offset_report;
        schedule_entry_lock_daily_repeating_report_t        schedule_entry_lock_daily_repeating_report;
        sensor_trigger_level_report_t                       sensor_trigger_level_report;
        simple_av_control_report_t                          simple_av_control_report;
        simple_av_control_supported_report_t                simple_av_control_supported_report;
        thermostat_fan_mode_report_v1_t                     thermostat_fan_mode_report_v1;
        thermostat_fan_mode_report_v2_t                     thermostat_fan_mode_report_v2;
        thermostat_fan_mode_supported_report_t              thermostat_fan_mode_supported_report;
        thermostat_fan_state_report_t                       thermostat_fan_state_report;
        thermostat_mode_report_v1_t                         thermostat_mode_report_v1;
        thermostat_mode_report_v3_t                         thermostat_mode_report_v3;
        thermostat_mode_supported_report_t                  thermostat_mode_supported_report;
        thermostat_operating_state_report_t                 thermostat_operating_state_report;
        thermostat_operating_logging_supported_report_t     thermostat_operating_logging_supported_report;
        thermostat_operating_logging_report_t               thermostat_operating_logging_report;
        thermostat_setback_report_t                         thermostat_setback_report;
        thermostat_setpoint_report_t                        thermostat_setpoint_report;
        thermostat_setpoint_supported_report_t              thermostat_setpoint_supported_report;
        thermostat_setpoint_capabilities_report_t           thermostat_setpoint_capabilities_report;
        time_report_t                                       time_report;
        time_date_report_t                                  time_date_report;
        time_offset_report_t                                time_offset_report;
        time_parameters_report_t                            time_parameters_report;
        user_code_report_t                                  user_code_report;
        users_number_report_t                               users_number_report;
        version_report_v1_t                                 version_report_v1;
        version_report_v2_t                                 version_report_v2;
        version_cmd_class_report_t                          version_cmd_class_report;
        wake_up_interval_report_t                           wake_up_interval_report;
        wake_up_interval_capabilities_report_t              wake_up_interval_capabilities_report;
        window_covering_supported_report_t                  window_covering_supported_report;
        window_covering_report_t                            window_covering_report;
        zwaveplus_info_report_t                             zwaveplus_info_report;
        language_report_t                                   language_report;
        clock_report_t                                      clock_report;
        antitheft_report_t                                  antitheft_report;
        scene_actuator_conf_report_t                        scene_actuator_conf_report;
        basic_set_t                                         basic_set;
        indicator_report_t                                  indicator_report;
        indicator_report_v2_t                               indicator_report_v2;
        indicator_support_report_t                          indicator_support_report;
        node_name_report_t                                  node_name_report;
        node_location_report_t                              node_location_report;
        firmware_md_report_t                                firmware_md_report;
        firmware_md_report_v3_t                             firmware_md_report_v3;
        firmware_update_md_request_report_t                 firmware_update_md_request_report;
        firmware_update_md_report_t                         firmware_update_md_report;
        firmware_update_md_status_report_t                  firmware_update_md_status_report;
        firmware_update_md_report_v2_t                      firmware_update_md_report_v2;
        firmware_update_md_get_t                            firmware_update_md_get;
        firmware_update_md_status_report_v3_t               firmware_update_md_status_report_v3;                                    
        supervision_report_t                                supervision_report;
        scene_controller_conf_report_t                      scene_controller_conf_report;
        scene_activation_set_t                              scene_activation_set;
        remote_association_configuration_report_t           remote_association_configuration_report;
        central_scene_notification_t                        central_scene_notification;
        central_scene_configuration_report_t                central_scene_configuration_report;
        central_scene_supported_report_t                    central_scene_supported_report;
        central_scene_supported_report_v2_t                 central_scene_supported_report_v2;
    };

}cmd_class_notify_t;



typedef struct update_suc_notify
{
    uint8_t suc_id;
    uint8_t my_capability;
}UPDATE_SUC_NOTIFY_T;


typedef struct zwave_plus_info_notify
{
    uint8_t zwave_plus_version;
    uint8_t role_type;
    uint8_t node_type;
    uint16_t installer_icon_type;
    uint16_t user_icon_type;
}ZWAVE_PLUS_INFO_NOTIFY_T;


typedef struct learn_mode_notify
{
    uint8_t     my_node_id;
    int         my_node_flags;
    uint8_t     status;
    uint8_t     including_secure_scheme;
    uint8_t     including_node_id;
    uint8_t     including_node_type;
    uint32_t    my_home_id;
    uint8_t     my_capability;
    NODE_INFO   including_node_info;
}LEARN_MODE_NOTIFY_T;


typedef struct controller_capability_notify
{
    uint8_t     capability;
    uint8_t     SUC_node_id;
}CONTROLLER_CAPABILITY_NOTIFY_T;


typedef struct Node_s
{
    uint8_t                         node_id;
    uint8_t                         node_type;
    NODE_INFO                       node_info;
    NODE_CAPABILITY                 node_capability;
    MANUFACTURE_INFO                node_manufacture;
    uint8_t                         node_secure_scheme;
    int                             node_flags;
    NODE_CAPABILITY                 node_capability_secureV0;
    NODE_CAPABILITY                 node_capability_secureV2;
    uint8_t                         no_endpoint;
    MULTI_CHANNEL_CAPABILITY        multi_channel_capability[20];
    uint8_t                         no_agg_endpoint;
    MULTI_CHANNEL_CAPABILITY        multi_channel_agg_capability[10];
    uint8_t                         is_zp_node;
    ZWAVE_PLUS_INFO_NOTIFY_T        zp_info;
}ZW_Node_t;


typedef struct add_node_zpc_notify
{
    uint8_t         bStatus;
    uint8_t         my_capability;
    ZW_Node_t       zpc_node;
}ADD_NODE_ZPC_NOTIFY_T;

typedef struct tranfer_network_notify
{
    uint8_t         node_id;
    uint8_t         basic_type;
    uint8_t         generic_type;
    uint8_t         specific_type;
    uint8_t         node_mode;
}TRANFER_NETWORK_NOTIFY_T;

typedef struct ping_failed_node_notify
{
    uint8_t    no_node;
    uint8_t    nodes[ZW_MAX_NODES];
}PING_FAILED_NODE_NOTIFY_T;

typedef struct network_health_notify
{
    uint8_t    node_id;
    uint8_t    neigbors_count;
    uint8_t    total_repeaters;
    uint8_t    neighbors[ZW_MAX_NODES];
    uint8_t    test_count;
    uint8_t    PER;
    uint8_t    RC;
    uint8_t    route_count;
    uint8_t    NH;
    char       NHS[10];            
}NETWORK_HEALTH_NOTIFY_T;

typedef struct re_discovery_notify
{
    uint8_t    status;
}RE_DISCOVERY_NOTIFY_T;

typedef struct update_network_notify
{
    uint8_t                     no_node;
    TRANFER_NETWORK_NOTIFY_T    TranferNetworkNotify[ZW_MAX_NODES];
}UPDATE_NETWORK_NOTIFY_T;

typedef struct inclusion_controller_notify
{
    uint8_t                     status;
}INCLUSION_CONTROLLER_T;

typedef struct firmware_update_notify
{
    uint8_t                     status;
    uint8_t                     version;
    uint8_t                     deviceId;    
    uint8_t                     wait_time;
    uint8_t                     percent;    
}FIRMWARE_UPDATE_NOTIFY_T;

typedef struct
{
    union 
    {
        s2_node_inclusion_request_t             kex_report;
        s2_node_inclusion_challenge_t           challenge_req;
        s2_node_inclusion_public_key_show_up_t  key_show_up;
    };
} S2_EVENT_T;

// typedef struct smart_start_list_notify
// {
//     uint8_t                     status;
//     pvs_iter_entry_t            pvs_list;
// }SMART_START_T;

typedef union _notify_tx_buffer_
{
    UPDATE_SUC_NOTIFY_T                 UpdateSUCNotify;
    LEARN_MODE_NOTIFY_T                 LearnModeNotify;
    CONTROLLER_CAPABILITY_NOTIFY_T      ControllerCapabilityNotify;
    ADD_NODE_ZPC_NOTIFY_T               AddNodeZPCNotify;
    ADD_NODE_ZPC_NOTIFY_T               ControlerChangeZPCNotify;   
    ADD_NODE_ZPC_NOTIFY_T               RemoveNodeZPCNotify;
    ADD_NODE_ZPC_NOTIFY_T               ReplaceFailedNodeNotify;                
    cmd_class_notify_t                  CmdClassNotify;
    cmd_req_in_queue_notify_t           CmdReqInQueueNotify;
    S2_EVENT_T                          S2EventNotify;
    UPDATE_NETWORK_NOTIFY_T             UpdateNetworkNotify;
    INCLUSION_CONTROLLER_T              InclusionControllerNotify;
    FIRMWARE_UPDATE_NOTIFY_T            FirmwareUpdateNotify;
    PING_FAILED_NODE_NOTIFY_T           PingFailedNodeNotify;
    RE_DISCOVERY_NOTIFY_T               ReDiscoveryNotify;
    NETWORK_HEALTH_NOTIFY_T             NetworkHealthNotify;

} NOTIFY_TX_BUFFER_T;


typedef struct notify
{
    uint8_t notify_status;
    uint8_t notify_message[1024+16];
}notify_t;

typedef struct notify_queue
{
    notify_t notify[100];
    uint8_t  notify_index;
}notify_queue_t;    


typedef struct cmd_set_s
{
    uint8_t cmd_length;
    uint8_t cmd[256];
}cmd_set_t;

typedef struct tagZWParam_s {
    uint8_t command;
    int     ret;
    uint8_t param1;
    uint8_t param2;
    uint8_t param3;
    uint32_t param4;
    uint8_t src_endpoint;
    uint8_t dest_endpoint;
    uint8_t scheme;
    void    *misc_data;
    union
    {
        cmd_set_t cmd_set;
        rd_node_list_t multi_node[ZW_MAX_NODES]; 
    };
    union
    {
        ZW_Node_t node;
        uint8_t   group_list[ZW_MAX_NODES];
        cmd_set_t data_out;
        uint8_t   raw_data[1024]; 
    };
} TZWParam;

typedef enum
{
    KEY_DOWN_ATTRIBUTE = 0,
    KEY_UP_ATTRIBUTE,
    KEEP_ALIVE_ATTRIBUTE,
}keyAttribute;



int zwaveInitialize(char *portName,notify_queue_t* notify);
int zwaveControllerInfo(ZW_OwnerID* zw_ownerID);

int zwaveDestruct(void);
int zwaveSendCommand(TZWParam *pzwParam);
int zwaveSendDataMeta(uint8_t nodeID, uint8_t *pData, uint8_t dataLength);
void PushNotificationToHandler(uint8_t bStatus, uint8_t* pData, uint32_t len);

//for polling device, endpoint of device
int addDeviceToPollingList(uint8_t nodeId, uint8_t endpoint, uint16_t pollingInterval, uint16_t commandClass);
/**
removeDeviceFromPollingList - remove polling requests that belong to the specified node
@param[in]  nodeId     node id
@return     ZW_ERR_NONE if success; else ZW_ERR_XXX on error
*/
int removeDeviceFromPollingList(uint8_t nodeId);

/*========================================   zwaveSendCommand   ======================================
* ======================================   ADD NODE TO NETWORK   =====================================              
**
**  Input
**      pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_ADD
**
**
**  Return
**      pzwParam->ret
**          -1 : Add node command is timeout (try again)
**           0 : Success
**  Output
**      pzwParam->node
**          pzwParam->node.node_id      : Node ID of new added node.
**          pzwParam->node.node_type    : Node type of new added node (Switch or MultiLevel,..) 
**--------------------------------------------------------------------------------------------------*/


/*========================================   zwaveSendCommand   ======================================
**===================================   REMOVE NODE FROM NETWORK   ===================================              
**
**  Input
**      pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_REMOVE
**
**
**  Return
**      pzwParam->ret
**          -2 : Node is not included in network
**          -1 : Add node command is timeout (try again)
**           0 : Success
**  Output
**      pzwParam->node
**          pzwParam->node.node_id      : Node ID of new added node.
**          pzwParam->node.node_type    : Node type of new added node (Switch or MultiLevel,..) 
**--------------------------------------------------------------------------------------------------*/

/*========================================   zwaveSendCommand   ======================================
**===================================   GET NODE LIST IN NETWORK   ===================================              
**
**  Input
**      pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_GET_LIST
**
**
**  Return
**      pzwParam->ret
**          -1 : Error to get node list 
**           0 : Success
**  Output
        pzwParam->param1
            Number of nodes in network (include static controller)
**      pzwParam->node_list
**          pzwParam->node_list[i].node_id      : Node ID of node[i]
**          pzwParam->node_list[i].node_type    : Node type of node[i] 
**--------------------------------------------------------------------------------------------------*/

/*========================================   zwaveSendCommand   ======================================
**==================================    ADD NEW GROUP TO NETWORK   ===================================              
**
**  Input
**      pzwParam->command=COMMAND_CLASS_SPECIFIC_GROUP_ADD_GROUP
        pzwParam->param1
            GroupID of new group
**
**
**  Return
**      pzwParam->ret
**          -1 : Error to add new group
**           0 : Success
             1 : GroupID has already existed
**  Output
        NOTHING
**--------------------------------------------------------------------------------------------------*/

/*========================================   zwaveSendCommand   ======================================
**==================================    REMOVE GROUP FROM NETWORK   ==================================              
**
**  Input
**      pzwParam->command=COMMAND_CLASS_SPECIFIC_GROUP_REMOVE_GROUP
        pzwParam->param1
            GroupID of group that you want to remove
**
**
**  Return
**      pzwParam->ret
**          -1 : Error to remove group
**           0 : Success
**  Output
        NOTHING
**--------------------------------------------------------------------------------------------------*/

/*========================================   zwaveSendCommand   ======================================
**=====================================     GET NODES IN GROUP   =====================================              
**
**  Input
**      pzwParam->command=COMMAND_CLASS_SPECIFIC_GROUP_GET_NODES
        pzwParam->param1
            GroupID of group in which you want to get all of nodes.
**
**
**  Return
**      pzwParam->ret
**          -1 : GroupID does not exist.
**         >=0 : number of nodes in this group 
**  Output
        pzwParam->node_list
            pzwParam->node_list[i].node_id      : Node ID of node[i]
**          pzwParam->node_list[i].node_type    : Node type of node[i] 
**--------------------------------------------------------------------------------------------------*/

/*========================================   zwaveSendCommand   ======================================
**===================================++==     GET GROUP LIST   =====================++================              
**
**  Input
**      pzwParam->command=COMMAND_CLASS_SPECIFIC_GROUP_GET_LIST
**
**
**  Return
**      pzwParam->ret
            0: success
**  Output
        pzwParam->param1
            Number of groups in network
        pzwParam->group_list
            pzwParam->group_list[i]             :groupID[i]
**--------------------------------------------------------------------------------------------------*/

/*========================================   zwaveSendCommand   ======================================
**===================================++==     GET GROUP LIST   =====================++================              
**
**  Input
**      pzwParam->command=COMMAND_CLASS_SPECIFIC_GROUP_GET_LIST
**
**
**  Return
**      pzwParam->ret
            0: success
**  Output
        pzwParam->param1
            Number of groups in network
        pzwParam->group_list
            pzwParam->group_list[i]             :groupID[i]
**--------------------------------------------------------------------------------------------------*/

/*========================================   zwaveSendCommand   ======================================
**====================================     TURN ON/OFF ONE NODE   ====================================              
**
**  Input
**      pzwParam->command=COMMAND_CLASS_SWITCH_ALL
        pzwParam->param1=COMMAND_IN_NODE
        pzwParam->param2=NodeID
            NodeID of this node that you want to turn on
        pzwParam->param3=SWITCH_ALL_ON (or SWITCH_ALL_OFF)
            SWITCH_ALL_ON                   : if you want to turn on
            SWITCH_ALL_OFF                  : if you want to turn off
**
**
**  Return
**      pzwParam->ret
           -2: NodeID is not added to network
           -1: send command is fail
            0: success
**  Output
        NOTHING
**--------------------------------------------------------------------------------------------------*/

/*========================================   zwaveSendCommand   ======================================
**====================================     TURN ON/OFF ONE GROUP   ====================================             
**
**  Input
**      pzwParam->command=COMMAND_CLASS_SWITCH_ALL
        pzwParam->param1=COMMAND_IN_GROUP
        pzwParam->param2=GroupID
            GroupID of this griup that you want to turn on
        pzwParam->param3=SWITCH_ALL_ON (or SWITCH_ALL_OFF)
            SWITCH_ALL_ON                   : if you want to turn on
            SWITCH_ALL_OFF                  : if you want to turn off
**
**
**  Return
**      pzwParam->ret
           -2: GroupID is not added to network
           -1: send command is fail
            0: success
**  Output
        NOTHING
**--------------------------------------------------------------------------------------------------*/

/*========================================   zwaveSendCommand   ======================================
**=================     RESET ZW CONTROLLER (CLEAR ALL OF NODEIDs) TO  DEFAULT  ======================
**
**  Input
**      pzwParam->command=COMMAND_CLASS_SPECIFIC_NODE_SET_DEFAULT
**
**  Return
**      pzwParam->ret
           -1: Set default command is timeout
            0: success
**  Output
        NOTHING
**--------------------------------------------------------------------------------------------------*/

#endif
#ifdef __cplusplus
}
#endif