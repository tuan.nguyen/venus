/****************************************************************************
 *
 *
 * Copyright (c) 2014
 * Veriksytems, Inc.
 * All Rights Reserved
****************************************************************************/
#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdint.h>

#ifdef NDEBUG
	#include <assert.h>
	#define ASSERT assert
#else
	#define ASSERT(a) {if(!(a)) {printf("Assertion at %s:%i\n", __FILE__,__LINE__ );return;}}
#endif

#define OFFSET_CLASSCMD                       0x00
#define OFFSET_CMD                            0x01
#define OFFSET_PARAM_1                        0x02
#define OFFSET_PARAM_2                        0x03
#define OFFSET_PARAM_3                        0x04
#define OFFSET_PARAM_4                        0x05

#define BIT0		0x01
#define BIT1		0x02
#define BIT2		0x04
#define BIT3		0x08
#define BIT4		0x10
#define BIT5		0x20
#define BIT6		0x40
#define BIT7		0x80

extern uint8_t gLogLevel;
extern int     fd;

enum eLogType{
    logQuerry=0,
    logWarning,
    logUI,
    logInfo,
    logDebug,
    logPolling
};

void 	hexdump(void *mem, unsigned int len);

void 	mainlog(uint8_t logLevel,const char *format,...);
int 	init_file_data(void);
int 	reset_file_data(void);
int 	write_data(uint16_t position, uint8_t* data, uint8_t data_len);
int 	read_data(uint16_t position, uint8_t* data, uint8_t data_len);
uint16_t ZW_CheckCrc16(uint16_t crc,uint8_t *pDataAddr,uint16_t bDataLen);
uint16_t ZW_CreateCrc16(uint8_t *pHeaderAddr,uint8_t bHeaderLen,uint8_t *pPayloadAddr,uint8_t bPayloadLen);
void     mainlog_printArray(char* str, void* mem,unsigned int len);
int 	ConvertValuetoInt32(uint8_t size, int value);








#endif
