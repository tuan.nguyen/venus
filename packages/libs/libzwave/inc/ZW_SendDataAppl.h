/* © 2014 Sigma Designs, Inc. This is an unpublished work protected by Sigma
 * Designs, Inc. as a trade secret, and is not to be used or disclosed except as
 * provided Z-Wave Controller Development Kit Limited License Agreement. All
 * rights reserved.
 *
 * Notice: All information contained herein is confidential and/or proprietary to
 * Sigma Designs and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination or
 * reproduction of the source code contained herein is expressly forbidden to
 * anyone except Licensees of Sigma Designs  who have executed a Sigma Designs’
 * Z-WAVE CONTROLLER DEVELOPMENT KIT LIMITED LICENSE AGREEMENT. The copyright
 * notice above is not evidence of any actual or intended publication of the
 * source code. THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED
 * INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS  TO REPRODUCE, DISCLOSE OR
 * DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL A PRODUCT THAT IT  MAY
 * DESCRIBE.
 *
 * THE SIGMA PROGRAM AND ANY RELATED DOCUMENTATION OR TOOLS IS PROVIDED TO COMPANY
 * "AS IS" AND "WITH ALL FAULTS", WITHOUT WARRANTY OF ANY KIND FROM SIGMA. COMPANY
 * ASSUMES ALL RISKS THAT LICENSED MATERIALS ARE SUITABLE OR ACCURATE FOR
 * COMPANY’S NEEDS AND COMPANY’S USE OF THE SIGMA PROGRAM IS AT COMPANY’S
 * OWN DISCRETION AND RISK. SIGMA DOES NOT GUARANTEE THAT THE USE OF THE SIGMA
 * PROGRAM IN A THIRD PARTY SERVICE ENVIRONMENT OR CLOUD SERVICES ENVIRONMENT WILL
 * BE: (A) PERFORMED ERROR-FREE OR UNINTERRUPTED; (B) THAT SIGMA WILL CORRECT ANY
 * THIRD PARTY SERVICE ENVIRONMENT OR CLOUD SERVICE ENVIRONMENT ERRORS; (C) THE
 * THIRD PARTY SERVICE ENVIRONMENT OR CLOUD SERVICE ENVIRONMENT WILL OPERATE IN
 * COMBINATION WITH COMPANY’S CONTENT OR COMPANY APPLICATIONS THAT UTILIZE THE
 * SIGMA PROGRAM; (D) OR WITH ANY OTHER HARDWARE, SOFTWARE, SYSTEMS, SERVICES OR
 * DATA NOT PROVIDED BY SIGMA. COMPANY ACKNOWLEDGES THAT SIGMA DOES NOT CONTROL
 * THE TRANSFER OF DATA OVER COMMUNICATIONS FACILITIES, INCLUDING THE INTERNET,
 * AND THAT THE SERVICES MAY BE SUBJECT TO LIMITATIONS, DELAYS, AND OTHER PROBLEMS
 * INHERENT IN THE USE OF SUCH COMMUNICATIONS FACILITIES. SIGMA IS NOT RESPONSIBLE
 * FOR ANY DELAYS, DELIVERY FAILURES, OR OTHER DAMAGE RESULTING FROM SUCH ISSUES.
 * SIGMA IS NOT RESPONSIBLE FOR ANY ISSUES RELATED TO THE PERFORMANCE, OPERATION
 * OR SECURITY OF THE THIRD PARTY SERVICE ENVIRONMENT OR CLOUD SERVICES
 * ENVIRONMENT THAT ARISE FROM COMPANY CONTENT, COMPANY APPLICATIONS OR THIRD
 * PARTY CONTENT. SIGMA DOES NOT MAKE ANY REPRESENTATION OR WARRANTY REGARDING THE
 * RELIABILITY, ACCURACY, COMPLETENESS, CORRECTNESS, OR USEFULNESS OF THIRD PARTY
 * CONTENT OR SERVICE OR THE SIGMA PROGRAM, AND DISCLAIMS ALL LIABILITIES ARISING
 * FROM OR RELATED TO THE SIGMA PROGRAM OR THIRD PARTY CONTENT OR SERVICES. TO THE
 * EXTENT NOT PROHIBITED BY LAW, THESE WARRANTIES ARE EXCLUSIVE. SIGMA OFFERS NO
 * WARRANTY OF NON-INFRINGEMENT, TITLE, OR QUIET ENJOYMENT. NEITHER SIGMA NOR ITS
 * SUPPLIERS OR LICENSORS SHALL BE LIABLE FOR ANY INDIRECT, SPECIAL, INCIDENTAL OR
 * CONSEQUENTIAL DAMAGES OR LOSS (INCLUDING DAMAGES FOR LOSS OF BUSINESS, LOSS OF
 * PROFITS, OR THE LIKE), ARISING OUT OF THIS AGREEMENT WHETHER BASED ON BREACH OF
 * CONTRACT, INTELLECTUAL PROPERTY INFRINGEMENT, TORT (INCLUDING NEGLIGENCE),
 * STRICT LIABILITY, PRODUCT LIABILITY OR OTHERWISE, EVEN IF SIGMA OR ITS
 * REPRESENTATIVES HAVE BEEN ADVISED OF OR OTHERWISE SHOULD KNOW ABOUT THE
 * POSSIBILITY OF SUCH DAMAGES. THERE ARE NO OTHER EXPRESS OR IMPLIED WARRANTIES
 * OR CONDITIONS INCLUDING FOR SOFTWARE, HARDWARE, SYSTEMS, NETWORKS OR
 * ENVIRONMENTS OR FOR MERCHANTABILITY, NONINFRINGEMENT, SATISFACTORY QUALITY AND
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * The Sigma Program  is not fault-tolerant and is not designed, manufactured or
 * intended for use or resale as on-line control equipment in hazardous
 * environments requiring fail-safe performance, such as in the operation of
 * nuclear facilities, aircraft navigation or communication systems, air traffic
 * control, direct life support machines, or weapons systems, in which the failure
 * of the Sigma Program, or Company Applications created using the Sigma Program,
 * could lead directly to death, personal injury, or severe physical or
 * environmental damage ("High Risk Activities").  Sigma and its suppliers
 * specifically disclaim any express or implied warranty of fitness for High Risk
 * Activities.Without limiting Sigma’s obligation of confidentiality as further
 * described in the Z-Wave Controller Development Kit Limited License Agreement,
 * Sigma has no obligation to establish and maintain a data privacy and
 * information security program with regard to Company’s use of any Third Party
 * Service Environment or Cloud Service Environment. For the avoidance of doubt,
 * Sigma shall not be responsible for physical, technical, security,
 * administrative, and/or organizational safeguards that are designed to ensure
 * the security and confidentiality of the Company Content or Company Application
 * in any Third Party Service Environment or Cloud Service Environment that
 * Company chooses to utilize.
 */

#ifndef ZW_SENDDATAAPPL_H_
#define ZW_SENDDATAAPPL_H_

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "timer.h"
#include "zw_serialapi/zw_classcmd.h"
#include "zw_serialapi/zw_serialapi.h"

#define TRANSMIT_OPTION_SECURE          0x0100
#define SCHEME_NOT_SUPPORT              0x00


/** \ingroup transport
 * \defgroup Send_Data_Appl Send Data Appl
 * SendDataAppl is the top level module for sending encapsulated packages on
 * the Z-Wave network.
 * @{
 */

/**
 * Enumeration of security schemes, this must map to the bit numbers in NODE_FLAGS
 */
typedef enum SECURITY_SCHEME 
{
    /**
    * Do not encrypt message
    */
    NO_SCHEME = 0xFF,

    /**
    * Let the SendDataAppl layer decide how to send the message
    */
    AUTO_SCHEME = 0xFE,

    USE_CRC16 = 0xFD, //Force use of CRC16, not really a security scheme..

    /**
    * Send the message with security Scheme 0
    */
    SECURITY_SCHEME_0 = 7,

    /**
    * Send the message with security Scheme 2
    */
    SECURITY_SCHEME_2_UNAUTHENTICATED = 1,
    SECURITY_SCHEME_2_AUTHENTICATED = 2,
    SECURITY_SCHEME_2_ACCESS = 3,

} security_scheme_t ;

/**
 * Structure describing how a package was received / should be transmitted
 */
typedef struct tx_param 
{
    /**
    * Source node
    */
    uint8_t snode;
    /**
    * Destination node
    */
    uint8_t dnode;

    /**
    * Source endpoint
    */
    uint8_t sendpoint;

    /**
    * Destination endpoint
    */
    uint8_t dendpoint;

    /**
    * Transmit flags
    * see txOptions in \ref ZW_SendData
    */
    uint8_t tx_flags;

    /**
    * Receive flags
    * see rxOptions in \ref ApplicationCommandHandler
    */
    uint8_t rx_flags;

    /**
    * Security scheme used for this package
    */
    security_scheme_t scheme; // Security scheme
} ts_param_t;



typedef void( *ZW_SendDataAppl_Callback_t)(uint8_t txStatus, void* user, TX_STATUS_TYPE *txStatEx);

typedef void( *ZW_SendData_Callback_t)(uint8_t txStatus);

typedef void (*ZW_sendRequestAppCb)(uint8_t, uint8_t*, uint8_t);

typedef enum
{
    REQ_SENDING = 1, 
    REQ_WAITING, 
    REQ_DONE,
} req_state_t;

typedef struct send_request_state
{
    ts_param_t param;
    req_state_t state;
    uint8_t class;
    uint8_t cmd;
    uint16_t timeout;
    ZW_sendRequestAppCb callback;
    timer_t  timer;
} send_request_state_t;



uint8_t send_data(ts_param_t* p, uint8_t* data, uint16_t len,
              ZW_SendDataAppl_Callback_t cb, void* user);

uint8_t
ts_param_cmp(ts_param_t* a1, ts_param_t* a2);

void ts_set_std(ts_param_t* p, uint8_t dnode);

security_scheme_t
highest_scheme(uint8_t scheme_mask);

int
scheme_compare(security_scheme_t a, security_scheme_t b);

security_scheme_t
ZW_SendData_scheme_select(const ts_param_t* param, const uint8_t* data, uint8_t len);

uint8_t  ZW_SendRequestAppl(ts_param_t* p, ZW_APPLICATION_TX_BUFFER *pData,
                        uint8_t dataLength,
                        uint8_t responseCmd, 
                        uint16_t timeout, ZW_sendRequestAppCb callback);

void ts_param_make_reply(ts_param_t* dst, const ts_param_t* src);

void ZW_SendDataAppl_init();


#endif /* ZW_SENDDATAAPPL_H_ */
