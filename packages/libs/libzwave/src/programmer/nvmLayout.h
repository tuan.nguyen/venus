#ifndef _NVM_LAYOUT_H_
#define _NVM_LAYOUT_H_


#define NVR_LAYOUT_REV                  0x01

#define SAWC_BYTE1                      0x0D
#define SAWC_BYTE2                      0xDC
#define SAWC_BYTE3                      0x84

#define PIN_SWAP                        0x00

#define SAWB                            0x08

#define NVM_CHIP_SELECT_REV_F           0x15
#define NVM_CHIP_SELECT_REV_E           0x15

#define NVM_TYPE                        0x01

#define NVM_SIZE_BYTE1                  0x00
#define NVM_SIZE_BYTE2                  0x20

#define NVM_PAGE_BYTE1                  0x00
#define NVM_PAGE_BYTE2                  0x40

#define USB_VID_BYTE1                   0x06
#define USB_VID_BYTE2                   0x58

#define USB_PID_BYTE1                   0x02
#define USB_PID_BYTE2                   0x00

#define CCAL                            0x03

#define TXCAL1                          0x18
#define TXCAL2                          0x14


#endif
