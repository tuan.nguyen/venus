#include <signal.h>
#include "programmerAPI.h"
#include "serial.h"
#include "util_intelhex.h"
#include "util_crc.h"
#include "gpio_intf.h"
#include "ZW_Programmer.h"
#include "zw_api.h"
#include "timing.h"
#include "utils.h"
#include "serialAPI.h"

sTiming txTiming;

#define PAGE_LENGTH 2048
#define ZWAVE_DEFAULT_PORT "/dev/ttyO2"
#define ZWAVE_DEFAULT_PORT_LEN  20
#define ZWAVE_DEFAULT_FIRMWARE_PATH "/lib/firmware/zwave"
#define ZWAVE_DEFAULT_FIRMWARE_NAME "serialapi_controller_static_ZM5304_US.hex"
#define GPIO_PIN_CTRL_NUM 49
#define GPIO_PIN_RST_NUM 46
uint8_t lockBits[9];
int zwaveReInit(char *portName, bool getVersion);

void ZWPGM_controller_enable(void)
{
    /* Enable GPIO pins */
    if (-1 == GPIOExport(GPIO_PIN_CTRL_NUM) || -1 == GPIOExport(GPIO_PIN_RST_NUM))
        return;

    /* Set GPIO directions */
    if (-1 == GPIODirection(GPIO_PIN_CTRL_NUM, OUT) || -1 == GPIODirection(GPIO_PIN_RST_NUM, OUT))
        return;

    /* Drive control pin to low (enable power) */
    if (-1 == GPIOWrite(GPIO_PIN_CTRL_NUM, 0))
        printf("Can't write to CTRL PIN:%d\n", GPIO_PIN_CTRL_NUM);

    /* Drive control pin to low (enable programming mode) */
    if (-1 == GPIOWrite(GPIO_PIN_RST_NUM, 0))
        printf("Can't write to RST PIN:%d\n", GPIO_PIN_RST_NUM);
}

void ZWPGM_controller_reset(void)
{
    /* Drive control pin to high (disable programming mode) */
    if (-1 == GPIOWrite(GPIO_PIN_RST_NUM, 1))
        printf("Can't write to RST PIN:%d\n", GPIO_PIN_RST_NUM);

    /* Disable GPIO pins */
    if (-1 == GPIOUnexport(GPIO_PIN_CTRL_NUM) || -1 == GPIOUnexport(GPIO_PIN_RST_NUM))
        return;
}



void delay_ms(int unit_ms)
{
    timingGetClockSystem(&txTiming);
    while (true)
    {
        usleep(1000);
        if (timingGetElapsedMSec(&txTiming) > unit_ms) //10000ms
        {
            timingGetClockSystemStop(&txTiming);
            return;
        }
    }
}
/*update firmware use zw_programmer*/
/*return -1 if it fail*/
int ZWPGM_UpdateFirmware(char *pdevID, char *pfmPath)
{
    zwaveDestruct();
    char* devID;

    int ret = -1;
    uint8_t sector;
    if (pdevID == NULL)
    {
        devID = malloc(strlen(ZWAVE_DEFAULT_PORT));
        strncpy(devID, ZWAVE_DEFAULT_PORT, strlen(ZWAVE_DEFAULT_PORT));
    }
    else
    {
        devID = malloc(ZWAVE_DEFAULT_PORT_LEN);
        strncpy(devID, pdevID, ZWAVE_DEFAULT_PORT_LEN);
    }

    ZWPGM_controller_enable();

    delay_ms(50);

    fd = open(devID, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0)
    {
        mainlog(logDebug,"error %d opening %s: %s", errno, devID, strerror(errno));
        goto finish;
    }

    set_interface_attribs(fd, B115200, 0, 2);

    ret = ZWPGM_enableInterface();
    if (ret == -1)
    {
        mainlog(logDebug, "ZWPGM_enableInterface fail");
        goto finish;
    }
    ret = ZWPGM_readSignature();
    if (ret == -1)
    {
        mainlog(logDebug, "ZWPGM_readSignature fail");
        goto finish;
    }

    ret = ZWPGM_readNVramPage();
    if (ret == -1)
    {
        mainlog(logDebug, "ZWPGM_readNVramPage fail");
        goto finish;
    }
    ZWPGM_loadDefaultNVramPage();

    mainlog(logDebug, "NVM :");
    hexdump(bNVRArray,0x7F);

    ret = ZWPGM_readLockBits(lockBits);
    if (ret == -1)
    {
        mainlog(logDebug, "ZWPGM_readLockBits fail");
        goto finish;
    }

    mainlog(logDebug, "lockBits :");
    hexdump(lockBits,9);

    uint8_t* flashMem = (uint8_t*)malloc(FLASH_MEM_LENGTH);
    uint32_t  flashMem_len;
    ret = intelHex_loadfile(pfmPath, flashMem, &flashMem_len);
    calculateCrc(flashMem);
    if (ret == -1)
    {
        mainlog(logDebug, "Open file fail");
        goto finish;
    }

    ret = ZWPGM_eraseChip();
    if (ret == -1)
    {
        mainlog(logDebug, "ZWPGM_eraseChip fail");
        goto finish;
    }
    ret = ZWPGM_writeNVramPage();
    if (ret == -1)
    {
        mainlog(logDebug, "ZWPGM_writeNVramPage fail");
        goto finish;
    }

    for (sector = 0; sector < 64; sector++)
    {
        ret = ZWPGM_writeFlashPage(&flashMem[sector * PAGE_LENGTH], PAGE_LENGTH, sector);
        if (ret == -1)
        {
            mainlog(logDebug,"ZWPGM_writeFlashPage fail");
            goto finish;
        }
    }
    ret = ZWPGM_runCrcCheck();
    if (ret == -1)
    {
        mainlog(logDebug, "ZWPGM_runCrcCheck fail");
        goto finish;
    }

    ret = ZWPGM_writeLockBits(lockBits);
    if (ret == -1)
    {
        mainlog(logDebug, "ZWPGM_writeLockBits fail");
        goto finish;
    }

finish:
    if (flashMem)
    {
        free(flashMem);
    }

    ZWPGM_controller_reset();
    close(fd);
    delay_ms(2000);
    zwaveReInit(devID, true);
    if (devID)
    {
        free(devID);
    }
    return ret;
}


int ZWPGM_CheckUpdateFirmware(char *pdevID)
{
    char* devID;
    serialAPIDestruct();
    close(fd);

    int ret = -1;

    if (pdevID == NULL)
    {
        devID = malloc(strlen(ZWAVE_DEFAULT_PORT));
        strncpy(devID, ZWAVE_DEFAULT_PORT, strlen(ZWAVE_DEFAULT_PORT));
    }
    else
    {
        devID = malloc(ZWAVE_DEFAULT_PORT_LEN);
        strncpy(devID, pdevID, ZWAVE_DEFAULT_PORT_LEN);
    }

    ZWPGM_controller_enable();

    delay_ms(50);

    fd = open(devID, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0)
    {
        mainlog(logDebug,"error %d opening %s: %s", errno, pdevID, strerror(errno));
        goto finish;
    }

    set_interface_attribs(fd, B115200, 0, 2);

    ret = ZWPGM_enableInterface();
    if (ret == -1)
    {
        mainlog(logDebug, "ZWPGM_enableInterface fail");
        goto finish;
    }
    ret = ZWPGM_readSignature();
    if (ret == -1)
    {
        mainlog(logDebug, "ZWPGM_readSignature fail");
        goto finish;
    }

finish:

    ZWPGM_controller_reset();
    close(fd);
    delay_ms(2000);
    zwaveReInit(devID, false);
    if (devID)
    {
        free(devID);
    }
    return ret;
}
