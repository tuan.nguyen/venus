#ifndef _ZW_PROGRAMMER_H_
#define _ZW_PROGRAMMER_H_

#include <stdint.h>
#include <stdio.h>
#include <string.h>

int ZWPGM_UpdateFirmware(char *pdevID, char *pfmPath);
int ZWPGM_CheckUpdateFirmware(char *pdevID);


#endif
