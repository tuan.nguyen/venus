#ifndef _PROGRAMMER_API_H_
#define _PROGRAMMER_API_H_

#include "serial.h"

#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>


#define POLY 0x1021

#define BIT0    0x01
#define BIT1    0x02
#define BIT2    0x04
#define BIT3    0x08
#define BIT4    0x10
#define BIT5    0x20
#define BIT6    0x40
#define BIT7    0x80

#define NVR_START           0x10
#define NVR_END             0x7F
#define CRC16_MSB_ADR       0x7E
#define CRC16_LSB_ADR       0x7F

#define REV_ADR_1_1         0x10

#define CCAL_ADR_1_1        0x11

#define PINS_ADR_1_1        0x12

#define NVMCS_ADR_1_1       0x13

#define SAWC_ADR_3_1        0x14
#define SAWC_ADR_3_2        0x15
#define SAWC_ADR_3_3        0x16

#define SAWB__ADR_1_1       0x17

#define NVMT_ADR_1_1        0x18

#define NVMS_ADR_2_1        0x19
#define NVMS_ADR_2_2        0x1A

#define NVMP_ADR_2_1        0x1B
#define NVMP_ADR_2_2        0x1C

#define UUID_ADR_16_1       0x1D
#define UUID_ADR_16_2       0x1E
#define UUID_ADR_16_3       0x1F
#define UUID_ADR_16_4       0x20
#define UUID_ADR_16_5       0x21
#define UUID_ADR_16_6       0x22
#define UUID_ADR_16_7       0x23
#define UUID_ADR_16_8       0x24
#define UUID_ADR_16_9       0x25
#define UUID_ADR_16_10      0x26
#define UUID_ADR_16_11      0x27
#define UUID_ADR_16_12      0x28
#define UUID_ADR_16_13      0x29
#define UUID_ADR_16_14      0x2A
#define UUID_ADR_16_15      0x2B
#define UUID_ADR_16_16      0x2C

#define VID_ADR_2_1         0x2D
#define VID_ADR_2_2         0x2E

#define PID_ADR_2_1         0x2F
#define PID_ADR_2_2         0x30

#define TXCAL1_ADR_1_1      0x31
#define TXCAL2_ADR_1_1      0x32

//Enable Interface
#define PROG_EN_0_BYTE 0xAC
#define PROG_EN_1_BYTE 0x53
#define PROG_EN_2_BYTE 0xAA
#define PROG_EN_3_BYTE 0x55

//Read Signature
#define READ_SIG_FLASH_0_BYTE 0x30
#define READ_SIG_FLASH_2_BYTE 0x00
#define READ_SIG_FLASH_3_BYTE 0x00
#define NO_OF_SIGNATURE 7

//Read Flash
#define READ_FLASH_MEM_0_BYTE 0x10
#define READ_FLASH_MEM_2_BYTE 0x00
#define READ_FLASH_MEM_3_BYTE 0x00
#define TRIADES_COUNT 683
#define CONT_READ_MEM_0_BYTE 0xA0
#define CONT_READ_MEM_1_BYTE 0x00
#define CONT_READ_MEM_2_BYTE 0x00
#define CONT_READ_MEM_3_BYTE 0x00

//Erase Chip
#define ERASE_CHIP_0_BYTE 0x0A
#define ERASE_CHIP_1_BYTE 0x00
#define ERASE_CHIP_2_BYTE 0x00
#define ERASE_CHIP_3_BYTE 0x00

#define SECTOR_ERASE_ZW050X 0x0B

//Check State
#define CHK_STATE_0_BYTE 0x7F
#define CHK_STATE_1_BYTE 0xFE
#define CHK_STATE_2_BYTE 0x00
#define CHK_STATE_3_BYTE 0x00

//Run CRC Check
#define RUN_CRC_CHK_0_BYTE 0xC3
#define RUN_CRC_CHK_1_BYTE 0x00
#define RUN_CRC_CHK_2_BYTE 0x00
#define RUN_CRC_CHK_3_BYTE 0x00

//Write SRAM
#define WRITE_SRAM_1_BYTE 0x04
#define WRITE_SRAM_4_BYTE 0x00

#define CONTINUE_WRITE_1_BYTE 0x80
#define CONTINUE_WRITE_2_BYTE 0x00
#define CONTINUE_WRITE_3_BYTE 0x00
#define CONTINUE_WRITE_4_BYTE 0x00

#define WRITE_FLASH_ZW050X 0x20

//Read Lock Bits
#define FLASH_READ_LOCK_1_BYTE 0xF1
#define FLASH_READ_LOCK_3_BYTE 0x00
#define FLASH_READ_LOCK_4_BYTE 0x00

//Write Lock Bits.
#define FLASH_WRITE_LOCK_1_BYTE 0xF0
#define FLASH_WRITE_LOCK_3_BYTE 0x00

#define NO_OF_LOCKBITS          0x09

//Read SRAM
#define READ_SRAM_1_BYTE 0x06
#define READ_SRAM_4_BYTE 0x00
//Enable EooS
#define ENABLE_EOOS_MODE_1_BYTE 0xC0
#define ENABLE_EOOS_MODE_2_BYTE 0x00
#define ENABLE_EOOS_MODE_3_BYTE 0x00
#define ENABLE_EOOS_MODE_4_BYTE 0x00

//Enable EooS
#define DISABLE_EOOS_MODE_1_BYTE 0xD0
#define DISABLE_EOOS_MODE_2_BYTE 0x00
#define DISABLE_EOOS_MODE_3_BYTE 0x00
#define DISABLE_EOOS_MODE_4_BYTE 0x00

#define NVR_SET_0_BYTE 0xFE
#define NVR_SET_1_BYTE 0x00

#define NVR_READ_0_BYTE 0xF2
#define NVR_READ_1_BYTE 0x00
#define NVR_READ_3_BYTE 0x00

#define NVR_START_ADDRESS 0x10
#define NVR_END_ADDRESS 0x7F

#define NO_OF_RETRY     3

extern uint8_t bNVRArray[256];
extern int fd;

// Public API

int ZWPGM_enableInterface(void);
int ZWPGM_readSignature(void);
int ZWPGM_readNVramByte(uint8_t index);
int ZWPGM_loadDefaultNVramPage(void);
int ZWPGM_readLockBits(uint8_t* lockBits);
int ZWPGM_eraseChip(void);
int ZWPGM_runCrcCheck(void);
int ZWPGM_writeLockBits(uint8_t* lockBits);
int ZWPGM_writeFlashPage(uint8_t *page, int pagelength, int sectorNumber);
int ZWPGM_writeNVramPage(void);
int ZWPGM_readNVramPage(void);

#endif
