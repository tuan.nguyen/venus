#ifndef _CMD_CLASS_MISC_H_
#define _CMD_CLASS_MISC_H_

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"
#include "config.h"
#include "utils.h"
#include "serialAPI.h"

uint8_t isSupportedCommandClass(ZW_Node_t *n,
                                const uint8_t commandClass);
uint8_t isEndpointSupportedCommandClass(ZW_Node_t *n, uint8_t index_endpoint,
                                const uint8_t commandClass);
uint8_t isSupportedCommandClassInNIF(uint8_t* nif, uint8_t len,
                                const uint8_t commandClass);
void arrayToString(char *string, uint8_t *data, uint8_t len);
char *GetValidLibTypeStr(uint8_t libType);
char *GetNMModeStr(uint8_t md);
char *GetNMStateStr(uint8_t st);
char *GetFrameTypeStr_Bridge(uint8_t frameType);
char *WriteNodeTypeString(uint8_t nodeType);
char *WriteNodeModeString(uint8_t nodeMode);
char *WriteSchemeString(uint8_t scheme);
void GetControllerCapabilityStr(char *bCharBuf, uint16_t bCharBuf_len);

/**
 * Check is given comand is a get
 * \param cls The Command class number
 * \param cmd The command number
 * \return True is the command is a Get type message. If the command is unknown this function returns false.
 */
int CommandAnalyzerIsGet(uint8_t cls, uint8_t cmd);

/**
 * Check if a given command is a set
 * \param cls The Command class number
 * \param cmd The command number
 * \return True is the command is a Set type message. If the command is unknown this function returns false.
 */
int CommandAnalyzerIsSet(uint8_t cls, uint8_t cmd);
uint16_t CommandAnalyzerExpectReport(int index);


/**
 * Status on incoming frame. Use same values as cc_supervision_status_t
 */
typedef enum 
{
    RECEIVED_FRAME_STATUS_NO_SUPPORT = 0x00, /**< Frame not supported*/
    RECEIVED_FRAME_STATUS_FAIL = 0x02,       /**< Could not handle incoming frame*/
    RECEIVED_FRAME_STATUS_SUCCESS = 0xFF     /**< Frame received successfully*/
} received_frame_status_t;


bool
SupportsFrameAtSecurityLevel(uint8_t class, security_scheme_t scheme);


#endif