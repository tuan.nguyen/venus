#ifndef _ZW_HANDLE_NOTIFY_DEVICE_H_
#define _ZW_HANDLE_NOTIFY_DEVICE_H_
#include "serialAPI.h"
#include "ZW_SendDataAppl.h"


#define COMMAND_SPECIFIC_MAX    50
#define COMMAND_SCHEDULING_MAX  10
#define STR_MAX_LEN             512
#define BUF_MAX_LEN             256
#define DEFAULT_RETRY           2


/*handle wake up notification from device*/
typedef struct handle_wake_up_notification
{
    uint8_t scheme;
    uint8_t sourceNodeId;
    uint8_t rxStatus;
}handle_wake_up_notification_t;

/*contain request from application layout*/
typedef struct _cmd_req_element
{
    uint8_t node_id;
    uint8_t buf[BUF_SIZE];
    uint8_t length;
    uint8_t source_endpoint;
    uint8_t dest_endpoint;
    uint8_t scheme;
    char    strData[STR_MAX_LEN];
} cmd_req_element_t;

//struct contain message from device
typedef struct _handle_message_from_device
{
    uint8_t cmd;
    uint8_t cmd_class;
    ts_param_t p;
} handle_message_from_device_t;

//struct waiting send wake up no more information
typedef struct _handle_send_wake_up_no_info
{
    uint8_t nodeId;
    uint8_t scheme;
    sTiming lastTimeRequestInfo;
} handle_send_wake_up_no_info_t;

//struct waiting massage from handler
typedef struct _waiting_message_from_handler
{
    uint8_t nodeId;
    sTiming lastTimeSendSpecCmd;
} waiting_message_from_handler_t;

//struct give info device, cmd for app into queue
typedef struct _scheduling_tast
{
    uint8_t task_type;//true --> frame send wake up no more info
    uint8_t retry;
    union
    {
        handle_message_from_device_t    handle_message_from_device;
        handle_send_wake_up_no_info_t   handle_send_wake_up_no_info;
        waiting_message_from_handler_t  waiting_message_from_handler;
    };
} scheduling_tast_t;

typedef enum SCHEDULE_TAST_TYPE 
{
    TT_WAKE_UP_NO_MORE_INFO = 1,
    TT_MESSAGE_WAKE_UP_NOTIFY,
    TT_MESSAGE_DEVICE_RESET_LOCALLY,
    TT_WAITING_MESSAGE_FROM_HANDLER,
}SCHEDULE_TAST_TYPE_t;

//variable and function for handle wake up notify and device reset locally
void HandleNotifyThreadProc(void *data);
//function check Remain Cmd Of Source Node In Queue
bool checkRemainCmdOfSourceNodeInQueue(uint8_t sourceNodeId, uint8_t *index);
void CheckNodeIdAndResetTimerSendWakupNoMoreInfo(uint8_t node_id);
bool checkNodeIdNoMoreInfoInQueue(uint8_t sourceNodeId, uint8_t *index);
bool checkNodeIdWaitingMessageFromHandlerInQueue(uint8_t sourceNodeId);
#endif /* ZW_HANDLEAPPLICATIONCMDFROMDEVICE_H_ */