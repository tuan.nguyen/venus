#include "cmd_class_misc.h"
#include "nvm.h"
#include "utils.h"

const int get_list_len = 224;
const int get_list[] = {
    0x2002, 0x2401, 0x2403, 0x2502, 0x2602, 0x2606, 0x2702, 0x2802,
    0x2902, 0x2a02, 0x2a05, 0x2a08, 0x2a0b, 0x2a0e, 0x2a11, 0x2a14,
    0x2a17, 0x2a1a, 0x2a1d, 0x2a20, 0x2a23, 0x2a26, 0x2c02, 0x2d02,
    0x2e01, 0x2e03, 0x2e05, 0x2f01, 0x2f03, 0x2f05, 0x3001, 0x3002,
    0x3101, 0x3103, 0x3104, 0x3201, 0x3203, 0x3301, 0x3303, 0x3504,
    0x3601, 0x3701, 0x3703, 0x3802, 0x3805, 0x3809, 0x380c, 0x3902,
    0x3905, 0x3908, 0x390a, 0x3a01, 0x3b01, 0x3b03, 0x3d01, 0x3d03,
    0x3d05, 0x3d07, 0x3d09, 0x3d0a, 0x3d0c, 0x3d0e, 0x3e02, 0x3f01,
    0x3f03, 0x4002, 0x4004, 0x4201, 0x4202, 0x4205, 0x4302, 0x4304,
    0x4309, 0x4402, 0x4404, 0x4502, 0x4602, 0x4604, 0x4607, 0x4702,
    0x4901, 0x4903, 0x4905, 0x4907, 0x4909, 0x4b01, 0x4b03, 0x4b05,
    0x4c01, 0x4c03, 0x4d08, 0x4e04, 0x4e07, 0x4e09, 0x4e0b, 0x4e0e,
    0x5102, 0x5201, 0x5203, 0x5301, 0x5304, 0x5308, 0x5901, 0x5903,
    0x5905, 0x5b01, 0x5b05, 0x5c02, 0x5d02, 0x5e01, 0x5f02, 0x5f05,
    0x5f09, 0x5f0c, 0x6004, 0x6007, 0x6009, 0x600e, 0x6103, 0x6202,
    0x6205, 0x6302, 0x6304, 0x6402, 0x6404, 0x6406, 0x6408, 0x6502,
    0x6504, 0x6602, 0x6604, 0x6607, 0x6702, 0x6704, 0x6802, 0x6805,
    0x6901, 0x6a01, 0x6a03, 0x6b01, 0x6b03, 0x6b06, 0x6b08, 0x6b0b,
    0x6b0f, 0x6c01, 0x6d02, 0x6d04, 0x6e01, 0x6f02, 0x6f04, 0x6f07,
    0x7005, 0x7008, 0x700a, 0x700c, 0x700e, 0x7101, 0x7104, 0x7107,
    0x7204, 0x7206, 0x7302, 0x7305, 0x7502, 0x7504, 0x7507, 0x750a,
    0x7602, 0x7702, 0x7705, 0x7a01, 0x7a03, 0x7a05, 0x7a0a, 0x7b02,
    0x7d02, 0x8002, 0x8105, 0x8405, 0x8409, 0x8502, 0x8505, 0x850b,
    0x8611, 0x8613, 0x8702, 0x8704, 0x8802, 0x8902, 0x8a01, 0x8a03,
    0x8a06, 0x8b02, 0x8c02, 0x8e02, 0x8e05, 0x9002, 0x9201, 0x9301,
    0x9402, 0x9404, 0x9501, 0x9503, 0x9505, 0x9507, 0x9601, 0x9701,
    0x9802, 0x9804, 0x9840, 0x98c1, 0x9901, 0x9a02, 0x9b01, 0x9b04,
    0x9c01, 0x9c03, 0x9e02, 0x9f01, 0x9f04, 0x9f09, 0x9f0d, 0x9f0f,
};

const int report_list[] = {
    0x2003, 0x2402, 0x2404, 0x2503, 0x2603, 0x2607, 0x2703, 0x2803,
    0x2903, 0x2a03, 0x2a06, 0x2a09, 0x2a0c, 0x2a0f, 0x2a12, 0x2a15,
    0x2a18, 0x2a1b, 0x2a1e, 0x2a21, 0x2a24, 0x2a27, 0x2c03, 0x2d03,
    0x2e02, 0x2e04, 0x2e06, 0x2f02, 0x2f04, 0x2f06, 0x3004, 0x3003,
    0x3102, 0x3106, 0x3105, 0x3202, 0x3204, 0x3302, 0x3304, 0x3505,
    0x3602, 0x3702, 0x3704, 0x3803, 0x3806, 0x380a, 0x380d, 0x3903,
    0x3906, 0x3909, 0x390b, 0x3a02, 0x3b02, 0x3b04, 0x3d02, 0x3d04,
    0x3d06, 0x3d08, 0x3d0b, 0x3d0b, 0x3d0d, 0x3d0f, 0x3e03, 0x3f02,
    0x3f04, 0x4003, 0x4005, 0x4204, 0x4203, 0x4206, 0x4303, 0x4305,
    0x430a, 0x4403, 0x4405, 0x4503, 0x4603, 0x4605, 0x4608, 0x4703,
    0x4902, 0x4904, 0x4906, 0x4908, 0x490a, 0x4b02, 0x4b04, 0x4b06,
    0x4c02, 0x4c04, 0x4d09, 0x4e05, 0x4e08, 0x4e0a, 0x4e0c, 0x4e0f,
    0x5103, 0x5202, 0x5204, 0x5302, 0x5305, 0x5309, 0x5902, 0x5904,
    0x5906, 0x5b02, 0x5b06, 0x5c03, 0x5d03, 0x5e02, 0x5f03, 0x5f06,
    0x5f0a, 0x5f0d, 0x6005, 0x6008, 0x600a, 0x600f, 0x6104, 0x6203,
    0x6206, 0x6303, 0x6305, 0x6403, 0x6405, 0x6407, 0x6409, 0x6503,
    0x6505, 0x6603, 0x6605, 0x6608, 0x6702, 0x6705, 0x6803, 0x6806,
    0x6903, 0x6a02, 0x6a04, 0x6b02, 0x6b04, 0x6b07, 0x6b09, 0x6b0c,
    0x6b10, 0x6c02, 0x6d03, 0x6d05, 0x6e02, 0x6f03, 0x6f05, 0x6f08,
    0x7006, 0x7009, 0x700b, 0x700d, 0x700f, 0x7102, 0x7104, 0x7108,
    0x7205, 0x7207, 0x7303, 0x7306, 0x7503, 0x7505, 0x7508, 0x750b,
    0x7603, 0x7703, 0x7706, 0x7a02, 0x7a04, 0x7a06, 0x7a0b, 0x7b03,
    0x7d03, 0x8003, 0x8106, 0x8406, 0x840a, 0x8503, 0x8506, 0x850c,
    0x8612, 0x8614, 0x8703, 0x8705, 0x8803, 0x8903, 0x8a02, 0x8a04,
    0x8a07, 0x8b03, 0x8c03, 0x8e03, 0x8e06, 0x9003, 0x9202, 0x9303,
    0x9403, 0x9405, 0x9502, 0x9504, 0x9506, 0x9508, 0x9602, 0x9702,
    0x9803, 0x9805, 0x9880, 0x98c1, 0x9902, 0x9a03, 0x9b02, 0x9b05,
    0x9c02, 0x9c04, 0x9e03, 0x9f02, 0x9f05, 0x9f0a, 0x9f0e, 0x9f10,
};

const int set_list_len = 138;
const int set_list[] = {
    0x0112, 0x0113, 0x0122, 0x2001, 0x2405, 0x2501, 0x2601, 0x2701,
    0x2801, 0x2901, 0x2a01, 0x2a04, 0x2a07, 0x2a0a, 0x2a0d, 0x2a10,
    0x2a13, 0x2a16, 0x2a19, 0x2a1a, 0x2a1b, 0x2a1f, 0x2a22, 0x2a25,
    0x2a28, 0x2b01, 0x2c01, 0x2d01, 0x3305, 0x3412, 0x3414, 0x3801,
    0x3804, 0x3805, 0x3806, 0x380b, 0x3811, 0x3901, 0x3904, 0x3907,
    0x3a03, 0x3c01, 0x3e01, 0x4001, 0x4301, 0x4302, 0x4303, 0x4304,
    0x4305, 0x4309, 0x430a, 0x4401, 0x4601, 0x4606, 0x4701, 0x4702,
    0x4703, 0x4801, 0x4a01, 0x4a02, 0x4d01, 0x4d02, 0x4d06, 0x4d07,
    0x4e01, 0x4e02, 0x4e03, 0x4e06, 0x4e0d, 0x4e10, 0x5101, 0x5303,
    0x5307, 0x5b04, 0x5c01, 0x5d01, 0x5f01, 0x5f04, 0x5f07, 0x5f08,
    0x5f0b, 0x6101, 0x6201, 0x6204, 0x6301, 0x6401, 0x6402, 0x6403,
    0x6404, 0x6405, 0x6406, 0x6407, 0x6408, 0x6409, 0x6501, 0x6601,
    0x6606, 0x6701, 0x6801, 0x6804, 0x6902, 0x6a05, 0x6b05, 0x6b0a,
    0x6b0e, 0x6d01, 0x6f06, 0x7004, 0x7007, 0x7106, 0x7301, 0x7304,
    0x7501, 0x7506, 0x7509, 0x7601, 0x7701, 0x7704, 0x7a08, 0x7b01,
    0x7d01, 0x8104, 0x8404, 0x8501, 0x8701, 0x8801, 0x8901, 0x8a05,
    0x8b01, 0x8c01, 0x8e01, 0x9401, 0x9806, 0x9a01, 0x9b03, 0x9d01,
    0x9e01, 0x9f06,
};

int cmpfunc(const void * a, const void * b)
{
    return ( *(int*)a - *(int*)b );
}

uint8_t isSupportedCommandClass(ZW_Node_t *n,
                                const uint8_t commandClass)
{
    int i;
    if (n)
    {
        for (i = 0; i < n->node_capability_secureV2.noCapability; i++)
        {
            if (n->node_capability_secureV2.aCapability[i] == commandClass)
            {
                mainlog(logDebug, "commandClass: %02X -> AUTO_SCHEME", commandClass);
                return AUTO_SCHEME;
            }
        }
    }

    if (n)
    {
        for (i = 0; i < n->node_capability_secureV0.noCapability; i++)
        {
            if (n->node_capability_secureV0.aCapability[i] == commandClass)
            {
                mainlog(logDebug, "commandClass: %02X -> SECURITY_SCHEME_0", commandClass);
                return SECURITY_SCHEME_0;
            }
        }
    }

    if (commandClass == COMMAND_CLASS_BASIC)
    {
        mainlog(logDebug, "commandClass: %02X -> NO_SCHEME", commandClass);
        return NO_SCHEME;
    }

    if (n)
    {
        for (i = 0; i < n->node_capability.noCapability; i++)
        {
            if (n->node_capability.aCapability[i] == commandClass)
            {
                mainlog(logDebug, "commandClass: %02X -> NO_SCHEME", commandClass);
                return NO_SCHEME;
            }
        }
    }
    mainlog(logDebug, "commandClass: %02X -> NOT_SUPPORT", commandClass);
    return 0;
}

uint8_t isEndpointSupportedCommandClass(ZW_Node_t *n, uint8_t index_endpoint,
                                const uint8_t commandClass)
{
    int i;
    if (n)
    {
        for (i = 0; i < n->multi_channel_capability[index_endpoint - 1].endpoint_capability.noCapability; i++)
        {
            if (n->multi_channel_capability[index_endpoint - 1].endpoint_capability.aCapability[i] == commandClass)
            {
                mainlog(logDebug, "commandClass: %02X -> NO_SCHEME", commandClass);
                return NO_SCHEME;
            }
        }
    }
    mainlog(logDebug, "commandClass: %02X -> NOT_SUPPORT", commandClass);
    return 0;
}

uint8_t isSupportedCommandClassInNIF(uint8_t* nif, uint8_t len,
                                const uint8_t commandClass)
{
    int i;
    for (i = 0; i < len; i++)
    {
        if (nif[i] == commandClass)
        {
            mainlog(logDebug, "commandClass: %02X -> NO_SCHEME", commandClass);
            return NO_SCHEME;
        }
    }
    return 0;
}

void arrayToString(char *string, uint8_t *data, uint8_t len)
{
    int i;
    sprintf(string, "%02X", data[0]);
    for (i = 1; i < len; i++)
    {
        sprintf(string, "%s%02X", string, data[i]);
    }
}

char *GetValidLibTypeStr(uint8_t libType)
{
    char *str;
    switch (libType)
    {
    case ZW_LIB_CONTROLLER_STATIC:
        str = "Z-Wave Controller Static";
        break;
    case ZW_LIB_CONTROLLER:
        str = "Z-Wave Controller";
        break;
    case ZW_LIB_SLAVE_ENHANCED:
        str = "Z-Wave Slave Enhanced";
        break;
    case ZW_LIB_SLAVE:
        str = "Z-Wave Slave";
        break;
    case ZW_LIB_INSTALLER:
        str = "Z-Wave Installer";
        break;
    case ZW_LIB_SLAVE_ROUTING:
        str = "Z-Wave Slave Routing";
        break;
    case ZW_LIB_CONTROLLER_BRIDGE:
        str = "Z-Wave Controller Bridge";
        break;
    default:
        str = " ";
        break;
    }
    return str;
}

char *WriteNodeModeString(uint8_t nodeMode)
{
    char *str;
    switch (nodeMode)
    {
    case MODE_PROBING:
        str = "MODE_PROBING";
        break;
    case MODE_NONLISTENING:
        str = "MODE_NONLISTENING";
        break;
    case MODE_ALWAYSLISTENING:
        str = "MODE_ALWAYSLISTENING";
        break;
    case MODE_FREQUENTLYLISTENING:
        str = "MODE_FREQUENTLYLISTENING";
        break;
    case MODE_MAILBOX:
        str = "MODE_MAILBOX";
        break;
    default:
        str = " ";
        break;
    }
    return str;
}

char *WriteSchemeString(uint8_t scheme)
{
    char *str;
    switch (scheme)
    {
    case NO_SCHEME:
        str = "NO_SCHEME";
        break;
    case SECURITY_SCHEME_0:
        str = "SECURITY_SCHEME_0";
        break;
    case USE_CRC16:
        str = "USE_CRC16";
        break;
    case SECURITY_SCHEME_2_ACCESS:
        str = "SECURITY_SCHEME_2_ACCESS";
        break;
    case SECURITY_SCHEME_2_AUTHENTICATED:
        str = "SECURITY_SCHEME_2_AUTHENTICATED";
        break;
    case SECURITY_SCHEME_2_UNAUTHENTICATED:
        str = "SECURITY_SCHEME_2_UNAUTHENTICATED";
        break;
    case AUTO_SCHEME:
        str = "AUTO_SCHEME";
        break;
    default:
        str = " ";
        break;
    }
    return str;
}
char *GetFrameTypeStr_Bridge(uint8_t frameType)
{
    char *str;
    switch (frameType)
    {
    case RECEIVE_STATUS_TYPE_SINGLE:
        str = "virtual singlecast";
        break;
    case RECEIVE_STATUS_TYPE_BROAD:
        str = "virtual broadcast";
        break;
    case RECEIVE_STATUS_TYPE_MULTI:
        str = "virtual multicast";
        break;
    case RECEIVE_STATUS_TYPE_EXPLORE:
        break;
    case RECEIVE_STATUS_ROUTED_BUSY:
        break;
    case RECEIVE_STATUS_LOW_POWER:
        break;
    default:
        str = "virtual Unknown frametype";
        break;
    }
    return str;
}

char *WriteNodeTypeString(uint8_t nodeType)
{
    char *str;
    switch (nodeType)
    {
    case GENERIC_TYPE_GENERIC_CONTROLLER:
        str = "Generic Controller";
        break;
    case GENERIC_TYPE_STATIC_CONTROLLER:
        str = "Static Controller";
        break;
    case GENERIC_TYPE_REPEATER_SLAVE:
        str = "Repeater Slave";
        break;
    case GENERIC_TYPE_SWITCH_BINARY:
        str = "Binary Switch";
        break;
    case GENERIC_TYPE_SWITCH_MULTILEVEL:
        str = "Multilevel Switch";
        break;
    case GENERIC_TYPE_SWITCH_REMOTE:
        str = "Remote Switch";
        break;
    case GENERIC_TYPE_SWITCH_TOGGLE:
        str = "Toggle Switch";
        break;
    case GENERIC_TYPE_SENSOR_BINARY:
        str = "Binary Sensor";
        break;
    case GENERIC_TYPE_SENSOR_MULTILEVEL:
        str = "Sensor Multilevel";
        break;
    case GENERIC_TYPE_SENSOR_ALARM:
        str = "Sensor Alarm";
        break;
    case GENERIC_TYPE_METER:
        str = "Meter";
        break;
    case GENERIC_TYPE_METER_PULSE:
        str = "Pulse Meter";
        break;
    case GENERIC_TYPE_ENTRY_CONTROL:
        str = "Entry Control";
        break;
    case GENERIC_TYPE_AV_CONTROL_POINT:
        str = "AV Control Point";
        break;
    case GENERIC_TYPE_DISPLAY:
        str = "Display";
        break;
    case GENERIC_TYPE_SEMI_INTEROPERABLE:
        str = "Semi Interoperable";
        break;
    case GENERIC_TYPE_NON_INTEROPERABLE:
        str = "Non Interoperable";
        break;
    case GENERIC_TYPE_THERMOSTAT:
        str = "Thermostat";
        break;
    case GENERIC_TYPE_VENTILATION:
        str = "Ventilation";
        break;
    case GENERIC_TYPE_WINDOW_COVERING:
        str = "Window Covering";
        break;
    case GENERIC_TYPE_SECURITY_PANEL:
        str = "Security Panel";
        break;
    case GENERIC_TYPE_WALL_CONTROLLER:
        str = "Wall Controller";
        break;
    case GENERIC_TYPE_APPLIANCE:
        str = "Appliance";
        break;
    case GENERIC_TYPE_SENSOR_NOTIFICATION:
        str = "Sensor Notification";
        break;
    case GENERIC_TYPE_NETWORK_EXTENDER:
        str = "Network Extender";
        break;
    case GENERIC_TYPE_ZIP_NODE:
        str = "Zip Node";
        break;
    case 0: /* No Nodetype registered */
        str = "No device";
        break;
    default:
        str = "Unknown Device type";
        break;
    }
    return str;
}
void GetControllerCapabilityStr(char *bCharBuf, uint16_t bCharBuf_len)
{
    uint8_t bControllerCapabilities;

    if (serialApiGetControllerCapabilities(&bControllerCapabilities))
    {
        if (bControllerCapabilities & CONTROLLER_CAPABILITIES_NODEID_SERVER_PRESENT)
        {
            if (bControllerCapabilities & CONTROLLER_CAPABILITIES_IS_SUC)
            {
                snprintf(bCharBuf, bCharBuf_len, "SIS");
            }
            else
            {
                snprintf(bCharBuf, bCharBuf_len, "Inclusion");
            }
        }
        else
        {
            if (bControllerCapabilities & CONTROLLER_CAPABILITIES_IS_SUC)
            {
                snprintf(bCharBuf, bCharBuf_len, "SUC");
            }
            else
            {
                if (bControllerCapabilities & CONTROLLER_CAPABILITIES_IS_SECONDARY)
                {
                    snprintf(bCharBuf, bCharBuf_len, "Secondary");
                }
                else
                {
                    snprintf(bCharBuf, bCharBuf_len, "Primary");
                }
            }
        }
        snprintf(&bCharBuf[strlen(bCharBuf)], bCharBuf_len - strlen(bCharBuf), " Controller");
        if (MySystemSUCId > 0)
        {
            if (bControllerCapabilities & CONTROLLER_CAPABILITIES_NODEID_SERVER_PRESENT)
            {
                snprintf(&bCharBuf[strlen(bCharBuf)], bCharBuf_len - strlen(bCharBuf), " in a SIS network");
            }
            else
            {
                snprintf(&bCharBuf[strlen(bCharBuf)], bCharBuf_len - strlen(bCharBuf), " in a SUC network");
            }
        }
        if (bControllerCapabilities & CONTROLLER_CAPABILITIES_IS_REAL_PRIMARY)
        {
            snprintf(&bCharBuf[strlen(bCharBuf)], bCharBuf_len - strlen(bCharBuf), ", (Real primary)");
        }
        if (bControllerCapabilities & CONTROLLER_CAPABILITIES_ON_OTHER_NETWORK)
        {
            snprintf(&bCharBuf[strlen(bCharBuf)], bCharBuf_len - strlen(bCharBuf), ", (Other network)");
        }
        if (bControllerCapabilities & CONTROLLER_CAPABILITIES_NO_NODES_INCUDED)
        {
            snprintf(&bCharBuf[strlen(bCharBuf)], bCharBuf_len - strlen(bCharBuf), ", (No nodes)");
        }
    }
    else
    {
        snprintf(bCharBuf, bCharBuf_len, "Controller could not determine Controller Capabilities");
    }
}


int CommandAnalyzerIsGet(uint8_t cls, uint8_t cmd)
{
    int key = cls << 8 | cmd;
    int *ret;
    //return bSearch(key, get_list, get_list_len);
    ret = (int*)bsearch (&key, get_list, get_list_len, sizeof (int), cmpfunc);
    if (ret != NULL)
    {   
        mainlog(logDebug,"found! index :%d", ret - get_list); 
        return ret - get_list;
    }
    else
    {
        mainlog(logDebug,"not found!"); 
        return -1;
    }
}

int CommandAnalyzerIsSet(uint8_t cls, uint8_t cmd)
{
    uint16_t key = cls << 8 | cmd;
    int *ret;
    ret = (int*)bsearch (&key, set_list, set_list_len, sizeof (uint16_t), cmpfunc);
    if (ret != NULL)
    {   
        mainlog(logDebug,"found! index :%d", ret - get_list); 
        return ret - get_list;
    }
    else
    {
        mainlog(logDebug,"not found!"); 
        return -1;
    }
}

uint16_t CommandAnalyzerExpectReport(int index)
{
    if ((index < 0) || (index > get_list_len)) return 0;

    return report_list[index];
}



/*
 * If a handler is supported with NO_SCHEME we allow the package no matter what
 * scheme it was received on.
 *
 * If a handler has a minimal security scheme it has to be sent with the net_scheme
 * ie the highest scheme supported by the node.
 */
bool
SupportsFrameAtSecurityLevel(uint8_t class, security_scheme_t scheme)
{
    uint8_t schemeGet;
    uint8_t net_scheme;
    uint8_t minimal_scheme;

    schemeGet = isSupportedCommandClass(&myZPCCapability, class);
    /* Not support this command class */
    if (!schemeGet) return false;

    int flags = GetCacheEntryFlag(MyNodeId);

    net_scheme = highest_scheme(flags & 0xFF);

    if (scheme_compare(net_scheme, SECURITY_SCHEME_0))
    {
        /* be included to Security network net_scheme >= S0 scheme */
        if ((schemeGet == SECURITY_SCHEME_0) || (schemeGet == AUTO_SCHEME))
        {
            minimal_scheme = net_scheme;
        }else
        {
            minimal_scheme = NO_SCHEME; 
        }
    }else
    {
        /* be included to Non Security network */
        minimal_scheme = NO_SCHEME;
    }
   
    return (scheme_compare(scheme , minimal_scheme)); 

}


char *GetNMStateStr(uint8_t st)
{
    char *str;
    switch (st)
    {
    case NMS_IDLE:
        str = "NMS Idle";
        break;
    case NMS_WAITING_FOR_ADD:
        str = "NMS waiting for add";
        break;
    case NMS_WAITING_FOR_REMOVE:
        str = "NMS waiting for remove";
        break;
    case NMS_NODE_FOUND:
        str = "NMS node found";
        break;
    case NMS_WAIT_FOR_PROTOCOL:
        str = "NMS wait for protocol";
        break;
    case NMS_NETWORK_UPDATE:
        str = "NMS network update";
        break;
    case NMS_WAITING_FOR_INTERVIEW:
        str = "NMS wait for interview";
        break;
    case NMS_SET_DEFAULT:
        str = "NMS set default";
        break;
    case NMS_LEARN_MODE:
        str = "NMS learn mode";
        break;
    case NMS_LEARN_MODE_STARTED:
        str = "NMS learn started";
        break;
    case NMS_WAIT_FOR_SECURE_ADD:
        str = "NMS wait for secure add";
        break;
    case NMS_SENDING_NODE_INFO:
        str = "NMS sending node info";
        break;
    case NMS_WAITING_FOR_NODE_REMOVAL:
        str = "NMS wait for node removal";
        break;
    case NMS_WAITING_FOR_FAIL_NODE_REMOVAL:
        str = "NMS wait for fail node removal";
        break;
    case NMS_WAITING_FOR_NODE_NEIGH_UPDATE:
        str = "NMS wait for node neighbor update";
        break;
    case NMS_WAITING_FOR_RETURN_ROUTE_ASSIGN:
        str = "NMS wait for return route assign";
        break;
    case NMS_WAITING_FOR_RETURN_ROUTE_DELETE:
        str = "NMS wait for return route delete";
        break;
    case NMS_WAIT_FOR_INTERVIEW_AFTER_ADD:
        str = "NMS wait for interview after add";
        break;
    case NMS_WAIT_FOR_SECURE_LEARN:
        str = "NMS wait for secure learn";
        break;
    case NMS_WAIT_FOR_INTERVIEW_BY_SIS:
        str = "NMS wait for interview by sis";
        break;
    case NMS_REMOVING_ASSOCIATIONS:
        str = "NMS removing associations";
        break;   
    case NMS_REPLACE_FAILED_REQ:
        str = "NMS replace failed req";
        break;
    case NMS_PREPARE_SUC_INCLUSION:
        str = "NMS prepare sus inclusion";
        break;
    case NMS_WAIT_FOR_SUC_INCLUSION:
        str = "NMS wait for sus inclusion";
        break;
    case NMS_PROXY_INCLUSION_WAIT_NIF:
        str = "NMS proxy inclusion wait nif";
        break;
    case NMS_INCLUSION_CONTROLLER:
        str = "NMS inclusion controller";
        break;
    default:
        str = " ";
        break;
    }
    return str;
}

char *GetNMModeStr(uint8_t md)
{
    char *str;
    switch (md)
    {
        case NMM_ADD_NODE:
            str = "NMM add node"; 
            break;
        case NMM_REMOVE_NODE:
            str = "NMM remove node"; 
            break;
        case NMM_AUTO_REMOVEADD_NODE:
            str = "NMM remove add node";
            break;
        case NMM_REPLACE_FAILED_NODE:
            str = "NMM replace failed node";
            break;
        case NMM_CONTROLLER_CHANGE:
            str = "NMM controller change";
            break;
        case NMM_DEFAULT:
            str = "NMM default";
            break;
        default:
            str = "";
            break;
    }
    return str;
}


