
#include "clock.h"
#include <time.h>

//Fix some bad lib c
#ifndef CLOCK_MONOTONIC_RAW
#define CLOCK_MONOTONIC_RAW 4
#endif
/*---------------------------------------------------------------------------*/
clock_time_t clock_time(void)
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC_RAW,&ts);

    return ts.tv_sec * 1000 + ts.tv_nsec / 1000000;
}
/*---------------------------------------------------------------------------*/
unsigned long clock_seconds(void)
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC_RAW,&ts);

    return ts.tv_sec;
}
/*---------------------------------------------------------------------------*/
void clock_delay(unsigned int d)
{
    /* Does not do anything. */
}
