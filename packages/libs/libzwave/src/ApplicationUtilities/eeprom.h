#ifndef __EEPROM_H__
#define __EEPROM_H__

typedef unsigned long eeprom_addr_t;
#define EEPROM_NULL 0

/**
 * Write a buffer into EEPROM.
 *
 * This function writes a buffer of the specified size into EEPROM.
 *
 * \param addr The address in EEPROM to which the buffer should be written.
 *
 * \param buf A pointer to the buffer from which data is to be read.
 *
 * \param size The number of bytes to write into EEPROM.
 *
 *
 */
void eeprom_write(eeprom_addr_t addr, unsigned char *buf, int size);

/**
 * Read data from the EEPROM.
 *
 * This function reads a number of bytes from the specified address in
 * EEPROM and into a buffer in memory.
 *
 * \param addr The address in EEPROM from which the data should be read.
 *
 * \param buf A pointer to the buffer to which the data should be stored.
 *
 * \param size The number of bytes to read.
 *
 *
 */
void eeprom_read(eeprom_addr_t addr, unsigned char *buf, int size);

/**
 * Initialize the EEPROM module
 *
 * This function initializes the EEPROM module and is called from the
 * bootup code.
 *
 */
 
void eeprom_init(void); 
/**
 * Find the size of eeprom file. This function sets the file index to end of file
 * while calculating the size 
 */
unsigned long eeprom_size();


#endif /* __EEPROM_H__ */