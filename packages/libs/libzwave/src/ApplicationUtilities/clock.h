
#ifndef _CLOCK_H_
#define _CLOCK_H_

typedef unsigned long clock_time_t;
#define CLOCK_SECOND 1000   // 1 ms ticks


clock_time_t clock_time(void);
unsigned long clock_seconds(void);
void clock_delay(unsigned int d);

#endif
