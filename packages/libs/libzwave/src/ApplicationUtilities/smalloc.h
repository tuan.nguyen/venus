
#ifndef SMALLOC_H_
#define SMALLOC_H_

#include <stdint.h>

typedef struct small_memory_device
{
    uint16_t offset;
    uint16_t size;
    uint8_t psize;

    uint16_t (*read)(uint16_t offset,uint8_t len,void* data);
    uint16_t (*write)(uint16_t offset,uint8_t len,void* data);
} small_memory_device_t;


/* Write a small memory chunk to storage and return a pointer to the
 * small memory chunk in the device storage. */
uint16_t smalloc(const small_memory_device_t* dev ,uint8_t datalen);

/**
 * Write a data block to memory device, and returning a pointer to the
 * data written. The returned pointer must be freed at a later stage using smfree
 */
uint16_t smalloc_write(const small_memory_device_t* dev ,uint8_t datalen,void* data);

/**
 * Deallocate a previously allocated memory block
 */
void smfree(const small_memory_device_t* dev ,uint16_t ptr);

/*Format the small memory device so it will be clean and ready for use */
void smformat(const small_memory_device_t* dev);

#endif /* SMALLOC_H_ */