#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "eeprom.h"
#include "utils.h"

static int f;
#ifndef STANDALONE
const char *linux_conf_eeprom_file = "/etc/backup_restore_zwave_controller/eeprom.dat";
#else
const char *linux_conf_eeprom_file = "eeprom.dat";
#endif

void eeprom_init(void)
{

#ifndef STANDALONE
    struct stat st = {0};

    if (stat("/etc/backup_restore_zwave_controller", &st) == -1)
    {
        mkdir("/etc/backup_restore_zwave_controller", 0700);
    }
#endif
   
    mainlog(logDebug, "Opening eeprom file %s\n", linux_conf_eeprom_file);
    f = open(linux_conf_eeprom_file, O_RDWR | O_CREAT, 0644);
    if (f < 0)
    {
        fprintf(stderr, "Error opening eeprom file %s\n", linux_conf_eeprom_file);
        perror("");
        exit(1);
    }
}

void eeprom_write(eeprom_addr_t addr, unsigned char *buf, int size)
{
    lseek(f, addr, SEEK_SET);
    if (write(f, buf, size) != size)
    {
        perror("Write error");
    }
}

void eeprom_read(eeprom_addr_t addr, unsigned char *buf, int size)
{
    lseek(f, addr, SEEK_SET);
    if (read(f, buf, size) != size)
    {
        perror("Read error");
    }
}


eeprom_addr_t eeprom_size()
{
  return lseek(f, 0, SEEK_END);
}
