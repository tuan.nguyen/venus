#ifndef _INLINE_FUNCTION_H_
#define _INLINE_FUNCTION_H_

static inline void swap_endian(uint32_t *val)
{
    *val = (*val<<24) | ((*val<<8) & 0x00ff0000) |
          ((*val>>8) & 0x0000ff00) | (*val>>24);
};

#endif