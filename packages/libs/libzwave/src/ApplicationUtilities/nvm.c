
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "nvm.h"

static int nvm_file = 0;

#ifndef STANDALONE
const char *linux_conf_nvm_file = "/etc/backup_restore_zwave_controller/nvm.dat";
#else
const char *linux_conf_nvm_file = "nvm.dat";
#endif

void nvm_init() 
{
    if(nvm_file==0) 
    {
        nvm_file = open(linux_conf_nvm_file, O_RDWR | O_CREAT , 0644);
    }

    if(nvm_file<0) 
    {
        fprintf(stderr, "Error opening NVM file %s\n",linux_conf_nvm_file);
        perror("");
        exit(1);
    }
}
void NVM_Read(uint16_t start, void* dst, uint8_t size)
{
    lseek(nvm_file, start, SEEK_SET);
    if(read(nvm_file,dst,size) != size) 
    {
        perror(__FUNCTION__);
    }
}


void NVM_Write(uint16_t start,const void* dst, uint8_t size)
{
    lseek(nvm_file, start, SEEK_SET);
    if(write(nvm_file,dst,size) != size) 
    {
        perror(__FUNCTION__);
    }
}