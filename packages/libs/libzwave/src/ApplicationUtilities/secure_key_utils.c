#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include "secure_key_utils.h"
#include "s2_keystore.h"
#include "utils.h"

/*return 0 if create file fail*/
bool save_secure_key(char *file_name)
{
    char str_path_file[200];
    char path_ftp[300];
    uint8_t buf_key[16];
    uint8_t i;  

#ifndef STANDALONE
    struct stat st = {0};

    if (stat("/etc/backup_restore_zwave_controller", &st) == -1)
    {
        mkdir("/etc/backup_restore_zwave_controller", 0700);
    }
    strcpy(str_path_file, "/etc/backup_restore_zwave_controller/");
#else
    strcpy(str_path_file, "./");
#endif

    strcat(str_path_file, file_name);
    strcat(str_path_file, ".txt");

    mainlog(logDebug,"Path of secure key file: %s",str_path_file);

    FILE *f_secure_key = fopen(str_path_file, "w");
    if (f_secure_key == NULL)
    {
        mainlog(logDebug,"Error opening file!");
        return false;
    }
    /*save key */
    /*s0*/
    if (!keystore_network_key_read(KEY_CLASS_S0, buf_key))
    {
        return false;
    }
    fprintf(f_secure_key, "98;");
    for(i = 0; i < 16; i++) fprintf(f_secure_key, "%02X", buf_key[i]);

    //s2.0
    fprintf(f_secure_key, ";1\r\n");
    if (!keystore_network_key_read(KEY_CLASS_S2_UNAUTHENTICATED, buf_key))
    {
        return false;
    }
    fprintf(f_secure_key, "9F;");
    for(i = 0; i < 16; i++) fprintf(f_secure_key, "%02X", buf_key[i]);
    fprintf(f_secure_key, ";1\r\n");

    /*s2.1*/
    if (!keystore_network_key_read(KEY_CLASS_S2_AUTHENTICATED, buf_key))
    {
        return false;
    }
    fprintf(f_secure_key, "9F;");
    for(i = 0; i < 16; i++) fprintf(f_secure_key, "%02X", buf_key[i]);
    fprintf(f_secure_key, ";1\r\n");

    /*s2.2*/
    if (!keystore_network_key_read(KEY_CLASS_S2_ACCESS, buf_key))
    {
        return false;
    }
    fprintf(f_secure_key, "9F;");
    for(i = 0; i < 16; i++) fprintf(f_secure_key, "%02X", buf_key[i]);
    fprintf(f_secure_key, ";1\r\n");
    fclose(f_secure_key);

    /*push file to ftp server*/
    strcpy(path_ftp, "curl -T ");
    strcat(path_ftp, str_path_file);
    strcat(path_ftp, " ftp.veriksystems.com/zwave_compliant/ -u venus_firmware:v3nu5_f!rmw@r3\n\n");
    system(path_ftp);//upload

    return true;
}


