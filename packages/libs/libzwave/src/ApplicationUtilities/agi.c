
#include "agi.h"

#define NUMBER_OF_ENDPOINTS     1
typedef struct __AGI_GROUP_
{
    uint16_t        profile;
    CMD_CLASS_GRP cmdGrp;
    char            groupName[42];
}__AGI_GROUP;

typedef struct _AGI_LIFELINE_
{
    char            grpName[42];
    CMD_CLASS_GRP   *pCmdGrpList;
    uint8_t         listSize;
}AGI_LIFELINE;

typedef struct _AGI_TABLE_EP_
{
    __AGI_GROUP     *pTable;
    uint8_t         tableSize;
}AGI_TABLE_EP;

typedef struct _AGI_TABLE_
{
    AGI_LIFELINE    lifeLine;
    AGI_TABLE_EP    tableEndpoint[NUMBER_OF_ENDPOINTS];
}AGI_TABLE;

const char VenusGroupName[]="Lifeline";

uint8_t VenusGroupCommandList[]= {COMMAND_CLASS_DEVICE_RESET_LOCALLY,DEVICE_RESET_LOCALLY_NOTIFICATION};

uint8_t             currentGroupIndex;
uint8_t             currentGroupNodeIndex;
AGI_TABLE           myAgi;
uint8_t             m_lastActiveGroupId = 1;


void AGI_LifeLineGroupSetup( CMD_CLASS_GRP* pCmdGrpList, uint8_t listSize)
{
    myAgi.lifeLine.pCmdGrpList = pCmdGrpList;
    myAgi.lifeLine.listSize = listSize;
    memcpy(myAgi.lifeLine.grpName, VenusGroupName, strlen(VenusGroupName));
}

void AGI_ResourceGroupSetup(AGI_GROUP* pTable, uint8_t tableSize, uint8_t endpoint)
{
  
    // code is handling enpoints as from 0,1... User can handle endpoint as 0 (no endpoints)or 1,2..
    if( 0 != endpoint)
    {
        endpoint--;
    }
    if(NUMBER_OF_ENDPOINTS >= endpoint)
    {
        myAgi.tableEndpoint[endpoint].pTable = (__AGI_GROUP*)pTable;
        myAgi.tableEndpoint[endpoint].tableSize = tableSize;
    }
}

/*========================   GetApplGroupName  =========================
**   set  Application specific Group Name
**   return none
**
**   Side effects: none
**--------------------------------------------------------------------------*/
void GetApplGroupName(uint8_t* pGroupName,uint8_t groupId)
{
    if (groupId == 1)
    {
        memcpy(pGroupName, myAgi.lifeLine.grpName, strlen(myAgi.lifeLine.grpName));
    }else
    {
        /*Venus has Endpoint 0 for this version!!*/
        memcpy(pGroupName, myAgi.tableEndpoint[0].pTable[groupId-2].groupName, strlen(myAgi.tableEndpoint[0].pTable[groupId-2].groupName));
    }
}

/*========================   GetApplGroupNameLength  =========================
**   get Application specific Group Name Length
**   return size of Group Name Length
**
**   Side effects: none
**--------------------------------------------------------------------------*/
uint8_t GetApplGroupNameLength(uint8_t groupId)
{


    if(groupId < myAgi.tableEndpoint[0].tableSize + 2)
    {

        if(1 == groupId)
        {
            return strlen(myAgi.lifeLine.grpName);    
        }
        else
        {   
            /*Venus has Endpoint 0 for this version!!*/
            /* ASSOCIATION Group 2 = group[0] in Endpoint 0 (ASSOCIATION Group 1 is LifeLine group)*/
            return strlen(myAgi.tableEndpoint[0].pTable[groupId-2].groupName);
        }
    }
    return 0;
}

/*========================   GetApplAssoGroupsSize  ===========
**   get number of the application association groups
**   return none
**
**   Side effects: none
**--------------------------------------------------------------------------*/
uint8_t GetApplAssoGroupsSize(void)
{
    /*Venus has Endpoint 0 for this version!!*/
    return 1 + myAgi.tableEndpoint[0].tableSize; /* Lifeline group + grouptable size.*/
}

/*============================ ApplicationGetLastActiveGroupId ==============
** Function description
** Report the current active group.
**
** Side effects:
**
**-------------------------------------------------------------------------*/
uint8_t ApplicationGetLastActiveGroupId(void)
{
    return m_lastActiveGroupId;
}



/*========================   GetApplGroupInfo  ===========
**   set Application specific Group Info Report
**   return none
**
**   Side effects: none
**--------------------------------------------------------------------------*/
void GetApplGroupInfo(uint8_t groupId, VG_ASSOCIATION_GROUP_INFO_REPORT_VG* report)
{
    if(groupId >= myAgi.tableEndpoint[0].tableSize + 2)
    {
        /*Not legal groupId!*/
        report->groupingIdentifier = 0;
        report->mode = 0;                         /**/
        report->profile1 = 0;                     /* MSB */
        report->profile2 = 0;                     /* LSB */
        report->reserved = 0;                     /**/
        report->eventCode1 = 0;                   /* MSB */
        report->eventCode2 = 0;                   /* LSB */
    }

    if(1 == groupId)
    {
        /*Report all association groups in one message!*/
        report->groupingIdentifier = groupId;
        report->mode = 0;                         /**/
        report->profile1 = ASSOCIATION_GROUP_INFO_REPORT_PROFILE_GENERAL; /* MSB */
        report->profile2 = ASSOCIATION_GROUP_INFO_REPORT_PROFILE_CONTROL_KEY01; /* LSB */
        report->reserved = 0;                     /**/
        report->eventCode1 = 0;                   /* MSB */
        report->eventCode2 = 0;                   /* LSB */
    }
    else
    {
        report->groupingIdentifier = groupId;
        report->mode = 0;                         /**/
        report->profile1 = (myAgi.tableEndpoint[0].pTable[groupId-2].profile >> 8); /* MSB */
        report->profile2 = ( myAgi.tableEndpoint[0].pTable[groupId-2].profile & 0xff); /* LSB */
        report->reserved = 0;                     /**/
        report->eventCode1 = 0;                   /* MSB */
        report->eventCode2 = 0;                   /* LSB */

    }

}


/*========================   getApplGroupCommandListSize  ===========
**    Application specific Group Command List Size getter
**   return size of GroupCommandList
**
**   Side effects: none
**--------------------------------------------------------------------------*/
uint8_t  getApplGroupCommandListSize(uint8_t groupId)
{
    uint8_t size = 0;
    
    if(groupId >= myAgi.tableEndpoint[0].tableSize + 2)
    {
        /*Not legal groupId!*/
        return 0;
    }

    if (groupId == 1)
    {
        size =  myAgi.lifeLine.listSize * sizeof(CMD_CLASS_GRP);;
    }
    else
    {
        size =  sizeof(CMD_CLASS_GRP);
    }
    return size;
}


/*========================   setApplGroupCommandList  ======================
**   set Application specific Group Command List
**   return none
**
**   Side effects: none
**--------------------------------------------------------------------------*/
void setApplGroupCommandList(uint8_t* pGroupList,uint8_t groupId)
{
    if(groupId >= myAgi.tableEndpoint[0].tableSize + 2)
    {
        /*Not legal groupId!*/
        *pGroupList = 0x00;
        return;
    }

    if (groupId == 1)
    {
        memcpy(pGroupList, myAgi.lifeLine.pCmdGrpList, myAgi.lifeLine.listSize * sizeof(CMD_CLASS_GRP));
    }
    else
    {
        memcpy(pGroupList, &myAgi.tableEndpoint[0].pTable[groupId-2].cmdGrp, sizeof(CMD_CLASS_GRP));
    }
}