
/****************************************************************************/
/*                              INCLUDE FILES                               */
/****************************************************************************/
#include <time.h>       /* time */

#include "ctrl_learn.h"
#include "nvm.h"
#include "zw_api.h"
#include "inline_swap_endian.h"
#include "resource_directory.h"
#include "config.h"
#include "node_cache.h"
#include "ZW_Poll.h"



/****************************************************************************/
/*                      PRIVATE TYPES and DEFINITIONS                       */
/****************************************************************************/

#define ZW_LEARN_NODE_STATE_TIMEOUT 1000 /* 1 second - The base learn timer timeout value */

#define LEARN_MODE_CLASSIC_TIMEOUT  2    /* 2*1000= 2 second - Timeout count for classic innlusion */
#define LEARN_MODE_TIMEOUT          60   /* 60*1000= 60 second - Timeout count for classic innlusion */
#define LEARN_MODE_NWI_TIMEOUT      4    /* 4*1000= 4 second - Timeout count for network wide innlusion */

typedef enum 
{
    NW_NONE,
    NW_INCLUSION,
    NW_EXCLUSION,
} NW_ACTION;

typedef enum 
{
    STATE_LEARN_IDLE = 0,
    STATE_LEARN_STOP,
    STATE_LEARN_CLASSIC,
    STATE_LEARN_NWI,
    STATE_LEARN_IN_PROGRESS,
} STATE_LEARN;


static void HandleLearnState(void);
static void SecureLearnDone(int status);
void ZCB_TransmitNodeInfoComplete(uint8_t bTXStatus);


static uint8_t  bInclusionTimeoutCount;
static uint8_t  bIncReqCount;
static bool     nodeInfoTransmitDone = true;
static uint8_t  old_nodeid;

static timer_t learnStateHandle = 0;
NW_ACTION NW_Action = NW_NONE;
uint8_t bIncReqCountSave = 4;  /* 60 seconds = 4 times*( 4 seconds +rand(0,7)),  1 time for classic 29 times for NWI */
uint8_t learnState_learning_mode = STATE_LEARN_IDLE; /*Application can use this flag to check if learn mode is active*/
uint8_t lastIncludingNodeID;
controller_role_t   controller_role;


extern void ResetStateFSMToIdle();
extern int SetApplNodeInfo(void);
extern zwnet_p zwaveNetworkPolling;//polling device


static char *GetStateLearnStr(uint8_t st)
{
    char *stStr;
    switch (st)
    {
    case STATE_LEARN_IDLE:
        stStr = "ST Idle";
        break;
    case STATE_LEARN_STOP:
        stStr = "ST Stop";
        break;
    case STATE_LEARN_CLASSIC:
        stStr = "ST Classic";
        break;
    case STATE_LEARN_NWI:
        stStr = "ST NWI";
        break;
    case STATE_LEARN_IN_PROGRESS:
        stStr = "ST In progress";
        break;
    default:
        stStr = " ";
        break;
    }
    return stStr;
}

static char *GetNW_ActionStr(uint8_t nw)
{
    char *stStr;
    switch (nw)
    {
    case NW_NONE:
        stStr = "NW None";
        break;
    case NW_INCLUSION:
        stStr = "NW Inclusion";
        break;
    case NW_EXCLUSION:
        stStr = "NW Exclusion";
        break;
    default:
        stStr = " ";
        break;
    }
    return stStr;
}

static uint16_t  GetRandomWord(void)
{
   uint16_t rn;
   srand (time(NULL));
   rn=(rand()&0xFFFF);

   return rn;
}


/**
 * Return true is this is a clean network containing only the ZW controller and
 * if this is a new network compared to what we have en RAM
 */
static void
isCleanNetwork(bool *clean_network, bool *new_network)
{
    uint8_t ver, capabilities, len;
    uint8_t node_list[29];
    uint8_t c, v;
    uint32_t h;
    uint8_t n;
    uint8_t i;

    serialApiMemoryGetID((uint8_t *)&h, &n);
    swap_endian(&h);
    serialApiGetInitData(&ver, &capabilities, &len, node_list, &c, &v);

    *new_network = (h != homeId);

    mainlog(logDebug, "clean_network -> homeId: %08X", h);
    homeId = h;
    MyNodeId = n;

    node_list[(n - 1) >> 3] &= ~(1 << ((n - 1) & 0x7));
    for (i = 0; i < 29; i++)
    {
        if (node_list[i])
        {
            *clean_network = false;
            return;
        }
    }

    *clean_network = true;
}
/**
push notify to handler, add secure S0, S2.0, S2.1, S2.2
 */
void PushNotifyWhenAddSecureFinish(uint8_t scheme_mask)
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    NODEINFO includingNodeInfo;
    uint8_t controller_capability;

    serialApiGetNodeProtocolInfo(lastIncludingNodeID, &includingNodeInfo);
    pTxNotify.LearnModeNotify.including_node_info.nodeType.basic = includingNodeInfo.nodeType.basic;
    pTxNotify.LearnModeNotify.including_node_info.nodeType.generic = includingNodeInfo.nodeType.generic;
    pTxNotify.LearnModeNotify.including_node_info.nodeType.specific = includingNodeInfo.nodeType.specific;
    pTxNotify.LearnModeNotify.including_node_info.mode = GetCacheEntryNodeMode(lastIncludingNodeID);
    serialApiGetControllerCapabilities(&controller_capability);
    pTxNotify.LearnModeNotify.my_home_id = homeId;
    pTxNotify.LearnModeNotify.my_capability = controller_capability;


    if (scheme_mask & NODE_FLAG_KNOWN_BAD) //non sucure
    {
        pTxNotify.LearnModeNotify.status = APP_SET_LEARN_MODE_INCLUDED_NON_SECURE;
    }
    else
    {
        pTxNotify.LearnModeNotify.status = APP_SET_LEARN_MODE_INCLUDED_SECURE_DONE;
    }
    
    pTxNotify.LearnModeNotify.including_node_id = lastIncludingNodeID;
    pTxNotify.LearnModeNotify.including_secure_scheme = highest_scheme(GetCacheEntryFlag(lastIncludingNodeID));
    pTxNotify.LearnModeNotify.including_node_type = pTxNotify.LearnModeNotify.including_node_info.nodeType.generic;
    pTxNotify.LearnModeNotify.my_node_id = MyNodeId;
    pTxNotify.LearnModeNotify.my_node_flags = (GetCacheEntryFlag(MyNodeId));


    PushNotificationToHandler(LEARN_MODE_NOTIFY, (uint8_t *)&pTxNotify.LearnModeNotify, sizeof(LEARN_MODE_NOTIFY_T));

}

/**
Push notify add secure ready to zw handler
 */

void PushNotifyAddSecureReady(void)
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    mainlog(logDebug, "SetLearnModeSecurity_Compl: READY");
    pTxNotify.LearnModeNotify.status = APP_SET_LEARN_MODE_INCLUDED_SECURE_READY;
    pTxNotify.LearnModeNotify.including_node_id = lastIncludingNodeID;
    pTxNotify.LearnModeNotify.my_node_id = MyNodeId;

    PushNotificationToHandler(LEARN_MODE_NOTIFY, (uint8_t *)&pTxNotify.LearnModeNotify, sizeof(LEARN_MODE_NOTIFY_T));
}
void SecureLearnDone(int status)
{
    learnState_learning_mode = STATE_LEARN_IDLE;
    mainlog(logDebug, "SecureLearnDone -> status %d", status);
    security_init();
    uint8_t node_id_list[ZW_MAX_NODES];
    memset(node_id_list, 0, ZW_MAX_NODES);
    uint8_t node_list_size = 0, i;
    //if (status)/*learning mode, add my device*/
    if (1)
    {
        /*This is a new network, restart resource directory*/
        rd_exit();
        rd_destroy();
        /*push notify all node into network: nodeId, Type (basic), nodeMode*/
        rd_init(false, node_id_list, &node_list_size);
        SetCacheEntryFlagMasked(lastIncludingNodeID, status & 0xFF, NODE_FLAGS_SECURITY);
        SetCacheEntryFlagMasked(MyNodeId, status & 0xFF, NODE_FLAGS_SECURITY);
    }else/*learning mode, shift role*/
    {
        rd_init(false, node_id_list, &node_list_size);
        SetCacheEntryFlagMasked(MyNodeId, sec2_get_my_node_flags(), 0xFF);

    }
    mainlog(logDebug, "my mode flags %d", GetCacheEntryFlag(MyNodeId));
    PushNotifyWhenAddSecureFinish(status & 0xFF);

    /*not push notify of Inclusion Controller*/
    NOTIFY_TX_BUFFER_T pTxNotify;

    pTxNotify.UpdateNetworkNotify.no_node = node_list_size;
    for (i = 0; i < node_list_size; i++)
    {   NODE_TYPE nodeType;
        nodeType = GetCacheEntryNodeType(node_id_list[i]);
        //push notify update network, not push Id of InclusionController 
        pTxNotify.UpdateNetworkNotify.TranferNetworkNotify[i].node_id = node_id_list[i];
        pTxNotify.UpdateNetworkNotify.TranferNetworkNotify[i].basic_type = nodeType.basic;
        pTxNotify.UpdateNetworkNotify.TranferNetworkNotify[i].generic_type = nodeType.generic;        
        pTxNotify.UpdateNetworkNotify.TranferNetworkNotify[i].specific_type = nodeType.specific;
        pTxNotify.UpdateNetworkNotify.TranferNetworkNotify[i].node_mode = 
                    GetCacheEntryNodeMode(node_id_list[i]);
    }
    PushNotificationToHandler(UPDATE_NETWORK_NOTIFY, (uint8_t *)&pTxNotify.UpdateNetworkNotify, sizeof(UPDATE_NETWORK_NOTIFY_T));
    /**< Learning mode done, cancel polling device)*/
    zwpoll_exit(zwaveNetworkPolling->poll_ctx);
    zwpoll_shutdown(zwaveNetworkPolling->poll_ctx);

}

void /*RET Nothing */
ZCB_LearnModeCompleted(uint8_t bStatus, uint8_t bSource, uint8_t *pCmd, uint8_t bLen)
{
    //----------------New code-------------------------------
    bool clean_network, new_network;
    NOTIFY_TX_BUFFER_T pTxNotify;
    mainlog(logDebug, "ZW_SetLearnMode callback status %02X, source %02X, len %02X", bStatus, bSource, bLen);
    switch (bStatus)
    {
    case LEARN_MODE_STARTED:
        mainlog(logDebug, "LearnMode -> start");
        learnState_learning_mode = STATE_LEARN_IN_PROGRESS;

#if 0 /*use s2_inclusion timer TB0 = 60s */
        if (learnStateHandle != 0)
        {
            timerCancel(&learnStateHandle);
            learnStateHandle = 0;
        }
        StartLearningModeTimer();

#endif

        pTxNotify.LearnModeNotify.status = APP_SET_LEARN_MODE_START;
        pTxNotify.LearnModeNotify.including_node_id = bSource;
        pTxNotify.LearnModeNotify.my_node_id = 0;
        lastIncludingNodeID = bSource;
        PushNotificationToHandler(LEARN_MODE_NOTIFY, (uint8_t *)&pTxNotify.LearnModeNotify, sizeof(LEARN_MODE_NOTIFY_T));

        /*inclusion s0, receive inclusion s0 --> abort S2 */
        security_learn_begin(SecureLearnDone); 
        /*inclusion S2, receive inclusion s2 --> abort S0 */
        sec2_start_learn_mode(lastIncludingNodeID, SecureLearnDone);

        //rd_probe_lock(true);
        old_nodeid = MyNodeId;
        MyNodeId = 0;

        break;

    case LEARN_MODE_DONE:
    {
        /*There are three outcomes of learn mode
        * 1) Controller has been included into a new network
        * 2) Controller has been excluded from a network
        * 3) Controller replication
        * */
        mainlog(logDebug, "LearnMode -> done");

#if 0 /*use s2_inclusion timer TB0 = 60s */

        if (learnStateHandle != 0)
        {
            timerCancel(&learnStateHandle);
            learnStateHandle = 0;
        }
#endif
        isCleanNetwork(&clean_network, &new_network);

        if (clean_network || (bSource == 0)) //Controller has been excluded from a network
        {
            uint8_t controller_capability;

            /* Set to Defaut for controller */
            MyNodeId = 1;
            security_set_default();

            rd_exit();
            rd_destroy();

            AssociationClearAll();

            s2_nvm_init(true);

            SetPreInclusionNIF(NO_SCHEME);
            SetApplNodeInfo();
            MySystemSUCId = serialApiGetSUCNodeID();
            mainlog(logDebug, "suc id %02X", MySystemSUCId);

            SetCacheEntryFlagMasked(MyNodeId, sec2_get_my_node_flags(), 0xFF);
            mainlog(logDebug, "my mode flags %d", GetCacheEntryFlag(MyNodeId));
            /*reset state remove/add, interview to idle*/
            ResetStateFSMToIdle();

            /*notify to higher layer*/
            serialApiGetControllerCapabilities(&controller_capability);
            if (controller_capability & CONTROLLER_CAPABILITIES_IS_SECONDARY)
            {
                /* OK, we are actually not really SUC at this point but later on ApplicationInitSW will
                  make us SUC */
                controller_role = SUC;
            }
            pTxNotify.LearnModeNotify.my_home_id = homeId;
            pTxNotify.LearnModeNotify.my_capability = controller_capability;
            pTxNotify.LearnModeNotify.status = APP_SET_LEARN_MODE_REMOVED;
            pTxNotify.LearnModeNotify.including_node_id = bSource;
            pTxNotify.LearnModeNotify.my_node_id = MyNodeId;
            pTxNotify.LearnModeNotify.my_node_flags = (GetCacheEntryFlag(MyNodeId));
            PushNotificationToHandler(LEARN_MODE_NOTIFY, (uint8_t *)&pTxNotify.LearnModeNotify, sizeof(LEARN_MODE_NOTIFY_T));

        }
        else if (new_network)
        {
            rd_mark_node_deleted(old_nodeid);
            MyNodeId = bSource;
            mainlog(logDebug, "MyNodeId = %02X\n", MyNodeId);
            mainlog(logDebug, "new homeId = %08X\n", homeId);

            PushNotifyAddSecureReady();

            /* Make temporary NIF, used for inclusion */
            SetPreInclusionNIF(SECURITY_SCHEME_0);

            sec2_refresh_homeid();
            sec2_set_inclusion_peer(lastIncludingNodeID, MyNodeId); 
                                                                         
        }
        else
        {
            serialApiMemoryGetID((uint8_t *)&homeId, &MyNodeId);
            swap_endian(&homeId);
            
            MyNodeId = bSource;
            mainlog(logDebug, "My Old NodeId = %02X\n", old_nodeid);
            mainlog(logDebug, "MyNodeId = %02X\n", MyNodeId);
            mainlog(logDebug, "new homeId = %08X\n", homeId);

            /*This was a controller replication, ie. this is not a new network. */
            mainlog(logDebug, "This was a controller replication");

            if (old_nodeid == MyNodeId)
            {
                SecureLearnDone(0);
            }
            else
            {
                rd_mark_node_deleted(old_nodeid);
                /*re-add to secure when controller replication mode assigns new id */
                sec2_refresh_homeid();
                sec2_set_inclusion_peer(lastIncludingNodeID, MyNodeId); 
            }
        }
    }
    break;

    case LEARN_MODE_FAILED:
        mainlog(logDebug, "ZW_SetLearnMode LEARN_MODE_FAILED");
        learnState_learning_mode = STATE_LEARN_STOP;
        HandleLearnState();
        pTxNotify.LearnModeNotify.status = APP_SET_LEARN_MODE_FAILED;
        PushNotificationToHandler(LEARN_MODE_NOTIFY, (uint8_t *)&pTxNotify.LearnModeNotify, sizeof(LEARN_MODE_NOTIFY_T));
        break;

    case LEARN_MODE_DELETED:
        mainlog(logDebug, "ZW_SetLearnMode LEARN_MODE_DELETED");
        learnState_learning_mode = STATE_LEARN_STOP;
        HandleLearnState();

        break;

    default:
        mainlog(logDebug, "ZW_SetLearnMode Unknown status");
        serialApiSetLearnMode(false, ZCB_LearnModeCompleted);
        break;
    }
}


/*============================   EndLearnNodeState   ========================
**    Function description
**      Timeout function that stop a learn mode
**      if we are in classic mode then switch to NWI else stop learn process
**      Should not be called directly.
**    Side effects:
**  NWI: Network Wide Inclusion
**--------------------------------------------------------------------------*/
void ZCB_LearnNodeStateTimeout(void *data)
{
    mainlog(logDebug, " learnState: %s \n ", GetStateLearnStr(learnState_learning_mode));
    mainlog(logDebug, " NW_Action: %s \n ", GetNW_ActionStr(NW_Action));

    if (learnState_learning_mode == STATE_LEARN_STOP)
    {
        if (learnStateHandle != 0)
        {
            timerCancel(&learnStateHandle);
        }
        return;
    }

    if (!(--bInclusionTimeoutCount))
    {
        if (learnStateHandle != 0)
        {
            timerCancel(&learnStateHandle);
        }
        if (learnState_learning_mode == STATE_LEARN_CLASSIC)
        {
            serialApiSetLearnMode(ZW_SET_LEARN_MODE_DISABLE, NULL);
            learnState_learning_mode = STATE_LEARN_NWI;
            HandleLearnState();
        }
        else if (learnState_learning_mode == STATE_LEARN_NWI)
        {
            if (bIncReqCount)
            {
                if (NW_Action == NW_INCLUSION)
                {
                    serialApiExploreRequestInclusion();
                }
                else if (NW_Action == NW_EXCLUSION)
                {
                    serialApiExploreRequestExclusion();
                }

                bIncReqCount--;

                /* Start timer sending  out a explore inclusion request after 4 + random sec */
                bInclusionTimeoutCount =  LEARN_MODE_NWI_TIMEOUT + (GetRandomWord() & 0x7);
                mainlog(logDebug, "bInclusionTimeoutCount %d\n", bInclusionTimeoutCount);
                timerStart(&learnStateHandle, ZCB_LearnNodeStateTimeout, NULL, ZW_LEARN_NODE_STATE_TIMEOUT, TIMER_FOREVER);
            }
            else
            {
                learnState_learning_mode = STATE_LEARN_STOP;
                HandleLearnState();
                /* push learn mode timeout */
                NOTIFY_TX_BUFFER_T pTxNotify;
                pTxNotify.LearnModeNotify.status = APP_SET_LEARN_MODE_TIMEOUT;
                PushNotificationToHandler(LEARN_MODE_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(LEARN_MODE_NOTIFY_T));
                
            }
        }
    }
}

/*========================   TransmitNodeInfoComplete   ======================
**    Function description
**      Callbackfunction called when the nodeinformation frame has
**      been transmitted. This function ensures that the Transmit Queue is not
**      flooded.
**    Side effects:
**
**--------------------------------------------------------------------------*/
void ZCB_TransmitNodeInfoComplete(uint8_t bTXStatus)
{
    nodeInfoTransmitDone = true;
}

/****************************************************************************/
/*                           EXPORTED FUNCTIONS                             */
/****************************************************************************/
/*============================   StartLearnModeNow   ======================
**    Function description
**      Call this function from the application whenever learnmode
**      should be enabled / Disabled.
**      This function do the following:
**        If the node is not included in network
**          Set the Slave in classic Learnmode
**          Starts a two seconds timeout after which we switch to NWI mode
**          Broadcast the NODEINFORMATION frame once when called.
**          If classic learn mode timeout start NWI learn mode
**          if bInclusionReqCount > 1 send explorer inclusion frame
**            start a 4 + random time timer
**          if bInclusionReqCount == 1 send explorer inclusion request frame and wait 4 seconds
**          when timer timeout and bInclusionReqCount == 0 disable NWI mode and call LearnCompleted
**        if node is not included in a network
**          Set the Slave in classic Learnmode
**          Starts a two seconds timeout after which we stop learn mode
**
**       LearnCompleted will be also called after the end of learn process or a timeout
**        if LearnComplete called due timeout out the bNodeID parameter would be 0xFF
**    Side effects:
**
**--------------------------------------------------------------------------*/

int StartLearnModeNow(LEARN_MODE_ACTION bMode /* The mode of the learn process
                                 LEARN_MODE_INCLUSION   Enable the learn mode to do an inclusion
                                 LEARN_MODE_EXCLUSION   Enable the learn mode to do an exclusion
                                 LEARN_MODE_EXCLUSION_NWE Enable the learn mode to do an network wide exclusion
                                 LEARN_MODE_DISABLE      Disable learn mode
                                */)
{

    if (LEARN_MODE_DISABLE != bMode)
    {
        /* Stay awake until inclusion (and security and wakeup configuraion) is complete */
        NW_Action = NW_NONE;
        
        if (learnState_learning_mode != STATE_LEARN_IDLE) 
        {
            learnState_learning_mode = STATE_LEARN_STOP;
            HandleLearnState();
        }
        

        if (bMode == LEARN_MODE_INCLUSION)
        {
            bIncReqCount = bIncReqCountSave;
            NW_Action = NW_INCLUSION;
        }
        else if (LEARN_MODE_EXCLUSION_NWE == bMode)
        {
            bIncReqCount = bIncReqCountSave;
            NW_Action = NW_EXCLUSION;
        }
        else
        {
            bIncReqCount = 0;
        }
        learnState_learning_mode = STATE_LEARN_CLASSIC;
        HandleLearnState();
    }
    else
    {
        NW_Action = NW_NONE;
        learnState_learning_mode = STATE_LEARN_STOP;
        sec2_abort_join();
        HandleLearnState();
    }
    return 0;
}

/*============================   HandleLearnState   ======================
**    Function description
**      Call this function from the application whenever learnmode
**      should be enabled.
**      This function do the following:
**        - Set the Slave in Learnmode
**        - Starts a one second timeout after which learn mode is disabled
**        - Broadcast the NODEINFORMATION frame once when called.
**      LearnCompleted will be called if a controller performs an assignID.
**    Side effects:
**
**--------------------------------------------------------------------------*/
static void
HandleLearnState(void)
{
    if (learnState_learning_mode == STATE_LEARN_CLASSIC)
    {
        //serialApiSetLearnMode(ZW_SET_LEARN_MODE_CLASSIC, ZCB_LearnModeCompleted);
        serialApiSetLearnMode(ZW_SET_LEARN_MODE_NWI, ZCB_LearnModeCompleted);
        if (nodeInfoTransmitDone)
        {
            SetPreInclusionNIF(NO_SCHEME);

            /* controller not need to send NIF in learn mode */
            
            if (serialApiSendNodeInformation(NODE_BROADCAST, ZWAVE_PLUS_TX_OPTIONS, ZCB_TransmitNodeInfoComplete))
            {
                nodeInfoTransmitDone = false;
            }
            
            
        }
        /*Disable Learn mode after 2 sec.*/
        bInclusionTimeoutCount = LEARN_MODE_CLASSIC_TIMEOUT;
        timerStart(&learnStateHandle, ZCB_LearnNodeStateTimeout, NULL, ZW_LEARN_NODE_STATE_TIMEOUT, TIMER_FOREVER);
    }
    else if (learnState_learning_mode == STATE_LEARN_NWI)
    {
        if (NW_EXCLUSION == NW_Action)
        {
            serialApiSetLearnMode(ZW_SET_LEARN_MODE_NWE, ZCB_LearnModeCompleted);
            bInclusionTimeoutCount = 1;
            ZCB_LearnNodeStateTimeout(NULL);
        }
        else
        {
            serialApiSetLearnMode(ZW_SET_LEARN_MODE_NWI, ZCB_LearnModeCompleted);
            bInclusionTimeoutCount = 1;
            ZCB_LearnNodeStateTimeout(NULL);
        }
    }
    else if (learnState_learning_mode == STATE_LEARN_STOP)
    {
        serialApiSetLearnMode(ZW_SET_LEARN_MODE_DISABLE, NULL);
        if (learnStateHandle != 0)
        {
            timerCancel(&learnStateHandle);
            learnStateHandle = 0;
        }

        learnState_learning_mode = STATE_LEARN_IDLE;
    }
    mainlog(logDebug, "HandleLearnState learnState %s", GetStateLearnStr(learnState_learning_mode));
    mainlog(logDebug, "bInclusionTimeoutCount %d", bInclusionTimeoutCount);
}

/*===========================   GetLearnModeState   =======================
**    Function description
**      Check if the learning mode is active
**
** Side effects: None
**--------------------------------------------------------------------------------*/
bool /*RET TRUE if the learning mode is active, else FALSE*/
    GetLearnModeState(void)
{
    return (learnState_learning_mode != STATE_LEARN_IDLE);
}

/*===========================   SetInclusionRequestCount   =======================
**    Function description
**      Set the number of timer we send the explorer inclusion request frame
**
**    Side effects: None
**
**--------------------------------------------------------------------------------*/
void SetInclusionRequestCount(uint8_t bInclusionRequestCount)
{
    bIncReqCountSave = bInclusionRequestCount;
}

/*=================== PushNotificationToHandler ================================
**    Function description
**      Push Notification to Phone App 
**
**    Side effects: None
**/

/*======================== SetLearnModeSecurity_Compl====================
*   push notify to app when comple security mode 
*
*/
void SetLearnModeSecurity_Compl_PushNotify(uint8_t bStatus, uint8_t bSource, uint8_t *pCmd, uint8_t bLen)
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    switch (bStatus)
    {
    case INCLUDED_SECURE_NODE_STATUS_LEARN_READY:
        mainlog(logDebug, "SetLearnModeSecurity_Compl: READY");
        pTxNotify.LearnModeNotify.status = APP_SET_LEARN_MODE_INCLUDED_SECURE_READY;
        pTxNotify.LearnModeNotify.including_node_id = lastIncludingNodeID;
        pTxNotify.LearnModeNotify.my_node_id = MyNodeId;

        PushNotificationToHandler(LEARN_MODE_NOTIFY, (uint8_t *)&pTxNotify.LearnModeNotify, sizeof(LEARN_MODE_NOTIFY_T));

        break;
    case INCLUDED_SECURE_NODE_STATUS_LEARN_FAILED:
        mainlog(logDebug, "SetLearnModeSecurity_Compl: FAILED");
        pTxNotify.LearnModeNotify.status = APP_SET_LEARN_MODE_INCLUDED_SECURE_FAILED;
        pTxNotify.LearnModeNotify.including_node_id = lastIncludingNodeID;
        pTxNotify.LearnModeNotify.my_node_id = MyNodeId;
        lastIncludingNodeID = 0;

        PushNotificationToHandler(LEARN_MODE_NOTIFY, (uint8_t *)&pTxNotify.LearnModeNotify, sizeof(LEARN_MODE_NOTIFY_T));
        break;
    case INCLUDED_SECURE_NODE_STATUS_LEARN_DONE:
        mainlog(logDebug, "SetLearnModeSecurity_Compl: DONE");
        pTxNotify.LearnModeNotify.status = APP_SET_LEARN_MODE_INCLUDED_SECURE_DONE;
        pTxNotify.LearnModeNotify.including_node_id = lastIncludingNodeID;
        pTxNotify.LearnModeNotify.my_node_id = MyNodeId;
        lastIncludingNodeID = 0;

        PushNotificationToHandler(LEARN_MODE_NOTIFY, (uint8_t *)&pTxNotify.LearnModeNotify, sizeof(LEARN_MODE_NOTIFY_T));
        break;
    }
}

void StartLearningModeTimer(void) /* IN Timeout in seconds */
{
    if (learnStateHandle != 0)
    {
        timerCancel(&learnStateHandle);
    }
    timerStart(&learnStateHandle, ZCB_learningModeTimeout, NULL, 60000, TIMER_ONETIME);
}

void ZCB_learningModeTimeout(void *data)
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    mainlog(logDebug, "ZCB_learningModeTimeout");
    learnState_learning_mode = STATE_LEARN_STOP;
    HandleLearnState();

    if (learnStateHandle != 0)
    {
        timerCancel(&learnStateHandle);
        learnStateHandle = 0;
    }

    pTxNotify.LearnModeNotify.status = APP_SET_LEARN_MODE_TIMEOUT;
    pTxNotify.LearnModeNotify.including_node_id = 0;
    pTxNotify.LearnModeNotify.my_node_id = 0;

    PushNotificationToHandler(LEARN_MODE_NOTIFY, (uint8_t *)&pTxNotify.LearnModeNotify, sizeof(LEARN_MODE_NOTIFY_T));
}
