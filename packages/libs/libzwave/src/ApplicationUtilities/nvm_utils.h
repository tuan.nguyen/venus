#ifndef _NVM_UTILS_H_
#define _NVM_UTILS_H_

void BackupNVM(char *path_of_file);
void RestoreNVM(char *path_of_file);

#endif 