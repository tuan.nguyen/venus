#ifndef _PLATFORM_CC_H_
#define _PLATFORM_CC_H_


#define INLINE

#ifndef INLINE
# if __GNUC__ && !__GNUC_STDC_INLINE__
#  define INLINE extern inline
# else
#  define INLINE inline
# endif
#else
# define INLINE 
#endif


#define CC_CONF_REGISTER_ARGS          1
#define CC_CONF_FUNCTION_POINTER_ARGS  1
#define CC_CONF_FASTCALL
#define CC_CONF_VA_ARGS                1
#define CC_CONF_ALIGN_PACK             1

/**
 * Configure if the C compiler supports the "reentrant" keyword for
 * functions.
 */
#if CC_CONF_REENTRANT_ARGS
#define CC_REENTRANT_ARG reentrant
#else /* CC_CONF_REGISTER_ARGS */
#define CC_REENTRANT_ARG
#endif /* CC_CONF_REGISTER_ARGS */


#if CC_CONF_ALIGN_PACK
#define CC_ALIGN_PACK __attribute__((__packed__))
#else /* CC_CONF_REGISTER_ARGS */
#define CC_ALIGN_PACK
#endif /* CC_CONF_REGISTER_ARGS */


/**
 * Configure if the C compiler supports the "register" keyword for
 * function arguments.
 */
#if CC_CONF_REGISTER_ARGS
#define CC_REGISTER_ARG register
#else /* CC_CONF_REGISTER_ARGS */
#define CC_REGISTER_ARG
#endif /* CC_CONF_REGISTER_ARGS */

/**
 * Configure if the C compiler supports the arguments for function
 * pointers.
 */
#if CC_CONF_FUNCTION_POINTER_ARGS
#define CC_FUNCTION_POINTER_ARGS 1
#else /* CC_CONF_FUNCTION_POINTER_ARGS */
#define CC_FUNCTION_POINTER_ARGS 0
#endif /* CC_CONF_FUNCTION_POINTER_ARGS */

/**
 * Configure if the C compiler supports fastcall function
 * declarations.
 */
#ifdef CC_CONF_FASTCALL
#define CC_FASTCALL CC_CONF_FASTCALL
#else /* CC_CONF_FASTCALL */
#define CC_FASTCALL
#endif /* CC_CONF_FASTCALL */

/**
 * Configure if the C compiler have problems with const function pointers
 */
#ifdef CC_CONF_CONST_FUNCTION_BUG
#define CC_CONST_FUNCTION
#else /* CC_CONF_FASTCALL */
#define CC_CONST_FUNCTION const
#endif /* CC_CONF_FASTCALL */

/**
 * Configure work-around for unsigned char bugs with sdcc.
 */
#if CC_CONF_UNSIGNED_CHAR_BUGS
#define CC_UNSIGNED_CHAR_BUGS 1
#else /* CC_CONF_UNSIGNED_CHAR_BUGS */
#define CC_UNSIGNED_CHAR_BUGS 0
#endif /* CC_CONF_UNSIGNED_CHAR_BUGS */

/**
 * Configure if C compiler supports double hash marks in C macros.
 */
#if CC_CONF_DOUBLE_HASH
#define CC_DOUBLE_HASH 1
#else /* CC_CONF_DOUBLE_HASH */
#define CC_DOUBLE_HASH 0
#endif /* CC_CONF_DOUBLE_HASH */

#ifdef CC_CONF_INLINE
#define CC_INLINE CC_CONF_INLINE
#else /* CC_CONF_INLINE */
#define CC_INLINE
#endif /* CC_CONF_INLINE */

/**
 * Configure if the C compiler supports the assignment of struct value.
 */
#ifdef CC_CONF_ASSIGN_AGGREGATE
#define CC_ASSIGN_AGGREGATE(dest, src)  CC_CONF_ASSIGN_AGGREGATE(dest, src)
#else /* CC_CONF_ASSIGN_AGGREGATE */
#define CC_ASSIGN_AGGREGATE(dest, src)  *dest = *src
#endif /* CC_CONF_ASSIGN_AGGREGATE */

#if CC_CONF_NO_VA_ARGS
#define CC_NO_VA_ARGS CC_CONF_VA_ARGS
#endif

#ifndef NULL
#define NULL 0
#endif /* NULL */

#define CC_CONCAT2(s1, s2) s1##s2
/**
 * A C preprocessing macro for concatenating to
 * strings.
 *
 * We need use two macros (CC_CONCAT and CC_CONCAT2) in order to allow
 * concatenation of two #defined macros.
 */
#define CC_CONCAT(s1, s2) CC_CONCAT2(s1, s2)

#ifdef __ASIX_C51__
#define REENTRANT reentrant
#else
#define REENTRANT
#endif


#endif