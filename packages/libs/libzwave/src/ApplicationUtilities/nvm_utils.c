#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"
#include "config.h"
#include "utils.h"
#include "serialAPI.h"
#include "nvm_utils.h"

/*========================================================================================================
================================ Read data and length of Backup Nvm file =================================
=================================== Return 1: if read file successfully ==================================*/
bool ReadBackupNvmFile(char *path_of_file, uint8_t *buf, uint16_t *length_data)
{
    //open file .txt
    FILE *fRestoreNVM = fopen(path_of_file, "r");
    if (NULL == fRestoreNVM)
    {
        mainlog(logDebug,"Error open file!\n");
        return 0;
    }
    else
    {
        uint16_t number_char = 0;
        char strOneByte[2];
        while (fscanf(fRestoreNVM, "%s", strOneByte) != EOF)
        {
            buf[number_char++] = (int)strtol(strOneByte, NULL, 16);
        }

        *length_data = number_char;
        mainlog(logDebug, "Length Data Into Backup NVM File: 0x%02X ", *length_data);
        fclose(fRestoreNVM);
        mainlog(logDebug, "Open file: %s Success \n", path_of_file);
        return 1;
    }
}

/*========================================================================================================
===================================== Write data to Backup Nvm file ======================================
================================ Return 1: if write data to file successfully ============================*/
bool WriteBackupNvmFile(char *path_of_file, uint8_t *buf, uint16_t length_data)
{
    //open file .txt
    FILE *fbackupNVM = fopen(path_of_file, "w");
    uint16_t index;
    if (NULL == fbackupNVM)
    {
        mainlog(logDebug,"Error opening file!\n");
        return 0;
    }
    else //save file
    {
        for (index = 0; index < length_data; index++)
        {
            fprintf(fbackupNVM, "%02X ", buf[index]);
        }

        fclose(fbackupNVM);
        mainlog(logDebug,"Save file: %s as back up file is success", path_of_file);
        return 1;
    }
}
/*========================================================================================================
============================ Backup data into Non-Volatile Memory of Venus ===============================
======================================= Path Backup file: ================================================
==========================================================================================================*/
void BackupNVM(char *path_of_file)
{
    uint8_t packetSize = 40;                 //size of data in one message
    uint8_t bufferReceiveDataBackup[60];     //get data respone from Controller
    uint16_t offsetMemmoryBackupRestore = 0; //Begin address of read/write data from/to NVM

    mainlog(logDebug,"NVM Backup started...\n");
    
    /*  operation:  0: Open,    1: Read,     2: Write,   3: Close    */
    //open NVM
    serialApiBackupRestoreNvm(0, offsetMemmoryBackupRestore, bufferReceiveDataBackup, packetSize);
    /*
    bufferReceiveDataBackup[0]: Status {0, 1, FF}
    bufferReceiveDataBackup[1]: number byte data
    bufferReceiveDataBackup[2]: LSB, last address of NVM
    bufferReceiveDataBackup[3]: MSB
    bufferReceiveDataBackup[4]...:data
    */
    uint16_t lengthDataIntoNVM = ((uint16_t)bufferReceiveDataBackup[2] * 256) + bufferReceiveDataBackup[3] + 1;
    mainlog(logUI, "lengthDataIntoNVM: 0x%02X ", lengthDataIntoNVM);

    bool done = false; //check when backup process get all data. ---> done = true;
    offsetMemmoryBackupRestore = 0;
    uint8_t bufferSaveDataBackupRestore[20000]; //Buffer save data of read data from NVM/backup file process

    //read NVM
    while (!done)
    {
        serialApiBackupRestoreNvm(1, offsetMemmoryBackupRestore, bufferReceiveDataBackup, packetSize);
        uint16_t lengthDataOneReceive = bufferReceiveDataBackup[1];
        memcpy(&bufferSaveDataBackupRestore[offsetMemmoryBackupRestore], &bufferReceiveDataBackup[4], lengthDataOneReceive);
        mainlog(logUI, "Reading begin from address: 0x%02X/0x%02X, read: %02d byte", offsetMemmoryBackupRestore, lengthDataIntoNVM, packetSize);
        if (lengthDataIntoNVM > 0)
        {
            offsetMemmoryBackupRestore += packetSize;
            if (0xFF == bufferReceiveDataBackup[0])
            {
                done = true;
                //close NVM
                serialApiBackupRestoreNvm(3, offsetMemmoryBackupRestore, bufferReceiveDataBackup, packetSize);
                mainlog(logDebug,"Read NVM for Backup OK \n");
                //save file .txt
                WriteBackupNvmFile(path_of_file, bufferSaveDataBackupRestore, lengthDataIntoNVM);
            }
        }
        else //no data respone
        {
            mainlog(logDebug,"NVM Backup operation failed \n");
            done = true;
        }
    }
}

/*========================================================================================================
============================ Restore data into Non-Volatile Memory of Venus ==============================
======================================= Path Restore file: ===============================================
==========================================================================================================*/
void RestoreNVM(char *path_of_file)
{
    uint8_t packetSize = 40;                 //size of data in one message
    uint8_t bufferReceiveDataBackup[60];     //get data respone from Controller
    uint16_t offsetMemmoryBackupRestore = 0; //Begin address of read/write data from/to NVM
    uint16_t lengthDataIntoFileNVM = 0;
    uint8_t bufferSaveDataBackupRestore[20000]; //Buffer save data of read data from NVM/backup file process

    //open file .txt
    if (ReadBackupNvmFile(path_of_file, bufferSaveDataBackupRestore, &lengthDataIntoFileNVM))
    {
        mainlog(logDebug,"NVM Restore started...\n");
        /*  operation:  0: Open,    1: Read,     2: Write,   3: Close    */
        //open NVM
        serialApiBackupRestoreNvm(0, offsetMemmoryBackupRestore, bufferReceiveDataBackup, packetSize);
        if (0 == bufferReceiveDataBackup[0])
        {
            //write NVM
            while ((offsetMemmoryBackupRestore + packetSize) < lengthDataIntoFileNVM)
            {
                serialApiBackupRestoreNvm(2, offsetMemmoryBackupRestore,
                                          &bufferSaveDataBackupRestore[offsetMemmoryBackupRestore], packetSize);
                mainlog(logUI, "Writing data begin from address: 0x%02X/0x%02X, write %02d byte ",
                        offsetMemmoryBackupRestore, lengthDataIntoFileNVM, packetSize);
                if (0 >= bufferSaveDataBackupRestore[1]) //no data respone
                {
                    printf("No data respone \n");
                    break;
                }
                offsetMemmoryBackupRestore += packetSize;
            }

            serialApiBackupRestoreNvm(2, offsetMemmoryBackupRestore,
                                      &bufferSaveDataBackupRestore[offsetMemmoryBackupRestore],
                                      lengthDataIntoFileNVM - offsetMemmoryBackupRestore);
            mainlog(logUI, "Writing data begin from address: 0x%02X/0x%02X, write %02d byte ",
                    offsetMemmoryBackupRestore, lengthDataIntoFileNVM, lengthDataIntoFileNVM - offsetMemmoryBackupRestore);
            if (0 >= bufferSaveDataBackupRestore[offsetMemmoryBackupRestore + 1]) //no data respone
            {
                mainlog(logDebug,"No data respone \n");
            }
            if (0xFF == bufferSaveDataBackupRestore[offsetMemmoryBackupRestore])
            {
                //close NVM
                serialApiBackupRestoreNvm(3, offsetMemmoryBackupRestore, bufferReceiveDataBackup, packetSize);
                mainlog(logDebug,"Write data to NVM for Restore OK \n");
            }
        }
        else
        {
            mainlog(logDebug,"NVM Restore operation failed \n");
        }
    }
}