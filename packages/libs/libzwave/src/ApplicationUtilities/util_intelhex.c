#include "util_intelhex.h"
#include "util_crc.h"
#include "utils.h"
uint32_t ex_addr;

int intelHex_parseHexLine(char *theline, int bytes[], uint32_t *addr, int *num, int *code)
{
    int sum, len, cksum;
    char *ptr;
    *num = 0;
    if (theline[0] != ':')
        return 0;
    if (strlen(theline) < 11)
        return 0;
    ptr = theline + 1;
    if (!sscanf(ptr, "%02x", &len))
        return 0;
    ptr += 2;
    if (strlen(theline) < (11 + (len * 2)))
        return 0;
    if (!sscanf(ptr, "%04x", addr))
        return 0;
    ptr += 4;
    if (!sscanf(ptr, "%02x", code))
        return 0;
    ptr += 2;
    if (*code == 0x04)
    {
        if (!sscanf(ptr, "%04x", &ex_addr))
            return 0;
        ptr += 4;
        sum = (len & 0xFF) + ((*addr >> 8) & 0xFF) + (*addr & 0xFF) + (*code & 0xFF) + ((ex_addr >> 8) & 0xFF) + (ex_addr & 0xFF);
        if (!sscanf(ptr, "%02x", &cksum))
            return 0;
        if (((sum & 0xFF) + (cksum & 0xFF)) & 0xFF)
            return 0;
        ex_addr <<= 16;
        return 1;
    }
    if (*code == 0x02)
    {
        if (!sscanf(ptr, "%04x", &ex_addr))
            return 0;
        ptr += 4;
        sum = (len & 0xFF) + ((*addr >> 8) & 0xFF) + (*addr & 0xFF) + (*code & 0xFF) + ((ex_addr >> 8) & 0xFF) + (ex_addr & 0xFF);
        if (!sscanf(ptr, "%02x", &cksum))
            return 0;
        if (((sum & 0xFF) + (cksum & 0xFF)) & 0xFF)
            return 0;
        ex_addr <<= 4;
        return 1;
    }

    sum = (len & 0xFF) + ((*addr >> 8) & 0xFF) + (*addr & 0xFF) + (*code & 0xFF);
    *addr += ex_addr;

    while (*num != len)
    {
        if (!sscanf(ptr, "%02x", &bytes[*num]))
            return 0;
        ptr += 2;
        sum += bytes[*num] & 0xFF;
        (*num)++;
        if (*num >= 256)
            return 0;
    }
    if (!sscanf(ptr, "%02x", &cksum))
        return 0;
    if (((sum & 0xFF) + (cksum & 0xFF)) & 0xFF)
        return 0;
    return 1;
}

int intelHex_loadfile(char *filename, uint8_t *flashMem, uint32_t *length_file_ota)
{
    char line[1000];
    FILE *fin;
    int n, status, bytes[256];
    int i, total = 0, lineno = 1;
    uint32_t addr;
    uint32_t minaddr = 0xFFFFFFFF;
    uint32_t maxaddr = 0;
    *length_file_ota = 0;

    memset(flashMem, 0xff, FLASH_MEM_LENGTH);
    if (strlen(filename) == 0)
    {
        mainlog(logDebug, "Can't load a file without the filename");
        return -1;
    }

    fin = fopen(filename, "r");
    if (fin == NULL)
    {
        mainlog(logDebug, "Can't open file '%s' for reading.", filename);
        return -1;
    }
    while (!feof(fin) && !ferror(fin))
    {
        line[0] = '\0';
        fgets(line, 1000, fin);
        if (line[strlen(line) - 1] == '\n')
            line[strlen(line) - 1] = '\0';

        if (line[strlen(line) - 1] == '\r')
            line[strlen(line) - 1] = '\0';

        if (intelHex_parseHexLine(line, bytes, &addr, &n, &status))
        {
            if (status == 0)
            {
                for (i = 0; i < n; i++)
                {
                    flashMem[addr + i] = bytes[i] & 0xff;
                    total++;
                    if (addr < minaddr)
                        minaddr = addr;
                    if (addr > maxaddr)
                        maxaddr = addr;
                    *length_file_ota = maxaddr + n;
                }
            }
            if (status == 1)
            {
                fclose(fin);
                mainlog(logDebug, "Load %d bytes between %04x to %04x ", total, minaddr, maxaddr);
                return 1;
            }
        }
        else
        {
            mainlog(logDebug, "Error '%s',line:%d\n", filename, lineno);
        }

        lineno++;
    }
    return 1;
}


void reverseBitsInBuf(uint8_t* buf, int offset, int len)
{
    int i;

    for (i = offset; i < (len + offset); i++)
    {
        buf[i] = (uint8_t)(((buf[i] << 4) & 0xF0) | ((buf[i] >> 4) & 0x0F));
        buf[i] = (uint8_t)(((buf[i] << 2) & 0xCC) | ((buf[i] >> 2) & 0x33));
        buf[i] = (uint8_t)(((buf[i] << 1) & 0xAA) | ((buf[i] >> 1) & 0x55));
    }
}


void calculateCrc(uint8_t* flashDataRaw)
{
    uint8_t isCrcOk = 1;
    uint8_t crcBuf[FLASH_MEM_LENGTH];
    uint8_t flashDataRawCrc[4];
    uint32_t crc_tab[256];
    uint32_t crc = 0xFFFFFFFF;
    uint32_t i;

    chksumCrc32gentab(crc_tab);
    memcpy(crcBuf,flashDataRaw,FLASH_MEM_LENGTH);

    flashDataRawCrc[0] = 0xFF;
    flashDataRawCrc[1] = 0xFF;
    flashDataRawCrc[2] = 0xFF;
    flashDataRawCrc[3] = 0xFF;

    if (flashDataRaw[FLASH_MEM_LENGTH - 4] != BLANK_VALUE ||
        flashDataRaw[FLASH_MEM_LENGTH - 3] != BLANK_VALUE ||
        flashDataRaw[FLASH_MEM_LENGTH - 2] != BLANK_VALUE ||
        flashDataRaw[FLASH_MEM_LENGTH - 1] != BLANK_VALUE)
    {
        flashDataRawCrc[0] = flashDataRaw[FLASH_MEM_LENGTH - 4];
        flashDataRawCrc[1] = flashDataRaw[FLASH_MEM_LENGTH - 3];
        flashDataRawCrc[2] = flashDataRaw[FLASH_MEM_LENGTH - 2];
        flashDataRawCrc[3] = flashDataRaw[FLASH_MEM_LENGTH - 1];

        crcBuf[FLASH_MEM_LENGTH - 4] = 0xFF;
        crcBuf[FLASH_MEM_LENGTH - 3] = 0xFF;
        crcBuf[FLASH_MEM_LENGTH - 2] = 0xFF;
        crcBuf[FLASH_MEM_LENGTH - 1] = 0xFF;
        isCrcOk = 0;
    }

    reverseBitsInBuf(crcBuf, 0, FLASH_MEM_LENGTH);

    for (i = 0; i < FLASH_MEM_LENGTH - 4; i++)
    {
        crc = ((crc >> 8) & 0x00FFFFFF) ^ crc_tab[(crc ^ crcBuf[i]) & 0xFF];
    }

    flashDataRaw[FLASH_MEM_LENGTH - 4] = (uint8_t)(0x000000FF & crc);
    flashDataRaw[FLASH_MEM_LENGTH - 3] = (uint8_t)(0x000000FF & (crc >> 8));
    flashDataRaw[FLASH_MEM_LENGTH - 2] = (uint8_t)(0x000000FF & (crc >> 16));
    flashDataRaw[FLASH_MEM_LENGTH - 1] = (uint8_t)(0x000000FF & (crc >> 24));

    reverseBitsInBuf(flashDataRaw, FLASH_MEM_LENGTH - 4, 4);
    
    if (isCrcOk == 0)
    {
        if (flashDataRawCrc[0] == flashDataRaw[FLASH_MEM_LENGTH - 4] &&
            flashDataRawCrc[1] == flashDataRaw[FLASH_MEM_LENGTH - 3] &&
            flashDataRawCrc[2] == flashDataRaw[FLASH_MEM_LENGTH - 2] &&
            flashDataRawCrc[3] == flashDataRaw[FLASH_MEM_LENGTH - 1])
        {
            isCrcOk = 1;
        }
    }
    
    return ;
}
