#include "smalloc.h"
#include "resource_directory.h"
#include "data_store_rd.h"
#include "eeprom.h"
#include "utils.h"
#include "nvm.h"
#include "data_store.h"
#include <stdio.h>

#define RD_MAGIC 0x491F00E5

static char rd_membuf[0x1000];

uint16_t rd_eeprom_read(uint16_t offset,uint8_t len,void* data) 
{
    eeprom_read(0x40 + offset,data,len);
    return len;
}


uint16_t rd_eeprom_write(uint16_t offset,uint8_t len,void* data) 
{
    if(len) 
    {
        eeprom_write(0x40 + offset,data,len);
    }
    return len;
}

static const small_memory_device_t nvm_dev  = 
{
    .offset = offsetof(rd_eeprom_static_hdr_t, smalloc_space),
    .size = sizeof(((rd_eeprom_static_hdr_t*)0)->smalloc_space),
    .psize = 8,
    .read = rd_eeprom_read,
    .write = rd_eeprom_write,
};



void rd_data_mem_free(void* p) 
{
    smfree(&rd_mem_dev,(char*)p - rd_membuf);
}

static uint16_t rd_mem_read(uint16_t offset,uint8_t len,void* data) 
{
    memcpy(data,rd_membuf + offset, len);
    return len;
}


static uint16_t rd_mem_write(uint16_t offset,uint8_t len,void* data) 
{
    memcpy(rd_membuf + offset,data, len);
    return len;
}


const small_memory_device_t rd_mem_dev  = 
{
    .offset = 1,
    .size = sizeof(rd_membuf),
    .psize = 8,
    .read = rd_mem_read,
    .write = rd_mem_write,
};


void* rd_data_mem_alloc(uint8_t size) 
{
    uint16_t p;
    if(size ==0) 
    {
        return 0;
    }
    p = smalloc(&rd_mem_dev,size);

    if(p) 
    {
        return rd_membuf + p;
    } 
    else 
    {
        mainlog(logDebug,"Out of memory\n");
        return 0;
    }
}



/*
 * Data store layout
 *
 * global_hdr | node_ptr | node_ptr | node_ptr | .... | node_ptr
 *
 * node_hdr | node_name | ep_ptr | ep_ptr | .... | ep_ptr
 *
 * ep_hdr | info | name | location
 *
 * */

rd_node_database_entry_t* rd_data_store_read(uint8_t nodeID) 
{
    rd_node_database_entry_t *r;
    uint16_t n_ptr;

    /*Size of static content of */
    const uint16_t static_size = offsetof(rd_node_database_entry_t,refCnt);

    n_ptr = 0;
    rd_eeprom_read(offsetof(rd_eeprom_static_hdr_t,node_ptrs[nodeID]),sizeof(uint16_t),&n_ptr);

    if(n_ptr==0) 
    {
        mainlog(logDebug,"Node %i node not eeprom\n",nodeID);
        return 0;
    }

    r = rd_data_mem_alloc(sizeof(rd_node_database_entry_t));

    if(r==0) 
    {
        mainlog(logDebug,"Out of memory\n");
        return 0;
    }
    memset(r,0,sizeof(rd_node_database_entry_t));

    /*Read the first part of the node entry*/
    rd_eeprom_read(n_ptr, static_size,r );

    return r;
}


/**
 * Write a rd_node_database_entry_t and all its endpoints to storage.
 * This is called when a new node is added.
 */
void rd_data_store_nvm_write(rd_node_database_entry_t *n) 
{
    uint16_t ptr;

    /*Size of static content of */
    const uint16_t static_size = offsetof(rd_node_database_entry_t,refCnt);
    mainlog(logDebug,"static_size :%d\n",static_size);

    rd_eeprom_read(offsetof(rd_eeprom_static_hdr_t,node_ptrs[n->nodeid]),sizeof(uint16_t),&ptr);
    mainlog(logDebug,"ptr :%d\n",ptr);
    ASSERT(ptr == 0);

    ptr = smalloc(&nvm_dev, static_size);
    mainlog(logDebug,"ptr :%d\n",ptr);
    if(ptr==0) 
    {
        mainlog(logDebug,"EEPROM is FULL\n");
        return;
    }
    rd_eeprom_write(offsetof(rd_eeprom_static_hdr_t,node_ptrs[n->nodeid]),sizeof(uint16_t),&ptr);

    /*for debugging
    rd_eeprom_read(offsetof(rd_eeprom_static_hdr_t,node_ptrs[n->nodeid]),sizeof(uint16_t),&ptr);
    mainlog(logDebug,"ptr :%d\n",ptr);
    */

    ptr+= rd_eeprom_write(ptr, static_size,n);
 
}

void rd_data_store_update(rd_node_database_entry_t *n) 
{
    uint16_t ptr;

    /*Size of static content of */
    const uint16_t static_size = offsetof(rd_node_database_entry_t,refCnt);

    rd_eeprom_read(offsetof(rd_eeprom_static_hdr_t,node_ptrs[n->nodeid]),sizeof(uint16_t),&ptr);

    if(ptr==0) return;

    ptr+= rd_eeprom_write(ptr, static_size,n);
    
}
void rd_data_store_mem_free(rd_node_database_entry_t *n) 
{
    rd_data_mem_free(n);
}

void rd_data_store_nvm_free(rd_node_database_entry_t *n) 
{
    uint16_t n_ptr;

    /*Size of static content of */
    //const uint16_t static_size = offsetof(rd_node_database_entry_t,refCnt);

    rd_eeprom_read(offsetof(rd_eeprom_static_hdr_t,node_ptrs[n->nodeid]),sizeof(uint16_t),&n_ptr);

    if(!n_ptr) 
    {
        mainlog(logDebug,"Tried to free a node from NVM which does not exist\n");
        return;
    }

    /*Read the first part of the node entry*/
    smfree(&nvm_dev,n_ptr);

    n_ptr = 0;
    rd_eeprom_write(offsetof(rd_eeprom_static_hdr_t,node_ptrs[n->nodeid]),sizeof(uint16_t),&n_ptr);
}

void data_store_init() 
{
    /* init: Precondition: SerialAPI up */
    char buf[offsetof(rd_eeprom_static_hdr_t,node_ptrs)];
    uint8_t i;
    uint16_t ptr,q;
    rd_eeprom_static_hdr_t* hdr = (rd_eeprom_static_hdr_t*)buf;

    rd_eeprom_read(0,sizeof(buf),buf);
    /* Note: Controllers dont necessarily change homeid after SetDefault. */
    /*for debugging */
    mainlog(logDebug,"rd magic : %d, homeid: %08X, nodeid: %02X", hdr->magic, hdr->homeID, hdr->nodeID);
    mainlog(logDebug,"my magic : %d, homeid: %08X, nodeid: %02X", RD_MAGIC, homeId, MyNodeId);
    if(hdr->magic != RD_MAGIC || hdr->homeID !=homeId || hdr->nodeID != MyNodeId)
    {
        mainlog(logDebug,"Reformatting eeprom\n");
        memset(buf,0,sizeof(buf));
        hdr->magic = RD_MAGIC;
        hdr->homeID = homeId;
        hdr->flags = 0;
        hdr->nodeID = MyNodeId;

        ptr = rd_eeprom_write(0,sizeof(buf),buf);
        for(i=0; i < ZW_MAX_NODES; i++) 
        {
            q=0;
            ptr += rd_eeprom_write(ptr,sizeof(uint16_t),&q);
        }
        smformat(&nvm_dev);

    }
    /*Reset the mem device*/
    smformat(&rd_mem_dev);
}

/* Invalidate entire data store, so we reformat on reset. */
void rd_data_store_invalidate()
{
    const uint32_t data = 0;

    uint16_t o = offsetof(rd_eeprom_static_hdr_t, magic);
    mainlog(logDebug,"rd invalidate");
    rd_eeprom_write(o, sizeof(uint32_t), (void*)&data);
}

