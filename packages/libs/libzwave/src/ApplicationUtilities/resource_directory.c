#include <stdbool.h>
#include <stdint.h>//have uint8_t

#include "resource_directory.h"
#include "data_store_rd.h"
#include "utils.h"//contain mainlog 
#include "node_cache.h"
#include "cmd_class_misc.h"
#include "inline_swap_endian.h"
#include "clock.h"
#include "nvm.h"
#include "config.h"


rd_node_database_entry_t *ndb[ZW_MAX_NODES];


static uint8_t probe_lock = 0;
static rd_node_database_entry_t *current_probe_entry = 0;

#define BIT8_TST(bit, array) ((array[(bit) >> 3] >> ((bit)&7)) & 0x1)
#define INCLUSION_CONTROLLER_CC_BIT_POSITION     0
#define TRANSPORT_SERVICE_CC_BIT_POSITION        1

#define INCLUSION_CONTROLLER_CC_BITMASK          (1<<INCLUSION_CONTROLLER_CC_BIT_POSITION)
#define TRANSPORT_SERVICE_CC_BITMASK             (1<<TRANSPORT_SERVICE_CC_BIT_POSITION)
#define CC_PROBED_BITMASK                        0x80


/**
 * called from ApplicationController Update, when a node info is received or if the
 * ZW_RequestNodeInfo is failed.
 *
 * @param success tell if the request went ok*/
/*IN  Node id of the node that send node info */
/*IN  Pointer to Application Node information */
/*IN  Node info length                        */
void
rd_nif_request_notify(uint8_t sucsess, uint8_t bNodeID, uint8_t* nif,
    uint8_t nif_len)
{

}
/**
 * Read the protocol info from NVM
 */
static void
update_protocol_info(rd_node_database_entry_t *n)
{
    NODEINFO ni;

    serialApiGetNodeProtocolInfo(n->nodeid, &ni);
    n->nodeTypeGeneric = ni.nodeType.generic;
    n->nodeType = ni.nodeType;

    if (ni.capability & NODEINFO_LISTENING_SUPPORT)
    {
        n->mode = MODE_ALWAYSLISTENING;
        //n->wakeUp_interval = 70*60; //70 Minutes, the node will be probed once each 70 minutes.
    }
    else if (ni.security &
             (NODEINFO_ZWAVE_SENSOR_MODE_WAKEUP_1000 | NODEINFO_ZWAVE_SENSOR_MODE_WAKEUP_250))
    {
        n->mode = MODE_FREQUENTLYLISTENING;
    }
    else
    {
        n->mode = MODE_NONLISTENING;
    }
}

/*
int
SupportsCmdClassSecure(BYTE nodeid, WORD class)
{
  return ((SupportsCmdClassFlags(nodeid, class) & SUPPORTED_SEC) > 0);
}
*/

/**
 * Set node attribute flags
 */
uint8_t
SetCacheEntryFlagMasked(uint8_t nodeid, uint8_t value, uint8_t mask)
{
    rd_node_database_entry_t *n;

    n = rd_get_node_dbe(nodeid);
    if (n)
    {
        n->security_flags = (n->security_flags & (~mask)) | value;
        rd_data_store_update(n);
        rd_free_node_dbe(n);
    }
    else
    {
        mainlog(logDebug, "Attempt to set flag of non existing node = %i\n", nodeid);
    }
    return 1;
}

/**
 * Set node support cms class Inclusion Controller
 */
uint8_t
SetSupportedInclusionControllerCmdClass(uint8_t nodeid, bool value)
{
    rd_node_database_entry_t *n;

    n = rd_get_node_dbe(nodeid);
    if (n)
    {
        n->isSupportedCmdClass |= CC_PROBED_BITMASK;
        n->isSupportedCmdClass |= (value << INCLUSION_CONTROLLER_CC_BIT_POSITION) ;
        rd_data_store_update(n);
        rd_free_node_dbe(n);
    }
    else
    {
        mainlog(logDebug, "Attempt to Set SupportedInclusionController of non existing node = %i\n", nodeid);
    }
    return 1;
}

/**
 * Set node support cms class Inclusion Controller
 */
uint8_t
SetSupportedTransportServiceCmdClass(uint8_t nodeid, bool value)
{
    rd_node_database_entry_t *n;

    n = rd_get_node_dbe(nodeid);
    if (n)
    {
        n->isSupportedCmdClass |= CC_PROBED_BITMASK;
        n->isSupportedCmdClass |= (value << TRANSPORT_SERVICE_CC_BIT_POSITION) ;
        rd_data_store_update(n);
        rd_free_node_dbe(n);
    }
    else
    {
        mainlog(logDebug, "Attempt to Set SupportedTransportService of non existing node = %i\n", nodeid);
    }
    return 1;
}

/**
 * Set last value (#0) switch multilevel
 */
uint8_t
SetLastValueSwitchMultilevel(uint8_t nodeid, uint8_t value)
{
    rd_node_database_entry_t *n;

    n = rd_get_node_dbe(nodeid);
    if (n)
    {
        if((0 < value) && (100 > value))
        {
            n->lastUpdate = (uint32_t)value;
            rd_data_store_update(n);
        }
        rd_free_node_dbe(n);
    }
    else
    {
        mainlog(logDebug, "Attempt to Set last Value SwitchMultilevel of non existing node = %i\n", nodeid);
    }
    return 1;
}

void rd_mark_node_deleted(uint8_t nodeid) 
{
    /* FIXME: Choose a better default value in case lookup fails */
    rd_node_database_entry_t *n = rd_get_node_dbe(nodeid);
    //ASSERT(n);
    if (n)
    {
        n->mode |= MODE_FLAGS_DELETED;
        rd_free_node_dbe(n);
    }
}

/**
 * Retrieve Cache entry flag
 */

uint8_t
GetCacheEntryFlag(uint8_t nodeid)
{
    /*entry : entry of database */
    rd_node_database_entry_t *entry;

    uint8_t rc;

    entry = rd_get_node_dbe(nodeid);

    if (!entry)
    {
        //mainlog(logDebug, "GetCacheEntryFlag: on non existing node = %i\n", nodeid);
        return 0;
    }

    rc = entry->security_flags;
    rd_free_node_dbe(entry);
    return rc;
}

/**
 * Retrieve SupportedInclusionControllerCmdClass flag
 */

uint8_t
GetSupportedInclusionControllerCmdClass(uint8_t nodeid)
{
    /*entry : entry of database */
    rd_node_database_entry_t *entry;

    uint8_t rc;

    entry = rd_get_node_dbe(nodeid);

    if (!entry)
    {
        //mainlog(logDebug, "GetSupportedInclusionContrllerCmdClass: on non existing node = %i\n", nodeid);
        return 0;
    }

    rc = entry->isSupportedCmdClass;
    if (!rc)
    {
        rc = CC_NOT_PROBED;
    }
    else
    {
        rc = ((entry->isSupportedCmdClass & INCLUSION_CONTROLLER_CC_BITMASK) >> INCLUSION_CONTROLLER_CC_BIT_POSITION) ? CC_SUPPORTED : CC_NOT_SUPPORTED;
    }

    rd_free_node_dbe(entry);
    return rc;
}

/**
 * Retrieve GetSupportedTransportServiceCmdClass flag
 */

uint8_t
GetSupportedTransportServiceCmdClass(uint8_t nodeid)
{
    /*entry : entry of database */
    rd_node_database_entry_t *entry;

    uint8_t rc;

    entry = rd_get_node_dbe(nodeid);

    if (!entry)
    {
        mainlog(logDebug, "GetSupportedInclusionContrllerCmdClass: on non existing node = %i\n", nodeid);
        return 0;
    }

    rc = entry->isSupportedCmdClass;
    if (!rc)
    {
        rc = CC_NOT_PROBED;
    }
    else
    {
        rc = ((entry->isSupportedCmdClass & TRANSPORT_SERVICE_CC_BITMASK) >> TRANSPORT_SERVICE_CC_BIT_POSITION) ? CC_SUPPORTED : CC_NOT_SUPPORTED;
    }

    rd_free_node_dbe(entry);
    return rc;
}

/**
 * Retrieve Cache entry nodeType
 */

uint8_t
GetCacheEntryNodeTypeGeneric(uint8_t nodeid)
{
    /*entry : entry of database */
    rd_node_database_entry_t *entry;

    uint8_t rc;

    entry = rd_get_node_dbe(nodeid);

    if (!entry)
    {
        mainlog(logDebug, "GetCacheEntryNodeTypeGeneric: on non existing node = %i\n", nodeid);
        return 0;
    }

    rc = entry->nodeTypeGeneric;
    rd_free_node_dbe(entry);
    return rc;
}

NODE_TYPE
GetCacheEntryNodeType(uint8_t nodeid)
{
    /*entry : entry of database */
    rd_node_database_entry_t *entry;

    NODE_TYPE rc;

    entry = rd_get_node_dbe(nodeid);

    if (!entry)
    {
        mainlog(logDebug, "GetCacheEntryNodeType: on non existing node = %i\n", nodeid);
        rc.basic = 0;
        rc.generic = 0;
        rc.specific = 0;
        return rc;
    }

    rc = entry->nodeType;
    rd_free_node_dbe(entry);
    return rc;
}

/**
 * Retrieve Cache entry nodeMode
    MODE_PROBING,
    MODE_NONLISTENING,
    MODE_ALWAYSLISTENING,
    MODE_FREQUENTLYLISTENING,
    MODE_MAILBOX,
 */

uint8_t
GetCacheEntryNodeMode(uint8_t nodeid)
{
    /*entry : entry of database */
    rd_node_database_entry_t *entry;

    uint8_t rc;

    entry = rd_get_node_dbe(nodeid);

    if (!entry)
    {
        //mainlog(logDebug, "GetCacheEntryMode: on non existing node = %i\n", nodeid);
        return 0;
    }

    rc = entry->mode;
    rd_free_node_dbe(entry);
    return rc;
}

/**
    MODE_PROBING,
    MODE_NONLISTENING,
    MODE_ALWAYSLISTENING,
    MODE_FREQUENTLYLISTENING,
    MODE_MAILBOX,
    MODE_REMOVE_FORCE,
 */

uint8_t
SetCacheEntryNodeMode(uint8_t nodeid, uint8_t value)
{

    rd_node_database_entry_t *n;

    n = rd_get_node_dbe(nodeid);
    if (n)
    {
        n->mode = value;
        rd_data_store_update(n);
        rd_free_node_dbe(n);
    }
    else
    {
        mainlog(logDebug, "Attempt to set mode of node = %i\n", nodeid);
    }
    return 1;
}

/**
 * Get last Value SwitchMultilevel
 */

uint8_t
GetLastValueSwitchMultilevel(uint8_t nodeid)
{
    /*entry : entry of database */
    rd_node_database_entry_t *entry;

    uint8_t rc;

    entry = rd_get_node_dbe(nodeid);

    if (!entry)
    {
        mainlog(logDebug, "GetlastValueSwitchMultilevel: on non existing node = %i\n", nodeid);
        return 0;
    }

    rc = entry->lastUpdate;
    rd_free_node_dbe(entry);
    return (uint8_t)rc;
}

/**
 * Get nodedatabase entry from data store. free_node_dbe, MUST
 * be called when the node entry if no longer needed.
 */
rd_node_database_entry_t *
rd_get_node_dbe(uint8_t nodeid)
{
    if ((nodeid == 0) || (nodeid > ZW_MAX_NODES))
    {
        return 0;
    }

    if (ndb[nodeid - 1])
    {
        ndb[nodeid - 1]->refCnt++;
    }
    return ndb[nodeid - 1];
}

/**
 * MUST be called when a node entry is no longer needed.
 */
void rd_free_node_dbe(rd_node_database_entry_t *entry)
{
    if (entry)
    {
        ASSERT(entry->refCnt > 0);
        entry->refCnt--;
    }
}

void rd_probe_resume()
{
    uint8_t i;
    if (current_probe_entry)
    {
        rd_node_probe_update(current_probe_entry);
        return;
    }

    for (i = 0; i < ZW_MAX_NODES; i++)
    {
        if (ndb[i] && (ndb[i]->state != STATUS_DONE && ndb[i]->state != STATUS_PROBE_FAIL && ndb[i]->state != STATUS_FAILING))
        {
            current_probe_entry = ndb[i];
            rd_node_probe_update(ndb[i]);
            break;
        }
    }
}

void rd_node_probe_update(rd_node_database_entry_t *n)
{
    //uint8_t i;
    //ts_param_t p;

    if (probe_lock)
    {
        mainlog(logDebug, "probe machine is locked\n");
        return;
    }

    if ((n->nodeid == 0) || (n->nodeid >ZW_MAX_NODES))
    {
        ASSERT(0);
        return;
    }

    mainlog(logDebug, "rd_node_probe_update state node = %d\n", n->nodeid);

    switch (n->state)
    {
    /*case STATUS_ADDING:
            update_protocol_info(n); //We do this here because the security layer needs to know if this is node is a controller
            break;*/

    case STATUS_CREATED:
        if (current_probe_entry && current_probe_entry != n)
        {
            mainlog(logDebug, "another probe is in progress waiting\n");
            return;
        }
        current_probe_entry = n;
        goto next_state;
        break;
    /*case STATUS_PROBE_PROTOCOL_INFO:
       {
       update_protocol_info(n);
       goto next_state;
       break;
       }*/
    case STATUS_PROBE_NODE_INFO:
        if (n->security_flags & NODE_FLAG_JUST_ADDED)
        {
            /* Update supported CC */
            
            if (n->nodeid == lastLearnedNodeZPC.node_id)
            {
                mainlog(logDebug, "Update support Inclusion CC and TransportService CC");
                SetSupportedInclusionControllerCmdClass(lastLearnedNodeZPC.node_id, 
                                (isSupportedCommandClass(&lastLearnedNodeZPC, COMMAND_CLASS_INCLUSION_CONTROLLER) != SCHEME_NOT_SUPPORT));
                SetSupportedTransportServiceCmdClass(lastLearnedNodeZPC.node_id, 
                                (isSupportedCommandClass(&lastLearnedNodeZPC, COMMAND_CLASS_TRANSPORT_SERVICE) != SCHEME_NOT_SUPPORT));
            }
            
            goto next_state;
        }
        else if (n->security_flags & NODE_FLAG_INFO_ONLY)
        {
            n->state = STATUS_DONE;
            rd_node_probe_update(n);
        }
        else
        {
            goto next_state;
        }
        break;

    case STATUS_PROBE_PRODUCT_ID:
        if (n->nodeid == MyNodeId)
        {
            /*
                n->productID = cfg.product_id;
                n->manufacturerID = cfg.manufacturer_id;
                n->productType = cfg.product_type;
                */
        }
        goto next_state;
        break;

    case STATUS_ENUMERATE_ENDPOINTS:
        goto next_state;
        break;

    case STATUS_PROBE_ENDPOINTS:
        goto next_state;
        break;

    case STATUS_SET_WAKE_UP_INTERVAL:
        goto next_state;
        break;

    case STATUS_ASSIGN_RETURN_ROUTE:
        goto next_state;
        break;

    case STATUS_PROBE_WAKE_UP_INTERVAL:
        goto next_state;
        break;

    case STATUS_MDNS_PROBE:
        goto next_state;
        break;

    case STATUS_MDNS_EP_PROBE:
        goto next_state;
        break;

    case STATUS_DONE:
        mainlog(logDebug, "Probe of node %d is done\n", n->nodeid);
        n->lastUpdate = clock_seconds();
        n->lastAwake = clock_seconds();
        n->security_flags &= ~NODE_FLAG_JUST_ADDED;
        goto probe_complete;
        break;

    case STATUS_PROBE_FAIL:
        mainlog(logDebug, "Probe of node %d failed\n", n->nodeid);
        goto probe_complete;
        break;

    default:
        break;
    }
    return;

next_state:
    n->state++;
    rd_node_probe_update(n);
    return;
/*
fail_state: 
    n->state = STATUS_PROBE_FAIL;
    rd_node_probe_update(n);
    return;
*/
probe_complete:
    /* Store all node data in persistent memory */
    rd_data_store_nvm_free(n);
    rd_data_store_nvm_write(n);
    current_probe_entry = NULL;

    rd_probe_resume();
}

void rd_register_new_node(uint8_t node, uint8_t adding)
{
    rd_node_database_entry_t *n;

    mainlog(logDebug, "rd_register_new_node with nodeid = %d, adding = %d\n", node, adding);

    ASSERT((node > 0) && (node < (ZW_MAX_NODES + 1)));

    n = ndb[node - 1];

    if (n)
    {
        mainlog(logDebug,"n->state :%d",n->state);
        if (n->state == STATUS_FAILING || n->state == STATUS_PROBE_FAIL || n->state == STATUS_DONE)
        {
            mainlog(logDebug, "re-probing node %i old state", n->nodeid);
            ASSERT(n->nodeid == node);
            n->state = STATUS_CREATED;

            return rd_node_probe_update(n);
        }
        else
        {
            mainlog(logDebug, "Node probe is already in progress.\n");
            return;
        }
    }

    n = rd_data_mem_alloc(sizeof(rd_node_database_entry_t));
    ndb[node - 1] = n;

    if (!n)
    {
        mainlog(logDebug, "Unable to register new node Out of mem!\n");
        return;
    }

    memset(n, 0, sizeof(rd_node_database_entry_t)),

    n->nodeid = node;

    /*When node name is 0, then we use the default names*/
    n->state = STATUS_CREATED;
    n->mode = MODE_PROBING;
    n->security_flags = 0;
    update_protocol_info(n); //Get protocol info new because this is always sync

    if (adding)
    {
        n->security_flags |= NODE_FLAG_JUST_ADDED;
    }

    n->wakeUp_interval = 60 * 60; //Default wakeup interval is 70 minutes

    if (node == MyNodeId)
    {
        //if (get_net_scheme() > 0)
        {
            n->security_flags = NODE_FLAGS_SECURITY;
        }
    }

    current_probe_entry = n; /*Make sure to probe this node as the first thing nest time around.*/
    rd_node_probe_update(n);
}

/*push_notify: push nodeId, Type, nodeMode to app into learn mode*/
int rd_init(uint8_t lock, uint8_t* node_id_list, uint8_t* node_list_size)
{
    mainlog(logUI, "------------------rd_init---------------------");
    uint8_t i;
    uint8_t nodelist[29];
    uint8_t ver, capabilities, len, chip_type, chip_version;

    uint32_t old_homeID;

    old_homeID = homeId;
    serialApiMemoryGetID((uint8_t *)&homeId, &MyNodeId);
    swap_endian(&homeId);

    /*check  get homeId MyNodeId */
    if ((!homeId) || (!MyNodeId))
    {
        printf("Please check Z-Wave module !!\n");
        exit(-1) ;
    }

    data_store_init();
    
    current_probe_entry = 0;
    probe_lock = lock;

    serialApiGetInitData(&ver, &capabilities, &len, nodelist, &chip_type, &chip_version);

    /*Always update the entry for this node, since this is without cost, and network role
    * might have changed. */
    rd_register_new_node(MyNodeId, false);


    for (i = 0; i < ZW_MAX_NODES; i++)
    {
            
        if (BIT8_TST(i, nodelist))
        {
            if (i+1 == MyNodeId)
                continue;

            mainlog(logDebug, "Network has node %i", i + 1);
            ndb[i] = rd_data_store_read(i + 1);
            if (ndb[i] == 0)
            {
                rd_register_new_node(i + 1, false);
            }
            else
            {
                rd_node_database_entry_t *n = ndb[i];

                if (n->state == STATUS_DONE)
                {
                    /* Here we just fake that the nodes has been alive recently.
                    * This is to prevent that the node becomes failing without
                    * reason*/
                    n->lastAwake = clock_seconds();
                    //n->state = STATUS_MDNS_PROBE;
                }
                //mainlog(logDebug,"node(%d) mode: %d, nodeType: %d, security_flags: %d \n",i+1,n->mode, n->nodeTypeGeneric, GetCacheEntryFlag(i+1) );
                mainlog(logInfo, 
                        "NodeID = 0x%02X(%d), Type = %s, SecurityFlags = 0x%02X (%d decimal), Mode = %s, SupportedInclusionContrllerCmdClas = %d, manufacturerID = 0x%04X, productType = 0x%04X, productID = 0x%04X", 
                        i + 1, i + 1,
                        WriteNodeTypeString(GetCacheEntryNodeTypeGeneric(i + 1)), GetCacheEntryFlag(i + 1), GetCacheEntryFlag(i + 1),
                        WriteNodeModeString(GetCacheEntryNodeMode(i + 1)), GetSupportedInclusionControllerCmdClass(i + 1),
                        GetNodeManufactureId(i + 1), GetNodeProductType(i + 1), GetNodeProductID(i + 1));
            }
            
            if((node_list_size) && (node_id_list))
            {
                node_id_list[*node_list_size] = i+ 1;
                *node_list_size += 1;
            }
            

        }
        else
        {
            rd_remove_node(i + 1);
        }
    }

    rd_probe_resume();        
    mainlog(logDebug, "Resource directory init done\n");
    return (old_homeID != homeId);
}

int GetNodeList(uint8_t* noNode, rd_node_list_t* nodeList)
{
    mainlog(logUI, "------------------GetNodeList---------------------");
    uint8_t i, node_list_size = 0;
    for (i = 1; i <= ZW_MAX_NODES; i++)
    {
            
        if(GetCacheEntryNodeMode(i))        
        {
            if (i == MyNodeId)
                continue;

            mainlog(logDebug, "Network of controller has node %i", i);

            mainlog(logInfo, 
                    "NodeID = 0x%02X(%d), Mode = %s", i, i,
                    WriteNodeModeString(GetCacheEntryNodeMode(i)));
            if(nodeList)
            {
                nodeList[node_list_size].nodeId = i;
                nodeList[node_list_size].mode = GetCacheEntryNodeMode(i);
                node_list_size += 1;
            }
        }
    }
    *noNode = node_list_size;
    mainlog(logDebug, "GetNodeList done\n");
    return 0;
}

/**return noDevice listening, noDevice noneListening**/
void getNoDeviceMode(uint8_t* noListening, uint8_t* noNoneListening)
{
    mainlog(logUI, "------------------getNoDeviceMode---------------------");
    uint8_t i;
    for (i = 1; i <= ZW_MAX_NODES; i++)
    {
        if(MODE_FREQUENTLYLISTENING == GetCacheEntryNodeMode(i))        
        {
            if (i == MyNodeId)
                continue;
            *noNoneListening += 1;
        }            
        else if(MODE_ALWAYSLISTENING == GetCacheEntryNodeMode(i))        
        {
            if (i == MyNodeId)
                continue;
            *noListening += 1;
        }
    }
    mainlog(logDebug, "getNoDeviceMode done\n");
}

void rd_remove_node(uint8_t node)
{
    rd_node_database_entry_t *n;

    if (node == 0)
        return;

    n = ndb[node - 1];

    if (n == 0)
        return;

    /*
    * Abort probe if we have one in progress.
    */
    if (current_probe_entry == n)
    {
        current_probe_entry = 0;
    }

    mainlog(logDebug, "Removing node %i %p\n", node, n);

    if (n->mode == MODE_MAILBOX)
    {
        /*Clear the mailbox for potential entries*/
        //mb_failing_notify(n->nodeid);
    }

    n->mode |= MODE_FLAGS_DELETED;

    if ((n->nodeid == 0) || (n->nodeid > ZW_MAX_NODES))
        return;

    /* Clear node from data storage */
    rd_data_store_nvm_free(n);
    rd_data_store_mem_free(n);
    ndb[node - 1] = 0;
}

/**
 * Returns true if node is registred in database
 * @param node
 * @return
 */
uint8_t
rd_node_exists(uint8_t node)
{
    if (node > 0 && node <= ZW_MAX_NODES)
    {
        return (ndb[node - 1] != 0);
    }
    return false;
}

/**
 *  Lock/Unlock the node probe machine. When the node probe lock is enabled, all probing will stop.
 *  Probing is resumed when the lock is disabled. The probe lock is used during a add node process or during learn mode.
 */
void rd_probe_lock(uint8_t enable)
{
    probe_lock = enable;

    if (!probe_lock)
    {
        rd_probe_resume();
    }
}

uint8_t
rd_probe_in_progress()
{
    return (current_probe_entry != 0);
}

/**
 * Called to instruct the RD to shutdown and send goodbye messages.
 * the function cbExitDone is called when there are no more messages to send.
 *
 */
void
rd_exit()
{

    rd_node_database_entry_t *n;
    uint8_t i;


    for (i = 0; i < ZW_MAX_NODES; i++)
    {
        n = ndb[i];
        if (n)
        {
            n->mode |= MODE_FLAGS_DELETED;
    
        }
    }
}


/**
 * Finally free memory allocated by RD
 */
void
rd_destroy()
{
    uint8_t i;
    rd_node_database_entry_t *n;
    for (i = 0; i < ZW_MAX_NODES; i++)
    {
        n = ndb[i];
        if (n)
        {
            rd_data_store_mem_free(n);
            ndb[i] = 0;
        }
    }
}

/*
 * https://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetNaive
 * Counting bits set, Brian Kernighan's way
 */
static inline uint8_t
bit8_count(uint8_t n)
{
    unsigned int c; // c accumulates the total bits set in v
    for (c = 0; n; c++)
    {
        n &= n - 1; // clear the least sigficant bit set
    }
    return c;
}

uint8_t
count_array_bits(uint8_t* a, uint8_t length)
{
    uint8_t c = 0;
    uint8_t i;
    for (i = 0; i < length; i++)
    {
        c += bit8_count(a[i]);
    }
    return c;
}

uint16_t
GetNodeManufactureId(uint8_t nodeid)
{
    /*entry : entry of database */
    rd_node_database_entry_t *entry;

    uint16_t rc;

    entry = rd_get_node_dbe(nodeid);

    if (!entry)
    {
        mainlog(logDebug, "GetNodeManufactureId: on non existing node = %i\n", nodeid);
        return 0;
    }

    rc = entry->manufacturerID;
    rd_free_node_dbe(entry);
    return rc;
}

uint16_t
GetNodeProductType(uint8_t nodeid)
{
    /*entry : entry of database */
    rd_node_database_entry_t *entry;

    uint16_t rc;

    entry = rd_get_node_dbe(nodeid);

    if (!entry)
    {
        mainlog(logDebug, "GetNodeProductType: on non existing node = %i\n", nodeid);
        return 0;
    }

    rc = entry->productType;
    rd_free_node_dbe(entry);
    return rc;
}

uint16_t
GetNodeProductID(uint8_t nodeid)
{
    /*entry : entry of database */
    rd_node_database_entry_t *entry;

    uint16_t rc;

    entry = rd_get_node_dbe(nodeid);

    if (!entry)
    {
        mainlog(logDebug, "GetNodeProductID: on non existing node = %i\n", nodeid);
        return 0;
    }

    rc = entry->productID;
    rd_free_node_dbe(entry);
    return rc;
}

/**
 * Set Node Manufacture
 */
uint8_t
SetNodeManufacture(uint8_t nodeid, uint8_t manu[NODE_MANUFACTURER_SIZE])
{
    rd_node_database_entry_t *n;

    n = rd_get_node_dbe(nodeid);
    if (n)
    {
        n->manufacturerID = ((uint16_t)manu[0] * CONVERT_UINT8_TO_UINT16 + manu[1]);
        n->productType = ((uint16_t)manu[2] * CONVERT_UINT8_TO_UINT16 + manu[3]);
        n->productID = ((uint16_t)manu[4] * CONVERT_UINT8_TO_UINT16 + manu[5]);
        rd_data_store_update(n);
        rd_free_node_dbe(n);
    }
    else
    {
        mainlog(logDebug, "Attempt to Set Node Manufacture of non existing node = %i\n", nodeid);
    }
    return 1;
}