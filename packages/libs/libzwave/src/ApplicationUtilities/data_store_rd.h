#ifndef RD_DATASTORE_H_
#define RD_DATASTORE_H_

#include <string.h>

#include "resource_directory.h"
#include "smalloc.h"
/**
 * \ingroup ZIP_Resource
 * \defgroup data_store Resource directory data store
 *
 * @{
 */
extern const small_memory_device_t rd_mem_dev;

void data_store_init();
/**
 * read node entry from persistent storage. Return a newly allocated
 * rd_node_database_entry_t on sucsess. The returned entry must be freed with
 * smfree
 */
rd_node_database_entry_t* rd_data_store_read(uint8_t nodeID);

/**
 * Write a rd_node_database_entry_t and all its endpoints to storage.
 * This is called when a new node is added.
 */
void rd_data_store_nvm_write(rd_node_database_entry_t *n);

/**
 * Called when a the status of a node or its endpoints has changed.
 */
void rd_data_store_update(rd_node_database_entry_t *n);

/**
 * Frees up all storage associated with a node, both memory buffer and nvm is freeed
 * @param n should be allocated with rd_data_store_read
 */

void rd_data_store_mem_free(rd_node_database_entry_t *n);
void rd_data_store_nvm_free(rd_node_database_entry_t *n);



/**
 * Allocate and deallocate memory from the rd_data_mem pool
 */
void* rd_data_mem_alloc(uint8_t size);
void rd_data_mem_free(void* p);
void rd_data_store_invalidate(void);


/**
 * }@
 */
#endif /* RD_DATASTORE_H_ */
