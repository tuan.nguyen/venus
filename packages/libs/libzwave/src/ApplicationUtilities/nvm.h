
#ifndef _NVM_H_
#define _NVM_H_
#include <stdint.h>
#include "platform_cc.h"
#include "supported_CommandClassAssociation.h"
#include "config.h"

#ifndef offsetof
#define offsetof(s,m)   (uint16_t)( (uint8_t*)&(((s *)0)->m) - (uint8_t*)0 )
#endif

#define NVM_CONFIG_VERSION 2
#define ZPCMAGIC 1645985


typedef struct zpc_nvm_config 
{
    uint32_t magic;
    uint16_t config_version;
    int8_t security_scheme;
    /* Security 0 key */
    uint8_t security_netkey[16];
    uint8_t security_status;

    /*mailbox_configuration_mode_t mb_conf_mode;*/
    /*venus config */
    uint8_t set_nif;  /* true has set, false not set */
    ASSOCIATION_GROUP ass_grp[MAX_ASSOCIATION_GROUPS];
    uint8_t ass_magic;

    /*S2 Network keys */
    uint8_t assigned_keys; /*Bitmask of keys which are assigned */
    uint8_t security2_key[3][16];
    uint8_t ecdh_priv_key[32];
}__attribute__((__packed__)) zpc_nvm_config_t ;



void NVM_Read(uint16_t start,void* dst,uint8_t size);
void NVM_Write(uint16_t start,const void* dst,uint8_t size);
#define nvm_config_get(par_name,dst) NVM_Read(offsetof(zpc_nvm_config_t,par_name),dst,sizeof(((zpc_nvm_config_t*)0)->par_name))
#define nvm_config_set(par_name,src) NVM_Write(offsetof(zpc_nvm_config_t,par_name),src,sizeof(((zpc_nvm_config_t*)0)->par_name))

void nvm_init(); 


extern uint8_t      MyNodeId;
extern uint32_t     homeId;
extern uint8_t      MySystemSUCId;

extern uint8_t bNodeExistMaskLen;
extern uint8_t bNodeExistMask[ZW_MAX_NODEMASK_LENGTH];


#endif