#ifndef DATASTORE_H_
#define DATASTORE_H_

#include "nvm.h"

#define RD_SMALLOC_SIZE 0x5E00


/* Convenience macro for saving var to a field in rd_eeprom_static_hdr_t */
#define DS_EEPROM_SAVE(VAR, FIELD)   rd_eeprom_write( \
    offsetof(rd_eeprom_static_hdr_t, FIELD), \
    sizeof(VAR), \
    (unsigned char*)&VAR)

/* Convenience macro for loading var from a field in rd_eeprom_static_hdr_t */
#define DS_EEPROM_LOAD(VAR, FIELD) rd_eeprom_read( \
    offsetof(rd_eeprom_static_hdr_t, FIELD), \
    sizeof(VAR), \
    &VAR)



/* General layout of the datastore. This is the place to add more data. */
typedef struct rd_eeprom_static_hdr 
{
    uint32_t magic;
    uint32_t homeID;
    uint8_t  nodeID;
    uint32_t flags;
    uint16_t node_ptrs[ZW_MAX_NODES];
    uint8_t  smalloc_space[RD_SMALLOC_SIZE];
} rd_eeprom_static_hdr_t;



uint16_t rd_eeprom_read(uint16_t offset,uint8_t len,void* data);

uint16_t rd_eeprom_write(uint16_t offset,uint8_t len,void* data);

void rd_data_store_invalidate();

void data_store_init();

#endif /* DATASTORE_H_ */
