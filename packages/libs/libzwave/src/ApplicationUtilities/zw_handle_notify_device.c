
#include <stdbool.h>
#include <stdint.h> //have uint8_t
#include <unistd.h> //contain usleep
#include <pthread.h>
#include "zw_serialapi/zw_classcmd.h"
#include "zw_serialapi/zw_serialapi.h"

#include "cmd_class_misc.h"
#include "zw_handle_notify_device.h"
#include "config.h"
#include "utils.h"
#include "timing.h"
#include "timer.h"
#include "nvm.h"

#include "ZW_TransportSecurity.h"
#include "ZW_TransportEndpoint.h"
#include "ZW_DevicePoll.h"
#include "ZW_PollApi.h"
#include "ZW_AutoRemoveAdd_FSM.h"

#define TIME_DELAY_SEND_WAKE_UP_NO_MORE_INFO 5000
#define TIME_DELAY_WAITING_CMD_FROM_HANDLER 7000

pthread_mutex_t CriticalMutexHandleNotify = PTHREAD_MUTEX_INITIALIZER;

/*variable for handle wake up notifycation and device reset locally*/
extern cmd_req_element_t cmdReqInQueue[COMMAND_SPECIFIC_MAX];
extern uint8_t cmdReqInQueueIndex;
extern cmd_req_element_t lastCmdReqInQueue;

/*variable for handle scheduling task*/
extern scheduling_tast_t handleAppCmdInQueue[COMMAND_SCHEDULING_MAX];
extern uint8_t handleAppCmdInQueueIndex;

/*variable and function for handle wake up notify and device reset locally*/
sTiming txTiming;
uint8_t requestWakeUpStatus = 0xFF;
uint8_t lastRemoveForceStatus = 0xFF;
uint8_t lastWakeUpNoMoreInfoStatus = 0xFF;

send_request_state_t queue_req;

/*========================================================================================================
=================================== Call back of Send command in Queue ===================================
==========================================================================================================*/

void wakeNoMoreInfo_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t)
{
    lastWakeUpNoMoreInfoStatus = bTxStatus;
    mainlog(logDebug, "wakeNoMoreInfo_Compl -> status: %02X", lastWakeUpNoMoreInfoStatus);
    switch (bTxStatus)
    {
    case TRANSMIT_COMPLETE_OK:
    case TRANSMIT_COMPLETE_NO_ACK:
    case TRANSMIT_COMPLETE_FAIL:
    default:
        break;
    }
}
void SendCmdInQueue_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t)

{

    mainlog(logDebug, "SendCmdInQueue_Compl with requestWakeUpStatus: %02X", requestWakeUpStatus);
    NOTIFY_TX_BUFFER_T pTxNotify;

    pTxNotify.CmdReqInQueueNotify.source_endpoint = lastCmdReqInQueue.source_endpoint;
    pTxNotify.CmdReqInQueueNotify.dest_endpoint = lastCmdReqInQueue.dest_endpoint;
    pTxNotify.CmdReqInQueueNotify.source_nodeId = lastCmdReqInQueue.node_id;
    pTxNotify.CmdReqInQueueNotify.dest_nodeId = MyNodeId;
    if (lastCmdReqInQueue.strData)
    {
        strncpy(pTxNotify.CmdReqInQueueNotify.str_data, lastCmdReqInQueue.strData, STR_MAX_LEN);
    }
    pTxNotify.CmdReqInQueueNotify.status = bTxStatus;

    PushNotificationToHandler(CMD_REQ_IN_QUEUE_ZPC_NOTIFY,
                              (uint8_t *)&pTxNotify.CmdReqInQueueNotify, sizeof(cmd_req_in_queue_notify_t));

    requestWakeUpStatus = bTxStatus;
}

/*========================================================================================================
================== This function is callback of serialApiRemoveFailedNode function =======================
==========================================================================================================*/
void RemoveDeviceIsReseted_Compl(uint8_t bStatus)
{
    mainlog(logDebug, "ZW_RemoveFailedNode callback status %02X", bStatus);
    lastRemoveForceStatus = bStatus;
    switch (bStatus)
    {
    case ZW_NODE_OK:
        mainlog(logDebug, "Failed node is not removed    - ZW_NODE_OK"); //, bNodeID);
        break;

    case ZW_FAILED_NODE_REMOVED:
        mainlog(logDebug, "Failed node removed        - ZW_FAILED_NODE_REMOVED"); //, bNodeID);
        break;

    case ZW_FAILED_NODE_NOT_REMOVED:
        mainlog(logDebug, "Failed node not removed    - ZW_FAILED_NODE_NOT_REMOVED"); //, bNodeID);
        break;

    default:
        mainlog(logDebug, "ZW_RemoveFailedNode unknown status %d", bStatus);
        break;
    }
}
extern int nodeSendRequestSpecification(uint8_t flag, uint32_t expected_timeout,
                                        uint8_t node_id, uint8_t *pDataIn, uint8_t pData_lenIn,
                                        uint8_t source_endpoint, uint8_t dest_endpoint, uint8_t scheme, char *strData);
/*========================================================================================================
======== This function will Remove Force deivce when receice reset locally notify from device ============
==========================================================================================================*/

int RemoveDeviceIsReseted(uint8_t node_id)
{
    mainlog(logDebug, "Removing Force Device with node_id: %u", node_id);
    int ret;
    bool timeout = false; //not time out
    uint8_t buf[1];
    buf[0] = COMMAND_CLASS_NO_OPERATION;
    ret = nodeSendRequestSpecification(true, 0, node_id, buf, 1, 0, 0, 0, "");
    if (ret == 0)
    {
        /*device is not failed node -> still working*/
        return -1;
    }
    else if (ret == -2)
    {
        /* device does not exist in network */
        rd_remove_node(node_id);
        return 0;
    }
    else /*device is failed node */
    {
        if (!serialApiIsFailedNode(node_id))
        {
            /*double check fail */
            return -1;
        }

        lastRemoveForceStatus = 0xFF;
        ret = serialApiRemoveFailedNode(node_id, RemoveDeviceIsReseted_Compl);
        if (ret != ZW_FAILED_NODE_REMOVE_STARTED)
        {
            /* controller is not ready */
            return -2;
        }

        timingGetClockSystem(&txTiming);
        while (lastRemoveForceStatus == 0xFF)
        {
            usleep(1000);
            if (timingGetElapsedMSec(&txTiming) > 10000) //10000ms
            {
                timeout = true;
                lastRemoveForceStatus = 0xFE;
                return -1; //timeout
            }
        }

        if (!timeout)
        {
            if (lastRemoveForceStatus == ZW_NODE_OK)
            {
                return -1;
            }
            rd_remove_node(node_id);
            return 0; //ok
        }
    }

    rd_remove_node(node_id);
    return 0;
}

/*========================================================================================================
======== Thread will call this function. This function will call nodeRemoveForce function ===============
==========================================================================================================*/
int DeviceResetLocallyProc(handle_message_from_device_t message_reset_locally_notify)
{
    /*remove device which send device reset locally notify */
    return RemoveDeviceIsReseted(message_reset_locally_notify.p.snode);
}
/*========================================================================================================
========================= Function send command request In queue to sleep device =========================
==========================================================================================================*/

int SendDataInQueue(handle_message_from_device_t message_wake_up_notify, uint8_t index)
{
    ts_param_t p;
    int ret;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = cmdReqInQueue[index].scheme;

    p.sendpoint = cmdReqInQueue[index].source_endpoint;
    p.snode = MyNodeId;

    p.dnode = cmdReqInQueue[index].node_id;
    p.dendpoint = cmdReqInQueue[index].dest_endpoint;

    /* return != -1 if cmdReq in queue is Get command */
    ret = CommandAnalyzerIsGet(cmdReqInQueue[index].buf[0], cmdReqInQueue[index].buf[1]);
    mainlog(logDebug, "CommandAnalyzerIsGet -> %02X%02X, ret -> %d", cmdReqInQueue[index].buf[0], cmdReqInQueue[index].buf[1], ret);

    static uint8_t buf[BUF_MAX_LEN];

    memcpy(buf, cmdReqInQueue[index].buf, cmdReqInQueue[index].length);
    Transport_SendDataApplEP(buf,
                             cmdReqInQueue[index].length,
                             &p,
                             SendCmdInQueue_Compl, 0);

    memcpy((uint8_t *)&lastCmdReqInQueue, (uint8_t *)&cmdReqInQueue[index], sizeof(cmd_req_element_t));
    cmdReqInQueueIndex--;
    memcpy((uint8_t *)&cmdReqInQueue[index], (uint8_t *)&cmdReqInQueue[index + 1], cmdReqInQueueIndex * sizeof(cmd_req_element_t));

    return ret;
}

/*========================================================================================================
================== Send Wake up No more Info to device after 5s after add successfully ===================
==========================================================================================================*/
int SendWakeUpNoMoreInfo(ts_param_t *p)
{
    bool timeout = false; //not time out
    ts_param_t pTxOptionsEx;

    RxToTxOptions(p, &pTxOptionsEx);

    static uint8_t buf[BUF_MAX_LEN];

    buf[0] = COMMAND_CLASS_WAKE_UP;
    buf[1] = WAKE_UP_NO_MORE_INFORMATION;

    lastWakeUpNoMoreInfoStatus = 0xFF;
    if (Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)buf,
                                 sizeof(ZW_WAKE_UP_NO_MORE_INFORMATION_FRAME),
                                 &pTxOptionsEx, wakeNoMoreInfo_Compl, 0))
    {
        timingGetClockSystem(&txTiming);
        while (lastWakeUpNoMoreInfoStatus == 0xFF)
        {
            usleep(1000);
            if (timingGetElapsedMSec(&txTiming) > 12000) //11seconds
            {
                timeout = true;
                timingGetClockSystemStop(&txTiming);
                lastWakeUpNoMoreInfoStatus = 0xFE;
                mainlog(logDebug, "SendWakeUpNoMoreInfo return -1");
                return -1;
            }
        }
        if (!timeout)
        {
            if (lastWakeUpNoMoreInfoStatus == TRANSMIT_COMPLETE_OK)
            {
                mainlog(logDebug, "SendWakeUpNoMoreInfo return 0");
                return 0;
            }
            else
            {
                mainlog(logDebug, "SendWakeUpNoMoreInfo return -1");
                return -1;
            }
        }
    }
    else
    {
        mainlog(logDebug, "SendWakeUpNoMoreInfo return -1");
        /*delay when controller cannot send data */
        sleep(3);
        return -1;
    }
    return 0;
}

/*========================================================================================================
============================ Thread handle wake up notify  from device ===================================
==========================================================================================================*/
int WakeUpNotificationProc(handle_message_from_device_t message_wake_up_notify)
{

    bool timeout; //not time out
    uint8_t index;
    int send_ret;

    while (checkRemainCmdOfSourceNodeInQueue(message_wake_up_notify.p.snode, &index))
    {
        send_ret = SendDataInQueue(message_wake_up_notify, index);
        mainlog(logDebug, "SendDataInQueue -> ret:%d", send_ret);
        timeout = false;
        requestWakeUpStatus = 0xFF;
        timingGetClockSystem(&txTiming); //8000ms
        while (requestWakeUpStatus == 0xFF)
        {
            usleep(1000);
            if (timingGetElapsedMSec(&txTiming) > 8000) //8000ms
            {
                timeout = true;
                timingGetClockSystemStop(&txTiming);
                requestWakeUpStatus = 0xFE;
                mainlog(logWarning, "WARNING: Send data coommand is timeout! Please try again! ");
                mainlog(logDebug, "Send Cmd in Queue return -1");
                break;
            }
        }
        if (!timeout)
        {
            if (requestWakeUpStatus == TRANSMIT_COMPLETE_OK)
            {
                mainlog(logDebug, "Send Cmd in Queue index %u return 0", index);
                if (send_ret != -1)
                {
                    uint16_t cls = CommandAnalyzerExpectReport(send_ret);
                    mainlog(logDebug, "Expect Report: %04X", cls);
                    pthread_mutex_lock(&CriticalMutexHandleNotify);
                    queue_req.state = REQ_SENDING;
                    queue_req.class = (cls >> 8) & 0xFF;
                    queue_req.cmd = (cls >> 0) & 0xFF;
                    queue_req.param.snode = message_wake_up_notify.p.snode;
                    pthread_mutex_unlock(&CriticalMutexHandleNotify);

                    timingGetClockSystem(&txTiming); //8000ms
                    while (queue_req.state == REQ_SENDING)
                    {
                        usleep(1000);
                        if (timingGetElapsedMSec(&txTiming) > 8000) //8000ms
                        {
                            timingGetClockSystemStop(&txTiming);
                            queue_req.state = REQ_DONE;
                            mainlog(logDebug, "QUEUE_WARNING: Cannot get Report ");
                            break;
                        }
                    }
                }

                continue;
            }
            else
            {
                mainlog(logDebug, "Send Cmd in Queue return -2");
                continue;
            }
        }
        else
        {
            mainlog(logDebug, "Send Cmd in Queue timeout");
            continue;
        }
    }

    /*send Wake up no more Infomation*/
    mainlog(logDebug, "p-> rx flags %02X", message_wake_up_notify.p.rx_flags);

    if (((message_wake_up_notify.p.rx_flags & RECEIVE_STATUS_TYPE_MASK) == RECEIVE_STATUS_TYPE_SINGLE) ||
        ((message_wake_up_notify.p.rx_flags & RECEIVE_STATUS_TYPE_MASK) == RECEIVE_STATUS_TYPE_EXPLORE))
    {
        mainlog(logDebug, "SendWakeUpNoMoreInfo");
        return SendWakeUpNoMoreInfo(&message_wake_up_notify.p);
    }
    mainlog(logDebug, "WakeUpNotificationProc return 0");    
    return 0;
}
/*========================================================================================================
================== Function copy handleAppCmdInQueue =====================
==========================================================================================================*/
void LockAndCopyCmdInQueue(void)
{
    pthread_mutex_lock(&CriticalMutexHandleNotify);
    handleAppCmdInQueueIndex--;
    memcpy((uint8_t *)&handleAppCmdInQueue[0], (uint8_t *)&handleAppCmdInQueue[1],
           handleAppCmdInQueueIndex * sizeof(scheduling_tast_t));
    pthread_mutex_unlock(&CriticalMutexHandleNotify);
}

/*========================================================================================================
================== Thread handle wake up notify and device reset locally from device =====================
==========================================================================================================*/
void HandleNotifyThreadProc(void *data)
{
    while (1)
    {
        if (handleAppCmdInQueueIndex > 0) /*have message from device*/
        {
            if (!handleAppCmdInQueue[0].retry)
            {
                LockAndCopyCmdInQueue();
                continue;//go to while (1)
            }

            postEventOperationPollingDev(EV_LIB_HANDLE_WAKEUP_FIRMWARE);
            switch (handleAppCmdInQueue[0].task_type)
            {
            case TT_MESSAGE_WAKE_UP_NOTIFY:
            {
                handleAppCmdInQueue[0].retry--;
                if (!WakeUpNotificationProc(handleAppCmdInQueue[0].handle_message_from_device))
                {
                    LockAndCopyCmdInQueue();
                }
                break;
            }
            case TT_MESSAGE_DEVICE_RESET_LOCALLY:
            {
                handleAppCmdInQueue[0].retry--;
                if (!DeviceResetLocallyProc(handleAppCmdInQueue[0].handle_message_from_device))
                {
                    LockAndCopyCmdInQueue();
                }
                break;
            }
            case TT_WAKE_UP_NO_MORE_INFO:
            {
                if (timingGetElapsedMSec(&handleAppCmdInQueue[0].handle_send_wake_up_no_info.lastTimeRequestInfo) > TIME_DELAY_SEND_WAKE_UP_NO_MORE_INFO) //5000ms
                {
                    timingGetClockSystemStop(&handleAppCmdInQueue[0].handle_send_wake_up_no_info.lastTimeRequestInfo);
                    mainlog(logDebug, "handle send_wakeup noinfo!");
                    ts_param_t p;
                    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
                    p.scheme = handleAppCmdInQueue[0].handle_send_wake_up_no_info.scheme;

                    p.sendpoint = 0;
                    p.snode = handleAppCmdInQueue[0].handle_send_wake_up_no_info.nodeId;

                    p.dnode = MyNodeId;
                    p.dendpoint = 0;

                    handleAppCmdInQueue[0].retry--;
                    if (!SendWakeUpNoMoreInfo(&p))
                    {
                        mainlog(logDebug, "remove send_wakeup noinfo!");
                        LockAndCopyCmdInQueue();
                    }
                }
                break;
            }
            case TT_WAITING_MESSAGE_FROM_HANDLER:
            {
                if (timingGetElapsedMSec(&handleAppCmdInQueue[0].waiting_message_from_handler.lastTimeSendSpecCmd) > TIME_DELAY_WAITING_CMD_FROM_HANDLER) //5000ms
                {
                    timingGetClockSystemStop(&handleAppCmdInQueue[0].waiting_message_from_handler.lastTimeSendSpecCmd);
                    mainlog(logDebug, "waiting message from handler timeout!");
                    PostEventAutoRemoveAdd(NO_ACTION_FROM_HANDLER);
                    handleAppCmdInQueue[0].retry = 0;
                    LockAndCopyCmdInQueue();
                }
                break;
            }
            default:
                break;
            }
            postEventOperationPollingDev(EV_LIB_HANDLE_WAKEUP_DONE);
        }
        else
        {
            /* delay 100ms */
            usleep(100000);
        }
    }
}
/*========================================================================================================
======================= Function check number Cmd Of remain in Queue =====================================
==========================================================================================================*/
bool checkRemainCmdOfSourceNodeInQueue(uint8_t sourceNodeId, uint8_t *index)
{
    uint8_t i = 0;
    if (cmdReqInQueueIndex == 0)
        return false;
    while (i < cmdReqInQueueIndex)
    {
        if (cmdReqInQueue[i].node_id == sourceNodeId)
        {
            *index = i;
            return true;
        }
        i++;
    }
    return false;
}

/*========================================================================================================
======== Function check node Id, device will be send wake up no more infomation remain in Queue ==========
======================= Function will return position element contain nodeId =============================
==========================================================================================================*/
bool checkNodeIdNoMoreInfoInQueue(uint8_t sourceNodeId, uint8_t *index)
{
    uint8_t i = 0;
    while (i < handleAppCmdInQueueIndex)
    {
        if (handleAppCmdInQueue[i].task_type == TT_WAKE_UP_NO_MORE_INFO)
        {
            if (handleAppCmdInQueue[i].handle_send_wake_up_no_info.nodeId == sourceNodeId)
            {
                *index = i;
                return true;
            }
        }

        i++;
    }
    return false;
}

bool checkNodeIdWaitingMessageFromHandlerInQueue(uint8_t sourceNodeId)
{
    uint8_t i = 0;
    while (i < handleAppCmdInQueueIndex)
    {
        if (handleAppCmdInQueue[i].task_type == TT_WAITING_MESSAGE_FROM_HANDLER)
        {
            if (handleAppCmdInQueue[i].waiting_message_from_handler.nodeId == sourceNodeId)
            {
                timingGetClockSystem(&handleAppCmdInQueue[i].waiting_message_from_handler.lastTimeSendSpecCmd); //6000ms
                return true;
            }
        }
        i++;
    }
    return false;
}
/*========================================================================================================
================== Function check node Id, Reset Timer Send Wakup No More Infomation =====================
==========================================================================================================*/
void CheckNodeIdAndResetTimerSendWakupNoMoreInfo(uint8_t node_id)
{
    uint8_t index;
    if (checkNodeIdNoMoreInfoInQueue(node_id, &index))
    {
        /*reset timer*/
        timingGetClockSystem(&handleAppCmdInQueue[index].handle_send_wake_up_no_info.lastTimeRequestInfo); //5000ms
    }
}