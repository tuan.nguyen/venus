#ifndef _AGI_H_
#define _AGI_H_

#include "stdint.h"
#include "string.h"
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"

typedef struct _CMD_CLASS_GRP_
{
    uint8_t cmdClass;
    uint8_t cmd;
} CMD_CLASS_GRP;

typedef struct _NODE_LIST_
{
    uint8_t         *pNodeList;
    uint8_t         len;
    uint8_t         pCurrentNode;
    CMD_CLASS_GRP   *pCurentCmdGrp;     
}NODE_LIST;

typedef struct _AGI_PROFILE_
{
    uint8_t profile_MS;
    uint8_t profile_LS;
}AGI_PROFILE;

typedef struct  _AGI_GROUP_
{
    uint8_t         profile_MS;
    uint8_t         profile_LS;
    CMD_CLASS_GRP   cmdGrp;
    char            groupName[42];
}AGI_GROUP;

void    AGI_LifeLineGroupSetup( CMD_CLASS_GRP* pCmdGrpList, uint8_t listSize);
void    AGI_ResourceGroupSetup(AGI_GROUP* pTable, uint8_t tableSize, uint8_t endpoint);
void    GetApplGroupName(uint8_t* pGroupName,uint8_t groupId);
uint8_t GetApplGroupNameLength(uint8_t groupId);
uint8_t GetApplAssoGroupsSize(void);
uint8_t ApplicationGetLastActiveGroupId(void);
void    GetApplGroupInfo(uint8_t groupId, VG_ASSOCIATION_GROUP_INFO_REPORT_VG* report);
uint8_t getApplGroupCommandListSize(uint8_t groupId);
void    setApplGroupCommandList(uint8_t* pGroupList,uint8_t groupId);

#endif


