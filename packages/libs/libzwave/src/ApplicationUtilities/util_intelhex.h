#ifndef _UTIL_INTELHEX_H_
#define _UTIL_INTELHEX_H_

#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define FLASH_MEM_LENGTH 0x20000
#define BLANK_VALUE     0xFF
int intelHex_loadfile(char *filename, uint8_t *flashMem, uint32_t *length_file_ota);
void calculateCrc(uint8_t* flashDataRaw);

#endif