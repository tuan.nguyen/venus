#ifndef NODECACHE_H_
#define NODECACHE_H_

#include "platform_cc.h" 
#include <stdint.h>


#define NODE_FLAG_SECURITY0                     0x01
#define NODE_FLAG_KNOWN_BAD                     0x02
#define NODE_FLAG_JUST_ADDED                    0x04
#define NODE_FLAG_INFO_ONLY                     0x08 /* Only probe the node info */
#define NODE_FLAG_SECURITY2_UNAUTHENTICATED     0x10
#define NODE_FLAG_SECURITY2_AUTHENTICATED       0x20
#define NODE_FLAG_SECURITY2_ACCESS              0x40
#define NODE_FLAG_REPLICATION_MODE              0x80


#define NODE_FLAGS_SECURITY (                   \
    NODE_FLAG_SECURITY0 |                       \
    NODE_FLAG_KNOWN_BAD|                        \
    NODE_FLAG_SECURITY2_UNAUTHENTICATED|        \
    NODE_FLAG_SECURITY2_AUTHENTICATED|          \
    NODE_FLAG_SECURITY2_ACCESS)

#define NODE_FLAGS_SECURITY2 (                  \
    NODE_FLAG_SECURITY2_UNAUTHENTICATED|        \
    NODE_FLAG_SECURITY2_AUTHENTICATED|          \
    NODE_FLAG_SECURITY2_ACCESS)


/**
 * Set node attribute flags
 */
uint8_t SetCacheEntryFlagMasked(uint8_t nodeid,uint8_t value, uint8_t mask)CC_REENTRANT_ARG;
uint8_t GetCacheEntryFlag(uint8_t nodeid) CC_REENTRANT_ARG;

#define  isNodeBad(n) ((GetCacheEntryFlag(n) & NODE_FLAG_KNOWN_BAD) !=0)
#define  isNodeSecure(n) ( (GetCacheEntryFlag(n) & (NODE_FLAG_SECURITY0 | NODE_FLAG_KNOWN_BAD)) == NODE_FLAG_SECURITY0)


#endif /* NODECACHE_H_ */
