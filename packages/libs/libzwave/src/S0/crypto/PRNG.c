#include "PRNG.h"
//#include "aes.h"
#include "serialAPI.h"


static uint8_t prngState[16];
static uint8_t k[16], h[16], ltemp[16], btemp[16], i, j;


#define ZW_AES_ECB(key, inputDat, outputDat) serialApiAES128Encrypt(inputDat, outputDat, key);


/*================================   AESRaw   ===============================
 **    AES Raw
 **
 **    Side effects :
 **
 **--------------------------------------------------------------------------*/
/*
 Declaration: void AESRaw(BYTE *pKey, BYTE *pSrc, BYTE *pDest)
 Called: When individual 128-bit blocks of data have to be encrypted
 Arguments: pKey Pointer to key (input; fixed size 16 bytes)
 pSrc Pointer to source data (input; fixed size 16 bytes)
 pDest Pointer to destination buffer (output; fixed size
 16 bytes)
 Return value: None
 Global vars: None affected
 Task: Encrypts 16 bytes of data at pSrc, using Raw AES and the key at pKey. The
 16-byte result is written to pDest.*/
void AESRaw(uint8_t *pKey, uint8_t *pSrc, uint8_t *pDest)
{
    memcpy(pDest, pSrc, 16);
    ZW_AES_ECB(pKey, pSrc, pDest);
}


/*==============================   PRNGUpdate   ==============================
**    PRNGUpdate
**
**    Side effects :
**
**--------------------------------------------------------------------------*/
/*
Declaration: void PRNGUpdate()
Called: When fresh input from hardware RNG is needed
Arguments: None
Return value: None
Global vars: prngState[0..15] Modify
Temp data: BYTE k[16], h[16], ltemp[16], i, j
Task: Incorporate new data from hardware RNG into the PRNG State
*/
void PRNGUpdate(void)
{
    /* H = 0xA5 (repeated x16) */
    memset((uint8_t *)h, 0xA5, 16);
    /* The two iterations of the hardware generator */
    for (j = 0; j <= 1; j++)
    {
        /* Random data to K */
        GetRNGData(k, 16);
        /* ltemp = AES(K, H) */
        AESRaw(k, h, ltemp);
        /* H = AES(K, H) ^ H */
        for (i = 0; i <= 15; i++)
        {
            h[i] ^= ltemp[i];
        }
    }
    /* Update inner state */
    /* S = S ^ H */
    for (i = 0; i <= 15; i++)
    {
        prngState[i] ^= h[i];
    }
    /* ltemp = 0x36 (repeated x16) */
    memset((uint8_t *)ltemp, 0x36, 16);
    /* S = AES(S, ltemp) */
    AESRaw(prngState, ltemp, btemp);
    memcpy(prngState, btemp, 16);

}

bool ZW_GetRandomWord(uint8_t * randomWord)
{
   uint16_t rn;
   srand (time(NULL));
   rn=(rand()&0xFFFF);

   randomWord[0]=(rn>>8)&0xFF;
   randomWord[1]=(rn>>0)&0xFF;
   return true;
}



void GetRNGData(uint8_t *pRNDData,uint8_t noRNDDataBytes)
{
    i = 0;
    do
    {
        ZW_GetRandomWord((uint8_t *) (pRNDData + i));
        i += 2;
    } while (--noRNDDataBytes && --noRNDDataBytes);
}

void PRNGInit(void)
{
    /* Reset PRNG State */
    memset(prngState, 0, 16);
    /* Update PRNG State */
    PRNGUpdate();
}
/*=============================   PRNGOutput   ===============================
**    PRNGOutput
**
**    Side effects :
**
**--------------------------------------------------------------------------*/
/*
Declaration: void PRNGOutput(BYTE *pDest)
Called: When 8 bytes of output are needed.
Arguments: pDest Pointer to output data (output; always 8 bytes)
Return value: none
Global vars: prngState[0..15] Modify
Temp data: BYTE ltemp[16]
Task: Generate pseudo-random output data and update PRNG state
*/
void PRNGOutput(uint8_t *pDest)
{
    PRNGUpdate();
    /* Generate output */
    /* ltemp = 0x5C (repeated x16) */
    memset((uint8_t *)ltemp, 0x5C/*0xA5*/, 16);
    /* ltemp = AES(PRNGState, ltemp) */
    AESRaw(prngState, ltemp, btemp);
    /* pDest[0..7] = ltemp[0..7] */
    memcpy(pDest, btemp, 8);
    /* Generate next internal state */
    /* ltemp = 0x36 (repeated x16) */
    memset((uint8_t *)ltemp, 0x36, 16);
    /* PRNGState = AES(PRNGState, ltemp) */
    AESRaw(prngState, ltemp, btemp);
    memcpy(prngState, btemp, 16);
}



