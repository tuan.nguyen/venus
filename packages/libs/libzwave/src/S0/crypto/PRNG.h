#ifndef _PRNG_H_
#define _PRNG_H_

#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <stdint.h>
#include <stdbool.h>
#include <string.h>


void PRNGUpdate(void);
void GetRNGData(uint8_t *pRNDData,uint8_t noRNDDataBytes);
void PRNGInit(void);
void PRNGOutput(uint8_t *pDest);


#endif