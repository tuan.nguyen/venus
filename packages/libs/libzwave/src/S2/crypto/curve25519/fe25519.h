#ifndef FE25519_H
#define FE25519_H

#include <stdint.h>


typedef struct
{
	uint8_t value[32];
}
fe25519_t;

void fe25519_freeze(fe25519_t *r);
void fe25519_unpack(fe25519_t *r, const uint8_t x[32]);
void fe25519_pack(uint8_t r[32], const fe25519_t *x);
int fe25519_iszero(const fe25519_t *x);
int fe25519_iseq_vartime(const fe25519_t *x, const fe25519_t *y);
void fe25519_cmov(fe25519_t *r, const fe25519_t *x, uint8_t b);
void fe25519_setone(fe25519_t *r);
void fe25519_setzero(fe25519_t *r);
void fe25519_neg(fe25519_t *r, const fe25519_t *x);
uint8_t fe25519_getparity(const fe25519_t *x);
void fe25519_add(fe25519_t *r, const fe25519_t *x, const fe25519_t *y);
void fe25519_sub(fe25519_t *r, const fe25519_t *x, const fe25519_t *y);
void fe25519_mul(fe25519_t *r, const fe25519_t *x, const fe25519_t *y);
void fe25519_square(fe25519_t *r, const fe25519_t *x);
void fe25519_invert(fe25519_t *r, const fe25519_t *x);
void fe25519_pow2523(fe25519_t *r, const fe25519_t *x);

#endif
