/**
 * @file
 */
/******************************************************************************
 * Copyright AllSeen Alliance. All rights reserved.
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ******************************************************************************/

#include <string.h>
#include <stdint.h>
#include "ctr_drbg.h"
#include "aes.h"
#include "utils.h"


void AJ_AES_ECB_128_ENCRYPT(uint8_t* key, uint8_t* in, uint8_t* out)
{
    AES128_ECB_encrypt(in, key, out);
}
static void AES_CTR_DRBG_Increment(uint8_t* __data, size_t size)
{
    while (size--) 
    {
        __data[size]++;
        if (__data[size]) 
        {
            break;
        }
    }
}

/*
CTR_DRBG_Update (provided_data, Key, V):
1.provided_data: The data to be used. This must be exactly seedlen
bits in length; this length is guaranteed by the construction of the
provided_data in the instantiate, reseed and generate functions.
2.Key: The current value of Key.
3.V: The current value of V.

Output:
1.K: The new value for Key.
2.V: The new value for V.
*/

static void AES_CTR_DRBG_Update(CTR_DRBG_CTX* ctx, uint8_t __data[SEEDLEN])
{
    size_t i = 0;
    uint8_t tmp[SEEDLEN];
    uint8_t* t = tmp;

    for (i = 0; i < SEEDLEN; i += OUTLEN) 
    {
        AES_CTR_DRBG_Increment(ctx->v, OUTLEN); /*V= (V+ 1) mod 2 pow(outlen) */
        AJ_AES_ECB_128_ENCRYPT(ctx->k, ctx->v, t); /* output_block =  Block_Encrypt(Key, V). */
        t += OUTLEN; /*temp = temp || ouput_block */
    }

    for (i = 0; i < SEEDLEN; i++) 
    {
        /* temp = Leftmost seedlen bits of temp.
           temp = temp || provided_data;
        */
        tmp[i] ^= __data[i];
    }

    memcpy(ctx->k, tmp, KEYLEN);

    /* for debugging */
    /*
    mainlog(logDebug,"key: ");
    hexdump(ctx->k,KEYLEN);
    */

	memcpy(ctx->v, tmp + KEYLEN, OUTLEN);

    /*for debugging*/
    /*
	mainlog(logDebug,"v: ");
	hexdump(ctx->v,OUTLEN);
    */
}




void AES_CTR_DRBG_Reseed(CTR_DRBG_CTX* ctx, uint8_t* seed)
{
    AES_CTR_DRBG_Update(ctx, seed);
}


void AES_CTR_DRBG_Instantiate(CTR_DRBG_CTX* ctx, uint8_t* entropy, const uint8_t* personal)
{
    uint8_t i;
    for (i = 0; i < SEEDLEN; i++) 
    {
        /* temp = Leftmost seedlen bits of temp.
           temp = temp || provided_data;
        */
        entropy[i] ^= personal[i];
    }

    memset(ctx->k, 0, KEYLEN);
    memset(ctx->v, 0, OUTLEN);
    ctx->df = 0;
    AES_CTR_DRBG_Reseed(ctx, entropy);
}


int AES_CTR_DRBG_Generate(CTR_DRBG_CTX* ctx, uint8_t* rand)
{
    uint8_t __data[SEEDLEN];
    size_t copy;
    size_t size = RANDLEN;

    /* Reseed interval 2^32 (counter wraps to zero)
     See section 10.2.1.5.1. Step 1 in "CTR_DRBG Generate Process"*/
    /*AJ_AES_Enable(ctx->k);*/
    while (size) 
    {
        AES_CTR_DRBG_Increment(ctx->v, OUTLEN);
        AJ_AES_ECB_128_ENCRYPT(ctx->k, ctx->v, __data);
        copy = (size < OUTLEN) ? size : OUTLEN;
        memcpy(rand, __data, copy);
        rand += copy;
        size -= copy;
    }

    memset(__data, 0, SEEDLEN);
    AES_CTR_DRBG_Update(ctx, __data);

    return 0;
}