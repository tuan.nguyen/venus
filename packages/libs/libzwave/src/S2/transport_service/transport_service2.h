#ifndef TRANSPORT_SERVICE2_H_
#define TRANSPORT_SERVICE2_H_

#include <stdio.h>
#include "ZW_SendDataAppl.h" /* for ts_param_t */
#include "transport_service2_fsm.h"

#include "zw_serialapi/zw_classcmd.h" /* for ZW_APPLICATION_TX_BUFFER */
#include "transport_service2_external.h"


extern TRANSPORT2_ST_T current_state;


#define FRAGMENT_FC_TIMEOUT          1000 /*ms*/
#define FRAGMENT_RX_TIMEOUT          800 /*ms*/

//#define DATAGRAM_SIZE_MAX       (UIP_BUFSIZE - UIP_LLH_LEN) /*1280*/
#define DATAGRAM_SIZE_MAX       (200) /*1280*/





#endif /* TRANSPORT_SERVICE2_H_ */

