
#include <string.h>
#include "node_cache.h"
#include "S2.h"
#include "S2_inclusion.h"
#include "s2_keystore.h"
#include "s2_classcmd.h"

#include "S2_wrap.h"
#include "s2_protocol.h"
#include "timer.h"
#include "curve25519.h"
#include "S2_wrap.h"
#include "timer.h"
#include "utils.h"
#include "clock.h"
#include "serialAPI.h"
#include "ZW_TransportEndpoint.h"
#include "cmd_class_misc.h"
#include "zw_api.h"
#include "nvm.h"
#include "config.h"
#include "memb.h"
#include "ZW_NetworkManagement.h"

//#define VALGRIND_TEST

#ifdef VALGRIND_TEST
    #define NONCE_REP_TIME 10000
#elif defined STANDALONE
    #define NONCE_REP_TIME 250
#else
    #define NONCE_REP_TIME 500
#endif

#define S2_NUM_KEY_CLASSES 4 /* Includes the S0 key */

struct S2 *s2_ctx;

/**
 * Multicast transmission
 */
struct 
{
    uint8_t l_node;
    const uint8_t* node_list;
    uint8_t  node_list_len;
    const uint8_t* data;
    uint8_t  data_len;
    enum {MC_SEND_STATE_IDLE,MC_SEND_STATE_SEND_FIRST,MC_SEND_STATE_SEND} state;
} mc_state;


clock_time_t transmit_start_time;

static timer_t s2_timer = 0L;
static timer_t s2_inclusion_timer = 0L;
static sec2_inclusion_cb_t sec_incl_cb;
static void *s2_send_user;
static ZW_SendDataAppl_Callback_t s2_send_callback;

/* Holds the TX_STATUS_TYPE of ZW_SendDataXX() callback for the most recent S2 frame */
static TX_STATUS_TYPE s2_send_tx_status;
//static TX_STATUS_TYPE zeroed_tx_status_type; /* static are always initialized to zero*/


extern void ZW_SendRequest_Callback(uint8_t status, TX_STATUS_TYPE *psTxInfo);


static uint8_t
keystore_flags_2_node_flags(uint8_t key_store_flags)
{
    uint8_t flags = 0;

    if (key_store_flags & KEY_CLASS_S0)
    {
        flags |= NODE_FLAG_SECURITY0;
    }

    if (key_store_flags & KEY_CLASS_S2_ACCESS)
    {
        flags |= NODE_FLAG_SECURITY2_ACCESS;
    }

    if (key_store_flags & KEY_CLASS_S2_AUTHENTICATED)
    {
        flags |= NODE_FLAG_SECURITY2_AUTHENTICATED;
    }

    if (key_store_flags & KEY_CLASS_S2_UNAUTHENTICATED)
    {
        flags |= NODE_FLAG_SECURITY2_UNAUTHENTICATED;
    }
    return flags;
}

int sec2_dsk_accept(uint8_t accept, uint8_t *dsk, uint8_t len)
{
    s2_inclusion_challenge_response(s2_ctx, accept, dsk, len);
    return 0;
}

int sec2_key_grant(uint8_t accept, uint8_t keys, uint8_t csa)
{
    s2_inclusion_key_grant(s2_ctx, accept, keys, csa);
    return 0;
}


uint8_t ecdh_dynamic_key[32];

void sec2_create_new_static_ecdh_key(void) 
{
    sec2_create_new_dynamic_ecdh_key();
    nvm_config_set(ecdh_priv_key,ecdh_dynamic_key);
    sec2_create_new_dynamic_ecdh_key();
}


void sec2_create_new_dynamic_ecdh_key()
{
    AES_CTR_DRBG_Generate(&s2_ctr_drbg, ecdh_dynamic_key);
    AES_CTR_DRBG_Generate(&s2_ctr_drbg, &ecdh_dynamic_key[16]);
}


uint8_t sec2_get_my_node_flags()
{
    uint8_t flags;
    nvm_config_get(assigned_keys, &flags);

    return keystore_flags_2_node_flags(flags);
}

void sec2_create_new_network_keys()
{
    uint8_t net_key[16];
    int class_id;

    mainlog(logDebug, "Creating new network keys");

    for (class_id = 0; class_id < S2_NUM_KEY_CLASSES - 1; class_id++)
    {
        S2_get_hw_random(net_key, 16);
        keystore_network_key_write(1 << class_id, net_key);

        /*for debugging */
        mainlog(logDebug, "S2_get_hw_random -- netkey class_id: %i", class_id);
        hexdump(net_key, 16);
    }

    /* Clearing of memory to ensure key is not exposed accidentially elsewhere.*/
    memset(net_key, 0, sizeof(net_key));
}

static void sec2_event_handler(zwave_event_t *ev)
{
    //uint16_t response;
    uint8_t flags;

    switch (ev->event_type)
    {
    case S2_NODE_INCLUSION_INITIATED_EVENT:
        sec0_abort_inclusion(); //Here we abort S0 inclusion
        mainlog(logDebug, "s2_node_inclusion_initiated_event, s2 s2_connection_t->peer node id %d",ev->evt.s2_event.peer.r_node);

        break;

    case S2_NODE_INCLUSION_PUBLIC_KEY_CHALLENGE_EVENT:
    {
        NetworkManagement_dsk_challenge(&(ev->evt.s2_event.s2_data.challenge_req));
        break;
    }

    case S2_NODE_INCLUSION_KEX_REPORT_EVENT:
        mainlog(logDebug, "s2_node_inclusion_kex_report_event");
        mainlog(logDebug, "csa %i keys %i\n", ev->evt.s2_event.s2_data.kex_report.csa, ev->evt.s2_event.s2_data.kex_report.security_keys);

        NetworkManagement_key_request(&ev->evt.s2_event.s2_data.kex_report);
        break;

    case S2_NODE_INCLUSION_COMPLETE_EVENT:
        mainlog(logDebug, "s2_node_inclusion_complete_event");
        //< Security 2 event, inclusion of the node  to the network has been completed.
        if (sec_incl_cb)
        {
            sec_incl_cb(keystore_flags_2_node_flags(ev->evt.s2_event.s2_data.inclusion_complete.exchanged_keys));
        }
        break;

    case S2_NODE_JOINING_COMPLETE_EVENT:
        mainlog(logDebug, "s2_node_inclusion_complete_event");
        nvm_config_get(assigned_keys, &flags);
        if (sec_incl_cb)
        {
            sec_incl_cb(keystore_flags_2_node_flags(flags));
        }
        break;

    case S2_NODE_INCLUSION_FAILED_EVENT:
        mainlog(logDebug, "s2_node_inclusion_failed_event");
        //< Security 2 event, inclusion of the node  to the network has failed. See the details
        if (sec_incl_cb)
        {
            sec_incl_cb(NODE_FLAG_KNOWN_BAD | (ev->evt.s2_event.s2_data.inclusion_fail.kex_fail_type << 16));
        }
        break;

    /*TRIDO*/
    case S2_NODE_INCLUSION_PUBLIC_KEY_SHOW_UP_EVENT:
        mainlog(logDebug, "s2 public_key_show_up_event");
        //< Security 2 event, inclusion of the node  to the network has failed. See the details
        PushNotificationToHandler(S2_PUBLIC_KEY_SHOW_UP_NOTIFY, (uint8_t *)&ev->evt.s2_event.s2_data.key_show_up, S2_EVT_MAX_BUFFER_SIZE);
        break;
    }
}

static void incl_timeout(void *ctxt)
{
    mainlog(logDebug, "incl_timeout\n");
    s2_inclusion_notify_timeout((struct S2 *)ctxt);
}

static void S2_send_frame_callback(uint8_t txStatus, void* user, TX_STATUS_TYPE *t)
{
    /* If this is non-zeroed we are overwriting a non-processed TX_STATUS */
    /* ... except during network management we dont except anyone to process it */

    /*TRIDO*/
    //memcmp(&s2_send_tx_status, &zeroed_tx_status_type, sizeof (s2_send_tx_status));

    if (t) 
    {
        s2_send_tx_status = *t;
    }

    S2_send_frame_done_notify((struct S2 *)user,
                              txStatus == TRANSMIT_COMPLETE_OK ? S2_TRANSMIT_COMPLETE_OK : S2_TRANSMIT_COMPLETE_NO_ACK,
                              clock_time() - transmit_start_time + NONCE_REP_TIME);
}
/**
 * TODO
 */
uint8_t S2_send_frame_multi(struct S2 *ctxt, s2_connection_t *conn, uint8_t *buf, uint16_t len)
{
    ts_param_t p;
    ts_set_std(&p, 0);
    p.snode = conn->l_node;
    p.dnode = NODE_BROADCAST;
    p.tx_flags = conn->zw_tx_options;

    mainlog(logDebug, "Sending S2_send_frame_multi - len: %i", len);
    transmit_start_time = clock_time();
    return send_data(&p, buf, len, S2_send_frame_callback, ctxt);
    
}

void S2_get_commands_supported(uint8_t lnode, uint8_t class_id, const uint8_t **cmdClasses, uint8_t *length)
{
    uint8_t net_scheme;

    int flags = GetCacheEntryFlag(MyNodeId);

    net_scheme = highest_scheme(flags & 0xFF);

    if(
      ((net_scheme == SECURITY_SCHEME_2_ACCESS) && class_id==2 ) ||
      ((net_scheme == SECURITY_SCHEME_2_AUTHENTICATED) && class_id==1 ) ||
      ((net_scheme == SECURITY_SCHEME_2_UNAUTHENTICATED) && class_id==0 )
    ) {
        if(lnode == MyNodeId) 
        {
            /* set supported command classes in S2 */
            *length = myZPCCapability.node_capability_secureV2.noCapability;
            *cmdClasses = myZPCCapability.node_capability_secureV2.aCapability;
        }
        return;
    }

    *length = 0;
}

/**
 * Emitted when a messages has been received and decrypted
 */
void S2_msg_received_event(struct S2 *ctxt, s2_connection_t *src, uint8_t *buf, uint16_t len)
{
    ts_param_t p;
    const int scheme_map[] = {
        SECURITY_SCHEME_2_UNAUTHENTICATED,
        SECURITY_SCHEME_2_AUTHENTICATED,
        SECURITY_SCHEME_2_ACCESS};
    ts_set_std(&p, 0);

    p.scheme = scheme_map[src->class_id];
    p.snode = src->r_node;
    p.dnode = src->l_node;

    mainlog(logDebug, "S2 message received!");

    Transport_ApplicationCommandHandler(&p, buf, len);
    return;
}

/**
 * Emitted when the security engine is done.
 * Note that this is also emitted when the security layer sends a SECURE_COMMANDS_SUPPORTED_REPORT
 */
void S2_send_done_event(struct S2 *ctxt, s2_tx_status_t status)
{
    mainlog(logDebug, "S2_send_done_event status %i\n", status);

    const uint8_t s2zw_codes[] = {TRANSMIT_COMPLETE_OK, TRANSMIT_COMPLETE_OK, TRANSMIT_COMPLETE_NO_ACK, TRANSMIT_COMPLETE_FAIL};
    ZW_SendDataAppl_Callback_t cb_save;

    if (s2_timer != 0)
    {
        timerCancel(&s2_timer);
        s2_timer = 0;
    }

    if(mc_state.state != MC_SEND_STATE_IDLE) 
    {
        if( mc_state.node_list_len ==0) 
        {
            mc_state.state = MC_SEND_STATE_IDLE;
            mainlog(logDebug,"Multicast transmission is done");
        } 
        else 
        {
            s2_connection_t s2_con;

            s2_con.l_node = mc_state.l_node;
            s2_con.r_node = *mc_state.node_list;
            s2_con.zw_tx_options = TRANSMIT_OPTION_ACK | TRANSMIT_OPTION_AUTO_ROUTE | TRANSMIT_OPTION_EXPLORE ;
            s2_con.tx_options = (mc_state.state ==MC_SEND_STATE_SEND)  ?
                        S2_TXOPTION_SINGLECAST_FOLLOWUP  | S2_TXOPTION_VERIFY_DELIVERY :
                        S2_TXOPTION_FIRST_SINGLECAST_FOLLOWUP | S2_TXOPTION_VERIFY_DELIVERY;
            s2_con.class_id = 0;

            mc_state.node_list++;
            mc_state.node_list_len--;
            
            mainlog(logDebug,"Sending Multicast followup to node %i",s2_con.r_node);
            mc_state.state = MC_SEND_STATE_SEND;

            if(S2_send_data(s2_ctx,&s2_con,mc_state.data,mc_state.data_len)) 
            {
                return;
            } 
            else 
            {
                status = S2_TRANSMIT_COMPLETE_FAIL;
                mc_state.state = MC_SEND_STATE_IDLE;
            }
        }
    }

    cb_save = s2_send_callback;
    s2_send_callback = 0;
    if (cb_save)
    {
        cb_save(s2zw_codes[status], s2_send_user, &s2_send_tx_status);
    }else
    {
        /*TRIDO*/
        ZW_SendRequest_Callback(s2zw_codes[status], NULL);
    }
    memset(&s2_send_tx_status, 0, sizeof s2_send_tx_status);
}

/** Must be implemented elsewhere maps to ZW_SendData or ZW_SendDataBridge note that ctxt is
 * passed as a handle. The ctxt MUST be provided when the \ref S2_send_frame_done_notify is called */
uint8_t S2_send_frame(struct S2 *ctxt, const s2_connection_t *conn, uint8_t *buf, uint16_t len)
{
    ts_param_t p;
    ts_set_std(&p, 0);
    p.snode = conn->l_node;
    p.dnode = conn->r_node;
    p.tx_flags = conn->zw_tx_options;

    mainlog(logDebug, "Sending S2_send_frame - len: %i", len);

    transmit_start_time = clock_time();
    return send_data(&p, buf, len, S2_send_frame_callback, ctxt);
}

typedef struct
{
    uint8_t buf[50];  /* TODO: This should be enough for a nonce report. Can be shrunk to save memory. */
} nonce_rep_tx_t;

/**
 *  This memblock caches outgoing nonce reports so we dont have to care if s2 overwrites the single buffer in there
 */
MEMB(nonce_rep_memb, nonce_rep_tx_t, 8);

/**
 * Callback function for S2_send_frame_no_cb()
 *
 * Cleans up the transmission
 */
static void nonce_rep_callback(uint8_t txStatus, void* user, TX_STATUS_TYPE *t)
{
    nonce_rep_tx_t *m = (nonce_rep_tx_t*)user;
    if(txStatus != TRANSMIT_COMPLETE_OK) 
    {
        mainlog(logDebug, "Nonce rep tx failed, status %u, seqno %u", txStatus, m->buf[2]);
    }
    memb_free(&nonce_rep_memb, m);
}

/** Does not provide callback to libs2 (because send_done can be confused with Msg Encapsulations)
 *  Caches the buf pointer outside libs2 so we dont have to care if s2 reuses the single buffer in there.
 *
 * Must be implemented elsewhere maps to ZW_SendData or ZW_SendDataBridge note that ctxt is
 * passed as a handle. The ctxt MUST be provided when the \ref S2_send_frame_done_notify is called */
uint8_t S2_send_frame_no_cb(struct S2* ctxt,const s2_connection_t* conn, uint8_t* buf, uint16_t len) 
{
    ts_param_t p;
    nonce_rep_tx_t *m;
    m = memb_alloc(&nonce_rep_memb);

    if (m == 0)
    {
        mainlog(logDebug, "OMG! No more queue space for nonce reps\n");
        return false;
    }

    ts_set_std(&p,0);
    p.snode = conn->l_node;
    p.dnode = conn->r_node;
    p.tx_flags = conn->zw_tx_options;

    if(len > sizeof(m->buf)) 
    {
        mainlog(logDebug, "nonce_rep_tx_t buf is too small, needed %u\n", len);
    }
    memcpy(m->buf, buf, len);

    mainlog(logDebug, "S2_send_frame_no_cb len %i, seqno %u\n", len, m->buf[2]);
    return send_data(&p, buf,  len, nonce_rep_callback, m);
}



void sec2_start_add_node(uint8_t node_id, sec2_inclusion_cb_t cb)
{
    s2_connection_t s2_con;

    //ZW_SendDataAppl_Callback_t cb_save;

    s2_con.l_node = MyNodeId;
    s2_con.r_node = node_id;
    s2_con.zw_tx_options = TRANSMIT_OPTION_ACK | TRANSMIT_OPTION_AUTO_ROUTE | TRANSMIT_OPTION_EXPLORE;

    sec_incl_cb = cb;
    s2_inclusion_including_start(s2_ctx, &s2_con);
}

uint8_t s2_inclusion_set_timeout(struct S2 *ctxt, uint32_t interval)
{
    mainlog(logDebug, "s2_inclusion_set_timeout interval = %i ms -- s2_inclusion_timer: %ld, \n", interval, (long)s2_inclusion_timer);

    if (s2_inclusion_timer != 0L)
    {
        mainlog(logDebug, "s2_inclusion_set_timeout timerCancel");
        timerCancel(&s2_inclusion_timer);
        s2_inclusion_timer = 0L;
    }
    timerStart(&s2_inclusion_timer, incl_timeout, (void *)ctxt, interval, TIMER_ONETIME);

    return 0;
}

/**
 * Get a number of bytes from a hardware random number generator
 */
void S2_get_hw_random(uint8_t *buf, uint8_t len)
{
    uint8_t rnd[8];
    int n, left, pos;

    left = len;
    pos = 0;
    while (left > 0)
    {
        serialApiGetRandomWord(rnd, false);
        n = left > 8 ? 8 : left;
        memcpy(&buf[pos], rnd, n);
        left -= n;
        pos += n;
    }
}

void security2_CommandHandler(ts_param_t *p, const ZW_APPLICATION_TX_BUFFER *pCmd, uint16_t cmdLength) /* IN Number of command bytes including the command */
{
    const uint8_t s2_cap_report[] = {COMMAND_CLASS_SECURITY_2, SECURITY_2_CAPABILITIES_REPORT, 0};

    s2_connection_t conn;
    conn.r_node = p->snode;
    conn.l_node = p->dnode;
    conn.zw_tx_options = p->tx_flags;
    conn.zw_rx_status = p->rx_flags;
    conn.tx_options = 0;

    conn.rx_options = p->rx_flags & (RECEIVE_STATUS_TYPE_BROAD | RECEIVE_STATUS_TYPE_MULTI) ? S2_RXOPTION_MULTICAST : 0;

    if ((pCmd->ZW_Common.cmd == SECURITY_2_CAPABILITIES_GET) && (p->scheme == SECURITY_SCHEME_2_ACCESS))
    {
        S2_send_data(s2_ctx, &conn, s2_cap_report, sizeof(s2_cap_report));
    }
    else
    {
        S2_application_command_handler(s2_ctx, &conn, (uint8_t *)pCmd, cmdLength);
    }
}

uint8_t sec2_send_data(ts_param_t *p, uint8_t *data, uint16_t len, ZW_SendDataAppl_Callback_t callback, void *user)
{
    s2_connection_t s2_con;

    ZW_SendDataAppl_Callback_t cb_save;
    void *user_save;

    s2_con.l_node = p->snode;
    s2_con.r_node = p->dnode;
    s2_con.zw_tx_options = p->tx_flags;
    s2_con.tx_options = 0;

    /* This enables mailbox to set S2_TXOPTION_VERIFY_DELIVERY which waits for the sleeping ndoe to send SOS/resync if
    needed. It puts delay of 500ms, but only for first frame popped out of Mailbox after the node is woken up*/
    if ( -1 != CommandAnalyzerIsGet(data[0], data[1]))
    {
        s2_con.tx_options |= S2_TXOPTION_VERIFY_DELIVERY;
    }

    s2_con.class_id = p->scheme - SECURITY_SCHEME_2_UNAUTHENTICATED;

    cb_save = callback;
    user_save = s2_send_user;

    s2_send_callback = callback;
    s2_send_user = user;

    if (S2_send_data(s2_ctx, &s2_con, data, len))
    {
        return 1;
    }
    else
    {
        s2_send_callback = cb_save;
        s2_send_user = user_save;
        return 0;
    }
}

uint8_t sec2_send_multicast(uint8_t src_node, const uint8_t* node_list, uint8_t node_list_len,const uint8_t * data, uint8_t data_len,ZW_SendDataAppl_Callback_t callback) 
{
    s2_connection_t s2_con;
    uint8_t group_id =0x42;

    if(mc_state.state == MC_SEND_STATE_IDLE) 
    {
        mc_state.l_node = src_node;
        mc_state.node_list = node_list;
        mc_state.node_list_len = node_list_len;
        mc_state.data = data;
        mc_state.data_len = data_len;
        mc_state.state = MC_SEND_STATE_SEND_FIRST;
        s2_send_callback = callback;

        s2_con.l_node = src_node;
        s2_con.r_node = group_id;
        s2_con.zw_tx_options = 0;
        s2_con.tx_options = 0;
        s2_con.class_id = 0;

        mainlog(logDebug,"Sending Multicast");
        if( S2_send_data_multicast(s2_ctx,&s2_con,mc_state.data,mc_state.data_len) ) 
        {
            return 1;
        }
        /* Transmission failed */
        mc_state.state = MC_SEND_STATE_IDLE;
    }
    return 0;
}

#ifdef TEST_MULTICAST_TX
uint8_t sec2_send_multicast_nofollowup(uint8_t src_node, const uint8_t* node_list, uint8_t node_list_len,const uint8_t * data, uint8_t data_len,ZW_SendDataAppl_Callback_t callback) 
{
    s2_connection_t s2_con;
    uint8_t group_id =0x42;

    if(mc_state.state == MC_SEND_STATE_IDLE) 
    {
        mc_state.l_node = src_node;
        mc_state.node_list = node_list;
        mc_state.node_list_len = node_list_len;
        mc_state.data = data;
        mc_state.data_len = data_len;
        /* Disable single-cast followup*/
        mc_state.state = MC_SEND_STATE_IDLE;
        s2_send_callback = callback;

        s2_con.l_node = src_node;
        s2_con.r_node = group_id;
        s2_con.zw_tx_options = 0;
        s2_con.tx_options = 0;
        s2_con.class_id = 0;

        mainlog(logDebug,"Sending Multicast");
        if( S2_send_data_multicast(s2_ctx,&s2_con,mc_state.data,mc_state.data_len) ) {
        return 1;
    }
    /* Transmission failed */
    mc_state.state = MC_SEND_STATE_IDLE;
    }
    return 0;
}
#endif


static void timeout(void *ctxt)
{
    mainlog(logDebug, "timeout");
    S2_timeout_notify((struct S2 *)ctxt);
}

/**
 * Must be implemented elsewhere maps to ZW_TimerStart. Note that this must start the same timer every time.
 * Ie. two calls to this function must reset the timer to a new value. On timout \ref S2_timeout_event must be called.
 *
 */
void S2_set_timeout(struct S2 *ctxt, uint32_t interval)
{

    mainlog(logDebug, "S2_set_timeout interval = %i ms\n", interval);

    if (s2_timer != 0)
    {
        timerCancel(&s2_timer);
        s2_timer = 0;
    }

    timerStart(&s2_timer, timeout, (void *)ctxt, interval, TIMER_ONETIME);
}

void sec2_init(void)
{
    //  static uint8_t s2_cmd_class_sup_report[64];
    /* stop s2 timer */
    if (s2_timer != 0)
    {
        timerCancel(&s2_timer);
        s2_timer = 0;
    }

    /* stop s2 inclusion timer */
    if (s2_inclusion_timer != 0)
    {
        timerCancel(&s2_inclusion_timer);
        s2_inclusion_timer = 0;
    }
    s2_send_user = 0;
    s2_send_callback = 0;

    if (s2_ctx)
    {
        S2_destroy(s2_ctx);
    }

    s2_inclusion_init(SECURITY_2_SCHEME_1_SUPPORT, KEX_REPORT_CURVE_25519,
                      SECURITY_2_SECURITY_2_CLASS_0 | SECURITY_2_SECURITY_2_CLASS_1 | SECURITY_2_SECURITY_2_CLASS_2 | SECURITY_2_SECURITY_0_NETWORK_KEY);

    s2_ctx = S2_init_ctx(homeId);

    s2_inclusion_set_event_handler(&sec2_event_handler);
    sec2_create_new_dynamic_ecdh_key();

    //memset(&s2_send_tx_status, 0, sizeof s2_send_tx_status); //Redundant: Static vars are always initialized to zero
    mc_state.state = MC_SEND_STATE_IDLE;
}

void sec2_abort_join()
{
    mainlog(logDebug, "S2 inclusion was aborted");
    if (!s2_ctx)
        return;

    s2_inclusion_abort(s2_ctx);
}

void sec2_start_learn_mode(uint8_t node_id, sec2_inclusion_cb_t cb)
{

    s2_connection_t s2_con;

    /*We update the context here becase the homeID has changed */
    sec2_init();

    s2_con.l_node = MyNodeId;
    s2_con.r_node = node_id;
    s2_con.zw_tx_options = TRANSMIT_OPTION_ACK | TRANSMIT_OPTION_AUTO_ROUTE | TRANSMIT_OPTION_EXPLORE;
    sec_incl_cb = cb;

    s2_inclusion_joining_start(s2_ctx, &s2_con, 0);
}

void sec2_refresh_homeid(void)
{
    s2_ctx->my_home_id = homeId;
}

void sec2_set_inclusion_peer(uint8_t remote_nodeID, uint8_t local_nodeID)
{
    S2_set_inclusion_peer(s2_ctx, remote_nodeID, local_nodeID);
}

extern void s2_inclusion_stop_timeout(void)
{
    if (s2_inclusion_timer != 0)
    {
        timerCancel(&s2_inclusion_timer);
        s2_inclusion_timer = 0;
    }
}
