/*
 * s2_keystore.c

 *
 *  Created on: Nov 2, 2015
 *      Author: aes
 */
#include "S0.h"
#include "s2_keystore.h"
#include "curve25519.h"
#include "serialAPI.h"
#include "utils.h"
#include "ctr_drbg.h"

extern uint8_t ecdh_dynamic_key[32];

bool keystore_network_key_clear(uint8_t keyclass)
{
    uint8_t random_bytes[64];
    uint8_t assigned_keys;
    AES_CTR_DRBG_Generate(&s2_ctr_drbg, random_bytes);

    if (keyclass == 0xFF)
    {
        keystore_network_key_clear(KEY_CLASS_S0);
        keystore_network_key_clear(KEY_CLASS_S2_UNAUTHENTICATED);
        keystore_network_key_clear(KEY_CLASS_S2_AUTHENTICATED);
        keystore_network_key_clear(KEY_CLASS_S2_ACCESS);
        return 1;
    }

    if (keystore_network_key_write(keyclass, random_bytes))
    {
        nvm_config_get(assigned_keys, &assigned_keys);
        assigned_keys &= ~keyclass;
        nvm_config_set(assigned_keys, &assigned_keys);
        memset(random_bytes,0,sizeof(random_bytes));
        return 1;
    }
    return 0;
}

void keystore_private_key_read(uint8_t *buf)
{
#if 0
    const uint8_t test_key[32] = {
    0x11,0x11,0x11,0x11,
    0x11,0x11,0x11,0x11,
    0x11,0x11,0x11,0x11,
    0x11,0x11,0x11,0x11,

    0x11,0x11,0x11,0x11,
    0x11,0x11,0x11,0x11,
    0x11,0x11,0x11,0x11,
    0x11,0x11,0x11,0x11,
    };
    memcpy(buf,test_key,32);
#endif
    //nvm_config_get(ecdh_priv_key, buf);
    if (GetLearnModeState())
    {
        mainlog(logDebug,"Use static private key");
        nvm_config_get(ecdh_priv_key,buf);
    }
    else
    {
        mainlog(logDebug,"Use dynamic private key");
        memcpy(buf,ecdh_dynamic_key,32);
    }
}

void keystore_public_key_read(uint8_t *buf)
{
    int i;
    uint8_t priv_key[32];

    keystore_private_key_read(priv_key);
    crypto_scalarmult_curve25519_base(buf, priv_key);

    
    mainlog(logDebug, "ECDH Public key is");
    for (i = 0; i < 16; i++)
    {
        if ((i % 4) == 0)
            mainlog(logDebug, "");
        uint16_t d = (buf[2 * i] << 8) | buf[2 * i + 1];
        printf("%05hu-", d);
    }
    mainlog(logDebug, "");
    

    memset(priv_key, 0, sizeof(priv_key));
}

bool keystore_network_key_read(uint8_t keyclass, uint8_t *buf)
{
    uint8_t assigned_keys;

    nvm_config_get(assigned_keys, &assigned_keys);

    if (!(keyclass & assigned_keys))
    {
        return 0;
    }

    switch (keyclass)
    {
    case KEY_CLASS_S0:
        nvm_config_get(security_netkey, buf);
        break;

    case KEY_CLASS_S2_UNAUTHENTICATED:
        nvm_config_get(security2_key[0], buf);
        break;

    case KEY_CLASS_S2_AUTHENTICATED:
        nvm_config_get(security2_key[1], buf);
        break;

    case KEY_CLASS_S2_ACCESS:
        nvm_config_get(security2_key[2], buf);
        break;

    default:
        ASSERT(0);
        return 0;
    }

    mainlog(logDebug, "Key  class  %x \n", keyclass);
    hexdump(buf, 16);

    return 1;
}

bool keystore_network_key_write(uint8_t keyclass, const uint8_t *buf)
{
    uint8_t assigned_keys;

    switch (keyclass)
    {

    case KEY_CLASS_S0:
        nvm_config_set(security_netkey, buf);
        sec0_set_key(buf);
        break;

    case KEY_CLASS_S2_UNAUTHENTICATED:
        nvm_config_set(security2_key[0], buf);
        break;

    case KEY_CLASS_S2_AUTHENTICATED:
        nvm_config_set(security2_key[1], buf);
        break;

    case KEY_CLASS_S2_ACCESS:
        nvm_config_set(security2_key[2], buf);
        break;

    default:
        ASSERT(0);
        return 0;
    }

    nvm_config_get(assigned_keys, &assigned_keys);
    assigned_keys |= keyclass;
    nvm_config_set(assigned_keys, &assigned_keys);
    return 1;
}
