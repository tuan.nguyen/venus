
#include <pthread.h>

#include "utils.h"
#include "util_crc.h"
#include "zw_api.h"
#include "zw_handle_notify_device.h"
#include "ZW_AddingInterviewDevice_FSM.h"
#include "Transport_ApplCmdHandlerEx.h"
#include "ZW_TransportEndpoint.h"

#include "controlled_CommandClassUpdateFirmware.h"
#include "controlled_CommandClassMultiChan.h"

#include "supported_CommandClassAssociation.h"
#include "supported_CommandClassAssociationGroupInfo.h"
#include "supported_CommandClassVersion.h"
#include "supported_CommandClassDeviceResetLocally.h"
#include "supported_CommandClassManufacturerSpecific.h"
#include "supported_CommandClassZWavePlusInfo.h"
#include "supported_CommandClassPowerLevel.h"
#include "supported_CommandClassSupervision.h"
#include "supported_CommandClassInclusionController.h"

extern send_request_state_t request;
extern send_request_state_t queue_req;

//variable for interview device process.
uint8_t targetEndpoint = 0;
uint8_t targetAggEndpoint = 0;
extern uint8_t handleAppCmdInQueueIndex;
extern pthread_mutex_t CriticalMutexNotification;
extern scheduling_tast_t handleAppCmdInQueue[COMMAND_SCHEDULING_MAX];
extern notify_queue_t *g_notify_buf;
//extern uint8_t request_report_from_device;
handle_duplicate_message_t oldApplicationCommand;
static timer_t timerHandlerDuplicateNotify = 0;

/**
 * @brief Transport_SendRequestEP
 * ZW_SendData with endpoint and secure options support.
 * @param pData IN Data buffer pointer.
 * @param dataLength IN Data buffer length.
 * @param pTxOptionsEx transmit options pointer of type TRANSMIT_OPTIONS_TYPE_EX.
 * @param completedFunc is a callback function-pointer returning result of the job.
 * @return RET Transaction ID or zero if failed
 */
uint8_t Transport_SendDataApplEP(uint8_t *pData,
                                uint8_t  dataLength,
                                ts_param_t *p,
                                void (*completedFunc)(uint8_t,void *, TX_STATUS_TYPE* ),
                                void* user)
{
    /*Check if frame should be encapsulated*/
    if(( 0 == p->sendpoint) && (0 == p->dendpoint))
    {
        return Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)pData, dataLength,
                                    p, completedFunc,user);
    }
    else
    {
        return CmdClassMultiChannelEncapsule( pData, dataLength, p, completedFunc, user);
    }


}


//interrupt of TimerHandlerDuplicateNotify
//reset old_message_notify
void ZCB_TimerOutHandlerDuplicateNotify(void *data)
{

    memset(oldApplicationCommand.old_message_notify, 0, BUF_SIZE);
    //mainlog(logDebug, "Reset old_message_notify");
}
//This timer will reset old_message_notify 500ms
void StartTimerHandlerDuplicateNotify(void) /* IN Timeout in seconds */
{
    if (timerHandlerDuplicateNotify != 0)
    {
        timerCancel(&timerHandlerDuplicateNotify);
        timerHandlerDuplicateNotify = 0;
    }
    timerStart(&timerHandlerDuplicateNotify, ZCB_TimerOutHandlerDuplicateNotify, NULL, 500, TIMER_ONETIME);
}

/*========================================================================================================
================================= check message from device ==============================================
============================= Return true if message is duplicated =======================================*/
bool CheckApplicationCommand(ts_param_t *p, uint8_t *pCmd, uint8_t cmdLength)
{
    if ((!memcmp(oldApplicationCommand.old_message_notify, pCmd, cmdLength)) &&
        ts_param_cmp(&oldApplicationCommand.p, p))
    {
        StartTimerHandlerDuplicateNotify();
        return true;//duplicate
    }
    else//no duplicate
    {
        memcpy(oldApplicationCommand.old_message_notify, pCmd, cmdLength);
        memcpy((uint8_t *)&oldApplicationCommand.p, p, sizeof(ts_param_t));
        StartTimerHandlerDuplicateNotify();
        return false;
    }
}

void Transport_ApplicationCommandHandler(
    ts_param_t  *p,                 /* IN Frame header info */
    uint8_t     *pCmd, /* IN Payload from the received frame, the union */
                                  /*    should be used to access the fields */
    uint16_t     cmdLength                /* IN Number of command bytes including the command */
)
{
    mainlog(logDebug, "Transport_ApplicationCommandHandler ->");
    hexdump(pCmd,cmdLength);
 

    if ((COMMAND_CLASS_MULTI_CHANNEL_V3 == *(pCmd + CMDBUF_CMDCLASS_OFFSET)) &&
        (MULTI_CHANNEL_CMD_ENCAP_V3 == *(pCmd + CMDBUF_CMDCLASS_OFFSET + 1))) //security
    {
        MultiChanCommandHandler( p, pCmd,cmdLength);
    }
    else
    {
        Transport_ApplicationCommandHandlerInternal( p, pCmd, cmdLength );
    }
}



void
RxToTxOptions( ts_param_t *rxopt,     /* IN  receive options to convert */
               ts_param_t *txopt)   /* OUT converted transmit options */
{
    
    txopt->dnode = rxopt->snode;
    txopt->dendpoint = rxopt->sendpoint;

    txopt->snode = rxopt->dnode;
    txopt->sendpoint = rxopt->dendpoint;
    txopt->scheme = ZW_SendData_scheme_select(rxopt, 0, 2);

    txopt->tx_flags =((rxopt->rx_flags & RECEIVE_STATUS_LOW_POWER) ?
                            TRANSMIT_OPTION_LOW_POWER : 0) | TRANSMIT_OPTION_ACK
                            | TRANSMIT_OPTION_EXPLORE | TRANSMIT_OPTION_AUTO_ROUTE;

}


/*============================ CheckOfEndpointBitAddr ===============================
** Function description
** This function...
**
** Side effects:
**
**-------------------------------------------------------------------------*/
bool CheckOfEndpointBitAddr(ts_param_t *p) /*p pointer of type ts_param_t */
{
    return (0 == (p->dendpoint & 0x80)) ? false : true;
}

bool
Check_not_legal_response_job(ts_param_t *p) /*rxOpt pointer of type RECEIVE_OPTIONS_TYPE_EX */
{
    bool status = false;

    /*
    * Check
    * 1: Get/Report do not support endpoint bit-addressing
    * 2: This check is to avoid that a response to a request is sent if the
    * request is not singlecast.
    * 3: Get command must no support Supervision encapsulation (CC:006C.01.00.13.002)
    */
    if((0 != (p->dendpoint & 0x80)) 
        || (0 != (p->rx_flags & (RECEIVE_STATUS_TYPE_BROAD | RECEIVE_STATUS_TYPE_MULTI)))) 
    {
        status = true;
    }
    return status;
}


/*Transport_ApplicationCommandHandlerEx
    RET received_frame_status_t                  */
received_frame_status_t
Transport_ApplicationCommandHandlerInternal(
    ts_param_t *p,      /* IN receive options of type ts_param_t  */
    uint8_t *pCmd,      /* IN  Payload from the received frame */
    uint16_t cmdLength) /*IN secure of Device Node*/
{
    received_frame_status_t frame_status = RECEIVED_FRAME_STATUS_NO_SUPPORT;
    
    mainlog(logDebug, "Transport_ApplicationCommandHandlerInternal");
    hexdump(pCmd, cmdLength);
    /*check duplicate message*/
    if (CheckApplicationCommand(p, pCmd, cmdLength))
    {
        mainlog(logDebug, "Got a duplicated message!!");
        return frame_status;
    }
    if ((queue_req.state == REQ_SENDING) && (queue_req.param.snode == p->snode) &&
                ((pCmd[0] == queue_req.class)  && (pCmd[1] == queue_req.cmd)) )
    {
        queue_req.state = REQ_DONE;
    }

    if ((request.state == REQ_SENDING) && 
        (request.param.snode == p->snode) && (request.param.sendpoint == p->sendpoint) &&
        ((pCmd[0] == request.class)  && (pCmd[1] == request.cmd)) )
    {
        /*stop request timer */
        if (request.timer)
        {
            timerCancel(&(request.timer));
            request.timer = 0;
        }

        request.state = REQ_DONE;

        if (request.callback)
        {
            request.callback(TRANSMIT_COMPLETE_OK, pCmd,cmdLength);
        }
        return frame_status;
    } 


    switch (*(pCmd + CMDBUF_CMDCLASS_OFFSET))
    {
   
    case COMMAND_CLASS_VERSION:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case VERSION_GET:
        case VERSION_COMMAND_CLASS_GET_V2:
            frame_status = handleCommandClassVersion(p, (ZW_APPLICATION_TX_BUFFER *)pCmd, cmdLength);
            return frame_status;            
        //case VERSION_REPORT://push notify to app

        case VERSION_COMMAND_CLASS_REPORT_V2:
        {
            if (nm.mode == NMM_UPDATE_FIRMWARE_DEVICE)
            {    
                handle_firmware_update_report.support_firmware_update_md_version = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                PostEventUpdateFirmwareMd(EV_VERSION_CC_REPORT);
                return frame_status;                            
            }
        }                  
            
        }
    }
    break;


    case COMMAND_CLASS_MULTI_CHANNEL_V3:
    {
        if ((nm.mode != NMM_DEFAULT))
        {
            switch (*(pCmd + CMDBUF_CMD_OFFSET))
            {
            case MULTI_CHANNEL_END_POINT_REPORT_V3:
            {
                lastLearnedNodeZPC.no_endpoint = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                
                /* Device suppoted MultiChannel Version 4 */
                if (cmdLength > 4)
                {
                    lastLearnedNodeZPC.no_agg_endpoint = *(pCmd + CMDBUF_CMD_OFFSET + 3);
                }
                else
                {
                    lastLearnedNodeZPC.no_agg_endpoint = 0;
                }

                PostEventInterviewDevice(EV_RECIEVE_END_POINT_REPORT_OK);
                return frame_status;
            }
            break;

            case MULTI_CHANNEL_CAPABILITY_REPORT_V3:
            {
                //send report to app, check duplicate message
                if ((*(pCmd + CMDBUF_CMD_OFFSET + 1) == targetEndpoint) && (cmdLength > 5))
                {
                    if (targetEndpoint > lastLearnedNodeZPC.no_endpoint)
                    {
                        lastLearnedNodeZPC.multi_channel_agg_capability[targetEndpoint - 1 - lastLearnedNodeZPC.no_endpoint].endpoint = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                        lastLearnedNodeZPC.multi_channel_agg_capability[targetEndpoint - 1 - lastLearnedNodeZPC.no_endpoint].generic_device_class = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                        lastLearnedNodeZPC.multi_channel_agg_capability[targetEndpoint - 1 - lastLearnedNodeZPC.no_endpoint].specific_device_class = *(pCmd + CMDBUF_CMD_OFFSET + 3);
                        lastLearnedNodeZPC.multi_channel_agg_capability[targetEndpoint - 1 - lastLearnedNodeZPC.no_endpoint].endpoint_capability.noCapability = cmdLength - 5;
                        memcpy(&lastLearnedNodeZPC.multi_channel_agg_capability[targetEndpoint - 1 - lastLearnedNodeZPC.no_endpoint].endpoint_capability.aCapability, (pCmd + 5), cmdLength - 5);
                    }
                    else
                    {
                        lastLearnedNodeZPC.multi_channel_capability[targetEndpoint - 1].endpoint = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                        lastLearnedNodeZPC.multi_channel_capability[targetEndpoint - 1].generic_device_class = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                        lastLearnedNodeZPC.multi_channel_capability[targetEndpoint - 1].specific_device_class = *(pCmd + CMDBUF_CMD_OFFSET + 3);
                        lastLearnedNodeZPC.multi_channel_capability[targetEndpoint - 1].endpoint_capability.noCapability = cmdLength - 5;
                        memcpy(&lastLearnedNodeZPC.multi_channel_capability[targetEndpoint - 1].endpoint_capability.aCapability, (pCmd + 5), cmdLength - 5);
                    }

                    targetEndpoint++;
                    mainlog(logDebug, "MULTI_CHANNEL_CAPABILITY_REPORT_V3_ENDPOINT_%u --> OK", (targetEndpoint - 1));
                }
                PostEventInterviewDevice(EV_REPORT_CAPABILITY_EP_OK);
                return frame_status;
            }
            break;

            case MULTI_CHANNEL_AGGREGATED_MEMBERS_REPORT_V4:
            {
                //send report to app, check duplicate message
                if ((*(pCmd + CMDBUF_CMD_OFFSET + 1) == (targetAggEndpoint + lastLearnedNodeZPC.no_endpoint)) && (cmdLength > 4))
                {
                    lastLearnedNodeZPC.multi_channel_agg_capability[targetAggEndpoint - 1].endpoint = *(pCmd + CMDBUF_CMD_OFFSET + 1);

                    uint8_t* aggregatedMembersBitMask;
                    uint8_t numberOfBitMasks;
                    numberOfBitMasks = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                    aggregatedMembersBitMask = malloc(sizeof(uint8_t) * numberOfBitMasks);
                    memcpy(aggregatedMembersBitMask, (pCmd + 4), numberOfBitMasks);
                    
                    /*Calculate the index of the bits set in the mask*/
                    uint8_t i, j, n = 0;
                    uint8_t c, no_memberof = 0;
                    for(i=0; i < numberOfBitMasks; i++) 
                    {
                        c = aggregatedMembersBitMask[i];
                        j=0;
                        while(c) 
                        {
                            if(c & 1) 
                            {
                                lastLearnedNodeZPC.multi_channel_agg_capability[targetAggEndpoint - 1].ep_member[no_memberof] = n + j + 1;
                                no_memberof ++;
                                lastLearnedNodeZPC.multi_channel_agg_capability[targetAggEndpoint - 1].no_memberof = no_memberof; 
                            }
                            c = c >> 1;
                            j++;
                        }
                        n=n+8;
                    }

                    targetAggEndpoint++;
                    if (aggregatedMembersBitMask)
                    {
                        free(aggregatedMembersBitMask);
                    }
                    mainlog(logDebug, "MULTI_CHANNEL_AGGREGATED_MEMBERS_REPORT_V4%u --> OK", (targetAggEndpoint - 1));
                }
                PostEventInterviewDevice(EV_REPORT_MEMBER_AGG_EP_OK);
                return frame_status;
            }
            break;
            }
        }
    }
    break;

    case COMMAND_CLASS_SECURITY_2:
    {
        if ((nm.mode != NMM_DEFAULT))
        {
            switch (*(pCmd + CMDBUF_CMD_OFFSET))
            {
            case SECURITY_2_COMMANDS_SUPPORTED_REPORT:
            {
                if ((p->sendpoint == targetEndpoint) && (cmdLength > 2))
                {
                    if (targetEndpoint > lastLearnedNodeZPC.no_endpoint)
                    {
                        memcpy((uint8_t *)&lastLearnedNodeZPC.multi_channel_agg_capability[targetEndpoint - 1 - lastLearnedNodeZPC.no_endpoint].endpoint_capability.aCapability +
                                   lastLearnedNodeZPC.multi_channel_agg_capability[targetEndpoint - 1 - lastLearnedNodeZPC.no_endpoint].endpoint_capability.noCapability,
                               (pCmd + 2), (cmdLength - 2));
                        lastLearnedNodeZPC.multi_channel_agg_capability[targetEndpoint - 1 - lastLearnedNodeZPC.no_endpoint].endpoint_capability.noCapability += (cmdLength - 2);
                    }
                    else
                    {
            
                        memcpy((uint8_t *)&lastLearnedNodeZPC.multi_channel_capability[targetEndpoint - 1].endpoint_capability.aCapability +
                                   lastLearnedNodeZPC.multi_channel_capability[targetEndpoint - 1].endpoint_capability.noCapability,
                               pCmd + 2, cmdLength - 2);
                        lastLearnedNodeZPC.multi_channel_capability[targetEndpoint - 1].endpoint_capability.noCapability += (cmdLength - 2);
                    }
                    targetEndpoint++;
                    mainlog(logDebug, "REPORT_SECURE_V2_CAPABILITY_TARGET_ENPOINT_%u --> OK", (targetEndpoint - 1));
                }
                PostEventInterviewDevice(EV_REPORT_SECURE_V2_CAPABILITY_EP_OK);
                return frame_status;
            }
            break;
            }
        }
    }
    break;
    case COMMAND_CLASS_SECURITY:
    {
        if ((nm.mode != NMM_DEFAULT))
        {
            switch (*(pCmd + CMDBUF_CMD_OFFSET))
            {
            case SECURITY_COMMANDS_SUPPORTED_REPORT:
            {
                //send report to app, check duplicate message
                if ((p->sendpoint == targetEndpoint) && (cmdLength > 3))
                {
                    if (targetEndpoint > lastLearnedNodeZPC.no_endpoint)
                    {
                        memcpy((uint8_t *)&lastLearnedNodeZPC.multi_channel_agg_capability[targetEndpoint - 1 - lastLearnedNodeZPC.no_endpoint].endpoint_capability.aCapability +
                                   lastLearnedNodeZPC.multi_channel_agg_capability[targetEndpoint - 1 - lastLearnedNodeZPC.no_endpoint].endpoint_capability.noCapability,
                               (pCmd + 2), (cmdLength - 2));
                        lastLearnedNodeZPC.multi_channel_agg_capability[targetEndpoint - 1 - lastLearnedNodeZPC.no_endpoint].endpoint_capability.noCapability += (cmdLength - 2);
                    }
                    else
                    {
            
                        memcpy((uint8_t *)&lastLearnedNodeZPC.multi_channel_capability[targetEndpoint - 1].endpoint_capability.aCapability +
                                   lastLearnedNodeZPC.multi_channel_capability[targetEndpoint - 1].endpoint_capability.noCapability,
                               pCmd + 2, cmdLength - 2);
                        lastLearnedNodeZPC.multi_channel_capability[targetEndpoint - 1].endpoint_capability.noCapability += (cmdLength - 2);
                    }
                    targetEndpoint++;
                    mainlog(logDebug, "REPORT_SECURE_V0_CAPABILITY_TARGET_ENPOINT_%u --> OK", (targetEndpoint - 1));
                }
                PostEventInterviewDevice(EV_REPORT_SECURE_V0_CAPABILITY_EP_OK);
                return frame_status;
            }
            break;
            }
        }
    }
    break;
    case COMMAND_CLASS_ASSOCIATION_GRP_INFO:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case ASSOCIATION_GROUP_NAME_GET:
        case ASSOCIATION_GROUP_INFO_GET:
        case ASSOCIATION_GROUP_COMMAND_LIST_GET:
        {
            frame_status = handleCommandClassAssociationGroupInfo(p, (ZW_APPLICATION_TX_BUFFER *)pCmd, cmdLength);
            return frame_status;
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_SUPERVISION:
    {
        frame_status = handleCommandClassSupervision(p, (ZW_APPLICATION_TX_BUFFER *)pCmd, cmdLength);
        return frame_status;
    }
    break;

    case COMMAND_CLASS_POWERLEVEL:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case POWERLEVEL_SET:
        case POWERLEVEL_GET:
        case POWERLEVEL_TEST_NODE_SET:
        case POWERLEVEL_TEST_NODE_GET:
        {
            frame_status = handleCommandClassPowerLevel(p, (ZW_APPLICATION_TX_BUFFER *)pCmd, cmdLength);
            return frame_status;
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_ZWAVEPLUS_INFO:
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case ZWAVEPLUS_INFO_REPORT:
        {
            if (nm.mode != NMM_DEFAULT)
            {
                lastLearnedNodeZPC.is_zp_node = true;
                lastLearnedNodeZPC.zp_info.zwave_plus_version = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                lastLearnedNodeZPC.zp_info.role_type = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                lastLearnedNodeZPC.zp_info.node_type = *(pCmd + CMDBUF_CMD_OFFSET + 3);
                lastLearnedNodeZPC.zp_info.installer_icon_type = *(pCmd + CMDBUF_CMD_OFFSET + 4);
                lastLearnedNodeZPC.zp_info.installer_icon_type = (lastLearnedNodeZPC.zp_info.installer_icon_type << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 5);
                lastLearnedNodeZPC.zp_info.user_icon_type = *(pCmd + CMDBUF_CMD_OFFSET + 6);
                lastLearnedNodeZPC.zp_info.user_icon_type = (lastLearnedNodeZPC.zp_info.user_icon_type << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 7);

                PostEventInterviewDevice(EV_ZWAVEPLUS_INFO_REPORT_OK);
                return frame_status;
            }

        }
        break;

        case ZWAVEPLUS_INFO_GET:
        {
            frame_status = handleCommandClassZWavePlusInfo(p, (ZW_APPLICATION_TX_BUFFER *)pCmd, cmdLength);
            return frame_status;
        }
        break;
        }
        break;

    case COMMAND_CLASS_DEVICE_RESET_LOCALLY:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case DEVICE_RESET_LOCALLY_NOTIFICATION:
        {
            mainlog(logDebug, "Add reset locally message with QueueIndex: %d", handleAppCmdInQueueIndex);
            pthread_mutex_lock(&CriticalMutexNotification);
            if (handleAppCmdInQueueIndex < COMMAND_SCHEDULING_MAX)
            {
                handleAppCmdInQueue[handleAppCmdInQueueIndex].task_type = TT_MESSAGE_DEVICE_RESET_LOCALLY;
                handleAppCmdInQueue[handleAppCmdInQueueIndex].retry = DEFAULT_RETRY;
                handleAppCmdInQueue[handleAppCmdInQueueIndex].handle_message_from_device.cmd_class = COMMAND_CLASS_DEVICE_RESET_LOCALLY;
                handleAppCmdInQueue[handleAppCmdInQueueIndex].handle_message_from_device.cmd = DEVICE_RESET_LOCALLY_NOTIFICATION;
                memcpy(&handleAppCmdInQueue[handleAppCmdInQueueIndex].handle_message_from_device.p,
                       p, sizeof(ts_param_t));
                handleAppCmdInQueueIndex++;
            }
            pthread_mutex_unlock(&CriticalMutexNotification);
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_ASSOCIATION:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case ASSOCIATION_GET_V2:
        case ASSOCIATION_SET_V2:
        case ASSOCIATION_REMOVE_V2:
        case ASSOCIATION_SPECIFIC_GROUP_GET_V2:
        case ASSOCIATION_GROUPINGS_GET_V2:
        {
            frame_status = handleCommandClassAssociation(p, (ZW_APPLICATION_TX_BUFFER *)pCmd, cmdLength);
            return frame_status;
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_CONTROLLER_REPLICATION:
    {
        serialApiReplicationCommandComplete();
        // serialApiGetControllerCapabilities(&pTxNotify.ControllerCapabilityNotify.capability);
        // pTxNotify.ControllerCapabilityNotify.SUC_node_id = serialApiGetSUCNodeID();

        return frame_status;
    }
    break;


    case COMMAND_CLASS_MANUFACTURER_SPECIFIC:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case MANUFACTURER_SPECIFIC_GET_V2:
        case DEVICE_SPECIFIC_GET_V2:
        {
            frame_status = handleCommandClassManufacturerSpecific(p, (ZW_APPLICATION_TX_BUFFER *)pCmd, cmdLength);
            return frame_status;
        }
        break;

        case MANUFACTURER_SPECIFIC_REPORT:
        {
            //save manufacturer
            SetNodeManufacture(p->snode, pCmd + CMDBUF_CMD_OFFSET + 1);
            if (nm.mode != NMM_DEFAULT)
            {
                memcpy((uint8_t *)&(lastLearnedNodeZPC.node_manufacture), pCmd + CMDBUF_CMD_OFFSET + 1, sizeof(MANUFACTURE_INFO));
                PostEventInterviewDevice(EV_MANU_SPECIFIC_REPORT_OK);
                return frame_status;
            }
        }
        break;

        }
    }
    break;

    case COMMAND_CLASS_WAKE_UP:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case WAKE_UP_NOTIFICATION:
        {
            uint8_t index;
            if (checkNodeIdNoMoreInfoInQueue(p->snode, &index))
                break;
            mainlog(logDebug, "Add Wake up notify message with QueueIndex: %d", handleAppCmdInQueueIndex);            
            pthread_mutex_lock(&CriticalMutexNotification);
            if (handleAppCmdInQueueIndex < COMMAND_SCHEDULING_MAX)
            {
                handleAppCmdInQueue[handleAppCmdInQueueIndex].task_type = TT_MESSAGE_WAKE_UP_NOTIFY;
                handleAppCmdInQueue[handleAppCmdInQueueIndex].retry = DEFAULT_RETRY;
                handleAppCmdInQueue[handleAppCmdInQueueIndex].handle_message_from_device.cmd_class = COMMAND_CLASS_WAKE_UP;
                handleAppCmdInQueue[handleAppCmdInQueueIndex].handle_message_from_device.cmd = WAKE_UP_NOTIFICATION;
                memcpy(&handleAppCmdInQueue[handleAppCmdInQueueIndex].handle_message_from_device.p, p, sizeof(ts_param_t));
                handleAppCmdInQueueIndex++;
            }
            pthread_mutex_unlock(&CriticalMutexNotification);
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_FIRMWARE_UPDATE_MD:
    {
        if (nm.mode == NMM_UPDATE_FIRMWARE_DEVICE)
        {
            switch (*(pCmd + CMDBUF_CMD_OFFSET))
            {
            case FIRMWARE_MD_REPORT:
            {
                if (8 == cmdLength)
                {
                    /*only handle version 1, 2 */
                    handle_firmware_update_report.manufacturer_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                    handle_firmware_update_report.manufacturer_id = (handle_firmware_update_report.manufacturer_id << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 2);
                    handle_firmware_update_report.firmware_id = *(pCmd + CMDBUF_CMD_OFFSET + 3);
                    handle_firmware_update_report.firmware_id = (handle_firmware_update_report.firmware_id << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 4);
                    handle_firmware_update_report.checksum = *(pCmd + CMDBUF_CMD_OFFSET + 5);
                    handle_firmware_update_report.checksum = (handle_firmware_update_report.checksum << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 6);
                    PostEventUpdateFirmwareMd(EV_RX_FIRMWARE_MD_REPORT_OK);
                }                
                else//version 3,4
                {
                    //Only for Firmware Target 0, 
                    /*Firmware Tafget 1, 2, n TODO*/
                    handle_firmware_update_report_v3_4.manufacturer_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                    handle_firmware_update_report_v3_4.manufacturer_id =
                        (handle_firmware_update_report_v3_4.manufacturer_id << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 2);
                    handle_firmware_update_report_v3_4.firmware0_id = *(pCmd + CMDBUF_CMD_OFFSET + 3);
                    handle_firmware_update_report_v3_4.firmware0_id =
                        (handle_firmware_update_report_v3_4.firmware0_id << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 4);
                    handle_firmware_update_report_v3_4.firmware0_checksum = *(pCmd + CMDBUF_CMD_OFFSET + 5);
                    handle_firmware_update_report_v3_4.firmware0_checksum =
                        (handle_firmware_update_report_v3_4.firmware0_checksum << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 6);
                    handle_firmware_update_report_v3_4.firmware_upgradable = *(pCmd + CMDBUF_CMD_OFFSET + 7);
                    handle_firmware_update_report_v3_4.no_firmware_targets = *(pCmd + CMDBUF_CMD_OFFSET + 8);
                    handle_firmware_update_report_v3_4.max_fragment_size = *(pCmd + CMDBUF_CMD_OFFSET + 9);
                    handle_firmware_update_report_v3_4.max_fragment_size =
                        (handle_firmware_update_report_v3_4.max_fragment_size << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 10);
                    int index;
                    for (index = 0; index < handle_firmware_update_report_v3_4.no_firmware_targets; index++)
                    {
                        handle_firmware_update_report_v3_4.firmware_id_list[index] = *(pCmd + CMDBUF_CMD_OFFSET + 11 + index * 2);
                        handle_firmware_update_report_v3_4.firmware_id_list[index] =
                            (handle_firmware_update_report_v3_4.firmware_id_list[index] << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 12 + index * 2);
                    }
                    PostEventUpdateFirmwareMd(EV_RX_FIRMWARE_MD_REPORT_VERSION_3_4_OK);                    
                }
                return frame_status;
            }
            break;

            case FIRMWARE_UPDATE_MD_REQUEST_REPORT:
            {
                if(FIRMWARE_UPDATE_MD_STATUS_REPORT_SUCCESSFULLY == *(pCmd + CMDBUF_CMD_OFFSET + 1))
                {
                    PostEventUpdateFirmwareMd(EV_RX_FIRMWARE_UPDATE_MD_REQUEST_REPORT_OK);
                }
                else
                {
                    PostEventUpdateFirmwareMd(EV_RX_FIRMWARE_UPDATE_MD_REQUEST_REPORT_NOT_OK);
                }

                return frame_status;
            }
            break;

            case FIRMWARE_UPDATE_MD_GET:
            { 
                /*send FIRMWARE_UPDATE_MD_REPORT to device*/
                handle_firmware_update_report.no_reports = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                handle_firmware_update_report.report_number = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                handle_firmware_update_report.report_number = (handle_firmware_update_report.report_number << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3);

                PostEventUpdateFirmwareMd(EV_RX_FIRMWARE_UPDATE_GET_OK);
                return frame_status;
            }
            break;

            case FIRMWARE_UPDATE_MD_STATUS_REPORT:
            {
                if (3 == cmdLength)
                {
                    
                    if (FIRMWARE_UPDATE_MD_STATUS_REPORT_SUCCESSFULLY == *(pCmd + CMDBUF_CMD_OFFSET + 1))
                    {
                        PostEventUpdateFirmwareMd(EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_OK);
                    }
                    else
                    {
                        PostEventUpdateFirmwareMd(EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_NOT_OK);
                    }
                }                
                else
                {
                    handle_firmware_update_report_v3_4.status = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                    handle_firmware_update_report_v3_4.wait_time = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                    handle_firmware_update_report_v3_4.wait_time =
                        (handle_firmware_update_report_v3_4.wait_time << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3);
                    if (FIRMWARE_UPDATE_MD_STATUS_REPORT_SUCCESSFULLY == *(pCmd + CMDBUF_CMD_OFFSET + 1))
                    {
                        PostEventUpdateFirmwareMd(EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_OK);
                    }
                    else
                    {
                        PostEventUpdateFirmwareMd(EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_NOT_OK);
                    }
                }
                return frame_status;
            }
            break;
            }
        }
    }
    break;

    case COMMAND_CLASS_INCLUSION_CONTROLLER:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case COMPLETE: //device support S2. or non support secure
        {
            nm.state = NMS_INCLUSION_CONTROLLER;
            if (COMPLETE_PROXY_INCLUSION == *(pCmd + CMDBUF_CMD_OFFSET + 1))
            {
                if (COMPLETE_STEP_OK == *(pCmd + CMDBUF_CMD_OFFSET + 2))
                {
                    PostEventInclusionController(EV_RECEIVE_INCLUSION_CONTROLLER_COMPLETE_OK);
                }
                else
                {
                    PostEventInclusionController(EV_RECEIVE_INCLUSION_CONTROLLER_COMPLETE_NOT_OK);
                }
            }
            else if (COMPLETE_S0_INCLUSION == *(pCmd + CMDBUF_CMD_OFFSET + 1))
            {
                if (COMPLETE_STEP_OK == *(pCmd + CMDBUF_CMD_OFFSET + 2))
                {
                    PostEventInclusionController(EV_RECEIVE_COMPLETE_OK_FROM_INCLUSION);
                }
                else
                {
                    PostEventInclusionController(EV_RECEIVE_COMPLETE_NOT_OK_FROM_INCLUSION);
                }
            }
            return frame_status;

            //PushNotificationToHandler after a controller has completed the requested inclusion steps.
        }
        break;

        case INITIATE: //device don't support S2.
        {
            /* PushNotificationToHandler after a controller has completed the requested inclusion steps.
               device support S2.*/
            nm.state = NMS_INCLUSION_CONTROLLER;

            if (INITIATE_S0_INCLUSION == *(pCmd + CMDBUF_CMD_OFFSET + 2))
            {
                /*step S0_INCLUSION --> my device is Inclusion */
                PostEventInclusionController(EV_RECEIVE_INCLUSION_CONTROLLER_INITIATE_S0);
            }
            else if ((INITIATE_PROXY_INCLUSION == *(pCmd + CMDBUF_CMD_OFFSET + 2)) || 
                    (INITIATE_PROXY_INCLUSION_REPLACE == *(pCmd + CMDBUF_CMD_OFFSET + 2)))//replace fail node
            {
                /*step PROXY_INCLUSION or INITIATE_PROXY_INCLUSION_REPLACE --> mys device is SIS */ 
                nm.mode = NMM_ADD_NODE;

                /*push notify Inclusion Controller Ready */
                NOTIFY_TX_BUFFER_T pTxNotify;
                pTxNotify.InclusionControllerNotify.status = INCLUSION_CONTROLLER_START;
                PushNotificationToHandler(INCLUSION_CONTROLLER_NOTIFY, (uint8_t *)&pTxNotify.InclusionControllerNotify, sizeof(INCLUSION_CONTROLLER_NOTIFY));


                nm.tmp_node = p->snode; /*Id of Inclusion Controller*/
                lastLearnedNodeZPC.node_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);/*id of device*/
                PostEventInclusionController(EV_RECEIVE_INCLUSION_CONTROLLER_INITIATE_PROXY);
            }
            return frame_status;
        }
        break;
        }
    }
    break;

    default:
        break;

        /////////////////////////////////////////////////
    }

    frame_status = Transport_ApplicationCommandHandlerEx( p, pCmd, cmdLength );

    return frame_status;
}







