#include "ZW_TransportSecurity.h"
#include "s2_keystore.h"
#include "S0.h"
#include "security_layer.h"
#include "transport_service2.h"




uint8_t crc_send_data(
    ts_param_t* p,
    uint8_t *pData,
    uint8_t dataLength,
    void (*completedFunc)(uint8_t,void *, TX_STATUS_TYPE* ),
    void* user)
{
    uint8_t *pTxBufEncap = pData;
    uint8_t sizeCmdFrameHeader = 2;
    /*Make space in start of the buffer for command MULTI_CHANNEL_CMD_ENCAP_V4*/
    uint8_t frameSize = dataLength;
    while(frameSize)
    {
        *(pData + frameSize - 1 + sizeCmdFrameHeader ) = *(pData + frameSize - 1);
        frameSize--;
    }

    if(NULL != pTxBufEncap)
    {
        *(pTxBufEncap+CMDBUF_CMDCLASS_OFFSET) = COMMAND_CLASS_CRC_16_ENCAP;
        *(pTxBufEncap+CMDBUF_CMDCLASS_OFFSET+1) = CRC_16_ENCAP;
        uint16_t crc=ZW_CheckCrc16(0x1D0F,pTxBufEncap,dataLength+2);    
        *(pTxBufEncap+CMDBUF_CMDCLASS_OFFSET+2+dataLength) = (uint8_t)(crc>>8);
        *(pTxBufEncap+CMDBUF_CMDCLASS_OFFSET+3+dataLength) = (uint8_t)(crc>>0);
    }
    return send_data(p, pTxBufEncap, dataLength+4, completedFunc, user);
   
}




void ApplicationCommandHandlerTransport_Compl(ts_param_t* p, uint8_t *pCmd, uint16_t cmdLength)
{
    switch (*(pCmd+CMDBUF_CMDCLASS_OFFSET))
    {
        case COMMAND_CLASS_CRC_16_ENCAP:
        {
            if (!ZW_CheckCrc16(0x1D0F,pCmd,cmdLength))
            {
                mainlog(logDebug,"CRC check ok");
                p->scheme = USE_CRC16;
                Transport_ApplicationCommandHandler(p,pCmd+2,cmdLength-4);
                return;
            }
        }
        break;

        case COMMAND_CLASS_SECURITY_2:
        {
            security2_CommandHandler(p,(ZW_APPLICATION_TX_BUFFER*)pCmd,cmdLength);
            return;
        }
        break;

        case COMMAND_CLASS_SECURITY:
        {
            if (*(pCmd+CMDBUF_CMD_OFFSET) != SECURITY_COMMANDS_SUPPORTED_REPORT) 
            {
                security_CommandHandler(p,(ZW_APPLICATION_TX_BUFFER*)pCmd,cmdLength);
                return;
            }
        }
        break;

        case COMMAND_CLASS_TRANSPORT_SERVICE:
        {
            TransportService_ApplicationCommandHandler(p, pCmd, cmdLength);
            return;
        }
        break;

        default:
        break;

    }

    Transport_ApplicationCommandHandler(p,pCmd,cmdLength);
}

void ApplicationCommandHandler_Compl(uint8_t rxStatus, uint8_t destNode, uint8_t sourceNode, uint8_t* pCmd, uint8_t cmdLength)
{

    /*create transport param for Transport Application Handler (Encapsulated+MultiChannel+Application)*/
    ts_param_t p;

    p.scheme = NO_SCHEME;
    p.dnode = destNode ? destNode : MyNodeId;
    p.snode = sourceNode;

    p.dendpoint = 0;
    p.sendpoint = 0;

    p.rx_flags = rxStatus;
    p.tx_flags = ((rxStatus & RECEIVE_STATUS_LOW_POWER) ?
                TRANSMIT_OPTION_LOW_POWER : 0) | TRANSMIT_OPTION_ACK
                | TRANSMIT_OPTION_EXPLORE | TRANSMIT_OPTION_AUTO_ROUTE;

    /*convert to Transport Application Handler*/
    ApplicationCommandHandlerTransport_Compl(&p, pCmd, cmdLength);
}





/*========================   Transport_SendRequest   ============================
**      Send request command over secure network
**    Side effects :
**
**--------------------------------------------------------------------------*/
uint8_t Transport_SendRequestExt(
    ZW_APPLICATION_TX_BUFFER *pBufData,
    uint8_t dataLength,
    ts_param_t *p,
    void (*completedFunc)(uint8_t,void *, TX_STATUS_TYPE* ),
    void* user)
{
    security_scheme_t scheme;
    /* Expire cached external nonce.
    * This disables the optimization of reusing the IV from previous message as
    * external nonce for the reply. This optimization is disabled for backward
    * compatibility reasons. */

    /*Select the right security shceme*/

    scheme = ZW_SendData_scheme_select(p, (uint8_t*)pBufData, dataLength);
    mainlog(logDebug,"Sending with scheme %d and data: ", scheme);
    hexdump((uint8_t*)pBufData, dataLength);
    switch (scheme)
    {
    case USE_CRC16:
        return crc_send_data(p, (uint8_t*)pBufData, dataLength, completedFunc, user);

    case NO_SCHEME:
        //return serialApiSendData(p->dnode, (uint8_t*)pBufData, dataLength, p->tx_flags, ZCB_ReqFuncStatusCb);
        return send_data(p, (uint8_t*)pBufData, dataLength,  completedFunc, user);

    case SECURITY_SCHEME_0:
        return sec0_send_data(p, (uint8_t*)pBufData, dataLength, completedFunc, user);
    break;

    case SECURITY_SCHEME_2_ACCESS:
    case SECURITY_SCHEME_2_AUTHENTICATED:
    case SECURITY_SCHEME_2_UNAUTHENTICATED:
        p->scheme = scheme;
        return sec2_send_data(p, (uint8_t*)pBufData, dataLength, completedFunc, user);
    break;

    default:
        return false;
    break;
    }
}





/*============================ CommandClassSecurityVersionGet ===============
** Function description
** Return version
**
** Side effects:
**
**-------------------------------------------------------------------------*/
uint8_t
CommandClassSecurityVersionGet(void)
{
    return SECURITY_VERSION;
}




/*============================ SetupActiveNIF ===============================
**      This function must be called to setup active node information frame
**
**    Side effects :
**
**-------------------------------------------------------------------------*/
void
SetPreInclusionNIF(security_scheme_t target_scheme)
{
    mainlog(logDebug,"SetPreInclusionNIF");
    
    if( NO_SCHEME != target_scheme )
    {
        mainlog(logDebug,"Secure Mode");
        memcpy((uint8_t*)&myZPCCapability.node_capability, (uint8_t*)&nodeCapabilitySecureIncluded, sizeof(NODE_CAPABILITY));
    }
    else
    {
        mainlog(logDebug,"Non Secure Mode");
        memcpy((uint8_t*)&myZPCCapability.node_capability, (uint8_t*)&nodeCapabilityNonSecureIncluded, sizeof(NODE_CAPABILITY));
    }
    
    APPL_NODE_TYPE applNodeType;
    applNodeType.generic=myZPCCapability.node_info.nodeType.generic;
    applNodeType.specific=myZPCCapability.node_info.nodeType.specific;

    serialApiApplicationNodeInformation(APP_NETWORK_CAPABILITY,
                                        applNodeType,
                                        myZPCCapability.node_capability.aCapability,
                                        myZPCCapability.node_capability.noCapability);

}


