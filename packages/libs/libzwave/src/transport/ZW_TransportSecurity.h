#ifndef _ZW_TRANSPORT_SECURITY_H_
#define _ZW_TRANSPORT_SECURITY_H_

#include <stdint.h>
#include <stdbool.h>
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"
#include "ZW_TransportEndpoint.h"
#include "security_layer.h"


#include "serialAPI.h"
#include "utils.h"
#include "timer.h"
#include "cmd_class_misc.h"
#include "ctrl_learn.h"
#include "ZW_SendDataAppl.h"


enum secure_node_status 
{
	UNINITIALIZED_NODE	=	0x00,
	SECURE_NODE,
	NON_SECURE_NODE,
};

enum inclusion_mode {
	INCLUSION_IDLE 	=	0x00,	
	INCLUSION_SLAVE,
	INCLUSION_CONTROLLER,
};

/**
 * Structure holding information about node lists, device option mask and node type.
 */
typedef struct _APP_NODE_INFORMATION_
{
	uint8_t *cmdClassListNonSecure;            /**< Nonsecure-list of supported command classes, when node communicate by this transport */
	uint8_t cmdClassListNonSecureCount;        /**< Count of elements in supported command classes Nonsecure-list */
	uint8_t *cmdClassListNonSecureIncludedSecure; /**< Nonsecure-list of supported command classes, when node communicate by this transport */
	uint8_t cmdClassListNonSecureIncludedSecureCount;  /**< Count of elements in supported command classes Nonsecure-list */
	uint8_t *cmdClassListSecure;               /**< Secure-list of supported command classes, when node communicate by this transport */
	uint8_t cmdClassListSecureCount;           /**< Count of elements in supported command classes Secure-list */
	uint8_t deviceOptionsMask; /**< See ZW_basic_api.h for ApplicationNodeInformation field deviceOptionMask */
	APPL_NODE_TYPE nodeType;
} APP_NODE_INFORMATION;

/**
 * Serial Application Command Handler : callback from serial data.
 */
void 	ApplicationCommandHandler_Compl(uint8_t rxStatus, uint8_t destNode, uint8_t sourceNode, uint8_t* pCmd, uint8_t cmdLength);

/**
 * Transport Application Command Handler : change serial to transport handler .
 * Description: entry point for dispatching recursive Command Handler
 * For example: ApplicationCommandHandlerTransport_Compl -> (CC=S0) S0 handler (decrypting) 
 *			-> ApplicationCommandHandlerTransport_Compl -> (CC!=S0) Transport_ ApplicationCommandHandlerEx
 *
 */
void 	ApplicationCommandHandlerTransport_Compl(ts_param_t* p, uint8_t *pCmd, uint16_t cmdLength);

uint8_t Transport_SendRequestExt(
    ZW_APPLICATION_TX_BUFFER *pBufData,
    uint8_t dataLength,
    ts_param_t *p,
    void (*completedFunc)(uint8_t,void *, TX_STATUS_TYPE* ),
    void* user);



/**
 * @brief CommandClassSecurityVersionGet
 * get version
 * @return version
 */
uint8_t CommandClassSecurityVersionGet(void);

/* Call from ZW_Security_AES module when FSM goes idle.
 * This will deliver a delayed App Cmd handler to the
 * application. */
void
SetPreInclusionNIF(security_scheme_t target_scheme);


#endif