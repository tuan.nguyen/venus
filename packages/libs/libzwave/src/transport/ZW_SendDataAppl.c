
/* © 2014 Sigma Designs, Inc. This is an unpublished work protected by Sigma
 * Designs, Inc. as a trade secret, and is not to be used or disclosed except as
 * provided Z-Wave Controller Development Kit Limited License Agreement. All
 * rights reserved.
 *
 * Notice: All information contained herein is confidential and/or proprietary to
 * Sigma Designs and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law. Dissemination or
 * reproduction of the source code contained herein is expressly forbidden to
 * anyone except Licensees of Sigma Designs  who have executed a Sigma Designs’
 * Z-WAVE CONTROLLER DEVELOPMENT KIT LIMITED LICENSE AGREEMENT. The copyright
 * notice above is not evidence of any actual or intended publication of the
 * source code. THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED
 * INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS  TO REPRODUCE, DISCLOSE OR
 * DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL A PRODUCT THAT IT  MAY
 * DESCRIBE.
 *
 * THE SIGMA PROGRAM AND ANY RELATED DOCUMENTATION OR TOOLS IS PROVIDED TO COMPANY
 * "AS IS" AND "WITH ALL FAULTS", WITHOUT WARRANTY OF ANY KIND FROM SIGMA. COMPANY
 * ASSUMES ALL RISKS THAT LICENSED MATERIALS ARE SUITABLE OR ACCURATE FOR
 * COMPANY’S NEEDS AND COMPANY’S USE OF THE SIGMA PROGRAM IS AT COMPANY’S
 * OWN DISCRETION AND RISK. SIGMA DOES NOT GUARANTEE THAT THE USE OF THE SIGMA
 * PROGRAM IN A THIRD PARTY SERVICE ENVIRONMENT OR CLOUD SERVICES ENVIRONMENT WILL
 * BE: (A) PERFORMED ERROR-FREE OR UNINTERRUPTED; (B) THAT SIGMA WILL CORRECT ANY
 * THIRD PARTY SERVICE ENVIRONMENT OR CLOUD SERVICE ENVIRONMENT ERRORS; (C) THE
 * THIRD PARTY SERVICE ENVIRONMENT OR CLOUD SERVICE ENVIRONMENT WILL OPERATE IN
 * COMBINATION WITH COMPANY’S CONTENT OR COMPANY APPLICATIONS THAT UTILIZE THE
 * SIGMA PROGRAM; (D) OR WITH ANY OTHER HARDWARE, SOFTWARE, SYSTEMS, SERVICES OR
 * DATA NOT PROVIDED BY SIGMA. COMPANY ACKNOWLEDGES THAT SIGMA DOES NOT CONTROL
 * THE TRANSFER OF DATA OVER COMMUNICATIONS FACILITIES, INCLUDING THE INTERNET,
 * AND THAT THE SERVICES MAY BE SUBJECT TO LIMITATIONS, DELAYS, AND OTHER PROBLEMS
 * INHERENT IN THE USE OF SUCH COMMUNICATIONS FACILITIES. SIGMA IS NOT RESPONSIBLE
 * FOR ANY DELAYS, DELIVERY FAILURES, OR OTHER DAMAGE RESULTING FROM SUCH ISSUES.
 * SIGMA IS NOT RESPONSIBLE FOR ANY ISSUES RELATED TO THE PERFORMANCE, OPERATION
 * OR SECURITY OF THE THIRD PARTY SERVICE ENVIRONMENT OR CLOUD SERVICES
 * ENVIRONMENT THAT ARISE FROM COMPANY CONTENT, COMPANY APPLICATIONS OR THIRD
 * PARTY CONTENT. SIGMA DOES NOT MAKE ANY REPRESENTATION OR WARRANTY REGARDING THE
 * RELIABILITY, ACCURACY, COMPLETENESS, CORRECTNESS, OR USEFULNESS OF THIRD PARTY
 * CONTENT OR SERVICE OR THE SIGMA PROGRAM, AND DISCLAIMS ALL LIABILITIES ARISING
 * FROM OR RELATED TO THE SIGMA PROGRAM OR THIRD PARTY CONTENT OR SERVICES. TO THE
 * EXTENT NOT PROHIBITED BY LAW, THESE WARRANTIES ARE EXCLUSIVE. SIGMA OFFERS NO
 * WARRANTY OF NON-INFRINGEMENT, TITLE, OR QUIET ENJOYMENT. NEITHER SIGMA NOR ITS
 * SUPPLIERS OR LICENSORS SHALL BE LIABLE FOR ANY INDIRECT, SPECIAL, INCIDENTAL OR
 * CONSEQUENTIAL DAMAGES OR LOSS (INCLUDING DAMAGES FOR LOSS OF BUSINESS, LOSS OF
 * PROFITS, OR THE LIKE), ARISING OUT OF THIS AGREEMENT WHETHER BASED ON BREACH OF
 * CONTRACT, INTELLECTUAL PROPERTY INFRINGEMENT, TORT (INCLUDING NEGLIGENCE),
 * STRICT LIABILITY, PRODUCT LIABILITY OR OTHERWISE, EVEN IF SIGMA OR ITS
 * REPRESENTATIVES HAVE BEEN ADVISED OF OR OTHERWISE SHOULD KNOW ABOUT THE
 * POSSIBILITY OF SUCH DAMAGES. THERE ARE NO OTHER EXPRESS OR IMPLIED WARRANTIES
 * OR CONDITIONS INCLUDING FOR SOFTWARE, HARDWARE, SYSTEMS, NETWORKS OR
 * ENVIRONMENTS OR FOR MERCHANTABILITY, NONINFRINGEMENT, SATISFACTORY QUALITY AND
 * FITNESS FOR A PARTICULAR PURPOSE.
 *
 * The Sigma Program  is not fault-tolerant and is not designed, manufactured or
 * intended for use or resale as on-line control equipment in hazardous
 * environments requiring fail-safe performance, such as in the operation of
 * nuclear facilities, aircraft navigation or communication systems, air traffic
 * control, direct life support machines, or weapons systems, in which the failure
 * of the Sigma Program, or Company Applications created using the Sigma Program,
 * could lead directly to death, personal injury, or severe physical or
 * environmental damage ("High Risk Activities").  Sigma and its suppliers
 * specifically disclaim any express or implied warranty of fitness for High Risk
 * Activities.Without limiting Sigma’s obligation of confidentiality as further
 * described in the Z-Wave Controller Development Kit Limited License Agreement,
 * Sigma has no obligation to establish and maintain a data privacy and
 * information security program with regard to Company’s use of any Third Party
 * Service Environment or Cloud Service Environment. For the avoidance of doubt,
 * Sigma shall not be responsible for physical, technical, security,
 * administrative, and/or organizational safeguards that are designed to ensure
 * the security and confidentiality of the Company Content or Company Application
 * in any Third Party Service Environment or Cloud Service Environment that
 * Company chooses to utilize.
 */
#include "ZW_SendDataAppl.h"
#include "node_cache.h"
#include "S2_wrap.h"
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"
#include "serialAPI.h"
#include "utils.h"
#include "nvm.h"
#include "timer.h"
#include "ZW_TransportSecurity.h"
#include "list.h"
#include "memb.h"
#include "transport_service2_external.h"

typedef struct
{
    void *next;
    const void *pData;
    uint16_t dataLength;
    ts_param_t param;
    void *user;
    ZW_SendDataAppl_Callback_t callback;
} send_data_appl_session_t;

LIST(session_list);
MEMB(session_memb, send_data_appl_session_t, 8);
LIST(send_data_list);

send_request_state_t request;

static send_data_appl_session_t* current_session_ll;


void ts_set_std(ts_param_t *p, uint8_t dnode)
{
    p->dendpoint = 0;
    p->sendpoint = 0;
    p->snode = MyNodeId;
    p->dnode = dnode;
    p->rx_flags = 0;
    p->tx_flags = TRANSMIT_OPTION_ACK | TRANSMIT_OPTION_AUTO_ROUTE | TRANSMIT_OPTION_EXPLORE;
    p->scheme = AUTO_SCHEME;
}

void ZW_SendRequest_Callback(uint8_t status, void *user, TX_STATUS_TYPE *t)
{
    mainlog(logDebug,"send request->callback");
    if (status != TRANSMIT_COMPLETE_OK)
    {
        if (request.state == REQ_SENDING)
        {
            if (request.timer)
            {
                timerCancel(&request.timer);
                request.timer = 0;
            }

            request.state = REQ_DONE;

            if (request.callback)
            {
                request.callback(status,0,0);
            }
        }
    }
}

void ZCB_serialAPISendData(uint8_t status, TX_STATUS_TYPE *psTxInfo)
{
    mainlog(logDebug, "send_data -> ZCB_serialAPISendData");

    send_data_appl_session_t *s = list_pop(send_data_list);
    memb_free(&session_memb, s);
    if (s)
    {
        if (s->callback)
        {
            mainlog(logDebug,"callback func");
            s->callback(status, s->user, psTxInfo);
        }
    }
    else
    {
        //ASSERT(0);
        mainlog(logDebug, "Double callback?");
    }
}

security_scheme_t
highest_scheme(uint8_t scheme_mask)
{
    if (scheme_mask & NODE_FLAG_SECURITY2_ACCESS)
    {
        return SECURITY_SCHEME_2_ACCESS;
    }
    else if (scheme_mask & NODE_FLAG_SECURITY2_AUTHENTICATED)
    {
        return SECURITY_SCHEME_2_AUTHENTICATED;
    }
    else if (scheme_mask & NODE_FLAG_SECURITY2_UNAUTHENTICATED)
    {
        return SECURITY_SCHEME_2_UNAUTHENTICATED;
    }
    else if (scheme_mask & NODE_FLAG_SECURITY0)
    {
        return SECURITY_SCHEME_0;
    }
    else
    {
        return NO_SCHEME;
    }
}


/**
 * Calculate a priority for the scheme
 */
static int
scheme_priority(security_scheme_t scheme)
{
    switch (scheme)
    {
        case SECURITY_SCHEME_2_ACCESS:
            return 4;
        case SECURITY_SCHEME_2_AUTHENTICATED:
            return 3;
        case SECURITY_SCHEME_2_UNAUTHENTICATED:
            return 2;
        case SECURITY_SCHEME_0:
            return 1;
        default:
            return 0;
    }
}

/**
 * return true if a has a larger or equal scheme to b
 */
int
scheme_compare(security_scheme_t a, security_scheme_t b)
{
    return scheme_priority(a) >= scheme_priority(b);
}



void ZW_SendDataAppl_init()
{
    list_init(session_list);
    list_init(send_data_list);
    memb_init(&session_memb);
}

security_scheme_t
ZW_SendData_scheme_select(const ts_param_t *param, const uint8_t *data, uint8_t len)
{
    uint8_t dst_scheme_mask = 0; //Mask of schemes supported by destination
    uint8_t src_scheme_mask = 0;
    static security_scheme_t scheme;

    dst_scheme_mask = GetCacheEntryFlag(param->dnode);
    src_scheme_mask = GetCacheEntryFlag(MyNodeId);

    mainlog(logDebug, "dst_scheme_mask %02X:", dst_scheme_mask);
    mainlog(logDebug, "src_scheme_mask %02X:", src_scheme_mask);

    /* Check that this node node has at least one security scheme */

    if (len < 2 || (dst_scheme_mask & NODE_FLAG_KNOWN_BAD) || (src_scheme_mask & NODE_FLAG_KNOWN_BAD))
    {
        return NO_SCHEME;
    }

    //Common schemes
    dst_scheme_mask &= src_scheme_mask;

    mainlog(logDebug, "Common schemes %02X:", dst_scheme_mask);

    switch (param->scheme)
    {
    case AUTO_SCHEME:
        scheme = highest_scheme(dst_scheme_mask);
        mainlog(logDebug, "highest_scheme %02X:", scheme);
        return scheme;

    case NO_SCHEME:
        return NO_SCHEME;
    case USE_CRC16:
        return USE_CRC16;
    case SECURITY_SCHEME_2_ACCESS:
        if (dst_scheme_mask & NODE_FLAG_SECURITY2_ACCESS)
            return param->scheme;
        break;
    case SECURITY_SCHEME_2_AUTHENTICATED:
        if (dst_scheme_mask & NODE_FLAG_SECURITY2_AUTHENTICATED)
            return param->scheme;
        break;
    case SECURITY_SCHEME_2_UNAUTHENTICATED:
        if (dst_scheme_mask & NODE_FLAG_SECURITY2_UNAUTHENTICATED)
            return param->scheme;
        break;
    case SECURITY_SCHEME_0:
        if (dst_scheme_mask & NODE_FLAG_SECURITY0)
            return param->scheme;
        break;
    }
    mainlog(logDebug, "Scheme %x NOT supported by destination %i \n", param->scheme, param->dnode);
    return param->scheme;
}

/** * Low level send data. This call will not do any encapsulation except transport service encap
 */
uint8_t send_data(ts_param_t *p, uint8_t *data, uint16_t len,
                  ZW_SendDataAppl_Callback_t cb, void *user)
{
    int ret;
    /* store user callback and user param */
    send_data_appl_session_t *s;
    s = memb_alloc(&session_memb);
    if (s == 0)
    {
        mainlog(logDebug, "OMG! No more queue space");
        current_session_ll = list_head(send_data_list);
        return false;
    }
    s->pData = data;
    s->dataLength = len;
    s->param = *p;
    s->user = user;
    s->callback = cb;
    list_add(send_data_list, s);

    if (len > META_DATA_MAX_DATA_SIZE)
    //if (len > 39)
    {
        if (GetSupportedTransportServiceCmdClass(p->dnode))
        {
            ret = ZW_TransportService_SendData(p, data, len, ZCB_serialAPISendData);
        }
        else /* cannot send data with payload > META_DATA_MAX_DATA_SIZE without Transport service */
        {   
            mainlog(logWarning,"cannot send data with payload > META_DATA_MAX_DATA_SIZE without Transport service");
            ret = 0;
        }
    }
    else
    {
        ret = serialApiSendData(p->dnode, data, len,
                            p->tx_flags | TRANSMIT_OPTION_ACK, ZCB_serialAPISendData);
    }
    
    if (ret == 0)
    {
        mainlog(logDebug, "send_data failed");
        /*Remove the session from the list */
        list_remove(send_data_list,s);
        /*De-allocate the session */
        memb_free(&session_memb, s);
        current_session_ll = list_head(send_data_list);
        return false;
    }
    current_session_ll = list_head(send_data_list);
    return true;
}

static void
request_timeout(void *d)
{
    send_request_state_t *s = (send_request_state_t *)d;
    s->param.dnode = 0;

    s->state = REQ_DONE;
    if (s->timer)
    {
        timerCancel(&(s->timer));
        s->timer = 0;
    }

    mainlog(logDebug, "SendRequest timeout waiting for 0x%02x 0x%02x", s->class, s->cmd);
    if (s->callback)
    {
        s->callback(TRANSMIT_COMPLETE_FAIL, 0, 0);
    }
}

void set_request_timeout(send_request_state_t *s)
{
    if (s->timer)
    {
        timerCancel(&(s->timer));
        s->timer = 0;
    }

    timerStart(&(s->timer), request_timeout, (void *)s, s->timeout, TIMER_ONETIME);
}

void reset_request_timeout(send_request_state_t *s)
{
    s->state = REQ_DONE;
    if (s->timer)
    {
        timerCancel(&(s->timer));
        s->timer = 0;
    }
    if (s->callback)
    {
        s->callback(TRANSMIT_COMPLETE_FAIL, 0,0);
    }
}

uint8_t ZW_SendRequestAppl(ts_param_t *p, ZW_APPLICATION_TX_BUFFER *pData,
                        uint8_t dataLength,
                        uint8_t responseCmd,
                        uint16_t timeout, ZW_sendRequestAppCb callback)
{
    
    request.state = REQ_SENDING;
    request.callback = callback;
    request.class = pData->ZW_Common.cmdClass;
    request.cmd = responseCmd;
    request.timeout = timeout;
    request.param.snode = p->dnode;
    request.param.sendpoint = p->dendpoint;

    set_request_timeout(&request);

    if (!Transport_SendDataApplEP((uint8_t*)pData, dataLength, p, ZW_SendRequest_Callback, 0))
    {
        reset_request_timeout(&request);
        return 0;
    }
    return 1;
}

void ts_param_make_reply(ts_param_t *dst, const ts_param_t *src)
{
    dst->snode = src->dnode;
    dst->dnode = src->snode;
    dst->sendpoint = src->dendpoint;
    dst->dendpoint = src->sendpoint;
    dst->scheme = ZW_SendData_scheme_select(src, 0, 2);

    dst->tx_flags = ((src->rx_flags & RECEIVE_STATUS_LOW_POWER) ? TRANSMIT_OPTION_LOW_POWER : 0) | TRANSMIT_OPTION_ACK | TRANSMIT_OPTION_EXPLORE | TRANSMIT_OPTION_AUTO_ROUTE;
}

/**
 * Returns true if source and destinations are identical
 */
uint8_t
ts_param_cmp(ts_param_t* a1, ts_param_t* a2)
{
    return (a1->snode == a2->snode && a1->dnode == a2->dnode
                && a1->sendpoint == a2->sendpoint && a1->dendpoint == a2->dendpoint);
}




