#ifndef _ZW_TRANSPORTENDPOINT_H_
#define _ZW_TRANSPORTENDPOINT_H_

#include <stdint.h>
#include <stdbool.h>
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"
#include "config.h"

#include "serialAPI.h"
#include "utils.h"
#include "ZW_TransportSecurity.h"
#include "ZW_SendDataAppl.h"
#include "cmd_class_misc.h"


typedef struct _MULTICHAN_SOURCE_NODE_ID_
{
	uint8_t 	nodeId;
	uint8_t 	endpoint:7;
	uint8_t 	res:1;
}MULTICHAN_SOURCE_NODE_ID;

typedef struct _MULTICHAN_DEST_NODE_ID_
{
	uint8_t		nodeId;
	uint8_t 	endpoint:7;
	uint8_t 	BitAddress:1;
}MULTICHAN_DEST_NODE_ID;


typedef enum _ENDPOINT_
{
	ENDPOINT_ROOT =0 ,
	ENDPOINT_1,
	ENDPOINT_2,
	ENDPOINT_3,
	ENDPOINT_4,
	ENDPOINT_5,
	ENDPOINT_6,
	ENDPOINT_7,
	ENDPOINT_8,
	ENDPOINT_9
} ENDPOINT;

void Transport_ApplicationCommandHandler(
    ts_param_t 	*p,              
    uint8_t 	*pCmd, /* IN Payload from the received frame, the union */
                                  /*    should be used to access the fields */
    uint16_t   	cmdLength                /* IN Number of command bytes including the command */
);

extern received_frame_status_t
Transport_ApplicationCommandHandlerEx(
	ts_param_t *p,
	uint8_t *pCmd,
	uint16_t   cmdLength);

received_frame_status_t
Transport_ApplicationCommandHandlerInternal(
    ts_param_t *p,      
    uint8_t *pCmd,      
    uint16_t cmdLength);

void
RxToTxOptions( ts_param_t *rxopt,     
               ts_param_t *txopt)   ;

uint8_t Transport_SendDataApplEP(uint8_t *pData,
  								uint8_t  dataLength,
  								ts_param_t *p,
  								void (*completedFunc)(uint8_t,void *, TX_STATUS_TYPE*),
  								void* user);


bool CheckOfEndpointBitAddr(ts_param_t *rxOpt) ;

bool
Check_not_legal_response_job(ts_param_t *p); /*rxOpt pointer of type RECEIVE_OPTIONS_TYPE_EX */



#endif
