
#include "security_layer.h"
/* for S2 */
#include "S2_wrap.h"
#include "S2.h"
/* for S0 */
#include "S0_inclusion.h"
#include "S0.h"
#include "S0_inclusionRequired.h"

#include "ZW_TransportEndpoint.h"

#include "resource_directory.h"
#include "data_store_rd.h"
#include "PRNG.h"
#include "utils.h"
#include "node_cache.h"
#include "serialAPI.h"
#include "config.h"
#include "timer.h"
#include "platform_cc.h"
#include "s2_keystore.h"
#include "ZW_SendDataAppl.h"
#include "nvm.h"
#include "inline_swap_endian.h"

#define SECURITY_SCHEME_0_BIT 0x1

//Number of implemented schemes
#define N_SCHEMES 1

static Secure_learn ctx;
static timer_t inclusion_timer;
static sec_learn_complete_t callback;

static const ZW_NETWORK_KEY_VERIFY_FRAME key_verify ={ COMMAND_CLASS_SECURITY, NETWORK_KEY_VERIFY };

static uint8_t *secureCommandClassesList[N_SCHEMES] = {0}; /* List of supported command classes, when node communicate by this transport */
static uint8_t secureCommandClassesListCount[N_SCHEMES]; /* Count of elements in supported command classes list */

static void (*cbFuncZWSecureInclusion)(uint8_t, uint8_t, uint8_t *, uint8_t);

extern ZW_Node_t lastLearnedNodeZPC;



/*Forward */
static void send_and_raise(uint8_t node, uint8_t *pBufData, uint8_t dataLength, uint8_t txOptions,security_scheme_t scheme);

ts_param_t cur_tsparm;
static void send_and_raise_ex(uint8_t *pBufData, uint8_t dataLength);

/**
 * These frames are never used at the same time.
 */
static union
{
    ZW_SECURITY_SCHEME_INHERIT_FRAME scheme_inherit_frame;
    ZW_SECURITY_SCHEME_REPORT_FRAME scheme_report_frame;
    ZW_SECURITY_SCHEME_GET_FRAME scheme_get_frame;
    uint8_t key_set_frame[16 + 2];
    uint8_t cmd[64];
} tx;


static void send_data_callback(uint8_t status, void* user, TX_STATUS_TYPE *t)
{
    if (status == TRANSMIT_COMPLETE_OK)
    {
        mainlog(logDebug,"TX done ok");
        secure_learnIface_raise_tx_done(&ctx);
    } 
    else
    {
        mainlog(logDebug,"TX done fail");
        secure_learnIface_raise_tx_fail(&ctx);
    }
    secure_poll();
}

/**
 * timeout function used by the statemachine timer
 */
static void timeout(void* user)
{
    mainlog(logDebug,"Security timer triggered.");
    secure_learn_raiseTimeEvent(&ctx, user);
    secure_poll();
}

void application_default_set(void)
{
    rd_exit();
    rd_data_store_invalidate();
    rd_init(false, 0, 0);

    /*Enable SUC/SIS */
    serialApiSetSUCNodeID(MyNodeId, true, ZW_SUC_FUNC_NODEID_SERVER, false, NULL);

    security_set_default();
    sec2_create_new_network_keys();
}

/*
 * Load security states from eeprom
 */
void security_load_state()
{
    int8_t scheme;
    keystore_network_key_read(KEY_CLASS_S0,networkKey);
    nvm_config_get(security_scheme, &scheme);
    secure_learnIface_set_net_scheme(&ctx, scheme);
    sec0_set_key(networkKey);
}

void security_set_supported_classes(uint8_t* classes, uint8_t n_classes) 
{
    secureCommandClassesList[0] = classes;
    secureCommandClassesListCount[0] = n_classes;
}

/*
 * Save security states to eeprom
 */
void security_save_state()
{
    int8_t scheme;
    scheme = secure_learnIface_get_net_scheme(&ctx);
    mainlog(logDebug,"Setting scheme %x", scheme);
    nvm_config_set(security_scheme, &scheme);
    keystore_network_key_write(KEY_CLASS_S0, networkKey);
}

void secure_learn_setTimer(Secure_learn* handle,const sc_eventid evid, const sc_integer time_ms, const sc_boolean periodic)
{
    if (inclusion_timer)
    {
        timerCancel(&inclusion_timer);
        inclusion_timer = 0;
    }
    mainlog(logDebug,"S0I timeout %d",time_ms );
    timerStart(&inclusion_timer, timeout, (void*)evid, time_ms, TIMER_ONETIME);

}

void secure_learn_unsetTimer(Secure_learn* handle,const sc_eventid evid)
{
    if (inclusion_timer)
    {
        timerCancel(&inclusion_timer);
        inclusion_timer = 0;
    }
}

void secure_poll(void)
{
    int s = -1;

    while (s!=ctx.stateConfVector[0]) 
    {
        //LOG_PRINTF("sec state %i\n",ctx.stateConfVector[0]);
        s=ctx.stateConfVector[0];
        secure_learn_runCycle(&ctx);
    }
}

void secure_learnIface_send_commands_supported(const sc_integer node,const sc_integer snode, const sc_integer txOptions) CC_REENTRANT_ARG
{
    uint8_t scheme = 0;
    /*uint8_t roomLeft;
    uint8_t copyLen;*/
    int len;

    tx.cmd[0] = COMMAND_CLASS_SECURITY;
    tx.cmd[1] = SECURITY_COMMANDS_SUPPORTED_REPORT;
    tx.cmd[2] = 0;

    /* set supported command classes in S0 */
    security_set_supported_classes(myZPCCapability.node_capability_secureV0.aCapability,
                                    myZPCCapability.node_capability_secureV0.noCapability);

    if((cur_tsparm.snode == MyNodeId) && secureCommandClassesList[scheme] &&
        ((GetCacheEntryFlag(MyNodeId) & NODE_FLAGS_SECURITY) == NODE_FLAG_SECURITY0)) 
    {
        memcpy( &tx.cmd[3], secureCommandClassesList[scheme], secureCommandClassesListCount[0]);
        len = secureCommandClassesListCount[scheme];

        /* Add Unsolicited Destination CCs */
        /*
        roomLeft = sizeof(tx.cmd) - 3 - len;
        copyLen = nSecureClassesPAN < roomLeft ? nSecureClassesPAN : roomLeft;
        memcpy(&tx.cmd[3+secureCommandClassesListCount[scheme]], SecureClassesPAN, copyLen);
        len += copyLen;
        */
    } 
    else 
    {
        secureCommandClassesListCount[scheme]=0;
        len =0;
    }
    send_and_raise_ex( (uint8_t *) tx.cmd, 3 +len);
}


void /*RET Nothing                  */
security_CommandHandler(ts_param_t* p,
                    const ZW_APPLICATION_TX_BUFFER *pCmd, /* IN Payload from the received frame, the union */
                    uint8_t cmdLength) /* IN Number of command bytes including the command */
{
    uint8_t txOption;

    txOption = ((p->rx_flags & RECEIVE_STATUS_LOW_POWER) ? TRANSMIT_OPTION_LOW_POWER : 0) | TRANSMIT_OPTION_ACK
                    | TRANSMIT_OPTION_EXPLORE;

    if (pCmd->ZW_Common.cmdClass != COMMAND_CLASS_SECURITY && p->dnode != MyNodeId)
    {
        return;
    }


    /*If we are not in any inclusion state, and have no scheme, then ignore all s0 frames */
    if(secure_learn_isActive(&ctx, Secure_learn_main_region_Idle) && get_net_scheme()<=0) 
    {
        mainlog(logDebug," ignore all s0 frames, get net scheme: %d ",get_net_scheme());
        return;
    }

    switch (pCmd->ZW_Common.cmd)
    {
        /*Learn mode */
        case NETWORK_KEY_SET:
            if(p->scheme == SECURITY_SCHEME_0 &&
                    secure_learn_isActive(&ctx, Secure_learn_main_region_LearnMode_r1_Scheme_report) &&
                    cmdLength >= (sizeof(ZW_NETWORK_KEY_SET_1BYTE_FRAME) +15)) 
            {
                memcpy(networkKey, &pCmd->ZW_NetworkKeySet1byteFrame.networkKeyByte1, 16);
                sec0_set_key(networkKey);
                secure_learnIface_set_txOptions(&ctx, txOption);
                secure_learnIface_raise_key_set(&ctx, p->snode);
            }
        break;

        case SECURITY_SCHEME_GET:
            /* Make temporary NIF, used for inclusion */
            SetPreInclusionNIF(SECURITY_SCHEME_0);
            /*Make sure to clear the s2 keys.*/
            keystore_network_key_clear(KEY_CLASS_S2_UNAUTHENTICATED);
            keystore_network_key_clear(KEY_CLASS_S2_AUTHENTICATED);
            keystore_network_key_clear(KEY_CLASS_S2_ACCESS);
            /*Stop the S2 FSM */
            sec2_abort_join();
            MyNodeId = p->dnode;

            if(p->scheme == NO_SCHEME && cmdLength >= sizeof(ZW_SECURITY_SCHEME_GET_FRAME) ) 
            {
                secure_learnIface_set_scheme(&ctx, (pCmd->ZW_SecuritySchemeGetFrame.supportedSecuritySchemes & 0xFE) | 0x1);
                secure_learnIface_set_txOptions(&ctx, txOption);
                secure_learnIface_raise_scheme_get(&ctx, p->snode);
            }
            break;

        case SECURITY_SCHEME_INHERIT:
            /* MUST be received securely */
            if(p->scheme == SECURITY_SCHEME_0 && cmdLength>= sizeof(ZW_SECURITY_SCHEME_INHERIT_FRAME) ) 
            {
                /* Must support SCHEME_0*/
                if ((pCmd->ZW_SecuritySchemeInheritFrame.supportedSecuritySchemes & SECURITY_SCHEME_0_BIT) == 0) 
                {
                    secure_learnIface_set_txOptions(&ctx, txOption);
                    secure_learnIface_raise_scheme_inherit(&ctx, p->snode);
                }
            }
        break;

            /* Add node */
        case SECURITY_SCHEME_REPORT:
            if(p->scheme == SECURITY_SCHEME_0 || secure_learn_isActive(&ctx, Secure_learn_main_region_InclusionMode_r1_SchemeRequest)) 
            {
                if(cmdLength >= sizeof( ZW_SECURITY_SCHEME_REPORT_FRAME) ) 
                {
                    secure_learnIface_set_scheme(&ctx, (pCmd->ZW_SecuritySchemeReportFrame.supportedSecuritySchemes & 0xFE) |0x1);
                    secure_learnIface_raise_scheme_report(&ctx, p->snode);
                }
            }
        break;

        case NETWORK_KEY_VERIFY:
            if(p->scheme == SECURITY_SCHEME_0) 
            {
                secure_learnIface_raise_key_verify(&ctx, p->snode);
            }
        break;

        /* General */
        case SECURITY_COMMANDS_SUPPORTED_GET:
            if(p->scheme == SECURITY_SCHEME_0) 
            {
                ts_param_make_reply(&cur_tsparm,p);
                secure_learnIface_raise_commandsSupportedRequest(&ctx);
            }
        break;

        case SECURITY_NONCE_GET:
            if(p->scheme == NO_SCHEME) 
            {
                sec0_send_nonce(p);
            }
        break;

        case SECURITY_NONCE_REPORT:
            /* for debugging */
            /*
            mainlog(logDebug,"security_CommandHandler->SECURITY_NONCE_REPORT");
            */
            if(p->scheme == NO_SCHEME && cmdLength >= sizeof(ZW_SECURITY_NONCE_REPORT_FRAME)) 
            {
                sec0_register_nonce(p->snode,p->dnode,&pCmd->ZW_SecurityNonceReportFrame.nonceByte1 );
            }
        break;

        case SECURITY_MESSAGE_ENCAPSULATION_NONCE_GET:
        case SECURITY_MESSAGE_ENCAPSULATION:
        {
            /*TODO This is not ok for Large Z/IP frames */
            uint8_t rxBuffer[64];
            uint8_t len;

            if(secure_learn_isActive(&ctx, Secure_learn_main_region_Idle) &&
                    (isNodeBad(p->dnode) || isNodeBad(p->snode)) ) 
            {
                mainlog(logDebug,"Dropping security package from KNWON BAD NODE");
                return;
            }

            len = sec0_decrypt_message(p->snode,p->dnode,(uint8_t*)pCmd,cmdLength,rxBuffer);

            if(len) 
            {
                if(!isNodeSecure(p->snode)) 
                {
                    secure_learnIfaceI_register_scheme(p->snode,1<<SECURITY_SCHEME_0); /*TODO Is this ok ?*/
                }
                p->scheme = SECURITY_SCHEME_0;
                ApplicationCommandHandlerTransport_Compl(p, rxBuffer, len);

            }

            if(pCmd->ZW_Common.cmd == SECURITY_MESSAGE_ENCAPSULATION_NONCE_GET) 
            {
                sec0_send_nonce(p);
            }
        }
        break;
    }
    secure_poll();

}

/**
 * Store security state, ie. save scheme and keys.
 */
void secure_learnIfaceL_save_state()
{
    security_save_state();
}
/**
 * New keys should be generated.
 */
void secure_learnIfaceL_new_keys()
{
    sec0_reset_netkey();
}



static void send_and_raise_ex(uint8_t *pBufData, uint8_t dataLength) 
{
    if (!Transport_SendDataApplEP(pBufData, dataLength, &cur_tsparm, send_data_callback, 0))
    {
        secure_learnIface_raise_tx_fail(&ctx);
        secure_poll();
    }
}

void secure_learnIface_complete(const sc_integer scheme_mask)
{
    uint8_t flags = 0;
    /*Update eeprom with negotiated scheme*/
    if (scheme_mask > 0)
    {
        mainlog(logDebug,"Secure add/inclusion succeeded, with schemes 0x%x", scheme_mask);
        flags = NODE_FLAG_SECURITY0;
    } 
    else if(scheme_mask < 0)
    {
        flags = NODE_FLAG_KNOWN_BAD;
        mainlog(logDebug,"Secure add/inclusion failed");
    }

    if (callback)
    {
        callback(flags);
    }
}

/**
 * SendData wrapper that raises a tx_done or tx_fial on completion.
 * Send using highest available security scheme
 */
static void send_and_raise(uint8_t node, uint8_t *pBufData, uint8_t dataLength, uint8_t txOptions,security_scheme_t scheme)
{
    ts_param_t p;
    ts_set_std(&p,node);
    p.scheme = scheme;
    p.tx_flags = txOptions;


    if (!Transport_SendDataApplEP(pBufData, dataLength, &p, send_data_callback, 0))
    {
        secure_learnIface_raise_tx_fail(&ctx);
        secure_poll();
    }
}

int8_t get_net_scheme()
{
    return secure_learnIface_get_net_scheme(&ctx);
}

/****************************** Learn mode related functions *************************************/

/**
 * Enter learn mode
 */
void security_learn_begin(sec_learn_complete_t __cb)
{
    if (secure_learn_isActive(&ctx, Secure_learn_main_region_Idle))
    { 
        /* Anoying check but needed to protect the isController variable*/
        callback = __cb;

        if (serialApiTypeLibrary() & (ZW_LIB_CONTROLLER_STATIC | ZW_LIB_CONTROLLER | ZW_LIB_CONTROLLER_BRIDGE))
        {
            secure_learnIface_set_isController(&ctx, true);
        } 
        else
        {
            secure_learnIface_set_isController(&ctx, false);
        }

        mainlog(logDebug,"security_learn_begin");
        secure_learnIface_raise_learnRequest(&ctx);
        secure_poll();
    }
}

void secure_learnIfaceL_send_scheme_report(const sc_integer node, const sc_integer txOptions)
{

    uint8_t ctrlScheme; // The including controllers scheme
    uint8_t my_schemes;
    ctrlScheme = secure_learnIface_get_scheme(&ctx);
    my_schemes = secure_learnIface_get_supported_schemes(&ctx) & ctrlScheme; //Common schemes

    /* Check if controller supports schemes that I don't */
    if (ctrlScheme ^ my_schemes)
    {
        /* I'm not allowed to include nodes */

        /*FIXME ... what to do? */
    }

    tx.scheme_report_frame.cmdClass = COMMAND_CLASS_SECURITY;
    tx.scheme_report_frame.cmd = SECURITY_SCHEME_REPORT;
    tx.scheme_report_frame.supportedSecuritySchemes = 0;//my_schemes;

    if(secure_learnIface_get_net_scheme(&ctx) == 0) 
    {
        send_and_raise(node, (uint8_t *) &tx.scheme_report_frame, sizeof(ZW_SECURITY_SCHEME_REPORT_FRAME), txOptions,NO_SCHEME);
    } 
    else 
    {
        send_and_raise(node, (uint8_t *) &tx.scheme_report_frame, sizeof(ZW_SECURITY_SCHEME_REPORT_FRAME), txOptions,SECURITY_SCHEME_0);
    }
}

void secure_learnIfaceL_set_inclusion_key() REENTRANT
{
    uint8_t key[16];
    memset(key, 0, sizeof(key));
    sec0_set_key(key);
}

void secure_learnIfaceL_send_key_verify(const sc_integer node, const sc_integer txOptions)
{
    send_and_raise(node, (uint8_t *) &key_verify, sizeof(key_verify), txOptions,SECURITY_SCHEME_0);
}

/********************************** Add node related ******************************************/

uint8_t security_add_begin(uint8_t node, uint8_t txOptions, bool controller, sec_learn_complete_t __cb)
{
    if (secure_learn_isActive(&ctx, Secure_learn_main_region_Idle)  && !isNodeSecure(node))
    { 
        /* Annoying check but needed to protect the isController variable*/
        mainlog(logDebug,"Secure add begin");
        callback = __cb;
        secure_learnIface_set_isController(&ctx, controller);
        secure_learnIface_set_txOptions(&ctx, txOptions);
        secure_learnIface_raise_inclusionRequest(&ctx, node);
        secure_poll();
        return true;
    } 
    else 
    {
        return false;
    }

    return false;
}

void secure_learnIfaceI_send_scheme_get(const sc_integer node, const sc_integer txOptions)
{
    tx.scheme_get_frame.cmdClass = COMMAND_CLASS_SECURITY;
    tx.scheme_get_frame.cmd = SECURITY_SCHEME_GET;
    tx.scheme_get_frame.supportedSecuritySchemes = 0;
    //tx.scheme_get_frame.supportedSecuritySchemes = secure_learnIface_get_net_scheme(&ctx);
    send_and_raise(node, (uint8_t *) &tx.scheme_get_frame, sizeof(tx.scheme_get_frame), txOptions,NO_SCHEME);
}

void secure_learnIfaceI_send_key(const sc_integer node, const sc_integer txOptions) REENTRANT
{
    uint8_t inclusionKey[16];
    tx.key_set_frame[0] = COMMAND_CLASS_SECURITY;
    tx.key_set_frame[1] = NETWORK_KEY_SET;
    memcpy(tx.key_set_frame + 2, networkKey, 16);

    /*TODO It might be better to do this in some other way, maybe a txOption .... */
    memset(inclusionKey, 0, 16);
    sec0_set_key(inclusionKey);

    send_and_raise(node, (uint8_t *) &tx.key_set_frame, sizeof(tx.key_set_frame), txOptions,SECURITY_SCHEME_0);
}

void secure_learnIfaceI_send_scheme_inherit(const sc_integer node, const sc_integer txOptions)
{
    tx.scheme_inherit_frame.cmdClass = COMMAND_CLASS_SECURITY;
    tx.scheme_inherit_frame.cmd = SECURITY_SCHEME_INHERIT;
    tx.scheme_inherit_frame.supportedSecuritySchemes = 0;
    //tx.scheme_inherit_frame.supportedSecuritySchemes = secure_learnIface_get_net_scheme(&ctx);
    send_and_raise(node, (uint8_t *) &tx.scheme_inherit_frame, sizeof(tx.scheme_inherit_frame), txOptions,SECURITY_SCHEME_0);
}

void secure_learnIfaceI_restore_key()
{
    sec0_set_key(networkKey);
}

/**
 * Register which schemes are supported by a node
 */
void secure_learnIfaceI_register_scheme(const sc_integer node, const sc_integer scheme)
{
    //UpdateCacheEntry(node,0,0,0); //Make sure node exists in node cache
    /*TODO .... */
    if (scheme & SECURITY_SCHEME_0_BIT)
    {
        mainlog(logDebug,"Registering node %d with schemes %d",node,SECURITY_SCHEME_0_BIT);
        SetCacheEntryFlagMasked(node, NODE_FLAG_SECURITY0, NODE_FLAG_SECURITY0);
    } 
    else
    {
        SetCacheEntryFlagMasked(node, 0, NODE_FLAG_SECURITY0);
    }
}

uint8_t secure_learn_active() 
{
    return !secure_learn_isActive(&ctx, Secure_learn_main_region_Idle);
}

void sec0_abort_inclusion() 
{
    if(!secure_learn_isActive(&ctx, Secure_learn_main_region_Idle)) 
    {
        mainlog(logDebug,"S0 inclusion was aborted");
        secure_learn_init(&ctx);
        secure_learn_enter(&ctx);
        if (inclusion_timer)
        {
            timerCancel(&inclusion_timer);
            inclusion_timer = 0;
        }
    }
}


void s2_nvm_init(bool force_clean)
{
    uint32_t    magic = 0;
    uint8_t     set_nif_var = 0;
    uint16_t    version = NVM_CONFIG_VERSION;

    nvm_init(); 

    nvm_config_get(magic,&magic);

    /*magic != ZPCMAGIC no file is found*/
    if ((magic != ZPCMAGIC ) || force_clean)
    {
        mainlog(logDebug,"first time to create");
        magic = ZPCMAGIC;
        nvm_config_set(magic,&magic);
        nvm_config_set(config_version,&version);
        nvm_config_set(set_nif,&set_nif_var);

        application_default_set();
        sec2_create_new_static_ecdh_key();
        return;
    }

    /*Migrate nvm for old version*/
    nvm_config_get(config_version,&version);
    mainlog(logDebug,"NVM version is %i\n",version);
    if(version <= 1) 
    {
        version = NVM_CONFIG_VERSION;
        sec0_reset_netkey();
        sec2_create_new_static_ecdh_key();
        sec2_create_new_network_keys();
        nvm_config_set(config_version,&version);
    }
    rd_init(false, 0, 0);

}

void security_init(void)
{
    static int prng_initialized = 0;

    if(!prng_initialized) 
    {
        S2_init_prng();
        PRNGInit();
        prng_initialized = 1;
    }

    secure_learn_init(&ctx);
    secure_learn_enter(&ctx);

    /* Initialize the transport layer*/

    sec0_init();

    security_load_state();

    sec2_init();

}


/**
 * TODO maybe this should be a part of the state machine
 */
void security_set_default(void)
{
    uint8_t scheme = 0;

    if (serialApiTypeLibrary() & (ZW_LIB_CONTROLLER_STATIC | ZW_LIB_CONTROLLER | ZW_LIB_CONTROLLER_BRIDGE))
    {
        
        /*scheme = secure_learnIface_get_supported_schemes(&ctx);*/
        scheme = 1; /*default scheme */
        sec0_reset_netkey();
    }
    secure_learnIface_set_net_scheme(&ctx, scheme);
    security_save_state();
    security_init();
}





void ZCB_S2CommandsSupportedGet (uint8_t status, uint8_t* buf, uint8_t len)
{
    mainlog(logDebug,"ZCB_S2CommandsSupportedGet");
    hexdump(buf,len);

    if (cbFuncZWSecureInclusion)
    {
        if (len>1)
        {
            cbFuncZWSecureInclusion(ADD_SECURE_2_NODE_STATUS_DONE,lastLearnedNodeZPC.node_id,buf+2,len-2);
        }else
        {
            cbFuncZWSecureInclusion(ADD_SECURE_2_NODE_STATUS_FAILED,lastLearnedNodeZPC.node_id,0,0);
        }
    }

}

void ZCB_S0CommandsSupportedGet (uint8_t status, uint8_t* buf, uint8_t len)
{
    mainlog(logDebug,"ZCB_S0CommandsSupportedGet");
    hexdump(buf,len);
    
    if (cbFuncZWSecureInclusion)
    {
        if (len>1)
        {
            cbFuncZWSecureInclusion(ADD_SECURE_NODE_STATUS_DONE,lastLearnedNodeZPC.node_id,buf+3,len-3);
        }else
        {
            cbFuncZWSecureInclusion(ADD_SECURE_NODE_STATUS_FAILED,lastLearnedNodeZPC.node_id,0,0);
        }
    }
    

}


void
S0InclusionDone(int status)
{
    mainlog(logDebug,"S0InclusionDone node: %02X, status %00d ",lastLearnedNodeZPC.node_id,status);
    
    SetCacheEntryFlagMasked(lastLearnedNodeZPC.node_id,status & 0xFF, NODE_FLAGS_SECURITY);

    if (status & NODE_FLAG_KNOWN_BAD) /*failed to add S2 */
    {
        if (cbFuncZWSecureInclusion)
        {
            cbFuncZWSecureInclusion(ADD_SECURE_NODE_STATUS_FAILED,lastLearnedNodeZPC.node_id,0,0);
        }
        return;
    }

    lastLearnedNodeZPC.node_secure_scheme = highest_scheme(status & 0xFF);

    ts_param_t p;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = AUTO_SCHEME;

    p.snode = MyNodeId;
    p.sendpoint = 0;

    p.dnode = lastLearnedNodeZPC.node_id;
    p.dendpoint = 0;

    pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmdClass = COMMAND_CLASS_SECURITY; /* The command class */
    pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmd = SECURITY_COMMANDS_SUPPORTED_GET;                /* The command */

    ZW_SendRequestAppl(&p,(ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                              sizeof(ZW_SECURITY_COMMANDS_SUPPORTED_GET_FRAME),
                              SECURITY_COMMANDS_SUPPORTED_REPORT, 
                              20000, /*timeout 20 seconds */
                              ZCB_S0CommandsSupportedGet);

}

void
S2InclusionDone(int status)
{
    mainlog(logDebug,"S2InclusionDone node: %02X, status %00d ",lastLearnedNodeZPC.node_id,status);
    SetCacheEntryFlagMasked(lastLearnedNodeZPC.node_id,status & 0xFF, NODE_FLAGS_SECURITY);
    
    /* Create a new ECDH pair for next inclusion.*/
    sec2_create_new_dynamic_ecdh_key();

    if (status & NODE_FLAG_KNOWN_BAD) /*failed to add S2 */
    {
        if (cbFuncZWSecureInclusion)
        {
            cbFuncZWSecureInclusion(ADD_SECURE_2_NODE_STATUS_FAILED,lastLearnedNodeZPC.node_id,0,0);
        }
        return;
    }

    lastLearnedNodeZPC.node_secure_scheme = highest_scheme(status & 0xFF);

    ts_param_t p;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = AUTO_SCHEME;

    p.snode = MyNodeId;
    p.sendpoint = 0;

    p.dnode = lastLearnedNodeZPC.node_id;
    p.dendpoint = 0;

    if (status == NODE_FLAG_SECURITY0)
    {
        pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmdClass = COMMAND_CLASS_SECURITY; /* The command class */
        pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmd = SECURITY_COMMANDS_SUPPORTED_GET;       
        
        ZW_SendRequestAppl(&p,(ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                              sizeof(ZW_SECURITY_COMMANDS_SUPPORTED_GET_FRAME),
                              SECURITY_COMMANDS_SUPPORTED_REPORT, 
                              20000, /*timeout 20 seconds */
                              ZCB_S0CommandsSupportedGet);
    }
    else
    {

        pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmdClass = COMMAND_CLASS_SECURITY_2; /* The command class */
        pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmd = SECURITY_2_COMMANDS_SUPPORTED_GET;                /* The command */

        ZW_SendRequestAppl(&p,(ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                                  sizeof(ZW_SECURITY_2_COMMANDS_SUPPORTED_GET_FRAME),
                                  SECURITY_2_COMMANDS_SUPPORTED_REPORT, 
                                  10000, /*timeout 2 seconds */
                                  ZCB_S2CommandsSupportedGet);
    }
}

void securityS2InclusionStart(uint8_t bSource,  void (* completedFunc)(uint8_t,uint8_t,uint8_t*,uint8_t))
{
    cbFuncZWSecureInclusion = completedFunc;
    sec2_start_add_node(bSource, S2InclusionDone);
}


void securityS0InclusionStart(uint8_t bSource,  bool controller, void (* completedFunc)(uint8_t,uint8_t,uint8_t*,uint8_t))
{
    cbFuncZWSecureInclusion = completedFunc;
    security_add_begin(bSource, ZWAVE_PLUS_TX_OPTIONS, controller, S0InclusionDone);
}


int 
SecureInclusionRestart(int status, void (* completedFunc)(uint8_t,uint8_t,uint8_t*,uint8_t))
{
    cbFuncZWSecureInclusion = completedFunc;
    mainlog(logDebug,"SecureInclusionDone node: %02X, status %00d ",lastLearnedNodeZPC.node_id,status);
    if (status & NODE_FLAGS_SECURITY2)
    {
        S2InclusionDone(status);
        return 1;
    }

    if (status & NODE_FLAG_SECURITY0)
    {
        S0InclusionDone(status);
        return 1;
    }

    return 0;

}