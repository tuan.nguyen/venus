#ifndef _SECURIY_LAYER_H_
#define _SECURIY_LAYER_H_

#include "S2.h"
#include "S2_wrap.h"
#include "S2_inclusion.h"


void s2_nvm_init(bool force_clean);

/** \ingroup Security_Scheme
 *
 * @{
 */

/**
 * Callback function which indicates that the secure inclusion or
 * learn mode has completed.
 *
 * @param status < 0 for failure 0 for non-secure >0 for secure .
 */
typedef void (*sec_learn_complete_t)(int status);

/**
 * One-time initialization of the state machine
 * \param classes pointer to a byte array of command classes which are supported secure.
 * \param n_classes length of byte array
 */
void security_init();

/*Sets the supported security classes */
void security_set_supported_classes(uint8_t* classes, uint8_t n_classes);
/**
 * Resetting security states.
 */
void security_set_default();
/**
 * Begin the learn mode
 * on completion the callback function is called with scheme mask negotiated for
 * the this node. An empty scheme mask means that the secure negotiation has failed.
 * \param __cb callback function.
 */
void security_learn_begin(sec_learn_complete_t __cb);

/**
 * Begin the add node mode
 * on completion the callback function is called with scheme mask negotiated for
 * the new node. An empty scheme mask means that the secure negotiation has failed.
 * Only call this for nodes which support security
 *
 * \param node Node to negotiate with
 * \param txOptions Transmit options
 * \param controller TRUE if the node is a controller
 * \param __cb callback function.
 */
uint8_t security_add_begin(uint8_t node, uint8_t txOptions, bool controller, sec_learn_complete_t __cb);

/**
 * Input handler for the security state machine.
 * \param p Reception parameters
 * \param pCmd Payload from the received frame
 * \param cmdLength Number of command bytes including the command
 */

void security_CommandHandler(ts_param_t* p,
    const ZW_APPLICATION_TX_BUFFER *pCmd,
    uint8_t cmdLength);


/**
 * Get the current net_scheme
 * \return bit mask of supported security schemes by this node in this network. If this function returns 0
 * no schemes are supported, ie. node is not securely included.
 */
int8_t get_net_scheme();

/**
 * State machine polling...
 */
void secure_poll();

/**
 * \return TRUE if we are in learnmode or add node
 */
uint8_t secure_learn_active();



/**
 * Abort current inclusion
 */
void sec0_abort_inclusion();

/**
 * }@
 */

void securityS2InclusionStart(uint8_t bSource,  void (* completedFunc)(uint8_t,uint8_t,uint8_t*,uint8_t));

void securityS0InclusionStart(uint8_t bSource,  bool controller, void (* completedFunc)(uint8_t,uint8_t,uint8_t*,uint8_t));

int  SecureInclusionRestart(int status, void (* completedFunc)(uint8_t,uint8_t,uint8_t*,uint8_t));




#endif