/****************************************************************************
 *
 *
 * Copyright (c) 2014
 * Veriksytems, Inc.
 * All Rights Reserved
****************************************************************************/


#include "utils.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdarg.h>

#ifndef HEXDUMP_COLS
#define HEXDUMP_COLS 16
#endif

#define FILE_NAME          "/etc/config/ZWaveFileData.bin"
#define FILE_SIZE           1024
#define NETKEY_POSITION     1000
#define POLY                0x1021

FILE *fp;

uint8_t gLogLevel=3;
int     fd;

int init_file_data(void)
{
    fp=fopen(FILE_NAME,"rb");
    
    if (!fp)
    {
        fp=fopen(FILE_NAME,"wb");

        if (!fp)
        {
            printf("Unable to create file data\n");
            return -1;
        }

        printf("First time to create filedata\n");
        uint8_t init_data[FILE_SIZE];
        memset(init_data,0x00,FILE_SIZE);

        fwrite(init_data,FILE_SIZE,1,fp);
        fclose(fp);
        return 1;
    }
    fclose(fp);

    return 0;

}

int reset_file_data(void)
{
    fp=fopen(FILE_NAME,"wb");

    if (!fp)
    {
        printf("Unable to create file data\n");
        return -1;
    }

    uint8_t init_data[FILE_SIZE];
    memset(init_data,0x00,FILE_SIZE);

    fwrite(init_data,FILE_SIZE,1,fp);
    fclose(fp);
    return 0;

}

int write_data(uint16_t position, uint8_t* data, uint8_t data_len)
{
    fp=fopen(FILE_NAME,"r+b");

    if (!fp)
    {
        printf("Failed to open file \n");
        return -1;
    }
    if ((position+data_len)>FILE_SIZE)
    {
        printf("Exceed file size \n");
        return -2;
    }
    fseek(fp,position,SEEK_SET);
    fwrite(data,data_len,1,fp);
    fclose(fp);
    return 0;
}

int read_data(uint16_t position, uint8_t* data, uint8_t data_len)
{
    fp=fopen(FILE_NAME,"rb");

    if (!fp)
    {
        printf("Failed to open file \n");
        return -1;
    }
    if ((position+data_len)>FILE_SIZE)
    {
        printf("Exceed file size \n");
        return -2;
    }
    fseek(fp,position,SEEK_SET);
    fread(data,data_len,1,fp);
    fclose(fp);
    return 0;
}

void hexdump(void *mem, unsigned int len)
{
    if (gLogLevel < logDebug) return;
    if (!mem) return;
    if (len > 200) 
    {
        printf("hexdump out of len!!!\n" );
        return;
    }

    unsigned int i, j;

    for(i = 0; i < len + ((len % HEXDUMP_COLS) ? (HEXDUMP_COLS - len % HEXDUMP_COLS) : 0); i++)
    {
        /* print offset */
        if(i % HEXDUMP_COLS == 0)
        {
            printf("0x%06x: ", i);
        }

        /* print hex data */
        if(i < len)
        {
            printf("%02x ", 0xFF & ((char*)mem)[i]);
        }
        else /* end of block, just aligning for ASCII dump */
        {
            printf("   ");
        }

        /* print ASCII dump */
        if(i % HEXDUMP_COLS == (HEXDUMP_COLS - 1))
        {
            for(j = i - (HEXDUMP_COLS - 1); j <= i; j++)
            {
                if(j >= len) /* end of block, not really printing */
                {
                    putchar(' ');
                }
                else if(isprint(((char*)mem)[j])) /* printable char */
                {
                    putchar(0xFF & ((char*)mem)[j]);
                }
                else /* other char */
                {
                    putchar('.');
                }
            }
            putchar('\n');
        }
    }
}



uint16_t ZW_CheckCrc16(uint16_t crc,uint8_t *pDataAddr,uint16_t bDataLen)
{
    uint8_t WorkData;
    uint8_t bitMask;
    uint8_t NewBit;
    while(bDataLen--)
    {
        WorkData = *pDataAddr++;
        for (bitMask = 0x80; bitMask != 0; bitMask >>= 1) 
        {
            NewBit = ((WorkData & bitMask) != 0) ^ ((crc & 0x8000) != 0);
            crc <<= 1;
            if (NewBit) 
            {
                crc ^= POLY;
            }
        } /* for (bitMask = 0x80; bitMask != 0; bitMask >>= 1) */
    }
    return crc;
}

uint16_t ZW_CreateCrc16(uint8_t *pHeaderAddr,uint8_t bHeaderLen,uint8_t *pPayloadAddr,uint8_t bPayloadLen)
{
    uint16_t crc;
    crc = 0x1D0F;
    crc = ZW_CheckCrc16(crc, pHeaderAddr, bHeaderLen);
    crc = ZW_CheckCrc16(crc, pPayloadAddr, bPayloadLen);
    return crc;
}


void mainlog(uint8_t logLevel,const char *format,...)
{
    char buffer[1024];
    va_list vArgs;
    unsigned int len;
    va_start(vArgs,format);
    vsprintf((char*)buffer,(char const*)format,vArgs);
    va_end(vArgs);
    len=strlen(buffer);
    if (logLevel <= gLogLevel)
    {
        if (len < 1024) printf("%s\n",buffer);
    }
}

void mainlog_printArray(char* str, void* mem,unsigned int len)
{
    if (gLogLevel < logDebug) return;

    printf("%s----\n", str);
    hexdump(mem,len);
}


/*========================================================================================================
========================== convert int8, int16, int32 --> int32 ==========================================
==================================== Return value sign int ===============================================*/
int ConvertValuetoInt32(uint8_t size, int value)
{ 
    /*  size = 1 --> 8bit: 0x7f    --> 0x0000007f,   0xfe   --> 0xff ff ff fe (-1);
        size = 2 --> 16bit: 0x7fff --> 0x00 00 7fff, 0xfffe --> 0xff ff ff fe (-1) */
    
    switch (size)
    {
    case 1:
    {
        value = (int)(int8_t)value;
        break;
    }
    case 2:
    {
        value = (int)(int16_t)value;
        break;
    }
    default:
        break;
    }
    return value;
}