#ifndef _POWERLEVEL_TEST_H_
#define _POWERLEVEL_TEST_H_

#include "supported_CommandClassPowerLevel.h"

#define POWERLEVEL_FRAMECOUNT 10


void PowerLevelTest_CPowerLevelTest(bool printActive);
void PowerLevelTest_Init(uint8_t MyNodeId, uint8_t bSourceNode, uint8_t bDestNode, uint8_t bPowerLevel, int NbrTestFrames, bool fSwitchSource);
int PowerLevelTest_Set(void (*completedFunc)(POWLEV_STATUS Status));
uint8_t PowerLevelTest_IncPowerLevel();
uint8_t PowerLevelTest_GetPowerLevel();
short int PowerLevelTest_GetTestResult();
ePowLevStatus PowerLevelTest_Busy();
uint8_t PowerLevelTest_GetSourceNode();
uint8_t PowerLevelTest_GetDestNode();
void CmdHandlerPowerLevel(uint8_t rxStatus, uint8_t sourceNode, uint8_t *pCmd, uint8_t cmdLength);
void PowerLevelTest_UserActionCancel();

#endif /*_POWERLEVEL_TEST_H_*/
