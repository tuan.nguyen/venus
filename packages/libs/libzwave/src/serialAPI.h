/****************************************************************************
 *
 *
 * Copyright (c) 2014
 * Veriksytems, Inc.
 * All Rights Reserved
****************************************************************************/
#ifndef _SERIAL_API_H_
#define _SERIAL_API_H_

#include <stdbool.h>
#include <stdint.h>
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"
#include "timing.h"
#include "platform_cc.h"

extern ZW_APPLICATION_TX_BUFFER pTxBuf;

/*
 * Structure holding all the which are know from the ZWave application programmers
 * guide. They all serve the essentially same function, except for the ApplicationInitHW,
 * which is always called with  bWakeupReason==0
 */
struct SerialAPI_Callbacks 
{
    void    (*ApplicationCommandHandler)(uint8_t  rxStatus, uint8_t destNode, uint8_t  sourceNode, uint8_t *pCmd, uint8_t cmdLength);
    void    (*ApplicationNodeInformation)(uint8_t *deviceOptionsMask,APPL_NODE_TYPE *nodeType,uint8_t **nodeParm,uint8_t *parmLength );
    void    (*ApplicationControllerUpdate)(uint8_t bStatus,uint8_t bNodeID, uint8_t* pCmd,uint8_t bLen, uint8_t*);
    uint8_t (*ApplicationInitHW)(uint8_t bWakeupReason);
    uint8_t (*ApplicationInitSW)(void );
    void    (*ApplicationPoll)(void);
    void    (*ApplicationTestPoll)(void);
    void    (*ApplicationCommandHandler_Bridge)(uint8_t  rxStatus,uint8_t destNode, uint8_t  sourceNode, uint8_t *pCmd, uint8_t cmdLength);
    void    (*SerialAPIStarted)(uint8_t *pData, uint8_t pLen);

};



/*define for backup/restore Nvm */
enum NVM_BACKUP_RESTORE_OPERATION 
{
    NVMBackupRestoreOperationOpen = 0,
    NVMBackupRestoreOperationRead,
    NVMBackupRestoreOperationWrite,
    NVMBackupRestoreOperationClose

};

/* Buffer size */
#define BUF_SIZE    256 /* enough */

/* Indexes into receive buffer */
#define IDX_CMD     2
#define IDX_DATA    3

bool    serialApiInitialize (const struct SerialAPI_Callbacks* callbacks);

int     serialAPIDestruct(void);
bool    serialApiGetControllerCapabilities(uint8_t* pbControllerCapabilities);
void    serialApiAddNodeToNetwork(uint8_t mode, 
                                void (* completedFunc)(uint8_t,uint8_t,uint8_t*,uint8_t));
void    serialApiRemoveNodeFromNetwork(uint8_t mode, 
                                    void (* completedFunc)(uint8_t,uint8_t,uint8_t*,uint8_t));
void    serialApiMemoryGetID(uint8_t* pHomeID, uint8_t* pNodeID);
uint8_t serialApiGetSUCNodeID(void);
bool    serialApiGetInitData(uint8_t *ver, uint8_t *capabilities, uint8_t *len, uint8_t *nodesList, 
                            uint8_t* chip_type, uint8_t* chip_version);
void    serialApiGetNodeProtocolInfo(uint8_t bNodeID, NODEINFO* nodeInfo);
void    serialApiSetDefault(void (* completedFunc)(void));
uint8_t serialApiSendData(uint8_t nodeID, uint8_t *pData, uint8_t dataLength, uint8_t txOptions,
                          void (*completedFunc)(uint8_t, TX_STATUS_TYPE *));
uint8_t serialApiSendDataMulti(uint8_t *pNodeIDList, uint8_t numberNodes,
                                uint8_t*pData,uint8_t dataLength,uint8_t txOptions,
                                void (*completedFunc)(uint8_t txStatus));
void    serialApiGetRoutingInfo(uint8_t bNodeID, uint8_t *buf, uint8_t bRemoveBad, uint8_t bRemoveNonReps);
uint8_t serialApiSetSlaveLearnMode (uint8_t node, uint8_t mode, 
                            void (*completedFunc)(uint8_t state, uint8_t orgID, uint8_t newID));
uint8_t serialApiSendSlaveNodeInformation (uint8_t srcNode, uint8_t destNode,uint8_t txOptions, 
                            void (*completedFunc)(uint8_t txStatus));
uint8_t serialApiRequestNetworkUpdate(void (*completedFunc)(uint8_t));
uint8_t serialApiSetSUCNodeID(uint8_t bNodeID, uint8_t state, uint8_t capabilities, uint8_t txOption, 
                                void (*complFunc)(uint8_t));
uint8_t serialApiVersion(uint8_t* pBuf);
uint8_t serialApiGetLastWorkingRoute(uint8_t bNodeID, uint8_t *pRoute);
void    serialApiSetLearnMode (uint8_t mode, 
                            void (*completedFunc)(uint8_t bStatus, uint8_t bSource, uint8_t *pCmd, uint8_t bLen));
uint8_t serialApiRemoveFailedNode(uint8_t nodeID,
                                    void (* completedFunc)(uint8_t));
uint8_t serialApiReplicationSendData(uint8_t nodeID, uint8_t *pData, uint8_t dataLength, uint8_t txOptions, 
                                        void (*completedFunc)(uint8_t));
void    serialApiControllerChange(uint8_t mode, 
                                    void (*completedFunc)(uint8_t,uint8_t,uint8_t *,uint8_t));
uint8_t serialApiReplaceFailedNode(uint8_t nodeID,
                                    void (*completedFunc)(uint8_t));
uint8_t serialApiRequestNodeInfo(uint8_t bNodeID, 
                                void (*completedFunc)(uint8_t));
void    serialApiReplicationCommandComplete();
uint8_t serialApiSendTestFrame(uint8_t nodeID,uint8_t bPowerLevel,
                                    void (*completedFunc)(uint8_t));
uint8_t serialApiRF_Power_Level_Set(uint8_t bPowerLevel);
uint8_t serialApiRF_Power_Level_Get(void);
void    serialApiApplicationNodeInformation(uint8_t listening, APPL_NODE_TYPE nodeType, uint8_t  *nodeParm, uint8_t  parmLength);
void    serialApiAssignReturnRoute(uint8_t bSrcNodeID, uint8_t bDstNodeID, 
                                        void (*completedFunc)(uint8_t));
void    serialApiDeleteReturnRoute(uint8_t nodeID, 
                                        void (*completedFunc)(uint8_t));
void    serialApiAssignSUCReturnRoute(uint8_t bSrcNodeID, 
                                        void (*completedFunc)(uint8_t));
void    serialApiDeleteSUCReturnRoute(uint8_t nodeID, 
                                        void (*completedFunc)(uint8_t));
uint8_t serialApiIsFailedNode(uint8_t nodeID);
uint8_t serialApiSendNodeInformation(uint8_t destNode, uint8_t txOptions, 
                                            void (*completedFunc)(uint8_t));
uint8_t serialApiExploreRequestInclusion(void);
uint8_t serialApiExploreRequestExclusion(void);
bool    serialApiGetRandomWord(uint8_t *randomWord, bool bResetRadio);
bool    serialApiAES128Encrypt(const uint8_t *ext_input, uint8_t *ext_output, const uint8_t *cipherKey) CC_REENTRANT_ARG;
uint8_t serialApiTypeLibrary( void );
void    serialApiBackupRestoreNvm(uint8_t operation, uint16_t offset, uint8_t *buf, uint8_t length);
void    serialApiSendDataAbort(void);
void    serialApiGetCapabilities(uint8_t *capabilities);


bool    BitMask_isBitSet(uint8_t* pabBitMask,uint8_t bBitNo);
uint8_t SeqNo(void);

void    serialApiAddNodeToNetworkSmartStart(uint8_t bMode, uint8_t *dsk,
                    void (*completedFunc)(uint8_t, uint8_t, uint8_t *, uint8_t));

void    serialApiGetBackgroundRSSI(uint8_t *rssi_values, uint8_t *values_length);
void    serialApiRequestNodeNeighborUpdate(uint8_t nodeID, void (*completedFunc)(uint8_t));
uint8_t serialApiSetLastWorkingRoute(uint8_t bNodeID, uint8_t *pRoute);





#endif
