
#include <sys/timeb.h>
#include <time.h>
#include <limits.h>
#include <math.h>
#include "ZW_NetworkManagement.h"
#include "ZW_TransportSecurity.h"
#include "ZW_PowerLevelTest.h"
#include "ZW_RssiTest.h"
#include "serialAPI.h"
#include "utils.h"
#include "timing.h"
#include "timer.h"
#include "config.h"
#include "s2_keystore.h"
#include "slog.h"
#include "ZW_DevicePoll.h"


sNetworkManagement_Stat *nodeStats;
sNetworkManagement *spNetworkManagement;
sSample *spCurrentSample;
sTiming sTxTiming;
timer_t timerSecH;
char networkHealth_logfilenameStr[255];
long lNetworkManagementTimeSecondsTicker = 0;
FILE *fp_statLog;
int bLWRRouteIndex;
uint8_t *pabLWRRoute;
bool pafLWRNodesSupportPowerLevel[6];
int iLWRNumberOfNodesInLink;


void (*cbNetworkManagement_ZW_SendData)(uint8_t, TX_STATUS_TYPE*);
void (*cbNetworkManagement_DoRediscoverSingle)(uint8_t);


/* Internal callback funtion used for sampling transmit time for NH/Maintenance ZW_SendData usage. */
void ( *cbNetworkManagement_Timed_ZW_SendData)(uint8_t, TX_STATUS_TYPE* );
uint8_t testConnectionPayload[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8_t testConnectionTxOptions = TRANSMIT_OPTION_ACK | TRANSMIT_OPTION_AUTO_ROUTE | TRANSMIT_OPTION_EXPLORE;

bool StartNextNetworkHealthTest();
void NextNodeNetworkHealthTest();
void NetworkManagement_DumpNetworkHealth();
bool NetworkManagement_NH_Start(sNetworkManagement_Stat *spNodeUT);
void DoRequestForNodeInformation(uint8_t bNextIndex);
void NetworkManagement_NH_Calculate_RouteChange(sNetworkManagement_Stat *spNodeStats, int sampleNo, bool doLog);
void NetworkManagement_UpdateNetworkHealthIndicators(sNetworkManagement_Stat *spNodeStats);
void NetworkManagement_DumpNodeNeighbors(uint8_t i, uint8_t mode, uint8_t* nodes, uint8_t* noNode);
bool StartNextSingleNodeRediscovery();

long    lNetworkManagementCurrentSecondsTickerSample;
long    iSecondsBetweenEveryMaintenanceSample;
uint8_t bNodeExistMaskLen = 0;
uint8_t bNodeExistMask[ZW_MAX_NODEMASK_LENGTH];
uint8_t nodeListSize;
uint8_t bNetworkRepeaterCount;

timer_t timerH = 0;
timer_t timerNWM = 0;
bool    testStarted = false;
unsigned int bNodeID = 1;
uint8_t abListeningNodeList[232];
uint8_t bListeningNodeListSize;
uint8_t MySystemSUCID;
uint8_t MySystemHomeId[4];
uint8_t bDeviceCapabilities = 0;
uint8_t bSerialAPIVer = 0;
bool    afIsNodeAFLiRS[232];
uint8_t nodeType[ZW_MAX_NODES];
uint8_t nodeList[ZW_MAX_NODES];
sNetworkManagement  sNetworkManagementUT;
S_SERIALAPI_CAPABILITIES sController_SerialAPI_Capabilities;
uint8_t bMaxNodeID;
bool    requestUpdateOn = false;
uint8_t bDutNodeID;
uint8_t bPingNodeIndex;
uint8_t abPingFailed[232];
uint8_t abPingFailedSize;


bool NetworkHealth_Maintenance_Forever;



/*========================== MaintenanceState2String =========================
** Function description
**      Converts specified Maintenance State to a string
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
char 
*MaintenanceState2String(
    eMAINTENANCESTATE eMaintenanceState)
{
    char *pStateStr = "";

    switch(eMaintenanceState)
    {
        case MAINTENANCE_STATE_IDLE:
            pStateStr = "IDLE";
            break;

        case MAINTENANCE_STATE_SENDTEST:
            pStateStr = "MAINTENANCE_STATE_SENDTEST";
            break;

        case MAINTENANCE_STATE_UPDATEPERRC:
            pStateStr = "MAINTENANCE_STATE_UPDATEPERRC";
            break;

        case MAINTENANCE_STATE_CHECKPERRC:
            pStateStr = "MAINTENANCE_STATE_CHECKPERRC";
            break;

        case MAINTENANCE_STATE_NH_RUN_NODE_N_NOTFAILED:
            pStateStr = "MAINTENANCE_STATE_NH_RUN_NODE_N_NOTFAILED";
            break;

        case MAINTENANCE_STATE_NH_CHECK_NOT_FAILED:
            pStateStr = "MAINTENANCE_STATE_NH_CHECK_NOT_FAILED";
            break;

        case MAINTENANCE_STATE_FLAG_NODE_REDISCOV:
            pStateStr = "MAINTENANCE_STATE_FLAG_NODE_REDISCOV";
            break;

        case MAINTENANCE_STATE_USER_CHECK:
            pStateStr = "MAINTENANCE_STATE_USER_CHECK";
            break;

        case MAINTENANCE_STATE_USER_CHECK_DONE:
            pStateStr = "MAINTENANCE_STATE_USER_CHECK_DONE";
            break;

        case MAINTENANCE_STATE_NH_RUN_NODE_N_FAILED:
            pStateStr = "MAINTENANCE_STATE_NH_RUN_NODE_N_FAILED";
            break;

        case MAINTENANCE_STATE_NH_CHECK_FAILED:
            pStateStr = "MAINTENANCE_STATE_NH_CHECK_FAILED";
            break;

        case MAINTENANCE_STATE_USER_FULL_REDISCOV_DO:
            pStateStr = "MAINTENANCE_STATE_USER_FULL_REDISCOV_DO";
            break;

        case MAINTENANCE_STATE_USER_FULL_REDISCOV_DONE:
            pStateStr = "MAINTENANCE_STATE_USER_FULL_REDISCOV_DONE";
            break;

        case MAINTENANCE_STATE_CLEAR_ALL_RCLIFETIME:
            pStateStr = "MAINTENANCE_STATE_CLEAR_ALL_RCLIFETIME";
            break;

        case MAINTENANCE_STATE_CALLCENTER_NOTIFY:
            pStateStr = "MAINTENANCE_STATE_CALLCENTER_NOTIFY";
            break;

        case MAINTENANCE_STATE_UNDEFINED:
            pStateStr = "MAINTENANCE_STATE_UNDEFINED";
            break;

        default:
            pStateStr = "UNKNOWN MAINTENANCE STATE";
            break;
    }
    return pStateStr;
}
   
/*=========================== GetNodeMaintenanceState ===========================
** Function description
**      Get Maintenance state for specified node
**      
** Side effects:
**-----------------------------------------------------------------------------*/
eMAINTENANCESTATE
GetNodeMaintenanceState(
    sNetworkManagement_Stat *spNodeStats)
{
    return spNodeStats->sNodeState.eMaintenanceState;
}


/*============================ powerStatus2String ============================
** Function description
**      Convert Powerlevel status received from PowerLevelTest module to string
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
char *powerStatus2String(
    ePowLevStatus eStatus)
{
    char *pStatus;

    switch(eStatus)
    {
        case POWERLEV_TEST_TRANSMIT_COMPLETE_OK:
            pStatus = "Transmit OK";
            break;

        case POWERLEV_TEST_TRANSMIT_COMPLETE_NO_ACK:
            pStatus = "Transmit NO ACK";
            break;

        case POWERLEV_TEST_TRANSMIT_COMPLETE_FAIL:
            pStatus = "Transmit FAIL";
            break;

        case POWERLEV_TEST_NODE_REPORT_ZW_TEST_FAILED:
            pStatus = "Report Failed";
            break;

        case POWERLEV_TEST_NODE_REPORT_ZW_TEST_SUCCES:
            pStatus = "Report Success";
            break;

        case POWERLEV_TEST_NODE_REPORT_ZW_TEST_INPROGRESS:
            pStatus = "Report Inprogress";
            break;

        case POWERLEV_TEST_NODE_REPORT_ZW_TEST_TIMEOUT:
            pStatus = "Report TIMEOUT";
            break;

        default:
            pStatus = "UNKNOWN CMD";
            break;
    }
    return pStatus;
}


/*============================= AreNodeANeighbor =============================
** Function description
**      Checks if bNodeID is a neighbor according to the neighbor nodemask
** specified abNodeMask
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
bool AreNodeANeighbor(uint8_t bNodeID,uint8_t *abNodeMask)
{
    bool retVal = false;

    if ((bNodeID > 0) && (bNodeID <= ZW_MAX_NODES))
    {
        retVal = BitMask_isBitSet(abNodeMask, bNodeID);

    }
    return retVal;
}


void
SecondTickerTimeout(
    void* data)
{
    /* One more second passed */
    lNetworkManagementTimeSecondsTicker++;
}


/*======================== SingleNodeRediscovery_Done ========================
** Function description
**      Callback function called when the Rediscovery functionality started
** by StartNextSingleNodeRediscovery has finished.
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
SingleNodeRediscovery_Done(
    uint8_t bStatus)
{
    if (NULL != spNetworkManagement)
    {
        uint8_t bNodeID = spNetworkManagement->abNodesUnderTest[spNetworkManagement->bMaintenanceNHSCurrentIndex];

        if (0x22 == bStatus)
        {
            sNetworkManagement_Stat *spNodeStats = &spNetworkManagement->nodeUT[bNodeID - 1];

            mainlog(logDebug, "Node %03u now removed from 'needing Rediscovery' list", bNodeID);
            /* Node rediscovery successfull */
            spNetworkManagement->afMaintenanceNeedsRediscovery[bNodeID - 1] = false;
            /* Reset lifetime route change */
            spNodeStats->routeChangeLifetime = 0;
            spNodeStats->routeChangeLifetimeOld = 0;
        }
        else
        {
            mainlog(logDebug, "Node %03u, Rediscovery failed", bNodeID);
        }
        NetworkManagement_DumpNodeNeighbors(spNetworkManagement->bMaintenanceNHSCurrentIndex, false, NULL, NULL);
        spNetworkManagement->bMaintenanceNHSCurrentIndex++;
        StartNextSingleNodeRediscovery();
    }
}

/*========================= NetworkRediscoveryComplete =======================
** Function description
**      Callback function called when Network RediscoveryFunctionality has
** stopped.
** Side effects:
**
**--------------------------------------------------------------------------*/
void
NetworkRediscoveryComplete(
    uint8_t bStatus)
{
    testStarted = false;
    mainlog(logDebug, "Network Rediscovery stopped - Status %u", bStatus);
}





/*================= NetworkManagement_ReDiscoverySingle_Compl ================
** Function description
**   Callback function called when ZW_RequestNodeNeighborUpdate (started in
**   NetworkManagement_DoRediscoverySingle) returns
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
NetworkManagement_ReDiscoverySingle_Compl(
  uint8_t bStatus) /*IN Status of neighbor update process*/
{
    if (requestUpdateOn)
    {
        if (bStatus != 0x21)
        {
            if (bStatus == 0x22)
            {
                /* Node rediscovered */
                mainlog(logDebug, "Rediscovery done successful");
            }
            else
            {
                /* Node failed rediscovery */
                mainlog(logDebug, "Rediscovery done unsuccessfully");
            }
            if (NULL != cbNetworkManagement_DoRediscoverSingle)
            {
                cbNetworkManagement_DoRediscoverSingle(bStatus);
            }
        }
        else
        {
            mainlog(logDebug, "Rediscovery status %02X", bStatus);
        }
    }
}




/*=================== NetworkManagement_DoRediscoverySingle ==================
** Function description
**      Do Request Node neighbor Update on specified node
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
bool
NetworkManagement_DoRediscoverySingle(
    sNetworkManagement_Stat *spNodeStats, void (*TestCompleted)(uint8_t bStatus))
{
    bool retVal = false;
    if ((NULL != spNodeStats) && ((0 < spNodeStats->bDestNodeID) && (ZW_MAX_NODES >= spNodeStats->bDestNodeID)))
    {
        cbNetworkManagement_DoRediscoverSingle = TestCompleted;
        requestUpdateOn = true;
        /* Assume all Rediscovered */
        serialApiRequestNodeNeighborUpdate(spNodeStats->bDestNodeID, NetworkManagement_ReDiscoverySingle_Compl);
        retVal = true;
    }
    else
    {
        requestUpdateOn = false;
    }
    return retVal;
}



/*===== NetworkManagement_NetworkHealth_Maintenance_RediscoveryComplete ======
** Function description
**      Callback function called when NetworkManagement_DoRediscoveryStart
** functionality (used by NetworkManagement_NetworkHealth_Maintenance)
** has been executed.
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
NetworkManagement_NetworkHealth_Maintenance_RediscoveryComplete(
    uint8_t bStatus)
{
    if (NULL != spNetworkManagement)
    {
        spNetworkManagement->fMaintenanceFullDiscoveryStarted = false;
        spNetworkManagement->fMaintenanceNeedsFullRediscovery = false;
        /* If failed then we need to */
        spNetworkManagement->fMaintenanceNotifyCallCenter = (0 == bStatus);
        mainlog(logDebug, "Maintenance Network Full Rediscovery stopped - Status %u", bStatus);
    }
}


/*================ NetworkManagement_NH_Calculate_MinMaxAverage =================
** Function description
**      Updates average, min and max for node transmit metrics specified with 
** specified transmit sample
**      
** Side effects:
**
**-----------------------------------------------------------------------------*/
void
NetworkManagement_NH_Calculate_MinMaxAverage(
    sNetworkManagement_Stat *spNodeStats, int sampleNo)
{
    if ((NULL != spNodeStats) && (TESTCOUNT > sampleNo))
    {
        if (!spNodeStats->samples[sampleNo].failed)
        {
            /* Success */
            if (spNodeStats->min > spNodeStats->samples[sampleNo].sampleTime)
            {
                spNodeStats->min = spNodeStats->samples[sampleNo].sampleTime;
            }
            if (spNodeStats->max < spNodeStats->samples[sampleNo].sampleTime)
            {
                spNodeStats->max = spNodeStats->samples[sampleNo].sampleTime;
            }
        }
        else
        {
            /* Failed - Increment PER */
            spNodeStats->testTXErrors++;
        }
        spNodeStats->average = (double)(spNodeStats->samples[sampleNo].sampleTime + sampleNo * spNodeStats->average) / (sampleNo + 1);
    }
}


/*======================== AnyNodesNeedingRediscovery ========================
** Function description
**      Test the afMaintenanceNeedsRediscovery array if any nodes needs to
** rediscovered and returns true if any.
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
bool
AnyNodesNeedingRediscovery()
{
    bool retVal = false;
    int i;

    if (NULL != spNetworkManagement)
    {
        for (i = 0; i < spNetworkManagement->bNodesUnderTestSize; i++)
        {
            if (true == spNetworkManagement->afMaintenanceNeedsRediscovery[spNetworkManagement->abNodesUnderTest[i] - 1])
            {
                /* Found a node needing a neighbor update */
                retVal = true;
                break;
            }
        }
    }
    return retVal;
}


/*============================ LogNodeNetworkHealth ==========================
** Function description
**      Logs the Network Health result for the specified node
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
LogNodeNetworkHealth(
    sNetworkManagement_Stat *spNodeStats,
    bool fLogAllNHs)
{
    int i;

    if ((NULL != spNetworkManagement) && (NULL != spNodeStats))
    {
        printf("\n");
        mainlog(logDebug, "Log NetworkHealth -- Z-Wave Network Health for nodeID %03u completed", spNodeStats->bDestNodeID); 
        mainlog(logDebug, "%u tries to reach DUT %u times, PER %u. RC %u, Uniq LWR %u", 
                   spNodeStats->testCount,
                   spNodeStats->testCount - spNodeStats->testTXErrors, spNodeStats->testTXErrors,
                   spNodeStats->routeChange, 
                   spNodeStats->routeCount);
        /* TO#3934 fix */
        mainlog(logDebug, "Neighbor count %03u (%3.1f percent) out of %03u repeaters",
                   spNodeStats->neighborCount, 
                   (spNetworkManagement->bNetworkRepeaterCount != 0) ? ((double) spNodeStats->neighborCount / (double) (spNetworkManagement->bNetworkRepeaterCount + (spNodeStats->bControllerIsANeighborAndNotARepeater ? 1 : 0))) * 100 : (double) 0,
                   spNetworkManagement->bNetworkRepeaterCount + (spNodeStats->bControllerIsANeighborAndNotARepeater ? 1 : 0));

        /* Debug - Dump to screen and log Repeater Neighbors */
        char pCharBuf[1024];
        if (spNodeStats->neighborCount > 0)
        {
            snprintf(pCharBuf, sizeof(pCharBuf), "Neighbors ");
            snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "[");
            char cch = ',';
            for (i = 0; i < spNodeStats->neighborCount; i++)
            {
                if (i != 0)
                {
                    snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "%c", cch);
                }
                snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "%u", spNodeStats->abNeighbors[i]);
            }
            snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf) - strlen(pCharBuf), "]");
            mainlog(logDebug, pCharBuf);
        }
        else
        {
            mainlog(logDebug, "NO NEIGHBORS");
        }

        mainlog(logDebug, "Average for Successful transmit %.0f ms", spNodeStats->average);
        mainlog(logDebug, "Minimum transmit time           %li ms", spNodeStats->min);
        mainlog(logDebug, "Maximum transmit time           %li ms", spNodeStats->max);
        if (!fLogAllNHs)
        {
            mainlog(logDebug, "NH  = %02i", spNodeStats->NetworkHealthNumber);
        }
        else
        {
            mainlog(logDebug, "NH  = %02i [%02i(%02i),%02i(%02i),%02i(%02i),%02i(%02i),%02i(%02i),%02i(%02i)]", 
                       spNodeStats->NetworkHealthNumber,
                       spNodeStats->aNetworkHealthNumber[0], spNodeStats->aNetworkHealthNumberPriorToPowerLevelTest[0],
                       spNodeStats->aNetworkHealthNumber[1], spNodeStats->aNetworkHealthNumberPriorToPowerLevelTest[1],
                       spNodeStats->aNetworkHealthNumber[2], spNodeStats->aNetworkHealthNumberPriorToPowerLevelTest[2],
                       spNodeStats->aNetworkHealthNumber[3], spNodeStats->aNetworkHealthNumberPriorToPowerLevelTest[3],
                       spNodeStats->aNetworkHealthNumber[4], spNodeStats->aNetworkHealthNumberPriorToPowerLevelTest[4],
                       spNodeStats->aNetworkHealthNumber[5], spNodeStats->aNetworkHealthNumberPriorToPowerLevelTest[5]);
        }
        mainlog(logDebug, "NHS = %s\n", spNodeStats->NetworkHealthSymbol);


        /*notify to App*/
        if (1)
        {
            NOTIFY_TX_BUFFER_T pTxNotify;
            pTxNotify.NetworkHealthNotify.node_id = spNodeStats->bDestNodeID;
            pTxNotify.NetworkHealthNotify.neigbors_count = spNodeStats->neighborCount;
            pTxNotify.NetworkHealthNotify.total_repeaters = spNetworkManagement->bNetworkRepeaterCount + (spNodeStats->bControllerIsANeighborAndNotARepeater ? 1 : 0);
            memcpy(pTxNotify.NetworkHealthNotify.neighbors, spNodeStats->abNeighbors, pTxNotify.NetworkHealthNotify.neigbors_count);
            pTxNotify.NetworkHealthNotify.test_count = spNodeStats->testCount;
            pTxNotify.NetworkHealthNotify.PER = spNodeStats->testTXErrors;
            pTxNotify.NetworkHealthNotify.RC = spNodeStats->routeChange;
            pTxNotify.NetworkHealthNotify.route_count = spNodeStats->routeCount;
            pTxNotify.NetworkHealthNotify.NH = spNodeStats->NetworkHealthNumber;
            strcpy(pTxNotify.NetworkHealthNotify.NHS,spNodeStats->NetworkHealthSymbol);
            PushNotificationToHandler(NETWORK_HEALTH_NOTIFY, (uint8_t *)&pTxNotify.NetworkHealthNotify, sizeof(NETWORK_HEALTH_NOTIFY_T));
        }

    }
}


/*=========================== SetNodeMaintenanceState ===========================
** Function description
**      Set Maintenance state for specified node according to specified state
**      
** Side effects:
**      Update last MAINTENANCESTATE_BACKLOGSIZE Maintenance States FIFO 
**-----------------------------------------------------------------------------*/
void
SetNodeMaintenanceState(
    sNetworkManagement_Stat *spNodeStats,
    eMAINTENANCESTATE eNodeState)
{
    int i;
    /* The last MAINTENANCESTATE_BACKLOGSIZE Maintenance States are saved in a FIFO buffer with element ZERO oldest */
    for (i = 1; i < MAINTENANCESTATE_BACKLOGSIZE; i++)
    {
        spNodeStats->sNodeState.aeMaintenanceState[i - 1] = spNodeStats->sNodeState.aeMaintenanceState[i];
    }

    /* Save previous Maintenance State at element MAINTENANCESTATE_BACKLOGSIZE - 1 */
    spNodeStats->sNodeState.aeMaintenanceState[MAINTENANCESTATE_BACKLOGSIZE - 1] = spNodeStats->sNodeState.eMaintenanceState;
    spNodeStats->sNodeState.eMaintenanceState = eNodeState;
    if (eNodeState < MAINTENANCE_STATE_UNDEFINED)
    {
        if (spNodeStats->sNodeState.aeMaintenanceState[MAINTENANCESTATE_BACKLOGSIZE - 1] != eNodeState)
        {
            /* Increment number of transistions to this state */
            spNodeStats->sNodeState.aiTransistionCount[eNodeState]++;
        }
        SLOGI("Network Health Maintenance State: Node %03u, %s (%u)", spNodeStats->bDestNodeID,
                   MaintenanceState2String(eNodeState), spNodeStats->sNodeState.aiTransistionCount[eNodeState]);
    }
    else
    {
        SLOGI("Network Health Maintenance State: Node %03u, %s", spNodeStats->bDestNodeID, 
                   MaintenanceState2String(eNodeState));
    }
}

/*==================== NetworkManagement_NH_Calculate_StdDev ====================
** Function description
**      Calculates sample standard deviation on the current node transmit metrics 
**      
** Side effects:
**
**-----------------------------------------------------------------------------*/
void
NetworkManagement_NH_Calculate_StdDev(
    sNetworkManagement_Stat *spNodeStats)
{
    double sum = 0;
    int i;

    if (NULL != spNodeStats)
    {
        for (i = 0; i < spNodeStats->testCount; i++)
        {
            sum += pow((double)spNodeStats->samples[i].sampleTime - spNodeStats->average, 2);
        }
        spNodeStats->sampleStdDev = sqrt(((double)1/(spNodeStats->testCount - 1)) * sum);
    }
}


/*===================== NetworkManagement_CheckRepeater =====================
** Function description
**      Check if a repeater is neighbor to the destination and if it has 
**      a Green neighbor or is neighbor to the controller
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
bool
NetworkManagement_CheckRepeater(
    uint8_t bRepNodeID, 
    uint8_t bDestNodeID,
    uint8_t bMyNodeId,
    sNetworkManagement *spNetworkMan)
{
    int i,t;
    uint8_t abRoutingInfoNodeMask[ZW_MAX_NODEMASK_LENGTH];

    mainlog(logDebug, "CheckRepeater(%i, %i, %i)\n",bRepNodeID, bDestNodeID, bMyNodeId);
    mainlog(logDebug, "Repeater list for %i - ", bRepNodeID);

    /* Get all neighbors for repeater */
    memset(abRoutingInfoNodeMask, 0, sizeof(abRoutingInfoNodeMask));
    serialApiGetRoutingInfo(bRepNodeID, abRoutingInfoNodeMask, false, false);

    /* Debug output */
    for (t = 1; t <= ZW_MAX_NODES; t++)
    {
        if (BitMask_isBitSet(abRoutingInfoNodeMask, t))
        {
            printf("%i ", t);
        }
    }

    /* Check if repeater is neighbor to destination */
    if (0 == BitMask_isBitSet(abRoutingInfoNodeMask, bDestNodeID))
    {
        mainlog(logDebug, "Not neighbor to destination\n");
        return false;
    }

    /* Check if repeater is neighbor to this controller */
    if (1 == BitMask_isBitSet(abRoutingInfoNodeMask, bMyNodeId))
    {
        mainlog(logDebug, "Neighbor to Controller\n");
        return true;
    }

    /* The controller is not a neighbor, check if it has another repeater as neighbor */
    /* Get all repeater neighbors for the repeater */
    memset(abRoutingInfoNodeMask, 0, sizeof(abRoutingInfoNodeMask));
    serialApiGetRoutingInfo(bRepNodeID, abRoutingInfoNodeMask, false, true);

    /* Check if one of the repeaters has a Green NHS (NH >=8 ) in the last test */
    for (t = 1; t <= ZW_MAX_NODES; t++)
    {
        if (1 == BitMask_isBitSet(abRoutingInfoNodeMask, t))
        {
            for (i = 0; i < spNetworkMan->bNodesUnderTestSize; i++)
            {
                if (spNetworkMan->abNodesUnderTest[i] == t)
                {
                    if (spNetworkMan->nodeUT[spNetworkMan->abNodesUnderTest[i] - 1].bDestNodeID == spNetworkMan->abNodesUnderTest[i])
                    {
                        if (spNetworkMan->nodeUT[spNetworkMan->abNodesUnderTest[i] - 1].NetworkHealthNumber > 7)
                        {
                            mainlog(logDebug, "Neighbor to Green Repeater %i\n", spNetworkMan->abNodesUnderTest[i]);
                            return true;
                        }
                    }
                    else
                    {
                        mainlog(logDebug, "Found Neighbor but no Network Health Test has been done on Repeater %i\n", spNetworkMan->abNodesUnderTest[i]);
                    }
                }
            }
        }
    }
    return false;
}




/*=================== NetworkManagement_Maintenance_StateUpdate =================
** Function description
**      Update Maintenance state for node according to specified transmit metrics
**      
** Side effects:
**      
**-----------------------------------------------------------------------------*/
void
NetworkManagement_Maintenance_StateUpdate(
    sNetworkManagement_Stat *spNodeStats)
{
    if (NULL != spNodeStats)
    {
        if ((((10 > spNodeStats->testTXErrors) && (0 < spNodeStats->testTXErrors)) || 
             ((spNodeStats->routeChangeLifetime) && (spNodeStats->routeChangeLifetime + 2 > spNodeStats->neighborCount))) && 
            (!spNetworkManagement->afMaintenanceNeedsRediscovery[spNodeStats->bDestNodeID - 1]))
        {
            SetNodeMaintenanceState(spNodeStats, MAINTENANCE_STATE_NH_RUN_NODE_N_NOTFAILED);
            if (!spNetworkManagement->afMaintenanceNeedsNetworkHealthTest[spNodeStats->bDestNodeID - 1])
            {
                mainlog(logDebug, "NMSNF - Node %03u flaged in 'needing Network Health Test' list", spNodeStats->bDestNodeID);
                /* TO#3936 fix */
                if ((spNodeStats->routeChangeLifetime) && (spNodeStats->routeChangeLifetime + 2 > spNodeStats->neighborCount))
                {
                    mainlog(logDebug, "NMSNF - Node %03u flaged reason: RCLifetime && (RCLiftime %u+2 > %u NB)", spNodeStats->bDestNodeID, spNodeStats->routeChangeLifetime, spNodeStats->neighborCount);
                }
                else
                {
                    mainlog(logDebug, "NMSNF - Node %03u flaged reason: PER %u inside '10 > PER > 0'", spNodeStats->bDestNodeID, spNodeStats->testTXErrors);
                }
            }
            /* TO#3941 */
            if (spNodeStats->routeChangeLifetime)
            {
                spNodeStats->routeChangeLifetime--;
            }
            /* Node needs an Network Health Update */
            spNetworkManagement->afMaintenanceNeedsNetworkHealthTest[spNodeStats->bDestNodeID - 1] = true;
        }
        else if ((10 == spNodeStats->testTXErrors) && (!spNetworkManagement->afMaintenanceNeedsRediscovery[spNodeStats->bDestNodeID - 1]))
        {
            if (MAINTENANCE == spNetworkManagement->bTestMode)
            {
                SetNodeMaintenanceState(spNodeStats, MAINTENANCE_STATE_USER_CHECK);
                /* Only do this if in maintenance mode */
                mainlog(logDebug, "** ALERT ** NODE %03u NEEDS POWER and MANUAL OPERATION CHECK", spNodeStats->bDestNodeID);
                mainlog(logDebug, "Press key when POWER and MANUAL OPERATION CHECK has been executed");
                //_getch();
                
            }
            if (!spNetworkManagement->afMaintenanceNeedsNetworkHealthTest[spNodeStats->bDestNodeID - 1])
            {
                mainlog(logDebug, "NMSF - Node %03u flaged in 'needing Network Health Test' list ", spNodeStats->bDestNodeID);
            }
            SetNodeMaintenanceState(spNodeStats, MAINTENANCE_STATE_NH_RUN_NODE_N_FAILED);
            /* Node needs an Network Health Update */
            spNetworkManagement->afMaintenanceNeedsNetworkHealthTest[spNodeStats->bDestNodeID - 1] = true;
        }
    }
}

void NetworkManagement_Maintenance_StateUpdate_Cont()
{
    if (spNetworkManagement)
    {
        uint8_t bNodeID;
        sNetworkManagement_Stat *spNodeStats;

        bNodeID = spNetworkManagement->abNodesUnderTest[spNetworkManagement->bMaintenanceNHSCurrentIndex];
        spNodeStats = &spNetworkManagement->nodeUT[bNodeID - 1];

        SetNodeMaintenanceState(spNodeStats, MAINTENANCE_STATE_USER_CHECK_DONE);
    }
}

/*================= NetworkManagement_NH_Maintenance_Recalculate ===============
** Function description
**      Recalculates NH on current node transmit metrics
**      
** Side effects:
**      
**-----------------------------------------------------------------------------*/
void
NetworkManagement_NH_Maintenance_Recalculate(
    uint8_t bNodeID)
{
    int i;

    if ((NULL != spNetworkManagement) && (0 < bNodeID) && (ZW_MAX_NODES >= bNodeID))
    {
        if (bNodeID == spNetworkManagement->nodeUT[bNodeID - 1].bDestNodeID)
        {
            sNetworkManagement_Stat *spNodeStat = &spNetworkManagement->nodeUT[bNodeID - 1];
            /* Reinitialize */
            spNodeStat->routeCount = 0;
            spNodeStat->routeChange = 0;
            spNodeStat->testTXErrors = 0;
            spNodeStat->average = 0;    
            spNodeStat->max = 0;
            spNodeStat->min = LONG_MAX;
            spNodeStat->sampleStdDev = 0;
            /* Traverse all TESTCOUNT samples and recalculate NH */
            for (i = 0; i < TESTCOUNT; i++)
            {
                NetworkManagement_NH_Calculate_MinMaxAverage(spNodeStat, i);
                NetworkManagement_NH_Calculate_RouteChange(spNodeStat, i, false);
            }
            NetworkManagement_NH_Calculate_StdDev(spNodeStat);

            if (spNodeStat->samples[TESTCOUNT - 1].routeChanged)
            {
                spNodeStat->routeChangeLifetime++;
            }
            for (i = TESTROUNDS - 1; i > 0; i--)
            {
                spNodeStat->aNetworkHealthNumber[i - 1] = spNodeStat->aNetworkHealthNumber[i];
            }
            /* TO#3935 fix */
            spNodeStat->aNetworkHealthNumber[TESTROUNDS - 1] = spNodeStat->NetworkHealthNumber;
            /* Update Network Health Indicators */
            NetworkManagement_UpdateNetworkHealthIndicators(spNodeStat);

            /* In Maintenance we do not do Powerlevel Tests - just set it anyway */
            spNodeStat->NetworkHealthNumberPriorToPowerLevelTest = spNodeStat->NetworkHealthNumber;
            spNodeStat->fRecalculateNetworkHealth = false;
            NetworkManagement_Maintenance_StateUpdate(spNodeStat);

            //LogNodeNetworkHealth(spNodeStat, false);

            if (spNodeStat->routeChangeLifetimeOld != spNodeStat->routeChangeLifetime)
            {
                spNodeStat->routeChangeLifetimeOld = spNodeStat->routeChangeLifetime;
                mainlog(logDebug, "Network Health Maintenance - Route Change in Node %03u, RClifetime %i", spNodeStat->bDestNodeID, spNodeStat->routeChangeLifetime);
                //LogNodeNetworkHealth(spNodeStat, false);
            }

            if (spNodeStat->aNetworkHealthNumber[TESTROUNDS - 1] != spNodeStat->NetworkHealthNumber)
            {
                mainlog(logDebug, "Network Health Maintenance - Change detected in Node %03u", spNodeStat->bDestNodeID);
                mainlog(logDebug, "Network Health Maintenance - NH was %u, now %u", 
                           spNodeStat->aNetworkHealthNumber[TESTROUNDS - 1], spNodeStat->NetworkHealthNumber);
                /* If any change in NetworkHealthNumber detected then log it */
                LogNodeNetworkHealth(spNodeStat, false);
            }
        }
    }
}



/*============== NetworkManagement_IsFuncIDsSupported =================
** Function description
**      Checks if the specified function IDs are supported according to the
** SerialAPI supported functionality bitmask
**      
** Side effects:
**
**--------------------------------------------------------------------------*/
bool
NetworkManagement_IsFuncIDsSupported(
    uint8_t *pabFuncIDsToSupport,
    uint8_t bFuncIDsToSupportSize)
{
    bool retVal = true;
    int i;
    uint8_t *psCap = spNetworkManagement->psSerialAPIControllerCapabilities->abFuncIDSupportedBitmask;

    for (i = 0; i < bFuncIDsToSupportSize; i++)
    {
        int bCmd = pabFuncIDsToSupport[i];
        retVal = BitMask_isBitSet(psCap, bCmd);
        if (!retVal)
        {
            break;
        }
    }
    return retVal;
}

/*========================== InitializeTransmitMetrics ==========================
** Function description
**      Initialize transmit metrics specified by nodeStats
**      
** Side effects:
**      
**-----------------------------------------------------------------------------*/
void 
InitializeTransmitMetrics()
{
    if (NULL != nodeStats)
    {
        nodeStats->testCount = 0;
        nodeStats->routeCount = 0;
        nodeStats->routeChange = 0;
        nodeStats->testTXErrors = 0;
        nodeStats->average = 0; 
        nodeStats->max = 0;
        nodeStats->min = LONG_MAX;
        nodeStats->sampleStdDev = 0;
        memset(nodeStats->powStatus, 0, sizeof(nodeStats->powStatus));
        memset(nodeStats->samples, 0, sizeof(nodeStats->samples));
    }
}




/*=================== NetworkManagement_UpdateTrafficLight ===================
** Function description
**      Update Traffic Light indicator in structure pointed to by spNodeStat
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
NetworkManagement_UpdateTrafficLight(
    sNetworkManagement_Stat *spNodeStats)
{
    if (NULL != spNodeStats)
    {
        /* Set the color of the traffic light */
        if (spNodeStats->NetworkHealthNumber > 7)
        {
            spNodeStats->NetworkHealthSymbol = "GREEN";
        }
        else if (spNodeStats->NetworkHealthNumber > 3)
        {
            spNodeStats->NetworkHealthSymbol = "YELLOW";
        }
        else if (spNodeStats->NetworkHealthNumber > 0)
        {
            spNodeStats->NetworkHealthSymbol = "RED";
        }
        else
        {
            spNodeStats->NetworkHealthSymbol = "FAILED";
        }
    }
}


/*================ NetworkHealthUpdate_Maintenance_Complete ==================
** Function description
**      Callback function called when the Rediscovery functionality started
** by StartNextSingleNodeRediscovery has finished.
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
NetworkHealthUpdate_Maintenance_Complete(
    uint8_t bStatus)
{
    if (NULL != spNetworkManagement)
    {
        uint8_t bNodeID;
        sNetworkManagement_Stat *spNodeStats;

        spNetworkManagement->bTestMode = MAINTENANCE;
        bNodeID = spNetworkManagement->abNodesUnderTest[spNetworkManagement->bMaintenanceNHSCurrentIndex];
        spNodeStats = &spNetworkManagement->nodeUT[bNodeID - 1];

        if (7 < spNodeStats->NetworkHealthNumber)
        {
            /* NH Green */
            SetNodeMaintenanceState(spNodeStats, MAINTENANCE_STATE_IDLE);
        }
        else if (8 > spNodeStats->NetworkHealthNumber)
        {
            /* NH below Green */
            if (((MAINTENANCE_STATE_NH_RUN_NODE_N_NOTFAILED == GetNodeMaintenanceState(spNodeStats)) &&
                 ((2 < spNodeStats->testTXErrors) || (1 >= spNodeStats->NetworkHealthNumber)))
                || (0 == spNodeStats->NetworkHealthNumber))
            {
                /* Not possible to get in touch with Node Under Test */
                if (MAINTENANCE_STATE_NH_RUN_NODE_N_FAILED == GetNodeMaintenanceState(spNodeStats))
                {
                    SetNodeMaintenanceState(spNodeStats, MAINTENANCE_STATE_USER_FULL_REDISCOV_DO);
                    mainlog(logDebug, "NMC - Node %03u trickered 'full network Rediscovery'", bNodeID);
                    /* Somethings not right... */
                    mainlog(logDebug, "** ALERT ** NODE %03u TRICKERED FULL NETWORK REDISCOVERY", spNodeStats->bDestNodeID);
                    mainlog(logDebug, "Press key when to initiate FULL NETWORK REDISCOVERY");
                    //_getch();
                }
                else
                {
                    SetNodeMaintenanceState(spNodeStats, MAINTENANCE_STATE_USER_CHECK);
                    /* Only do this if in maintenance mode */
                    mainlog(logDebug, "** ALERT ** NODE %03u NEEDS POWER and MANUAL OPERATION CHECK", spNodeStats->bDestNodeID);
                    mainlog(logDebug, "Press key when POWER and MANUAL OPERATION CHECK has been executed");
                    //_getch();
                }
            }
            else
            {
                SetNodeMaintenanceState(spNodeStats, MAINTENANCE_STATE_FLAG_NODE_REDISCOV);
                if (!spNetworkManagement->afMaintenanceNeedsRediscovery[bNodeID - 1])
                {
                    mainlog(logDebug, "Node %03u flagged in 'needing Rediscovery' list", bNodeID);
                }
                spNetworkManagement->afMaintenanceNeedsRediscovery[bNodeID - 1] = true;
                SetNodeMaintenanceState(spNodeStats, MAINTENANCE_STATE_IDLE);
            }
        }
        /* Next node index to test */
        if (++spNetworkManagement->bMaintenanceNHSCurrentIndex >= spNetworkManagement->bNodesUnderTestSize)
        {
            if (NetworkHealth_Maintenance_Forever)
            {
                spNetworkManagement->bMaintenanceNHSCurrentIndex = 0;
            }
            else
            {
                NetworkManagement_NetworkHealth_MaintenanceStop();
                if (timerNWM)
                {
                    timerCancel(&timerNWM);  
                }
            }
        }
    }
}

void
NetworkHealthUpdate_Maintenance_Continued(
    uint8_t mode)
{
    if (NULL != spNetworkManagement)
    {
        uint8_t bNodeID;
        sNetworkManagement_Stat *spNodeStats;

        bNodeID = spNetworkManagement->abNodesUnderTest[spNetworkManagement->bMaintenanceNHSCurrentIndex];
        spNodeStats = &spNetworkManagement->nodeUT[bNodeID - 1];

        if (mode)
        {
            spNetworkManagement->fMaintenanceNeedsFullRediscovery = true;
        }
        else
        {
            SetNodeMaintenanceState(spNodeStats, MAINTENANCE_STATE_USER_CHECK_DONE);
            if (!spNetworkManagement->afMaintenanceNeedsNetworkHealthTest[spNodeStats->bDestNodeID - 1])
            {
                mainlog(logDebug, "NMC - Node %03u flaged in 'needing Network Health Test' list ", spNodeStats->bDestNodeID);
            }
            SetNodeMaintenanceState(spNodeStats, MAINTENANCE_STATE_NH_RUN_NODE_N_FAILED);
            /* Node needs an Network Health Update */
            spNetworkManagement->afMaintenanceNeedsNetworkHealthTest[spNodeStats->bDestNodeID - 1] = true;
        }
    }
}


/*============= NetworkManagement_UpdateNetworkHealthIndicators ==============
** Function description
**      Update Network Health Indicators in structure pointed to by spNodeStat
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
NetworkManagement_UpdateNetworkHealthIndicators(
    sNetworkManagement_Stat *spNodeStats)
{
    if (NULL != spNodeStats)
    {
        if (spNodeStats->testTXErrors == spNodeStats->testCount) // Failed
        {
            spNodeStats->NetworkHealthNumber = 0;
        }
        else if (spNodeStats->testTXErrors > 0) // Red
        {
            spNodeStats->NetworkHealthNumber = 1;
            if (spNodeStats->testTXErrors == 2)
            {
                spNodeStats->NetworkHealthNumber = 2;
            }
            if (spNodeStats->testTXErrors == 1)
            {
                spNodeStats->NetworkHealthNumber = 3;
            }
        }
        else if (spNodeStats->testTXErrors == 0) // Green or Yellow
        {
            if (spNodeStats->routeChange > 1) // Yellow
            {
                if (spNodeStats->routeChange <= 4)
                {
                    spNodeStats->NetworkHealthNumber = 5;
                }
                else if (spNodeStats->routeChange > 4)
                {
                    spNodeStats->NetworkHealthNumber = 4;
                }
            }
            else    // Green
            {
                if ((spNodeStats->routeChange == 0) && 
                    (spNodeStats->neighborCount > 2))
                {
                    spNodeStats->NetworkHealthNumber = 10;
                }
                else if ((spNodeStats->routeChange == 1) &&
                         (spNodeStats->neighborCount > 2))
                {
                    spNodeStats->NetworkHealthNumber = 9;
                }
                else
                {
                    spNodeStats->NetworkHealthNumber = 8;
                }
            }
        }
        NetworkManagement_UpdateTrafficLight(spNodeStats);
    }
}

/*===== NetworkManagement_NetworkHealth_Maintenance_TestNode_Complete ========
** Function description
**      Callback function called when NetworkManagement_ZW_SendData
** functionality has been executed or when a node needs a NetworkHealth 
** Recalculation (used by NetworkManagement_NetworkHealth_Maintenance).
**      A Network Health test can be started for node under Maintenance test.
** If no Network Health is started then the next node for Maintenance test
** is determined by incrementing the bMaintenanceNHSCurrentIndex.
**
** Side effects:
**
**--------------------------------------------------------------------------*/
void
NetworkManagement_NetworkHealth_Maintenance_TestNode_Complete(
    uint8_t bStatus, 
    TX_STATUS_TYPE *psTxInfo)
{
    bool retVal = false;

    mainlog(logDebug, "NetworkManagement_NetworkHealth_Maintenance_TestNode_Complete-->");
    if (NULL != spNetworkManagement)
    {
        if (spNetworkManagement->nodeUT[spNetworkManagement->abNodesUnderTest[spNetworkManagement->bMaintenanceNHSCurrentIndex] - 1].fRecalculateNetworkHealth)
        {
            NetworkManagement_NH_Maintenance_Recalculate(spNetworkManagement->abNodesUnderTest[spNetworkManagement->bMaintenanceNHSCurrentIndex]);
            if (spNetworkManagement->afMaintenanceNeedsNetworkHealthTest[spNetworkManagement->abNodesUnderTest[spNetworkManagement->bMaintenanceNHSCurrentIndex] - 1])
            {
                /* We need to make Network Health Test on node */
                spNetworkManagement->bTestMode = SINGLE;
                spNetworkManagement->bCurrentTestNodeID = spNetworkManagement->abNodesUnderTest[spNetworkManagement->bMaintenanceNHSCurrentIndex];
                retVal = NetworkManagement_NetworkHealth_Start(NetworkHealthUpdate_Maintenance_Complete);
                mainlog(logDebug, "Maintenance Network Health Update needed for Node %03u - %s", spNetworkManagement->abNodesUnderTest[spNetworkManagement->bMaintenanceNHSCurrentIndex], retVal ? "Initiated" : "Failed initiation");
                spNetworkManagement->afMaintenanceNeedsNetworkHealthTest[spNetworkManagement->abNodesUnderTest[spNetworkManagement->bMaintenanceNHSCurrentIndex] - 1] = (false == retVal);
            }
        }
        if (false == retVal)
        {
            /* Next node index to test */
            if (++spNetworkManagement->bMaintenanceNHSCurrentIndex >= spNetworkManagement->bNodesUnderTestSize)
            {
                spNetworkManagement->bMaintenanceNHSCurrentIndex = 0;
            }
        }
    }
}


/*================= NetworkManagement_ZW_SendData_Completed ==================
** Function description
**      Callback function for NetworkManagement API Z-Wave API ZW_SendData 
** wrapper function. If destination was eligible for transmit metric gathering
** then the destination is marked as needing a NH recalculation.
** A potential Application specified callback function is called with 
** transmit status.
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
NetworkManagement_ZW_SendData_Completed(
    uint8_t bTxStatus, TX_STATUS_TYPE *psTxInfo)
{
    if (NULL != spCurrentSample)
    {
        /* Now a recalculation of NH is needed */
        nodeStats->fRecalculateNetworkHealth = true;
        /* Release nodeStats and spCurrentSample for next sample */
        spCurrentSample = NULL;
        nodeStats = NULL;
    }
    /* Call the registered callback function with transmission result */
    if (NULL != cbNetworkManagement_ZW_SendData)
    {
        cbNetworkManagement_ZW_SendData(bTxStatus, psTxInfo);
    }
}




/*=================== NetworkManagement_UpdateNeighborInfo ===================
** Function description
**      Update neighbor info in structure pointed to by spNodeStat.
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void NetworkManagement_UpdateNodeNeighbor(uint8_t bControllerNodeID, sNetworkManagement_Stat *spNodeStat)
{
    uint8_t bNeighborCount = 0;
    int bCurrentNodeID;

    memset(spNodeStat->abRoutingInfoNodeMask, 0, sizeof(spNodeStat->abRoutingInfoNodeMask));
    serialApiGetRoutingInfo(spNodeStat->bDestNodeID, spNodeStat->abRoutingInfoNodeMask, false, false);

    if ((bControllerNodeID != spNodeStat->bDestNodeID) && 
        AreNodeANeighbor(bControllerNodeID, spNodeStat->abRoutingInfoNodeMask))
    {
        /* bControllerNodeID/GW are a Neighbor */
        spNodeStat->abNeighbors[bNeighborCount] = bControllerNodeID;
        spNodeStat->bControllerIsANeighborAndNotARepeater = true;
        bNeighborCount++;
    }
    else
    {
        spNodeStat->bControllerIsANeighborAndNotARepeater = false;
    }

    serialApiGetRoutingInfo(spNodeStat->bDestNodeID, spNodeStat->abRoutingInfoNodeMask, false, true);

    for (bCurrentNodeID = 1; bCurrentNodeID <= ZW_MAX_NODES; bCurrentNodeID++)
    {
        if (AreNodeANeighbor(bCurrentNodeID, spNodeStat->abRoutingInfoNodeMask))
        {
            /* We must not count bControllerNodeID/GW twice */
            if (bCurrentNodeID != bControllerNodeID)
            {
                /* Another neighbor found */
                spNodeStat->abNeighbors[bNeighborCount] = bCurrentNodeID;
                bNeighborCount++;
            }
            else
            {
                /* Controller is a neighbor but also a repeater */
                spNodeStat->bControllerIsANeighborAndNotARepeater = false;
            }
        }
    }
    spNodeStat->neighborCount = bNeighborCount;
}

/*======================== IsCommandClassSupported ===========================
** Function description
**      Determines if bNodeID supports the bCommandClass according to the
** registered nodeDescriptor in spNetworkManagement.
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
bool
IsCommandClassSupported(
    uint8_t bNodeID,
    uint8_t bCommandClassID)
{
    uint8_t bIndex;
    bool fSupported = false;
    int n;

    if (NULL != spNetworkManagement)
    {
        if ((bNodeID > 0) && (bNodeID <= ZW_MAX_NODES))
        {
            bIndex = bNodeID - 1;
            for (n = 0; n < sizeof(spNetworkManagement->nodeDescriptor[bIndex].cmdClasses); n++)
            {
                if (0 == spNetworkManagement->nodeDescriptor[bIndex].cmdClasses[n])
                {
                    /* No more COMMAND CLASSES supported by bNodeID */
                    break;
                }
                if (bCommandClassID == spNetworkManagement->nodeDescriptor[bIndex].cmdClasses[n])
                {
                    fSupported = true;
                    break;
                }
            }
        }
    }
    return fSupported;
}


void NetworkManagement_UpdateNeighborInformation()
{
    int i;
    for (i = 0; i < spNetworkManagement->bNodesUnderTestSize; i++)
    {
        uint8_t n = spNetworkManagement->abNodesUnderTest[i] - 1;
        spNetworkManagement->nodeUT[n].bDestNodeID = spNetworkManagement->abNodesUnderTest[i];
        NetworkManagement_UpdateNodeNeighbor(spNetworkManagement->bControllerNodeID, &spNetworkManagement->nodeUT[n]);
    }
}

/*============== NetworkManagement_CalculateSerialAPIComDelay ================
** Function description
**      Makes a serial transmit time measurement to determine the Tapp, 
** which is the Serial communication delay introduced by the interface between
** the HOST SerialAPI implementation and the Z-Wave SerialAPI Module.
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
long
NetworkManagement_CalculateSerialAPIComDelay()
{
    /* HOST transmit metrics sampling specifics - Only needed if hw do not support transmit latency sampling */
    //double retVal = LONG_MAX;
    timingGetClockSystem(&sTxTiming);
    serialApiGetSUCNodeID();

    return (long)timingGetElapsedMSec(&sTxTiming);
}

/*========================== RouteChangeByLatencyJitter =========================
** Function description
**      Checks if testRouteDurationMAX has been exceeded when comparing specified
** sampleNo and sampleNo - 1 transmit latencies, if so the sample is classified
** as a RC and the function returns true. 
**      
** Side effects:
**
**-----------------------------------------------------------------------------*/
bool
RouteChangeByLatencyJitter(
    sNetworkManagement_Stat *spNodeStats, int sampleNo)
{
    return ((spNodeStats->samples[sampleNo].sampleTime > spNodeStats->samples[sampleNo - 1].sampleTime) && 
            (spNodeStats->testRouteDurationMAX < spNodeStats->samples[sampleNo].sampleTime - spNodeStats->samples[sampleNo - 1].sampleTime));
}


/*================== NetworkManagement_NH_Calculate_RouteChange =================
** Function description
**      Traverses through the current node transmit metrics and determines if any
** route changes has occured either through long transmit time or LWR change
**      
** Side effects:
**
**-----------------------------------------------------------------------------*/
void
NetworkManagement_NH_Calculate_RouteChange(
    sNetworkManagement_Stat *spNodeStats, int sampleNo, bool doLog)
{
    int i;
    if ((NULL != spNodeStats) && (TESTCOUNT > sampleNo))
    {
        /* Route Change detection - First Test Sample no route change can be detected */
        if (0 < sampleNo)
        {
            bool fEqual = false;
            for (i = 0; i < sampleNo; i++)
            {
                if ((spNodeStats->samples[sampleNo].failed) ||
                    (memcmp(spNodeStats->samples[i].pRoute, spNodeStats->samples[sampleNo].pRoute, sizeof(spNodeStats->samples[i].pRoute)) == 0))
                {
                    /* Either a matching LWR has been found or current LWR failed */
                    /* A failed transmit have no LWR, so it cannot be uniq... */
                    fEqual = true;
                    break;
                }
            }
            if (false == fEqual)
            {
                spNodeStats->samples[sampleNo].routeUniq = true;
                spNodeStats->routeCount++;
            }
            else
            {
                spNodeStats->samples[sampleNo].routeUniq = false;
            }
            /* If either testRouteDurationMAX Route Change detection time has been crossed or */
            /* a change in the LWR has been detected then the transmission will be marked as having */
            /* a "Route Change" occurence */
            spNodeStats->samples[sampleNo].routeChanged = false;

            if ((memcmp(spNodeStats->samples[sampleNo - 1].pRoute, spNodeStats->samples[sampleNo].pRoute, sizeof(spNodeStats->samples[sampleNo - 1].pRoute)) != 0) ||
                RouteChangeByLatencyJitter(spNodeStats, sampleNo))
            {
                /* Test Transmit classified as a "Route Change" occured */
                spNodeStats->routeChange++;
                spNodeStats->samples[sampleNo].routeChanged = true;
                /* */
                doLog = true;
            }
            if (doLog)
            {
                mainlog(logDebug, "Route quality - %s (%lf), uniq LWR %s",
                           spNodeStats->samples[sampleNo].routeChanged ? "Change" : "Stable", 
                           spNodeStats->samples[sampleNo].sampleTime - spNodeStats->samples[sampleNo - 1].sampleTime,
                           spNodeStats->samples[sampleNo].routeUniq ? "TRUE" : "FALSE");
            }
        }
        else
        {
            if (doLog)
            {
                spNodeStats->samples[0].routeUniq = true;
                spNodeStats->routeCount = 1;
                mainlog(logDebug, "Route quality - %s (%li), uniq LWR TRUE",
                           spNodeStats->samples[sampleNo].routeChanged ? "Change" : "Stable", 0);
            }
        }
        if (doLog)
        {
            mainlog(logDebug, "Node %03u test %03u %s... Latency %lf ms", spNodeStats->bDestNodeID, sampleNo, !spNodeStats->samples[sampleNo].failed ? "Success" : "Failed", 
                       spNodeStats->samples[sampleNo].sampleTime);
            mainlog(logDebug, "Node %03u LWR - %s [0x%02X, 0x%02X, 0x%02X, 0x%02X at %s]", spNodeStats->bDestNodeID, 
                        (spNodeStats->samples[sampleNo].routeFound == 0) ? "NONE" : "FOUND", 
                        spNodeStats->samples[sampleNo].pRoute[ROUTECACHE_LINE_REPEATER_1_INDEX], 
                        spNodeStats->samples[sampleNo].pRoute[ROUTECACHE_LINE_REPEATER_2_INDEX], 
                        spNodeStats->samples[sampleNo].pRoute[ROUTECACHE_LINE_REPEATER_4_INDEX], 
                        spNodeStats->samples[sampleNo].pRoute[ROUTECACHE_LINE_REPEATER_4_INDEX],
                        (spNodeStats->samples[sampleNo].pRoute[ROUTECACHE_LINE_CONF_INDEX] == ZW_LAST_WORKING_ROUTE_SPEED_100K) ? "100k" :
                        (spNodeStats->samples[sampleNo].pRoute[ROUTECACHE_LINE_CONF_INDEX] == ZW_LAST_WORKING_ROUTE_SPEED_40K) ? "40k" :
                        (spNodeStats->samples[sampleNo].pRoute[ROUTECACHE_LINE_CONF_INDEX] == ZW_LAST_WORKING_ROUTE_SPEED_9600) ? "9.6k" : "Auto");
        }
    }
    else if (TESTCOUNT > sampleNo)
    {
        mainlog(logDebug, "Node %03u test %03u NOT valid - Only test 000..%03u", spNodeStats->bDestNodeID, sampleNo, TESTCOUNT - 1);
    }
}



bool
NetworkManagement_Init(
    S_SERIALAPI_CAPABILITIES *psSerialAPIControllerCapabilities,
    uint8_t *abListeningNodeList, 
    uint8_t bListeningNodeListSize, 
    bool *afIsNodeAFLiRS, 
    uint8_t bNetworkRepeaterCount,
    sNetworkManagement *spNetworkMan)
{
    bool retVal = false;
    uint8_t abNeedSerialAPIFunctionality[] = {FUNC_ID_ZW_SEND_DATA,
                                           FUNC_ID_ZW_REQUEST_NODE_NEIGHBOR_UPDATE,
                                           FUNC_ID_ZW_GET_SUC_NODE_ID,
                                           FUNC_ID_ZW_REQUEST_NODE_INFO,
                                           FUNC_ID_GET_ROUTING_TABLE_LINE,
                                           FUNC_ID_ZW_GET_LAST_WORKING_ROUTE};
    uint8_t abNeedForPowerLevelSupport[] = {FUNC_ID_ZW_RF_POWER_LEVEL_SET,
                                         FUNC_ID_ZW_RF_POWER_LEVEL_GET,
                                         FUNC_ID_ZW_SEND_TEST_FRAME};


    spNetworkManagement = spNetworkMan;


    if (NULL != spNetworkManagement)
    {
        
        struct timeb timebuffer;
        struct tm *timeinfo;
        const char* timestamp = "log_networkHealth_%u%02u%02u-%02u%02u%02u.txt";
        ftime(&timebuffer);
        timeinfo = localtime(&timebuffer.time);
        sprintf(networkHealth_logfilenameStr, timestamp,
                  timeinfo->tm_year+1900, timeinfo->tm_mon+1, timeinfo->tm_mday,
                  timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);

        /* No need to initialize log more than once */
        SLOG_Init(SLOG_STDOUT, networkHealth_logfilenameStr);
        SLOGI("Network Health Maintenance log initialized");
        
        
        spNetworkManagement->psSerialAPIControllerCapabilities = psSerialAPIControllerCapabilities;
        spNetworkManagement->abNodesUnderTest = abListeningNodeList;
        spNetworkManagement->bNodesUnderTestSize = bListeningNodeListSize;
        spNetworkManagement->afIsNodeAFLiRS = afIsNodeAFLiRS;
        spNetworkManagement->bNetworkRepeaterCount = bNetworkRepeaterCount;
        spNetworkManagement->fTestStarted = false;
        spNetworkManagement->fStop = false;
        spNetworkManagement->bTestMode = TEST_IDLE;
        /* Use - 60Min / NumberofNodeInNetwork */
        spNetworkManagement->iMaintenanceSamplePeriod = 0;
        spNetworkManagement->iMaintenanceRoundsBeforeRediscoveryExecuted = MAINTENANCEPRIORREDISCOVERY;
        /* HOST transmit metrics sampling specifics - Only needed if hw do not support transmit latency sampling */
        spNetworkManagement->dTapp = NetworkManagement_CalculateSerialAPIComDelay();

        serialApiMemoryGetID(spNetworkManagement->abHomeID, &spNetworkManagement->bControllerNodeID);

        NetworkManagement_UpdateNeighborInformation();
        
        //If timer is running, kill it!
        if (0 != timerSecH)
        {
            timerCancel(&timerSecH);
            timerSecH = 0;

        }
        timerStart(&timerSecH, SecondTickerTimeout, 0, SECONDTICKERTIMEOUT, TIMER_FOREVER);
        
        spNetworkManagement->fControllerSupportPowerLevelTest = NetworkManagement_IsFuncIDsSupported(abNeedForPowerLevelSupport, sizeof(abNeedForPowerLevelSupport));
        retVal = NetworkManagement_IsFuncIDsSupported(abNeedSerialAPIFunctionality, sizeof(abNeedSerialAPIFunctionality));
    }

    return retVal;
}


/*===================== NetworkManagement_DumpNodeNeighbors ======================
** Function description
**      Dump the registered neighbors for the node specified with an index (i) 
** into the abNodesUnderTest list
**      mode : true: all node, false: only repeater      
**      out = | bCurrentNode | numberOfNeighbor | neighbor[0] | ... | neighbor[numberOfNeighbor-1] |    
**      len = numberOfNeighbor + 2
** Side effects:
**      
**--------------------------------------------------------------------------*/
void NetworkManagement_DumpNodeNeighbors(uint8_t i, uint8_t mode, uint8_t* out, uint8_t* len)
{
    uint8_t abRoutingInfoNodeMask[232/8];
    sNetworkManagement_Stat *spNodeStat;
    uint8_t bCurrentNode;
    char pCharBuf[1024];
    int j;
    int n;
    char cch = ',';

    bCurrentNode = spNetworkManagement->abNodesUnderTest[i];
    spNodeStat = &spNetworkManagement->nodeUT[bCurrentNode - 1];
    if (out) out[0] = bCurrentNode;


    /* First dump all neighbors */
    serialApiGetRoutingInfo(bCurrentNode, abRoutingInfoNodeMask, false, false);
    j = 0;
    memset(pCharBuf, 0, sizeof(pCharBuf));
    snprintf(pCharBuf, sizeof(pCharBuf), "[");

    for (n = 1; n <= ZW_MAX_NODES; n++)
    {
        if (AreNodeANeighbor(n, abRoutingInfoNodeMask))
        {
            if (j++ != 0)
            {
                snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "%c", cch);
            }
            snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "%u", n);

            /* get output*/
            if (mode && out && len) out[1 + j] = n;
        }
    }

    if (mode && out && len) 
    { 
        out[1] = j;
        *len = j +2;
    }

    snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf) - strlen(pCharBuf), "]");
    mainlog(logUI,"Node %03u - 'All' %u NBs %s", bCurrentNode, j, pCharBuf);


    /* Now dump repeaters only */
    serialApiGetRoutingInfo(bCurrentNode, abRoutingInfoNodeMask, false, true);
    j = 0;
    memset(pCharBuf, 0, sizeof(pCharBuf));
    snprintf(pCharBuf, sizeof(pCharBuf), "[");

    for (n = 1; n <= ZW_MAX_NODES; n++)
    {
        if (AreNodeANeighbor(n, abRoutingInfoNodeMask))
        {
            if (j++ != 0)
            {
                snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "%c", cch);
            }
            snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "%u", n);

            /* get output*/
            if (!mode && out && len) out[1 + j] = n;
        }
    }

    if (!mode && out && len) 
    { 
        out[1] = j;
        *len = j +2;
    }

    snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf) - strlen(pCharBuf), "]");
    mainlog(logUI,"Node %03u - 'Repeaters' %u NBs %s", bCurrentNode, j, pCharBuf);


    /* Now dump the registered GW centric "repeater" neighbors for node */
    j = 0;
    memset(pCharBuf, 0, sizeof(pCharBuf));
    snprintf(pCharBuf, sizeof(pCharBuf), "[");
    for (n = 0; n < spNodeStat->neighborCount; n++)
    {
        if (j++ != 0)
        {
            snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "%c", cch);
        }
        snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "%u", 
                  spNodeStat->abNeighbors[n]);
    }
    snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf) - strlen(pCharBuf), "]");
    mainlog(logUI,"Node %03u - 'GW centric' %u NBs %s", bCurrentNode, j, pCharBuf);
}



/*===================== NetworkManagement_MyNodeNeighbors ======================
** Function description
**      Dump the registered neighbors for the node specified with an index (i) 
** into the abNodesUnderTest list
**      mode : true: all node, false: only repeater      
**      out = | bCurrentNode | numberOfNeighbor | neighbor[0] | ... | neighbor[numberOfNeighbor-1] |    
**      len = numberOfNeighbor + 2
** Side effects:
**      
**--------------------------------------------------------------------------*/
void NetworkManagement_DumpMyNodeNeighbors(uint8_t mode, uint8_t* out, uint8_t* len)
{
    uint8_t abRoutingInfoNodeMask[232/8];
    uint8_t bCurrentNode;
    char pCharBuf[1024];
    int j;
    int n;
    char cch = ',';

    bCurrentNode = MyNodeId;
    if (out) out[0] = bCurrentNode;

    /* First dump all neighbors */
    serialApiGetRoutingInfo(bCurrentNode, abRoutingInfoNodeMask, false, false);
    j = 0;
    memset(pCharBuf, 0, sizeof(pCharBuf));
    snprintf(pCharBuf, sizeof(pCharBuf), "[");

    for (n = 1; n <= ZW_MAX_NODES; n++)
    {
        if (AreNodeANeighbor(n, abRoutingInfoNodeMask))
        {
            if (j++ != 0)
            {
                snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "%c", cch);
            }
            snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "%u", n);

            /* get output*/
            if (mode && out && len) out[1 + j] = n;
        }
    }

    if (mode && out && len) 
    { 
        out[1] = j;
        *len = j +2;
    }

    snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf) - strlen(pCharBuf), "]");
    mainlog(logUI,"Node %03u - 'All' %u NBs %s", bCurrentNode, j, pCharBuf);


    /* Now dump repeaters only */
    serialApiGetRoutingInfo(bCurrentNode, abRoutingInfoNodeMask, false, true);
    j = 0;
    memset(pCharBuf, 0, sizeof(pCharBuf));
    snprintf(pCharBuf, sizeof(pCharBuf), "[");

    for (n = 1; n <= ZW_MAX_NODES; n++)
    {
        if (AreNodeANeighbor(n, abRoutingInfoNodeMask))
        {
            if (j++ != 0)
            {
                snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "%c", cch);
            }
            snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "%u", n);

            /* get output*/
            if (!mode && out && len) out[1 + j] = n;
        }
    }

    if (!mode && out && len) 
    { 
        out[1] = j;
        *len = j +2;
    }

    snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf) - strlen(pCharBuf), "]");
    mainlog(logUI,"Node %03u - 'Repeaters' %u NBs %s", bCurrentNode, j, pCharBuf);
}


/*===================== NetworkManagement_DumpNeighbors ======================
** Function description
**      Dump the registered neighbors for the nodes in the network
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
int NetworkManagement_DumpNeighbors(uint8_t mode, uint8_t* out, uint8_t* noNode)
{
    int i; 
    int j = 0;
    uint8_t len;
    if (NULL != spNetworkManagement)
    {
        NetworkManagement_DumpMyNodeNeighbors(mode, (uint8_t*)&out[j], &len);
        j = j + len;

        if (noNode) *noNode = spNetworkManagement->bNodesUnderTestSize + 1;

        if (0 < spNetworkManagement->bNodesUnderTestSize)
        {
            for (i = 0; i < spNetworkManagement->bNodesUnderTestSize; i++)
            {
                NetworkManagement_DumpNodeNeighbors(i, mode, (uint8_t*)&out[j], &len);
                j = j + len;
            }
            return 0;
        }
        else
        {
            mainlog(logUI,"No nodes to dump neighbors on");
            return 0;
        }
    }
    else
    {
        mainlog(logUI,"NetworkManagement_Init has not been called");
    }

    return -1;
}

/*========================= logPowerlevelTestResult ==========================
** Function description
**      Log in mainlog the result of specified Powerlevel Test result
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void 
logPowerlevelTestResult(
    POWLEV_STATUS powStatus)
{
    char* pStatus;

    pStatus = powerStatus2String(powStatus.Status);
    mainlog(logDebug, "PowerLevel %03d-%03d, POW %u, tx %02u[%02u], %s", 
                powStatus.sourceNode,
                powStatus.destNode,
                powStatus.PowerLevel,
                powStatus.ackCountSuccess, 
                powStatus.NbrTestFrames,
                pStatus);
}



/*==================== NetworkManagement_NH_PowerLevelTest_Completed =================
** Function description
**      Callback staus function for NetworkManagement_NH_PowerLevelTest(...)
**      
** Side effects:
**
**--------------------------------------------------------------------------*/
void
NetworkManagement_NH_PowerLevelTest_Completed(
    POWLEV_STATUS powStatus)
{
    uint8_t firstNode = 0;
    uint8_t secondNode = 0;

    if ((NULL != spNetworkManagement) && (NULL != nodeStats))
    {
        if (MAX_ROUTE_LINKS > bLWRRouteIndex)
        {
            memcpy(&nodeStats->powStatus[bLWRRouteIndex], &powStatus, sizeof(POWLEV_STATUS));
        }

        logPowerlevelTestResult(powStatus);

        if (powStatus.Status == POWERLEV_TEST_NODE_REPORT_ZW_TEST_SUCCES)
        {
            if (MAX_ROUTE_LINKS > bLWRRouteIndex)
            {
                nodeStats->bPowerTestSuccess[bLWRRouteIndex] = powStatus.ackCountSuccess;
            }
            if (4 > bLWRRouteIndex)
            {
                if (0 < pabLWRRoute[bLWRRouteIndex])
                {
                    if (4 > ++bLWRRouteIndex)
                    {
                        if (0 < pabLWRRoute[bLWRRouteIndex])
                        {
                            if (pafLWRNodesSupportPowerLevel[bLWRRouteIndex])
                            {
                                /* Previous Repeater - Source of Powerlevel Test  */
                                firstNode = pabLWRRoute[bLWRRouteIndex - 1];
                                /* Current Repeater - Destination of Powerlevel Test */
                                secondNode = pabLWRRoute[bLWRRouteIndex];
                            }
                            else
                            {
                                /* Current Repeater - Source of Powerlevel Test */
                                firstNode = pabLWRRoute[bLWRRouteIndex];
                                /* Previous Repeater - Destination of Powerlevel Test  */
                                secondNode = pabLWRRoute[bLWRRouteIndex - 1];
                            }
                        }
                    }
                    if (0 == firstNode)
                    {
                        if (pafLWRNodesSupportPowerLevel[bLWRRouteIndex + 1])
                        {
                            /* Destination - Source of Powerlevel Test */
                            firstNode = nodeStats->bDestNodeID;
                            /* Last Repeater */
                            secondNode = pabLWRRoute[bLWRRouteIndex - 1];
                        }
                        else
                        {
                            /* Last Repeater - Source of Powerlevel Test */
                            firstNode = pabLWRRoute[bLWRRouteIndex - 1];
                            /* Destination */
                            secondNode = nodeStats->bDestNodeID;
                        }
                    }
                }
            }
        }
        if (0 < firstNode)
        {
            PowerLevelTest_Init(spNetworkManagement->bControllerNodeID, firstNode, secondNode, POWERLEVELTEST_LEVEL, POWERLEVELTEST_COUNT, false);
            PowerLevelTest_Set(&NetworkManagement_NH_PowerLevelTest_Completed);
        }
        else
        {
            int i = 0;
            bool fPowerLevelTestSuccess = true;
            while ((i < MAX_ROUTE_LINKS) && ((i == 0) || (0 < pabLWRRoute[i - 1])))
            {
                if (nodeStats->bPowerTestSuccess[i] != powStatus.NbrTestFrames)
                {
                    fPowerLevelTestSuccess = false;
                    break;
                }
                i++;
            }
            /* If PowerLevel success then NetworkHealthNumber staýs at the current level */
            if (!fPowerLevelTestSuccess)
            {
                /* Failed PowerLevel Test - Degrade */
                if (9 <= nodeStats->NetworkHealthNumber)
                {
                    /* Was a 9 or 10 and due to failed PowerLeveltest NH is degraded to 7 */
                    nodeStats->NetworkHealthNumber = 7;
                }
                else if (8 == nodeStats->NetworkHealthNumber)
                {
                    /* Was an 8 and due to failed PowerLeveltest NH is degraded to 6 */
                    nodeStats->NetworkHealthNumber = 6;
                }
                NetworkManagement_UpdateTrafficLight(nodeStats);
            }
            /* RSSI test is only necessary for NH values of 8-10 */
            if (nodeStats->NetworkHealthNumber >= 8)
            {
                PerformRSSITest();
            }
            else
            {
                NextNetworkHealthTestRound();
            }

        }
    }
}





/*================== NetworkManagement_NH_PowerLevelTest =====================
** Function description
**      If all nodes in LWR supports the COMMAND_CLASS_POWER_LEVEL then all
** links are tested for link margin with the Powerlevel Command Class
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
NetworkManagement_NH_PowerLevelTest(
    uint8_t* spRoute)
{
    if ((NULL != spNetworkManagement) && (NULL != nodeStats) && (NULL != spRoute))
    {
        /* */
        pabLWRRoute = spRoute;
        bLWRRouteIndex = 0;
        PowerLevelTest_CPowerLevelTest(true);
        /* Do more than Source and Destination exist in LWR */
        if (2 < iLWRNumberOfNodesInLink)
        {
            /* Repeaters in LWR */
            /* ControllerNodeID/GW to first Repeater Powerlevel Test */
            if (pafLWRNodesSupportPowerLevel[0])
            {
                /* Controller supports PowerLevel -> use Controller as Source in the PowerLevel Test */
                PowerLevelTest_Init(spNetworkManagement->bControllerNodeID, spNetworkManagement->bControllerNodeID, pabLWRRoute[bLWRRouteIndex], POWERLEVELTEST_LEVEL, POWERLEVELTEST_COUNT, false);
            }
            else
            {
                /* Controller do not support PowerLevel -> use first Repeater as Source in the PowerLevel Test */
                PowerLevelTest_Init(spNetworkManagement->bControllerNodeID, pabLWRRoute[bLWRRouteIndex], spNetworkManagement->bControllerNodeID, POWERLEVELTEST_LEVEL, POWERLEVELTEST_COUNT, false);
            }
        }
        else
        {
            /* Direct route */
            if (pafLWRNodesSupportPowerLevel[0] && !spNetworkManagement->afIsNodeAFLiRS[nodeStats->bDestNodeID - 1])
            {
                /* Controller supports PowerLevel -> use Controller as Source in the PowerLevel Test */
                PowerLevelTest_Init(spNetworkManagement->bControllerNodeID, spNetworkManagement->bControllerNodeID, nodeStats->bDestNodeID, POWERLEVELTEST_LEVEL, POWERLEVELTEST_COUNT, false);
            }
            else
            {
                /* Controller do not support PowerLevel -> use Destination as Source in the PowerLevel Test */
                PowerLevelTest_Init(spNetworkManagement->bControllerNodeID, nodeStats->bDestNodeID, spNetworkManagement->bControllerNodeID, POWERLEVELTEST_LEVEL, POWERLEVELTEST_COUNT, false);
            }
        }
        PowerLevelTest_Set(&NetworkManagement_NH_PowerLevelTest_Completed);
    }
}




/*============== NetworkManagement_Timed_ZW_SendData_Completed ===============
** Function description
**      Callback function for NetworkManagement_Timed_ZW_SendData which is an
** Internal NetworkManagement module wrapper function for Z-Wave API 
** ZW_SendData function call 
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void NetworkManagement_Timed_ZW_SendData_Completed(uint8_t bTxStatus, TX_STATUS_TYPE *psTxInfo)
{

    /* HOST transmit metrics sampling specifics - Only needed if hw do not support transmit latency sampling */
    /* Sample transmit Stop time */
    timingGetClockSystemStop(&sTxTiming);

    if (NULL != spCurrentSample)
    {
        /* Sample time and LWR - Callback function do the following NH calculations according to testMode */
        if (psTxInfo->transmitTicks > 0)
        {
            /* Only use wTime if bigger than ZERO - if ZERO then we assume not supported */
            spCurrentSample->sampleTime = psTxInfo->transmitTicks * 10;
            mainlog(logDebug, "NM ZW_SendData sample0 --  %lf, transmitTicks %i ", spCurrentSample->sampleTime, psTxInfo->transmitTicks);
        }
        else
        {
            spCurrentSample->sampleTime = (double)timingGetElapsedMSec(&sTxTiming);
            psTxInfo->transmitTicks = spCurrentSample->sampleTime / 10;
            mainlog(logDebug, "NM ZW_SendData sample1 %lf", spCurrentSample->sampleTime);
        }
        spCurrentSample->failed = (TRANSMIT_COMPLETE_OK != bTxStatus);
        spCurrentSample->routeFound = ((NULL != nodeStats) && (serialApiGetLastWorkingRoute(nodeStats->bDestNodeID, spCurrentSample->pRoute) == true));
    }
    else
    {
        if (psTxInfo->transmitTicks == 0)
        {
            psTxInfo->transmitTicks = (long)timingGetElapsedMSec(&sTxTiming) / 10;
        }
    }

    /* Call the registered callback function with transmission result */
    if (NULL != cbNetworkManagement_Timed_ZW_SendData)
    {
        cbNetworkManagement_Timed_ZW_SendData(bTxStatus, psTxInfo);
    }
}


/*==================== NetworkManagement_Timed_ZW_SendData ===================
** Function description
**      Internal NetworkManagement module wrapper function for Z-Wave API 
** ZW_SendData function call, which does the actual time metric gathering 
** (if bNodeID eligible for transmit metric gathering)
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
uint8_t NetworkManagement_Timed_ZW_SendData(uint8_t bNodeID,
                                            uint8_t *pData,
                                            uint8_t bDataLength,
                                            void (*completedFunc)(uint8_t, TX_STATUS_TYPE *))
{
    uint8_t retVal;
    cbNetworkManagement_Timed_ZW_SendData = completedFunc;
    retVal = serialApiSendData(bNodeID, pData, bDataLength, testConnectionTxOptions, 
                                        NetworkManagement_Timed_ZW_SendData_Completed);

    if (0 != retVal)
    {
        /* HOST transmit metrics sampling specifics - Only needed if hw do not support transmit latency sampling */
        timingGetClockSystem(&sTxTiming);
    }
    return retVal;
}


/*=================== NetworkManagement_DoRediscoveryStop ====================
** Function description
**      Stop Network Rediscovery if running
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
NetworkManagement_DoRediscoveryStop(
    sNetworkManagement *spNetworkMan)
{
    /* ReDiscovery stopping */
    spNetworkMan->fTestStarted = false;
}


/*===================== NetworkManagement_ReDiscovery_Compl ==================
** Function description
**   Callback function called when ZW_RequestNodeNeighborUpdate (started in
**   NetworkManagement_DoRediscoveryStart) returns
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
NetworkManagement_ReDiscovery_Compl(
  uint8_t bStatus) /*IN Status of neighbor update process*/
{
    if (bStatus != 0x21)
    {
        if (requestUpdateOn && spNetworkManagement->fTestStarted)
        {
            if (bStatus == 0x22)
            {
                /* Node rediscovered */
                spNetworkManagement->abNodesReDiscovered[spNetworkManagement->bNHSCurrentIndex] = true;
                mainlog(logDebug, "Rediscovery Round %u, Node %u Rediscovered", spNetworkManagement->bReDiscoverRound, spNetworkManagement->abNodesUnderTest[spNetworkManagement->bNHSCurrentIndex]);
            }
            else
            {
                /* Node failed rediscovery */
                mainlog(logDebug, "Rediscovery Round %u, Node %u failed Rediscovery", spNetworkManagement->bReDiscoverRound, spNetworkManagement->abNodesUnderTest[spNetworkManagement->bNHSCurrentIndex]);
                /* Now at least 1 failed the rediscovery try again if applicable */
                spNetworkManagement->fAllDiscovered = false;
            }
            NetworkManagement_DumpNodeNeighbors(spNetworkManagement->bNHSCurrentIndex, false, NULL, NULL);
            while (++spNetworkManagement->bNHSCurrentIndex < spNetworkManagement->bNodesUnderTestSize)
            {
                if (!spNetworkManagement->abNodesReDiscovered[spNetworkManagement->bNHSCurrentIndex])
                {
                    /* Found a node which havent been reached yet */
                    break;
                }
            }
            if ((spNetworkManagement->bNHSCurrentIndex >= spNetworkManagement->bNodesUnderTestSize) && !spNetworkManagement->fAllDiscovered)
            {
                /* New rediscovery round */
                if (++spNetworkManagement->bReDiscoverRound < MAX_REDISCOVER_ROUNDS)
                {
                    for (spNetworkManagement->bNHSCurrentIndex = 0; spNetworkManagement->bNHSCurrentIndex < spNetworkManagement->bNodesUnderTestSize; spNetworkManagement->bNHSCurrentIndex++)
                    {
                        if (!spNetworkManagement->abNodesReDiscovered[spNetworkManagement->bNHSCurrentIndex])
                        {
                            /* Assume Success */
                            spNetworkManagement->fAllDiscovered = true;
                            /* Found a node which havent been reached yet */
                            break;
                        }
                    }
                }
            }
            if (spNetworkManagement->bNHSCurrentIndex >= spNetworkManagement->bNodesUnderTestSize)
            {
                spNetworkManagement->bNHSCurrentIndex = 0;
                spNetworkManagement->fTestStarted = false;
                requestUpdateOn = false;
                NetworkManagement_UpdateNeighborInformation();
                /* Done */
                if (spNetworkManagement->fAllDiscovered)
                {
                    mainlog(logDebug, "NetworkManagement ReDiscovery Done - SUCCESS");
                    if (spNetworkManagement->CurrentTestCompleted)
                    {
                        spNetworkManagement->CurrentTestCompleted(1);
                    }

                    NOTIFY_TX_BUFFER_T pTxNotify;
                    pTxNotify.ReDiscoveryNotify.status = REDISCOVERY_DONE_OK;
                    PushNotificationToHandler(RE_DISCOVERY_NOTIFY, (uint8_t *)&pTxNotify.ReDiscoveryNotify, sizeof(RE_DISCOVERY_NOTIFY_T));

                }
                else
                {
                    mainlog(logDebug, "NetworkManagement ReDiscovery Done - FAILED");
                    if (spNetworkManagement->CurrentTestCompleted)
                    {
                        spNetworkManagement->CurrentTestCompleted(0);
                    }

                    NOTIFY_TX_BUFFER_T pTxNotify;
                    pTxNotify.ReDiscoveryNotify.status = REDISCOVERY_DONE_FAIL;
                    PushNotificationToHandler(RE_DISCOVERY_NOTIFY, (uint8_t *)&pTxNotify.ReDiscoveryNotify, sizeof(RE_DISCOVERY_NOTIFY_T));
                }
            }
            else
            {
                bDutNodeID = spNetworkManagement->abNodesUnderTest[spNetworkManagement->bNHSCurrentIndex];
                serialApiRequestNodeNeighborUpdate(bDutNodeID, NetworkManagement_ReDiscovery_Compl);
            }
        }
        else
        {
            if (requestUpdateOn)
            {
                requestUpdateOn = false;
                spNetworkManagement->fTestStarted = false;
                NetworkManagement_UpdateNeighborInformation();
                mainlog(logDebug, "NetworkManagement ReDiscovery Stopping");
                if (spNetworkManagement->CurrentTestCompleted)
                {
                    spNetworkManagement->CurrentTestCompleted(0);
                }

                NOTIFY_TX_BUFFER_T pTxNotify;
                pTxNotify.ReDiscoveryNotify.status = REDISCOVERY_STOP;
                PushNotificationToHandler(RE_DISCOVERY_NOTIFY, (uint8_t *)&pTxNotify.ReDiscoveryNotify, sizeof(RE_DISCOVERY_NOTIFY_T));
            }
            else
            {
                mainlog(logDebug, "NetworkManagement ReDiscovery not running");
                NOTIFY_TX_BUFFER_T pTxNotify;
                pTxNotify.ReDiscoveryNotify.status = REDISCOVERY_NOT_RUNNING;
                PushNotificationToHandler(RE_DISCOVERY_NOTIFY, (uint8_t *)&pTxNotify.ReDiscoveryNotify, sizeof(RE_DISCOVERY_NOTIFY_T));
            }
        }
    }
    else
    {
        mainlog(logDebug, "Rediscovery Round %u, Node %u (%02X)", spNetworkManagement->bReDiscoverRound, bDutNodeID, bStatus);
    }
}




/*================== NetworkManagement_NH_TestConnectionComplete ================
** Function description
**      Callback called when the NetworkManagement_NH_TestConnection has been executed
** and called by the NetworkManagement Network Health Test functionality if specified.
**      
** Side effects:
**      
**-----------------------------------------------------------------------------*/
void
NetworkManagement_NH_TestConnectionComplete(
    uint8_t bTxStatus, 
    TX_STATUS_TYPE *psTxInfo)
{
    mainlog(logDebug, "NetworkManagement_NH_TestConnectionComplete -->");
    if (spNetworkManagement->bTestMode == TEST_IDLE) 
    {
        mainlog(logDebug, "------- STOP -------");
        postEventOperationPollingDev(EV_USER_NETWORK_TEST_DONE);
        return;
    }

    if (NULL != nodeStats)
    {
        NetworkManagement_NH_Calculate_MinMaxAverage(nodeStats, nodeStats->testCount);
        NetworkManagement_NH_Calculate_RouteChange(nodeStats, nodeStats->testCount, true);

        if (nodeStats->testCount < TESTCOUNT)
        {
            nodeStats->testCount++;
        }
        if (nodeStats->testCount < TESTCOUNT)
        {
            spCurrentSample = &nodeStats->samples[nodeStats->testCount];
            NetworkManagement_NH_TestConnection((uint8_t)nodeStats->bDestNodeID, NetworkManagement_NH_TestConnectionComplete);
        }
        else
        {
            bool doLWRNodesSupportPowerLevel;
            int n;
            int bNumberOfRepeatersFound = 0;

            NetworkManagement_NH_Calculate_StdDev(nodeStats);
            /* Calculate traffic light indication */
            NetworkManagement_UpdateNetworkHealthIndicators(nodeStats);
            /* Do Source/Controller Support POWERLEVEL COMMAND CLASS */
            pafLWRNodesSupportPowerLevel[0] = spNetworkManagement->fControllerSupportPowerLevelTest;
            /* Max 4 repeaters - ZERO based */
            n = 4 - 1;
            /* Find last repeater in LWR */
            /* We use the last sampled Last Working Route */
            while ((n >= 0) && ((0 == nodeStats->samples[TESTCOUNT - 1].pRoute[n])))
            {
                /* No repeater */
                n--;
            }
            if (n >= 0)
            {
                /* LWR repeaters defined */
                bNumberOfRepeatersFound = n + 1;
                /* Repeater found */
                for (; n >= 0; n--)
                {
                    pafLWRNodesSupportPowerLevel[n + 1] = IsCommandClassSupported(nodeStats->samples[TESTCOUNT - 1].pRoute[n], COMMAND_CLASS_POWERLEVEL);
                }
            }
            pafLWRNodesSupportPowerLevel[bNumberOfRepeatersFound + 1] = IsCommandClassSupported(nodeStats->bDestNodeID, COMMAND_CLASS_POWERLEVEL);
            for (n = 0; n < bNumberOfRepeatersFound + 1; n++)
            {
                /* Do any of the nodes in Link support PowerLevel Command Class */
                doLWRNodesSupportPowerLevel = pafLWRNodesSupportPowerLevel[n] || pafLWRNodesSupportPowerLevel[n + 1];
                if (!doLWRNodesSupportPowerLevel)
                {
                    /* Found a link with no PowerLevel Command Class support - No need for testing anymore */
                    break;
                }
            }
            
            if (doLWRNodesSupportPowerLevel)
            {
                /* Make sure if a FLiRS is under test then it is the FLiRS which must execute the */
                /* PowerLevel test on link in question */
                if (true == spNetworkManagement->afIsNodeAFLiRS[nodeStats->bDestNodeID - 1])
                {
                    doLWRNodesSupportPowerLevel = pafLWRNodesSupportPowerLevel[bNumberOfRepeatersFound + 1];
                }
            }
            nodeStats->NetworkHealthNumberPriorToPowerLevelTest = nodeStats->NetworkHealthNumber;
            nodeStats->aNetworkHealthNumberPriorToPowerLevelTest[nodeStats->testRounds] = nodeStats->NetworkHealthNumber;
            /* Check if NH = 8, 9 or 10 and if so do a PowerLevel test on all links in the LWR for DUT */
            if (doLWRNodesSupportPowerLevel && (nodeStats->NetworkHealthNumber >= 8))
            {
                /* Number of nodes in LWR including source and destination */
                iLWRNumberOfNodesInLink = bNumberOfRepeatersFound + 1 + 1;
                NetworkManagement_NH_PowerLevelTest(nodeStats->samples[TESTCOUNT - 1].pRoute);
            }
            else
            {
                if (!doLWRNodesSupportPowerLevel)
                {
                    mainlog(logDebug, "PowerLevel Test not executed - One node in every link");
                    mainlog(logDebug, " in LWR must support PowerLevel Command Class");
                }
                NextNetworkHealthTestRound();
            }
        }
    }
}

/*====================== StartNextSingleNodeRediscovery ======================
** Function description
**      Find Next Node which is marked as needing a Rediscovery and start
** the rediscovery.
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
bool
StartNextSingleNodeRediscovery()
{
    bool retVal = false;
    mainlog(logDebug, "StartNextSingleNodeRediscovery -->");

    if (NULL != spNetworkManagement)
    {
        while (spNetworkManagement->bMaintenanceNHSCurrentIndex < spNetworkManagement->bNodesUnderTestSize)
        {
            if (spNetworkManagement->afMaintenanceNeedsRediscovery[spNetworkManagement->abNodesUnderTest[spNetworkManagement->bMaintenanceNHSCurrentIndex] - 1])
            {
                /* Do we need a timer... */
                retVal = NetworkManagement_DoRediscoverySingle(&spNetworkManagement->nodeUT[spNetworkManagement->abNodesUnderTest[spNetworkManagement->bMaintenanceNHSCurrentIndex] - 1], SingleNodeRediscovery_Done);
                mainlog(logDebug, "Node %03u needs to be Rediscovered - %s", spNetworkManagement->abNodesUnderTest[spNetworkManagement->bMaintenanceNHSCurrentIndex], retVal ? "Initiated" : "Failed");
                return retVal;
            }
            spNetworkManagement->bMaintenanceNHSCurrentIndex++;
        }
        if (spNetworkManagement->bMaintenanceNHSCurrentIndex >= spNetworkManagement->bNodesUnderTestSize)
        {
            /* Rediscovery done */
            spNetworkManagement->bMaintenanceNHSCurrentIndex = 0;
            spNetworkManagement->fMaintenanceSingleDiscoveryInitiated = false;
        }
    }
    return retVal;
}





/*===================== NetworkManagement_NH_TestConnection =====================
** Function description
**      Function used to do the actual communication test
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
uint8_t NetworkManagement_NH_TestConnection(uint8_t bNodeID,
                                                void (*CompletedFunc)(uint8_t bTxStatus, TX_STATUS_TYPE *psTxInfo))
{
    return NetworkManagement_Timed_ZW_SendData(bNodeID, testConnectionPayload, sizeof(testConnectionPayload), CompletedFunc);
}


/*======================== NetworkManagement_NH_Start =========================
** Function description
**      Determines the Z-Wave Network Health for the node described in
** the structure pointed to by spNodeUT
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
bool 
NetworkManagement_NH_Start(
    sNetworkManagement_Stat *spNodeUT)
{
    bool retVal = false;

    nodeStats = spNodeUT;
    if ((NULL != spNetworkManagement) && (NULL != nodeStats))
    {
        if ((nodeStats->bDestNodeID > 0) || (nodeStats->bDestNodeID <= ZW_MAX_NODES))
        {
            if (spNetworkManagement->testNeeded <= TESTCOUNT)
            {
                nodeStats->testRounds = 0;
                InitializeTransmitMetrics();
                spCurrentSample = &nodeStats->samples[nodeStats->testCount];
                NetworkManagement_NH_TestConnection((uint8_t)nodeStats->bDestNodeID, NetworkManagement_NH_TestConnectionComplete);
                retVal = true;
            }
            else
            {
                mainlog(logDebug,"Testcount not valid. Min 1, Max %u.\n", TESTCOUNT);
            }
        }
        else
        {
            mainlog(logDebug, "NodeID not valid must be between 1 - %u, parameter specified %u.\n", ZW_MAX_NODES, nodeStats->bDestNodeID);
        }
    }
    else
    {
        mainlog(logDebug, "Network Health not initialized.\n");
    }
    return retVal;
}



/*========================== NextNetworkHealthTestRound =========================
** Function description
**      Executes the Network Health Test on every node TESTROUNDS times and 
** calculates the resulting NH as the average of the NH results from the 
** TESTROUNDS Network Health Test runs (10 NOPs + [10 Powerlevel Tests])
**
** Side effects:
**      
**-----------------------------------------------------------------------------*/
void
NextNetworkHealthTestRound()
{
    int i;
    if (NULL != nodeStats)
    {
        nodeStats->aNetworkHealthNumber[nodeStats->testRounds] = nodeStats->NetworkHealthNumber;
        nodeStats->testRounds++;
        if (TESTROUNDS > nodeStats->testRounds)
        {
            InitializeTransmitMetrics();
            spCurrentSample = &nodeStats->samples[nodeStats->testCount];
            NetworkManagement_NH_TestConnection((uint8_t)nodeStats->bDestNodeID, NetworkManagement_NH_TestConnectionComplete);
        }
        else
        {
            nodeStats->NetworkHealthNumber = 0;
            nodeStats->NetworkHealthNumberPriorToPowerLevelTest = 0;
            for (i = 0; i < TESTROUNDS; i++)
            {
                nodeStats->NetworkHealthNumber += nodeStats->aNetworkHealthNumber[i];
                nodeStats->NetworkHealthNumberPriorToPowerLevelTest += nodeStats->aNetworkHealthNumberPriorToPowerLevelTest[i];
            }
            nodeStats->NetworkHealthNumber /= TESTROUNDS;
            nodeStats->NetworkHealthNumberPriorToPowerLevelTest /= TESTROUNDS;
            NetworkManagement_UpdateTrafficLight(nodeStats);
            NextNodeNetworkHealthTest();
        }
    }
}





/*======================= NetworkManagement_ZW_SendData ======================
** Function description
**      NetworkManagement API wrapper function for Z-Wave API ZW_SendData 
** function. Besides calling ZW_SendData and transmitting to the specified 
** bNodeID it also uses the transmission for transmit metric gathering 
** (if applicable), which then are used to determine the NH for destination 
** bNodeID
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
uint8_t
NetworkManagement_ZW_SendData(
    uint8_t bNodeID,
    uint8_t *pData,
    uint8_t bDataLength,
    void (*completedFunc)(uint8_t, TX_STATUS_TYPE *psTxInfo))
{
    bool retVal;
    bool nodeMetricFound = false;
    int i;

    if (bNodeID > 0)
    {
        /* Make sure Network Management Network Health has been initialized and test is not allready inprogress */
        if ((NULL != spNetworkManagement) && (0 < spNetworkManagement->bNodesUnderTestSize) && (NULL == nodeStats))
        {
            for (i = 0; i < spNetworkManagement->bNodesUnderTestSize; i++)
            {
                if (bNodeID == spNetworkManagement->abNodesUnderTest[i])
                {
                    /* Set nodeStat to point to node under test transmit metric structure */
                    nodeStats = &spNetworkManagement->nodeUT[bNodeID - 1];
                    nodeStats->bDestNodeID = bNodeID;
                    /* Set spCurrentSample to point to sample slot to place new sample in - always last sample slot */
                    spCurrentSample = &spNetworkManagement->nodeUT[bNodeID - 1].samples[TESTCOUNT - 1];
                    nodeMetricFound = true;
                    break;
                }
            }
        }
        cbNetworkManagement_ZW_SendData = completedFunc;
        retVal = (0 != NetworkManagement_Timed_ZW_SendData(bNodeID, pData, bDataLength, NetworkManagement_ZW_SendData_Completed));
        /* Only update transmit metric if transmit could be initiated and node metric to update has been found */
        if (retVal && nodeMetricFound)
        {
            if (!nodeStats->samples[0].failed)
            {
                nodeStats->routeCount--;
                if (nodeStats->samples[1].routeUniq)
                {
                    nodeStats->routeCount--;
                }
            }
            /* Make room for new sample - move/copy last (TESTCOUNT - 1) samples one sample slot in sample array */
            memcpy(&nodeStats->samples[0], &nodeStats->samples[1], 
                   sizeof(nodeStats->samples) - sizeof(sSample));
            /* First LWR always uniq if not failed */
            if (!nodeStats->samples[0].failed)
            {
                nodeStats->samples[0].routeUniq = true;
                nodeStats->routeCount++;
            }
        }
    }
    return retVal;
}



/*==================== NetworkManagement_NH_TestComplete =====================
** Function description
**      Callback function called when a NH test has been executed for a node.
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
NetworkManagement_NH_TestComplete(
    uint8_t bStatus)
{
    mainlog(logDebug, "spNetworkManagement->fTestStarted ---------------- %d", spNetworkManagement->fTestStarted);
    if (NULL != spNetworkManagement)
    {
        if (spNetworkManagement->fTestStarted && (FULL == spNetworkManagement->bTestMode) &&
            (spNetworkManagement->bNHSCurrentIndex < spNetworkManagement->bNodesUnderTestSize))
        {
            StartNextNetworkHealthTest();
        }
        else
        {
            NetworkManagement_DumpNetworkHealth();
            spNetworkManagement->bNHSCurrentIndex = 0;
            spNetworkManagement->fTestStarted = false;
            nodeStats = NULL;
            mainlog(logDebug, "Done with Z-Wave Network Health");
            if (spNetworkManagement->CurrentTestCompleted)
            {
                spNetworkManagement->CurrentTestCompleted(1);
            }
        }
    }
}


/*================== NetworkManagement_DumpNetworkHealth =====================
** Function description
**      Dump Network Health result in new Network Health filelog
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
NetworkManagement_DumpNetworkHealth()
{
    char pFileName[256];
    const char* timestamp = "Z-Wave_Network_Health_%u%02u%02u-%02u%02u%02u.txt";
    struct timeb timebuffer;
    struct tm *timeinfo;
    ftime(&timebuffer);
    timeinfo = localtime(&timebuffer.time);
    int m, n, j;
    //NOTIFY_TX_BUFFER_T pTxNotify;

    if (NULL != spNetworkManagement)
    {
        sprintf(pFileName, timestamp,
                  timeinfo->tm_year+1900, timeinfo->tm_mon+1, timeinfo->tm_mday,
                  timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);

        fp_statLog = fopen(pFileName, "w");

        for (m = 0; m < spNetworkManagement->bNodesUnderTestSize; m++)
        {
            if ((FULL == spNetworkManagement->bTestMode) || (spNetworkManagement->bNHSCurrentIndex == m))
            {
                uint8_t i = spNetworkManagement->abNodesUnderTest[m] - 1;

                fprintf(fp_statLog, "\nZ-Wave Network Health for Node %03u\n", spNetworkManagement->nodeUT[i].bDestNodeID);
                /* TO#3934 fix */
                fprintf(fp_statLog, "Neighbor count %03u (%3.1f percent) out of %03u\n", 
                        spNetworkManagement->nodeUT[i].neighborCount, 
                        (spNetworkManagement->bNetworkRepeaterCount != 0) ? 
                                ((double) spNetworkManagement->nodeUT[i].neighborCount / (double) (spNetworkManagement->bNetworkRepeaterCount + (spNetworkManagement->nodeUT[i].bControllerIsANeighborAndNotARepeater ? 1 : 0))) * 100 : 
                                //or
                                (double) 0, 
                        spNetworkManagement->bNetworkRepeaterCount + (spNetworkManagement->nodeUT[i].bControllerIsANeighborAndNotARepeater ? 1 : 0));


                char pCharBuf[1024];
                memset(pCharBuf, 0, sizeof(pCharBuf));
                if (spNetworkManagement->nodeUT[i].neighborCount > 0)
                {
                    snprintf(pCharBuf, sizeof(pCharBuf), "Neighbors ");
                    snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "[");
                    char cch = ',';
                    for (n = 0; n < spNetworkManagement->nodeUT[i].neighborCount; n++)
                    {
                        if (n != 0)
                        {
                            snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "%c", cch);
                        }
                        snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf)-strlen(pCharBuf), "%u",
                                  spNetworkManagement->nodeUT[i].abNeighbors[n]);
                    }
                    snprintf(&pCharBuf[strlen(pCharBuf)], sizeof(pCharBuf) - strlen(pCharBuf), "]");
                    fprintf(fp_statLog, "%s\n", pCharBuf);
                }
                else
                {
                    fprintf(fp_statLog, "NO NEIGHBORS\n");
                }
                for (j = 0; j < spNetworkManagement->nodeUT[i].testCount; j++)
                {
                    fprintf(fp_statLog, "%lf %s ", spNetworkManagement->nodeUT[i].samples[j].sampleTime,
                              spNetworkManagement->nodeUT[i].samples[j].failed ? "#failed" : "");
                    fprintf(fp_statLog, "LWR %s [%02X, %02X, %02X, %02X at %s] ", 
                              (spNetworkManagement->nodeUT[i].samples[j].routeFound == 0) ? "NONE" : "FOUND", 
                              spNetworkManagement->nodeUT[i].samples[j].pRoute[ROUTECACHE_LINE_REPEATER_1_INDEX], 
                              spNetworkManagement->nodeUT[i].samples[j].pRoute[ROUTECACHE_LINE_REPEATER_2_INDEX], 
                              spNetworkManagement->nodeUT[i].samples[j].pRoute[ROUTECACHE_LINE_REPEATER_4_INDEX], 
                              spNetworkManagement->nodeUT[i].samples[j].pRoute[ROUTECACHE_LINE_REPEATER_4_INDEX],
                              (spNetworkManagement->nodeUT[i].samples[j].pRoute[ROUTECACHE_LINE_CONF_INDEX] == ZW_LAST_WORKING_ROUTE_SPEED_100K) ? "100k" :
                               (spNetworkManagement->nodeUT[i].samples[j].pRoute[ROUTECACHE_LINE_CONF_INDEX] == ZW_LAST_WORKING_ROUTE_SPEED_40K) ? "40k" :
                                (spNetworkManagement->nodeUT[i].samples[j].pRoute[ROUTECACHE_LINE_CONF_INDEX] == ZW_LAST_WORKING_ROUTE_SPEED_9600) ? "9.6k" : "Auto");
                    fprintf(fp_statLog, "Route quality - %s, uniq LWR %s\n",
                                          spNetworkManagement->nodeUT[i].samples[j].routeChanged ? "Change" : "Stable",
                                          spNetworkManagement->nodeUT[i].samples[j].routeUniq ? "TRUE" : "FALSE");
                }
                for (j = 0; j < 5; j++)
                {
                    if (spNetworkManagement->nodeUT[i].powStatus[j].sourceNode != 0)
                    {
                        char* pStatus = "";
                        POWLEV_STATUS powStatus = spNetworkManagement->nodeUT[i].powStatus[j];

                        pStatus = powerStatus2String(powStatus.Status);
                        fprintf(fp_statLog, "PowerLevel %03d-%03d, POW %u, tx %02u[%02u], %s\n", 
                                  powStatus.sourceNode,
                                  powStatus.destNode,
                                  powStatus.PowerLevel,
                                  powStatus.ackCountSuccess, 
                                  powStatus.NbrTestFrames,
                                  pStatus);
                    }
                }
                fprintf(fp_statLog, "%u tries to reach DUT %u times [PER %u] [RC %u] Uniq LWR %u\n", 
                          spNetworkManagement->nodeUT[i].testCount,
                          spNetworkManagement->nodeUT[i].testCount - spNetworkManagement->nodeUT[i].testTXErrors, 
                          spNetworkManagement->nodeUT[i].testTXErrors,
                          spNetworkManagement->nodeUT[i].routeChange, spNetworkManagement->nodeUT[i].routeCount);
                fprintf(fp_statLog, "Average %.0f ms\n", spNetworkManagement->nodeUT[i].average);
                fprintf(fp_statLog, "Minimum transmit time           %li ms\n", spNetworkManagement->nodeUT[i].min);
                fprintf(fp_statLog, "Maximum transmit time           %li ms\n", spNetworkManagement->nodeUT[i].max);
                fprintf(fp_statLog, "NH  = %02i [%02i(%02i),%02i(%02i),%02i(%02i),%02i(%02i),%02i(%02i),%02i(%02i)]\n", 
                          spNetworkManagement->nodeUT[i].NetworkHealthNumber,
                          spNetworkManagement->nodeUT[i].aNetworkHealthNumber[0], spNetworkManagement->nodeUT[i].aNetworkHealthNumberPriorToPowerLevelTest[0],
                          spNetworkManagement->nodeUT[i].aNetworkHealthNumber[1], spNetworkManagement->nodeUT[i].aNetworkHealthNumberPriorToPowerLevelTest[1],
                          spNetworkManagement->nodeUT[i].aNetworkHealthNumber[2], spNetworkManagement->nodeUT[i].aNetworkHealthNumberPriorToPowerLevelTest[2],
                          spNetworkManagement->nodeUT[i].aNetworkHealthNumber[3], spNetworkManagement->nodeUT[i].aNetworkHealthNumberPriorToPowerLevelTest[3],
                          spNetworkManagement->nodeUT[i].aNetworkHealthNumber[4], spNetworkManagement->nodeUT[i].aNetworkHealthNumberPriorToPowerLevelTest[4],
                          spNetworkManagement->nodeUT[i].aNetworkHealthNumber[5], spNetworkManagement->nodeUT[i].aNetworkHealthNumberPriorToPowerLevelTest[5]);
                fprintf(fp_statLog, "NHS = %s\n",
                          spNetworkManagement->nodeUT[i].NetworkHealthSymbol);
            }
        }
        fclose(fp_statLog);
    }
}


/*================= LogCurrentAndInitiateNextNetworkHealthTest ===============
** Function description
**      Logs the Network Health result for current node and initiates next
** Network Health test for next node.
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
NextNodeNetworkHealthTest()
{
    if (NULL != nodeStats)
    {
        LogNodeNetworkHealth(nodeStats, true);
        NetworkManagement_NH_TestComplete(true);
    }
}



/*=================== NetworkManagement_NetworkHealthStart ===================
** Function description
**      Start Network Health if initialized with 
** NetworkManagement_InitNetworkHealth
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
bool
NetworkManagement_NetworkHealth_Start(
    void (*TestCompleted)(uint8_t bStatus))
{
    bool retVal = false;

    if (NULL != spNetworkManagement) 
    {
        if (spNetworkManagement->bNodesUnderTestSize > 0)
        {
            spNetworkManagement->CurrentTestCompleted = TestCompleted;
            spNetworkManagement->bNHSCurrentIndex = 0;
            spNetworkManagement->fTestStarted = false;

            StartNextNetworkHealthTest();
            retVal = true;
        }
        else
        {
            spNetworkManagement->fTestStarted = false;
        }
    }
    else
    {
        mainlog(logDebug, "Network Health not initialized - use NetworkManagement_NetworkHealth_Init");
    }
    return retVal;
}

/*======================= StartNextNetworkHealthTest =========================
** Function description
**      Starts the Network Health test on the node identified by the
** spNetworkManagement->abNodesUnderTest[spNetworkManagement->bNHSCurrentIndex] nodeID
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
bool
StartNextNetworkHealthTest()
{
    bool retVal = false;

    if (NULL != spNetworkManagement)
    {
        switch (spNetworkManagement->bTestMode)
        {
            case FULL:
                {
                    uint8_t bNextNodeID = spNetworkManagement->abNodesUnderTest[spNetworkManagement->bNHSCurrentIndex];
                    if (0 < bNextNodeID)
                    {
                        uint8_t bNextIndex = bNextNodeID - 1;
                        mainlog(logDebug, "Starting Z-Wave Network Health for Node %03u", bNextNodeID);

                        spNetworkManagement->nodeUT[bNextIndex].bDestNodeID = bNextNodeID;

                        /* Number of Test transmits used to determine the Z-Wave Network Health */
                        spNetworkManagement->testNeeded = TESTCOUNT;
                        spNetworkManagement->testRoundsNeeded = TESTROUNDS;

                        /* If a Test Transmit takes more than  TRANSMIT_LATENCY_JITTER_MAX ms */
                        /* then classify the transmit as a "Route Change" */
                        spNetworkManagement->nodeUT[bNextIndex].testRouteDurationMAX = TRANSMIT_LATENCY_JITTER_MAX + spNetworkManagement->dTapp;
                        spNetworkManagement->fTestStarted = (NetworkManagement_NH_Start(&spNetworkManagement->nodeUT[bNextIndex]) && (!spNetworkManagement->fStop));

                        mainlog(logDebug, "spNetworkManagement->fStop = %d", spNetworkManagement->fStop);

                        if (spNetworkManagement->fTestStarted)
                        {
                            spNetworkManagement->bNHSCurrentIndex++;
                        }
                        else
                        {
                            spNetworkManagement->bNHSCurrentIndex = 0;
                            printf("\n");
                            mainlog(logDebug, "Done with Z-Wave Network Health");
                            if (spNetworkManagement->CurrentTestCompleted)
                            {
                                spNetworkManagement->CurrentTestCompleted(1);
                            }
                        }
                    }
                }
                break;

            case SINGLE:
                {
                    for (spNetworkManagement->bNHSCurrentIndex = 0; spNetworkManagement->bNHSCurrentIndex < spNetworkManagement->bNodesUnderTestSize; spNetworkManagement->bNHSCurrentIndex++)
                    {
                        if (spNetworkManagement->abNodesUnderTest[spNetworkManagement->bNHSCurrentIndex] == spNetworkManagement->bCurrentTestNodeID)
                        {
                            break;
                        }
                    }
                    if (spNetworkManagement->bNHSCurrentIndex >= spNetworkManagement->bNodesUnderTestSize)
                    {
                        mainlog(logDebug, "Could not start Z-Wave Network Health for Node %03u - Not a member of the Nodes Under Test", spNetworkManagement->bCurrentTestNodeID);
                    }
                    else
                    {
                        uint8_t bNextIndex = spNetworkManagement->bCurrentTestNodeID - 1;
                        mainlog(logDebug, "Starting Z-Wave Network Health for Node %03u", spNetworkManagement->bCurrentTestNodeID);

                        spNetworkManagement->nodeUT[bNextIndex].bDestNodeID = spNetworkManagement->bCurrentTestNodeID;
                        /* Number of Test transmits used to determine the Z-Wave Network Health for every testround */
                        spNetworkManagement->testNeeded = TESTCOUNT;
                        /* Number of Test Rounds used to determine the Z-Wave Network Health */
                        spNetworkManagement->testRoundsNeeded = TESTROUNDS;

                        /* If a Test Transmit takes more than  TRANSMIT_LATENCY_JITTER_MAX ms */
                        /* then classify the transmit as a "Route Change" */
                        spNetworkManagement->nodeUT[bNextIndex].testRouteDurationMAX = TRANSMIT_LATENCY_JITTER_MAX + spNetworkManagement->dTapp;
                        spNetworkManagement->fTestStarted = (NetworkManagement_NH_Start(&spNetworkManagement->nodeUT[bNextIndex]) && (!spNetworkManagement->fStop));
                    }

                    if (!spNetworkManagement->fTestStarted)
                    {
                        spNetworkManagement->bNHSCurrentIndex = 0;
                        printf("\n");
                        mainlog(logDebug, "Could not start Z-Wave Network Health");
                        if (spNetworkManagement->CurrentTestCompleted)
                        {
                            spNetworkManagement->CurrentTestCompleted(1);
                        }
                    }
                }
                break;

            default:
                {
                    mainlog(logDebug, "Unknown Z-Wave Network Health test mode %i", spNetworkManagement->bTestMode);
                }
                break;
        }
        retVal = spNetworkManagement->fTestStarted;
    }
    return retVal;
}



/*=========================== NetworkHealthComplete ==========================
** Function description
**      Callback function called when Network Health Functionality has stopped.
** Side effects:
**
**--------------------------------------------------------------------------*/
void
NetworkHealthComplete(
    uint8_t bStatus)
{
    testStarted = false;
    mainlog(logDebug, "Network Health stopped - Status %u", bStatus);
    postEventOperationPollingDev(EV_USER_NETWORK_TEST_DONE);

}

/*========================== StartNetworkHealthTest ===========================
** Start a single network health test.
** Side effects:
**
**--------------------------------------------------------------------------*/
bool
StartNetworkHealthTestSingle(void)
{
    if (!testStarted && (0 < bListeningNodeListSize))
    {
        /* Only if a full Network Health have been run */
        testStarted = NetworkHealth(SINGLE);
    }
    else
    {
        testStarted = false;
        if (0 < bListeningNodeListSize)
        {
            NetworkManagement_NetworkHealth_Stop(&sNetworkManagementUT);
            mainlog(logDebug, "Stopping Z-Wave Network Health Test for Node %i", bNodeID);
        }
        else
        {
            mainlog(logDebug, "No Nodes in network eligible for establishing Z-Wave Network Health");
        }
    }
    return testStarted;
}



/*=========================== NetworkHealthComplete ==========================
** Function description
**      Function called starting the FULL or SINGLE Network Health Test
** Side effects:
**
**--------------------------------------------------------------------------*/
bool
NetworkHealthStart()
{
    bool retVal = false;
    /* We got all the nodeinformation so start the real Network Health Test */

    

    switch (sNetworkManagementUT.bTestMode)
    {
        case FULL:
        {
            retVal = NetworkManagement_NetworkHealth_Start(NetworkHealthComplete);
        }
        break;

        case SINGLE:
        {
            sNetworkManagementUT.bCurrentTestNodeID = bNodeID;
            retVal = NetworkManagement_NetworkHealth_Start(NetworkHealthComplete);
        }
        break;

        case MAINTENANCE:
        {
        }
        break;

        default:
            break;
    }
    return retVal;
}


/*=================== NetworkManagement_NetworkHealthStop ====================
** Function description
**      Stop Network Health if running
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
NetworkManagement_NetworkHealth_Stop(
    sNetworkManagement *spNetwork)
{
    /* ReDiscovery stopping */
    spNetwork->fTestStarted = false;
    spNetwork->fStop = true;
}




/*===================== RequestNodeInformation_Completed =====================
** Function description
**      Callback function called when the ZW_RequestNodeInfo call started by
** DoRequestForNodeInformation has been executed
** Side effects:
**
**--------------------------------------------------------------------------*/
void
RequestNodeInformation_Completed(
    uint8_t bTxStatus)
{
    if (!bTxStatus)
    {
        mainlog(logDebug, "Request Node Info transmitted successfully to node %03d", abListeningNodeList[sNetworkManagementUT.bNHSCurrentIndex]);
    }
    else
    {
        mainlog(logDebug, "Request Node Info transmit failed to node %03d", abListeningNodeList[sNetworkManagementUT.bNHSCurrentIndex]);
    }
}


/*============================ CB_PingTestComplete ===========================
** Function description
**      Callback function called when Ping test transmit has been executed.
** Side effects:
**
**--------------------------------------------------------------------------*/
void
CB_PingTestComplete(
    uint8_t bTxStatus, 
    TX_STATUS_TYPE *psTxInfo)
{
    int i;
    mainlog(logDebug, "Ping Done - Node %03u - %s - Latency %ums", abListeningNodeList[bPingNodeIndex], 
                (TRANSMIT_COMPLETE_OK == bTxStatus) ? "SUCCESS" : "FAILED", psTxInfo->transmitTicks * 10);

    if (TRANSMIT_COMPLETE_OK != bTxStatus)
    {
        abPingFailed[abPingFailedSize++] = abListeningNodeList[bPingNodeIndex];
    }
    if (++bPingNodeIndex < bListeningNodeListSize)
    {
        testStarted = NetworkManagement_NH_TestConnection(abListeningNodeList[bPingNodeIndex], CB_PingTestComplete);
        if (testStarted)
        {
            mainlog(logDebug, "Ping      - Node %03u", abListeningNodeList[bPingNodeIndex]);
        }
        else
        {
            mainlog(logDebug, "Ping Node - could not be started - stopping");
        }
    }
    else
    {
        testStarted = false;
    }
    if (!testStarted)
    {
        NOTIFY_TX_BUFFER_T pTxNotify;

        if (0 < abPingFailedSize)
        {
            char bCharBuf[1024];

            snprintf(bCharBuf, sizeof(bCharBuf), "Ping Failed - ");
            for (i = 0; i < abPingFailedSize; i++)
            {
                snprintf(&bCharBuf[strlen(bCharBuf)], sizeof(bCharBuf) - strlen(bCharBuf),"%0X ", abPingFailed[i]);
            }
            mainlog(logDebug ,bCharBuf);

            pTxNotify.PingFailedNodeNotify.no_node = abPingFailedSize;
            memcpy((uint8_t*)&pTxNotify.PingFailedNodeNotify.nodes, abPingFailed, abPingFailedSize);
        
        }
        else
        {   
            pTxNotify.PingFailedNodeNotify.no_node = 0;
            mainlog(logDebug, "Ping Success - All Nodes answered");
        }

        PushNotificationToHandler(PING_FAILED_NODE_NOTIFY, (uint8_t *)&pTxNotify.PingFailedNodeNotify, sizeof(PING_FAILED_NODE_NOTIFY_T));
        mainlog(logDebug, "Ping Node - End");
        testStarted = false;
        postEventOperationPollingDev(EV_USER_NETWORK_TEST_DONE);

    }
}





/*========================== RequestNextNodeInformation ======================
** Function description
**      Callback function called if timeout waiting for Nodeinformation frame
** requested with DoRequestnodeInformation.
** Side effects:
**
**--------------------------------------------------------------------------*/
void
RequestNextNodeInformation(void* H)
{
    bool retVal = false;
    int i;

    for (i = sNetworkManagementUT.bNHSCurrentIndex + 1; i < bListeningNodeListSize; i++)
    {
        /* If first Command Class is NONE ZERO then we do have Command Class information for Node */
        if (0 == sNetworkManagementUT.nodeDescriptor[abListeningNodeList[i] - 1].cmdClasses[0])
        {
            /* We need to request nodeinformation from at least this node */
            retVal = true;
            break;
        }
    }
    /* Are we done with NodeInformation Requests */
    if (true == retVal)
    {
        DoRequestForNodeInformation(i);
    }
    else
    {
        sNetworkManagementUT.bNHSCurrentIndex = 0;
        /* We got all the nodeinformation so start the real Network Health Test */
        testStarted = NetworkHealthStart();
        if (!testStarted)
        {
            mainlog(logDebug, "NetworkHealthStart Failed to start Network Health Test");
        }
    }
}



/* Set Request Nodeinformation timeout to 8 seconds */

/*======================= DoRequestForNodeInformation ========================
** Function description
**      Function to update 
** Side effects:
**
**--------------------------------------------------------------------------*/
void
DoRequestForNodeInformation(
    uint8_t bNextIndex)
{
    sNetworkManagementUT.bNHSCurrentIndex = bNextIndex;
    //If timer is running, kill it!
    if (0 != timerH)
    {
        timerCancel(&timerH);
    }
    timerStart(&timerH, RequestNextNodeInformation, 0, TIMEOUT_NODEINFORMATION, TIMER_ONETIME);

    if (0 == timerH)
    {
        mainlog(logDebug, "DoRequestForNodeInformation no timer! ERROR!");
        /* Go on and test anyway */
        if (!NetworkHealthStart())
        {
            mainlog(logDebug, "Network Health Test could not be started - FAILED");
            /* Could not start NetworkHealth */
            NetworkHealthComplete(0);
        }
    }
    else
    {
        VR_(usleep)(10000);
        if (serialApiRequestNodeInfo(abListeningNodeList[sNetworkManagementUT.bNHSCurrentIndex], RequestNodeInformation_Completed))
        {
            mainlog(logDebug, "ZW_RequestNodeInfo has been initiated for node %u", abListeningNodeList[sNetworkManagementUT.bNHSCurrentIndex]);
        }
        else
        {
            mainlog(logDebug, "ZW_RequestNodeInfo could not be initiated for node %u", abListeningNodeList[sNetworkManagementUT.bNHSCurrentIndex]);
            mainlog(logDebug, "Timeout callback function will initiate and try with next node");
        }
    }
}

/*========================   InitializeSerialAPICapabilities =================
** Function description
**      Initialize sController_SerialAPI_Capabilities with the attached
**  module SerialAPI capabilities using api.SerialAPI_Get_Capabilities()
**      a SUC
**
** Side effects:
**      
**--------------------------------------------------------------------------*/
void
InitializeSerialAPICapabilities(void)
{
    uint8_t abController_SerialAPI_Capabilities[64];

    memset(&abController_SerialAPI_Capabilities, 0, sizeof(abController_SerialAPI_Capabilities));
    memset((uint8_t*)&sController_SerialAPI_Capabilities, 0, sizeof(sController_SerialAPI_Capabilities));
    serialApiGetCapabilities((uint8_t*)&abController_SerialAPI_Capabilities);
    /* If Application Version and Revision both are ZERO then we assume we did not get any useful information */
    if ((0 != abController_SerialAPI_Capabilities[0]) || (0 != abController_SerialAPI_Capabilities[1]))
    {
        memcpy((uint8_t*)&sController_SerialAPI_Capabilities, &abController_SerialAPI_Capabilities, sizeof(sController_SerialAPI_Capabilities));
    }
}

/*============================== ReloadNodeList =============================
** Function description
**      Reloads the node IDs and types from the Device module
**      if fInitializeNetworkManagement = true then the NetworkManagement_Init
** Side effects:
**      Reinitializes the nodeType, nodeList and
**      the abListeningNodeList arrays
**
**--------------------------------------------------------------------------*/
bool 
ReloadNodeList(
    bool fInitializeNetworkManagement)
{
    bool bFound = false;
    uint8_t byNode = 1;
    uint8_t j,i;
    NODEINFO nodeInfo;
    uint8_t chip_type, chip_version;

    bMaxNodeID = 0;
    nodeListSize = 0;
    bListeningNodeListSize = 0;
    bNetworkRepeaterCount = 0;

    serialApiMemoryGetID(MySystemHomeId, &MyNodeId);
    MySystemSUCID = serialApiGetSUCNodeID();

    mainlog(logDebug, "SUC ID %03u", MySystemSUCID);
    mainlog(logDebug, "Device HomeID %02X%02X%02X%02X, NodeID %03u", 
            MySystemHomeId[0], MySystemHomeId[1], MySystemHomeId[2], MySystemHomeId[3], MyNodeId);

    serialApiGetInitData(&bSerialAPIVer, &bDeviceCapabilities, &bNodeExistMaskLen, bNodeExistMask, &chip_type, &chip_version);

    if (bDeviceCapabilities & GET_INIT_DATA_FLAG_SLAVE_API)
    {
        mainlog(logDebug, "Device is Slave");
    }
    else
    {
        char controllerStr[BUF_SIZE];
        GetControllerCapabilityStr(controllerStr, BUF_SIZE);
        mainlog(logDebug, "Device is %s", controllerStr);
    }
    /* Reset the nodelists before loading them again */
    memset(nodeType, 0, sizeof(nodeType));
    memset(nodeList, 0, sizeof(nodeList));
    memset(abListeningNodeList, 0, sizeof(abListeningNodeList));
    memset(afIsNodeAFLiRS, 0, sizeof(afIsNodeAFLiRS));

    for (i = 0; i < bNodeExistMaskLen; i++)
    {
        if (bNodeExistMask[i] != 0)
        {
            bFound = true;
            for (j = 0; j < 8; j++)
            {
                if (bNodeExistMask[i] & (1 << j))
                {
                    serialApiGetNodeProtocolInfo(byNode, &nodeInfo);
                    memcpy(&sNetworkManagementUT.nodeDescriptor[byNode - 1].nodeInfo, &nodeInfo, sizeof(nodeInfo));
                    nodeType[byNode] = nodeInfo.nodeType.generic;
                    nodeList[nodeListSize++] = byNode;
                    /* We want listening nodes and FLiRS nodes in networkTest list */
                    if (byNode != MyNodeId)
                    {
                        if ((nodeInfo.capability & NODEINFO_LISTENING_SUPPORT) ||
                            (0 != (nodeInfo.security & (NODEINFO_ZWAVE_SENSOR_MODE_WAKEUP_1000 | NODEINFO_ZWAVE_SENSOR_MODE_WAKEUP_250))))
                        {
                            /* Node is either a AC powered Listening node or a Beam wakeup Node */
                            abListeningNodeList[bListeningNodeListSize++] = byNode;
                            if (0 == (nodeInfo.security & (NODEINFO_ZWAVE_SENSOR_MODE_WAKEUP_1000 | NODEINFO_ZWAVE_SENSOR_MODE_WAKEUP_250)))
                            {
                                if (nodeInfo.capability & NODEINFO_ROUTING_SUPPORT)
                                {
                                    /* One more repeater - AC powered Listening nodes are counted as Repeater */
                                    bNetworkRepeaterCount++;
                                }
                            }
                            else
                            {
                                afIsNodeAFLiRS[byNode - 1] = true;
                            }
                        }
                    }
                    else
                    {
                    }
                    mainlog(logDebug, "Node %03u %s %02X %s", 
                               byNode, (nodeInfo.capability & NODEINFO_LISTENING_SUPPORT) ? "TRUE " : "FALSE", 
                               nodeInfo.security & (NODEINFO_ZWAVE_SENSOR_MODE_WAKEUP_1000 | NODEINFO_ZWAVE_SENSOR_MODE_WAKEUP_250), 
                               WriteNodeTypeString(nodeType[byNode]));
                    bMaxNodeID = byNode;
                }
                byNode++;
            }
        }
        else
        {
            byNode += 8;
        }
    }
    mainlog(logDebug, "Listening nodes registered %u", bListeningNodeListSize);

    if (fInitializeNetworkManagement)
    {
        InitializeSerialAPICapabilities();
        if (!NetworkManagement_Init(&sController_SerialAPI_Capabilities, abListeningNodeList, bListeningNodeListSize, afIsNodeAFLiRS, bNetworkRepeaterCount, &sNetworkManagementUT))
        {
            mainlog(logDebug, "NetworkManagement_Init: Essential functionality NOT supported");
        }
        else
        {
            mainlog(logDebug, "NetworkManagement_Init: Essential functionality supported");
        }
    }
    return bFound;
}





/*=============================== NetworkHealth ==============================
** Function description
**      Function called when starting either FULL or SINGLE Network Health 
** test. First is determined if any node needs to be requested for 
** nodeinformation which includes command classes supported. If so then
** this is then initiated. The Network Health Test is then started if no node
** needs to inform Controller about Command Classes supported or when all
** nodes needing it has been queried
** Side effects:
**
**--------------------------------------------------------------------------*/
bool
NetworkHealth(
    eTESTMODE bTestMode)
{
    bool retVal = false;
    int i;
    
    if (NULL == spNetworkManagement) return retVal;
    
    spNetworkManagement->fStop = false;

    sNetworkManagementUT.bTestMode = bTestMode;
    for (i = 0; i < bListeningNodeListSize; i++)
    {
        /* If first Command Class is NONE ZERO then we do have Command Class information for Node */
        if (0 == sNetworkManagementUT.nodeDescriptor[abListeningNodeList[i] - 1].cmdClasses[0])
        {
            /* We need to request nodeinformation from at least this node */
            retVal = true;
            break;
        }
    }
    /* Do we need to request NodeInformation from Nodes Under Test */
    if (true == retVal)
    {
        DoRequestForNodeInformation(i);
    }
    else
    {
        /* Start the real Network Health Test */
        retVal = NetworkHealthStart();
    }
    return retVal;
}


/*=================== NetworkManagement_DoRediscoveryStart ===================
** Function description
**      Start Network Health if running
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
bool
NetworkManagement_DoRediscoveryStart(
    void (*TestCompleted)(uint8_t bStatus))
{
    bool retVal = false;
    if (spNetworkManagement->bNodesUnderTestSize > 0)
    {
        spNetworkManagement->CurrentTestCompleted = TestCompleted;
        spNetworkManagement->bNHSCurrentIndex = 0;
        spNetworkManagement->fTestStarted = true;
        spNetworkManagement->bReDiscoverRound = 0;
        /* Initially no nodes have been rediscovered/touched */
        memset(spNetworkManagement->abNodesReDiscovered, 0, sizeof(spNetworkManagement->abNodesReDiscovered));

        bDutNodeID = spNetworkManagement->abNodesUnderTest[spNetworkManagement->bNHSCurrentIndex];
        requestUpdateOn = true;
        /* Assume all Rediscovered */
        spNetworkManagement->fAllDiscovered = true;
        serialApiRequestNodeNeighborUpdate(bDutNodeID, NetworkManagement_ReDiscovery_Compl);
        retVal = true;;
    }
    else
    {
        spNetworkManagement->fTestStarted = false;
        spNetworkManagement->fAllDiscovered = false;
        requestUpdateOn = false;
    }
    return retVal;
}



/*=============== NetworkManagement_NetworkHealth_Maintenance ===============
** Function description
**      NetworkManagement API Maintenance function which do the actual 
** Network Health Maintenance in Runtime.
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
bool
NetworkManagement_NetworkHealth_Maintenance()
{
    static long currentTick;
    /* Return value true if next Network Health Maintenance sample has been initiated */
    bool retVal = false;
    /* Is it time to touch next node */
    if ((NULL != spNetworkManagement) && (MAINTENANCE == spNetworkManagement->bTestMode))
    {
        /* We only check on tick transition */
        if (currentTick != lNetworkManagementTimeSecondsTicker)
        {
            currentTick = lNetworkManagementTimeSecondsTicker;
            if  (iSecondsBetweenEveryMaintenanceSample <= lNetworkManagementTimeSecondsTicker - lNetworkManagementCurrentSecondsTickerSample)
            {
                if ((!spNetworkManagement->fMaintenanceFullDiscoveryStarted) && (!spNetworkManagement->fMaintenanceSingleDiscoveryInitiated))
                {
                    sNetworkManagement_Stat *spNodeStats = &spNetworkManagement->nodeUT[spNetworkManagement->abNodesUnderTest[spNetworkManagement->bMaintenanceNHSCurrentIndex] - 1];
                    uint8_t bNodeID = spNetworkManagement->abNodesUnderTest[spNetworkManagement->bMaintenanceNHSCurrentIndex];
                    /* TODO - The Full Rediscovery situation should be decided howto and when to handle it */
                    /* For now we just do it, with no regard to time of day etc. */
                    if (spNetworkManagement->fMaintenanceNeedsFullRediscovery)
                    {
                        mainlog(logDebug, "Maintenance: Full network rediscovery - Current Node %03u", bNodeID);
                        spNetworkManagement->fMaintenanceFullDiscoveryStarted = NetworkManagement_DoRediscoveryStart(NetworkManagement_NetworkHealth_Maintenance_RediscoveryComplete);
                    }
                    else
                    {
                        if (AnyNodesNeedingRediscovery() && (0 == spNetworkManagement->bMaintenanceNHSCurrentIndex))
                        {
                            /* TODO - when to do single rediscovery should be decided */
                            /* For now we do it when a specified number of Maintenance Rounds have been executed, with no regard to time of day etc. */
                            /* Also all flagged nodes are rediscovered one after the other */
                            if (++spNetworkManagement->iMaintenanceRoundsSinceLastRediscovery >= spNetworkManagement->iMaintenanceRoundsBeforeRediscoveryExecuted)
                            {
                                mainlog(logDebug, "Maintenance: Rediscovery of flagged nodes trickered");
                                /* Restart Maintenance Round count */
                                spNetworkManagement->iMaintenanceRoundsSinceLastRediscovery = 0;
                                /* Start the Rediscovery of Nodes needing it... */
                                spNetworkManagement->fMaintenanceSingleDiscoveryInitiated = StartNextSingleNodeRediscovery();
                            }
                        }
                        else
                        {
                            if (spNodeStats->fRecalculateNetworkHealth)
                            {
                                mainlog(logDebug, "Maintenance: Recalc - Node %03u", bNodeID);
                                NetworkManagement_NetworkHealth_Maintenance_TestNode_Complete(TRANSMIT_COMPLETE_OK, 0);
                            }
                            else
                            {
                                mainlog(logDebug, "Maintenance: Sample - Node %03u, NH %02u, NB %u", bNodeID, spNodeStats->NetworkHealthNumber, spNodeStats->neighborCount);
                                /* We are in Maintenance Network Health Test mode and its time for next sample */
                                retVal = (0 != NetworkManagement_ZW_SendData(bNodeID, testConnectionPayload, sizeof(testConnectionPayload), NetworkManagement_NetworkHealth_Maintenance_TestNode_Complete));
                            }
                        }
                    }
                }
                /* Either way - Setup next Maintenance sample event */
                lNetworkManagementCurrentSecondsTickerSample = lNetworkManagementTimeSecondsTicker;
            }
        }
    }
    else
    {
        mainlog(logDebug, "MAINTENANCE != spNetworkManagement->bTestMode ");
    }
    return retVal;
}

/*============= NetworkManagement_NetworkHealth_MaintenanceStop ==============
** Function description
**      NetworkManagement API Maintenance Stop function - If in Network Health
** Maintenance Mode then Mode set to IDLE. Return true if Network Health
** Maintenance Mode was actually stopped.
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
bool
NetworkManagement_NetworkHealth_MaintenanceStop()
{
    bool retVal = false;

    if ((NULL != spNetworkManagement) && (0 < spNetworkManagement->bNodesUnderTestSize))
    {
        switch (spNetworkManagement->bTestMode)
        {
            case TEST_IDLE:
            {
                mainlog(logDebug, "Not in Network Health Maintenance Mode - IDLE");
            }
            break;

            case MAINTENANCE:
            {
                spNetworkManagement->bTestMode = TEST_IDLE;
                spNetworkManagement->fTestStarted = false;
                mainlog(logDebug, "Network Health Maintenance Mode stopped");
                retVal = true;
            }
            break;

            case SINGLE:
            {
                mainlog(logDebug, "Not in Network Health Maintenance Mode - SINGLE");
                spNetworkManagement->bTestMode = TEST_IDLE;
                spNetworkManagement->fTestStarted = false;
            }
            break;

            case FULL:
            {
                mainlog(logDebug,"Not in Network Health Maintenance Mode - FULL");
                spNetworkManagement->bTestMode = TEST_IDLE;
                spNetworkManagement->fTestStarted = false;
            }
            break;

            default:
            {
                mainlog(logDebug, "Not in Network Health Maintenance Mode - Undefined mode %i, Reset to IDLE", spNetworkManagement->bTestMode);
                spNetworkManagement->bTestMode = TEST_IDLE;
                spNetworkManagement->fTestStarted = false;
            }
            break;
        }
    }
    return retVal;
}


/*============= NetworkManagement_NetworkHealth_MaintenanceStart =============
** Function description
**      NetworkManagement API Maintenance Start function needed to be called
** before using NetworkManagement_NetworkHealth_Maintenance.
**      
** Side effects:
**      
**--------------------------------------------------------------------------*/
bool
NetworkManagement_NetworkHealth_MaintenanceStart()
{
    bool retVal = false;
    int i;

    if ((NULL != spNetworkManagement) && (0 < spNetworkManagement->bNodesUnderTestSize))
    {
        switch (spNetworkManagement->bTestMode)
        {
            case TEST_IDLE:
                /* Drop through */

            case MAINTENANCE:
                {
                    if ((NULL != spNetworkManagement) && (0 < spNetworkManagement->bNodesUnderTestSize))
                    {
                        if (MAINTENANCE != spNetworkManagement->bTestMode)
                        {
                            mainlog(logDebug, "Maintenance restart...");
                        }

                        spNetworkManagement->bTestMode = MAINTENANCE;
                        mainlog(logDebug, "Maintenance initiated");

                        for (i = 0; i < spNetworkManagement->bNodesUnderTestSize; i++)
                        {
                            if (spNetworkManagement->nodeUT[spNetworkManagement->abNodesUnderTest[i] - 1].fRecalculateNetworkHealth)
                            {
                                mainlog(logDebug, "Node %03u needs Network Health to be recalculated", spNetworkManagement->abNodesUnderTest[i]);
                                NetworkManagement_NH_Maintenance_Recalculate(spNetworkManagement->abNodesUnderTest[i]);
                            }
                            /* Reinitialiaze routeChangeLifeTime variables */
                            spNetworkManagement->nodeUT[spNetworkManagement->abNodesUnderTest[i] - 1].routeChangeLifetime = 0;
                            spNetworkManagement->nodeUT[spNetworkManagement->abNodesUnderTest[i] - 1].routeChangeLifetimeOld = 0;
                        }

                        /* Sample Maintenance start second tick */
                        lNetworkManagementCurrentSecondsTickerSample = lNetworkManagementTimeSecondsTicker;
                        iSecondsBetweenEveryMaintenanceSample = (0 != spNetworkManagement->iMaintenanceSamplePeriod) ? spNetworkManagement->iMaintenanceSamplePeriod : MAINTENANCE_PERIOD_SECS / spNetworkManagement->bNodesUnderTestSize;
                        spNetworkManagement->bMaintenanceNHSCurrentIndex = 0;
                        mainlog(logDebug, "Maintenance start tick %lu, sample period %u seconds", 
                                   lNetworkManagementCurrentSecondsTickerSample, iSecondsBetweenEveryMaintenanceSample);
                        retVal = true;
                    }
                }
                break;

            case SINGLE:
                {
                    mainlog(logDebug, "Maintenance not possible Network Health are in SINGLE testmode ");
                }
                break;

            case FULL:
                {
                    mainlog(logDebug, "Maintenance not possible Network Health are in FULL testmode ");
                }
                break;

            default:
                {
                    mainlog(logDebug, "Network Health in undefined testmode %u, Reset to IDLE", spNetworkManagement->bTestMode);
                    spNetworkManagement->bTestMode = TEST_IDLE;
                }
        }
    }
    return retVal;
}

void NetworkHealth_MaintenanceTimeout(void* data)
{
    mainlog(logDebug, "MaintenanceTimeout -> NetworkHealth_Maintenance");
    NetworkManagement_NetworkHealth_Maintenance();
}

/*========================== DoNetworkHealthMaintenance ======================
** Function description
**      Does the actual Maintenance update loop.
** requested with DoRequestnodeInformation.
** Side effects:
**
**--------------------------------------------------------------------------*/
void
DoNetworkHealthMaintenance(uint8_t mode, bool attribute)
{
    NetworkHealth_Maintenance_Forever = attribute;
    if (mode)
    {
        if (!timerNWM)
        {
            sNetworkManagementUT.bTestMode = MAINTENANCE;
            if (NetworkManagement_NetworkHealth_MaintenanceStart()) 
            {    
                timerStart(&timerNWM, NetworkHealth_MaintenanceTimeout, 0, 10000, TIMER_FOREVER);
            }
            else
            {
                mainlog(logDebug, "Network Health Maintenance Mode could not be started");
            }
        }
    }
    else
    {
        NetworkManagement_NetworkHealth_MaintenanceStop();
        timerCancel(&timerNWM);
        timerNWM = 0;
    }
}




void NetworkManagement_key_request(s2_node_inclusion_request_t* inclusion_request) 
{
    if (nm.flags & NMS_FLAG_SMART_START_INCLUSION)
    {
#if 0
        struct provision* pe = provisioning_list_dev_get(16, nm.just_included_dsk);
        uint8_t keys = inclusion_request->security_keys;
        if(pe) 
        {
            struct pvs_tlv *tlv= provisioning_list_tlv_get(pe, PVS_TLV_TYPE_ADV_JOIN);
            if( tlv && (tlv->length==1) ) 
            {
                keys = *tlv->value;
            }
        }

        /* TODO: Consult provisioning list for granted keys */
        mainlog(logDebug, "Smart Start: Grant to highest key!");
        sec2_key_grant(NODE_ADD_KEYS_SET_EX_ACCEPT_BIT,keys, 0);
#endif
    }
    else
    {
        PushNotificationToHandler(S2_KEY_GRANT_NOTIFY, (uint8_t *)inclusion_request, S2_EVT_MAX_BUFFER_SIZE);
    }

}

void NetworkManagement_dsk_challenge(s2_node_inclusion_challenge_t *challenge_evt) 
{
    if (nm.flags & NMS_FLAG_SMART_START_INCLUSION)
    {
#if 0
        /* Consult provisioning list and fill in missing Input DSK digits*/
        struct provision* w = provisioning_list_dev_match_challenge(challenge_evt->length, challenge_evt->public_key);
        if (w)
        {
            challenge_evt->public_key[0] = w->dsk[0];
            challenge_evt->public_key[1] = w->dsk[1];
            mainlog(logDebug, "Smart Start: Input DSK  found in provisioning list");
        }
        else
        {
            mainlog(logDebug, "Smart Start: Input DSK not found in provisioning list");
        }

        memcpy(nm.just_included_dsk, challenge_evt->public_key, sizeof(nm.just_included_dsk));
        sec2_dsk_accept(1, challenge_evt->public_key, 2);
#endif
    }
    else
    {
        /* Input DSK length is 2 for ACCESS and AUTHENTICATED keys, zero otherwise */

        memcpy(nm.just_included_dsk, challenge_evt->public_key, sizeof(nm.just_included_dsk));

        if (GetLearnModeState())
        {
            SetPreInclusionNIF(SECURITY_SCHEME_2_ACCESS);
            sec2_dsk_accept(1, challenge_evt->public_key, 2);
        }
        else
        {
            if (challenge_evt->granted_keys & (KEY_CLASS_S2_ACCESS | KEY_CLASS_S2_AUTHENTICATED)
                                            && ((nm.flags & NMS_FLAG_CSA_INCLUSION) == 0))
            {
                mainlog(logDebug, "reserved_dsk_len = 0 nm.flags %04x",nm.flags);
                PushNotificationToHandler(S2_PUBLIC_KEY_CHALLENGE_NOTIFY, (uint8_t *)challenge_evt, S2_EVT_MAX_BUFFER_SIZE);
            } 
            else 
            {
                if (nm.flags & NMS_FLAG_CSA_INCLUSION)
                {
                    mainlog(logDebug, "reserved_dsk_len = 4 nm.flags %04x",nm.flags);
                    nm.flags &= ~NMS_FLAG_CSA_INCLUSION;
                    sec2_dsk_accept(1, challenge_evt->public_key, 4);
                }
                else
                {
                    mainlog(logDebug, "reserved_dsk_len = 2 nm.flags %04x",nm.flags);
                    sec2_dsk_accept(1, challenge_evt->public_key, 2);
                }

            }
        }
    }

}

#if 0
void NetworkManagement_INIF_Received(uint8_t bNodeID, uint8_t INIF_rxStatus,
    uint8_t *INIF_NWI_homeid)
{

    struct provision *ple = provisioning_list_dev_get_homeid(INIF_NWI_homeid);
    if(ple) 
    {
        mainlog(logDebug, "INIF received and item is in provisioning list HOMEID %4X\n", *((int32_t*)INIF_NWI_homeid));   
    } 
    else 
    {
        mainlog(logDebug, "INIF received but item is not in provisioning list HOMEID %4X\n", *((int32_t*)INIF_NWI_homeid));
    }
}
#endif

