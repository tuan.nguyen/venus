#include "ZW_AddingInterviewDevice_FSM.h"
#include "ZW_AutoRemoveAdd_FSM.h"
#include "timer.h"
#include "resource_directory.h"
#include "nvm.h"
#include "supported_CommandClassInclusionController.h"
#include "node_cache.h"
#include "Transport_ApplCmdHandlerEx.h"

#define NO_TIMER 0   /* Unused timer handle */
#define NO_TIMEOUT 0 /* No timeout from this state_auto_remove_add */

uint8_t actionFromUser = NO_ACTION_NETWORK; //save event of user

typedef struct auto_remove_add_s
{
    uint8_t bStatus; // status
    uint8_t bSource; //NodeID of Node Info
    uint8_t *pCmd;   //Pointer of Node Info
    uint8_t bLen;    //Length of Node Info
} AUTOADD_t, *AUTOADD_p_t;

typedef struct
{
    STATE_AUTO_REMOVE_ADD st;      // current state_auto_remove_add
    AUTOREMOVEADD_FSM_EV_T ev;     // incoming event
    STATE_AUTO_REMOVE_ADD next_st; // next state_auto_remove_add
    void (*fn)(AUTOADD_p_t);       // action function returning next state_auto_remove_add
} transition_t;

typedef struct
{
    STATE_AUTO_REMOVE_ADD st; //current state_auto_remove_add
    uint32_t timeout;         //timeout of sate (ms)
    void (*tfn)(void *);      //fuction is execute when timeout
} state_auto_remove_add_timeout_t;

//variable push notify
NOTIFY_TX_BUFFER_T pTxNotify;
NODE_TYPE node_Type;
uint8_t schemeGet;

extern void NmTimeout(void *data);
extern void resetNMTimer(void);

//variable for timeout
static timer_t bFsmTimerHandle_AutoAdd = NO_TIMER;
static uint16_t bFsmTimerCountdown_AutoAdd;
static void (*cbFsmTimer_AutoAdd)(void *);

//variable for this file
STATE_AUTO_REMOVE_ADD state_auto_remove_add = ST_AUTOADD_IDLE;
AUTOADD_t auto_add_para;

void AutoRemoveDeviceZPC_Compl(uint8_t bStatus, uint8_t bSource, uint8_t *pCmd, uint8_t bLen);
void AutoAddDeviceZPC_Compl(uint8_t bStatus, uint8_t bSource, uint8_t *pCmd, uint8_t bLen);

//----------------Action of Auto Remove Add device process------------------
void AC_InitRemoveReady(AUTOADD_p_t auto_add_info)
{
    nm.mode = NMM_AUTO_REMOVEADD_NODE;
    serialApiRemoveNodeFromNetwork(REMOVE_NODE_ANY | REMOVE_NODE_OPTION_NETWORK_WIDE, AutoRemoveDeviceZPC_Compl);
}

void AC_PushNotifyRemoveReady(AUTOADD_p_t auto_add_info)
{
    pTxNotify.RemoveNodeZPCNotify.bStatus = auto_add_info->bStatus;
    PushNotificationToHandler(REMOVE_NODE_ZPC_NOTIFY_AUTO, (uint8_t *)&pTxNotify.RemoveNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
}

void AC_CloseRemove(AUTOADD_p_t auto_add_info)
{
    mainlog(logDebug, "Close Remove Device");
    serialApiRemoveNodeFromNetwork(REMOVE_NODE_STOP, NULL);
}

void AC_CloseRemove_StopS2(AUTOADD_p_t auto_add_info)
{
    mainlog(logDebug, "Close Remove Device");
    serialApiRemoveNodeFromNetwork(REMOVE_NODE_STOP, NULL);
    /*Stop the S2 FSM */
    nm.mode = NMM_DEFAULT;
    sec2_abort_join();
}

void AC_PushNotifyRemoveFoud(AUTOADD_p_t auto_add_info)
{
}

void AC_SaveEV_OpenToSchedular(AUTOADD_p_t auto_add_info)
{
    mainlog(logDebug, "Waiting Remove/Add Finish");
    actionFromUser = OPEN_NETWORK;
}

void AC_SaveEV_Open_CancelS2(AUTOADD_p_t auto_add_info)
{
    mainlog(logDebug, "Waiting Remove/Add Finish");
    actionFromUser = OPEN_NETWORK;
    sec2_abort_join();        
}

void AC_SaveEV_CloseToSchedular(AUTOADD_p_t auto_add_info)
{
    mainlog(logDebug, "Waiting Remove/Add Finish");
    actionFromUser = CLOSE_NETWORK;

}

void AC_SaveEV_Close_CancelS2(AUTOADD_p_t auto_add_info)
{
    mainlog(logDebug, "Waiting Remove/Add Finish");
    actionFromUser = CLOSE_NETWORK;
    sec2_abort_join();    
}

void AC_PushNotifyRemoving(AUTOADD_p_t auto_add_info)
{
    node_Type = *((NODE_TYPE *)auto_add_info->pCmd);
    
    if (auto_add_info->bSource)
    {
        //rd_remove_node(auto_add_info->bSource);
        /*store removed node to lastRemoveNode struct*/
        lastRemovedNode.node_id = auto_add_info->bSource;
        lastRemovedNode.node_type = node_Type.generic;
        lastRemovedNode.node_info.nodeType.basic = node_Type.basic;
        lastRemovedNode.node_info.nodeType.generic = node_Type.generic;
        lastRemovedNode.node_info.nodeType.specific = node_Type.specific;
        if (auto_add_info->bLen > 3)
        {
            lastRemovedNode.node_capability.noCapability = auto_add_info->bLen - 3;
            memcpy((char *)&lastRemovedNode.node_capability.aCapability, auto_add_info->pCmd + 3, auto_add_info->bLen - 3);
        }
    }
    else
    {
        lastRemovedNode.node_id = 0;
    }

    pTxNotify.RemoveNodeZPCNotify.bStatus = auto_add_info->bStatus;
    pTxNotify.RemoveNodeZPCNotify.zpc_node.node_id = auto_add_info->bSource;
    pTxNotify.RemoveNodeZPCNotify.zpc_node.node_type = node_Type.generic;

    PushNotificationToHandler(REMOVE_NODE_ZPC_NOTIFY_AUTO, (uint8_t *)&pTxNotify.RemoveNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));

    mainlog(logDebug, "ZPC: [AUTO_REMOVE_NODE_STATUS_REMOVING(%02X)]  - Removing Node %03u (%s)... (Nodeinfor length %u)",
            auto_add_info->bStatus, auto_add_info->bSource, WriteNodeTypeString(node_Type.generic), auto_add_info->bLen);
    serialApiRemoveNodeFromNetwork(REMOVE_NODE_STOP, AutoRemoveDeviceZPC_Compl);
}

void AC_PushNotifyRemoveDone_CloseRemove_CheckNetWork(AUTOADD_p_t auto_add_info)
{
    pTxNotify.RemoveNodeZPCNotify.bStatus = auto_add_info->bStatus;

    rd_remove_node(auto_add_info->bSource);
    removeKeyHeldDownMessageListFromNodeId(auto_add_info->bSource);
    removeSequenceNumberFromNodeId(auto_add_info->bSource);

    PushNotificationToHandler(REMOVE_NODE_ZPC_NOTIFY_AUTO, (uint8_t *)&pTxNotify.RemoveNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
    mainlog(logDebug, "---------------------------------------------------------------");
    AC_CloseRemove(NULL);
}

void AC_CloseRemove_InitRemoveReady(AUTOADD_p_t auto_add_info)
{
    pTxNotify.RemoveNodeZPCNotify.bStatus = auto_add_info->bStatus;
    PushNotificationToHandler(REMOVE_NODE_ZPC_NOTIFY_AUTO, (uint8_t *)&pTxNotify.RemoveNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
    mainlog(logDebug, "---------------------------------------------------------------");
    AC_CloseRemove(NULL);
    AC_InitRemoveReady(NULL);
}

void AC_PushNotifyRemoveFail_CloseRemove_CheckNetWork(AUTOADD_p_t auto_add_info)
{
    pTxNotify.RemoveNodeZPCNotify.bStatus = auto_add_info->bStatus;
    PushNotificationToHandler(REMOVE_NODE_ZPC_NOTIFY_AUTO, (uint8_t *)&pTxNotify.RemoveNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
    AC_CloseRemove(NULL);
}

void AC_InitAddReady(AUTOADD_p_t auto_add_info)
{
    //--------------------------------------------------
    memset((uint8_t *)&lastLearnedNodeZPC, 0, sizeof(ZW_Node_t));
    serialApiAddNodeToNetwork(ADD_NODE_ANY | ADD_NODE_OPTION_NORMAL_POWER |
                                   ADD_NODE_OPTION_NETWORK_WIDE, AutoAddDeviceZPC_Compl);
}

void AC_PushNotifyAddReady(AUTOADD_p_t auto_add_info)
{
    pTxNotify.AddNodeZPCNotify.bStatus = auto_add_info->bStatus;
    PushNotificationToHandler(ADD_NODE_ZPC_NOTIFY_AUTO, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
}

void AC_CloseAdd(AUTOADD_p_t auto_add_info)
{
    serialApiAddNodeToNetwork(ADD_NODE_STOP, NULL);
}

void AC_CloseAdd_StopS2(AUTOADD_p_t auto_add_info)
{
    serialApiAddNodeToNetwork(ADD_NODE_STOP, NULL);
    /*Stop the S2 FSM */
    nm.mode = NMM_DEFAULT;
    sec2_abort_join();
}

void AC_PushNotifyAddFoud(AUTOADD_p_t auto_add_info)
{
    pTxNotify.AddNodeZPCNotify.bStatus = auto_add_info->bStatus;
    PushNotificationToHandler(ADD_NODE_ZPC_NOTIFY_AUTO, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
}

void AC_PushNotifyAdding(AUTOADD_p_t auto_add_info)
{
    NODE_TYPE node_Type = *((NODE_TYPE *)auto_add_info->pCmd);
    
    if (auto_add_info->bSource > 0)
    {
        //stored new node to lastLearnedNode struct
        lastLearnedNodeZPC.node_id = auto_add_info->bSource;
        lastLearnedNodeZPC.node_type = node_Type.generic;
        lastLearnedNodeZPC.node_info.nodeType.basic = node_Type.basic;
        lastLearnedNodeZPC.node_info.nodeType.generic = node_Type.generic;
        lastLearnedNodeZPC.node_info.nodeType.specific = node_Type.specific;
        if (auto_add_info->bLen > 3)
        {
            lastLearnedNodeZPC.node_capability.noCapability = auto_add_info->bLen - 3;
            memcpy((char *)&lastLearnedNodeZPC.node_capability.aCapability, auto_add_info->pCmd + 3, auto_add_info->bLen - 3);
        }
    }
    pTxNotify.AddNodeZPCNotify.bStatus = auto_add_info->bStatus;
    pTxNotify.AddNodeZPCNotify.zpc_node.node_id = auto_add_info->bSource;
    pTxNotify.AddNodeZPCNotify.zpc_node.node_type = node_Type.generic;
    PushNotificationToHandler(ADD_NODE_ZPC_NOTIFY_AUTO, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
}

void AC_AddProtocolDone(AUTOADD_p_t auto_add_info)
{
    serialApiAddNodeToNetwork(ADD_NODE_STOP, AutoAddDeviceZPC_Compl);
}

void AC_PushNotifyAddDone_CloseAdd(AUTOADD_p_t auto_add_info)
{

    serialApiAddNodeToNetwork(ADD_NODE_STOP, NULL);

    if (rd_node_exists(lastLearnedNodeZPC.node_id))
    {
        int flags = GetCacheEntryFlag(lastLearnedNodeZPC.node_id);
        mainlog(logDebug, "This node has already been included with flags: %d", flags);
        if (SecureInclusionRestart(flags, SecurityInclusionDone_Compl)) return;
        goto interview;
    }
    else /* new node */
    {
        //rd_probe_lock(true);
        rd_register_new_node(lastLearnedNodeZPC.node_id, true);
    }
   
    MySystemSUCId = serialApiGetSUCNodeID();
    mainlog(logDebug, "AutoRemoveAdd Mode, MySystemSUCId: %d, suc support Inclusion CC %d", MySystemSUCId, GetSupportedInclusionControllerCmdClass(MySystemSUCId));

    if ((MySystemSUCId != MyNodeId) && MySystemSUCId)
    {
        if (GetSupportedInclusionControllerCmdClass(MySystemSUCId) == CC_SUPPORTED)
        {
            /* inclusion Device When Have SIS in network and support command class Inclusion controller */            
            nm.state = NMS_INCLUSION_CONTROLLER;//inclusion controller is 1 state of auto remove add
            nm.tmp_node = lastLearnedNodeZPC.node_id;
            PostEventInclusionController(EV_ADD_NODE_NORMAL_OK);
            return;
        }
        else if (GetSupportedInclusionControllerCmdClass(MySystemSUCId) == CC_NOT_PROBED)
        {
            nm.state = NMS_WAIT_FOR_SUC_ADD_NODE;//inclusion controller is 1 state of auto remove add
            nm.tmp_node = MySystemSUCId;
            resetNMTimer();
            timerStart(&(nm.nm_timer), NmTimeout, 0, WAITING_SEND_REQ_NIF, TIMER_ONETIME);//waiting NIF 5s
            return;

        }
    }
    //cancel timer add manual

    nm.state = NMS_WAIT_FOR_SECURE_ADD;

    if (((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC, COMMAND_CLASS_SECURITY_2)) != SCHEME_NOT_SUPPORT) &&
        (GetCacheEntryFlag(MyNodeId) & NODE_FLAGS_SECURITY2))
    {
        securityS2InclusionStart(auto_add_info->bSource, SecurityInclusionDone_Compl);
        return;
    }

    if (((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC, COMMAND_CLASS_SECURITY)) != SCHEME_NOT_SUPPORT) &&
        (GetCacheEntryFlag(MyNodeId) & NODE_FLAG_SECURITY0))
    {
        if ((lastLearnedNodeZPC.node_info.nodeType.basic == BASIC_TYPE_CONTROLLER) ||
            (lastLearnedNodeZPC.node_info.nodeType.basic == BASIC_TYPE_STATIC_CONTROLLER))
        {
            securityS0InclusionStart(auto_add_info->bSource, true, SecurityInclusionDone_Compl);
        }
        else
        {
            securityS0InclusionStart(auto_add_info->bSource, false, SecurityInclusionDone_Compl);
        }
        return;
    }

interview:
    /* This is a non secure node */
    nm.state = NMS_WAITING_FOR_INTERVIEW;
    lastLearnedNodeZPC.node_secure_scheme = NO_SCHEME;
    PostEventAutoRemoveAdd(EV_FINISH_ADD_SECURITY);    
    PostEventInterviewDevice(EV_ADD_NODE_OK);
}

void AC_PushNotifyAddFail_CloseAdd_CheckNetWork(AUTOADD_p_t auto_add_info)
{
    memset((uint8_t *)&lastLearnedNodeZPC, 0, sizeof(ZW_Node_t));
    pTxNotify.AddNodeZPCNotify.bStatus = auto_add_info->bStatus;
    PushNotificationToHandler(ADD_NODE_ZPC_NOTIFY_AUTO, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
    serialApiAddNodeToNetwork(ADD_NODE_STOP, NULL);
}

//stop s2 fsm when receive close network event
void AC_StopS2FMS(AUTOADD_p_t auto_add_info)
{
    /*Stop the S2 FSM */
    nm.mode = NMM_DEFAULT;
    sec2_abort_join();
}
//timeout of state_auto_remove_add
void ZCB_TimerAddReady(void *data)
{
    PostEventAutoRemoveAdd(EV_TIMEOUT_ADD_READY);
}

void ZCB_TimerCloseAddReady(void *data)
{
    PostEventAutoRemoveAdd(EV_TIMEOUT_WAITING_CLOSE_ADD_READY);
}

void ZCB_TimerWaitingOpenNetwork(void *data)
{
    PostEventAutoRemoveAdd(EV_TIMEOUT_WAITING_OPEN_NETWORK);
}

void ZCB_TimerWaitingChangeRemveToAdd(void *data)
{
    PostEventAutoRemoveAdd(EV_TIMEOUT_CHANGE_REMOVE_TO_ADD);
}

void ZCB_TimerWaitingAdding(void *data)
{
    mainlog(logDebug, "ZCB_TimerWaitingAdding!!!!, actionFromUser: %d", actionFromUser);
    //Check network Open/Close
    if (CLOSE_NETWORK == actionFromUser)
    {
        PostEventAutoRemoveAdd(EV_CLOSE_NWK_AT_ADDING);
    }
    else
    {
        PostEventAutoRemoveAdd(EV_TIMEOUT_WAITING_ADDING);
    }
    actionFromUser = NO_ACTION_NETWORK;

}
/********************************************************/
/*    uint8_t bStatus, IN Status of learn process*/
/*    uint8_t bSource, IN Node ID of node learned*/
/*    uint8_t *pCmd,   IN Pointer to node information*/
/*    uint8_t bLen     IN Length of node information*/
/********************************************************/
void AutoRemoveDeviceZPC_Compl(uint8_t bStatus, uint8_t bSource, uint8_t *pCmd, uint8_t bLen)
{

    auto_add_para.bStatus = bStatus;
    auto_add_para.pCmd = pCmd;
    auto_add_para.bLen = bLen;
    switch (bStatus)
    {
    case REMOVE_NODE_STATUS_LEARN_READY:
        PostEventAutoRemoveAdd(EV_REMOVE_NODE_READY_OK);
        break;
    case REMOVE_NODE_STATUS_NODE_FOUND:
        auto_add_para.bSource = bSource;
        PostEventAutoRemoveAdd(EV_REMOVE_NODE_FOUND_OK);
        break;
    case REMOVE_NODE_STATUS_REMOVING_SLAVE:
        auto_add_para.bSource = bSource;
        PostEventAutoRemoveAdd(EV_REMOVING_ACK);
        break;
    case REMOVE_NODE_STATUS_REMOVING_CONTROLLER:
        auto_add_para.bSource = bSource;
        PostEventAutoRemoveAdd(EV_REMOVING_ACK);
        break;
    case REMOVE_NODE_STATUS_DONE:
        //Check network Open/Close
        if (CLOSE_NETWORK == actionFromUser)
        {
            PostEventAutoRemoveAdd(EV_CLOSE_NETWORK);
        }
        else
        {
            if (OPEN_NETWORK == actionFromUser)
            {
                PostEventAutoRemoveAdd(EV_OPEN_NETWORK);
            }
            else
            {
                PostEventAutoRemoveAdd(EV_REMOVE_DONE_ACK);
            }
        }
        actionFromUser = NO_ACTION_NETWORK;
        break;
    case REMOVE_NODE_STATUS_FAILED:
        auto_add_para.bSource = bSource;
        if (CLOSE_NETWORK == actionFromUser)
        {
            PostEventAutoRemoveAdd(EV_CLOSE_NETWORK);
        }
        else
        {
            PostEventAutoRemoveAdd(EV_REMOVE_FAIL_ACK); //<=>Open Network
        }
        break;
    default:
        break;
    }
}

void AutoAddDeviceZPC_Compl(uint8_t bStatus, uint8_t bSource, uint8_t *pCmd, uint8_t bLen)
{
    //AUTOADD_t auto_add_para;
    auto_add_para.bStatus = bStatus;
    auto_add_para.bSource = bSource;
    auto_add_para.pCmd = pCmd;
    auto_add_para.bLen = bLen;
    switch (bStatus)
    {
    case ADD_NODE_STATUS_LEARN_READY:
        PostEventAutoRemoveAdd(EV_ADD_READY_OK);
        break;
    case ADD_NODE_STATUS_NODE_FOUND:
        PostEventAutoRemoveAdd(EV_ADD_FOUND_OK);
        break;
    case ADD_NODE_STATUS_ADDING_SLAVE:
        PostEventAutoRemoveAdd(EV_ADDING_ACK);
        break;
    case ADD_NODE_STATUS_ADDING_CONTROLLER:
        PostEventAutoRemoveAdd(EV_ADDING_ACK);
        break;
    case ADD_NODE_STATUS_PROTOCOL_DONE:
        PostEventAutoRemoveAdd(EV_ADD_PROTOCOL_OK);
        break;
    case ADD_NODE_STATUS_DONE:
        //Check network Open/Close
        PostEventAutoRemoveAdd(EV_ADD_DONE_ACK);
        break;
    case ADD_NODE_STATUS_FAILED:
        //Check network Open/Close
        if (CLOSE_NETWORK == actionFromUser)
            PostEventAutoRemoveAdd(EV_CLOSE_NETWORK);
        else
        {
            if (OPEN_NETWORK == actionFromUser)
                PostEventAutoRemoveAdd(EV_OPEN_NETWORK);
            else
            {
                PostEventAutoRemoveAdd(EV_ADD_FAIL_ACK);
            }
        }
        actionFromUser = NO_ACTION_NETWORK;
        break;
    default:
        break;
    }
}

//--------------------------------------------------------------------------
/* State transition table
 * This is where the state_auto_remove_add machine is defined.
 *
 * Each transition can optionally trigger a call to an action function.
 * An action function defines the actions that must occur when a particular
 * transition occurs.
 *
 * Format: {Current state_auto_remove_add, incoming event, next state_auto_remove_add, action_function} */

static const transition_t trans[] =
    {
        {ST_AUTOADD_IDLE,              EV_OPEN_NETWORK,                     ST_WAITING_REMOVE_READY_ACK,        &AC_InitRemoveReady},
        {ST_AUTOADD_IDLE,              EV_CLOSE_NETWORK,                    ST_AUTOADD_IDLE,                    &AC_StopS2FMS},
        /*handle event finish interview device, nm.mode != NMM_AUTO_REMOVEADD_NODE*/
        {ST_AUTOADD_IDLE,              EV_CLOSE_NETWORK_AT_INTERVIEW,       ST_AUTOADD_IDLE,                    NULL},
        {ST_AUTOADD_IDLE,              EV_OPEN_NETWORK_AT_INTERVIEW,        ST_AUTOADD_IDLE,                    NULL},
        {ST_AUTOADD_IDLE,              EV_FINISH_INTERVIEW,                 ST_AUTOADD_IDLE,                    NULL},
        
        {ST_WAITING_REMOVE_READY_ACK,  EV_REMOVE_NODE_READY_OK,             ST_WAITING_REMOVE_FOUND_ACK,        &AC_PushNotifyRemoveReady},
        {ST_WAITING_REMOVE_READY_ACK,  EV_REMOVE_FAIL_ACK,                  ST_WAITING_OPEN_NETWORK,            &AC_PushNotifyRemoveFail_CloseRemove_CheckNetWork},
        {ST_WAITING_REMOVE_READY_ACK,  EV_OPEN_NETWORK,                     ST_WAITING_REMOVE_READY_ACK,        NULL},
        {ST_WAITING_REMOVE_READY_ACK,  EV_CLOSE_NETWORK,                    ST_AUTOADD_IDLE,                    &AC_CloseRemove_StopS2},
        
        {ST_WAITING_REMOVE_FOUND_ACK,  EV_REMOVE_NODE_FOUND_OK,             ST_WAITING_REMOVING_ACK,            &AC_PushNotifyRemoveFoud},
        {ST_WAITING_REMOVE_FOUND_ACK,  EV_REMOVE_FAIL_ACK,                  ST_WAITING_OPEN_NETWORK,            &AC_PushNotifyRemoveFail_CloseRemove_CheckNetWork},
        {ST_WAITING_REMOVE_FOUND_ACK,  EV_OPEN_NETWORK,                     ST_WAITING_REMOVE_FOUND_ACK,        NULL},
        {ST_WAITING_REMOVE_FOUND_ACK,  EV_CLOSE_NETWORK,                    ST_AUTOADD_IDLE,                    &AC_CloseRemove_StopS2},
        
        {ST_WAITING_REMOVING_ACK,      EV_REMOVING_ACK,                     ST_WAITING_REMOVE_DONE_ACK,         &AC_PushNotifyRemoving},
        {ST_WAITING_REMOVING_ACK,      EV_REMOVE_FAIL_ACK,                  ST_WAITING_OPEN_NETWORK,            &AC_PushNotifyRemoveFail_CloseRemove_CheckNetWork},
        {ST_WAITING_REMOVING_ACK,      EV_OPEN_NETWORK,                     ST_WAITING_FINISH_REMOVE,           &AC_SaveEV_OpenToSchedular},
        {ST_WAITING_REMOVING_ACK,      EV_CLOSE_NETWORK,                    ST_WAITING_FINISH_REMOVE,           &AC_SaveEV_CloseToSchedular},
        
        {ST_WAITING_FINISH_REMOVE,     EV_OPEN_NETWORK,                     ST_WAITING_REMOVE_DONE_ACK,         &AC_SaveEV_OpenToSchedular},
        {ST_WAITING_FINISH_REMOVE,     EV_CLOSE_NETWORK,                    ST_WAITING_REMOVE_DONE_ACK,         &AC_SaveEV_CloseToSchedular},
        
        {ST_WAITING_REMOVE_DONE_ACK,   EV_REMOVE_DONE_ACK,                  ST_WAITING_REMOVE_TO_ADD,           &AC_PushNotifyRemoveDone_CloseRemove_CheckNetWork}, //check event Open/Close Network
        {ST_WAITING_REMOVE_DONE_ACK,   EV_REMOVE_FAIL_ACK,                  ST_WAITING_OPEN_NETWORK,            &AC_PushNotifyRemoveFail_CloseRemove_CheckNetWork},  //check event Open/Close Network
        {ST_WAITING_REMOVE_DONE_ACK,   EV_OPEN_NETWORK,                     ST_WAITING_REMOVE_READY_ACK,        &AC_CloseRemove_InitRemoveReady},
        {ST_WAITING_REMOVE_DONE_ACK,   EV_CLOSE_NETWORK,                    ST_AUTOADD_IDLE,                    &AC_CloseRemove_StopS2},
        
        {ST_WAITING_REMOVE_FAILED_ACK, EV_REMOVE_FAIL_ACK,                  ST_WAITING_OPEN_NETWORK,            &AC_PushNotifyRemoveFail_CloseRemove_CheckNetWork},
        {ST_WAITING_REMOVE_FAILED_ACK, EV_OPEN_NETWORK,                     ST_WAITING_OPEN_NETWORK,            &AC_CloseRemove},
        {ST_WAITING_REMOVE_FAILED_ACK, EV_CLOSE_NETWORK,                    ST_AUTOADD_IDLE,                    &AC_CloseRemove_StopS2},
    
        {ST_WAITING_REMOVE_TO_ADD,     EV_TIMEOUT_CHANGE_REMOVE_TO_ADD,     ST_WAITING_ADD_READY_ACK,           &AC_InitAddReady},
        {ST_WAITING_REMOVE_TO_ADD,     EV_OPEN_NETWORK,                     ST_WAITING_REMOVE_READY_ACK,        &AC_InitRemoveReady},
        {ST_WAITING_REMOVE_TO_ADD,     EV_CLOSE_NETWORK,                    ST_AUTOADD_IDLE,                    &AC_StopS2FMS},
    
        {ST_WAITING_ADD_READY_ACK,     EV_ADD_READY_OK,                     ST_WAITING_ADD_FOUND_ACK,           &AC_PushNotifyAddReady},
        {ST_WAITING_ADD_READY_ACK,     EV_ADD_FAIL_ACK,                     ST_WAITING_OPEN_NETWORK,            &AC_PushNotifyAddFail_CloseAdd_CheckNetWork},
        {ST_WAITING_ADD_READY_ACK,     EV_TIMEOUT_ADD_READY,                ST_WAITING_OPEN_NETWORK,            &AC_CloseAdd}, //Function Timeout, Push Notify, change to Remove Reay
        {ST_WAITING_ADD_READY_ACK,     EV_OPEN_NETWORK,                     ST_WAITING_CLOSE_ADD,               &AC_CloseAdd},
        {ST_WAITING_ADD_READY_ACK,     EV_CLOSE_NETWORK,                    ST_AUTOADD_IDLE,                    &AC_CloseAdd_StopS2},
    
        {ST_WAITING_ADD_FOUND_ACK,     EV_ADD_FOUND_OK,                     ST_WAITING_ADDING_ACK,              &AC_PushNotifyAddFoud},
        {ST_WAITING_ADD_FOUND_ACK,     EV_ADD_FAIL_ACK,                     ST_WAITING_OPEN_NETWORK,            &AC_PushNotifyAddFail_CloseAdd_CheckNetWork},
        {ST_WAITING_ADD_FOUND_ACK,     EV_TIMEOUT_ADD_READY,                ST_WAITING_OPEN_NETWORK,            &AC_CloseAdd},
        {ST_WAITING_ADD_FOUND_ACK,     EV_OPEN_NETWORK,                     ST_WAITING_CLOSE_ADD,               &AC_CloseAdd},
        {ST_WAITING_ADD_FOUND_ACK,     EV_CLOSE_NETWORK,                    ST_AUTOADD_IDLE,                    &AC_CloseAdd_StopS2},
    
        {ST_WAITING_ADDING_ACK,        EV_ADDING_ACK,                       ST_WAITING_ADD_PROTOCOL_ACK,        &AC_PushNotifyAdding},
        {ST_WAITING_ADDING_ACK,        EV_ADD_FAIL_ACK,                     ST_WAITING_OPEN_NETWORK,            &AC_PushNotifyAddFail_CloseAdd_CheckNetWork},
        {ST_WAITING_ADDING_ACK,        EV_OPEN_NETWORK,                     ST_WAITING_ADDING_ACK,              &AC_SaveEV_OpenToSchedular},
        {ST_WAITING_ADDING_ACK,        EV_CLOSE_NETWORK,                    ST_WAITING_ADDING_ACK,              &AC_SaveEV_CloseToSchedular},
        {ST_WAITING_ADDING_ACK,        EV_TIMEOUT_WAITING_ADDING,           ST_WAITING_OPEN_NETWORK,            &AC_PushNotifyAddFail_CloseAdd_CheckNetWork},
        {ST_WAITING_ADDING_ACK,        EV_CLOSE_NWK_AT_ADDING,              ST_AUTOADD_IDLE,                    &AC_PushNotifyAddFail_CloseAdd_CheckNetWork},
    
        {ST_WAITING_ADD_PROTOCOL_ACK,  EV_ADD_PROTOCOL_OK,                  ST_WAITING_ADD_DONE_ACK,            &AC_AddProtocolDone},
        {ST_WAITING_ADD_PROTOCOL_ACK,  EV_ADD_FAIL_ACK,                     ST_WAITING_OPEN_NETWORK,            &AC_PushNotifyAddFail_CloseAdd_CheckNetWork},
        {ST_WAITING_ADD_PROTOCOL_ACK,  EV_OPEN_NETWORK,                     ST_WAITING_ADD_PROTOCOL_ACK,        &AC_SaveEV_OpenToSchedular},
        {ST_WAITING_ADD_PROTOCOL_ACK,  EV_CLOSE_NETWORK,                    ST_WAITING_ADD_PROTOCOL_ACK,        &AC_SaveEV_CloseToSchedular},
        {ST_WAITING_ADD_PROTOCOL_ACK,  EV_TIMEOUT_WAITING_ADDING,           ST_WAITING_OPEN_NETWORK,            &AC_PushNotifyAddFail_CloseAdd_CheckNetWork},
        {ST_WAITING_ADD_PROTOCOL_ACK,  EV_CLOSE_NWK_AT_ADDING,              ST_AUTOADD_IDLE,                    &AC_PushNotifyAddFail_CloseAdd_CheckNetWork},

        {ST_WAITING_ADD_DONE_ACK,      EV_ADD_DONE_ACK,                     ST_WAITING_ADD_SECURE,              &AC_PushNotifyAddDone_CloseAdd},
        {ST_WAITING_ADD_DONE_ACK,      EV_OPEN_NETWORK,                     ST_WAITING_ADD_DONE_ACK,            &AC_SaveEV_OpenToSchedular},
        {ST_WAITING_ADD_DONE_ACK,      EV_CLOSE_NETWORK,                    ST_WAITING_ADD_DONE_ACK,            &AC_SaveEV_CloseToSchedular},

        {ST_WAITING_ADD_SECURE,        EV_FINISH_ADD_SECURITY,              ST_WAITING_FINISH_INTERVIEW,        NULL},
        {ST_WAITING_ADD_SECURE,        EV_OPEN_NETWORK,                     ST_WAITING_FINISH_INTERVIEW,        &AC_SaveEV_Open_CancelS2},
        {ST_WAITING_ADD_SECURE,        EV_CLOSE_NETWORK,                    ST_WAITING_FINISH_INTERVIEW,        &AC_SaveEV_Close_CancelS2},

        {ST_WAITING_ADD_FAIL_ACK,      EV_ADD_FAIL_ACK,                     ST_WAITING_OPEN_NETWORK,            &AC_PushNotifyAddFail_CloseAdd_CheckNetWork},
        {ST_WAITING_ADD_FAIL_ACK,      EV_OPEN_NETWORK,                     ST_WAITING_OPEN_NETWORK,            &AC_CloseAdd},
        {ST_WAITING_ADD_FAIL_ACK,      EV_CLOSE_NETWORK,                    ST_AUTOADD_IDLE,                    &AC_CloseAdd_StopS2},
    
        {ST_WAITING_FINISH_INTERVIEW,  EV_FINISH_INTERVIEW,                 ST_WAITING_INTERVIEW_TO_OPEN,       NULL},
        {ST_WAITING_FINISH_INTERVIEW,  EV_OPEN_NETWORK,                     ST_WAITING_FINISH_INTERVIEW,        &AC_SaveEV_OpenToSchedular},
        {ST_WAITING_FINISH_INTERVIEW,  EV_CLOSE_NETWORK,                    ST_WAITING_FINISH_INTERVIEW,        &AC_SaveEV_CloseToSchedular},
        {ST_WAITING_FINISH_INTERVIEW,  EV_OPEN_NETWORK_AT_INTERVIEW,        ST_WAITING_REMOVE_READY_ACK,        &AC_InitRemoveReady},
        {ST_WAITING_FINISH_INTERVIEW,  EV_CLOSE_NETWORK_AT_INTERVIEW,       ST_AUTOADD_IDLE,                    &AC_StopS2FMS},
//        {ST_WAITING_FINISH_INTERVIEW,  EV_TIMEOUT_WAITING_ADDING,           ST_WAITING_OPEN_NETWORK,            &AC_PushNotifyAddFail_CloseAdd_CheckNetWork},
            
        {ST_WAITING_OPEN_NETWORK,      EV_TIMEOUT_WAITING_OPEN_NETWORK,     ST_WAITING_REMOVE_READY_ACK,        &AC_InitRemoveReady},
        {ST_WAITING_OPEN_NETWORK,      EV_OPEN_NETWORK,                     ST_WAITING_OPEN_NETWORK,            &AC_InitRemoveReady},
        {ST_WAITING_OPEN_NETWORK,      EV_CLOSE_NETWORK,                    ST_AUTOADD_IDLE,                    &AC_StopS2FMS},

        {ST_WAITING_CLOSE_ADD,         EV_TIMEOUT_WAITING_CLOSE_ADD_READY,  ST_WAITING_REMOVE_READY_ACK,        &AC_InitRemoveReady}, //timeout, push event
        {ST_WAITING_CLOSE_ADD,         EV_OPEN_NETWORK,                     ST_WAITING_CLOSE_ADD,               NULL},
        {ST_WAITING_CLOSE_ADD,         EV_CLOSE_NETWORK,                    ST_AUTOADD_IDLE,                    &AC_StopS2FMS},

        {ST_WAITING_INTERVIEW_TO_OPEN, EV_TIMEOUT_WAITING_OPEN_NETWORK,     ST_WAITING_REMOVE_READY_ACK,        &AC_InitRemoveReady},
        {ST_WAITING_INTERVIEW_TO_OPEN, NO_ACTION_FROM_HANDLER,              ST_WAITING_REMOVE_READY_ACK,        &AC_InitRemoveReady},
        {ST_WAITING_INTERVIEW_TO_OPEN, EV_OPEN_NETWORK,                     ST_WAITING_INTERVIEW_TO_OPEN,       &AC_InitRemoveReady},
        {ST_WAITING_INTERVIEW_TO_OPEN, EV_CLOSE_NETWORK,                    ST_AUTOADD_IDLE,                    &AC_StopS2FMS},
};

#define TRANS_COUNT (sizeof(trans) / sizeof(*trans))

/* States can have a default timeout. If the FSM remains in the same
 * state_auto_remove_add for the specified duration, the specified callback function
 * will be called.
 * Format: {STATE, timeout in milliseconds, callback_function}
 * Only state_auto_remove_adds listed here have timeouts */

static state_auto_remove_add_timeout_t timeoutTable[] = 
{
    {ST_WAITING_ADD_FOUND_ACK,      30000,  ZCB_TimerAddReady}, //ST_WAITING_ADD_FOUND_ACK not ST_WAITING_ADD_READY_ACK
    {ST_WAITING_CLOSE_ADD,          2000,   ZCB_TimerCloseAddReady}, //ZCB_TimerCloseAddReadyWhenHaveOpenNetwork
    {ST_WAITING_OPEN_NETWORK,       2000,   ZCB_TimerWaitingOpenNetwork},
    {ST_WAITING_ADDING_ACK,         60000,  ZCB_TimerWaitingAdding},
    {ST_WAITING_ADD_PROTOCOL_ACK,   60000,  ZCB_TimerWaitingAdding},
    {ST_WAITING_INTERVIEW_TO_OPEN,  30000,   ZCB_TimerWaitingOpenNetwork},//30s for case interview done --> remove Ready
    //if 5s, libzwave deosn't receive command from zwave_handler_cli --> change state to remomve ready
    {ST_WAITING_REMOVE_TO_ADD,      3000,   ZCB_TimerWaitingChangeRemveToAdd}
};
#define TIMEOUT_TABLE_COUNT (sizeof(timeoutTable) / sizeof(*timeoutTable))

static char *GetStateTypeStr(uint8_t st)
{

    char *stStr;

    switch (st)
    {
    case ST_AUTOADD_IDLE:
        stStr = "ST_AUTOADD_IDLE";
        break;
    case ST_WAITING_REMOVE_READY_ACK:
        stStr = "ST_WAITING_REMOVE_READY_ACK";
        break;
    case ST_WAITING_REMOVE_FOUND_ACK:
        stStr = "ST_WAITING_REMOVE_FOUND_ACK";
        break;
    case ST_WAITING_REMOVING_ACK:
        stStr = "ST_WAITING_REMOVING_ACK";
        break;
    case ST_WAITING_REMOVE_DONE_ACK:
        stStr = "ST_WAITING_REMOVE_DONE_ACK";
        break;
    case ST_WAITING_REMOVE_FAILED_ACK:
        stStr = "ST_WAITING_REMOVE_FAILED_ACK"; //5
        break;
    case ST_WAITING_ADD_READY_ACK:
        stStr = "ST_WAITING_ADD_READY_ACK";
        break;
    case ST_WAITING_ADD_FOUND_ACK:
        stStr = "ST_WAITING_ADD_FOUND_ACK";
        break;
    case ST_WAITING_ADDING_ACK:
        stStr = "ST_WAITING_ADDING_ACK";
        break;
    case ST_WAITING_ADD_PROTOCOL_ACK:
        stStr = "ST_WAITING_ADD_PROTOCOL_ACK";
        break;
    case ST_WAITING_ADD_DONE_ACK:
        stStr = "ST_WAITING_ADD_DONE_ACK"; //10
        break;
    case ST_WAITING_ADD_FAIL_ACK:
        stStr = "ST_WAITING_ADD_FAIL_ACK";
        break;
    case ST_INTERVIEWING_DEVICE:
        stStr = "ST_INTERVIEWING_DEVICE";
        break;
    case ST_WAITING_OPEN_NETWORK:
        stStr = "ST_WAITING_OPEN_NETWORK";
        break;
    case ST_WAITING_REMOVE_TO_ADD:
        stStr = "ST_WAITING_REMOVE_TO_ADD";
        break;
    case ST_WAITING_CLOSE_ADD:
        stStr = "ST_WAITING_CLOSE_ADD"; //15
        break;
    case ST_WAITING_FINISH_REMOVE:
        stStr = "ST_WAITING_FINISH_REMOVE";
        break;
    case ST_WAITING_FINISH_ADD:
        stStr = "ST_WAITING_FINISH_ADD";
        break;
    case ST_WAITING_FINISH_INTERVIEW:
        stStr = "ST_WAITING_FINISH_INTERVIEW"; //10
        break;
    case ST_WAITING_INTERVIEW_TO_OPEN:
        stStr = "ST_WAITING_INTERVIEW_TO_OPEN"; //10
        break;
    case ST_WAITING_ADD_SECURE:
        stStr = "ST_WAITING_ADD_SECURE"; //10
        break;
    default:
        stStr = " ";
        break;
    }

    return stStr;
}

static char *GetEventTypeStr(uint8_t ev)
{

    char *evStr;

    switch (ev)
    {
    case EV_OPEN_NETWORK: //0
        evStr = "EV_OPEN_NETWORK";
        break;
    case EV_REMOVE_NODE_READY_OK:
        evStr = "EV_REMOVE_NODE_READY_OK";
        break;
    case EV_REMOVE_NODE_FOUND_OK:
        evStr = "EV_REMOVE_NODE_FOUND_OK";
        break;
    case EV_REMOVING_ACK:
        evStr = "EV_REMOVING_ACK";
        break;
    case EV_REMOVE_DONE_ACK:
        evStr = "EV_REMOVE_DONE_ACK";
        break;
    case EV_REMOVE_FAIL_ACK: //5
        evStr = "EV_REMOVE_FAIL_ACK";
        break;
    // case EV_REMOVING_NAK:
    //     evStr= "EV_REMOVING_NAK";
    //     break;
    case EV_ADD_READY_OK:
        evStr = "EV_ADD_READY_OK";
        break;
    case EV_ADD_FOUND_OK:
        evStr = "EV_ADD_FOUND_OK";
        break;
    case EV_ADD_DONE_ACK:
        evStr = "EV_ADD_DONE_ACK";
        break;
    case EV_ADD_FAIL_ACK: //10
        evStr = "EV_ADD_FAIL_ACK";
        break;
    case EV_ADD_PROTOCOL_OK:
        evStr = "EV_ADD_PROTOCOL_OK";
        break;
    case EV_ADDING_ACK:
        evStr = "EV_ADDING_ACK";
        break;
    // case EV_ADDING_NAK:
    //     evStr= "EV_ADDING_NAK";
    //     break;
    case EV_FINISH_INTERVIEW:
        evStr = "EV_FINISH_INTERVIEW";
        break;
    case EV_CLOSE_NETWORK: //15
        evStr = "EV_CLOSE_NETWORK";
        break;
    case EV_TIMEOUT_ADD_READY:
        evStr = "EV_TIMEOUT_ADD_READY";
        break;
    case EV_TIMEOUT_CHANGE_REMOVE_TO_ADD:
        evStr = "EV_TIMEOUT_CHANGE_REMOVE_TO_ADD";
        break;
    case EV_TIMEOUT_WAITING_CLOSE_ADD_READY:
        evStr = "EV_TIMEOUT_WAITING_CLOSE_ADD_READY";
        break;
    case EV_TIMEOUT_WAITING_OPEN_NETWORK:
        evStr = "EV_TIMEOUT_WAITING_OPEN_NETWORK";
        break;
    case NO_ACTION_FROM_HANDLER:
        evStr = "NO_ACTION_FROM_HANDLER";
        break;
    case EV_OPEN_NETWORK_AT_INTERVIEW:
        evStr = "EV_OPEN_NETWORK_AT_INTERVIEW";
        break;
    case EV_CLOSE_NETWORK_AT_INTERVIEW:
        evStr = "EV_CLOSE_NETWORK_AT_INTERVIEW";
        break;
    case EV_TIMEOUT_WAIT_INTERVIEW_TO_OPEN:
        evStr = "EV_TIMEOUT_WAIT_INTERVIEW_TO_OPEN";
        break;
    case EV_FINISH_ADD_SECURITY:
        evStr = "EV_FINISH_ADD_SECURITY";
        break;
    case EV_TIMEOUT_WAITING_ADDING:
        evStr = "EV_TIMEOUT_WAITING_ADDING";
        break;
    case EV_CLOSE_NWK_AT_ADDING:
        evStr = "EV_CLOSE_NWK_AT_ADDING";
        break;
    default:
        evStr = " ";
        break;
    }
    return evStr;
}

/*============================   StopTimer   ===============================
**
**  Cancels the FSM timer.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/
static void StopTimer()
{
    if (bFsmTimerHandle_AutoAdd != NO_TIMER)
    {
        timerCancel(&bFsmTimerHandle_AutoAdd);
        bFsmTimerHandle_AutoAdd = NO_TIMER;
    }
}

/*===========================   AutoAdd_ZCB_FsmTimer   ==============================
**
**  Timer callback function for the default FSM timer.
**  If a timeout and a callback function is specified in the
**  timeoutTable, ths callback function will be called automatically
**  by this timer function.
**
**  The callback function will only be called if no state_auto_remove_add change has occurred
**  within the specified timeout since entering the current state_auto_remove_add.
**
**  Side effects: None
**
**  If Timeout, this function is executed
**-------------------------------------------------------------------------*/
void AutoAdd_ZCB_FsmTimer(void *data)
{
    mainlog(logDebug, "FsmTimer-bFsmTimerCountdown_AutoAdd: %u", bFsmTimerCountdown_AutoAdd);
    if (bFsmTimerCountdown_AutoAdd > 0)
    {
        bFsmTimerCountdown_AutoAdd--;
        return;
    }
    /* The timer repeat count should already have stopped the timer,
    * but better safe than sorry. */
    StopTimer();
    if (cbFsmTimer_AutoAdd != NULL) //pointer function
    {
        cbFsmTimer_AutoAdd(data); //function
    }
}

/*============================   StartTimer   ===============================
**
**  Start the FSM timer. Note the higher resolution if timeout is
**  shorter than or equal to 2.55 seconds.
**
**  Side effects: Cancels running FSM timer if any.
**
**-------------------------------------------------------------------------*/
static void StartTimer(
    uint32_t timeout,       /* IN Unit: 10 ms ticks. If over 255, resolution is 1 second */
    void (*cbFunc)(void *)) /* IN timeout callback function */
{
    StopTimer();
    int ret = 0;
    if (timeout > 255)
    {
        /* Timeout larger than single timeout. */
        /* Convert timeout from 10 ms ticks to seconds*/
        bFsmTimerCountdown_AutoAdd = timeout / 1000;
        cbFsmTimer_AutoAdd = cbFunc;
        ret = timerStart(&bFsmTimerHandle_AutoAdd, AutoAdd_ZCB_FsmTimer, NULL, 1000, TIMER_FOREVER);
    }
    else
    {
        /* timeout is in range 10..2550 ms */
        ret = timerStart(&bFsmTimerHandle_AutoAdd, cbFunc, NULL, timeout * 10, TIMER_ONETIME);
    }

    if (ret)
    {
        mainlog(logDebug, "Failed to get timer for FsmTimer +++++");
    }
}

/*=========================   auto_add_paraStartStateTimer   ============================
**
**  Find and start the state_auto_remove_add timer for current state_auto_remove_add, if one has been
**  defined in timeoutTable.
**
**  Side effects: Cancels the currently running state_auto_remove_add timer.
**
**-------------------------------------------------------------------------*/
static void StartStateTimer()
{
    uint8_t i;
    for (i = 0; i < TIMEOUT_TABLE_COUNT; i++)
    {
        if (state_auto_remove_add == timeoutTable[i].st)
        {
            StartTimer(timeoutTable[i].timeout, timeoutTable[i].tfn);
            mainlog(logDebug, "StartStateTimer: index %d", i);
            if (timeoutTable[i].tfn == NULL)
            {
                mainlog(logDebug, "StartStateTimer:NULL function");
            }
        }
    }
}

void ChangeTimeoutOfState(uint8_t state, uint32_t newTimeout)
{
    uint8_t i;
    for (i = 0; i < TIMEOUT_TABLE_COUNT; i++)
    {
        if (state == timeoutTable[i].st)
        {
            timeoutTable[i].timeout = newTimeout;
        }
    }
}

/*=========================   InitAutoAddFsm   ============================
**
**  Initialized the finite state_auto_remove_add machine.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/
void InitAutoAddFsm()
{
    StopTimer();
}

/*============================   InterviewDevice_ZCB_PostEvent   ===============================
**
**  Post an event to the finite state_auto_remove_add machine. Update the state_auto_remove_add,
**  run action function associated with the transition and start
**  the state_auto_remove_add timer for the next state_auto_remove_add.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/
void PostEventAutoRemoveAdd(AUTOREMOVEADD_FSM_EV_T event)
{
    AUTOREMOVEADD_FSM_STATES_T old_state_auto_remove_add;
    uint8_t i;
    for (i = 0; i < TRANS_COUNT; i++)
    {
        if ((state_auto_remove_add == trans[i].st)) //have state_auto_remove_add none support
        {
            if ((event == trans[i].ev))
            {
                old_state_auto_remove_add = trans[i].st;

                state_auto_remove_add = trans[i].next_st;

                mainlog(logDebug, "----AutoAdd_ZCB_PostEvent: %s [%u]-----", GetEventTypeStr(event), event);
                mainlog(logDebug, "----%s [%u] ---> %s [%u]---------------", GetStateTypeStr(old_state_auto_remove_add), old_state_auto_remove_add, GetStateTypeStr(state_auto_remove_add), state_auto_remove_add);
                if (old_state_auto_remove_add != state_auto_remove_add)
                {
                    StopTimer();
                    StartStateTimer();
                }
                if ((trans[i].fn) != NULL)
                {
                    (trans[i].fn(&auto_add_para));
                }
                break;
            }
        }
    }
}

/*=======================   GetInterviewDeviceFsmState   ==========================
**
**  Return the current state_auto_remove_add of the state_auto_remove_add machine.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/
AUTOREMOVEADD_FSM_STATES_T GetAutoAddFsmState(void)
{
    return state_auto_remove_add;
}