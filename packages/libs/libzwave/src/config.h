#ifndef _CONFIG_H_
#define _CONFIG_H_
#include "zw_api.h"


#define ZWAVE_CONSOLE_PORT_DEFAULT "/dev/ttyO2"


#define ADD_REMOVE_TIMEOUT                  (60000) /*60 seconds */
#define WAITING_NEWNODE_TIMEOUT             (2000)  /*2 seconds */
#define TIMEOUT_NODEINFORMATION             (8000)  /*8 seconds */



#define NMS_FLAG_S2_ADD                     0x01
#define NMS_FLAG_PROXY_INCLUSION            0x02
#define NMS_FLAG_LEARNMODE_NEW              0x04
#define NMS_FLAG_LEARNMODE_NWI              0x08
#define NMS_FLAG_LEARNMODE_NWE              0x10
#define NMS_FLAG_CONTROLLER_REPLICATION     0x20
#define NMS_FLAG_SMART_START_INCLUSION      0x40
#define NMS_FLAG_REPORT_DSK                 0x80      /* We should include a DSK in NODE_ADD_STATUS */
#define NMS_FLAG_CSA_INCLUSION              0x100


typedef enum 
{
    NMS_IDLE,
    NMS_WAITING_FOR_ADD,
    NMS_WAITING_FOR_REMOVE,
    NMS_NODE_FOUND,
    NMS_WAIT_FOR_PROTOCOL,
    NMS_NETWORK_UPDATE,
    NMS_WAITING_FOR_INTERVIEW,
    NMS_SET_DEFAULT,
    NMS_LEARN_MODE,
    NMS_LEARN_MODE_STARTED,
    NMS_WAIT_FOR_SECURE_ADD,
    NMS_SENDING_NODE_INFO,
    NMS_WAITING_FOR_NODE_REMOVAL,
    NMS_WAITING_FOR_FAIL_NODE_REMOVAL,
    NMS_WAITING_FOR_NODE_NEIGH_UPDATE,
    NMS_WAITING_FOR_RETURN_ROUTE_ASSIGN,
    NMS_WAITING_FOR_RETURN_ROUTE_DELETE,

    NMS_WAIT_FOR_INTERVIEW_AFTER_ADD,
    NMS_WAIT_FOR_SECURE_LEARN,
    NMS_WAIT_FOR_INTERVIEW_BY_SIS,
    NMS_REMOVING_ASSOCIATIONS,

    NMS_REPLACE_FAILED_REQ,
    NMS_PREPARE_SUC_INCLUSION,
    NMS_WAIT_FOR_SUC_INCLUSION,
    NMS_PROXY_INCLUSION_WAIT_NIF,
    NMS_INCLUSION_CONTROLLER,//intro Auto remove add have inclusion controller
    NMS_WAIT_FOR_SUC_NIF,
    NMS_WAIT_FOR_SUC_ADD_NODE,
} nm_state_t;

typedef enum 
{
    NMM_ADD_NODE,
    NMM_REMOVE_NODE,
    NMM_AUTO_REMOVEADD_NODE,
    NMM_REPLACE_FAILED_NODE,
    NMM_CONTROLLER_CHANGE,
//  NMM_INCLUSION_CONTROLLER,
    NMM_UPDATE_FIRMWARE_DEVICE,
    NMM_DEFAULT,

} nm_mode_t;


typedef struct 
{
    nm_state_t  state;
    nm_mode_t   mode;
    int         flags;
    uint8_t     tmp_node;
    timer_t     nm_timer;
    uint8_t     just_included_dsk[16];
}nm_t;


/****************************************************************************
 *
 * Defines used to initialize the Manufacturer Specific Command Class.
 *
 ****************************************************************************/


#define APP_FIRMWARE_ID                     APP_PRODUCT_ID | (APP_PRODUCT_TYPE_ID << 8)

#define APP_DEVICE_ID_TYPE                  DEVICE_ID_TYPE_SERIAL_NBR
#define APP_DEVICE_ID_FORMAT                DEVICE_ID_FORMAT_BIN

#define APP_MANUFACTURER_ID_ZINNO_INC_H     0x03
#define APP_MANUFACTURER_ID_ZINNO_INC_L     0x6E

#define APP_MANUFACTURER_ID_H               APP_MANUFACTURER_ID_ZINNO_INC_H
#define APP_MANUFACTURER_ID_L               APP_MANUFACTURER_ID_ZINNO_INC_L
#define APP_PRODUCT_TYPE_ID_H               0x00
#define APP_PRODUCT_TYPE_ID_L               0x01
#define APP_PRODUCT_ID_H                    0x00
#define APP_PRODUCT_ID_L                    0x01

#define APP_NETWORK_CAPABILITY              0xD3
#define APP_NETWORK_SECURITY                0x16
#define APP_NETWORK_RESERVED                0x00
#define APP_ZWAVE_PLUS_VERSION              0x01

#define APP_MANUFACTURER_ID                 (APP_MANUFACTURER_ID_L | (APP_MANUFACTURER_ID_H << 8)) /*0x0302*/

#define APP_PRODUCT_TYPE_ID                 (APP_PRODUCT_TYPE_ID_L | (APP_PRODUCT_TYPE_ID_H << 8)) /*0x0001*/
#define APP_PRODUCT_ID                      (APP_PRODUCT_ID_L     | (APP_PRODUCT_ID_H << 8 ))      /*0x0001*/

#define DEFAULT_WAKE_UP_INTERVAL            0x5460 /* 6*3600 = 6 hours */

#define PROTOCOL_SDK_LIB_VERSION            6
#define PROTOCOL_SDK_LIB_SUB_VERSION        81



/****************************************************************************
 *
 * Defines used to initialize the Association Group Information (AGI) 
 * Command Class.
 *
 ****************************************************************************/


#define APP_VERSION                             2
#define APP_REVISION                            01

#define NUMBER_OF_TARGET_FIRMWARE               2      /*firware on Linux +firmware on Z-Wave Controller */  

#define APP_NUMBER_OF_ENDPOINTS                 0 
#define MAX_ASSOCIATION_GROUPS                  4
#define MAX_ASSOCIATION_IN_GROUP                5

#define AGITABLE_LIFELINE_GROUP \
 {COMMAND_CLASS_DEVICE_RESET_LOCALLY, DEVICE_RESET_LOCALLY_NOTIFICATION}

#define  AGITABLE_ROOTDEVICE_GROUPS NULL


#define APPL_MAGIC_VALUE                        0x56

#define ZWAVE_PLUS_TX_OPTIONS \
 (TRANSMIT_OPTION_ACK | TRANSMIT_OPTION_AUTO_ROUTE | TRANSMIT_OPTION_EXPLORE)

 typedef enum {
    SECONDARY,
    SUC,
    SLAVE
} controller_role_t;

#define MY_NODE_CONFIG_DEFAULT                                                                  \
{                                                                                               \
    .node_id = 1,                                                                               \
    .node_type = GENERIC_TYPE_STATIC_CONTROLLER,                                                \
    .node_info = {.mode = MODE_ALWAYSLISTENING,                                                 \
                .nodeType = {.basic = BASIC_TYPE_STATIC_CONTROLLER,                             \
                            .generic = GENERIC_TYPE_STATIC_CONTROLLER,                          \
                            .specific = SPECIFIC_TYPE_PC_CONTROLLER}},                          \
    .node_capability = {                .noCapability = 10,                                     \
                                        .aCapability = {COMMAND_CLASS_ZWAVEPLUS_INFO,           \
                                                        COMMAND_CLASS_INCLUSION_CONTROLLER,     \
                                                        COMMAND_CLASS_CRC_16_ENCAP,             \
                                                        COMMAND_CLASS_TRANSPORT_SERVICE_V2,     \
                                                        COMMAND_CLASS_SECURITY,                 \
                                                        COMMAND_CLASS_SECURITY_2,               \
                                                        COMMAND_CLASS_MANUFACTURER_SPECIFIC,    \
                                                        COMMAND_CLASS_POWERLEVEL,               \
                                                        COMMAND_CLASS_SUPERVISION,              \
                                                        COMMAND_CLASS_VERSION}},                \
    .node_manufacture = {.manufacturerID = {APP_MANUFACTURER_ID_H, APP_MANUFACTURER_ID_L},      \
                         .productTypeID = {APP_PRODUCT_TYPE_ID_H, APP_PRODUCT_TYPE_ID_L},       \
                         .productID = {APP_PRODUCT_ID_H, APP_PRODUCT_ID_L}},                    \
    .node_secure_scheme = SECURITY_SCHEME_2_ACCESS,                                             \
    .node_capability_secureV0 = {       .noCapability = 6,                                      \
                                        .aCapability = {COMMAND_CLASS_ASSOCIATION,              \
                                                        COMMAND_CLASS_ASSOCIATION_GRP_INFO,     \
                                                        COMMAND_CLASS_DEVICE_RESET_LOCALLY,     \
                                                        COMMAND_CLASS_MANUFACTURER_SPECIFIC,    \
                                                        COMMAND_CLASS_VERSION,                  \
                                                        COMMAND_CLASS_MARK}},                   \
    .node_capability_secureV2 = {       .noCapability = 6,                                      \
                                        .aCapability = {COMMAND_CLASS_ASSOCIATION,              \
                                                        COMMAND_CLASS_ASSOCIATION_GRP_INFO,     \
                                                        COMMAND_CLASS_DEVICE_RESET_LOCALLY,     \
                                                        COMMAND_CLASS_MANUFACTURER_SPECIFIC,    \
                                                        COMMAND_CLASS_VERSION,                  \
                                                        COMMAND_CLASS_MARK}},                   \
    .no_endpoint = APP_NUMBER_OF_ENDPOINTS,                                                     \
    .is_zp_node = true,                                                                         \
    .zp_info = {.zwave_plus_version = APP_ZWAVE_PLUS_VERSION,                                   \
                .role_type = ROLE_TYPE_CONTROLLER_CENTRAL_STATIC,                               \
                .node_type = ZWAVEPLUS_INFO_REPORT_NODE_TYPE_ZWAVEPLUS_NODE,                    \
                .installer_icon_type = ICON_TYPE_GENERIC_CENTRAL_CONTROLLER,                    \
                .user_icon_type = ICON_TYPE_GENERIC_CENTRAL_CONTROLLER},                        \
}

#define MY_NODE_SECURE_NIF                                                                      \
{                                                                                               \
                                        .noCapability = 8,                                      \
                                        .aCapability = {COMMAND_CLASS_ZWAVEPLUS_INFO,           \
                                                        COMMAND_CLASS_INCLUSION_CONTROLLER,     \
                                                        COMMAND_CLASS_CRC_16_ENCAP,             \
                                                        COMMAND_CLASS_TRANSPORT_SERVICE_V2,     \
                                                        COMMAND_CLASS_SECURITY,                 \
                                                        COMMAND_CLASS_SECURITY_2,               \
                                                        COMMAND_CLASS_POWERLEVEL,               \
                                                        COMMAND_CLASS_SUPERVISION}              \
}

#define MY_NODE_NON_SECURE_NIF                                                                  \
{                                                                                               \
                                        .noCapability = 10,                                     \
                                        .aCapability = {COMMAND_CLASS_ZWAVEPLUS_INFO,           \
                                                        COMMAND_CLASS_INCLUSION_CONTROLLER,     \
                                                        COMMAND_CLASS_CRC_16_ENCAP,             \
                                                        COMMAND_CLASS_TRANSPORT_SERVICE_V2,     \
                                                        COMMAND_CLASS_SECURITY,                 \
                                                        COMMAND_CLASS_SECURITY_2,               \
                                                        COMMAND_CLASS_MANUFACTURER_SPECIFIC,    \
                                                        COMMAND_CLASS_POWERLEVEL,               \
                                                        COMMAND_CLASS_SUPERVISION,              \
                                                        COMMAND_CLASS_VERSION}                  \
}               


extern ZW_Node_t            myZPCCapability;
extern NODE_CAPABILITY      nodeCapabilitySecureIncluded;
extern NODE_CAPABILITY      nodeCapabilityNonSecureIncluded;
extern controller_role_t    controller_role;
extern ZW_Node_t            lastRemovedNode;
extern ZW_Node_t            lastLearnedNodeZPC;
extern nm_t                 nm;



#endif /* _CONFIG_APP_H_ */

