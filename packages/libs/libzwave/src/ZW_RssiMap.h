#ifndef RSSIMAP_H
#define RSSIMAP_H

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

/*============================== RssiMapStart =============================
** Function description
**     Start the RSSI map functionality. Begin measuring and displaying an
**     RSSI map of the entire network.
**
**     Returns false if an rssi map operation is already ongoing.
**     
**--------------------------------------------------------------------------*/
bool RssiMapStart(void);


#endif