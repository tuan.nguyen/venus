/****************************************************************************
 *
 *
 * Copyright (c) 2014-2015
 * Veriksytems, Inc.
 * All Rights Reserved
 *
 *
****************************************************************************/
#include <pthread.h>
#include <math.h>
#include "zw_api.h"
#include "serial.h"

#include "ZW_NetworkManagement.h"
#include "zw_handle_notify_device.h"
#include "nvm_utils.h"

#include "supported_CommandClassAssociationGroupInfo.h"
#include "supported_CommandClassVersion.h"
#include "supported_CommandClassDeviceResetLocally.h"
#include "supported_CommandClassManufacturerSpecific.h"
#include "supported_CommandClassZWavePlusInfo.h"
#include "supported_CommandClassPowerLevel.h"
#include "supported_CommandClassInclusionController.h"
#include "supported_CommandClassSupervision.h"
#include "controlled_CommandClassUpdateFirmware.h"
#include "Transport_ApplCmdHandlerEx.h"

#include "ZW_AddingInterviewDevice_FSM.h"
#include "ZW_AutoRemoveAdd_FSM.h"
#include "resource_directory.h"
#include "nvm.h"
#include "eeprom.h"
#include "transport_service2_external.h"
#include "node_cache.h"
#include "s2_keystore.h"//get key
#include "secure_key_utils.h"
#include "ZW_Programmer.h"
#include "S0.h"
#include "ZW_PollApi.h"
#include "ZW_DevicePoll.h"
#include "ZW_Poll.h"

#include "ZW_RssiMap.h"

nm_t nm = {0};
zwnet_p zwaveNetworkPolling;

CMD_CLASS_GRP agiTableLifeLine[] = {AGITABLE_LIFELINE_GROUP};

/*node capability of Venus */
NODE_CAPABILITY nodeCapabilitySecureIncluded = MY_NODE_SECURE_NIF;
NODE_CAPABILITY nodeCapabilityNonSecureIncluded = MY_NODE_NON_SECURE_NIF;
ZW_Node_t myZPCCapability = MY_NODE_CONFIG_DEFAULT;


/*save message from app when device sleep */
cmd_req_element_t   cmdReqInQueue[COMMAND_SPECIFIC_MAX];
uint8_t             cmdReqInQueueIndex = 0;
cmd_req_element_t   lastCmdReqInQueue;

/*variable for handle wake up notifycation and device reset locally*/
scheduling_tast_t   handleAppCmdInQueue[COMMAND_SCHEDULING_MAX];
uint8_t             handleAppCmdInQueueIndex = 0;

/*variable and function for handle notify*/
pthread_t           handleNotifyThreadProcID; 
notify_queue_t      *g_notify_buf;

bool                bWaitForHandleDeviceResetLocally;
uint32_t            homeId;
uint8_t             MyNodeId;
uint8_t             MySystemSUCId;

uint8_t             lastRequestStatus = 0xFF;
uint8_t             lastRemoveNodeForceStatus = 0xFF;

ZW_Node_t           lastRemovedNode;
ZW_Node_t           lastLearnedNodeZPC;

sTiming             txTiming;
pthread_mutex_t     CriticalMutexNotification = PTHREAD_MUTEX_INITIALIZER;

void ActionWhenInterviewDeviceFailed(void);
void RequestNetworkUpdate_Compl(uint8_t bStatus);
int getControllerInfo(uint32_t *myHomeId, uint8_t *myCapability, ZW_Node_t *zpc_node);
void ApplicationControllerUpdate(uint8_t bStatus, uint8_t bSource, uint8_t *pCmd, uint8_t bLen, uint8_t *prospectHomeID);

typedef struct update_node_id_notify
{
    timer_t         waiting_timer;
    ZW_Node_t       zpc_node;
}UpdateNodeId_t;


const struct SerialAPI_Callbacks serialApiCallbacks =
{   ApplicationCommandHandler_Compl, 
    NULL,
    ApplicationControllerUpdate, 
    0, 
    NULL, 
    NULL, 
    NULL,
    NULL, 
    NULL, //SerialAPIStarted -> NetworkManagement_smart_start_init_if_pending
};

void resetNMTimer(void)
{
    if (nm.nm_timer)
    {
        timerCancel(&(nm.nm_timer));
        nm.nm_timer = 0;
    }
}


/*=============Push Notification To Handler=================================*/
void PushNotificationToHandler(uint8_t bStatus, uint8_t *pData, uint32_t len)
{
    pthread_mutex_lock(&CriticalMutexNotification);

    g_notify_buf->notify[g_notify_buf->notify_index].notify_status = bStatus;
    memcpy(g_notify_buf->notify[g_notify_buf->notify_index].notify_message, pData, len);
    g_notify_buf->notify_index = g_notify_buf->notify_index + 1;

    pthread_mutex_unlock(&CriticalMutexNotification);
}

uint8_t handleNbrFirmwareVersions(void)
{
    return NUMBER_OF_TARGET_FIRMWARE; /*CHANGE THIS - firmware 0 version*/
}

/*============================ handleGetFirmwareVersion ====================
** Function description
** Read application firmware version informations
**
**-------------------------------------------------------------------------*/
void handleGetFirmwareVersion(uint8_t bFirmwareNumber, VG_VERSION_REPORT_V2_VG *pVariantgroup)
{
    /*firmware 0 version and sub version*/
    if (bFirmwareNumber == 0)
    {
        pVariantgroup->firmwareVersion      = LASTEST_FIRMWARE_0_VERSION;
        pVariantgroup->firmwareSubVersion   = LASTEST_FIRMWARE_0_SUB_VERSION;
    }
    else if (bFirmwareNumber == 1)
    {
        /*Just set it to 0 if firmware n is not present*/
        pVariantgroup->firmwareVersion      = APP_VERSION;
        pVariantgroup->firmwareSubVersion   = APP_REVISION;
    }
    else
    {
        /*Just set it to 0 if firmware n is not present*/
        pVariantgroup->firmwareVersion      = 0x00;
        pVariantgroup->firmwareSubVersion   = 0;
    }
}

/*========================================================================================================
======================================= handle CommandClassVersionAppl ===================================
==========================================================================================================*/
uint8_t handleCommandClassVersionAppl(uint8_t cmdClass)
{
    uint8_t commandClassVersion = UNKNOWN_VERSION;

    switch (cmdClass)
    {
    case COMMAND_CLASS_VERSION:
        commandClassVersion = CommandClassVersionVersionGet();
        break;

    case COMMAND_CLASS_POWERLEVEL:
        commandClassVersion = CommandClassPowerLevelVersionGet();
        break;

    case COMMAND_CLASS_MANUFACTURER_SPECIFIC:
        commandClassVersion = CommandClassManufacturerVersionGet();
        break;

    case COMMAND_CLASS_ASSOCIATION:
        commandClassVersion = CommandClassAssociactionVersionGet();
        break;

    case COMMAND_CLASS_ASSOCIATION_GRP_INFO:
        commandClassVersion = CommandClassAssociationGroupInfoVersionGet();
        break;

    case COMMAND_CLASS_DEVICE_RESET_LOCALLY:
        commandClassVersion = CommandClassDeviceResetLocallyVersionGet();
        break;

    case COMMAND_CLASS_ZWAVEPLUS_INFO:
        commandClassVersion = CommandClassZWavePlusInfoVersionGet();
        break;

    case COMMAND_CLASS_MARK:
        commandClassVersion = MARK_VERSION;
        break;

    case COMMAND_CLASS_BASIC:
        commandClassVersion = BASIC_VERSION;
        break;

    case COMMAND_CLASS_CRC_16_ENCAP:
        commandClassVersion = CRC_16_ENCAP_VERSION;
        break;

    case COMMAND_CLASS_SECURITY:
        commandClassVersion = SECURITY_VERSION;
        break;

    case COMMAND_CLASS_SECURITY_2:
        commandClassVersion = SECURITY_2_VERSION;
        break;

    case COMMAND_CLASS_TRANSPORT_SERVICE:
        commandClassVersion = TRANSPORT_SERVICE_VERSION_V2;
        break;

    case COMMAND_CLASS_SUPERVISION:
        commandClassVersion = SUPERVISION_VERSION;
        break;

    case COMMAND_CLASS_INCLUSION_CONTROLLER:
        commandClassVersion = INCLUSION_CONTROLLER_VERSION;
        break;

    default:
        commandClassVersion = UNKNOWN_VERSION;
    }
    return commandClassVersion;
}

/*========================   TransmitNodeInfoComplete   ======================
**    Function description
**      Callbackfunction called when the nodeinformation frame has
**      been transmitted. This function ensures that the Transmit Queue is not
**      flooded.
**    Side effects:
**
**--------------------------------------------------------------------------*/
void ZCB_TransmitMyNodeInfoComplete(uint8_t bTXStatus)
{

}

void NmTimeout(void *data)
{
    static uint8_t schemeGet;

    switch (nm.mode)
    {
        case NMM_REPLACE_FAILED_NODE:
        {
            switch (nm.state)
            {
                case NMS_REPLACE_FAILED_REQ:
                {
                    /*close replace failed node when time out */
                    nm.mode = NMM_DEFAULT;
                    serialApiAddNodeToNetwork(REMOVE_NODE_STOP, NULL);
                    NOTIFY_TX_BUFFER_T pTxNotify;
                    pTxNotify.AddNodeZPCNotify.bStatus = ADD_REMOVE_NODE_TIMEOUT;
                    PushNotificationToHandler(REPLACE_FAILED_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
                    return;
                }
                break;

                case  NMS_PROXY_INCLUSION_WAIT_NIF:
                {
                    /*close replace failed node when time out */
                    nm.mode = NMM_DEFAULT;
                    serialApiAddNodeToNetwork(REMOVE_NODE_STOP, NULL);
                    NOTIFY_TX_BUFFER_T pTxNotify;
                    pTxNotify.AddNodeZPCNotify.bStatus = ADD_REMOVE_NODE_TIMEOUT;
                    PushNotificationToHandler(REPLACE_FAILED_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
                    return;
                }
                break;

                case NMS_WAIT_FOR_SUC_NIF:
                    goto continue_secure_adding;
                break;

                case NMS_WAIT_FOR_SUC_ADD_NODE:
                    goto request_suc_nif;
                break;

                default:
                return;
            }
        }
        break;

        case NMM_CONTROLLER_CHANGE:
        {
            NOTIFY_TX_BUFFER_T pTxNotify;
            pTxNotify.AddNodeZPCNotify.bStatus = ADD_REMOVE_NODE_TIMEOUT;
            PushNotificationToHandler(CONTROLLER_CHANGE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
            serialApiControllerChange(CONTROLLER_CHANGE_STOP, NULL);
            return;
        }
        break;

        case NMM_ADD_NODE:
        {
            switch (nm.state)
            {
                case NMS_WAIT_FOR_SUC_NIF:
                    goto continue_secure_adding;
                break;

                case NMS_WAIT_FOR_SUC_ADD_NODE:
                    goto request_suc_nif;
                break;

                default:
                {
                    NOTIFY_TX_BUFFER_T pTxNotify;
                    pTxNotify.AddNodeZPCNotify.bStatus = ADD_REMOVE_NODE_TIMEOUT;

                    PushNotificationToHandler(ADD_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
                    serialApiAddNodeToNetwork(ADD_NODE_STOP, NULL);
                    return;
                }
                break; 
            }
        }   
        break;

        case NMM_AUTO_REMOVEADD_NODE:
        {
            switch (nm.state)
            {
                case NMS_WAIT_FOR_SUC_NIF:
                    goto continue_secure_adding;
                break;

                case NMS_WAIT_FOR_SUC_ADD_NODE:
                    goto request_suc_nif;
                break;

                default:
                    return; 
            }
        }
        break;

        case NMM_REMOVE_NODE:
        {
            NOTIFY_TX_BUFFER_T pTxNotify;
            pTxNotify.RemoveNodeZPCNotify.bStatus = ADD_REMOVE_NODE_TIMEOUT;
            PushNotificationToHandler(REMOVE_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.RemoveNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
            serialApiAddNodeToNetwork(REMOVE_NODE_STOP, NULL);
            return;
        }   
        break;


        default:
        return;
    }


request_suc_nif:
    nm.state = NMS_WAIT_FOR_SUC_NIF;//inclusion controller is 1 state of auto remove add
    nm.tmp_node = MySystemSUCId;
    serialApiRequestNodeInfo(nm.tmp_node,RequestNetworkUpdate_Compl);
    resetNMTimer();
    timerStart(&(nm.nm_timer), NmTimeout, 0, REQUEST_NIF_TIMEOUT, TIMER_ONETIME);//waiting NIF 5s
    return;

continue_secure_adding:
    mainlog(logDebug,"nmm timeout ->continue_secure_adding");
    nm.state = NMS_WAIT_FOR_SECURE_ADD;

    if (((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC, COMMAND_CLASS_SECURITY_2)) != SCHEME_NOT_SUPPORT) &&
        (GetCacheEntryFlag(MyNodeId) & NODE_FLAGS_SECURITY2))
    {
        securityS2InclusionStart(lastLearnedNodeZPC.node_id, SecurityInclusionDone_Compl);
        return;
    }

    if (((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC, COMMAND_CLASS_SECURITY)) != SCHEME_NOT_SUPPORT) &&
        (GetCacheEntryFlag(MyNodeId) & NODE_FLAG_SECURITY0))
    {
        if ((lastLearnedNodeZPC.node_info.nodeType.basic == BASIC_TYPE_CONTROLLER) ||
            (lastLearnedNodeZPC.node_info.nodeType.basic == BASIC_TYPE_STATIC_CONTROLLER))
        {
            securityS0InclusionStart(lastLearnedNodeZPC.node_id, true, SecurityInclusionDone_Compl);
        }
        else
        {
            securityS0InclusionStart(lastLearnedNodeZPC.node_id, false, SecurityInclusionDone_Compl);
        }
        return;
    }

    /* This is a non secure node */
    nm.state = NMS_WAITING_FOR_INTERVIEW;
    lastLearnedNodeZPC.node_secure_scheme = NO_SCHEME;
    PostEventAutoRemoveAdd(EV_FINISH_ADD_SECURITY);/*change ST_WAITING_ADD_SECURE --> ST_WAITING_FINISH_INTERVIEW*/    
    PostEventInterviewDevice(EV_ADD_NODE_OK);
    return;
}

/*when timeout and nm.mode == update network, push notify*/
void NmTimeoutPushNotifyNodeInclude(void *data)
{
    UpdateNodeId_t *lastUpdateNodeZPC = (UpdateNodeId_t*)data;
    if(!lastUpdateNodeZPC)
    {
        mainlog (logDebug,"lastUpdateNodeZPC is null !!!");
        return;
    }

    timerCancel(&(lastUpdateNodeZPC->waiting_timer));

    mainlog (logDebug,"NmTimeoutPushNotifyNodeInclude -> lastUpdateNodeZPC id: %d, lastLearnedNodeZPC id: %d", 
                        lastUpdateNodeZPC->zpc_node.node_id, lastLearnedNodeZPC.node_id);

    if(lastUpdateNodeZPC->zpc_node.node_id != lastLearnedNodeZPC.node_id)
    {
        NOTIFY_TX_BUFFER_T pTxNotify;
        pTxNotify.AddNodeZPCNotify.bStatus = ADD_NODE_STATUS_DONE;
        memcpy((uint8_t *)&pTxNotify.AddNodeZPCNotify.zpc_node, (uint8_t *)&lastUpdateNodeZPC->zpc_node, sizeof(ZW_Node_t));
        serialApiGetControllerCapabilities(&pTxNotify.AddNodeZPCNotify.my_capability);
        pTxNotify.AddNodeZPCNotify.zpc_node.node_secure_scheme = lastUpdateNodeZPC->zpc_node.node_secure_scheme;
        pTxNotify.AddNodeZPCNotify.zpc_node.node_info.mode = GetCacheEntryNodeMode(lastUpdateNodeZPC->zpc_node.node_id);
        PushNotificationToHandler(ADD_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));       
    }
    free(lastUpdateNodeZPC);

}
/*========================= SecurityInclusionDone_Compl ======================
 ** Function description
 **      Callback when Security Inclusion process done
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/

void SecurityInclusionDone_Compl(uint8_t bStatus, uint8_t bSource, uint8_t *pCmd, uint8_t bLen)
{

    /* check network management mode */
    mainlog (logDebug,"SecurityInclusionDone_Compl -> nm mode: %s, status: %02X", GetNMModeStr(nm.mode), bStatus);

    switch (bStatus)
    {

    case ADD_SECURE_NODE_STATUS_DONE:
    {
        mainlog(logDebug, "ZPCSecurity: [ADD_SECURE_S0_NODE_STATUS_DONE(%02X)] - Waiting ... (Nodeinformation length %u)", bStatus, bLen);
        lastLearnedNodeZPC.node_capability_secureV0.noCapability = bLen;
        memcpy((char *)&lastLearnedNodeZPC.node_capability_secureV0.aCapability, pCmd, bLen);
        lastLearnedNodeZPC.node_secure_scheme = SECURITY_SCHEME_0;
    }
    break;

    case ADD_SECURE_NODE_STATUS_FAILED:
    {
        mainlog(logDebug, "ZPCSecurity: [[ADD_SECURE_S0_NODE_STATUS_FAILED(%02X)] - Waiting ... (Nodeinformation length %u)", bStatus, bLen);
        lastLearnedNodeZPC.node_capability_secureV0.noCapability = 0;
        lastLearnedNodeZPC.node_secure_scheme = NO_SCHEME;
        lastLearnedNodeZPC.node_flags = NODE_FLAG_KNOWN_BAD;
    }
    break;

    case ADD_SECURE_2_NODE_STATUS_DONE:
    {
        mainlog(logDebug, "ZPCSecurity: [ADD_SECURE_2_NODE_STATUS_DONE(%02X)] - Waiting ... (Nodeinformation length %u)", bStatus, bLen);
        lastLearnedNodeZPC.node_capability_secureV2.noCapability = bLen;
        memcpy((char *)&lastLearnedNodeZPC.node_capability_secureV2.aCapability, pCmd, bLen);
    }
    break;

    case ADD_SECURE_2_NODE_STATUS_FAILED:
    {
        mainlog(logDebug, "ZPCSecurity: [ADD_SECURE_2_NODE_STATUS_FAILED(%02X)] - Waiting ... (Nodeinformation length %u)", bStatus, bLen);
        lastLearnedNodeZPC.node_capability_secureV2.noCapability = 0;
        lastLearnedNodeZPC.node_secure_scheme = NO_SCHEME;
        lastLearnedNodeZPC.node_flags = NODE_FLAG_KNOWN_BAD;        
    }
    break;

    default:
        break;
    }
    nm.state = NMS_WAITING_FOR_INTERVIEW;
    //if (NMM_AUTO_REMOVEADD_NODE == nm.mode)         
    PostEventAutoRemoveAdd(EV_FINISH_ADD_SECURITY);
    PostEventInterviewDevice(EV_ADD_NODE_OK);
}

/*============================ ZCB_CommandClassSupervisionGetReceived ======================
 * Function description 
        Handles a received Supervision Get command.
 * Details The purpose of Supervision is to inform the source node (controller) when the 
 * operation is finished. 
 */
void ZCB_CommandClassSupervisionGetReceived(SUPERVISION_GET_RECEIVED_HANDLER_ARGS *pArgs)
{
    static uint8_t sessionID;
    mainlog(logDebug, "cb SupervisionGetReceived, status %02X", pArgs->status);
    uint8_t tempStatus = pArgs->status;
    sessionID = CC_SUPERVISION_EXTRACT_SESSION_ID(pArgs->properties1);

    /* Status for all other commands */
    if (0) /* for Command Classes with duration >0 */
    {
        pArgs->properties1 = CC_SUPERVISION_ADD_MORE_STATUS_UPDATE(CC_SUPERVISION_MORE_STATUS_UPDATES_THIS_IS_LAST) | CC_SUPERVISION_ADD_SESSION_ID(sessionID);
        pArgs->status = CC_SUPERVISION_STATUS_WORKING;
        pArgs->duration = 0;
    }
    else /* for other command class */
    {
        pArgs->properties1 = CC_SUPERVISION_ADD_MORE_STATUS_UPDATE(CC_SUPERVISION_MORE_STATUS_UPDATES_THIS_IS_LAST) | CC_SUPERVISION_ADD_SESSION_ID(sessionID);
        pArgs->status = tempStatus;
        pArgs->duration = 0;
    }
}

void ZCB_CommandClassSupervisionReportReceived(ts_param_t *p, uint8_t class, uint8_t cmd, cc_supervision_status_t status, uint8_t duration, bool moreUpdates)
{
    mainlog(logDebug, "cb SupervisionReportReceived class %02X, cmd %02X, status %02X, duration %02X, moreUpdates %02X", class, cmd, status, duration, moreUpdates);
    NOTIFY_TX_BUFFER_T pTxNotify;

    //ZPC notify
    pTxNotify.CmdClassNotify.source_endpoint = p->sendpoint;
    pTxNotify.CmdClassNotify.dest_endpoint = p->dendpoint;
    pTxNotify.CmdClassNotify.source_nodeId = p->snode;
    pTxNotify.CmdClassNotify.dest_nodeId = p->dnode;
    pTxNotify.CmdClassNotify.node_type = GetCacheEntryNodeTypeGeneric(p->snode);
    pTxNotify.CmdClassNotify.node_is_listening = GetCacheEntryNodeMode(p->snode);

    pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SUPERVISION;
    pTxNotify.CmdClassNotify.cmd = SUPERVISION_REPORT;
    pTxNotify.CmdClassNotify.version = 1;
    pTxNotify.CmdClassNotify.supervision_report.class = class;
    pTxNotify.CmdClassNotify.supervision_report.cmd = cmd;
    pTxNotify.CmdClassNotify.supervision_report.more_status_updates = moreUpdates;
    pTxNotify.CmdClassNotify.supervision_report.status = status;
    pTxNotify.CmdClassNotify.supervision_report.duration = duration;

    PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));

}

/*============================ SetLearnNodeStateZPC_Compl ======================
 ** Function description
 **      Called when a new node have been added in ZWave PLus Compliant (ZPC)
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/

void SetLearnNodeStateZPC_Compl(
    uint8_t bStatus, /*IN Status of learn process*/
    uint8_t bSource, /*IN Node ID of node learned*/
    uint8_t *pCmd,   /*IN Pointer to node information*/
    uint8_t bLen)    /*IN Length of node information*/
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    static uint8_t schemeGet;


    switch (bStatus)
    {
    case ADD_NODE_STATUS_LEARN_READY:
    {
        nm.state = NMS_WAITING_FOR_ADD;
        //cancel old timer, start timer from 0
        resetNMTimer();

        timerStart(&(nm.nm_timer), NmTimeout, 0, ADD_REMOVE_TIMEOUT, TIMER_ONETIME);

        pTxNotify.AddNodeZPCNotify.bStatus = bStatus;
        if (NMM_ADD_NODE == nm.mode)
        {
            PushNotificationToHandler(ADD_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        }
        else if (NMM_CONTROLLER_CHANGE == nm.mode)
        {
            PushNotificationToHandler(CONTROLLER_CHANGE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        }
        mainlog(logDebug, "[ADD_NODE_STATUS_LEARN_READY(%02X)] - Press node button... (Nodeinformation length %u)", bStatus, bLen);
    }
    break;

    case ADD_NODE_STATUS_NODE_FOUND:
    {
        nm.state = NMS_NODE_FOUND;
        resetNMTimer();
        //caculator timout of add device process
        uint8_t noListening = 0, noNoneListening = 0;
        getNoDeviceMode(&noListening, &noNoneListening);
        //76000ms + LISTENINGNODES*217ms + FLIRNODES*3517ms
        uint32_t addNodeTimeout = 76000 + noListening*217 + noNoneListening*3517;
        timerStart(&(nm.nm_timer), NmTimeout, 0, addNodeTimeout, TIMER_ONETIME);

        pTxNotify.AddNodeZPCNotify.bStatus = bStatus;
        if (NMM_ADD_NODE == nm.mode)
        {
            PushNotificationToHandler(ADD_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        }
        else if (NMM_CONTROLLER_CHANGE == nm.mode)
        {
            PushNotificationToHandler(CONTROLLER_CHANGE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        }
        mainlog(logDebug, "[ADD_NODE_STATUS_NODE_FOUND(%02X)] - Waiting ... (Nodeinformation length %u)", bStatus, bLen);
    }
    break;

    case ADD_NODE_STATUS_ADDING_SLAVE:
    {
        NODE_TYPE node_Type = *((NODE_TYPE *)pCmd);

        if (bSource > 0)
        {
            //stored new node to lastLearnedNode struct
            lastLearnedNodeZPC.node_id = bSource;
            lastLearnedNodeZPC.node_type = node_Type.generic;
            lastLearnedNodeZPC.node_info.nodeType.basic = node_Type.basic;
            lastLearnedNodeZPC.node_info.nodeType.generic = node_Type.generic;
            lastLearnedNodeZPC.node_info.nodeType.specific = node_Type.specific;
            if (bLen > 3)
            {
                lastLearnedNodeZPC.node_capability.noCapability = bLen - 3;
                memcpy((char *)&lastLearnedNodeZPC.node_capability.aCapability, pCmd + 3, bLen - 3);
            }
        }

        pTxNotify.AddNodeZPCNotify.bStatus = bStatus;
        pTxNotify.AddNodeZPCNotify.zpc_node.node_id = bSource;
        pTxNotify.AddNodeZPCNotify.zpc_node.node_type = node_Type.generic;;
        PushNotificationToHandler(ADD_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));

        mainlog(logDebug, "[ADD_NODE_STATUS_ADDING_SLAVE(%02X)]  - Adding Slave unit... Nodeinformation length %u", bStatus, bLen);
    }
    break;

    case ADD_NODE_STATUS_ADDING_CONTROLLER:
    {
        NODE_TYPE node_Type = *((NODE_TYPE *)pCmd);

        if (bSource > 0)
        {
            //stored new node to lastLearnedNode struct
            lastLearnedNodeZPC.node_id = bSource;
            lastLearnedNodeZPC.node_type = node_Type.generic;
            lastLearnedNodeZPC.node_info.nodeType.basic = node_Type.basic;
            lastLearnedNodeZPC.node_info.nodeType.generic = node_Type.generic;
            lastLearnedNodeZPC.node_info.nodeType.specific = node_Type.specific;
            if (bLen > 3)
            {
                lastLearnedNodeZPC.node_capability.noCapability = bLen - 3;
                memcpy((char *)&lastLearnedNodeZPC.node_capability.aCapability, pCmd + 3, bLen - 3);
            }
        }

        pTxNotify.AddNodeZPCNotify.bStatus = bStatus;
        pTxNotify.AddNodeZPCNotify.zpc_node.node_id = bSource;
        pTxNotify.AddNodeZPCNotify.zpc_node.node_type = node_Type.generic;
        if (NMM_ADD_NODE == nm.mode)
        {
            PushNotificationToHandler(ADD_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        }
        else if (NMM_CONTROLLER_CHANGE ==  nm.mode)
        {
            PushNotificationToHandler(CONTROLLER_CHANGE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        }

        mainlog(logDebug, "[ADD_NODE_STATUS_ADDING_CONTROLLER(%02X)] - Adding Controller unit...  length %u", bStatus, bLen);
    }
    break;

    case ADD_NODE_STATUS_PROTOCOL_DONE:
    {
        if (NMM_ADD_NODE == nm.mode)
        {
            serialApiAddNodeToNetwork(ADD_NODE_STOP, SetLearnNodeStateZPC_Compl);
        }
        else if (NMM_CONTROLLER_CHANGE ==  nm.mode)
        {
            serialApiControllerChange(CONTROLLER_CHANGE_STOP, SetLearnNodeStateZPC_Compl);
        }
        mainlog(logDebug, "[ADD_NODE_STATUS_PROTOCOL_DONE(%02X)] - Now stopping, app has nothing... (length %u)", bStatus, bLen);
    }
    break;

    case ADD_NODE_STATUS_DONE:
    {
        mainlog(logDebug, "[ADD_NODE_STATUS_DONE(%02X)] - Node included, stopping, nm mode: %s.", bStatus, GetNMModeStr(nm.mode));
        //cancel time add manual (60s)
        resetNMTimer();

        if (NMM_ADD_NODE == nm.mode)
        {
            serialApiAddNodeToNetwork(ADD_NODE_STOP, NULL);
        }
        else if (NMM_CONTROLLER_CHANGE ==  nm.mode)
        {
            serialApiControllerChange(CONTROLLER_CHANGE_STOP, NULL);
        }

        if (rd_node_exists(lastLearnedNodeZPC.node_id))
        {
            int flags = GetCacheEntryFlag(lastLearnedNodeZPC.node_id);
            mainlog(logDebug, "This node has already been included with flags: %d", flags);
            if (SecureInclusionRestart(flags, SecurityInclusionDone_Compl)) return;
            goto interview;
        }
        else /* new node */
        {
            //rd_probe_lock(true);
            rd_register_new_node(lastLearnedNodeZPC.node_id, true);
        }

        MySystemSUCId = serialApiGetSUCNodeID();
        mainlog(logDebug, "RemoveAdd Manual Mode MySystemSUCId: %d, suc support Inclusion CC %d", MySystemSUCId, GetSupportedInclusionControllerCmdClass(MySystemSUCId));

        if ((MySystemSUCId != MyNodeId) && MySystemSUCId)
        {
            if (GetSupportedInclusionControllerCmdClass(MySystemSUCId) == CC_SUPPORTED)
            {
                /* inclusion Device When Have SIS in network and support command class Inclusion controller */            
                nm.state = NMS_INCLUSION_CONTROLLER;//inclusion controller is 1 state of auto remove add
                nm.tmp_node = lastLearnedNodeZPC.node_id;
                PostEventInclusionController(EV_ADD_NODE_NORMAL_OK);
                return;
            }
            else if (GetSupportedInclusionControllerCmdClass(MySystemSUCId) == CC_NOT_PROBED)
            {
                nm.state = NMS_WAIT_FOR_SUC_ADD_NODE;//inclusion controller is 1 state of auto remove add
                nm.tmp_node = MySystemSUCId;
                resetNMTimer();
                timerStart(&(nm.nm_timer), NmTimeout, 0, WAITING_SEND_REQ_NIF, TIMER_ONETIME);//waiting NIF 5s
                return;

            }
        }
        //cancel timer add manual
        nm.state = NMS_WAIT_FOR_SECURE_ADD;

        if (((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC, COMMAND_CLASS_SECURITY_2)) != SCHEME_NOT_SUPPORT) &&
            (GetCacheEntryFlag(MyNodeId) & NODE_FLAGS_SECURITY2))
        {
            securityS2InclusionStart(bSource, SecurityInclusionDone_Compl);
            return;
        }

        if (((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC, COMMAND_CLASS_SECURITY)) != SCHEME_NOT_SUPPORT) &&
            (GetCacheEntryFlag(MyNodeId) & NODE_FLAG_SECURITY0))
        {
            if ((lastLearnedNodeZPC.node_info.nodeType.basic == BASIC_TYPE_CONTROLLER) ||
                (lastLearnedNodeZPC.node_info.nodeType.basic == BASIC_TYPE_STATIC_CONTROLLER))
            {
                securityS0InclusionStart(bSource, true, SecurityInclusionDone_Compl);
            }
            else
            {
                securityS0InclusionStart(bSource, false, SecurityInclusionDone_Compl);
            }
            return;
        }

interview:
        /* This is a non secure node */
        nm.state = NMS_WAITING_FOR_INTERVIEW;
        lastLearnedNodeZPC.node_secure_scheme = NO_SCHEME;
        /*Add device Normal*/
        PostEventInterviewDevice(EV_ADD_NODE_OK);
    }
    break;

    case ADD_NODE_STATUS_FAILED:
    {
        resetNMTimer();

        memset((uint8_t *)&lastLearnedNodeZPC, 0, sizeof(ZW_Node_t));
        nm.state = NMS_IDLE;
        pTxNotify.AddNodeZPCNotify.bStatus = bStatus;
       if (NMM_ADD_NODE == nm.mode)
        {
            PushNotificationToHandler(ADD_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
            serialApiAddNodeToNetwork(ADD_NODE_STOP, NULL);
        }
        else if (NMM_CONTROLLER_CHANGE == nm.mode)
        {
            PushNotificationToHandler(CONTROLLER_CHANGE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
            serialApiControllerChange(CONTROLLER_CHANGE_STOP, NULL);
        }
        mainlog(logDebug, "ZPC: [ADD_NODE_STATUS_FAILED(%02X)] - AddNodeToNetwork failed, stopping... (Nodeinformation length %u)", bStatus, bLen);
    }
    break;

    default:
    {
        resetNMTimer();

        /* If were not pending, we want to turn off learn mode.. */
        if (NMS_WAITING_FOR_ADD == nm.state)
        {
            serialApiAddNodeToNetwork(ADD_NODE_STOP, NULL);
        }
        else
        {
            serialApiControllerChange(CONTROLLER_CHANGE_STOP, NULL);
        }
        mainlog(logDebug, "ZPC: [%02X] - Undefined status (Nodeinformation length %u)", bStatus, bLen);
    }
    break;
    }
}


/*============================ SetLearnNodeStateDeleteZPC_Compl ======================
 ** Function description
 **      Called when a node have been removed in ZWave PLus Compliant (ZPC)
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void SetLearnNodeStateDeleteZPC_Compl(uint8_t bStatus, uint8_t bSource, uint8_t *pCmd, uint8_t bLen)
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    NODE_TYPE node_Type;
    switch (bStatus)
    {
    case REMOVE_NODE_STATUS_LEARN_READY:
        nm.state = NMS_WAITING_FOR_REMOVE;
        resetNMTimer();
        timerStart(&(nm.nm_timer), NmTimeout, 0, ADD_REMOVE_TIMEOUT, TIMER_ONETIME);

        pTxNotify.RemoveNodeZPCNotify.bStatus = bStatus;
        PushNotificationToHandler(REMOVE_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.RemoveNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));

        mainlog(logDebug, "ZPC: [REMOVE_NODE_STATUS_LEARN_READY(%02X)] - Waiting... (Nodeinformation length %u)", bStatus, bLen);
        break;
    case REMOVE_NODE_STATUS_NODE_FOUND:
        nm.state = NMS_NODE_FOUND;
        mainlog(logDebug, "ZPC: [REMOVE_NODE_STATUS_NODE_FOUND(%02X)]  - Node found %03u (Nodeinformation length %u)", bStatus, bSource, bLen);
        break;
    case REMOVE_NODE_STATUS_REMOVING_SLAVE:
        node_Type = *((NODE_TYPE *)pCmd);

        if (bSource)
        {
            //store removed node to lastRemoveNode struct
            lastRemovedNode.node_id = bSource;
            lastRemovedNode.node_type = node_Type.generic;
            lastRemovedNode.node_info.nodeType.basic = node_Type.basic;
            lastRemovedNode.node_info.nodeType.generic = node_Type.generic;
            lastRemovedNode.node_info.nodeType.specific = node_Type.specific;
            if (bLen > 3)
            {
                lastRemovedNode.node_capability.noCapability = bLen - 3;
                memcpy((char *)&lastRemovedNode.node_capability.aCapability, pCmd + 3, bLen - 3);
            }
        }
        else
        {
            lastRemovedNode.node_id = 0;
        }

        pTxNotify.RemoveNodeZPCNotify.bStatus = bStatus;
        pTxNotify.RemoveNodeZPCNotify.zpc_node.node_id = bSource;
        pTxNotify.RemoveNodeZPCNotify.zpc_node.node_type = node_Type.generic;

        PushNotificationToHandler(REMOVE_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.RemoveNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        memset((uint8_t *)&lastRemovedNode, 0, sizeof(ZW_Node_t));

        mainlog(logDebug, "ZPC: [REMOVE_NODE_STATUS_REMOVING_SLAVE(%02X)]  - Removing Slave Node %03u (%s)... (Nodeinformation length %u)", bStatus, bSource, WriteNodeTypeString(node_Type.generic), bLen);
        serialApiRemoveNodeFromNetwork(REMOVE_NODE_STOP, SetLearnNodeStateDeleteZPC_Compl);
        break;

    case REMOVE_NODE_STATUS_REMOVING_CONTROLLER:
        node_Type = *((NODE_TYPE *)pCmd);

        if (bSource)
        {
            //store removed node to lastRemoveNode struct
            lastRemovedNode.node_id = bSource;
            lastRemovedNode.node_type = node_Type.generic;
            lastRemovedNode.node_info.nodeType.basic = node_Type.basic;
            lastRemovedNode.node_info.nodeType.generic = node_Type.generic;
            lastRemovedNode.node_info.nodeType.specific = node_Type.specific;
            if (bLen > 3)
            {
                lastRemovedNode.node_capability.noCapability = bLen - 3;
                memcpy((char *)&lastRemovedNode.node_capability.aCapability, pCmd + 3, bLen - 3);
            }
        }
        else
        {
            lastRemovedNode.node_id = 0;
        }
        pTxNotify.RemoveNodeZPCNotify.bStatus = bStatus;
        pTxNotify.RemoveNodeZPCNotify.zpc_node.node_id = bSource;
        pTxNotify.RemoveNodeZPCNotify.zpc_node.node_type = node_Type.generic;

        PushNotificationToHandler(REMOVE_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.RemoveNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        
        mainlog(logDebug, "ZPC: [REMOVE_NODE_STATUS_REMOVING_CONTROLLER(%02X)]  - Removing Controller Node %03u (%s)... (Nodeinformation length %u)", bStatus, bSource, WriteNodeTypeString(node_Type.generic), bLen);
        serialApiRemoveNodeFromNetwork(REMOVE_NODE_STOP, SetLearnNodeStateDeleteZPC_Compl);
        break;

    case REMOVE_NODE_STATUS_DONE:
        mainlog(logDebug, "ZPC: [REMOVE_NODE_STATUS_DONE(%02X)]  - Removing Node %03u", bStatus, bSource);
        pTxNotify.RemoveNodeZPCNotify.bStatus = bStatus;
        PushNotificationToHandler(REMOVE_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.RemoveNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        serialApiRemoveNodeFromNetwork(REMOVE_NODE_STOP, NULL);

        rd_remove_node(bSource);
        removeKeyHeldDownMessageListFromNodeId(bSource);
        removeSequenceNumberFromNodeId(bSource);

        resetNMTimer();
        memset((uint8_t *)&lastRemovedNode, 0, sizeof(ZW_Node_t));
        /* reset network management */
        nm.state = NMS_IDLE;
        nm.mode = NMM_DEFAULT;
        break;
    case REMOVE_NODE_STATUS_FAILED:
        pTxNotify.RemoveNodeZPCNotify.bStatus = bStatus;
        PushNotificationToHandler(REMOVE_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.RemoveNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        mainlog(logDebug, "ZPC: [REMOVE_NODE_STATUS_FAILED(%02X)]  - Remove Node failed... (Nodeinformation length %u)", bStatus, bLen);
        serialApiRemoveNodeFromNetwork(REMOVE_NODE_STOP, NULL);
        resetNMTimer();
        memset((uint8_t *)&lastRemovedNode, 0, sizeof(ZW_Node_t));
        /* reset network management */
        nm.state = NMS_IDLE;
        nm.mode = NMM_DEFAULT;
        break;
    default:
        mainlog(logDebug, "ZPC: [%02X]  - Unknown status, stopping... (Nodeinformation length %u)", bStatus, bLen);
        serialApiRemoveNodeFromNetwork(REMOVE_NODE_STOP, NULL);
        resetNMTimer();
        memset((uint8_t *)&lastRemovedNode, 0, sizeof(ZW_Node_t));
        /* reset network management */
        nm.state = NMS_IDLE;
        nm.mode = NMM_DEFAULT;
        break;
    }
}

/*============================ SendSlaveNodeInformation_Compl ======================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void SendSlaveNodeInformation_Compl(uint8_t bTxStatus)
{
    if (!bTxStatus)
    {
        mainlog(logDebug, "SendSlaveNodeInformation_Compl : Success... [%02X]", bTxStatus);
    }
    else
    {
        mainlog(logDebug, "SendSlaveNodeInformation_Compl : Failure... [%02X] ", bTxStatus);
    }
}
/*============================ SetDefault_Compl ======================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void SetDefault_Compl(void)
{
    mainlog(logDebug, "serialApiSetDefault done..");
   
    bWaitForHandleDeviceResetLocally = false;
}




/*========================== RequestNetworkUpdate_Compl ======================
** Function description
**      Callback function called by RequestNetworkUpdate functionality when done
** Side effects:
**
**--------------------------------------------------------------------------*/
void RequestNetworkUpdate_Compl(uint8_t bStatus)
{
    lastRequestStatus = bStatus;
    mainlog(logDebug, "serialApiRequestNetworkUpdate status - %d", bStatus);
}
/*======================== RequestNodeNeighborUpdate_Compl ===================
** Function description
**   ZW_RequestNodeNeighborUpdate callback
** Side effects:
**      
**--------------------------------------------------------------------------*/
void RequestNodeNeighborUpdate_Compl(uint8_t bStatus) /*IN Status of neighbor update process*/
{
    mainlog(logDebug, "RequestNodeNeighborUpdate_Compl %02X", bStatus);
}

/*======================== RequestNodeNeighborUpdate_Compl ===================
** Function description
**   ZW_RequestNodeNeighborUpdate callback
** Side effects:
**      
**--------------------------------------------------------------------------*/
void SetSUCNodeID_Compl(uint8_t bStatus) /*IN Status of neighbor update process*/
{
    mainlog(logDebug, "SetSUCNodeID_Compl %02X", bStatus);
    lastRequestStatus = bStatus;
}

/*========================= RequestNodeInfo_Compl ============================
** Function description
**   ZW_RequestNodeInfo callback called when the request transmission has 
** been tried.
** Side effects:
**      
**--------------------------------------------------------------------------*/
void RequestNodeInfo_Compl(uint8_t bTxStatus)
{
    if (!bTxStatus)
    {
        mainlog(logDebug, "Request Node Info transmitted successfully");
    }
    else
    {
        mainlog(logDebug, "Request Node Info transmit failed");
    }
}
/*============================ SetSlaveLearnMode_Compl ======================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void SetSlaveLearnMode_Compl(uint8_t state, uint8_t orgID, uint8_t newID)
{
    switch (state)
    {
    case ASSIGN_COMPLETE:
    {
        mainlog(logDebug, "Assign complete with orgID:%02X, newID:%02X", orgID, newID);
    }
    break;
    case ASSIGN_NODEID_DONE:
    {
        mainlog(logDebug, "Assign NodeID done with orgID:%02X, newID:%02X", orgID, newID);
    }
    break;
    case ASSIGN_RANGE_INFO_UPDATE:
    {
        mainlog(logDebug, "Assign range info update with orgID:%02X, newID:%02X", orgID, newID);
    }
    break;
    default:
        mainlog(logDebug, "SetSlaveLearnMode_Compl unknown state");
        break;
    }
}
/*============================ IdleLearnNodeState_Compl ======================
 ** Function description
 ** Handle Node Info from device
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void ApplicationControllerUpdate(uint8_t bStatus, uint8_t bSource, uint8_t *pCmd, uint8_t bLen, uint8_t *prospectHomeID)
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    static uint8_t schemeGet;

    switch (bStatus)
    {
    case UPDATE_STATE_NODE_INFO_FOREIGN_HOMEID_RECEIVED:
    {
#if 0
        /* This bStatus has different syntax from others, so handle this first */
        /* pCmd is first part of the public key-derived homeID */

        assert(prospectHomeID != NULL);
        NetworkManagement_smart_start_inclusion(ADD_NODE_OPTION_NETWORK_WIDE | ADD_NODE_OPTION_NORMAL_POWER, prospectHomeID);
#endif

    }
    break;

    case UPDATE_STATE_INCLUDED_NODE_INFO_RECEIVED:
    {
#if 0
        uint8_t INIF_rxStatus;
        uint8_t INIF_NWI_homeid[4];

        INIF_rxStatus = pCmd[0];
        memcpy(INIF_NWI_homeid, &pCmd[1], 4);
        NetworkManagement_INIF_Received(bSource, INIF_rxStatus, INIF_NWI_homeid);
#endif

    }
    break;

    case UPDATE_STATE_ROUTING_PENDING:
    {
        mainlog(logDebug, "waiting ...");
    }
    break;

    case UPDATE_STATE_NEW_ID_ASSIGNED:
    {
        mainlog(logDebug, "Node %u included ... ", bSource);
        UpdateNodeId_t* lastUpdateNodeZPC = (UpdateNodeId_t*)malloc(sizeof(UpdateNodeId_t));
        memset((uint8_t *)lastUpdateNodeZPC, 0, sizeof(UpdateNodeId_t));
        NODE_TYPE node_Type = *((NODE_TYPE *)pCmd);
        if ((bSource > 0) && (bSource != lastLearnedNodeZPC.node_id) && 
            !((nm.mode == NMM_REPLACE_FAILED_NODE) && (nm.tmp_node == bSource)))
        {
            //stored new node to lastLearnedNode struct
            lastUpdateNodeZPC->zpc_node.node_id = bSource;
            lastUpdateNodeZPC->zpc_node.node_type = node_Type.generic;
            lastUpdateNodeZPC->zpc_node.node_info.nodeType.basic = node_Type.basic;
            lastUpdateNodeZPC->zpc_node.node_info.nodeType.generic = node_Type.generic;
            lastUpdateNodeZPC->zpc_node.node_info.nodeType.specific = node_Type.specific;
            lastUpdateNodeZPC->zpc_node.node_secure_scheme = NO_SCHEME;
            if (bLen > 3)
            {
                lastUpdateNodeZPC->zpc_node.node_capability.noCapability = bLen - 3;
                memcpy((char *)&lastUpdateNodeZPC->zpc_node.node_capability.aCapability, pCmd + 3, bLen - 3);
            }

            if (rd_node_exists(lastUpdateNodeZPC->zpc_node.node_id))
            {
                return;
            }
            else /* new node */
            {
                rd_register_new_node(lastUpdateNodeZPC->zpc_node.node_id, true);
            }
            timerStart(&(lastUpdateNodeZPC->waiting_timer), NmTimeoutPushNotifyNodeInclude, (void*)lastUpdateNodeZPC, WAITING_NEWNODE_TIMEOUT, TIMER_ONETIME);//waiting NIF 5s
        }

    }
    break;

    case UPDATE_STATE_DELETE_DONE:
    {
        mainlog(logDebug, "Node %u removed  ... ", bSource);
        UpdateNodeId_t lastUpdateNodeZPC;
        memset((uint8_t *)&lastUpdateNodeZPC, 0, sizeof(UpdateNodeId_t));
        NODE_TYPE node_Type = *((NODE_TYPE *)pCmd);
        if ((bSource > 0) && (bSource != lastRemovedNode.node_id))
        {
            rd_remove_node(bSource);
            removeKeyHeldDownMessageListFromNodeId(bSource);
            removeSequenceNumberFromNodeId(bSource);

            /*store removed node to lastRemoveNode struct*/
            lastUpdateNodeZPC.zpc_node.node_id = bSource;
            lastUpdateNodeZPC.zpc_node.node_type = node_Type.generic;
            lastUpdateNodeZPC.zpc_node.node_info.nodeType.basic = node_Type.basic;
            lastUpdateNodeZPC.zpc_node.node_info.nodeType.generic = node_Type.generic;
            lastUpdateNodeZPC.zpc_node.node_info.nodeType.specific = node_Type.specific;
            if (bLen > 3)
            {
                lastUpdateNodeZPC.zpc_node.node_capability.noCapability = bLen - 3;
                memcpy((char *)&lastUpdateNodeZPC.zpc_node.node_capability.aCapability, pCmd + 3, bLen - 3);
            }

            pTxNotify.RemoveNodeZPCNotify.bStatus = REMOVE_NODE_STATUS_DONE;
            memcpy((uint8_t *)&pTxNotify.RemoveNodeZPCNotify.zpc_node, (uint8_t *)&lastUpdateNodeZPC.zpc_node, sizeof(ZW_Node_t));
            PushNotificationToHandler(REMOVE_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.RemoveNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        }
    }
    break;

    case UPDATE_STATE_NODE_INFO_RECEIVED:
    {
        mainlog(logDebug, "Node %u sends nodeinformation", bSource);
        /*  For Inclusion Controller 
            nm.tmp_node is node Id of Inclusion
            lastLearnedNodeZPC.node_id is nodeId of device */
        if ((nm.state == NMS_WAIT_FOR_SUC_NIF) && (MySystemSUCId == bSource)) //into inclusion controller mode
        {
            mainlog(logDebug, "waif for sis nif ");
            hexdump(pCmd,bLen);
            resetNMTimer();
            if (isSupportedCommandClassInNIF(pCmd,bLen,COMMAND_CLASS_INCLUSION_CONTROLLER))
            {
                SetSupportedInclusionControllerCmdClass(bSource,true);
                nm.state = NMS_INCLUSION_CONTROLLER;//inclusion controller is 1 state of auto remove add
                nm.tmp_node = lastLearnedNodeZPC.node_id;
                PostEventInclusionController(EV_ADD_NODE_NORMAL_OK);
                return;
            }

            nm.state = NMS_WAIT_FOR_SECURE_ADD;

            if (((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC, COMMAND_CLASS_SECURITY_2)) != SCHEME_NOT_SUPPORT) &&
                (GetCacheEntryFlag(MyNodeId) & NODE_FLAGS_SECURITY2))
            {
                securityS2InclusionStart(bSource, SecurityInclusionDone_Compl);
                return;
            }

            if (((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC, COMMAND_CLASS_SECURITY)) != SCHEME_NOT_SUPPORT) &&
                (GetCacheEntryFlag(MyNodeId) & NODE_FLAG_SECURITY0))
            {
                if ((lastLearnedNodeZPC.node_info.nodeType.basic == BASIC_TYPE_CONTROLLER) ||
                    (lastLearnedNodeZPC.node_info.nodeType.basic == BASIC_TYPE_STATIC_CONTROLLER))
                {
                    securityS0InclusionStart(bSource, true, SecurityInclusionDone_Compl);
                }
                else
                {
                    securityS0InclusionStart(bSource, false, SecurityInclusionDone_Compl);
                }
                return;
            }

            /* This is a non secure node */
            nm.state = NMS_WAITING_FOR_INTERVIEW;
            lastLearnedNodeZPC.node_secure_scheme = NO_SCHEME;
            PostEventAutoRemoveAdd(EV_FINISH_ADD_SECURITY);
            /*Inclusion Controller Mode, Adding device is non secure*/
            PostEventInterviewDevice(EV_ADD_NODE_OK);

        }

        if ((nm.state == NMS_INCLUSION_CONTROLLER) && (lastLearnedNodeZPC.node_id == bSource)) //into inclusion controller mode
        {
            //register new node
            NODE_TYPE node_Type = *((NODE_TYPE *)pCmd);

            if (bSource > 0)
            {
                //stored new node to lastLearnedNode struct
                lastLearnedNodeZPC.node_id = bSource;
                lastLearnedNodeZPC.node_type = node_Type.generic;
                lastLearnedNodeZPC.node_info.nodeType.basic = node_Type.basic;
                lastLearnedNodeZPC.node_info.nodeType.generic = node_Type.generic;
                lastLearnedNodeZPC.node_info.nodeType.specific = node_Type.specific;
                if (bLen > 3)
                {
                    lastLearnedNodeZPC.node_capability.noCapability = bLen - 3;
                    memcpy((char *)&lastLearnedNodeZPC.node_capability.aCapability, pCmd + 3, bLen - 3);
                }
            }

            if (rd_node_exists(lastLearnedNodeZPC.node_id))//replace fail node
            {
                int flags = GetCacheEntryFlag(lastLearnedNodeZPC.node_id);
                mainlog(logDebug, "This node has already been included with flags: %d", flags);
            }
            else
            {
                mainlog(logDebug, "Register for nodeId: %d", lastLearnedNodeZPC.node_id);
                rd_register_new_node(lastLearnedNodeZPC.node_id, true);
            }
            //when receive INCLUSION_CONTROLLER_COMPLETE --> device support S2 secure--> support S0.
            PostEventInclusionController(EV_RECEIVE_NODE_INFOMATION);
        }


        if ((NMM_REPLACE_FAILED_NODE == nm.mode) && (nm.tmp_node == bSource))
        {
            mainlog(logDebug, "lastReplaceNodeID: %02X update nodeinfo", bSource);

            rd_remove_node(nm.tmp_node);
            removeKeyHeldDownMessageListFromNodeId(nm.tmp_node);
            removeSequenceNumberFromNodeId(nm.tmp_node);
            
            rd_register_new_node(nm.tmp_node, true);

            if (bLen > 3)
            {
                lastLearnedNodeZPC.node_capability.noCapability = bLen - 3;
                memcpy((char *)&lastLearnedNodeZPC.node_capability.aCapability, pCmd + 3, bLen - 3);
            }

            //replace fail node have SIS in network
             if ((MySystemSUCId != MyNodeId) && MySystemSUCId)
            {
                if (GetSupportedInclusionControllerCmdClass(MySystemSUCId) == CC_SUPPORTED)
                {
                    /* inclusion Device When Have SIS in network and support command class Inclusion controller */            
                    nm.state = NMS_INCLUSION_CONTROLLER;//inclusion controller is 1 state of auto remove add
                    nm.tmp_node = lastLearnedNodeZPC.node_id;
                    PostEventInclusionController(EV_ADD_NODE_NORMAL_OK);
                    return;
                }
                else if (GetSupportedInclusionControllerCmdClass(MySystemSUCId) == CC_NOT_PROBED)
                {
                    nm.state = NMS_WAIT_FOR_SUC_NIF;//inclusion controller is 1 state of auto remove add
                    nm.tmp_node = MySystemSUCId;
                    serialApiRequestNodeInfo(nm.tmp_node,0);
                    resetNMTimer();
                    timerStart(&(nm.nm_timer), NmTimeout, 0, REQUEST_NIF_TIMEOUT, TIMER_ONETIME);//waiting NIF 5s
                    return;

                }
            }
            
            nm.state = NMS_WAIT_FOR_SECURE_ADD;

            if (((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC, COMMAND_CLASS_SECURITY_2)) != SCHEME_NOT_SUPPORT) &&
                (GetCacheEntryFlag(MyNodeId) & NODE_FLAGS_SECURITY2))
            {
                securityS2InclusionStart(bSource, SecurityInclusionDone_Compl);
                return;
            }

            if (((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC, COMMAND_CLASS_SECURITY)) != SCHEME_NOT_SUPPORT) &&
                (GetCacheEntryFlag(MyNodeId) & NODE_FLAG_SECURITY0))
            {
                if ((lastLearnedNodeZPC.node_info.nodeType.basic == BASIC_TYPE_CONTROLLER) ||
                    (lastLearnedNodeZPC.node_info.nodeType.basic == BASIC_TYPE_STATIC_CONTROLLER))
                {
                    securityS0InclusionStart(bSource, true, SecurityInclusionDone_Compl);
                }
                else
                {
                    securityS0InclusionStart(bSource, false, SecurityInclusionDone_Compl);
                }
                return;
            }

            /* This is a non secure node */
            nm.state = NMS_WAITING_FOR_INTERVIEW;
            lastLearnedNodeZPC.node_secure_scheme = NO_SCHEME;
            /*Replace Failed Node, Interview Dev*/
            PostEventInterviewDevice(EV_ADD_NODE_OK);
        }

    }
    break;

    case UPDATE_STATE_SUC_ID:
    {
        MySystemSUCId = bSource;
        pTxNotify.UpdateSUCNotify.suc_id = bSource;
        serialApiGetControllerCapabilities(&pTxNotify.UpdateSUCNotify.my_capability);
        PushNotificationToHandler(UPDATE_SUC_NOTIFY, (uint8_t *)&pTxNotify.UpdateSUCNotify, sizeof(UPDATE_SUC_NOTIFY_T));
    }
    break;

    default:
    {
        mainlog(logDebug, "Node %u sends %02x", bSource, *pCmd);
    }
    break;
    }
}

/*============================ RemoveFailedNode_Compl ======================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void RemoveFailedNode_Compl(uint8_t bStatus)
{
    mainlog(logDebug, "ZW_RemoveFailedNode callback status %02X", bStatus);
    lastRemoveNodeForceStatus = bStatus;
    switch (bStatus)
    {
    case ZW_NODE_OK:
        mainlog(logDebug, "Failed node %03d not removed    - ZW_NODE_OK"); //, bNodeID);
        break;

    case ZW_FAILED_NODE_REMOVED:
        mainlog(logDebug, "Failed node %03d removed        - ZW_FAILED_NODE_REMOVED"); //, bNodeID);
        break;

    case ZW_FAILED_NODE_NOT_REMOVED:
        mainlog(logDebug, "Failed node %03d not removed    - ZW_FAILED_NODE_NOT_REMOVED"); //, bNodeID);
        break;

    default:
        mainlog(logDebug, "ZW_RemoveFailedNode unknown status %d", bStatus);
        break;
    }
}
/*============================ ApplicationCommandHandler_Bridge_Compl ======================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void ApplicationCommandHandler_Bridge_Compl(uint8_t rxStatus, uint8_t destNode, uint8_t sourceNode, uint8_t *multi, uint8_t *pCmd, uint8_t cmdLength)
{
    //ZW_APPLICATION_TX_BUFFER *pdata= (ZW_APPLICATION_TX_BUFFER*)pCmd;
    char *frameType;
    char bCharBuf[256] = "";
    int i, j;

    frameType = GetFrameTypeStr_Bridge(rxStatus & RECEIVE_STATUS_TYPE_MASK);

    mainlog(logDebug, "Node 0x%02X says to 0x%02X - %u, %u (%s)", sourceNode, destNode,
            *(pCmd + CMDBUF_CMDCLASS_OFFSET), *(pCmd + CMDBUF_PARM1_OFFSET), frameType);

    if ((multi != NULL) && (multi[0] > 0))
    {
        snprintf(bCharBuf, sizeof(bCharBuf), "Nodemask: ");
        for (i = 1; i <= multi[0]; i++)
        {
            snprintf(&bCharBuf[strlen(bCharBuf)], sizeof(bCharBuf) - strlen(bCharBuf), "%0X ", multi[i]);
        }
        mainlog(logDebug, "%s", bCharBuf);
        snprintf(bCharBuf, sizeof(bCharBuf), "Destination: ");

        int lNodeID = 1;
        int bitmask;

        for (i = 1; i <= multi[0]; i++)
        {
            bitmask = BIT0;
            for (j = 1; j <= 8; j++, bitmask <<= 1)
            {
                if (bitmask & multi[i])
                {
                    snprintf(&bCharBuf[strlen(bCharBuf)], sizeof(bCharBuf) - strlen(bCharBuf), "%u ", lNodeID++);
                }
            }
        }
        mainlog(logDebug, "%s", bCharBuf);
    }
    switch (*(pCmd + CMDBUF_CMDCLASS_OFFSET))
    {
    case COMMAND_CLASS_SWITCH_MULTILEVEL:
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case SWITCH_MULTILEVEL_REPORT:
            mainlog(logDebug, "Node 0x%02X says SWITCH MULTILEVEL REPORT %u (%s)", sourceNode, *(pCmd + CMDBUF_PARM1_OFFSET), frameType);
            break;
        case SWITCH_MULTILEVEL_SET:
            mainlog(logDebug, "Node 0x%02X says SWITCH MULTILEVEL SET %u (%s)", sourceNode, *(pCmd + CMDBUF_PARM1_OFFSET), frameType);
            break;
        case SWITCH_MULTILEVEL_GET:
            mainlog(logDebug, "Node %u says SWITCH MULTILEVEL GET %u (%s)", sourceNode, *(pCmd + CMDBUF_PARM1_OFFSET), frameType);
            break;
        }
        break;
    case COMMAND_CLASS_BASIC:
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case BASIC_REPORT:
            mainlog(logDebug, "Node %u says BASIC REPORT %u (%s)", sourceNode, *(pCmd + CMDBUF_PARM1_OFFSET), frameType);
            break;
        case BASIC_SET:
            mainlog(logDebug, "Node %u says BASIC SET %u (%s)", sourceNode, *(pCmd + CMDBUF_PARM1_OFFSET), frameType);
            break;
        case BASIC_GET:
            mainlog(logDebug, "Node %u says BASIC GET (%s)", sourceNode, frameType);
            break;
        default:
            mainlog(logDebug, "Node %u says none supported BASIC command %u (%s)", sourceNode, *(pCmd + CMDBUF_PARM1_OFFSET), frameType);
            break;
        }
        break;
    case COMMAND_CLASS_SENSOR_BINARY:
        break;
    case COMMAND_CLASS_CONTROLLER_REPLICATION:
        //TODO
        //RePlicationCommandComplete();
        break;
    case COMMAND_CLASS_METER:
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case METER_REPORT:
            mainlog(logInfo, "Node %u says METER REPORT (%s)", sourceNode, frameType);
            break;
        }
        break;
    default:
        mainlog(logInfo, "Node %u says COMMANDCLASS=%u, COMMAND=%u (%s)", sourceNode, *(pCmd + CMDBUF_CMDCLASS_OFFSET), *(pCmd + CMDBUF_CMD_OFFSET), frameType);
        break;
    }
}

/*============================ SetApplNodeInfo ======================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
int SetApplNodeInfo(void)
{

    APPL_NODE_TYPE nodeType;
    nodeType.generic = myZPCCapability.node_info.nodeType.generic;
    nodeType.specific = myZPCCapability.node_info.nodeType.specific;
    uint8_t isNodeInfoValid = 0;
    nvm_config_get(set_nif, &isNodeInfoValid);

    if (isNodeInfoValid)
    {
        mainlog(logDebug, "SetApplNodeInfo has already had");
        return 1;
    }

    serialApiApplicationNodeInformation(APP_NETWORK_CAPABILITY, 
                                        nodeType,
                                        myZPCCapability.node_capability.aCapability,
                                        myZPCCapability.node_capability.noCapability);

    isNodeInfoValid = 1;
    nvm_config_set(set_nif, &isNodeInfoValid);
    mainlog(logDebug, "SetApplNodeInfo has set");
    int ret;
    ret = serialApiSetSUCNodeID(MyNodeId, true, ZW_SUC_FUNC_NODEID_SERVER, false, NULL);

    if (ret)
    {
        return 0;
    }
    else
    {
        mainlog(logDebug, "cannot set SIS for self");
        return -1;
    }
    return 0;
}

/*============================ ZCB_DeviceResetLocallyDone ======================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void ZCB_DeviceResetLocallyDone(uint8_t status)
{
    /* CHANGE THIS - clean your own application data from NVM*/
    bWaitForHandleDeviceResetLocally = false;
}

/*============================ ResetStateFSMToIdle ======================
 ** Function description
 ** Side effects:
 ** Reset state interviewDevice to Idle
 **--------------------------------------------------------------------------*/

void ResetStateFSMToIdle(void)
{
    //Reset state interviewDevice to Idle
    if (nm.state == NMS_WAITING_FOR_INTERVIEW)
    {
        ActionWhenInterviewDeviceFailed();
    }
    //reset state interview, auto_remove_add, manual_remove_add
    nm.state = NMS_IDLE;
    nm.mode = NMM_DEFAULT;
    nm.flags = 0;
    state_interview = ST_INTERVIEW_IDLE;
    state_auto_remove_add = ST_AUTOADD_IDLE;
}

void initZwavePollingDev(void)
{
    initPollingDevList();
    zwnet_init(&zwaveNetworkPolling);
}
/*============================ ZCB_DeviceResetLocallyDone ======================
 ** Function description
 ** Side effects:
 ** Return
 **    -1: timeout
 **     0: success
 **--------------------------------------------------------------------------*/
int nodeSetDefault(uint32_t *myHomeId, uint8_t *myCapability, ZW_Node_t *zpc_node)
{
    timingGetClockSystem(&txTiming); //5000ms

    bWaitForHandleDeviceResetLocally = true;
    handleCommandClassDeviceResetLocally(ZCB_DeviceResetLocallyDone);

    while (bWaitForHandleDeviceResetLocally == true)
    {
        usleep(1000);
        if (timingGetElapsedMSec(&txTiming) > 10000) //5000ms
        {
            timingGetClockSystemStop(&txTiming);
            mainlog(logWarning, "Warning: handleCommandClassDeviceResetLocally is timeout!");
            break;
        }
    }

    timerDelete();
    timingGetClockSystem(&txTiming); //5000ms

    bWaitForHandleDeviceResetLocally = true;
    serialApiSetDefault(SetDefault_Compl);

    while (bWaitForHandleDeviceResetLocally == true)
    {
        usleep(1000);
        if (timingGetElapsedMSec(&txTiming) > 5000) //5000ms
        {
            timingGetClockSystemStop(&txTiming);
            mainlog(logWarning, "Warning: set default command is timeout!");
            return -1;
        }
    }
    
    security_set_default();

    rd_exit();
    rd_destroy();

    AssociationClearAll();
    ZW_SendDataAppl_init();

    s2_nvm_init(true);

    SetPreInclusionNIF(NO_SCHEME);
    SetApplNodeInfo();
    MySystemSUCId = serialApiGetSUCNodeID();
    mainlog(logDebug, "suc id %02X", MySystemSUCId);

    SetCacheEntryFlagMasked(MyNodeId, sec2_get_my_node_flags(), 0xFF);
    mainlog(logDebug, "my node flags %d", GetCacheEntryFlag(MyNodeId));
    //reset state remove/add, interview to idle
    ResetStateFSMToIdle();
    getControllerInfo(myHomeId, myCapability, zpc_node);

    zwpoll_exit(zwaveNetworkPolling->poll_ctx);
    zwpoll_shutdown(zwaveNetworkPolling->poll_ctx);
    /**init polling device**/
    initZwavePollingDev();
    removeAllSequenceNumberFromList();
    removeAllKeyHeldDownMessageList();
    return 0;
}

/*============================ controllerChangeRole ======================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
int controllerChangeRole(uint8_t enable)
{
    if (enable)
    {
        nm.mode = NMM_CONTROLLER_CHANGE;
        memset((uint8_t *)&lastLearnedNodeZPC, 0, sizeof(ZW_Node_t));
        serialApiControllerChange(CONTROLLER_CHANGE_START, SetLearnNodeStateZPC_Compl);
    }
    else
    {
        nm.mode = NMM_DEFAULT;
        resetNMTimer();
        serialApiControllerChange(CONTROLLER_CHANGE_STOP, NULL);
    }
    return 0;
}

/*============================ zwaveAddNodeZPC ======================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
int zwaveAddNodeZPC(uint8_t enable)
{
    if (enable)
    {
        postEventOperationPollingDev(EV_USER_ADD_NODE_ZPC);
        nm.mode = NMM_ADD_NODE;
        setNWBusy(true);
        nm.state = NMS_IDLE;
        memset((uint8_t *)&lastLearnedNodeZPC, 0, sizeof(ZW_Node_t));
        serialApiAddNodeToNetwork(ADD_NODE_ANY | ADD_NODE_OPTION_NORMAL_POWER | ADD_NODE_OPTION_NETWORK_WIDE,
                                SetLearnNodeStateZPC_Compl);
        
    }
    else
    {
        if (nm.state == NMS_INCLUSION_CONTROLLER) PostEventInclusionController(EV_CLOSE_INCLUSION_CONTROLLER);
        resetNMTimer();
        sec2_abort_join();
        serialApiAddNodeToNetwork(ADD_NODE_STOP, NULL);
        if (nm.state == NMS_IDLE) nm.mode = NMM_DEFAULT;
        setNWBusy(false);
        postEventOperationPollingDev(EV_USER_ADD_NODE_ZPC_DONE);
    }
    return 0;
}

int zwaveRemoveNodeZPC(uint8_t enable)
{
    if (enable)
    {
        postEventOperationPollingDev(EV_USER_REMOVE_NODE_ZPC);
        nm.mode = NMM_REMOVE_NODE;
        setNWBusy(true);
        serialApiRemoveNodeFromNetwork(REMOVE_NODE_ANY | REMOVE_NODE_OPTION_NETWORK_WIDE, SetLearnNodeStateDeleteZPC_Compl);
    }
    else
    {
        nm.mode = NMM_DEFAULT;
        setNWBusy(false);
        resetNMTimer();
        serialApiRemoveNodeFromNetwork(REMOVE_NODE_STOP, NULL);
        postEventOperationPollingDev(EV_USER_REMOVE_NODE_ZPC_DONE);
    }
    return 0;
}

int zwaveAddRemoveCombinationNodeZPC(uint8_t enable)
{
    if (enable)
    {
        postEventOperationPollingDev(EV_USER_REMOVE_NODE_ZPC_AUTO);
        //caculator timout of add device process
        uint8_t noListening = 0, noNoneListening = 0;
        getNoDeviceMode(&noListening, &noNoneListening);
        //76000ms + LISTENINGNODES*217ms + FLIRNODES*3517ms
        uint32_t addNodeTimeout = 76000 + noListening*217 + noNoneListening*3517;
        ChangeTimeoutOfState(ST_WAITING_ADD_PROTOCOL_ACK, addNodeTimeout);
        nm.mode = NMM_AUTO_REMOVEADD_NODE;
        setNWBusy(true);
        PostEventAutoRemoveAdd(EV_OPEN_NETWORK);
    }
    else
    {
        //nm.mode = NMM_DEFAULT;
        PostEventAutoRemoveAdd(EV_CLOSE_NETWORK);
        PostEventInclusionController(EV_CLOSE_INCLUSION_CONTROLLER);
        setNWBusy(false);
        postEventOperationPollingDev(EV_USER_REMOVE_NODE_ZPC_AUTO_DONE);
    }
    return 0;
}

int nodeSetSlaveLearnMode(uint8_t node)
{

    if (node == 0)
    {
        serialApiSetSlaveLearnMode(node, VIRTUAL_SLAVE_LEARN_MODE_ENABLE, SetSlaveLearnMode_Compl);
        usleep(1000);
        serialApiSendSlaveNodeInformation(node, NODE_BROADCAST, TRANSMIT_OPTION_ACK, SendSlaveNodeInformation_Compl);
    }
    else
    {
        serialApiSetSlaveLearnMode(node, VIRTUAL_SLAVE_LEARN_MODE_ADD, SetSlaveLearnMode_Compl);
    }
    return 0;
}

int nodeRequestNetworkUpdate(void)
{
    bool timeout = false; //not timeout
    lastRequestStatus = 0xFF;
    if (serialApiRequestNetworkUpdate(RequestNetworkUpdate_Compl))
    {
        timingGetClockSystem(&txTiming);
        while (lastRequestStatus == 0xFF)
        {
            usleep(1000);
            if (timingGetElapsedMSec(&txTiming) > 10000) //10000ms
            {
                timeout = true; //timeout 10s
                mainlog(logDebug, "Warning: request network command is timeout! Please try again!");
                timingGetClockSystemStop(&txTiming);
                return -1;
            }
        }

        if (!timeout)
        {
            if (lastRequestStatus == ZW_SUC_UPDATE_DONE)
            {
                mainlog(logDebug, "request network is successful");
                //rd_init(false, true);
                return 0;
            }
            else
            {
                mainlog(logDebug, "request networkD is failed");
                return -2;
            }
        }
    }
    else
    {
        if (serialApiGetSUCNodeID()>0)
        {
            /*I'm the SUC/SIS or i don't know the SUC/SIS*/
            return 0;
        }
        else
        {
            /* SUC/SIS does not exist */
            return -1;
        }
    }
    return 0;
}

int nodeRequestNodeInfo(uint8_t nodeID)
{
    serialApiRequestNodeInfo(nodeID,0);
    return 0;
}

int nodeSendMyNodeInfo(uint8_t nodeID)
{
    if (!serialApiSendNodeInformation(nodeID, ZWAVE_PLUS_TX_OPTIONS, ZCB_TransmitMyNodeInfoComplete))
    {
        return -1;
    }
    return 0;
}

/*get s0, s2 key, save it to .txt file,
push this file to ftp server
file name is Home ID*/

int getSecureKeyAndSave(char *file_name)
{
  
    //use homeId to file name
    //char name_file[16];
    sprintf(file_name, "%X", homeId); 
    if(!save_secure_key(file_name))//create file
        return -1;//can not create file.
    return 0;
}

int nodeSetSUCMode(uint8_t nodeID, bool enable_suc_mode, uint8_t capabilities, uint8_t* controllerCapability)
{

    bool timeout = false; //not timeout
    uint8_t ret;

    lastRequestStatus = 0xFF;

    ret = serialApiSetSUCNodeID(nodeID, enable_suc_mode, capabilities, false, SetSUCNodeID_Compl);
    if (ret != true)
        return -2;

    if (nodeID != MyNodeId)
    {
        timingGetClockSystem(&txTiming); //10000ns
        while (lastRequestStatus == 0xFF)
        {
            usleep(1000);
            if (timingGetElapsedMSec(&txTiming) > 10000) //10000ms
            {
                timeout = true; //timeout 10s
                mainlog(logDebug, "Warning: set suc mode command is timeout! Please try again!");
                timingGetClockSystemStop(&txTiming);
                return -1;
            }
        }

        if (!timeout)
        {
            serialApiGetControllerCapabilities(controllerCapability);
            if (lastRequestStatus == ZW_SUC_SET_SUCCEEDED)
            {
                mainlog(logDebug, "SET SUC MODE COMMAND is successful");
                return 0;
            }
            else
            {
                mainlog(logDebug, "SET SUC MODE COMMAND is failed");
                return -2;
            }
        }
    }
    serialApiGetControllerCapabilities(controllerCapability);
    return 0;
}

int nodeGetList(uint8_t* noNode, rd_node_list_t* nodeList)
{
    GetNodeList(noNode, nodeList);
    return 0;
}


void requestSpec_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t)
{
    lastRequestStatus = bTxStatus;
    mainlog(logDebug, "requestSpec_Compl -> status: %02X", lastRequestStatus);
    switch (bTxStatus)
    {
    case TRANSMIT_COMPLETE_OK:
    case TRANSMIT_COMPLETE_NO_ACK:
    case TRANSMIT_COMPLETE_FAIL:
    default:
        break;
    }
}

/*note flag BIT0 : forceImnediately
            BIT1 : send with SuperVision encapsulate */
//return 0: -- fail node
//return #0: node is  faiiled node 

int nodeSendRequestSpecification(uint8_t flag, uint32_t expected_timeout,
                                 uint8_t node_id, uint8_t *pDataIn, uint8_t pData_lenIn,
                                 uint8_t source_endpoint, uint8_t dest_endpoint, uint8_t scheme, char *strData)
{
    CheckNodeIdAndResetTimerSendWakupNoMoreInfo(node_id);
    checkNodeIdWaitingMessageFromHandlerInQueue(node_id);
    bool forceImnediately = flag & 0x01;
    bool timeout = false; //not time out
    int maxTimeout;
    static uint8_t pData[BUF_SIZE];
    if (pData_lenIn > BUF_SIZE)
    {
        mainlog(logDebug, "data over length!!");
        return -1;
    }
    memcpy(pData, pDataIn, pData_lenIn);

    /* maxTimeout = 15s for AC devices and 25s for FLIRS device */
    maxTimeout = (MODE_FREQUENTLYLISTENING == GetCacheEntryNodeMode(node_id)) ? 25000 : 15000;



    if (flag & 0x02) /* send with SuperVision encap*/
    {
        pData_lenIn = CmdClassSupervisionEncapsule(pData, pData_lenIn);
        /* for debuging */
        mainlog(logDebug, "Supervision encap");
        hexdump(pData, pData_lenIn);
    }

    if ((!forceImnediately) && (MODE_NONLISTENING == GetCacheEntryNodeMode(node_id)))
    {
        if (cmdReqInQueueIndex < COMMAND_SPECIFIC_MAX)
        {
            memcpy(cmdReqInQueue[cmdReqInQueueIndex].buf, pData, pData_lenIn);
            cmdReqInQueue[cmdReqInQueueIndex].length = pData_lenIn;
            cmdReqInQueue[cmdReqInQueueIndex].node_id = node_id;
            cmdReqInQueue[cmdReqInQueueIndex].source_endpoint = source_endpoint;
            cmdReqInQueue[cmdReqInQueueIndex].dest_endpoint = dest_endpoint;
            cmdReqInQueue[cmdReqInQueueIndex].scheme = scheme;
            if (strData)
            {
                strncpy(cmdReqInQueue[cmdReqInQueueIndex].strData, strData, STR_MAX_LEN);
            }
            cmdReqInQueueIndex++;
            mainlog(logDebug, "Add to cmdReqInQueue with cmdReqInQueueIndex:%d", cmdReqInQueueIndex - 1);
        }
        mainlog(logDebug, "nodeSendRequestSpecification return 2");
        return 2;
    }

    ts_param_t p;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = scheme;

    p.snode = MyNodeId;
    p.sendpoint = source_endpoint;

    p.dnode = node_id;
    p.dendpoint = dest_endpoint;

    lastRequestStatus = 0xFF;
    Transport_SendDataApplEP(pData, pData_lenIn, &p, requestSpec_Compl, 0);

    timingGetClockSystem(&txTiming); //
    while (lastRequestStatus == 0xFF)
    {
        usleep(1000);
        if (expected_timeout > 0)
        {
            if (timingGetElapsedMSec(&txTiming) > expected_timeout) 
            {
                serialApiSendDataAbort();
                sec0_abort_all_tx_sessions();
                expected_timeout = 0;
            }
        }

        if (timingGetElapsedMSec(&txTiming) > maxTimeout)
        {
            timeout = true;
            timingGetClockSystemStop(&txTiming);
            serialApiSendDataAbort();
            sec0_abort_all_tx_sessions();
            lastRequestStatus = 0xFE;
            mainlog(logWarning, "Warning: SEND DATA COMMAND is timeout! Please try again! nodeSendRequestSpecification return -1 ");
            return -1;
        }
    }
    if (!timeout)
    {
        if (lastRequestStatus == TRANSMIT_COMPLETE_OK)
        {
            mainlog(logDebug, "nodeSendRequestSpecification return 0");
            //save last value switch multilevel set, basic set
            if(((COMMAND_CLASS_SWITCH_MULTILEVEL == *pDataIn) && (SWITCH_MULTILEVEL_SET == *(pDataIn + 1))) || 
                ((COMMAND_CLASS_BASIC == *pDataIn) && (BASIC_SET == *(pDataIn + 1))))
            {
            /*
            pDataIn[0] = cmdClass
            pDataIn[1] = cmd
            pDataIn[2] = value
            */
                SetLastValueSwitchMultilevel(node_id, *(pDataIn + 2));
            }
            return 0;
        }
        else
        {
            mainlog(logDebug, "nodeSendRequestSpecification return -1");
            return -1;
        }
    }
    mainlog(logDebug, "nodeSendRequestSpecification return 0");
    return 0;
}

int nodeRemoveForce(uint8_t node_id, ZW_Node_t *node_id_rm)
{
    mainlog(logDebug, "Removing Force Device with node_id: %u", node_id);
    int ret;
    bool timeout = false; //not time out
    uint8_t buf[1];
    int maxTimeout;

    /* maxTimeout = 10s for AC devices and 20s for FLIRS device */
    maxTimeout = (MODE_FREQUENTLYLISTENING == GetCacheEntryNodeMode(node_id)) ? 20000 : 10000;

    mainlog(logDebug,"device is remove from app --> remove device from libzwave.");
    SetCacheEntryNodeMode(node_id, MODE_REMOVE_FORCE);

    buf[0] = COMMAND_CLASS_NO_OPERATION;
    node_id_rm->node_id = node_id;
    ret = nodeSendRequestSpecification(true, 0, node_id, buf, 1, 0, 0, 0, "");
    if (ret == 0)
    {
        return 2;
    }
    else if (ret == -2)
    {
        goto done;
    }
    else //send data fail
    {
        if (!serialApiIsFailedNode(node_id))
        {
            return 2;
        }
        lastRemoveNodeForceStatus = 0xFF;
        ret = serialApiRemoveFailedNode(node_id, RemoveFailedNode_Compl);
        if (ret != ZW_FAILED_NODE_REMOVE_STARTED)
        {
            return -2;
        }
        timingGetClockSystem(&txTiming); //10000ms
        while (lastRemoveNodeForceStatus == 0xFF)
        {
            usleep(1000);
            if (timingGetElapsedMSec(&txTiming) > maxTimeout) 
            {
                timeout = true;
                lastRemoveNodeForceStatus = 0xFE;
                return -1;
            }
        }

        if (!timeout)
        {
            if (lastRemoveNodeForceStatus == ZW_NODE_OK)
            {
                return 2;
            }
            goto done;
        }
    }

done:
    rd_remove_node(node_id);
    removeKeyHeldDownMessageListFromNodeId(node_id);
    removeSequenceNumberFromNodeId(node_id);
    return 0;
}


void NewNode_Notify(uint8_t bStatus)
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    static uint8_t schemeGet;

    if ((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC,
                                             COMMAND_CLASS_WAKE_UP)) != SCHEME_NOT_SUPPORT)
    {
        pthread_mutex_lock(&CriticalMutexNotification);
        if (handleAppCmdInQueueIndex < COMMAND_SCHEDULING_MAX)
        {
            mainlog(logDebug, "Add Wake up no more infomation message to schedule task, index: %d", handleAppCmdInQueueIndex);
            handleAppCmdInQueue[handleAppCmdInQueueIndex].task_type = TT_WAKE_UP_NO_MORE_INFO;
            handleAppCmdInQueue[handleAppCmdInQueueIndex].retry = DEFAULT_RETRY;
            handleAppCmdInQueue[handleAppCmdInQueueIndex].handle_send_wake_up_no_info.nodeId = lastLearnedNodeZPC.node_id;
            handleAppCmdInQueue[handleAppCmdInQueueIndex].handle_send_wake_up_no_info.scheme = schemeGet;
            //start timer
            timingGetClockSystem(&handleAppCmdInQueue[handleAppCmdInQueueIndex].handle_send_wake_up_no_info.lastTimeRequestInfo); //5000ms
            handleAppCmdInQueueIndex++;
        }
        pthread_mutex_unlock(&CriticalMutexNotification);
    }

    //*****************handler message from handler at adding*********************
    pthread_mutex_lock(&CriticalMutexNotification);
    if (handleAppCmdInQueueIndex < COMMAND_SCHEDULING_MAX)
    {
        mainlog(logDebug, "Add task waiting message from handler to schedule task, index: %d", handleAppCmdInQueueIndex);
        handleAppCmdInQueue[handleAppCmdInQueueIndex].task_type = TT_WAITING_MESSAGE_FROM_HANDLER;
        handleAppCmdInQueue[handleAppCmdInQueueIndex].retry = DEFAULT_RETRY;
        handleAppCmdInQueue[handleAppCmdInQueueIndex].waiting_message_from_handler.nodeId = lastLearnedNodeZPC.node_id;
        //start timer
        timingGetClockSystem(&handleAppCmdInQueue[handleAppCmdInQueueIndex].waiting_message_from_handler.lastTimeSendSpecCmd); //5000ms
        handleAppCmdInQueueIndex++;
    }
    pthread_mutex_unlock(&CriticalMutexNotification);

    //------------------------------------------------
    if (isSupportedCommandClass(&lastLearnedNodeZPC,
                                             COMMAND_CLASS_SWITCH_MULTILEVEL) == SCHEME_NOT_SUPPORT)
    {
        SetLastValueSwitchMultilevel(lastLearnedNodeZPC.node_id, INVALUE_LAST_VALUE);
    }
    switch (bStatus)
    {
    case ADD_NODE_STATUS_DONE:
    {
        if(lastLearnedNodeZPC.node_flags & NODE_FLAG_KNOWN_BAD)//add secure failed
            pTxNotify.AddNodeZPCNotify.bStatus = ADD_NODE_STATUS_ADD_SECURE_FAILED;
        else
            pTxNotify.AddNodeZPCNotify.bStatus = bStatus;

        mainlog(logDebug, "Add node state done ->nm mode %s, node_flags: %d", GetNMModeStr(nm.mode), lastLearnedNodeZPC.node_flags);

        memcpy((uint8_t *)&pTxNotify.AddNodeZPCNotify.zpc_node, (uint8_t *)&lastLearnedNodeZPC, sizeof(ZW_Node_t));
        serialApiGetControllerCapabilities(&pTxNotify.AddNodeZPCNotify.my_capability);
        pTxNotify.AddNodeZPCNotify.zpc_node.node_secure_scheme = lastLearnedNodeZPC.node_secure_scheme;
        pTxNotify.AddNodeZPCNotify.zpc_node.node_info.mode = GetCacheEntryNodeMode(lastLearnedNodeZPC.node_id);

        switch (nm.mode)
        {
        case NMM_ADD_NODE:
        {
            PushNotificationToHandler(ADD_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        }
        break;

        case NMM_CONTROLLER_CHANGE:
        {
            PushNotificationToHandler(CONTROLLER_CHANGE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        }
        break;

        case  NMM_AUTO_REMOVEADD_NODE:
        {
            PushNotificationToHandler(ADD_NODE_ZPC_NOTIFY_AUTO, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        }
        break;

        case NMM_REPLACE_FAILED_NODE:
        {
            PushNotificationToHandler(REPLACE_FAILED_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        }
        break;

        default:
        break;

        }
            
    }
    break;

    default:
        break;
    }
    memset((uint8_t *)&lastLearnedNodeZPC, 0, sizeof(ZW_Node_t));
}

void ReplaceFailedNode_Comp(uint8_t bStatus)
{
    switch (bStatus)
    {
    case ZW_NODE_OK:
        mainlog(logDebug, "ReplaceFailedNode_Comp: node OK");
        break;
    case ZW_FAILED_NODE_REPLACE:
        mainlog(logDebug, "ReplaceFailedNode_Comp: failed node replace");

        break;
    case ZW_FAILED_NODE_REPLACE_DONE:
        mainlog(logDebug, "ReplaceFailedNode_Comp: done -> nm mode: %s nm tmp id %02X", GetNMModeStr(nm.mode), nm.tmp_node);
        if (( nm.mode == NMM_REPLACE_FAILED_NODE) && (nm.tmp_node))
        {
            nm.state = NMS_PROXY_INCLUSION_WAIT_NIF;
            NODEINFO lastLearnedNodeInfo;
            lastLearnedNodeZPC.node_id = nm.tmp_node;

            /* Get node information from nvm of controller */
            serialApiGetNodeProtocolInfo(lastLearnedNodeZPC.node_id, &lastLearnedNodeInfo);
            lastLearnedNodeZPC.node_info.nodeType.basic = lastLearnedNodeInfo.nodeType.basic;
            lastLearnedNodeZPC.node_info.nodeType.generic = lastLearnedNodeInfo.nodeType.generic;
            lastLearnedNodeZPC.node_info.nodeType.specific = lastLearnedNodeInfo.nodeType.specific;
            lastLearnedNodeZPC.node_type = lastLearnedNodeZPC.node_info.nodeType.generic; /* Store learned generic node type*/

            serialApiRequestNodeInfo(lastLearnedNodeZPC.node_id, RequestNodeInfo_Compl);
            resetNMTimer();
            timerStart(&nm.nm_timer, NmTimeout, 0, 5000, TIMER_ONETIME);//waiting NIF 5s
        }
        break;
    case ZW_FAILED_NODE_REPLACE_FAILED:
        mainlog(logDebug, "ReplaceFailedNode_Comp: ZW_FAILED_NODE_REPLACE_FAILED");
        break;
    }
}

int nodeReplaceFailedNode(uint8_t node_id, uint8_t mode)
{
    int ret;
    if (mode == 1)
    {
        postEventOperationPollingDev(EV_USER_REPLACE_FAILED_NODE);
        memset((uint8_t *)&lastLearnedNodeZPC, 0, sizeof(ZW_Node_t));
        ret = serialApiReplaceFailedNode(node_id, ReplaceFailedNode_Comp);
        if (ret != ZW_FAILED_NODE_REMOVE_STARTED)
        {
            return -2;
        }
        //node is failed node
        nm.mode = NMM_REPLACE_FAILED_NODE;
        nm.state = NMS_REPLACE_FAILED_REQ;
        nm.tmp_node = node_id;
        resetNMTimer();
        timerStart(&(nm.nm_timer), NmTimeout, 0, ADD_REMOVE_TIMEOUT, TIMER_ONETIME);
        return 0;
    }
    else if (mode == 0)
    {
        if (nm.state == NMS_INCLUSION_CONTROLLER) PostEventInclusionController(EV_CLOSE_INCLUSION_CONTROLLER);
        resetNMTimer();
        sec2_abort_join();
        serialApiAddNodeToNetwork(REMOVE_NODE_STOP, NULL);
        if (nm.state == NMS_IDLE) nm.mode = NMM_DEFAULT;
        postEventOperationPollingDev(EV_USER_REPLACE_FAILED_NODE_DONE);
        return 0;
    }

    return 0;
}

int nodeIsFailedNode(uint8_t node_id, uint8_t *status)
{

    uint8_t buf[1];
    buf[0] = COMMAND_CLASS_NO_OPERATION;
    nodeSendRequestSpecification(true, 0, node_id, buf, 1, 0, 0, 0, "");

    *status = serialApiIsFailedNode(node_id);//status = 1 --> fail node, status = 0 --> not fail node
    return 0;
}

/*======================================================================================================================
===================================================== External API =====================================================
======================================================================================================================*/

int zwaveDestruct(void)
{
    serialAPIDestruct();
    removeAllSequenceNumberFromList();
    removeAllKeyHeldDownMessageList();
    pthread_cancel(handleNotifyThreadProcID);
    close(fd);
    //cancel polling dev
    zwpoll_exit(zwaveNetworkPolling->poll_ctx);
    zwpoll_shutdown(zwaveNetworkPolling->poll_ctx);
    return 0;
}

int zwaveReInit(char *portName, bool getVersion)
{
    int n;
    uint8_t libType;
    uint8_t uCversion[100];
    char *pLibTypeStr;
    memset((uint8_t *)&lastRemovedNode, 0, sizeof(ZW_Node_t));
    memset((uint8_t *)&lastLearnedNodeZPC, 0, sizeof(ZW_Node_t));
    initZwavePollingDev();
    fd = open(portName, O_RDWR | O_NDELAY | O_NOCTTY);
    if (fd >= 0)
    {
        // Cancel the O_NDELAY flag.
        n = fcntl(fd, F_GETFL, 0);
        fcntl(fd, F_SETFL, n & ~O_NDELAY);
    }

    if (fd < 0)
    {
        mainlog(logWarning, "ERROR %d opening %s: %s\n", errno, portName, strerror(errno));
        exit(0);
    }
    
    set_interface_attribs(fd, B115200, 0, 1); // set speed to 115,200 bps, 8n1 (no parity)
    
    serialApiInitialize(&serialApiCallbacks);

    if (getVersion)
    {
        libType = serialApiVersion((uint8_t *)uCversion);
        pLibTypeStr = GetValidLibTypeStr(libType);
        if (0 != strcmp(" ", pLibTypeStr))
        {
            mainlog(logUI, "Z-Wave %s based serial API found", pLibTypeStr);
            snprintf((char *)uCversion, 100, "%s - %s", uCversion, pLibTypeStr);
        }
        else
        {
            mainlog(logUI, "No Serial API module detected, [libtype %u]- check serial connections.\nThis application requires a Serial API\non a Z-Wave module connected via a serialport.\nDownload the correct Serial API code to the Z-Wave Module and restart the application.", libType);
            libType = 0; /* Indicate we want to end this */
        }

        if (!libType)
        {
            return -1;
        }

        /*for S2lib */
        s2_nvm_init(false);
        SetApplNodeInfo();
        MySystemSUCId = serialApiGetSUCNodeID();
        mainlog(logDebug, "suc id %02X", MySystemSUCId);

        /*for S2lib */
        security_init();
        SetCacheEntryFlagMasked(MyNodeId, sec2_get_my_node_flags(), 0xFF);
        AssociationInit(false);
        AGI_LifeLineGroupSetup(agiTableLifeLine, (sizeof(agiTableLifeLine) / sizeof(CMD_CLASS_GRP)));
        AGI_ResourceGroupSetup(NULL, 0, 1);
        CommandClassSupervisionInit(CC_SUPERVISION_STATUS_UPDATES_SUPPORTED,
                                    ZCB_CommandClassSupervisionGetReceived,
                                    ZCB_CommandClassSupervisionReportReceived);
    }

    /*create thread handle notify from device*/
    pthread_create(&handleNotifyThreadProcID, NULL, (void *)&HandleNotifyThreadProc, NULL);
   
    return 0;
}

int zwaveInitialize(char *portName, notify_queue_t *notify)
{
    int n;
    uint8_t libType;
    uint8_t uCversion[100];
    char *pLibTypeStr;
    initKeyHeldDownMessageList();
    initZwavePollingDev();
    printf("Z-Wave lib version %d.%02d\n",APP_VERSION, APP_REVISION );

    eeprom_init();
    ZW_SendDataAppl_init();
    ZW_TransportService_Init(ApplicationCommandHandlerTransport_Compl);

    memset((uint8_t *)&lastRemovedNode, 0, sizeof(ZW_Node_t));
    memset((uint8_t *)&lastLearnedNodeZPC, 0, sizeof(ZW_Node_t));
    g_notify_buf = notify;
    memset((char *)g_notify_buf, 0, sizeof(notify_queue_t));

    fd = open(portName, O_RDWR | O_NDELAY | O_NOCTTY);
    if (fd >= 0)
    {
        // Cancel the O_NDELAY flag.
        n = fcntl(fd, F_GETFL, 0);
        fcntl(fd, F_SETFL, n & ~O_NDELAY);
    }

    if (fd < 0)
    {
        mainlog(logWarning, "ERROR %d opening %s: %s\n", errno, portName, strerror(errno));
        exit(0);
    }
    
    set_interface_attribs(fd, B115200, 0, 1); // set speed to 115,200 bps, 8n1 (no parity)
    
    serialApiInitialize(&serialApiCallbacks);
    libType = serialApiVersion((uint8_t *)uCversion);
    pLibTypeStr = GetValidLibTypeStr(libType);
    if (0 != strcmp(" ", pLibTypeStr))
    {
        mainlog(logUI, "Z-Wave %s based serial API found", pLibTypeStr);
        snprintf((char *)uCversion, 100, "%s - %s", uCversion, pLibTypeStr);
    }
    else
    {
        mainlog(logUI, "No Serial API module detected, [libtype %u]- check serial connections.\nThis application requires a Serial API\non a Z-Wave module connected via a serialport.\nDownload the correct Serial API code to the Z-Wave Module and restart the application.", libType);
        libType = 0; /* Indicate we want to end this */
    }

    if (!libType)
    {
        if (!ZWPGM_CheckUpdateFirmware(NULL))
        {
            mainlog(logUI, "Firmware for Z-Wave controller is not available!");
            return 1;
        }
        return -1;
    }

    /*for S2lib */
    s2_nvm_init(false);
    SetApplNodeInfo();
    MySystemSUCId = serialApiGetSUCNodeID();
    mainlog(logDebug, "suc id %02X", MySystemSUCId);

    /*create thread handle notify from device*/
    pthread_create(&handleNotifyThreadProcID, NULL, (void *)&HandleNotifyThreadProc, NULL);
    /*for S2lib */
    security_init();
    SetCacheEntryFlagMasked(MyNodeId, sec2_get_my_node_flags(), 0xFF);
    AssociationInit(false);
    AGI_LifeLineGroupSetup(agiTableLifeLine, (sizeof(agiTableLifeLine) / sizeof(CMD_CLASS_GRP)));
    AGI_ResourceGroupSetup(NULL, 0, 1);
    CommandClassSupervisionInit(CC_SUPERVISION_STATUS_UPDATES_SUPPORTED,
                                ZCB_CommandClassSupervisionGetReceived,
                                ZCB_CommandClassSupervisionReportReceived);
    return 0;
}

int getFirmwareVersion(uint8_t *firmareVersion)
{
    uint8_t libType;
    //serialApiInitialize(ApplicationCommandHandler_Compl, ApplCmdGetVersion_Compl);    
    
    libType = serialApiVersion(firmareVersion);
    mainlog(logUI, "Firmware Version: %s", (char*)firmareVersion);


    if (!libType)
        return -1;
    return 0;
        
}

int zwaveControllerInfo(ZW_OwnerID *zw_ownerID)
{
    zw_ownerID->HomeID = homeId;
    zw_ownerID->MyNodeID = MyNodeId;
    return 0;
}
int getControllerInfo(uint32_t *myHomeId, uint8_t *myCapability, ZW_Node_t *zpc_node)
{
    *myHomeId = homeId;
    memcpy((uint8_t *)zpc_node, (uint8_t *)&myZPCCapability, sizeof(ZW_Node_t));
    zpc_node->node_secure_scheme = highest_scheme(GetCacheEntryFlag(MyNodeId));
    zpc_node->node_id = MyNodeId;
    zpc_node->node_flags = GetCacheEntryFlag(MyNodeId);
    serialApiGetControllerCapabilities(myCapability);
    return 0;
}

//interview device
void ActionWhenInterviewDeviceFailed(void)
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    if(lastLearnedNodeZPC.node_flags & NODE_FLAG_KNOWN_BAD)//add secure failed
        pTxNotify.AddNodeZPCNotify.bStatus = ADD_NODE_STATUS_ADD_SECURE_FAILED;
    else
        pTxNotify.AddNodeZPCNotify.bStatus = ADD_NODE_STATUS_INTERVIEW_DEV_FAILED;    
    mainlog(logDebug, "lastLearnedNodeZPC.node_flags: %d", lastLearnedNodeZPC.node_flags);        
    memcpy((uint8_t *)&pTxNotify.AddNodeZPCNotify.zpc_node, (uint8_t *)&lastLearnedNodeZPC, sizeof(ZW_Node_t));
    serialApiGetControllerCapabilities(&pTxNotify.AddNodeZPCNotify.my_capability);
    pTxNotify.AddNodeZPCNotify.zpc_node.node_secure_scheme = lastLearnedNodeZPC.node_secure_scheme;
    pTxNotify.AddNodeZPCNotify.zpc_node.node_info.mode = GetCacheEntryNodeMode(lastLearnedNodeZPC.node_id);
    //*****************handler message from handler at adding*********************
    pthread_mutex_lock(&CriticalMutexNotification);
    if (handleAppCmdInQueueIndex < COMMAND_SCHEDULING_MAX)
    {
        mainlog(logDebug, "Add task waiting message from handler to schedule task, index: %d", handleAppCmdInQueueIndex);
        handleAppCmdInQueue[handleAppCmdInQueueIndex].task_type = TT_WAITING_MESSAGE_FROM_HANDLER;
        handleAppCmdInQueue[handleAppCmdInQueueIndex].retry = DEFAULT_RETRY;
        handleAppCmdInQueue[handleAppCmdInQueueIndex].waiting_message_from_handler.nodeId = lastLearnedNodeZPC.node_id;
        //start timer
        timingGetClockSystem(&handleAppCmdInQueue[handleAppCmdInQueueIndex].waiting_message_from_handler.lastTimeSendSpecCmd); //5000ms
        handleAppCmdInQueueIndex++;
    }
    pthread_mutex_unlock(&CriticalMutexNotification);
    if (nm.mode == NMM_ADD_NODE)
    {
        resetNMTimer();
        PushNotificationToHandler(ADD_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        serialApiAddNodeToNetwork(ADD_NODE_STOP, NULL);
    }
    else if (nm.mode == NMM_AUTO_REMOVEADD_NODE)
    {
        PushNotificationToHandler(ADD_NODE_ZPC_NOTIFY_AUTO, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        serialApiAddNodeToNetwork(ADD_NODE_STOP, NULL);

    }
    else if (nm.mode == NMM_CONTROLLER_CHANGE)
    {
        PushNotificationToHandler(CONTROLLER_CHANGE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
        serialApiControllerChange(CONTROLLER_CHANGE_STOP, NULL);
    }
    else if (nm.mode == NMM_REPLACE_FAILED_NODE)
    {
        resetNMTimer();
        //pTxNotify.AddNodeZPCNotify.bStatus = REPLACE_FAILED_NODE_FAILED;
        PushNotificationToHandler(REPLACE_FAILED_NODE_ZPC_NOTIFY, (uint8_t *)&pTxNotify.AddNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
    }

    memset((uint8_t *)&lastLearnedNodeZPC, 0, sizeof(ZW_Node_t));
}

int changeChannelOfTV(TZWParam *pzwParamHandler)
{
    int i, j, res = 0;
    
    TZWParam *pzwParam = (TZWParam *)malloc(sizeof(TZWParam));
    memset(pzwParam, 0x00, sizeof(TZWParam));
    memcpy(pzwParam, pzwParamHandler, sizeof(TZWParam));

    for(j = CMDBUF_PARM1_OFFSET; j < pzwParamHandler->cmd_set.cmd_length; j++)
    {
        i = CMDBUF_PARM1_OFFSET;
        pzwParam->cmd_set.cmd[i++] = (pzwParam->param1)++; //Sequence Number
        pzwParam->cmd_set.cmd[i++] = (uint8_t)KEY_DOWN_ATTRIBUTE;//Key Attributes, Key Down – Sent when a new key is pressed.
        pzwParam->cmd_set.cmd[i++] = 0;//Item ID MSB
        pzwParam->cmd_set.cmd[i++] = 0;//Item ID LSB
        pzwParam->cmd_set.cmd[i++] = 0;//Command MSB,1
        pzwParam->cmd_set.cmd[i++] = pzwParamHandler->cmd_set.cmd[j] - CONVERT_ASCII_TO_INTERGER + CONVERT_CHANNEL_ID_TO_TV_CMD;//channel 0, --> command 6, //Command LSB,1
        pzwParam->cmd_set.cmd_length = i;
        pzwParam->ret = nodeSendRequestSpecification(pzwParam->param3, pzwParam->param4, pzwParam->param2,
                                                            pzwParam->cmd_set.cmd, pzwParam->cmd_set.cmd_length,
                                                            pzwParam->src_endpoint, pzwParam->dest_endpoint, pzwParam->scheme,
                                                            (char *)pzwParam->misc_data);
        if(pzwParam->ret)
        {
            break;
        }
    }
    res = pzwParam->ret;
    free(pzwParam);
    return res;
}





/*========================================================================================================
======================================= Handle Command from Zw handler ===================================
==========================================================================================================*/

int zwaveSendCommand(TZWParam *pzwParam)
{
    switch (pzwParam->command)
    {
    case COMMAND_CLASS_SPECIFIC_NODE_GET_LIST:
        postEventOperationPollingDev(EV_USER_GET_LIST);
        pzwParam->ret = nodeGetList(&pzwParam->param1, pzwParam->multi_node);//param1: noNode
        postEventOperationPollingDev(EV_USER_GET_LIST_DONE);
        break;
    case COMMAND_CLASS_SPECIFIC_NODE_SET_DEFAULT:
        postEventOperationPollingDev(EV_USER_SET_DEFAULT);
        pzwParam->ret = nodeSetDefault(&pzwParam->param4, &pzwParam->param2, &pzwParam->node);
        postEventOperationPollingDev(EV_USER_SET_DEFAULT_DONE);
        break;
    case COMMAND_CLASS_SPECIFIC_NETWORK_TEST_CONNECTION:
        postEventOperationPollingDev(EV_USER_NETWORK_TEST);
        switch (pzwParam->param2)
        {
            case COMMAND_HEALTH_TEST:
            {
                if (!pzwParam->param3)
                {
                    if(!testStarted)  ReloadNodeList(true);
                    if (pzwParam->param1 == 0xFF)
                    {
                        if (!testStarted && (0 < bListeningNodeListSize))
                        {
                            testStarted = NetworkHealth(FULL);
                            pzwParam->ret = 0;
                        }
                        else
                        {
                            pzwParam->ret = 1;
                        }
                    }else
                    {
                        bNodeID = pzwParam->param1;
                        pzwParam->ret = !(StartNetworkHealthTestSingle());
                    }
                }
                else
                {
                    testStarted = false;
                    if (0 < bListeningNodeListSize)
                    {
                        NetworkManagement_NetworkHealth_Stop(&sNetworkManagementUT);
                        mainlog(logDebug, "Stopping Z-Wave Network Health Test");
                    }
                    else
                    {
                        mainlog(logDebug, "No Nodes in network eligible for establishing Z-Wave Network Health");
                    }
                    pzwParam->ret = 0;
                     postEventOperationPollingDev(EV_USER_NETWORK_TEST_DONE);
                }
            }
            break;

            case COMMAND_MAINTENANCE:
            { 
                if (!pzwParam->param3)
                {
                    ReloadNodeList(true);
                    DoNetworkHealthMaintenance(true, false);
                    pzwParam->ret = 0;
                }
                else
                {
                    DoNetworkHealthMaintenance(false, false);
                    pzwParam->ret = 0;    
                    postEventOperationPollingDev(EV_USER_NETWORK_TEST_DONE);
                }

            }
            break;

            case COMMAND_REDISCOVERY:
            {
                if (!pzwParam->param3)
                {
                    if(!testStarted)  ReloadNodeList(true);
                    if (!testStarted && (0 < bListeningNodeListSize))
                    {
                        testStarted = NetworkManagement_DoRediscoveryStart(NetworkRediscoveryComplete);
                        if (testStarted)
                        {
                            mainlog(logUI, "Starting Network Rediscovery");
                            pzwParam->ret = 0;
                        }
                        else
                        {
                            mainlog(logUI, "Could not start Network Rediscovery");
                            pzwParam->ret = 1;
                        }
                    }
                    else
                    {
                        mainlog(logUI, "Network Rediscovery is in progress");
                        pzwParam->ret = 1;
                    }
                }
                else
                {
                    NetworkManagement_DoRediscoveryStop(&sNetworkManagementUT);
                    testStarted = false;
                    pzwParam->ret = 0;
                    postEventOperationPollingDev(EV_USER_NETWORK_TEST_DONE);
                }
            }
            break;

            case COMMAND_PING_ALL_NODE:
            {
                if (!pzwParam->param3)
                {
                    if (!testStarted)
                    {
                        ReloadNodeList(true);
                        if (0 < bListeningNodeListSize)
                        {
                            bPingNodeIndex = 0;
                            abPingFailedSize = 0;
                            memset(abPingFailed, 0, sizeof(abPingFailed));
                            testStarted = NetworkManagement_NH_TestConnection(abListeningNodeList[bPingNodeIndex], CB_PingTestComplete);
                            if (testStarted)
                            {
                                mainlog(logDebug, "Ping Start   - Node %03u", abListeningNodeList[bPingNodeIndex]);
                                pzwParam->ret = 0;   
                            }
                            else
                            {
                                mainlog(logDebug, "Ping Node - could not be started");
                                pzwParam->ret = 1;   
                            }
                        }
                    }
                    else
                    {
                        mainlog(logDebug, "A test is allready in progress");
                        pzwParam->ret = 1;   
                    }
                }
                else
                {
                    mainlog(logDebug, "Stop ping all nodes");
                    bPingNodeIndex = bListeningNodeListSize;
                    pzwParam->ret = 0;
                    postEventOperationPollingDev(EV_USER_NETWORK_TEST_DONE);   
                }
            }
            break;

            case COMMAND_RSSI_MAP:
            {
                ReloadNodeList(true);
                RssiMapStart();
                pzwParam->ret = 0;   
            }
            break;
        }
        break;

    case COMMAND_CLASS_SPECIFIC_NETWORK_DUMP_NEIGHBORS:
    {
        postEventOperationPollingDev(EV_USER_NETWORK_TEST);
        ReloadNodeList(true);
        pzwParam->ret = NetworkManagement_DumpNeighbors(true, pzwParam->raw_data, &pzwParam->param1);
        postEventOperationPollingDev(EV_USER_NETWORK_TEST_DONE);
        //mainlog(logDebug, "NetworkManagement_DumpNeighbors");
        //hexdump(pzwParam->raw_data, 200);
    }
    break;

    case COMMAND_CLASS_SPECIFIC_NODE_SET_LEARN_MODE:
    {
        if (pzwParam->param1 == 1)
        {

            pzwParam->ret = StartLearnModeNow(LEARN_MODE_INCLUSION);
        }
        else
        {
            pzwParam->ret = StartLearnModeNow(LEARN_MODE_DISABLE);
        }
    }
    break;

    case COMMAND_CLASS_SPECIFIC_NODE_SET_SUC_SIS_MODE:
        /*only set SIS for device, not set SUC
        pzwParam->param3 return controllerCapability*/
        postEventOperationPollingDev(EV_USER_SET_SUC_SIS_MODE);
        pzwParam->ret = nodeSetSUCMode(pzwParam->param1, pzwParam->param2, ZW_SUC_FUNC_NODEID_SERVER, &pzwParam->param3);
        postEventOperationPollingDev(EV_USER_SET_SUC_SIS_MODE_DONE);
        break;

    case COMMAND_CLASS_SPECIFIC_NODE_REQUEST_UPDATE:
        postEventOperationPollingDev(EV_USER_REQUEST_UPDATE);
        pzwParam->ret = nodeRequestNetworkUpdate();
        postEventOperationPollingDev(EV_USER_REQUEST_UPDATE_DONE);
        break;

    case COMMAND_CLASS_SPECIFIC_NODE_REMOVE_FORCE:
        postEventOperationPollingDev(EV_USER_REMOVE_FORCE);
        pzwParam->ret = nodeRemoveForce(pzwParam->param1, &pzwParam->node);
        postEventOperationPollingDev(EV_USER_REMOVE_FORCE_DONE);
        break;
    
    case COMMAND_CLASS_SPECIFIC_NODE_CONTROLLER_CHANGE:
        postEventOperationPollingDev(EV_USER_CONTROLLER_CHANGE);
        pzwParam->ret = controllerChangeRole(pzwParam->param1);
        postEventOperationPollingDev(EV_USER_CONTROLLER_CHANGE_DONE);
        break;

    case COMMAND_CLASS_SPECIFIC_NODE_REPLACE_FAILED_NODE:
        pzwParam->ret = nodeReplaceFailedNode(pzwParam->param1, pzwParam->param2);
        break;

    case COMMAND_CLASS_SPECIFIC_NODE_IS_FAILED_NODE:
        postEventOperationPollingDev(EV_USER_IS_FAILED_NODE);
        pzwParam->ret = nodeIsFailedNode(pzwParam->param1, &pzwParam->param2);
        postEventOperationPollingDev(EV_USER_IS_FAILED_NODE_DONE);
        break;

    case COMMAND_CLASS_SPECIFIC_NODE_ADD_NODE_ZPC:
        pzwParam->ret = zwaveAddNodeZPC(pzwParam->param1);
        break;

    case COMMAND_CLASS_SPECIFIC_NODE_REMOVE_NODE_ZPC:
        pzwParam->ret = zwaveRemoveNodeZPC(pzwParam->param1);
        break;

    case COMMAND_CLASS_SPECIFIC_NODE_REMOVE_NODE_ZPC_AUTO:
        pzwParam->ret = zwaveAddRemoveCombinationNodeZPC(pzwParam->param1);
        break;

    case COMMAND_CLASS_SPECIFIC_NODE_GET_CONTROLLER_INFO_ZPC:
        postEventOperationPollingDev(EV_USER_GET_CONTROLLER_INFO_ZPC);
        pzwParam->ret = getControllerInfo(&pzwParam->param4, &pzwParam->param2, &pzwParam->node);
        postEventOperationPollingDev(EV_USER_GET_CONTROLLER_INFO_ZPC_DONE);
        break;

    case COMMAND_CLASS_SPECIFIC_NODE_REQUEST_SPECIFICATION_ZPC:
        postEventOperationPollingDev(EV_USER_REQUEST_SPECIFICATION_ZPC);
        pzwParam->ret = nodeSendRequestSpecification(pzwParam->param3, pzwParam->param4, pzwParam->param2,
                                                     pzwParam->cmd_set.cmd, pzwParam->cmd_set.cmd_length,
                                                     pzwParam->src_endpoint, pzwParam->dest_endpoint, pzwParam->scheme,
                                                     (char *)pzwParam->misc_data);
        postEventOperationPollingDev(EV_USER_REQUEST_SPECIFICATION_ZPC_DONE);
        break;
    case COMMAND_CLASS_SPECIFIC_CONTROLLER_BACKUP:
        BackupNVM((char *)pzwParam->misc_data);
        break;

    case COMMAND_CLASS_SPECIFIC_CONTROLLER_RESTORE:
        RestoreNVM((char *)pzwParam->misc_data);
        break;

    case COMMAND_CLASS_SPECIFIC_FIRMWARE_UPDATE_MD:
        if (pzwParam->param2)//enable update firmware md for dev
        {
            postEventOperationPollingDev(EV_USER_FIRMWARE_UPDATE_MD);
            nm.mode = NMM_UPDATE_FIRMWARE_DEVICE;
            nm.tmp_node = pzwParam->param1;
            nm.flags = pzwParam->scheme;//security scheme;
            PostEventUpdateFirmwareMd(EV_UPDATE_FIRMWARE_MD_START); 
            CheckFileFirmwareUpdate((char *)pzwParam->misc_data);           
        }
        else            
        {
            PostEventUpdateFirmwareMd(EV_CANCEL_UPDATE_FIRMWARE);                   
        }
        break;

    case COMMAND_CLASS_SPECIFIC_S2_BOOTSTRAPPING_KEX_REPORT:
        if (pzwParam->param3)
        {
            nm.flags |= NMS_FLAG_CSA_INCLUSION;
        }
        else
        {
            nm.flags &= ~NMS_FLAG_CSA_INCLUSION;   
        }
        mainlog(logDebug, "nm.flags %04x",nm.flags);
        pzwParam->ret = sec2_key_grant(pzwParam->param1, pzwParam->param2, pzwParam->param3);
        break;

    case COMMAND_CLASS_SPECIFIC_S2_BOOTSTRAPPING_CHALLENGE_RESPONSE:
        nm.flags |= NMS_FLAG_REPORT_DSK;//report dsk to app if device support S2 secure
        if (pzwParam->param2)
        {
            uint8_t dsk[2];
            dsk[0] = (pzwParam->param4 >> 8) & 0xFF;
            dsk[1] = (pzwParam->param4 >> 0) & 0xFF;
            mainlog(logDebug, "dsk --> %02X%02X", dsk[0], dsk[1]);
            /*copy dsk_user_ipnut to just_included_dsk to send to app*/
            nm.just_included_dsk[0] = (pzwParam->param4 >> 8) & 0xFF;
            nm.just_included_dsk[1] = (pzwParam->param4 >> 0) & 0xFF;
            pzwParam->ret = sec2_dsk_accept(pzwParam->param1, dsk, 2);
        }
        else
        {
            uint8_t dsk[4];
            dsk[0] = (pzwParam->param4 >> 24) & 0xFF;
            dsk[1] = (pzwParam->param4 >> 16) & 0xFF;
            dsk[2] = (pzwParam->param4 >> 8) & 0xFF;
            dsk[3] = (pzwParam->param4 >> 0) & 0xFF;
            mainlog(logDebug, "dsk-->");
            hexdump(dsk, 4);
            pzwParam->ret = sec2_dsk_accept(pzwParam->param1, dsk, 4);
        }
        break;

    case COMMAND_CLASS_SPECIFIC_PRIORITY_ROUTE:
        postEventOperationPollingDev(EV_USER_PRIORITY_ROUTE);
        bNodeID = pzwParam->param2; 
        uint8_t pRoute[ROUTECACHE_LINE_SIZE];

        if (pzwParam->param1) 
        {
            if (ZW_PRIORITY_ROUTE_ZW_NO_VALID == serialApiGetLastWorkingRoute(bNodeID, pRoute))
            {
                pzwParam->ret = 1;
                mainlog(logDebug, "No valid LWR exists for %03u", bNodeID);
            }   
            else
            {
                pzwParam->ret = 0;
                mainlog(logDebug, "Existing valid LWR for %03u to - %03u,%03u,%03u,%03u at %s", 
                        bNodeID, 
                        pRoute[ROUTECACHE_LINE_REPEATER_1_INDEX], 
                        pRoute[ROUTECACHE_LINE_REPEATER_2_INDEX], 
                        pRoute[ROUTECACHE_LINE_REPEATER_3_INDEX], 
                        pRoute[ROUTECACHE_LINE_REPEATER_4_INDEX],
                        (ZW_LAST_WORKING_ROUTE_SPEED_100K == pRoute[ROUTECACHE_LINE_CONF_INDEX]) ? "100k" :
                        (ZW_LAST_WORKING_ROUTE_SPEED_40K == pRoute[ROUTECACHE_LINE_CONF_INDEX]) ? "40k" :
                        (ZW_LAST_WORKING_ROUTE_SPEED_9600 == pRoute[ROUTECACHE_LINE_CONF_INDEX]) ? "9.6k" : "Auto");

                pzwParam->data_out.cmd_length = ROUTECACHE_LINE_SIZE;
                memcpy(pzwParam->data_out.cmd, pRoute, ROUTECACHE_LINE_SIZE);
            }
            
        }
        else
        {
            int bRouteLen = 0;
            uint8_t bRouteSpeed;
            uint8_t abFuncIDToSupport[] = {FUNC_ID_ZW_SET_LAST_WORKING_ROUTE};
            bool fIsZW_SetLastWorkingRouteSupported = NetworkManagement_IsFuncIDsSupported(abFuncIDToSupport, sizeof(abFuncIDToSupport));

            mainlog(logDebug, "Inject new LWR for %03u", bNodeID);

            if (false == fIsZW_SetLastWorkingRouteSupported)
            {
                mainlog(logDebug, "Inject new LWR NOT supported by attached SerialAPI module");
                break;
            }

            memset(pRoute, 0, sizeof(pRoute));

            bRouteLen = pzwParam->cmd_set.cmd_length;
            memcpy(pRoute, pzwParam->cmd_set.cmd, bRouteLen);
            
            bRouteSpeed = pzwParam->param3;
            pRoute[ROUTECACHE_LINE_CONF_INDEX] = bRouteSpeed;

            if ((0 < bNodeID) && (ZW_MAX_NODES >= bNodeID))
            {
                mainlog(logDebug, "Setting LWR for %03u to - %03u,%03u,%03u,%03u at %s", 
                           bNodeID, 
                           pRoute[ROUTECACHE_LINE_REPEATER_1_INDEX], 
                           pRoute[ROUTECACHE_LINE_REPEATER_2_INDEX], 
                           pRoute[ROUTECACHE_LINE_REPEATER_3_INDEX], 
                           pRoute[ROUTECACHE_LINE_REPEATER_4_INDEX],
                           (ZW_LAST_WORKING_ROUTE_SPEED_100K == pRoute[ROUTECACHE_LINE_CONF_INDEX]) ? "100k" :
                            (ZW_LAST_WORKING_ROUTE_SPEED_40K == pRoute[ROUTECACHE_LINE_CONF_INDEX]) ? "40k" :
                             (ZW_LAST_WORKING_ROUTE_SPEED_9600 == pRoute[ROUTECACHE_LINE_CONF_INDEX]) ? "9.6k" : "Auto");
                /* Write new LWR Entry for current NodeID */
                if (false == serialApiSetLastWorkingRoute(bNodeID, pRoute))
                {
                    pzwParam->ret = 1;
                    mainlog(logDebug, "Failed could not set new LWR for nodeID %03u", bNodeID);
                }
                else
                {
                    pzwParam->ret = 0;
                    mainlog(logDebug, "Success new LWR set for nodeID %03u", bNodeID);
                }
            }
            else
            {
                pzwParam->ret = 1;
                mainlog(logDebug, "Current NodeID %03u not valid must be inside (0 < NodeID <= 232)");
            }
        }
        postEventOperationPollingDev(EV_USER_PRIORITY_ROUTE_DONE);
        break;    

    case COMMAND_CLASS_SPECIFIC_REQUEST_NIF:
        postEventOperationPollingDev(EV_USER_REQUEST_NIF);
        pzwParam->ret = nodeRequestNodeInfo(pzwParam->param1);
        postEventOperationPollingDev(EV_USER_REQUEST_NIF_DONE);
        break;

    case COMMAND_CLASS_SPECIFIC_SEND_MY_NIF:
        postEventOperationPollingDev(EV_USER_SEND_MY_NIF);
        pzwParam->ret = nodeSendMyNodeInfo(pzwParam->param1);
        postEventOperationPollingDev(EV_USER_SEND_MY_NIF_DONE);
        break;

    case COMMAND_CLASS_SPECIFIC_GET_SECURE_KEY:
        postEventOperationPollingDev(EV_USER_GET_SECURE_KEY);
        pzwParam->ret = getSecureKeyAndSave((char *)pzwParam->misc_data);
        postEventOperationPollingDev(EV_USER_GET_SECURE_KEY_DONE);
        break;

    case COMMAND_CLASS_SPECIFIC_UPDATE_FIRMWARE:
    //return 0 if update successfully
    //return -1 if update fail
        postEventOperationPollingDev(EV_USER_UPDATE_FIRMWARE);
        pzwParam->ret = ZWPGM_UpdateFirmware(NULL, (char *)pzwParam->misc_data);
        postEventOperationPollingDev(EV_USER_UPDATE_FIRMWARE_DONE);
        break;

    case COMMAND_CLASS_SPECIFIC_GET_VERSION_FIRMWARE:
        postEventOperationPollingDev(EV_USER_GET_VERSION_FIRMWARE);
        pzwParam->ret = getFirmwareVersion((uint8_t *)pzwParam->misc_data);
        postEventOperationPollingDev(EV_USER_GET_VERSION_FIRMWARE_DONE);
        break;
    case COMMAND_CLASS_SPECIFIC_CHANGE_CHANNEL_TV:
        pzwParam->ret = changeChannelOfTV(pzwParam);//param1: commandIndex, param4: channelId
        break;
    default:
        break;
    }

    return 0;
}
