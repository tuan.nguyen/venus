/****************************************************************************
 *
 *
 * Copyright (c) 2014-2015
 * Veriksytems, Inc.
 * All Rights Reserved
 *
 *
****************************************************************************/
#include <pthread.h>

#include "zw_api.h"
#include "Transport_ApplCmdHandlerEx.h"
#include "util_crc.h"
#include "timer.h"
#include "controlled_CommandClassUpdateFirmware.h"
#include "ZW_SendDataAppl.h"
#include "ZW_TransportSecurity.h"
#include "ZW_PollApi.h"

//variable for interview device process.
extern pthread_mutex_t CriticalMutexNotification;

extern notify_queue_t *g_notify_buf;
#define FRAME_METER_REPORT_V1_LENGHT 4
#define TOTAL_DELTA_TIME_LENGHT_AND_SIZE_PARAM_POS 6
#define METER_VALUE_PARAM_POS 4
#define NEW_KEY_HELD_DOWN_NOTIFICATION 60000
#define ANY_KEY_HELD_DOWN_NOTIFICATION 400
#define LENGHT_KEY_ATTRIBUTES_AND_SCENE_NUMBER 2

/*
If not receiving a new Key Held Down notification within 60 seconds after the most recent Key
Held Down notification, a receiving node MUST respond as if it received a Key Release
notification.
*/
/*
Initially, the controller SHOULD time out if not receiving any Key Held Down Notification
refresh after 400ms and consider this to be a Key Up Notification
*/
pthread_mutex_t handleKeyHeldDownMessageListMutex;
handleKeyHeldDownMessage_t globalhandleKeyHeldDownMessage;
handleSequenceNumber_t globalHandleSequenceNumber;

void initKeyHeldDownMessageList(void)
{
    VR_INIT_LIST_HEAD(&globalhandleKeyHeldDownMessage.list);
    VR_INIT_LIST_HEAD(&globalHandleSequenceNumber.list);
    pthread_mutex_init(&handleKeyHeldDownMessageListMutex, 0);
}

void addKeyHeldDownMessageList(void *id)
{
    pthread_mutex_lock(&handleKeyHeldDownMessageListMutex);
    handleKeyHeldDownMessage_t *input = (handleKeyHeldDownMessage_t *)id;
    VR_(list_add_tail)(&(input->list), &(globalhandleKeyHeldDownMessage.list));

    pthread_mutex_unlock(&handleKeyHeldDownMessageListMutex);
}

void removeKeyHeldDownMessageList(uint16_t extendId)
{
    mainlog(logDebug, "remove Message Key Held From List for extendId: 0x%04X", extendId);
    pthread_mutex_lock(&handleKeyHeldDownMessageListMutex);
    handleKeyHeldDownMessage_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &globalhandleKeyHeldDownMessage.list)
    {
        tmp = VR_(list_entry)(pos, handleKeyHeldDownMessage_t, list);
        if(tmp->extendId == extendId)
        {
            VR_(list_del)(pos);
            free(tmp);
        }
    }
    pthread_mutex_unlock(&handleKeyHeldDownMessageListMutex);
}

void removeKeyHeldDownMessageListFromNodeId(uint8_t nodeId)
{
    mainlog(logDebug, "remove Message Key Held From List for nodeId: 0x%02X", nodeId);
    pthread_mutex_lock(&handleKeyHeldDownMessageListMutex);
    handleKeyHeldDownMessage_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &globalhandleKeyHeldDownMessage.list)
    {
        tmp = VR_(list_entry)(pos, handleKeyHeldDownMessage_t, list);
        if((tmp->extendId & 0x00FF) == nodeId)
        {
            mainlog(logDebug, "remove Message Key Held From List for extendId: 0x%04X", tmp->extendId);
            VR_(list_del)(pos);
            free(tmp);
        }
    }
    pthread_mutex_unlock(&handleKeyHeldDownMessageListMutex);
}

void removeAllKeyHeldDownMessageList(void)
{
    pthread_mutex_lock(&handleKeyHeldDownMessageListMutex);
    handleKeyHeldDownMessage_t *tmp = NULL;
    mainlog(logDebug, "remove All Message Key Held From List");
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &globalhandleKeyHeldDownMessage.list)
    {
        tmp = VR_(list_entry)(pos, handleKeyHeldDownMessage_t, list);
        VR_(list_del)(pos);
        free(tmp);
    }
    pthread_mutex_unlock(&handleKeyHeldDownMessageListMutex);
}
handleKeyHeldDownMessage_t *getKeyHeldDownMessageListFromExtendId(uint16_t extendId)
{
    if(!extendId)
    {
        return NULL;
    }

    handleKeyHeldDownMessage_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &(globalhandleKeyHeldDownMessage.list), list)
    {
        if(tmp->extendId == extendId)
        {
            return tmp;
        }
    }

    return NULL;
}

void addSequenceNumberLastList(void *id)
{
    pthread_mutex_lock(&handleKeyHeldDownMessageListMutex);
    handleSequenceNumber_t *input = (handleSequenceNumber_t *)id;
    VR_(list_add_tail)(&(input->list), &(globalHandleSequenceNumber.list));

    pthread_mutex_unlock(&handleKeyHeldDownMessageListMutex);
}


void removeSequenceNumberFromNodeId(uint8_t nodeId)
{
    mainlog(logDebug, "remove Sequence Number From List for nodeId: 0x%02X", nodeId);
    pthread_mutex_lock(&handleKeyHeldDownMessageListMutex);
    handleSequenceNumber_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &globalHandleSequenceNumber.list)
    {
        tmp = VR_(list_entry)(pos, handleSequenceNumber_t, list);
        if((tmp->extendId & 0x00FF) == nodeId)
        {    
            mainlog(logDebug, "remove Sequence Number From List for extendId: 0x%04X", tmp->extendId);
            VR_(list_del)(pos);
            free(tmp);
        }
    }
    pthread_mutex_unlock(&handleKeyHeldDownMessageListMutex);
}

void removeAllSequenceNumberFromList(void)
{
    pthread_mutex_lock(&handleKeyHeldDownMessageListMutex);
    handleSequenceNumber_t *tmp = NULL;
    mainlog(logDebug, "remove All Sequence Number From List");
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &globalHandleSequenceNumber.list)
    {
        tmp = VR_(list_entry)(pos, handleSequenceNumber_t, list);
        VR_(list_del)(pos);
        free(tmp);
    }
    pthread_mutex_unlock(&handleKeyHeldDownMessageListMutex);
}

handleSequenceNumber_t *getSequenceNumberFromExtendId(uint16_t extendId)
{
    if(!extendId)
    {
        return NULL;
    }

    handleSequenceNumber_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &(globalHandleSequenceNumber.list), list)
    {
        if(tmp->extendId == extendId)
        {
            return tmp;
        }
    }

    return NULL;
}

//check key released message from device
//reset old_message_notify
void ZCB_TimerOutHandlerMessageKeyReleased(void *data)
{
    uint16_t extendId = *(uint16_t*)data;
    mainlog(logDebug, "Timeout for Handler Message Key Released for nodeId: 0x%04X", extendId);
    handleKeyHeldDownMessage_t *tempMessageHeldDown = getKeyHeldDownMessageListFromExtendId(extendId);
    if(!tempMessageHeldDown)
    {
        return;
    }

    timerCancel(&(tempMessageHeldDown->timerId));

    //reset old key down message notification.
    if(!tempMessageHeldDown->receiveMessageKeyReleased)
    {
        mainlog(logDebug, "---> Send Key Released Notification...");

        NOTIFY_TX_BUFFER_T pTxNotify;
        pTxNotify.CmdClassNotify.source_endpoint = tempMessageHeldDown->p.sendpoint;
        pTxNotify.CmdClassNotify.dest_endpoint = tempMessageHeldDown->p.dendpoint;
        pTxNotify.CmdClassNotify.source_nodeId = tempMessageHeldDown->p.snode;
        pTxNotify.CmdClassNotify.dest_nodeId = tempMessageHeldDown->p.dnode;
        pTxNotify.CmdClassNotify.node_type = GetCacheEntryNodeTypeGeneric(tempMessageHeldDown->p.snode);
        pTxNotify.CmdClassNotify.node_is_listening = GetCacheEntryNodeMode(tempMessageHeldDown->p.snode);
        pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_CENTRAL_SCENE;
        pTxNotify.CmdClassNotify.cmd = CENTRAL_SCENE_NOTIFICATION;
        pTxNotify.CmdClassNotify.central_scene_notification.key_attributes = CENTRAL_SCENE_NOTIFICATION_KEY_ATTRIBUTES_KEY_RELEASED_V2;
        pTxNotify.CmdClassNotify.central_scene_notification.scene_number = tempMessageHeldDown->sceneNumber;
        pTxNotify.CmdClassNotify.central_scene_notification.sequence_number = tempMessageHeldDown->sequenceNumber;
        PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        removeKeyHeldDownMessageList(extendId);
    }
    
    if(data)
    {
        free(data);
    }
}

void StartTimerHandlerMessageKeyReleased(handleKeyHeldDownMessage_t *tmp) /* IN Timeout in seconds */
{

    timerCancel(&(tmp->timerId));
    timerStart(&(tmp->timerId), ZCB_TimerOutHandlerMessageKeyReleased, (void*)&tmp->extendId, tmp->timeout, TIMER_ONETIME);
    mainlog(logDebug, "Start Timer Handler Message Key Released for extendId: 0x%04X", tmp->extendId);

}

/*========================================================================================================
================================= check central scene report =============================================
============================= Return true if message is duplicated =======================================*/
bool CheckCentralSceneReportCommand(ts_param_t *p, uint8_t sceneNumber, uint8_t sequenceNumber, uint16_t timeout, uint8_t keyAyttributes)
{
    uint16_t extendId = (p->sendpoint << 8) + p->snode;
    handleKeyHeldDownMessage_t *tmp = getKeyHeldDownMessageListFromExtendId(extendId);
    if((tmp->sceneNumber == sceneNumber) &&
            (tmp->keyAyttributes == keyAyttributes))
    {
        StartTimerHandlerMessageKeyReleased(tmp);
        return true;//duplicate        
    }
    else//no duplicate
    {
        StartTimerHandlerMessageKeyReleased(tmp);
        return false;
    }
}


/*Transport_ApplicationCommandHandlerEx
    RET received_frame_status_t                  */
received_frame_status_t
Transport_ApplicationCommandHandlerEx(
    ts_param_t *p,      /* IN receive options of type ts_param_t  */
    uint8_t *pCmd,      /* IN  Payload from the received frame */
    uint16_t cmdLength) /*IN secure of Device Node*/
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    received_frame_status_t frame_status = RECEIVED_FRAME_STATUS_NO_SUPPORT;
    uint8_t tmp[14];
    uint8_t i = 0;
    //can't compare if new_message_from_device start with zero
    memset(tmp, 'Z', 14);

    mainlog(logDebug, "Transport_ApplicationCommandHandlerEx");
    hexdump(pCmd, cmdLength);

    pTxNotify.CmdClassNotify.source_endpoint = p->sendpoint;
    pTxNotify.CmdClassNotify.dest_endpoint = p->dendpoint;
    pTxNotify.CmdClassNotify.source_nodeId = p->snode;
    pTxNotify.CmdClassNotify.dest_nodeId = p->dnode;
    pTxNotify.CmdClassNotify.node_type = GetCacheEntryNodeTypeGeneric(p->snode);
    pTxNotify.CmdClassNotify.node_is_listening = GetCacheEntryNodeMode(p->snode);
    pTxNotify.CmdClassNotify.pollingReport = 0;
    uint16_t extendId = (p->sendpoint << 8) + p->snode;
    setNoPollingFail(extendId, 0);
    
    if((MODE_REMOVE_FORCE == GetCacheEntryNodeMode(p->snode)) || (MODE_PROBING == GetCacheEntryNodeMode(p->snode)))
    {
        //device has been removed, not exist on application.
        mainlog(logDebug, "device has been removed, not exist on application");
        return frame_status;
    }
    switch (*(pCmd + CMDBUF_CMDCLASS_OFFSET))
    {
    case COMMAND_CLASS_ALARM:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case ALARM_REPORT:
            //ZPC notify
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_ALARM;
            pTxNotify.CmdClassNotify.cmd = ALARM_REPORT;
            if (cmdLength == 4)
            {
                /*add 2 bytes alarm pattern to meta_data */
                memcpy(tmp, pCmd + CMDBUF_CMD_OFFSET + 1, 2);
                arrayToString(pTxNotify.CmdClassNotify.meta_data.alarm_pattern, tmp, 14);

                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.alarm_report_v1.alarm_type = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.alarm_report_v1.alarm_level = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            }
            else if (cmdLength > 4)
            {
                /*add 7 bytes alarm pattern to meta_data */
                memcpy(tmp, pCmd + CMDBUF_CMD_OFFSET + 1, 7);
                arrayToString(pTxNotify.CmdClassNotify.meta_data.alarm_pattern, tmp, 14);

                int no_event_params = (*(pCmd + CMDBUF_CMD_OFFSET + 7)) & 0x1F;
                if (cmdLength == (no_event_params + 9))
                {
                    pTxNotify.CmdClassNotify.version = 2;
                    pTxNotify.CmdClassNotify.alarm_report_v2.alarm_type = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                    pTxNotify.CmdClassNotify.alarm_report_v2.alarm_level = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                    pTxNotify.CmdClassNotify.alarm_report_v2.zensor_src_id = *(pCmd + CMDBUF_CMD_OFFSET + 3);
                    pTxNotify.CmdClassNotify.alarm_report_v2.zwave_alarm_status = *(pCmd + CMDBUF_CMD_OFFSET + 4);
                    pTxNotify.CmdClassNotify.alarm_report_v2.zwave_alarm_type = *(pCmd + CMDBUF_CMD_OFFSET + 5);
                    pTxNotify.CmdClassNotify.alarm_report_v2.zwave_alarm_event = *(pCmd + CMDBUF_CMD_OFFSET + 6);
                    pTxNotify.CmdClassNotify.alarm_report_v2.no_event_params = *(pCmd + CMDBUF_CMD_OFFSET + 7);
                    memcpy((uint8_t *)pTxNotify.CmdClassNotify.alarm_report_v2.event_param, pCmd + CMDBUF_CMD_OFFSET + 8,
                           pTxNotify.CmdClassNotify.alarm_report_v2.no_event_params);
                }
                else if (cmdLength > (no_event_params + 9))
                {
                    pTxNotify.CmdClassNotify.version = 3;
                    pTxNotify.CmdClassNotify.notification_report.alarm_type = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                    pTxNotify.CmdClassNotify.notification_report.alarm_level = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                    pTxNotify.CmdClassNotify.notification_report.zwave_alarm_status = *(pCmd + CMDBUF_CMD_OFFSET + 4);
                    pTxNotify.CmdClassNotify.notification_report.zwave_alarm_type = *(pCmd + CMDBUF_CMD_OFFSET + 5);
                    pTxNotify.CmdClassNotify.notification_report.zwave_alarm_event = *(pCmd + CMDBUF_CMD_OFFSET + 6);
                    pTxNotify.CmdClassNotify.notification_report.no_event_params = (*(pCmd + CMDBUF_CMD_OFFSET + 7)) & 0x1F;
                    pTxNotify.CmdClassNotify.notification_report.sequence = ((*(pCmd + CMDBUF_CMD_OFFSET + 7)) & 0x80) >> 7;
                    memset((uint8_t *)pTxNotify.CmdClassNotify.notification_report.event_param, 0, 32);
                    memcpy((uint8_t *)pTxNotify.CmdClassNotify.notification_report.event_param, pCmd + CMDBUF_CMD_OFFSET + 8,
                           pTxNotify.CmdClassNotify.notification_report.no_event_params);
                    pTxNotify.CmdClassNotify.notification_report.sequence_number = *(pCmd + CMDBUF_CMD_OFFSET + 8 + no_event_params);
                }
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));

            break;

        case ALARM_TYPE_SUPPORTED_REPORT_V2:

            //ZPC notify
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_ALARM;
            pTxNotify.CmdClassNotify.cmd = ALARM_TYPE_SUPPORTED_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.alarm_supported_report.v1_alarm = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 7;
            pTxNotify.CmdClassNotify.alarm_supported_report.no_bit_masks = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x1F);
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.alarm_supported_report.bit_mask, pCmd + CMDBUF_CMD_OFFSET + 2, pTxNotify.CmdClassNotify.alarm_supported_report.no_bit_masks);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));

            break;

        case EVENT_SUPPORTED_REPORT_V3:

            //ZPC notify
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_ALARM;
            pTxNotify.CmdClassNotify.cmd = EVENT_SUPPORTED_REPORT_V3;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.event_supported_report.notification_type = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.event_supported_report.no_bit_masks = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x1F);
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.event_supported_report.bit_mask,
                   pCmd + CMDBUF_CMD_OFFSET + 3, pTxNotify.CmdClassNotify.event_supported_report.no_bit_masks);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));

            break;
        }
    }
    break;

    case COMMAND_CLASS_SENSOR_ALARM:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case SENSOR_ALARM_REPORT:

            //ZPC notify
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SENSOR_ALARM;
            pTxNotify.CmdClassNotify.cmd = SENSOR_ALARM_REPORT;

            /*add 4 bytes alarm pattern to meta_data */
            memcpy(tmp + 8, pCmd + CMDBUF_CMD_OFFSET + 2, 4);
            arrayToString(pTxNotify.CmdClassNotify.meta_data.alarm_pattern, tmp, 14);

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.sensor_alarm_report.src_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.sensor_alarm_report.sensor_type = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.sensor_alarm_report.sensor_state = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.sensor_alarm_report.seconds = *(pCmd + CMDBUF_CMD_OFFSET + 4);
            pTxNotify.CmdClassNotify.sensor_alarm_report.seconds = (pTxNotify.CmdClassNotify.sensor_alarm_report.seconds << 8) + (*(pCmd + CMDBUF_CMD_OFFSET + 5));

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));

            break;

        case SENSOR_ALARM_SUPPORTED_REPORT:

            //ZPC notify
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SENSOR_ALARM;
            pTxNotify.CmdClassNotify.cmd = SENSOR_ALARM_SUPPORTED_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.sensor_alarm_supported_report.no_bit_masks = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.sensor_alarm_supported_report.bit_mask, pCmd + CMDBUF_CMD_OFFSET + 2, pTxNotify.CmdClassNotify.sensor_alarm_supported_report.no_bit_masks);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
            break;
        }
    }
    break;

    case COMMAND_CLASS_HAIL:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case HAIL:
        {
            //send basic get to device
            ts_param_t para;
            para.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
            para.scheme = NO_SCHEME;

            para.snode = p->dnode;
            para.sendpoint = 0;

            para.dnode = p->snode;
            para.dendpoint = 0;

            pTxBuf.ZW_BasicSetFrame.cmdClass = COMMAND_CLASS_BASIC; /* The command class */
            pTxBuf.ZW_BasicSetFrame.cmd = BASIC_GET;                /* The command */

            Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                                     sizeof(ZW_BASIC_GET_FRAME), &para, NULL, NULL);
            //ZPC notify
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_HAIL;
            pTxNotify.CmdClassNotify.cmd = HAIL;
            pTxNotify.CmdClassNotify.version = 1;

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_SENSOR_BINARY:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case SENSOR_BINARY_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SENSOR_BINARY;
            pTxNotify.CmdClassNotify.cmd = SENSOR_BINARY_REPORT;

            if (cmdLength == 3)
            {
                /*add 1 byte alarm pattern to meta_data */
                memcpy(tmp + 12, pCmd + CMDBUF_CMD_OFFSET + 1, 1);
                arrayToString(pTxNotify.CmdClassNotify.meta_data.alarm_pattern, tmp, 14);

                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.sensor_binary_report_v1.sensor_value = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            }
            else if (cmdLength >= 3)
            {
                /*add 2 byte alarm pattern to meta_data */
                memcpy(tmp + 12, pCmd + CMDBUF_CMD_OFFSET + 1, 2);
                arrayToString(pTxNotify.CmdClassNotify.meta_data.alarm_pattern, tmp, 14);

                pTxNotify.CmdClassNotify.version = 2;
                pTxNotify.CmdClassNotify.sensor_binary_report_v2.sensor_value = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.sensor_binary_report_v2.sensor_type = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case SENSOR_BINARY_SUPPORTED_SENSOR_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SENSOR_BINARY;
            pTxNotify.CmdClassNotify.cmd = SENSOR_BINARY_SUPPORTED_SENSOR_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 2;
            pTxNotify.CmdClassNotify.sensor_binary_supported_report.no_bit_masks = cmdLength - 2;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.sensor_binary_supported_report.bit_mask, pCmd + CMDBUF_CMD_OFFSET + 1, pTxNotify.CmdClassNotify.sensor_binary_supported_report.no_bit_masks);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_SWITCH_ALL:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case SWITCH_ALL_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SWITCH_ALL;
            pTxNotify.CmdClassNotify.cmd = SWITCH_ALL_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.switch_all_report.mode = *(pCmd + CMDBUF_CMD_OFFSET + 1);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_SWITCH_BINARY:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case SWITCH_BINARY_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SWITCH_BINARY;
            pTxNotify.CmdClassNotify.cmd = SWITCH_BINARY_REPORT;

            if (cmdLength == 3)
            {
                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.switch_binary_report_v1.value = *(pCmd + CMDBUF_CMD_OFFSET + 1);    
            }
            else if (cmdLength > 3)
            {
                pTxNotify.CmdClassNotify.version = 2;
                pTxNotify.CmdClassNotify.switch_binary_report_v2.value = *(pCmd + CMDBUF_CMD_OFFSET + 1);    
                pTxNotify.CmdClassNotify.switch_binary_report_v2.target_value = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                pTxNotify.CmdClassNotify.switch_binary_report_v2.duration = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_VERSION:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case VERSION_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_VERSION;
            pTxNotify.CmdClassNotify.cmd = VERSION_REPORT;

            if (cmdLength == 7)
            {
                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.version_report_v1.zw_lib_type = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.version_report_v1.zw_protocol_version = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                pTxNotify.CmdClassNotify.version_report_v1.zw_protocol_sub_version = *(pCmd + CMDBUF_CMD_OFFSET + 3);
                pTxNotify.CmdClassNotify.version_report_v1.app_version = *(pCmd + CMDBUF_CMD_OFFSET + 4);
                pTxNotify.CmdClassNotify.version_report_v1.app_sub_version = *(pCmd + CMDBUF_CMD_OFFSET + 5);
            }
            else if (cmdLength > 7)
            {
                pTxNotify.CmdClassNotify.version = 2;
                pTxNotify.CmdClassNotify.version_report_v2.zw_lib_type = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.version_report_v2.zw_protocol_version = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                pTxNotify.CmdClassNotify.version_report_v2.zw_protocol_sub_version = *(pCmd + CMDBUF_CMD_OFFSET + 3);
                pTxNotify.CmdClassNotify.version_report_v2.firmware0_version = *(pCmd + CMDBUF_CMD_OFFSET + 4);
                pTxNotify.CmdClassNotify.version_report_v2.firmware0_sub_version = *(pCmd + CMDBUF_CMD_OFFSET + 5);
                pTxNotify.CmdClassNotify.version_report_v2.hardware_version = *(pCmd + CMDBUF_CMD_OFFSET + 6);
                pTxNotify.CmdClassNotify.version_report_v2.no_firmware_targets = *(pCmd + CMDBUF_CMD_OFFSET + 7);
                int index;
                for (index = 0; index < pTxNotify.CmdClassNotify.version_report_v2.no_firmware_targets; index++)
                {
                    pTxNotify.CmdClassNotify.version_report_v2.fw_target_list[index].firmware_version = *(pCmd + CMDBUF_CMD_OFFSET + 8 + index * 2);
                    pTxNotify.CmdClassNotify.version_report_v2.fw_target_list[index].firmware_sub_version = *(pCmd + CMDBUF_CMD_OFFSET + 9 + index * 2);
                }
            }
            if (nm.mode == NMM_UPDATE_FIRMWARE_DEVICE)
            {    
                PostEventUpdateFirmwareMd(EV_GET_APPLICATION_VER_OK);
            }
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case VERSION_COMMAND_CLASS_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_VERSION;
            pTxNotify.CmdClassNotify.cmd = VERSION_COMMAND_CLASS_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.version_cmd_class_report.request_cmd_class = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.version_cmd_class_report.cmd_class_version = *(pCmd + CMDBUF_CMD_OFFSET + 2);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_PROTECTION:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case PROTECTION_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_PROTECTION;
            pTxNotify.CmdClassNotify.cmd = PROTECTION_REPORT;

            if (cmdLength == 3)
            {
                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.protection_report_v1.protection_state = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            }
            else if (cmdLength > 3)
            {
                pTxNotify.CmdClassNotify.version = 2;
                pTxNotify.CmdClassNotify.protection_report_v2.local_protection_state = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x0F;
                pTxNotify.CmdClassNotify.protection_report_v2.rf_protection_state = (*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x0F;
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case PROTECTION_SUPPORTED_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_PROTECTION;
            pTxNotify.CmdClassNotify.cmd = PROTECTION_SUPPORTED_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.protection_supported_report.exclusive_control = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x02) >> 1;
            pTxNotify.CmdClassNotify.protection_supported_report.timeout = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x01) >> 0;
            pTxNotify.CmdClassNotify.protection_supported_report.local_protection_state = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.protection_supported_report.local_protection_state = ((pTxNotify.CmdClassNotify.protection_supported_report.local_protection_state) << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.protection_supported_report.rf_protection_state = *(pCmd + CMDBUF_CMD_OFFSET + 4);
            pTxNotify.CmdClassNotify.protection_supported_report.rf_protection_state = ((pTxNotify.CmdClassNotify.protection_supported_report.rf_protection_state) << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 5);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case PROTECTION_EC_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_PROTECTION;
            pTxNotify.CmdClassNotify.cmd = PROTECTION_EC_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.protection_ec_report.node_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case PROTECTION_TIMEOUT_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_PROTECTION;
            pTxNotify.CmdClassNotify.cmd = PROTECTION_TIMEOUT_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.protection_timeout_report.timeout = *(pCmd + CMDBUF_CMD_OFFSET + 1);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_MULTI_CHANNEL_V3:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case MULTI_CHANNEL_END_POINT_REPORT_V3:
        {
            
            if (cmdLength == 4)
            {
                pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_MULTI_CHANNEL_V3;
                pTxNotify.CmdClassNotify.cmd = MULTI_CHANNEL_END_POINT_REPORT_V3;

                pTxNotify.CmdClassNotify.version = 3;
                pTxNotify.CmdClassNotify.multi_channel_report.dynamic = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 7;
                pTxNotify.CmdClassNotify.multi_channel_report.identical = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x40) >> 6;
                pTxNotify.CmdClassNotify.multi_channel_report.no_endpoint = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x7F);
            }
            else
            {
                pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_MULTI_CHANNEL_V4;
                pTxNotify.CmdClassNotify.cmd = MULTI_CHANNEL_END_POINT_REPORT_V4;

                pTxNotify.CmdClassNotify.version = 4;
                pTxNotify.CmdClassNotify.multi_channel_endpoint_report_v4.dynamic = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 7;
                pTxNotify.CmdClassNotify.multi_channel_endpoint_report_v4.identical = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x40) >> 6;
                pTxNotify.CmdClassNotify.multi_channel_endpoint_report_v4.individual_end_points = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x7F);
                pTxNotify.CmdClassNotify.multi_channel_endpoint_report_v4.aggregated_end_points = ((*(pCmd + CMDBUF_CMD_OFFSET + 3)) & 0x7F);
            }
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case MULTI_CHANNEL_CAPABILITY_REPORT_V3:
        {
            
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_MULTI_CHANNEL_V3;
            pTxNotify.CmdClassNotify.cmd = MULTI_CHANNEL_CAPABILITY_REPORT_V3;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.multi_channel_capability_report.dynamic = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 7;
            pTxNotify.CmdClassNotify.multi_channel_capability_report.endpoint = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x7F);
            pTxNotify.CmdClassNotify.multi_channel_capability_report.generic_device_class = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.multi_channel_capability_report.specific_device_class = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.multi_channel_capability_report.no_cmd_class = cmdLength - 5;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.multi_channel_capability_report.cmd_class_list, pCmd + CMDBUF_CMD_OFFSET + 4, pTxNotify.CmdClassNotify.multi_channel_capability_report.no_cmd_class);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case MULTI_CHANNEL_END_POINT_FIND_REPORT_V3:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_MULTI_CHANNEL_V3;
            pTxNotify.CmdClassNotify.cmd = MULTI_CHANNEL_END_POINT_FIND_REPORT_V3;

            pTxNotify.CmdClassNotify.multi_channel_endpoint_find_report.report_to_follow = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.multi_channel_endpoint_find_report.generic_device_class = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.multi_channel_endpoint_find_report.specific_device_class = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.multi_channel_endpoint_find_report.no_endpoint = cmdLength - 5;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.multi_channel_endpoint_find_report.endpoint_list, pCmd + CMDBUF_CMD_OFFSET + 4, pTxNotify.CmdClassNotify.multi_channel_endpoint_find_report.no_endpoint);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case MULTI_CHANNEL_AGGREGATED_MEMBERS_REPORT_V4:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_MULTI_CHANNEL_V4;
            pTxNotify.CmdClassNotify.cmd = MULTI_CHANNEL_AGGREGATED_MEMBERS_REPORT_V4;

            pTxNotify.CmdClassNotify.multi_channel_aggregated_members_report_v4.aggregated_end_point = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.multi_channel_aggregated_members_report_v4.no_bit_masks = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.multi_channel_aggregated_members_report_v4.bit_mask,
                   pCmd + CMDBUF_CMD_OFFSET + 3, pTxNotify.CmdClassNotify.multi_channel_aggregated_members_report_v4.no_bit_masks);
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_ASSOCIATION_GRP_INFO:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case ASSOCIATION_GROUP_NAME_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_ASSOCIATION_GRP_INFO;
            pTxNotify.CmdClassNotify.cmd = ASSOCIATION_GROUP_NAME_REPORT;
            memset((uint8_t *)pTxNotify.CmdClassNotify.association_group_name_report.name, 0, MAX_BUFFER_LENGTH);
            pTxNotify.CmdClassNotify.association_group_name_report.group_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.association_group_name_report.name_len = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.association_group_name_report.name, pCmd + CMDBUF_CMD_OFFSET + 3, pTxNotify.CmdClassNotify.association_group_name_report.name_len);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case ASSOCIATION_GROUP_COMMAND_LIST_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_ASSOCIATION_GRP_INFO;
            pTxNotify.CmdClassNotify.cmd = ASSOCIATION_GROUP_COMMAND_LIST_REPORT;

            pTxNotify.CmdClassNotify.association_group_cmd_list_report.group_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.association_group_cmd_list_report.list_len = *(pCmd + CMDBUF_CMD_OFFSET + 2);

            int index;

            for (index = 0; index < (pTxNotify.CmdClassNotify.association_group_cmd_list_report.list_len / 2); index++)
            {
                pTxNotify.CmdClassNotify.association_group_cmd_list_report.cmd_list[index].cmd_class = *(pCmd + CMDBUF_CMD_OFFSET + 3 + index * 2);
                pTxNotify.CmdClassNotify.association_group_cmd_list_report.cmd_list[index].cmd = *(pCmd + CMDBUF_CMD_OFFSET + 4 + index * 2);
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case ASSOCIATION_GROUP_INFO_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_ASSOCIATION_GRP_INFO;
            pTxNotify.CmdClassNotify.cmd = ASSOCIATION_GROUP_INFO_REPORT;

            pTxNotify.CmdClassNotify.association_group_info_report.list_mode = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 7;
            pTxNotify.CmdClassNotify.association_group_info_report.dynamic_info = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x40) >> 6;
            pTxNotify.CmdClassNotify.association_group_info_report.group_count = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x3F);

            int index;

            for (index = 0; index <= pTxNotify.CmdClassNotify.association_group_info_report.group_count; index++)
            {
                pTxNotify.CmdClassNotify.association_group_info_report.group_info[index].group_id = *(pCmd + CMDBUF_CMD_OFFSET + 2 + index * 7);
                pTxNotify.CmdClassNotify.association_group_info_report.group_info[index].mode = *(pCmd + CMDBUF_CMD_OFFSET + 3 + index * 7);
                pTxNotify.CmdClassNotify.association_group_info_report.group_info[index].profile = *(pCmd + CMDBUF_CMD_OFFSET + 4 + index * 7);
                pTxNotify.CmdClassNotify.association_group_info_report.group_info[index].profile = ((pTxNotify.CmdClassNotify.association_group_info_report.group_info[index].profile) << 8) + (*(pCmd + CMDBUF_CMD_OFFSET + 5 + index * 7));
                // *(pCmd+CMDBUF_CMD_OFFSET+2+index*7) reserved
                pTxNotify.CmdClassNotify.association_group_info_report.group_info[index].event_code = *(pCmd + CMDBUF_CMD_OFFSET + 7 + index * 7);
                pTxNotify.CmdClassNotify.association_group_info_report.group_info[index].event_code = ((pTxNotify.CmdClassNotify.association_group_info_report.group_info[index].event_code) << 8) + (*(pCmd + CMDBUF_CMD_OFFSET + 8 + index * 7));
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_POWERLEVEL:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case POWERLEVEL_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_POWERLEVEL;
            pTxNotify.CmdClassNotify.cmd = POWERLEVEL_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.powerlevel_report.powerlevel = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.powerlevel_report.timeout = *(pCmd + CMDBUF_CMD_OFFSET + 2);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case POWERLEVEL_TEST_NODE_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_POWERLEVEL;
            pTxNotify.CmdClassNotify.cmd = POWERLEVEL_TEST_NODE_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.powerlevel_test_node_report.test_node_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.powerlevel_test_node_report.status_of_operation = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.powerlevel_test_node_report.test_frame_ack_count = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.powerlevel_test_node_report.test_frame_ack_count = (pTxNotify.CmdClassNotify.powerlevel_test_node_report.test_frame_ack_count << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 4);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_DEVICE_RESET_LOCALLY:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case DEVICE_RESET_LOCALLY_NOTIFICATION:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_DEVICE_RESET_LOCALLY;
            pTxNotify.CmdClassNotify.cmd = DEVICE_RESET_LOCALLY_NOTIFICATION;
            pTxNotify.CmdClassNotify.version = 1;
            removeKeyHeldDownMessageListFromNodeId(p->snode);
            removeSequenceNumberFromNodeId(p->snode);
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&(pTxNotify.CmdClassNotify), sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_SWITCH_MULTILEVEL:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case SWITCH_MULTILEVEL_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SWITCH_MULTILEVEL;
            pTxNotify.CmdClassNotify.cmd = SWITCH_MULTILEVEL_REPORT;

            if (cmdLength == 3)
            {
                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.switch_multilevel_report_v1.value = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            }
            // else if (cmdLength == 4)
            // {
            //     pTxNotify.CmdClassNotify.version = 2;
            //     pTxNotify.CmdClassNotify.switch_multilevel_report_v2.value = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            //     pTxNotify.CmdClassNotify.switch_multilevel_report_v2.duration = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            // }
            /*version 2, 3 similar version 1*/
            else if (cmdLength > 4)
            {
                pTxNotify.CmdClassNotify.version = 4;
                pTxNotify.CmdClassNotify.switch_multilevel_report_v3.value = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.switch_multilevel_report_v3.target_value = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                pTxNotify.CmdClassNotify.switch_multilevel_report_v3.duration = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }

        break;

        case SWITCH_MULTILEVEL_SET:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SWITCH_MULTILEVEL;
            pTxNotify.CmdClassNotify.cmd = SWITCH_MULTILEVEL_SET;

            if (cmdLength == 3)
            {
                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.switch_multilevel_set_v1.value = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            }
            else if (cmdLength == 4)
            {
                pTxNotify.CmdClassNotify.version = 2;
                pTxNotify.CmdClassNotify.switch_multilevel_set_v2.value = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.switch_multilevel_set_v2.duration = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            }
            /*version 3, 4 similar version 2*/

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }

        break;

        case SWITCH_MULTILEVEL_SUPPORTED_REPORT_V3:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SWITCH_MULTILEVEL;
            pTxNotify.CmdClassNotify.cmd = SWITCH_MULTILEVEL_SUPPORTED_REPORT_V3;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.switch_multilevel_supported_report.primary_switch_type = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x1F;
            pTxNotify.CmdClassNotify.switch_multilevel_supported_report.secondary_switch_type = (*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x1F;

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_BASIC:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case BASIC_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_BASIC;
            pTxNotify.CmdClassNotify.cmd = BASIC_REPORT;

            if (cmdLength == 3)
            {
                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.basic_report_v1.value = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            }
            else if (cmdLength > 3)
            {
                pTxNotify.CmdClassNotify.version = 2;
                pTxNotify.CmdClassNotify.basic_report_v2.value = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.basic_report_v2.target_value = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                pTxNotify.CmdClassNotify.basic_report_v2.duration = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case BASIC_SET:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_BASIC;
            pTxNotify.CmdClassNotify.cmd = BASIC_SET;
            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.basic_set.value = *(pCmd + CMDBUF_CMD_OFFSET + 1);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_ZWAVEPLUS_INFO:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case ZWAVEPLUS_INFO_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_ZWAVEPLUS_INFO;
            pTxNotify.CmdClassNotify.cmd = ZWAVEPLUS_INFO_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.zwaveplus_info_report.zwaveplus_version = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.zwaveplus_info_report.role_type = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.zwaveplus_info_report.node_type = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.zwaveplus_info_report.installer_icon_type = *(pCmd + CMDBUF_CMD_OFFSET + 4);
            pTxNotify.CmdClassNotify.zwaveplus_info_report.installer_icon_type = (pTxNotify.CmdClassNotify.zwaveplus_info_report.installer_icon_type << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 5);
            pTxNotify.CmdClassNotify.zwaveplus_info_report.user_icon_type = *(pCmd + CMDBUF_CMD_OFFSET + 6);
            pTxNotify.CmdClassNotify.zwaveplus_info_report.user_icon_type = (pTxNotify.CmdClassNotify.zwaveplus_info_report.user_icon_type << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 7);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_APPLICATION_STATUS:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case APPLICATION_BUSY:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_APPLICATION_STATUS;
            pTxNotify.CmdClassNotify.cmd = APPLICATION_BUSY;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.application_busy.status = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.application_busy.wait_time = *(pCmd + CMDBUF_CMD_OFFSET + 2);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case APPLICATION_REJECTED_REQUEST:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_APPLICATION_STATUS;
            pTxNotify.CmdClassNotify.cmd = APPLICATION_REJECTED_REQUEST;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.application_reject_request.status = *(pCmd + CMDBUF_CMD_OFFSET + 1);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_SWITCH_COLOR:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case SWITCH_COLOR_SUPPORTED_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SWITCH_COLOR;
            pTxNotify.CmdClassNotify.cmd = SWITCH_COLOR_SUPPORTED_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.color_control_supported_report.color_component_mask = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.color_control_supported_report.color_component_mask = (pTxNotify.CmdClassNotify.color_control_supported_report.color_component_mask << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 2);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case SWITCH_COLOR_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SWITCH_COLOR;
            pTxNotify.CmdClassNotify.cmd = SWITCH_COLOR_REPORT;

            if (cmdLength == 4)
            {
                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.color_control_report_v1.color_component_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.color_control_report_v1.value = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            }
            else if (cmdLength > 4)
            {
                pTxNotify.CmdClassNotify.version = 3;
                pTxNotify.CmdClassNotify.color_control_report_v3.color_component_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.color_control_report_v3.value = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                pTxNotify.CmdClassNotify.color_control_report_v3.target_value = *(pCmd + CMDBUF_CMD_OFFSET + 3);
                pTxNotify.CmdClassNotify.color_control_report_v3.duration = *(pCmd + CMDBUF_CMD_OFFSET + 4);
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_BATTERY:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case BATTERY_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_BATTERY;
            pTxNotify.CmdClassNotify.cmd = BATTERY_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.battery_report.battery_level = *(pCmd + CMDBUF_CMD_OFFSET + 1);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_SENSOR_MULTILEVEL:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case SENSOR_MULTILEVEL_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SENSOR_MULTILEVEL;
            pTxNotify.CmdClassNotify.cmd = SENSOR_MULTILEVEL_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.sensor_multilevel_report.sensor_type = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.sensor_multilevel_report.precision = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0xE0) >> 5;
            pTxNotify.CmdClassNotify.sensor_multilevel_report.scale = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x18) >> 3;
            pTxNotify.CmdClassNotify.sensor_multilevel_report.size = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x07);

            int index;
            pTxNotify.CmdClassNotify.sensor_multilevel_report.sensor_value = 0;
            for (index = 0; index < pTxNotify.CmdClassNotify.sensor_multilevel_report.size; index++)
            {
                pTxNotify.CmdClassNotify.sensor_multilevel_report.sensor_value =
                    (pTxNotify.CmdClassNotify.sensor_multilevel_report.sensor_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3 + index);
            }
            pTxNotify.CmdClassNotify.sensor_multilevel_report.sensor_value =
                ConvertValuetoInt32(pTxNotify.CmdClassNotify.sensor_multilevel_report.size,
                                    pTxNotify.CmdClassNotify.sensor_multilevel_report.sensor_value);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case SENSOR_MULTILEVEL_SUPPORTED_SENSOR_REPORT_V5:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SENSOR_MULTILEVEL;
            pTxNotify.CmdClassNotify.cmd = SENSOR_MULTILEVEL_SUPPORTED_SENSOR_REPORT_V5;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.sensor_multilevel_supported_report.no_bit_masks = cmdLength - 2;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.sensor_multilevel_supported_report.bit_mask, pCmd + CMDBUF_CMD_OFFSET + 1, pTxNotify.CmdClassNotify.sensor_multilevel_supported_report.no_bit_masks);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case SENSOR_MULTILEVEL_SUPPORTED_SCALE_REPORT_V5:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SENSOR_MULTILEVEL;
            pTxNotify.CmdClassNotify.cmd = SENSOR_MULTILEVEL_SUPPORTED_SCALE_REPORT_V5;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.sensor_multilevel_supported_scale_report.sensor_type = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.sensor_multilevel_supported_scale_report.scale_bit_mask = (*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x0F;

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_ASSOCIATION:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case ASSOCIATION_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_ASSOCIATION;
            pTxNotify.CmdClassNotify.cmd = ASSOCIATION_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.association_report.groud_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.association_report.max_nodes_supported = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.association_report.report_to_follow = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.association_report.no_nodes = cmdLength - 5;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.association_report.node_list, pCmd + CMDBUF_CMD_OFFSET + 4, pTxNotify.CmdClassNotify.association_report.no_nodes);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case ASSOCIATION_GROUPINGS_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_ASSOCIATION;
            pTxNotify.CmdClassNotify.cmd = ASSOCIATION_GROUPINGS_REPORT;

            pTxNotify.CmdClassNotify.version = 2;
            pTxNotify.CmdClassNotify.association_groupings_report.supported_groups = *(pCmd + CMDBUF_CMD_OFFSET + 1);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case ASSOCIATION_SPECIFIC_GROUP_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_ASSOCIATION;
            pTxNotify.CmdClassNotify.cmd = ASSOCIATION_SPECIFIC_GROUP_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 2;
            pTxNotify.CmdClassNotify.association_specific_group_report.group = *(pCmd + CMDBUF_CMD_OFFSET + 1);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION_V2:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case MULTI_CHANNEL_ASSOCIATION_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION_V2;
            pTxNotify.CmdClassNotify.cmd = MULTI_CHANNEL_ASSOCIATION_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 2;
            pTxNotify.CmdClassNotify.multi_channel_association_report.groud_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.multi_channel_association_report.max_nodes_supported = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.multi_channel_association_report.report_to_follow = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            i = 5;
            while (MULTI_CHANNEL_ASSOCIATION_SET_MARKER_V2 != *(pCmd + i) && (i < cmdLength))
            {
                pTxNotify.CmdClassNotify.multi_channel_association_report.node_list[i - 5] = *(pCmd + i);
                i++;
            }
            pTxNotify.CmdClassNotify.multi_channel_association_report.no_nodes = i - 5;
            pTxNotify.CmdClassNotify.multi_channel_association_report.no_multi_channel_node = 0;

            if (i < cmdLength) //frame contain multi_channel_node_list, endpoint_list
            {
                int position_marker = i;
                while (i < (cmdLength - 1))
                {
                    pTxNotify.CmdClassNotify.multi_channel_association_report.multi_channel_node_list[(i - position_marker) / 2] = *(pCmd + i + 1);
                    pTxNotify.CmdClassNotify.multi_channel_association_report.bit_address_list[(i - position_marker) / 2] = ((*(pCmd + i + 2) & 0x80) >> 7);
                    pTxNotify.CmdClassNotify.multi_channel_association_report.endpoint_list[(i - position_marker) / 2] = (*(pCmd + i + 2) & 0x7F);
                    i += 2;
                }
                pTxNotify.CmdClassNotify.multi_channel_association_report.no_multi_channel_node = (i - position_marker) / 2;
            }
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case MULTI_CHANNEL_ASSOCIATION_GROUPINGS_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION_V2;
            pTxNotify.CmdClassNotify.cmd = MULTI_CHANNEL_ASSOCIATION_GROUPINGS_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 2;
            pTxNotify.CmdClassNotify.multi_channel_association_groupings_report.supported_groups = *(pCmd + CMDBUF_CMD_OFFSET + 1);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_THERMOSTAT_SETPOINT:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case THERMOSTAT_SETPOINT_CAPABILITIES_REPORT_V3:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_THERMOSTAT_SETPOINT;
            pTxNotify.CmdClassNotify.cmd = THERMOSTAT_SETPOINT_CAPABILITIES_REPORT_V3;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.setpoint_type = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x0F;
            pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.min_precision = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0xE0) >> 5;
            pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.min_scale = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x18) >> 3;
            pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.min_size = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x07);

            int index;
            pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.min_value = 0;
            for (index = 0; index < pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.min_size; index++)
            {
                pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.min_value = (pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.min_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3 + index);
            }
            int j = 2 + pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.min_size;

            pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.max_precision = ((*(pCmd + CMDBUF_CMD_OFFSET + j + 1)) & 0xE0) >> 5;
            pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.max_scale = ((*(pCmd + CMDBUF_CMD_OFFSET + j + 1)) & 0x18) >> 3;
            pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.max_size = ((*(pCmd + CMDBUF_CMD_OFFSET + j + 1)) & 0x07);

            pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.max_value = 0;
            for (index = 0; index < pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.max_size; index++)
            {
                pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.max_value = (pTxNotify.CmdClassNotify.thermostat_setpoint_capabilities_report.max_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + j + 2 + index);
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case THERMOSTAT_SETPOINT_SUPPORTED_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_THERMOSTAT_SETPOINT;
            pTxNotify.CmdClassNotify.cmd = THERMOSTAT_SETPOINT_SUPPORTED_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.thermostat_setpoint_supported_report.no_bit_masks = cmdLength - 2;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.thermostat_setpoint_supported_report.bit_mask, pCmd + CMDBUF_CMD_OFFSET + 1, pTxNotify.CmdClassNotify.thermostat_setpoint_supported_report.no_bit_masks);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case THERMOSTAT_SETPOINT_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_THERMOSTAT_SETPOINT;
            pTxNotify.CmdClassNotify.cmd = THERMOSTAT_SETPOINT_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.thermostat_setpoint_report.setpoint_type = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x0F;
            pTxNotify.CmdClassNotify.thermostat_setpoint_report.precision = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0xE0) >> 5;
            pTxNotify.CmdClassNotify.thermostat_setpoint_report.scale = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x18) >> 3;
            pTxNotify.CmdClassNotify.thermostat_setpoint_report.size = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x07);
            int index;
            pTxNotify.CmdClassNotify.thermostat_setpoint_report.value = 0;
            for (index = 0; index < pTxNotify.CmdClassNotify.thermostat_setpoint_report.size; index++)
            {
                pTxNotify.CmdClassNotify.thermostat_setpoint_report.value = (pTxNotify.CmdClassNotify.thermostat_setpoint_report.value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3 + index);
            }
            pTxNotify.CmdClassNotify.thermostat_setpoint_report.value =
                ConvertValuetoInt32(pTxNotify.CmdClassNotify.thermostat_setpoint_report.size,
                                    pTxNotify.CmdClassNotify.thermostat_setpoint_report.value);
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_TIME_PARAMETERS:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case TIME_PARAMETERS_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_TIME_PARAMETERS;
            pTxNotify.CmdClassNotify.cmd = TIME_PARAMETERS_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.time_parameters_report.year = (*(pCmd + CMDBUF_CMD_OFFSET + 1));
            pTxNotify.CmdClassNotify.time_parameters_report.year = (pTxNotify.CmdClassNotify.time_parameters_report.year << 8) + (*(pCmd + CMDBUF_CMD_OFFSET + 2));
            pTxNotify.CmdClassNotify.time_parameters_report.month = (*(pCmd + CMDBUF_CMD_OFFSET + 3));
            pTxNotify.CmdClassNotify.time_parameters_report.day = (*(pCmd + CMDBUF_CMD_OFFSET + 4));
            pTxNotify.CmdClassNotify.time_parameters_report.hour_UTC = (*(pCmd + CMDBUF_CMD_OFFSET + 5));
            pTxNotify.CmdClassNotify.time_parameters_report.minute_UTC = (*(pCmd + CMDBUF_CMD_OFFSET + 6));
            pTxNotify.CmdClassNotify.time_parameters_report.second_UTC = (*(pCmd + CMDBUF_CMD_OFFSET + 7));

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_TIME:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case TIME_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_TIME;
            pTxNotify.CmdClassNotify.cmd = TIME_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.time_report.RTC_failure = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 7;
            pTxNotify.CmdClassNotify.time_report.hour_local_time = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x1F) >> 0;
            pTxNotify.CmdClassNotify.time_report.minute_local_time = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.time_report.second_local_time = *(pCmd + CMDBUF_CMD_OFFSET + 3);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case DATE_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_TIME;
            pTxNotify.CmdClassNotify.cmd = DATE_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.time_date_report.year = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.time_date_report.year = (pTxNotify.CmdClassNotify.time_date_report.year << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.time_date_report.month = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.time_date_report.day = *(pCmd + CMDBUF_CMD_OFFSET + 4);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case TIME_OFFSET_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_TIME;
            pTxNotify.CmdClassNotify.cmd = TIME_OFFSET_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.time_offset_report.sign_TZO = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 7;
            pTxNotify.CmdClassNotify.time_offset_report.hour_TZO = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x7F) >> 0;
            pTxNotify.CmdClassNotify.time_offset_report.minute_TZO = (*(pCmd + CMDBUF_CMD_OFFSET + 2));
            pTxNotify.CmdClassNotify.time_offset_report.sign_offset_DST = ((*(pCmd + CMDBUF_CMD_OFFSET + 3)) & 0x80) >> 7;
            pTxNotify.CmdClassNotify.time_offset_report.minute_offset_DST = ((*(pCmd + CMDBUF_CMD_OFFSET + 3)) & 0x7F) >> 0;
            pTxNotify.CmdClassNotify.time_offset_report.month_start_DST = (*(pCmd + CMDBUF_CMD_OFFSET + 4));
            pTxNotify.CmdClassNotify.time_offset_report.day_start_DST = (*(pCmd + CMDBUF_CMD_OFFSET + 5));
            pTxNotify.CmdClassNotify.time_offset_report.hour_start_DST = (*(pCmd + CMDBUF_CMD_OFFSET + 6));
            pTxNotify.CmdClassNotify.time_offset_report.month_end_DST = (*(pCmd + CMDBUF_CMD_OFFSET + 7));
            pTxNotify.CmdClassNotify.time_offset_report.day_end_DST = (*(pCmd + CMDBUF_CMD_OFFSET + 8));
            pTxNotify.CmdClassNotify.time_offset_report.hour_end_DST = (*(pCmd + CMDBUF_CMD_OFFSET + 9));

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_THERMOSTAT_SETBACK:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case THERMOSTAT_SETBACK_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_THERMOSTAT_SETBACK;
            pTxNotify.CmdClassNotify.cmd = THERMOSTAT_SETBACK_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.thermostat_setback_report.setback_type = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x03;
            pTxNotify.CmdClassNotify.thermostat_setback_report.setback_state = (*(pCmd + CMDBUF_CMD_OFFSET + 2));

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_THERMOSTAT_OPERATING_STATE:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case THERMOSTAT_OPERATING_STATE_LOGGING_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_THERMOSTAT_OPERATING_STATE;
            pTxNotify.CmdClassNotify.cmd = THERMOSTAT_OPERATING_STATE_LOGGING_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.thermostat_operating_logging_report.report_to_follow = (*(pCmd + CMDBUF_CMD_OFFSET + 1));

            pTxNotify.CmdClassNotify.thermostat_operating_logging_report.no_usages = (cmdLength - 3) / 5;
            int index;

            for (index = 0; index < pTxNotify.CmdClassNotify.thermostat_operating_logging_report.no_usages; index++)
            {
                pTxNotify.CmdClassNotify.thermostat_operating_logging_report.usage_list[index].operating_state_log_type = (*(pCmd + CMDBUF_CMD_OFFSET + 2 + index * 5)) & 0x0F;
                pTxNotify.CmdClassNotify.thermostat_operating_logging_report.usage_list[index].usage_today_hours = (*(pCmd + CMDBUF_CMD_OFFSET + 3 + index * 5));
                pTxNotify.CmdClassNotify.thermostat_operating_logging_report.usage_list[index].usage_today_minutes = (*(pCmd + CMDBUF_CMD_OFFSET + 4 + index * 5));
                pTxNotify.CmdClassNotify.thermostat_operating_logging_report.usage_list[index].usage_yesterday_hours = (*(pCmd + CMDBUF_CMD_OFFSET + 5 + index * 5));
                pTxNotify.CmdClassNotify.thermostat_operating_logging_report.usage_list[index].usage_yesterday_minutes = (*(pCmd + CMDBUF_CMD_OFFSET + 6 + index * 5));
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case THERMOSTAT_OPERATING_LOGGING_SUPPORTED_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_THERMOSTAT_OPERATING_STATE;
            pTxNotify.CmdClassNotify.cmd = THERMOSTAT_OPERATING_LOGGING_SUPPORTED_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.thermostat_operating_logging_supported_report.no_bit_masks = cmdLength - 2;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.thermostat_operating_logging_supported_report.bit_mask, pCmd + CMDBUF_CMD_OFFSET + 1, pTxNotify.CmdClassNotify.thermostat_operating_logging_supported_report.no_bit_masks);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case THERMOSTAT_OPERATING_STATE_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_THERMOSTAT_OPERATING_STATE;
            pTxNotify.CmdClassNotify.cmd = THERMOSTAT_OPERATING_STATE_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.thermostat_operating_state_report.operating_state = (*(pCmd + CMDBUF_CMD_OFFSET + 1));

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_THERMOSTAT_MODE:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case THERMOSTAT_MODE_SUPPORTED_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_THERMOSTAT_MODE;
            pTxNotify.CmdClassNotify.cmd = THERMOSTAT_MODE_SUPPORTED_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.thermostat_mode_supported_report.no_bit_masks = cmdLength - 2;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.thermostat_mode_supported_report.bit_mask, pCmd + CMDBUF_CMD_OFFSET + 1, pTxNotify.CmdClassNotify.thermostat_mode_supported_report.no_bit_masks);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case THERMOSTAT_MODE_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_THERMOSTAT_MODE;
            pTxNotify.CmdClassNotify.cmd = THERMOSTAT_MODE_REPORT;

            pTxNotify.CmdClassNotify.version = 1;

            if (cmdLength == 3)
            {
                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.thermostat_mode_report_v1.mode = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x1F;
            }
            else if (cmdLength > 3)
            {
                pTxNotify.CmdClassNotify.version = 3;
                pTxNotify.CmdClassNotify.thermostat_mode_report_v3.mode = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x1F;
                pTxNotify.CmdClassNotify.thermostat_mode_report_v3.no_manufacturer_data = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0xE0) >> 5;
                memcpy((uint8_t *)pTxNotify.CmdClassNotify.thermostat_mode_report_v3.manufacturer_data, pCmd + CMDBUF_CMD_OFFSET + 2, pTxNotify.CmdClassNotify.thermostat_mode_report_v3.no_manufacturer_data);
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_THERMOSTAT_FAN_MODE:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case THERMOSTAT_FAN_MODE_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_THERMOSTAT_FAN_MODE;
            pTxNotify.CmdClassNotify.cmd = THERMOSTAT_FAN_MODE_REPORT;

            pTxNotify.CmdClassNotify.version = 2;
            pTxNotify.CmdClassNotify.thermostat_fan_mode_report_v2.off = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 7;
            pTxNotify.CmdClassNotify.thermostat_fan_mode_report_v2.fan_mode = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x0F);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case THERMOSTAT_FAN_MODE_SUPPORTED_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_THERMOSTAT_FAN_MODE;
            pTxNotify.CmdClassNotify.cmd = THERMOSTAT_FAN_MODE_SUPPORTED_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.thermostat_fan_mode_supported_report.no_bit_masks = cmdLength - 2;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.thermostat_fan_mode_supported_report.bit_mask, pCmd + CMDBUF_CMD_OFFSET + 1, pTxNotify.CmdClassNotify.thermostat_fan_mode_supported_report.no_bit_masks);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_THERMOSTAT_FAN_STATE:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case THERMOSTAT_FAN_STATE_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_THERMOSTAT_FAN_STATE;
            pTxNotify.CmdClassNotify.cmd = THERMOSTAT_FAN_STATE_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.thermostat_fan_state_report.fan_operating_state = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x0F;

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_CONFIGURATION:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case CONFIGURATION_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_CONFIGURATION;
            pTxNotify.CmdClassNotify.cmd = CONFIGURATION_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.configuration_report.parameter_number = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.configuration_report.size = (*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x07;
            int index;
            pTxNotify.CmdClassNotify.configuration_report.configuration_value = 0;
            for (index = 0; index < pTxNotify.CmdClassNotify.configuration_report.size; index++)
            {
                pTxNotify.CmdClassNotify.configuration_report.configuration_value =
                    (pTxNotify.CmdClassNotify.configuration_report.configuration_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3 + index);
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case CONFIGURATION_BULK_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_CONFIGURATION;
            pTxNotify.CmdClassNotify.cmd = CONFIGURATION_BULK_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 2;
            pTxNotify.CmdClassNotify.configuration_bulk_report.parameter_offset = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.configuration_bulk_report.parameter_offset = (pTxNotify.CmdClassNotify.configuration_bulk_report.parameter_offset << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.configuration_bulk_report.no_params = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.configuration_bulk_report.report_to_follow = *(pCmd + CMDBUF_CMD_OFFSET + 4);
            pTxNotify.CmdClassNotify.configuration_bulk_report.default_value = ((*(pCmd + CMDBUF_CMD_OFFSET + 5)) & 0x80) >> 7;
            pTxNotify.CmdClassNotify.configuration_bulk_report.handshake = ((*(pCmd + CMDBUF_CMD_OFFSET + 5)) & 0x40) >> 6;
            pTxNotify.CmdClassNotify.configuration_bulk_report.size = ((*(pCmd + CMDBUF_CMD_OFFSET + 5)) & 0x07);

            int index;
            int j;
            for (index = 0; index < pTxNotify.CmdClassNotify.configuration_bulk_report.no_params; index++)
            {
                int configuration_value = 0;
                ;
                for (j = 0; j < pTxNotify.CmdClassNotify.configuration_bulk_report.size; j++)
                {
                    configuration_value = (configuration_value << 8) + (*(pCmd + CMDBUF_CMD_OFFSET + 6 + j + pTxNotify.CmdClassNotify.configuration_bulk_report.size * index));
                }
                pTxNotify.CmdClassNotify.configuration_bulk_report.params_list[index] = configuration_value;
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case CONFIGURATION_NAME_REPORT_V3:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_CONFIGURATION;
            pTxNotify.CmdClassNotify.cmd = CONFIGURATION_NAME_REPORT_V3;

            pTxNotify.CmdClassNotify.version = 3;
            pTxNotify.CmdClassNotify.configuration_name_report.parameter_number = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.configuration_name_report.parameter_number = (pTxNotify.CmdClassNotify.configuration_name_report.parameter_number << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.configuration_name_report.report_to_follow = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.configuration_name_report.name_len = cmdLength - 5;
            memset((uint8_t *)pTxNotify.CmdClassNotify.configuration_name_report.name, 0, MAX_BUFFER_LENGTH);
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.configuration_name_report.name, pCmd + CMDBUF_CMD_OFFSET + 4, pTxNotify.CmdClassNotify.configuration_name_report.name_len);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case CONFIGURATION_INFO_REPORT_V3:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_CONFIGURATION;
            pTxNotify.CmdClassNotify.cmd = CONFIGURATION_INFO_REPORT_V3;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.configuration_info_report.parameter_number = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.configuration_info_report.parameter_number = (pTxNotify.CmdClassNotify.configuration_info_report.parameter_number << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.configuration_info_report.report_to_follow = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.configuration_info_report.info_len = cmdLength - 5;
            memset((uint8_t *)pTxNotify.CmdClassNotify.configuration_info_report.info, 0, MAX_BUFFER_LENGTH);
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.configuration_info_report.info, pCmd + CMDBUF_CMD_OFFSET + 4, pTxNotify.CmdClassNotify.configuration_info_report.info_len);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case CONFIGURATION_PROPERTIES_REPORT_V3:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_CONFIGURATION;
            pTxNotify.CmdClassNotify.cmd = CONFIGURATION_PROPERTIES_REPORT_V3;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.configuration_properties_report.parameter_number = (*(pCmd + CMDBUF_CMD_OFFSET + 1));
            pTxNotify.CmdClassNotify.configuration_properties_report.parameter_number = (pTxNotify.CmdClassNotify.configuration_properties_report.parameter_number << 8) + (*(pCmd + CMDBUF_CMD_OFFSET + 2));
            pTxNotify.CmdClassNotify.configuration_properties_report.format = ((*(pCmd + CMDBUF_CMD_OFFSET + 3)) & 0x38) >> 3;
            pTxNotify.CmdClassNotify.configuration_properties_report.size = ((*(pCmd + CMDBUF_CMD_OFFSET + 3)) & 0x07) >> 0;

            int index;
            pTxNotify.CmdClassNotify.configuration_properties_report.min_value = 0;
            for (index = 0; index < pTxNotify.CmdClassNotify.configuration_properties_report.size; index++)
            {
                pTxNotify.CmdClassNotify.configuration_properties_report.min_value = (pTxNotify.CmdClassNotify.configuration_properties_report.min_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 4 + index);
            }
            int j = 3 + pTxNotify.CmdClassNotify.configuration_properties_report.size;

            pTxNotify.CmdClassNotify.configuration_properties_report.max_value = 0;
            for (index = 0; index < pTxNotify.CmdClassNotify.configuration_properties_report.size; index++)
            {
                pTxNotify.CmdClassNotify.configuration_properties_report.max_value = (pTxNotify.CmdClassNotify.configuration_properties_report.max_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + j + 1 + index);
            }
            j = j + pTxNotify.CmdClassNotify.configuration_properties_report.size;

            pTxNotify.CmdClassNotify.configuration_properties_report.default_value = 0;
            for (index = 0; index < pTxNotify.CmdClassNotify.configuration_properties_report.size; index++)
            {
                pTxNotify.CmdClassNotify.configuration_properties_report.default_value = (pTxNotify.CmdClassNotify.configuration_properties_report.default_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + j + 1 + index);
            }
            j = j + pTxNotify.CmdClassNotify.configuration_properties_report.size;

            pTxNotify.CmdClassNotify.configuration_properties_report.next_parameter_number = (*(pCmd + CMDBUF_CMD_OFFSET + j + 1));
            pTxNotify.CmdClassNotify.configuration_properties_report.next_parameter_number = (pTxNotify.CmdClassNotify.configuration_properties_report.next_parameter_number << 8) + (*(pCmd + CMDBUF_CMD_OFFSET + j + 2));

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_METER:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case METER_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_METER;
            pTxNotify.CmdClassNotify.cmd = METER_REPORT;
            uint8_t size = (*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x07;
            if ((size + FRAME_METER_REPORT_V1_LENGHT) == cmdLength)
            {
                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.meter_report_v1.meter_type = (*(pCmd + CMDBUF_CMD_OFFSET + 1)& 0x1F);
                pTxNotify.CmdClassNotify.meter_report_v1.precision = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0xE0) >> 5;
                pTxNotify.CmdClassNotify.meter_report_v1.scale = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x18) >> 3;
                pTxNotify.CmdClassNotify.meter_report_v1.size = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x07);

                int index;
                pTxNotify.CmdClassNotify.meter_report_v1.meter_value = 0;
                for (index = 0; index < pTxNotify.CmdClassNotify.meter_report_v1.size; index++)
                {
                    pTxNotify.CmdClassNotify.meter_report_v1.meter_value = (pTxNotify.CmdClassNotify.meter_report_v1.meter_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3 + index);
                }
                pTxNotify.CmdClassNotify.meter_report_v1.meter_value =
                    ConvertValuetoInt32(pTxNotify.CmdClassNotify.meter_report_v1.size,
                                        pTxNotify.CmdClassNotify.meter_report_v1.meter_value);
            }
            else
            {
                int k = METER_VALUE_PARAM_POS + pTxNotify.CmdClassNotify.meter_report_v2.size;

                pTxNotify.CmdClassNotify.meter_report_v2.delta_time = (*(pCmd + k));
                pTxNotify.CmdClassNotify.meter_report_v2.delta_time =
                    (pTxNotify.CmdClassNotify.meter_report_v2.delta_time << 8) + (*(pCmd + k + 1));
                if(pTxNotify.CmdClassNotify.meter_report_v2.delta_time)
                {
                    //version 2 similar version 3
                    if ((size * 2 + TOTAL_DELTA_TIME_LENGHT_AND_SIZE_PARAM_POS) == cmdLength)
                    {
                        pTxNotify.CmdClassNotify.version = 2;
                        //version 3: Scale (3 bits) -The Scale field is composed of two sub-fields Scale (2) and Scale (1:0)
                        pTxNotify.CmdClassNotify.meter_report_v2.rate_type = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x60) >> 5;
                        pTxNotify.CmdClassNotify.meter_report_v2.meter_type = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x1F);
                        pTxNotify.CmdClassNotify.meter_report_v2.precision = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0xE0) >> 5;
                        pTxNotify.CmdClassNotify.meter_report_v2.scale = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x18) >> 3;
                        pTxNotify.CmdClassNotify.meter_report_v2.scale =
                            (pTxNotify.CmdClassNotify.meter_report_v2.scale | (((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 5));
                        pTxNotify.CmdClassNotify.meter_report_v2.size = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x07);
        
                        int index;
                        int j;
                        pTxNotify.CmdClassNotify.meter_report_v2.meter_value = 0;
                        for (index = 0; index < pTxNotify.CmdClassNotify.meter_report_v2.size; index++)
                        {
                            pTxNotify.CmdClassNotify.meter_report_v2.meter_value =
                                (pTxNotify.CmdClassNotify.meter_report_v2.meter_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3 + index);
                        }
                        pTxNotify.CmdClassNotify.meter_report_v2.meter_value =
                            ConvertValuetoInt32(pTxNotify.CmdClassNotify.meter_report_v2.size,
                                                pTxNotify.CmdClassNotify.meter_report_v2.meter_value);
                        j = 2 + pTxNotify.CmdClassNotify.meter_report_v2.size;
        
                        pTxNotify.CmdClassNotify.meter_report_v2.delta_time = (*(pCmd + CMDBUF_CMD_OFFSET + j + 1));
                        pTxNotify.CmdClassNotify.meter_report_v2.delta_time =
                            (pTxNotify.CmdClassNotify.meter_report_v2.delta_time << 8) + (*(pCmd + CMDBUF_CMD_OFFSET + j + 2));
        
                        pTxNotify.CmdClassNotify.meter_report_v2.previous_meter_value = 0;
                        for (index = 0; index < pTxNotify.CmdClassNotify.meter_report_v2.size; index++)
                        {
                            pTxNotify.CmdClassNotify.meter_report_v2.previous_meter_value =
                                (pTxNotify.CmdClassNotify.meter_report_v2.previous_meter_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + j + 3 + index);
                        }
                        pTxNotify.CmdClassNotify.meter_report_v2.previous_meter_value =
                            ConvertValuetoInt32(pTxNotify.CmdClassNotify.meter_report_v2.size,
                                                pTxNotify.CmdClassNotify.meter_report_v2.previous_meter_value);
                    }
                    else
                    {            
                        //version 4 similar version 5
                        
                        pTxNotify.CmdClassNotify.version = 4;
                        //version 3: Scale (3 bits) -The Scale field is composed of two sub-fields Scale (2) and Scale (1:0)
                        pTxNotify.CmdClassNotify.meter_report_v4.rate_type = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x60) >> 5;
                        pTxNotify.CmdClassNotify.meter_report_v4.meter_type = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x1F);
                        pTxNotify.CmdClassNotify.meter_report_v4.precision = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0xE0) >> 5;
                        pTxNotify.CmdClassNotify.meter_report_v4.scale = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x18) >> 3;
                        pTxNotify.CmdClassNotify.meter_report_v4.scale =
                            (pTxNotify.CmdClassNotify.meter_report_v4.scale | (((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 5));
                        pTxNotify.CmdClassNotify.meter_report_v4.size = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x07);
        
                        int index;
                        int j;
                        pTxNotify.CmdClassNotify.meter_report_v4.meter_value = 0;
                        for (index = 0; index < pTxNotify.CmdClassNotify.meter_report_v4.size; index++)
                        {
                            pTxNotify.CmdClassNotify.meter_report_v4.meter_value =
                                (pTxNotify.CmdClassNotify.meter_report_v4.meter_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3 + index);
                        }
                        pTxNotify.CmdClassNotify.meter_report_v4.meter_value =
                            ConvertValuetoInt32(pTxNotify.CmdClassNotify.meter_report_v4.size,
                                                pTxNotify.CmdClassNotify.meter_report_v4.meter_value);
                        j = 2 + pTxNotify.CmdClassNotify.meter_report_v4.size;
        
                        pTxNotify.CmdClassNotify.meter_report_v4.delta_time = (*(pCmd + CMDBUF_CMD_OFFSET + j + 1));
                        pTxNotify.CmdClassNotify.meter_report_v4.delta_time =
                            (pTxNotify.CmdClassNotify.meter_report_v4.delta_time << 8) + (*(pCmd + CMDBUF_CMD_OFFSET + j + 2));
        
                        pTxNotify.CmdClassNotify.meter_report_v4.previous_meter_value = 0;
                        for (index = 0; index < pTxNotify.CmdClassNotify.meter_report_v4.size; index++)
                        {
                            pTxNotify.CmdClassNotify.meter_report_v4.previous_meter_value =
                                (pTxNotify.CmdClassNotify.meter_report_v4.previous_meter_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + j + 3 + index);
                        }
                        pTxNotify.CmdClassNotify.meter_report_v4.previous_meter_value =
                            ConvertValuetoInt32(pTxNotify.CmdClassNotify.meter_report_v4.size,
                                                pTxNotify.CmdClassNotify.meter_report_v4.previous_meter_value);
        
                        pTxNotify.CmdClassNotify.meter_report_v4.scale2 = (*(pCmd + CMDBUF_CMD_OFFSET + size * 2 + 5));                      
                    }                        
                }
                else
                {
                    //version 2 similar version 3
                    if ((size + TOTAL_DELTA_TIME_LENGHT_AND_SIZE_PARAM_POS) == cmdLength)
                    {
                        pTxNotify.CmdClassNotify.version = 2;
                        //version 3: Scale (3 bits) -The Scale field is composed of two sub-fields Scale (2) and Scale (1:0)
                        pTxNotify.CmdClassNotify.meter_report_v2.rate_type = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x60) >> 5;
                        pTxNotify.CmdClassNotify.meter_report_v2.meter_type = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x1F);
                        pTxNotify.CmdClassNotify.meter_report_v2.precision = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0xE0) >> 5;
                        pTxNotify.CmdClassNotify.meter_report_v2.scale = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x18) >> 3;
                        pTxNotify.CmdClassNotify.meter_report_v2.scale =
                            (pTxNotify.CmdClassNotify.meter_report_v2.scale | (((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 5));
                        pTxNotify.CmdClassNotify.meter_report_v2.size = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x07);
        
                        int index;
                        int j;
                        pTxNotify.CmdClassNotify.meter_report_v2.meter_value = 0;
                        for (index = 0; index < pTxNotify.CmdClassNotify.meter_report_v2.size; index++)
                        {
                            pTxNotify.CmdClassNotify.meter_report_v2.meter_value =
                                (pTxNotify.CmdClassNotify.meter_report_v2.meter_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3 + index);
                        }
                        pTxNotify.CmdClassNotify.meter_report_v2.meter_value =
                            ConvertValuetoInt32(pTxNotify.CmdClassNotify.meter_report_v2.size,
                                                pTxNotify.CmdClassNotify.meter_report_v2.meter_value);
                        j = 2 + pTxNotify.CmdClassNotify.meter_report_v2.size;
        
                        pTxNotify.CmdClassNotify.meter_report_v2.delta_time = (*(pCmd + CMDBUF_CMD_OFFSET + j + 1));
                        pTxNotify.CmdClassNotify.meter_report_v2.delta_time =
                            (pTxNotify.CmdClassNotify.meter_report_v2.delta_time << 8) + (*(pCmd + CMDBUF_CMD_OFFSET + j + 2));
        
                        pTxNotify.CmdClassNotify.meter_report_v2.previous_meter_value = 0;
                    }
                    else
                    {            
                        //version 4 similar version 5
                        
                        pTxNotify.CmdClassNotify.version = 4;
                        //version 3: Scale (3 bits) -The Scale field is composed of two sub-fields Scale (2) and Scale (1:0)
                        pTxNotify.CmdClassNotify.meter_report_v4.rate_type = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x60) >> 5;
                        pTxNotify.CmdClassNotify.meter_report_v4.meter_type = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x1F);
                        pTxNotify.CmdClassNotify.meter_report_v4.precision = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0xE0) >> 5;
                        pTxNotify.CmdClassNotify.meter_report_v4.scale = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x18) >> 3;
                        pTxNotify.CmdClassNotify.meter_report_v4.scale =
                            (pTxNotify.CmdClassNotify.meter_report_v4.scale | (((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 5));
                        pTxNotify.CmdClassNotify.meter_report_v4.size = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x07);
        
                        int index;
                        int j;
                        pTxNotify.CmdClassNotify.meter_report_v4.meter_value = 0;
                        for (index = 0; index < pTxNotify.CmdClassNotify.meter_report_v4.size; index++)
                        {
                            pTxNotify.CmdClassNotify.meter_report_v4.meter_value =
                                (pTxNotify.CmdClassNotify.meter_report_v4.meter_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3 + index);
                        }
                        pTxNotify.CmdClassNotify.meter_report_v4.meter_value =
                            ConvertValuetoInt32(pTxNotify.CmdClassNotify.meter_report_v4.size,
                                                pTxNotify.CmdClassNotify.meter_report_v4.meter_value);
                        j = 2 + pTxNotify.CmdClassNotify.meter_report_v4.size;
        
                        pTxNotify.CmdClassNotify.meter_report_v4.delta_time = (*(pCmd + CMDBUF_CMD_OFFSET + j + 1));
                        pTxNotify.CmdClassNotify.meter_report_v4.delta_time =
                            (pTxNotify.CmdClassNotify.meter_report_v4.delta_time << 8) + (*(pCmd + CMDBUF_CMD_OFFSET + j + 2));
        
                        pTxNotify.CmdClassNotify.meter_report_v4.previous_meter_value = 0;
                        pTxNotify.CmdClassNotify.meter_report_v4.scale2 = (*(pCmd + CMDBUF_CMD_OFFSET + size + 5));                                          
                    }
                }                   
            }
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        case METER_SUPPORTED_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_METER;
            pTxNotify.CmdClassNotify.cmd = METER_SUPPORTED_REPORT_V2;

            if (cmdLength == 4)
            {
                pTxNotify.CmdClassNotify.version = 2; //and version 3
                pTxNotify.CmdClassNotify.meter_supported_report_v2.meter_reset = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 7;
                pTxNotify.CmdClassNotify.meter_supported_report_v2.meter_type = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x1F);
                pTxNotify.CmdClassNotify.meter_supported_report_v2.scale_supported = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            }
            else if (cmdLength > 4)
            {
                pTxNotify.CmdClassNotify.version = 4;
                pTxNotify.CmdClassNotify.meter_supported_report_v4.meter_reset = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 7;
                pTxNotify.CmdClassNotify.meter_supported_report_v4.rate_type = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x60) >> 5;
                pTxNotify.CmdClassNotify.meter_supported_report_v4.meter_type = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x1F);
                pTxNotify.CmdClassNotify.meter_supported_report_v4.more_scale_types = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x80) >> 7; //M.S.T
                pTxNotify.CmdClassNotify.meter_supported_report_v4.bit_mask[0] = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x7F);
                pTxNotify.CmdClassNotify.meter_supported_report_v4.no_bit_masks = (*(pCmd + CMDBUF_CMD_OFFSET + 3)) + 1;
                memcpy((uint8_t *)&pTxNotify.CmdClassNotify.meter_supported_report_v4.bit_mask[1],
                       pCmd + CMDBUF_CMD_OFFSET + 4, (pTxNotify.CmdClassNotify.meter_supported_report_v4.no_bit_masks - 1));
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_MANUFACTURER_SPECIFIC:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {

        case MANUFACTURER_SPECIFIC_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_MANUFACTURER_SPECIFIC;
            pTxNotify.CmdClassNotify.cmd = MANUFACTURER_SPECIFIC_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.manufacturer_specific_report.manufacturer_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.manufacturer_specific_report.manufacturer_id = (pTxNotify.CmdClassNotify.manufacturer_specific_report.manufacturer_id << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.manufacturer_specific_report.product_type_id = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.manufacturer_specific_report.product_type_id = (pTxNotify.CmdClassNotify.manufacturer_specific_report.product_type_id << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 4);
            pTxNotify.CmdClassNotify.manufacturer_specific_report.product_id = *(pCmd + CMDBUF_CMD_OFFSET + 5);
            pTxNotify.CmdClassNotify.manufacturer_specific_report.product_id = (pTxNotify.CmdClassNotify.manufacturer_specific_report.product_id << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 6);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case DEVICE_SPECIFIC_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_MANUFACTURER_SPECIFIC;
            pTxNotify.CmdClassNotify.cmd = DEVICE_SPECIFIC_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.device_specific_report.device_id_type = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x07;
            pTxNotify.CmdClassNotify.device_specific_report.device_id_data_format = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0xE0) >> 5;
            pTxNotify.CmdClassNotify.device_specific_report.device_id_data_length = (*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x1F;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.device_specific_report.device_id_data, pCmd + CMDBUF_CMD_OFFSET + 3, pTxNotify.CmdClassNotify.device_specific_report.device_id_data_length);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;
    case COMMAND_CLASS_WAKE_UP:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case WAKE_UP_NOTIFICATION:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_WAKE_UP;
            pTxNotify.CmdClassNotify.cmd = WAKE_UP_NOTIFICATION;
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));            
        }
        break;

        case WAKE_UP_INTERVAL_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_WAKE_UP;
            pTxNotify.CmdClassNotify.cmd = WAKE_UP_INTERVAL_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.wake_up_interval_report.seconds = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.wake_up_interval_report.seconds = (pTxNotify.CmdClassNotify.wake_up_interval_report.seconds << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.wake_up_interval_report.seconds = (pTxNotify.CmdClassNotify.wake_up_interval_report.seconds << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.wake_up_interval_report.node_id = *(pCmd + CMDBUF_CMD_OFFSET + 4);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case WAKE_UP_INTERVAL_CAPABILITIES_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_WAKE_UP;
            pTxNotify.CmdClassNotify.cmd = WAKE_UP_INTERVAL_CAPABILITIES_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.min_wk_interval_seconds = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.min_wk_interval_seconds = (pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.min_wk_interval_seconds << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.min_wk_interval_seconds = (pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.min_wk_interval_seconds << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.max_wk_interval_seconds = *(pCmd + CMDBUF_CMD_OFFSET + 4);
            pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.max_wk_interval_seconds = (pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.max_wk_interval_seconds << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 5);
            pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.max_wk_interval_seconds = (pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.max_wk_interval_seconds << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 6);
            pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.default_wk_interval_seconds = *(pCmd + CMDBUF_CMD_OFFSET + 7);
            pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.default_wk_interval_seconds = (pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.default_wk_interval_seconds << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 8);
            pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.default_wk_interval_seconds = (pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.default_wk_interval_seconds << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 9);
            pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.wk_interval_step_seconds = *(pCmd + CMDBUF_CMD_OFFSET + 10);
            pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.wk_interval_step_seconds = (pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.wk_interval_step_seconds << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 11);
            pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.wk_interval_step_seconds = (pTxNotify.CmdClassNotify.wake_up_interval_capabilities_report.wk_interval_step_seconds << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 12);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_SCHEDULE_ENTRY_LOCK:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case SCHEDULE_ENTRY_TYPE_SUPPORTED_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SCHEDULE_ENTRY_LOCK;
            pTxNotify.CmdClassNotify.cmd = SCHEDULE_ENTRY_TYPE_SUPPORTED_REPORT;

            if (cmdLength == 4)
            {
                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.schedule_entry_type_supported_report_v1.no_slots_week_day = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.schedule_entry_type_supported_report_v1.no_slots_year_day = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            }
            else if (cmdLength > 4)
            {
                pTxNotify.CmdClassNotify.version = 3;
                pTxNotify.CmdClassNotify.schedule_entry_type_supported_report_v3.no_slots_week_day = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.schedule_entry_type_supported_report_v3.no_slots_year_day = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                pTxNotify.CmdClassNotify.schedule_entry_type_supported_report_v3.no_slots_daily_repeating = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case SCHEDULE_ENTRY_LOCK_WEEK_DAY_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SCHEDULE_ENTRY_LOCK;
            pTxNotify.CmdClassNotify.cmd = SCHEDULE_ENTRY_LOCK_WEEK_DAY_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.schedule_entry_lock_week_day_report.user_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.schedule_entry_lock_week_day_report.schedule_slot_id = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.schedule_entry_lock_week_day_report.day_of_week = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.schedule_entry_lock_week_day_report.start_hour = *(pCmd + CMDBUF_CMD_OFFSET + 4);
            pTxNotify.CmdClassNotify.schedule_entry_lock_week_day_report.start_minute = *(pCmd + CMDBUF_CMD_OFFSET + 5);
            pTxNotify.CmdClassNotify.schedule_entry_lock_week_day_report.stop_hour = *(pCmd + CMDBUF_CMD_OFFSET + 6);
            pTxNotify.CmdClassNotify.schedule_entry_lock_week_day_report.stop_minute = *(pCmd + CMDBUF_CMD_OFFSET + 7);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case SCHEDULE_ENTRY_LOCK_YEAR_DAY_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SCHEDULE_ENTRY_LOCK;
            pTxNotify.CmdClassNotify.cmd = SCHEDULE_ENTRY_LOCK_YEAR_DAY_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.user_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.schedule_slot_id = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.start_year = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.start_month = *(pCmd + CMDBUF_CMD_OFFSET + 4);
            pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.start_day = *(pCmd + CMDBUF_CMD_OFFSET + 5);
            pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.start_hour = *(pCmd + CMDBUF_CMD_OFFSET + 6);
            pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.start_minute = *(pCmd + CMDBUF_CMD_OFFSET + 7);
            pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.stop_year = *(pCmd + CMDBUF_CMD_OFFSET + 8);
            pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.stop_month = *(pCmd + CMDBUF_CMD_OFFSET + 9);
            pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.stop_day = *(pCmd + CMDBUF_CMD_OFFSET + 10);
            pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.stop_hour = *(pCmd + CMDBUF_CMD_OFFSET + 11);
            pTxNotify.CmdClassNotify.schedule_entry_lock_year_day_report.stop_minute = *(pCmd + CMDBUF_CMD_OFFSET + 12);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case SCHEDULE_ENTRY_LOCK_TIME_OFFSET_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SCHEDULE_ENTRY_LOCK;
            pTxNotify.CmdClassNotify.cmd = SCHEDULE_ENTRY_LOCK_TIME_OFFSET_REPORT_V2;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.schedule_entry_lock_time_offset_report.sign_TZO = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 7;
            pTxNotify.CmdClassNotify.schedule_entry_lock_time_offset_report.hour_TZO = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x7F;
            pTxNotify.CmdClassNotify.schedule_entry_lock_time_offset_report.minute_TZO = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.schedule_entry_lock_time_offset_report.sign_offset_DST = ((*(pCmd + CMDBUF_CMD_OFFSET + 3)) & 0x80) >> 7;
            pTxNotify.CmdClassNotify.schedule_entry_lock_time_offset_report.minute_offset_DST = (*(pCmd + CMDBUF_CMD_OFFSET + 3)) & 0x7F;

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case SCHEDULE_ENTRY_LOCK_DAILY_REPEATING_REPORT_V3:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SCHEDULE_ENTRY_LOCK;
            pTxNotify.CmdClassNotify.cmd = SCHEDULE_ENTRY_LOCK_DAILY_REPEATING_REPORT_V3;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.schedule_entry_lock_daily_repeating_report.user_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.schedule_entry_lock_daily_repeating_report.schedule_slot_id = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.schedule_entry_lock_daily_repeating_report.week_day_bitmask = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.schedule_entry_lock_daily_repeating_report.start_hour = *(pCmd + CMDBUF_CMD_OFFSET + 4);
            pTxNotify.CmdClassNotify.schedule_entry_lock_daily_repeating_report.start_minute = *(pCmd + CMDBUF_CMD_OFFSET + 5);
            pTxNotify.CmdClassNotify.schedule_entry_lock_daily_repeating_report.duration_hour = *(pCmd + CMDBUF_CMD_OFFSET + 6);
            pTxNotify.CmdClassNotify.schedule_entry_lock_daily_repeating_report.duration_minute = *(pCmd + CMDBUF_CMD_OFFSET + 7);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_DOOR_LOCK:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case DOOR_LOCK_OPERATION_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_DOOR_LOCK;
            pTxNotify.CmdClassNotify.cmd = DOOR_LOCK_OPERATION_REPORT;

            if (cmdLength == 7)
            {
                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.door_lock_operation_report_v1.door_lock_mode = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.door_lock_operation_report_v1.outside_door_handles_mode = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0xF0) >> 4;
                pTxNotify.CmdClassNotify.door_lock_operation_report_v1.inside_door_handles_mode = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x0F);
                pTxNotify.CmdClassNotify.door_lock_operation_report_v1.door_condition = *(pCmd + CMDBUF_CMD_OFFSET + 3);
                pTxNotify.CmdClassNotify.door_lock_operation_report_v1.lock_timeout_minutes = *(pCmd + CMDBUF_CMD_OFFSET + 4);
                pTxNotify.CmdClassNotify.door_lock_operation_report_v1.lock_timeout_seconds = *(pCmd + CMDBUF_CMD_OFFSET + 5);
            }
            else if (cmdLength > 7)
            {
                pTxNotify.CmdClassNotify.version = 3;
                pTxNotify.CmdClassNotify.door_lock_operation_report_v3.door_lock_mode = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.door_lock_operation_report_v3.outside_door_handles_mode = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0xF0) >> 4;
                pTxNotify.CmdClassNotify.door_lock_operation_report_v3.inside_door_handles_mode = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x0F);
                pTxNotify.CmdClassNotify.door_lock_operation_report_v3.door_condition = *(pCmd + CMDBUF_CMD_OFFSET + 3);
                pTxNotify.CmdClassNotify.door_lock_operation_report_v3.lock_timeout_minutes = *(pCmd + CMDBUF_CMD_OFFSET + 4);
                pTxNotify.CmdClassNotify.door_lock_operation_report_v3.lock_timeout_seconds = *(pCmd + CMDBUF_CMD_OFFSET + 5);
                pTxNotify.CmdClassNotify.door_lock_operation_report_v3.target_door_lock_mode = *(pCmd + CMDBUF_CMD_OFFSET + 6);
                pTxNotify.CmdClassNotify.door_lock_operation_report_v3.duration = *(pCmd + CMDBUF_CMD_OFFSET + 7);
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case DOOR_LOCK_CONFIGURATION_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_DOOR_LOCK;
            pTxNotify.CmdClassNotify.cmd = DOOR_LOCK_CONFIGURATION_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.door_lock_configuration_report.operation_type = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.door_lock_configuration_report.outside_door_handles_mode = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0xF0) >> 4;
            pTxNotify.CmdClassNotify.door_lock_configuration_report.inside_door_handles_mode = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x0F);
            pTxNotify.CmdClassNotify.door_lock_configuration_report.lock_timeout_minutes = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.door_lock_configuration_report.lock_timeout_seconds = *(pCmd + CMDBUF_CMD_OFFSET + 4);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_DOOR_LOCK_LOGGING:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case DOOR_LOCK_LOGGING_RECORDS_SUPPORTED_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_DOOR_LOCK_LOGGING;
            pTxNotify.CmdClassNotify.cmd = DOOR_LOCK_LOGGING_RECORDS_SUPPORTED_REPORT;
            pTxNotify.CmdClassNotify.door_lock_logging_records_supported_report.max_records_stored = *(pCmd + CMDBUF_CMD_OFFSET + 1);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        case RECORD_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_DOOR_LOCK_LOGGING;
            pTxNotify.CmdClassNotify.cmd = RECORD_REPORT;
            pTxNotify.CmdClassNotify.door_lock_logging_record_report.record_number = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.door_lock_logging_record_report.timestamp_year = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.door_lock_logging_record_report.timestamp_year =
                (pTxNotify.CmdClassNotify.door_lock_logging_record_report.timestamp_year << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.door_lock_logging_record_report.timestamp_month = *(pCmd + CMDBUF_CMD_OFFSET + 4);
            pTxNotify.CmdClassNotify.door_lock_logging_record_report.timestamp_day = *(pCmd + CMDBUF_CMD_OFFSET + 5);
            pTxNotify.CmdClassNotify.door_lock_logging_record_report.record_status = ((*(pCmd + CMDBUF_CMD_OFFSET + 6)) & 0xE0) >> 5;
            pTxNotify.CmdClassNotify.door_lock_logging_record_report.timestamp_hour_local_time = (*(pCmd + CMDBUF_CMD_OFFSET + 6)) & 0x1F;
            ;
            pTxNotify.CmdClassNotify.door_lock_logging_record_report.timestamp_minute_local_time = *(pCmd + CMDBUF_CMD_OFFSET + 7);
            pTxNotify.CmdClassNotify.door_lock_logging_record_report.timestamp_second_local_time = *(pCmd + CMDBUF_CMD_OFFSET + 8);
            pTxNotify.CmdClassNotify.door_lock_logging_record_report.event_type = *(pCmd + CMDBUF_CMD_OFFSET + 9);
            pTxNotify.CmdClassNotify.door_lock_logging_record_report.user_identifier = *(pCmd + CMDBUF_CMD_OFFSET + 10);
            pTxNotify.CmdClassNotify.door_lock_logging_record_report.user_code_length = *(pCmd + CMDBUF_CMD_OFFSET + 11);
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.door_lock_logging_record_report.user_code,
                   pCmd + CMDBUF_CMD_OFFSET + 12, pTxNotify.CmdClassNotify.door_lock_logging_record_report.user_code_length);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_BARRIER_OPERATOR:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case BARRIER_OPERATOR_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_BARRIER_OPERATOR;
            pTxNotify.CmdClassNotify.cmd = BARRIER_OPERATOR_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.barrier_operator_report.state = *(pCmd + CMDBUF_CMD_OFFSET + 1);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case BARRIER_OPERATOR_SIGNAL_SUPPORTED_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_BARRIER_OPERATOR;
            pTxNotify.CmdClassNotify.cmd = BARRIER_OPERATOR_SIGNAL_SUPPORTED_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.barrier_operator_signal_supported_report.no_bit_masks = cmdLength - 2;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.barrier_operator_signal_supported_report.bit_mask, pCmd + CMDBUF_CMD_OFFSET + 1, pTxNotify.CmdClassNotify.barrier_operator_signal_supported_report.no_bit_masks);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case BARRIER_OPERATOR_SIGNAL_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_BARRIER_OPERATOR;
            pTxNotify.CmdClassNotify.cmd = BARRIER_OPERATOR_SIGNAL_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.barrier_operator_signal_report.subsystem_type = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.barrier_operator_signal_report.subsystem_state = *(pCmd + CMDBUF_CMD_OFFSET + 2);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
        break;
    }
    break;

    case COMMAND_CLASS_IRRIGATION:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case IRRIGATION_SYSTEM_INFO_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_IRRIGATION;
            pTxNotify.CmdClassNotify.cmd = IRRIGATION_SYSTEM_INFO_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.irrigation_system_info_report.master_valve = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x01;
            pTxNotify.CmdClassNotify.irrigation_system_info_report.total_no_valves = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.irrigation_system_info_report.total_no_valve_tables = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.irrigation_system_info_report.valve_table_max_size = (*(pCmd + CMDBUF_CMD_OFFSET + 4)) & 0x0F;

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case IRRIGATION_SYSTEM_STATUS_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_IRRIGATION;
            pTxNotify.CmdClassNotify.cmd = IRRIGATION_SYSTEM_STATUS_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.irrigation_system_status_report.system_voltage = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.irrigation_system_status_report.sensor_status = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.irrigation_system_status_report.flow_precision = ((*(pCmd + CMDBUF_CMD_OFFSET + 3)) & 0xE0) >> 5;
            pTxNotify.CmdClassNotify.irrigation_system_status_report.flow_scale = ((*(pCmd + CMDBUF_CMD_OFFSET + 3)) & 0x18) >> 3;
            pTxNotify.CmdClassNotify.irrigation_system_status_report.flow_size = (*(pCmd + CMDBUF_CMD_OFFSET + 3)) & 0x07;

            int index;
            int j;
            pTxNotify.CmdClassNotify.irrigation_system_status_report.flow_value = 0;
            for (index = 0; index < pTxNotify.CmdClassNotify.irrigation_system_status_report.flow_size; index++)
            {
                pTxNotify.CmdClassNotify.irrigation_system_status_report.flow_value = (pTxNotify.CmdClassNotify.irrigation_system_status_report.flow_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 4 + index);
            }
            j = pTxNotify.CmdClassNotify.irrigation_system_status_report.flow_size + 3;
            pTxNotify.CmdClassNotify.irrigation_system_status_report.pressure_precision = ((*(pCmd + CMDBUF_CMD_OFFSET + j + 1)) & 0xE0) >> 5;
            pTxNotify.CmdClassNotify.irrigation_system_status_report.pressure_scale = ((*(pCmd + CMDBUF_CMD_OFFSET + j + 1)) & 0x18) >> 3;
            pTxNotify.CmdClassNotify.irrigation_system_status_report.pressure_size = (*(pCmd + CMDBUF_CMD_OFFSET + j + 1)) & 0x07;

            pTxNotify.CmdClassNotify.irrigation_system_status_report.pressure_value = 0;

            for (index = 0; index < pTxNotify.CmdClassNotify.irrigation_system_status_report.pressure_size; index++)
            {
                pTxNotify.CmdClassNotify.irrigation_system_status_report.pressure_value = (pTxNotify.CmdClassNotify.irrigation_system_status_report.pressure_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + j + 2 + index);
            }
            j = j + 1 + pTxNotify.CmdClassNotify.irrigation_system_status_report.pressure_size;

            pTxNotify.CmdClassNotify.irrigation_system_status_report.shutoff_duration = *(pCmd + CMDBUF_CMD_OFFSET + j + 1);
            pTxNotify.CmdClassNotify.irrigation_system_status_report.system_error_status = *(pCmd + CMDBUF_CMD_OFFSET + j + 2);
            pTxNotify.CmdClassNotify.irrigation_system_status_report.master_valve = (*(pCmd + CMDBUF_CMD_OFFSET + j + 3)) & 0x01;
            pTxNotify.CmdClassNotify.irrigation_system_status_report.valve_id = *(pCmd + CMDBUF_CMD_OFFSET + j + 4);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case IRRIGATION_SYSTEM_CONFIG_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_IRRIGATION;
            pTxNotify.CmdClassNotify.cmd = IRRIGATION_SYSTEM_CONFIG_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.irrigation_system_config_report.master_valve_delay = *(pCmd + CMDBUF_CMD_OFFSET + 1);

            pTxNotify.CmdClassNotify.irrigation_system_config_report.HPT_precision = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0xE0) >> 5;
            pTxNotify.CmdClassNotify.irrigation_system_config_report.HPT_scale = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x18) >> 3;
            pTxNotify.CmdClassNotify.irrigation_system_config_report.HPT_size = (*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x07;

            int index;
            int j;
            pTxNotify.CmdClassNotify.irrigation_system_config_report.HPT_value = 0;
            for (index = 0; index < pTxNotify.CmdClassNotify.irrigation_system_config_report.HPT_size; index++)
            {
                pTxNotify.CmdClassNotify.irrigation_system_config_report.HPT_value = (pTxNotify.CmdClassNotify.irrigation_system_config_report.HPT_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3 + index);
            }
            j = pTxNotify.CmdClassNotify.irrigation_system_config_report.HPT_size + 2;

            pTxNotify.CmdClassNotify.irrigation_system_config_report.LPT_precision = ((*(pCmd + CMDBUF_CMD_OFFSET + j + 1)) & 0xE0) >> 5;
            pTxNotify.CmdClassNotify.irrigation_system_config_report.LPT_scale = ((*(pCmd + CMDBUF_CMD_OFFSET + j + 1)) & 0x18) >> 3;
            pTxNotify.CmdClassNotify.irrigation_system_config_report.LPT_size = (*(pCmd + CMDBUF_CMD_OFFSET + j + 1)) & 0x07;

            pTxNotify.CmdClassNotify.irrigation_system_config_report.LPT_value = 0;
            for (index = 0; index < pTxNotify.CmdClassNotify.irrigation_system_config_report.LPT_size; index++)
            {
                pTxNotify.CmdClassNotify.irrigation_system_config_report.LPT_value = (pTxNotify.CmdClassNotify.irrigation_system_config_report.LPT_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + j + 2 + index);
            }
            j = j + 1 + pTxNotify.CmdClassNotify.irrigation_system_config_report.LPT_size;

            pTxNotify.CmdClassNotify.irrigation_system_config_report.sensor_polarity = *(pCmd + CMDBUF_CMD_OFFSET + j + 1);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case IRRIGATION_VALVE_INFO_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_IRRIGATION;
            pTxNotify.CmdClassNotify.cmd = IRRIGATION_VALVE_INFO_REPORT;

            pTxNotify.CmdClassNotify.irrigation_valve_info_report.connected = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x02) >> 1;
            pTxNotify.CmdClassNotify.irrigation_valve_info_report.master_valve = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x01);
            pTxNotify.CmdClassNotify.irrigation_valve_info_report.valve_id = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.irrigation_valve_info_report.nominal_current = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.irrigation_valve_info_report.valve_error_status = *(pCmd + CMDBUF_CMD_OFFSET + 4);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case IRRIGATION_VALVE_CONFIG_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_IRRIGATION;
            pTxNotify.CmdClassNotify.cmd = IRRIGATION_VALVE_CONFIG_REPORT;

            pTxNotify.CmdClassNotify.irrigation_valve_config_report.master_valve = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x01);
            pTxNotify.CmdClassNotify.irrigation_valve_config_report.valve_id = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.irrigation_valve_config_report.nominal_current_high_threshold = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.irrigation_valve_config_report.nominal_current_low_threshold = *(pCmd + CMDBUF_CMD_OFFSET + 4);

            pTxNotify.CmdClassNotify.irrigation_valve_config_report.maximum_flow_precision = ((*(pCmd + CMDBUF_CMD_OFFSET + 5)) & 0xE0) >> 5;
            pTxNotify.CmdClassNotify.irrigation_valve_config_report.maximum_flow_scale = ((*(pCmd + CMDBUF_CMD_OFFSET + 5)) & 0x18) >> 3;
            pTxNotify.CmdClassNotify.irrigation_valve_config_report.maximum_flow_size = (*(pCmd + CMDBUF_CMD_OFFSET + 5)) & 0x07;

            int index;
            int j;
            pTxNotify.CmdClassNotify.irrigation_valve_config_report.maximum_flow_value = 0;
            for (index = 0; index < pTxNotify.CmdClassNotify.irrigation_valve_config_report.maximum_flow_size; index++)
            {
                pTxNotify.CmdClassNotify.irrigation_valve_config_report.maximum_flow_value = (pTxNotify.CmdClassNotify.irrigation_valve_config_report.maximum_flow_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 6 + index);
            }
            j = pTxNotify.CmdClassNotify.irrigation_valve_config_report.maximum_flow_size + 5;

            pTxNotify.CmdClassNotify.irrigation_valve_config_report.FHT_precision = ((*(pCmd + CMDBUF_CMD_OFFSET + j + 1)) & 0xE0) >> 5;
            pTxNotify.CmdClassNotify.irrigation_valve_config_report.FHT_scale = ((*(pCmd + CMDBUF_CMD_OFFSET + j + 1)) & 0x18) >> 3;
            pTxNotify.CmdClassNotify.irrigation_valve_config_report.FHT_size = (*(pCmd + CMDBUF_CMD_OFFSET + j + 1)) & 0x07;

            pTxNotify.CmdClassNotify.irrigation_valve_config_report.FHT_value = 0;

            for (index = 0; index < pTxNotify.CmdClassNotify.irrigation_valve_config_report.FHT_size; index++)
            {
                pTxNotify.CmdClassNotify.irrigation_valve_config_report.FHT_value = (pTxNotify.CmdClassNotify.irrigation_valve_config_report.FHT_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + j + 2 + index);
            }

            j = j + 1 + pTxNotify.CmdClassNotify.irrigation_valve_config_report.FHT_size;

            pTxNotify.CmdClassNotify.irrigation_valve_config_report.FLT_precision = ((*(pCmd + CMDBUF_CMD_OFFSET + j + 1)) & 0xE0) >> 5;
            pTxNotify.CmdClassNotify.irrigation_valve_config_report.FLT_scale = ((*(pCmd + CMDBUF_CMD_OFFSET + j + 1)) & 0x18) >> 3;
            pTxNotify.CmdClassNotify.irrigation_valve_config_report.FLT_size = (*(pCmd + CMDBUF_CMD_OFFSET + j + 1)) & 0x07;

            pTxNotify.CmdClassNotify.irrigation_valve_config_report.FLT_value = 0;

            for (index = 0; index < pTxNotify.CmdClassNotify.irrigation_valve_config_report.FLT_size; index++)
            {
                pTxNotify.CmdClassNotify.irrigation_valve_config_report.FLT_value = (pTxNotify.CmdClassNotify.irrigation_valve_config_report.FLT_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + j + 2 + index);
            }

            j = j + 1 + pTxNotify.CmdClassNotify.irrigation_valve_config_report.FLT_size;

            pTxNotify.CmdClassNotify.irrigation_valve_config_report.sensor_usage = *(pCmd + CMDBUF_CMD_OFFSET + j + 1);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case IRRIGATION_VALVE_TABLE_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_IRRIGATION;
            pTxNotify.CmdClassNotify.cmd = IRRIGATION_VALVE_TABLE_REPORT;

            pTxNotify.CmdClassNotify.irrigation_valve_table_report.valve_table_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.irrigation_valve_table_report.no_valves = (cmdLength - 3) / 3;

            int index;
            for (index = 0; index < pTxNotify.CmdClassNotify.irrigation_valve_table_report.no_valves; index++)
            {
                pTxNotify.CmdClassNotify.irrigation_valve_table_report.valve_list[index].valve_id = *(pCmd + CMDBUF_CMD_OFFSET + 2 + index * 3);
                pTxNotify.CmdClassNotify.irrigation_valve_table_report.valve_list[index].duration = *(pCmd + CMDBUF_CMD_OFFSET + 3 + index * 3);
                pTxNotify.CmdClassNotify.irrigation_valve_table_report.valve_list[index].duration = (pTxNotify.CmdClassNotify.irrigation_valve_table_report.valve_list[index].duration << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 4 + index * 3);
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_LANGUAGE:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case LANGUAGE_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_LANGUAGE;
            pTxNotify.CmdClassNotify.cmd = LANGUAGE_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.language_report.language_1 = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.language_report.language_2 = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.language_report.language_3 = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.language_report.country = *(pCmd + CMDBUF_CMD_OFFSET + 4);
            pTxNotify.CmdClassNotify.language_report.country = (pTxNotify.CmdClassNotify.language_report.country << 8) + (*(pCmd + CMDBUF_CMD_OFFSET + 5));

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;
    case COMMAND_CLASS_SENSOR_CONFIGURATION:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case SENSOR_TRIGGER_LEVEL_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SENSOR_CONFIGURATION;
            pTxNotify.CmdClassNotify.cmd = SENSOR_TRIGGER_LEVEL_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.sensor_trigger_level_report.sensor_type = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.sensor_trigger_level_report.precision = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0xE0) >> 5;
            pTxNotify.CmdClassNotify.sensor_trigger_level_report.scale = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x18) >> 3;
            pTxNotify.CmdClassNotify.sensor_trigger_level_report.size = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x07);

            int index;
            pTxNotify.CmdClassNotify.sensor_trigger_level_report.trigger_value = 0;
            for (index = 0; index < pTxNotify.CmdClassNotify.sensor_trigger_level_report.size; index++)
            {
                pTxNotify.CmdClassNotify.sensor_trigger_level_report.trigger_value = (pTxNotify.CmdClassNotify.sensor_trigger_level_report.trigger_value << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3 + index);
            }
            pTxNotify.CmdClassNotify.sensor_trigger_level_report.trigger_value =
                ConvertValuetoInt32(pTxNotify.CmdClassNotify.sensor_trigger_level_report.size,
                                    pTxNotify.CmdClassNotify.sensor_trigger_level_report.trigger_value);
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_SIMPLE_AV_CONTROL:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case SIMPLE_AV_CONTROL_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SIMPLE_AV_CONTROL;
            pTxNotify.CmdClassNotify.cmd = SIMPLE_AV_CONTROL_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.simple_av_control_report.no_reports = *(pCmd + CMDBUF_CMD_OFFSET + 1);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case SIMPLE_AV_CONTROL_SUPPORTED_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SIMPLE_AV_CONTROL;
            pTxNotify.CmdClassNotify.cmd = SIMPLE_AV_CONTROL_SUPPORTED_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.simple_av_control_supported_report.report_no = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.simple_av_control_supported_report.no_bit_masks = cmdLength - 3;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.simple_av_control_supported_report.bit_mask, pCmd + CMDBUF_CMD_OFFSET + 2, pTxNotify.CmdClassNotify.simple_av_control_supported_report.no_bit_masks);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }

    break;

    case COMMAND_CLASS_USER_CODE:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case USER_CODE_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_USER_CODE;
            pTxNotify.CmdClassNotify.cmd = USER_CODE_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.user_code_report.user_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.user_code_report.user_id_status = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.user_code_report.no_user_code = cmdLength - 4;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.user_code_report.user_code, pCmd + CMDBUF_CMD_OFFSET + 3, pTxNotify.CmdClassNotify.user_code_report.no_user_code);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case USERS_NUMBER_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_USER_CODE;
            pTxNotify.CmdClassNotify.cmd = USERS_NUMBER_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.users_number_report.supported_users = *(pCmd + CMDBUF_CMD_OFFSET + 1);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_WINDOW_COVERING:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case WINDOW_COVERING_SUPPORTED_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_WINDOW_COVERING;
            pTxNotify.CmdClassNotify.cmd = WINDOW_COVERING_SUPPORTED_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.window_covering_supported_report.no_param_masks = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x0F;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.window_covering_supported_report.param_mask, pCmd + CMDBUF_CMD_OFFSET + 2, pTxNotify.CmdClassNotify.window_covering_supported_report.no_param_masks);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case WINDOW_COVERING_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_WINDOW_COVERING;
            pTxNotify.CmdClassNotify.cmd = WINDOW_COVERING_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.window_covering_report.param_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.window_covering_report.value = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.window_covering_report.target_value = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.window_covering_report.duration = *(pCmd + CMDBUF_CMD_OFFSET + 4);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;
    case COMMAND_CLASS_CLOCK:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case CLOCK_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_CLOCK;
            pTxNotify.CmdClassNotify.cmd = CLOCK_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.clock_report.weekday = ((*(pCmd + CMDBUF_CMD_OFFSET + 1) & 0xE0) >> 5);
            pTxNotify.CmdClassNotify.clock_report.hour = (*(pCmd + CMDBUF_CMD_OFFSET + 1) & 0x1F);
            pTxNotify.CmdClassNotify.clock_report.minute = *(pCmd + CMDBUF_CMD_OFFSET + 2);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;
    case COMMAND_CLASS_ANTITHEFT:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case ANTITHEFT_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_ANTITHEFT;
            pTxNotify.CmdClassNotify.cmd = ANTITHEFT_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.antitheft_report.protection_status = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.antitheft_report.manufacturer_id = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.antitheft_report.manufacturer_id =
                (pTxNotify.CmdClassNotify.antitheft_report.manufacturer_id << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3);
            pTxNotify.CmdClassNotify.antitheft_report.no_anti_theft_hint = *(pCmd + CMDBUF_CMD_OFFSET + 4);
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.antitheft_report.hint_list, pCmd + CMDBUF_CMD_OFFSET + 5,
                   pTxNotify.CmdClassNotify.antitheft_report.no_anti_theft_hint);
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_INDICATOR:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case INDICATOR_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_INDICATOR;
            pTxNotify.CmdClassNotify.cmd = INDICATOR_REPORT;
            if (cmdLength == 3)
            {
                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.indicator_report.value = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            }
            else
            {
                pTxNotify.CmdClassNotify.indicator_report_v2.indicator_0_value = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.indicator_report_v2.indicator_object_count = ((*(pCmd + CMDBUF_CMD_OFFSET + 2)) & 0x1F);

                int index;

                for (index = 0; index <= pTxNotify.CmdClassNotify.indicator_report_v2.indicator_object_count; index++)
                {
                    pTxNotify.CmdClassNotify.indicator_report_v2.indicator_object[index].indicator_id = *(pCmd + CMDBUF_CMD_OFFSET + 3 + index * 3);
                    pTxNotify.CmdClassNotify.indicator_report_v2.indicator_object[index].property_id = *(pCmd + CMDBUF_CMD_OFFSET + 4 + index * 3);
                    pTxNotify.CmdClassNotify.indicator_report_v2.indicator_object[index].value = *(pCmd + CMDBUF_CMD_OFFSET + 5 + index * 3);
                }      
            }
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        case INDICATOR_SUPPORTED_REPORT_V2:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_INDICATOR;
            pTxNotify.CmdClassNotify.cmd = INDICATOR_SUPPORTED_REPORT_V2;
            pTxNotify.CmdClassNotify.version = 2;
            pTxNotify.CmdClassNotify.indicator_support_report.indicator_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.indicator_support_report.next_indicator_id = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.indicator_support_report.property_supported_bit_mask_length = ((*(pCmd + CMDBUF_CMD_OFFSET + 3)) & 0x1F);
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.indicator_support_report.property_supported_bit_mask_list, pCmd + CMDBUF_CMD_OFFSET + 4, 
                    pTxNotify.CmdClassNotify.indicator_support_report.property_supported_bit_mask_length);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;
    case COMMAND_CLASS_NODE_NAMING:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case NODE_NAMING_NODE_NAME_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_NODE_NAMING;
            pTxNotify.CmdClassNotify.cmd = NODE_NAMING_NODE_NAME_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.node_name_report.char_presentation = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x07;
            pTxNotify.CmdClassNotify.node_name_report.no_node_name = cmdLength - 3;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.node_name_report.node_name_list,
                   pCmd + CMDBUF_CMD_OFFSET + 2, pTxNotify.CmdClassNotify.node_name_report.no_node_name);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        case NODE_NAMING_NODE_LOCATION_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_NODE_NAMING;
            pTxNotify.CmdClassNotify.cmd = NODE_NAMING_NODE_NAME_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.node_location_report.char_presentation = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x07;
            pTxNotify.CmdClassNotify.node_location_report.no_node_location = cmdLength - 3;
            memcpy((uint8_t *)pTxNotify.CmdClassNotify.node_location_report.node_location_list,
                   pCmd + CMDBUF_CMD_OFFSET + 2, pTxNotify.CmdClassNotify.node_location_report.no_node_location);
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_FIRMWARE_UPDATE_MD:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case FIRMWARE_MD_REPORT:
        {
            if (8 == cmdLength)
            {
                pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_FIRMWARE_UPDATE_MD;
                pTxNotify.CmdClassNotify.cmd = FIRMWARE_MD_REPORT;

                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.firmware_md_report.manufacturer_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.firmware_md_report.manufacturer_id =
                    (pTxNotify.CmdClassNotify.firmware_md_report.manufacturer_id << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 2);
                pTxNotify.CmdClassNotify.firmware_md_report.firmware_id = *(pCmd + CMDBUF_CMD_OFFSET + 3);
                pTxNotify.CmdClassNotify.firmware_md_report.firmware_id =
                    (pTxNotify.CmdClassNotify.firmware_md_report.firmware_id << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 4);
                pTxNotify.CmdClassNotify.firmware_md_report.checksum = *(pCmd + CMDBUF_CMD_OFFSET + 5);
                pTxNotify.CmdClassNotify.firmware_md_report.checksum =
                    (pTxNotify.CmdClassNotify.firmware_md_report.checksum << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 6);
                PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
            }
            else
            {
                pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_FIRMWARE_UPDATE_MD;
                pTxNotify.CmdClassNotify.cmd = FIRMWARE_MD_REPORT;

                pTxNotify.CmdClassNotify.version = 3;
                pTxNotify.CmdClassNotify.firmware_md_report_v3.manufacturer_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.firmware_md_report_v3.manufacturer_id =
                    (pTxNotify.CmdClassNotify.firmware_md_report_v3.manufacturer_id << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 2);
                pTxNotify.CmdClassNotify.firmware_md_report_v3.firmware0_id = *(pCmd + CMDBUF_CMD_OFFSET + 3);
                pTxNotify.CmdClassNotify.firmware_md_report_v3.firmware0_id =
                    (pTxNotify.CmdClassNotify.firmware_md_report_v3.firmware0_id << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 4);
                pTxNotify.CmdClassNotify.firmware_md_report_v3.firmware0_checksum = *(pCmd + CMDBUF_CMD_OFFSET + 5);
                pTxNotify.CmdClassNotify.firmware_md_report_v3.firmware0_checksum =
                    (pTxNotify.CmdClassNotify.firmware_md_report_v3.firmware0_checksum << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 6);
                pTxNotify.CmdClassNotify.firmware_md_report_v3.firmware_upgradable = *(pCmd + CMDBUF_CMD_OFFSET + 7);
                pTxNotify.CmdClassNotify.firmware_md_report_v3.no_firmware_targets = *(pCmd + CMDBUF_CMD_OFFSET + 8);
                pTxNotify.CmdClassNotify.firmware_md_report_v3.max_fragment_size = *(pCmd + CMDBUF_CMD_OFFSET + 9);
                pTxNotify.CmdClassNotify.firmware_md_report_v3.max_fragment_size =
                    (pTxNotify.CmdClassNotify.firmware_md_report_v3.max_fragment_size << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 10);
                int index;
                for (index = 0; index < pTxNotify.CmdClassNotify.firmware_md_report_v3.no_firmware_targets; index++)
                {
                    pTxNotify.CmdClassNotify.firmware_md_report_v3.firmware_id_list[index] = *(pCmd + CMDBUF_CMD_OFFSET + 11 + index * 2);
                    pTxNotify.CmdClassNotify.firmware_md_report_v3.firmware_id_list[index] =
                        (pTxNotify.CmdClassNotify.firmware_md_report_v3.firmware_id_list[index] << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 12 + index * 2);
                }
                PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
            }
            
        }
        break;

        case FIRMWARE_UPDATE_MD_REQUEST_REPORT:
        {

            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_FIRMWARE_UPDATE_MD;
            pTxNotify.CmdClassNotify.cmd = FIRMWARE_UPDATE_MD_REQUEST_REPORT;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.firmware_update_md_request_report.status = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case FIRMWARE_UPDATE_MD_GET:
        { 

            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_FIRMWARE_UPDATE_MD;
            pTxNotify.CmdClassNotify.cmd = FIRMWARE_UPDATE_MD_GET;

            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.firmware_update_md_get.no_reports = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.firmware_update_md_get.report_number = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.firmware_update_md_get.report_number =
                (pTxNotify.CmdClassNotify.firmware_update_md_get.report_number << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3);
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));

        }
        break;

        case FIRMWARE_UPDATE_MD_REPORT:
        { 
            /*send FIRMWARE_UPDATE_MD_REPORT to device*/
            uint16_t temp_checksum; //check FIRMWARE_UPDATE_MD_REPORT have checksum ? because don't know lenth of data
            temp_checksum = *(pCmd + cmdLength - 2);
            temp_checksum = (temp_checksum << 8) + *(pCmd + cmdLength - 1);
            if (ccittCrc16(0x1D0F, pCmd, cmdLength - 2) == temp_checksum)
            {
                pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_FIRMWARE_UPDATE_MD;
                pTxNotify.CmdClassNotify.cmd = FIRMWARE_UPDATE_MD_REPORT;

                pTxNotify.CmdClassNotify.version = 2;
                pTxNotify.CmdClassNotify.firmware_update_md_report_v2.last = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 7;
                pTxNotify.CmdClassNotify.firmware_update_md_report_v2.report_number = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x7F;
                pTxNotify.CmdClassNotify.firmware_update_md_report_v2.report_number =
                    (pTxNotify.CmdClassNotify.firmware_update_md_report_v2.report_number << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 2);
                pTxNotify.CmdClassNotify.firmware_update_md_report_v2.no_data = cmdLength - 6;
                memcpy((uint8_t *)pTxNotify.CmdClassNotify.firmware_update_md_report_v2.data_list,
                       pCmd + CMDBUF_CMD_OFFSET + 3, pTxNotify.CmdClassNotify.firmware_update_md_report_v2.no_data);
                pTxNotify.CmdClassNotify.firmware_update_md_report_v2.checksum = temp_checksum;
            }
            else
            {
                pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_FIRMWARE_UPDATE_MD;
                pTxNotify.CmdClassNotify.cmd = FIRMWARE_UPDATE_MD_REPORT;

                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.firmware_update_md_report.last = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 7;
                pTxNotify.CmdClassNotify.firmware_update_md_report.report_number = (*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x7F;
                pTxNotify.CmdClassNotify.firmware_update_md_report.report_number =
                    (pTxNotify.CmdClassNotify.firmware_update_md_report.report_number << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 2);
                pTxNotify.CmdClassNotify.firmware_update_md_report.no_data = cmdLength - 4;
                memcpy((uint8_t *)pTxNotify.CmdClassNotify.firmware_update_md_report.data_list,
                       pCmd + CMDBUF_CMD_OFFSET + 3, pTxNotify.CmdClassNotify.firmware_update_md_report.no_data);
            }

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;

        case FIRMWARE_UPDATE_MD_STATUS_REPORT:
        {

            if (3 == cmdLength)
            { 
                pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_FIRMWARE_UPDATE_MD;
                pTxNotify.CmdClassNotify.cmd = FIRMWARE_UPDATE_MD_STATUS_REPORT;

                pTxNotify.CmdClassNotify.version = 1;
                pTxNotify.CmdClassNotify.firmware_update_md_status_report.status = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
            }
            else
            {
                pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_FIRMWARE_UPDATE_MD;
                pTxNotify.CmdClassNotify.cmd = FIRMWARE_UPDATE_MD_STATUS_REPORT;

                pTxNotify.CmdClassNotify.version = 3;
                pTxNotify.CmdClassNotify.firmware_update_md_status_report_v3.status = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.firmware_update_md_status_report_v3.wait_time = *(pCmd + CMDBUF_CMD_OFFSET + 2);
                pTxNotify.CmdClassNotify.firmware_update_md_status_report_v3.wait_time =
                    (pTxNotify.CmdClassNotify.firmware_update_md_status_report_v3.wait_time << 8) + *(pCmd + CMDBUF_CMD_OFFSET + 3);
                PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
            }

        }
        break;
        }
    }
    break;
    case COMMAND_CLASS_SCENE_ACTUATOR_CONF:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case SCENE_ACTUATOR_CONF_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SCENE_ACTUATOR_CONF;
            pTxNotify.CmdClassNotify.cmd = SCENE_ACTUATOR_CONF_REPORT;
            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.scene_actuator_conf_report.scene_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.scene_actuator_conf_report.level = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.scene_actuator_conf_report.dimming_duration = *(pCmd + CMDBUF_CMD_OFFSET + 3);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_SCENE_CONTROLLER_CONF:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case SCENE_CONTROLLER_CONF_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SCENE_CONTROLLER_CONF;
            pTxNotify.CmdClassNotify.cmd = SCENE_CONTROLLER_CONF_REPORT;
            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.scene_controller_conf_report.group_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.scene_controller_conf_report.scene_id = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.scene_controller_conf_report.dimming_duration = *(pCmd + CMDBUF_CMD_OFFSET + 3);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_CENTRAL_SCENE:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case  CENTRAL_SCENE_SUPPORTED_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_CENTRAL_SCENE;
            pTxNotify.CmdClassNotify.cmd =  CENTRAL_SCENE_SUPPORTED_REPORT;

            if (cmdLength == 3)
            {
                pTxNotify.CmdClassNotify.version = CENTRAL_SCENE_VERSION;
                pTxNotify.CmdClassNotify.central_scene_supported_report.supported_scenes = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            }
            else if (cmdLength > 3)
            {
                pTxNotify.CmdClassNotify.version = CENTRAL_SCENE_VERSION_V2;
                pTxNotify.CmdClassNotify.central_scene_supported_report_v2.supported_scenes = *(pCmd + CMDBUF_CMD_OFFSET + 1);
                pTxNotify.CmdClassNotify.central_scene_supported_report_v2.identical = (*(pCmd + CMDBUF_CMD_OFFSET + 2) & CENTRAL_SCENE_SUPPORTED_REPORT_PROPERTIES1_IDENTICAL_BIT_MASK_V2) ? 1:0;
                pTxNotify.CmdClassNotify.central_scene_notification.slow_refresh = (*(pCmd + CMDBUF_CMD_OFFSET + 2) & CENTRAL_SCENE_SUPPORTED_REPORT_PROPERTIES1_SLOW_REFRESH_SUPPORT_BIT_MASK_V3) ? 1:0;
                pTxNotify.CmdClassNotify.central_scene_supported_report_v2.no_bit_masks = 
                            (*(pCmd + CMDBUF_CMD_OFFSET + 2) & CENTRAL_SCENE_SUPPORTED_REPORT_PROPERTIES1_NUMBER_OF_BIT_MASK_BYTES_MASK_V2) >> CENTRAL_SCENE_SUPPORTED_REPORT_PROPERTIES1_NUMBER_OF_BIT_MASK_BYTES_SHIFT_V2;
                int sceneId;
                for(sceneId = 0; sceneId < pTxNotify.CmdClassNotify.central_scene_supported_report_v2.supported_scenes; sceneId ++)
                {
                    if(pTxNotify.CmdClassNotify.central_scene_supported_report_v2.identical && sceneId)
                    {
                        memcpy((uint8_t *)&pTxNotify.CmdClassNotify.central_scene_supported_report_v2.supported_key_attributes[sceneId][0], 
                            (uint8_t *)&pTxNotify.CmdClassNotify.central_scene_supported_report_v2.supported_key_attributes[0][0], 
                            pTxNotify.CmdClassNotify.central_scene_supported_report_v2.no_bit_masks);
                    }
                    else
                    {
                        memcpy((uint8_t *)&pTxNotify.CmdClassNotify.central_scene_supported_report_v2.supported_key_attributes[sceneId][0], 
                            (pCmd + CMDBUF_CMD_OFFSET + 3 + sceneId * pTxNotify.CmdClassNotify.central_scene_supported_report_v2.no_bit_masks), 
                            pTxNotify.CmdClassNotify.central_scene_supported_report_v2.no_bit_masks);
                    }
                }
            }
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;


        case CENTRAL_SCENE_NOTIFICATION:
        {
            uint16_t extendId = (p->sendpoint << 8) + p->snode;
            uint8_t key_attributes = (*(pCmd + CMDBUF_CMD_OFFSET + 2) & CENTRAL_SCENE_NOTIFICATION_PROPERTIES1_KEY_ATTRIBUTES_MASK_V2);
            uint8_t slow_refresh = (*(pCmd + CMDBUF_CMD_OFFSET + 2) & CENTRAL_SCENE_SUPPORTED_REPORT_PROPERTIES1_SLOW_REFRESH_SUPPORT_BIT_MASK_V3) ? 1:0;
            uint8_t scene_number = *(pCmd + CMDBUF_CMD_OFFSET + 3);
            uint8_t sequence_number = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            uint16_t timeoutSendKeyReleased = slow_refresh ? NEW_KEY_HELD_DOWN_NOTIFICATION:ANY_KEY_HELD_DOWN_NOTIFICATION;
            handleKeyHeldDownMessage_t *tmp = getKeyHeldDownMessageListFromExtendId(extendId);
            handleSequenceNumber_t *tmpSequenceNumber = getSequenceNumberFromExtendId(extendId);
            if(tmpSequenceNumber)
            {
                if(tmpSequenceNumber->sequenceNumber == sequence_number)
                {
                    mainlog(logDebug, "CENTRAL_SCENE_NOTIFICATION --> duplicated sequence nummber for NodeId: 0x%04X", extendId);
                    break;
                }
                tmpSequenceNumber->sequenceNumber = sequence_number;
            }
            else
            {
                handleSequenceNumber_t* tempSeqNo = (handleSequenceNumber_t*)malloc(sizeof(handleSequenceNumber_t));
                memset(tempSeqNo, 0x00, sizeof(handleSequenceNumber_t));
                tempSeqNo->sequenceNumber = sequence_number;
                tempSeqNo->extendId = extendId;
                addSequenceNumberLastList((void *)tempSeqNo);
            }
            if(tmp)
            {
                tmp->sceneNumber = scene_number;
                tmp->keyAyttributes = key_attributes;
                tmp->sequenceNumber = sequence_number;
                if(key_attributes == CENTRAL_SCENE_NOTIFICATION_KEY_ATTRIBUTES_KEY_HELD_DOWN_V2)
                {
                    tmp->receiveMessageKeyReleased = false;
                    //check duplicate message
                    if (CheckCentralSceneReportCommand(p,
                        scene_number, sequence_number, timeoutSendKeyReleased,
                        key_attributes))
                    
                    {
                        mainlog(logDebug, "Got a duplicated message for Central Scene Report, key held down!!!!!");
                        break;
                    }
                }
                if(key_attributes == CENTRAL_SCENE_NOTIFICATION_KEY_ATTRIBUTES_KEY_RELEASED_V2)
                {
                    tmp->receiveMessageKeyReleased = true;
                } 
            }
            else
            {//new node
                handleKeyHeldDownMessage_t* tempMessageHeldDown = (handleKeyHeldDownMessage_t*)malloc(sizeof(handleKeyHeldDownMessage_t));
                memset(tempMessageHeldDown, 0x00, sizeof(handleKeyHeldDownMessage_t));
                memcpy((uint8_t *)&tempMessageHeldDown->p, p, sizeof(ts_param_t));
                tempMessageHeldDown->sceneNumber = scene_number;
                tempMessageHeldDown->keyAyttributes = key_attributes;
                tempMessageHeldDown->timeout = timeoutSendKeyReleased;
                tempMessageHeldDown->extendId = extendId;
                tempMessageHeldDown->sequenceNumber = sequence_number;
                if(key_attributes == CENTRAL_SCENE_NOTIFICATION_KEY_ATTRIBUTES_KEY_HELD_DOWN_V2)
                {
                    tempMessageHeldDown->receiveMessageKeyReleased = false;
                    mainlog(logDebug, "Start Timer Handler Message Key Released for nodeId: 0x%04X", tempMessageHeldDown->extendId);
                    timerStart(&(tempMessageHeldDown->timerId), ZCB_TimerOutHandlerMessageKeyReleased, (void*)&tempMessageHeldDown->extendId, timeoutSendKeyReleased, TIMER_ONETIME);
                }
                if(key_attributes == CENTRAL_SCENE_NOTIFICATION_KEY_ATTRIBUTES_KEY_RELEASED_V2)
                {
                    tempMessageHeldDown->receiveMessageKeyReleased = true;
                }
                addKeyHeldDownMessageList((void *)tempMessageHeldDown);
            }


            pTxNotify.CmdClassNotify.central_scene_notification.key_attributes = key_attributes;
            pTxNotify.CmdClassNotify.central_scene_notification.slow_refresh = slow_refresh;
            pTxNotify.CmdClassNotify.central_scene_notification.scene_number = scene_number;
            pTxNotify.CmdClassNotify.central_scene_notification.sequence_number = sequence_number;
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_CENTRAL_SCENE;
            pTxNotify.CmdClassNotify.cmd = CENTRAL_SCENE_NOTIFICATION;
            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        case CENTRAL_SCENE_CONFIGURATION_REPORT_V3:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_CENTRAL_SCENE;
            pTxNotify.CmdClassNotify.cmd = CENTRAL_SCENE_CONFIGURATION_REPORT_V3;
            pTxNotify.CmdClassNotify.central_scene_configuration_report.slow_refresh = (*(pCmd + CMDBUF_CMD_OFFSET + 1) & CENTRAL_SCENE_SUPPORTED_REPORT_PROPERTIES1_SLOW_REFRESH_SUPPORT_BIT_MASK_V3) ? 1:0;

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_SCENE_ACTIVATION:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case SCENE_ACTIVATION_SET:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SCENE_ACTIVATION;
            pTxNotify.CmdClassNotify.cmd = SCENE_ACTIVATION_SET;
            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.scene_activation_set.scene_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.scene_activation_set.dimming_duration = *(pCmd + CMDBUF_CMD_OFFSET + 2);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;

    case COMMAND_CLASS_SUPERVISION:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case SUPERVISION_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_SUPERVISION;
            pTxNotify.CmdClassNotify.cmd = SUPERVISION_REPORT;
            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.supervision_report.manual_supervision = true;;
            pTxNotify.CmdClassNotify.supervision_report.more_status_updates = ((*(pCmd + CMDBUF_CMD_OFFSET + 1)) & 0x80) >> 7;
            pTxNotify.CmdClassNotify.supervision_report.session_id = *(pCmd + CMDBUF_CMD_OFFSET + 1) & 0x3F;
            pTxNotify.CmdClassNotify.supervision_report.status = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.supervision_report.duration = *(pCmd + CMDBUF_CMD_OFFSET + 3);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));

        }
        break;
        }
    }
    break;
    case COMMAND_CLASS_REMOTE_ASSOCIATION:
    {
        switch (*(pCmd + CMDBUF_CMD_OFFSET))
        {
        case REMOTE_ASSOCIATION_CONFIGURATION_REPORT:
        {
            pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_REMOTE_ASSOCIATION;
            pTxNotify.CmdClassNotify.cmd = REMOTE_ASSOCIATION_CONFIGURATION_REPORT;
            pTxNotify.CmdClassNotify.version = 1;
            pTxNotify.CmdClassNotify.remote_association_configuration_report.local_grouping_id = *(pCmd + CMDBUF_CMD_OFFSET + 1);
            pTxNotify.CmdClassNotify.remote_association_configuration_report.remote_node_id = *(pCmd + CMDBUF_CMD_OFFSET + 2);
            pTxNotify.CmdClassNotify.remote_association_configuration_report.remote_grouping_id = *(pCmd + CMDBUF_CMD_OFFSET + 3);

            PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
        }
        break;
        }
    }
    break;
    }
    return frame_status;
}