/****************************************************************************
 *
 *
 * Copyright (c) 2014
 * Veriksytems, Inc.
 * All Rights Reserved
****************************************************************************/
#include "utils.h"
#include "serialAPI.h"
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "serial.h"

#define IDX_CMD 2
#define IDX_DATA 3

#define MAX_SERIAL_RETRY 3

#define SER_TIMEOUT 600

#define MAX_TX_QUEUE 0x0A
#define QUEUE_MAX 0x0A

#define FRAME_TRANSMITTING 0x22
#define FRAME_WAITING_FOR_ACK 0x33
#define FRAME_COMPLETED 0x44

ZW_APPLICATION_TX_BUFFER pTxBuf;
pthread_t apiThreadProcID, dispatchThreadProcID;

static struct _command_element_
{
    uint8_t buf[BUF_SIZE];
    uint8_t length;
} commandQueue[QUEUE_MAX];

static struct _transmit_element_
{
    uint8_t wLen;
    uint8_t wBuf[BUF_SIZE];
    uint8_t *transmitStatus;
} transmitQueue[MAX_TX_QUEUE];

//statusFrameReceived:      Received a valid data frame
//statusFrameSent:          Received ACK after send data frame
//statusFrameErr:           Received a data frame with error checksum
//statusRxTimeout:          Rx timeout (not receiced valid data)
//statusTxCANned:           Received CAN frame, it means tx frame has been CANned
//statusExit:               Thread got signalled to Exit
enum
{
    statusFrameReceived,
    statusFrameSent,
    statusFrameErr,
    statusRxTimeout,
    statusTxCANned,
    statusExit,
};

/* Global buffer passed by reference to each ZW_SendData() callback */
TX_STATUS_TYPE txStat;

//typedef void (*cbFuncZWTxInfo)(uint8_t txStatus, TX_STATUS_TYPE *psTxInfo);
//typedef void (*cbFuncZW)(uint8_t txStatus);


static uint8_t queueIndex = 0;

bool m_bEndThread = false;
bool TimeOut = false;
uint8_t learnState = 0;

// received buffer
static uint8_t RecvBuffer[256];

//Transmit buffer
//static uint8_t txBuf[BUF_SIZE]={SOF };

static uint8_t seqNo;
extern int  fd;

static bool waitForAck = false;
static bool waitForResponse = false;
static bool noAckReceiced = false;

static int retry = 0;
static int retryDelay = 0;
static int canRetry = 0;
static struct SerialAPI_Callbacks* callbacks;

static pthread_mutex_t  CriticalMutexCommandQueue = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t  CriticalMutexTransmitQueue = PTHREAD_MUTEX_INITIALIZER;
static uint8_t          transmitIndex = 0;

static void (*cbFuncZWReplicationSendData)(uint8_t txStatus);
static void (*cbFuncZWSendDataMeta)(uint8_t txStatus);
static void (*cbFuncZWSendData)(uint8_t txStatus, TX_STATUS_TYPE *psTxInfo);
static void (*cbFuncZWSendDataMulti)(uint8_t txStatus);
static void (*cbFuncZWRequestNodeNodeNeighborUpdate_Option)(uint8_t bStatus);
static void (*cbFuncZWRequestNodeNodeNeighborUpdate)(uint8_t bStatus);
static void (*cbFuncZWSendSUCID)(uint8_t bStatus);
static void (*cbFuncZWRequestNetworkUpdate)(uint8_t bStatus);
static void (*cbFuncZWRemoveFailedNode)(uint8_t bStatus);
static void (*cbFuncZWReplaceFailedNode)(uint8_t bStatus);
static void (*cbFuncZWSetSUCNodeID)(uint8_t bStatus);
static void (*cbFuncZWAssignReturnRoute)(uint8_t bStatus);
static void (*cbFuncZWAssignSUCReturnRoute)(uint8_t bStatus);
static void (*cbFuncZWDeleteSUCReturnRoute)(uint8_t bStatus);
static void (*cbFuncZWDeleteReturnRoute)(uint8_t bStatus);
static void (*cbFuncZWSendNodeInformation)(uint8_t txStatus);
static void (*cbFuncZWSendSlaveNodeInformation)(uint8_t txStatus);
static void (*cbFuncZWRequestNodeNodeNeighborUpdate)(uint8_t bStatus);
static void (*cbFuncZWSendTestFrame)(uint8_t txStatus);
static void (*cbFuncZWSetLearnMode)(uint8_t bStatus, uint8_t bSource, uint8_t *pCmd, uint8_t bLen);
static void (*cbFuncZWSetSlaveLearnMode)(uint8_t state, uint8_t orgID, uint8_t newID);
static void (*cbFuncZWSetDefault)();
static void (*cbFuncZWNewController)(uint8_t, uint8_t, uint8_t *, uint8_t);
static void (*cbFuncZWRequestNodeInfo)(uint8_t bStatus);

void WaitForResponse(uint8_t byFunc, uint8_t *buffer, uint8_t *byLen);
bool WaitForAck(uint8_t *bStatus);
void Dispatch(uint8_t *pData, uint16_t byLen);

bool BitMask_isBitSet(uint8_t *pabBitMask, uint8_t bBitNo)
{
    return (0 != (*(pabBitMask + ((bBitNo - 1) >> 3)) >> (((bBitNo - 1) & 7)) & 0x01));
}

uint8_t CalculateChecksum(uint8_t *pData, int nLength)
{
    uint8_t byChecksum = 0xff;
    int i;
    for (i = 0; i < nLength; nLength--)
    {
        byChecksum ^= *pData++;
    }
    return byChecksum;
}
int ReceiveData(uint8_t *buffer, uint8_t *len, bool *bStop)
{
    int i = 0;
    uint8_t ch;
    if (getchar_t(fd, &ch) == 0)
    {
        return statusRxTimeout;
    }
    while (ch != SOF)
    {
        if ((ch == ACK) || (ch == NAK))
        {
            *len = 0;
            mainlog(logDebug, "Received - %s [0x%02X]", (ch == ACK) ? "ACK" : "NAK", waitForAck);
            if (waitForAck)
            {
                return ch == ACK ? statusFrameSent : statusFrameErr;
            }
        }
        else
        {
            if (ch == CAN)
            {
                //received a CAN, we treat it as a timeout action but only if we have answered with an ACK
                mainlog(logDebug, "Received - CAN [0x%02X]", waitForAck);
                if (waitForAck)
                {
                    if (canRetry)
                    {
                        //have been CANned
                        putchar_t(fd, ACK);
                        mainlog(logDebug, "Transmit - ACK CAN [0x%02X]", canRetry);
                    }
                    return statusTxCANned;
                }
            }
            else
            {
                //mainlog(logWarning,"Received - Garbage %02X", ch);
            }
        }
        if (*bStop)
        {
            return statusExit;
        }
        if (getchar_t(fd, &ch) == 0)
        {
            mainlog(logWarning, "Received - RxTimeout SYNC hunt");
            return statusRxTimeout;
        }
    }
    if (getchar_t(fd, &buffer[i++]) == 0)
    {
        mainlog(logWarning, "Received - RxTimeout on Length byte");
        return statusRxTimeout;
    }

    uint8_t bLen = buffer[0];
    while (i < bLen + 1)
    {
        if (*bStop)
        {
            return statusExit;
        }
        if (getchar_t(fd, &buffer[i++]) == 0)
        {
            mainlog(logWarning, "Received - RxTimeout inframe byte timeout");
            return statusRxTimeout;
        }
    }
    if (CalculateChecksum(buffer, i - 1) != buffer[i - 1])
    {
        mainlog(logWarning, "Received - LRC error");
        putchar_t(fd, NAK);
        mainlog(logDebug, "Transmit - NAK");
        return statusFrameErr;
    }
    buffer[i - 1] = 0;
    if (putchar_t(fd, ACK) == 0)
    {
        mainlog(logDebug, "Transmit - ACK retry");
        putchar_t(fd, ACK);
    }
    *len = i - 1;
    mainlog(logDebug, "Received----");
    hexdump(buffer, *len);

    return statusFrameReceived;
}
void TransmitData(uint8_t *buffer, uint8_t length)
{
    static uint8_t *wBuf;
    static uint8_t wLen;
    if (buffer != NULL)
    {
        wBuf = buffer;
        wLen = length;
    }
    uint8_t byChk = CalculateChecksum(wBuf, wLen);
    putchar_t(fd, SOF);
    putbuffer_t(fd, wBuf, wLen);
    putchar_t(fd, byChk);
    waitForAck = true;
    mainlog(logDebug, "Transmit----");
    hexdump(wBuf, wLen);
}

void SendData(uint8_t *buffer, uint8_t length, uint8_t *bStatus)
{
    pthread_mutex_lock(&CriticalMutexCommandQueue);
    if (transmitIndex < MAX_TX_QUEUE)
    {
        transmitQueue[transmitIndex].transmitStatus = bStatus;
        transmitQueue[transmitIndex].wLen = length;
        memcpy(&transmitQueue[transmitIndex].wBuf[0], buffer, length);
        transmitIndex++;
    }
    pthread_mutex_unlock(&CriticalMutexCommandQueue);
}
void SendDataAndWaitForResponse(uint8_t *buffer, uint8_t length, uint8_t byFunc, uint8_t *out, uint8_t *byLen)
{
    SendData(buffer, length, NULL);
    WaitForResponse(byFunc, out, byLen);
}
bool SendDataAndWaitForAck(uint8_t *buffer, uint8_t length)
{
    uint8_t bStatus = 0;
    SendData(buffer, length, &bStatus);
    return WaitForAck(&bStatus);
}
uint8_t SeqNo(void)
{
    seqNo++;
    if (!seqNo)
    {
        seqNo++;
    }
    return seqNo;
}
/**
 * Return the minimum of two unsigned numbers.
 * Preferring function to macro to avoid double evaluation. No need to performance optimize
 * to preprocessor macro.
 *
 * Cannot find this in standard library,
 */
static unsigned int min(unsigned int x, unsigned int y)
{
    if (y < x)
    {
        return y;
    }
    return x;
}

bool WaitForAck(uint8_t *bStatus)
{
    noAckReceiced = false;
    // Wait until ack is received or the transmit thread signal us to stop

    //wait for buffer to be send
    while (*bStatus != FRAME_TRANSMITTING)
    {
        sleep(0);
    }

    //wait for ack
    while (waitForAck && !noAckReceiced)
    {
        sleep(0);
    }
    if (!noAckReceiced)
    {
        return true;
    }
    noAckReceiced = false;
    return false;
}

void WaitForResponse(uint8_t byFunc, uint8_t *buffer, uint8_t *byLen)
{
    bool bFound = false;
    int timeOutVal;
    waitForResponse = true;
    TimeOut = false;

    //Set TimeOut=3s
    timeOutVal=5000;

    while ((waitForResponse) && (!TimeOut))
    {
        while ((RecvBuffer[0] == 0) && waitForResponse)
        {
            usleep(1000);
            timeOutVal--;
            if (timeOutVal==0)
            {
                TimeOut=true;
                break;
            }
        }

        if (!TimeOut && waitForResponse && (RecvBuffer[1] == RESPONSE) && (RecvBuffer[2] == byFunc))
        {
            //Found a valid response
            bFound = true;
            waitForResponse = false;
            break;
        }
        else
        {
            RecvBuffer[0] = 0;
        }
    }
    waitForResponse = false;
    if (!bFound)
    {
        if (byLen != NULL)
        {
            *byLen = 0;
        }
    }
    else
    {
        if (buffer != NULL)
        {
            memcpy(buffer, RecvBuffer, RecvBuffer[0]);
        }
        if (byLen != NULL)
        {
            *byLen = RecvBuffer[0];
        }
    }
    RecvBuffer[0] = 0;
}

void *DispatchThreadProc(void *pParam)
{
    uint8_t buf[BUF_SIZE];
    uint8_t length;
    while (!m_bEndThread)
    {
        if (queueIndex > 0)
        {
            pthread_mutex_lock(&CriticalMutexCommandQueue);
            queueIndex--;
            memset(buf, 0, sizeof(buf));
            length = commandQueue[0].length;
            //Copy command to buf and process it.
            memcpy(buf, commandQueue[0].buf, commandQueue[0].length);

            //POP the message from the queue
            memcpy((uint8_t *)&commandQueue[0], (uint8_t *)&commandQueue[1], queueIndex * sizeof(struct _command_element_));
            pthread_mutex_unlock(&CriticalMutexCommandQueue);
            Dispatch(buf, length);
        }
        else
        {
            usleep(3000);
        }
    }
    return 0;
}
void *APIThreadProc(void *pParam)
{
    uint8_t buf[4100];
    uint8_t length;
    uint8_t status;

    for (;;)
    {
        if (m_bEndThread)
        {
            return 0;
        }

        //if there are something in the transmitQueue and we are not waitng ack, send it
        if ((transmitIndex > 0) && !waitForAck)
        {
            if (transmitQueue[0].transmitStatus != NULL)
            {
                *(transmitQueue[0].transmitStatus) = FRAME_TRANSMITTING;
            }
            TransmitData(transmitQueue[0].wBuf, transmitQueue[0].wLen);
        }
        length = 0;
        status = ReceiveData(buf, &length, &m_bEndThread);
        if (m_bEndThread)
        {
            return 0;
        }
        switch (status)
        {
        case statusFrameReceived:
            if (!m_bEndThread && (length > 0))
            {
                if (buf[1] == REQUEST)
                {
                    //ZW->HOST a request, put this request to commmandQueue
                    mainlog(logDebug, "ZW->HOST: REQUEST: received a request from ZW");
                    pthread_mutex_lock(&CriticalMutexCommandQueue);
                    if (queueIndex < QUEUE_MAX)
                    {
                        memcpy(commandQueue[queueIndex].buf, buf, length);
                        commandQueue[queueIndex].length = length;
                        queueIndex++;
                    }
                    pthread_mutex_unlock(&CriticalMutexCommandQueue);
                }
                else if (buf[1] == RESPONSE)
                {
                    //ZW->HOST a response, so copy it to RecvBuffer for handling.
                    mainlog(logDebug, "ZW->HOST: RESPONSE: received a response from ZW");
                    memcpy(&RecvBuffer[1], &buf[1], length - 1);
                    RecvBuffer[0] = buf[0];
                }
            }
            usleep(1000);
            break;
        case statusFrameSent:
            //Remove request from transmitQueue if an ACK is received and we are waiting for ACK
            //If ACK is received and we are not waiting for ACK, just ignore ACK.
            if (waitForAck)
            {
                pthread_mutex_lock(&CriticalMutexTransmitQueue);
                transmitIndex--;
                memcpy((uint8_t *)&transmitQueue[0], (uint8_t *)&transmitQueue[1], transmitIndex * sizeof(struct _transmit_element_));
                pthread_mutex_unlock(&CriticalMutexTransmitQueue);
                waitForAck = false;
                retry = 0;
                canRetry = 0;
            }
            break;
        case statusFrameErr:
        //Code here
        case statusRxTimeout:
        //Code here
        case statusTxCANned:
            if (waitForAck)
            {
                if (retry == 0)
                {
                    retryDelay = 0;
                }
                // retryDelay is used to delay the last retry a couple of seconds
                if (retry == MAX_SERIAL_RETRY - 1)
                {
                    retryDelay++;
                    if (retryDelay < 2000 / SER_TIMEOUT)
                    {
                        break;
                    }
                }
                if (((status == statusTxCANned) && (canRetry < MAX_SERIAL_RETRY)) || (retry++ < MAX_SERIAL_RETRY))
                {
                    if (status == statusTxCANned)
                    {
                        canRetry++;
                    }
                    else
                    {
                        //Not a CAN retry
                        canRetry = 0;
                    }
                    //Retry to transmit
                    TransmitData(NULL, 0);
                }
                else
                {
                    retry = canRetry = 0;
                    pthread_mutex_lock(&CriticalMutexTransmitQueue);
                    transmitIndex--;
                    memcpy((uint8_t *)&transmitQueue[0], (uint8_t *)&transmitQueue[1], transmitIndex * sizeof(struct _transmit_element_));
                    pthread_mutex_unlock(&CriticalMutexTransmitQueue);
                    noAckReceiced = true;
                    waitForAck = false;

                    waitForResponse = false;
                }
            }
            else if ((learnState) && (TimeOut))
            {
                waitForAck = false;
                waitForResponse = false;
                mainlog(logWarning, "COMLOST: COMM NO RESPONSE\n");
            }
            break;
        default:
            break;
        }
        usleep(3000);
    }
    return 0;
}

void Dispatch(uint8_t *pData, uint16_t byLen)
{
    int i;
    uint8_t *pCmd;
    mainlog(logDebug, "FUNC_ID = 0x%02X", pData[IDX_CMD]);
    switch (pData[IDX_CMD])
    {
    case FUNC_ID_ZW_APPLICATION_CONTROLLER_UPDATE:
    {
        uint8_t bStatus;

        if (callbacks->ApplicationControllerUpdate == NULL)
        {
            break;
        }

        bStatus = pData[ IDX_DATA ];

        if (bStatus == UPDATE_STATE_NODE_INFO_FOREIGN_HOMEID_RECEIVED)
        {
            /* ZW->HOST: 0x49 | 0x85 | prospectID | bLen
                           | prospectHomeID[0] | prospectHomeID[1] | prospectHomeID[2] | prospectHomeID[3]
                           | nodeInfoLen | BasicDeviceType | GenericDeviceType | SpecificDeviceType
                           | nodeInfo[nodeInfoLen]
             */
            uint8_t prospectID, prospectHomeID[4], length;
            uint8_t idx = IDX_DATA + 1;

            prospectID = pData[idx++];
            length = min(pData[idx++], BUF_SIZE);

            if (length > BUF_SIZE)
            {
                mainlog(logWarning, "ApplicationUpdate get overflow!");
            }

            pCmd = (uint8_t *)malloc(pData[IDX_DATA + 2]);
            memcpy(prospectHomeID, &pData[idx], sizeof prospectHomeID);
            idx += sizeof prospectHomeID;
            memcpy(pCmd, &pData[idx], length);

            callbacks->ApplicationControllerUpdate(bStatus,
                prospectID,
                pCmd,
                length,
                prospectHomeID);
            free(pCmd);

          }
          else if (bStatus == UPDATE_STATE_INCLUDED_NODE_INFO_RECEIVED)
          {
            /* ZW-HOST: FUNC_ID_ZW_APPLICATION_UPDATE | UPDATE_STATE_INCLUDED_NODE_INFO_RECEIVED | bSource |
                  bINIFrxStatus | abINIFsmartStartNWIHomeID[4]
             */
            uint8_t srcId = pData[IDX_DATA + 2];
            pCmd = (uint8_t *)malloc(5);
            memcpy(pCmd, &pData[IDX_DATA + 3], 5);
            callbacks->ApplicationControllerUpdate(bStatus,
                srcId,
                pCmd,
                5, /* no variable length here */
                NULL); /* IN this case, homeid is embedded in pCmd*/
            free(pCmd);
          }
          else
          {
            /* ZW->HOST: 0x49 | bStatus | srcID | bLen
                          | BasicDeviceType | GenericDeviceType | SpecificDeviceType
                          | nodeInfo
                         */
            pCmd = (uint8_t *)malloc(pData[IDX_DATA + 2]);
            for ( i = 0;i < pData[ IDX_DATA + 2 ];i++ )
            {
                pCmd[i] = pData[ IDX_DATA + 3 + i ];
            }
            callbacks->ApplicationControllerUpdate(pData[IDX_DATA], 
                pData[IDX_DATA + 1], 
                pCmd, 
                pData[IDX_DATA + 2],
                NULL);
            free(pCmd);
        }
        
    }
    break;

    case FUNC_ID_APPLICATION_COMMAND_HANDLER:
        //ZW->PC REQ | 0x04 | rxStatus |sourceNode| cmdLength |pCmd[]
        if (callbacks->ApplicationCommandHandler)
        {
            pCmd = (uint8_t *)malloc(pData[IDX_DATA + 2]);
            for (i = 0; i < pData[IDX_DATA + 2]; i++)
            {
                pCmd[i] = pData[IDX_DATA + 3 + i];
            }
            callbacks->ApplicationCommandHandler(pData[IDX_DATA], 0, pData[IDX_DATA + 1], pCmd, pData[IDX_DATA + 2]);
            free(pCmd);
        }
        break;

    case FUNC_ID_PROMISCUOUS_APPLICATION_COMMAND_HANDLER:
        if (callbacks->ApplicationCommandHandler)
        {
            pCmd = (uint8_t *)malloc(pData[IDX_DATA + 2]);
            for (i = 0; i < pData[IDX_DATA + 2]; i++)
            {
                pCmd[i] = pData[IDX_DATA + 3 + i];
            }
            callbacks->ApplicationCommandHandler(pData[IDX_DATA], pData[byLen - 1], pData[IDX_DATA + 1], pCmd, pData[IDX_DATA + 2]);
            free(pCmd);
        }

    case FUNC_ID_APPLICATION_COMMAND_HANDLER_BRIDGE:
        //code here
        break;

    //The rest are callback functions
    case FUNC_ID_ZW_SEND_NODE_INFORMATION:
        if (cbFuncZWSendNodeInformation != NULL)
        {
            cbFuncZWSendNodeInformation(pData[IDX_DATA + 1]);
        }
        break;
    case FUNC_ID_ZW_SEND_TEST_FRAME:
        if (cbFuncZWSendTestFrame != NULL)
        {
            cbFuncZWSendTestFrame(pData[IDX_DATA + 1]);
        }
        break;
    case FUNC_ID_ZW_SEND_DATA:
        if (cbFuncZWSendData != NULL)
        {
            memset(&txStat, 0, sizeof(TX_STATUS_TYPE));      
            if (byLen > 7)
            {
                /* Copy detailed tx info from serial buffer to txStat struct */
                txStat.transmitTicks        = pData[IDX_DATA + 2] * 256 + pData[IDX_DATA + 3]; /* txt timing */
                txStat.bRepeaters           = pData[IDX_DATA + 4]; /* repeater count */
                memcpy(&txStat.rssi_values, &pData[IDX_DATA + 5], MAX_REPEATERS + 1);
                txStat.ackChannel           = pData[IDX_DATA + 10];
                txStat.lastTxChannel        = pData[IDX_DATA + 11];
                txStat.routeSchemeState     = pData[IDX_DATA + 12];
                memcpy(&txStat.repeaters, &pData[IDX_DATA + 13], MAX_REPEATERS);
                txStat.routeSpeed           = pData[IDX_DATA + 17];
                txStat.routeTries           = pData[IDX_DATA + 18];
                txStat.lastFailedLinkFrom   = pData[IDX_DATA + 19];
                txStat.lastFailedLinkTo     = pData[IDX_DATA + 20];
                cbFuncZWSendData(pData[IDX_DATA + 1], &txStat); 
            }
            else if (byLen > 5)
            {
                txStat.transmitTicks        = pData[IDX_DATA + 2] * 256 + pData[IDX_DATA + 3]; /* txt timing */
                txStat.bRepeaters           = 0xFF; /* repeater count not available*/
                cbFuncZWSendData(pData[IDX_DATA + 1], &txStat); 
            }
            else
            {
                txStat.bRepeaters           = 0xFF; /* repeater count not available*/
                cbFuncZWSendData(pData[IDX_DATA + 1], &txStat);
            }
        }
        break;
    case FUNC_ID_ZW_SEND_DATA_META:
        if (cbFuncZWSendDataMeta != NULL)
        {
            cbFuncZWSendDataMeta(pData[IDX_DATA + 1]);
        }
        break;
    case FUNC_ID_ZW_SEND_DATA_MULTI:
        if (cbFuncZWSendDataMulti != NULL)
        {
            cbFuncZWSendDataMulti(pData[IDX_DATA + 1]);
        }
        break;
    case FUNC_ID_ZW_SEND_SLAVE_NODE_INFORMATION:
        if (cbFuncZWSendSlaveNodeInformation != NULL)
        {
            cbFuncZWSendSlaveNodeInformation(pData[IDX_DATA + 1]);
        }
        break;
    case FUNC_ID_ZW_SEND_DATA_MR:
        //TODO
        break;
    case FUNC_ID_ZW_SEND_DATA_META_MR:
        //TODO
        break;
    case FUNC_ID_ZW_SEND_DATA_BRIDGE:
        //TODO
        break;
    case FUNC_ID_ZW_SEND_DATA_META_BRIDGE:
        //code here
        break;
    case FUNC_ID_ZW_SEND_DATA_MULTI_BRIDGE:
        //code here
        break;
    case FUNC_ID_RTC_TIMER_CALL:
        //code here
        break;
    case FUNC_ID_RTC_TIMER_CREATE:
        //code here
        break;
    case FUNC_ID_MEMORY_PUT_BUFFER:
        //code here
        break;
    case FUNC_ID_ZW_SET_DEFAULT:
        if (cbFuncZWSetDefault != NULL)
        {
            cbFuncZWSetDefault();
        }
        break;
    case FUNC_ID_ZW_CONTROLLER_CHANGE:
    case FUNC_ID_ZW_CREATE_NEW_PRIMARY:
    case FUNC_ID_ZW_REMOVE_NODE_FROM_NETWORK:
        if (cbFuncZWNewController != NULL)
        {
            pCmd = (uint8_t *)malloc(pData[IDX_DATA + 3]);
            for (i = 0; i < pData[IDX_DATA + 3]; i++)
            {
                pCmd[i] = pData[IDX_DATA + 4 + i];
            }
            cbFuncZWNewController(pData[IDX_DATA + 1], pData[IDX_DATA + 2], pCmd, pData[IDX_DATA + 3]);
            free(pCmd);
        }
        break;
    case FUNC_ID_ZW_ADD_NODE_TO_NETWORK:
        if (cbFuncZWNewController != NULL)
        {
            pCmd = (uint8_t *)malloc(pData[IDX_DATA + 3]);
            for (i = 0; i < pData[IDX_DATA + 3]; i++)
            {
                pCmd[i] = pData[IDX_DATA + 4 + i];
            }
            cbFuncZWNewController(pData[IDX_DATA + 1], pData[IDX_DATA + 2], pCmd, pData[IDX_DATA + 3]);
            free(pCmd);
        }
        break;
    case FUNC_ID_ZW_REPLICATION_SEND_DATA:
        if (cbFuncZWReplicationSendData != NULL)
        {
            cbFuncZWReplicationSendData(pData[IDX_DATA + 1]);
        }
        break;
    case FUNC_ID_ZW_ASSIGN_RETURN_ROUTE:
        if (cbFuncZWAssignReturnRoute != NULL)
        {
            cbFuncZWAssignReturnRoute(pData[IDX_DATA + 1]);
        }
        break;
    case FUNC_ID_ZW_DELETE_RETURN_ROUTE:
        if (cbFuncZWDeleteReturnRoute != NULL)
        {
            cbFuncZWDeleteReturnRoute(pData[IDX_DATA + 1]);
        }
        break;
    case FUNC_ID_ZW_ASSIGN_SUC_RETURN_ROUTE:
        if (cbFuncZWAssignSUCReturnRoute != NULL)
        {
            cbFuncZWAssignSUCReturnRoute(pData[IDX_DATA + 1]);
        }
        break;
    case FUNC_ID_ZW_DELETE_SUC_RETURN_ROUTE:
        if (cbFuncZWDeleteSUCReturnRoute != NULL)
        {
            cbFuncZWDeleteSUCReturnRoute(pData[IDX_DATA + 1]);
        }
        break;
    case FUNC_ID_ZW_REQUEST_NETWORK_UPDATE:
        if (cbFuncZWRequestNetworkUpdate != NULL)
        {
            cbFuncZWRequestNetworkUpdate(pData[IDX_DATA + 1]);
        }
        break;
    case FUNC_ID_ZW_SET_LEARN_MODE:
        if (cbFuncZWSetLearnMode != NULL)
        {
            uint8_t len = (byLen >= IDX_DATA + 3 + 1) ? pData[IDX_DATA + 3] : 0;
            if (len)
            {
                pCmd = (uint8_t *)malloc(len);
                for (i = 0; i < pData[IDX_DATA + 3]; i++)
                {
                    pCmd[i] = pData[IDX_DATA + 4 + i];
                }
            }
            cbFuncZWSetLearnMode(pData[IDX_DATA + 1], pData[IDX_DATA + 2], pCmd, len);

            if (len)
            {
                free(pCmd);
            }
        }
        break;

    case FUNC_ID_ZW_SET_SUC_NODE_ID:
        if (cbFuncZWSetSUCNodeID != NULL)
        {
            cbFuncZWSetSUCNodeID(pData[IDX_DATA + 1]);
        }
        break;

    case FUNC_ID_ZW_SEND_SUC_ID:
        if (cbFuncZWSendSUCID != NULL)
        {
            cbFuncZWSendSUCID(pData[IDX_DATA + 1]);
        }
        break;

    case FUNC_ID_ZW_REPLACE_FAILED_NODE:
        if (cbFuncZWReplaceFailedNode != NULL)
        {
            cbFuncZWReplaceFailedNode(pData[IDX_DATA + 1]);
        }
        break;

    case FUNC_ID_ZW_REMOVE_FAILED_NODE_ID:
        if (cbFuncZWRemoveFailedNode != NULL)
        {
            cbFuncZWRemoveFailedNode(pData[IDX_DATA + 1]);
        }
        break;

    case FUNC_ID_ZW_REQUEST_NODE_NEIGHBOR_UPDATE:
        if (cbFuncZWRequestNodeNodeNeighborUpdate != NULL)
        {
            cbFuncZWRequestNodeNodeNeighborUpdate(pData[IDX_DATA + 1]);
        }
        break;

    case FUNC_ID_ZW_REQUEST_NODE_NEIGHBOR_UPDATE_OPTION:
       if (cbFuncZWRequestNodeNodeNeighborUpdate_Option != NULL)
        {
            cbFuncZWRequestNodeNodeNeighborUpdate_Option(pData[IDX_DATA + 1]);
        }
        break;

    case FUNC_ID_SERIALAPI_STARTED:
        /* ZW->HOST: bWakeupReason | bWatchdogStarted | deviceOptionMask | */
        /*           nodeType_generic | nodeType_specific | cmdClassLength | cmdClass[] */
        if ( callbacks->SerialAPIStarted != NULL )
        {
            pCmd = (uint8_t *)malloc(pData[IDX_DATA + 3]);
            for ( i = 0;i < pData[ IDX_DATA + 2 ]; i++ ) pCmd[i] = pData[IDX_DATA + 3 + i];
            callbacks->SerialAPIStarted(pCmd, pData [IDX_DATA + 2]);
            free(pCmd);
        }
        break;

    default:
        mainlog(logDebug, "FUNC_ID 0x%02X not supported", pData[IDX_CMD]);
        break;
    }
}

int serialAPIDestruct(void)
{
    int ret;
    m_bEndThread = true;
    ret = pthread_cancel(apiThreadProcID);
    ret = pthread_cancel(dispatchThreadProcID);
    usleep(50000);
    return ret;
}

bool serialApiInitialize(const struct SerialAPI_Callbacks* _callbacks)
{
    uint8_t listening;
    APPL_NODE_TYPE nodeType;
    uint8_t *nodeParm;
    uint8_t parmLength;

    cbFuncZWAssignReturnRoute = NULL;
    cbFuncZWAssignSUCReturnRoute = NULL;
    cbFuncZWDeleteSUCReturnRoute = NULL;
    cbFuncZWDeleteReturnRoute = NULL;
    cbFuncZWRequestNetworkUpdate = NULL;
    cbFuncZWSetSUCNodeID = NULL;
    cbFuncZWSendSUCID = NULL;
    cbFuncZWRemoveFailedNode = NULL;
    cbFuncZWReplaceFailedNode = NULL;
    cbFuncZWRequestNodeNodeNeighborUpdate_Option = NULL;
    cbFuncZWRequestNodeNodeNeighborUpdate = NULL;
    cbFuncZWSendDataMulti = NULL;
    cbFuncZWSendDataMeta = NULL;
    cbFuncZWReplicationSendData = NULL;
    cbFuncZWSendData = NULL;
    cbFuncZWSendTestFrame = NULL;
    cbFuncZWSendNodeInformation = NULL;
    cbFuncZWSendSlaveNodeInformation = NULL;
    cbFuncZWSetLearnMode = NULL;
    cbFuncZWSetSlaveLearnMode = NULL;
    cbFuncZWSetDefault = NULL;
    cbFuncZWNewController = NULL;
    cbFuncZWRequestNodeInfo = NULL;

    waitForAck = false;
    learnState = false;
    TimeOut = false;
    retry = 0;
    m_bEndThread = false;

    callbacks = (struct SerialAPI_Callbacks*) _callbacks;

    pthread_create(&apiThreadProcID, NULL, &APIThreadProc, NULL);
    pthread_create(&dispatchThreadProcID, NULL, &DispatchThreadProc, NULL);

    if(callbacks->ApplicationInitHW) callbacks->ApplicationInitHW(0);
    if(callbacks->ApplicationInitSW) callbacks->ApplicationInitSW();

    if(callbacks->ApplicationNodeInformation) 
    {
        callbacks->ApplicationNodeInformation(&listening, &nodeType, &nodeParm, &parmLength);
        serialApiApplicationNodeInformation(listening, nodeType, nodeParm, parmLength);
    }

    return true;
}

/*===============================   ZW_Version   ============================
**    Get the Z-Wave library basis version
**
**    Returns:      Nothing
**
**    Parameters
**    pBuf          OUT Pointer to buffer where version string will be
**                      copied to. Buffer must be at least 14 bytes long.
**--------------------------------------------------------------------------*/
uint8_t serialApiVersion(uint8_t *pBuf)
{
    uint8_t idx = 0;
    uint8_t byLen = 0;
    uint8_t retVal = 0;
    static uint8_t buffer[BUF_SIZE];
    memset(buffer, 0, sizeof(buffer));
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_GET_VERSION;
    buffer[0] = idx;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_GET_VERSION, buffer, &byLen);
    if (memcmp(&buffer[IDX_DATA], "Z-Wave", 6) == 0)
    {
        retVal = buffer[IDX_DATA + 12];
        memcpy(pBuf, &buffer[IDX_DATA + 6], 6);
        pBuf[6] = 0;
    }
    return retVal;
}

/**
  * \ingroup ZWCMD
  * Get the Z Wave library type.
  * \return
  *   = ZW_LIB_CONTROLLER_STATIC    Static controller library
  *   = ZW_LIB_CONTROLLER_BRIDGE    Bridge controller library
  *   = ZW_LIB_CONTROLLER           Portable controller library
  *   = ZW_LIB_SLAVE_ENHANCED       Enhanced slave library
  *   = ZW_LIB_SLAVE_ROUTING        Routing slave library
  *   = ZW_LIB_SLAVE                Slave library
  *   = ZW_LIB_INSTALLER            Installer library
  */
uint8_t serialApiTypeLibrary( void )
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_TYPE_LIBRARY;
    buffer[0] = idx;

    uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_TYPE_LIBRARY, buffer, &byLen);

    return buffer[ IDX_DATA ];
}



void serialApiSoftReset(uint8_t resetMode)
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_SERIAL_API_SOFT_RESET;
    buffer[idx++] = resetMode;
    buffer[0] = idx;
    SendData(buffer, idx, NULL);
}

/*========================   SerialAPI_GetInitData   ========================
**    HOST->ZW: REQ | 0x02
**    ZW->HOST: RES | 0x02 | ver | capabilities | 29 | nodes[29] | chip_type | chip_version
**    Get Serial API initialization data from remote side (Enhanced Z-Wave module)
**
**    Returns:        Nothing
**
**    Parameters
**    ver             OUT  Remote sides Serial API version
**    capabilities    OUT  Capabilities flag (GET_INIT_DATA_FLAG_xxx)
**    len             OUT  Number of bytes in nodesList
**    nodesList       OUT  Bitmask list with nodes known by Z-Wave module
**
**    Capabilities flag:
**    Bit 0: 0 = Controller API; 1 = Slave API
**    Bit 1: 0 = Timer functions not supported; 1 = Timer functions supported.
**    Bit 2: 0 = Primary Controller; 1 = Secondary Controller
**    Bit 3: 0 = Not SUC; 1 = This node is SUC (static controller only)
**    Bit 3-7: reserved
**    Timer functions are: TimerStart, TimerRestart and TimerCancel.
**--------------------------------------------------------------------------*/
bool serialApiGetInitData(uint8_t *ver, uint8_t *capabilities, uint8_t *len, uint8_t *nodesList, 
                            uint8_t* chip_type, uint8_t* chip_version)
{
    int i;
    uint8_t idx = 0;
    uint8_t* p;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_SERIAL_API_GET_INIT_DATA;
    buffer[0] = idx;

    uint8_t byLen = 0;
    *ver = 0;
    *capabilities = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_SERIAL_API_GET_INIT_DATA, buffer, &byLen);
    p = &buffer[ IDX_DATA ];
    *ver = *p++;

    //react as controller
    *capabilities = *p++;
    *len = *p++;

    for (i = 0; i < 29; i++)
    {
        nodesList[i] =  *p++;
    }

    *chip_type=*p++;
    *chip_version=*p++;

    //Bit 2 (*capabilities) tells if it is Primary Controller (FALSE) or Secondary Controller (TRUE).
    if ((*capabilities) & GET_INIT_DATA_FLAG_SECONDARY_CTRL)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/*========================   ZW_AddNodeToNetwork   ==========================
**    Start a Add Node To Network sequence
**
**    Returns:      Nothing
**
**    Parameters
**--------------------------------------------------------------------------*/

void serialApiAddNodeToNetwork(uint8_t mode,
                                void (*completedFunc)(uint8_t, uint8_t, uint8_t *, uint8_t))
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_ADD_NODE_TO_NETWORK;
    buffer[idx++] = mode;
    buffer[idx++] = byCompletedFunc;
    buffer[0] = idx;
    cbFuncZWNewController = completedFunc;
    SendData(buffer, idx, NULL);
}
/*======================   ZW_RemoveNodeFromNetwork   ========================
**    Start a Remove Node From Network sequence
**    HOST->ZW: REQ | 0x4B | mode | funcID
**    ZW->HOST: REQ | 0x4B | funcID | bStatus | bSource (nodeID) | bLen | basic | generic | specific | cmdclassses[] 

**    Returns:      Nothing
**
**    Parameters
**--------------------------------------------------------------------------*/
void serialApiRemoveNodeFromNetwork(uint8_t mode,
                                    void (*completedFunc)(uint8_t, uint8_t, uint8_t *, uint8_t))
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_REMOVE_NODE_FROM_NETWORK;
    buffer[idx++] = mode;
    buffer[idx++] = byCompletedFunc;
    buffer[0] = idx;
    cbFuncZWNewController = completedFunc;
    SendData(buffer, idx, NULL);
}

/*========================   ZW_RemoveFailedNode   ==========================
**    Start a Remove Failed Node sequence
**    HOST->ZW: REQ | 0x61 | nodeID | funcID
**    ZW->HOST: RES | 0x61 | retVal
**    ZW->HOST: REQ | 0x61 | funcID | txStatus
**
**    Returns:      Nothing
**
**    Parameters
**--------------------------------------------------------------------------*/
uint8_t serialApiRemoveFailedNode(uint8_t nodeID,
                                  void (*completedFunc)(uint8_t))
{
    uint8_t retVal;
    uint8_t idx = 0;
    uint8_t byLen = 0;
    static uint8_t buffer[BUF_SIZE];
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_REMOVE_FAILED_NODE_ID;
    buffer[idx++] = nodeID;
    buffer[idx++] = byCompletedFunc;
    buffer[0] = idx; // length

    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_REMOVE_FAILED_NODE_ID, buffer, &byLen);

    if (byLen > IDX_DATA)
    {
        retVal = buffer[IDX_DATA];
        cbFuncZWRemoveFailedNode = completedFunc;
    }
    else
    {
        /* No answer */
        retVal = 0xFF;
        cbFuncZWRemoveFailedNode = NULL;
    }
    return retVal;
}

/*===============================   ZW_SendData   ===========================
**    Transmit data buffer to a single ZW-node or all ZW-nodes (broadcast).
**
**
**    Returns:      FALSE if transmitter queue overflow
**
**    Parameters:
**    nodeID          IN  Destination node ID (0xFF == broadcast)
**    pData           IN  Data buffer pointer
**    dataLength      IN  Data buffer length
**    txOptions       IN  Transmit option flags
**    completedFunc   IN  Transmit completed call back function
**--------------------------------------------------------------------------*/
uint8_t serialApiSendData(uint8_t nodeID, uint8_t *pData, uint8_t dataLength, uint8_t txOptions,
                          void (*completedFunc)(uint8_t, TX_STATUS_TYPE *))
{
    int i;
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_SEND_DATA;
    buffer[idx++] = nodeID;
    buffer[idx++] = dataLength;
    for (i = 0; i < dataLength; i++)
    {
        buffer[idx++] = pData[i];
    }
    buffer[idx++] = txOptions;
    buffer[idx++] = byCompletedFunc;
    buffer[0] = idx;
    uint8_t byLen = 0;
    cbFuncZWSendData = completedFunc;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_SEND_DATA, buffer, &byLen);
    if (byLen > IDX_DATA)
    {
        return buffer[IDX_DATA];
    }
    else
    {
        //no answer
        return 0;
    }
}

/*===============================   ZW_SendDataMulti   ======================
**    Transmit data buffer to a list of Z-Wave Nodes (multicast frame).
**
**    Returns:          RET  FALSE if transmitter queue overflow
**
**    Parameters:
**    pNodeIDList   IN  List of destination node ID's
**    numberNodes   IN  Number of Nodes
**    pData         IN  Data buffer pointer
**    dataLength    IN  Data buffer length
**    txOptions     IN  Transmit option flags
**    completedFunc IN  Transmit completed call back function
**--------------------------------------------------------------------------*/

uint8_t serialApiSendDataMulti(uint8_t *pNodeIDList, uint8_t numberNodes,
                               uint8_t *pData, uint8_t dataLength, uint8_t txOptions,
                               void (*completedFunc)(uint8_t txStatus))
{
    int i;
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    static uint8_t buffer[BUF_SIZE];

    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_SEND_DATA_MULTI;
    numberNodes = (numberNodes <= ZW_MAX_NODES_IN_MULTICAST) ? numberNodes : ZW_MAX_NODES_IN_MULTICAST;
    buffer[idx++] = numberNodes;
    for (i = 0; i < numberNodes; i++)
    {
        buffer[idx++] = pNodeIDList[i];
    }
    buffer[idx++] = dataLength;
    for (i = 0; i < dataLength; i++)
    {
        buffer[idx++] = pData[i];
    }
    buffer[idx++] = txOptions;
    buffer[idx++] = byCompletedFunc;
    buffer[0] = idx;
    uint8_t byLen = 0;
    cbFuncZWSendDataMulti = completedFunc;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_SEND_DATA_MULTI, buffer, &byLen);
    return buffer[IDX_DATA];
}

/*=============================   ZW_SendDataMeta   =========================
**    Transmit data buffer to a single ZW-node or all ZW-nodes (broadcast).
**
**
**    Returns:      FALSE if transmitter queue overflow
**
**    Parameters:
**    nodeID          IN  Destination node ID (0xFF == broadcast)
**    pData           IN  Data buffer pointer
**    dataLength      IN  Data buffer length
**    txOptions       IN  Transmit option flags
**    completedFunc   IN  Transmit completed call back function
**--------------------------------------------------------------------------*/
uint8_t serialApiSendDataMeta(uint8_t nodeID, uint8_t *pData, uint8_t dataLength, uint8_t txOptions,
                              void (*completedFunc)(uint8_t txStatus))
{
    int i;
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_SEND_DATA_META;
    buffer[idx++] = nodeID;
    buffer[idx++] = dataLength;
    for (i = 0; i < dataLength; i++)
    {
        buffer[idx++] = pData[i];
    }
    buffer[idx++] = txOptions;
    buffer[idx++] = byCompletedFunc; // Func id for CompletedFunc
    buffer[0] = idx;                 // length
    uint8_t byLen = 0;
    cbFuncZWSendDataMeta = completedFunc;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_SEND_DATA_META, buffer, &byLen);

    return buffer[IDX_DATA];
}

/*============================   MemoryGetID   ===============================
**    Copy the Home-ID and Node-ID to the specified RAM addresses
**    HOST->ZW: REQ | 0x20
**    ZW->HOST: RES | 0x20 | HomeID (4bytes) | NodeID
**
**    Returns:      Nothing
**
**    Parameters
**    homeID        OUT  Home-ID pointer
**    nodeID        OUT  Node-ID pointer
**--------------------------------------------------------------------------*/

void serialApiMemoryGetID(uint8_t *pHomeID, uint8_t *pNodeID)
{
    ASSERT(pHomeID != NULL);
    ASSERT(pNodeID != NULL);
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];

    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_MEMORY_GET_ID;
    buffer[0] = idx;
    uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_MEMORY_GET_ID, buffer, &byLen);

    //Get response data
    pHomeID[0] = buffer[IDX_DATA];
    pHomeID[1] = buffer[IDX_DATA + 1];
    pHomeID[2] = buffer[IDX_DATA + 2];
    pHomeID[3] = buffer[IDX_DATA + 3];

    pNodeID[0] = buffer[IDX_DATA + 4];
}

/*========================   ZW_GetControllerCapabilities   ========================
**    Get Controller Capabilities
**    HOST->ZW: REQ | 0x05
**    ZW->HOST: RES | 0x05 | RetVal
**
**    Returns: TRUE if Controller Capabilities has been received and the specified
**             variable pointed to by pBControllerCapabilities has been updated accordingly
**             FALSE if Controller Capabilities could not been determined.
**
**   Capabilities flags:
**   CONTROLLER_CAPABILITIES_IS_SECONDARY                 : Controller is a secondary controller
**   CONTROLLER_CAPABILITIES_ON_OTHER_NETWORK             : Controller is not using its built-in home ID
**   CONTROLLER_CAPABILITIES_NODEID_SERVER_PRESENT        : Controller is a SIS, can include/exclude nodes in the network (inclusion controller) 
**   CONTROLLER_CAPABILITIES_IS_REAL_PRIMARY              : Controller was the original primary controller before SIS was added to the network          
**   CONTROLLER_CAPABILITIES_IS_SUC                       : Controller is a SUC
**   CONTROLLER_CAPABILITIES_NO_NODES_INCUDED
**--------------------------------------------------------------------------*/
bool serialApiGetControllerCapabilities(uint8_t *pbControllerCapabilities)
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];

    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_GET_CONTROLLER_CAPABILITIES;
    buffer[0] = idx;
    uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_GET_CONTROLLER_CAPABILITIES, buffer, &byLen);
    *pbControllerCapabilities = buffer[IDX_DATA];
    return (IDX_DATA < byLen);
}

/*=====================   ZW_GetNodeProtocolInfo   ==========================
**    Copy the Node's current protocol information from the non-volatile memory.
**    HOST->ZW: REQ | 0x41 | bNodeID
**    ZW->HOST: RES | 0x41 | nodeInfo
**
**    Returns:      Nothing
**
**    Parameters
**    nodeID        IN Node ID
**    nodeInfo      OUT Node info buffer
**--------------------------------------------------------------------------*/
void serialApiGetNodeProtocolInfo(uint8_t bNodeID, NODEINFO *nodeInfo)
{
    ASSERT(nodeInfo != NULL);
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    uint8_t byLen = 0;

    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_GET_NODE_PROTOCOL_INFO;
    buffer[idx++] = bNodeID;
    buffer[0] = idx;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_GET_NODE_PROTOCOL_INFO, buffer, &byLen);

    //Get response data
    nodeInfo->capability = buffer[IDX_DATA];
    nodeInfo->security = buffer[IDX_DATA + 1];
    nodeInfo->reserved = buffer[IDX_DATA] + 2;
    nodeInfo->nodeType.basic = buffer[IDX_DATA + 3];
    nodeInfo->nodeType.generic = buffer[IDX_DATA + 4];
    nodeInfo->nodeType.specific = buffer[IDX_DATA + 5];
}

/*===========================   ZW_SetDefault   ================================
**    Remove all Nodes and timers from the EEPROM memory.
      HOST->ZW: REQ | 0x42 | funcID
      ZW->HOST: REQ | 0x42 | funcID
**
**    Returns:      Nothing
**
**    Parameters
**    completedFunc IN  Command completed call back function
**--------------------------------------------------------------------------*/
void serialApiSetDefault(void (*completedFunc)(void))
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());

    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_SET_DEFAULT;
    buffer[idx++] = byCompletedFunc;
    buffer[0] = idx;
    cbFuncZWSetDefault = completedFunc;
    SendData(buffer, idx, NULL);
}
/*===========================   ZW_SetLearnMode   ===========================
**    Enable/Disable home/node ID learn mode.
**    When learn mode is enabled, received "Assign ID's Command" are handled:
**    If the current stored ID's are zero, the received ID's will be stored.
**    If the received ID's are zero the stored ID's will be set to zero.
**
**    The learnFunc is called when the received assign command has been handled.
**
**    Returns:      Nothing
**
**    Parameters
**    mode          IN  TRUE: Enable; FALSE: Disable
**    learnFunc     IN  Node learn call back function
**--------------------------------------------------------------------------*/
void serialApiSetLearnMode(uint8_t mode,
                           void (*completedFunc)(uint8_t bStatus, uint8_t bSource, uint8_t *pCmd, uint8_t bLen))
{
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_SET_LEARN_MODE;
    buffer[idx++] = mode;
    buffer[idx++] = byCompletedFunc; // Func id for CompletedFunc

    buffer[0] = idx; // length
    cbFuncZWSetLearnMode = completedFunc;
    SendData(buffer, idx, NULL);
}

uint8_t serialApiSetSlaveLearnMode(uint8_t node, uint8_t mode,
                                   void (*completedFunc)(uint8_t state, uint8_t orgID, uint8_t newID))
{
    uint8_t retVal = false;
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_SET_SLAVE_LEARN_MODE;
    buffer[idx++] = node;
    buffer[idx++] = mode;
    buffer[idx++] = byCompletedFunc; // Func id for CompletedFunc
    buffer[0] = idx;                 // length
    cbFuncZWSetSlaveLearnMode = completedFunc;

    static uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_SET_SLAVE_LEARN_MODE, buffer, &byLen);
    if (byLen > 0)
    {
        retVal = buffer[IDX_DATA];
    }
    return retVal;
}

uint8_t serialApiSendSlaveNodeInformation(uint8_t srcNode, uint8_t destNode, uint8_t txOptions,
                                          void (*completedFunc)(uint8_t txStatus))
{
    uint8_t retVal = false;
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_SEND_SLAVE_NODE_INFORMATION;
    buffer[idx++] = srcNode;
    buffer[idx++] = destNode;
    buffer[idx++] = txOptions;
    buffer[idx++] = byCompletedFunc; // Func id for CompletedFunc
    buffer[0] = idx;                 // length
    cbFuncZWSendSlaveNodeInformation = completedFunc;

    static uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_SEND_SLAVE_NODE_INFORMATION, buffer, &byLen);
    if (byLen > 0)
    {
        retVal = buffer[IDX_DATA];
    }
    return retVal;
}


/*========================   ZW_GetLastWorkingRoute   ========================
**    Get Last Working Route for bNodeID
**
**    Returns:      TRUE if a Last Working Route was found.
**
**    Parameters
**    bNodeID       IN   NodeID for which the Last Working Route is wanted
**    pRoute        OUT  Pointer to a BYTE[5] structure where the found route
**                       is to be placed. The first 4 byte (index 0-3) are the
**                       the repeaters used, first one ZERO means no more 
**                       repeaters. The 5 byte (index 4) is the routespeed,
**                       will be ZERO if not supported by SerialAPI module
**                       (DevKit 4.5x).
**--------------------------------------------------------------------------*/
uint8_t serialApiGetLastWorkingRoute(uint8_t bNodeID, uint8_t *pRoute)
{

    uint8_t retVal = false;
    uint8_t idx = 1;

    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_GET_LAST_WORKING_ROUTE;
    buffer[idx++] = bNodeID;
    buffer[0] = idx; // length
    static uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_GET_LAST_WORKING_ROUTE, buffer, &byLen);

    if (byLen > 0)
    {
        /* Did we find any Last Working Route */
        retVal = buffer[IDX_DATA + 1];
        /* If we found a route the route is here */
        pRoute[0] = buffer[IDX_DATA + 2];
        pRoute[1] = buffer[IDX_DATA + 3];
        pRoute[2] = buffer[IDX_DATA + 4];
        pRoute[3] = buffer[IDX_DATA + 5];
        /* Have the SerialAPI module delivered any routspeed info s*/
        if (byLen > 6)
        {
            /* We got the routespeed */
            pRoute[4] = buffer[IDX_DATA + 6];
        }
        else
        {
            /* No routespeed received */
            pRoute[4] = 0;
        }
    }
    return retVal;
}

/*========================   ZW_SetLastWorkingRoute   ========================
**    Set Last Working Route for bNodeID
**
**    Returns:      TRUE if a Last Working Route was set.
**
**    Parameters
**    bNodeID       IN   NodeID for which the Last Working Route should be set.
**    pRoute        IN   Pointer to a BYTE[5] structure where the wanted route
**                       is to placed. The first 4 byte (index 0-3) are the
**                       the repeaters used, first one ZERO means no more 
**                       repeaters. The 5 byte (index 4) is the routespeed.
**--------------------------------------------------------------------------*/
uint8_t serialApiSetLastWorkingRoute(uint8_t bNodeID, uint8_t *pRoute)
{

    uint8_t retVal = false;
    uint8_t idx = 1;

    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_SET_LAST_WORKING_ROUTE;
    buffer[idx++] = bNodeID;
    buffer[idx++] = pRoute[0];
    buffer[idx++] = pRoute[1];
    buffer[idx++] = pRoute[2];
    buffer[idx++] = pRoute[3];
    buffer[idx++] = pRoute[4];
    buffer[0] = idx; // length
    static uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_SET_LAST_WORKING_ROUTE, buffer, &byLen);

    if (byLen > 0)
    {
        /* Did we find any Last Working Route */
        retVal = buffer[IDX_DATA + 1];
    }
    return retVal;
}
/*============================   ZW_GetRoutingInfo   =============================
**    Read number of bytes from the EEPROM to a RAM buffer
**
**    Returns:      Nothing
**
**    Parameters
**    bNodeID          IN    The NodeID to get routing information from
**    buf             IN    Buffer pointer to where the data is placed
**    bRemoveBad      IN    If TRUE remove bad repeaters
**    bRemoveNonReps  IN    If TRUE remove non repeating devices
**--------------------------------------------------------------------------*/
void serialApiGetRoutingInfo(uint8_t bNodeID, uint8_t *buf, uint8_t bRemoveBad, uint8_t bRemoveNonReps)
{
    int i;
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    /*  bNodeID | bRemoveBad | bRemoveNonReps | funcID */
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_GET_ROUTING_TABLE_LINE;
    buffer[idx++] = bNodeID;
    buffer[idx++] = bRemoveBad;
    buffer[idx++] = bRemoveNonReps;
    buffer[0] = idx;
    uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_GET_ROUTING_TABLE_LINE, buffer, &byLen);
    for (i = 0; i < ZW_MAX_NODEMASK_LENGTH; i++)
    {
        buf[i] = buffer[IDX_DATA + i];
    }
}

/*=========================   ZW_RequestNetworkUpdate   =====================
**    Request SUC/SIS for network an update.
**
**    Returns:      Nothing
**
**    Parameters
**    completedFunc     IN  Completion function
**          bStatus        IN  Status of find neighbor process
**    \serialapi{
**    HOST->ZW: REQ | 0x53 | funcID
**    ZW->HOST: RES | 0x53 | retVal
**    ZW->HOST: REQ | 0x53 | funcID | txStatus
**    }
**--------------------------------------------------------------------------*/
uint8_t serialApiRequestNetworkUpdate(void (*completedFunc)(uint8_t))
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_REQUEST_NETWORK_UPDATE;
    buffer[idx++] = byCompletedFunc;
    buffer[0] = idx; // length
    cbFuncZWRequestNetworkUpdate = completedFunc;
    uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_REQUEST_NETWORK_UPDATE, buffer, &byLen);
    return buffer[IDX_DATA];
}

/*==========================   ZW_RequestNodeNeighborUpdate   =======================
**    Request controller to update neighbor information for a specific node.
**
**    Returns:      Nothing
**
**    Parameters
**    nodeID,           IN nodeID on node to ask for neighbors
**    completedFunc     IN  Completion function
**          bStatus        IN  Status of find neighbor process
**--------------------------------------------------------------------------*/
void serialApiRequestNodeNeighborUpdate(uint8_t nodeID, void (*completedFunc)(uint8_t))
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_REQUEST_NODE_NEIGHBOR_UPDATE;
    buffer[idx++] = nodeID;
    buffer[idx++] = byCompletedFunc;
    buffer[0] = idx; // length
    cbFuncZWRequestNodeNodeNeighborUpdate = completedFunc;
    if (!SendDataAndWaitForAck(buffer, idx))
    {
        if (cbFuncZWRequestNodeNodeNeighborUpdate != NULL)
        {
            cbFuncZWRequestNodeNodeNeighborUpdate(0x23);
        }
    }
}

/*=================   ZW_RequestNodeNeighborUpdate_Option   =================
**    Request controller to update neighbor information for a specific node.
**
**    Returns:      Nothing
**
**    Parameters
**    nodeID,           IN nodeID on node to ask for neighbors
**    bTxOption         IN Transmit Options to use when requesting node neighbor update
**                         if ZERO then default transmit options are used
**    completedFunc     IN  Completion function
**          bStatus        IN  Status of find neighbor process
**--------------------------------------------------------------------------*/
void serialApiRequestNodeNeighborUpdate_Option(uint8_t nodeID,
                                               uint8_t bTxOption,
                                               void (*completedFunc)(uint8_t))
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_REQUEST_NODE_NEIGHBOR_UPDATE_OPTION;
    buffer[idx++] = nodeID;
    buffer[idx++] = bTxOption;
    buffer[idx++] = byCompletedFunc;
    buffer[0] = idx; // length
    cbFuncZWRequestNodeNodeNeighborUpdate_Option = completedFunc;
    SendData(buffer, idx, NULL);
}

/*============================   GetSUCNodeID   =============================
**    Get the currently registered SUC node ID
**
**    Returns:      SUC node ID, ZERO if no SUC available
**
**--------------------------------------------------------------------------*/
uint8_t serialApiGetSUCNodeID(void)
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_GET_SUC_NODE_ID;
    buffer[0] = idx; // length
    uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_GET_SUC_NODE_ID, buffer, &byLen);
    return buffer[IDX_DATA];
}

/*===========================   ZW_SetSUCNodeID ===============================
**    This function is used to select the static controller that should act
**    as SUC in the network. Callback indicates if the Static controller
**    accepts.
**    Returns:      Nothing
**
**    Parameters
**    nodeID        IN  Node ID to try and use as SUC
**    state         IN  state telling if it is a enable SUC (TRUE) or disable (FALSE)
**    txOption      IN  transmit options
**    capabilities  IN  SUC capabilities
**    complfunc     IN  call back function with status ZW_SUC_SET_SUCCEEDED if
**                      succesfull and ZW_SUC_SET_FAILED if the assignment failed
**--------------------------------------------------------------------------*/
uint8_t serialApiSetSUCNodeID(uint8_t bNodeID, uint8_t state, uint8_t capabilities, uint8_t txOption,
                              void (*completedFunc)(uint8_t))
{
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_SET_SUC_NODE_ID;
    buffer[idx++] = bNodeID;
    buffer[idx++] = state; /* Do we want to enable or disable?? */
    buffer[idx++] = txOption;
    buffer[idx++] = capabilities;
    buffer[idx++] = byCompletedFunc; // Func id for CompletedFunc

    buffer[0] = idx; // length
    cbFuncZWSetSUCNodeID = completedFunc;
    uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_SET_SUC_NODE_ID, buffer, &byLen);
    return buffer[IDX_DATA];
}

/*============================   ZW_SendSUCID ================================
**--------------------------------------------------------------------------*/
uint8_t serialApiSendSUCID(uint8_t bNodeID, uint8_t txOption,
                           void (*completedFunc)(uint8_t))
{
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : 0x43);
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_SEND_SUC_ID;
    buffer[idx++] = bNodeID;
    buffer[idx++] = txOption;
    buffer[idx++] = byCompletedFunc; // Func id for CompletedFunc

    buffer[0] = idx; // length
    cbFuncZWSendSUCID = completedFunc;
    uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_SEND_SUC_ID, buffer, &byLen);
    return buffer[IDX_DATA];
}

/*================   SerialAPI_ApplicationNodeInformation   =================
**  Set ApplicationNodeInformation data to be used in subsequent calls to
**  ZW_SendNodeInformation.
**
**  Returns:        Nothing
**
**  Parameters
**  listening       IN  TRUE if this node is always on air
**  nodeType        IN  Device type
**  nodeParm        IN  Device parameter buffer
**  parmLength      IN  Number of Device parameter bytes
**--------------------------------------------------------------------------*/
void serialApiApplicationNodeInformation(
    uint8_t listening,
    APPL_NODE_TYPE nodeType,
    uint8_t *nodeParm,
    uint8_t parmLength)
{
    int i;
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_SERIAL_API_APPL_NODE_INFORMATION;
    buffer[idx++] = listening;
    buffer[idx++] = nodeType.generic;
    buffer[idx++] = nodeType.specific;
    buffer[idx++] = parmLength;
    for (i = 0; i < parmLength; i++)
    {
        buffer[idx++] = nodeParm[i];
    }
    buffer[0] = idx; // length
    SendData(buffer, idx, NULL);
}

/*=====================   SerialAPI_Get_Capabilities   =======================
**    Get SerialAPI Capabilities - Request module for SerialAPI Capabilities
**
**    Returns:      The SerialAPI Capabilities Structure specified filled with
**                  the module capabilities
**
**    Parameters
**    offset        IN   Pointer to SerialAPI Capability Structure where
**                       the module capabilities should be delivered
**--------------------------------------------------------------------------*/
void
serialApiGetCapabilities(
    uint8_t *capabilities)
{
    uint8_t idx=0;
    static uint8_t buffer[BUF_SIZE];
    uint8_t *pBuffer = capabilities;
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_SERIAL_API_GET_CAPABILITIES;
    buffer[0] = idx;    // length
    uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_SERIAL_API_GET_CAPABILITIES, buffer, &byLen);

    for (idx = 0; idx < byLen; idx++)
    {
        *pBuffer++ = buffer[IDX_DATA + idx];
    }
}



/*==========================   ZW_WatchDogEnable   ===========================
**    Enable WatchDog on Z-Wave module
**
**    Returns:      Nothing
**
**    Parameters
**--------------------------------------------------------------------------*/
void serialApiWatchDogEnable(void)
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_WATCHDOG_ENABLE;
    buffer[0] = idx; // length
    SendData(buffer, idx, NULL);
}


/*==========================   ZW_WatchDogEnable   ===========================
 * \ingroup ZWCMD
 * Abort the ongoing transmit started with ZW_SendData() or ZW_SendDataMulti(). If an ongoing
 * transmission is aborted, the callback function from the send call will return with the status
 * TRANSMIT_COMPLETE_NO_ACK.
 */
void serialApiSendDataAbort(void)
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_SEND_DATA_ABORT;
    buffer[0] = idx; // length
    SendData(buffer, idx, NULL);
}



/*===========================   ZW_WatchDogKick   ============================
**    Kick WatchDog on Z-Wave module
**
**    Returns:      Nothing
**
**    Parameters
**--------------------------------------------------------------------------*/
void serialApiWatchDogKick(void)
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_WATCHDOG_KICK;
    buffer[0] = idx; // length
    SendData(buffer, idx, NULL);
}

/*==========================   ZW_WatchDogDisable   ==========================
**    Disable WatchDog on Z-Wave module
**
**    Returns:      Nothing
**
**    Parameters
**--------------------------------------------------------------------------*/
void serialApiWatchDogDisable(void)
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_WATCHDOG_DISABLE;
    buffer[0] = idx; // length
    SendData(buffer, idx, NULL);
}

/*===========================   ZW_WatchDogStart   ===========================
**    Start WatchDog servicing on the Z-Wave module.
**    This means that the WatcDog is enabled and every ApplicationPoll call
**    it is Kicked. The information that it is started is also saved in NVM
**    so that the WatchDog is enabled again after Reset.
**
**    Returns:      Nothing
**
**    Parameters
**--------------------------------------------------------------------------*/
void serialApiWatchDogStart(void)
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_WATCHDOG_START;
    buffer[0] = idx; // length
    SendData(buffer, idx, NULL);
}

/*===========================   ZW_WatchDogStop   ============================
**    Stop WatchDog servicing on the Z-Wave module
**
**    Returns:      Nothing
**
**    Parameters
**--------------------------------------------------------------------------*/
void serialApiWatchDogStop(void)
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_WATCHDOG_STOP;
    buffer[0] = idx; // length
    SendData(buffer, idx, NULL);
}

/*====================   ZW_ReplicationCommandComplete ======================
**    Sends command completed to master remote. Called in replication mode
**    when a command from the sender has been processed.
**
**    Returns:      Nothing
**
**    Parameters    None
**
**--------------------------------------------------------------------------*/
void serialApiReplicationCommandComplete()
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_REPLICATION_COMMAND_COMPLETE;
    buffer[0] = idx; // length
    SendData(buffer, idx, NULL);
}

uint8_t serialApiEnableSUC(uint8_t state, uint8_t capabilities)
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_ENABLE_SUC;
    buffer[idx++] = state;
    buffer[idx++] = capabilities;
    buffer[0] = idx; // length
    uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_ENABLE_SUC, buffer, &byLen);

    return buffer[IDX_DATA];
}

uint8_t serialApiIsFailedNode(uint8_t nodeID)
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_IS_FAILED_NODE_ID;
    buffer[idx++] = nodeID;
    buffer[0] = idx; // length
    uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_IS_FAILED_NODE_ID, buffer, &byLen);

    return buffer[IDX_DATA];
}

/*======================   ZW_REPLICATION_SEND_DATA   ======================
**    Used when the controller is replication mode.
**    It sends the payload and expects the receiver to respond with a
**    command complete message.
**
**    Returns:      FALSE if transmitter queue overflow
**
**    Parameters
**    nodeID            IN  Destination node ID
**    pData             IN  Data buffer pointer
**    dataLength        IN  Data buffer length
**    txOptions         IN  Transmit option flags
**    completedFunc     IN  Transmit completed call back function
**          txStatus        IN Transmit status
**--------------------------------------------------------------------------*/
uint8_t serialApiReplicationSendData(uint8_t nodeID, uint8_t *pData, uint8_t dataLength, uint8_t txOptions,
                                     void (*completedFunc)(uint8_t))
{
    int i;
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_REPLICATION_SEND_DATA;
    buffer[idx++] = nodeID;
    buffer[idx++] = dataLength;
    for (i = 0; i < dataLength; i++)
    {
        buffer[idx++] = pData[i];
    }
    buffer[idx++] = txOptions;
    buffer[idx++] = byCompletedFunc; // Func id for CompletedFunc
    buffer[0] = idx;                 // length
    uint8_t byLen = 0;
    cbFuncZWReplicationSendData = completedFunc;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_REPLICATION_SEND_DATA, buffer, &byLen);

    return buffer[IDX_DATA];
}

/*========================   ZW_ControllerChange   ==========================
**    Start a Controller Change sequence
**
**    Returns:      Nothing
**
**    Parameters
**--------------------------------------------------------------------------*/
void serialApiControllerChange(uint8_t mode,
                               void (*completedFunc)(uint8_t, uint8_t, uint8_t *, uint8_t))
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_CONTROLLER_CHANGE;
    buffer[idx++] = mode;
    buffer[idx++] = byCompletedFunc;
    buffer[0] = idx; // length
    cbFuncZWNewController = completedFunc;
    SendData(buffer, idx, NULL);
}

/*========================   ZW_ReplaceFailedNode   ==========================
**    Start a Replace Failed Node sequence
**
**    Returns:      Nothing
**
**    Parameters
**--------------------------------------------------------------------------*/
uint8_t serialApiReplaceFailedNode(uint8_t nodeID,
                                   void (*completedFunc)(uint8_t))
{
    uint8_t retVal;
    uint8_t idx = 0;
    uint8_t byLen = 0;
    static uint8_t buffer[BUF_SIZE];
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_REPLACE_FAILED_NODE;
    buffer[idx++] = nodeID;
    buffer[idx++] = byCompletedFunc;
    buffer[0] = idx; // length

    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_REPLACE_FAILED_NODE, buffer, &byLen);

    if (byLen > IDX_DATA)
    {
        retVal = buffer[IDX_DATA];
        cbFuncZWReplaceFailedNode = completedFunc;
    }
    else
    {
        /* No answer */
        retVal = 0xFF;
        cbFuncZWReplaceFailedNode = NULL;
    }
    return retVal;
}

uint8_t serialApiRequestNodeInfo(uint8_t bNodeID,
                                 void (*completedFunc)(uint8_t))
{
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_REQUEST_NODE_INFO;
    buffer[idx++] = bNodeID;
    buffer[idx++] = byCompletedFunc; // Func id for CompletedFunc
    buffer[0] = idx;                 // length
    uint8_t byLen = 0;
    cbFuncZWRequestNodeInfo = completedFunc;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_REQUEST_NODE_INFO, buffer, &byLen);

    if (byLen > IDX_DATA)
    {
        return buffer[IDX_DATA];
    }
    else
    {
        /* No answer */
        return false;
    }
}

/*===========================   ZW_SendTestFrame   ===========================
**    Send test frame to destination node with current powerlevel
**
**    Returns:      FALSE if transmitter queue overflow or no answer
**
**    Parameters:
**    nodeID          IN  Destination node ID
**    completedFunc   IN  Transmit completed call back function
**--------------------------------------------------------------------------*/
uint8_t serialApiSendTestFrame(uint8_t nodeID, uint8_t bPowerLevel,
                               void (*completedFunc)(uint8_t))
{
    uint8_t retVal = false;
    uint8_t byLen = 0;
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    static uint8_t buffer[BUF_SIZE];

    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_SEND_TEST_FRAME;
    buffer[idx++] = nodeID;
    buffer[idx++] = bPowerLevel;
    buffer[idx++] = byCompletedFunc; // Func id for CompletedFunc
    buffer[0] = idx;                 // length
    cbFuncZWSendTestFrame = completedFunc;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_SEND_TEST_FRAME, buffer, &byLen);
    if (byLen > IDX_DATA)
    {
        retVal = buffer[IDX_DATA];
    }
    return retVal;
}

/*=========================   ZW_RF_Power_Level_Set   ========================
**    Set current RF Powerlevel setting on Z-Wave SerialAPI module
**
**    Returns:      Powerlevel set
**
**    Parameters:
**--------------------------------------------------------------------------*/
uint8_t serialApiRF_Power_Level_Set(uint8_t bPowerLevel)
{
    static uint8_t buffer[BUF_SIZE];
    uint8_t retVal = false;
    uint8_t idx = 0;
    uint8_t byLen = 0;

    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_RF_POWER_LEVEL_SET;
    buffer[idx++] = bPowerLevel;
    buffer[0] = idx;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_RF_POWER_LEVEL_SET, buffer, &byLen);

    if (byLen > IDX_DATA)
    {
        retVal = buffer[IDX_DATA];
    }
    return retVal;
}

/*=========================   ZW_RF_Power_Level_Get   ========================
**    Get current RF Powerlevel setting from Z-Wave SerialAPI module
**
**    Returns:      Powerlevel setting
**
**    Parameters:
**--------------------------------------------------------------------------*/
uint8_t serialApiRF_Power_Level_Get(void)
{
    static uint8_t buffer[BUF_SIZE];
    uint8_t retVal = false;
    uint8_t idx = 0;
    uint8_t byLen = 0;

    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_RF_POWER_LEVEL_GET;
    buffer[0] = idx;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_RF_POWER_LEVEL_GET, buffer, &byLen);

    if (byLen > IDX_DATA)
    {
        retVal = buffer[IDX_DATA];
    }
    return retVal;
}

/*========================   ZW_AssignReturnRoute   =========================
**
**    Assign static return routes within a Routing Slave node.
**    Calculate the shortest transport routes to a Routing Slave node
**    from the Report destination Node and
**    transmit the return routes to the Routing Slave node.
**
**    Returns:      Nothing
**
**    Parameters
**    bSrcNodeID        IN  Routing Slave Node ID
**    bDstNodeID        IN  Report destination Node ID
**    completedFunc IN   Completion handler
**          bStatus     IN  Transmit complete status
**
**--------------------------------------------------------------------------*/
void serialApiAssignReturnRoute(uint8_t bSrcNodeID, uint8_t bDstNodeID,
                                void (*completedFunc)(uint8_t))
{
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_ASSIGN_RETURN_ROUTE;
    buffer[idx++] = bSrcNodeID;
    buffer[idx++] = bDstNodeID;
    buffer[idx++] = byCompletedFunc; // Func id for CompletedFunc
    buffer[0] = idx;                 // length
    cbFuncZWAssignReturnRoute = completedFunc;
    SendData(buffer, idx, NULL);
}

/*========================   ZW_DeleteReturnRoute   =========================
**
**    Delete static return routes within a Routing Slave node.
**    Transmit "NULL" routes to the Routing Slave node.
**
**    Returns:      Nothing
**
**    Parameters
**    nodeID        IN   Routing Slave
**    completedFunc IN   Completion handler
**          bStatus     IN  Transmit complete status
**
**--------------------------------------------------------------------------*/
void serialApiDeleteReturnRoute(uint8_t nodeID,
                                void (*completedFunc)(uint8_t))
{
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_DELETE_RETURN_ROUTE;
    buffer[idx++] = nodeID;
    buffer[idx++] = byCompletedFunc; // Func id for CompletedFunc
    buffer[0] = idx;                 // length
    cbFuncZWDeleteReturnRoute = completedFunc;
    SendData(buffer, idx, NULL);
}

/*========================   ZW_AssignSUCReturnRoute   =========================
**
**    Assign static return routes within a Routing Slave node.
**    Calculate the shortest transport routes to a Routing Slave node
**    from the Static Update Controller (SUC) Node and
**    transmit the return routes to the Routing Slave node.
**
**    Returns:      Nothing
**
**    Parameters
**    bSrcNodeID        IN  Routing Slave Node ID
**    bDstNodeID        IN  SUC node ID
**    completedFunc IN   Completion handler
**          bStatus     IN  Transmit complete status
**
**--------------------------------------------------------------------------*/
void serialApiAssignSUCReturnRoute(uint8_t bSrcNodeID,
                                   void (*completedFunc)(uint8_t))
{
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_ASSIGN_SUC_RETURN_ROUTE;
    buffer[idx++] = bSrcNodeID;
    buffer[idx++] = byCompletedFunc; /* The extra funcID is added to ensures backward compatible. 
                                        This parameter has been removed starting  from dev. kit 4.1x. 
                                        and onwards and has therefore no meaning anymore.*/

    buffer[idx++] = byCompletedFunc; // Func id for CompletedFunc
    buffer[0] = idx;                 // length
    cbFuncZWAssignSUCReturnRoute = completedFunc;
    SendData(buffer, idx, NULL);
}

/*========================   ZW_DeleteSUCReturnRoute   =========================
**
**    Delete the ( Static Update Controller -SUC-) static return routes
**    within a Routing Slave node.
**    Transmit "NULL" routes to the Routing Slave node.
**
**    Returns:      Nothing
**
**    Parameters
**    nodeID        IN   Routing Slave
**    completedFunc IN   Completion handler
**          bStatus     IN  Transmit complete status
**
**--------------------------------------------------------------------------*/
void serialApiDeleteSUCReturnRoute(uint8_t nodeID,
                                   void (*completedFunc)(uint8_t))
{
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : SeqNo());
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_DELETE_SUC_RETURN_ROUTE;
    buffer[idx++] = nodeID;
    buffer[idx++] = byCompletedFunc; // Func id for CompletedFunc
    buffer[0] = idx;                 // length
    cbFuncZWDeleteSUCReturnRoute = completedFunc;
    SendData(buffer, idx, NULL);
}

/*========================   ZW_SendNodeInformation   ======================
**    Create and transmit a node informations frame
**
**    Returns:      Nothing
**
**    Parameters:
**    destNode      IN  Destination Node ID
**    txOptions     IN  Transmit option flags
**    completedFunc IN  Transmit completed call back function
**--------------------------------------------------------------------------*/
uint8_t serialApiSendNodeInformation(uint8_t destNode, uint8_t txOptions,
                                     void (*completedFunc)(uint8_t))
{
    uint8_t retVal = false;
    uint8_t idx = 0;
    uint8_t byCompletedFunc = (completedFunc == NULL ? 0 : 0x43);
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_SEND_NODE_INFORMATION;
    buffer[idx++] = destNode;
    buffer[idx++] = txOptions;
    buffer[idx++] = byCompletedFunc; // Func id for CompletedFunc
    buffer[0] = idx;                 // length
    uint8_t byLen = 0;
    cbFuncZWSendNodeInformation = completedFunc;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_SEND_NODE_INFORMATION, buffer, &byLen);
    if (byLen > IDX_DATA)
    {
        retVal = buffer[IDX_DATA];
    }
    return retVal;
}

/*=========================   ZW_ExploreRequestInclusion   ========================
**    To initial a Network-Wide Inclusion Process from Z-Wave SerialAPI module
**
**    Returns:      TRUE  Inclusion request queued for transmission
                    FALSE Node is not in learn mode 
**
**    Parameters:
**--------------------------------------------------------------------------*/
uint8_t serialApiExploreRequestInclusion(void)
{
    static uint8_t buffer[BUF_SIZE];
    uint8_t retVal = false;
    uint8_t idx = 0;
    uint8_t byLen = 0;

    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_EXPLORE_REQUEST_INCLUSION;
    buffer[0] = idx;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_EXPLORE_REQUEST_INCLUSION, buffer, &byLen);

    if (byLen > IDX_DATA)
    {
        retVal = buffer[IDX_DATA];
    }
    return retVal;
}

/*=========================   ZW_ExploreRequestExclusion   ========================
**    To initial a Network-Wide Exclusion Process from Z-Wave SerialAPI module
**
**    Returns:      TRUE  Inclusion request queued for transmission
                    FALSE Node is not in learn mode 
**
**    Parameters:
**--------------------------------------------------------------------------*/
uint8_t serialApiExploreRequestExclusion(void)
{
    static uint8_t buffer[BUF_SIZE];
    uint8_t retVal = false;
    uint8_t idx = 0;
    uint8_t byLen = 0;

    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_EXPLORE_REQUEST_EXCLUSION;
    buffer[0] = idx;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_EXPLORE_REQUEST_EXCLUSION, buffer, &byLen);

    if (byLen > IDX_DATA)
    {
        retVal = buffer[IDX_DATA];
    }
    return retVal;
}

/*================   SerialAPI_ApplicationNodeInformation   =================
**  Set ApplicationNodeInformation data to be used in subsequent calls to
**  ZW_SendNodeInformation.
**
**  Returns:        Nothing
**
**  Parameters
    HOST->ZW:
        operation          [open=0|read=1|write=2|close=3]
        length             desired length of read/write operation
        offset(MSB)        pointer to NVM memory
        offset(LSB)
        buffer[]           buffer only sent for operation=write

    ZW->HOST:
        retVal             [OK=0|error=1|EOF=-1]
        length             actual length of read/written data
        offset(MSB)        pointer to NVM memory (EOF ptr for operation=open)
        offset(LSB)
        buffer[]           buffer only returned for operation=read

**--------------------------------------------------------------------------*/
void serialApiBackupRestoreNvm(uint8_t operation, uint16_t offset, uint8_t *buf, uint8_t length)
{

    int i;
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    uint8_t byLen = 0;

    switch (operation) /* operation */
    {
    case NVMBackupRestoreOperationOpen: /* open */
                                        /* Lock everyone else out from making changes to the NVM content */
                                        /* Remember to have some kind of dead-mans-pedal to release lock again. */
                                        /* TODO */
        buffer[idx++] = 0;
        buffer[idx++] = REQUEST;
        buffer[idx++] = FUNC_ID_NVM_BACKUP_RESTORE;
        buffer[idx++] = 0; //open
        buffer[0] = idx;   // length
        byLen = 0;
        SendDataAndWaitForResponse(buffer, idx, FUNC_ID_NVM_BACKUP_RESTORE, buffer, &byLen);
        
        for (i = 0; i < byLen - 3; i++)
        {
            buf[i] = buffer[IDX_DATA + i];
        }
        break;
    case NVMBackupRestoreOperationRead: /* read */
        /* Validate input */
        buffer[idx++] = 0;
        buffer[idx++] = REQUEST;
        buffer[idx++] = FUNC_ID_NVM_BACKUP_RESTORE;
        buffer[idx++] = 1;                    //read
        buffer[idx++] = length;               //length data read NVM
        buffer[idx++] = (offset >> 8) & 0xFF; //address LSB
        buffer[idx++] = (uint8_t)offset;      //address MSB
        buffer[0] = idx;                      // length
        byLen = 0;
        SendDataAndWaitForResponse(buffer, idx, FUNC_ID_NVM_BACKUP_RESTORE, buffer, &byLen);
        
        //Get response data
        for (i = 0; i < byLen - 3; i++)
        {
            buf[i] = buffer[IDX_DATA + i];
            //mainlog(logUI,"buf read: 0x%02X ", buf[i]);
        }
        break;
    case NVMBackupRestoreOperationWrite: /* write */
        /* Validate input */
        buffer[idx++] = 0;
        buffer[idx++] = REQUEST;
        buffer[idx++] = FUNC_ID_NVM_BACKUP_RESTORE;
        buffer[idx++] = 2;                    //write
        buffer[idx++] = length;               //length data write NVM
        buffer[idx++] = (offset >> 8) & 0xFF; //address LSB
        buffer[idx++] = (uint8_t)offset;      //address MSB
        for (i = 0; i < length; i++)
        {
            buffer[idx++] = buf[i]; //data
        }
        buffer[0] = idx; // length
        byLen = 0;
        SendDataAndWaitForResponse(buffer, idx, FUNC_ID_NVM_BACKUP_RESTORE, buffer, &byLen);

        //Get response data
        for (i = 0; i < byLen - 3; i++)
        {
            buf[i] = buffer[IDX_DATA + i];
            //mainlog(logUI,"buf read: 0x%02X ", buf[i]);
        }
        break;
    case NVMBackupRestoreOperationClose: /* Close */
        /* Unlock NVM content, so everyone else can make changes again */
        /* TODO */
        buffer[idx++] = 0;
        buffer[idx++] = REQUEST;
        buffer[idx++] = FUNC_ID_NVM_BACKUP_RESTORE;
        buffer[idx++] = 3; //close
        buffer[0] = idx;   // length
        byLen = 0;

        SendDataAndWaitForResponse(buffer, idx, FUNC_ID_NVM_BACKUP_RESTORE, buffer, &byLen);

        for (i = 0; i < byLen - 3; i++)
        {
            buf[i] = buffer[IDX_DATA + i];
        }
        break;
    default:
        break;
    }
}


/**
 * \ingroup BASIS
 * The API call generates a random word using the ZW0201/ZW0301 builtin random number
 * generator (RNG). If RF needs to be in Receive then ZW_SetRFReceiveMode should be called afterwards.
 * \return
 * TRUE If possible to generate random number.
 * \return
 * FALSE  If not possible e.g. RF not powered down.
 * \param[in,out] randomWord Pointer to word variable, which should receive the random word.
 * \param[in] bResetRadio  If TRUE the RF radio is reinitialized after generating the random word.
 *
 * \note
 * The ZW0201/ZW0301 RNG is based on the RF transceiver, which must be in powerdown
 * state (see ZW_SetRFReceiveMode) to assure proper operation of the RNG. Remember
 * to call ZW_GetRandomWord with bResetRadio = TRUE when the last random word is to
 * be generated. This is needed for the RF to be reinitialized, so that it can be
 * used to transmit and receive again.
 * *
 * \macro{ZW_GET_RANDOM_WORD(randomWord\,bResetRadio)}
 * \serialapi{
 * HOST -> ZW: REQ | 0x1C | noRandomBytes
 * ZW -> HOST: RES | 0x1C | randomGenerationSuccess | noRandomBytesGenerated | randombytes[]
 * }
 * \note
 * The Serial API function 0x1C makes use of the ZW_GetRandomWord to generate a specified number of random bytes and takes care of the handling of the RF:
 * - Set the RF in powerdown prior to calling the ZW_GetRandomWord the first time, if not possible then return result to HOST.
 * - Call ZW_GetRandomWord until enough random bytes generated or ZW_GetRandomWord returns FALSE.
 * - Call ZW_GetRandomWord with bResetRadio = TRUE to reinitialize the radio.
 * - Call ZW_SetRFReceiveMode with TRUE if the serialAPI hardware is a listening device or with FALSE if it is a non-listening device.
 * - Return result to HOST.
 *
 * @param[in]  noRandomBytes to generate
 * @param[out] randomGenerationSuccess  TRUE if random bytes could be generated
 * @param[out] noRandomBytesGenerated   Number of random numbers generated
 * @param[out] randombytes[] Array of generated random bytes
 *
 */
bool serialApiGetRandomWord(uint8_t *randomWord, bool bResetRadio)
{

    uint8_t idx=0;
    int i;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_GET_RANDOM;
    buffer[idx++] = 0x8;

    buffer[0] = idx;    // length

    uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_GET_RANDOM, buffer, &byLen);
    for ( i = 0;i < 0x8;i++ )
    {
        randomWord[ i ] = buffer[ IDX_DATA + i +2];
    }
    return 0;
}


#include "rijndael-alg-fst.h"
bool serialApiAES128Encrypt(const uint8_t *ext_input, uint8_t *ext_output, const uint8_t *cipherKey) CC_REENTRANT_ARG
{
    int Nr;                     /* key-length-dependent number of rounds */
    uint32_t rk[4*(MAXNR + 1)]; /* key schedule */
#if 0
    uint8_t idx=0;
    if(SupportsCommand(FUNC_ID_ZW_AES_ECB)) 
    {
        buffer[idx++] = REQUEST;
        buffer[idx++] = FUNC_ID_ZW_AES_ECB;
        memcpy(&buffer[2],cipherKey,16);
        memcpy(&buffer[18],ext_input,16);
        SendDataAndWaitForResponse(buffer, 32+2, FUNC_ID_ZW_AES_ECB, buffer, &byLen);
        memcpy(ext_output,&buffer[IDX_DATA],16);
        return 1;
    } else
#endif
    {
        Nr = rijndaelKeySetupEnc(rk, cipherKey, 128);
        rijndaelEncrypt(rk, Nr, ext_input, ext_output);
        return 1;
    }
}


/*========================   ZW_AddNodeToNetworkSmartStart   ======================
**
**    Add any type of node to the network and accept prekit inclusion.
**    This should be called after receiving a ApplicationControllerUpdate
**    with a whitelisted DSK.
**
**    The modes is always ADD_NODE_HOMEID 0x07,
**
**    potentially coupled with these flags
**
**    ADD_NODE_OPTION_HIGH_POWER    Set this flag in bMode for High Power inclusion.
**    ADD_NODE_OPTION_NETWORK_WIDE  Set this flag in bMode for Network wide inclusion
**
**    DSK is a pointer to the 16-byte DSK.
**
**    Side effects:
**
**--------------------------------------------------------------------------*/
void
serialApiAddNodeToNetworkSmartStart(uint8_t bMode, uint8_t *dsk,
                    void (*completedFunc)(uint8_t, uint8_t, uint8_t *, uint8_t))
{
    uint8_t idx = 0;
    static uint8_t buffer[BUF_SIZE];
    uint8_t byCompletedFunc = ( completedFunc == NULL ? 0 : 0x03 );
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_ADD_NODE_TO_NETWORK;
    buffer[idx++] = bMode;
    buffer[idx++] = byCompletedFunc;
    buffer[idx++] = dsk[0];
    buffer[idx++] = dsk[1];
    buffer[idx++] = dsk[2];
    buffer[idx++] = dsk[3];
    buffer[idx++] = dsk[4];
    buffer[idx++] = dsk[5];
    buffer[idx++] = dsk[6];
    buffer[idx++] = dsk[7];
    buffer[0] = idx;
    cbFuncZWNewController = completedFunc;
    SendData(buffer, idx, NULL);

}

void serialApiAutoProgrammingEnable(void) 
{
    /* Chip will reboot after receiving this command,
    * so send as raw bytes without waiting for SerialAPI ACKs */
    uint8_t buf[5] = { 0x01, 0x03, 0x00, 0x27, 0xdb};
    putbuffer_t(fd, buf, 5);
}

/*========================   ZW_GetBackgroundRSSI   ======================
 *
 * Returns the latest  value of the background RSSI
 *
 * Returns an array of RSSI values
 * and a length of that array.
 *
 * values must be an array with a length of 3 or more bytes.
 *
 * HOST->ZW: (no arguments)
 * ZW->HOST: RES | RSSI Ch0 | RSSI Ch1 | RSSI Ch2 (3CH systems only)
 *
 *--------------------------------------------------------------------------*/
void
serialApiGetBackgroundRSSI(uint8_t *rssi_values, uint8_t *values_length)
{
    uint8_t numChannels; /* Number of channels returned*/
    uint8_t idx=0;
    static uint8_t buffer[BUF_SIZE];
    buffer[idx++] = 0;
    buffer[idx++] = REQUEST;
    buffer[idx++] = FUNC_ID_ZW_GET_BACKGROUND_RSSI;

    buffer[0] = idx;    // length

    uint8_t byLen = 0;
    SendDataAndWaitForResponse(buffer, idx, FUNC_ID_ZW_GET_BACKGROUND_RSSI, buffer, &byLen);

    numChannels = byLen - IDX_DATA; 
    if (numChannels > 3)
    {
        numChannels = 3;
    }
    *values_length = numChannels;

    memcpy(rssi_values, &buffer[IDX_DATA], numChannels);
}


