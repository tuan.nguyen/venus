#include <stdlib.h>
#include <memory.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "zw_serialapi/zw_classcmd.h"
#include "zw_serialapi/zw_serialapi.h"

#include "ZW_Poll.h"
#include "ZW_PollUtil.h"
#include "ZW_DevicePoll.h"
#include "ZW_PollApi.h"
#include "utils.h"

bool waitingBasicReport = false;
extern zwnet_p zwaveNetworkPolling;;
char *get_nwstr(uint8_t nw)
{
    char *nwstr;
    switch (nw)
    {
    case ZWNET_OP_NONE:
        nwstr = "ZWNET_OP_NONE";
        break;
    case ZWNET_OP_INITIALIZE:
        nwstr = "ZWNET_OP_INITIALIZE";
        break;
    case ZWNET_OP_ADD_NODE:
        nwstr = "ZWNET_OP_ADD_NODE";
        break;
    case ZWNET_OP_RM_NODE:
        nwstr = "ZWNET_OP_RM_NODE";
        break;
    case ZWNET_OP_RP_NODE:
        nwstr = "ZWNET_OP_RP_NODE";
        break;
    case ZWNET_OP_RM_FAILED_ID:
        nwstr = "ZWNET_OP_RM_FAILED_ID";
        break;
    case ZWNET_OP_NETWORK_TEST:
        nwstr = "ZWNET_OP_NETWORK_TEST";
        break;
    case ZWNET_OP_INITIATE:
        nwstr = "ZWNET_OP_INITIATE";
        break;
    case ZWNET_OP_UPDATE:
        nwstr = "ZWNET_OP_UPDATE";
        break;
    case ZWNET_OP_RESET:
        nwstr = "ZWNET_OP_RESET";
        break;
    case ZWNET_OP_MIGRATE_SUC:
        nwstr = "ZWNET_OP_MIGRATE_SUC";
        break;
    case ZWNET_OP_MIGRATE:
        nwstr = "ZWNET_OP_MIGRATE";
        break;
    case ZWNET_OP_ASSIGN:
        nwstr = "ZWNET_OP_ASSIGN";
        break;
    case ZWNET_OP_NODE_UPDATE:
        nwstr = "ZWNET_OP_NODE_UPDATE";
        break;
    case ZWNET_OP_SEND_NIF:
        nwstr = "ZWNET_OP_SEND_NIF";
        break;
    case ZWNET_OP_NW_CHANGED:
        nwstr = "ZWNET_OP_NW_CHANGED";
        break;
    case ZWNET_OP_NODE_CACHE_UPT:
        nwstr = "ZWNET_OP_NODE_CACHE_UPT";
        break;
    case ZWNET_OP_SAVE_NW:
        nwstr = "ZWNET_OP_SAVE_NW";
        break;
    case ZWNET_OP_SLEEP_NODE_UPT:
        nwstr = "ZWNET_OP_SLEEP_NODE_UPT";
        break;
    case ZWNET_OP_FW_UPDT:
        nwstr = "ZWNET_OP_FW_UPDT";
        break;
    case ZWNET_OP_IS_FAILED_NODE:
        nwstr = "ZWNET_OP_IS_FAILED_NODE";
        break;
    case ZWNET_OP_GET_CTRL_INFO:
        nwstr = "ZWNET_OP_GET_CTRL_INFO";
        break;
    case ZWNET_OP_SEND_SPEC_CMD:
        nwstr = "ZWNET_OP_SEND_SPEC_CMD";
        break;
    case ZWNET_OP_GET_SECURE_KEY:
        nwstr = "ZWNET_OP_GET_SECURE_KEY";
        break;
    case ZWNET_OP_FW_UPDT_CTRL:
        nwstr = "ZWNET_OP_FW_UPDT_CTRL";
        break;
    case ZWNET_OP_GET_FW_VERSION:
        nwstr = "ZWNET_OP_GET_FW_VERSION";
        break;
    case ZWNET_OP_GET_NODE_LIST:
        nwstr = "ZWNET_OP_GET_NODE_LIST";
        break;
    case ZWNET_OP_HANDLE_WAKEUP:
        nwstr = "ZWNET_OP_HANDLE_WAKEUP";
        break;
    case ZWNET_OP_MAIN_THREAD_NONE:
        nwstr = "ZWNET_OP_MAIN_THREAD_NONE";
        break;
    case ZWNET_OP_WAKEUP_THREAD_NONE:
        nwstr = "ZWNET_OP_WAKEUP_THREAD_NONE";
        break;
    default:
        nwstr = " ";
        break;
    }
    return nwstr;
}

/**
zwpoll_mv_ent_endlist - Move entry to end of list
@param[in]  poll_ctx        Context
@param[in]  ent             Poll entry
@return
@pre Caller must lock poll_mtx
*/
static void zwpoll_mv_ent_endlist(zwpoll_ctx_t *poll_ctx, poll_q_ent_t *ent)
{
    //There is only 1 entry in the list
    if (poll_ctx->poll_lst_hd == poll_ctx->poll_lst_tail)
    {
        return;
    }

    //The entry is already at the end of list
    if (poll_ctx->poll_lst_tail == ent)
    {
        return;
    }

    //The entry is the head of the list
    if (poll_ctx->poll_lst_hd == ent)
    {
        poll_ctx->poll_lst_hd = ent->next;
        poll_ctx->poll_lst_hd->prev = NULL;
    }
    else
    {
        ent->prev->next = ent->next;
        ent->next->prev = ent->prev;
    }

    //Adjust the former tail
    poll_ctx->poll_lst_tail->next = ent;

    ent->prev = poll_ctx->poll_lst_tail;
    ent->next = NULL;

    //Assign the entry as tail
    poll_ctx->poll_lst_tail = ent;
}

static void changeNextPollingTime(uint32_t* nextPolling, uint32_t currentTime)
{
    if(!nextPolling)
    {
        return;
    }
    if (util_tmr_exp_chk32(*nextPolling, currentTime) == 0)
    {
        *nextPolling = currentTime;
    }
}
/**
zwpoll_add_ent_endlist - Add entry to end of list
@param[in]  poll_ctx        Context
@param[in]  ent             New poll entry
@return     1 if the ent is the only entry in the list after adding; else return 0.
@pre Caller must lock poll_mtx
*/
static int zwpoll_add_ent_endlist(zwpoll_ctx_t *poll_ctx, poll_q_ent_t *ent)
{
    mainlog(logDebug, "zwpoll_add_ent_endlist");
    //The list is empty
    if (!poll_ctx->poll_lst_hd && !poll_ctx->poll_lst_tail)
    {
        poll_ctx->poll_lst_hd = poll_ctx->poll_lst_tail = ent;
        ent->next = ent->prev = NULL;
        mainlog(logDebug, "zwpoll_add_ent_endlist return 1");
        return 1;
    }

    //Adjust the former tail
    poll_ctx->poll_lst_tail->next = ent;

    ent->prev = poll_ctx->poll_lst_tail;
    ent->next = NULL;

    //Assign the entry as tail
    poll_ctx->poll_lst_tail = ent;
    mainlog(logDebug, "zwpoll_add_ent_endlist return 0");

    poll_q_ent_t *en_q;
    int c = 0;
    en_q = poll_ctx->poll_lst_hd;
    while (en_q)
    {
        en_q = en_q->next;
        c++;
    }
    mainlog(logDebug, "number of en_q: %d", c);

    return 0;
}

/**
zwpoll_rm_ent - Remove entry from list
@param[in]  poll_ctx        Context
@param[in]  ent             Poll entry
@return
@pre Caller must lock poll_mtx
@post ent should not be used as it is freed
*/
static void zwpoll_rm_ent(zwpoll_ctx_t *poll_ctx, poll_q_ent_t *ent)
{
    //There is only 1 entry in the list
    if (poll_ctx->poll_lst_hd == poll_ctx->poll_lst_tail)
    {
        poll_ctx->poll_lst_hd = poll_ctx->poll_lst_tail = NULL;
        free(ent);
        return;
    }

    //The entry is the head of the list
    if (poll_ctx->poll_lst_hd == ent)
    {
        poll_ctx->poll_lst_hd = ent->next;
        if (poll_ctx->poll_lst_hd)
        {
            poll_ctx->poll_lst_hd->prev = NULL;
        }
    }
    //The entry is the tail of the list
    else if (poll_ctx->poll_lst_tail == ent)
    {
        poll_ctx->poll_lst_tail = ent->prev;
        poll_ctx->poll_lst_tail->next = NULL;
    }
    else
    {
        ent->prev->next = ent->next;
        ent->next->prev = ent->prev;
    }

    free(ent);
}


/**
zwpoll_wkup_chk - Check entries that are waiting for wakeup notification from the node
@param[in]  nodeId Node ID of the sleeping node
@return
*/
static void zwpoll_wkup_chk(zwpoll_ctx_t *poll_ctx, uint8_t nodeId)
{
    poll_q_ent_t *ent;
    poll_q_ent_t *prev_ent;
    uint32_t curr_time;

    plt_mtx_lck(poll_ctx->poll_mtx);

    if (poll_ctx->poll_lst_hd == NULL)
    { //Poll queue is empty
        plt_mtx_ulck(poll_ctx->poll_mtx);
        return;
    }

    curr_time = poll_ctx->tmr_tick;
    ent = poll_ctx->poll_lst_hd;

    while (ent)
    {
        if (ent->active_sts == POLL_STS_ACTIVE)
        {
            if (ent->nodeId == nodeId)
            {
                if (ent->wr_cmd_q)
                { //This entry had written a command to the command queue
                    //before this wakeup notification arrived
                    ent->wr_cmd_q = 0;
                    ent->expired = 0;

                    if (ent->poll_cnt == 0)
                    { //Repetitive polling
                        ent->next_poll_tm = ent->interval + curr_time;
                    }
                    else
                    { //Non-repetitive polling
                        uint16_t intv_cnt;
                        uint16_t last_tot_poll_cnt; //For checking overflow case

                        intv_cnt = util_time_diff32(curr_time, ent->next_poll_tm) / ent->interval + 1;
                        last_tot_poll_cnt = ent->tot_poll_cnt;
                        ent->tot_poll_cnt += intv_cnt;
                        ent->next_poll_tm = ent->next_poll_tm + (intv_cnt * ent->interval);

                        //Check whether this is the last poll
                        if ((ent->tot_poll_cnt > ent->poll_cnt) || (ent->tot_poll_cnt < last_tot_poll_cnt))
                        {
                            ent->active_sts = POLL_STS_INACTIVE_WAIT_RPT; //not active, waiting for report
                            ent->rpt_tout = POLL_RPT_TIMEOUT + curr_time;
                        }
                    }
                    //Save previous entry since the current entry will be moved to the end of list
                    prev_ent = ent->prev;

                    //Move entry to end of list
                    zwpoll_mv_ent_endlist(poll_ctx, ent);

                    ent = prev_ent;
                }
            }
        }

        //Next
        if (!ent)
        {
            ent = poll_ctx->poll_lst_hd;
        }
        else
        {
            ent = ent->next;
        }
    }
    plt_mtx_ulck(poll_ctx->poll_mtx);
}

/**
zwpoll_same_node_poll - Poll the same node for expired entries
@param[in]  poll_ctx    Context
@return
@pre    Caller must lock the poll_mtx
*/
static void zwpoll_same_node_poll(zwpoll_ctx_t *poll_ctx)
{
    poll_q_ent_t *ent;
    uint32_t curr_time;

    if (poll_ctx->poll_lst_hd == NULL)
    { //Poll queue is empty
        return;
    }

    ent = poll_ctx->poll_lst_hd;

    while (ent)
    {
        if (ent->active_sts == POLL_STS_ACTIVE)
        {
            if (ent->nodeId == poll_ctx->cur_nodeId)
            {
                if (ent->expired && (ent->poll_seq_num != poll_ctx->poll_seq_num))
                {
                    zwif_exec_ex(&ent->ifd);

                    //Update poll sequence number to prevent it is polled again whtihin the same node polling cycle
                    ent->poll_seq_num = poll_ctx->poll_seq_num;
                    ent->wait_rpt = 1;

                    curr_time = poll_ctx->tmr_tick;

                    //Save the following for calculation of command time
                    poll_ctx->cur_start_tm = curr_time;
                    poll_ctx->cur_cmd_tm = 0;
                    poll_ctx->cur_cmd_cls = ent->cmd_cls;
                    poll_ctx->cur_rpt = ent->rpt;

                    //Update poll context next polling time
                    poll_ctx->next_poll_tm = curr_time + MIN_POLL_TIME;

                    if (ent->poll_cnt == 0)
                    { //Repetitive polling
                        ent->next_poll_tm = ent->interval + curr_time;
                    }
                    else
                    { //Non-repetitive polling
                        uint16_t intv_cnt;
                        uint16_t last_tot_poll_cnt; //For checking overflow case

                        intv_cnt = util_time_diff32(curr_time, ent->next_poll_tm) / ent->interval + 1;
                        last_tot_poll_cnt = ent->tot_poll_cnt;
                        ent->tot_poll_cnt += intv_cnt;
                        ent->next_poll_tm = ent->next_poll_tm + (intv_cnt * ent->interval);

                        //Check whether this is the last poll
                        if ((ent->tot_poll_cnt > ent->poll_cnt) || (ent->tot_poll_cnt < last_tot_poll_cnt))
                        {
                            //debug_zwapi_ts_msg(&poll_ctx->net->plt_ctx, "Expiring entry:%u", ent->handle);
                            ent->active_sts = POLL_STS_INACTIVE_WAIT_RPT; //not active, waiting for report
                            ent->rpt_tout = POLL_RPT_TIMEOUT + curr_time;
                        }
                    }

                    ent->expired = 0;

                    //Move entry to end of list
                    zwpoll_mv_ent_endlist(poll_ctx, ent);

                    return;
                }
            }
        }

        //Next
        ent = ent->next;
    }

    //No polling expired in the same node, next poll will be for the other node
    poll_ctx->poll_seq_num += 2; //Make sure it is always an odd number
}

/**
zwpoll_rpt_rcv_chk - Callback when a report is received
@param[in]  poll_ctx    Poll context
@param[in]  msg         Message
@return
*/
static void zwpoll_rpt_rcv_chk(zwpoll_ctx_t *poll_ctx, poll_evt_msg_t *msg)
{
    poll_q_ent_t *ent;

    plt_mtx_lck(poll_ctx->poll_mtx);

    if (poll_ctx->poll_lst_hd == NULL)
    { //Poll queue is empty
        plt_mtx_ulck(poll_ctx->poll_mtx);
        return;
    }

    if ((poll_ctx->cur_nodeId == msg->nodeid) && (poll_ctx->cur_cmd_cls == msg->cls) && (poll_ctx->cur_rpt == msg->rpt))
    { //Found matching node and report of the latest report_get polling command

        //Re-calculate next poll time
        poll_ctx->next_poll_tm = poll_ctx->tmr_tick + poll_ctx->cur_cmd_tm + MIN_POLL_TIME;

        //Clear the command class and report
        poll_ctx->cur_cmd_cls = poll_ctx->cur_rpt = 0;
    }

    //Start from end of list
    ent = poll_ctx->poll_lst_tail;

    while (ent)
    {
        if (ent->nodeId == msg->nodeid)
        {
            if (ent->wait_rpt)
            {
                if ((ent->cmd_cls == msg->cls) && (ent->rpt == msg->rpt))
                { //The polling report has arrived
                    ent->wait_rpt = 0;

                    if (ent->active_sts == POLL_STS_INACTIVE_WAIT_RPT)
                    { //Callback to report poll completion
                        if (ent->cmplt_cb)
                        {
                            poll_cb_msg_t cb_msg;

                            cb_msg.cmplt_cb = ent->cmplt_cb;
                            cb_msg.handle = ent->handle;
                            cb_msg.usr_param = ent->usr_param;
                            cb_msg.usr_token = ent->usr_token;

                            util_msg_loop_send(poll_ctx->cb_msg_loop_ctx, (void *)&cb_msg, sizeof(poll_cb_msg_t));
                        }

                        //Remove the entry
                        zwpoll_rm_ent(poll_ctx, ent);
                    }
                    //Poll for other expired entries in the same node
                    zwpoll_same_node_poll(poll_ctx);
                    break;
                }
            }
        }

        //Next
        ent = ent->prev;
    }
    plt_mtx_ulck(poll_ctx->poll_mtx);
}

/**
zwpoll_chk_all - Check all entries and mark expiry flag if poll timer has expired
@param[in]  poll_ctx        Context
@return
@pre Caller must lock poll_mtx
*/
static void zwpoll_chk_all(zwpoll_ctx_t *poll_ctx)
{
    poll_q_ent_t *ent;
    poll_q_ent_t *prev_ent; //Pointer to previous list entry

    if (!poll_ctx->poll_lst_hd)
    {
        return;
    }

    ent = poll_ctx->poll_lst_hd;

    while (ent)
    {
        if (!ent->expired)
        {
            if (ent->active_sts == POLL_STS_ACTIVE)
            {
                //Check whether the next poll time has expired
                if (util_tmr_exp_chk32(poll_ctx->tmr_tick, ent->next_poll_tm))
                { //Expired, mark it
                    ent->expired = 1;
                }
            }
            else //active_sts == POLL_STS_INACTIVE_WAIT_RPT
            {
                //Check whether the report timeout occured
                if (util_tmr_exp_chk32(poll_ctx->tmr_tick, ent->rpt_tout))
                { //Callback and remove the entry

                    if (ent->cmplt_cb)
                    {
                        poll_cb_msg_t cb_msg;

                        cb_msg.cmplt_cb = ent->cmplt_cb;
                        cb_msg.handle = ent->handle;
                        cb_msg.usr_param = ent->usr_param;
                        cb_msg.usr_token = ent->usr_token;

                        util_msg_loop_send(poll_ctx->cb_msg_loop_ctx, (void *)&cb_msg, sizeof(poll_cb_msg_t));
                    }

                    //Save previous entry since the current entry will be removed
                    prev_ent = ent->prev;
                    //Remove the entry
                    zwpoll_rm_ent(poll_ctx, ent);
                    ent = prev_ent;
                }
            }
        }

        //Next
        if (!ent)
        {
            ent = poll_ctx->poll_lst_hd;
        }
        else
        {
            ent = ent->next;
        }
    }
}

/**
zwpoll_handle_expiry - Handle poll timer expiry of a non-sleeping node
@param[in]  poll_ctx        Context
@param[in]  ent             Poll entry that has expired
@return
@pre Caller must lock poll_mtx
*/
static void zwpoll_handle_expiry(zwpoll_ctx_t *poll_ctx, poll_q_ent_t *ent)
{
    uint32_t curr_time;
    uint16_t extendId = (ent->ifd.epid << 8) + ent->nodeId;
    if(extendId == getLastListFromPollingList())
    {
        //Update poll context next polling time
        //stop polling 10s
        changeNextPollingTime(&poll_ctx->next_poll_tm, poll_ctx->tmr_tick + ZWAVE_TIME_STOP_POLLING);
    }
    else
    {
        //Update poll context next polling time
        changeNextPollingTime(&poll_ctx->next_poll_tm, poll_ctx->tmr_tick + MIN_POLL_TIME);
    }
    if (poll_ctx->net->curr_op == ZWNET_OP_NONE)
    {
        if(!(getNoPollingFail(extendId) >= ZWAVE_MAX_POLLING_FAILED))
        {
            waitingBasicReport = true;
            zwif_exec_ex(&ent->ifd);
        }
    }

    poll_ctx->poll_seq_num += 2; //Make sure it is always an odd number
    poll_ctx->cur_nodeId = ent->nodeId;
    poll_ctx->cur_endpoint = ent->ifd.epid;

    curr_time = poll_ctx->tmr_tick;

    //Save the following for calculation of command time
    poll_ctx->cur_start_tm = curr_time;
    poll_ctx->cur_cmd_tm = 0;
    poll_ctx->cur_cmd_cls = ent->cmd_cls;
    poll_ctx->cur_rpt = ent->rpt;

    //Update poll sequence number to prevent it is polled again whtihin the same node polling cycle
    ent->poll_seq_num = poll_ctx->poll_seq_num;
    ent->wait_rpt = 1;

    if (ent->poll_cnt == 0)
    { //Repetitive polling
        ent->next_poll_tm = ent->interval + curr_time;
    }
    else
    { //Non-repetitive polling
        uint16_t intv_cnt;
        uint16_t last_tot_poll_cnt; //For checking overflow case

        intv_cnt = util_time_diff32(curr_time, ent->next_poll_tm) / ent->interval + 1;
        last_tot_poll_cnt = ent->tot_poll_cnt;
        ent->tot_poll_cnt += intv_cnt;
        ent->next_poll_tm = ent->next_poll_tm + (intv_cnt * ent->interval);

        //Check whether this is the last poll
        if ((ent->tot_poll_cnt > ent->poll_cnt) || (ent->tot_poll_cnt < last_tot_poll_cnt))
        {
            ent->active_sts = POLL_STS_INACTIVE_WAIT_RPT; //not active, waiting for report
            ent->rpt_tout = POLL_RPT_TIMEOUT + curr_time;
        }
    }

    ent->expired = 0;
    //Move entry to end of list
    zwpoll_mv_ent_endlist(poll_ctx, ent);
}


/**
zwpoll_tmr_chk_thrd - thread to process timer tick event
@param[in]  data        Context
@return
*/
static void zwpoll_tmr_chk_thrd(void *data)
{
    zwpoll_ctx_t *poll_ctx = (zwpoll_ctx_t *)data;
    poll_q_ent_t *ent;
    poll_q_ent_t *saved_tail; //Saved poll list tail
    int last_poll_ent;
    uint32_t next_exp_chk_tm;
    uint32_t prev_poll_tm;

    poll_ctx->tmr_chk_thrd_sts = 1;

    next_exp_chk_tm = poll_ctx->tmr_tick + CHECK_EXPIRY_INTERVAL; //CHECK_EXPIRY_INTERVAL = 2

    while (1)
    {
        //Wait for timer tick event
        plt_sem_wait(poll_ctx->tmr_sem);
        if(waitingBasicReport)
        {
            changeNextPollingTime(&poll_ctx->next_poll_tm, poll_ctx->tmr_tick + MIN_POLL_TIME);
        }
        //mainlog(logPolling, "### Next Polling: %d, currrentTime: %d", poll_ctx->next_poll_tm, poll_ctx->tmr_tick);
        if (poll_ctx->net->curr_op != ZWNET_OP_NONE)
        {
            changeNextPollingTime(&poll_ctx->next_poll_tm, poll_ctx->tmr_tick + ZWAVE_POLLING_FREE_TIME);
        }

        //Check whether to exit the thread
        if (poll_ctx->tmr_chk_thrd_run == 0)
        {
            poll_ctx->tmr_chk_thrd_sts = 0;
            return;
        }

        plt_mtx_lck(poll_ctx->poll_mtx);

        //Check whether it's time to check entries for expiry
        if (util_tmr_exp_chk32(poll_ctx->tmr_tick, next_exp_chk_tm))
        {
            zwpoll_chk_all(poll_ctx);
            //Recalculate next checking time
            next_exp_chk_tm = poll_ctx->tmr_tick + CHECK_EXPIRY_INTERVAL;
        }

        //Check whether the next poll time has expired
        if (util_tmr_exp_chk32(poll_ctx->tmr_tick, poll_ctx->next_poll_tm) == 0)
        {
            //Not expire yet, continue to wait
            plt_mtx_ulck(poll_ctx->poll_mtx);
            continue;
        }

        if (poll_ctx->poll_lst_hd == NULL)
        {
            //Poll queue is empty
            mainlog(logPolling, "For debug, Poll queue is empty");
            changeNextPollingTime(&poll_ctx->next_poll_tm, poll_ctx->tmr_tick + MIN_POLL_TIME);

            plt_mtx_ulck(poll_ctx->poll_mtx);
            continue;
        }
        ent = poll_ctx->poll_lst_hd;
        saved_tail = poll_ctx->poll_lst_tail;
        last_poll_ent = 0;

        //Save the poll time
        prev_poll_tm = poll_ctx->next_poll_tm;

        while (ent)
        {
            if (ent == saved_tail)
            {
                //This is the last entry to check for polling
                last_poll_ent = 1;
            }
            if (ent->active_sts == POLL_STS_ACTIVE)
            {
                if (ent->expired)
                {
                    zwpoll_handle_expiry(poll_ctx, ent);
                    break;
                }
            }

            if (last_poll_ent)
            {
                break;
            }

            //Next
            if (!ent)
            {
                ent = poll_ctx->poll_lst_hd;
            }
            else
            {
                ent = ent->next;
            }
        }

        //Check whether poll time has been refreshed
        if (prev_poll_tm == poll_ctx->next_poll_tm)
        { //Recalculate poll time using shorter time since the minimum poll time has been met.
            changeNextPollingTime(&poll_ctx->next_poll_tm, poll_ctx->tmr_tick + CHECK_EXPIRY_INTERVAL);
        }

        plt_mtx_ulck(poll_ctx->poll_mtx);
    }
}

/**
zwpoll_tmr_tick_cb - Timer tick timeout callback
@param[in] data     Pointer to poll context
@return
*/
static void zwpoll_tmr_tick_cb(void *data)
{
    zwpoll_ctx_t *poll_ctx = (zwpoll_ctx_t *)data;

    //Increment timer tick
    poll_ctx->tmr_tick++;

    //Send timer tick event
    if (poll_ctx->run)
    {
        plt_sem_post(poll_ctx->tmr_sem);
    }
}

/**
zwpoll_rm_all - remove all polling requests
@param[in]  poll_ctx    Context
@return
*/
void zwpoll_rm_all(zwpoll_ctx_t *poll_ctx)
{
    poll_q_ent_t *ent;
    poll_q_ent_t *rm_ent; //Entry to be removed

    plt_mtx_lck(poll_ctx->poll_mtx);
    if (!poll_ctx->poll_lst_hd)
    {
        plt_mtx_ulck(poll_ctx->poll_mtx);
        return;
    }

    ent = poll_ctx->poll_lst_hd;

    while (ent)
    {
        rm_ent = ent;
        ent = ent->next;
        free(rm_ent);
    }

    poll_ctx->poll_lst_hd = poll_ctx->poll_lst_tail = NULL;

    plt_mtx_ulck(poll_ctx->poll_mtx);
}


/**
zwpoll_rm - remove polling requests that belong to the specified node
@param[in]  net         network
@param[in]  nodeId     node id
@return     ZW_ERR_NONE if success; else ZW_ERR_XXX on error
*/
int zwavepoll_rm(zwnet_p net, uint8_t nodeId)
{
    zwpoll_ctx_t *poll_ctx = net->poll_ctx;
    poll_q_ent_t *ent;
    poll_q_ent_t *rm_ent; //Entry to be removed

    if (!net->poll_enable)
    {
        return ZW_ERR_UNSUPPORTED;
    }

    plt_mtx_lck(poll_ctx->poll_mtx);
    if (!poll_ctx->poll_lst_hd)
    {
        plt_mtx_ulck(poll_ctx->poll_mtx);
        return ZW_ERR_FAILED;
    }

    ent = poll_ctx->poll_lst_hd;

    while (ent)
    {
        rm_ent = ent;
        ent = ent->next;

        if (rm_ent->nodeId == nodeId)
        {
            zwpoll_rm_ent(poll_ctx, rm_ent);
        }
    }

    plt_mtx_ulck(poll_ctx->poll_mtx);
    return ZW_ERR_NONE;
}

/**
zwpoll_add - add a polling request into polling queue
@param[in]  net         network
@param[in]  poll_ent    polling request entry (allocated by caller). All unused fields must be zeroed.
@param[out] handle      Polling request handle
@return     ZW_ERR_NONE if success; else ZW_ERR_XXX on error
@post       If return error, caller must free poll_ent
*/
int zwpoll_add(zwnet_p net, poll_q_ent_t *poll_ent, uint16_t *handle)
{
    zwpoll_ctx_t *poll_ctx = net->poll_ctx;
    //int          result;
    int first_ent;

    if (poll_ent->poll_cnt > 0)
    {
        /* Non-repetitive poll*/
        uint32_t poll_intv;         //poll interval in seconds
        poll_ent->tot_poll_cnt = 1; //include the one which will be executed shortly after this

        //Adjust poll count for interval of less than 4 seconds to take into consideration of SSL delay
        //and coarse timer resolution
        poll_intv = poll_ent->interval / POLL_TICK_PER_SEC;
        if (poll_intv < 4)
        {
            switch (poll_intv)
            {
            case 1:
                poll_ent->poll_cnt += 3;
                break;

            case 2:
                poll_ent->poll_cnt += 2;
                break;

            case 3:
                poll_ent->poll_cnt++;
                break;
            }
        }
    }

    poll_ent->active_sts = (poll_ent->poll_cnt == 1) ? POLL_STS_INACTIVE_WAIT_RPT : POLL_STS_ACTIVE;


    plt_mtx_lck(poll_ctx->poll_mtx);

    *handle = poll_ent->handle = ++poll_ctx->handle_gen; //TODO: make sure it's unique

    first_ent = zwpoll_add_ent_endlist(poll_ctx, poll_ent);

    //Non Execute the get_report command. No callback required as this is not considered a poll.
    //zwif_exec_ex(&poll_ent->ifd);

    //Important: start timer only after the sending of first command as it may take some time
    //           to decrypt SSL Z/IP command by ZIPR
    poll_ent->next_poll_tm = poll_ent->interval + poll_ctx->tmr_tick;

    //debug_zwapi_ts_msg(&net->plt_ctx, "Poll entry added");

    if (first_ent)
    { //This is the only entry in the poll list
        poll_ctx->next_poll_tm = poll_ctx->tmr_tick + MIN_POLL_TIME;

        //Save the following for calculation of command time
        poll_ctx->cur_start_tm = poll_ctx->tmr_tick;
        poll_ctx->cur_cmd_tm = 0;
        poll_ctx->cur_cmd_cls = poll_ent->cmd_cls;
        poll_ctx->cur_rpt = poll_ent->rpt;
    }

    poll_ent->rpt_tout = POLL_RPT_TIMEOUT + poll_ctx->tmr_tick;
    poll_ent->wait_rpt = 0;

    plt_mtx_ulck(poll_ctx->poll_mtx);

    return ZW_ERR_NONE;
}

/**
zwpoll_evt_msg_hdlr - Process message loop on message arrival
@param[in]  usr_prm     user parameter supplied during initialization
@param[in]  msg         message
return      1 to exit message loop; else return 0 to continue the message loop
*/
static int zwpoll_evt_msg_hdlr(void *usr_prm, void *msg)
{
    zwpoll_ctx_t *poll_ctx = (zwpoll_ctx_t *)usr_prm;
    poll_evt_msg_t *evt_msg = (poll_evt_msg_t *)msg;

    if (poll_ctx->run)
    {
        if (evt_msg->type == POLL_EVT_TYPE_WAKEUP)
        {
            zwpoll_wkup_chk(poll_ctx, evt_msg->nodeid);
        }
        else if (evt_msg->type == POLL_EVT_TYPE_REPORT)
        {
            zwpoll_rpt_rcv_chk(poll_ctx, evt_msg);
        }
    }

    return 0;
}

/**
zwpoll_cb_msg_hdlr - Process message loop on message arrives
@param[in]  usr_prm     user parameter supplied during initialization
@param[in]  msg         message
return      1 to exit message loop; else return 0 to continue the message loop
*/
static int zwpoll_cb_msg_hdlr(void *usr_prm, void *msg)
{
    zwpoll_ctx_t *poll_ctx = (zwpoll_ctx_t *)usr_prm;
    poll_cb_msg_t *cb_msg = (poll_cb_msg_t *)msg;

    if (poll_ctx->run)
    {
        cb_msg->cmplt_cb(poll_ctx->net, cb_msg->handle, cb_msg->usr_token, cb_msg->usr_param);
    }

    return 0;
}

/**
zwpoll_init - Initialize the polling facility
@param[in]	poll_ctx	    Polling context
@return  0 on success; negative error number on failure
@note  Must call zwpoll_shutdown followed by zwpoll_exit to shutdown and clean up the polling facility
*/
int zwpoll_init(zwpoll_ctx_t *poll_ctx)
{
    int res;

    poll_ctx->run = 1;
    poll_ctx->tmr_tick = 0;
    poll_ctx->next_poll_tm = MIN_POLL_TIME;
    poll_ctx->poll_lst_hd = NULL;
    poll_ctx->poll_lst_tail = NULL;
    poll_ctx->handle_gen = 0;
    poll_ctx->poll_seq_num = 1; //Make sure it is always an odd number
    poll_ctx->cur_nodeId = 0;

    res = util_msg_loop_init(zwpoll_cb_msg_hdlr, poll_ctx, NULL, 3, &poll_ctx->cb_msg_loop_ctx);

    if (res != 0)
    {
        return res;
    }

    res = util_msg_loop_init(zwpoll_evt_msg_hdlr, poll_ctx, NULL, 4, &poll_ctx->evt_msg_loop_ctx);

    if (res != 0)
    {
        goto l_POLL_INIT_ERROR1;
    }

    if (!plt_mtx_init(&poll_ctx->poll_mtx))
        goto l_POLL_INIT_ERROR2;

    if (!plt_sem_init(&poll_ctx->tmr_sem))
    {
        goto l_POLL_INIT_ERROR3;
    }
    poll_ctx->tick_tmr_ctx = plt_periodic_start(&poll_ctx->net->plt_ctx, POLL_TIMER_TICK, zwpoll_tmr_tick_cb, poll_ctx);
    if (!poll_ctx->tick_tmr_ctx)
        goto l_POLL_INIT_ERROR4;

    //Start timer check thread
    poll_ctx->tmr_chk_thrd_run = 1;
    if (plt_thrd_create(zwpoll_tmr_chk_thrd, poll_ctx) < 0)
    {
        goto l_POLL_INIT_ERROR5;
    }

    return ZW_ERR_NONE;

l_POLL_INIT_ERROR5:
    plt_tmr_stop(&poll_ctx->net->plt_ctx, poll_ctx->tick_tmr_ctx);
l_POLL_INIT_ERROR4:
    plt_sem_destroy(poll_ctx->tmr_sem);
l_POLL_INIT_ERROR3:
    plt_mtx_destroy(poll_ctx->poll_mtx);
l_POLL_INIT_ERROR2:
    util_msg_loop_shutdown(poll_ctx->evt_msg_loop_ctx, 0);
    util_msg_loop_exit(poll_ctx->evt_msg_loop_ctx);
l_POLL_INIT_ERROR1:
    util_msg_loop_shutdown(poll_ctx->cb_msg_loop_ctx, 0);
    util_msg_loop_exit(poll_ctx->cb_msg_loop_ctx);
    return ZW_ERR_NO_RES;
}

/**
zwpoll_shutdown - Shutdown the polling facility
@param[in]  poll_ctx        Polling context
@return
*/
void zwpoll_shutdown(zwpoll_ctx_t *poll_ctx)
{
    if (ZWAVE_INITED_POLLING_LIST == getInitPollingDev())
    {
        poll_ctx->run = 0;
        util_msg_loop_shutdown(poll_ctx->cb_msg_loop_ctx, 0);
        util_msg_loop_shutdown(poll_ctx->evt_msg_loop_ctx, 0);

        //Stop timer
        plt_tmr_stop(&poll_ctx->net->plt_ctx, poll_ctx->tick_tmr_ctx);

        //Stop thread
        poll_ctx->tmr_chk_thrd_run = 0;
        plt_sem_post(poll_ctx->tmr_sem);
        //Stop the timer tick thread
        (&zwaveNetworkPolling->plt_ctx)->tmr_chk_thrd_run = 0;
        (&zwaveNetworkPolling->plt_ctx)->tmr_tck_thrd_run = 0;
        while ((&zwaveNetworkPolling->plt_ctx)->tmr_tck_thrd_sts)
        {
            plt_sleep(100);
        }
        while (poll_ctx->tmr_chk_thrd_sts)
        {
            plt_sleep(100);
        }
        setInitPollingDev(ZWAVE_NO_INIT_POLLING_LIST);
        plt_sleep(20); //delay 20ms to give timer thread enough time to terminate and clean up
    }
    removeAllPollingDevList();
}

/**
zwpoll_exit - Clean up
@param[in]  poll_ctx        Polling context
@return
*/
void zwpoll_exit(zwpoll_ctx_t *poll_ctx)
{
    if (ZWAVE_INITED_POLLING_LIST == getInitPollingDev())
    {
        util_msg_loop_exit(poll_ctx->cb_msg_loop_ctx);
        util_msg_loop_exit(poll_ctx->evt_msg_loop_ctx);

        //Flush the poll queue
        zwpoll_rm_all(poll_ctx);

        plt_sem_destroy(poll_ctx->tmr_sem);
        plt_mtx_destroy(poll_ctx->poll_mtx);
    }
}
