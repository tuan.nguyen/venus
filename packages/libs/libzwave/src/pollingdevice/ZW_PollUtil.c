
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include <ctype.h>
#include <unistd.h>

#include "ZW_PollPlatform.h"
#include "ZW_PollUtil.h"


/**
util_list_add - add an entry into the end of the list.
@param[in]      mtx_ctx     Mutex context; optional, if NULL no locking will be used.
@param[in,out]	head		Pointer to the list head
@param[in]      buf         Buffer that store the data
@param[in]      dat_sz      Size of data to be stored
@return                     Return 0 on success, negative error number on failure.
*/
int util_list_add(struct plt_mtx_t *mtx_ctx, util_lst_t **head, uint8_t  *buf, uint16_t dat_sz)
{
    util_lst_t   *ent;     //Pointer to list entry
    util_lst_t   *temp;

    ent = (util_lst_t   *)malloc(sizeof(util_lst_t) + dat_sz);

    if (!ent)
    {
        return ZWHCI_ERROR_MEMORY;
    }

    ent->dat_sz = dat_sz;
    memcpy(ent->wr_buf, buf, dat_sz);

    if (mtx_ctx)
    {
        plt_mtx_lck(mtx_ctx);
    }

    if (*head == NULL)
    {
        ent->next = NULL;
        *head = ent;
        if (mtx_ctx)
        {
            plt_mtx_ulck(mtx_ctx);
        }
        return ZWHCI_NO_ERROR;
    }

    temp = *head;
    while (temp->next)
    {
        temp = temp->next;
    }

    temp->next = ent;
    ent->next = NULL;

    if (mtx_ctx)
    {
        plt_mtx_ulck(mtx_ctx);
    }
    return ZWHCI_NO_ERROR;
}



/**
util_list_get - get the entry from the beginning of the list.
@param[in]      mtx_ctx     Mutex context; optional, if NULL no locking will be used.
@param[in, out]	head		List head
@return     The first entry in the list if the list is not empty; otherwise, NULL.
@post       The caller should free the returned entry.
*/
util_lst_t *util_list_get(struct plt_mtx_t *mtx_ctx, util_lst_t **head)
{
    util_lst_t   *first_entry;  //The entry at the beginning of the list

    if (mtx_ctx)
    {
        plt_mtx_lck(mtx_ctx);
    }
    first_entry = *head;

    if (*head == NULL)
    {
        if (mtx_ctx)
        {
            plt_mtx_ulck(mtx_ctx);
        }
        return NULL;
    }

    *head = (*head)->next;
    if (mtx_ctx)
    {
        plt_mtx_ulck(mtx_ctx);
    }
    return first_entry;
}


/**
util_list_flush - flush the list.
@param[in]      mtx_ctx     Mutex context; optional, if NULL no locking will be used.
@param[in, out]	head		List head
@return
*/
void util_list_flush(struct plt_mtx_t *mtx_ctx, util_lst_t **head)
{
    util_lst_t   *first_entry;  //The entry at the beginning of the list
    util_lst_t   *del_entry;    //Entry to be deleted

    if (mtx_ctx)
    {
        plt_mtx_lck(mtx_ctx);
    }

    first_entry = *head;

    while (first_entry)
    {
        del_entry = first_entry;
        first_entry = first_entry->next;
        free(del_entry);
    }

    *head = NULL;
    if (mtx_ctx)
    {
        plt_mtx_ulck(mtx_ctx);
    }
}





/**
util_msg_loop_thrd - Thread to wait for message
@param[in]	ctx		Context
@return
*/
static void util_msg_loop_thrd(void *ctx)
{
    util_msg_loop_t     *msg_ctx = (util_msg_loop_t *)ctx;
    util_lst_t          *lst_ent;
    int                 exit_thrd;

    msg_ctx->thrd_sts = 1;


    while (1)
    {
        //Wait for a message
        plt_sem_wait(msg_ctx->sem);

        //Check whether to exit the thread
        if (msg_ctx->thrd_run == 0)
        {
            msg_ctx->thrd_sts = 0;
            return;
        }

        while ((lst_ent = util_list_get(msg_ctx->mtx, &msg_ctx->msg_hd)) != NULL)
        {
            exit_thrd = msg_ctx->cb(msg_ctx->usr_prm, (void *)lst_ent->wr_buf);

            if (msg_ctx->free_fn)
            {
                msg_ctx->free_fn((void *)lst_ent->wr_buf);
            }
            free(lst_ent);

            //Check whether to exit the thread
            if ((msg_ctx->thrd_run == 0) || exit_thrd)
            {
                msg_ctx->thrd_sts = 0;
                return;
            }
        }
    }
}


/**
util_msg_loop_init - Initialize message loop
@param[in]  cb      Callback function on message arrival
@param[in]  usr_prm	User supplied parameter which will be used on callback
@param[in]  free_fn	User supplied function to free the content of message. This is optional.
                    If not used, set it to NULL.
@param[in]  id	    Identifier for the message loop (for debug use)
@param[out] ctx     Context
@return     Return 0 on success, negative error number on failure.
@post       Caller should call util_msg_loop_shutdown() to stop the message loop and util_msg_loop_exit()
            to free the allocated memory
*/
int util_msg_loop_init(util_msg_loop_fn cb, void *usr_prm, util_msg_free_fn free_fn, unsigned id, util_msg_loop_t **ctx)
{
    util_msg_loop_t     *msg_ctx;
    int                 wait_count;

    msg_ctx = calloc(1, sizeof(util_msg_loop_t));

    if (!msg_ctx)
    {
        return ZWHCI_ERROR_MEMORY;
    }

    msg_ctx->cb = cb;
    msg_ctx->usr_prm = usr_prm;
    msg_ctx->free_fn = free_fn;
    msg_ctx->id = id;

    if (!plt_sem_init(&msg_ctx->sem))
    {
        goto l_MSG_INIT_ERROR1;
    }

    if (!plt_mtx_init(&msg_ctx->mtx))
    {
        goto l_MSG_INIT_ERROR2;
    }

    msg_ctx->thrd_run = 1;
    if (plt_thrd_create(util_msg_loop_thrd, msg_ctx) < 0)
    {
        goto l_MSG_INIT_ERROR3;
    }
    //Wait for the thread to be ready
    wait_count = 20;
    while (wait_count-- > 0)
    {
        if (msg_ctx->thrd_sts)
            break;
        plt_sleep(100);
    }

    //ok
    *ctx = msg_ctx;
    return 0;

l_MSG_INIT_ERROR3:
    plt_mtx_destroy(msg_ctx->mtx);
l_MSG_INIT_ERROR2:
    plt_sem_destroy(msg_ctx->sem);
l_MSG_INIT_ERROR1:
    free(msg_ctx);
    return  ZWHCI_ERROR_RESOURCE;

}



/**
util_msg_loop_shutdown - Stop the message loop
@param[in]  msg_ctx Context
@param[in]  tm_out	Timeout (in milliseconds) to wait for message loop to stop. If zero, no timeout.
@return     Return 0 on success, negative error number on failure.
*/
int util_msg_loop_shutdown(util_msg_loop_t *msg_ctx, uint32_t tm_out)
{
    int res = ZWHCI_ERROR_TIMEOUT;

    msg_ctx->thrd_run = 0;
    plt_sem_post(msg_ctx->sem);

    //Wait for thread to exit
    if (tm_out == 0)
    {
        while (1)
        {
            if (msg_ctx->thrd_sts == 0)
            {
                res = 0;
                break;
            }
            plt_sleep(100);
        }
    }
    else
    {
        int wait_count;

        wait_count = tm_out/100;
        while (wait_count-- > 0)
        {
            if (msg_ctx->thrd_sts == 0)
            {
                res = 0;
                break;
            }
            plt_sleep(100);
        }
    }

    return res;
}



/**
util_msg_loop_exit - Free the allocated memory
@param[in]  msg_ctx Context
@return
@pre  Should call util_msg_loop_shutdown() first, then wait for the appropriate time to call this function safely
@post Do not call util_msg_loop_send() after calling this function
*/
void util_msg_loop_exit(util_msg_loop_t *msg_ctx)
{
    if (msg_ctx->free_fn)
    {   //Free one entry at a time
        util_lst_t  *lst_ent;
        while ((lst_ent = util_list_get(msg_ctx->mtx, &msg_ctx->msg_hd)) != NULL)
        {
            msg_ctx->free_fn((void *)lst_ent->wr_buf);
            free(lst_ent);
        }
    }
    else
    {
        util_list_flush(msg_ctx->mtx, &msg_ctx->msg_hd);
    }

    plt_sem_destroy(msg_ctx->sem);
    plt_mtx_destroy(msg_ctx->mtx);
    free(msg_ctx);
}



/**
util_msg_loop_send - Send a message to message loop
@param[in]  msg_ctx Context
@param[in]  msg	    Message
@param[in]  msg_sz  Message size
@return     Return 0 on success, negative error number on failure.
*/
int util_msg_loop_send(util_msg_loop_t *msg_ctx, void *msg, uint8_t msg_sz)
{
    int res;

    if (msg_ctx->thrd_sts == 0)
    {
        return ZWHCI_ERROR_WRITE;
    }

    res = util_list_add(msg_ctx->mtx, &msg_ctx->msg_hd, (uint8_t *)msg, msg_sz);

    if (res == 0)
    {
        plt_sem_post(msg_ctx->sem);
    }

    return res;
}




/**
util_tmr_exp_chk32 - Check whether the 32-bit timer has expired
@param[in] now     Current time
@param[in] timer   The time of the timer to check
@return 1=timer has expired; otherwise 0
*/
int util_tmr_exp_chk32(uint32_t now, uint32_t timer)
{
    uint32_t    time_diff;
    if (now == timer)
    {
        return 1;
    }

    //Handle wrap over case
    if (now > timer)
    {
        time_diff = now - timer;
        if (time_diff < 0xF0000000)
        {
            return 1;
        }
    }
    else //now < timer
    {
        time_diff = timer - now;
        if (time_diff >= 0xF0000000)
        {
            return 1;
        }
    }
    //Not expired
    return 0;
}

/**
util_time_diff32 - Calculate 32-bit time difference (i.e. tm1 - tm2) in terms of timer ticks
@param[in] tm1     Time 1
@param[in] tm2     Time 2
@return time different in terms of timer tick
*/
uint32_t   util_time_diff32(uint32_t tm1, uint32_t tm2)
{
    uint32_t    time_diff;
    uint32_t    temp;

    if (tm1 == tm2)
    {
        return 0;
    }

    if (tm1 < tm2)
    {   //swap
        temp = tm1;
        tm1 = tm2;
        tm2 = temp;
    }

    time_diff = tm1 - tm2;
    if (time_diff >= 0xF0000000)
    {   //wrap over
        time_diff = (0xFFFFFFFF - tm1) + 1 + tm2;
    }
    return time_diff;

}


















