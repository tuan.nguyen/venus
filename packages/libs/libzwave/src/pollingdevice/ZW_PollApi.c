
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "ZW_PollApi.h"
#include "ZW_DevicePoll.h"
#include "ZW_Poll.h"
#include "zw_serialapi/zw_classcmd.h"
#include "zw_serialapi/zw_serialapi.h"
#include "utils.h"
#include "nvm.h"
#include "resource_directory.h"
#include "ZW_SendDataAppl.h"
#include "resource_directory.h"//get/set no polling failed

extern zwnet_p zwaveNetworkPolling;
extern bool waitingBasicReport;
uint8_t initPollingDev;
bool networkBusy = false;
pthread_mutex_t handlePollingList;
pollingZwaveDevList_t globalPollingZwaveDevList;
uint8_t tempLog;
POLLING_DEV_FSM_STATE_T stateMainThreadOperation = ZWNET_OP_MAIN_THREAD_NONE;
POLLING_DEV_FSM_STATE_T stateWakeupThreadOperation = ZWNET_OP_WAKEUP_THREAD_NONE;

uint8_t getNoPollingFail(uint16_t extendId)
{
    if(!extendId)
    {
        return 0;
    }

    pollingZwaveDevList_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &(globalPollingZwaveDevList.list), list)
    {
        if(tmp->extendId == extendId)
        {
            return tmp->noPollingFailed;
        }
    }

    return 0;
}
uint8_t setNoPollingFail(uint16_t extendId, uint8_t value)
{
    if(!extendId)
    {
        return 0;
    }

    pollingZwaveDevList_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &(globalPollingZwaveDevList.list), list)
    {
        if(tmp->extendId == extendId)
        {
            tmp->noPollingFailed = value;
        }
    }

    return 0;
}

/**
zwobj_add - Add object to list
@param[in,out]  head    list head
@param[in]      obj     object
*/
void zwobj_add(zwobj_p *head, zwobj_p obj)
{
    zwobj_p temp;

    if (*head == NULL)
    {
        obj->next = NULL;
        *head = obj;
        return;
    }

    temp = *head;
    while (temp->next)
    {
        temp = temp->next;
    }

    temp->next = obj;
    obj->next = NULL;

}

/**
zwnode_find - find a node based on node id
@param[in]  first_node  first node in the network
@param[in]  nodeid      node id
@return     node if found; else return NULL
@pre        Caller must lock the zwaveNetworkPolling->mtx before calling this function.
*/
zwnode_p zwnode_find(zwnode_p first_node, uint8_t nodeid)
{
    zwnode_p    temp_node;

    temp_node = first_node;
    while (temp_node)
    {
        if (temp_node->nodeid == nodeid)
        {
            return temp_node;
        }
        temp_node = (zwnode_p)temp_node->obj.next;
    }
    return  NULL;
}

/*============================ send Basic Get Callback ======================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void sendBasicGetCallback(uint8_t status, uint8_t *buf, uint8_t len)
{
    uint8_t nodeId = zwaveNetworkPolling->poll_ctx->cur_nodeId;
    uint8_t endpoint = zwaveNetworkPolling->poll_ctx->cur_endpoint;
    uint16_t extendId = (endpoint << 8) + nodeId;
    gLogLevel = tempLog;
    if (len > 1)
    {
        NOTIFY_TX_BUFFER_T pTxNotify;
        pTxNotify.CmdClassNotify.source_endpoint = endpoint;
        pTxNotify.CmdClassNotify.dest_endpoint = 0;
        pTxNotify.CmdClassNotify.source_nodeId = nodeId;
        pTxNotify.CmdClassNotify.dest_nodeId = MyNodeId;
        pTxNotify.CmdClassNotify.node_type = GetCacheEntryNodeTypeGeneric(nodeId);
        pTxNotify.CmdClassNotify.node_is_listening = GetCacheEntryNodeMode(nodeId);
        pTxNotify.CmdClassNotify.pollingReport = 1;
        switch (buf[0])
        {
            case COMMAND_CLASS_BASIC:
            {
                switch (buf[1])
                {
                case BASIC_REPORT:
                {
                    pTxNotify.CmdClassNotify.cmd_class = COMMAND_CLASS_BASIC;
                    pTxNotify.CmdClassNotify.cmd = BASIC_REPORT;

                    if (len == 3)
                    {
                        pTxNotify.CmdClassNotify.version = 1;
                        pTxNotify.CmdClassNotify.basic_report_v1.value = buf[2];
                    }
                    else if (len > 3)
                    {
                        pTxNotify.CmdClassNotify.version = 2;
                        pTxNotify.CmdClassNotify.basic_report_v2.value = buf[2];
                        pTxNotify.CmdClassNotify.basic_report_v2.target_value = buf[3];
                        pTxNotify.CmdClassNotify.basic_report_v2.duration = buf[4];
                    }

                    PushNotificationToHandler(CMD_CLASS_ZPC_NOTIFY, (uint8_t *)&pTxNotify.CmdClassNotify, sizeof(cmd_class_notify_t));
                }
                break;
                }
            break;
            }
        }
    }   
    else
    {
        setNoPollingFail(extendId, getNoPollingFail(extendId) + 1);
        mainlog(logPolling, "Polling Node: 0x%04X, Current Polling Failed Number: 0x%02X", extendId, getNoPollingFail(extendId));
    }
    waitingBasicReport = false;
}
/**
zwif_exec_ex - execute action on an interface with extra parameters
@param[in]  ifd         interface
@param[in]  cmd_buf     command and parameters
@param[in]  buf_len     length of cmd_buf in bytes
@param[in]  cb          callback function for transmit status
@param[in]  user        user parameter of callback function
@param[in]  opt         option, ZWIF_OPT_XXX
@param[in]  xtra        extra parameter
@return ZW_ERR_xxx
*/
int zwif_exec_ex(zwifd_p ifd)
{
    mainlog(logPolling, "Send request polling, ifd->nodeid: 0x%02X (%02d), ifd->epid: 0x%02X", ifd->nodeid, ifd->nodeid, ifd->epid);

    uint8_t             result;
    zwnode_p            node;
    zwnet_p             zwaveNetworkPolling = ifd->net;

    //Initialize command to the original input command

    plt_mtx_lck(zwaveNetworkPolling->mtx);
    node = zwnode_find(&zwaveNetworkPolling->ctl, ifd->nodeid);
    if (!node)
    {
         plt_mtx_ulck(zwaveNetworkPolling->mtx);
         return ZW_ERR_NODE_NOT_FOUND;
    }
    plt_mtx_ulck(zwaveNetworkPolling->mtx);
    //-------------------------------------------------
    // Send the command get status
    //-------------------------------------------------
    tempLog = gLogLevel;
    if(gLogLevel < logPolling)
    {
        gLogLevel = logUI;
    }
    ts_param_t p;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = AUTO_SCHEME;

    p.snode = MyNodeId;
    p.sendpoint = 0;

    p.dnode = ifd->nodeid;
    p.dendpoint = ifd->epid;
    switch(ifd->cls)
    {
        case COMMAND_CLASS_BASIC:
        pTxBuf.ZW_ZwaveplusInfoGetFrame.cmdClass = COMMAND_CLASS_BASIC; /* The command class */
        pTxBuf.ZW_ZwaveplusInfoGetFrame.cmd = BASIC_GET;                /* The command */
        result = ZW_SendRequestAppl(&p, (ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                           sizeof(ZW_BASIC_GET_FRAME),
                           BASIC_REPORT,
                           5000,                                   /*timeout 5 seconds */
                           sendBasicGetCallback); //waiting S2CmdSupportedGetCallback 10000 ms
        break;
        default:
        mainlog(logDebug, "Not found find commandClass: 0x%04X", ifd->cls);
        break;
    }

    return result;

}


int zwnet_init(zwnet_p *net)//init memory
{
    zwnet_p zwaveNetworkPolling;
    int result;

    //Allocate memory for network internal structure
    zwaveNetworkPolling = (zwnet_p) calloc(1, sizeof(zwnet_t));
    if (!zwaveNetworkPolling)
        return ZW_ERR_MEMORY;


    //Set default values
    zwnode_ctl_clr(zwaveNetworkPolling);


    if (plt_init(&zwaveNetworkPolling->plt_ctx) != 0)
    {
        mainlog(logDebug, "Failed to calloc for plt_init");
        goto l_ZWNET_INI_ERR15;
    }


    if (!plt_mtx_init(&zwaveNetworkPolling->mtx))
    {
        mainlog(logDebug, "Failed to calloc for plt_mtx_init");
        goto l_ZWNET_INI_ERR15;
    }

    //Initialize poll facility
    zwaveNetworkPolling->poll_ctx = (zwpoll_ctx_t *)calloc(1, sizeof(zwpoll_ctx_t));

    if (!zwaveNetworkPolling->poll_ctx)
    {
        result = ZW_ERR_MEMORY;
        goto l_ZWNET_INI_ERR15;
    }

    zwaveNetworkPolling->poll_ctx->net = zwaveNetworkPolling;
    result = zwpoll_init(zwaveNetworkPolling->poll_ctx);
    if (result != 0)
    {
        mainlog(logDebug, "Init poll facility failed:%d", result);
        goto l_ZWNET_INI_ERR15;
    }
    zwaveNetworkPolling->poll_enable = true;
    initPollingDev = ZWAVE_INITED_POLLING_LIST;
    *net = zwaveNetworkPolling;
    return ZW_ERR_NONE;

l_ZWNET_INI_ERR15:
    //zwnet_exit(zwaveNetworkPolling);
    return result;

}

/**
zwif_get_report_poll - get interface report through report callback
@param[in]  ifd         interface
@param[in]  param       Parameter for the report get command
@param[in]  len         Length of param
@param[in]  get_rpt_cmd Command to get the report
@param[in, out] poll_req Poll request
@return     ZW_ERR_xxx
*/
int zwif_get_report_poll(zwifd_p ifd, zwpoll_req_t *poll_req)
{
    zwnode_p        node;
    zwnet_p         zwaveNetworkPolling = ifd->net;
    int             result;
    poll_q_ent_t    *poll_ent;
    uint16_t        handle;

    //Check if the parameter length is too long

    mainlog(logDebug, "zwif_get_report_poll");

    plt_mtx_lck(zwaveNetworkPolling->mtx);
    node = zwnode_find(&zwaveNetworkPolling->ctl, ifd->nodeid);//find ifd->nodeid into nodeList
    if (!node)
    {
        mainlog(logDebug, "Don't find nodeid 0x%02X into nodeList!!!!", ifd->nodeid);
        plt_mtx_ulck(zwaveNetworkPolling->mtx);
        return ZW_ERR_NODE_NOT_FOUND;
    }
    plt_mtx_ulck(zwaveNetworkPolling->mtx);

    poll_ent = (poll_q_ent_t *)calloc(1, sizeof(poll_q_ent_t));

    if (!poll_ent)
    {
        return ZW_ERR_MEMORY;
    }

    //Check for extended command class (2-byte command class)

    poll_ent->interval = (poll_req->interval == 0)? MIN_POLL_TIME : (poll_req->interval * POLL_TICK_PER_SEC);
    poll_ent->poll_cnt = poll_req->poll_cnt;
    poll_ent->ifd = *ifd;
    poll_ent->nodeId = ifd->nodeid;

    result = zwpoll_add(zwaveNetworkPolling, poll_ent, &handle);

    if (result == ZW_ERR_NONE)
    {
        //Save the polling handle
        poll_req->handle = handle;
    }
    else
    {
        mainlog(logDebug, "zwif_get_report_poll with error:%d", result);
        free(poll_ent);
    }

    return result;
}

/**
 * @brief       Function to perform add node to polling list
 * @param[in]   nodeId          Node id
 * @param[in]   endpoint        EP id
 * @param[in]   pollingInterval       polling interval
 * @return
 */
int addDeviceToPollingList(uint8_t nodeId, uint8_t endpoint, uint16_t pollingInterval, uint16_t commandClass)
{
    int result = 0;
    uint16_t extendId = (endpoint << 8) + nodeId;
    if(!nodeId)
        return ZWAVE_NO_NODE;//node Id == 0, no exist
    if(ZWAVE_INITED_POLLING_LIST == getInitPollingDev())
    {
        zwifd_t         if_desc;
        zwpoll_req_t    zw_poll_req = {0};
        zwnet_p         zwnet;

        pthread_mutex_lock(&handlePollingList);
        pollingZwaveDevList_t *tmp = NULL;
        
        VR_(list_for_each_entry)(tmp, &(globalPollingZwaveDevList.list), list)
        {
            if(tmp->extendId == extendId)
            {
                mainlog(logDebug, "Found extendId: 0x%04X into nodeList!!!!", extendId);
                result = ZWAVE_EXTEND_ID_EXIST;
                break;
            }
        }

        if(!result)
        {
            pollingZwaveDevList_t* tempExtendId = (pollingZwaveDevList_t*)malloc(sizeof(pollingZwaveDevList_t));
            memset(tempExtendId, 0x00, sizeof(pollingZwaveDevList_t));
            tempExtendId->noPollingFailed = 0;
            tempExtendId->extendId = extendId;

            VR_(list_add_tail)(&(tempExtendId->list), &(globalPollingZwaveDevList.list));
            pthread_mutex_unlock(&handlePollingList);

            zwnet = zwaveNetworkPolling;//globle variable 

            mainlog(logDebug, "addDeviceToPollingList -> node id = 0x%02X (%02u), endpoint = 0x%02X", nodeId, nodeId, endpoint);
            register_node_to_polling_list(nodeId);
            if_desc.nodeid = nodeId;
            if_desc.epid = endpoint;
            if_desc.cls = commandClass;
            if_desc.net = zwnet;

            zw_poll_req.interval = pollingInterval;
            zw_poll_req.poll_cnt = ZWAVE_POLLING_COUNT;

            //Initialize return code
            result = zwif_get_report_poll(&if_desc, &zw_poll_req);
            /*Set no polling failed is 0*/
            setNoPollingFail(extendId, 0);
            return result;
        }
        pthread_mutex_unlock(&handlePollingList);
    }
    return result;
}
/**
 * @brief       Function to perform remove node from polling list
 * @param[in]   nodeId     Node id
 * @return
 */
int removeDeviceFromPollingList(uint8_t nodeId)
{
    if(!getPollingDevListFromExtendId(nodeId))
        return ZWAVE_NO_NODE;//no exist into polling list
    if(ZWAVE_INITED_POLLING_LIST == getInitPollingDev())
    {
        mainlog(logDebug, "remove Device From Polling List -> node id = 0x%02X (%02u)",nodeId , nodeId);
        //removePollingDevListFromNodeId(nodeId);
        //if device is added to PollingDevList
        //it will be removed when device is removed from network
        return zwavepoll_rm(zwaveNetworkPolling, nodeId);
    }
    return 0;
}

/**
 * @brief       Function to perform register node to polling list
 *              Register node, after user can polling this node
 * @param[in]   nodeId     Node id
 * @return
 */
int register_node_to_polling_list(uint8_t nodeId)
{
    plt_mtx_lck(zwaveNetworkPolling->mtx);
    if (nodeId != zwaveNetworkPolling->ctl.nodeid)
    {
        //Register a new node
        zwnode_p node = (zwnode_p)calloc(1, sizeof(zwnode_t));
        if (!node)
        {
            plt_mtx_ulck(zwaveNetworkPolling->mtx);
            return ZW_ERR_MEMORY;
        }
        //Init all the back links
        node->net = zwaveNetworkPolling;

        //Save node id
        node->nodeid = nodeId;

        zwobj_add(&zwaveNetworkPolling->ctl.obj.next, &node->obj);//add node to &zwaveNetworkPolling->ctl.obj
    }
    plt_mtx_ulck(zwaveNetworkPolling->mtx);
    return 0;
}

void setInitPollingDev(uint8_t value)
{
    initPollingDev = value;
}

uint8_t getInitPollingDev(void)
{
    return initPollingDev;
}

void setNWBusy(bool value)
{
    networkBusy = value;
}

void initPollingDevList(void)
{
    VR_INIT_LIST_HEAD(&globalPollingZwaveDevList.list);
    pthread_mutex_init(&handlePollingList, 0);
}

void removePollingDevListFromNodeId(uint8_t nodeId)
{
    mainlog(logDebug, "remove ExternId From List for nodeId: 0x%02X", nodeId);
    pthread_mutex_lock(&handlePollingList);
    pollingZwaveDevList_t *tmp = NULL;

    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &globalPollingZwaveDevList.list)
    {
        tmp = VR_(list_entry)(pos, pollingZwaveDevList_t, list);
        if((tmp->extendId & 0x00FF) == nodeId)
        {
            mainlog(logDebug, "remove ExternId From List for extendId: 0x%04X", tmp->extendId);
            VR_(list_del)(pos);
            free(tmp);
        }
    }
    pthread_mutex_unlock(&handlePollingList);
}

void removeAllPollingDevList(void)
{
    pthread_mutex_lock(&handlePollingList);
    pollingZwaveDevList_t *tmp = NULL;
    mainlog(logDebug, "remove All ExternId From List");
    struct VR_list_head *pos, *q;
    VR_(list_for_each_safe)(pos, q, &globalPollingZwaveDevList.list)
    {
        tmp = VR_(list_entry)(pos, pollingZwaveDevList_t, list);
        VR_(list_del)(pos);
        free(tmp);
    }
    pthread_mutex_unlock(&handlePollingList);
}
pollingZwaveDevList_t *getPollingDevListFromExtendId(uint8_t nodeId)
{
    if(!nodeId)
    {
        return NULL;
    }

    pollingZwaveDevList_t *tmp = NULL;
    
    VR_(list_for_each_entry)(tmp, &(globalPollingZwaveDevList.list), list)
    {
        if((tmp->extendId & 0x00FF) == nodeId)
        {
            return tmp;
        }
    }

    return NULL;
}

uint16_t getLastListFromPollingList(void)
{
    pollingZwaveDevList_t *tmp  = VR_list_entry(globalPollingZwaveDevList.list.prev, pollingZwaveDevList_t, list);

    return tmp->extendId;
}

//Change operation of polling device

typedef struct
{
    POLLING_DEV_FSM_STATE_T st;      // current state
    POLLING_DEV_FSM_EV_T ev;   // incoming event
    POLLING_DEV_FSM_STATE_T next_st; // next state
} transition_t;

static const transition_t transMainThreadOp[] =
{
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_GET_LIST, ZWNET_OP_GET_NODE_LIST},
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_SET_DEFAULT, ZWNET_OP_RESET},
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_SET_SUC_SIS_MODE, ZWNET_OP_ASSIGN},
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_REQUEST_UPDATE, ZWNET_OP_UPDATE},
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_REMOVE_FORCE, ZWNET_OP_RM_FAILED_ID},

    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_CONTROLLER_CHANGE, ZWNET_OP_MIGRATE_SUC},
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_NETWORK_TEST, ZWNET_OP_NETWORK_TEST},
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_REPLACE_FAILED_NODE, ZWNET_OP_RP_NODE},
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_IS_FAILED_NODE, ZWNET_OP_IS_FAILED_NODE},

    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},

    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_GET_CONTROLLER_INFO_ZPC, ZWNET_OP_GET_CTRL_INFO},
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_REQUEST_SPECIFICATION_ZPC, ZWNET_OP_SEND_SPEC_CMD},
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_FIRMWARE_UPDATE_MD, ZWNET_OP_FW_UPDT},

    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_REQUEST_NIF, ZWNET_OP_NODE_UPDATE},
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_PRIORITY_ROUTE, ZWNET_OP_PRIORITY_ROUTE},
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_GET_SECURE_KEY, ZWNET_OP_GET_SECURE_KEY},
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_UPDATE_FIRMWARE, ZWNET_OP_FW_UPDT_CTRL},
    {ZWNET_OP_MAIN_THREAD_NONE, EV_USER_GET_VERSION_FIRMWARE, ZWNET_OP_GET_FW_VERSION},

//-----------------------------------------------------------------------
    {ZWNET_OP_GET_NODE_LIST, EV_USER_GET_LIST_DONE, ZWNET_OP_MAIN_THREAD_NONE},
    {ZWNET_OP_RESET, EV_USER_SET_DEFAULT_DONE, ZWNET_OP_MAIN_THREAD_NONE},
    {ZWNET_OP_ASSIGN, EV_USER_SET_SUC_SIS_MODE_DONE, ZWNET_OP_MAIN_THREAD_NONE},
    {ZWNET_OP_UPDATE, EV_USER_REQUEST_UPDATE_DONE, ZWNET_OP_MAIN_THREAD_NONE},
    {ZWNET_OP_RM_FAILED_ID, EV_USER_REMOVE_FORCE_DONE, ZWNET_OP_MAIN_THREAD_NONE},

    {ZWNET_OP_MIGRATE_SUC, EV_USER_CONTROLLER_CHANGE_DONE, ZWNET_OP_MAIN_THREAD_NONE},
    {ZWNET_OP_NETWORK_TEST, EV_USER_NETWORK_TEST_DONE, ZWNET_OP_MAIN_THREAD_NONE},
    {ZWNET_OP_RP_NODE, EV_USER_REPLACE_FAILED_NODE_DONE, ZWNET_OP_MAIN_THREAD_NONE},
    {ZWNET_OP_IS_FAILED_NODE, EV_USER_IS_FAILED_NODE_DONE, ZWNET_OP_MAIN_THREAD_NONE},

    {ZWNET_OP_ADD_NODE, EV_USER_ADD_NODE_ZPC_DONE, ZWNET_OP_MAIN_THREAD_NONE},

    {ZWNET_OP_RM_NODE, EV_USER_REMOVE_NODE_ZPC_DONE, ZWNET_OP_MAIN_THREAD_NONE},
    {ZWNET_OP_ADD_NODE, EV_USER_REMOVE_NODE_ZPC_AUTO_DONE, ZWNET_OP_MAIN_THREAD_NONE},
    {ZWNET_OP_GET_CTRL_INFO, EV_USER_GET_CONTROLLER_INFO_ZPC_DONE, ZWNET_OP_MAIN_THREAD_NONE},
    {ZWNET_OP_SEND_SPEC_CMD, EV_USER_REQUEST_SPECIFICATION_ZPC_DONE, ZWNET_OP_MAIN_THREAD_NONE},
    {ZWNET_OP_FW_UPDT, EV_USER_FIRMWARE_UPDATE_MD_DONE, ZWNET_OP_MAIN_THREAD_NONE},

    {ZWNET_OP_NODE_UPDATE, EV_USER_REQUEST_NIF_DONE, ZWNET_OP_MAIN_THREAD_NONE},
    {ZWNET_OP_PRIORITY_ROUTE, EV_USER_PRIORITY_ROUTE_DONE, ZWNET_OP_MAIN_THREAD_NONE},
    {ZWNET_OP_GET_SECURE_KEY, EV_USER_GET_SECURE_KEY_DONE, ZWNET_OP_MAIN_THREAD_NONE},
    {ZWNET_OP_FW_UPDT_CTRL, EV_USER_UPDATE_FIRMWARE_DONE, ZWNET_OP_MAIN_THREAD_NONE},
    {ZWNET_OP_GET_FW_VERSION, EV_USER_GET_VERSION_FIRMWARE_DONE, ZWNET_OP_MAIN_THREAD_NONE},

//-----------------------------------------------------------------------
    {ZWNET_OP_GET_NODE_LIST, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_RESET, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_ASSIGN, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_UPDATE, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_RM_FAILED_ID, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},

    {ZWNET_OP_MIGRATE_SUC, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_NETWORK_TEST, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_RP_NODE, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_IS_FAILED_NODE, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},

    {ZWNET_OP_RM_NODE, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_GET_CTRL_INFO, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_SEND_SPEC_CMD, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_FW_UPDT, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},

    {ZWNET_OP_NODE_UPDATE, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_GET_SECURE_KEY, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_FW_UPDT_CTRL, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_GET_FW_VERSION, EV_USER_ADD_NODE_ZPC, ZWNET_OP_ADD_NODE},
//-----------------------------------------------------------------------
    {ZWNET_OP_GET_NODE_LIST, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
    {ZWNET_OP_RESET, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
    {ZWNET_OP_ASSIGN, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
    {ZWNET_OP_UPDATE, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
    {ZWNET_OP_RM_FAILED_ID, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},

    {ZWNET_OP_MIGRATE_SUC, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
    {ZWNET_OP_NETWORK_TEST, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
    {ZWNET_OP_RP_NODE, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
    {ZWNET_OP_IS_FAILED_NODE, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},

    //{ZWNET_OP_ADD_NODE, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
    {ZWNET_OP_RM_NODE, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
    {ZWNET_OP_GET_CTRL_INFO, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
    {ZWNET_OP_SEND_SPEC_CMD, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
    {ZWNET_OP_FW_UPDT, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},

    {ZWNET_OP_NODE_UPDATE, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
    {ZWNET_OP_GET_SECURE_KEY, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
    {ZWNET_OP_FW_UPDT_CTRL, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
    {ZWNET_OP_GET_FW_VERSION, EV_USER_REMOVE_NODE_ZPC, ZWNET_OP_RM_NODE},
//-----------------------------------------------------------------------
    {ZWNET_OP_GET_NODE_LIST, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_RESET, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_ASSIGN, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_UPDATE, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_RM_FAILED_ID, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},

    {ZWNET_OP_MIGRATE_SUC, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_NETWORK_TEST, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_RP_NODE, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_IS_FAILED_NODE, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},

    {ZWNET_OP_ADD_NODE, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},

    {ZWNET_OP_RM_NODE, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_GET_CTRL_INFO, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_SEND_SPEC_CMD, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_FW_UPDT, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},

    {ZWNET_OP_NODE_UPDATE, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_GET_SECURE_KEY, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_FW_UPDT_CTRL, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},
    {ZWNET_OP_GET_FW_VERSION, EV_USER_REMOVE_NODE_ZPC_AUTO, ZWNET_OP_ADD_NODE},
};
#define TRANS_COUNT_MAIN_THREAD (sizeof(transMainThreadOp) / sizeof(*transMainThreadOp))

static const transition_t transWakeupThreadOp[] =
{
    {ZWNET_OP_WAKEUP_THREAD_NONE, EV_LIB_HANDLE_WAKEUP_FIRMWARE, ZWNET_OP_HANDLE_WAKEUP},
//-------------------------------------------------------------------------
    {ZWNET_OP_HANDLE_WAKEUP, EV_LIB_HANDLE_WAKEUP_DONE, ZWNET_OP_WAKEUP_THREAD_NONE},
};
#define TRANS_COUNT_WAKEUP_THREAD (sizeof(transWakeupThreadOp) / sizeof(*transWakeupThreadOp))

void postEventOperationPollingDev(POLLING_DEV_FSM_EV_T event)
{
    POLLING_DEV_FSM_STATE_T old_state;
    uint8_t i;
    if(ZWAVE_INITED_POLLING_LIST != getInitPollingDev())
        return;
    for (i = 0; i < TRANS_COUNT_MAIN_THREAD; i++)
    {
        if ((stateMainThreadOperation == transMainThreadOp[i].st)) //have state none support
        {
            if ((event == transMainThreadOp[i].ev))
            {
                old_state = transMainThreadOp[i].st;

                stateMainThreadOperation = transMainThreadOp[i].next_st;
                mainlog(logPolling, "Polling Z-Wave device, current nw state: %s --> new nw state : %s",
                get_nwstr(old_state), get_nwstr(stateMainThreadOperation));

                zwaveNetworkPolling->poll_ctx->next_poll_tm = zwaveNetworkPolling->poll_ctx->tmr_tick + ZWAVE_POLLING_FREE_TIME;

                break;
            }
        }
    }
    for (i = 0; i < TRANS_COUNT_WAKEUP_THREAD; i++)
    {
        if ((stateWakeupThreadOperation == transWakeupThreadOp[i].st)) //have state none support
        {
            if ((event == transWakeupThreadOp[i].ev))
            {
                old_state = transWakeupThreadOp[i].st;

                stateWakeupThreadOperation = transWakeupThreadOp[i].next_st;
                mainlog(logPolling, "Polling Z-Wave device, current nw state: %s --> new nw state : %s",
                get_nwstr(old_state), get_nwstr(stateWakeupThreadOperation));

                zwaveNetworkPolling->poll_ctx->next_poll_tm = zwaveNetworkPolling->poll_ctx->tmr_tick + ZWAVE_POLLING_FREE_TIME;

                break;
            }
        }
    }
    if((ZWNET_OP_MAIN_THREAD_NONE == stateMainThreadOperation) && (ZWNET_OP_WAKEUP_THREAD_NONE == stateWakeupThreadOperation))
    {
        zwaveNetworkPolling->curr_op = ZWNET_OP_NONE;
    }
    else
    {
        zwaveNetworkPolling->curr_op = stateMainThreadOperation;
    }
}