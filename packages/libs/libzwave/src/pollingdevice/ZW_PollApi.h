
#ifndef _ZW_POLL_API_H_
#define _ZW_POLL_API_H_


#include <stdint.h>
#include <stdbool.h>
#include "ZW_PollUtil.h"
#include "ZW_PollPlatform.h"
#include "VR_define.h"
#include "VR_list.h"

/** Options for ZINNO device */

#define ZWAVE_POLLING_INTERVAL  10      /**< Polling interval in terms of timer tick*/
#define ZWAVE_POLLING_COUNT     0       /**< Number of times to poll; zero = unlimited times (repetitive poll)*/
#define ZWAVE_POLLING_FREE_TIME 60     /**< After ZWAVE_POLLING_FREE_TIME /2 (second) when user complete our command,*/
                                        /*    libzwave will auto polling device*/
#define ZWAVE_MAX_POLLING_FAILED 3       /**< Maximum No Polling Failed*/
#define ZWAVE_NO_NODE            1
#define ZWAVE_EXTEND_ID_EXIST    1

#define ZWAVE_NO_INIT_POLLING_LIST  255
#define ZWAVE_INITED_POLLING_LIST   1
#define ZWAVE_NON_POLLING_DEV   0
#define ZWAVE_TIME_STOP_POLLING   20      //stop polling 10s after polling all device

/** Z-Wave base object for linking and user context */
typedef struct _zwobj
{
    struct _zwobj   *next;      /**< next object in list */
    void            *ctx;       /**< user context (opaque to server) */
}
zwobj_t, *zwobj_p;



struct _zwep;
struct _zwif;

typedef struct _zwif    *zwif_p;        /**< interface handle */
typedef struct _zwep    *zwep_p;        /**< endpoint handle */

struct _zwnet;
typedef struct _zwnet   *zwnet_p;       /**< network handle */

struct _zwnode;
typedef struct _zwnode  *zwnode_p;      /**< node handle */

struct  _zwnoded;
typedef struct _zwnoded     *zwnoded_p;     /**<Node descriptor */

struct  _zwifd;
typedef struct _zwifd       *zwifd_p;       /**<Interface descriptor */


typedef void (*zwpoll_cmplt_fn)(zwnet_p net, uint16_t handle, uint32_t usr_token, void *usr_param);
/**<
callback on polling completion
@param[in]  net         network
@param[in]  handle      handle of the polling request
@param[in]  usr_token   user defined token to facilitate deletion of multiple polling requests
@param[in]  usr_param   user parameter
*/



/** interface descriptor */
typedef struct _zwifd
{
    uint16_t    cls;            /**< Command class */
    uint8_t     epid;           /**< Endpoint ID */
    uint8_t     nodeid;         /**< Node ID */
    zwnet_p     net;            /**< Network that the node belongs to */
}
zwifd_t;


/** Polling request */
typedef struct
{   /* Input*/
    uint32_t                usr_token;      /**< User defined token to facilitate deletion of multiple polling requests */
    uint16_t                interval;       /**< Polling interval in seconds; zero = the smallest possible interval*/
    uint16_t                poll_cnt;       /**< Number of times to poll; zero = unlimited times (i.e. repetitive polling)*/
    zwpoll_cmplt_fn         cmplt_cb;       /**< Polling completion callback. NULL if callback is not required*/
    void                    *usr_param;     /**< User parameter of polling completion callback */

    /* Output*/
    uint16_t                handle;         /**< Handle if the request is accepted into the polling queue. The handle
                                                 can be used to facilitate deletion of the polling request */
}
zwpoll_req_t;


/** Z-Wave node abstraction */
typedef struct _zwnode
{
    zwobj_t         obj;        /**< base link object */
    struct _zwnet   *net;       /**< back link to associated network */

    /* Node information report */
    uint8_t     nodeid;         /**< Node ID */
    uint8_t     listen;         /**< Flag to indicate whether the node is always listening (awake) */

    /* Sleeping node (wake up command class)*/
    uint8_t     enable_cmd_q;   /**< Flag to enable command queuing */
    uint8_t     sleep_cap;      /**< Flag to indicate the node is capable to sleep (i.e. non-listening and support Wake up command class) */
    int32_t     wkup_intv;      /**< Wake up interval in seconds. Negative value = invalid or unknown */

}zwnode_t;


/** Network */
typedef struct _zwnet
{
    zwnode_t            ctl;            /**< controller device, head in device list */
    struct plt_mtx_t    *mtx;           /**< Mutex to access zwnet_t structure*/
    uint8_t             curr_op;        /**< Current executing operation*/

    uint8_t             poll_enable;    /**< Flag to indicate poll facility is enabled*/

    plt_ctx_t           plt_ctx;        /**< Platform context */
    struct _poll_ctx    *poll_ctx;      /**< Polling context */
}zwnet_t;





/** Clear controller variables */
#define zwnode_ctl_clr(nw)  do{ \
                                (nw)->ctl.nodeid = 0;           \
                                (nw)->ctl.sleep_cap = 0;        \
                                (nw)->ctl.listen = 1;           \
                                (nw)->ctl.enable_cmd_q = 0;     \
                                (nw)->ctl.wkup_intv = -1;       \
                              } while(0)


int zwnet_init(zwnet_p *net);
void zwnet_add_node(zwnet_p nw, uint8_t nodeId);
zwnode_p zwnode_find(zwnode_p first_node, uint8_t nodeid);

/**<
execute action on an interface with extra parameters
@param[in]  ifd         interface
@param[in]  cmd_buf     command and parameters
@param[in]  buf_len     length of cmd_buf in bytes
@param[in]  cb          callback function for transmit status
@param[in]  user        user parameter of callback function
@param[in]  opt         option, ZWIF_OPT_XXX
@param[in]  xtra        extra parameter
@return ZW_ERR_xxx
*/
int zwif_exec_ex(zwifd_p ifd);

/**
 * @brief       Function to perform register node to polling list
 *              Register node, after user can polling this node
 * @param[in]   nodeId     Node id
 * @return
 */
int register_node_to_polling_list(uint8_t nodeId);


/**
 * @brief       Function to perform set value of initPollingDev 
 * @param[in]   value == true --> init success
 * @return
 */
void setInitPollingDev(uint8_t value);

/**
 * @brief       Function to perform get value of initPollingDev 

 * @return      initPollingDev
 */
uint8_t getInitPollingDev(void);

/**
 * @brief       Function to perform set network busy 

 */
void setNWBusy(bool value);

typedef struct pollingZwaveDevList
{
    uint8_t noPollingFailed;
    uint16_t extendId;//extendId = endpoint * 256 + nodeId
    struct VR_list_head list;
}pollingZwaveDevList_t;
void initPollingDevList(void);
void removePollingDevListFromNodeId(uint8_t nodeId);
void removeAllPollingDevList(void);
pollingZwaveDevList_t *getPollingDevListFromExtendId(uint8_t nodeId);
uint16_t getLastListFromPollingList(void);
uint8_t getNoPollingFail(uint16_t extendId);
uint8_t setNoPollingFail(uint16_t extendId, uint8_t value);
#endif