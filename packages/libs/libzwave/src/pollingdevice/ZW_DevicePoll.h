
#ifndef _ZW_DEVICE_POLL_H_
#define _ZW_DEVICE_POLL_H_

#include <stdint.h>
#include "ZW_PollPlatform.h"
#include "ZW_PollApi.h"
#include "zw_serialapi/zw_classcmd.h"

/** @name Zw_error_codes
* Z-Wave error code definition
@{
*/
#define ZW_ERR_NONE               0    /**< Operation succeeded */
#define ZW_ERR_FAILED            -1    /**< Operation failed */
#define ZW_ERR_MEMORY           -12    /**< Out of memory */
#define ZW_ERR_NODE_NOT_FOUND   -13    /**< Node not found*/
#define ZW_ERR_UNSUPPORTED      -27    /**< The requested function is unsupported for this node */
#define ZW_ERR_NO_RES           -34    /**< No resource for mutex, semaphore, timer, etc */

/** @name Network_operation
* Network operation definition
@{
*/
//States
typedef enum POLLING_DEV_FSM_STATES
{
	ZWNET_OP_NONE = 0,   		/**< No operation is executing*/
	ZWNET_OP_INITIALIZE,		/**< Initialization operation*/
	ZWNET_OP_ADD_NODE,			/**< Add node operation*/
	ZWNET_OP_RM_NODE,			/**< Remove node operation*/
	ZWNET_OP_RP_NODE,			/**< Replace failed node operation*/
	ZWNET_OP_RM_FAILED_ID,		/**< Remove failed node id operation*/
	ZWNET_OP_INITIATE,			/**< Initiation operation by controller*/
	ZWNET_OP_UPDATE,			/**< Update network topology from the SUC/SIS*/
	ZWNET_OP_RESET,				/**< Restore to factory default setting*/
	ZWNET_OP_MIGRATE_SUC,		/**< Create primary controller by a SUC*/
	ZWNET_OP_MIGRATE,			/**< Migrate primary controller operation*/
	ZWNET_OP_NETWORK_TEST,		/**< Network test*/
	ZWNET_OP_ASSIGN,			/**< assign or deassign SUC/SIS operation*/
	ZWNET_OP_NODE_UPDATE,		/**< Update node info*/
    ZWNET_OP_PRIORITY_ROUTE,    /**< Get/Set priority route info*/
	ZWNET_OP_SEND_NIF,			/**< Send node info frame*/
	ZWNET_OP_NW_CHANGED,		/**< Network change detection*/
	ZWNET_OP_NODE_CACHE_UPT,	/**< Update node cache info. (For internal use only)*/
	ZWNET_OP_SAVE_NW,			/**< Save network and node information to persistent storage. (For internal use only)*/
	ZWNET_OP_SLEEP_NODE_UPT,	/**< Update sleeping detailed node information when it is awake. (For internal use only)*/
	ZWNET_OP_FW_UPDT,			/**< Firmware update*/
	ZWNET_OP_IS_FAILED_NODE,	/**< Is Failed node*/
	ZWNET_OP_GET_CTRL_INFO,		/**< Get controller information*/
	ZWNET_OP_SEND_SPEC_CMD,		/**< Send specification command*/
	ZWNET_OP_GET_SECURE_KEY,	/**< Get secure key*/
	ZWNET_OP_FW_UPDT_CTRL,		/**< Update Firmware For Controller*/
	ZWNET_OP_GET_FW_VERSION,	/**< Get firmware version*/
	ZWNET_OP_GET_NODE_LIST,		/**< Get Node List*/
	ZWNET_OP_HANDLE_WAKEUP,		/**< Handle wake up notify and reset locally*/
	ZWNET_OP_MAIN_THREAD_NONE,	/**< Main thread No operation*/
	ZWNET_OP_WAKEUP_THREAD_NONE,/**< wake up notify and reset locally thread, no operation*/
	ZWNET_OP_WORKING,			/**< working*/

}POLLING_DEV_FSM_STATE_T;


//Events
typedef enum POLLING_DEV_FSM_EV
{
    EV_USER_GET_LIST = 0,

    EV_USER_SET_DEFAULT,
    EV_USER_SET_SUC_SIS_MODE,
    EV_USER_REQUEST_UPDATE,

    EV_USER_REMOVE_FORCE,
    EV_USER_CONTROLLER_CHANGE,
    EV_USER_NETWORK_TEST,

    EV_USER_REPLACE_FAILED_NODE,
    EV_USER_IS_FAILED_NODE,
    EV_USER_ADD_NODE_ZPC,

    EV_USER_REMOVE_NODE_ZPC,
    EV_USER_REMOVE_NODE_ZPC_AUTO,
    EV_USER_GET_CONTROLLER_INFO_ZPC,

    EV_USER_REQUEST_SPECIFICATION_ZPC,
    EV_USER_FIRMWARE_UPDATE_MD,
    EV_USER_REQUEST_NIF,
    EV_USER_PRIORITY_ROUTE,


    EV_USER_SEND_MY_NIF,
    EV_USER_GET_SECURE_KEY,
    EV_USER_UPDATE_FIRMWARE,

    EV_USER_GET_VERSION_FIRMWARE,
    EV_LIB_HANDLE_WAKEUP_FIRMWARE,

//---------------------------------------
    EV_USER_GET_LIST_DONE,
    EV_USER_SET_DEFAULT_DONE,
    EV_USER_SET_SUC_SIS_MODE_DONE,
    EV_USER_REQUEST_UPDATE_DONE,

    EV_USER_REMOVE_FORCE_DONE,
    EV_USER_CONTROLLER_CHANGE_DONE,
    EV_USER_NETWORK_TEST_DONE,

    EV_USER_REPLACE_FAILED_NODE_DONE,
    EV_USER_IS_FAILED_NODE_DONE,
    EV_USER_ADD_NODE_ZPC_DONE,

    EV_USER_REMOVE_NODE_ZPC_DONE,
    EV_USER_REMOVE_NODE_ZPC_AUTO_DONE,
    EV_USER_GET_CONTROLLER_INFO_ZPC_DONE,

    EV_USER_REQUEST_SPECIFICATION_ZPC_DONE,
    EV_USER_FIRMWARE_UPDATE_MD_DONE,
    EV_USER_REQUEST_NIF_DONE,
    EV_USER_PRIORITY_ROUTE_DONE,

    EV_USER_SEND_MY_NIF_DONE,
    EV_USER_GET_SECURE_KEY_DONE,
    EV_USER_UPDATE_FIRMWARE_DONE,

    EV_USER_GET_VERSION_FIRMWARE_DONE,

    EV_LIB_HANDLE_WAKEUP_DONE,

}POLLING_DEV_FSM_EV_T;

void postEventOperationPollingDev(POLLING_DEV_FSM_EV_T event);

#endif