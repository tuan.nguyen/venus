
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

#include "ZW_PollPlatform.h"
#include "ZW_PollUtil.h"
#include "utils.h"

/*Thread context*/
typedef struct
{
    void (*start_adr)(void * ); ///< The function to run
    void *args;                 ///< Argument to pass to the start_adr()
} thrd_ctx_t;


typedef struct 
{
    uint32_t    tmr_expiry;     ///< Timer ticks the timer will expire
    uint32_t    tmr_reload;     ///<Timer ticks to reload (for periodic timer only)
    tmr_cb_t    tmr_cb;    ///< The callback function to call when timer expires
    void        *data;          ///< The data passed as parameter to the callback function
    uint16_t    id;             ///< Timer identifier
} tmr_ctx_t;

/**
plt_mtx_lck - Lock a mutex
@param[in] context     The mutex context
@return
@pre       The mutex context must be initialized by plt_mtx_init
*/
void     plt_mtx_lck(struct plt_mtx_t *context)
{
    pthread_mutex_lock((pthread_mutex_t *)context);
}

/**
plt_mtx_ulck - Unlock a mutex
@param[in] context     The mutex context
@return
@pre       The mutex context must be initialized by plt_mtx_init
*/
void     plt_mtx_ulck(struct plt_mtx_t *context)
{
    pthread_mutex_unlock((pthread_mutex_t *)context);
}

/**
plt_mtx_destroy - Destroy a mutex
@param[in] context     The mutex context
@return
@pre       The mutex context must be initialized by plt_mtx_init
*/
void plt_mtx_destroy(struct plt_mtx_t *context)
{
	pthread_mutex_t *mutex = (pthread_mutex_t *)context;
	pthread_mutex_destroy(mutex);
	free(mutex);
}


/**
plt_sem_init - Init a semaphore
@param[in, out] context     Pointer to a void * for storing the semaphore context
@return     Return non-zero indicates success, zero indicates failure.
@post       Should call  plt_sem_destroy to free the semaphore context

*/

uint32_t plt_sem_init(void **context)
{
	sem_t *sem = (sem_t *)malloc(sizeof(sem_t));

	if (!sem)
	{
		return 0;
	}

	/* Initialize semaphore*/
	if (sem_init(sem, 0 ,0) != -1)
	{
		/* successful */
		*context =sem;
		return 1;
	}
	else
	{
		mainlog(logDebug, "sem init error");
	}
	free(sem);
	return 0;
}

/**
plt_sem_wait - Wait until the semaphore is signaled
@param[in] context     The semaphore context
@return
@pre       	The semaphore context must be initialized by plt_sem_init
*/
void plt_sem_wait(void *context)
{
	sem_wait((sem_t *)context);
}

/**
plt_sem_post - Increment the semaphore count by 1
@param[in] context     The semaphore context
@return
@pre       	The semaphore context must be initialized by plt_sem_init
*/

void plt_sem_post(void *context)
{
	sem_post((sem_t *)context);
}

/**
plt_sem_destroy - Destroy a semaphore
@param[in] context     The semaphore context
@return
@pre       	The semaphore context must be initialized by plt_sem_init
*/

void plt_sem_destroy(void *context)
{
	sem_destroy((sem_t *)context);
	free(context);
}



/**
plt_thrd_run - Run a thread
@param[in] args         Thread context.
@return     NULL on completion.
*/
static void *plt_thrd_run(void *args)
{
    thrd_ctx_t      *thrd_ctx = (thrd_ctx_t *)args;
    thrd_ctx->start_adr(thrd_ctx->args);
    free(thrd_ctx);
    return NULL;
}


/**
plt_thrd_create - Create a thread and run it
@param[in] start_adr    Pointer to a function for the newly created thread to execute.
@param[in] args         The argument passed to the newly created thread function.
@return     Return zero indicates success, negative number indicates failure.
*/
int     plt_thrd_create(void(*start_adr)( void * ), void *args)
{
    thrd_ctx_t      *thrd_ctx;
    pthread_t       thrd;
    pthread_attr_t  attrib;
    thrd_ctx = (thrd_ctx_t *)malloc(sizeof(thrd_ctx_t));
    if (!thrd_ctx)
    {
        return -1;
    }
    thrd_ctx->start_adr = start_adr;
    thrd_ctx->args = args;

    pthread_attr_init(&attrib);
    pthread_attr_setdetachstate(&attrib, PTHREAD_CREATE_DETACHED);
    pthread_attr_setstacksize(&attrib, 16384);
    if (pthread_create(&thrd, &attrib, plt_thrd_run, thrd_ctx) == 0)
    {   //Create thread succeeded
        return 0;
    }
    return -2;
}


/**
plt_sleep - Suspends the execution of the current thread until the time-out interval elapses.
@param[in] tmout_ms   The time interval for which execution is to be suspended, in milliseconds.
@return
*/
void     plt_sleep(uint32_t  tmout_ms)
{
    struct timespec timeout_val;

    // Calculate timeout
    timeout_val.tv_sec = tmout_ms / 1000;
    timeout_val.tv_nsec = (tmout_ms % 1000) * 1000000; //convert ms to ns

    nanosleep(&timeout_val, NULL);
}


/**
plt_tmr_create - Create and run a timer
@param[in] pltfm_ctx    Context
@param[in] tmout_ms     Timeout value in milliseconds
@param[in] tmout_cb     Pointer to the timeout callback function
@param[in] data         The data that will be passed back as parameter when the callback function is invoked
@param[in] periodic     Flag to indicate timer is a periodic timer.
@return     Return a pointer to the internal timer context, NULL indicates failure.
@pre        Must call plt_init once before using all timer related functions
@post       Must call plt_tmr_stop to release the timer resource even if the timer has expired.

*/
static void *plt_tmr_create(plt_ctx_t *pltfm_ctx, uint32_t  tmout_ms, tmr_cb_t  tmout_cb, void *data, int16_t periodic)
{
    tmr_ctx_t   tmr_ctx;
    int         ret_val;
    uint32_t    tmr_period;

    //Generate timer id
    //Note: Periodic timer id should be in different range from one-shot timer id.  If not
    //      there will be a possibility one-shot timer id equals to periodic timer id after id roll-over
    //      from 0xFFFF and this may cause the periodic timer being removed while the real intention is to
    //      remove the one-shot timer with the identical timer id.
    plt_mtx_lck(pltfm_ctx->tmr_mtx);

    if (!periodic)
    {   //One-shot
        tmr_ctx.id = ++pltfm_ctx->id_gen & ~0x8000;
        if (tmr_ctx.id == 0)
        {   //Don't use value of zero as it also represents NULL
            tmr_ctx.id++;
            pltfm_ctx->id_gen++;
        }
    }
    else
    {   //Periodic
        tmr_ctx.id = (++pltfm_ctx->per_id_gen) | 0x8000;
    }


    plt_mtx_ulck(pltfm_ctx->tmr_mtx);

    //Save the parameters for callback when timer expires
    tmr_ctx.tmr_cb = tmout_cb;
    tmr_ctx.data = data;

    //Calculate expiry timer ticks
    tmr_period = tmout_ms / PLT_TIMER_RESOLUTION;

    if ((tmout_ms % PLT_TIMER_RESOLUTION) > 0)
    {
        tmr_period++;
    }

    tmr_ctx.tmr_reload = (periodic)? tmr_period : 0;
    tmr_ctx.tmr_expiry = pltfm_ctx->tmr_tick + tmr_period;

    //Add to the timer list
    ret_val = util_list_add(pltfm_ctx->tmr_mtx, &pltfm_ctx->tmr_lst_hd, (uint8_t *)&tmr_ctx, sizeof(tmr_ctx_t));
    if (ret_val == 0)
    {
        return (void *)((uintptr_t)tmr_ctx.id);
    }
    return NULL;

}



/**
plt_periodic_start - Start a periodic timer
@param[in] pltfm_ctx    Context
@param[in] tmout_ms     Periodic timeout value in milliseconds
@param[in] tmout_cb     Pointer to the timeout callback function which executes a short task only
@param[in] data         The data that will be passed back as parameter when the callback function is invoked
@return     Return a pointer to the internal timer context, NULL indicates failure.
@pre        Must call plt_init once before using all timer related functions
@post       Must call plt_tmr_stop to release the timer resource even if the timer has expired.
@note       The periodic callback function (tmout_cb) MUST NOT sleep or be blocked
            (e.g. calling plt_sem_wait, plt_mtx_lck, etc) in order to avoid deadlock
*/
void    *plt_periodic_start(plt_ctx_t *pltfm_ctx, uint32_t  tmout_ms, tmr_cb_t  tmout_cb, void *data)
{
    return plt_tmr_create(pltfm_ctx, tmout_ms, tmout_cb, data, 1);
}

/**
tmr_tck_thrd - thread to generate timer tick
@param[in]  data        Context
@return
*/
static void tmr_tck_thrd(void   *data)
{
    plt_ctx_t *plt_ctx = (plt_ctx_t *)data;
    struct timespec timeout_val;

    plt_ctx->tmr_tck_thrd_sts = 1;

    // Calculate timeout
    timeout_val.tv_sec = 0;
    timeout_val.tv_nsec = PLT_TIMER_RESOLUTION * 1000000; //convert ms to ns

    while (plt_ctx->tmr_tck_thrd_run)
    {
        nanosleep(&timeout_val, NULL);
        //Update timer tick
        plt_ctx->tmr_tick++;

        //Send timer tick event
        plt_sem_post(plt_ctx->tmr_sem);

    }

    plt_ctx->tmr_tck_thrd_sts = 0;

}

/**
tmr_expr_cb_thrd - Timer expire callback thread
@param[in] args         Thread context.
@return
*/
static void tmr_expr_cb_thrd(void *args)
{
    thrd_ctx_t      *thrd_ctx = (thrd_ctx_t *)args;
    thrd_ctx->start_adr(thrd_ctx->args);
    free(thrd_ctx);
}

/**
tmr_chk_thrd - thread to process timer tick event
@param[in]  data        Context
@return
*/
static void tmr_chk_thrd(void   *data)
{
    plt_ctx_t       *plt_ctx = (plt_ctx_t *)data;
    util_lst_t      *prev_ent;     //Pointer to previous list entry
    util_lst_t      *temp;
    tmr_ctx_t       *tmr_ctx;
    tmr_cb_t        tmr_cb;         //The callback function to call when timer expires
    void            *cb_prm;        //The data passed as parameter to the callback function

    plt_ctx->tmr_chk_thrd_sts = 1;

    while (1)
    {
        /*Wait for timer tick event*/
        plt_sem_wait(plt_ctx->tmr_sem);

        /*Check whether to exit the thread*/
        if (plt_ctx->tmr_chk_thrd_run == 0)
        {
            plt_ctx->tmr_chk_thrd_sts = 0;
            return;
        }

        /*Check each of the timer contexts for expiry*/
        plt_mtx_lck(plt_ctx->tmr_mtx);

        if (plt_ctx->tmr_lst_hd == NULL)
        {   
            /*Nothing to remove*/
            plt_mtx_ulck(plt_ctx->tmr_mtx);
            continue;
        }

        temp = plt_ctx->tmr_lst_hd;
        prev_ent = NULL;

        while (temp)
        {
            tmr_ctx = (tmr_ctx_t *)temp->wr_buf;

            if (util_tmr_exp_chk32(plt_ctx->tmr_tick, tmr_ctx->tmr_expiry))
            {   
                /*Timer expired*/
                thrd_ctx_t      *thrd_ctx;
                int             periodic;   //flag whether timer is periodic

                tmr_cb = tmr_ctx->tmr_cb;
                cb_prm = tmr_ctx->data;

                /*Check whether to reload timer expiry value for periodic timer*/
                if (tmr_ctx->tmr_reload > 0)
                {   //Periodic timer
                    periodic = 1;
                    tmr_ctx->tmr_expiry = plt_ctx->tmr_tick + tmr_ctx->tmr_reload;
                }
                else
                {   //One shot timer, remove it.
                    periodic = 0;
                    if (!prev_ent)
                    {   //No previous entry, this is the head of list
                        plt_ctx->tmr_lst_hd = temp->next;
                    }
                    else
                    {   //Assign the next entry to the previous entry
                        prev_ent->next = temp->next;
                    }
                    free(temp);

                }

                //Callback timer callback function
                //DON'T hold the lock as this could cause deadlock if timer callback function calls
                //plt_sem_wait() etc. that may sleep.
                plt_mtx_ulck(plt_ctx->tmr_mtx);
                if (periodic)
                {
                    //IMPORTANT: Make sure the periodic callback function does not sleep/block
                    //          (by calling plt_sem_wait, plt_mtx_lck, etc) to avoid deadlock
                    tmr_cb(cb_prm);
                }
                else
                {
                    //Create new thread for every callback to avoid deadlock
                    thrd_ctx = (thrd_ctx_t *)malloc(sizeof(thrd_ctx_t));
                    if (thrd_ctx)
                    {
                        thrd_ctx->start_adr = tmr_cb;
                        thrd_ctx->args = cb_prm;
                        plt_thrd_create(tmr_expr_cb_thrd, thrd_ctx);
                    }
                }
                plt_mtx_lck(plt_ctx->tmr_mtx);

                //Start checking from the list head again since the timer callback function
                //may have changed the timer list.
                temp = plt_ctx->tmr_lst_hd;
                prev_ent = NULL;
                continue;
            }
            prev_ent = temp;
            temp = temp->next;
        }

        plt_mtx_ulck(plt_ctx->tmr_mtx);
    }
}



/**
plt_tmr_stop - Stop the timer and release timer resource
@param[in] pltfm_ctx   Context
@param[in] context     The timer context returned from the plt_tmr_start
@return     Return non-zero indicates success, zero indicates failure.
@pre        Must call plt_init once before using all timer related functions
@post       The caller should not use the timer context after this call.
*/
uint32_t     plt_tmr_stop(plt_ctx_t *pltfm_ctx, void  *context)
{
    uint32_t        ret_val;
    uint16_t        tmr_id = (uintptr_t)context;
    util_lst_t      *prev_ent;     //Pointer to previous list entry
    util_lst_t      *temp;
    tmr_ctx_t       *tmr_ctx;

    if (!tmr_id)
        return 0;

    plt_mtx_lck(pltfm_ctx->tmr_mtx);

    if (pltfm_ctx->tmr_lst_hd == NULL)
    {   //Nothing to remove
        plt_mtx_ulck(pltfm_ctx->tmr_mtx);
        return 0;
    }

    temp = pltfm_ctx->tmr_lst_hd;
    prev_ent = NULL;

    ret_val = 0;    //initialize return value to failure
    while (temp)
    {
        tmr_ctx = (tmr_ctx_t *)temp->wr_buf;

        if (tmr_ctx->id == tmr_id)
        {   //Found timer
            if (!prev_ent)
            {   //No previous entry, this is the head of list
                pltfm_ctx->tmr_lst_hd = temp->next;
            }
            else
            {   //Assign the next entry to the previous entry
                prev_ent->next = temp->next;
            }
            free(temp);
            ret_val = 1;
            break;
        }
        prev_ent = temp;
        temp = temp->next;
    }

    plt_mtx_ulck(pltfm_ctx->tmr_mtx);
    return ret_val;
}


/**
plt_mtx_init - Initialize a recursive mutex
@param[in, out] context     Pointer to a void * for storing the mutex context
@return     Return non-zero indicates success, zero indicates failure.
@post       Should call  plt_mtx_destroy to free the mutex context
*/
uint32_t     plt_mtx_init(struct plt_mtx_t **context)
{
    uint32_t                ret_val;
    pthread_mutexattr_t     attr;
    pthread_mutex_t         *mutex = (pthread_mutex_t  *)malloc(sizeof (pthread_mutex_t));

    if (!mutex)
        return 0;

    if (pthread_mutexattr_init(&attr) != 0)
    {
        free(mutex);
        return 0;
    }

    //Set attribute to recursive mutex

    if (pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE_NP) != 0)
    {
        pthread_mutexattr_destroy(&attr);
        free(mutex);
        return 0;
    }

    //Initialize mutex
    if (pthread_mutex_init (mutex, &attr) == 0)
    {   //Successful
        *context = (struct plt_mtx_t *)mutex;
        ret_val = 1;
    }
    else
    {
        free(mutex);
        ret_val = 0;
    }

    pthread_mutexattr_destroy(&attr);
    return ret_val;

}



/**
plt_init - Initialize platform
@param[in] pltfm_ctx        Context
@param[in] display_txt_fn   Function pointer to display null terminated string
@return    Return zero on success; negative error number indicates failure.
*/
int plt_init(plt_ctx_t *pltfm_ctx)
{
    if (pltfm_ctx->init_done > 0)
    {
        //Update initialization count
        pltfm_ctx->init_done++;

        return ZWHCI_NO_ERROR;
    }

    if (!plt_mtx_init(&pltfm_ctx->tmr_mtx))
    {
        return ZWHCI_ERROR_RESOURCE;
    }

    if (!plt_sem_init(&pltfm_ctx->tmr_sem))
    {
        goto l_PLATFORM_INIT_ERROR;
    }

    //Init random number seed
    srand(time(NULL));

    //Start timer tick thread
    pltfm_ctx->tmr_tck_thrd_run = 1;
    if (plt_thrd_create(tmr_tck_thrd, pltfm_ctx) < 0)
    {
        goto l_PLATFORM_INIT_ERROR1;
    }

    //Start timer check thread
    pltfm_ctx->tmr_chk_thrd_run = 1;
    if (plt_thrd_create(tmr_chk_thrd, pltfm_ctx) < 0)
    {
        goto l_PLATFORM_INIT_ERROR2;
    }

    //Done. Update status
    pltfm_ctx->init_done = 1;
    return 0;

l_PLATFORM_INIT_ERROR2:
    //Stop the timer tick thread
    pltfm_ctx->tmr_tck_thrd_run = 0;
    while (pltfm_ctx->tmr_tck_thrd_sts)
    {
        plt_sleep(100);
    }

l_PLATFORM_INIT_ERROR1:
    plt_sem_destroy(pltfm_ctx->tmr_sem);

l_PLATFORM_INIT_ERROR:
    plt_mtx_destroy(pltfm_ctx->tmr_mtx);

    return ZWHCI_ERROR_RESOURCE;

}