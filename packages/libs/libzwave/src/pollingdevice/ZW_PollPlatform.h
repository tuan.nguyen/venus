
#ifndef _ZW_POLL_PLATFORM_H_
#define _ZW_POLL_PLATFORM_H_

#include "ZW_PollUtil.h"


/// Platform timer resolution in milliseconds
#define PLT_TIMER_RESOLUTION    100

/// Timer callback function
typedef void    (*tmr_cb_t)(void *data);


/// Print text function
typedef void    (*print_fn)(void *msg);


struct plt_mtx_t;     ///< Mutex type for multi-platform support


///Platform context
typedef struct
{
    volatile uint32_t   tmr_tick;       	///< Timer tick every PLT_TIMER_RESOLUTION
    uint16_t            id_gen;         	///< One-shot Timer identifier generator (range 1 to 0x7FFF)
    uint16_t            per_id_gen;     	///< Periodic Timer identifier generator (range 0x8000 to 0xFFFF)
    int                 init_done;      	///< Counter to indicated how many times platform initialization has been invoked
    struct plt_mtx_t    *tmr_mtx;       	///< mutex for accessing timer list
    void                *tmr_sem;       	///< semaphore for waiting timer tick event
    util_lst_t          *tmr_lst_hd;    	///< head of linked list for timers
    volatile int        tmr_tck_thrd_run;   ///< control the timer tick thread whether to run. 1 = run, 0 = stop
    volatile int        tmr_tck_thrd_sts;   ///< timer tick thread status. 1 = running, 0 = thread exited
    volatile int        tmr_chk_thrd_run;   ///< control the timer check thread whether to run. 1 = run, 0 = stop
    volatile int        tmr_chk_thrd_sts;   ///< timer check thread status. 1 = running, 0 = thread exited

} plt_ctx_t;



/**<
Stop the timer and release timer resource
@param[in] pltfm_ctx    Context
@param[in] context     The timer context returned from the plt_tmr_start
@return     Return non-zero indicates success, zero indicates failure.
@post       The caller should not use the timer context after this call.
*/
uint32_t    plt_tmr_stop(plt_ctx_t *pltfm_ctx, void *context);


/**<
Initialize a recursive mutex
@param[in, out] context     Pointer to a void * for storing the mutex context
@return     Return non-zero indicates success, zero indicates failure.
@post       Should call  plt_mtx_destroy to free the mutex context
*/
uint32_t    plt_mtx_init(struct plt_mtx_t **context);



/**<
Destroy a mutex
@param[in] context     The mutex context
@return
@pre       The mutex context must be initialized by plt_mtx_init
*/
void        plt_mtx_destroy(struct plt_mtx_t *context);



/**<
Lock a mutex
@param[in] context     The mutex context
@return
@pre       The mutex context must be initialized by plt_mtx_init
*/
void        plt_mtx_lck(struct plt_mtx_t *context);



/**<
Unlock a mutex
@param[in] context     The mutex context
@return
@pre       The mutex context must be initialized by plt_mtx_init
*/
void        plt_mtx_ulck(struct plt_mtx_t *context);



/**<
Init a semaphore
@param[in, out] context     Pointer to a void * for storing the semaphore context
@return     Return non-zero indicates success, zero indicates failure.
@post       Should call  plt_sem_destroy to free the semaphore context
*/
uint32_t    plt_sem_init(void **context);


/**<
Destroy a semaphore
@param[in] context     The semaphore context
@return
@pre        The semaphore context must be initialized by plt_sem_init
*/
void        plt_sem_destroy(void *context);


/**<
Wait until the semaphore is signaled
@param[in] context     The semaphore context
@return
@pre        The semaphore context must be initialized by plt_sem_init
*/
void        plt_sem_wait(void *context);



/**<
Increment the semaphore count by 1
@param[in] context     The semaphore context
@return
@pre        The semaphore context must be initialized by plt_sem_init
*/
void        plt_sem_post(void *context);


/**<
Create a thread and run it
@param[in] start_adr    Pointer to a function for the newly created thread to execute.
@param[in] args         The argument passed to the newly created thread function.
@return     Return zero indicates success, negative number indicates failure.
*/
int         plt_thrd_create(void (*start_adr)( void * ), void *args);



/**<
Suspends the execution of the current thread until the time-out interval elapses.
@param[in] tmout_ms   The time interval for which execution is to be suspended, in milliseconds.
@return
*/
void        plt_sleep(uint32_t    tmout_ms);



/**<
Start a periodic timer
@param[in] pltfm_ctx    Context
@param[in] tmout_ms     Periodic timeout value in milliseconds
@param[in] tmout_cb     Pointer to the timeout callback function which executes a short task only
@param[in] data         The data that will be passed back as parameter when the callback function is invoked
@return     Return a pointer to the internal timer context, NULL indicates failure.
@post       Must call plt_tmr_stop to release the timer resource even if the timer has expired.
@note       The periodic callback function (tmout_cb) MUST NOT sleep or be blocked
            (e.g. calling plt_sem_wait, plt_mtx_lck, etc) in order to avoid deadlock
*/
void        *plt_periodic_start(plt_ctx_t *pltfm_ctx, uint32_t  tmout_ms, tmr_cb_t  tmout_cb, void *data);



/**<
Initialize platform
@param[in] pltfm_ctx        Context
@param[in] display_txt_fn   Function pointer to display null terminated string
@return    Return zero on success; negative error number indicates failure.
*/
int         plt_init(plt_ctx_t *pltfm_ctx);


#endif
