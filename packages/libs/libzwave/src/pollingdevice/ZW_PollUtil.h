
#ifndef _ZW_POLL_UTIL_H_
#define _ZW_POLL_UTIL_H_

#include <time.h>

/// Error codes definition
#define     ZWHCI_NO_ERROR                     0 ///< No error
#define     ZWHCI_ERROR_MEMORY              -100 ///< Out of memory
#define     ZWHCI_ERROR_RESOURCE            -101 ///< Out of resource
#define     ZWHCI_ERROR_WRITE               -120 ///< Write failed
#define     ZWHCI_ERROR_TIMEOUT             -122 ///< Time out



/**<
message loop on message arrives callback
@param[in]	usr_prm	    user parameter supplied during initialization
@param[in]	msg	        message
return      1 to exit message loop; else return 0 to continue the message loop
*/
typedef int (*util_msg_loop_fn)(void *usr_prm, void *msg);


/**<
function to free the content of message apart from the message itself
@param[in]	msg	        message
return
@note    The function should not free the "msg" itself, it is freed by the message loop related function.
*/
typedef void (*util_msg_free_fn)(void *msg);



/* List entry */
typedef struct  _util_lst
{
    struct _util_lst  *next;        ///< Point to the next list entry
    uint16_t          dat_sz;       ///< Data size
    uint8_t           wr_buf[1];    ///< Place holder for the data buffer. MUST be 4-byte aligned.

} util_lst_t;



/* Message loop context */
typedef struct
{
    volatile int            thrd_run;   /**< Control the thread whether to run. 1 = run, 0 = stop*/
    volatile int            thrd_sts;   /**< Thread status. 1 = run, 0 = thread exited*/
    void                    *sem;       /**< Semaphore for waiting messages*/
    struct plt_mtx_t        *mtx;       /**< Mutex for message queue */
    util_lst_t              *msg_hd;    /**< Head of linked list of messages*/
    util_msg_free_fn        free_fn;    /**< Function to free message content (optional)*/
    unsigned                id;         /**< Identifier for the message loop (for debug use)*/

    //Callback
    util_msg_loop_fn        cb;         /**< Callback function on message arrives*/
    void                    *usr_prm;   /**< User supplied parameter*/

} util_msg_loop_t;


/**<
add an entry into the end of the list.
@param[in]      mtx_ctx     Mutex context; optional, if NULL no locking will be used.
@param[in,out]	head		Pointer to the list head
@param[in]      buf         Buffer that store the data
@param[in]      dat_sz      Size of data to be stored
@return                     Return 0 on success, negative error number on failure.
*/
int     util_list_add(struct plt_mtx_t *mtx_ctx, util_lst_t **head, uint8_t  *buf, uint16_t dat_sz);



/**<
get the entry from the beginning of the list.
@param[in]      mtx_ctx     Mutex context; optional, if NULL no locking will be used.
@param[in, out]	head		List head
@return     The first entry in the list if the list is not empty; otherwise, NULL.
@post       The caller should free the returned entry.
*/
util_lst_t *util_list_get(struct plt_mtx_t *mtx_ctx, util_lst_t **head);



/**<
flush the list.
@param[in]      mtx_ctx     Mutex context; optional, if NULL no locking will be used.
@param[in, out]	head		List head
@return
*/
void    util_list_flush(struct plt_mtx_t *mtx_ctx, util_lst_t **head);




/**<
Initialize message loop
@param[in]  cb      Callback function on message arrival
@param[in]  usr_prm	User supplied parameter which will be used on callback
@param[in]  free_fn	User supplied function to free the content of message. This is optional.
                    If not used, set it to NULL.
@param[in]  id	    Identifier for the message loop (for debug use)
@param[out] ctx     Context
@return     Return 0 on success, negative error number on failure.
@post       Caller should call util_msg_loop_shutdown() to stop the message loop and util_msg_loop_exit()
            to free the allocated memory
*/
int util_msg_loop_init(util_msg_loop_fn cb, void *usr_prm, util_msg_free_fn free_fn, unsigned id, util_msg_loop_t **ctx);



/**<
Stop the message loop
@param[in]  msg_ctx Context
@param[in]  tm_out	Timeout (in milliseconds) to wait for message loop to stop. If zero, no timeout.
@return     Return 0 on success, negative error number on failure.
*/
int util_msg_loop_shutdown(util_msg_loop_t *msg_ctx, uint32_t tm_out);




/**<
Free the allocated memory
@param[in]  msg_ctx Context
@return
@pre  Should call util_msg_loop_shutdown() first, then wait for the appropriate time to call this function safely
@post Do not call util_msg_loop_send() after calling this function
*/
void util_msg_loop_exit(util_msg_loop_t *msg_ctx);



/**<
Send a message to message loop
@param[in]  msg_ctx Context
@param[in]  msg	    Message
@param[in]  msg_sz  Message size
@return     Return 0 on success, negative error number on failure.
*/
int util_msg_loop_send(util_msg_loop_t *msg_ctx, void *msg, uint8_t msg_sz);





/**<
Check whether the 32-bit timer has expired
@param[in] now     Current time
@param[in] timer   The time of the timer to check
@return 1=timer has expired; otherwise 0
*/
int util_tmr_exp_chk32(uint32_t now, uint32_t timer);




/**<
Calculate 32-bit time difference (i.e. tm1 - tm2) in terms of timer ticks
@param[in] tm1     Time 1
@param[in] tm2     Time 2
@return time different in terms of timer tick
*/
uint32_t   util_time_diff32(uint32_t tm1, uint32_t tm2);


#endif