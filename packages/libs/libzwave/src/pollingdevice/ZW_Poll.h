
#ifndef _ZW_POLL_H_
#define _ZW_POLL_H_

#include <stdint.h>
#include "ZW_PollPlatform.h"
#include "ZW_PollApi.h"
#include "zw_serialapi/zw_classcmd.h"

#define POLL_TIMER_TICK             500                         /**< Periodic timer tick interval in ms */
#define POLL_TICK_PER_SEC           (1000/POLL_TIMER_TICK)      /**< Number of timer ticks per second */
#define MIN_POLL_TIME               (3)                         /**< Minimum polling time in terms of timer tick */
#define CHECK_EXPIRY_INTERVAL       (1 * POLL_TICK_PER_SEC)     /**< Check for polling entries expiry interval
                                                                     in terms of timer tick */
#define POLL_RPT_TIMEOUT            (10 * POLL_TICK_PER_SEC)    /**< Poll report timeout */

/** Event definition */
#define POLL_EVT_TYPE_WAKEUP        0       /**< Wakeup event*/
#define POLL_EVT_TYPE_REPORT        1       /**< Report arrival event*/

/** Poll entry active status */
#define POLL_STS_INACTIVE_WAIT_RPT  1       /**< inactive and waiting for report to complete the poll process*/
#define POLL_STS_ACTIVE             2       /**< active and ready to poll*/

/** Wakeup and report arrival event message*/
typedef struct
{
    int         type;           /**< Event type POLL_EVT_TYPE_XXX */
    uint8_t     nodeid;         /**< Node ID */
    uint8_t     rpt;            /**< Report*/
    uint16_t    cls;            /**< Command class*/
}
poll_evt_msg_t;


/** Callback request message*/
typedef struct
{
    zwpoll_cmplt_fn cmplt_cb;       /**< Polling completion callback*/
    void            *usr_param;     /**< User parameter*/
    uint32_t        usr_token;      /**< User defined token to facilitate deletion of multiple polling requests */
    uint16_t        handle;         /**< Polling request handle*/
}
poll_cb_msg_t;



/** Polling queue entry*/
typedef struct _poll_q_ent
{
    struct _poll_q_ent *next;   /**< Next entry */
    struct _poll_q_ent *prev;   /**< Previous entry */
	zwifd_t     ifd;	        /**< Interface associated with the command */
    uint32_t    next_poll_tm;   /**< Next polling time */
    uint32_t    rpt_tout;       /**< Report timeout. Only valid for active_sts is POLL_STS_INACTIVE_WAIT_RPT */
    uint32_t    slp_node_tout;  /**< Sleeping node timeout. Only valid for sleeping==1 */
    uint32_t    usr_token;      /**< User defined token to facilitate deletion of multiple polling requests */
    uint32_t    interval;       /**< Polling interval in terms of timer tick*/
    zwpoll_cmplt_fn cmplt_cb;   /**< Polling completion callback. NULL if callback is not required*/
    void        *usr_param;     /**< User parameter of polling completion callback */
    uint16_t    active_sts;     /**< Active status, POLL_STS_XXX */
    uint16_t    expired;        /**< Flag to indicate the polling time is expired and ready to send poll command */
    uint16_t    wr_cmd_q;       /**< Flag to indicate the polling command has been written to sleeping node command queue */
    uint16_t    wait_rpt;       /**< Flag to indicate the node is waiting for report */
    uint16_t    poll_seq_num;   /**< Poll sequence number to avoid polling stuck at one particular node */
    uint16_t    poll_cnt;       /**< Number of times to poll; zero = unlimited times (repetitive poll)*/
    uint16_t    tot_poll_cnt;   /**< Number of polls being executed */
    uint16_t    handle;         /**< Polling request handle*/
    uint16_t    cmd_cls;        /**< Expected command class of the report*/
    uint8_t     rpt;            /**< Expected report command of the report*/
    uint8_t     nodeId;        /**< Node id*/
}
poll_q_ent_t;




/** Polling context */
typedef struct  _poll_ctx
{
    volatile uint32_t   tmr_tick;           /**< Periodic timer tick, incremented every POLL_TIMER_TICK ms */
    volatile int        run;                /**< Control whether to run polling facility. 1 = run, 0 = stop */
    volatile int        tmr_chk_thrd_run;   /**< Control the timer check thread whether to run. 1 = run, 0 = stop */
    volatile int        tmr_chk_thrd_sts;   /**< Timer check thread status. 1 = running, 0 = thread exited */
    uint32_t            next_poll_tm;       /**< Next polling time */
    void                *tmr_sem;           /**< Semaphore for waiting timer tick event */
    struct plt_mtx_t    *poll_mtx;          /**< Mutex for the polling facility */
    void                *tick_tmr_ctx;      /**< Tick timer context */
    poll_q_ent_t        *poll_lst_hd;       /**< Head of double linked list for polling requests */
    poll_q_ent_t        *poll_lst_tail;     /**< Tail of double linked list for polling requests */
    util_msg_loop_t     *cb_msg_loop_ctx;   /**< Context of message loop for callback*/
    util_msg_loop_t     *evt_msg_loop_ctx;  /**< Context of message loop for wakeup and report arrival events*/
    zwnet_p             net;                /**< Network */
    uint16_t            handle_gen;         /**< Handle number generator */
    uint16_t            poll_seq_num;       /**< Poll sequence number to avoid polling stuck at one particular node */
    uint32_t            cur_start_tm;       /**< Start time of the current poll*/
    uint32_t            cur_cmd_tm;         /**< Command time of the current poll*/
    uint16_t            cur_cmd_cls;        /**< Expected command class of the report*/
    uint8_t             cur_rpt;            /**< Expected report command of the report*/
    uint8_t             cur_nodeId;         /**< Node id of the current poll*/
    uint8_t             cur_endpoint;       /**< Ep id of the current poll*/
}
zwpoll_ctx_t;

/**
zwpoll_init - Initialize the polling facility
@param[in]	poll_ctx	    Polling context
@return  0 on success; negative error number on failure
@note  Must call zwpoll_shutdown followed by zwpoll_exit to shutdown and clean up the polling facility
*/
int zwpoll_init(zwpoll_ctx_t *poll_ctx);

/**<
add a polling request into polling queue
@param[in]  net         network
@param[in]  poll_ent    polling request entry (allocated by caller). All unused fields must be zeroed.
@param[out] handle      Polling request handle
@return     ZW_ERR_NONE if success; else ZW_ERR_XXX on error
@post       If return error, caller must free poll_ent
*/
int zwpoll_add(zwnet_p net, poll_q_ent_t *poll_ent, uint16_t *handle);

/**
zwpoll_rm - remove polling requests that belong to the specified node
@param[in]  net         network
@param[in]  nodeId     node id
@return     ZW_ERR_NONE if success; else ZW_ERR_XXX on error
*/
int zwavepoll_rm(zwnet_p net, uint8_t nodeId);

/**
zwpoll_rm_all - remove all polling requests
@param[in]  poll_ctx    Context
@return
*/
void zwpoll_rm_all(zwpoll_ctx_t *poll_ctx);

/**
zwpoll_exit - Clean up
@param[in]  poll_ctx        Polling context
@return
*/
void zwpoll_exit(zwpoll_ctx_t *poll_ctx);

/**
zwpoll_shutdown - Shutdown the polling facility
@param[in]  poll_ctx        Polling context
@return
*/
void zwpoll_shutdown(zwpoll_ctx_t *poll_ctx);

/**
get_nwstr - get string of network operation
@return
*/
char *get_nwstr(uint8_t nw);

#endif