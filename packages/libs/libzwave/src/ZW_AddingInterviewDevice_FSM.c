#include "ZW_AddingInterviewDevice_FSM.h"
#include "supported_CommandClassInclusionController.h"
#include "timer.h"
#include "nvm.h"
#include "node_cache.h"

#define NO_TIMER 0   /* Unused timer handle */
#define NO_TIMEOUT 0 /* No timeout from this state */

typedef struct
{
    INTERVIEW_DEVICE_STATE st;      // current state
    INTERVIEW_DEVICE_FSM_EV_T ev;   // incoming event
    INTERVIEW_DEVICE_STATE next_st; // next state
    void (*fn)(void);               // action function returning next state
} transition_t;

typedef struct
{
    INTERVIEW_DEVICE_STATE st;
    uint16_t timeout;
    void (*tfn)(void *);
} state_timeout_t;

INTERVIEW_DEVICE_STATE state_interview = ST_INTERVIEW_IDLE;
static timer_t bFsmTimerHandleInterviewDevice = NO_TIMER;
static void (*cbFsmTimerInterviewDevice)(void *);
uint8_t bFsmTimerCountdownInterview;
uint8_t schemeGet;
//variable in zw_api.c
extern uint8_t targetEndpoint;
extern uint8_t targetAggEndpoint;
extern uint8_t MySystemSUCId;

//auto remove add variable
extern uint8_t actionFromUser;

extern void ActionWhenInterviewDeviceFailed(void);
extern void NewNode_Notify(uint8_t bStatus);

static void SetAssociation_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t);
static void wakeUpSet_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t);
static void AssignReturnRoute_Compl(uint8_t bTxStatus);
//action of state
void ZCB_TimerInterviewDevice_ReturnToIdle(void *data);
void AC_SendZwavePlusInfo();
void AC_SetMultiChannelAssociation(uint8_t schemeGet);
void AC_SetAssociation();
void AC_SendAssignReturnRoute();
void AC_SendGetMultiChannel();
void AC_SetWakeup_Interval();
void AC_GetManufacturerSpecific();
void AC_FinishInterviewDevice(void);
void AC_GetCapEptMulChannel();
void AC_GetS2CapEptMulChannel();
void AC_GetS0CapEptMulChannel();
void AC_InterviewDeviceFail(void);
void AC_InterviewDeviceSuccess(void);
void AC_GetMemberAggEptMulChannel(void);
void PostEventInterviewDevice(INTERVIEW_DEVICE_FSM_EV_T event);

/*=======================   ZCB_TimerOutStateInterviewDevice   ========================
**
**  Timeout function that returns the FSB to idle.
**  Called by the FSM timer.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/

void ZCB_TimerOutStateInterviewDevice(void *data)
{
    PostEventInterviewDevice(EV_TIMEOUT_STATE);
}

void ZCB_TimerOutStateGetManu(void *data)
{
    PostEventInterviewDevice(EV_TIMEOUT_TIME_1);
}

/* State transition table
 * This is where the state machine is defined.
 *
 * Each transition can optionally trigger a call to an action function.
 * An action function defines the actions that must occur when a particular
 * transition occurs.
 *
 * Format: {Current state, incoming event, next state, action_function} */

static const transition_t trans[] =
    {
        {ST_INTERVIEW_IDLE, EV_ADD_NODE_OK, ST_AWAIT_ZWAVEPLUS_INFO_REPORT, &AC_SendZwavePlusInfo},
        {ST_INTERVIEW_IDLE, EV_CLOSE_NWK, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},
        {ST_AWAIT_ZWAVEPLUS_INFO_REPORT, EV_ZWAVEPLUS_INFO_REPORT_OK, ST_AWAIT_MULTICHANNEL_REPORT, &AC_SendGetMultiChannel},
        {ST_AWAIT_ZWAVEPLUS_INFO_REPORT, EV_NOT_SUPPORT_CC_ZWAVEPLUS, ST_AWAIT_MULTICHANNEL_REPORT, &AC_SendGetMultiChannel},
        {ST_AWAIT_ZWAVEPLUS_INFO_REPORT, EV_ZWAVEPLUS_INFO_REPORT_NOT_OK, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},
        {ST_AWAIT_ZWAVEPLUS_INFO_REPORT, EV_TIMEOUT_STATE, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},

        {ST_AWAIT_MULTICHANNEL_REPORT, EV_RECIEVE_END_POINT_REPORT_OK, ST_AWAIT_CAP_REPORT_OF_EP, &AC_GetCapEptMulChannel},
        {ST_AWAIT_MULTICHANNEL_REPORT, EV_NOT_SUPPORT_CC_MULTI_CHANNEL, ST_AWAIT_ASSIGN_ROUTE_COMPL, &AC_SendAssignReturnRoute},
        {ST_AWAIT_MULTICHANNEL_REPORT, EV_RECIEVE_END_POINT_REPORT_NOT_OK, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},
        {ST_AWAIT_MULTICHANNEL_REPORT, EV_TIMEOUT_STATE, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},

        {ST_AWAIT_CAP_REPORT_OF_EP, EV_REPORT_CAPABILITY_EP_OK, ST_AWAIT_CAP_REPORT_OF_NEXT_EP, &AC_GetCapEptMulChannel},
        {ST_AWAIT_CAP_REPORT_OF_EP, EV_REPORT_CAPABILITY_EP_NOT_OK, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},
        {ST_AWAIT_CAP_REPORT_OF_EP, EV_TIMEOUT_STATE, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},
        {ST_AWAIT_CAP_REPORT_OF_EP, EV_ALL_EP_REPORT_OK, ST_AWAIT_CAP_SECURE_V2_REPORT_OF_EP, &AC_GetS2CapEptMulChannel},
        //*Meet event, not change state --> timer not run from 0 *
        {ST_AWAIT_CAP_REPORT_OF_NEXT_EP, EV_REPORT_CAPABILITY_EP_OK, ST_AWAIT_CAP_REPORT_OF_EP, &AC_GetCapEptMulChannel},
        {ST_AWAIT_CAP_REPORT_OF_NEXT_EP, EV_ALL_EP_REPORT_OK, ST_AWAIT_CAP_SECURE_V2_REPORT_OF_EP, &AC_GetS2CapEptMulChannel},
        {ST_AWAIT_CAP_REPORT_OF_NEXT_EP, EV_ALL_EP_REPORT_NOT_OK, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},
        {ST_AWAIT_CAP_REPORT_OF_NEXT_EP, EV_TIMEOUT_STATE, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},

        {ST_AWAIT_CAP_REPORT_OF_NEXT_EP, EV_AWAIT_MEMEBER_REPORT_OF_AGG_EP, ST_AWAIT_MEMBER_REPORT_OF_NEXT_AGG_EP, &AC_GetMemberAggEptMulChannel},

        {ST_AWAIT_MEMBER_REPORT_OF_NEXT_AGG_EP, EV_REPORT_MEMBER_AGG_EP_OK, ST_AWAIT_MEMBER_REPORT_OF_NEXT_AGG_EP, &AC_GetMemberAggEptMulChannel},
        {ST_AWAIT_MEMBER_REPORT_OF_NEXT_AGG_EP, EV_ALL_EP_REPORT_OK, ST_AWAIT_CAP_SECURE_V2_REPORT_OF_EP, &AC_GetS2CapEptMulChannel},
        {ST_AWAIT_MEMBER_REPORT_OF_NEXT_AGG_EP, EV_ALL_EP_REPORT_NOT_OK, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},
        {ST_AWAIT_MEMBER_REPORT_OF_NEXT_AGG_EP, EV_TIMEOUT_STATE, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},

        /*2017-10-16, add  ep_capability_secureV2*/
        {ST_AWAIT_CAP_SECURE_V2_REPORT_OF_EP, EV_REPORT_SECURE_V2_CAPABILITY_EP_OK, ST_AWAIT_CAP_SECURE_V2_REPORT_OF_NEXT_EP, &AC_GetS2CapEptMulChannel},
        {ST_AWAIT_CAP_SECURE_V2_REPORT_OF_EP, EV_REPORT_SECURE_V2_CAPABILITY_EP_NOT_OK, ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, &AC_GetS0CapEptMulChannel},
        {ST_AWAIT_CAP_SECURE_V2_REPORT_OF_EP, EV_ALL_EP_REPORT_SECURE_V2_CAPABILITY_NOT_OK, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},
        {ST_AWAIT_CAP_SECURE_V2_REPORT_OF_EP, EV_EP_NOT_SUPPORT_CC_SECURE_V2, ST_AWAIT_CAP_SECURE_V2_REPORT_OF_NEXT_EP, &AC_GetS2CapEptMulChannel},
        {ST_AWAIT_CAP_SECURE_V2_REPORT_OF_EP, EV_TIMEOUT_STATE, ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, &AC_GetS0CapEptMulChannel},
        {ST_AWAIT_CAP_SECURE_V2_REPORT_OF_EP, EV_ALL_EP_REPORT_SECURE_V2_CAPABILITY_OK, ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, &AC_GetS0CapEptMulChannel},

        {ST_AWAIT_CAP_SECURE_V2_REPORT_OF_NEXT_EP, EV_REPORT_SECURE_V2_CAPABILITY_EP_OK, ST_AWAIT_CAP_SECURE_V2_REPORT_OF_EP, &AC_GetS2CapEptMulChannel},
        {ST_AWAIT_CAP_SECURE_V2_REPORT_OF_NEXT_EP, EV_REPORT_SECURE_V2_CAPABILITY_EP_NOT_OK, ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, &AC_GetS0CapEptMulChannel},
        {ST_AWAIT_CAP_SECURE_V2_REPORT_OF_NEXT_EP, EV_ALL_EP_REPORT_SECURE_V2_CAPABILITY_NOT_OK, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},
        {ST_AWAIT_CAP_SECURE_V2_REPORT_OF_NEXT_EP, EV_EP_NOT_SUPPORT_CC_SECURE_V2, ST_AWAIT_CAP_SECURE_V2_REPORT_OF_EP, &AC_GetS2CapEptMulChannel},
        {ST_AWAIT_CAP_SECURE_V2_REPORT_OF_NEXT_EP, EV_TIMEOUT_STATE, ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, &AC_GetS0CapEptMulChannel},
        {ST_AWAIT_CAP_SECURE_V2_REPORT_OF_NEXT_EP, EV_ALL_EP_REPORT_SECURE_V2_CAPABILITY_OK, ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, &AC_GetS0CapEptMulChannel},
        /*2017-10-16, add ep_capability_secureV0*/
        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, EV_REPORT_SECURE_V0_CAPABILITY_EP_OK, ST_AWAIT_CAP_SECURE_V0_REPORT_OF_NEXT_EP, &AC_GetS0CapEptMulChannel},
        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, EV_EP_SUPPORT_CC_SECURE_V2, ST_AWAIT_CAP_SECURE_V0_REPORT_OF_NEXT_EP, &AC_GetS0CapEptMulChannel},
        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, EV_REPORT_SECURE_V0_CAPABILITY_EP_NOT_OK, ST_AWAIT_ASSIGN_ROUTE_COMPL, &AC_SendAssignReturnRoute},
        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, EV_ALL_EP_REPORT_SECURE_V0_CAPABILITY_NOT_OK, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},
        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, EV_EP_NOT_SUPPORT_CC_SECURE_V0, ST_AWAIT_CAP_SECURE_V0_REPORT_OF_NEXT_EP, &AC_GetS0CapEptMulChannel},
        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, EV_TIMEOUT_STATE, ST_AWAIT_ASSIGN_ROUTE_COMPL, &AC_SendAssignReturnRoute},
        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, EV_ALL_EP_REPORT_SECURE_V0_CAPABILITY_OK, ST_AWAIT_ASSIGN_ROUTE_COMPL, &AC_SendAssignReturnRoute},

        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_NEXT_EP, EV_REPORT_SECURE_V0_CAPABILITY_EP_OK, ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, &AC_GetS0CapEptMulChannel},
        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_NEXT_EP, EV_EP_SUPPORT_CC_SECURE_V2, ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, &AC_GetS0CapEptMulChannel},
        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_NEXT_EP, EV_REPORT_SECURE_V0_CAPABILITY_EP_NOT_OK, ST_AWAIT_ASSIGN_ROUTE_COMPL, &AC_SendAssignReturnRoute},
        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_NEXT_EP, EV_ALL_EP_REPORT_SECURE_V0_CAPABILITY_NOT_OK, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},
        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_NEXT_EP, EV_EP_NOT_SUPPORT_CC_SECURE_V0, ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, &AC_GetS0CapEptMulChannel},
        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_NEXT_EP, EV_TIMEOUT_STATE, ST_AWAIT_ASSIGN_ROUTE_COMPL, &AC_SendAssignReturnRoute},
        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_NEXT_EP, EV_ALL_EP_REPORT_SECURE_V0_CAPABILITY_OK, ST_AWAIT_ASSIGN_ROUTE_COMPL, &AC_SendAssignReturnRoute},

        {ST_AWAIT_ASSOCIATION_COMPL, EV_SET_ASSOCIATION_COMPL_OK, ST_INTERVIEW_DEVICE_SUCCESS, &AC_InterviewDeviceSuccess},
        {ST_AWAIT_ASSOCIATION_COMPL, EV_NOT_SUPPORT_CC_ASSOCIATION, ST_INTERVIEW_DEVICE_SUCCESS, &AC_InterviewDeviceSuccess},
        {ST_AWAIT_ASSOCIATION_COMPL, EV_SET_ASSOCIATION_COMPL_NOT_OK, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},
        {ST_AWAIT_ASSOCIATION_COMPL, EV_TIMEOUT_STATE, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},

        {ST_AWAIT_ASSIGN_ROUTE_COMPL, EV_ASSIGN_RETURN_ROUTE_COMPL_OK, ST_AWAIT_SET_WAKEUP_COMPL, &AC_SetWakeup_Interval},
        {ST_AWAIT_ASSIGN_ROUTE_COMPL, EV_NOT_SUPPORT_CC_ASSIGN_ROUTE, ST_AWAIT_SET_WAKEUP_COMPL, &AC_SetWakeup_Interval},
        /*TODO  
        {ST_AWAIT_ASSIGN_ROUTE_COMPL,           EV_ASSIGN_RETURN_ROUTE_NOT_OK,      ST_INTERVIEW_DEVICE_FAIL,               &AC_InterviewDeviceFail},
        */
        {ST_AWAIT_ASSIGN_ROUTE_COMPL, EV_ASSIGN_RETURN_ROUTE_NOT_OK, ST_AWAIT_SET_WAKEUP_COMPL, &AC_SetWakeup_Interval},
        {ST_AWAIT_ASSIGN_ROUTE_COMPL, EV_TIMEOUT_STATE, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},

        {ST_AWAIT_SET_WAKEUP_COMPL, EV_WAKEUP_SET_COMPL_OK, ST_AWAIT_MANU_SPECIFIC_REPORT, &AC_GetManufacturerSpecific},
        {ST_AWAIT_SET_WAKEUP_COMPL, EV_NOT_SUPPORT_CC_WAKEUP, ST_AWAIT_MANU_SPECIFIC_REPORT, &AC_GetManufacturerSpecific},
        {ST_AWAIT_SET_WAKEUP_COMPL, EV_WAKEUP_SET_COMPL_NOT_OK, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},
        {ST_AWAIT_SET_WAKEUP_COMPL, EV_TIMEOUT_STATE, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},

        {ST_AWAIT_MANU_SPECIFIC_REPORT, EV_MANU_SPECIFIC_REPORT_OK, ST_AWAIT_ASSOCIATION_COMPL, &AC_SetAssociation},
        {ST_AWAIT_MANU_SPECIFIC_REPORT, EV_NOT_SUPPORT_CC_MANU_SPECIFIC, ST_AWAIT_ASSOCIATION_COMPL, &AC_SetAssociation},
        {ST_AWAIT_MANU_SPECIFIC_REPORT, EV_MANU_SPECIFIC_REPORT_NOT_OK, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},
        {ST_AWAIT_MANU_SPECIFIC_REPORT, EV_TIMEOUT_TIME_1, ST_AWAIT_MANU_SPECIFIC_REPORT_TIME_2, &AC_GetManufacturerSpecific},

        {ST_AWAIT_MANU_SPECIFIC_REPORT_TIME_2, EV_MANU_SPECIFIC_REPORT_OK, ST_AWAIT_ASSOCIATION_COMPL, &AC_SetAssociation},
        {ST_AWAIT_MANU_SPECIFIC_REPORT_TIME_2, EV_MANU_SPECIFIC_REPORT_NOT_OK, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},
        {ST_AWAIT_MANU_SPECIFIC_REPORT_TIME_2, EV_TIMEOUT_STATE, ST_INTERVIEW_DEVICE_FAIL, &AC_InterviewDeviceFail},

        {ST_INTERVIEW_DEVICE_SUCCESS, EV_INTERVIEW_DEVICE_FINISH, ST_INTERVIEW_IDLE, &AC_FinishInterviewDevice},
        {ST_INTERVIEW_DEVICE_FAIL, EV_INTERVIEW_DEVICE_FINISH, ST_INTERVIEW_IDLE, &AC_FinishInterviewDevice},

};
#define TRANS_COUNT (sizeof(trans) / sizeof(*trans))

/* States can have a default timeout. If the FSM remains in the same
 * state for the specified duration, the specified callback function
 * will be called.
 * Format: {STATE, timeout in milliseconds, callback_function}
 * Only states listed here have timeouts */
static state_timeout_t timeoutTable[] =
    {
        {ST_AWAIT_ZWAVEPLUS_INFO_REPORT, 10000, ZCB_TimerOutStateInterviewDevice},
        {ST_AWAIT_ASSOCIATION_COMPL, 10000, ZCB_TimerOutStateInterviewDevice},
        {ST_AWAIT_ASSIGN_ROUTE_COMPL, 20000, ZCB_TimerOutStateInterviewDevice},
        {ST_AWAIT_MULTICHANNEL_REPORT, 10000, ZCB_TimerOutStateInterviewDevice},
        {ST_AWAIT_CAP_REPORT_OF_EP, 10000, ZCB_TimerOutStateInterviewDevice},
        {ST_AWAIT_CAP_REPORT_OF_NEXT_EP, 10000, ZCB_TimerOutStateInterviewDevice},
        {ST_AWAIT_SET_WAKEUP_COMPL, 10000, ZCB_TimerOutStateInterviewDevice},
        {ST_AWAIT_MANU_SPECIFIC_REPORT, 10000, ZCB_TimerOutStateGetManu},
        {ST_AWAIT_MANU_SPECIFIC_REPORT_TIME_2, 10000, ZCB_TimerOutStateInterviewDevice},
        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP, 10000, ZCB_TimerOutStateInterviewDevice},
        {ST_AWAIT_CAP_SECURE_V0_REPORT_OF_NEXT_EP, 10000, ZCB_TimerOutStateInterviewDevice},
        {ST_AWAIT_CAP_SECURE_V2_REPORT_OF_EP, 10000, ZCB_TimerOutStateInterviewDevice},
        {ST_AWAIT_CAP_SECURE_V2_REPORT_OF_NEXT_EP, 10000, ZCB_TimerOutStateInterviewDevice},

};
#define TIMEOUT_TABLE_COUNT (sizeof(timeoutTable) / sizeof(*timeoutTable))

static char *GetStateTypeStr(uint8_t st)
{

    char *stStr;

    switch (st)
    {
    case ST_INTERVIEW_IDLE:
        stStr = "ST Interview Idle";
        break;
    case ST_AWAIT_ZWAVEPLUS_INFO_REPORT:
        stStr = "ST Await ZwavePlus Info Report";
        break;
    case ST_AWAIT_ASSOCIATION_COMPL:
        stStr = "ST Await Association Complete";
        break;
    case ST_AWAIT_ASSIGN_ROUTE_COMPL:
        stStr = "ST Await Assign Return Route Complete";
        break;
    case ST_AWAIT_MULTICHANNEL_REPORT:
        stStr = "ST Await Multichannel Report";
        break;
    case ST_AWAIT_CAP_REPORT_OF_EP:
        stStr = "ST Await Capability Report of Endpoint";
        break;
    case ST_AWAIT_CAP_REPORT_OF_NEXT_EP:
        stStr = "ST Await Capability Report of Next Endpoint";
        break;
    case ST_AWAIT_SET_WAKEUP_COMPL:
        stStr = "ST Await Set Wakeup Interval Complete";
        break;
    case ST_AWAIT_MANU_SPECIFIC_REPORT:
        stStr = "ST Await Manufacturer Specific Report";
        break;
    case ST_INTERVIEW_DEVICE_SUCCESS:
        stStr = "ST Interview Device Success";
        break;
    case ST_INTERVIEW_DEVICE_FAIL:
        stStr = "ST Interview Device Fail";
        break;
    case ST_AWAIT_MANU_SPECIFIC_REPORT_TIME_2:
        stStr = "ST Await Manufacturer Specific Report Time 2";
        break;
    case ST_AWAIT_CAP_SECURE_V0_REPORT_OF_EP:
        stStr = "ST Await Get Capability Secure 0 of Endpoint";
        break;
    case ST_AWAIT_CAP_SECURE_V0_REPORT_OF_NEXT_EP:
        stStr = "ST Await Get Capability Secure 0 of Next Endpoint";
        break;
    case ST_AWAIT_CAP_SECURE_V2_REPORT_OF_EP:
        stStr = "ST Await Get Capability Secure 2 of Endpoint";
        break;
    case ST_AWAIT_CAP_SECURE_V2_REPORT_OF_NEXT_EP:
        stStr = "ST Await Get Capability Secure 2 of Next Endpoint";
        break;
    default:
        stStr = " ";
        break;
    }

    return stStr;
}

static char *GetEventTypeStr(uint8_t ev)
{

    char *evStr;
    switch (ev)
    {
    case EV_ADD_NODE_OK:
        evStr = "EV Add Node OK";
        break;
    case EV_CLOSE_NWK:
        evStr = "EV Close Network";
        break;
    case EV_ZWAVEPLUS_INFO_REPORT_OK:
        evStr = "EV ZwavePlus Info Report OK";
        break;
    case EV_NOT_SUPPORT_CC_ZWAVEPLUS:
        evStr = "EV Not Support CC ZwavePlus Info";
        break;
    case EV_ZWAVEPLUS_INFO_REPORT_NOT_OK:
        evStr = "EV ZwavePlus Info Report Not OK";
        break;
    case EV_SET_ASSOCIATION_COMPL_OK:
        evStr = "EV Set Association Complete OK";
        break;
    case EV_NOT_SUPPORT_CC_ASSOCIATION:
        evStr = "EV Not Support CC Association";
        break;
    case EV_SET_ASSOCIATION_COMPL_NOT_OK:
        evStr = "EV Set Association Complete Not OK";
        break;
    case EV_ASSIGN_RETURN_ROUTE_COMPL_OK:
        evStr = "EV Assign Return Route Complete OK";
        break;
    case EV_NOT_SUPPORT_CC_ASSIGN_ROUTE:
        evStr = "EV Not Support Assign Return Route";
        break;
    case EV_ASSIGN_RETURN_ROUTE_NOT_OK:
        evStr = "EV Assign Return Route Complete Not OK";
        break;
    case EV_RECIEVE_END_POINT_REPORT_OK:
        evStr = "EV Receive Multi Channel Endpoint Report OK";
        break;
    case EV_NOT_SUPPORT_CC_MULTI_CHANNEL:
        evStr = "EV Not Support CC Multi Channel";
        break;
    case EV_RECIEVE_END_POINT_REPORT_NOT_OK:
        evStr = "EV Receive Multi Channel Endpoint Report Not OK";
        break;

    case EV_REPORT_CAPABILITY_EP_OK:
        evStr = "EV Receive Report capability Endpoint OK";
        break;
    case EV_REPORT_CAPABILITY_EP_NOT_OK:
        evStr = "EV Receive Report capability Endpoint Not OK";
        break;
    case EV_ALL_EP_REPORT_OK:
        evStr = "EV Multichannel All Endpoint Report OK";
        break;
    case EV_ALL_EP_REPORT_NOT_OK:
        evStr = "EV Multichannel All Endpoint Report Not OK";
        break;
    case EV_WAKEUP_SET_COMPL_OK:
        evStr = "EV Set Wakeup Interval Complete OK";
        break;
    case EV_NOT_SUPPORT_CC_WAKEUP:
        evStr = "EV Not Support CC Wake Up";
        break;
    case EV_WAKEUP_SET_COMPL_NOT_OK:
        evStr = "EV Set Wakeup Interval Complete OK";
        break;
    case EV_MANU_SPECIFIC_REPORT_OK:
        evStr = "EV Manufacturer Specific Report OK";
        break;
    case EV_NOT_SUPPORT_CC_MANU_SPECIFIC:
        evStr = "EV Not Support CC Manufacturer Specific";
        break;
    case EV_MANU_SPECIFIC_REPORT_NOT_OK:
        evStr = "EV Set Wakeup Interval Complete OK";
        break;
    case EV_INTERVIEW_DEVICE_FINISH:
        evStr = "EV Finish Interview Device";
        break;
    case EV_TIMEOUT_STATE:
        evStr = "EV Timer Out State Interview Device";
        break;
    case EV_REPORT_SECURE_V0_CAPABILITY_EP_OK:
        evStr = "EV_REPORT_SECURE_V0_CAPABILITY_EP_OK";
        break;
    case EV_REPORT_SECURE_V0_CAPABILITY_EP_NOT_OK:
        evStr = "EV_REPORT_SECURE_V0_CAPABILITY_EP_NOT_OK";
        break;
    case EV_REPORT_SECURE_V2_CAPABILITY_EP_OK:
        evStr = "EV_REPORT_SECURE_V2_CAPABILITY_EP_OK";
        break;
    case EV_REPORT_SECURE_V2_CAPABILITY_EP_NOT_OK:
        evStr = "EV_REPORT_SECURE_V2_CAPABILITY_EP_NOT_OK";
        break;
    case EV_ALL_EP_REPORT_SECURE_V0_CAPABILITY_OK:
        evStr = "EV_ALL_EP_REPORT_SECURE_V0_CAPABILITY_OK";
        break;
    case EV_ALL_EP_REPORT_SECURE_V0_CAPABILITY_NOT_OK:
        evStr = "EV_ALL_EP_REPORT_SECURE_V0_CAPABILITY_NOT_OK";
        break;
    case EV_ALL_EP_REPORT_SECURE_V2_CAPABILITY_OK:
        evStr = "EV_ALL_EP_REPORT_SECURE_V2_CAPABILITY_OK";
        break;
    case EV_ALL_EP_REPORT_SECURE_V2_CAPABILITY_NOT_OK:
        evStr = "EV_ALL_EP_REPORT_SECURE_V2_CAPABILITY_NOT_OK";
        break;
    case EV_EP_NOT_SUPPORT_CC_SECURE_V0:
        evStr = "EV_EP_NOT_SUPPORT_CC_SECURE_V0";
        break;
    case EV_EP_NOT_SUPPORT_CC_SECURE_V2:
        evStr = "EV_EP_NOT_SUPPORT_CC_SECURE_V2";
        break;
    case EV_EP_SUPPORT_CC_SECURE_V2:
        evStr = "EV_EP_SUPPORT_CC_SECURE_V2";
        break;
    default:
        evStr = " ";
        break;
    }
    return evStr;
}

/*============================   StopTimer   ===============================
**
**  Cancels the FSM timer.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/
static void StopTimerInterviewDevice()
{
    if (bFsmTimerHandleInterviewDevice != NO_TIMER)
    {
        timerCancel(&bFsmTimerHandleInterviewDevice);
        bFsmTimerHandleInterviewDevice = NO_TIMER;
    }
}

/*===========================   ZCB_TimerInterviewDevice   ==============================
**
**  Timer callback function for the default FSM timer.
**  If a timeout and a callback function is specified in the
**  timeoutTable, ths callback function will be called automatically
**  by this timer function.
**
**  The callback function will only be called if no state change has occurred
**  within the specified timeout since entering the current state.
**
**  Side effects: None
**
**  If Timeout, this function is executed
**-------------------------------------------------------------------------*/
void ZCB_TimerInterviewDevice(void *data)
{
    mainlog(logDebug, "Countdown Interview Device: %u", bFsmTimerCountdownInterview);
    if (bFsmTimerCountdownInterview > 0)
    {
        bFsmTimerCountdownInterview--;
        return;
    }
    /* The timer repeat count should already have stopped the timer,
    * but better safe than sorry. */
    StopTimerInterviewDevice();
    if (cbFsmTimerInterviewDevice != NULL) //pointer function
    {
        cbFsmTimerInterviewDevice(data); //function
    }
}

/*============================   StartTimerFsmInterviewDevice   ===============================
**
**  Start the FSM timer. Note the higher resolution if timeout is
**  shorter than or equal to 2.55 seconds.
**
**  Side effects: Cancels running FSM timer if any.
**
**-------------------------------------------------------------------------*/
static void StartTimerFsmInterviewDevice(
    uint16_t timeout,       /* IN Unit: 10 ms ticks. If over 255, resolution is 1 second */
    void (*cbFunc)(void *)) /* IN timeout callback function */
{
    StopTimerInterviewDevice();
    int ret = 0;
    if (timeout > 255)
    {
        /* Timeout larger than single timeout. */
        /* Convert timeout from 10 ms ticks to seconds*/
        bFsmTimerCountdownInterview = timeout / 1000;
        cbFsmTimerInterviewDevice = cbFunc;
        ret = timerStart(&bFsmTimerHandleInterviewDevice, ZCB_TimerInterviewDevice, NULL, 1000, TIMER_FOREVER);
    }
    else
    {
        /* timeout is in range 10..2550 ms */
        ret = timerStart(&bFsmTimerHandleInterviewDevice, cbFunc, NULL, timeout * 10, TIMER_ONETIME);
    }

    if (ret)
    {
        mainlog(logDebug, "Failed to get timer for FsmTimer +++++");
    }
}

/*=========================   auto_add_paraStartStateTimerInterview   ============================
**
**  Find and start the state timer for current state, if one has been
**  defined in timeoutTable.
**
**  Side effects: Cancels the currently running state timer.
**
**-------------------------------------------------------------------------*/
static void StartStateTimerInterview()
{
    uint8_t i;
    for (i = 0; i < TIMEOUT_TABLE_COUNT; i++)
    {
        if (state_interview == timeoutTable[i].st)
        {
            StartTimerFsmInterviewDevice(timeoutTable[i].timeout, timeoutTable[i].tfn);
            mainlog(logDebug, "Start State Timer Interview: state %s,  index %d", GetStateTypeStr(state_interview), i);
            if (timeoutTable[i].tfn == NULL)
            {
                mainlog(logDebug, "Start State Timer Interview: NULL function");
            }
        }
    }
}

/*============================   InterviewDevice_ZCB_PostEvent   ===============================
**
**  Post an event to the finite state machine. Update the state,
**  run action function associated with the transition and start
**  the state timer for the next state.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/
void PostEventInterviewDevice(INTERVIEW_DEVICE_FSM_EV_T event)
{
    INTERVIEW_DEVICE_FSM_STATE_T old_state;
    uint8_t i;
    for (i = 0; i < TRANS_COUNT; i++)
    {
        if ((state_interview == trans[i].st)) //have state none support
        {
            if ((event == trans[i].ev))
            {
                old_state = trans[i].st;

                state_interview = trans[i].next_st;

                mainlog(logDebug, "----InterviewDevice_PostEvent: %s [%u]-----", GetEventTypeStr(event), event);
                mainlog(logDebug, "----%s [%u] ---> %s [%u]---------------",
                        GetStateTypeStr(old_state), old_state, GetStateTypeStr(state_interview), state_interview);
                if (old_state != state_interview)
                {
                    StopTimerInterviewDevice();
                    StartStateTimerInterview();
                }
                if ((trans[i].fn) != NULL)
                {
                    (trans[i].fn());
                }
                break;
            }
        }
    }
}

//----------------Action of Interview device process------------------
void AC_SendZwavePlusInfo()
{
    if ((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC,
                                             COMMAND_CLASS_ZWAVEPLUS_INFO)) != SCHEME_NOT_SUPPORT)
    {
        ts_param_t p;
        p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
        p.scheme = schemeGet;

        p.snode = MyNodeId;
        p.sendpoint = 0;

        p.dnode = lastLearnedNodeZPC.node_id;
        p.dendpoint = 0;

        pTxBuf.ZW_ZwaveplusInfoGetFrame.cmdClass = COMMAND_CLASS_ZWAVEPLUS_INFO; /* The command class */
        pTxBuf.ZW_ZwaveplusInfoGetFrame.cmd = ZWAVEPLUS_INFO_GET;                /* The command */

        Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                                 sizeof(ZW_ZWAVEPLUS_INFO_GET_FRAME), &p, NULL, NULL);
        return;
    }
    else //not support this cmd
    {
        PostEventInterviewDevice(EV_NOT_SUPPORT_CC_ZWAVEPLUS);
    }
}

void AC_SetAssociation()
{
    if ((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC,
                                             COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION_V2)) != SCHEME_NOT_SUPPORT)
    {
        MySystemSUCId = serialApiGetSUCNodeID();
        uint8_t targetNodeId = MySystemSUCId ? MySystemSUCId : MyNodeId;
        mainlog(logDebug, "multi association targetNodeId:%02X", targetNodeId);
        ts_param_t p;
        p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
        p.scheme = schemeGet;

        p.snode = MyNodeId;
        p.sendpoint = 0;

        p.dnode = lastLearnedNodeZPC.node_id;
        p.dendpoint = 0;
        pTxBuf.ZW_MultiChannelAssociationSet1byteV2Frame.cmdClass = COMMAND_CLASS_MULTI_CHANNEL_ASSOCIATION_V2; /* The command class */
        pTxBuf.ZW_MultiChannelAssociationSet1byteV2Frame.cmd = MULTI_CHANNEL_ASSOCIATION_SET_V2;                /* The command */
        pTxBuf.ZW_MultiChannelAssociationSet1byteV2Frame.groupingIdentifier = ASSOCIATION_GROUP_INFO_REPORT_PROFILE_GENERAL_LIFELINE;
        pTxBuf.ZW_MultiChannelAssociationSet1byteV2Frame.nodeId1 = targetNodeId;
        pTxBuf.ZW_MultiChannelAssociationSet1byteV2Frame.marker = MULTI_CHANNEL_ASSOCIATION_SET_MARKER_V2;

        Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                                 sizeof(ZW_MULTI_CHANNEL_ASSOCIATION_SET_1BYTE_V2_FRAME) - 2, /*not need set endpoint*/
                                 &p, SetAssociation_Compl, 0);
        return;
    }
    else if ((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC,
                                                  COMMAND_CLASS_ASSOCIATION)) != SCHEME_NOT_SUPPORT)
    {
        MySystemSUCId = serialApiGetSUCNodeID();
        uint8_t targetNodeId = MySystemSUCId ? MySystemSUCId : MyNodeId;
        mainlog(logDebug, "multi association targetNodeId:%02X", targetNodeId);
        ts_param_t p;
        p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
        p.scheme = schemeGet;

        p.snode = MyNodeId;
        p.sendpoint = 0;

        p.dnode = lastLearnedNodeZPC.node_id;
        p.dendpoint = 0;

        pTxBuf.ZW_AssociationSet1byteFrame.cmdClass = COMMAND_CLASS_ASSOCIATION; /* The command class */
        pTxBuf.ZW_AssociationSet1byteFrame.cmd = ASSOCIATION_SET_V2;             /* The command */
        pTxBuf.ZW_AssociationSet1byteFrame.groupingIdentifier = ASSOCIATION_GROUP_INFO_REPORT_PROFILE_GENERAL_LIFELINE;
        pTxBuf.ZW_AssociationSet1byteFrame.nodeId1 = targetNodeId;

        Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                                 sizeof(ZW_ASSOCIATION_SET_1BYTE_FRAME),
                                 &p, SetAssociation_Compl, 0);
        return;
    }
    else //not support this cmd
    {
        PostEventInterviewDevice(EV_NOT_SUPPORT_CC_ASSOCIATION);
    }
}

//------------AC_SetMultiChannelAssociation GroupId = 0x01, nodeId = 0x01, for all enpoint--------------//

void AC_SendAssignReturnRoute()
{
    /*If the node was added by me then set the  assign return route
    Not need to set Assign SUC return route because controller automatically does that 
    */
    /*if ((lastLearnedNodeZPC.node_info.nodeType.basic != BASIC_TYPE_CONTROLLER) &&
        (lastLearnedNodeZPC.node_info.nodeType.basic != BASIC_TYPE_STATIC_CONTROLLER))*/
    if (1)
    {
        serialApiAssignReturnRoute(lastLearnedNodeZPC.node_id, MyNodeId, AssignReturnRoute_Compl);
        return;
    }
    else //not support this cmd
    {
        PostEventInterviewDevice(EV_NOT_SUPPORT_CC_ASSIGN_ROUTE);
    }
}

void AC_SendGetMultiChannel()
{
    if ((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC,
                                             COMMAND_CLASS_MULTI_CHANNEL_V3)) != SCHEME_NOT_SUPPORT)
    {
        ts_param_t p;
        p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
        p.scheme = schemeGet;

        p.snode = MyNodeId;
        p.sendpoint = 0;

        p.dnode = lastLearnedNodeZPC.node_id;
        p.dendpoint = 0;

        /* reset target endpoint */
        targetEndpoint = 1;

        pTxBuf.ZW_MultiChannelEndPointGetV3Frame.cmdClass = COMMAND_CLASS_MULTI_CHANNEL_V3; /* The command class */
        pTxBuf.ZW_MultiChannelEndPointGetV3Frame.cmd = MULTI_CHANNEL_END_POINT_GET_V3;      /* The command */
        Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf, sizeof(ZW_MULTI_CHANNEL_END_POINT_GET_V3_FRAME),
                                 &p, NULL, NULL);
        return;
    }
    else //not support this cmd
    {
        PostEventInterviewDevice(EV_NOT_SUPPORT_CC_MULTI_CHANNEL);
    }
}

void AC_GetCapEptMulChannel()
{
    //check send to all endpoint
    if (targetEndpoint > (lastLearnedNodeZPC.no_endpoint + lastLearnedNodeZPC.no_agg_endpoint))
    {
        mainlog(logDebug, "MULTI_CHANNEL_CAPABILITY_REPORT_V3_ALL_EP --> OK");
        if (lastLearnedNodeZPC.no_agg_endpoint > 0)
        {
            targetAggEndpoint = 1;
            PostEventInterviewDevice(EV_AWAIT_MEMEBER_REPORT_OF_AGG_EP);
            return;
        }
        /* reset target endpoint */
        targetEndpoint = 1;
        PostEventInterviewDevice(EV_ALL_EP_REPORT_OK);
    }
    else //check send get capability to all enpoint
    {
        schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC,
                                            COMMAND_CLASS_MULTI_CHANNEL_V3);
        ts_param_t p;
        p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
        p.scheme = schemeGet;

        p.snode = MyNodeId;
        p.sendpoint = 0;

        p.dnode = lastLearnedNodeZPC.node_id;
        p.dendpoint = 0;

        pTxBuf.ZW_MultiChannelCapabilityGetV3Frame.cmdClass = COMMAND_CLASS_MULTI_CHANNEL_V3;
        pTxBuf.ZW_MultiChannelCapabilityGetV3Frame.cmd = MULTI_CHANNEL_CAPABILITY_GET_V3;
        pTxBuf.ZW_MultiChannelCapabilityGetV3Frame.properties1 = targetEndpoint;

        Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                                 sizeof(ZW_MULTI_CHANNEL_CAPABILITY_GET_V3_FRAME), &p, NULL, NULL);
    }
}

void requestGetS2CapEptMulChannel_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t)
{
    mainlog(logDebug, "requestGetS2CapEptMulChannel_Compl -> status: %02X", bTxStatus);
    switch (bTxStatus)
    {
    case TRANSMIT_COMPLETE_OK:
    case TRANSMIT_COMPLETE_NO_ACK:
    case TRANSMIT_COMPLETE_FAIL:
    default:
        break;
    }
}
void AC_GetS0CapEptMulChannel()
{
    //check send to all endpoint
    if (targetEndpoint > (lastLearnedNodeZPC.no_endpoint + lastLearnedNodeZPC.no_agg_endpoint))
    {
        mainlog(logDebug, "EV_ALL_EP_REPORT_SECURE_V0_CAPABILITY --> OK");
        if (lastLearnedNodeZPC.no_agg_endpoint > 0)
        {
            targetAggEndpoint = 1;
        }
        /* reset target endpoint */
        targetEndpoint = 1;
        PostEventInterviewDevice(EV_ALL_EP_REPORT_SECURE_V0_CAPABILITY_OK);
    }
    else //check send get capability to all enpoint
    {
        mainlog(logDebug, "GET_SECURE_V0_CAPABILITY_TARGET_ENPOINT_%d", targetEndpoint);
        if (((schemeGet = isEndpointSupportedCommandClass(&lastLearnedNodeZPC, targetEndpoint, COMMAND_CLASS_SECURITY)) != SCHEME_NOT_SUPPORT) &&
            (GetCacheEntryFlag(MyNodeId) & COMMAND_CLASS_SECURITY))
        {
            if ((schemeGet = isEndpointSupportedCommandClass(&lastLearnedNodeZPC, targetEndpoint, COMMAND_CLASS_SECURITY_2)) != SCHEME_NOT_SUPPORT)
            {
                targetEndpoint++;
                PostEventInterviewDevice(EV_EP_SUPPORT_CC_SECURE_V2);
                return;
            }
            else
            {
                ts_param_t p;
                p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
                p.scheme = AUTO_SCHEME;

                p.snode = MyNodeId;
                p.sendpoint = 0;

                p.dnode = lastLearnedNodeZPC.node_id;
                p.dendpoint = targetEndpoint;

                pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmdClass = COMMAND_CLASS_SECURITY; /* The command class */
                pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmd = SECURITY_COMMANDS_SUPPORTED_GET;

                Transport_SendDataApplEP((uint8_t *)&pTxBuf,
                                         sizeof(ZW_ZWAVEPLUS_INFO_GET_FRAME), &p, requestGetS2CapEptMulChannel_Compl, NULL);
                return;
            }
        }
        else
        {
            targetEndpoint++;
            PostEventInterviewDevice(EV_EP_NOT_SUPPORT_CC_SECURE_V0);
        }
    }
}

void AC_GetS2CapEptMulChannel()
{
    //check send to all endpoint
    if (targetEndpoint > (lastLearnedNodeZPC.no_endpoint + lastLearnedNodeZPC.no_agg_endpoint))
    {
        mainlog(logDebug, "EV_ALL_EP_REPORT_SECURE_V2_CAPABILITY --> OK");

        targetEndpoint = 1;
        PostEventInterviewDevice(EV_ALL_EP_REPORT_SECURE_V2_CAPABILITY_OK);
    }
    else //check send get capability to all enpoint
    {
        mainlog(logDebug, "GET_SECURE_V2_CAPABILITY_TARGET_ENPOINT_%d", targetEndpoint);
        if (((schemeGet = isEndpointSupportedCommandClass(&lastLearnedNodeZPC, targetEndpoint, COMMAND_CLASS_SECURITY_2)) != SCHEME_NOT_SUPPORT) &&
            (GetCacheEntryFlag(MyNodeId) & NODE_FLAGS_SECURITY2))
        {
            ts_param_t p;
            p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
            p.scheme = AUTO_SCHEME;

            p.snode = MyNodeId;
            p.sendpoint = 0;

            p.dnode = lastLearnedNodeZPC.node_id;
            p.dendpoint = targetEndpoint;

            pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmdClass = COMMAND_CLASS_SECURITY_2;     /* The command class */
            pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmd = SECURITY_2_COMMANDS_SUPPORTED_GET; /* The command */

            Transport_SendDataApplEP((uint8_t *)&pTxBuf,
                                     sizeof(ZW_ZWAVEPLUS_INFO_GET_FRAME), &p, requestGetS2CapEptMulChannel_Compl, NULL);

            return;
        }
        else
        {
            targetEndpoint++;
            PostEventInterviewDevice(EV_EP_NOT_SUPPORT_CC_SECURE_V2);
        }
    }
}
void AC_GetMemberAggEptMulChannel()
{
    //check send to all aggregated endpoint
    if (targetAggEndpoint > lastLearnedNodeZPC.no_agg_endpoint)
    {
        mainlog(logDebug, "MULTI_CHANNEL_AGGREGATED_MEMBERS_REPORT ALL_EP --> OK");
        PostEventInterviewDevice(EV_ALL_EP_REPORT_OK);
    }
    else //check send get member of aggregated enpoints
    {
        schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC,
                                            COMMAND_CLASS_MULTI_CHANNEL_V3);
        ts_param_t p;
        p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
        p.scheme = schemeGet;

        p.snode = MyNodeId;
        p.sendpoint = 0;

        p.dnode = lastLearnedNodeZPC.node_id;
        p.dendpoint = 0;

        pTxBuf.ZW_MultiChannelCapabilityGetV3Frame.cmdClass = COMMAND_CLASS_MULTI_CHANNEL_V3;
        pTxBuf.ZW_MultiChannelCapabilityGetV3Frame.cmd = MULTI_CHANNEL_AGGREGATED_MEMBERS_GET_V4;
        pTxBuf.ZW_MultiChannelCapabilityGetV3Frame.properties1 = targetAggEndpoint + lastLearnedNodeZPC.no_endpoint;

        Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                                 sizeof(ZW_MULTI_CHANNEL_AGGREGATED_MEMBERS_GET_V4_FRAME), &p, NULL, NULL);
    }
}

void AC_SetWakeup_Interval()
{
    if ((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC,
                                             COMMAND_CLASS_WAKE_UP)) != SCHEME_NOT_SUPPORT)
    {
        MySystemSUCId = serialApiGetSUCNodeID();

        /*If the node was added by me then set the wakeup interval*/
        /* Three scenarios:
        1. If I am SIS 
                I set WUI and WAKE UP Destination to myself
        2. If I am inclusion controller(not SIS) and SIS supports COMMAND_CLASS_INCLUSION_CONTROLLER,
                I WONT set WUI and WAKE UP Destination
        3. If I inclusion controller (not SIS) and SIS does not support COMMAND_CLASS_INCLUSION_CONTROLLER
                I set WUI and WAKE UP Destination to SIS
        */
        if ((MySystemSUCId == MyNodeId) || !GetSupportedInclusionControllerCmdClass(MySystemSUCId))
        {
            /* in case 1. and 3. */
            if ((lastLearnedNodeZPC.is_zp_node) && (lastLearnedNodeZPC.zp_info.role_type == ROLE_TYPE_SLAVE_PORTABLE))
            {
                /* Set Wake Up Interval = 0  for Portable Slave devices such as remote */
                pTxBuf.ZW_WakeUpIntervalSetV2Frame.seconds1 = 0;
                pTxBuf.ZW_WakeUpIntervalSetV2Frame.seconds2 = 0;
                pTxBuf.ZW_WakeUpIntervalSetV2Frame.seconds3 = 0;
            }
            else
            {
                /* Set Wake Up Interval > 0 for other devices*/
                pTxBuf.ZW_WakeUpIntervalSetV2Frame.seconds1 = (uint8_t)(DEFAULT_WAKE_UP_INTERVAL >> 16);
                pTxBuf.ZW_WakeUpIntervalSetV2Frame.seconds2 = (uint8_t)(DEFAULT_WAKE_UP_INTERVAL >> 8);
                pTxBuf.ZW_WakeUpIntervalSetV2Frame.seconds3 = (uint8_t)(DEFAULT_WAKE_UP_INTERVAL >> 0);
            }

            pTxBuf.ZW_WakeUpIntervalSetV2Frame.cmdClass = COMMAND_CLASS_WAKE_UP;
            pTxBuf.ZW_WakeUpIntervalSetV2Frame.cmd = WAKE_UP_INTERVAL_SET_V2;
            pTxBuf.ZW_WakeUpIntervalSetV2Frame.nodeid = MySystemSUCId;

            ts_param_t p;
            p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
            p.scheme = schemeGet;

            p.snode = MyNodeId;
            p.sendpoint = 0;

            p.dnode = lastLearnedNodeZPC.node_id;
            p.dendpoint = 0;

            Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf, sizeof(ZW_WAKE_UP_INTERVAL_SET_V2_FRAME),
                                     &p, wakeUpSet_Compl, 0);
            return;
        }
        else /* in case 2. */
        {
            PostEventInterviewDevice(EV_NOT_SUPPORT_CC_WAKEUP);
        }
    }
    else //not support this cmd
    {
        PostEventInterviewDevice(EV_NOT_SUPPORT_CC_WAKEUP);
    }
}

void AC_GetManufacturerSpecific()
{
    if ((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC,
                                             COMMAND_CLASS_MANUFACTURER_SPECIFIC)) != SCHEME_NOT_SUPPORT)
    {
        pTxBuf.ZW_ManufacturerSpecificGetFrame.cmdClass = COMMAND_CLASS_MANUFACTURER_SPECIFIC;
        pTxBuf.ZW_ManufacturerSpecificGetFrame.cmd = MANUFACTURER_SPECIFIC_GET;

        ts_param_t p;
        p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
        p.scheme = schemeGet;

        p.snode = MyNodeId;
        p.sendpoint = 0;

        p.dnode = lastLearnedNodeZPC.node_id;
        p.dendpoint = 0;

        Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf, sizeof(ZW_MANUFACTURER_SPECIFIC_GET_FRAME),
                                 &p, NULL, NULL);
        return;
    }
    else //not support this cmd
    {
        PostEventInterviewDevice(EV_NOT_SUPPORT_CC_MANU_SPECIFIC);
    }
}
//--------------------------------------------------------
//call back function of interview device
void SetAssociation_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t)
{
    switch (bTxStatus)
    {
    case TRANSMIT_COMPLETE_OK:
        PostEventInterviewDevice(EV_SET_ASSOCIATION_COMPL_OK);
        break;
    default:
        PostEventInterviewDevice(EV_SET_ASSOCIATION_COMPL_NOT_OK);
        break;
    }
}

void wakeUpSet_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t)
{
    switch (bTxStatus)
    {
    case TRANSMIT_COMPLETE_OK:
        PostEventInterviewDevice(EV_WAKEUP_SET_COMPL_OK);
        break;
    default:
        PostEventInterviewDevice(EV_WAKEUP_SET_COMPL_NOT_OK);
        break;
    }
}

void AssignReturnRoute_Compl(uint8_t bTxStatus)
{
    if (TRANSMIT_COMPLETE_OK == bTxStatus)
    {
        PostEventInterviewDevice(EV_ASSIGN_RETURN_ROUTE_COMPL_OK);
    }
    else
    {
        PostEventInterviewDevice(EV_ASSIGN_RETURN_ROUTE_NOT_OK);
    }
}

void AC_FinishInterviewDevice(void)
{
    PostEventInclusionController(EV_INTERVIEW_DEVICE_OK);
    //change state to remove ready
    //close adding, move to remove ready in auto remove add mode
    if (CLOSE_NETWORK == actionFromUser)
        PostEventAutoRemoveAdd(EV_CLOSE_NETWORK_AT_INTERVIEW);
    else
    {
        if (OPEN_NETWORK == actionFromUser)
            PostEventAutoRemoveAdd(EV_OPEN_NETWORK_AT_INTERVIEW);
        else
        {
            PostEventAutoRemoveAdd(EV_FINISH_INTERVIEW);
        }
    }
    actionFromUser = NO_ACTION_NETWORK;
    
    /*reset */
    nm.mode = NMM_DEFAULT;
    nm.state = NMS_IDLE;
}

void AC_InterviewDeviceFail(void)
{
    StopTimerInterviewDevice();
    ActionWhenInterviewDeviceFailed();
    PostEventInterviewDevice(EV_INTERVIEW_DEVICE_FINISH);
}

void AC_InterviewDeviceSuccess(void)
{
    if (nm.mode == NMM_AUTO_REMOVEADD_NODE)
    {
        StopTimerInterviewDevice();
        NewNode_Notify(ADD_NODE_STATUS_DONE); //push notify add node success
    }
    else if ((nm.mode == NMM_REPLACE_FAILED_NODE) || (nm.mode == NMM_CONTROLLER_CHANGE) || (nm.mode == NMM_ADD_NODE))
    {
        NewNode_Notify(ADD_NODE_STATUS_DONE); //replace fail node is successful
    }
    PostEventInterviewDevice(EV_INTERVIEW_DEVICE_FINISH); //post event add node success
}