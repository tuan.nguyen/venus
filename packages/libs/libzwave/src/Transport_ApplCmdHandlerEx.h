
#ifndef ZW_TRANSPORT_APPLICATIONCOMMANDHANDLEREX_H_
#define ZW_TRANSPORT_APPLICATIONCOMMANDHANDLEREX_H_
#include "serialAPI.h"
#include "cmd_class_misc.h"
#include "VR_define.h"
#include "VR_list.h"

typedef struct handle_duplicate_message
{
    ts_param_t p;
    char old_message_notify[BUF_SIZE];
} handle_duplicate_message_t;

typedef struct handleSequenceNumber
{
    uint8_t sequenceNumber;
    uint16_t extendId;//extendId = endpoint * 256 + nodeId
    struct VR_list_head list;
}handleSequenceNumber_t;

typedef struct handleKeyHeldDownMessage
{
    bool receiveMessageKeyReleased;
    uint8_t  sceneNumber;
    uint8_t sequenceNumber;
    uint8_t keyAyttributes;
    uint16_t timeout;
    timer_t timerId;
    ts_param_t p;
    uint16_t extendId;//extendId = endpoint * 256 + nodeId
    struct VR_list_head list;
}handleKeyHeldDownMessage_t;

/*Transport_ApplicationCommandHandlerEx
    RET received_frame_status_t                  */
received_frame_status_t
Transport_ApplicationCommandHandlerEx(
    ts_param_t *p,     /* IN receive options of type ts_param_t  */
    uint8_t *pCmd,     /* IN  Payload from the received frame */
    uint16_t cmdLength); /*IN secure of Device Node*/
void initKeyHeldDownMessageList(void);
//for handler central scene command class
void removeKeyHeldDownMessageListFromNodeId(uint8_t nodeId);
void removeSequenceNumberFromNodeId(uint8_t nodeId);
void removeAllSequenceNumberFromList(void);
void removeAllKeyHeldDownMessageList(void);
#endif /* ZW_TRANSPORT_APPLICATIONCOMMANDHANDLEREX_H_ */
