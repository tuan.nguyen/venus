#include "supported_CommandClassAssociationGroupInfo.h"
#include "agi.h"
#include "serialAPI.h"
#include "utils.h"
#include "cmd_class_misc.h"


#define REPORT_ONE_GROUP 1
#define REPORT_ALL_GROUPS 2

static uint8_t currentGroupId;
static uint8_t grInfoStatus = false;

static void (*cbFuncResponseJobStatus)(uint8_t txStatus);
static timer_t AGIReportSendHandle = 0;
static ts_param_t rxOptionsEx;


void ZCB_AGIReport(uint8_t txStatus);
void SendAssoGroupInfoReport(ts_param_t *rxOpt);


void  associationGroupInfoResponse_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t)
{
    mainlog(logDebug, "associationGroupInfoResponse_Compl\r\n");
    if (TRANSMIT_COMPLETE_OK == bTxStatus)
    {
        mainlog(logDebug, "TRANSMIT_COMPLETE_OK\r\n"); 
    }
    else
    {
        mainlog(logDebug, "TRANSMIT_COMPLETE_NOT_OK\r\n"); 
    }

    if (cbFuncResponseJobStatus!=NULL)
    {
        cbFuncResponseJobStatus(bTxStatus);
    }
}

/****************************************************************************/
/*                            PRIVATE FUNCTIONS                             */
/****************************************************************************/

void ZCB_AGIReportSendTimer(void *data)
{
    timerCancel(&AGIReportSendHandle);
    SendAssoGroupInfoReport(&rxOptionsEx);
}

void ZCB_AGIReport(uint8_t txStatus)
{
    if (grInfoStatus == REPORT_ALL_GROUPS)
    {
        if (currentGroupId++ < GetApplAssoGroupsSize())
        {
            timerStart(&AGIReportSendHandle, ZCB_AGIReportSendTimer, NULL, 1000, TIMER_ONETIME);
            return;
        }
    }
    grInfoStatus = false;
}

void SendAssoGroupInfoReport(ts_param_t *p)
{
    cbFuncResponseJobStatus=ZCB_AGIReport;
    ts_param_t pTxOptionsEx;
    RxToTxOptions(p, &pTxOptionsEx);

    pTxBuf.ZW_AssociationGroupInfoReport1byteFrame.cmdClass = COMMAND_CLASS_ASSOCIATION_GRP_INFO;
    pTxBuf.ZW_AssociationGroupInfoReport1byteFrame.cmd      = ASSOCIATION_GROUP_INFO_REPORT;

    /*If thelist mode bit is set in the get frame it should be also set in the report frame.*/
    pTxBuf.ZW_AssociationGroupInfoReport1byteFrame.properties1 = (grInfoStatus == REPORT_ALL_GROUPS)? 
                                    (ASSOCIATION_GROUP_INFO_REPORT_PROPERTIES1_LIST_MODE_BIT_MASK |0x01) : 0x01; /*we send one report per group*/
    GetApplGroupInfo(currentGroupId, &pTxBuf.ZW_AssociationGroupInfoReport1byteFrame.variantgroup1);
    if (false==Transport_SendDataApplEP((uint8_t *)&pTxBuf,
                  sizeof(ZW_ASSOCIATION_GROUP_INFO_REPORT_1BYTE_FRAME),
                  &pTxOptionsEx,
                  associationGroupInfoResponse_Compl, 0))
    {
        /*Job failed, free transmit-buffer pTxBuf by cleaing mutex */
        grInfoStatus = false;

    }
}

received_frame_status_t 
handleCommandClassAssociationGroupInfo(ts_param_t *p,                  
                                        ZW_APPLICATION_TX_BUFFER *pCmd,   
                                        uint8_t   cmdLength)
{
    uint8_t length;
    uint8_t grId;

    if (!SupportsFrameAtSecurityLevel(COMMAND_CLASS_ASSOCIATION_GRP_INFO, p->scheme))
    {
        mainlog(logDebug, "handle AGI CC p->scheme %d SupportsFrame ret 0",p->scheme );
        return RECEIVED_FRAME_STATUS_FAIL;
    }

    switch (pCmd->ZW_Common.cmd)
    {
        case ASSOCIATION_GROUP_NAME_GET:

            if(true == Check_not_legal_response_job(p))
            { 
                /*Get/Report do not support endpoint bit-addressing */
                return RECEIVED_FRAME_STATUS_FAIL;
            }

            if(0 == pCmd->ZW_AssociationGroupNameGetFrame.groupingIdentifier)
            {
                /* error, not legal number!*/
                return RECEIVED_FRAME_STATUS_FAIL;
            }

            if (3 != cmdLength)
            {
                return RECEIVED_FRAME_STATUS_FAIL;
            }

            length = GetApplGroupNameLength(pCmd->ZW_AssociationGroupNameGetFrame.groupingIdentifier);

            if (length != 0)
            {
                ts_param_t pTxOptionsEx;
                RxToTxOptions(p, &pTxOptionsEx);

                cbFuncResponseJobStatus=NULL;
                pTxBuf.ZW_AssociationGroupNameReport1byteFrame.cmdClass = COMMAND_CLASS_ASSOCIATION_GRP_INFO;
                pTxBuf.ZW_AssociationGroupNameReport1byteFrame.cmd      = ASSOCIATION_GROUP_NAME_REPORT;
                grId = pCmd->ZW_AssociationGroupNameGetFrame.groupingIdentifier;
                pTxBuf.ZW_AssociationGroupNameReport1byteFrame.groupingIdentifier =  grId;
                pTxBuf.ZW_AssociationGroupNameReport1byteFrame.lengthOfName = length;
                GetApplGroupName(&(pTxBuf.ZW_AssociationGroupNameReport1byteFrame.name1), grId);

                Transport_SendDataApplEP((uint8_t *)&pTxBuf,
                                        sizeof(ZW_ASSOCIATION_GROUP_NAME_REPORT_1BYTE_FRAME)
                                        - sizeof(uint8_t)
                                        + length,
                                        &pTxOptionsEx,
                                        associationGroupInfoResponse_Compl, 0);

            }

            return RECEIVED_FRAME_STATUS_SUCCESS;

    	case ASSOCIATION_GROUP_INFO_GET:

            if(true == Check_not_legal_response_job(p))
            { 
                /*Get/Report do not support endpoint bit-addressing */
                return RECEIVED_FRAME_STATUS_FAIL;
            }

            /*if we already sending reports ingore more requestes*/
            if (grInfoStatus) 
            {
                return RECEIVED_FRAME_STATUS_FAIL;
            }

            memcpy((uint8_t*)&rxOptionsEx, (uint8_t*)p, sizeof(ts_param_t));

            if (pCmd->ZW_AssociationGroupInfoGetFrame.properties1 & ASSOCIATION_GROUP_INFO_GET_PROPERTIES1_LIST_MODE_BIT_MASK)
            {
                /*if list mode is one then ignore groupid and report information about all the asscoication group one group at a time*/
                grInfoStatus =REPORT_ALL_GROUPS;
                currentGroupId = 1;
            }
            else if (pCmd->ZW_AssociationGroupInfoGetFrame.groupingIdentifier)
            {
                /*if list mode is zero and group id is not then report the association group info for the specific group*/
                grInfoStatus = REPORT_ONE_GROUP;
                currentGroupId = pCmd->ZW_AssociationGroupInfoGetFrame.groupingIdentifier;
            }
            else
            {
                /*the get frame is invalid*/
                grInfoStatus = false;
            }

            if(grInfoStatus)
            {
                SendAssoGroupInfoReport(&rxOptionsEx);
                return RECEIVED_FRAME_STATUS_SUCCESS;
            }

            return RECEIVED_FRAME_STATUS_FAIL;

        case ASSOCIATION_GROUP_COMMAND_LIST_GET:

            if(true == Check_not_legal_response_job(p))
            { 
                /*Get/Report do not support endpoint bit-addressing */
                return RECEIVED_FRAME_STATUS_FAIL;
            }

            length = getApplGroupCommandListSize(pCmd->ZW_AssociationGroupCommandListGetFrame.groupingIdentifier);
            if (length != 0)
            {
                ts_param_t pTxOptionsEx;
                RxToTxOptions(p, &pTxOptionsEx);
                cbFuncResponseJobStatus=NULL;
                pTxBuf.ZW_AssociationGroupCommandListReport1byteFrame.cmdClass = COMMAND_CLASS_ASSOCIATION_GRP_INFO;
                pTxBuf.ZW_AssociationGroupCommandListReport1byteFrame.cmd      = ASSOCIATION_GROUP_COMMAND_LIST_REPORT;
                grId = pCmd->ZW_AssociationGroupCommandListGetFrame.groupingIdentifier;
                pTxBuf.ZW_AssociationGroupCommandListReport1byteFrame.groupingIdentifier = grId;
                pTxBuf.ZW_AssociationGroupCommandListReport1byteFrame.listLength = length;
                setApplGroupCommandList(&pTxBuf.ZW_AssociationGroupCommandListReport1byteFrame.command1, grId);

                Transport_SendDataApplEP((uint8_t *)&pTxBuf,
                                        sizeof(ZW_ASSOCIATION_GROUP_COMMAND_LIST_REPORT_1BYTE_FRAME)
                                        - sizeof(uint8_t)
                                        + length,
                                        &pTxOptionsEx,
                                        associationGroupInfoResponse_Compl, 0);

                return RECEIVED_FRAME_STATUS_SUCCESS;
          
            }

            return RECEIVED_FRAME_STATUS_FAIL;

        default:
        break;
    }
    return RECEIVED_FRAME_STATUS_NO_SUPPORT;
}


/*============================ CommandClassAssociationGroupInfoVersionGet ===
** Function description
** Return version
**
** Side effects:
**
**-------------------------------------------------------------------------*/
uint8_t
CommandClassAssociationGroupInfoVersionGet(void)
{
    return ASSOCIATION_GRP_INFO_VERSION_V3;
}
