#include "supported_CommandClassPowerLevel.h"
#include "nvm.h"
#include "serialAPI.h"
#include "utils.h"
#include "cmd_class_misc.h"


#define miniumPower POWERLEVEL_REPORT_MINUS9DBM
#define normalPower POWERLEVEL_REPORT_NORMALPOWER

static timer_t DelayTestFrameHandle = 0;
static timer_t timerPowerLevelHandle = 0;

static uint8_t timerPowerLevelSec = 0;
static uint8_t testNodeID = ZW_TEST_NOT_A_NODEID;
static uint8_t testSourceNodeID;
static uint8_t testPowerLevel;
static uint16_t testFrameCount;
static uint16_t testFrameSuccessCount;
static uint8_t testState = POWERLEVEL_TEST_NODE_REPORT_ZW_TEST_FAILED;
static uint8_t currentPower = normalPower;

void (*pPowStopPowerDownTimer)(void);
void (*pPowStartPowerDownTimer)(void);

void PowerLevelTimerCancel(void);
void ZCB_PowerLevelTimeout(void *data);

void SendTestReport(void);
void StartTest(void);
void ZCB_DelayTestFrame(void *data);

void ZCB_SendTestDone(uint8_t bStatus)
{
    mainlog(logDebug, "ZCB_SendTestDone! bStatus:%02X", bStatus);

    if (bStatus == TRANSMIT_COMPLETE_OK)
    {
        testFrameSuccessCount++;
    }

    mainlog(logDebug, "testFrameCount: %u", testFrameCount);

    if (0 != DelayTestFrameHandle)
    {
        timerCancel(&DelayTestFrameHandle);
    }

    if (testFrameCount && (--testFrameCount))
    {
        timerStart(&DelayTestFrameHandle, ZCB_DelayTestFrame, NULL, 1, TIMER_ONETIME);
    }
    else
    {
        mainlog(logDebug, "testFrameSuccessCount:%u", testFrameSuccessCount);
        if (testFrameSuccessCount)
        {
            testState = POWERLEVEL_TEST_NODE_REPORT_ZW_TEST_SUCCES;
        }
        else
        {
            testState = POWERLEVEL_TEST_NODE_REPORT_ZW_TEST_FAILED;
        }

        serialApiRF_Power_Level_Set(currentPower); /* Back to previous setting */
        SendTestReport();
        if (NULL != pPowStartPowerDownTimer)
        {
            mainlog(logDebug, "pPowStartPowerDownTimer");
            pPowStartPowerDownTimer();
        }
    }
}

void powerlevelResponse_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t)
{
    mainlog(logDebug, "powerlevelResponse_Compl\r\n");
    if (TRANSMIT_COMPLETE_OK == bTxStatus)
    {
        mainlog(logDebug, "TRANSMIT_COMPLETE_OK\r\n");
    }
    else
    {
        mainlog(logDebug, "TRANSMIT_COMPLETE_NOT_OK\r\n");
    }
}

received_frame_status_t
handleCommandClassPowerLevel(ts_param_t *p,
                             ZW_APPLICATION_TX_BUFFER *pCmd, /* IN Payload from the received frame, the union */
                             uint8_t cmdLength               /* IN Number of command bytes including the command */
                             )
{
    if (!SupportsFrameAtSecurityLevel(COMMAND_CLASS_POWERLEVEL, p->scheme))
    {
        mainlog(logDebug, "handle PowerLevel CC p->scheme %d SupportsFrame ret 0",p->scheme );
        return RECEIVED_FRAME_STATUS_FAIL;
    }

    mainlog(logDebug, "handleCommandClassPowerLevel ");

    if (true == Check_not_legal_response_job(p))
    {
        /*Do not support endpoint bit-addressing */
        return RECEIVED_FRAME_STATUS_FAIL;
    }

    switch (pCmd->ZW_Common.cmd)
    {
    case POWERLEVEL_SET:

        if (pCmd->ZW_PowerlevelSetFrame.powerLevel <= miniumPower)
        {

            /*Allways cancel timer if receiving POWERLEVEL_SET*/
            if (timerPowerLevelHandle)
            {
                PowerLevelTimerCancel();
                timerPowerLevelSec = 0;
            }

            if (pCmd->ZW_PowerlevelSetFrame.timeout == 0 ||              /*If timerout is 0 stop test*/
                (pCmd->ZW_PowerlevelSetFrame.powerLevel == normalPower)) /* If powerLevel is normalPower stop test*/
            {
                /* Set in normal mode. Also if we are in normal mode*/
                serialApiRF_Power_Level_Set(normalPower);
                timerPowerLevelSec = 0;
            }
            else
            {
                /*Start or Restart test*/
                if (NULL != pPowStopPowerDownTimer)
                {
                    /*Stop Powerdown timer because Test frame is send*/
                    pPowStopPowerDownTimer();
                }

                timerPowerLevelSec = pCmd->ZW_PowerlevelSetFrame.timeout;
                timerStart(&timerPowerLevelHandle, ZCB_PowerLevelTimeout, NULL, 1000, TIMER_FOREVER);
                serialApiRF_Power_Level_Set(pCmd->ZW_PowerlevelSetFrame.powerLevel);
            }
            mainlog(logDebug,"powerlevel return success");
            return RECEIVED_FRAME_STATUS_SUCCESS;
        }
        mainlog(logDebug,"powerlevel return fail");
        return RECEIVED_FRAME_STATUS_FAIL;
        break;

    case POWERLEVEL_GET:
    {
        ts_param_t pTxOptionsEx;
        RxToTxOptions(p, &pTxOptionsEx);
        pTxBuf.ZW_PowerlevelReportFrame.cmdClass = COMMAND_CLASS_POWERLEVEL;
        pTxBuf.ZW_PowerlevelReportFrame.cmd = POWERLEVEL_REPORT;
        pTxBuf.ZW_PowerlevelReportFrame.powerLevel = serialApiRF_Power_Level_Get();
        pTxBuf.ZW_PowerlevelReportFrame.timeout = timerPowerLevelSec;
        Transport_SendDataApplEP((uint8_t *)&pTxBuf, sizeof(pTxBuf.ZW_PowerlevelReportFrame),
                                 &pTxOptionsEx, powerlevelResponse_Compl, 0);

        return RECEIVED_FRAME_STATUS_SUCCESS;
    }
    break;

    case POWERLEVEL_TEST_NODE_SET:

        if (POWERLEVEL_TEST_NODE_REPORT_ZW_TEST_INPROGRESS == testState) // 0x02
        {
            return RECEIVED_FRAME_STATUS_SUCCESS;
        }

        testSourceNodeID = p->snode;
        testNodeID = pCmd->ZW_PowerlevelTestNodeSetFrame.testNodeid;
        testPowerLevel = pCmd->ZW_PowerlevelTestNodeSetFrame.powerLevel;
        testFrameCount = (uint16_t)((uint16_t)pCmd->ZW_PowerlevelTestNodeSetFrame.testFrameCount1 << 8) + pCmd->ZW_PowerlevelTestNodeSetFrame.testFrameCount2;
        testFrameSuccessCount = 0;

        if (testFrameCount)
        {
            StartTest();
        }
        else
        {
            testState = POWERLEVEL_TEST_NODE_REPORT_ZW_TEST_FAILED;
            SendTestReport();
        }
        return RECEIVED_FRAME_STATUS_SUCCESS;
        break;

    case POWERLEVEL_TEST_NODE_GET:
    {
        ts_param_t pTxOptionsEx;
        RxToTxOptions(p, &pTxOptionsEx);
        pTxBuf.ZW_PowerlevelTestNodeReportFrame.cmdClass = COMMAND_CLASS_POWERLEVEL;
        pTxBuf.ZW_PowerlevelTestNodeReportFrame.cmd = POWERLEVEL_TEST_NODE_REPORT;
        pTxBuf.ZW_PowerlevelTestNodeReportFrame.testNodeid = testNodeID;
        pTxBuf.ZW_PowerlevelTestNodeReportFrame.statusOfOperation = testState;
        pTxBuf.ZW_PowerlevelTestNodeReportFrame.testFrameCount1 = (uint8_t)(testFrameSuccessCount >> 8);
        pTxBuf.ZW_PowerlevelTestNodeReportFrame.testFrameCount2 = (uint8_t)(testFrameSuccessCount >> 0);
        /* Send Report - we do not care if it gets there or not - if needed report can be requested again */
        Transport_SendDataApplEP((uint8_t *)&pTxBuf, sizeof(pTxBuf.ZW_PowerlevelTestNodeReportFrame),
                                 &pTxOptionsEx, powerlevelResponse_Compl, 0);

        return RECEIVED_FRAME_STATUS_SUCCESS;
    }
    break;

    default:
        break;
    }

    return RECEIVED_FRAME_STATUS_NO_SUPPORT;
}

/*=======================   PowerLevelTimerCancel   ==========================
**    Cancels PowerLevel timer
**
**    This is an application function example
**
**--------------------------------------------------------------------------*/
void PowerLevelTimerCancel(void)
{
    mainlog(logDebug, "PowerLevelTimerCancel");
    timerCancel(&timerPowerLevelHandle);
    timerPowerLevelHandle = 0;
    if (NULL != pPowStartPowerDownTimer)
    {
        /*Stop Powerdown timer because Test frame is send*/
        mainlog(logDebug, "pPowStartPowerDownTimer");
        pPowStartPowerDownTimer();
    }
}

void ZCB_PowerLevelTimeout(void *data)
{
    mainlog(logDebug, "ZCB_PowerLevelTimeout TO ");
    mainlog(logDebug, "timerPowerLevelSec: %u", timerPowerLevelSec);
    if (!--timerPowerLevelSec)
    {
        PowerLevelTimerCancel();
        serialApiRF_Power_Level_Set(normalPower); /* Reset powerlevel to normalPower */
    }
}

void StartTest(void)
{
    if (POWERLEVEL_TEST_NODE_REPORT_ZW_TEST_INPROGRESS != testState)
    {
        testState = POWERLEVEL_TEST_NODE_REPORT_ZW_TEST_INPROGRESS;

        currentPower = serialApiRF_Power_Level_Get(); /* Get current (normalPower) */
        mainlog(logDebug, "StartTest CP currentPower:%02X", currentPower);
        serialApiRF_Power_Level_Set(testPowerLevel);
        timerStart(&DelayTestFrameHandle, ZCB_DelayTestFrame, NULL, 5, TIMER_ONETIME);
    }
}

/*============================ DelayTestFrame ===============================
** Function description
** delay test frame 10 mSec for open up a window responding Get commands on the node.
**
** Side effects:
**
**-------------------------------------------------------------------------*/
void ZCB_DelayTestFrame(void *data)
{
    DelayTestFrameHandle = 0;
    if (true == serialApiSendTestFrame(testNodeID, testPowerLevel, ZCB_SendTestDone))
    {
        if (NULL != pPowStopPowerDownTimer)
        {
            /*Stop Powerdown timer because Test frame is send*/
            pPowStopPowerDownTimer();
        }
    }
}

void SendTestReport(void)
{
    mainlog(logDebug, "SendTestReport Suc Count:%u, testState: %02X ", testFrameSuccessCount, testState);
    pTxBuf.ZW_PowerlevelTestNodeReportFrame.cmdClass = COMMAND_CLASS_POWERLEVEL;
    pTxBuf.ZW_PowerlevelTestNodeReportFrame.cmd = POWERLEVEL_TEST_NODE_REPORT;
    pTxBuf.ZW_PowerlevelTestNodeReportFrame.testNodeid = testNodeID;
    pTxBuf.ZW_PowerlevelTestNodeReportFrame.statusOfOperation = testState;
    pTxBuf.ZW_PowerlevelTestNodeReportFrame.testFrameCount1 = (uint8_t)(testFrameSuccessCount >> 8);
    pTxBuf.ZW_PowerlevelTestNodeReportFrame.testFrameCount2 = (uint8_t)(testFrameSuccessCount >> 0);
    /* Send Report - we do not care if it gets there or not - if needed report can be requested again */
    ts_param_t p;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = NO_SCHEME;

    p.snode = MyNodeId;
    p.sendpoint = 0;

    p.dnode = testSourceNodeID;
    p.dendpoint = 0;
    Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                             sizeof(pTxBuf.ZW_PowerlevelTestNodeReportFrame),
                             &p, powerlevelResponse_Compl, 0);
}

uint8_t CommandClassPowerLevelVersionGet(void)
{
    return POWERLEVEL_VERSION;
}
