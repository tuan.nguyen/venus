#ifndef _UPDATE_FIRMWARE_MD_H_
#define _UPDATE_FIRMWARE_MD_H_

//handle wake up notification from device
typedef struct _handle_firmware_update_report
{
    uint8_t support_firmware_update_md_version;
    uint8_t no_reports;
    uint16_t report_number;
    uint16_t manufacturer_id;
    uint16_t firmware_id;
    uint16_t checksum;
} handle_firmware_update_report_t;

handle_firmware_update_report_t handle_firmware_update_report;
typedef struct _handle_firmware_update_report_v3
{
    uint8_t no_reports;
    uint16_t report_number;
    uint16_t    manufacturer_id;
    uint16_t    firmware0_id;
    uint16_t    firmware0_checksum;
    uint8_t     firmware_upgradable;
    uint8_t     no_firmware_targets;
    uint16_t    max_fragment_size;
    uint16_t    firmware_id_list[MAX_BUFFER_LENGTH];
    uint16_t checksum;
    uint8_t status;
    uint16_t wait_time;
} handle_firmware_update_report_v3_t;

handle_firmware_update_report_v3_t handle_firmware_update_report_v3_4;

//States
typedef enum UPDATE_FIRMWARE_MD_FSM_STATES 
{
    ST_UPDATE_FIRMWARE_MD_IDLE = 0,
    ST_WAITING_CHECK_FILE_FINISH,
    ST_WAITING_LOAD_FILE_HEX_FINISH,
    ST_WAITING_FIRMWARE_MD_REPORT,
    ST_WAITING_FIRMWARE_UPDATE_MD_REPORT,
    ST_WAITING_FIRMWARE_UPDATE_GET,
    ST_WAITING_FIRMWARE_UPDATE_STATUS_REPORT,
    ST_FIRMWARE_UPDATE_OK,
    ST_FW_UPDATE_FAIL,
    ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET,
    ST_WAITING_35_MS_BETWEEN_2_TRAN,
    ST_WAITING_ACK,
    /*ver 3,4*/
    ST_GET_FIRMWARE_UPDATE_MD_CC_VER,
    ST_WAITING_GET_APPLICATION_VER,
    ST_WAITING_10S,
}UPDATE_FIRMWARE_MD_FSM_STATES_T;

//Events
typedef enum UPDATE_FIRMWARE_MD_FSM_EV
{
    EV_UPDATE_FIRMWARE_MD_START = 0,
    EV_CHECK_FILE_OK,
    EV_CHECK_FILE_NOT_OK,
    EV_CALCULATE_CRC16_FINISH,
    EV_RX_FIRMWARE_MD_REPORT_OK,
    EV_RX_FIRMWARE_UPDATE_MD_REQUEST_REPORT_OK,
    EV_RX_FIRMWARE_UPDATE_MD_REQUEST_REPORT_NOT_OK,
    EV_RX_FIRMWARE_UPDATE_GET_OK,
    EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_OK,
    EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_NOT_OK,
    EV_CANCEL_UPDATE_FIRMWARE,
    EV_TIMEOUT_STATE_FIRMWARE_UPDATE,
    EV_END_OF_FILE,
    EV_FIRMWARE_UPDATE_FINISH,
    EV_CANNOT_SEND_CMD,
    EV_SEND_ALL_NO_REPORT,
    EV_RECEIVE_ACK,
    /*ver 3,4*/
    EV_RX_FIRMWARE_MD_REPORT_VERSION_3_4_OK,
    EV_VERSION_CC_REPORT,
    EV_GET_APPLICATION_VER_OK,
    EV_RETRY_3_TIMES,
    
}UPDATE_FIRMWARE_MD_FSM_EV_T;

void PostEventUpdateFirmwareMd(UPDATE_FIRMWARE_MD_FSM_EV_T ev);
void CheckFileFirmwareUpdate(char *path_of_file);

#define STATE_UPDATE_FIRMWARE_MD enum UPDATE_FIRMWARE_MD_FSM_STATES

#endif