#include "controlled_CommandClassMultiChan.h"
#include "utils.h"
#include "serialAPI.h"
#include "cmd_class_misc.h"



/*============================ CmdClassMultiChannelEncapsule ===============
** Function description
** ZW_SendData with endpoint and secure options support.
**-------------------------------------------------------------------------*/
uint8_t CmdClassMultiChannelEncapsule(uint8_t 	*pData,
                                    uint8_t  dataLength,
                                    ts_param_t *p,
                                    void (*completedFunc)(uint8_t, void *, TX_STATUS_TYPE *t),
                                    void* user)
{
    uint8_t *pTxBufEncap = pData;
    uint8_t sizeCmdFrameHeader = sizeof(ZW_MULTI_CHANNEL_CMD_ENCAP_V2_FRAME) - sizeof(ALL_EXCEPT_ENCAP);

    /*Make space in start of the buffer for command MULTI_CHANNEL_CMD_ENCAP_V4*/
    uint8_t frameSize = dataLength;
    while(frameSize)
    {
        *(pData + frameSize - 1 + sizeCmdFrameHeader ) = *(pData + frameSize - 1);
        frameSize--;
    }

    if(NULL != pTxBufEncap)
    {
        *(pTxBufEncap+CMDBUF_CMDCLASS_OFFSET) = COMMAND_CLASS_MULTI_CHANNEL_V2;
        *(pTxBufEncap+CMDBUF_CMDCLASS_OFFSET+1) = MULTI_CHANNEL_CMD_ENCAP_V2;
        *(pTxBufEncap+CMDBUF_CMDCLASS_OFFSET+2) = p->sendpoint;
        *(pTxBufEncap+CMDBUF_CMDCLASS_OFFSET+3) = p->dendpoint;

        mainlog(logDebug,"CCM Transport_SendRequestExt");
        return Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)pData,
                                        dataLength + sizeCmdFrameHeader,
                                        p, completedFunc,user);
    }
    return false;
}


/*======================== CmdClassMultiChanelEncapsulateCmd  ================
**    Application command  Get Version from Command Class Version.
**    MULTI_CHANNEL_CMD_ENCAP_V2
**--------------------------------------------------------------------------*/
void CmdClassMultiChannelEncapsulateCmd(
                                        ts_param_t *p,           /*transport param */
                                        ZW_MULTI_CHANNEL_CMD_ENCAP_V2_FRAME *pCmd,/*IN Payload*/
                                        uint8_t   cmdLength) /* IN Number of command bytes including the command */

{
    /*Strip off multi channel encap */
    p->sendpoint = pCmd->properties1;
    p->dendpoint = pCmd->properties2;

    if (pCmd->properties2 & 0x80)
    {
        p->rx_flags |= RECEIVE_STATUS_TYPE_MULTI;
    }

    Transport_ApplicationCommandHandlerInternal(p, (uint8_t*)&pCmd->encapFrame, cmdLength - 4);
    return;
}


void  MultiChanCommandHandler(  
                            ts_param_t    *p,              /* transport param */
                            uint8_t       *pCmd,          /* IN  Payload from the received frame */
                            uint8_t       cmdLength)      /* IN Number of command bytes including the command */
{
  	/* Update txoptions with the low power flag from the received frame */
 
    switch (*(pCmd+CMDBUF_CMD_OFFSET))
  	{
    case MULTI_CHANNEL_END_POINT_GET_V3:
    {

    }
    break;
    case MULTI_CHANNEL_CAPABILITY_GET_V3:
    {

    }
    break;

    case MULTI_CHANNEL_END_POINT_FIND_V3:
    {

    }
    break;

    case MULTI_CHANNEL_CMD_ENCAP_V3:
    {
        CmdClassMultiChannelEncapsulateCmd(p,(ZW_MULTI_CHANNEL_CMD_ENCAP_V2_FRAME*) pCmd, cmdLength);
    }
    break;

    default:
    break;
    }
}