
#include "supported_CommandClassSupervision.h"
#include "serialAPI.h"
#include "utils.h"
#include "cmd_class_misc.h"

/****************************************************************************/
/*                              PRIVATE DATA                                */
/****************************************************************************/

static uint8_t m_sessionId = 0;
static uint8_t previously_receive_session_id = 0;
static uint8_t previously_rxStatus = 0;
static ts_param_t previously_received_destination={0,0,0,0};

#define MAX_SESSION             0x3F

typedef struct 
{
    uint8_t class;
    uint8_t cmd;
}sv_session;
sv_session sv_sessions[MAX_SESSION];

static cc_supervision_status_updates_t m_status_updates = CC_SUPERVISION_STATUS_UPDATES_NOT_SUPPORTED;
void(*m_pGetReceivedHandler)(SUPERVISION_GET_RECEIVED_HANDLER_ARGS * pArgs) = NULL;
void(*m_pReportReceivedHandler)(ts_param_t *p, uint8_t class, uint8_t cmd, cc_supervision_status_t status, uint8_t duration, bool moreUpdates) = NULL;


uint8_t SessionNo(void)
{
    m_sessionId = ((m_sessionId + 1) > MAX_SESSION) ? 1 : (m_sessionId + 1); /* increment m_sessionId, wrap around if over 0x3F */
    return m_sessionId;
}

void
CommandClassSupervisionInit (cc_supervision_status_updates_t status_updates,
        void(*pGetReceivedHandler)(SUPERVISION_GET_RECEIVED_HANDLER_ARGS * pArgs),
        void(*pReportReceivedHandler)(ts_param_t *p, uint8_t class, uint8_t cmd, cc_supervision_status_t status, uint8_t duration, bool moreUpdates))
{
    memset((uint8_t*)&sv_sessions,0, sizeof(sv_session)*MAX_SESSION);
    m_status_updates = status_updates;
    m_pGetReceivedHandler = pGetReceivedHandler;
    m_pReportReceivedHandler = pReportReceivedHandler;
    previously_receive_session_id = 0;
}


void
CmdClassSupervisionReportSend(ts_param_t* p,
                            uint8_t properties,
                            cc_supervision_status_t status,
                            uint8_t duration,
                            void(*pCallback)(uint8_t, void *, TX_STATUS_TYPE *t))
{
    

    pTxBuf.ZW_SupervisionReportFrame.cmdClass = COMMAND_CLASS_SUPERVISION;
    pTxBuf.ZW_SupervisionReportFrame.cmd = SUPERVISION_REPORT;
    pTxBuf.ZW_SupervisionReportFrame.properties1 = properties;
    pTxBuf.ZW_SupervisionReportFrame.status = status;
    pTxBuf.ZW_SupervisionReportFrame.duration = duration;

    Transport_SendDataApplEP((uint8_t *)&pTxBuf,
                                sizeof(ZW_SUPERVISION_REPORT_FRAME),
                                p,
                                pCallback, 0);
    
}



received_frame_status_t
handleCommandClassSupervision(
                            ts_param_t *p,                      /* IN receive options of type ts_param_t  */
                            ZW_APPLICATION_TX_BUFFER *pCmd,     /* IN  Payload from the received frame */
                            uint8_t cmdLength)                  /* IN Number of command bytes including the command */
{
    switch (pCmd->ZW_Common.cmd)
    {
    case SUPERVISION_GET:
    {
        /**
        * SUPERVISION_GET handle:
        * 1. Single-cast:
        *    a. Transport_ApplicationCommandHandlerEx() is called by checking previously_receive_session_id.
        *    b. single-cast trigger supervision_report is send back.
        *
        * 2. Multi-cast:
        *    a. If multi-cast is received (rxStatus includes flag RECEIVE_STATUS_TYPE_MULTI).
        *    b. Transport_ApplicationCommandHandlerEx() is called
        *    c. Do not send supervision_report.
        *
        * 3. Multi-cast single-cast follow up:
        *    a. Transport_ApplicationCommandHandlerEx is discarded on single-cast by checking previously_receive_session_id.
        *    b. single-cast trigger supervision_report is send back.
        *
        * 4. Single-cast CC multichannel bit-adr.:
        *    CommandClassMultiChan handle bit addressing by calling each endpoint with the payload.
        *    a. If Single-cast CC multichannel bit-adr. (rxStatus includes flag RECEIVE_STATUS_TYPE_MULTI).
        *    b. Transport_ApplicationCommandHandlerEx() must be called every time. Check previously_received_destination
        *       differ from EXTRACT_SESSION_ID(pCmd->ZW_SupervisionGetFrame.sessionid)
        *    c. Do not send supervision_report.
        */
        if (!SupportsFrameAtSecurityLevel(COMMAND_CLASS_SUPERVISION, p->scheme))
        {
            mainlog(logDebug, "handle Supervision CC p->scheme %d SupportsFrame ret 0",p->scheme );
            return RECEIVED_FRAME_STATUS_FAIL;
        }

        uint8_t properties1;
        /*
        * status need to be static to handle multi-cast single-cast follow up.
        * Multi-cast get status Transport_ApplicationCommandHandlerEx() and sing-cast send Supervision report.
        */
        static cc_supervision_status_t status = CC_SUPERVISION_STATUS_NOT_SUPPORTED;
        uint8_t duration;
        ts_param_t pTxOptions;

        mainlog(logDebug,"Call Supervision CC handler -> previously_receive_session_id %d, sessionid %d ", previously_receive_session_id,
                                                            CC_SUPERVISION_EXTRACT_SESSION_ID(pCmd->ZW_SupervisionGetFrame.properties1));
        //ActivateFlagSupervisionEncap();

        if(previously_receive_session_id != CC_SUPERVISION_EXTRACT_SESSION_ID(pCmd->ZW_SupervisionGetFrame.properties1))
        {
            /*
            * Reset status session id is changed.
            */
            status = CC_SUPERVISION_STATUS_NOT_SUPPORTED;

        }

        /*if previously session-id differ from current OR */
        /*if previously [nodeid + endpoint] differ from current THEN*/
        /*PARSE frame to Transport_ApplicationCommandHandlerEx*/

        if ( previously_receive_session_id != CC_SUPERVISION_EXTRACT_SESSION_ID(pCmd->ZW_SupervisionGetFrame.properties1) ||   
            ((p->dnode != previously_received_destination.dnode) || (p->dendpoint != previously_received_destination.dendpoint)))
        {
            status = (received_frame_status_t)Transport_ApplicationCommandHandlerInternal(p,
                                            ((uint8_t*)((uint8_t*)pCmd + sizeof(ZW_SUPERVISION_GET_FRAME))),
                                            (pCmd->ZW_SupervisionGetFrame.encapsulatedCommandLength));
            mainlog(logDebug,"return from  ApplicationCommandHandlerInternal status: %02X", status);

        }


        /* RECEIVE_STATUS_TYPE_MULTI: It is a multi-cast frame or CC multichannel bit-adr frame.*/
        if (p->rx_flags & RECEIVE_STATUS_TYPE_MULTI)
        {
            mainlog(logDebug,"Multicast");

            /*update previously session-id and reset previously_received_destination [node-id + endpoint]*/
            previously_receive_session_id = CC_SUPERVISION_EXTRACT_SESSION_ID(pCmd->ZW_SupervisionGetFrame.properties1);
            previously_received_destination.dnode = p->dnode;
            previously_received_destination.dendpoint = p->dendpoint;
            previously_rxStatus = RECEIVE_STATUS_TYPE_MULTI;

            return RECEIVED_FRAME_STATUS_SUCCESS;
        }
        else
        {
            /*CC:006C.01.01.11.009: A receiving node MUST ignore duplicate singlecast commands having the same Session ID*/
            if ( previously_receive_session_id == CC_SUPERVISION_EXTRACT_SESSION_ID(pCmd->ZW_SupervisionGetFrame.properties1) &&
                ((p->dnode != previously_received_destination.dnode) && (p->dendpoint != previously_received_destination.dendpoint)) &&
                (0 == previously_rxStatus ))
            {
                return RECEIVED_FRAME_STATUS_FAIL;
            }
        }

        /*update previously session-id and reset previously_received_destination [node-id + endpoint]*/
        previously_receive_session_id = CC_SUPERVISION_EXTRACT_SESSION_ID(pCmd->ZW_SupervisionGetFrame.properties1);
        previously_received_destination.dnode = p->dnode;
        previously_received_destination.dendpoint = p->dendpoint;
        previously_rxStatus = 0;

        if (m_pGetReceivedHandler)
        {
            // Call the assigned function.
            SUPERVISION_GET_RECEIVED_HANDLER_ARGS args;

            args.cmdClass = *(((uint8_t *)pCmd) + sizeof(ZW_SUPERVISION_GET_FRAME));
            args.cmd      = *(((uint8_t *)pCmd) + sizeof(ZW_SUPERVISION_GET_FRAME) + 1);
            args.properties1 = pCmd->ZW_SupervisionGetFrame.properties1;
            args.status = status;
            //args.rxOpt = rxOpt;

            m_pGetReceivedHandler(&args);

            status = args.status;
            duration = args.duration;
            properties1 = args.properties1;
        }
        else
        {
            // Set variables to standard values.
            duration = 0;
            properties1 = CC_SUPERVISION_EXTRACT_SESSION_ID(pCmd->ZW_SupervisionGetFrame.properties1);
            properties1 |= CC_SUPERVISION_ADD_MORE_STATUS_UPDATE(CC_SUPERVISION_MORE_STATUS_UPDATES_THIS_IS_LAST);
        }

//send_report:
        if (-1 == CommandAnalyzerIsGet(*(((uint8_t *)pCmd) + 4), *(((uint8_t *)pCmd) + 5)))
        {
            mainlog(logDebug,"Not Get cmd");
            return CC_SUPERVISION_STATUS_NOT_SUPPORTED;
        }
        //When we have gotten the information, we can send a Supervision report.
        RxToTxOptions(p, &pTxOptions);

        CmdClassSupervisionReportSend(&pTxOptions,properties1,status,duration,NULL);
    }
    return RECEIVED_FRAME_STATUS_SUCCESS;
    break;

    case SUPERVISION_REPORT:
    {
        uint8_t r_sessionId = pCmd->ZW_SupervisionReportFrame.properties1 & 0x3F;
        bool    moreUpdates = pCmd->ZW_SupervisionReportFrame.properties1 & 0x80;
        //if (m_sessionId  == r_sessionId)
        if (1)
        {
            /* The received session ID matches the one we sent. */
            mainlog(logDebug,"Session ID match");

            if ((m_pReportReceivedHandler))
            {
                m_pReportReceivedHandler(p, sv_sessions[r_sessionId].class, 
                                        sv_sessions[r_sessionId].cmd,   
                                        pCmd->ZW_SupervisionReportFrame.status,
                                        pCmd->ZW_SupervisionReportFrame.duration,
                                        moreUpdates);
                if (!moreUpdates)
                {
                    sv_sessions[r_sessionId].class = 0;
                    sv_sessions[r_sessionId].cmd = 0;
                }
            }

            mainlog(logDebug,"Src: %02X ",p->snode);
        }
        else
        {
            /* The received session ID not matches the one we sent. */
            mainlog(logDebug,"Session ID not match");

            if ((m_pReportReceivedHandler))
            {
                m_pReportReceivedHandler(p, 0, 
                                        0,  
                                        pCmd->ZW_SupervisionReportFrame.status,
                                        pCmd->ZW_SupervisionReportFrame.duration,
                                        moreUpdates);

                sv_sessions[r_sessionId].class = 0;
                sv_sessions[r_sessionId].cmd = 0;
            }
        }
    }
    return RECEIVED_FRAME_STATUS_SUCCESS;
    break;

    }
    return RECEIVED_FRAME_STATUS_NO_SUPPORT;
}


/*============================ CmdClassMultiChannelEncapsule ===============
** Function description
** ZW_SendData with endpoint and secure options support.
**-------------------------------------------------------------------------*/
uint8_t CmdClassSupervisionEncapsule(uint8_t    *pData,
                                    uint8_t  dataLength)
{
    uint8_t *pTxBufEncap = pData;
    uint8_t sizeCmdFrameHeader = 4;
    /*Make space in start of the buffer for command MULTI_CHANNEL_CMD_ENCAP_V4*/
    uint8_t frameSize = dataLength;
    while(frameSize)
    {
        *(pData + frameSize - 1 + sizeCmdFrameHeader ) = *(pData + frameSize - 1);
        frameSize--;
    }

    if(NULL != pTxBufEncap)
    {
        *(pTxBufEncap+CMDBUF_CMDCLASS_OFFSET) = COMMAND_CLASS_SUPERVISION;
        *(pTxBufEncap+CMDBUF_CMDCLASS_OFFSET+1) = SUPERVISION_GET;
        *(pTxBufEncap+CMDBUF_CMDCLASS_OFFSET+2) = CC_SUPERVISION_ADD_SESSION_ID(SessionNo());
        *(pTxBufEncap+CMDBUF_CMDCLASS_OFFSET+2) |= CC_SUPERVISION_ADD_STATUS_UPDATE(m_status_updates);
        *(pTxBufEncap+CMDBUF_CMDCLASS_OFFSET+3) = dataLength;

        /*save cmd to super vision session */
        sv_sessions[m_sessionId].class = pData[4];
        sv_sessions[m_sessionId].cmd = pData[5];
    }

    return dataLength+4;
}



