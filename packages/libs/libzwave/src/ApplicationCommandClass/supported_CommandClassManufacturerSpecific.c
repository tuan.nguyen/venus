#include "supported_CommandClassManufacturerSpecific.h"
#include "utils.h"
#include "serialAPI.h"
#include "cmd_class_misc.h"


static uint8_t tmp_i;


uint8_t manSpecificDeviceID[MAN_DEVICE_ID_SIZE]={'T','i','T','a','n','0','0','1'};


void  manufacturerSpecificResponse_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t)
{
    mainlog(logDebug, "manufacturerSpecificResponse_Compl\r\n");
    if (TRANSMIT_COMPLETE_OK == bTxStatus)
    {
        mainlog(logDebug, "TRANSMIT_COMPLETE_OK\r\n"); 
    }
    else
    {
        mainlog(logDebug, "TRANSMIT_COMPLETE_NOT_OK\r\n"); 
    }
}

/*============================ ApplDeviceSpecificInfoGet ===================
** Function description
** This function...
**
** Side effects:
**
**-------------------------------------------------------------------------*/

uint8_t ManufacturerSpecificDeviceIDGet(uint8_t *deviceID)
{
    for (tmp_i = 0; tmp_i < MAN_DEVICE_ID_SIZE; tmp_i++)
    {
        deviceID[tmp_i] = manSpecificDeviceID[tmp_i];
    }
    return MAN_DEVICE_ID_SIZE;
}

bool ApplDeviceSpecificInfoGet(uint8_t *deviceIdType,
                                DEVICE_ID_FORMAT* pDevIdDataFormat,
                                uint8_t* pDevIdDataLen,
                                uint8_t* pDevIdData)
{
    mainlog(logDebug,"ApplDeviceSpecificInfoGet");
    mainlog(logDebug,"ApplDeviceSpecificInfoGet:*deviceIdType:%d",*deviceIdType);
    if(*deviceIdType == DEVICE_ID_TYPE_OEM || *deviceIdType == DEVICE_ID_TYPE_SERIAL_NBR || *deviceIdType == DEVICE_ID_TYPE_PSEUDO_RANDOM )
    {
        *deviceIdType = APP_DEVICE_ID_TYPE;
        *pDevIdDataFormat = APP_DEVICE_ID_FORMAT;
        *pDevIdDataLen = ManufacturerSpecificDeviceIDGet(pDevIdData);
        mainlog(logDebug,"ManufacturerSpecificDeviceIDGet:ApplDeviceSpecificInfoGet");

        return true;
    }
    else
    {
        *deviceIdType = 0;
        *pDevIdDataFormat = 0;
        *pDevIdDataLen = 0;
    }
    return false;
}


void ApplManufacturerSpecificInfoGet(T_MSINFO *pMsInfo)
{
    pMsInfo->manufacturerId1 = (APP_MANUFACTURER_ID >> 8);
    pMsInfo->manufacturerId2 = (APP_MANUFACTURER_ID & 0xFF);
    pMsInfo->productTypeId1 = (APP_PRODUCT_TYPE_ID >> 8);
    pMsInfo->productTypeId2 = (APP_PRODUCT_TYPE_ID & 0xFF);
    pMsInfo->productId1 = (APP_PRODUCT_ID>>8);
    pMsInfo->productId2 = (APP_PRODUCT_ID & 0xFF);

}

received_frame_status_t
handleCommandClassManufacturerSpecific(ts_param_t *p,                   /* IN receive options of type RECEIVE_OPTIONS_TYPE_EX  */
                                        ZW_APPLICATION_TX_BUFFER *pCmd, /* IN Payload from the received frame, the union */
                                        uint8_t   cmdLength             /* IN Number of command bytes including the command */
)
{
    if (!SupportsFrameAtSecurityLevel(COMMAND_CLASS_MANUFACTURER_SPECIFIC_V2, p->scheme))
    {
        mainlog(logDebug, "handle manufacturer CC p->scheme %d SupportsFrame ret 0",p->scheme );
        return RECEIVED_FRAME_STATUS_FAIL;
    }

    if (true == Check_not_legal_response_job(p))
    {
        // None of the following commands support endpoint bit addressing.
        return RECEIVED_FRAME_STATUS_FAIL;
    }

    switch(pCmd->ZW_Common.cmd)
    {
        case MANUFACTURER_SPECIFIC_GET_V2:
        {
            ts_param_t pTxOptionsEx;
            RxToTxOptions(p, &pTxOptionsEx);
            pTxBuf.ZW_ManufacturerSpecificReportV2Frame.cmdClass = COMMAND_CLASS_MANUFACTURER_SPECIFIC_V2;
            pTxBuf.ZW_ManufacturerSpecificReportV2Frame.cmd = MANUFACTURER_SPECIFIC_REPORT_V2;
            ApplManufacturerSpecificInfoGet((T_MSINFO*)&(pTxBuf.ZW_ManufacturerSpecificReportV2Frame.manufacturerId1));
            Transport_SendDataApplEP((uint8_t *)&pTxBuf,
                                sizeof(ZW_MANUFACTURER_SPECIFIC_REPORT_V2_FRAME),
                                &pTxOptionsEx,
                                manufacturerSpecificResponse_Compl, 0);

            return RECEIVED_FRAME_STATUS_SUCCESS;
        }
        break;

        case DEVICE_SPECIFIC_GET_V2:
        {
            mainlog(logDebug,"DEVICE_SPECIFIC_GET_V2");
            bool sendFrame = false;
            /*Check pTxBuf is free*/
   
            DEVICE_ID_FORMAT DevIdDataFormat;
            uint8_t DevIdDataLen;
            pTxBuf.ZW_DeviceSpecificReport1byteV2Frame.cmdClass = COMMAND_CLASS_MANUFACTURER_SPECIFIC_V2;
            pTxBuf.ZW_DeviceSpecificReport1byteV2Frame.cmd = DEVICE_SPECIFIC_REPORT_V2;

            sendFrame = ApplDeviceSpecificInfoGet(&pCmd->ZW_DeviceSpecificGetV2Frame.properties1,
                                &DevIdDataFormat,
                                &DevIdDataLen,
                                &(pTxBuf.ZW_DeviceSpecificReport1byteV2Frame.deviceIdData1));
                                
            ts_param_t pTxOptionsEx;
            RxToTxOptions(p, &pTxOptionsEx);
            pTxBuf.ZW_DeviceSpecificReport1byteV2Frame.properties1 = pCmd->ZW_DeviceSpecificGetV2Frame.properties1;
            pTxBuf.ZW_DeviceSpecificReport1byteV2Frame.properties2 = (DevIdDataFormat << 5) | (DevIdDataLen & 0x1F);
     
            mainlog(logDebug,"sendFrame:%d",sendFrame);
            if(true == sendFrame)
            {
                Transport_SendDataApplEP((uint8_t *)&pTxBuf,
                                    sizeof(ZW_DEVICE_SPECIFIC_REPORT_1BYTE_V2_FRAME) + 
                                    (pTxBuf.ZW_DeviceSpecificReport1byteV2Frame.properties2 & 0x1F) - 1, /*Drag out length field 0x1F*/
                                    &pTxOptionsEx,
                                    manufacturerSpecificResponse_Compl, 0);

                return RECEIVED_FRAME_STATUS_SUCCESS;
            }
        }
        break;
    }
    return RECEIVED_FRAME_STATUS_NO_SUPPORT;
}


/*============================ CommandClassManufacturerVersionGet ==========
** Function description
** Return version
**
** Side effects: 
**
**-------------------------------------------------------------------------*/
uint8_t CommandClassManufacturerVersionGet(void)
{
    return MANUFACTURER_SPECIFIC_VERSION_V2; 
}
