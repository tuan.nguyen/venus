#include <stdbool.h>
#include <stdint.h> //have uint8_t
#include "supported_CommandClassInclusionController.h"
#include "utils.h" //contain mainlog
#include "timer.h"
#include "ZW_SendDataAppl.h" //contain SECURITY_SCHEME
#include "zw_api.h"          //contain ZW_Node_t
#include "ZW_AddingInterviewDevice_FSM.h"
#include "ZW_AutoRemoveAdd_FSM.h"
#include "node_cache.h" //contain NODE_FLAGS_SECURITY
#include "nvm.h"
#include "config.h"
#include "ZW_PollApi.h"//contain polling dev#include "ZW_DevicePoll.h"
#include "ZW_DevicePoll.h"

#define NO_TIMER 0   /* Unused timer handle */
#define NO_TIMEOUT 0 /* No timeout from this state */

#define STATE enum INCLUSION_CONTROLLER_FSM_STATES

typedef struct
{
    STATE st;                         // current state
    INCLUSION_CONTROLLER_FSM_EV_T ev; // incoming event
    STATE next_st;                    // next state
    void (*fn)();                     // action function returning next state
} transition_t;

typedef struct
{
    STATE st;            //current state
    uint32_t timeout;    //timeout of sate (ms)
    void (*tfn)(void *); //fuction is execute when timeout
} state_timeout_t;

//variable push notify
NOTIFY_TX_BUFFER_T pTxNotify;
NODE_TYPE node_Type;
uint8_t schemeGet;

//variable for timeout
static timer_t bFsmTimerHandle_AutoAdd = NO_TIMER;
static uint8_t bFsmTimerCountdown_InclusionController;
static void (*cbFsmTimer_AutoAdd)(void *);
//variable for this file
static STATE state = ST_INCLUSIONCONTROLLER_IDLE;
extern uint8_t MySystemSUCId;
extern void ActionWhenInterviewDeviceFailed(void);

//flow chart
/*
SIS and device bootstrapping S2 success
send request nodeinfo No_scheme
get nodeInformation support S0, S2

send S0 cmd Supported Get with scheme S0

waiting S0 cmd Supported Report

send S2 None Get
waiting S2 None Report
send S2 cmd Supported Get with scheme S2.2
waiting S2 cmd Supported Report
Interview device with S2 scheme
*/
/*=====================================================================================================
===================================== inclusionControllerInitiate =====================================
=================== Send COMMAND_CLASS_INCLUSION_CONTROLLER INITIATE to SIS ===========================*/
void inclusionControllerInitiate(uint8_t nodeIdDevice, uint8_t stepId, uint8_t nodeIdSIS, uint8_t scheme)
{
    ZW_APPLICATION_TX_BUFFER pTxBuf;
    pTxBuf.ZW_InitiateFrame.cmdClass = COMMAND_CLASS_INCLUSION_CONTROLLER;
    pTxBuf.ZW_InitiateFrame.cmd = INITIATE;
    pTxBuf.ZW_InitiateFrame.nodeId = nodeIdDevice;
    pTxBuf.ZW_InitiateFrame.stepId = stepId;
    ts_param_t p;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = scheme;

    p.snode = MyNodeId;
    p.sendpoint = 0;

    p.dnode = nodeIdSIS;
    p.dendpoint = 0;

    Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf, sizeof(ZW_INITIATE_FRAME),
                             &p, NULL, NULL);
}

/*=====================================================================================================
===================================== inclusionControllerInitiate =====================================
=================== Send COMMAND_CLASS_INCLUSION_CONTROLLER INITIATE to SIS ===========================*/
void inclusionControllerComplete(uint8_t nodeIdSIS, uint8_t stepId, uint8_t status, uint8_t scheme)
{
    ZW_APPLICATION_TX_BUFFER pTxBuf;
    pTxBuf.ZW_CompleteFrame.cmdClass = COMMAND_CLASS_INCLUSION_CONTROLLER;
    pTxBuf.ZW_CompleteFrame.cmd = COMPLETE;
    pTxBuf.ZW_CompleteFrame.stepId = stepId;
    pTxBuf.ZW_CompleteFrame.status = status;
    ts_param_t p;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = scheme;

    p.snode = MyNodeId;
    p.sendpoint = 0;

    p.dnode = nodeIdSIS;
    p.dendpoint = 0;

    Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf, sizeof(ZW_COMPLETE_FRAME),
                             &p, NULL, NULL);
}

/*========================= sendRequestNodeInfo_Compl ============================
** Function description
**   ZW_RequestNodeInfo callback called when the request transmission has 
** been tried.
** Side effects:
**      
**--------------------------------------------------------------------------*/
void sendRequestNodeInfo_Compl(uint8_t bTxStatus)
{
    if (!bTxStatus)
    {
        mainlog(logDebug, "Request Node Info transmitted successfully");
    }
    else
    {
        mainlog(logDebug, "Request Node Info transmit failed");
    }
}

/*=======================   ZCB_TimerOutStateInterviewDevice   ========================
**
**  Timeout function that returns the FSB to idle.
**  Called by the FSM timer.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/

void ZCB_TimerOutStateInclusionController(void *data)
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    pTxNotify.RemoveNodeZPCNotify.bStatus = INCLUSION_CONTROLLER_TIMEOUT;

    PushNotificationToHandler(INCLUSION_CONTROLLER_NOTIFY, (uint8_t *)&pTxNotify.RemoveNodeZPCNotify, sizeof(ADD_NODE_ZPC_NOTIFY_T));
    PostEventInclusionController(EV_TIMEOUT_INCLUSION_CONTROLLER_STATE);
    //push notify timout support inclusion controller CC

}
/*========================= SecureBootStrapping_Complete ======================
 ** Function description
 **      Callback when Security Inclusion process done
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/

void SecureBootStrapping_Complete(uint8_t bStatus, uint8_t bSource, uint8_t *pCmd, uint8_t bLen)
{
    switch (bStatus)
    {

    case ADD_SECURE_NODE_STATUS_DONE:
    {
        mainlog(logDebug, "ZPCSecurity: [ADD_SECURE_S0_NODE_STATUS_DONE(%02X)] - Waiting ... (Nodeinformation length %u)", bStatus, bLen);
        lastLearnedNodeZPC.node_capability_secureV0.noCapability = bLen;
        memcpy((char *)&lastLearnedNodeZPC.node_capability_secureV0.aCapability, pCmd, bLen);
        lastLearnedNodeZPC.node_secure_scheme = SECURITY_SCHEME_0;
        SetCacheEntryFlagMasked(lastLearnedNodeZPC.node_id, NODE_FLAG_SECURITY0, NODE_FLAGS_SECURITY);

        PostEventInclusionController(EV_ADD_SO_SECURE_OK);
    }
    break;

    case ADD_SECURE_NODE_STATUS_FAILED:
    {
        mainlog(logDebug, "ZPCSecurity: [ADD_SECURE_S0_NODE_STATUS_FAILED(%02X)] - Waiting ... (Nodeinformation length %u)", bStatus, bLen);
        lastLearnedNodeZPC.node_capability_secureV0.noCapability = 0;
        lastLearnedNodeZPC.node_secure_scheme = NO_SCHEME;

        PostEventInclusionController(EV_ADD_SO_SECURE_FAIL);
    }
    break;
    case ADD_SECURE_2_NODE_STATUS_DONE:
    {
        mainlog(logDebug, "ZPCSecurity: [ADD_SECURE_S2_NODE_STATUS_DONE(%02X)] - Waiting ... (Nodeinformation length %u)", bStatus, bLen);
        lastLearnedNodeZPC.node_capability_secureV2.noCapability = bLen;
        memcpy((char *)&lastLearnedNodeZPC.node_capability_secureV2.aCapability, pCmd, bLen);
        PostEventInclusionController(EV_ADD_S2_SECURE_OK);
    }
    break;

    case ADD_SECURE_2_NODE_STATUS_FAILED:
    {
        mainlog(logDebug, "ZPCSecurity: [ADD_SECURE_2_NODE_STATUS_FAILED(%02X)] - Waiting ... (Nodeinformation length %u)", bStatus, bLen);
        lastLearnedNodeZPC.node_capability_secureV2.noCapability = 0;
        lastLearnedNodeZPC.node_secure_scheme = NO_SCHEME;
        PostEventInclusionController(EV_ADD_S2_SECURE_FAIL);
    }
    break;
    default:
        break;
    }
}
/*============================ Callback of S0 Commands Supported Get ======================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void S0CmdSupportedGetCallback(uint8_t status, uint8_t *buf, uint8_t len)
{
    mainlog(logDebug, "S0CmdSupportedGetCallback");
    hexdump(buf, len);

    if (len > 1)
    {
        SetCacheEntryFlagMasked(lastLearnedNodeZPC.node_id, NODE_FLAG_SECURITY0, NODE_FLAGS_SECURITY);
        SecureBootStrapping_Complete(ADD_SECURE_NODE_STATUS_DONE, lastLearnedNodeZPC.node_id, buf + 3, len - 3);
    }
    else
    {
        SecureBootStrapping_Complete(ADD_SECURE_NODE_STATUS_FAILED, lastLearnedNodeZPC.node_id, 0, 0);
    }
}

void SaveCapabilityS2Secure_InterviewDevice(uint8_t bStatus, uint8_t bSource, uint8_t *pCmd, uint8_t bLen)
{
    mainlog(logDebug, "ZPCSecurity: [ADD_SECURE_2_NODE_STATUS_DONE(%02X)] - Waiting ... (Nodeinformation length %u)", bStatus, bLen);
    lastLearnedNodeZPC.node_capability_secureV2.noCapability = bLen;
    memcpy((char *)&lastLearnedNodeZPC.node_capability_secureV2.aCapability, pCmd, bLen);
    PostEventAutoRemoveAdd(EV_FINISH_ADD_SECURITY);/*change ST_WAITING_ADD_SECURE --> ST_WAITING_FINISH_INTERVIEW*/        
    nm.state = NMS_WAITING_FOR_INTERVIEW;
    PostEventInterviewDevice(EV_ADD_NODE_OK);
}

//---------------------------------------------------------------------------
//-----------------------------Action----------------------------------------
//---------------------------------------------------------------------------
/*========================= AC_SendInitiate ============================
** Function description
**   AC_SendInitiate include device S2, S0 scheme when in network have
**   SIS and my controller is incluision controller 
**      
**--------------------------------------------------------------------------*/
void AC_SendInitiate(void)
{
    lastLearnedNodeZPC.node_secure_scheme = NO_SCHEME;
    if (NMM_REPLACE_FAILED_NODE == nm.mode)//replace fail node
        inclusionControllerInitiate(lastLearnedNodeZPC.node_id, INITIATE_PROXY_INCLUSION_REPLACE, MySystemSUCId, NO_SCHEME);
    else//add new node
        inclusionControllerInitiate(lastLearnedNodeZPC.node_id, INITIATE_PROXY_INCLUSION, MySystemSUCId, NO_SCHEME);
}

/*=================== AC_SendRequestNodeInfo from device =====================
 ** Function description
 ** Side effects:
 **  
 **--------------------------------------------------------------------------*/
void AC_SendRequestNodeInfo(void)
{
    postEventOperationPollingDev(EV_USER_ADD_NODE_ZPC);
    lastLearnedNodeZPC.node_secure_scheme = NO_SCHEME;
    serialApiRequestNodeInfo(lastLearnedNodeZPC.node_id, sendRequestNodeInfo_Compl);
}

/*============================ Get Capability S0 of device ======================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void AC_SendS0CmdSupportedGet(void)
{
    mainlog(logDebug, "GetCapabilitiesS0Support");
    if (!(GetCacheEntryFlag(MyNodeId) && NODE_FLAG_SECURITY0))
    {
        PostEventInclusionController(EV_MY_NODE_NOT_SUPPORT_THIS_SCHEME);
    }
    else if ((isSupportedCommandClass(&lastLearnedNodeZPC,
                                 COMMAND_CLASS_SECURITY) == SCHEME_NOT_SUPPORT))
        PostEventInclusionController(EV_DEVICE_NOT_SUPPORT_S0);
    else
    {
        ts_param_t p;
        p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
        p.scheme = SECURITY_SCHEME_0;

        p.snode = MyNodeId;
        p.sendpoint = 0;

        p.dnode = lastLearnedNodeZPC.node_id;
        p.dendpoint = 0;

        pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmdClass = COMMAND_CLASS_SECURITY;     /* The command class */
        pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmd = SECURITY_COMMANDS_SUPPORTED_GET; /* The command */

        ZW_SendRequestAppl(&p, (ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                           sizeof(ZW_SECURITY_2_COMMANDS_SUPPORTED_GET_FRAME),
                           SECURITY_COMMANDS_SUPPORTED_REPORT,
                           10000,                      /*timeout 20 seconds */
                           S0CmdSupportedGetCallback); //waiting S0CmdSupportedGetCallback 20000 ms
    }
}

/*============================ Callback of S2 Commands Supported Get ======================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void S2AccessCmdSupportedGetCallback(uint8_t status, uint8_t *buf, uint8_t len)
{
    mainlog(logDebug, "S2AccessCmdSupportedGetCallback");
    hexdump(buf, len);
    if (len > 1)
    {
        lastLearnedNodeZPC.node_secure_scheme = SECURITY_SCHEME_2_ACCESS;
        SetCacheEntryFlagMasked(lastLearnedNodeZPC.node_id, NODE_FLAG_SECURITY2_ACCESS, NODE_FLAGS_SECURITY);
        SecureBootStrapping_Complete(ADD_SECURE_2_NODE_STATUS_DONE, lastLearnedNodeZPC.node_id, buf + 2, len - 2);
    }
    else
    {
        mainlog(logDebug, "ADD NODE_FLAG_SECURITY2_ACCESS FAIL");
        SecureBootStrapping_Complete(ADD_SECURE_2_NODE_STATUS_FAILED, lastLearnedNodeZPC.node_id, buf + 2, len - 2);
    }
}
/*========================== Get Capability S0 of device =====================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void AC_SendS2AccessCmdSupportedGet(void)
{
    mainlog(logDebug, "AC_SendS2AccessCmdSupportedGet");
    if (!(GetCacheEntryFlag(MyNodeId) && NODE_FLAG_SECURITY2_ACCESS))
    {
        PostEventInclusionController(EV_MY_NODE_NOT_SUPPORT_THIS_SCHEME);
    }
    else if ((isSupportedCommandClass(&lastLearnedNodeZPC,
                                 COMMAND_CLASS_SECURITY_2) == SCHEME_NOT_SUPPORT))
        PostEventInclusionController(EV_DEVICE_NOT_SUPPORT_S2);
    else
    {

        ts_param_t p;
        p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
        p.scheme = SECURITY_SCHEME_2_ACCESS;

        p.snode = MyNodeId;
        p.sendpoint = 0;

        p.dnode = lastLearnedNodeZPC.node_id;
        p.dendpoint = 0;

        pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmdClass = COMMAND_CLASS_SECURITY_2;     /* The command class */
        pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmd = SECURITY_2_COMMANDS_SUPPORTED_GET; /* The command */

        ZW_SendRequestAppl(&p, (ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                           sizeof(ZW_SECURITY_2_COMMANDS_SUPPORTED_GET_FRAME),
                           SECURITY_2_COMMANDS_SUPPORTED_REPORT,
                           10000,                            /*timeout 2 seconds */
                           S2AccessCmdSupportedGetCallback); //waiting S2CmdSupportedGetCallback 10000 ms
    }
}

/*========================== AC_S0BootStrapping =====================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void AC_S0BootStrapping(void)
{
    if (isSupportedCommandClass(&lastLearnedNodeZPC,
                                COMMAND_CLASS_SECURITY) != SCHEME_NOT_SUPPORT)
    {
        if ((lastLearnedNodeZPC.node_info.nodeType.basic == BASIC_TYPE_CONTROLLER) ||
            (lastLearnedNodeZPC.node_info.nodeType.basic == BASIC_TYPE_STATIC_CONTROLLER))
        {
            securityS0InclusionStart(lastLearnedNodeZPC.node_id, true, SecureBootStrapping_Complete);
        }
        else
        {
            securityS0InclusionStart(lastLearnedNodeZPC.node_id, false, SecureBootStrapping_Complete);
        }
    }
    else
        PostEventInclusionController(EV_DEVICE_NOT_SUPPORT_S0); 
}

/*========================== AC_SecureS2BootStrapping =====================
 ** Function description
 ** Side effects:
 ** Note: Check my node scheme
 **--------------------------------------------------------------------------*/
void AC_SecureS2BootStrapping(void)
{
    if ((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC,
                                             COMMAND_CLASS_SECURITY_2)) != SCHEME_NOT_SUPPORT)
    {
        securityS2InclusionStart(lastLearnedNodeZPC.node_id, SecureBootStrapping_Complete);//have check scheme
    }
    else //not support S2 secure
    {
        PostEventInclusionController(EV_DEVICE_NOT_SUPPORT_S2);
    }
}

/*========================== AC_SecureS0BootStrapping =====================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void AC_SecureS0BootStrapping(void)
{
    if ((schemeGet = isSupportedCommandClass(&lastLearnedNodeZPC,
                                             COMMAND_CLASS_SECURITY)) != SCHEME_NOT_SUPPORT)
    {
        if ((lastLearnedNodeZPC.node_info.nodeType.basic == BASIC_TYPE_CONTROLLER) ||
            (lastLearnedNodeZPC.node_info.nodeType.basic == BASIC_TYPE_STATIC_CONTROLLER))
        {
            securityS0InclusionStart(lastLearnedNodeZPC.node_id, true, SecureBootStrapping_Complete);
        }
        else
        {
            securityS0InclusionStart(lastLearnedNodeZPC.node_id, false, SecureBootStrapping_Complete);
        }
    }
    else //not support S0 secure
    {
        PostEventInclusionController(EV_DEVICE_NOT_SUPPORT_S0);
    }
}

/*========================== AC_InterviewDevice =====================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void AC_InterviewDevice(void)
{
    nm.mode = NMM_ADD_NODE;
    PostEventAutoRemoveAdd(EV_FINISH_ADD_SECURITY);/*change ST_WAITING_ADD_SECURE --> ST_WAITING_FINISH_INTERVIEW*/    
    nm.state = NMS_WAITING_FOR_INTERVIEW;
    PostEventInterviewDevice(EV_ADD_NODE_OK);
}
/*========================== AC_SaveCapability_InterviewDevice =====================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void AC_SaveCapability_InterviewDevice(void)
{
}

/*============================ Callback of S2 Commands Supported Get ======================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void S2AuthenticatedCmdSupportedGetCallback(uint8_t status, uint8_t *buf, uint8_t len)
{
    mainlog(logDebug, "S2AuthenticatedCmdSupportedGetCallback");
    hexdump(buf, len);
    if (len > 1)
    {
        lastLearnedNodeZPC.node_secure_scheme = SECURITY_SCHEME_2_AUTHENTICATED;
        SetCacheEntryFlagMasked(lastLearnedNodeZPC.node_id, NODE_FLAG_SECURITY2_AUTHENTICATED, NODE_FLAGS_SECURITY);
        SecureBootStrapping_Complete(ADD_SECURE_2_NODE_STATUS_DONE, lastLearnedNodeZPC.node_id, buf + 2, len - 2);
    }
    else
    {
        mainlog(logDebug, "ADD NODE_FLAG_SECURITY2_AUTHENTICATED FAIL");
        SecureBootStrapping_Complete(ADD_SECURE_2_NODE_STATUS_FAILED, lastLearnedNodeZPC.node_id, buf + 2, len - 2);
    }
}
/*========================== AC_SendS2AuthenticatedCmdSupportedGet =====================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void AC_SendS2AuthenticatedCmdSupportedGet(void)
{
    mainlog(logDebug, "AC_SendS2AuthenticatedCmdSupportedGet");
    if (!(GetCacheEntryFlag(MyNodeId) && NODE_FLAG_SECURITY2_AUTHENTICATED))
    {
        PostEventInclusionController(EV_MY_NODE_NOT_SUPPORT_THIS_SCHEME);
    }
    else if ((isSupportedCommandClass(&lastLearnedNodeZPC,
                                 COMMAND_CLASS_SECURITY_2) == SCHEME_NOT_SUPPORT))
        PostEventInclusionController(EV_DEVICE_NOT_SUPPORT_S2);
    else
    {

        ts_param_t p;
        p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
        p.scheme = SECURITY_SCHEME_2_AUTHENTICATED;

        p.snode = MyNodeId;
        p.sendpoint = 0;

        p.dnode = lastLearnedNodeZPC.node_id;
        p.dendpoint = 0;

        pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmdClass = COMMAND_CLASS_SECURITY_2;     /* The command class */
        pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmd = SECURITY_2_COMMANDS_SUPPORTED_GET; /* The command */

        ZW_SendRequestAppl(&p, (ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                           sizeof(ZW_SECURITY_2_COMMANDS_SUPPORTED_GET_FRAME),
                           SECURITY_2_COMMANDS_SUPPORTED_REPORT,
                           10000,                                   /*timeout 2 seconds */
                           S2AuthenticatedCmdSupportedGetCallback); //waiting S2CmdSupportedGetCallback 10000 ms
    }
}

/*============================ Callback of S2 Commands Supported Get ======================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void S2UnAuthenticatedCmdSupportedGetCallback(uint8_t status, uint8_t *buf, uint8_t len)
{
    mainlog(logDebug, "S2UnAuthenticatedCmdSupportedGetCallback");
    hexdump(buf, len);
    if (len > 1)
    {
        lastLearnedNodeZPC.node_secure_scheme = SECURITY_SCHEME_2_UNAUTHENTICATED;
        SetCacheEntryFlagMasked(lastLearnedNodeZPC.node_id, NODE_FLAG_SECURITY2_UNAUTHENTICATED, NODE_FLAGS_SECURITY);
        SecureBootStrapping_Complete(ADD_SECURE_2_NODE_STATUS_DONE, lastLearnedNodeZPC.node_id, buf + 2, len - 2);
    }
    else
    {
        mainlog(logDebug, "ADD NODE_FLAG_SECURITY2_UNAUTHENTICATED FAIL");
        SecureBootStrapping_Complete(ADD_SECURE_2_NODE_STATUS_FAILED, lastLearnedNodeZPC.node_id, buf + 2, len - 2);
    }
}
/*========================== AC_SendS2UnAuthenticatedCmdSupportedGet =====================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void AC_SendS2UnAuthenticatedCmdSupportedGet(void)
{
    mainlog(logDebug, "AC_SendS2UnAuthenticatedCmdSupportedGet");
    if (!(GetCacheEntryFlag(MyNodeId) && NODE_FLAG_SECURITY2_UNAUTHENTICATED))
    {
        PostEventInclusionController(EV_MY_NODE_NOT_SUPPORT_THIS_SCHEME);
    }
    else if ((isSupportedCommandClass(&lastLearnedNodeZPC,
                                 COMMAND_CLASS_SECURITY_2) == SCHEME_NOT_SUPPORT))
        PostEventInclusionController(EV_DEVICE_NOT_SUPPORT_S2);
    else
    {

        ts_param_t p;
        p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
        p.scheme = SECURITY_SCHEME_2_UNAUTHENTICATED;

        p.snode = MyNodeId;
        p.sendpoint = 0;

        p.dnode = lastLearnedNodeZPC.node_id;
        p.dendpoint = 0;

        pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmdClass = COMMAND_CLASS_SECURITY_2;     /* The command class */
        pTxBuf.ZW_Security2CommandsSupportedGetFrame.cmd = SECURITY_2_COMMANDS_SUPPORTED_GET; /* The command */

        ZW_SendRequestAppl(&p, (ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                           sizeof(ZW_SECURITY_2_COMMANDS_SUPPORTED_GET_FRAME),
                           SECURITY_2_COMMANDS_SUPPORTED_REPORT,
                           10000,                                     /*timeout 2 seconds */
                           S2UnAuthenticatedCmdSupportedGetCallback); //waiting S2CmdSupportedGetCallback 10000 ms
    }
}
/*========================== AC_SendS2UnAuthenticatedCmdSupportedGet =====================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void AC_SendInclusionS0Complete_OK(void)
{
    inclusionControllerComplete(MySystemSUCId, COMPLETE_S0_INCLUSION, COMPLETE_STEP_OK, NO_SCHEME);
}
void AC_SendInclusionS0Complete_Fail(void)
{
    inclusionControllerComplete(MySystemSUCId, COMPLETE_S0_INCLUSION, COMPLETE_STEP_FAILED, NO_SCHEME);
}

//my device is SIS device
/*========================== AC_Send_Complete_StatusFail =====================
 ** Function description
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void AC_Send_Complete_StatusFail(void)
{
    inclusionControllerComplete(nm.tmp_node, COMPLETE_PROXY_INCLUSION, COMPLETE_STEP_FAILED, NO_SCHEME);
    nm.state = NMS_IDLE;
}

/*========================== AC_InclusionDeviceBootStrap_S0 =====================
 ** Function description
 ** Side effects:
 ** Inclusion Bootstrap S0
 **--------------------------------------------------------------------------*/
void AC_InclusionDeviceBootStrap_S0(void)
{
    inclusionControllerInitiate(lastLearnedNodeZPC.node_id, INITIATE_S0_INCLUSION, nm.tmp_node, NO_SCHEME);
    nm.state = NMS_IDLE;
}
/*========================== AC_Send_Complete_Status_OK =====================
 ** Function description
 ** waiting S0 bootstrap complete from device
 ** Side effects:
 **
 **--------------------------------------------------------------------------*/
void AC_Send_Complete_Status_OK(void)
{
    inclusionControllerComplete(nm.tmp_node, COMPLETE_PROXY_INCLUSION, COMPLETE_STEP_OK, NO_SCHEME);
    nm.state = NMS_IDLE;
    postEventOperationPollingDev(EV_USER_ADD_NODE_ZPC_DONE);
}

void AC_Reset_InclusionControllerMode(void)
{
    nm.mode = NMM_DEFAULT;
    nm.state = NMS_IDLE;
}

void AC_Reset_State_PushInterviewDeviceFail(void)
{
    nm.mode = NMM_ADD_NODE;
    inclusionControllerComplete(nm.tmp_node, COMPLETE_PROXY_INCLUSION, COMPLETE_STEP_USER_REJECTED, NO_SCHEME);
    PostEventInterviewDevice(EV_CLOSE_NWK);//push event EV_CLOSE_NWK --> interview device failed, close network
}
//--------------------------------------------------------------------------
/* State transition table
 * This is where the state machine is defined.
 *
 * Each transition can optionally trigger a call to an action function.
 * An action function defines the actions that must occur when a particular
 * transition occurs.
 *
 * Format: {Current state, incoming event, next state, action_function} */

static const transition_t trans[] =
    {
        //my device is Inclusion 
        {ST_INCLUSIONCONTROLLER_IDLE,   EV_ADD_NODE_NORMAL_OK, ST_WAITING_INCLUSION_CONTROLLER_COMPLETE, &AC_SendInitiate},
        {ST_INCLUSIONCONTROLLER_IDLE,   EV_NO_SIS_DEVICE, ST_INCLUSIONCONTROLLER_IDLE, NULL},
        //cancel inclusion controller when it does not finishes
        {ST_INCLUSIONCONTROLLER_IDLE,   EV_RECEIVE_INCLUSION_CONTROLLER_INITIATE_S0, ST_INCLUSIONCONTROLLER_IDLE, &AC_SendInclusionS0Complete_Fail},

        {ST_WAITING_INCLUSION_CONTROLLER_COMPLETE, EV_RECEIVE_INCLUSION_CONTROLLER_COMPLETE_OK, ST_WAITING_S0_CMD_SUPPORTED_REPORT, &AC_SendS0CmdSupportedGet},
        {ST_WAITING_INCLUSION_CONTROLLER_COMPLETE, EV_RECEIVE_INCLUSION_CONTROLLER_INITIATE_S0, ST_WAITING_SO_BOOTSTRAPPING_COMPLETE, &AC_S0BootStrapping},
        {ST_WAITING_INCLUSION_CONTROLLER_COMPLETE, EV_TIMEOUT_INCLUSION_CONTROLLER_STATE, ST_WAITING_SECURE_S0_BOOTSTRAPPING_COMPLETE, &AC_SecureS0BootStrapping},
        {ST_WAITING_INCLUSION_CONTROLLER_COMPLETE, EV_RECEIVE_INCLUSION_CONTROLLER_COMPLETE_NOT_OK, ST_WAITING_SECURE_S0_BOOTSTRAPPING_COMPLETE, &AC_SecureS0BootStrapping},
        {ST_WAITING_INCLUSION_CONTROLLER_COMPLETE, EV_CLOSE_INCLUSION_CONTROLLER, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_State_PushInterviewDeviceFail},

        {ST_WAITING_SECURE_S0_BOOTSTRAPPING_COMPLETE, EV_ADD_SO_SECURE_OK, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_SECURE_S0_BOOTSTRAPPING_COMPLETE, EV_ADD_SO_SECURE_FAIL, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_SECURE_S0_BOOTSTRAPPING_COMPLETE, EV_DEVICE_NOT_SUPPORT_S0, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_SECURE_S0_BOOTSTRAPPING_COMPLETE, EV_TIMEOUT_INCLUSION_CONTROLLER_STATE, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_SECURE_S0_BOOTSTRAPPING_COMPLETE, EV_CLOSE_INCLUSION_CONTROLLER, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_State_PushInterviewDeviceFail},

        {ST_WAITING_S0_CMD_SUPPORTED_REPORT, EV_ADD_SO_SECURE_OK, ST_WAITING_S2_ACCESS_CMD_SUPPORTED_REPORT, &AC_SendS2AccessCmdSupportedGet},
        {ST_WAITING_S0_CMD_SUPPORTED_REPORT, EV_ADD_SO_SECURE_FAIL, ST_WAITING_S2_ACCESS_CMD_SUPPORTED_REPORT, &AC_SendS2AccessCmdSupportedGet},
        {ST_WAITING_S0_CMD_SUPPORTED_REPORT, EV_MY_NODE_NOT_SUPPORT_THIS_SCHEME, ST_WAITING_S2_ACCESS_CMD_SUPPORTED_REPORT, &AC_SendS2AccessCmdSupportedGet},
        {ST_WAITING_S0_CMD_SUPPORTED_REPORT, EV_DEVICE_NOT_SUPPORT_S0, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        //not support S0 --> not support S2 --> interview device
        {ST_WAITING_S0_CMD_SUPPORTED_REPORT, EV_TIMEOUT_INCLUSION_CONTROLLER_STATE, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_S0_CMD_SUPPORTED_REPORT, EV_CLOSE_INCLUSION_CONTROLLER, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_State_PushInterviewDeviceFail},

        {ST_WAITING_S2_ACCESS_CMD_SUPPORTED_REPORT, EV_ADD_S2_SECURE_OK, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_S2_ACCESS_CMD_SUPPORTED_REPORT, EV_ADD_S2_SECURE_FAIL, ST_WAITING_S2_AUTHENTICATED_CMD_SUPPORTED_REPORT, &AC_SendS2AuthenticatedCmdSupportedGet},
        {ST_WAITING_S2_ACCESS_CMD_SUPPORTED_REPORT, EV_MY_NODE_NOT_SUPPORT_THIS_SCHEME, ST_WAITING_S2_AUTHENTICATED_CMD_SUPPORTED_REPORT, &AC_SendS2AuthenticatedCmdSupportedGet},
        {ST_WAITING_S2_ACCESS_CMD_SUPPORTED_REPORT, EV_DEVICE_NOT_SUPPORT_S2, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_S2_ACCESS_CMD_SUPPORTED_REPORT, EV_TIMEOUT_INCLUSION_CONTROLLER_STATE, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_S2_ACCESS_CMD_SUPPORTED_REPORT, EV_CLOSE_INCLUSION_CONTROLLER, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_State_PushInterviewDeviceFail},

        {ST_WAITING_S2_AUTHENTICATED_CMD_SUPPORTED_REPORT,   EV_ADD_S2_SECURE_OK, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_S2_AUTHENTICATED_CMD_SUPPORTED_REPORT,   EV_ADD_S2_SECURE_FAIL, ST_WAITING_S2_UNAUTHENTICATED_CMD_SUPPORTED_REPORT, &AC_SendS2UnAuthenticatedCmdSupportedGet},
        {ST_WAITING_S2_AUTHENTICATED_CMD_SUPPORTED_REPORT,   EV_MY_NODE_NOT_SUPPORT_THIS_SCHEME, ST_WAITING_S2_UNAUTHENTICATED_CMD_SUPPORTED_REPORT, &AC_SendS2UnAuthenticatedCmdSupportedGet},
        {ST_WAITING_S2_AUTHENTICATED_CMD_SUPPORTED_REPORT,   EV_DEVICE_NOT_SUPPORT_S2, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_S2_AUTHENTICATED_CMD_SUPPORTED_REPORT,   EV_TIMEOUT_INCLUSION_CONTROLLER_STATE, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_S2_AUTHENTICATED_CMD_SUPPORTED_REPORT,   EV_CLOSE_INCLUSION_CONTROLLER, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_State_PushInterviewDeviceFail},

        {ST_WAITING_S2_UNAUTHENTICATED_CMD_SUPPORTED_REPORT,    EV_ADD_S2_SECURE_OK, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_S2_UNAUTHENTICATED_CMD_SUPPORTED_REPORT,    EV_ADD_S2_SECURE_FAIL, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_S2_UNAUTHENTICATED_CMD_SUPPORTED_REPORT,    EV_MY_NODE_NOT_SUPPORT_THIS_SCHEME, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_S2_UNAUTHENTICATED_CMD_SUPPORTED_REPORT,    EV_DEVICE_NOT_SUPPORT_S2, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_S2_UNAUTHENTICATED_CMD_SUPPORTED_REPORT,    EV_TIMEOUT_INCLUSION_CONTROLLER_STATE, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_S2_UNAUTHENTICATED_CMD_SUPPORTED_REPORT,    EV_CLOSE_INCLUSION_CONTROLLER, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_State_PushInterviewDeviceFail},

        {ST_WAITING_INTERVIEW,  EV_INTERVIEW_DEVICE_OK, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_InclusionControllerMode},
        {ST_WAITING_INTERVIEW,  EV_TIMEOUT_INCLUSION_CONTROLLER_STATE, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_InclusionControllerMode},
        {ST_WAITING_INTERVIEW,  EV_CLOSE_INCLUSION_CONTROLLER, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_State_PushInterviewDeviceFail},

        {ST_WAITING_SO_BOOTSTRAPPING_COMPLETE, EV_ADD_SO_SECURE_OK, ST_WAITING_RECEIVE_COMPLETE_AFTER_BOOT_S0, &AC_SendInclusionS0Complete_OK},
        {ST_WAITING_SO_BOOTSTRAPPING_COMPLETE, EV_DEVICE_NOT_SUPPORT_S0, ST_WAITING_RECEIVE_COMPLETE_AFTER_BOOT_S0, &AC_SendInclusionS0Complete_Fail},
        {ST_WAITING_SO_BOOTSTRAPPING_COMPLETE, EV_ADD_SO_SECURE_FAIL, ST_WAITING_RECEIVE_COMPLETE_AFTER_BOOT_S0, &AC_SendInclusionS0Complete_Fail},
        {ST_WAITING_SO_BOOTSTRAPPING_COMPLETE, EV_TIMEOUT_INCLUSION_CONTROLLER_STATE, ST_WAITING_RECEIVE_COMPLETE_AFTER_BOOT_S0, &AC_SendInclusionS0Complete_Fail},
        {ST_WAITING_SO_BOOTSTRAPPING_COMPLETE, EV_CLOSE_INCLUSION_CONTROLLER, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_State_PushInterviewDeviceFail},

        {ST_WAITING_RECEIVE_COMPLETE_AFTER_BOOT_S0, EV_RECEIVE_INCLUSION_CONTROLLER_COMPLETE_OK, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_RECEIVE_COMPLETE_AFTER_BOOT_S0, EV_TIMEOUT_INCLUSION_CONTROLLER_STATE, ST_WAITING_INTERVIEW, &AC_InterviewDevice},
        {ST_WAITING_RECEIVE_COMPLETE_AFTER_BOOT_S0, EV_CLOSE_INCLUSION_CONTROLLER, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_State_PushInterviewDeviceFail},

        //my device is SIS role in network
        {ST_INCLUSIONCONTROLLER_IDLE, EV_RECEIVE_INCLUSION_CONTROLLER_INITIATE_PROXY, ST_SIS_WAITING_NODE_INFOMATION, &AC_SendRequestNodeInfo},

        {ST_SIS_WAITING_NODE_INFOMATION, EV_RECEIVE_NODE_INFOMATION, ST_WAITING_PUSH_NOTIFY_S2_BOOTSTRAP, NULL},
        {ST_SIS_WAITING_NODE_INFOMATION, EV_TIMEOUT_INCLUSION_CONTROLLER_STATE, ST_INCLUSIONCONTROLLER_IDLE, &AC_Send_Complete_StatusFail},
        {ST_SIS_WAITING_NODE_INFOMATION, EV_CLOSE_INCLUSION_CONTROLLER, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_State_PushInterviewDeviceFail},

        {ST_WAITING_PUSH_NOTIFY_S2_BOOTSTRAP, EV_TIMEOUT_INCLUSION_CONTROLLER_STATE, ST_WAITING_SIS_BOOTSTRAP_S2_COMPLETE, &AC_SecureS2BootStrapping},
        {ST_WAITING_SIS_BOOTSTRAP_S2_COMPLETE, EV_ADD_S2_SECURE_OK, ST_WAITING_SIS_INTERVIEW_DEVICE, &AC_InterviewDevice},
        {ST_WAITING_SIS_BOOTSTRAP_S2_COMPLETE, EV_ADD_SO_SECURE_OK, ST_WAITING_SIS_INTERVIEW_DEVICE, &AC_InterviewDevice},//user only choose S0
        {ST_WAITING_SIS_BOOTSTRAP_S2_COMPLETE, EV_ADD_S2_SECURE_FAIL, ST_WAITING_SIS_INTERVIEW_DEVICE, &AC_InterviewDevice},
        //2 case: 1 add s2 fail, case 2: user choose key is 00, case 2 --> interview device
        {ST_WAITING_SIS_BOOTSTRAP_S2_COMPLETE, EV_ADD_SO_SECURE_FAIL, ST_INCLUSIONCONTROLLER_IDLE, &AC_Send_Complete_StatusFail},//user only choose S0
        {ST_WAITING_SIS_BOOTSTRAP_S2_COMPLETE, EV_DEVICE_NOT_SUPPORT_S2, ST_WAITING_INCLUSION_BOOTSTRAP_S0_COMPLETE, &AC_InclusionDeviceBootStrap_S0},
        {ST_WAITING_SIS_BOOTSTRAP_S2_COMPLETE, EV_TIMEOUT_INCLUSION_CONTROLLER_STATE, ST_INCLUSIONCONTROLLER_IDLE, &AC_Send_Complete_StatusFail},
        {ST_WAITING_SIS_BOOTSTRAP_S2_COMPLETE, EV_CLOSE_INCLUSION_CONTROLLER, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_State_PushInterviewDeviceFail},

        {ST_WAITING_SIS_INTERVIEW_DEVICE, EV_INTERVIEW_DEVICE_OK, ST_INCLUSIONCONTROLLER_IDLE, &AC_Send_Complete_Status_OK},
        {ST_WAITING_SIS_INTERVIEW_DEVICE, EV_TIMEOUT_INCLUSION_CONTROLLER_STATE, ST_INCLUSIONCONTROLLER_IDLE, &AC_Send_Complete_StatusFail},
        {ST_WAITING_SIS_INTERVIEW_DEVICE, EV_CLOSE_INCLUSION_CONTROLLER, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_State_PushInterviewDeviceFail},

        {ST_WAITING_INCLUSION_BOOTSTRAP_S0_COMPLETE, EV_RECEIVE_COMPLETE_OK_FROM_INCLUSION, ST_SIS_WAITING_S0_CMD_SUPPORTED_REPORT, &AC_SendS0CmdSupportedGet},
        {ST_WAITING_INCLUSION_BOOTSTRAP_S0_COMPLETE, EV_RECEIVE_COMPLETE_NOT_OK_FROM_INCLUSION, ST_SIS_WAITING_S0_CMD_SUPPORTED_REPORT, &AC_SendS0CmdSupportedGet},
        {ST_WAITING_INCLUSION_BOOTSTRAP_S0_COMPLETE, EV_DEVICE_NOT_SUPPORT_S0, ST_WAITING_SIS_INTERVIEW_DEVICE, &AC_InterviewDevice},
        {ST_WAITING_INCLUSION_BOOTSTRAP_S0_COMPLETE, EV_TIMEOUT_INCLUSION_CONTROLLER_STATE, ST_INCLUSIONCONTROLLER_IDLE, &AC_Send_Complete_StatusFail},
        {ST_WAITING_INCLUSION_BOOTSTRAP_S0_COMPLETE, EV_CLOSE_INCLUSION_CONTROLLER, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_State_PushInterviewDeviceFail},

        {ST_SIS_WAITING_S0_CMD_SUPPORTED_REPORT, EV_ADD_SO_SECURE_OK, ST_WAITING_SIS_INTERVIEW_DEVICE, &AC_InterviewDevice},
        {ST_SIS_WAITING_S0_CMD_SUPPORTED_REPORT, EV_ADD_SO_SECURE_FAIL, ST_WAITING_SIS_INTERVIEW_DEVICE, &AC_InterviewDevice},
        {ST_SIS_WAITING_S0_CMD_SUPPORTED_REPORT, EV_DEVICE_NOT_SUPPORT_S0, ST_WAITING_SIS_INTERVIEW_DEVICE, &AC_InterviewDevice},
        {ST_SIS_WAITING_S0_CMD_SUPPORTED_REPORT, EV_MY_NODE_NOT_SUPPORT_THIS_SCHEME, ST_WAITING_SIS_INTERVIEW_DEVICE, &AC_InterviewDevice},
        {ST_SIS_WAITING_S0_CMD_SUPPORTED_REPORT, EV_TIMEOUT_INCLUSION_CONTROLLER_STATE, ST_WAITING_SIS_INTERVIEW_DEVICE, &AC_InterviewDevice},
        {ST_SIS_WAITING_S0_CMD_SUPPORTED_REPORT, EV_CLOSE_INCLUSION_CONTROLLER, ST_INCLUSIONCONTROLLER_IDLE, &AC_Reset_State_PushInterviewDeviceFail},

};

#define TRANS_COUNT (sizeof(trans) / sizeof(*trans))

/* States can have a default timeout. If the FSM remains in the same
 * state for the specified duration, the specified callback function
 * will be called.
 * Format: {STATE, timeout in milliseconds, callback_function}
 * Only states listed here have timeouts */
static state_timeout_t timeoutTable[] = {
    {ST_WAITING_INCLUSION_CONTROLLER_COMPLETE, 60000, ZCB_TimerOutStateInclusionController},
    {ST_WAITING_NODE_INFOMATION, 10000, ZCB_TimerOutStateInclusionController},
    {ST_WAITING_SO_BOOTSTRAPPING_COMPLETE, 60000, ZCB_TimerOutStateInclusionController},
    {ST_WAITING_SECURE_S2_BOOTSTRAPPING_COMPLETE, 240000, ZCB_TimerOutStateInclusionController},
    {ST_WAITING_SECURE_S0_BOOTSTRAPPING_COMPLETE, 60000, ZCB_TimerOutStateInclusionController},

    {ST_WAITING_S0_CMD_SUPPORTED_REPORT, 20000, ZCB_TimerOutStateInclusionController},
    {ST_WAITING_S2_ACCESS_CMD_SUPPORTED_REPORT, 10000, ZCB_TimerOutStateInclusionController},
    {ST_WAITING_S2_AUTHENTICATED_CMD_SUPPORTED_REPORT, 10000, ZCB_TimerOutStateInclusionController},
    {ST_WAITING_S2_UNAUTHENTICATED_CMD_SUPPORTED_REPORT, 10000, ZCB_TimerOutStateInclusionController},
    {ST_WAITING_INTERVIEW, 50000, ZCB_TimerOutStateInclusionController},

    //my device is SIS role in network
    {ST_SIS_WAITING_NODE_INFOMATION, 10000, ZCB_TimerOutStateInclusionController},
    {ST_WAITING_SIS_BOOTSTRAP_S2_COMPLETE, 240000, ZCB_TimerOutStateInclusionController},
    {ST_WAITING_SIS_INTERVIEW_DEVICE, 50000, ZCB_TimerOutStateInclusionController},
    {ST_WAITING_INCLUSION_BOOTSTRAP_S0_COMPLETE, 60000, ZCB_TimerOutStateInclusionController},
    {ST_SIS_WAITING_S0_CMD_SUPPORTED_REPORT, 20000, ZCB_TimerOutStateInclusionController},
    {ST_WAITING_SIS_INTERVIEW_DEVICE_S0, 50000, ZCB_TimerOutStateInclusionController},
    {ST_WAITING_PUSH_NOTIFY_S2_BOOTSTRAP, 1000, ZCB_TimerOutStateInclusionController},

};
#define TIMEOUT_TABLE_COUNT (sizeof(timeoutTable) / sizeof(*timeoutTable))

/*============================   Get string of State   ===============================
**
**  Cancels the FSM timer.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/
static char *GetStateTypeStr(uint8_t st)
{

    char *stStr;

    switch (st)
    {
    case ST_INCLUSIONCONTROLLER_IDLE:
        stStr = "ST_INCLUSIONCONTROLLER_IDLE";
        break;
    case ST_WAITING_INCLUSION_CONTROLLER_COMPLETE:
        stStr = "ST_WAITING_INCLUSION_CONTROLLER_COMPLETE";
        break;
    case ST_WAITING_NODE_INFOMATION:
        stStr = "ST_WAITING_NODE_INFOMATION";
        break;
    case ST_WAITING_SO_BOOTSTRAPPING_COMPLETE:
        stStr = "ST_WAITING_SO_BOOTSTRAPPING_COMPLETE";
        break;
    case ST_WAITING_SECURE_S2_BOOTSTRAPPING_COMPLETE:
        stStr = "ST_WAITING_SECURE_S2_BOOTSTRAPPING_COMPLETE";
        break;
    case ST_WAITING_SECURE_S0_BOOTSTRAPPING_COMPLETE:
        stStr = "ST_WAITING_SECURE_S0_BOOTSTRAPPING_COMPLETE";
        break;
    case ST_WAITING_S0_CMD_SUPPORTED_REPORT:
        stStr = "ST_WAITING_S0_CMD_SUPPORTED_REPORT";
        break;
    case ST_WAITING_S2_ACCESS_CMD_SUPPORTED_REPORT:
        stStr = "ST_WAITING_S2_ACCESS_CMD_SUPPORTED_REPORT"; //5
        break;
    case ST_WAITING_S2_AUTHENTICATED_CMD_SUPPORTED_REPORT:
        stStr = "ST_WAITING_S2_AUTHENTICATED_CMD_SUPPORTED_REPORT";
        break;
    case ST_WAITING_S2_UNAUTHENTICATED_CMD_SUPPORTED_REPORT:
        stStr = "ST_WAITING_S2_UNAUTHENTICATED_CMD_SUPPORTED_REPORT";
        break;
    case ST_WAITING_INTERVIEW:
        stStr = "ST_WAITING_INTERVIEW";
        break;
    //when my device is SIS role in network
    case ST_SIS_WAITING_NODE_INFOMATION:
        stStr = "ST_SIS_WAITING_NODE_INFOMATION";
        break;
    case ST_WAITING_SIS_BOOTSTRAP_S2_COMPLETE:
        stStr = "ST_WAITING_SIS_BOOTSTRAP_S2_COMPLETE";
        break;
    case ST_WAITING_SIS_INTERVIEW_DEVICE:
        stStr = "ST_WAITING_SIS_INTERVIEW_DEVICE";
        break;
    case ST_WAITING_INCLUSION_BOOTSTRAP_S0_COMPLETE:
        stStr = "ST_WAITING_INCLUSION_BOOTSTRAP_S0_COMPLETE";
        break;
    case ST_SIS_WAITING_S0_CMD_SUPPORTED_REPORT:
        stStr = "ST_SIS_WAITING_S0_CMD_SUPPORTED_REPORT";
        break;
    case ST_WAITING_SIS_INTERVIEW_DEVICE_S0:
        stStr = "ST_WAITING_SIS_INTERVIEW_DEVICE_S0";
        break;
    case ST_WAITING_RECEIVE_COMPLETE_AFTER_BOOT_S0:
        stStr = "ST_WAITING_RECEIVE_COMPLETE_AFTER_BOOT_S0";
        break;
    case ST_WAITING_PUSH_NOTIFY_S2_BOOTSTRAP:
        stStr = "ST_WAITING_PUSH_NOTIFY_S2_BOOTSTRAP";
        break;
    default:
        stStr = " ";
        break;
    }

    return stStr;
}

/*============================   Get string of Event   ===============================
**
**  Cancels the FSM timer.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/
static char *GetEventTypeStr(uint8_t ev)
{

    char *evStr;

    switch (ev)
    {
    case EV_ADD_NODE_NORMAL_OK: //0
        evStr = "EV_ADD_NODE_NORMAL_OK";
        break;
    case EV_NO_SIS_DEVICE:
        evStr = "EV_NO_SIS_DEVICE";
        break;
    case EV_RECEIVE_INCLUSION_CONTROLLER_INITIATE_S0:
        evStr = "EV_RECEIVE_INCLUSION_CONTROLLER_INITIATE_S0";
        break;
    case EV_RECEIVE_INCLUSION_CONTROLLER_COMPLETE_OK:
        evStr = "EV_RECEIVE_INCLUSION_CONTROLLER_COMPLETE_OK";
        break;
    case EV_RECEIVE_INCLUSION_CONTROLLER_COMPLETE_NOT_OK:
        evStr = "EV_RECEIVE_INCLUSION_CONTROLLER_COMPLETE_NOT_OK";
        break;
    case EV_RECEIVE_NODE_INFOMATION: //15
        evStr = "EV_RECEIVE_NODE_INFOMATION";
        break;
    case EV_ADD_SO_SECURE_OK:
        evStr = "EV_ADD_SO_SECURE_OK";
        break;
    case EV_ADD_SO_SECURE_FAIL: //5
        evStr = "EV_ADD_SO_SECURE_FAIL";
        break;
    case EV_DEVICE_NOT_SUPPORT_S2:
        evStr = "EV_DEVICE_NOT_SUPPORT_S2";
        break;
    case EV_DEVICE_NOT_SUPPORT_S0: //5
        evStr = "EV_DEVICE_NOT_SUPPORT_S0";
        break;

    case EV_ADD_S2_SECURE_OK:
        evStr = "EV_ADD_S2_SECURE_OK";
        break;
    case EV_ADD_S2_SECURE_FAIL: //5
        evStr = "EV_ADD_S2_SECURE_FAIL";
        break;
    case EV_INTERVIEW_DEVICE_OK:
        evStr = "EV_INTERVIEW_DEVICE_OK";
        break;
    case EV_TIMEOUT_INCLUSION_CONTROLLER_STATE:
        evStr = "EV_TIMEOUT_INCLUSION_CONTROLLER_STATE";
        break;
    case EV_RECEIVE_INCLUSION_CONTROLLER_INITIATE_PROXY:
        evStr = "EV_RECEIVE_INCLUSION_CONTROLLER_INITIATE_PROXY";
        break;
    case EV_MY_NODE_NOT_SUPPORT_THIS_SCHEME:
        evStr = "EV_MY_NODE_NOT_SUPPORT_THIS_SCHEME";
        break;
    case EV_RECEIVE_COMPLETE_OK_FROM_INCLUSION:
        evStr = "EV_RECEIVE_COMPLETE_OK_FROM_INCLUSION";
        break;
    case EV_RECEIVE_COMPLETE_NOT_OK_FROM_INCLUSION:
        evStr = "EV_RECEIVE_COMPLETE_NOT_OK_FROM_INCLUSION";
        break;
    case EV_CLOSE_INCLUSION_CONTROLLER:
        evStr = "EV_CLOSE_INCLUSION_CONTROLLER";
        break;
    default:
        evStr = " ";
        break;
    }
    return evStr;
}

/*============================   StopTimer   ===============================
**
**  Cancels the FSM timer.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/
static void StopTimer()
{
    if (bFsmTimerHandle_AutoAdd != NO_TIMER)
    {
        timerCancel(&bFsmTimerHandle_AutoAdd);
        bFsmTimerHandle_AutoAdd = NO_TIMER;
    }
}

/*===========================   InclusionController_ZCB_FsmTimer   ==============================
**
**  Timer callback function for the default FSM timer.
**  If a timeout and a callback function is specified in the
**  timeoutTable, ths callback function will be called automatically
**  by this timer function.
**
**  The callback function will only be called if no state change has occurred
**  within the specified timeout since entering the current state.
**
**  Side effects: None
**
**  If Timeout, this function is executed
**-------------------------------------------------------------------------*/
void InclusionController_ZCB_FsmTimer(void *data)
{
    mainlog(logDebug, "FsmTimer-bFsmTimerCountdown_InclusionController: %u", bFsmTimerCountdown_InclusionController);
    if (bFsmTimerCountdown_InclusionController > 0)
    {
        bFsmTimerCountdown_InclusionController--;
        return;
    }
    /* The timer repeat count should already have stopped the timer,
    * but better safe than sorry. */
    StopTimer();
    if (cbFsmTimer_AutoAdd != NULL) //pointer function
    {
        cbFsmTimer_AutoAdd(data); //function
    }
}

/*============================   StartTimer   ===============================
**
**  Start the FSM timer. Note the higher resolution if timeout is
**  shorter than or equal to 2.55 seconds.
**
**  Side effects: Cancels running FSM timer if any.
**
**-------------------------------------------------------------------------*/
static void StartTimer(
    uint32_t timeout,       /* IN Unit: 10 ms ticks. If over 255, resolution is 1 second */
    void (*cbFunc)(void *)) /* IN timeout callback function */
{
    StopTimer();
    int ret = 0;
    if (timeout > 255)
    {
        /* Timeout larger than single timeout. */
        /* Convert timeout from 10 ms ticks to seconds*/
        bFsmTimerCountdown_InclusionController = timeout / 1000;
        cbFsmTimer_AutoAdd = cbFunc;
        ret = timerStart(&bFsmTimerHandle_AutoAdd, InclusionController_ZCB_FsmTimer, NULL, 1000, TIMER_FOREVER);
    }
    else
    {
        /* timeout is in range 10..2550 ms */
        ret = timerStart(&bFsmTimerHandle_AutoAdd, cbFunc, NULL, timeout * 10, TIMER_ONETIME);
    }

    if (ret)
    {
        mainlog(logDebug, "Failed to get timer for FsmTimer +++++");
    }
}

/*=========================   auto_add_paraStartStateTimer   ============================
**
**  Find and start the state timer for current state, if one has been
**  defined in timeoutTable.
**
**  Side effects: Cancels the currently running state timer.
**
**-------------------------------------------------------------------------*/
static void StartStateTimer()
{
    uint8_t i;
    for (i = 0; i < TIMEOUT_TABLE_COUNT; i++)
    {
        if (state == timeoutTable[i].st)
        {
            StartTimer(timeoutTable[i].timeout, timeoutTable[i].tfn);
            mainlog(logDebug, "StartStateTimer: index %d", i);
            if (timeoutTable[i].tfn == NULL)
            {
                mainlog(logDebug, "StartStateTimer:NULL function");
            }
        }
    }
}

/*============================   InterviewDevice_ZCB_PostEvent   ===============================
**
**  Post an event to the finite state machine. Update the state,
**  run action function associated with the transition and start
**  the state timer for the next state.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/
void PostEventInclusionController(INCLUSION_CONTROLLER_FSM_EV_T event)
{
    INCLUSION_CONTROLLER_FSM_STATES_T old_state;
    uint8_t i;
    for (i = 0; i < TRANS_COUNT; i++)
    {
        if ((state == trans[i].st)) //have state none support
        {
            if ((event == trans[i].ev))
            {
                old_state = trans[i].st;

                state = trans[i].next_st;

                mainlog(logDebug, "----InclusionController_ZCB_PostEvent: %s [%u]-----", GetEventTypeStr(event), event);
                mainlog(logDebug, "----%s [%u] ---> %s [%u]---------------", GetStateTypeStr(old_state), old_state, GetStateTypeStr(state), state);
                if (old_state != state)
                {
                    StopTimer();
                    StartStateTimer();
                }
                if ((trans[i].fn) != NULL)
                {
                    (trans[i].fn());
                }
                break;
            }
        }
    }
}
