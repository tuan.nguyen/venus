#ifndef _SUPPORTED_COMMAND_CLASS_MULTICHAN__
#define _SUPPORTED_COMMAND_CLASS_MULTICHAN__

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"
#include "config.h"
#include "ZW_TransportEndpoint.h"


uint8_t CmdClassMultiChannelEncapsule(
                                    uint8_t     *pData,
                                    uint8_t     dataLength,
                                    ts_param_t  *p,
                                    void (*completedFunc)(uint8_t, void * , TX_STATUS_TYPE *t ),
                                    void* user);

void  MultiChanCommandHandler(  
                                    ts_param_t    *p,               /* transport param */
                                    uint8_t       *pCmd,            /* IN  Payload from the received frame */
                                    uint8_t       cmdLength);       /* IN Number of command bytes including the command */

#endif
