#ifndef _SUPPORTED_COMMAND_CLASS_ZWAVEPLUSINFO__
#define _SUPPORTED_COMMAND_CLASS_ZWAVEPLUSINFO__

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"
#include "config.h"
#include "ZW_TransportEndpoint.h"





/*==============================   handleCommandClassZWavePlusInfo  ============
**
**  Function:  handler for ZWavePlusInfo CC
**
**  Side effects: None
**
**--------------------------------------------------------------------------*/
received_frame_status_t  
handleCommandClassZWavePlusInfo(ts_param_t *p,
                                ZW_APPLICATION_TX_BUFFER *pCmd,    /* IN Payload from the received frame, the union */
                                uint8_t   cmdLength                   /* IN Number of command bytes including the command */
);

uint8_t CommandClassZWavePlusInfoVersionGet(void);




#endif