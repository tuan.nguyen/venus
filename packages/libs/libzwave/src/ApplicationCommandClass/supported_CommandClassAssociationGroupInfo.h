#ifndef _SUPPORTED_COMMAND_CLASS_ASSOCIATION_GROUP_INFO_
#define _SUPPORTED_COMMAND_CLASS_ASSOCIATION_GROUP_INFO_

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"
#include "config.h"
#include "timer.h"
#include "ZW_TransportEndpoint.h"


#define ASSOCIATION_SIZE            (MAX_ASSOCIATION_GROUPS * MAX_ASSOCIATION_IN_GROUP)




received_frame_status_t 
handleCommandClassAssociationGroupInfo(ts_param_t *p,                  
                                        ZW_APPLICATION_TX_BUFFER *pCmd,   
                                        uint8_t   cmdLength);

/** 
 * @brief CommandClassAssociationGroupInfoVersionGet
 * Get version
 * @return version
 */
uint8_t
CommandClassAssociationGroupInfoVersionGet(void);

#endif
