#include "supported_CommandClassVersion.h"
#include "serialAPI.h"
#include "utils.h"
#include "cmd_class_misc.h"
#include "config.h"
#include "zw_api.h"

void  versionResponse_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t)
{
    mainlog(logDebug, "versionResponse_Compl");
    if (TRANSMIT_COMPLETE_OK == bTxStatus)
    {
        mainlog(logDebug, "TRANSMIT_COMPLETE_OK"); 
    }
    else
    {
        mainlog(logDebug, "TRANSMIT_COMPLETE_NOT_OK"); 
    }
}


received_frame_status_t
handleCommandClassVersion(ts_param_t *p,                        /* IN receive options of type RECEIVE_OPTIONS_TYPE_EX  */
                            ZW_APPLICATION_TX_BUFFER *pCmd,     /* IN Payload from the received frame, the union */
                            uint8_t   cmdLength                /* IN Number of command bytes including the command */
)
{
    if (!SupportsFrameAtSecurityLevel(COMMAND_CLASS_VERSION_V2, p->scheme))
    {
        mainlog(logDebug, "handle Version CC p->scheme %d SupportsFrame ret 0",p->scheme );
        return RECEIVED_FRAME_STATUS_FAIL;
    }

    if(true == Check_not_legal_response_job(p))
    {
        /*Do not support endpoint bit-addressing */
        return RECEIVED_FRAME_STATUS_FAIL;
    }

        switch (pCmd->ZW_VersionGetFrame.cmd)
        {
        case VERSION_GET_V2:
        {
            uint8_t n;/*firmware target number 1..N */
            ts_param_t pTxOptionsEx;
            RxToTxOptions(p, &pTxOptionsEx);
            pTxBuf.ZW_VersionReport1byteV2Frame.cmdClass                = COMMAND_CLASS_VERSION_V2;
            pTxBuf.ZW_VersionReport1byteV2Frame.cmd                     = VERSION_REPORT_V2;
            pTxBuf.ZW_VersionReport1byteV2Frame.zWaveLibraryType        = ZW_LIB_CONTROLLER_STATIC;
            pTxBuf.ZW_VersionReport1byteV2Frame.zWaveProtocolVersion    = PROTOCOL_SDK_LIB_VERSION;
            pTxBuf.ZW_VersionReport1byteV2Frame.zWaveProtocolSubVersion = PROTOCOL_SDK_LIB_SUB_VERSION;
            handleGetFirmwareVersion( 0, (VG_VERSION_REPORT_V2_VG*)&(pTxBuf.ZW_VersionReport1byteV2Frame.firmware0Version));
            pTxBuf.ZW_VersionReport1byteV2Frame.hardwareVersion         = handleGetFirmwareHwVersion();
            pTxBuf.ZW_VersionReport1byteV2Frame.numberOfFirmwareTargets = handleNbrFirmwareVersions() - 1;/*-1 : Firmware version 0*/                                   
          
	        for( n = 1; n < handleNbrFirmwareVersions();n++)
	        {
	            handleGetFirmwareVersion( n, 
                            (VG_VERSION_REPORT_V2_VG*)(&(pTxBuf.ZW_VersionReport1byteV2Frame.variantgroup1) + (n-1)*(sizeof(VG_VERSION_REPORT_V2_VG))));
            }
          
            Transport_SendDataApplEP((uint8_t *)&pTxBuf,
                                        sizeof(pTxBuf.ZW_VersionReport1byteV2Frame) + (handleNbrFirmwareVersions() - 1)*sizeof(VG_VERSION_REPORT_V2_VG) - sizeof(VG_VERSION_REPORT_V2_VG), /*-1 is Firmware version 0*/
                                        &pTxOptionsEx,
                                        versionResponse_Compl, 0);

            return RECEIVED_FRAME_STATUS_SUCCESS;
        }
        break;

        case VERSION_COMMAND_CLASS_GET_V2:
        {
            ts_param_t pTxOptionsEx;
            RxToTxOptions(p, &pTxOptionsEx);
            pTxBuf.ZW_VersionCommandClassReportFrame.cmdClass = COMMAND_CLASS_VERSION_V2;
            pTxBuf.ZW_VersionCommandClassReportFrame.cmd = VERSION_COMMAND_CLASS_REPORT_V2;
            pTxBuf.ZW_VersionCommandClassReportFrame.requestedCommandClass = pCmd->ZW_VersionCommandClassGetFrame.requestedCommandClass;
            pTxBuf.ZW_VersionCommandClassReportFrame.commandClassVersion = handleCommandClassVersionAppl(pCmd->ZW_VersionCommandClassGetFrame.requestedCommandClass);

            Transport_SendDataApplEP((uint8_t *)&pTxBuf,
                                sizeof(pTxBuf.ZW_VersionCommandClassReportFrame),
                                &pTxOptionsEx,
                                versionResponse_Compl, 0);

            return RECEIVED_FRAME_STATUS_SUCCESS;
        }
        break;

    default:
        break;
    }
    return RECEIVED_FRAME_STATUS_NO_SUPPORT;
}


uint8_t handleGetFirmwareHwVersion(void)
{
	uint8_t hwVersion;
	/* Read hwVersion from NVR.*/
	hwVersion = LASTEST_HARDWARE_VERSION;
	return hwVersion; /*HW version*/
}

/*============================ CommandClassVersionVersionGet ================
** Function description
** Read version
**
** Side effects: 
**
**-------------------------------------------------------------------------*/
uint8_t CommandClassVersionVersionGet(void)
{
	return VERSION_VERSION_V2; 
}

