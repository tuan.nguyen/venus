#include <stdbool.h>
#include <stdint.h> //have uint8_t
#include <unistd.h> //contain usleep
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"

#include "config.h"
#include "utils.h"
#include "util_intelhex.h"
#include "util_crc.h"
#include "nvm.h"
#include "config.h"

#include "timing.h"
#include "timer.h"
#include "ZW_TransportSecurity.h"
#include "controlled_CommandClassUpdateFirmware.h"
#include "ZW_PollApi.h"//polling dev
#include "ZW_DevicePoll.h"
#define NO_TIMER 0   /* Unused timer handle */
#define NO_TIMEOUT 0 /* No timeout from this state */

#define STATE enum UPDATE_FIRMWARE_MD_FSM_STATES

static uint8_t percent = 0;

typedef struct
{
    STATE st;                       // current state
    UPDATE_FIRMWARE_MD_FSM_EV_T ev; // incoming event
    STATE next_st;                  // next state
    void (*fn)();                   // action function returning next state
} transition_t;

typedef struct
{
    STATE st;            //current state
    uint32_t timeout;    //timeout of sate (ms)
    void (*tfn)(void *); //fuction is execute when timeout
} state_timeout_t;

#define LENGTH_DATA_NON_SECURE 40
#define LENGTH_DATA_S0 30
#define LENGTH_DATA_S2 20

/*
Version 1
Firmware Update Meta Data Report Command does not contain checksum
Version 2
Firmware Update Meta Data Report Command contain Checksum
All fields not described below remain unchanged from version 1
Firmware Update Meta Data Command Class, version 3

*/
//variable push notify
NOTIFY_TX_BUFFER_T pTxNotify;
NODE_TYPE node_Type;

//variable for timeout
static timer_t bFsmTimerHandle_UpdateFirmware = NO_TIMER;
static uint8_t bFsmTimerCountdown_UpdateFirmware;
static void (*cbFsmTimer_AutoAdd)(void *);
//variable for this file
static STATE state = ST_UPDATE_FIRMWARE_MD_IDLE;

uint8_t *flash_mem_update_file = NULL;
//uint8_t pData_UpdateFirmwareReport[50];
uint32_t length_file_update, offset_address = 0;
uint8_t scheme_node;
uint16_t crc_flashMem;
uint8_t index_frame_into_1_report = 1;
uint8_t retry_send_firmware_report = 1;

/*=============================== SendDataUpdateFirmware_Compl ====================================
**    Description:  
**    Returns:      Nothing
**    Parameters
**-----------------------------------------------------------------------------------------------*/
void SendDataUpdateFirmware_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t)
{
    switch (bTxStatus)
    {
    case TRANSMIT_COMPLETE_OK:
        mainlog(logDebug, "SDA: [TRANSMIT_COMPLETE_OK(%02X)]  Transmit Data is successful.", bTxStatus);
        PostEventUpdateFirmwareMd(EV_RECEIVE_ACK);
        break;
    case TRANSMIT_COMPLETE_NO_ACK:
        mainlog(logDebug, "SDA: [TRANSMIT_COMPLETE_NO_ACK(%02X)]  No ack is received before timeout.", bTxStatus);
        PostEventUpdateFirmwareMd(EV_CANNOT_SEND_CMD);
        break;
    case TRANSMIT_COMPLETE_FAIL:
        mainlog(logDebug, "SDA: [TRANSMIT_COMLETE_FAIL(%02X)]  Not possible to transmit data (Network busy)", bTxStatus);
        PostEventUpdateFirmwareMd(EV_CANNOT_SEND_CMD);
        break;
    }
}

void send_data_scheme(uint8_t node_id, uint8_t *pDataIn, uint8_t pData_lenIn, uint8_t scheme)
{

    ts_param_t p;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = scheme;

    p.snode = MyNodeId;
    p.sendpoint = 0;

    p.dnode = node_id;
    p.dendpoint = 0;
    Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)pDataIn,
                             pData_lenIn,
                             &p,
                             SendDataUpdateFirmware_Compl, 0);
}

void AC_Calculate_CRC(void)
{
    crc_flashMem = ccittCrc16(0x1D0F, flash_mem_update_file, length_file_update);
    mainlog(logDebug, "flashMem length: 0x%04X,  crc = %04X", length_file_update, crc_flashMem);

    PostEventUpdateFirmwareMd(EV_CALCULATE_CRC16_FINISH);
}

void AC_SendFirmwareMdGet(void)
{

    ts_param_t p;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = scheme_node;

    p.snode = MyNodeId;
    p.sendpoint = 0;

    p.dnode = nm.tmp_node;
    p.dendpoint = 0;

    pTxBuf.ZW_FirmwareMdGetV2Frame.cmdClass = COMMAND_CLASS_FIRMWARE_UPDATE_MD;
    pTxBuf.ZW_FirmwareMdGetV2Frame.cmd = FIRMWARE_MD_GET;

    Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                             sizeof(ZW_FIRMWARE_MD_GET_V2_FRAME), &p, SendDataUpdateFirmware_Compl, NULL);
}

void AC_SendVersionGet(void)
{
    ts_param_t p;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = scheme_node;

    p.snode = MyNodeId;
    p.sendpoint = 0;

    p.dnode = nm.tmp_node;
    p.dendpoint = 0;

    pTxBuf.ZW_VersionGetFrame.cmdClass = COMMAND_CLASS_VERSION;
    pTxBuf.ZW_VersionGetFrame.cmd = VERSION_GET;

    Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                             sizeof(ZW_VERSION_GET_FRAME), &p, SendDataUpdateFirmware_Compl, NULL);
}

void AC_SendVersionCommandClassGet(void)
{
    ts_param_t p;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = scheme_node;

    p.snode = MyNodeId;
    p.sendpoint = 0;

    p.dnode = nm.tmp_node;
    p.dendpoint = 0;

    pTxBuf.ZW_VersionCommandClassGetFrame.cmdClass = COMMAND_CLASS_VERSION;
    pTxBuf.ZW_VersionCommandClassGetFrame.cmd = VERSION_COMMAND_CLASS_GET;
    pTxBuf.ZW_VersionCommandClassGetFrame.requestedCommandClass = COMMAND_CLASS_FIRMWARE_UPDATE_MD;

    Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                             sizeof(ZW_VERSION_COMMAND_CLASS_GET_FRAME), &p, SendDataUpdateFirmware_Compl, NULL);
}

/*============================ firmware_update_md_request_get ====================================
**    Description:  
**    Returns:      Nothing
**    Parameters
**-----------------------------------------------------------------------------------------------*/
void firmware_update_md_request_get(uint8_t nodeId, uint16_t crc_flashMem, uint16_t manufacturer_id, uint16_t firmware_id, uint8_t scheme)
{

    ts_param_t p;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = scheme_node;

    p.snode = MyNodeId;
    p.sendpoint = 0;

    p.dnode = nm.tmp_node;
    p.dendpoint = 0;

    pTxBuf.ZW_FirmwareUpdateMdRequestGetV2Frame.cmdClass = COMMAND_CLASS_FIRMWARE_UPDATE_MD;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV2Frame.cmd = FIRMWARE_UPDATE_MD_REQUEST_GET;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV2Frame.manufacturerId1 = manufacturer_id >> 8;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV2Frame.manufacturerId2 = manufacturer_id & 0xFF;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV2Frame.firmwareId1 = firmware_id >> 8;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV2Frame.firmwareId2 = firmware_id & 0xFF;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV2Frame.checksum1 = crc_flashMem >> 8;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV2Frame.checksum2 = crc_flashMem & 0xFF;
    Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                             sizeof(ZW_FIRMWARE_UPDATE_MD_REQUEST_GET_V2_FRAME), &p, SendDataUpdateFirmware_Compl, NULL);
}

/*============================ firmware_update_md_request_get_version_3_4 ====================================
**    Description:  
**    Returns:      Nothing
**    Parameters
**-----------------------------------------------------------------------------------------------*/
void firmware_update_md_request_get_V3_4(uint8_t nodeId, uint16_t crc_flashMem, uint16_t manufacturer_id,
                                         uint16_t firmware_id, uint8_t firmware_target, uint16_t fragment_size, uint8_t scheme)
{
    ts_param_t p;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = scheme_node;

    p.snode = MyNodeId;
    p.sendpoint = 0;

    p.dnode = nm.tmp_node;
    p.dendpoint = 0;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV3Frame.cmdClass = COMMAND_CLASS_FIRMWARE_UPDATE_MD;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV3Frame.cmd = FIRMWARE_UPDATE_MD_REQUEST_GET;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV3Frame.manufacturerId1 = manufacturer_id >> 8;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV3Frame.manufacturerId2 = manufacturer_id & 0xFF;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV3Frame.firmwareId1 = firmware_id >> 8;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV3Frame.firmwareId2 = firmware_id & 0xFF;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV3Frame.checksum1 = crc_flashMem >> 8;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV3Frame.checksum2 = crc_flashMem & 0xFF;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV3Frame.firmwareTarget = firmware_target;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV3Frame.fragmentSize1 = fragment_size >> 8;
    pTxBuf.ZW_FirmwareUpdateMdRequestGetV3Frame.fragmentSize2 = fragment_size & 0xFF;
    Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                             sizeof(ZW_FIRMWARE_UPDATE_MD_REQUEST_GET_V3_FRAME), &p, SendDataUpdateFirmware_Compl, NULL);
}

void AC_SendFirmwareUpdateMdRequestGet(void)
{
    firmware_update_md_request_get(nm.tmp_node, crc_flashMem,
                                   handle_firmware_update_report.manufacturer_id,
                                   handle_firmware_update_report.firmware_id, scheme_node);
}

void AC_SendFirmwareUpdateMdRequestGet_V3_4(void)
{
    //only update for FirmwareTaget 0
    firmware_update_md_request_get_V3_4(nm.tmp_node, crc_flashMem,
                                        handle_firmware_update_report_v3_4.manufacturer_id,
                                        handle_firmware_update_report_v3_4.firmware0_id, 0x00, handle_firmware_update_report_v3_4.max_fragment_size, scheme_node);
}
/*==================================== SendEndOfFrame ===========================================
**    Description:  
**    Returns:      Nothing
**    Parameters
**-----------------------------------------------------------------------------------------------*/

void SendEndOfFrame(uint8_t length_last_frame, uint32_t offset_address)
{

    mainlog(logDebug, "offset_address: 0x%04X / 0x%04X report_number: %04u",
            offset_address, length_file_update, handle_firmware_update_report.report_number);
    //push percent to app, 25% --> 75 %.
    NOTIFY_TX_BUFFER_T pTxNotify;
    pTxNotify.FirmwareUpdateNotify.deviceId = nm.tmp_node;
    pTxNotify.FirmwareUpdateNotify.status = WAITING_FIRMWARE_UPDATE;
    pTxNotify.FirmwareUpdateNotify.percent = (offset_address * 100) / length_file_update;

    if (pTxNotify.FirmwareUpdateNotify.percent > percent)
    {
        PushNotificationToHandler(FIRMWARE_UPDATE_NOTIFY, (uint8_t *)&pTxNotify.FirmwareUpdateNotify, sizeof(FIRMWARE_UPDATE_NOTIFY_T));
        percent = pTxNotify.FirmwareUpdateNotify.percent;
    }

    uint8_t indx;
    ts_param_t p;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = scheme_node;

    p.snode = MyNodeId;
    p.sendpoint = 0;

    p.dnode = nm.tmp_node;
    p.dendpoint = 0;

    pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.cmdClass = COMMAND_CLASS_FIRMWARE_UPDATE_MD;
    pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.cmd = FIRMWARE_UPDATE_MD_REPORT;
    pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.properties1 = (handle_firmware_update_report.report_number >> 8) | (0x80);
    pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.reportNumber2 = (uint8_t)(handle_firmware_update_report.report_number & 0xff);

    for (indx = 0; indx < length_last_frame; indx++)
    {
        *(&pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.data1 + indx) = flash_mem_update_file[offset_address + indx];
    }
    /*calculate crc*/
    uint16_t crc;
    crc = ccittCrc16(0x1D0F, &pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.cmdClass, length_last_frame + 4);
    *(&pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.cmdClass + length_last_frame + 4) = (uint8_t)(crc >> 8);
    *(&pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.cmdClass + length_last_frame + 5) = (uint8_t)crc;

    Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                             sizeof(ZW_FIRMWARE_UPDATE_MD_REPORT_1BYTE_V2_FRAME) + length_last_frame - 1, &p, SendDataUpdateFirmware_Compl, NULL);
}

/*==================================== SendNormalFrame ===========================================
**    Description:  
**    Returns:      Nothing
**    Parameters
**-----------------------------------------------------------------------------------------------*/

void SendNormalFrame(uint8_t length_data, uint32_t offset_address)
{

    mainlog(logDebug, "offset_address: 0x%04X / 0x%04X report_number: %04u",
            offset_address, length_file_update, handle_firmware_update_report.report_number);
    //push percent to app, 25% --> 75 %.
    NOTIFY_TX_BUFFER_T pTxNotify;
    pTxNotify.FirmwareUpdateNotify.deviceId = nm.tmp_node;
    pTxNotify.FirmwareUpdateNotify.status = WAITING_FIRMWARE_UPDATE;
    pTxNotify.FirmwareUpdateNotify.percent = (offset_address * 100) / length_file_update;

    if (pTxNotify.FirmwareUpdateNotify.percent > percent)
    {
        PushNotificationToHandler(FIRMWARE_UPDATE_NOTIFY, (uint8_t *)&pTxNotify.FirmwareUpdateNotify, sizeof(FIRMWARE_UPDATE_NOTIFY_T));
        percent = pTxNotify.FirmwareUpdateNotify.percent;
    }

    uint8_t indx;
    ts_param_t p;
    p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
    p.scheme = scheme_node;

    p.snode = MyNodeId;
    p.sendpoint = 0;

    p.dnode = nm.tmp_node;
    p.dendpoint = 0;

    pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.cmdClass = COMMAND_CLASS_FIRMWARE_UPDATE_MD;
    pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.cmd = FIRMWARE_UPDATE_MD_REPORT;
    pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.properties1 = (handle_firmware_update_report.report_number >> 8) & (0x7F);
    pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.reportNumber2 = (uint8_t)(handle_firmware_update_report.report_number & 0xff);

    for (indx = 0; indx < length_data; indx++)
    {
        *(&pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.data1 + indx) = *(flash_mem_update_file + offset_address + indx);
    }
    /*calculate crc*/
    uint16_t crc;
    crc = ccittCrc16(0x1D0F, &pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.cmdClass, length_data + 4);
    *(&pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.cmdClass + length_data + 4) = (uint8_t)(crc >> 8);
    *(&pTxBuf.ZW_FirmwareUpdateMdReport1byteV2Frame.cmdClass + length_data + 5) = (uint8_t)crc;

    Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                             sizeof(ZW_FIRMWARE_UPDATE_MD_REPORT_1BYTE_V2_FRAME) + length_data - 1, &p, SendDataUpdateFirmware_Compl, NULL);
}

void SendUpdateFirmwareMdReport(void)
{
    uint8_t length_data = ((scheme_node == SECURITY_SCHEME_0) || (scheme_node == NO_SCHEME)) ? LENGTH_DATA_NON_SECURE : LENGTH_DATA_S2;
    length_data = ((handle_firmware_update_report.support_firmware_update_md_version >= 3) && (length_data > handle_firmware_update_report_v3_4.max_fragment_size)) ? handle_firmware_update_report_v3_4.max_fragment_size : length_data;
    uint32_t offset_address = (handle_firmware_update_report.report_number - 1) * length_data;
    /*firmware update md version 4, max data in 1 frame is 20 byte*/
    if ((offset_address + length_data) >= length_file_update)
    {
        PostEventUpdateFirmwareMd(EV_END_OF_FILE);
    }
    else
    {
        SendNormalFrame(length_data, offset_address);
    }
}

void AC_SendUpdateFirmwareMdReport(void)
{
    SendUpdateFirmwareMdReport();
}

void AC_SendLastUpdateFirmwareMdReport(void)
{
    /*send the lastFrame with last = true*/
    /*firmware update md version 4, max data in 1 frame is 20 byte*/
    uint8_t length_data = ((scheme_node == SECURITY_SCHEME_0) || (scheme_node == NO_SCHEME)) ? LENGTH_DATA_NON_SECURE : LENGTH_DATA_S2;
    length_data = ((handle_firmware_update_report.support_firmware_update_md_version >= 3) && 
        (length_data > handle_firmware_update_report_v3_4.max_fragment_size)) ? handle_firmware_update_report_v3_4.max_fragment_size : length_data;    
    uint32_t offset_address = (handle_firmware_update_report.report_number - 1) * length_data;
    uint16_t length_last_frame = length_file_update - offset_address;

    SendEndOfFrame(length_last_frame, offset_address);
}

void AC_PushNotifyFirmWareUpdateOK(void)
{
    /*delay WaitTime if support_firmware_update_md_version >=3 */
    /*The WaitTime field MUST report the time that is needed before the receiving node again becomes
        available for communication after the transfer of an image.*/
    NOTIFY_TX_BUFFER_T pTxNotify;
    pTxNotify.FirmwareUpdateNotify.deviceId = nm.tmp_node;
    pTxNotify.FirmwareUpdateNotify.version = handle_firmware_update_report.support_firmware_update_md_version;
    pTxNotify.FirmwareUpdateNotify.status = FIRMWARE_UPDATE_SUCCESS;
    if (pTxNotify.FirmwareUpdateNotify.version >= 3)
        pTxNotify.FirmwareUpdateNotify.wait_time = handle_firmware_update_report_v3_4.wait_time;
    else
        pTxNotify.FirmwareUpdateNotify.wait_time = 0;
    PushNotificationToHandler(FIRMWARE_UPDATE_NOTIFY, (uint8_t *)&pTxNotify.FirmwareUpdateNotify, sizeof(FIRMWARE_UPDATE_NOTIFY_T));

    if (flash_mem_update_file)
    {
        free(flash_mem_update_file);
    }
    postEventOperationPollingDev(EV_USER_FIRMWARE_UPDATE_MD_DONE);
}

void AC_PushNotifyFileIsFail(void)
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    pTxNotify.FirmwareUpdateNotify.deviceId = nm.tmp_node;
    pTxNotify.FirmwareUpdateNotify.status = FIRMWARE_UPDATE_FAIL_TO_OPEN_FILE;

    PushNotificationToHandler(FIRMWARE_UPDATE_NOTIFY, (uint8_t *)&pTxNotify.FirmwareUpdateNotify, sizeof(FIRMWARE_UPDATE_NOTIFY_T));

    if (flash_mem_update_file)
    {
        free(flash_mem_update_file);
    }
    postEventOperationPollingDev(EV_USER_FIRMWARE_UPDATE_MD_DONE);
}

void AC_PushNotify_DeviceNotRespone(void)
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    pTxNotify.FirmwareUpdateNotify.deviceId = nm.tmp_node;
    pTxNotify.FirmwareUpdateNotify.status = FIRMWARE_UPDATE_DEVICE_NOT_RESPONE;

    PushNotificationToHandler(FIRMWARE_UPDATE_NOTIFY, (uint8_t *)&pTxNotify.FirmwareUpdateNotify, sizeof(FIRMWARE_UPDATE_NOTIFY_T));

    if (flash_mem_update_file)
    {
        free(flash_mem_update_file);
    }
    postEventOperationPollingDev(EV_USER_FIRMWARE_UPDATE_MD_DONE);
}

void AC_PushNotify_DeviceNotRespone_NonFree(void)
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    pTxNotify.FirmwareUpdateNotify.deviceId = nm.tmp_node;
    pTxNotify.FirmwareUpdateNotify.status = FIRMWARE_UPDATE_DEVICE_NOT_RESPONE;

    PushNotificationToHandler(FIRMWARE_UPDATE_NOTIFY, (uint8_t *)&pTxNotify.FirmwareUpdateNotify, sizeof(FIRMWARE_UPDATE_NOTIFY_T));
    /*send the lastFrame with last = true*/
    uint8_t length_data = ((scheme_node == SECURITY_SCHEME_0) || (scheme_node == NO_SCHEME)) ? LENGTH_DATA_NON_SECURE : LENGTH_DATA_S2;
    uint32_t offset_address = (handle_firmware_update_report.report_number - 1) * length_data;

    SendEndOfFrame(length_data, offset_address);
}

void AC_PushNotifyFirmWareUpdateFail(void)
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    pTxNotify.FirmwareUpdateNotify.deviceId = nm.tmp_node;
    pTxNotify.FirmwareUpdateNotify.version = handle_firmware_update_report.support_firmware_update_md_version;
    pTxNotify.FirmwareUpdateNotify.status = FIRMWARE_UPDATE_FAIL;
    if (pTxNotify.FirmwareUpdateNotify.version >= 3)
        pTxNotify.FirmwareUpdateNotify.wait_time = handle_firmware_update_report_v3_4.wait_time;
    else
        pTxNotify.FirmwareUpdateNotify.wait_time = 0;
    PushNotificationToHandler(FIRMWARE_UPDATE_NOTIFY, (uint8_t *)&pTxNotify.FirmwareUpdateNotify, sizeof(FIRMWARE_UPDATE_NOTIFY_T));

    if (flash_mem_update_file)
    {
        free(flash_mem_update_file);
    }
    postEventOperationPollingDev(EV_USER_FIRMWARE_UPDATE_MD_DONE);
}

/*push notify, free flash_mem_update_file*/
void AC_PushNotify_CancelFirmwareUpdate(void)
{
    /*send the lastFrame with last = true*/
    uint8_t length_data = ((scheme_node == SECURITY_SCHEME_0) || (scheme_node == NO_SCHEME)) ? LENGTH_DATA_NON_SECURE : LENGTH_DATA_S2;
    uint32_t offset_address = (handle_firmware_update_report.report_number - 1) * length_data;

    SendEndOfFrame(length_data, offset_address);

    NOTIFY_TX_BUFFER_T pTxNotify;
    pTxNotify.FirmwareUpdateNotify.deviceId = nm.tmp_node;
    pTxNotify.FirmwareUpdateNotify.status = FIRMWARE_UPDATE_STOP_BY_USER;

    PushNotificationToHandler(FIRMWARE_UPDATE_NOTIFY, (uint8_t *)&pTxNotify.FirmwareUpdateNotify, sizeof(FIRMWARE_UPDATE_NOTIFY_T));

    if (flash_mem_update_file)
    {
        free(flash_mem_update_file);
    }
    postEventOperationPollingDev(EV_USER_FIRMWARE_UPDATE_MD_DONE);
}

/*push notify, free flash_mem_update_file*/
void AC_PushNotify_CancelNonFreeFlash(void)
{
    /*send the lastFrame with last = true*/
    uint8_t length_data = ((scheme_node == SECURITY_SCHEME_0) || (scheme_node == NO_SCHEME)) ? LENGTH_DATA_NON_SECURE : LENGTH_DATA_S2;
    uint32_t offset_address = (handle_firmware_update_report.report_number - 1) * length_data;

    SendEndOfFrame(length_data, offset_address);

    NOTIFY_TX_BUFFER_T pTxNotify;
    pTxNotify.FirmwareUpdateNotify.deviceId = nm.tmp_node;
    pTxNotify.FirmwareUpdateNotify.status = FIRMWARE_UPDATE_STOP_BY_USER;

    PushNotificationToHandler(FIRMWARE_UPDATE_NOTIFY, (uint8_t *)&pTxNotify.FirmwareUpdateNotify, sizeof(FIRMWARE_UPDATE_NOTIFY_T));
}

void AC_PushNotify_TimeoutFirmwareUpdate(void)
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    pTxNotify.FirmwareUpdateNotify.deviceId = nm.tmp_node;
    pTxNotify.FirmwareUpdateNotify.status = FIRMWARE_UPDATE_TIMOUT_STATE;

    PushNotificationToHandler(FIRMWARE_UPDATE_NOTIFY, (uint8_t *)&pTxNotify.FirmwareUpdateNotify, sizeof(FIRMWARE_UPDATE_NOTIFY_T));

    if (flash_mem_update_file)
    {
        free(flash_mem_update_file);
    }
    postEventOperationPollingDev(EV_USER_FIRMWARE_UPDATE_MD_DONE);
}

void AC_PushNotify_CancelByUserAndFreeFlash(void)
{
    NOTIFY_TX_BUFFER_T pTxNotify;
    pTxNotify.FirmwareUpdateNotify.deviceId = nm.tmp_node;
    pTxNotify.FirmwareUpdateNotify.status = FIRMWARE_UPDATE_STOP_BY_USER;

    PushNotificationToHandler(FIRMWARE_UPDATE_NOTIFY, (uint8_t *)&pTxNotify.FirmwareUpdateNotify, sizeof(FIRMWARE_UPDATE_NOTIFY_T));

    if (flash_mem_update_file)
    {
        free(flash_mem_update_file);
    }
    postEventOperationPollingDev(EV_USER_FIRMWARE_UPDATE_MD_DONE);
}

void AC_CheckNoReportInto1frame(void)
{
    /*If no_reports = 1 --> don't increase report_number
      If no_reports = 2 --> increase report_number 1 unit */

    if (index_frame_into_1_report < handle_firmware_update_report.no_reports)
    {
        SendUpdateFirmwareMdReport();
        index_frame_into_1_report++;
        handle_firmware_update_report.report_number++;
    }
    else
    {
        PostEventUpdateFirmwareMd(EV_SEND_ALL_NO_REPORT);
        index_frame_into_1_report = 1;
    }
    retry_send_firmware_report = 1;
}

void AC_CheckNoRetry(void)
{
    /*If no_reports = 1 --> don't increase report_number
      If no_reports = 2 --> increase report_number 1 unit */

    if (retry_send_firmware_report < 3)
    {
        retry_send_firmware_report++;
    }
    else
    {
        PostEventUpdateFirmwareMd(EV_RETRY_3_TIMES);
        retry_send_firmware_report = 1;
    }
}
/*=======================   ZCB_TimerOutState_UpdateFirmware   ========================
**
**  Timeout function that returns the FSB to idle.
**  Called by the FSM timer.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/

void ZCB_TimerOutState_UpdateFirmware(void *data)
{
    PostEventUpdateFirmwareMd(EV_TIMEOUT_STATE_FIRMWARE_UPDATE);
}
//--------------------------------------------------------------------------
/* State transition table
 * This is where the state machine is defined.
 *
 * Each transition can optionally trigger a call to an action function.
 * An action function defines the actions that must occur when a particular
 * transition occurs.
 *
 * Format: {Current state, incoming event, next state, action_function} */

static const transition_t trans[] =
    {
        //my device is Inclusion
        {ST_UPDATE_FIRMWARE_MD_IDLE, EV_UPDATE_FIRMWARE_MD_START, ST_WAITING_CHECK_FILE_FINISH, NULL},
        {ST_UPDATE_FIRMWARE_MD_IDLE, EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_OK, ST_FIRMWARE_UPDATE_OK, &AC_PushNotifyFirmWareUpdateOK},
        {ST_UPDATE_FIRMWARE_MD_IDLE, EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_NOT_OK, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotifyFirmWareUpdateFail},
        //{ST_UPDATE_FIRMWARE_MD_IDLE, EV_RX_FIRMWARE_UPDATE_GET_OK, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_CancelFirmwareUpdate},

        {ST_WAITING_CHECK_FILE_FINISH, EV_CHECK_FILE_OK, ST_WAITING_LOAD_FILE_HEX_FINISH, &AC_Calculate_CRC},
        {ST_WAITING_CHECK_FILE_FINISH, EV_CHECK_FILE_NOT_OK, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotifyFileIsFail},
        {ST_WAITING_CHECK_FILE_FINISH, EV_TIMEOUT_STATE_FIRMWARE_UPDATE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_TimeoutFirmwareUpdate},
        {ST_WAITING_CHECK_FILE_FINISH, EV_CANCEL_UPDATE_FIRMWARE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_CancelByUserAndFreeFlash},

        {ST_WAITING_LOAD_FILE_HEX_FINISH, EV_CALCULATE_CRC16_FINISH, ST_WAITING_GET_APPLICATION_VER, &AC_SendVersionGet},
        {ST_WAITING_LOAD_FILE_HEX_FINISH, EV_CANCEL_UPDATE_FIRMWARE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_CancelByUserAndFreeFlash},
        {ST_WAITING_LOAD_FILE_HEX_FINISH, EV_TIMEOUT_STATE_FIRMWARE_UPDATE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_TimeoutFirmwareUpdate},
        /*get supported firmwareUpdateMd Version*/
        /*GET APPLICATION VERSION*/
        {ST_WAITING_GET_APPLICATION_VER, EV_GET_APPLICATION_VER_OK, ST_GET_FIRMWARE_UPDATE_MD_CC_VER, &AC_SendVersionCommandClassGet},
        {ST_WAITING_GET_APPLICATION_VER, EV_CANCEL_UPDATE_FIRMWARE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_CancelByUserAndFreeFlash},
        {ST_WAITING_GET_APPLICATION_VER, EV_TIMEOUT_STATE_FIRMWARE_UPDATE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_TimeoutFirmwareUpdate},
        {ST_WAITING_GET_APPLICATION_VER, EV_CANNOT_SEND_CMD, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_DeviceNotRespone},
        {ST_WAITING_GET_APPLICATION_VER, EV_GET_APPLICATION_VER_OK, ST_GET_FIRMWARE_UPDATE_MD_CC_VER, &AC_SendVersionCommandClassGet},

        {ST_GET_FIRMWARE_UPDATE_MD_CC_VER, EV_VERSION_CC_REPORT, ST_WAITING_FIRMWARE_MD_REPORT, &AC_SendFirmwareMdGet},
        {ST_GET_FIRMWARE_UPDATE_MD_CC_VER, EV_CANCEL_UPDATE_FIRMWARE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_CancelByUserAndFreeFlash},
        {ST_GET_FIRMWARE_UPDATE_MD_CC_VER, EV_TIMEOUT_STATE_FIRMWARE_UPDATE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_TimeoutFirmwareUpdate},
        {ST_GET_FIRMWARE_UPDATE_MD_CC_VER, EV_CANNOT_SEND_CMD, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_DeviceNotRespone},
        {ST_GET_FIRMWARE_UPDATE_MD_CC_VER, EV_GET_APPLICATION_VER_OK, ST_WAITING_GET_APPLICATION_VER, &AC_SendVersionGet},

        {ST_WAITING_FIRMWARE_MD_REPORT, EV_RX_FIRMWARE_MD_REPORT_OK, ST_WAITING_FIRMWARE_UPDATE_MD_REPORT, &AC_SendFirmwareUpdateMdRequestGet},
        {ST_WAITING_FIRMWARE_MD_REPORT, EV_CANCEL_UPDATE_FIRMWARE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_CancelByUserAndFreeFlash},
        {ST_WAITING_FIRMWARE_MD_REPORT, EV_TIMEOUT_STATE_FIRMWARE_UPDATE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_TimeoutFirmwareUpdate},
        {ST_WAITING_FIRMWARE_MD_REPORT, EV_CANNOT_SEND_CMD, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_DeviceNotRespone},
        /*version 3,4*/
        {ST_WAITING_FIRMWARE_MD_REPORT, EV_RX_FIRMWARE_MD_REPORT_VERSION_3_4_OK, ST_WAITING_FIRMWARE_UPDATE_MD_REPORT, &AC_SendFirmwareUpdateMdRequestGet_V3_4},

        {ST_WAITING_FIRMWARE_UPDATE_MD_REPORT, EV_RX_FIRMWARE_UPDATE_MD_REQUEST_REPORT_OK, ST_WAITING_FIRMWARE_UPDATE_GET, NULL},
        {ST_WAITING_FIRMWARE_UPDATE_MD_REPORT, EV_CANCEL_UPDATE_FIRMWARE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_CancelByUserAndFreeFlash},
        {ST_WAITING_FIRMWARE_UPDATE_MD_REPORT, EV_TIMEOUT_STATE_FIRMWARE_UPDATE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_TimeoutFirmwareUpdate},
        {ST_WAITING_FIRMWARE_UPDATE_MD_REPORT, EV_CANNOT_SEND_CMD, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_DeviceNotRespone},

        {ST_WAITING_FIRMWARE_UPDATE_GET, EV_RX_FIRMWARE_UPDATE_GET_OK, ST_WAITING_ACK, &AC_CheckNoReportInto1frame},
        {ST_WAITING_FIRMWARE_UPDATE_GET, EV_CANCEL_UPDATE_FIRMWARE, ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET, NULL},
        {ST_WAITING_FIRMWARE_UPDATE_GET, EV_TIMEOUT_STATE_FIRMWARE_UPDATE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_TimeoutFirmwareUpdate},
        {ST_WAITING_FIRMWARE_UPDATE_GET, EV_END_OF_FILE, ST_WAITING_FIRMWARE_UPDATE_STATUS_REPORT, &AC_SendLastUpdateFirmwareMdReport},
        {ST_WAITING_FIRMWARE_UPDATE_GET, EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_NOT_OK, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotifyFirmWareUpdateFail},
        {ST_WAITING_FIRMWARE_UPDATE_GET, EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_OK, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotifyFirmWareUpdateOK},
        {ST_WAITING_FIRMWARE_UPDATE_GET, EV_CANNOT_SEND_CMD, ST_WAITING_10S, &AC_CheckNoRetry},
        {ST_WAITING_10S,                 EV_TIMEOUT_STATE_FIRMWARE_UPDATE,  ST_WAITING_FIRMWARE_UPDATE_GET, &AC_SendUpdateFirmwareMdReport},
        {ST_WAITING_10S,                 EV_CANCEL_UPDATE_FIRMWARE, ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET, NULL},
        {ST_WAITING_FIRMWARE_UPDATE_GET, EV_RETRY_3_TIMES,                  ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_DeviceNotRespone_NonFree},
        {ST_WAITING_10S,                 EV_RETRY_3_TIMES,                  ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_DeviceNotRespone_NonFree},

        {ST_WAITING_ACK, EV_SEND_ALL_NO_REPORT, ST_WAITING_FIRMWARE_UPDATE_GET, &AC_SendUpdateFirmwareMdReport},
        {ST_WAITING_ACK, EV_CANCEL_UPDATE_FIRMWARE, ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET, NULL},
        {ST_WAITING_ACK, EV_TIMEOUT_STATE_FIRMWARE_UPDATE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_DeviceNotRespone},
        {ST_WAITING_ACK, EV_END_OF_FILE, ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET, &AC_SendLastUpdateFirmwareMdReport},
        {ST_WAITING_ACK, EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_NOT_OK, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotifyFirmWareUpdateFail},
        {ST_WAITING_ACK, EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_OK, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotifyFirmWareUpdateOK},
        {ST_WAITING_ACK, EV_CANNOT_SEND_CMD, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_DeviceNotRespone_NonFree},
        {ST_WAITING_ACK, EV_RECEIVE_ACK, ST_WAITING_35_MS_BETWEEN_2_TRAN, NULL},//waiting timeout 35ms

        {ST_WAITING_35_MS_BETWEEN_2_TRAN, EV_SEND_ALL_NO_REPORT, ST_WAITING_FIRMWARE_UPDATE_GET, &AC_SendUpdateFirmwareMdReport},
        {ST_WAITING_35_MS_BETWEEN_2_TRAN, EV_CANCEL_UPDATE_FIRMWARE, ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET, NULL},
        {ST_WAITING_35_MS_BETWEEN_2_TRAN, EV_TIMEOUT_STATE_FIRMWARE_UPDATE, ST_WAITING_ACK, &AC_CheckNoReportInto1frame},
        {ST_WAITING_35_MS_BETWEEN_2_TRAN, EV_END_OF_FILE, ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET, &AC_SendLastUpdateFirmwareMdReport},
        {ST_WAITING_35_MS_BETWEEN_2_TRAN, EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_NOT_OK, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotifyFirmWareUpdateFail},
        {ST_WAITING_35_MS_BETWEEN_2_TRAN, EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_OK, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotifyFirmWareUpdateOK},
        {ST_WAITING_35_MS_BETWEEN_2_TRAN, EV_CANNOT_SEND_CMD, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_DeviceNotRespone_NonFree},

        {ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET, EV_RX_FIRMWARE_UPDATE_GET_OK, ST_WAITING_FIRMWARE_UPDATE_STATUS_REPORT, &AC_PushNotify_CancelNonFreeFlash},
        {ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET, EV_CANCEL_UPDATE_FIRMWARE, ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET, NULL},
        {ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET, EV_TIMEOUT_STATE_FIRMWARE_UPDATE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_TimeoutFirmwareUpdate},
        {ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET, EV_END_OF_FILE, ST_WAITING_FIRMWARE_UPDATE_STATUS_REPORT, &AC_SendLastUpdateFirmwareMdReport},
        {ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET, EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_NOT_OK, ST_FW_UPDATE_FAIL, &AC_PushNotifyFirmWareUpdateFail},
        {ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET, EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_OK, ST_FIRMWARE_UPDATE_OK, &AC_PushNotifyFirmWareUpdateOK},
        {ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET, EV_CANNOT_SEND_CMD, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_DeviceNotRespone},

        {ST_WAITING_FIRMWARE_UPDATE_STATUS_REPORT, EV_RX_FIRMWARE_UPDATE_GET_OK, ST_WAITING_FIRMWARE_UPDATE_GET, &AC_SendUpdateFirmwareMdReport},
        {ST_WAITING_FIRMWARE_UPDATE_STATUS_REPORT, EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_OK, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotifyFirmWareUpdateOK},
        {ST_WAITING_FIRMWARE_UPDATE_STATUS_REPORT, EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_NOT_OK, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotifyFirmWareUpdateFail},
        {ST_WAITING_FIRMWARE_UPDATE_STATUS_REPORT, EV_CANCEL_UPDATE_FIRMWARE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_CancelFirmwareUpdate},
        {ST_WAITING_FIRMWARE_UPDATE_STATUS_REPORT, EV_TIMEOUT_STATE_FIRMWARE_UPDATE, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_TimeoutFirmwareUpdate},
        {ST_WAITING_FIRMWARE_UPDATE_STATUS_REPORT, EV_CANNOT_SEND_CMD, ST_UPDATE_FIRMWARE_MD_IDLE, &AC_PushNotify_DeviceNotRespone},

};

#define TRANS_COUNT (sizeof(trans) / sizeof(*trans))

/* States can have a default timeout. If the FSM remains in the same
 * state for the specified duration, the specified callback function
 * will be called.
 * Format: {STATE, timeout in milliseconds, callback_function}
 * Only states listed here have timeouts */
static state_timeout_t timeoutTable[] =
    {
        {ST_WAITING_CHECK_FILE_FINISH, 5000, ZCB_TimerOutState_UpdateFirmware},
        {ST_WAITING_LOAD_FILE_HEX_FINISH, 10000, ZCB_TimerOutState_UpdateFirmware},
        {ST_GET_FIRMWARE_UPDATE_MD_CC_VER, 10000, ZCB_TimerOutState_UpdateFirmware},
        {ST_WAITING_FIRMWARE_MD_REPORT, 10000, ZCB_TimerOutState_UpdateFirmware},
        {ST_WAITING_FIRMWARE_UPDATE_MD_REPORT, 10000, ZCB_TimerOutState_UpdateFirmware},
        {ST_WAITING_GET_APPLICATION_VER, 10000, ZCB_TimerOutState_UpdateFirmware},
        {ST_WAITING_FIRMWARE_UPDATE_GET, 30000, ZCB_TimerOutState_UpdateFirmware},

        {ST_WAITING_FIRMWARE_UPDATE_STATUS_REPORT, 20000, ZCB_TimerOutState_UpdateFirmware},
        {ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET, 10000, ZCB_TimerOutState_UpdateFirmware},
        {ST_WAITING_35_MS_BETWEEN_2_TRAN, 35, ZCB_TimerOutState_UpdateFirmware},
        {ST_WAITING_10S, 5000, ZCB_TimerOutState_UpdateFirmware},

};
#define TIMEOUT_TABLE_COUNT (sizeof(timeoutTable) / sizeof(*timeoutTable))

/*============================   Get string of State   ===============================
**
**  Cancels the FSM timer.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/
static char *GetStateTypeStr(uint8_t st)
{

    char *stStr;

    switch (st)
    {
    case ST_UPDATE_FIRMWARE_MD_IDLE:
        stStr = "ST_UPDATE_FIRMWARE_MD_IDLE";
        break;
    case ST_WAITING_CHECK_FILE_FINISH:
        stStr = "ST_WAITING_CHECK_FILE_FINISH";
        break;
    case ST_WAITING_LOAD_FILE_HEX_FINISH:
        stStr = "ST_WAITING_LOAD_FILE_HEX_FINISH";
        break;
    case ST_WAITING_FIRMWARE_MD_REPORT:
        stStr = "ST_WAITING_FIRMWARE_MD_REPORT";
        break;
    case ST_WAITING_FIRMWARE_UPDATE_MD_REPORT:
        stStr = "ST_WAITING_FIRMWARE_UPDATE_MD_REPORT";
        break;
    case ST_WAITING_FIRMWARE_UPDATE_GET:
        stStr = "ST_WAITING_FIRMWARE_UPDATE_GET";
        break;
    case ST_WAITING_FIRMWARE_UPDATE_STATUS_REPORT:
        stStr = "ST_WAITING_FIRMWARE_UPDATE_STATUS_REPORT";
        break;
    case ST_FIRMWARE_UPDATE_OK:
        stStr = "ST_FIRMWARE_UPDATE_OK"; //5
        break;
    case ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET:
        stStr = "ST_WAITING_CANCEL_FIRMWARE_UPDATE_GET";
        break;
    case ST_WAITING_35_MS_BETWEEN_2_TRAN:
        stStr = "ST_WAITING_35_MS_BETWEEN_2_TRAN";
        break;
    case ST_WAITING_ACK:
        stStr = "ST_WAITING_ACK";
        break;
    case ST_GET_FIRMWARE_UPDATE_MD_CC_VER:
        stStr = "ST_GET_FIRMWARE_UPDATE_MD_CC_VER";
        break;
    case ST_WAITING_GET_APPLICATION_VER:
        stStr = "ST_WAITING_GET_APPLICATION_VER";
        break;
    case ST_WAITING_10S:
        stStr = "ST_WAITING_10S";
        break;
    default:
        stStr = " ";
        break;
    }

    return stStr;
}

/*============================   Get string of Event   ===============================
**
**  Cancels the FSM timer.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/
static char *GetEventTypeStr(uint8_t ev)
{

    char *evStr;

    switch (ev)
    {
    case EV_UPDATE_FIRMWARE_MD_START: //0
        evStr = "EV_UPDATE_FIRMWARE_MD_START";
        break;
    case EV_CHECK_FILE_OK:
        evStr = "EV_CHECK_FILE_OK";
        break;
    case EV_CHECK_FILE_NOT_OK:
        evStr = "EV_CHECK_FILE_NOT_OK";
        break;
    case EV_CALCULATE_CRC16_FINISH:
        evStr = "EV_CALCULATE_CRC16_FINISH";
        break;
    case EV_RX_FIRMWARE_MD_REPORT_OK:
        evStr = "EV_RX_FIRMWARE_MD_REPORT_OK";
        break;
    case EV_RX_FIRMWARE_UPDATE_MD_REQUEST_REPORT_OK:
        evStr = "EV_RX_FIRMWARE_UPDATE_MD_REQUEST_REPORT_OK";
        break;
    case EV_RX_FIRMWARE_UPDATE_GET_OK:
        evStr = "EV_RX_FIRMWARE_UPDATE_GET_OK";
        break;
    case EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_OK:
        evStr = "EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_OK";
        break;
    case EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_NOT_OK:
        evStr = "EV_RX_FIRMWARE_UPDATE_STATUS_REPORT_NOT_OK";
        break;
    case EV_CANCEL_UPDATE_FIRMWARE:
        evStr = "EV_CANCEL_UPDATE_FIRMWARE";
        break;
    case EV_TIMEOUT_STATE_FIRMWARE_UPDATE:
        evStr = "EV_TIMEOUT_STATE_FIRMWARE_UPDATE";
        break;
    case EV_FIRMWARE_UPDATE_FINISH:
        evStr = "EV_FIRMWARE_UPDATE_FINISH";
        break;
    case EV_END_OF_FILE:
        evStr = "EV_END_OF_FILE";
        break;
    case EV_RX_FIRMWARE_UPDATE_MD_REQUEST_REPORT_NOT_OK:
        evStr = "EV_RX_FIRMWARE_UPDATE_MD_REQUEST_REPORT_NOT_OK";
        break;
    case EV_CANNOT_SEND_CMD:
        evStr = "EV_CANNOT_SEND_CMD";
        break;
    case EV_SEND_ALL_NO_REPORT:
        evStr = "EV_SEND_ALL_NO_REPORT";
        break;
    case EV_RECEIVE_ACK:
        evStr = "EV_RECEIVE_ACK";
        break;
    case EV_RX_FIRMWARE_MD_REPORT_VERSION_3_4_OK:
        evStr = "EV_RX_FIRMWARE_MD_REPORT_VERSION_3_4_OK";
        break;
    case EV_VERSION_CC_REPORT:
        evStr = "EV_VERSION_CC_REPORT";
        break;
    case EV_GET_APPLICATION_VER_OK:
        evStr = "EV_GET_APPLICATION_VER_OK";
        break;
    case EV_RETRY_3_TIMES:
        evStr = "EV_RETRY_3_TIMES";
        break;
    default:
        evStr = " ";
        break;
    }
    return evStr;
}

/*============================   StopTimer   ===============================
**
**  Cancels the FSM timer.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/
static void StopTimer()
{
    if (bFsmTimerHandle_UpdateFirmware != NO_TIMER)
    {
        timerCancel(&bFsmTimerHandle_UpdateFirmware);
        bFsmTimerHandle_UpdateFirmware = NO_TIMER;
    }
}

/*===========================   UpdateFirmware_ZCB_FsmTimer   ==============================
**
**  Timer callback function for the default FSM timer.
**  If a timeout and a callback function is specified in the
**  timeoutTable, ths callback function will be called automatically
**  by this timer function.
**
**  The callback function will only be called if no state change has occurred
**  within the specified timeout since entering the current state.
**
**  Side effects: None
**
**  If Timeout, this function is executed
**-------------------------------------------------------------------------*/
void UpdateFirmware_ZCB_FsmTimer(void *data)
{
    mainlog(logDebug, "FsmTimer-bFsmTimerCountdown_UpdateFirmware: %u", bFsmTimerCountdown_UpdateFirmware);
    if (bFsmTimerCountdown_UpdateFirmware > 0)
    {
        bFsmTimerCountdown_UpdateFirmware--;
        return;
    }
    /* The timer repeat count should already have stopped the timer,
    * but better safe than sorry. */
    StopTimer();
    if (cbFsmTimer_AutoAdd != NULL) //pointer function
    {
        cbFsmTimer_AutoAdd(data); //function
    }
}

/*============================   StartTimer   ===============================
**
**  Start the FSM timer. Note the higher resolution if timeout is
**  shorter than or equal to 2.55 seconds.
**
**  Side effects: Cancels running FSM timer if any.
**
**-------------------------------------------------------------------------*/
static void StartTimer(
    uint32_t timeout,       /* IN Unit: 10 ms ticks. If over 255, resolution is 1 second */
    void (*cbFunc)(void *)) /* IN timeout callback function */
{
    StopTimer();
    int ret = 0;
    if (timeout > 255)
    {
        /* Timeout larger than single timeout. */
        /* Convert timeout from 10 ms ticks to seconds*/
        bFsmTimerCountdown_UpdateFirmware = timeout / 1000;
        cbFsmTimer_AutoAdd = cbFunc;
        ret = timerStart(&bFsmTimerHandle_UpdateFirmware, UpdateFirmware_ZCB_FsmTimer, NULL, 1000, TIMER_FOREVER);
    }
    else
    {
        /* timeout is in range 10..2550 ms */
        ret = timerStart(&bFsmTimerHandle_UpdateFirmware, cbFunc, NULL, timeout * 10, TIMER_ONETIME);
    }

    if (ret)
    {
        mainlog(logDebug, "Failed to get timer for FsmTimer +++++");
    }
}

/*=========================   auto_add_paraStartStateTimer   ============================
**
**  Find and start the state timer for current state, if one has been
**  defined in timeoutTable.
**
**  Side effects: Cancels the currently running state timer.
**
**-------------------------------------------------------------------------*/
static void StartStateTimer()
{
    uint8_t i;
    for (i = 0; i < TIMEOUT_TABLE_COUNT; i++)
    {
        if (state == timeoutTable[i].st)
        {
            StartTimer(timeoutTable[i].timeout, timeoutTable[i].tfn);
            mainlog(logDebug, "StartStateTimer: index %d", i);
            if (timeoutTable[i].tfn == NULL)
            {
                mainlog(logDebug, "StartStateTimer:NULL function");
            }
        }
    }
}

/*============================   InterviewDevice_ZCB_PostEvent   ===============================
**
**  Post an event to the finite state machine. Update the state,
**  run action function associated with the transition and start
**  the state timer for the next state.
**
**  Side effects: None
**
**-------------------------------------------------------------------------*/
void PostEventUpdateFirmwareMd(UPDATE_FIRMWARE_MD_FSM_EV_T event)
{
    UPDATE_FIRMWARE_MD_FSM_STATES_T old_state;
    uint8_t i;
    for (i = 0; i < TRANS_COUNT; i++)
    {
        if ((state == trans[i].st)) //have state none support
        {
            if ((event == trans[i].ev))
            {
                old_state = trans[i].st;

                state = trans[i].next_st;

                mainlog(logDebug, "----UpdateFirmware_PostEvent: %s [%u]-----", GetEventTypeStr(event), event);
                mainlog(logDebug, "----%s [%u] ---> %s [%u]---------------", GetStateTypeStr(old_state), old_state, GetStateTypeStr(state), state);
                if (old_state != state)
                {
                    StopTimer();
                    StartStateTimer();
                }
                if ((trans[i].fn) != NULL)
                {
                    (trans[i].fn());
                }
                break;
            }
        }
    }
}

void CheckFileFirmwareUpdate(char *path_of_file)
{
    /*push notify */
    NOTIFY_TX_BUFFER_T pTxNotify;
    pTxNotify.FirmwareUpdateNotify.deviceId = nm.tmp_node;
    pTxNotify.FirmwareUpdateNotify.status = FIRMWARE_UPDATE_START;
    PushNotificationToHandler(FIRMWARE_UPDATE_NOTIFY, (uint8_t *)&pTxNotify.FirmwareUpdateNotify, sizeof(FIRMWARE_UPDATE_NOTIFY_T));

    flash_mem_update_file = (uint8_t *)malloc(FLASH_MEM_LENGTH);
    scheme_node = highest_scheme(GetCacheEntryFlag(nm.tmp_node));
    mainlog(logDebug, "scheme_node %02X", scheme_node);

    percent = 0;

    if (-1 == intelHex_loadfile(path_of_file, flash_mem_update_file, &length_file_update))
    {
        PostEventUpdateFirmwareMd(EV_CHECK_FILE_NOT_OK);
    }
    else
    {
        PostEventUpdateFirmwareMd(EV_CHECK_FILE_OK);
    }
}