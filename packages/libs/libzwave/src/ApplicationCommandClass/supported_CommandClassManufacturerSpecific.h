/***************************************************************************
*
* Copyright (c) 2001-2013
* Sigma Designs, Inc.
* All Rights Reserved
*
*---------------------------------------------------------------------------
*
* Description:  Manufacturer specific Command Class header file.
*
* Author: Samer
*
* Last Changed By: $Author: tro $
* Revision: $Revision: 0.00 $
* Last Changed: $Date: 2013/06/28 08:56:16 $
*
****************************************************************************/
#ifndef _SUPPORTED_COMMAND_CLASS_MANUFACTURER_SPECIFIC_
#define _SUPPORTED_COMMAND_CLASS_MANUFACTURER_SPECIFIC_

/****************************************************************************/
/*                              INCLUDE FILES                               */
/****************************************************************************/
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"
#include "config.h"
#include "ZW_TransportEndpoint.h"


#define MAN_DEVICE_ID_SIZE  8


/****************************************************************************/
/*                     EXPORTED TYPES and DEFINITIONS                       */
/****************************************************************************/

typedef struct _T_MSINFO_{
    uint8_t  manufacturerId1;
    uint8_t  manufacturerId2;
    uint8_t  productTypeId1;
    uint8_t  productTypeId2;
    uint8_t  productId1;
    uint8_t  productId2;
} T_MSINFO;


typedef enum _DEVICE_ID_TYPE_ { DEVICE_ID_TYPE_OEM = 0, DEVICE_ID_TYPE_SERIAL_NBR, DEVICE_ID_TYPE_PSEUDO_RANDOM} DEVICE_ID_TYPE;
typedef enum _DEVICE_ID_FORMAT_ { DEVICE_ID_FORMAT_UTF_8 = 0, DEVICE_ID_FORMAT_BIN} DEVICE_ID_FORMAT;

typedef struct _DEV_ID_DATA
{
    uint8_t DevIdDataFormat: 3; /*Type DEVICE_ID_FORMAT*/
    uint8_t DevIdDataLen: 5;
    uint8_t* pDevIdData;
} DEV_ID_DATA;
/****************************************************************************/
/*                              EXPORTED DATA                               */
/****************************************************************************/


/****************************************************************************/
/*                           EXPORTED FUNCTIONS                             */
/****************************************************************************/

 
/** 
 * @brief handleCommandClassManufacturerSpecific
 * @param option IN Frame header info.
 * @param sourceNode IN Command sender Node ID.
 * @param pCmd IN Payload from the received frame, the union should be used to access 
 * the fields.
 * @param cmdLength IN Number of command bytes including the command.
 * @return none.
 */
received_frame_status_t 
handleCommandClassManufacturerSpecific(ts_param_t *p, /* IN receive options of type RECEIVE_OPTIONS_TYPE_EX  */                
                                        ZW_APPLICATION_TX_BUFFER *pCmd,  
                                        uint8_t   cmdLength                
);

/** 
 * @brief ApplManufacturerSpecificInfoGet
 * Read the manufacturer specific info from the application the manufacturer 
 * specific info are the manufacturerId1/2, productTypeId1/2 and productId1/2.
 *
 * @param msInfo: OUT pointer of type t_MSInfo that should hold the manufacturer 
 * specific information
 * @return None.
 */
void ApplManufacturerSpecificInfoGet(T_MSINFO *t_msInfo); 


/** 
 * @brief ApplDeviceSpecificInfoGet
 * Read the Device specifict ID Data fields.
 * @param deviceIdType values for the Device ID Type of enum type DEVICE_ID_TYPE
 * @param pDevIdData OUT pointer to the Device ID Data fields.
 * @param pDevIdDataLen OUT pointer returning len of the Device ID Data fields.
 * @return none.
 */
bool ApplDeviceSpecificInfoGet(uint8_t *deviceIdType, 
                                DEVICE_ID_FORMAT* pDevIdDataFormat,
                                uint8_t* pDevIdDataLen,
                                uint8_t* pDevIdData);


/** 
 * @brief CommandClassManufacturerVersionGet
 * Get version
 * @return version..
 */
extern uint8_t CommandClassManufacturerVersionGet(void);
#endif 

