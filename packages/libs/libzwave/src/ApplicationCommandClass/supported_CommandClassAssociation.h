#ifndef _SUPPORTED_COMMAND_CLASS_ASSOCIATION_
#define _SUPPORTED_COMMAND_CLASS_ASSOCIATION_

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"
#include "config.h"
#include "agi.h"
#include "ZW_TransportEndpoint.h"


#define ASSOCIATION_SIZE            (MAX_ASSOCIATION_GROUPS * MAX_ASSOCIATION_IN_GROUP)

typedef struct _ASSOCIATION_GROUP_
{
    uint8_t     nodeID[MAX_ASSOCIATION_IN_GROUP];
}ASSOCIATION_GROUP;


void        AssociationClearAll(void);
void        AssociationInit(bool forceClearMem);
void        AssociationStoreAll(void);
bool        handleCheckgroupIden(uint8_t* pGroupIden);
uint8_t     handleGetMaxAssociationGroups(void);
uint8_t     handleGetMaxNodesInGroup(void);

void        AssociationRemove(
                                uint8_t groupIden,          /*IN groupIden number to remove nodes from*/
                                uint8_t *nodeIdP,           /*IN pointer to array of nodes to remove*/
                                uint8_t noOfNodes           /*IN number of nodes in list*/
);

void        AssociationAdd(
                                uint8_t groupIden,     /*IN group number to add nodes to*/
                                uint8_t *nodeIdP, /*IN pointer to list of nodes*/
                                uint8_t noOfNodes  /*IN number of nodes in List*/
);










/*============================ handleAssociationGetnodeList =================
** Function description
** Deliver group number node list.
** Return status on job: TRUE/FALSE
** Side effects:
**
**-------------------------------------------------------------------------*/


bool
handleAssociationGetnodeList(
                        uint8_t     groupIden,              /* IN  Group identifier*/
                        uint8_t** ppNodeId,             /* OUT Doublepointer to list of nodes in group*/
                        uint8_t* pNbrOfNodes);          /* OUT Number of nodes in group*/

received_frame_status_t  
handleCommandClassAssociation(ts_param_t *p, 
                                ZW_APPLICATION_TX_BUFFER *pCmd, 
                                uint8_t   cmdLength);



/*============================ CommandClassAssociactionVersionGet ===========
** Function description
** Return version
**
** Side effects: 
**
**-------------------------------------------------------------------------*/
uint8_t 
CommandClassAssociactionVersionGet(void);



/*=======================   AssociationGetLifeLineNodeID   ========================
**    Function description
**      Reads the nodeID asscoiated to the lifeLine group
**
**    Side effects:
**
**------------------------------------------------------------------------------*/
uint8_t /*RET the nodeID of the lifeline node, 0xFF if not associated*/
AssociationGetLifeLineNodeID(void);

#endif

