#ifndef _ZW_POWERLEVEL_TEST_H_
#define _ZW_POWERLEVEL_TEST_H_

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"
#include "timer.h"
#include "ZW_TransportEndpoint.h"



#define POWERLEVEL_FRAMECOUNT 10
#define ZW_TEST_NOT_A_NODEID                            0x00


typedef enum _ePowLevStatus
{
    POWERLEV_TEST_TRANSMIT_COMPLETE_OK              = 0x00,
    POWERLEV_TEST_TRANSMIT_COMPLETE_NO_ACK          = 0x01,
    POWERLEV_TEST_TRANSMIT_COMPLETE_FAIL            = 0x02,

    POWERLEV_TEST_NODE_REPORT_ZW_TEST_FAILED        = 0x10,
    POWERLEV_TEST_NODE_REPORT_ZW_TEST_SUCCES        = 0x11,
    POWERLEV_TEST_NODE_REPORT_ZW_TEST_INPROGRESS    = 0x12,
    POWERLEV_TEST_NODE_REPORT_ZW_TEST_TIMEOUT       = 0x13,
}ePowLevStatus;

typedef struct _POWLEV_STATUS
{
    ePowLevStatus   Status;
    uint8_t         sourceNode;
    uint8_t         destNode;
    uint8_t         PowerLevel;
    int             ackCountSuccess;
    int             NbrTestFrames;
}POWLEV_STATUS;

received_frame_status_t 
handleCommandClassPowerLevel(ts_param_t *p,
                                ZW_APPLICATION_TX_BUFFER *pCmd, 
                                uint8_t cmdLength);

uint8_t CommandClassPowerLevelVersionGet(void);
#endif

