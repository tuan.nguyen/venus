#include "supported_CommandClassAssociation.h"
#include "utils.h"
#include "serialAPI.h"
#include "nvm.h"
#include "cmd_class_misc.h"




#define MAGIC_VALUE                 0x42
#define DEFAULT_GROUP               0x01


uint8_t*    pNodeList = NULL;
uint8_t     nodeListLen = 0;


ASSOCIATION_GROUP groups[MAX_ASSOCIATION_GROUPS];

uint8_t indx;


void AssociationClearAll(void)
{
    uint8_t magic_value=MAGIC_VALUE;
    memset((uint8_t *)&groups[0],0x00,ASSOCIATION_SIZE);

    nvm_config_set(ass_grp,&groups[0]);
    nvm_config_set(ass_magic,&magic_value);

}


/*============================   AssociationInit   ==========================
**    Function description
**      Reads the groups stored in the Nonvolatile memory
**    Side effects:
**
**--------------------------------------------------------------------------*/
void AssociationInit(bool forceClearMem)
{
    uint8_t magic_value;

    nvm_config_get(ass_magic,&magic_value);
    if ((magic_value != MAGIC_VALUE) ||(true == forceClearMem))
    {
        /* Clear it */
        AssociationClearAll();
    }
    nvm_config_get(ass_grp,&groups[0]);
}

/*======================= ReorderGroupAfterRemove  =======================
** Function description
** reorder the association group node ID list so there are no gaps
**
** Side effects:
**
**-------------------------------------------------------------------------*/



static void ReorderGroupAfterRemove(
                                        uint8_t groupID,            /*the group ID to reorder*/
                                        uint8_t emptyIndx)          /*the index of the empty field*/
{
    uint8_t move;
    for(move = emptyIndx+1; move < MAX_ASSOCIATION_IN_GROUP; move++)
    {
        groups[groupID].nodeID[move-1] = groups[groupID].nodeID[move];
        groups[groupID].nodeID[move] = 0;
    }
}



void AssociationStoreAll(void)
{
    nvm_config_set(ass_grp,&groups[0]);
}

void  associationResponse_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t)
{
    mainlog(logDebug, "associationResponse_Compl\r\n");
    if (TRANSMIT_COMPLETE_OK == bTxStatus)
    {
        mainlog(logDebug, "TRANSMIT_COMPLETE_OK\r\n"); 
    }
    else
    {
        mainlog(logDebug, "TRANSMIT_COMPLETE_NOT_OK\r\n"); 
    }
}

bool handleCheckgroupIden(uint8_t* pGroupIden)
{
    if((GetApplAssoGroupsSize() <= (*pGroupIden - 1)) || (0 == *pGroupIden))
    {
        return false; /*not legal number*/
    }
  return true;
}

/*============================ handleGetMaxAssociationGroups ===============
** Function description
** Return max association groups..
**
** Side effects:
**
**-------------------------------------------------------------------------*/
uint8_t handleGetMaxAssociationGroups(void)
{
    return GetApplAssoGroupsSize();
}

bool handleAssociationGetnodeList(
                                    uint8_t groupIden,              /* IN  Group Number*/
                                    uint8_t** ppNodeId,             /* OUT Doublepointer to list of nodes in group*/
                                    uint8_t* pNbrOfNodes)           /* OUT Number of nodes in group*/
{
    uint8_t indx;
    /*Check group number*/
    if(false == handleCheckgroupIden(&groupIden))
    {
        return false; /*not legal number*/
    }
    *ppNodeId = &groups[groupIden - 1].nodeID[0];
    *pNbrOfNodes = MAX_ASSOCIATION_IN_GROUP; /*default set to max*/
    for (indx = 0; indx < MAX_ASSOCIATION_IN_GROUP; indx++)
    {
        if(0 == groups[groupIden - 1].nodeID[indx])
        {
            *pNbrOfNodes = indx; /*number of nodes in list*/
            break;  /* break out of loop*/
        }
    }
    return true;
}
/*============================ handleGetMaxNodesInGroup ====================
** Function description
** Return max nodes supported in a group.
**
** Side effects:
**
**-------------------------------------------------------------------------*/
uint8_t handleGetMaxNodesInGroup(void)
{
    return MAX_ASSOCIATION_IN_GROUP;
}
/*============================   AssociationRemove   ======================
**    Function description
**      Removes association members from specified group
**      If noOfNodes = 0 group is removed
**
**    In version 2 grouping identifier in conjunction with sequence of NodeID’s
**    are interpreted as follows:
**                                               | Grouping identifier | Number of node ID’s in list
**    ------------------------------------------------------------------------------------------------
**    Clear all node ID’s in grouping X          | 1 ≤ X ≤ N           | 0
**    Clear specified node ID’s in all groupings | 0                   | >0
**    Clear all node ID’s in all groupings       | 0                   | 0
**
**    Side effects:
**
**--------------------------------------------------------------------------*/
void    AssociationRemove(
                            uint8_t groupIden,          /*IN groupIden number to remove nodes from*/
                            uint8_t *nodeIdP,           /*IN pointer to array of nodes to remove*/
                            uint8_t noOfNodes           /*IN number of nodes in list*/
)
{
    uint8_t i,j,indx;
    //uint8_t tempgroupIden;
 

    /*Use a temp groupIden because it most not be changed*/
    //tempgroupIden = groupIden;

    if ( (0 < groupIden) && (GetApplAssoGroupsSize() >= groupIden))
    {
        if(noOfNodes)
        {
            /*Remove noOfNodes in list "groupIden" */
            for (i = 0; i < noOfNodes; i++)
            {
                for (indx = 0; indx < MAX_ASSOCIATION_IN_GROUP; indx++)
                {
                    if (groups[groupIden-1].nodeID[indx] == *(nodeIdP+i))
                    {
                        //uint8_t move;
                        /*Set it to 0, if it is the last one*/
                        groups[groupIden-1].nodeID[indx] = 0;
                        /*Move data into the empty area*/
                        ReorderGroupAfterRemove(groupIden-1, indx);
                        break;  /* Found */
                    }
                }
            }
        }
        else
        {
            /*Remove all nodes in list "groupIden" */
            for (indx = 0; indx < MAX_ASSOCIATION_IN_GROUP; indx++)
            {
                groups[groupIden-1].nodeID[indx] = 0;
            }
        }
        AssociationStoreAll();
    }
    else if(0 == groupIden)
    {
        if(noOfNodes)
        {
            /*Clear specified node ID's in all groupings*/
            for(j = 0; j < GetApplAssoGroupsSize(); j++)
            {
                for (i = 0; i < noOfNodes; i++)
                {
                    for (indx = 0; indx < MAX_ASSOCIATION_IN_GROUP; indx++)
                    {
                        if (groups[j].nodeID[indx] == *(nodeIdP+i))
                        {
                            groups[j].nodeID[indx] = 0;
                            ReorderGroupAfterRemove(j, indx);
                            break;  /* Found */
                        }   
                    }
                }
            }
            AssociationStoreAll();
        }
        else
        {
            /*Clear all node ID's in all groupings*/
            AssociationInit(true);
        }
    }
}

/*============================   AssociationAdd   ======================
**    Function description
**      Adds the nodes in nodeIdP to the group
**    Side effects:
**
**--------------------------------------------------------------------------*/
void    AssociationAdd(
                uint8_t groupIden,     /*IN group number to add nodes to*/
                uint8_t *nodeIdP, /*IN pointer to list of nodes*/
                uint8_t noOfNodes  /*IN number of nodes in List*/
)
{
    uint8_t i;
    uint8_t tempID;

  
    /*check group number*/
    if(false == handleCheckgroupIden(&groupIden))
    {
        return;
    }


    for (i = 0; i < noOfNodes; i++)
    {
        uint8_t vacant = 0xff;
        tempID = *(nodeIdP + i);
        for (indx = 0; indx < MAX_ASSOCIATION_IN_GROUP; indx++)
        {
            if (groups[groupIden - 1].nodeID[indx])
            {
                if (groups[groupIden - 1].nodeID[indx] == tempID)
                {
                    vacant = 0xFF;  /*prevent duplicated nodeID since the list can have gaps*/
                    break;  /* Allready in */
                }
            }
            else
            {
                if (vacant == 0xff)
                {
                    vacant = indx;
                }
            }
        }
        if (vacant != 0xff)
        {
            groups[groupIden - 1].nodeID[vacant] = tempID;
            AssociationStoreAll();
        }
    }
        //CmdReceivedEvent(COMMAND_CLASS_ASSOCIATION, ASSOCIATION_SET_V2);
}
received_frame_status_t
handleCommandClassAssociation(ts_param_t *p, 
                                    ZW_APPLICATION_TX_BUFFER *pCmd, 
                                    uint8_t   cmdLength)
{
    if (!SupportsFrameAtSecurityLevel(COMMAND_CLASS_ASSOCIATION, p->scheme))
    {
        mainlog(logDebug, "handle Association CC p->scheme %d SupportsFrame ret 0",p->scheme );
        return RECEIVED_FRAME_STATUS_FAIL;
    }

    switch (pCmd->ZW_AssociationGetFrame.cmd)
    {
        case ASSOCIATION_GET_V2:
        {
            if(true == Check_not_legal_response_job(p))
            { 
                return RECEIVED_FRAME_STATUS_FAIL;
            }

            ts_param_t pTxOptionsEx;
            RxToTxOptions(p, &pTxOptionsEx);

            pTxBuf.ZW_AssociationReport1byteFrame.cmdClass           = COMMAND_CLASS_ASSOCIATION;
            pTxBuf.ZW_AssociationReport1byteFrame.cmd                = ASSOCIATION_REPORT_V2;

            if(true == handleAssociationGetnodeList(pCmd->ZW_AssociationGetFrame.groupingIdentifier, &pNodeList,&nodeListLen))
            {
                uint8_t indx;
                mainlog(logDebug,"1groupingIdentifier -> %02X", pCmd->ZW_AssociationGetFrame.groupingIdentifier);
                pTxBuf.ZW_AssociationReport1byteFrame.groupingIdentifier = pCmd->ZW_AssociationGetFrame.groupingIdentifier;
                pTxBuf.ZW_AssociationReport1byteFrame.maxNodesSupported  = handleGetMaxNodesInGroup();
                pTxBuf.ZW_AssociationReport1byteFrame.reportsToFollow = 0; //Nodes fit in one report

                for (indx = 0; indx < nodeListLen; indx++)
                {
                    *(&pTxBuf.ZW_AssociationReport1byteFrame.nodeid1 + indx) = *(pNodeList + indx);
                }
            
            }
            else
            {
                uint8_t indx;
                handleAssociationGetnodeList(DEFAULT_GROUP , &pNodeList,&nodeListLen);
                mainlog(logDebug,"2groupingIdentifier -> %02X", pCmd->ZW_AssociationGetFrame.groupingIdentifier);
                pTxBuf.ZW_AssociationReport1byteFrame.groupingIdentifier = DEFAULT_GROUP;
                pTxBuf.ZW_AssociationReport1byteFrame.maxNodesSupported  = handleGetMaxNodesInGroup();
                pTxBuf.ZW_AssociationReport1byteFrame.reportsToFollow = 0; //Nodes fit in one report

                for (indx = 0; indx < nodeListLen; indx++)
                {
                    *(&pTxBuf.ZW_AssociationReport1byteFrame.nodeid1 + indx) = *(pNodeList + indx);
                }
            }

            Transport_SendDataApplEP((uint8_t*)&pTxBuf,
                                    sizeof(ZW_ASSOCIATION_REPORT_1BYTE_FRAME) - 1 + nodeListLen,
                                    &pTxOptionsEx,
                                    associationResponse_Compl, 0);

            return RECEIVED_FRAME_STATUS_SUCCESS;
        }
        break;

        case ASSOCIATION_SET_V2:
            AssociationAdd(pCmd->ZW_AssociationSet1byteV2Frame.groupingIdentifier,
                            &(pCmd->ZW_AssociationSet1byteV2Frame.nodeId1),cmdLength - OFFSET_PARAM_2);
            return RECEIVED_FRAME_STATUS_SUCCESS;

        case ASSOCIATION_REMOVE_V2:
            if (3 > cmdLength)
            {
                return RECEIVED_FRAME_STATUS_FAIL;
            }
            AssociationRemove(pCmd->ZW_AssociationRemove1byteV2Frame.groupingIdentifier,
                                    &(pCmd->ZW_AssociationRemove1byteV2Frame.nodeId1),cmdLength - OFFSET_PARAM_2);
            return RECEIVED_FRAME_STATUS_SUCCESS;

        case ASSOCIATION_GROUPINGS_GET_V2:
        {
            if(true == Check_not_legal_response_job(p))
            { 
                /*Get/Report do not support endpoint bit-addressing */
                return RECEIVED_FRAME_STATUS_FAIL;
            }

            ts_param_t pTxOptionsEx;
            RxToTxOptions(p, &pTxOptionsEx);
            pTxBuf.ZW_AssociationGroupingsReportV2Frame.cmdClass = COMMAND_CLASS_ASSOCIATION;
            pTxBuf.ZW_AssociationGroupingsReportV2Frame.cmd = ASSOCIATION_GROUPINGS_REPORT_V2;
            pTxBuf.ZW_AssociationGroupingsReportV2Frame.supportedGroupings = handleGetMaxAssociationGroups();
            Transport_SendDataApplEP((uint8_t*)&pTxBuf, 
                                    sizeof(pTxBuf.ZW_AssociationGroupingsReportV2Frame), 
                                    &pTxOptionsEx, 
                                    associationResponse_Compl, 0);
            return RECEIVED_FRAME_STATUS_SUCCESS;
         
        }
        break;
        case ASSOCIATION_SPECIFIC_GROUP_GET_V2:
        {
            if(true == Check_not_legal_response_job(p))
            { 
                /*Get/Report do not support endpoint bit-addressing */
                return RECEIVED_FRAME_STATUS_FAIL;
            }
            
            ts_param_t pTxOptionsEx;
            RxToTxOptions(p, &pTxOptionsEx);
            pTxBuf.ZW_AssociationSpecificGroupReportV2Frame.cmdClass = COMMAND_CLASS_ASSOCIATION;
            pTxBuf.ZW_AssociationSpecificGroupReportV2Frame.cmd = ASSOCIATION_SPECIFIC_GROUP_REPORT_V2;
            pTxBuf.ZW_AssociationSpecificGroupReportV2Frame.group = ApplicationGetLastActiveGroupId();
            Transport_SendDataApplEP((uint8_t*)&pTxBuf, 
                                    sizeof(pTxBuf.ZW_AssociationSpecificGroupReportV2Frame), 
                                    &pTxOptionsEx, 
                                    associationResponse_Compl, 0);

            return RECEIVED_FRAME_STATUS_SUCCESS;   
         
        }
        break;

    }
    return RECEIVED_FRAME_STATUS_NO_SUPPORT;
}


/*=======================   AssociationGetLifeLineNodeID   ========================
**    Function description
**      Reads the nodeID asscoiated to the lifeLine group
**
**    Side effects:
**
**------------------------------------------------------------------------------*/
uint8_t                              /*RET the nodeID of the lifeline node, 0xFF if not associated*/
AssociationGetLifeLineNodeID(void)
{
    /*get the nodeID associated to the lifeline group*/
    return (groups[0].nodeID[0])?groups[0].nodeID[0]:0xFF;
}



/*============================ CommandClassAssociactionVersionGet ===========
** Function description
** Return version
**
** Side effects: 
**
**-------------------------------------------------------------------------*/
uint8_t CommandClassAssociactionVersionGet(void)
{
    return ASSOCIATION_VERSION_V2;  
}
