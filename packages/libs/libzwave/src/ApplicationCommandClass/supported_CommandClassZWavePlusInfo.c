#include "supported_CommandClassZWavePlusInfo.h"
#include "serialAPI.h"
#include "utils.h"
#include "cmd_class_misc.h"

void  zwavePlusInfoResponse_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t)
{
    mainlog(logDebug, "zwavePlusInfoResponse_Compl");
    if (TRANSMIT_COMPLETE_OK == bTxStatus)
    {
        mainlog(logDebug, "TRANSMIT_COMPLETE_OK"); 
    }
    else
    {
        mainlog(logDebug, "TRANSMIT_COMPLETE_NOT_OK"); 
    }
}


received_frame_status_t  
handleCommandClassZWavePlusInfo(ts_param_t *p, /* IN receive options of type RECEIVE_OPTIONS_TYPE_EX  */
                                ZW_APPLICATION_TX_BUFFER *pCmd, /* IN Payload from the received frame, the union */
                                uint8_t   cmdLength                /* IN Number of command bytes including the command */
)
{
    if (!SupportsFrameAtSecurityLevel(COMMAND_CLASS_ZWAVEPLUS_INFO, p->scheme))
    {
        mainlog(logDebug, "handle ZWave+ CC p->scheme %d SupportsFrame ret 0",p->scheme );
        return RECEIVED_FRAME_STATUS_FAIL;
    }

    if(true == Check_not_legal_response_job(p))
    {
        /*Do not support endpoint bit-addressing */
        return RECEIVED_FRAME_STATUS_FAIL;
    }

    switch (pCmd->ZW_VersionGetFrame.cmd)
    {
        case ZWAVEPLUS_INFO_GET:
        {
            ts_param_t pTxOptionsEx;
            RxToTxOptions(p, &pTxOptionsEx);
            pTxBuf.ZW_ZwaveplusInfoReportV2Frame.cmdClass = COMMAND_CLASS_ZWAVEPLUS_INFO;
            pTxBuf.ZW_ZwaveplusInfoReportV2Frame.cmd = ZWAVEPLUS_INFO_REPORT;
            pTxBuf.ZW_ZwaveplusInfoReportV2Frame.zWaveVersion = APP_ZWAVE_PLUS_VERSION;
            pTxBuf.ZW_ZwaveplusInfoReportV2Frame.roleType = ROLE_TYPE_CONTROLLER_CENTRAL_STATIC;
            pTxBuf.ZW_ZwaveplusInfoReportV2Frame.nodeType = ZWAVEPLUS_INFO_REPORT_NODE_TYPE_ZWAVEPLUS_NODE;
            pTxBuf.ZW_ZwaveplusInfoReportV2Frame.installerIconType1 = (ICON_TYPE_GENERIC_CENTRAL_CONTROLLER >> 8) & 0xFF;
            pTxBuf.ZW_ZwaveplusInfoReportV2Frame.installerIconType2 = (ICON_TYPE_GENERIC_CENTRAL_CONTROLLER >> 0) & 0xFF;
            pTxBuf.ZW_ZwaveplusInfoReportV2Frame.userIconType1 = (ICON_TYPE_GENERIC_CENTRAL_CONTROLLER >> 8) & 0xFF;
            pTxBuf.ZW_ZwaveplusInfoReportV2Frame.userIconType2 = (ICON_TYPE_GENERIC_CENTRAL_CONTROLLER >> 0) & 0xFF;
          
            Transport_SendDataApplEP((uint8_t *)&pTxBuf,
                                    sizeof(ZW_ZWAVEPLUS_INFO_REPORT_V2_FRAME),
                                    &pTxOptionsEx,
                                    zwavePlusInfoResponse_Compl, 0);

            return RECEIVED_FRAME_STATUS_SUCCESS;
        }
        break;

    default:
        break;
    }
    return RECEIVED_FRAME_STATUS_NO_SUPPORT;

}


/*============================ CommandClassVersionVersionGet ================
** Function description
** Read version
**
** Side effects: 
**
**-------------------------------------------------------------------------*/
uint8_t CommandClassZWavePlusInfoVersionGet(void)
{
    return ZWAVEPLUS_INFO_VERSION_V2; 
}

