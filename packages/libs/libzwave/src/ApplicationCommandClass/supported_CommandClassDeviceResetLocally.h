
#ifndef _SUPPORTED_COMMAND_CLASS_DEVICE_RESET_
#define _SUPPORTED_COMMAND_CLASS_DEVICE_RESET_

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"
#include "config.h"
#include "agi.h"
#include "ZW_TransportEndpoint.h"


/** 
 * @brief handleCommandClassDeviceResetLocally
 * Handler for Device Reset Locally CC
 * @param completedFunc Callback function pointer. Use the callback call to reset
 * the node. This function callback must be implemented!
 */
void  handleCommandClassDeviceResetLocally( void(* completedFunc)(uint8_t));


 
/** 
 * @brief CommandClassDeviceResetLocallyVersionGet
 * Get version
 * @return version
 */
uint8_t CommandClassDeviceResetLocallyVersionGet(void);



#endif
