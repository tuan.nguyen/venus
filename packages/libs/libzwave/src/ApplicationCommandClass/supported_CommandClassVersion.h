#ifndef _SUPPORTED_COMMAND_CLASS_VERSION__
#define _SUPPORTED_COMMAND_CLASS_VERSION__

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "zw_serialapi/zw_serialapi.h"
#include "zw_serialapi/zw_classcmd.h"
#include "config.h"
#include "ZW_TransportEndpoint.h"


uint8_t handleGetFirmwareHwVersion(void);


extern uint8_t handleCommandClassVersionAppl(uint8_t);


/*==============================   handleCommandClassVersion  ============
**
**  Function:  handler for Version CC
**
**  Side effects: None
**
**--------------------------------------------------------------------------*/
received_frame_status_t  
handleCommandClassVersion(ts_param_t *p,
                            ZW_APPLICATION_TX_BUFFER *pCmd,    /* IN Payload from the received frame, the union */
                            uint8_t   cmdLength                   /* IN Number of command bytes including the command */
);

uint8_t CommandClassVersionVersionGet(void);


/** 
 * @brief handleGetFirmwareVersion
 * Comment function...
 * @param N read version number n (0,1..N-1)
 * @param pVersion returns the Firmware n Version. Firmware n is dedicated to the 
 * Z-Wave chip firmware. The manufacturer MUST assign a version number.
 * @param pVariantgroup returns pointer to application version group number n.
 */
extern void
handleGetFirmwareVersion( uint8_t n, VG_VERSION_REPORT_V2_VG* pVariantgroup);


/** 
 * @brief handleNbrFirmwareVersions
 * Return number (N) of firmware versions.
 * @return N
 */
extern uint8_t
handleNbrFirmwareVersions(void);



#endif