#include "supported_CommandClassDeviceResetLocally.h"
#include "supported_CommandClassAssociation.h"

#include "utils.h"
#include "nvm.h"
#include "serialAPI.h"

static void (*cbFuncResponseJobStatus)(uint8_t txStatus);

void  deviceResetLocallyResponse_Compl(uint8_t bTxStatus, void *user, TX_STATUS_TYPE *t)
{
    mainlog(logDebug, "deviceResetLocallyResponse_Compl");
    if (TRANSMIT_COMPLETE_OK == bTxStatus)
    {
        mainlog(logDebug, "TRANSMIT_COMPLETE_OK"); 
    }
    else
    {
        mainlog(logDebug, "TRANSMIT_COMPLETE_NOT_OK"); 
    }

    if (cbFuncResponseJobStatus!=NULL)
    {
        cbFuncResponseJobStatus(bTxStatus);
    }
}

/*==============================   handleCommandClassDeviceResetLocally  ============
**
**  Function:  handler for Device Reset Locally CC
**
**  Side effects: None
**
**--------------------------------------------------------------------------*/
void  handleCommandClassDeviceResetLocally( void(* completedFunc)(uint8_t))
{
    uint8_t dest;
    cbFuncResponseJobStatus=completedFunc;
    dest = AssociationGetLifeLineNodeID();
    mainlog(logDebug,"DeviceResetLocally: destNode:%02X",dest);
    pTxBuf.ZW_DeviceResetLocallyNotificationFrame.cmdClass = COMMAND_CLASS_DEVICE_RESET_LOCALLY;
    pTxBuf.ZW_DeviceResetLocallyNotificationFrame.cmd = DEVICE_RESET_LOCALLY_NOTIFICATION;
    if (dest)
    {
        ts_param_t p;
        p.tx_flags = ZWAVE_PLUS_TX_OPTIONS;
        if (dest < ZW_MAX_NODES)
        {
            p.scheme = highest_scheme(GetCacheEntryFlag(MyNodeId));
        }else
        {
            p.scheme = NO_SCHEME;
        }

        p.snode = MyNodeId;
        p.sendpoint = 0;

        p.dnode = dest;
        p.dendpoint = 0;
        Transport_SendRequestExt((ZW_APPLICATION_TX_BUFFER *)&pTxBuf,
                            sizeof(ZW_DEVICE_RESET_LOCALLY_NOTIFICATION_FRAME),
                            &p,
                            deviceResetLocallyResponse_Compl, 0);
    }else
    {
        deviceResetLocallyResponse_Compl(TRANSMIT_COMPLETE_FAIL, NULL, NULL);
    }
     
}

/*============================ CommandClassDeviceResetLocallyVersionGet ======
** Function description
** Return version
**
** Side effects: 
**
**-------------------------------------------------------------------------*/
uint8_t 
CommandClassDeviceResetLocallyVersionGet(void)
{
    return DEVICE_RESET_LOCALLY_VERSION;  
}
