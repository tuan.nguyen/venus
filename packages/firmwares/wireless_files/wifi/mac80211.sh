#!/bin/sh
append DRIVERS "mac80211"

lookup_phy() {
	[ -n "$phy" ] && {
		[ -d /sys/class/ieee80211/$phy ] && return
	}

	local devpath
	config_get devpath "$device" path
	[ -n "$devpath" ] && {
		for _phy in /sys/devices/$devpath/ieee80211/phy*; do
			[ -e "$_phy" ] && {
				phy="${_phy##*/}"
				return
			}
		done
	}

	local macaddr="$(config_get "$device" macaddr | tr 'A-Z' 'a-z')"
	[ -n "$macaddr" ] && {
		for _phy in /sys/class/ieee80211/*; do
			[ -e "$_phy" ] || continue

			[ "$macaddr" = "$(cat ${_phy}/macaddress)" ] || continue
			phy="${_phy##*/}"
			return
		done
	}
	phy=
	return
}

find_mac80211_phy() {
	local device="$1"

	config_get phy "$device" phy
	lookup_phy
	[ -n "$phy" -a -d "/sys/class/ieee80211/$phy" ] || {
		echo "PHY for wifi device $1 not found"
		return 1
	}
	config_set "$device" phy "$phy"

	config_get macaddr "$device" macaddr
	[ -z "$macaddr" ] && {
		config_set "$device" macaddr "$(cat /sys/class/ieee80211/${phy}/macaddress)"
	}

	return 0
}

check_mac80211_device() {
	config_get phy "$1" phy
	[ -z "$phy" ] && {
		find_mac80211_phy "$1" >/dev/null || return 0
		config_get phy "$1" phy
	}
	[ "$phy" = "$dev" ] && found=1
}

vr_generate_mac() {
	local id=1
	local macaddr="$1"
	local oIFS="$IFS"; IFS=":"; set -- $macaddr; IFS="$oIFS"

	[ "$((0x$6))" -lt 255 ] && {
		printf "%s:%s:%s:%s:%s:%02x" $1 $2 $3 $4 $5 $(( 0x$6 ^ $id))
		return
	}

	off2=$(( (0x$6 + $id) / 0x100 ))
	printf "%s:%s:%s:%s:%02x:%02x" \
		$1 $2 $3 $4 \
		$(( (0x$5 + $off2) % 0x100 )) \
		$(( (0x$6 + $id) % 0x100 ))
}

detect_mac80211() {
	devidx=0
	config_load wireless
	while :; do
		config_get type "radio$devidx" type
		[ -n "$type" ] || break
		devidx=$(($devidx + 1))
	done

	for _dev in /sys/class/ieee80211/*; do
		[ -e "$_dev" ] || continue

		dev="${_dev##*/}"

		found=0
		config_foreach check_mac80211_device wifi-device
		[ "$found" -gt 0 ] && continue

		mode_band="g"
		channel="11"
		htmode=""
		ht_capab=""

		iw phy "$dev" info | grep -q 'Capabilities:' && htmode=HT20
		iw phy "$dev" info | grep -q '2412 MHz' || { mode_band="a"; channel="36"; }

		vht_cap=$(iw phy "$dev" info | grep -c 'VHT Capabilities')
		[ "$vht_cap" -gt 0 ] && {
			mode_band="a";
			channel="36"
			htmode="VHT80"
		}

		[ -n $htmode ] && append ht_capab "	option htmode	$htmode" "$N"

		if [ -x /usr/bin/readlink ]; then
			path="$(readlink -f /sys/class/ieee80211/${dev}/device)"
			path="${path##/sys/devices/}"
			dev_id="	option path	'$path'"
		else
			dev_id="	option macaddr	$(cat /sys/class/ieee80211/${dev}/macaddress)"
		fi

		local disabled="0"
		local sta_mac_addr=$(cat /sys/class/ieee80211/${dev}/macaddress)
		local ap_mac_addr=$(vr_generate_mac $sta_mac_addr)

		local ap_ssid=$(uci_get security @access-point[0] ssid | cut -c1-32)
		if [ -z "$ap_ssid" ]; then
			local boardName=`fw_printenv -n board_name | sed 's/ //g'`
			local boardSer=`fw_printenv -n serial_number | sed 's/ //g'`
			ap_ssid="$boardName ${boardSer#${boardSer%???}}"
		fi

		local ap_encryption=$(uci_get security @access-point[0] encryption)
		if [ -z "$ap_encryption" ]; then
			ap_encryption="open"
		fi

		local ap_key=$(uci_get security @access-point[0] key)
		if [ -z "$ap_key" ]; then
			ap_key="open"
		fi

		local ssid=$(uci_get alljoyn-onboarding @onboarding[0] ssid | cut -c1-32)
		if [ -z "$ssid" ]; then
			disabled="1"
		fi 
		local encryption=$(uci_get alljoyn-onboarding @onboarding[0] encryption)
		local key=$(uci_get alljoyn-onboarding @onboarding[0] key)

		cat <<EOF
config wifi-device  radio$devidx
	option type     mac80211
	option channel  ${channel}
	option hwmode	11${mode_band}
$dev_id
$ht_capab
	option disabled 0

config wifi-iface
	option device   radio$devidx
	option network  cereswifi
	option mode     'sta'
	option ssid     '$ssid'
	option encryption '$encryption'
	option key '$key'
	option macaddr '$sta_mac_addr'
	option ifname 'wlan0'
	option disabled '$disabled'

config wifi-iface
	option device   radio$devidx
	option network  lan
	option mode     ap
	option ssid     '$ap_ssid'
	option encryption '$ap_encryption'
	option key '$ap_key'
	option macaddr '$ap_mac_addr'
	option ifname 'wlan0-1'
	option disabled '0'

EOF
	devidx=$(($devidx + 1))
	done
}

