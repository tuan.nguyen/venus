#!/bin/sh

FIRMWARE_LOCATION=/
FILE_NAME=firmware.tar.gz

string="$1"

if [ -f "$1" ] && [ "${string##*.img}" == ".tar.gz" ]; then
    mv $1 $FIRMWARE_LOCATION/$FILE_NAME
else
    tmp=${string#*//}

    if [ "${tmp%%/*}" == "ftp.veriksystems.com" ];then
        curl -u venus_firmware:v3nu5_f!rmw@r3 $1 -o $FIRMWARE_LOCATION/$FILE_NAME    # $substring is in $string
    else
        wget $1 -O $FIRMWARE_LOCATION/$FILE_NAME    # $substring is not in $string
    fi
fi

if [ -f "$FIRMWARE_LOCATION/$FILE_NAME" ];
then
    bootstate=$(fw_printenv -n bootstate)

    if [ "${bootstate}x" == "0x" ]; 
    then
        DEV=/dev/mmcblk0p3
        echo "Start Writing to rootfs1 $DEV"
    else
        if [ "${bootstate}x" == "2x" ];
        then
            DEV=/dev/mmcblk0p2
            echo "Start Writing to rootfs0 $DEV"
        else
            echo "wrong bootstate"
            exit
        fi
    fi

    tar -xvzf $FIRMWARE_LOCATION/$FILE_NAME -C $FIRMWARE_LOCATION > $FIRMWARE_LOCATION/temp.txt 
    if [ $? == 0 ]; then
        cat $FIRMWARE_LOCATION/`cat $FIRMWARE_LOCATION/temp.txt` | dd of=$DEV bs=2048
        echo "Writing SUCCESS"

        if [ "${bootstate}x" == "0x" ]; 
        then
            fw_setenv bootstate 3
            fw_setenv checkboot 0
        else
            if [ "${bootstate}x" == "2x" ];
            then
                fw_setenv bootstate 1
                fw_setenv checkboot 0
            fi
        fi

        cd /overlay
        ls -A | grep -v "etc" | xargs rm -rf

        cd /overlay/etc
        mv /overlay/etc/crontabs/root /overlay/etc/config/crontabs_root
        ls -A | grep -v "database" | grep -v "alljoyn-onboarding" | grep -v "config" | grep -v "ssl" | grep -v "backup_restore_zwave_controller" | grep -v "devVoiceInform" | grep -v "bluetooth" | xargs rm -rf

        cd /overlay/etc/config
        ls -A | grep -v "alljoyn-onboarding" | grep -v "auto_config" | grep -v "security" | grep -v "wireless" | grep -v "crontabs_root" | grep -v "system" | grep -v "ZWaveFileData.bin" | xargs rm -rf

        echo "UPDATE FIRMWARE SUCCESS"
    else
        echo "UNTAR FAILED"
    fi
else
    echo "WRONG URL"
fi