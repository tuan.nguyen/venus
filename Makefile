#
# Venus Top Level Makefile
#
#
# Package command:
#  make VER="1.01" BUILD=01 all
#  make VER="1.01" BUILD=01 package
#
VER:="0.01"
BUILD:="01"

# External toolchain
EXT_TOOLCHAIN_DIR:=${PWD}/tools/gcc-4.8-linaro_uClibc-0.9.33.2_eabi/
# Default Environment
OPENWRT_DIR := ./openwrt

# Check download folder
OPENWRT_DL_DIR := /opensources

OPENWRT_DL_DIR := $(shell cd $(OPENWRT_DL_DIR) && /bin/pwd)
GIT_BRANCH:=$(shell git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ \1/' | cut -d " " -f 2)
GIT_LATEST_TAG:=$(shell git describe --tags `git rev-list --tags --max-count=1`)
FW_UPLOAD_FILE="openwrt-PhD-ext4"
FOTA_SERVER_URL="https://fw.zinnoinc.com/api/products/$(PRODUCT_NAME)-staging/versions"
FOTA_TOKEN=`cat ../patches/fota_token`
DROPBOX_APP_KEY=`cat ../patches/dropbox_token`
RESULT_FILE="fw_url_tmp.txt"



VENUS_APP_PKG_LIST:=radiomon alljoyn_handler pubsub_handler system_handler upnp_handler zigbee_handler zwave_handler bt_handler hue_bridge zigbee_programmer zwave_programmer
VENUS_LIB_PKG_LIST:=libverik-utils libzigbee libzwave libbluez-compact
VENUS_FIRMWARE_PKG_LIST:=./ 

ifeq ($(OPENWRT_DL_DIR),)
OPENWRT_DL_DIR := $(PWD)/dl
# Attempt to create a output directory.
$(shell [ -d $(OPENWRT_DL_DIR) ] || mkdir -p $(OPENWRT_DL_DIR))
endif

export OPENWRT_DL_DIR
export CROSS_TOOL_DIR=$(EXT_TOOLCHAIN_DIR)

.PHONY: all openwrt

all: release bsp_package

clean:
	@rm -fr *.o
	cd $(OPENWRT_DIR); make clean; cd -

openwrt_all_in_one_config:
	@echo
	@echo "############################## Configure openWRT for Development ##############################"
	@cp configs/all_in_one_config ${OPENWRT_DIR}/.config

openwrt_titan_develop_config:
	@echo
	@echo "############################## Configure openWRT for Development ##############################"
	@cp configs/titan_development_config ${OPENWRT_DIR}/.config

openwrt_titan_release_config:
	@echo
	@echo "############################## Configure openWRT for Release ##############################"
	@cp configs/titan_release_config ${OPENWRT_DIR}/.config

openwrt_venus_2_develop_config:
	@echo
	@echo "############################## Configure openWRT for Development ##############################"
	@cp configs/venus_2_development_config ${OPENWRT_DIR}/.config

openwrt_venus_2_release_config:
	@echo
	@echo "############################## Configure openWRT for Release ##############################"
	@cp configs/venus_2_release_config ${OPENWRT_DIR}/.config

openwrt_venus_develop_config:
	@echo
	@echo "############################## Configure openWRT for Development ##############################"
	@cp configs/venus_development_config ${OPENWRT_DIR}/.config

openwrt_venus_release_config:
	@echo
	@echo "############################## Configure openWRT for Release ##############################"
	@cp configs/venus_release_config ${OPENWRT_DIR}/.config

openwrt_ceres_develop_config:
	@echo
	@echo "############################## Configure openWRT for Development ##############################"
	@cp configs/ceres_development_config ${OPENWRT_DIR}/.config

openwrt_ceres_release_config:
	@echo
	@echo "############################## Configure openWRT for Release ##############################"
	@cp configs/ceres_release_config ${OPENWRT_DIR}/.config


#create_feed:
#	@cp ${OPENWRT_DIR}/feeds.conf.default ${OPENWRT_DIR}/feeds.conf
#	@echo "src-link venus_feed $(PWD)/packages/openwrt_venus" >> ${OPENWRT_DIR}/feeds.conf
#	@cd ${OPENWRT_DIR}; ./scripts/feeds update venus_feed; ./scripts/feeds install -p venus_feed -a; cd -

openwrt:
	@echo
	@echo "############################## Build openWRT for Release ##############################"
	cd $(OPENWRT_DIR); make $(V:%=V=%); cd -

openwrt_menuconfig:
	@echo
	@echo "############################## openWRT menuconfig ##############################"
	cd $(OPENWRT_DIR); make menuconfig; cd -

pkg_clean:
	@echo
	@echo "############################## Clean all temp and release packages ##############################"
	cd packages/apps; for n in $(VENUS_APP_PKG_LIST); do make -C $$n clean; done; cd -
	cd packages/libs; for n in $(VENUS_LIB_PKG_LIST); do make -C $$n clean; done; cd -
	cd packages/firmwares; for n in $(VENUS_FIRMWARE_PKG_LIST); do make -C $$n clean; done; cd -

#pkg_remove: pkg_clean
#	@echo
#	@echo "############################## Remove package from source repo ######################"
#	cd ${OPENWRT_DL_DIR}; for n in $(VENUS_APP_PKG_LIST); do rm -fr $$n*.tar.gz; done; cd -
#	cd ${OPENWRT_DL_DIR}; for n in $(VENUS_LIB_PKG_LIST); do rm -fr $$n*.tar.gz; done; cd -
#	cd ${OPENWRT_DL_DIR}; for n in $(VENUS_FIRMWARE_PKG_LIST); do rm -fr $$n*.tar.gz; done; cd -

#pkg_prepare: pkg_remove
#	@echo
#	@echo "############################## Prepare Release packages ##############################"
#	cd packages; for n in $(VENUS_APP_PKG_LIST); do make -C apps/$$n release; done; cd -
#	cd packages; for n in $(VENUS_LIB_PKG_LIST); do make -C libs/$$n release; done; cd -
#	cd packages; for n in $(VENUS_FIRMWARE_PKG_LIST); do make -C firmwares/$$n release; done; cd -

bsp_package:
	@echo
	@echo "############################## Package Venus BSP release #############################"
	@tar -cvzf venus_bsp_release_$(VER)_$(BUILD).tar.gz README Makefile ./src ./patches ./tools

trigger_job:
	@curl -X POST -F token=163af39cc08f0e1ca8519f1a37b018 -F ref="$(GIT_BRANCH)" https://gitlab.com/api/v4/projects/2266589/trigger/pipeline
	@echo

update_version:
	@echo "Update firmware version $(GIT_LATEST_TAG)"
	@sed -i -e '/config\ firmware-pre/{n;s/.*/\toption version \x27$(GIT_LATEST_TAG)\x27/}' ./openwrt/package/venus/system_handler/files/security.config
	@sed -i -e '/config\ firmware-info/{n;n;s/.*/\toption current \x27$(GIT_LATEST_TAG)\x27/}' ./openwrt/package/venus/system_handler/files/firmware.config
	@sed -i -e '/config\ firmware-info/{n;n;n;s/.*/\toption latest \x27$(GIT_LATEST_TAG)\x27/}' ./openwrt/package/venus/system_handler/files/firmware.config

deploy: ../patches/fota_token
	@echo "Update the firmware to dropbox"
	@curl -X POST https://content.dropboxapi.com/2/files/upload \
		--header "Authorization: Bearer $(DROPBOX_APP_KEY)" \
		--header "Dropbox-API-Arg: {\"path\": \"/$(FW_UPLOAD_FILE)_$(GIT_LATEST_TAG).img.tar.gz\",\"mode\": \"add\",\"autorename\": false,\"mute\": false,\"strict_conflict\": false}" \
		--header "Content-Type: application/octet-stream" \
		--data-binary @./bin/omap/$(FW_UPLOAD_FILE).img.tar.gz
	@echo "\nGet share link from dropbox"
	@curl -X POST https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings \
			--header "Authorization: Bearer $(DROPBOX_APP_KEY)" \
			--header "Content-Type: application/json" \
			--data "{\"path\": \"/$(FW_UPLOAD_FILE)_$(GIT_LATEST_TAG).img.tar.gz\",\"settings\": {\"requested_visibility\": \"public\"}}" > $(RESULT_FILE)
	@if [ `cat $(RESULT_FILE) | grep error | wc -l` -eq 1 ]; then \
		echo "The issue is:" &&  cat $(RESULT_FILE) && rm -f $(RESULT_FILE);\
	else \
		echo "Update link and tag to fota server" && \
		MD5CH="`md5sum ./bin/omap/$(FW_UPLOAD_FILE).img.tar.gz | cut -d " " -f 1`" && \
		DROPBOX_URL="`cat $(RESULT_FILE) | cut -d " " -f 4 | cut -d \\" -f 2`" && \
		curl -X POST --header "Content-Type: application/json" \
			     --header "Accept: application/json" \
			     --header "authorization: $(FOTA_TOKEN)" \
			     --data "{\"version\":\"$(GIT_LATEST_TAG)\",\"fwChecksum\": \"`echo $$MD5CH`\",\"fwUrl\":\"`echo $$DROPBOX_URL`\"}" "$(FOTA_SERVER_URL)" && \
		curl -X POST -F token=7c504281e947a4b66fadb04ea3300c -F ref="master" \
			-F "variables[DROPBOX_URL]=$$DROPBOX_URL" \
			-F "variables[TAG_VERSION]=$(GIT_LATEST_TAG)" \
			-F "variables[MD5CH]=$$MD5CH" \
			-F "variables[PRODUCT_NAME]=$(PRODUCT_NAME)" \
			https://gitlab.com/api/v4/projects/10890685/trigger/pipeline ; \
	fi
	@rm -fr $(RESULT_FILE) && echo "\r"

ceres_develop: openwrt_ceres_develop_config openwrt
ceres_release: openwrt_ceres_release_config openwrt

venus_develop: openwrt_venus_develop_config openwrt
venus_release: openwrt_venus_release_config openwrt

venus_2_develop: openwrt_venus_2_develop_config openwrt
venus_2_release: openwrt_venus_2_release_config openwrt

titan_develop: openwrt_titan_develop_config openwrt
titan_release: openwrt_titan_release_config openwrt

all_in_one: openwrt_all_in_one_config openwrt
