package com.verik.venus.sample;

import org.allseen.lsf.LampGroup;
import org.allseen.lsf.sampleapp.CapabilityData;
import org.allseen.lsf.sampleapp.DimmableItemDataModel;
import org.allseen.lsf.sampleapp.EffectType;

public class VenusElementDataModel  extends DimmableItemDataModel{
	public static final char TAG_PREFIX_SCENE_ELEMENT = 'v';

    protected static int nextID = 1;

    public final EffectType type;
    public LampGroup members;
    public String presetID;

    public VenusElementDataModel() {
        this(null, "");
    }

    public VenusElementDataModel(EffectType type, String name) {
        super(String.valueOf(nextID++), TAG_PREFIX_SCENE_ELEMENT, name);

        this.type = type;
        this.members = new LampGroup();

        // State is always set to "on". To turn the lamp off as part of an effect,
        // you have to set the brightness to zero
        this.state.setOnOff(true);

        this.capability.dimmable = CapabilityData.ALL;
        this.capability.color = CapabilityData.ALL;
        this.capability.temp = CapabilityData.ALL;
    }

    public VenusElementDataModel(VenusElementDataModel other) {
        super(other);

        this.type = other.type;
        this.members = new LampGroup(other.members);
        this.presetID = other.presetID;
    }

    public boolean containsGroup(String groupID) {
        String[] childIDs = members.getLampGroups();

        for (String childID : childIDs) {
            if (childID.equals(groupID)) {
                return true;
            }
        }

        return false;
    }

    public boolean containsPreset(String presetID) {
        //TODO-FIX
        return false;
    }
}
