package com.verik.venus.sample;

import org.allseen.lsf.LampState;
import org.allseen.lsf.sampleapp.BasicSceneElementDataModel;
import org.allseen.lsf.sampleapp.DimmableItemDataModel;
import org.allseen.lsf.sampleapp.DimmableItemPresetsFragment;
import org.allseen.lsf.sampleapp.PresetDataModel;
import org.allseen.lsf.sampleapp.PulseEffectDataModel;
import org.allseen.lsf.sampleapp.PulseEffectFragment;

public class VenusElementPresetsFragment extends
		DimmableItemPresetsFragment {

	@Override
	protected DimmableItemDataModel getDimmableItemDataModel() {
		return null;
	}

	@Override
	protected boolean isItemSelected(PresetDataModel presetModel) {
		if (key2 == PulseEffectFragment.STATE2_ITEM_TAG) {
			PulseEffectDataModel pulseModel = (PulseEffectDataModel) getDimmableItemDataModel();
			return presetModel.stateEquals(pulseModel.endState);
		} else {
			return super.isItemSelected(presetModel);
		}
	}

	@Override
	protected void doSavePreset(String presetName) {
		if (key2 == PulseEffectFragment.STATE2_ITEM_TAG) {
			PulseEffectDataModel pulseModel = (PulseEffectDataModel) getDimmableItemDataModel();
			doSavePreset(presetName, pulseModel.endState);
		} else {
			super.doSavePreset(presetName);
		}
	}

	@Override
	protected void doApplyPreset(PresetDataModel presetModel) {
		if (key2 == PulseEffectFragment.STATE2_ITEM_TAG) {
			PulseEffectDataModel pulseModel = (PulseEffectDataModel) getDimmableItemDataModel();
			pulseModel.endPresetID = presetModel.id;
			pulseModel.endState = new LampState(presetModel.state);
		} else {
			BasicSceneElementDataModel elementModel = (BasicSceneElementDataModel) getDimmableItemDataModel();
			elementModel.presetID = presetModel.id;
			elementModel.state = new LampState(presetModel.state);
		}
	}
}
