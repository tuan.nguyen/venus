package com.verik.venus.sample;

import org.allseen.lsf.LampGroup;
import org.allseen.lsf.sampleapp.CapabilityData;
import org.allseen.lsf.sampleapp.EnterNameFragment;
import org.allseen.lsf.sampleapp.R;
import org.allseen.lsf.sampleapp.SampleAppActivity;

import android.util.Log;

public class VenusEnterNameFragment extends EnterNameFragment {
	
	public VenusEnterNameFragment() {
        super(R.string.label_basic_scene);
    }

    @Override
    protected int getTitleID() {
        return R.string.title_basic_scene_add;
    }

    @Override
    protected void setName(String name) {
        VenusDataModel sceneModel = new VenusDataModel();

        sceneModel.setName(name);

        SampleAppActivity activity = (SampleAppActivity)getActivity();
        activity.pendingVenusModel = sceneModel;
        activity.pendingVenusElementMembers = new LampGroup();
        activity.pendingVenusElementCapability = new CapabilityData(true, true, true);

        Log.d(SampleAppActivity.TAG, "Pending venus basic scene name: " + activity.pendingVenusModel.getName());
    }

    @Override
    protected String getDuplicateNameMessage() {
        return this.getString(R.string.duplicate_name_message_scene);
    }

    @Override
    protected boolean duplicateName(String name) {
        for (VenusDataModel data : ((SampleAppActivity) this.getActivity()).venusModels.values()) {
            if (data.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }
}
