package com.verik.venus.sample;

import org.allseen.lsf.sampleapp.AllJoynManager;
import org.allseen.lsf.sampleapp.PageFrameChildFragment;
import org.allseen.lsf.sampleapp.PageMainContainerFragment;
import org.allseen.lsf.sampleapp.R;
import org.allseen.lsf.sampleapp.SampleAppActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class VenusInfoFragment extends PageFrameChildFragment implements View.OnClickListener {

	protected VenusDataModel getVenusBasicSceneDataModel() {
        return ((SampleAppActivity) getActivity()).pendingVenusModel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SampleAppActivity activity = (SampleAppActivity) getActivity();
        VenusDataModel venusModel = getVenusBasicSceneDataModel();

        view = inflater.inflate(R.layout.fragement_venus_info, container, false);
        View statusView = view.findViewById(R.id.venusInfoStatusRow);

        setImageButtonBackgroundResource(statusView, R.id.statusButtonPower, R.drawable.scene_set_icon);

        // item name
        TextView nameLabel = (TextView)statusView.findViewById(R.id.statusLabelName);
        nameLabel.setText(R.string.venus_info_name);
        nameLabel.setClickable(true);
        nameLabel.setOnClickListener(this);

        TextView nameText = (TextView)statusView.findViewById(R.id.statusTextName);
        nameText.setClickable(true);
        nameText.setOnClickListener(this);

        venusUpdateInfoFields(activity, venusModel);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        ((SampleAppActivity)getActivity()).updateActionBar(R.string.title_venus_info, false, true, false, true, true);
    }

    @Override
    public void onActionAdd() {
    	
    	Log.d("VENUS", "Start to scan device");

        ((PageMainContainerFragment)parent).showSelectMembersChildFragment();
    }

    @Override
    public void onActionDone() {
        parent.clearBackStack();
    }

    @Override
    public void onClick(View view) {
        int viewID = view.getId();
        
        if (viewID == R.id.statusLabelName || viewID == R.id.statusTextName) {
        	Log.d("VENUS", "onHeaderClick()");
//            onHeaderClick();
        } else if (viewID == R.id.detailedItemRowTextHeader || viewID == R.id.detailedItemRowTextDetails) {
        	Log.d("VENUS", "onElementTextClick()");
//            onElementTextClick(view.getTag().toString());
        } else if (viewID == R.id.detailedItemButtonMore) {
        	Log.d("VENUS", "onElementMoreClick()");
            onElementMoreClick(view, view.getTag().toString());
        }
    }

    protected void onHeaderClick() {
        SampleAppActivity activity = (SampleAppActivity)getActivity();
        VenusDataModel sceneModel = getVenusBasicSceneDataModel();

        activity.showItemNameDialog(R.string.title_basic_scene_rename, new VenusUpdateNameAdapter(sceneModel, (SampleAppActivity) getActivity()));
    }

    protected void onElementTextClick(String elementID) {
//        SampleAppActivity activity = (SampleAppActivity)getActivity();

//        if (basicSceneModel.effects != null) {
//            for (VenusEffectDataModel elementModel : basicSceneModel.effects) {
//                if (elementID.equals(elementModel.id)) {
//                    activity.pendingVenusBasicSceneElementMembers = new LampGroup(elementModel.members);
//                    activity.pendingVenusElementCapability = new CapabilityData(elementModel.capability);
//
//                    ((ScenesPageFragment)parent).showSelectMembersChildFragment();
//                    return;
//                }
//            }
//        }
    }

    protected void onElementMoreClick(View anchor, String elementID) {
    	Log.d("VENUS","onElementMoreClick: "+elementID);
        ((SampleAppActivity)getActivity()).onItemButtonMore(parent, SampleAppActivity.Type.DEVICE, anchor, key, elementID);
    }

    public void venusUpdateInfoFields() {
        SampleAppActivity activity = (SampleAppActivity)getActivity();
        venusUpdateInfoFields(activity, getVenusBasicSceneDataModel());
    }

	public void venusUpdateInfoFields(SampleAppActivity activity, VenusDataModel venusModel) {
        // Update name and members
        setTextViewValue(view, R.id.statusTextName, venusModel.getName(), 0);

        // Displays list of effects in this scene
        TableLayout elementsTable = (TableLayout) view.findViewById(R.id.venusInfoElementTable);
        elementsTable.removeAllViews();
        
        if (AllJoynManager.venuses.size() != 0 && AllJoynManager.venusConnected){
        	if(AllJoynManager.subDevices.size() > 0){
        		for(int i=0;i<AllJoynManager.subDevices.size(); i++){
            		if(AllJoynManager.subDevices.get(i).getControllerId().equals(activity.pendingVenusDevice)){
            			addElementRow(activity, elementsTable, R.drawable.list_constant_icon, AllJoynManager.subDevices.get(i).getId()+"", AllJoynManager.subDevices.get(i).getName(), R.string.effect_name_none);
            		}
            	}
        	}
        }


        if (elementsTable.getChildCount() == 0) {
            //TODO-CHK Do we show "no effects" here?
        }
    }

    protected void addElementRow(SampleAppActivity activity, TableLayout elementTable, int iconID, String elementID, String members, int detailsID) {
        addElementRow(activity, elementTable, iconID, elementID, members, getString(detailsID));
    }

    protected void addElementRow(SampleAppActivity activity, TableLayout elementTable, int iconID, String elementID, String members, String details) {
        
    	if(members != null){
	    	TableRow tableRow = new TableRow(view.getContext());
	        activity.getLayoutInflater().inflate(R.layout.view_scene_element_row, tableRow);
	
	        ((ImageButton)tableRow.findViewById(R.id.detailedItemButtonIcon)).setImageResource(iconID);
	
	        TextView textHeader = (TextView)tableRow.findViewById(R.id.detailedItemRowTextHeader);
	        textHeader.setText(members);
	        textHeader.setTag(elementID);
	        textHeader.setClickable(true);
	        textHeader.setOnClickListener(this);

        	TextView textDetails = (TextView)tableRow.findViewById(R.id.detailedItemRowTextDetails);
            textDetails.setText(details);
            textDetails.setTag(elementID);
            textDetails.setClickable(true);
            textDetails.setOnClickListener(this);

            ImageButton moreButton = (ImageButton)tableRow.findViewById(R.id.detailedItemButtonMore);
            moreButton.setImageResource(R.drawable.group_more_menu_icon);
            moreButton.setTag(elementID);
            moreButton.setOnClickListener(this);
            elementTable.addView(tableRow);
        }
    }

}
