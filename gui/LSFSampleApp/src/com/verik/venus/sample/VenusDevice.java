package com.verik.venus.sample;

import org.alljoyn.bus.Mutable;

public class VenusDevice {
	
	private String name;
	
	private Mutable.IntegerValue sessionId;
	
	public VenusDevice(String name, Mutable.IntegerValue sessionId){
		this.name = name;
		this.sessionId = sessionId;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setSessionId(Mutable.IntegerValue sessionId){
		this.sessionId = sessionId;
	}
	
	public Mutable.IntegerValue getSessionId(){
		return this.sessionId;
	}
}
