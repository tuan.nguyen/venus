package com.verik.venus.sample;

import org.allseen.lsf.sampleapp.PageFrameChildFragment;
import org.allseen.lsf.sampleapp.PageMainContainerFragment;
import org.allseen.lsf.sampleapp.SampleAppActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class VenusPageFragment extends PageMainContainerFragment{
	public static final String CHILD_TAG_SELECT_DETAIL = "DETAILS";

    public static String TAG;

    @Override
    protected PageFrameChildFragment createChildFragment(String tag)
    {
    	return super.createChildFragment(tag);
    }

    @Override
    public PageFrameChildFragment createTableChildFragment() {
        return new VenusTableFragment();
    }

    @Override
    public PageFrameChildFragment createInfoChildFragment() {
        return new VenusInfoFragment();
    }

    @Override
    public PageFrameChildFragment createPresetsChildFragment() {
        return new VenusElementPresetsFragment();
    }

    @Override
    public PageFrameChildFragment createEnterNameChildFragment() {
        return new VenusEnterNameFragment();
    }

    @Override
    public PageFrameChildFragment createSelectMembersChildFragment() {
        return new VenusSelectMembersFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);

        VenusPageFragment.TAG = getTag();

        return root;
    }

    @Override
    public int onBackPressed() {
        String startingChildTag = child.getTag();
        int backStackCount = super.onBackPressed();

        if (CHILD_TAG_ENTER_NAME.equals(startingChildTag)) {
            // To support the basic scene creation workflow, when going backwards
            // from the enter name fragment we have to skip over the dummy scene
            // info fragment (see SampleAppActivity.doAddScene()). So we queue up
            // a second back press here.
            ((SampleAppActivity)getActivity()).postOnBackPressed();
        }

        return backStackCount;
    }
}
