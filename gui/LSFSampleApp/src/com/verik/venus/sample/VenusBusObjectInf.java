package com.verik.venus.sample;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.annotation.BusInterface;
import org.alljoyn.bus.annotation.BusSignal;

@BusInterface(name = "com.verik.bus.VENUS_BOARD")
public interface VenusBusObjectInf {
	
	@BusSignal
	public void GetDeviceList(String input) throws BusException;
	
	@BusSignal
	public void GetDeviceListCB(String input) throws BusException;
	
	@BusSignal
	public void SendCommand(String input1, String input2) throws BusException;
	
	@BusSignal
	public void SendCommandCB(String input) throws BusException;
	
	@BusSignal
	public void AddDevice(String input) throws BusException;
	
	@BusSignal
	public void AddDeviceCB(String input) throws BusException;
	
	@BusSignal
	public void RemoveDevice(String input) throws BusException;
	
	@BusSignal
	public void RemoveDeviceCB(String input) throws BusException;
	
	@BusSignal
	public void AddGroup(String input1, String input2) throws BusException;
	
	@BusSignal
	public void AddGroupCB(String input) throws BusException;
	
	@BusSignal
	public void RemoveGroup(String input) throws BusException;
	
	@BusSignal
	public void RemoveGroupCB(String input) throws BusException;
	
	@BusSignal
	public void RemoveNode(String input1, String input2) throws BusException;
	
	@BusSignal
	public void RemoveNodeCB(String input) throws BusException;
	
	@BusSignal
	public void SendGroupCommand(String input1, String input2) throws BusException;
	
	@BusSignal
	public void SendGroupCommandCB(String input) throws BusException;
	
	@BusSignal
	public void SendNotify(String input) throws BusException;
	
}
