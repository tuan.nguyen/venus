package com.verik.venus.sample;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.SignalEmitter;
import org.allseen.lsf.sampleapp.AllJoynManager;
import org.allseen.lsf.sampleapp.PageFrameParentFragment;
import org.allseen.lsf.sampleapp.SampleAppActivity;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.verik.venus.VenusManagerCallback;

//public class VenusSampleManagerCallback extends SceneManagerCallback {
public class VenusSampleManagerCallback extends VenusManagerCallback{
	protected SampleAppActivity activity;
	protected FragmentManager fragmentManager;
	protected Handler handler;

	public VenusSampleManagerCallback(SampleAppActivity activity,
			FragmentManager fragmentManager, Handler handler) {
		super();

		this.activity = activity;
		this.fragmentManager = fragmentManager;
		this.handler = handler;
	}

	// implement and handle callback functions here

	public void connectToVenus(final String venusID) {
		String uniqueName = AllJoynManager.bus.getUniqueName();
		Log.d("VENUS", "LSF unique name: "+ uniqueName);
		
		handler.post(new Runnable() {
			@Override
			public void run() {
				if(AllJoynManager.SESSION_ID != null){
					AllJoynManager.signalEmitter = new SignalEmitter(activity, AllJoynManager.SESSION_ID.value, SignalEmitter.GlobalBroadcast.Off);
					
					AllJoynManager.busObjectInf = AllJoynManager.signalEmitter.getInterface(VenusBusObjectInf.class);
					
					if(AllJoynManager.busObjectInf != null){
						Log.d("VENUS","Start to send signal GetDeviceList");
						try {
							AllJoynManager.busObjectInf.GetDeviceList(venusID);
							Log.d("VENUS", "Send GetDeviceList signal successful.");
						} catch (BusException e) {
							Log.d("VENUS", "Error when calling GetDeviceList: "+e.getMessage());
						}
					}
				}
			}
			
		});
	}
	
	public void removeSubDevice(final String subDeviceId){
		Log.d("VENUS","Start to remove sub devicce: "+subDeviceId);
		
		handler.post(new Runnable() {
			@Override
			public void run() {
				if(AllJoynManager.SESSION_ID != null){
					AllJoynManager.signalEmitter = new SignalEmitter(activity, AllJoynManager.SESSION_ID.value, SignalEmitter.GlobalBroadcast.Off);
					
					AllJoynManager.busObjectInf = AllJoynManager.signalEmitter.getInterface(VenusBusObjectInf.class);
					
					if(AllJoynManager.busObjectInf != null){
						Log.d("VENUS","Start to send signal RemoveDevice");
						try {
							AllJoynManager.busObjectInf.RemoveDevice(subDeviceId);
							Log.d("VENUS", "Send RemoveDevice signal successful.");
						} catch (BusException e) {
							Log.d("VENUS", "Error when calling RemoveDevice: "+e.getMessage());
						}
					}
				}
			}
		});
	}
	
	public void blinkSubDevice(final String subDeviceId){
		Log.d("VENUS","Start to remove sub devicce: "+subDeviceId);
		
		handler.post(new Runnable() {
			@Override
			public void run() {
				if(AllJoynManager.SESSION_ID != null){
					AllJoynManager.signalEmitter = new SignalEmitter(activity, AllJoynManager.SESSION_ID.value, SignalEmitter.GlobalBroadcast.Off);
					
					AllJoynManager.busObjectInf = AllJoynManager.signalEmitter.getInterface(VenusBusObjectInf.class);
					
					if(AllJoynManager.busObjectInf != null){
						Log.d("VENUS","Start to send signal to blink device");
						try {
							AllJoynManager.busObjectInf.SendCommand(subDeviceId,AllJoynManager.BLINK+"");
							Log.d("VENUS", "Send blink signal successful.");
						} catch (BusException e) {
							Log.d("VENUS", "Error when calling blink device: "+e.getMessage());
						}
					}
				}
			}
			
		});
	}
	
	public void disconnectVenus(final String venusID) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				Log.d("VENUS", "Handle disconnect from venus here");
			}
			
		});
	}

	public void postUpdateVenusID(final String venusID) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				VenusDataModel venusModel = activity.venusModels.get(venusID);

				if (venusModel == null) {
					venusModel = new VenusDataModel(venusID);
					activity.venusModels.put(venusID, venusModel);
				}
			}
		});
		postUpdateVenusDisplay(venusID);
	}

	public void postRemoveVenus(final String venusID) {
		Log.d("VENUS", "Remove Venus device: "+venusID);
		handler.post(new Runnable() {
			@Override
			public void run() {
				
				Fragment pageFragment = fragmentManager.findFragmentByTag(VenusPageFragment.TAG);
                FragmentManager childManager = pageFragment != null ? pageFragment.getChildFragmentManager() : null;
                VenusTableFragment tableFragment = childManager != null ? (VenusTableFragment)childManager.findFragmentByTag(PageFrameParentFragment.CHILD_TAG_TABLE) : null;

				activity.venusModels.remove(venusID);
				
				if(tableFragment != null){
					tableFragment.removeElement(venusID);
				}
				
				if(tableFragment.isVisible()){
					activity.resetActionBar();
				}

				if (activity.venusModels.size() == 0) {
					AllJoynManager.venusConnected = false;
				}
				
				tableFragment.updateLoading(true);
			}
		});
	}
	
	public void postRemoveSubDevice(final String deviceID){
		Log.d("VENUS", "Remove Venus sub device: "+deviceID);
		removeSubdevice(deviceID);
		handler.post(new Runnable() {
			
			@Override
			public void run() {
				Fragment pageFragment = fragmentManager.findFragmentByTag(VenusPageFragment.TAG);
                FragmentManager childManager = pageFragment != null ? pageFragment.getChildFragmentManager() : null;
                VenusInfoFragment infoFragment = childManager != null ? (VenusInfoFragment)childManager.findFragmentByTag(PageFrameParentFragment.CHILD_TAG_INFO) : null;
                
                if(infoFragment != null){
                	infoFragment.venusUpdateInfoFields();
                }
			}
		});
	}

	protected void postUpdateSceneName(final String sceneID,
			final String sceneName) {

	}

	protected void postUpdateVenusDisplay(final String sceneID) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				refreshScene(sceneID);
			}
		});
	}

	public void refreshScene(String sceneID) {
		VenusPageFragment venusPageFragment = (VenusPageFragment) fragmentManager
				.findFragmentByTag(VenusPageFragment.TAG);

		if (venusPageFragment != null) {
			VenusTableFragment venusTableFragment = (VenusTableFragment) venusPageFragment
					.getChildFragmentManager().findFragmentByTag(
							PageFrameParentFragment.CHILD_TAG_TABLE);
			if (venusTableFragment != null && sceneID != null) {
				venusTableFragment.addElement(sceneID);

				if (venusTableFragment.isVisible()) {
					activity.resetActionBar();
				}
			}
		}

	}
	
	private void removeSubdevice(String deviceID){
		Log.d("VENUS","Remove sub device: "+deviceID);
		for(int i=0; i<AllJoynManager.subDevices.size();i++){
			if(AllJoynManager.subDevices.get(i).getId() == Integer.parseInt(deviceID)){
				AllJoynManager.subDevices.remove(i);
				break;
			}
		}
	}
}
