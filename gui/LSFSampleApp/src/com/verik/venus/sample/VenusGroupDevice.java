package com.verik.venus.sample;

import java.util.ArrayList;

public class VenusGroupDevice {
	
	private int id;
	
	private String name;
	
	private ArrayList<VenusSubDevice> members;
	
	private int status;
	
	private String controllerId;
	
	public VenusGroupDevice(){
		
	}
	
	public VenusGroupDevice(int id, String name, ArrayList<VenusSubDevice> members, int status, String controllerId){
		this.id = id;
		this.name = name;
		this.members = members;
		this.status = status;
		this.controllerId = controllerId;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setMembers(ArrayList<VenusSubDevice> members){
		this.members = members;
	}
	
	public ArrayList<VenusSubDevice> getMembers(){
		return this.members;
	}
	
	public void setStatus(int status){
		this.status = status;
	}
	
	public int getStatus(){
		return this.status;
	}
	
	public void setControllerId(String controllerId){
		this.controllerId = controllerId;
	}
	
	public String getControllerId(){
		return this.controllerId;
	}

}
