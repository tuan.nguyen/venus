package com.verik.venus.sample;

import java.util.ArrayList;

import org.alljoyn.bus.SessionListener;
import org.allseen.lsf.sampleapp.AllJoynManager;

import android.util.Log;

public class VenusSessionListener extends SessionListener {
	
	@Override
	public void sessionLost(int sessionId, int reason){
		//will change layout of connect here
		Log.d("VENUS", "Session "+sessionId+" has been lost.");
		
		String venusID = "";
		
		for(String id : AllJoynManager.venuses.keySet()){
			if(AllJoynManager.venuses.get(id).getSessionId().value == sessionId){
				AllJoynManager.venuses.remove(id);
				removeSubDevices(id);
				venusID = id;
			}
		}
		
		Log.d("VENUS","Disconnected venus device: "+venusID);
		
		ArrayList<VenusGroupDevice> removeGroup = new ArrayList<VenusGroupDevice>();
		
		for(VenusGroupDevice group : AllJoynManager.venusGroups){
			if(group.getControllerId().equals(venusID)){
				removeGroup.add(group);
				AllJoynManager.aboutManager.removeVenusGroup("G"+group.getId());
			}
		}
		
		for(VenusGroupDevice group : removeGroup){
			AllJoynManager.venusGroups.remove(group);
			
		}
		
		if(AllJoynManager.venuses.size() == 0){
			AllJoynManager.venusConnected = false;
		}
		
		AllJoynManager.aboutManager.removeVenus(venusID);
		
		AllJoynManager.SESSION_ID.value = 0;
		
		Log.d("VENUS","subdevice size: "+AllJoynManager.subDevices.size());
		Log.d("VENUS","Venus size: "+AllJoynManager.venuses.size());
	}
	
	private void removeSubDevices(String venusID){
		Log.d("VENUS","subdevice of: "+venusID);
		ArrayList<VenusSubDevice> removeDevice = new ArrayList<VenusSubDevice>();
		for(int i=0; i<AllJoynManager.subDevices.size();i++){
			Log.d("VENUS","Compare controllerId = "+AllJoynManager.subDevices.get(i).getControllerId()+", VenusID = "+venusID+". Result = "+AllJoynManager.subDevices.get(i).getControllerId().equals(venusID));
			if(AllJoynManager.subDevices.get(i).getControllerId().equals(venusID)){
				removeDevice.add(AllJoynManager.subDevices.get(i));
			}
		}
		
		for(int i=0;i<removeDevice.size();i++){
			AllJoynManager.subDevices.remove(removeDevice.get(i));
			AllJoynManager.aboutManager.removeVenusLamp("Z"+removeDevice.get(i).getId());
		}
	}

}
