package com.verik.venus.sample;

public class VenusSubDevice{
	
	private int id;
	
	private String name;
	
	private int type;
	
	private int status;
	
	private String controllerId;
	
	public VenusSubDevice(int id, String name, int type, int status, String controllerId){
		this.id = id;
		this.name = name;
		this.type = type;
		this.status = status;
		this.controllerId = controllerId;
	}
	
	public void setId(int id){
		this.id= id;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setType(int type){
		this.type = type;
	}
	
	public int getType(){
		return this.type;
	}
	
	public void setStatus(int status){
		this.status = status;
	}
	
	public int getStatus(){
		return this.status;
	}
	
	public void setControllerId(String controllerId){
		this.controllerId = controllerId;
	}
	
	public String getControllerId(){
		return this.controllerId;
	}

	@Override
	public String toString() {
		return "Status: "+id+", name: "+name+", type: "+type+", status: "+status+", controllerId: "+controllerId+".";
	}
}
