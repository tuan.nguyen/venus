package com.verik.venus.sample;

import org.allseen.lsf.sampleapp.AllJoynManager;
import org.allseen.lsf.sampleapp.R;
import org.allseen.lsf.sampleapp.SampleAppActivity;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.TextView.BufferType;

public class VenusTableFragment extends VenusItemTableFragement {
	
	public VenusTableFragment() {
        super();
        type = SampleAppActivity.Type.VENUS;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);

        for (String venusID : ((SampleAppActivity) getActivity()).venusModels.keySet()) {
            addElement(venusID);
        }

        return root;
    }

    @Override
    public void removeElement(String id) {
        super.removeElement(id);
        updateLoading(true);
    }

    @SuppressLint("DefaultLocale")
	@Override
    public void addElement(String id) {
        SampleAppActivity activity = (SampleAppActivity) getActivity();
        VenusDataModel venusModel = activity.venusModels.get(id);

		String details = id.toUpperCase();
		
		insertDetailedItemRow(getActivity(), venusModel.id,
				venusModel.tag, venusModel.getName(), details, false);
		updateLoading(true);
    }

    @Override
    public void updateLoading(boolean isVenus) {
        super.updateLoading(isVenus);
        
        //loading screen - nga.le
        
        if (AllJoynManager.controllerConnected && ((SampleAppActivity) getActivity()).venusModels.size() == 0) {
            // connected but no scenes found; display create scenes screen, hide the scroll table
            layout.findViewById(R.id.scrollLoadingView).setVisibility(View.VISIBLE);
            layout.findViewById(R.id.scrollScrollView).setVisibility(View.GONE);

            View loadingView = layout.findViewById(R.id.scrollLoadingView);

            ((TextView) loadingView.findViewById(R.id.loadingText1)).setText(getActivity().getText(R.string.no_scenes));

            // creates text with the plus icon
            TextView loadingText2 = (TextView) loadingView.findViewById(R.id.loadingText2);
            String createScenesText = getActivity().getText(R.string.create_scenes).toString();
            SpannableStringBuilder ssb = new SpannableStringBuilder(createScenesText);

            // gets the plus icon, and finds where in the text it should go
            // the icon is scaled down slightly to look less "floaty" as it is bottom aligned to the text
            Drawable plusIcon = getResources().getDrawable(R.drawable.nav_add_icon_normal);
            plusIcon.setBounds(0, 0, (int) (plusIcon.getIntrinsicWidth() * 0.8), (int) (plusIcon.getIntrinsicHeight() * 0.8));

            int plusPosition = createScenesText.indexOf('+');
            ssb.setSpan(new ImageSpan(plusIcon, ImageSpan.ALIGN_BOTTOM), plusPosition, plusPosition + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            loadingText2.setText(ssb, BufferType.SPANNABLE);

            loadingView.findViewById(R.id.loadingProgressBar).setVisibility(View.GONE);
        } else {
            View loadingView = layout.findViewById(R.id.scrollLoadingView);
            loadingView.findViewById(R.id.loadingProgressBar).setVisibility(View.VISIBLE);
        }
    }
}
