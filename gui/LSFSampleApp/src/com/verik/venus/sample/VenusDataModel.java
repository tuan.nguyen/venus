package com.verik.venus.sample;

import java.util.List;

import org.allseen.lsf.Scene;
import org.allseen.lsf.sampleapp.ItemDataModel;

public class VenusDataModel extends ItemDataModel {
	public static final char TAG_PREFIX_SCENE = 'V';

    public static String defaultName = "VENUS";

    public VenusDataModel() {
        this("");
    }

    public VenusDataModel(String sceneID) {
        this(sceneID, defaultName);
    }

    public VenusDataModel(String sceneID, String sceneName) {
        super(sceneID, TAG_PREFIX_SCENE, sceneName);
    }

    public VenusDataModel(VenusDataModel other) {
        super(other);
    }

    protected <T extends VenusElementDataModel> void updateElement(List<T> elementModels, T elementModel) {
        boolean updated = false;

        for (int i = 0; !updated && i < elementModels.size(); i++) {
            if (elementModels.get(i).id.equals(elementModel.id)){
                elementModels.set(i, elementModel);
                updated = true;
            }
        }

        if (!updated) {
            elementModels.add(elementModel);
        }
   }

    protected <T extends VenusElementDataModel> boolean removeElement(List<T> elementModels, String elementID) {
        boolean removed = false;

        for (int i = 0; !removed && i < elementModels.size(); i++) {
            if (elementModels.get(i).id.equals(elementID)){
                elementModels.remove(i);
                removed = true;
            }
        }

        return removed;
   }

    public Scene toScene() {
        Scene scene = new Scene();

        return scene;
    }

	public void fromScene(Scene scene) {
	}
}
