package com.verik.venus.sample;


import org.allseen.lsf.sampleapp.SampleAppActivity;
import org.allseen.lsf.sampleapp.UpdateItemNameAdapter;

public class VenusUpdateNameAdapter extends UpdateItemNameAdapter {

	public VenusUpdateNameAdapter(VenusDataModel basicSceneModel, SampleAppActivity activity) {
        super(basicSceneModel, activity);
    }

    @Override
    protected void doUpdateName() {

    }

    @Override
    protected String getDuplicateNameMessage() {
    	return null;
    }

    @Override
    protected boolean duplicateName(String name) {
        return false;
    }
}
