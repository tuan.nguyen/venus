package com.verik.venus.sample;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.BusObject;
import org.alljoyn.bus.SignalEmitter;
import org.alljoyn.bus.Status;
import org.alljoyn.bus.annotation.BusSignal;
import org.alljoyn.bus.annotation.BusSignalHandler;
import org.allseen.lsf.LampDetails;
import org.allseen.lsf.LampGroup;
import org.allseen.lsf.sampleapp.AllJoynManager;
import org.allseen.lsf.sampleapp.CapabilityData;
import org.allseen.lsf.sampleapp.DimmableItemScaleConverter;
import org.allseen.lsf.sampleapp.GroupDataModel;
import org.allseen.lsf.sampleapp.LampDataModel;
import org.allseen.lsf.sampleapp.R;
import org.allseen.lsf.sampleapp.SampleAppActivity;
import org.allseen.lsf.sampleapp.SelectMembersFragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class VenusSelectMembersFragment extends SelectMembersFragment implements VenusBusObjectInf, BusObject  {

	private LayoutInflater inflater;
	private View root;

	private static VenusSubDevice subDevice;
	private static String venusID;

	public VenusSelectMembersFragment() {
		super(R.string.label_venus);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View root = super.onCreateView(inflater, container, savedInstanceState);

		this.inflater = inflater;
		this.root = root;

		View loadingView = view.findViewById(R.id.venusScrollLoadingView);

		loadingView.setVisibility(View.VISIBLE);

		((TextView) loadingView.findViewById(R.id.loadingText1))
				.setText(getActivity().getText(R.string.no_device));
		((TextView) loadingView.findViewById(R.id.loadingText2))
				.setText(getActivity().getText(R.string.search_device));

		// remove two selection option
		view.findViewById(R.id.selectAll).setVisibility(View.GONE);
		view.findViewById(R.id.selectNone).setVisibility(View.GONE);

		venusID = ((SampleAppActivity) getActivity()).pendingVenusDevice;

		return root;
	}

	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		
		// add signal handler
		Status rs = AllJoynManager.bus.registerBusObject(this,
				AllJoynManager.VENUS_OBJECT_PATH);
		Log.d("VENUS", "Register bus signal: " + rs.toString());

		rs = AllJoynManager.bus.registerSignalHandlers(this);
		Log.d("VENUS", "Register signal handler: " + rs.toString());
		((SampleAppActivity) getActivity()).handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				if(AllJoynManager.SESSION_ID != null){
					AllJoynManager.signalEmitter = new SignalEmitter((SampleAppActivity)getActivity(), AllJoynManager.SESSION_ID .value, SignalEmitter.GlobalBroadcast.Off);
					
					AllJoynManager.busObjectInf = AllJoynManager.signalEmitter.getInterface(VenusBusObjectInf.class);
					
					if(AllJoynManager.busObjectInf != null){
						try {
							AllJoynManager.busObjectInf.AddDevice(venusID);
							Log.d("VENUS", "Send signal AddDevice successful.");
						} catch (BusException e) {
							Log.d("VENUS", "Send signal AddDevice error: " + e.getMessage());
						}
					}
				}
			}
		}, 500);
	}

	public void updateSubDeviceList() {
		if (subDevice != null) {
			View loadingView = view.findViewById(R.id.venusScrollLoadingView);
			
			loadingView.setVisibility(View.INVISIBLE);

			updateSelectableItemRow(inflater, root, "", null,
					R.drawable.group_lightbulb_icon, subDevice.getName(), true);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		((SampleAppActivity) getActivity()).updateActionBar(
				R.string.title_venus_device_add, false, false, false, true,
				false);
	}

	@Override
	protected String getHeaderText() {
		return getString(R.string.venus_device_add);
	}

	@Override
	protected LampGroup getPendingMembers() {
		return ((SampleAppActivity) getActivity()).pendingVenusElementMembers;
	}

	@Override
	protected String getPendingItemID() {
		return ((SampleAppActivity) getActivity()).pendingVenusModel.id;
	}

	@Override
	protected void processSelection(SampleAppActivity activity,
			List<String> lampIDs, List<String> groupIDs, List<String> sceneIDs, List<String> subDeviceIDs,
			CapabilityData capability) {
		Log.d("VENUS", "Handle selection. 1");
		activity.pendingVenusElementCapability = capability;
		super.processSelection(activity, lampIDs, groupIDs, sceneIDs, subDeviceIDs,
				capability);
	}

	@Override
	protected void processSelection(SampleAppActivity activity,
			List<String> lampIDs, List<String> groupIDs, List<String> sceneIDs, List<String> subDeviceIDs) {
		Log.d("VENUS", "Handle selection. 2");
		activity.pendingVenusElementMembers.setLamps(lampIDs
				.toArray(new String[lampIDs.size()]));
		activity.pendingBasicSceneElementMembers.setLampGroups(groupIDs
				.toArray(new String[groupIDs.size()]));

		activity.pendingVenusElementMembersHaveEffects = false;
		activity.pendingVenusElementMembersMinColorTemp = -1;
		activity.pendingVenusElementMembersMaxColorTemp = -1;

		processGroupSelection(activity, groupIDs);
		processLampSelection(activity, lampIDs);

		if (activity.pendingVenusElementMembersMinColorTemp == -1) {
			activity.pendingVenusElementMembersMinColorTemp = DimmableItemScaleConverter.VIEW_COLORTEMP_MIN;
		}

		if (activity.pendingVenusElementMembersMaxColorTemp == -1) {
			activity.pendingVenusElementMembersMaxColorTemp = DimmableItemScaleConverter.VIEW_COLORTEMP_MAX;
		}
	}

	protected void processGroupSelection(SampleAppActivity activity,
			List<String> groupIDs) {
		if (groupIDs.size() > 0) {
			for (Iterator<String> it = groupIDs.iterator(); it.hasNext();) {
				processLampSelection(activity, it.next());
			}
		}
	}

	protected void processLampSelection(SampleAppActivity activity,
			String groupID) {
		Log.d("VENUS", "Handle Lamp selection.");
		GroupDataModel groupModel = activity.groupModels.get(groupID);

		if (groupModel != null) {
			processLampSelection(activity, groupModel.getLamps());
		}
	}

	protected void processLampSelection(SampleAppActivity activity,
			Collection<String> lampIDs) {
		if (lampIDs.size() > 0) {
			for (Iterator<String> it = lampIDs.iterator(); it.hasNext();) {
				LampDataModel lampModel = activity.lampModels.get(it.next());

				if (lampModel != null) {
					LampDetails lampDetails = lampModel.getDetails();

					if (lampDetails != null) {
						boolean lampHasEffects = lampDetails.hasEffects();
						int lampMinTemperature = lampDetails
								.getMinTemperature();
						int lampMaxTemperature = lampDetails
								.getMaxTemperature();

						if (lampHasEffects) {
							activity.pendingVenusElementMembersHaveEffects = true;
						}

						if (lampMinTemperature < activity.pendingVenusElementMembersMinColorTemp
								|| activity.pendingVenusElementMembersMinColorTemp == -1) {
							activity.pendingVenusElementMembersMinColorTemp = lampMinTemperature;
						}

						if (lampMaxTemperature < activity.pendingVenusElementMembersMaxColorTemp
								|| activity.pendingVenusElementMembersMaxColorTemp == -1) {
							activity.pendingVenusElementMembersMaxColorTemp = lampMaxTemperature;
						}
					}
				}
			}
		}
	}

	@Override
	public void onActionDone() {
		parent.clearBackStack();
		((SampleAppActivity) getActivity()).lampManagerCB.postVenusLampUI();
	}

	protected void showNoEffectChildFragment(SampleAppActivity activity) {
	}

	@Override
	protected int getMixedSelectionMessageID() {
		return R.string.mixing_lamp_types_message_scene;
	}

	@Override
	protected int getMixedSelectionPositiveButtonID() {
		return R.string.create_scene;
	}

	@Override
	@BusSignal
	public void AddDevice(String input) throws BusException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	@BusSignalHandler(iface="com.verik.bus.VENUS_BOARD", signal="AddDeviceCB")
	public void AddDeviceCB(String input) throws BusException {
		Log.d("VENUS", "Received AddDeviceCB: "+input);
		StringTokenizer stk = new StringTokenizer(input,":");
		
		if(stk.countTokens() > 1){
			subDevice = new VenusSubDevice(Integer.parseInt(stk.nextToken()),stk.nextToken(),Integer.parseInt(stk.nextToken()),Integer.parseInt(stk.nextToken()),venusID);
			Log.d("VENUS","Device info: "+subDevice.toString());
			if(!isContain(subDevice)){
				AllJoynManager.subDevices.add(subDevice);
			}
		}
		((SampleAppActivity) getActivity()).handler.post(new Runnable() {
			
			@Override
			public void run() {
				updateSubDeviceList();
				
			}
		});
	}

	@Override
	@BusSignal
	public void GetDeviceList(String input) throws BusException {
		// TODO Auto-generated method stub
	}

	@Override
	@BusSignal
	public void GetDeviceListCB(String input) throws BusException {
		// TODO Auto-generated method stub
		
	}

	@Override
	@BusSignal
	public void SendCommand(String input1, String input2) throws BusException {
		// TODO Auto-generated method stub
		
	}

	@Override
	@BusSignal
	public void SendCommandCB(String input) throws BusException {
		// TODO Auto-generated method stub
		
	}
	
	private boolean isContain(VenusSubDevice device){
		
		for(int i=0; i<AllJoynManager.subDevices.size();i++){
			if(AllJoynManager.subDevices.get(i).getId() == device.getId()){
				return true;
			}
		}
		
		return false;
	}

	@Override
	@BusSignal
	public void RemoveDevice(String input) throws BusException {
		// TODO Auto-generated method stub
		
	}

	@Override
	@BusSignal
	public void RemoveDeviceCB(String input) throws BusException {
		// TODO Auto-generated method stub
		
	}

	@Override
	@BusSignal
	public void AddGroup(String input1, String input2) throws BusException {
		// TODO Auto-generated method stub
		
	}

	@Override
	@BusSignal
	public void AddGroupCB(String input) throws BusException {
		// TODO Auto-generated method stub
		
	}

	@Override
	@BusSignal
	public void RemoveGroup(String input) throws BusException {
		// TODO Auto-generated method stub
		
	}

	@Override
	@BusSignal
	public void RemoveGroupCB(String input) throws BusException {
		// TODO Auto-generated method stub
		
	}

	@Override
	@BusSignal
	public void RemoveNode(String input1, String input2) throws BusException {
		// TODO Auto-generated method stub
		
	}

	@Override
	@BusSignal
	public void RemoveNodeCB(String input) throws BusException {
		// TODO Auto-generated method stub
		
	}

	@Override
	@BusSignal
	public void SendGroupCommand(String input1, String input2)
			throws BusException {
		// TODO Auto-generated method stub
		
	}

	@Override
	@BusSignal
	public void SendGroupCommandCB(String input) throws BusException {
		// TODO Auto-generated method stub
		
	}

	@Override
	@BusSignal
	public void SendNotify(String input) throws BusException {
		// TODO Auto-generated method stub
		
	}
}
