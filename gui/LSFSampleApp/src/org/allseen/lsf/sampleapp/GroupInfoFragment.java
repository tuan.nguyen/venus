/*
 * Copyright (c) 2014, AllSeen Alliance. All rights reserved.
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package org.allseen.lsf.sampleapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.verik.venus.sample.VenusGroupDevice;
import com.verik.venus.sample.VenusSubDevice;

public class GroupInfoFragment extends DimmableItemInfoFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(((SampleAppActivity) getActivity()).isVenusGroup){
        	String itemID = key;
        	
            view = inflater.inflate(getLayoutID(), container, false);
            statusView = view.findViewById(R.id.infoStatusRow);
            
            // power button
            ImageButton powerButton = (ImageButton)statusView.findViewById(R.id.statusButtonPower);
            powerButton.setImageResource(R.drawable.launcher_icon);
            powerButton.setTag(itemID);
//            powerButton.setOnClickListener(this);

            // item name
            TextView nameLabel = (TextView)statusView.findViewById(R.id.statusLabelName);
            nameLabel.setClickable(true);
            nameLabel.setOnClickListener(this);

            TextView nameText = (TextView)statusView.findViewById(R.id.statusTextName);
            nameText.setClickable(true);
//            nameText.setOnClickListener(this);
            
            itemType = SampleAppActivity.Type.GROUP;
            itemModels = ((SampleAppActivity)getActivity()).groupModels;

            ((TextView)statusView.findViewById(R.id.statusLabelName)).setText(R.string.label_group_name);

            // displays members of this group
            TextView membersLabel = (TextView)(view.findViewById(R.id.groupInfoMembers).findViewById(R.id.nameValueNameText));
            membersLabel.setText(R.string.group_info_label_members);
            membersLabel.setClickable(true);
            membersLabel.setOnClickListener(this);

            TextView membersValue = (TextView)(view.findViewById(R.id.groupInfoMembers).findViewById(R.id.nameValueValueText));
            membersValue.setClickable(true);
            membersValue.setOnClickListener(this);
            
            updateInfoFields(AllJoynManager.getVenusGroupDeviceByID(itemID));

            return view;
        } else {
        	View view = super.onCreateView(inflater, container, savedInstanceState);
            String groupID = key;
            
            itemType = SampleAppActivity.Type.GROUP;
            itemModels = ((SampleAppActivity)getActivity()).groupModels;

            ((TextView)statusView.findViewById(R.id.statusLabelName)).setText(R.string.label_group_name);

            // displays members of this group
            TextView membersLabel = (TextView)(view.findViewById(R.id.groupInfoMembers).findViewById(R.id.nameValueNameText));
            membersLabel.setText(R.string.group_info_label_members);
            membersLabel.setClickable(true);
            membersLabel.setOnClickListener(this);

            TextView membersValue = (TextView)(view.findViewById(R.id.groupInfoMembers).findViewById(R.id.nameValueValueText));
            membersValue.setClickable(true);
            membersValue.setOnClickListener(this);

            updateInfoFields(((SampleAppActivity)getActivity()).groupModels.get(groupID));

            return view;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        ((SampleAppActivity)getActivity()).updateActionBar(R.string.title_group_info, false, false, false, false, true);
    }

    @Override
    public void onClick(View view) {
        int viewID = view.getId();
        if (viewID == R.id.nameValueNameText || viewID == R.id.nameValueValueText) {
            if ((parent != null) && (!AllLampsDataModel.ALL_LAMPS_GROUP_ID.equals(key))) {
                GroupDataModel groupModel = ((SampleAppActivity)getActivity()).groupModels.get(key);

                if (groupModel != null) {
                    ((SampleAppActivity)getActivity()).pendingGroupModel = new GroupDataModel(groupModel);
                    ((PageMainContainerFragment)parent).showSelectMembersChildFragment();
                }
                
                if(((SampleAppActivity) getActivity()).isVenusGroup){
                	Log.d("VENUS", "Handle onclick sub device name by open select layout with id: " + key);
                	((SampleAppActivity)getActivity()).pendingVenusGroup = new VenusGroupDevice();
                	((SampleAppActivity)getActivity()).pendingVenusGroup.setId(Integer.parseInt(key.replace("G", "")));
                	
                	for(VenusGroupDevice group : AllJoynManager.venusGroups){
                		if(group.getId() == Integer.parseInt(key.replace("G", ""))){
                			((SampleAppActivity)getActivity()).pendingVenusGroup.setMembers(group.getMembers());
                		}
                	}
                	
                	((PageMainContainerFragment)parent).showSelectMembersChildFragment();
                }
            }
        } else {
            super.onClick(view);
        }
    }

    public void updateInfoFields(GroupDataModel groupModel) {
        if (groupModel.id.equals(key)) {
            stateAdapter.setCapability(groupModel.getCapability());
            super.updateInfoFields(groupModel);

            String details = Util.createMemberNamesString((SampleAppActivity)getActivity(), groupModel.members, ", ", R.string.group_info_members_none);
            TextView membersValue = (TextView)(view.findViewById(R.id.groupInfoMembers).findViewById(R.id.nameValueValueText));

            if (details != null && !details.isEmpty()) {
                membersValue.setText(details);
            }
            membersValue.setOnClickListener(this);
        }
    }
    
    public void updateInfoFields(VenusGroupDevice groupDevice){
    	setImageButtonBackgroundResource(statusView, R.id.statusButtonPower, android.R.color.transparent);
    	setTextViewValue(statusView, R.id.statusTextName, groupDevice.getName(), 0);
    	String details = "";
    	
    	for(VenusSubDevice lamp : groupDevice.getMembers()){
    		details += lamp.getName() + ", ";
    	}
    	TextView membersValue = (TextView)(view.findViewById(R.id.groupInfoMembers).findViewById(R.id.nameValueValueText));
    	membersValue.setText(details);
    }

    @Override
    protected int getLayoutID() {
    	if(((SampleAppActivity) getActivity()).isVenusGroup){
    		return R.layout.fragment_venus_group_info;
    	} else{
    		return R.layout.fragment_group_info;
    	}
    }

    @Override
    protected int getColorTempMin() {
        SampleAppActivity activity = (SampleAppActivity)getActivity();
        GroupDataModel groupModel = activity.groupModels.get(key);

        return groupModel != null ? groupModel.viewColorTempMin : DimmableItemScaleConverter.VIEW_COLORTEMP_MIN;
    }

    @Override
    protected int getColorTempSpan() {
        SampleAppActivity activity = (SampleAppActivity)getActivity();
        GroupDataModel groupModel = activity.groupModels.get(key);

        return groupModel != null ? groupModel.viewColorTempMax - groupModel.viewColorTempMin : DimmableItemScaleConverter.VIEW_COLORTEMP_SPAN;
    }

    @Override
    protected void onHeaderClick() {
        if (!AllLampsDataModel.ALL_LAMPS_GROUP_ID.equals(key)) {
            SampleAppActivity activity = (SampleAppActivity)getActivity();
            GroupDataModel groupModel = activity.groupModels.get(key);

            activity.showItemNameDialog(R.string.title_group_rename, new UpdateGroupNameAdapter(groupModel, (SampleAppActivity) getActivity()));
        }
    }
}
