/*
 * Copyright (c) 2014, AllSeen Alliance. All rights reserved.
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package org.allseen.lsf.sampleapp;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.SignalEmitter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TableRow;
import android.widget.TextView;

import com.verik.venus.sample.VenusBusObjectInf;
import com.verik.venus.sample.VenusGroupDevice;
import com.verik.venus.sample.VenusSubDevice;

public abstract class DimmableItemTableFragment
    extends ScrollableTableFragment
    implements
        View.OnClickListener,
        SeekBar.OnSeekBarChangeListener{
	
    protected abstract int getInfoButtonImageID();
    protected abstract Fragment getInfoFragment();
    
    public <T> TableRow insertDimmableItemRow(Context context, String itemID, Comparable<T> tag, boolean powerOn, boolean uniformPower, String name, long modelBrightness, boolean uniformBrightness, int infoBG) {
        return insertDimmableItemRow(
            context,
            (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE),
            itemID,
            tag,
            powerOn,
            uniformPower,
            name,
            modelBrightness,
            uniformBrightness,
            infoBG,
            true);
    }

    public <T> TableRow insertDimmableItemRow(Context context, String itemID, Comparable<T> tag, boolean powerOn, boolean uniformPower, String name, long modelBrightness, boolean uniformBrightness, int infoBG, boolean enabled) {
        return insertDimmableItemRow(
            context,
            (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE),
            itemID,
            tag,
            powerOn,
            uniformPower,
            name,
            modelBrightness,
            uniformBrightness,
            infoBG,
            enabled);
    }

    public <T> TableRow insertDimmableItemRow(Context context, LayoutInflater inflater, String itemID, Comparable<T> tag, boolean powerOn, boolean uniformPower, String name, long modelBrightness, boolean uniformBrightness, int infoBG, boolean enabled) {
        Log.d(SampleAppActivity.TAG, "insertDimmableItemRow(): " + itemID + ", " + tag + ", " + name);

        final boolean isEnabled = enabled;

        TableRow tableRow = (TableRow)table.findViewWithTag(itemID);

        if (tableRow == null) {
            tableRow = new TableRow(context);

            inflater.inflate(R.layout.view_dimmable_item_row, tableRow);

            ImageButton powerButton = (ImageButton)tableRow.findViewById(R.id.dimmableItemButtonPower);
            powerButton.setTag(itemID);
            powerButton.setBackgroundResource(uniformPower ? (powerOn ? R.drawable.power_button_on : R.drawable.power_button_off) : R.drawable.power_button_mix);
            powerButton.setOnClickListener(this);

            ((TextView)tableRow.findViewById(R.id.dimmableItemRowText)).setText(name);

            SeekBar seekBar = (SeekBar)tableRow.findViewById(R.id.dimmableItemRowSlider);
            seekBar.setProgress(DimmableItemScaleConverter.convertBrightnessModelToView(modelBrightness));
            seekBar.setTag(itemID);
            seekBar.setSaveEnabled(false);
            seekBar.setOnSeekBarChangeListener(this);
            seekBar.setThumb(getResources().getDrawable(uniformBrightness ? R.drawable.slider_thumb_normal : R.drawable.slider_thumb_midstate));
            seekBar.setEnabled(isEnabled);

            ImageButton infoButton = (ImageButton)tableRow.findViewById(R.id.dimmableItemButtonMore);
            infoButton.setImageResource(getInfoButtonImageID());
            infoButton.setTag(itemID);
            infoButton.setOnClickListener(this);
            if (infoBG != 0) {
                infoButton.setBackgroundColor(infoBG);
            }

            tableRow.setTag(itemID);
            TableSorter.insertSortedTableRow(table, tableRow, tag);
        } else {
            ((ImageButton)tableRow.findViewById(R.id.dimmableItemButtonPower)).setBackgroundResource(uniformPower ? (powerOn ? R.drawable.power_button_on : R.drawable.power_button_off) : R.drawable.power_button_mix);
            ((TextView)tableRow.findViewById(R.id.dimmableItemRowText)).setText(name);

            SeekBar seekBar = (SeekBar)tableRow.findViewById(R.id.dimmableItemRowSlider);
            seekBar.setProgress(DimmableItemScaleConverter.convertBrightnessModelToView(modelBrightness));
            seekBar.setThumb(getResources().getDrawable(uniformBrightness ? R.drawable.slider_thumb_normal : R.drawable.slider_thumb_midstate));
            seekBar.setEnabled(isEnabled);

            if (infoBG != 0) {
                ((ImageButton)tableRow.findViewById(R.id.dimmableItemButtonMore)).setBackgroundColor(infoBG);
            }

            TableSorter.updateSortedTableRow(table, tableRow, tag);
        }

        tableRow.setClickable(true);
        tableRow.setOnClickListener(this);
        ((SampleAppActivity)getActivity()).setTabTitles();
        return tableRow;
    }
    
    public <T> TableRow insertVenusLampItemRow(Context context, String itemID, Comparable<T> tag, boolean powerOn, boolean uniformPower, String name, long modelBrightness, boolean uniformBrightness, int infoBG, boolean enabled) {
        return insertVenusLampItemRow(
            context,
            (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE),
            "Z"+itemID,
            tag,
            powerOn,
            uniformPower,
            name,
            modelBrightness,
            uniformBrightness,
            infoBG,
            enabled);
    }
	
	public <T> TableRow insertVenusLampItemRow(Context context, LayoutInflater inflater, String itemID, Comparable<T> tag, boolean powerOn, boolean uniformPower, String name, long modelBrightness, boolean uniformBrightness, int infoBG, boolean enabled) {
        final boolean isEnabled = enabled;

        TableRow tableRow = (TableRow)table.findViewWithTag(itemID);

        if (tableRow == null) {
        	Log.d("VENUS","Start to add new lamp");
            tableRow = new TableRow(context);

            inflater.inflate(R.layout.view_dimmable_item_row, tableRow);

            ImageButton powerButton = (ImageButton)tableRow.findViewById(R.id.dimmableItemButtonPower);
            powerButton.setTag(itemID);
            powerButton.setBackgroundResource(powerOn ? R.drawable.power_button_on : R.drawable.power_button_off);
            powerButton.setOnClickListener(this);

            ((TextView)tableRow.findViewById(R.id.dimmableItemRowText)).setText(name);

            if(isDimmable(itemID.replace("Z", ""))){
            	SeekBar seekBar = (SeekBar)tableRow.findViewById(R.id.dimmableItemRowSlider);
                seekBar.setTag(itemID);
                seekBar.setSaveEnabled(false);
                seekBar.setOnSeekBarChangeListener(this);
                seekBar.setThumb(getResources().getDrawable(uniformBrightness ? R.drawable.slider_thumb_normal : R.drawable.slider_thumb_midstate));
                seekBar.setMax(99);
                seekBar.setProgress((int)modelBrightness);
                seekBar.setEnabled(isEnabled);
            }else{
            	SeekBar seekBar = (SeekBar)tableRow.findViewById(R.id.dimmableItemRowSlider);
                seekBar.setTag(itemID);
                seekBar.setSaveEnabled(false);
                seekBar.setOnSeekBarChangeListener(this);
                seekBar.setThumb(getResources().getDrawable(uniformBrightness ? R.drawable.slider_thumb_normal : R.drawable.slider_thumb_midstate));
                seekBar.setMax(99);
                seekBar.setProgress((int)modelBrightness);
                seekBar.setEnabled(false);
            }

            ImageButton infoButton = (ImageButton)tableRow.findViewById(R.id.dimmableItemButtonMore);
            infoButton.setImageResource(getInfoButtonImageID());
            infoButton.setTag(itemID);
            infoButton.setBackgroundColor(Color.parseColor("#F5DA81"));

            tableRow.setTag(itemID);
            char prefix = 'V';
            LightingItemSortableTag venusTag = new LightingItemSortableTag(itemID, prefix, name);
            Log.d("VENUS", "insertVenusLampItemRow(): " + itemID + ", " + venusTag + ", " + name);
            TableSorter.updateSortedTableRow(table, tableRow, venusTag);
        } else {
            ((ImageButton)tableRow.findViewById(R.id.dimmableItemButtonPower)).setBackgroundResource(powerOn ? R.drawable.power_button_on : R.drawable.power_button_off);
            ((TextView)tableRow.findViewById(R.id.dimmableItemRowText)).setText(name);

            if(isDimmable(itemID.replace("Z", ""))){
            	SeekBar seekBar = (SeekBar)tableRow.findViewById(R.id.dimmableItemRowSlider);
                seekBar.setProgress((int)modelBrightness);
                seekBar.setThumb(getResources().getDrawable(uniformBrightness ? R.drawable.slider_thumb_normal : R.drawable.slider_thumb_midstate));
                seekBar.setEnabled(isEnabled);
            }else{
            	SeekBar seekBar = (SeekBar)tableRow.findViewById(R.id.dimmableItemRowSlider);
                seekBar.setProgress((int)modelBrightness);
                seekBar.setThumb(getResources().getDrawable(uniformBrightness ? R.drawable.slider_thumb_normal : R.drawable.slider_thumb_midstate));
                seekBar.setEnabled(false);
            }
            
            
            ((ImageButton)tableRow.findViewById(R.id.dimmableItemButtonMore)).setBackgroundColor(Color.parseColor("#F5DA81"));
            
            char prefix = 'V';
            LightingItemSortableTag venusTag = new LightingItemSortableTag(itemID, prefix, name);
            Log.d("VENUS", "insertVenusLampItemRow(): " + itemID + ", " + venusTag + ", " + name);
            TableSorter.updateSortedTableRow(table, tableRow, venusTag);
        }

        tableRow.setClickable(true);
        tableRow.setOnClickListener(this);
        ((SampleAppActivity)getActivity()).setTabTitles();
        return tableRow;
    }
	
	public <T> TableRow insertVenusGroupItemRow(Context context, String itemID, boolean powerOn, boolean uniformPower, String name, long modelBrightness, boolean enabled){
		return insertVenusGroupItemRow(context,
	            (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE),
	            itemID,
	            powerOn,
	            uniformPower,
	            name,
	            modelBrightness,
	            enabled);
	}
	
	public <T> TableRow insertVenusGroupItemRow(Context context, LayoutInflater inflater, String itemID, boolean powerOn, boolean uniformPower, String name, long modelBrightness, boolean enabled){
		TableRow tableRow = (TableRow)table.findViewWithTag(itemID);
		
		if (tableRow == null) {
        	Log.d("VENUS","Start to add new group");
            tableRow = new TableRow(context);

            inflater.inflate(R.layout.view_dimmable_item_row, tableRow);

            ImageButton powerButton = (ImageButton)tableRow.findViewById(R.id.dimmableItemButtonPower);
            powerButton.setTag(itemID);
            powerButton.setImageResource(R.drawable.launcher_icon);
            powerButton.setClickable(false);

            ((TextView)tableRow.findViewById(R.id.dimmableItemRowText)).setText(name);

            SeekBar seekBar = (SeekBar)tableRow.findViewById(R.id.dimmableItemRowSlider);
            seekBar.setTag(itemID);
            seekBar.setSaveEnabled(false);
            seekBar.setOnSeekBarChangeListener(this);
            seekBar.setThumb(getResources().getDrawable(R.drawable.slider_thumb_normal));
            seekBar.setMax(99);
            seekBar.setProgress((int)modelBrightness);
            
            if(isGroupDimmable(itemID)){
            	seekBar.setEnabled(true);
            }else{
            	seekBar.setEnabled(false);
            }
            
            ImageButton infoButton = (ImageButton)tableRow.findViewById(R.id.dimmableItemButtonMore);
            infoButton.setImageResource(getInfoButtonImageID());
            infoButton.setTag(itemID);
            infoButton.setOnClickListener(this);
            
            tableRow.setTag(itemID);
            char prefix = 'G';
            LightingItemSortableTag venusTag = new LightingItemSortableTag(itemID, prefix, name);
            Log.d("VENUS", "insertVenusLampItemRow(): " + itemID + ", " + venusTag + ", " + name);
            TableSorter.updateSortedTableRow(table, tableRow, venusTag);
        } else {
            ((ImageButton)tableRow.findViewById(R.id.dimmableItemButtonPower)).setImageResource(R.drawable.launcher_icon);
            ((ImageButton)tableRow.findViewById(R.id.dimmableItemButtonPower)).setClickable(false);
            ((TextView)tableRow.findViewById(R.id.dimmableItemRowText)).setText(name);

            SeekBar seekBar = (SeekBar)tableRow.findViewById(R.id.dimmableItemRowSlider);
            seekBar.setProgress((int)modelBrightness);
            seekBar.setThumb(getResources().getDrawable(R.drawable.slider_thumb_normal));
            if(isGroupDimmable(itemID)){
            	seekBar.setEnabled(true);
            }else{
            	seekBar.setEnabled(false);
            }
            
            char prefix = 'G';
            LightingItemSortableTag venusTag = new LightingItemSortableTag(itemID, prefix, name);
            Log.d("VENUS", "insertVenusLampItemRow(): " + itemID + ", " + venusTag + ", " + name);
            TableSorter.updateSortedTableRow(table, tableRow, venusTag);
        }

        tableRow.setClickable(true);
        tableRow.setOnClickListener(this);
        ((SampleAppActivity)getActivity()).setTabTitles();
		
		return tableRow;
	}

    @Override
    public void onClick(final View button) {
        int buttonID = button.getId();
        Log.d(SampleAppActivity.TAG, "onClick() buttonId: "+button.getTag().toString()+"is more button: "+(button.getId() == R.id.dimmableItemButtonMore));

        if (parent != null) {
            if (buttonID == R.id.dimmableItemButtonPower) {
            	if(button.getTag().toString().contains("Z")){
            		Log.d("VENUS", "clicked on power button: "+button.getTag().toString());
            		
            		final String subDeviceID = button.getTag().toString();
            		
            		((SampleAppActivity)getActivity()).handler.post(new Runnable() {
						
						@Override
						public void run() {
							if(AllJoynManager.SESSION_ID != null){
								AllJoynManager.signalEmitter = new SignalEmitter((SampleAppActivity)getActivity(), AllJoynManager.SESSION_ID.value, SignalEmitter.GlobalBroadcast.Off);
								
								AllJoynManager.busObjectInf = AllJoynManager.signalEmitter.getInterface(VenusBusObjectInf.class);
								
								if(AllJoynManager.busObjectInf != null){
									Log.d("VENUS","Start to send signal");
									try {
										if(getStatus(subDeviceID) > 0){
											button.setBackgroundResource(R.drawable.power_button_off);
											updateStatus(subDeviceID,AllJoynManager.OFF);
											AllJoynManager.busObjectInf.SendCommand(subDeviceID.replace("Z", ""),AllJoynManager.OFF+"");
										}else{
											button.setBackgroundResource(R.drawable.power_button_on);
											updateStatus(subDeviceID,AllJoynManager.ON);
											AllJoynManager.busObjectInf.SendCommand(subDeviceID.replace("Z", ""),AllJoynManager.ON+"");
										}
										Log.d("VENUS", "Send signal successful.");
									} catch (BusException e) {
										Log.d("VENUS", "Error when sending signal: "+e.getMessage());
									}
								}
							}
						}
					});
            	} else {
            		((SampleAppActivity)getActivity()).togglePower(type, button.getTag().toString());
            	}
            } else if (buttonID == R.id.dimmableItemButtonMore) {
            	Log.d("VENUS","Clicked on more button");
                ((SampleAppActivity)getActivity()).onItemButtonMore(parent, type, button, button.getTag().toString(), null);
            } else if (!((SeekBar)button.findViewById(R.id.dimmableItemRowSlider)).isEnabled()) {
            	
            	((SampleAppActivity)getActivity()).showToast(R.string.no_support_dimmable);
            }
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int brightness, boolean fromUser) {
        //AJSI-291: UI Slider behavior change (from continuous updating to updating when finger is lifted)
        /*if (parent != null && fromUser) {
            ((SampleAppActivity)getActivity()).setBrightness(type, seekBar.getTag().toString(), seekBar.getProgress());
        }*/
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // Currently nothing to do
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        if (parent != null) {
        	Log.d("VENUS", "changed on seek bar: "+seekBar.getProgress()+" on device "+seekBar.getTag().toString());
        	
        	final String subDeviceID = seekBar.getTag().toString();
        	final int status = seekBar.getProgress();
        	
        	((SampleAppActivity)getActivity()).handler.post(new Runnable() {
				
				@Override
				public void run() {
					if(subDeviceID.contains("Z")){
						if(getStatus(subDeviceID) > 0){
							if(AllJoynManager.SESSION_ID != null){
								AllJoynManager.signalEmitter = new SignalEmitter((SampleAppActivity)getActivity(), AllJoynManager.SESSION_ID.value, SignalEmitter.GlobalBroadcast.Off);
								
								AllJoynManager.busObjectInf = AllJoynManager.signalEmitter.getInterface(VenusBusObjectInf.class);
								
								if(AllJoynManager.busObjectInf != null){
									try{
										AllJoynManager.busObjectInf.SendCommand(subDeviceID.replace("Z", ""), status+"");
										updateStatus(subDeviceID,status);
									}catch (BusException e){
										Log.d("VENUS","Change dimmable error: "+e.getMessage());
									}
								}
							}
						}
					} else if(subDeviceID.contains("G")){
						Log.d("VENUS","Change brightness Group.");
						AllJoynManager.signalEmitter = new SignalEmitter((SampleAppActivity)getActivity(), AllJoynManager.SESSION_ID.value, SignalEmitter.GlobalBroadcast.Off);
						
						AllJoynManager.busObjectInf = AllJoynManager.signalEmitter.getInterface(VenusBusObjectInf.class);
						
						if(AllJoynManager.busObjectInf != null){
							try{
								AllJoynManager.busObjectInf.SendGroupCommand(subDeviceID.replace("G", ""), status+"");
								updateStatus(subDeviceID,status);
							}catch (BusException e){
								Log.d("VENUS","Change dimmable error: "+e.getMessage());
							}
						}
					}
				}
			});
        }
    }
    
    private int getStatus(String deviceId){
    	if(deviceId.contains("G")){
    		for(int i=0; i< AllJoynManager.venusGroups.size(); i++){
    			if(AllJoynManager.venusGroups.get(i).getId() == Integer.parseInt(deviceId.replace("G", ""))){
    				return AllJoynManager.venusGroups.get(i).getStatus();
    			}
    		}
    	} else if (deviceId.contains("Z")){
    		for(int i=0;i<AllJoynManager.subDevices.size();i++){
        		if(AllJoynManager.subDevices.get(i).getId() == Integer.parseInt(deviceId.replace("Z", ""))){
        			return AllJoynManager.subDevices.get(i).getStatus();
        		}
        	}
    	}
    	return 0;
    }
    
    private void updateStatus(String deviceId, int status){
    	if(deviceId.contains("G")){
    		for(int i=0;i<AllJoynManager.venusGroups.size(); i++){
    			if(AllJoynManager.venusGroups.get(i).getId() == Integer.parseInt(deviceId.replace("G", ""))){
    				AllJoynManager.venusGroups.get(i).setStatus(status);
    			}
    		}
    	}else if(deviceId.contains("Z")){
    		for(int i=0;i<AllJoynManager.subDevices.size();i++){
        		if(AllJoynManager.subDevices.get(i).getId() == Integer.parseInt(deviceId.replace("Z", ""))){
        			AllJoynManager.subDevices.get(i).setStatus(status);
        		}
        	}
    	}
    }
    
    private boolean isDimmable(String deviceId){
    	for(int i=0; i<AllJoynManager.subDevices.size(); i++){
    		if(AllJoynManager.subDevices.get(i).getId() == Integer.parseInt(deviceId.replace("Z", "")) 
    				&& AllJoynManager.subDevices.get(i).getType() == AllJoynManager.MULTIPLE_SWITCH){
    			return true;
    		}
    	}
    	return false;
    }
    
    private boolean isGroupDimmable(String groupId){
    	boolean rs = true;
    	
    	for(VenusGroupDevice group : AllJoynManager.venusGroups){
    		if(group.getId() == Integer.parseInt(groupId.replace("G", ""))){
    			for(VenusSubDevice device : group.getMembers()){
    				rs = rs && (device.getType() == AllJoynManager.MULTIPLE_SWITCH);
        		}
    		}
    	}
    	
    	return rs;
    }

    public abstract void addElement(String id);
}
