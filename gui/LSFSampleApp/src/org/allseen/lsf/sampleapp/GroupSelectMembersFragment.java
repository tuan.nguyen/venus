/*
 * Copyright (c) 2014, AllSeen Alliance. All rights reserved.
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package org.allseen.lsf.sampleapp;

import java.util.ArrayList;
import java.util.List;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.SignalEmitter;
import org.allseen.lsf.LampGroup;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.verik.venus.sample.VenusBusObjectInf;
import com.verik.venus.sample.VenusDataModel;
import com.verik.venus.sample.VenusSubDevice;

public class GroupSelectMembersFragment extends SelectMembersFragment {

    public GroupSelectMembersFragment() {
        super(R.string.label_group);
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
    	View root = super.onCreateView(inflater, container, savedInstanceState);
    	
    	if(((SampleAppActivity)getActivity()).pendingVenusGroup.getMembers() != null){
    		Log.d("VENUS","update group items.");
    		if(AllJoynManager.subDevices.size() > 0){
	   	       	 for(int i=0;i<AllJoynManager.subDevices.size();i++){
	   	       		 VenusDataModel venusLamp = new VenusDataModel(AllJoynManager.subDevices.get(i).getId()+"");
	   	       		 boolean isSelected = false;
	   	       		 
	   	       		 for(VenusSubDevice device : ((SampleAppActivity)getActivity()).pendingVenusGroup.getMembers()){
	   	       			 if(device.getId() == AllJoynManager.subDevices.get(i).getId()){
	   	       				 isSelected = true;
	   	       			 }
	   	       		 }
	   	       		 
	   	       		 updateSelectableItemRow(inflater, root, venusLamp.id, venusLamp.tag, R.drawable.group_lightbulb_icon, AllJoynManager.subDevices.get(i).getName(), isSelected);
	   	       	 }
    		}
    	} else {
    		if(AllJoynManager.subDevices.size() > 0){
    	       	 for(int i=0;i<AllJoynManager.subDevices.size();i++){
    	       		 VenusDataModel venusLamp = new VenusDataModel(AllJoynManager.subDevices.get(i).getId()+"");
    	       		 updateSelectableItemRow(inflater, root, venusLamp.id, venusLamp.tag, R.drawable.group_lightbulb_icon, AllJoynManager.subDevices.get(i).getName(), false);
    	       	 }
    	    }
    	}
    	return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        SampleAppActivity activity = (SampleAppActivity)getActivity();
        
        if(activity.isVenusGroup){
        	activity.updateActionBar(activity.pendingVenusGroup.getId() == 0 ? R.string.title_group_add : R.string.title_group_edit, false, false, false, true, true);
        } else {
        	activity.updateActionBar(activity.pendingGroupModel.id.isEmpty() ? R.string.title_group_add : R.string.title_group_edit, false, false, false, true, true);
        }
    }

    @Override
    protected String getHeaderText() {
        return getString(R.string.group_select_members);
    }

    @Override
    protected LampGroup getPendingMembers() {
        return ((SampleAppActivity)getActivity()).pendingGroupModel.members;
    }

    @Override
    protected String getPendingItemID() {
    	if(((SampleAppActivity)getActivity()).isVenusGroup){
    		Log.d("VENUS","Current venus group id: "+((SampleAppActivity)getActivity()).pendingVenusGroup.getId());
    		return "G"+((SampleAppActivity)getActivity()).pendingVenusGroup.getId();
    	}
        return ((SampleAppActivity)getActivity()).pendingGroupModel.id;
    }

   @Override
    protected void processSelection(SampleAppActivity activity, List<String> lampIDs, List<String> groupIDs, List<String> sceneIDs, List<String> subDeviceIDs) {
	   if(activity.pendingVenusGroup.getMembers() != null){
		   Log.d("VENUS","Start to updated group: G"+activity.pendingVenusGroup.getId());
		   AllJoynManager.signalEmitter = new SignalEmitter((SampleAppActivity)getActivity(), AllJoynManager.SESSION_ID.value, SignalEmitter.GlobalBroadcast.Off);
			
			AllJoynManager.busObjectInf = AllJoynManager.signalEmitter.getInterface(VenusBusObjectInf.class);
			
			ArrayList<VenusSubDevice> members = new ArrayList<VenusSubDevice>();
	        
	        for(String deviceID : subDeviceIDs){
	        	for(VenusSubDevice device : AllJoynManager.subDevices){
	            	if(device.getId() == Integer.parseInt(deviceID)){
	            		members.add(device);
	            	}
	            }
	        }
			
			String memberIds = "";
			
			for(VenusSubDevice subDevice : members){
				memberIds += subDevice.getId()+":";
			}
			
			if(AllJoynManager.busObjectInf != null){
				Log.d("VENUS","Send add group signal");
				try {
					AllJoynManager.busObjectInf.AddGroup( activity.pendingVenusGroup.getId()+"", memberIds.substring(0, memberIds.length()-1));
				} catch (BusException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			for(int i=0; i<AllJoynManager.venusGroups.size(); i++){
				if(AllJoynManager.venusGroups.get(i).getId() == activity.pendingVenusGroup.getId()){
					AllJoynManager.venusGroups.get(i).setMembers(members);
				}
			}
		   
	   } else {
		   Log.d("VENUS","Start to add new group.");
		    activity.pendingGroupModel.members.setLamps(lampIDs.toArray(new String[lampIDs.size()]));
	        activity.pendingGroupModel.members.setLampGroups(groupIDs.toArray(new String[groupIDs.size()]));
	        
	        ArrayList<VenusSubDevice> members = new ArrayList<VenusSubDevice>();
	        
	        for(String deviceID : subDeviceIDs){
	        	for(VenusSubDevice device : AllJoynManager.subDevices){
	            	if(device.getId() == Integer.parseInt(deviceID)){
	            		members.add(device);
	            	}
	            }
	        }
	        
	        //add for venus - nga.le
	        activity.pendingVenusGroup.setMembers(members);
	        activity.pendingVenusGroup.setId(AllJoynManager.venusGroups.size()+1);
	        activity.pendingVenusGroup.setControllerId(members.get(0).getControllerId());
	        AllJoynManager.venusGroups.add(activity.pendingVenusGroup);
	        
	        AllJoynManager.signalEmitter = new SignalEmitter((SampleAppActivity)getActivity(), AllJoynManager.SESSION_ID.value, SignalEmitter.GlobalBroadcast.Off);
			
			AllJoynManager.busObjectInf = AllJoynManager.signalEmitter.getInterface(VenusBusObjectInf.class);
			
			String memberIds = "";
			
			for(VenusSubDevice subDevice : members){
				memberIds += subDevice.getId()+":";
			}
			
			if(AllJoynManager.busObjectInf != null){
				Log.d("VENUS","Send add group signal");
				try {
					AllJoynManager.busObjectInf.AddGroup( activity.pendingVenusGroup.getId()+"", memberIds.substring(0, memberIds.length()-1));
				} catch (BusException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
	        
	        if (activity.pendingGroupModel.id.isEmpty()) {
	            AllJoynManager.groupManager.createLampGroup(activity.pendingGroupModel.members, activity.pendingGroupModel.getName(), SampleAppActivity.LANGUAGE);
	        } else {
	            AllJoynManager.groupManager.updateLampGroup(activity.pendingGroupModel.id, activity.pendingGroupModel.members);
	        }
	   }
    }

@Override
protected int getMixedSelectionMessageID() {
    return R.string.mixing_lamp_types_message_group;
}

@Override
protected int getMixedSelectionPositiveButtonID() {
    return R.string.create_group;
}
}
