/*
 * Copyright (c) 2014, AllSeen Alliance. All rights reserved.
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package org.allseen.lsf.sampleapp;

import android.content.Context;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class ScrollableTableFragment extends PageFrameChildFragment {
	protected TableLayout table = null;
	protected LinearLayout layout = null;
	protected SampleAppActivity.Type type;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fragment_scrollable_table, container,
				false);
		
		table = (TableLayout) view.findViewById(R.id.scrollableTable);
		layout = (LinearLayout) view.findViewById(R.id.scrollLayout);
		
		if(AllJoynManager.venusConnected){
			updateLoading(true);
		}else{
			updateLoading(false);
		}

		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	public void updateLoading(boolean isVenus) {
		Log.d("VENUS", "isVenus: "+isVenus+", AllJoynManager.controllerConnected: "+AllJoynManager.controllerConnected+", AllJoynManager.venusConnected: "+AllJoynManager.venusConnected);
		if (!AllJoynManager.controllerConnected && !isVenus) {
			Log.d("VENUS", "Case 1: Not remove loading view");
			// display loading controller service screen, hide the scroll table
			layout.findViewById(R.id.scrollLoadingView).setVisibility(
					View.VISIBLE);
			layout.findViewById(R.id.scrollScrollView).setVisibility(View.GONE);

			View loadingView = layout.findViewById(R.id.scrollLoadingView);

			String ssid = ((WifiManager) getActivity().getSystemService(
					Context.WIFI_SERVICE)).getConnectionInfo().getSSID();

			if (ssid == null) {
				ssid = "<unknown ssid>";
			}
			((TextView) loadingView.findViewById(R.id.loadingText1))
					.setText(getActivity().getText(R.string.no_controller)
							+ " " + ssid);
			((TextView) loadingView.findViewById(R.id.loadingText2))
					.setText(getActivity().getText(R.string.loading_controller));

		} else if (isVenus && !AllJoynManager.venusConnected) {
			Log.d("VENUS", "Case 2: Not remove loading view");
			// display loading controller service screen, hide the scroll table
			layout.findViewById(R.id.scrollLoadingView).setVisibility(
					View.VISIBLE);
			layout.findViewById(R.id.scrollScrollView).setVisibility(View.GONE);

			View loadingView = layout.findViewById(R.id.scrollLoadingView);

			String ssid = ((WifiManager) getActivity().getSystemService(
					Context.WIFI_SERVICE)).getConnectionInfo().getSSID();

			if (ssid == null) {
				ssid = "<unknown ssid>";
			}
			((TextView) loadingView.findViewById(R.id.loadingText1))
					.setText(getActivity().getText(R.string.no_venus)
							+ " " + ssid);
			((TextView) loadingView.findViewById(R.id.loadingText2))
					.setText(getActivity().getText(R.string.loading_venus));
		} else {
			Log.d("VENUS", "Remove loading view");
			// remove the loading view, and resume showing the scroll table
			layout.findViewById(R.id.scrollLoadingView)
					.setVisibility(View.GONE);
			layout.findViewById(R.id.scrollScrollView).setVisibility(
					View.VISIBLE);
		}
	}

	public void removeElement(String id) {
		final SampleAppActivity activity = (SampleAppActivity) getActivity();
		Log.d("VENUS","Going to remove elementID: "+id);
		final TableRow row = (TableRow) table.findViewWithTag(id);
		if (row != null) {
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					table.removeView(row);
					table.postInvalidate();
				}
			});
		}

		activity.setTabTitles();
	}
	
	public void updateElement(int id){
		final SampleAppActivity activity = (SampleAppActivity) getActivity();
		final TableRow row = (TableRow) table.findViewById(id);
		Log.d("VENUS","Element will be updated id "+id);
		if (row != null) {
			Log.d("VENUS","Start change UI for item "+id);
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					row.setBackgroundColor(Color.GRAY);
				}
			});
		}

		activity.setTabTitles();
	}
}
