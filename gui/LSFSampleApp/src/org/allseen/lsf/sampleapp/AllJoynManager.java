/*
 * Copyright (c) 2014, AllSeen Alliance. All rights reserved.
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package org.allseen.lsf.sampleapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;

import org.alljoyn.bus.BusAttachment;
import org.alljoyn.bus.Mutable;
import org.alljoyn.bus.SignalEmitter;
import org.allseen.lsf.ControllerClient;
import org.allseen.lsf.ControllerClientCallback;
import org.allseen.lsf.ControllerClientStatus;
import org.allseen.lsf.ControllerServiceManager;
import org.allseen.lsf.ControllerServiceManagerCallback;
import org.allseen.lsf.LampGroupManager;
import org.allseen.lsf.LampManager;
import org.allseen.lsf.LampManagerCallback;
import org.allseen.lsf.MasterSceneManager;
import org.allseen.lsf.MasterSceneManagerCallback;
import org.allseen.lsf.PresetManager;
import org.allseen.lsf.PresetManagerCallback;
import org.allseen.lsf.SceneManager;
import org.allseen.lsf.SceneManagerCallback;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.verik.venus.VenusManager;
import com.verik.venus.sample.VenusBusObjectInf;
import com.verik.venus.sample.VenusDevice;
import com.verik.venus.sample.VenusGroupDevice;
import com.verik.venus.sample.VenusSampleManagerCallback;
import com.verik.venus.sample.VenusSubDevice;

public class AllJoynManager {
    private static final String TAG = "TagAlljoynManager";
    public static final String VENUS_OBJECT_PATH = "/VEriK/Service";
    public static final String VENUS_INTERFACE = "com.verik.bus.VENUS_BOARD";
    public static final int ON = 255;
    public static final int OFF = 0;
    public static final int BLINK = 256;

    public static BusAttachment bus;
    public static SignalEmitter signalEmitter;
    public static ControllerClient controllerClient;
    public static ControllerServiceManager controllerServiceManager;
    public static LampManager lampManager;
    public static LampGroupManager groupManager;
    public static PresetManager presetManager;
    public static SceneManager sceneManager;
    public static VenusManager venusManager;
    public static MasterSceneManager masterSceneManager;
    public static SampleAppActivity activity;
    public static AboutManager aboutManager;
    
    public static final int BINARY_SWITCH = 16;
    public static final int MULTIPLE_SWITCH = 17;
    public static final short SERVICE_PORT = 25;
    public static final Mutable.IntegerValue SESSION_ID = new Mutable.IntegerValue();
    
    public static boolean controllerConnected = false;
    public static boolean controllerStarted = false;
    public static boolean venusConnected = false;
    public static VenusBusObjectInf busObjectInf = null;
    public static Map<String,VenusDevice> venuses = new HashMap<String, VenusDevice>();
    public static ArrayList<VenusSubDevice> subDevices = new ArrayList<VenusSubDevice>();
    public static ArrayList<VenusGroupDevice> venusGroups = new ArrayList<VenusGroupDevice>();
    public static final Semaphore alljoynSemaphore = new Semaphore(1);
    
    public static void init(
            final FragmentManager fragmentManager,
            final ControllerClientCallback controllerClientCallback,
            final ControllerServiceManagerCallback controllerServiceManagerCallback,
            final LampManagerCallback lampManagerCallback,
            final SampleGroupManagerCallback groupManagerCallback,
            final PresetManagerCallback presetManagerCallback,
            final SceneManagerCallback sceneManagerCallback,
            final MasterSceneManagerCallback masterSceneManagerCallback,
            final VenusSampleManagerCallback venusManagerCallback,
            final AboutManager aboutManager,
            final SampleAppActivity activity) {

        // acquire lock on new thread to prevent deadlock
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(AllJoynManager.TAG, "AllJoynManager.init() - acquiring lock");
                alljoynSemaphore.acquireUninterruptibly();
                Log.d(AllJoynManager.TAG, "AllJoynManager.init() - locked");

                // run business logic back in main UI thread
                activity.handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(AllJoynManager.TAG, "AllJoynManager.init() - running");

                        AllJoynManager.activity = activity;
                        AllJoynManager.aboutManager = aboutManager;

                        controllerConnected = false;

                        AllJoynManagerFragment alljoynManagerFragment = (AllJoynManagerFragment) fragmentManager.findFragmentByTag(AllJoynManager.TAG);

                        if (alljoynManagerFragment == null) {
                            alljoynManagerFragment = new AllJoynManagerFragment();
                            alljoynManagerFragment.controllerClientCallback = controllerClientCallback;
                            alljoynManagerFragment.controllerServiceManagerCallback = controllerServiceManagerCallback;
                            alljoynManagerFragment.lampManagerCallback = lampManagerCallback;
                            alljoynManagerFragment.groupManagerCallback = groupManagerCallback;
                            alljoynManagerFragment.presetManagerCallback = presetManagerCallback;
                            alljoynManagerFragment.sceneManagerCallback = sceneManagerCallback;
                            alljoynManagerFragment.masterSceneManagerCallback = masterSceneManagerCallback;
                            alljoynManagerFragment.venusManagerCallback = venusManagerCallback;
                            
                            fragmentManager.beginTransaction().add(alljoynManagerFragment, AllJoynManager.TAG).commit();
                        }
                    }
                });
            }
        }).start();
        Log.d(AllJoynManager.TAG, "AllJoynManager.init() - thread started, resuming");
    }

    public static void restart() {
        Log.d(AllJoynManager.TAG, "AllJoynManager.restart()");

        AllJoynManager.stop();
        AllJoynManager.start();
    }

    public static void start() {
        activity.handler.post(new Runnable() {
            @Override
            public void run() {
                if (controllerClient != null) {
                    if (!controllerStarted) {
                        ControllerClientStatus status = controllerClient.start();

                        if (status.equals(ControllerClientStatus.OK)) {
                            Log.d(AllJoynManager.TAG, "AllJoynManager.start(): succeeded");

                            aboutManager.start(AllJoynManager.bus);

                            controllerStarted = true;
                        } else {
                            Log.d(AllJoynManager.TAG, "AllJoynManager.start(): reposting");

                            activity.handler.postDelayed(this, 5000);
                        }
                    }
                } else {
                    Log.w(AllJoynManager.TAG, "AllJoynManager.start(): no controller client");
                }
            }
        });
    }

    public static void stop() {
        activity.handler.post(new Runnable() {
            @Override
            public void run() {
                if (controllerClient != null) {
                    if (controllerStarted) {
                        Log.d(AllJoynManager.TAG, "AllJoynManager.stop(): succeeded");

                        controllerClient.stop();
                        aboutManager.stop();

                        controllerStarted = false;
                    }
                } else {
                    Log.w(AllJoynManager.TAG, "AllJoynManager.stop(): no controller client");
                }
            }
        });
    }

    public static void destroy(final FragmentManager fragmentManager) {

        // acquire lock on new thread to prevent deadlock
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(AllJoynManager.TAG, "AllJoynManager.destroy() - acquiring lock");
                alljoynSemaphore.acquireUninterruptibly();
                Log.d(AllJoynManager.TAG, "AllJoynManager.destroy() - locked");

                // run business logic back in main UI thread
                activity.handler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(AllJoynManager.TAG, "AllJoynManager.destroy() - running");

                        AllJoynManagerFragment alljoynManagerFragment = (AllJoynManagerFragment) fragmentManager.findFragmentByTag(AllJoynManager.TAG);

                        if (alljoynManagerFragment != null) {
                            fragmentManager.beginTransaction().remove(alljoynManagerFragment).commit();
                        }
                    }
                });
            }
        }).start();
        Log.d(AllJoynManager.TAG, "AllJoynManager.destroy() - thread started, resuming");
    }
    
    public static VenusGroupDevice getVenusGroupDeviceByID(String devceID){
    	VenusGroupDevice device = null;
    	
    	for(VenusGroupDevice dev : venusGroups){
    		if(dev.getId() == Integer.parseInt(devceID.replace("G", ""))){
    			device = dev;
    		}
    	}
    	
    	return device;
    }

    public static class AllJoynManagerFragment extends Fragment {
        private AllJoynManagerAsyncTask alljoynManagerTask;

        public ControllerClientCallback controllerClientCallback;
        public ControllerServiceManagerCallback controllerServiceManagerCallback;
        public LampManagerCallback lampManagerCallback;
        public SampleGroupManagerCallback groupManagerCallback;
        public PresetManagerCallback presetManagerCallback;
        public SceneManagerCallback sceneManagerCallback;
        public MasterSceneManagerCallback masterSceneManagerCallback;
        public VenusSampleManagerCallback venusManagerCallback;

        public BusAttachment bus;
        public ControllerClient controllerClient;
        public ControllerServiceManager controllerServiceManager;
        public LampManager lampManager;
        public LampGroupManager groupManager;
        public PresetManager presetManager;
        public SceneManager sceneManager;
        public VenusManager venusManager;
        public MasterSceneManager masterSceneManager;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setRetainInstance(true);

            Context context = getActivity().getApplicationContext();

            org.alljoyn.bus.alljoyn.DaemonInit.PrepareDaemon(context);

            alljoynManagerTask = new AllJoynManagerAsyncTask(this);
            alljoynManagerTask.execute(context.getPackageName());
        }

        @Override
        public void onDestroy() {
            super.onDestroy();

            Log.d(AllJoynManager.TAG, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            Log.d(AllJoynManager.TAG, "onDestroy()");
            aboutManager.destroy();

            masterSceneManager.destroy();
            sceneManager.destroy();
            presetManager.destroy();
            groupManager.destroy();
            lampManager.destroy();
            controllerServiceManager.destroy();
            controllerClient.destroy();

            bus.disconnect();
            bus.release();

            AllJoynManager.controllerConnected = false;
            AllJoynManager.masterSceneManager = null;
            AllJoynManager.sceneManager = null;
            AllJoynManager.venusManager = null;
            AllJoynManager.presetManager = null;
            AllJoynManager.groupManager = null;
            AllJoynManager.lampManager = null;
            AllJoynManager.controllerServiceManager = null;
            AllJoynManager.controllerClient = null;
            AllJoynManager.bus = null;

            AllJoynManager.alljoynSemaphore.release();
            Log.d(AllJoynManager.TAG, "AllJoynManager.destroy() - unlocked");
        }
    };

    private static class AllJoynManagerAsyncTask extends AsyncTask<String, Void, Void> {
        private final AllJoynManagerFragment alljoynManagerFragment;

        private AllJoynManagerAsyncTask(AllJoynManagerFragment alljoynManagerFragment) {
            this.alljoynManagerFragment = alljoynManagerFragment;
        }

        @Override
        protected Void doInBackground(String... params) {
            Log.d(AllJoynManager.TAG, "Creating BusAttachment");
            alljoynManagerFragment.bus = new BusAttachment(params[0], BusAttachment.RemoteMessage.Receive);
            alljoynManagerFragment.bus.connect();
            alljoynManagerFragment.bus.setDebugLevel("CONTROLLER_CLIENT", 7);
//            alljoynManagerFragment.bus.setDaemonDebug("ALL", 7);
            
            alljoynManagerFragment.controllerClient = new ControllerClient(alljoynManagerFragment.bus, alljoynManagerFragment.controllerClientCallback);
            alljoynManagerFragment.controllerServiceManager = new ControllerServiceManager(alljoynManagerFragment.controllerClient, alljoynManagerFragment.controllerServiceManagerCallback);
            alljoynManagerFragment.lampManager = new LampManager(alljoynManagerFragment.controllerClient, alljoynManagerFragment.lampManagerCallback);
            alljoynManagerFragment.groupManager = new SampleGroupManager(alljoynManagerFragment.controllerClient, alljoynManagerFragment.groupManagerCallback);
            alljoynManagerFragment.presetManager = new PresetManager(alljoynManagerFragment.controllerClient, alljoynManagerFragment.presetManagerCallback);
            alljoynManagerFragment.sceneManager = new SceneManager(alljoynManagerFragment.controllerClient, alljoynManagerFragment.sceneManagerCallback);
            alljoynManagerFragment.venusManager = new VenusManager(alljoynManagerFragment.controllerClient, alljoynManagerFragment.venusManagerCallback);            
            alljoynManagerFragment.masterSceneManager = new MasterSceneManager(alljoynManagerFragment.controllerClient, alljoynManagerFragment.masterSceneManagerCallback);
            
            alljoynManagerFragment.bus.addMatch("type='signal',interface='"+VENUS_INTERFACE+"',member='SendNotify',sessionless='t'");
            
            return null;
        }

        @Override
        protected void onPostExecute(Void response) {
            super.onPostExecute(response);

            AllJoynManager.bus = alljoynManagerFragment.bus;
            AllJoynManager.controllerClient = alljoynManagerFragment.controllerClient;
            AllJoynManager.controllerServiceManager = alljoynManagerFragment.controllerServiceManager;
            AllJoynManager.lampManager = alljoynManagerFragment.lampManager;
            AllJoynManager.groupManager = alljoynManagerFragment.groupManager;
            AllJoynManager.presetManager = alljoynManagerFragment.presetManager;
            AllJoynManager.sceneManager = alljoynManagerFragment.sceneManager;
            AllJoynManager.venusManager = alljoynManagerFragment.venusManager;
            AllJoynManager.masterSceneManager = alljoynManagerFragment.masterSceneManager;

            aboutManager.start(AllJoynManager.bus);

            AllJoynManager.alljoynSemaphore.release();

            activity.onAllJoynManagerInitialized();

            Log.d(AllJoynManager.TAG, "AllJoynManager.init() - unlocked");
       }
    }
}
