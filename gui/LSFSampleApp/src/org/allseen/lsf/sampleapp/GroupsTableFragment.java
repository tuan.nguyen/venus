/*
 * Copyright (c) 2014, AllSeen Alliance. All rights reserved.
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package org.allseen.lsf.sampleapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.verik.venus.sample.VenusGroupDevice;

public class GroupsTableFragment extends DimmableItemTableFragment {

    public GroupsTableFragment() {
        super();
        type = SampleAppActivity.Type.GROUP;
    }

    @Override
    protected int getInfoButtonImageID() {
        return R.drawable.nav_more_menu_icon;
    }

    @Override
    protected Fragment getInfoFragment() {
        return new GroupInfoFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);

        for (String id : ((SampleAppActivity) getActivity()).groupModels.keySet()) {
            addElement(id);
        }
        
        for(VenusGroupDevice group : AllJoynManager.venusGroups){
        	addElement("G"+group.getId()); 
        }

        return root;
    }

    @Override
    public void addElement(String id) {
        GroupDataModel groupModel = ((SampleAppActivity) getActivity()).groupModels.get(id);
        if (groupModel != null) {
            insertDimmableItemRow(getActivity(), groupModel.id, groupModel.tag, groupModel.state.getOnOff(), groupModel.uniformity.power, groupModel.getName(), groupModel.state.getBrightness(), groupModel.uniformity.brightness, 0, groupModel.capability.dimmable >= CapabilityData.SOME);
        }
        
        VenusGroupDevice venusGroup = null;
        
        for(VenusGroupDevice device : AllJoynManager.venusGroups){
        	if(device.getId() == Integer.parseInt(id.replace("G", ""))){
        		venusGroup = device;
        	}
        }
        
        if(venusGroup != null){
        	Log.d("VENUS","Insert venus group: "+id);
        	insertVenusGroupItemRow(getActivity(),id,false,false,venusGroup.getName(),0,true);
        }
        updateLoading(true);
    }
}
