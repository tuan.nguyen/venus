#
# Copyright (C) 2013 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define Profile/VENUS
  NAME:=Venus board version 1.0
  PACKAGES:= uboot-omap-venus
endef

define Profile/VENUS/Description
 Package set compatible with Venus board of Verik system.
endef

$(eval $(call Profile,VENUS))
