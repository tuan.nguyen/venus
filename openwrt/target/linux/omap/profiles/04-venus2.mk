#
# Copyright (C) 2013 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define Profile/VENUS_2
  NAME:=Venus board version 2.0
  PACKAGES:= uboot-omap-venus-2 kmod-leds-lp5523
endef

define Profile/VENUS_2/Description
 Package set compatible with Venus board of Verik system.
endef

$(eval $(call Profile,VENUS_2))
