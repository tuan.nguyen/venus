#
# Copyright (C) 2013 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define Profile/PhD
  NAME:=PhD board (all in one)
  PACKAGES:= uboot-omap-zinno kmod-leds-lp5523
endef

define Profile/PhD/Description
 Package set compatible with Venus board of Verik system.
endef

$(eval $(call Profile,PhD))
