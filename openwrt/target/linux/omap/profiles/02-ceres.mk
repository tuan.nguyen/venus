#
# Copyright (C) 2013 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define Profile/CERES
  NAME:=Ceres board version 1.0
  PACKAGES:= uboot-omap-ceres
endef

define Profile/CERES/Description
 Package set compatible with Ceres board of Verik system.
endef

$(eval $(call Profile,CERES))
