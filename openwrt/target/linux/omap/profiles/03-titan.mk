#
# Copyright (C) 2013 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

define Profile/TITAN
  NAME:=Titan board version 1.0
  PACKAGES:= uboot-omap-titan kmod-leds-lp5523
endef

define Profile/TITAN/Description
 Package set compatible with Titan board of Verik system.
endef

$(eval $(call Profile,TITAN))
