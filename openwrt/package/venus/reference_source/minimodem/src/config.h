/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Name of package */
#define PACKAGE "minimodem"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "kamal@whence.com"

/* Define to the full name of this package. */
#define PACKAGE_NAME "minimodem"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "minimodem 0.22.1"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "minimodem"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "0.22.1"

/* Define to 1 to enable ALSA support */
#define USE_ALSA 1

/* Define to 1 to enable internal benchmarks */
#define USE_BENCHMARKS 0

/* Define to 1 to enable pulseaudio support */
#define USE_PULSEAUDIO 0

/* Define to 1 to enable sndfile support */
#define USE_SNDFILE 0

/* Version number of package */
#define VERSION "0.22.1"
