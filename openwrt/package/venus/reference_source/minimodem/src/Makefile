# define the C compiler to use
#CC = gcc
VER=0
REL=1

TARGET=minimodem
PACKAGE_NAME=$(TARGET)

# define any compile-time flags
CFLAGS = -Wall -g

INCLUDES = -I./ -I../../commons/inc/

LFLAGS = -L./ -L../../commons/libs/

# define any libraries to link into executable:
LIBS = -lfftw3f -lm -lasound

# define the C source files
SRCS=$(shell echo *.c)

OBJECTS=$(patsubst %.c,%.o,$(notdir $(wildcard *.cpp) $(wildcard *.c)))

# define the executable file
MAIN = minimodem

#
# The following part of the makefile is generic; it can be used to
# build any executable just by changing the definitions above and by
# deleting dependencies appended to the file from 'make depend'
#

.PHONY: depend clean

all: $(MAIN)
	@echo  Simple compiler named $(MAIN) has been compiled

$(MAIN): $(OBJECTS)
	$(CC) $(CFLAGS) -o $(MAIN) $(OBJECTS) $(LDFLAGS)  $(LIBS)

# this is a suffix replacement rule for building .o's from .c's
# it uses automatic variables $<: the name of the prerequisite of
# the rule(a .c file) and $@: the name of the target of the rule (a .o file)
# (see the gnu make manual section about automatic variables)
.c.o:
	$(CC) -DHAVE_CONFIG_H $(CFLAGS) -c $<  -o $@

clean:
	$(RM) *.o *~ $(MAIN) *.d
	$(RM) -r ./${PACKAGE_NAME}-${VER}
	$(RM) -f ${PACKAGE_NAME}-${VER}.${REL}.tar.gz

depend: $(SRCS)
	makedepend $^

package:
	@mkdir -p ./${PACKAGE_NAME}-${VER}
	@cp *.c *.h Makefile ./$(PACKAGE_NAME)-${VER}
	tar -cz -f ${PACKAGE_NAME}-${VER}.${REL}.tar.gz ./$(PACKAGE_NAME)-${VER}

release: package
	@echo "Copy Package minimodem app Files to Release Folder"
	@if [ ! -d ../../release ]; then mkdir -p ../../release; fi
	@cp $(PACKAGE_NAME)-$(VER).$(REL).tar.gz ../../release
