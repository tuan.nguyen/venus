#!/bin/sh
. /usr/share/libubox/jshn.sh
. /etc/vrlb.sh

# set -x
PRO_ADDR="api.zinnoinc.com"
STA_ADDR="staging-api.zinnoinc.com"

DB_PRODUCT=$(uci get firmware.@supported-database-info[0].id)
DB_STAGING=$(uci get firmware.@supported-database-info[0].idDev)

DB_PRODUCT_TOKEN=$(uci get firmware.@supported-database-info[0].token)
DB_STAGING_TOKEN=$(uci get firmware.@supported-database-info[0].tokenDev)

FIRMWARE_ADDRESS="https://fw.zinnoinc.com"
IS_UPDATE_LINK="/api/products/%s/isupdated?currentVersion=%s&uuid=%s"

FIRMWARE_INFO="{\"infor\":[
{\"board\":$PHD,
\"proName\":\"phd\",\"proToken\":\"6a7a7b8426bc4d25a92d16ee4233e151\",
\"staName\":\"phd-staging\",\"staToken\":\"6a217b90364a49e9b70a737287108f3d\"}]}"

UUID=$(uci get security.@remote-access[0].hubUUID)
IS_STAGING=$(uci get security.@remote-access[0].cloud_address)

get_update() 
{
    local name="$1"
    local ver="$2"
    local token="$3"
    local address=`printf $FIRMWARE_ADDRESS$IS_UPDATE_LINK "$name" "$ver" "$UUID"`
    local is_update=`curl --cacert /etc/ssl/certs/ca-certificates.crt --capath /etc/ssl/certs/ -X GET --header "Accept: application/json" --header "authorization: $token" $address`
    echo $is_update
}

database_update() 
{
    local is_update
    local current_ver=$(uci get firmware.@supported-database-info[0].current)

    cur_ver_length=${#current_ver}
    if [ "$cur_ver_length" -lt "5" ]; then
        current_ver="0.0.0"
    fi

    if [ "$IS_STAGING" == "$STA_ADDR" ]; then
        is_update=$(get_update "$DB_STAGING" "$current_ver" "$DB_STAGING_TOKEN")
    else
        is_update=$(get_update "$DB_PRODUCT" "$current_ver" "$DB_PRODUCT_TOKEN")
    fi

    # jshn -r "$is_update"
    json_load "$is_update"
    json_select error
    if [ "$?" == "0" ];then
        json_get_var err_message message
        echo "$err_message"
        # exit 1
    else
        json_get_var fwUpdate fwUpdate
        if [ "$fwUpdate" == "1" ]; then
            json_get_var fwChecksum fwChecksum
            json_get_var fwUrl fwUrl
            json_get_var version version

            curl -L -o /tmp/databases.tar.gz $fwUrl --capath /etc/ssl/certs/ --cacert /etc/ssl/certs/ca-certificates.crt
            local fw_md5sum=`md5sum /tmp/databases.tar.gz | cut -d' ' -f1`

            if [ "$fw_md5sum" == "$fwChecksum" ]; then
                tar -xvzf /tmp/databases.tar.gz -C /tmp/
                if [ "$?" == "0" ];then
                    if [ -f /tmp/supported_devices.db ]; then
                        dd if=/tmp/supported_devices.db of=/etc/supported_devices.db
                    fi
                    if [ -d /tmp/sound/ ]; then
                        rm -rf /etc/sound
                        mv /tmp/sound/ /etc/
                        if [ "$?" == "0" ];then
                            uci set firmware.@supported-database-info[0].current=$version
                            uci set firmware.@supported-database-info[0].latest=$version
                            uci set firmware.@supported-database-info[0].checksum=$fwChecksum
                            uci set firmware.@supported-database-info[0].url=$fwUrl
                            uci commit firmware
                            ubus send updateSupportedDb {\"version\":\"$version\"}
                        fi
                    fi
                fi
            fi
            rm -rf /tmp/databases.tar.gz
        else
            echo "database latest"
        fi
        # exit 0
    fi
}

firmware_get_update()
{
    local is_update
    local current_ver=$(uci get firmware.@firmware-info[0].current)

    cur_ver_length=${#current_ver}
    if [ "$cur_ver_length" -lt "5" ]; then
        current_ver="0.0.0"
    fi

    if [ "$IS_STAGING" == "$STA_ADDR" ]; then
        json_get_vars staName staToken
        is_update=$(get_update "$staName" "$current_ver" "$staToken")
    else
        json_get_vars proName proToken
        is_update=$(get_update "$proName" "$current_ver" "$proToken")
    fi

    json_load "$is_update"
    json_select error
    if [ "$?" == "0" ];then
        json_get_var err_message message
        echo "$err_message"
        # exit 1
    else
        json_get_var fwUpdate fwUpdate
        if [ "$fwUpdate" == "1" ]; then
            json_get_var fwChecksum fwChecksum
            json_get_var fwUrl fwUrl
            json_get_var version version

            uci set firmware.@firmware-info[0].latest=$version 
            uci set firmware.@firmware-info[0].checksum=$fwChecksum 
            uci set firmware.@firmware-info[0].url=$fwUrl 
            uci commit firmware
        else
            echo "firmware latest"
        fi
    fi
}

firmware_update() 
{
    # getBoardType
    # local boardType=$?
    local element
    # echo "$FIRMWARE_INFO"
    json_load "$FIRMWARE_INFO"
    json_get_keys arrays infor
    json_select infor

    for element in $arrays; do
        json_select "$element"
        json_get_vars board
        # if [ "$board" == "$boardType" ]; then
            firmware_get_update
            break;
        # fi
        json_select ..
    done
}

database_update
firmware_update

