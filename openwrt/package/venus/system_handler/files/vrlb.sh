
TITAN=1
PHD=2
Rev_00_OE=48484869
DEFAULT_LOG=/system.log

log_location() {
    local log_location=`fw_printenv -n log | sed 's/ //g'`
    if [ "$log_location" == "" ]; then
        set_log_location $DEFAULT_LOG
        log_location="$DEFAULT_LOG"
    fi
    echo "$log_location"
}

set_log_location() {
    local log_location=$1
    if [ "$log_location" != "" ]; then
        fw_setenv log "$log_location"
    else
        fw_setenv log "$DEFAULT_LOG"
    fi
}

getBoardType() {
	board_name=`fw_printenv -n board_name | sed 's/ //g'`
	if [ "$board_name" == "PhD" ];then
		return $PHD
	elif [ "$board_name" == "Titan" ];then
		return $TITAN
	fi
}

finding_zigbee_module() {
    getBoardType
    local boardType=$?

	if [ $boardType == $PHD ];then
        local board_version=`fw_printenv -n board_version | sed 's/ //g'`
        if [ "$board_version" -lt "$Rev_00_OE" ]; then
            echo "telegesis"
    	else
    		echo "gecko"
        fi
	fi
}

contains() {
    local string="$1"
    local substring="$2"
    if test "${string#*$substring}" != "$string"
    then
        return 1    # $substring is in $string
    else
        return 0    # $substring is not in $string
    fi
}

compare_version() { # return 1: $1 < $2, 0: $1 > $2, 2 $1 = $2
    local ver1="$1"
    local ver2="$2"
    contains "$ver1" "."
    local res=$?
    if [ "$res" == "1" ]; then
        local major1=`echo $ver1 | cut -d'.' -f1`
        local major2=`echo $ver2 | cut -d'.' -f1`
        if [ "$major1" -lt "$major2" ]; then
            return 1
        else 
            if [ "$major1" -gt "$major2" ]; then
                return 0
            fi
        fi

        local minor1=`echo $ver1 | cut -d'.' -f2`
        local minor2=`echo $ver2 | cut -d'.' -f2`
        if [ "$minor1" -lt "$minor2" ]; then
            return 1
        else 
            if [ "$minor1" -gt "$minor2" ]; then
                return 0
            fi
        fi

        local patch1=`echo $ver1 | cut -d'.' -f3`
        local patch2=`echo $ver2 | cut -d'.' -f3`
        if [ "$patch1" -lt "$patch2" ]; then
            return 1
        else 
            if [ "$patch1" -gt "$patch2" ]; then
                return 0
            fi
        fi

        return 0
    else
        return 1
    fi
}
