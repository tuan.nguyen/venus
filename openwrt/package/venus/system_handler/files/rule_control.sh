#!/bin/sh
# set -x
. /etc/vrlb.sh

echo There are $# arguments
ruleId=$1
hasLoop=$2
actions=$3
# curHubUUID=`uci get security.@remote-access[0].hubUUID`
alljoynService=`ubus list | grep alljoyn`
pubsubService=`ubus list | grep pubsub`

if [ ! -z "$alljoynService" ]; then
	serviceControl=$alljoynService
else if [ ! -z "$pubsubService" ]; then
	serviceControl=$pubsubService
	fi
fi

log=$(log_location)
echo "#### EXERCUTE RULE_ACTION $RuleID using $serviceControl ####" >> $log
ubus call $serviceControl trigger {\"ruleId\":\"$ruleId\",\"actions\":\"$actions\"}

if [ "$hasLoop" == "noLoop" ]; then
	echo "noLoop"
	sed -e "/\"$ruleId\"/ s/^#*/#/" -i /etc/crontabs/root
	sqlite3 -cmd ".timeout 200" /etc/database/rule.db "update RULES set status='INACTIVE' where id='$ruleId'"
fi

if [ "$hasLoop" == "autoOff" ]; then
	echo "autoOff"
	sed -e "/\"$ruleId\"/d" -i /etc/crontabs/root
fi
