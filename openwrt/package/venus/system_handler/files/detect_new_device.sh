#!/bin/sh

CONFIG_FOLDER="/tmp/"
CONFIG_FILE_NAME="inform"

RENEW_INFORM_TIMEOUT=1800 #after this time, all new or old will be informed
NEW_INFORM_TIMEOUT=300 #after this time, all new will be informed

state=$1 #add | old
macAddress=$2
ipAddress=$3
deviceName=$4

is_config_file_exist() {
	if [ ! -e "$CONFIG_FOLDER$CONFIG_FILE_NAME" ]; then
		touch $CONFIG_FOLDER$CONFIG_FILE_NAME
		uci add -c $CONFIG_FOLDER $CONFIG_FILE_NAME "setup"
		uci set -c $CONFIG_FOLDER $CONFIG_FILE_NAME.@setup[0].lastInform="0"
		uci set -c $CONFIG_FOLDER $CONFIG_FILE_NAME.@setup[0].identify="0"
		uci commit -c $CONFIG_FOLDER $CONFIG_FILE_NAME
		return 0
	fi

	return 1
}

update_configuration() {
	local currentTime=$1
	uci set -c $CONFIG_FOLDER $CONFIG_FILE_NAME.@setup[0].lastInform="$currentTime"
	uci set -c $CONFIG_FOLDER $CONFIG_FILE_NAME.@setup[0].identify="$currentTime"
	uci commit -c $CONFIG_FOLDER $CONFIG_FILE_NAME
}

inform() {
	update_configuration $1

	local devState=$(uci get security.@local-access[0].state)
	if [ "$devState" == "available" ]; then
		# echo "INFORM connected, go ahead to Zinno app to finish the setup."
		ubus send ledAndSound {\"type\":\"sound\",\"deviceId\":\"system\",\"cmd\":1,\"repeat\":1,\"delayTime\":0,\"file\":\"/etc/sound/system/pleaseFinishSetup\"}

	elif [ "$devState" == "connected" ]; then
		# echo "INFORM connected, now you can back to application to control"
		ubus send ledAndSound {\"type\":\"sound\",\"deviceId\":\"system\",\"cmd\":1,\"repeat\":1,\"delayTime\":0,\"file\":\"/etc/sound/system/backToApp\"}
	fi
}

is_config_file_exist
res=$?

uptime=`awk -F"." '{printf $1}' /proc/uptime`
if [ $uptime -lt 50 ]; then
	exit 0
fi

if [ "$res" == "1" ]; then
	local lastInform=$(uci get -c $CONFIG_FOLDER $CONFIG_FILE_NAME.@setup[0].lastInform)
	local currentTime=$(date +%s)
	local tmp=$((currentTime - lastInform))
	if [ $tmp -gt $RENEW_INFORM_TIMEOUT ]; then
		inform $currentTime
	elif [ $tmp -gt $NEW_INFORM_TIMEOUT ]; then
		if [ "$state" == "add" ];then
			inform $currentTime
		fi
	fi
else
	# that mean after rebooting
	local currentTime=$(date +%s)
	inform $currentTime
fi