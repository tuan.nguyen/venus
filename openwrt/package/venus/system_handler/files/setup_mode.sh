#!/bin/sh

if [ "$1" == "enter" ]; then
    ps -w | grep "/etc/init.d/alljoyn-onboarding poll_connection" | cut -d" " -f1 | xargs kill -9
    ps -w | grep "/etc/init.d/alljoyn-onboarding connect" | cut -d" " -f1 | xargs kill -9
    ps -w | grep "/etc/init.d/alljoyn-onboarding retry" | cut -d" " -f1 | xargs kill -9
    /etc/init.d/alljoyn-onboarding softaponly
elif [ "$1" == "quit" ]; then
    /etc/init.d/alljoyn-onboarding poll_connection
elif [ "$1" == "ping" ]; then
    local host=$2
    local timeout=$3
    if [ -z $timeout ]; then
        timeout=3
    fi
    ping -w $timeout -c 3 $host 1>/dev/null 2>/dev/null
    res=$?
    if [ "$res" == "0" ]; then
        echo "ping ok"
    else
        echo "ping failed"
    fi
elif [[ "$1" == "isConnected" ]]; then
    local iface=$2
    if [ -z $iface ]; then
        iface=wlan0
    fi

    local conn_state=$(wpa_cli -i ${iface} status 2>/dev/null | grep wpa_state | cut -d= -f2)
    if [ "$conn_state" == "COMPLETED" ]; then
        echo "connect ok"
    else
        echo "connect failed"
    fi
fi