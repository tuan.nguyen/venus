#!/bin/sh

ALLJOYN_SERVICE="alljoyn_handler"
UPNP_SERVICE="upnp_handler"
ZIGBEE_SERVICE="zigbee_handler"
ZWAVE_SERVICE="zwave_handler"
REMOTE_SERVICE="pubsub_handler"
RULE_SERVICE="rule_handler"
VENUS_SERVICE="venus_handler"

check_network(){
	## if state is ap mode, no need to check
	local ap_status=$(uci get wireless.@wifi-iface[1].disabled)
	if [ "$ap_status" != "1" ]; then
		return;
	fi

	local sta_mode=$(iw wlan0 info | grep "type" | cut -d' ' -f2)
	if [ ${sta_mode} == "managed" ];
	then
		local status=$(wpa_cli -i wlan0 status | grep wpa_state | cut -d= -f2)
		while [ ${status} != "COMPLETED" ]
		do
			sleep 1
			status=$(wpa_cli -i wlan0 status | grep wpa_state | cut -d= -f2)
		done
	fi
}

# get_value(){
# 	local service="$1"
# 	local value=`uci get start_services.@wifi-services[0].$service`
# 	echo value
# }

stopd_all(){
	ps | grep "$ALLJOYN_SERVICE\|$UPNP_SERVICE\|$ZIGBEE_SERVICE\|$ZWAVE_SERVICE\|$RULE_SERVICE\|$VENUS_SERVICE" | cut -c-5 | xargs kill -9 &>/dev/null	
}

startd_all(){
	check_network

	$ALLJOYN_SERVICE &

	$ZWAVE_SERVICE &
	$ZIGBEE_SERVICE &
	$UPNP_SERVICE &
	$VENUS_SERVICE &
}

stopd_Wifi_Services(){
	ps | grep "$REMOTE_SERVICE" | cut -c-5 | xargs kill -9 &>/dev/null
	ps | grep "$ALLJOYN_SERVICE\|$UPNP_SERVICE" | cut -c-5 | xargs kill -9 &>/dev/null	
}

startd_Wifi_Services(){
	check_network

	local value=`uci get start_services.@wifi-services[0].alljoyn`
	if [ "$value" == "1" ]; then
		runned=`pgrep $ALLJOYN_SERVICE`
		if [ -z "$runned" ]; then
			$ALLJOYN_SERVICE &
		else
			stopd_Service $ALLJOYN_SERVICE
		fi
	fi

	value=`uci get start_services.@wifi-services[0].upnp`
	if [ "$value" == "1" ]; then
		runned=`pgrep $UPNP_SERVICE`
		if [ -z "$runned" ]; then
			$UPNP_SERVICE &
		else
			stopd_Service $UPNP_SERVICE
		fi
	fi

	value=`uci get start_services.@wifi-services[0].pubsub`
	if [ "$value" == "1" ]; then
		runned=`pgrep $REMOTE_SERVICE`
		if [ -z "$runned" ]; then
			$REMOTE_SERVICE &
		else
			local remote_state=`uci get security.@remote-access[0].state`
			if [ "$remote_state" != "connected" ]; then
				$REMOTE_SERVICE &
			else
				stopd_Service $REMOTE_SERVICE
			fi
		fi
	fi
}

stopd_ExtraServices(){
	ps | grep "$ZIGBEE_SERVICE\|$ZWAVE_SERVICE\|$RULE_SERVICE" | cut -c-5 | xargs kill -9 &>/dev/null
}

startd_ExtraServices(){

	local value=`uci get start_services.@extra-services[0].zwave`
	if [ "$value" == "1" ]; then
		runned=`pgrep $ZWAVE_SERVICE`
		if [ -z "$runned" ]; then
			$ZWAVE_SERVICE -l & 
		else
			stopd_Service $ZWAVE_SERVICE
		fi
	fi

	value=`uci get start_services.@extra-services[0].zigbee`
	if [ "$value" == "1" ]; then
		runned=`pgrep $ZIGBEE_SERVICE`
		if [ -z "$runned" ]; then
			$ZIGBEE_SERVICE -l & 
		else
			stopd_Service $ZIGBEE_SERVICE
		fi
	fi

	value=`uci get start_services.@extra-services[0].rule`
	if [ "$value" == "1" ]; then
		runned=`pgrep $RULE_SERVICE`
		if [ -z "$runned" ]; then
			$RULE_SERVICE & 
		else
			stopd_Service $RULE_SERVICE
		fi
	fi
}

stopd_Service(){
	ps | grep "$1" | cut -c-5 | xargs kill -9 &>/dev/null	
}

startd_Service(){

	local service="$1"
	if [ $service == $ALLJOYN_SERVICE ] || [ $service == $UPNP_SERVICE ] || [ $service == $REMOTE_SERVICE ];
	then
		check_network
	fi

	runned=`pgrep $service`
	if [ -z "$runned" ]; then
		$service &
	else
		if [ $service == $REMOTE_SERVICE ]; then
			ps | grep "$REMOTE_SERVICE" | cut -c-5 | xargs kill -9 &>/dev/null
		else
			stopd_Service $service
		fi
	fi
}


case $1 in
	start_all)
		#killall already run
		stopd_all
		startd_all
		;;
	stop_all)
		stopd_all
		;;
	start_wifiservice)
		#killall already run
		#stopd_Wifi_Services
		startd_Wifi_Services
		;;
	stop_wifiservice)
		stopd_Wifi_Services
		;;
	start_extraservice)
		#killall already run
		#stopd_ExtraServices
		startd_ExtraServices
		;;
	stop_extraservice)
		stopd_ExtraServices
		;;
	start_service)
		#stopd_Service $2
		startd_Service $2
		;;
	stop_service)
		stopd_Service $2
		;;
esac
