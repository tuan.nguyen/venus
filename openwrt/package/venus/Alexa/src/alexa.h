#ifndef ALEXA_H_
#define ALEXA_H_

#include <libubus.h>
#include <libubox/blobmsg_json.h>
#include <upnp/upnp.h>
#include "upnpCommon.h"

#define SIZE_32B     32
#define SIZE_256B     256
#define MAX_FW_URL_LEN      SIZE_256B
#define PLUGIN_MAX_VARS                               16

#define DEFAULT_WEB_DIR   "./web"

typedef enum {
    PLUGIN_E_EVENT_SERVICE=0,
    PLUGIN_MAX_SERVICES
  } PLUGIN_SERVICE_TYPE;

typedef int (*upnp_action)(pUPnPActionRequest pRequest, IXML_Document *request, IXML_Document **out, const char **errorString);

struct plugin_device_upnp_action
{
    const char*         actionName;
    upnp_action         pUpnpAction;
};
typedef struct plugin_device_upnp_action PluginDeviceUpnpAction, *pDeviceAction;

/** The data struct for device servuce */
struct plugin_service
{
  char                           UDN[NAME_SIZE];
  char                           ServiceId[NAME_SIZE];
  char                           ServiceType[NAME_SIZE];
  const  char*                   VariableName[PLUGIN_MAX_VARS];
  char*                          VariableStrVal[PLUGIN_MAX_VARS];
  PluginDeviceUpnpAction*        ActionTable;
  int                            cntTableSize;
  int VariableCount;
};

typedef struct plugin_service PluginServiceT, *pPluginService;

typedef int UPnP_Device_handle;
struct plugin_device
{
    /** Deviec handler*/
  UPnP_Device_handle   device_handle;
  /** The service number device has*/
  int                  service_number;
  /** service table entry*/
  PluginServiceT       service_table[PLUGIN_MAX_SERVICES];
};

typedef struct plugin_device PluginDevice;

int ControlleeDeviceStart(char *ip_address,
        unsigned short port,
        char *desc_doc_name,
        char *web_dir_path);

int ControlleeDeviceStateTableInit(char *DescDocURL);

int CtrleeDeviceSetServiceTable(int serviceType,
        const char* UDN,
        const char* serviceId,
        const char* szServiceType,
        pPluginService pService);

int CtrleeDeviceSetActionTable(PLUGIN_SERVICE_TYPE serviceType, pPluginService pOut);

int ControlleeDeviceCallbackEventHandler( Upnp_EventType EventType,
        void *Event,
        void *Cookie);
int SetBinaryState(pUPnPActionRequest pActionRequest, IXML_Document *request, IXML_Document **out, const char **errorString);
#endif