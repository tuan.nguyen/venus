#include "alexa.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#define WEB "/etc/web/"

static struct ubus_context *ctx;
static struct blob_buf buff;
static struct ubus_request g_req;
static uint32_t alexa_id;

static int command_success = 0;

char devicetype[SIZE_32B];
char serial[SIZE_32B];
char name[SIZE_256B];
char proid[SIZE_256B];
char epnum[SIZE_256B];

UpnpDevice_Handle device_handle = -1;

int default_advr_expire = 86400;//24 * 60 *60;

//- Basic event callback list
PluginDeviceUpnpAction g_basic_event_Actions[] = {

	{"SetBinaryState", SetBinaryState},
};

char* szServiceTypeInfo[] = {
	"PLUGIN_E_EVENT_SERVICE"
};

char *CtrleeDeviceServiceType[] = {
	"urn:Belkin:service:basicevent:1"
};

PluginDevice SocketDevice = {-1, PLUGIN_MAX_SERVICES};

int ControlleeDeviceStart(char *ip_address,
				unsigned short port,
				char *desc_doc_name,
				char *web_dir_path)
{
	int ret = UPNP_E_SUCCESS;
	char desc_doc_url[MAX_FW_URL_LEN];

	ControlleeDeviceStop();

	port = 49152;

	printf("Initializing UPnP Sdk with ipaddress = %s port = %u", ip_address, port);

    ret = UpnpInit( ip_address, port);

	if( ( ret != UPNP_E_SUCCESS ) && ( ret != UPNP_E_INIT ) )
	{
		printf( "Error with UpnpInit -- %d\n", ret );
		UpnpFinish();
		return ret;
	}

	ip_address = UpnpGetServerIpAddress();

	port = UpnpGetServerPort();

	printf( "UPnP Initialized ipaddress= %s port = %u", ip_address, port );

	if( desc_doc_name == NULL ) {
		desc_doc_name = "setup.xml";
	}

	if( web_dir_path == NULL ) {
		web_dir_path = DEFAULT_WEB_DIR;
	}

	snprintf( desc_doc_url, MAX_FW_URL_LEN, "http://%s:%d/%s", ip_address, port, desc_doc_name );

	//UpdateXML2Factory();
	//AsyncSaveData();

	printf("Specifying the webserver root directory -- %s\n",
					web_dir_path );

	if( ( ret = UpnpSetWebServerRootDir( web_dir_path ) ) != UPNP_E_SUCCESS ) {
		printf("Error specifying webserver root directory -- %s: %d\n",
						web_dir_path, ret );
		UpnpFinish();
		return ret;
	}

	printf(	"Registering the RootDevice\n"
					"\t with desc_doc_url: %s\n",
					desc_doc_url );

	if( ( ret = UpnpRegisterRootDevice( desc_doc_url,
									ControlleeDeviceCallbackEventHandler,
									&device_handle, &device_handle ) )
				!= UPNP_E_SUCCESS ) {
		printf("Error registering the rootdevice : %d\n", ret );
		UpnpFinish();
		return ret;
	}
	else
	{
        printf("RootDevice Registered with device_handle:%d\nInitializing State Table\n", device_handle);
		ControlleeDeviceStateTableInit(desc_doc_url);
		printf("State Table Initialized\n");

		if( ( ret = UpnpSendAdvertisement( device_handle, default_advr_expire ) )
						!= UPNP_E_SUCCESS ) {
			printf("Error sending advertisements : %d\n", ret );
			UpnpFinish();
			return ret;
		}

		printf( "Advertisements Sent\n");
	}
	
	return UPNP_E_SUCCESS;
}

int ControlleeDeviceStateTableInit(char *DescDocURL)
{
	IXML_Document *DescDoc = NULL;
	int ret = UPNP_E_SUCCESS;
	char *servid = NULL;
	char *evnturl = NULL;
	char *ctrlurl = NULL;
	char *udn = NULL;

	/*Download description document */
	if (UpnpDownloadXmlDoc(DescDocURL, &DescDoc) != UPNP_E_SUCCESS)
	{
			printf("Controllee device table initialization -- Error Parsing %s\n",
							DescDocURL);
			ret = UPNP_E_INVALID_DESC;
			ixmlDocument_free(DescDoc);

			return ret;
	}
	else
	{
			printf("Download %s success \n", DescDocURL);
	}

	udn = Util_GetFirstDocumentItem(DescDoc, "UDN");

	if (udn)
	{
			printf("UDN: %s\n", udn);
	}
	else
	{
			printf("UDN: reading failure");
	}


	//- Add basic event service
	if (!Util_FindAndParseService(DescDoc, DescDocURL, CtrleeDeviceServiceType[PLUGIN_E_EVENT_SERVICE], &servid, &evnturl, &ctrlurl))
	{
			printf("%s -- Error: Could not find Service: %s\n", __FILE__, CtrleeDeviceServiceType[PLUGIN_E_EVENT_SERVICE]);

	ret = UPNP_E_INVALID_DESC;
	goto FreeServiceResource;
	}
	else
	{
		CtrleeDeviceSetServiceTable(PLUGIN_E_EVENT_SERVICE, udn, servid,
				CtrleeDeviceServiceType[PLUGIN_E_EVENT_SERVICE],
				&SocketDevice.service_table[PLUGIN_E_EVENT_SERVICE]);
	}

FreeServiceResource:
	{
			ixmlDocument_free(DescDoc);
			FreeXmlSource(servid);
			FreeXmlSource(evnturl);
			FreeXmlSource(ctrlurl);
			FreeXmlSource(udn);

			servid = NULL;
			evnturl = NULL;
			ctrlurl = NULL;
	}


	return ret;
}

int CtrleeDeviceSetServiceTable(int serviceType,
				const char* UDN,
				const char* serviceId,
				const char* szServiceType,
				pPluginService pService)
{
		strncpy(pService->UDN, UDN, sizeof(pService->UDN)-1);
		strncpy(pService->ServiceId, serviceId, sizeof(pService->ServiceId)-1);
		strncpy(pService->ServiceType, szServiceType, sizeof(pService->ServiceType)-1);

		CtrleeDeviceSetActionTable(serviceType, &SocketDevice.service_table[serviceType]);

		return 0x00;
}

int CtrleeDeviceSetActionTable(PLUGIN_SERVICE_TYPE serviceType, pPluginService pOut)
{
	switch (serviceType)
	{
		case PLUGIN_E_EVENT_SERVICE:
			pOut->ActionTable  = g_basic_event_Actions;
			pOut->cntTableSize = sizeof(g_basic_event_Actions)/sizeof(PluginDeviceUpnpAction);
			printf("PLUGIN_E_EVENT_SERVICE: service: %s Action Table set: %d", pOut->ServiceType, pOut->cntTableSize);
			break;
	
		default:
				printf("WRONG service ID");
					break;
		}

		return UPNP_E_SUCCESS;
}

int ControlleeDeviceCallbackEventHandler( Upnp_EventType EventType,
				void *Event,
				void *Cookie)
{

		switch (EventType)
		{

				case UPNP_EVENT_SUBSCRIPTION_REQUEST:
				//printf("in UPNP_EVENT_SUBSCRIPTION_REQUEST\n");
						//PluginDeviceHandleSubscriptionRequest( (struct Upnp_Subscription_Request *)Event );
						break;

				case UPNP_CONTROL_GET_VAR_REQUEST:
						break;

				case UPNP_CONTROL_ACTION_REQUEST:
						{
							//printf("in UPNP_CONTROL_ACTION_REQUEST\n");
								struct Upnp_Action_Request* pEvent = (struct Upnp_Action_Request*)Event;
								CtrleeDeviceHandleActionRequest(pEvent);
						}
						break;

				case UPNP_DISCOVERY_ADVERTISEMENT_ALIVE:
				case UPNP_DISCOVERY_SEARCH_RESULT:
				case UPNP_DISCOVERY_SEARCH_TIMEOUT:
				case UPNP_DISCOVERY_ADVERTISEMENT_BYEBYE:
				case UPNP_CONTROL_ACTION_COMPLETE:
				case UPNP_CONTROL_GET_VAR_COMPLETE:
				case UPNP_EVENT_RECEIVED:
				case UPNP_EVENT_RENEWAL_COMPLETE:
				case UPNP_EVENT_SUBSCRIBE_COMPLETE:
				case UPNP_EVENT_UNSUBSCRIBE_COMPLETE:
						break;

				default:
						printf( "Error in ControlleeDeviceCallbackEventHandler: unknown event type %d\n",
										EventType );
		}

		return (0);
}


int startUPnP(char* desc_doc_name, char* web_dir_path)
{
	char* ip_address = NULL;
	unsigned int port = 0;
	int ret=-1;
    ret = ControlleeDeviceStart(ip_address, port, desc_doc_name, web_dir_path);
	if(( ret != UPNP_E_SUCCESS ) && ( ret != UPNP_E_INIT ) )
	{
		printf("UPNP on error: %d", ret);
	}
	return ret;
}

int ControlleeDeviceStop()
{

                int ret=-1;
		if (-1 == device_handle)
				return 0x00;

		ret=UpnpUnRegisterRootDevice(device_handle);
        if((UPNP_E_SUCCESS != ret) && (UPNP_E_FINISH != ret))
        {
            printf("################### Wemo App going to be reset, ret:%d ###################", ret);
            //resetSystem();
        }
		device_handle = -1;
		UpnpFinish();

		printf("UPNP is to stop for setup");

		return 0x00;

}

int CtrleeDeviceHandleActionRequest(struct Upnp_Action_Request *pActionRequest)
{
	int loop = 0;
	int action_found = 0;
	int retVal = UPNP_E_SUCCESS;
	char *errorString = NULL;

	if (0x00 == pActionRequest || 0x00 == pActionRequest->ActionRequest || 0x00 == strlen(pActionRequest->DevUDN))
	{
		//It should be returned as SOAP Failure Error...
		printf("Parameters error\n");
		retVal = UPNP_E_INVALID_SERVICE;
	}

	if( UPNP_E_SUCCESS == retVal )
	{
		for (loop = 0x00; loop < PLUGIN_MAX_SERVICES; loop++)
		{
			//- to locate service containing this command
			if (0 == strcmp(pActionRequest->DevUDN, SocketDevice.service_table[loop].UDN) &&
					0 == strcmp(pActionRequest->ServiceID, SocketDevice.service_table[loop].ServiceId))
			{
				//- Service found, and to locate the action name and callback function
				int index = 0;
				int cntActionNo = SocketDevice.service_table[loop].cntTableSize;

				PluginDeviceUpnpAction* pTable = SocketDevice.service_table[loop].ActionTable;

				if ( pTable )
				{
					for (index = 0; index < cntActionNo; index++)
					{
						if (0 == strcmp(pActionRequest->ActionName, (pTable + index)->actionName))
						{
							printf("Action found: %s\n", pActionRequest->ActionName);

							PluginDeviceUpnpAction* pAction = SocketDevice.service_table[loop].ActionTable + index;

							if (pAction && pAction->pUpnpAction)
							{
									retVal = pAction->pUpnpAction(pActionRequest, pActionRequest->ActionRequest,
													&pActionRequest->ActionResult, (const char **)&errorString);
									action_found = 1;
							}
							break;
						}
					}

					if( action_found )
							break;
				}
				else
				{
					break;
				}
			}
		}
	}

	if( !action_found )
	{
		//Invalid Action
		pActionRequest->ErrCode = 401;
		strncpy(pActionRequest->ErrStr, "Invalid Action", sizeof(pActionRequest->ErrStr));
	}
	else
	{
		if( retVal == UPNP_E_SUCCESS )
		{
				pActionRequest->ErrCode = UPNP_E_SUCCESS;
		}
		else
		{
			if( errorString )
			{
				//It should be returned as UPnP Error format.
				printf("Action %s: Error = %d, %s", pActionRequest->ActionName, retVal, errorString);
				switch( retVal )
				{
						case UPNP_E_INVALID_PARAM:
								pActionRequest->ErrCode = 402;
								break;
						case UPNP_E_INVALID_ARGUMENT:
								pActionRequest->ErrCode = 600;
								break;
						case UPNP_E_INTERNAL_ERROR:
								pActionRequest->ErrCode = 501;
								break;
						default:
								pActionRequest->ErrCode = retVal;
								break;
				}
				strncpy(pActionRequest->ErrStr, errorString, sizeof(pActionRequest->ErrStr));
			}
			else
			{
				//The below code does not need if action command returns the correct value.
				//But currently many action commands are returning the value with incorrect value.
				pActionRequest->ErrCode = UPNP_E_SUCCESS;
			}
		}
	}

	//- Get service type
	return pActionRequest->ErrCode;
}

static const struct blobmsg_policy msg_policy[1] = {
    [0] = { .name = "msg", .type = BLOBMSG_TYPE_STRING },
};

static void recive_data_cb(struct ubus_request *req, int type, struct blob_attr *msg)
{
	printf("receive msg from server\n");
    const char *msgstr = "(unknown)";
    struct blob_attr *tb[1];
    blobmsg_parse(msg_policy, ARRAY_SIZE(msg_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
    {
        msgstr = blobmsg_data(tb[0]);
    }
    printf("msgstr = %s\n", msgstr);
    command_success = 1;
}

int SetBinaryState(pUPnPActionRequest pActionRequest, IXML_Document *request, IXML_Document **out, const char **errorString)
{
	printf("in SetBinaryState\n");
	int ret = 0;
	if (0x00 == pActionRequest || 0x00 == request)
	{
		printf("SetBinaryState: command paramter invalid\n");
		return 0x01;
	}

	char* paramValue = Util_GetFirstDocumentItem(pActionRequest->ActionRequest, "BinaryState");

	if (0x00 == paramValue || 0x00 == strlen(paramValue))
	{
		pActionRequest->ActionResult = NULL;
		pActionRequest->ErrCode = 0x01;
		UpnpAddToActionResponse(&pActionRequest->ActionResult, "SetBinaryState",
						CtrleeDeviceServiceType[PLUGIN_E_EVENT_SERVICE], "BinaryState", "Error");

		return 0x00;
	}

	printf("to request state change to %s\n", paramValue);
	int timeout=2000;
	command_success = 0;

	blob_buf_init(&buff, 0);
	printf("devicetype = %s\n", devicetype);
	printf("serial = %s\n", serial);
	printf("alexa_id = %lu\n", alexa_id);
    blobmsg_add_string(&buff, "id", serial);
    blobmsg_add_string(&buff, "value", paramValue);
    if(!strcmp(devicetype, "zigbee"))
    {
    	blobmsg_add_string(&buff, "epnum", epnum);
    	blobmsg_add_string(&buff, "proid", proid);
    }
    ubus_invoke_async(ctx, alexa_id, "set_binary", buff.head, &g_req);
    g_req.data_cb = recive_data_cb;
    //g_req.priv = (void *) UPnPActionRequest_cb;
    ubus_complete_request_async(ctx, &g_req);

    char szCurState[SIZE_32B];
	memset(szCurState, 0x00, sizeof(szCurState));

	while((!command_success)&&(timeout-->0))
    {
        usleep(1000);
    }

	if (command_success)
	{
			//UPnPInternalToggleUpdate(toState);
            snprintf (szCurState, sizeof (szCurState), "%s", paramValue);

            printf("Local Binary State Parameters: %s\n",
                     szCurState);
			pActionRequest->ActionResult = NULL;
			pActionRequest->ErrCode = 0x00;
			UpnpAddToActionResponse(&pActionRequest->ActionResult, "SetBinaryState",
							CtrleeDeviceServiceType[PLUGIN_E_EVENT_SERVICE], "BinaryState", szCurState);

			printf("State changed, current state: %s\n", paramValue);
	}
	else
	{
		snprintf(szCurState, sizeof(szCurState), "%s", "Error");
		/* WEMO-33379: Error response should be sent even when countdown rule is in last minute */
		pActionRequest->ActionResult = NULL;
		pActionRequest->ErrCode = 0x00;
		UpnpAddToActionResponse(&pActionRequest->ActionResult, "SetBinaryState",
				CtrleeDeviceServiceType[PLUGIN_E_EVENT_SERVICE], "BinaryState", szCurState);

		printf("State not changed, current state: %s\n", paramValue);
	}

	FreeXmlSource(paramValue);

	return UPNP_E_SUCCESS;
}

int UpdateXML2Factory(char* location, char* devicetype, char* friendlyname, char* serial)
{
	FILE* pfReadStream  = 0x00;
	FILE* pfWriteStream = 0x00;
	char szBuff[SIZE_256B];
	char szBuff1[SIZE_256B];

	char file[SIZE_256B];
	memset(file, 0x00, sizeof(file));
	sprintf(file, "%s%s", location, "/setup.xml");

	char basic_xmlfile[SIZE_256B];
	memset(basic_xmlfile, 0x00, sizeof(basic_xmlfile));
	sprintf(basic_xmlfile, "%s%s", WEB, "/setup.xml");

	pfReadStream 	= fopen(basic_xmlfile, "r");
	pfWriteStream = fopen(file, "w");

	if (0x00 == pfReadStream || 0x00 == pfWriteStream)
	{
		printf("UpdateXML2Factory: open files handles failure\n");

		if(pfReadStream)
				fclose(pfReadStream);

		if(pfWriteStream)
				fclose(pfWriteStream);

		return 0;
	}

	while (!feof(pfReadStream))
	{
		memset(szBuff, 0x00, sizeof(szBuff));

		fgets(szBuff, SIZE_256B, pfReadStream);

		// if (strstr(szBuff, "<deviceType>"))
		// {
		// 	memset(szBuff, 0x00, sizeof(szBuff));
		// 	sprintf(szBuff, "<deviceType>urn:Belkin:device:%s:1</deviceType>\n", devicetype);
		// }
		// else 
		if (strstr(szBuff, "<UDN>"))
		{
			//- reset it again
			memset(szBuff, 0x00, sizeof(szBuff));
			memset(szBuff1, 0x00, sizeof(szBuff1));

			sprintf(szBuff1, "uuid:%s-1_0-%s", devicetype, serial);
			sprintf(szBuff, "<UDN>%s</UDN>\n", szBuff1);

	    	printf("Device UDN: %s\n", szBuff1);
		}
		else if (strstr(szBuff, "<serialNumber>"))
		{
		    memset(szBuff, 0x00, sizeof(szBuff));
		    snprintf(szBuff, SIZE_256B, "<serialNumber>%s</serialNumber>\n", serial);
		}
		else if (strstr(szBuff, "friendlyName"))
		{
		    memset(szBuff, 0x00, sizeof(szBuff));
		    snprintf(szBuff, SIZE_256B, "<friendlyName>%s</friendlyName>\n", friendlyname);
		}
		// else if (strstr(szBuff, "firmwareVersion"))
		// {
		//     memset(szBuff, 0x00, sizeof(szBuff));
		//     snprintf(szBuff, SIZE_256B, "<firmwareVersion>%s</firmwareVersion>\n", g_szFirmwareVersion);
		// }
		else if (strstr(szBuff, "macAddress"))
		{
		    memset(szBuff, 0x00, sizeof(szBuff));
		    snprintf(szBuff, SIZE_256B, "<macAddress>%s</macAddress>\n", serial);
		}

		fwrite(szBuff, 1, strlen(szBuff), pfWriteStream);
	}

	fclose(pfReadStream);
	fclose(pfWriteStream);

    printf("Replace set up XML successfully\n");
    return 0x00;
}

static const struct ubus_method alexa_methods[] = {
};

static struct ubus_object_type alexa_object_type;

static struct ubus_object alexa_object = {
    //.subscribe_cb = test_client_subscribe_cb,
    .name = "alexa",
    .type = &alexa_object_type,
    .methods = alexa_methods,
    .n_methods = ARRAY_SIZE(alexa_methods),
};

static int init_ubus_service()
{
    const char *ubus_socket = NULL;

    uloop_init();

    ctx = ubus_connect(ubus_socket);
    if (!ctx) {
        fprintf(stderr, "Failed to connect to ubus\n");
        return -1;
    }

    ubus_add_uloop(ctx);

    return 0;
}

static void alexa_ubus_service(char* devicetype)
{
    int ret;
    
    // ret = ubus_add_object(ctx, &alexa_object);
    // if (ret)
    //     fprintf(stderr, "Failed to add object: %s\n", ubus_strerror(ret));

    if (ubus_lookup_id(ctx, devicetype, &alexa_id)) {
        fprintf(stderr, "Failed to look up %s object\n", devicetype);
        return;
    }
    printf("alexa_id = %lu\n", alexa_id);
    uloop_run();
}

static void free_ubus_service()
{
    ubus_free(ctx);
    uloop_done();

    if(buff.buf)
    {
        free(buff.buf);
    }
}

void signalhandler(int signum)
{
    if(signum == SIGSEGV)
    {
        printf("Sorry, program got segmentation fault, see you again\n");
        exit(1);
    }

    if(signum == SIGINT)
    {
        printf("handler SIGINT\n");
		ControlleeDeviceStop();
		free_ubus_service();
    }
    
}

int main(int argc, char *argv[])
{
	signal(SIGINT, signalhandler);
	signal(SIGSEGV, signalhandler);
	int opt;

	if(argc == 1)
	{
		fprintf(stderr, "Usage: %s [-t devicetype] [-n] friendlyname [-s] Identify\n",
			       argv[0]);
		exit(EXIT_FAILURE);
	}
	while ((opt = getopt(argc, argv, "t:n:s:p:e:")) != -1) 
	{
		switch (opt) {
		case 'n':
			strncpy(name, optarg, sizeof(name)-1);
			break;
		case 't':
			strncpy(devicetype, optarg, sizeof(devicetype)-1);
			break;
		case 's':
			strncpy(serial, optarg, sizeof(serial)-1);
			break;
		case 'p':
			strncpy(proid, optarg, sizeof(serial)-1);
			break;
		case 'e':
			strncpy(epnum, optarg, sizeof(serial)-1);
			break;
		default: /* '?' */
			fprintf(stderr, "Usage: %s [-t devicetype] [-n] friendlyname [-s] Identify\n",
			       argv[0]);
			exit(EXIT_FAILURE);
		}
    }

    printf("devicetype = %s\n", devicetype);
    printf("friendlyname = %s\n", name);
    printf("serial = %s\n", serial);
    printf("proid = %s\n", proid);
    printf("epnum = %s\n", epnum);

    char command[SIZE_256B];
    char location[SIZE_256B];
	memset(command, 0x00, sizeof(command));
	sprintf(command, "%s%s%s-%s", "mkdir ",WEB ,devicetype, serial);
	system(command);

	memset(location, 0x00, sizeof(location));
	sprintf(location, "%s%s-%s", WEB, devicetype, serial);
	printf("location = %s\n", location);

    UpdateXML2Factory(location, devicetype, name, serial);

    startUPnP("setup.xml", location);

    init_ubus_service();
    alexa_ubus_service(devicetype);
    printf("++++++++++++ exit alexa service +++++++++++++\n");
    ControlleeDeviceStop();
    free_ubus_service();
    return 0;
}
