#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netinet/if_ether.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <pthread.h>
#include <json-c/json.h>

#include "database.h"
#include <slog.h> 
#include <libubus.h>
#include <libubox/blobmsg_json.h>
#include <uci.h>
#include "VR_define.h"
#include "verik_utils.h"
#include "timer.h"

#define DEBUG 0
#define VENUS_DEVICE 1
#define SUPPORT_ALEXA 0
#define SUPPORT_GOOGLE 1

#define NET_IF      "wlan0"
#define SSDP_ADDR   "239.255.255.250"
#define SSDP_PORT   1900
#define MAX_TRIES   20

#define BUF_SIZE    1024
#define SOCKET_ERROR -1

static char server_ip[32] = {0};
#define SERVER_PORT 80

void create_light_info(char *uniqueid, char* name, int on, int bri, json_object* light_info);

sqlite3 *hue_db;
static struct ubus_context *ctx;
static struct blob_buf buff;

static int upnp_msearch_response = 0;

static int g_turn_off_yourself = 1;
static struct ubus_event_handler ubus_listener;

char g_network_id[SIZE_64B];
char g_venus_service_id[SIZE_64B];
char g_device_id[SIZE_32B];
char g_google_username[SIZE_128B];

unsigned int g_inform_service_off_timer = 0;

pthread_mutex_t hue_ubus_listMutex;

typedef struct _hubus_list 
{
    char name[SIZE_256B];
    void *data;
    unsigned int action_time;
    struct ubus_request *req;
    struct _hubus_list *next;
} hubus_list;

hubus_list *g_hubus_list = NULL;
static int list_count = 0;

typedef struct _daemon_service 
{
    const char *service_name;
    const char *name;
    uint32_t id;
    int timeout;
    unsigned int timer_restart;
    unsigned int restart_time;
} daemon_service;

daemon_service daemon_services[] = {
    {"alljoyn_handler",     "alljoyn",           0,        0,   0},
    {"pubsub_handler",      "pubsub",            0,        0,   0},
};

pthread_mutex_t hue_light_list;
struct hue_light
{
    char device_type[256];
    char name[256];
    char uniqueid[256];
    int on;
    int bri;
    struct hue_light *next;
};

struct hue_light *hue_lights = NULL;

void add( char *device_type, char *name, char* uniqueid, int on, int bri )
{
    struct hue_light *temp;
    temp = (struct hue_light *)malloc(sizeof(struct hue_light));
    memset(temp, 0x00, sizeof(struct hue_light));
    strcpy(temp->device_type, device_type);
    strcpy(temp->name, name);
    strcpy(temp->uniqueid, uniqueid);
    temp->on=on;
    temp->bri=bri;

    if (hue_lights == NULL)
    {
        hue_lights=temp;
        hue_lights->next=NULL;
    }
    else
    {
        temp->next=hue_lights;
        hue_lights=temp;
    }
}

int delete_all()
{
    struct hue_light *temp;
    temp=hue_lights;
    while(hue_lights!=NULL)
    {
        hue_lights=hue_lights->next;
        free(temp);
        temp=hue_lights;
    }
    return 0;
}

void display()
{
    struct hue_light *temp = hue_lights;
    if(temp==NULL)
    {
        return;
    }
    while(temp!=NULL)
    {
        printf("device_type %s\n ",temp->device_type);
        printf("name %s\n ",temp->name);
        printf("uniqueid %s\n ",temp->uniqueid);
        printf("on %d\n ",temp->on);
        printf("bri %d\n ",temp->bri);
        temp=temp->next;
    }
    printf("\n");
}

static int test_count=0;
static void complete_data_cb(struct ubus_request *req, int ret)
{
    // SLOGI("free req\n");
    // if(req->priv)
    // {
    //     free(req->priv);
    // };
    if(req)
    {
        free(req);
        req = NULL;
    }
    
    test_count++;
    // printf("%d\n",test_count);
    // printf("%d\n",list_count);
    // SLOGI("free done\n");
}

static void add_last_list(hubus_list *node)
{
    pthread_mutex_lock(&hue_ubus_listMutex);
    hubus_list *tmp = g_hubus_list;
    // while(tmp)
    // {
    //     printf("name = %s action_time = %d\n", tmp->name, tmp->action_time);
    //     tmp=tmp->next;
    // }
    // tmp = g_allubus;
    if(tmp)
    {
        while(tmp->next)
        {
            tmp=tmp->next;
        }
        tmp->next = node;
        list_count++;
    }
    else
    {
        g_hubus_list = node;
        list_count++;
    }
    pthread_mutex_unlock(&hue_ubus_listMutex);
}

static void remove_list(hubus_list *node)
{
    pthread_mutex_lock(&hue_ubus_listMutex);
    hubus_list *tmp = g_hubus_list;
    hubus_list *pre = g_hubus_list;
    // while(tmp)
    // {
    //     printf("name = %s action_time = %d\n", tmp->name, tmp->action_time);
    //     tmp=tmp->next;
    // }
    // tmp = g_hubus_list;
    while(tmp)
    {
        if(tmp->action_time == node->action_time && tmp->req == node->req)
        {
            if(tmp == g_hubus_list)
            {
                g_hubus_list = tmp->next;
                free(tmp);
                list_count--;
            }
            else
            {
                pre->next = tmp->next;
                free(tmp);
                list_count--;
            }
            break;
        }
        pre=tmp;
        tmp=tmp->next;
    }
    pthread_mutex_unlock(&hue_ubus_listMutex);
}

static const struct blobmsg_policy msg_policy[1] = {
    [0] = { .name = "msg", .type = BLOBMSG_TYPE_STRING },
};

static void receive_data_cb(struct ubus_request *req, int type, struct blob_attr *msg)
{
    //SLOGI("receive msg from server");
    const char *msgstr = "(unknown)";
    struct blob_attr *tb[1];
    blobmsg_parse(msg_policy, ARRAY_SIZE(msg_policy), tb, blob_data(msg), blob_len(msg));

    if (tb[0])
    {
        msgstr = blobmsg_data(tb[0]);
    }
    SLOGI("msgstr = %s\n", msgstr);
    hubus_list *data = ((hubus_list *)(req->priv));

    if(data)
    {
        //SLOGI("req.priv = %x\n", req->priv);
        //SLOGI("data = %x\n", data);
        //SLOGI("data.name = %s\n", data->name);

        remove_list(data);
    }
}

int sendBuffer(int s, char *buf, int size)
{
    while (size > 0)
    {
        int sz = send(s, buf, size, 0);
        if (sz < 0) return 0; // Failure
        size -= sz; // Decrement number of bytes to send
        buf += sz;  // Advance read pointer
    }
    return 1; // All buffer has been sent
}

int init_ssdp_listen( void )
{
    int sd, fd;
    int ok = 0;
    struct ip_mreq group;
    struct ifreq ifr;
    struct sockaddr_in *psockaddr;
    struct sockaddr_in addr;
    int reuse = 1;
    int join_group = 0;

    if ( ( fd = socket( AF_INET, SOCK_DGRAM, 0 ) ) > 0 ) 
    {
        memset( &ifr, 0, sizeof( struct ifreq ) );
        ifr.ifr_addr.sa_family = AF_INET;
        strncpy( ifr.ifr_name, NET_IF, IFNAMSIZ-1 );
        if ( ioctl( fd, SIOCGIFADDR, &ifr ) < 0 ) 
        {
            SLOGE( "ioctl failed %s\n", strerror( errno ) );
            close( fd );
            return -1;
        }
        psockaddr = ( struct sockaddr_in * )&ifr.ifr_addr;
        sprintf(server_ip, "%d.%d.%d.%d",
            (int)(psockaddr->sin_addr.s_addr&0xFF),
            (int)((psockaddr->sin_addr.s_addr&0xFF00)>>8),
            (int)((psockaddr->sin_addr.s_addr&0xFF0000)>>16),
            (int)((psockaddr->sin_addr.s_addr&0xFF000000)>>24));
        close( fd );
    } 
    else 
    {
        SLOGE( "socket failed %s\n", strerror( errno ) );
        return -1;
    }
    close( fd );

    sd = socket( AF_INET, SOCK_DGRAM, 0 );
    if( setsockopt( sd, SOL_SOCKET, SO_REUSEADDR, ( char * )&reuse, sizeof( reuse ) ) < 0 ) 
    {
        SLOGW( "SO_REUSEADDR ERROR: %s\n", strerror( errno ) );
    }

    memset( &addr, 0, sizeof( struct sockaddr_in ) );
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl( INADDR_ANY );
    //addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    addr.sin_port = htons( SSDP_PORT );

    if( bind( sd, ( const struct sockaddr* )&addr, sizeof( struct sockaddr_in ) ) != 0 ) 
    {
        SLOGE( "BIND FAILED: %s\n", strerror( errno ) );
        goto failed;
    } 
    else 
    {
        ok = 0;
        do 
        {
            if ( setsockopt( sd, IPPROTO_IP, IP_MULTICAST_IF, &( psockaddr->sin_addr ), sizeof( struct in_addr ) ) < 0 ) 
            {
                SLOGW( "setsockopt IP_MULTICAST_IF returns %s\n", strerror( errno ) );
            }
            memset( &group, 0, sizeof( struct ip_mreq ) );
            group.imr_multiaddr.s_addr = inet_addr( SSDP_ADDR );
            group.imr_interface.s_addr = ( psockaddr->sin_addr ).s_addr;

            join_group = setsockopt( sd, IPPROTO_IP, IP_ADD_MEMBERSHIP, ( char* )&group, sizeof( group ) );
            if( join_group < 0 ) 
            {
                SLOGW( "setsockopt IP_ADD_MEMBERSHIP failed %s\n", strerror( errno ) );
                sleep( 1 );
            }
            ok++;
            if( ok >= MAX_TRIES ) 
            {
                SLOGW( "max tries exceeded\n\r" );
                goto failed;
            }
        } 
        while( join_group < 0 );
    }
    return sd;

failed:
    close( sd );
    return -1;
}

int data_waiting( int sd, int timeOut ) // milliseconds
{
    fd_set socketReadSet;
    FD_ZERO( &socketReadSet );
    FD_SET( sd,&socketReadSet );
    struct timeval tv;
    if ( timeOut ) 
    {
        tv.tv_sec  = timeOut / 1000;
        tv.tv_usec = ( timeOut % 1000 ) * 1000;
    } 
    else 
    {
        tv.tv_sec  = 0;
        tv.tv_usec = 0;
    }
    if ( select( sd+1,&socketReadSet,0,0,&tv ) == SOCKET_ERROR ) 
    {
        return 0;
    }
    return FD_ISSET( sd,&socketReadSet ) != 0;
}

int parse_search_target( char *resp, char *search_target, int slen )
{
    char *cptr = NULL;
    char *eptr = NULL;
    int len = 0;

    if( strncasecmp( resp, "M-SEARCH", 8 ) == 0 ) 
    {
        cptr = strchr ( resp, 'S' );
        while( cptr ) 
        {
            if(!strncasecmp( cptr, "ST: urn:", 8 )) 
            {
                memset( search_target, 0, slen );
                eptr = strchr ( ( const char* )cptr, '\r' );
                len = eptr - ( cptr+4 );
                if( len <= slen ) 
                {
                    memcpy( search_target, cptr+4, len );
#if DEBUG
                    SLOGI( "search_target: %s => %d len\n\r", search_target, len );
#endif
                    break;
                } 
                else 
                {
                    goto err;
                }
            } 
            else if(!strncasecmp( cptr, "ST:urn:", 7 )) 
            {
                memset( search_target, 0, slen );
                eptr = strchr ( ( const char* )cptr, '\r' );
                len = eptr - ( cptr+3 );
                if( len <= slen ) 
                {
                    memcpy( search_target, cptr+3, len );
#if DEBUG
                    SLOGI( "search_target: %s => %d len\n\r", search_target, len );
#endif
                    break;
                } 
                else 
                {
                    goto err;
                }
            }
            else if(!strncasecmp( cptr, "ST: ssdp:all", 12 )) 
            {
                memset( search_target, 0, slen );
                eptr = strchr ( ( const char* )cptr, '\r' );
                len = eptr - ( cptr+4 );
                if( len <= slen ) 
                {
                    memcpy( search_target, cptr+4, len );
#ifdef DEBUG
                    SLOGI( "search_target: %s => %d len\n\r", search_target, len );
#endif
                    break;
                } 
                else 
                {
                    goto err;
                }
            } 
            else
            {
                cptr = strchr ( cptr+1, 'S' );
            }
        }
    }
    return len;
err:
    return -1;
}

const char *getRandomUUIDString(){
    return "88f6698f-2c83-4393-bd03-c0123456789"; // https://xkcd.com/221/
}

int send_upnp_msearch_response(struct sockaddr_in request_addr)
{
    SLOGI("send_upnp_msearch_response \n ");
    char buf[1024*2];
    int tot_len = 0;

    memset( buf, 0, sizeof( buf ) );

    tot_len += sprintf( buf, "HTTP/1.1 200 OK\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "CACHE-CONTROL: max-age=86400\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "EXT:\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "LOCATION: http://%s:%d/upnp/huebridge/setup.xml\r\n", server_ip, SERVER_PORT );
    tot_len += sprintf( buf + strlen( buf ), "OPT: \"http://schemas.upnp.org/upnp/1/0/\"; ns=01\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "01-NLS: %s\r\n" , getRandomUUIDString());
    tot_len += sprintf( buf + strlen( buf ), "ST: urn:schemas-upnp-org:device:basic:1\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "USN: uuid:Socket-1_0-221438K0100073::urn:Belkin:device:**\r\n\r\n" );

    upnp_msearch_response ++;
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock > 0)
    {
        int rc = sendto( sock , buf, tot_len, 0 , (const struct sockaddr *)&request_addr, sizeof(request_addr));
        close(sock);
        return rc;
    }
    else
    {
        return -1;
    }
}

int send_upnp_rootdevice(struct sockaddr_in request_addr)
{
    SLOGI("send_upnp_rootdevice \n ");
    char buf[1024*2];
    int tot_len = 0;

    memset( buf, 0, sizeof( buf ) );

    tot_len += sprintf( buf, "HTTP/1.1 200 OK\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "CACHE-CONTROL: max-age=100\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "EXT:\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "LOCATION: http://%s:%d/description.xml\r\n", server_ip, SERVER_PORT );
    tot_len += sprintf( buf + strlen( buf ), "SERVER: Linux/3.14.0 UPnP/1.0 IpBridge/1.15.0\r\n");
    tot_len += sprintf( buf + strlen( buf ), "hue-bridgeid: 543530FFFEBD6AC7\r\n");
    tot_len += sprintf( buf + strlen( buf ), "ST: upnp:rootdevice\r\n");
    tot_len += sprintf( buf + strlen( buf ), "USN: uuid:2f402f80-da50-11e1-9b23-543530bd6ac7::upnp:rootdevice\r\n\r\n" );
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock > 0)
    {
        int rc = sendto( sock , buf, tot_len, 0 , (const struct sockaddr *)&request_addr, sizeof(request_addr));
        close(sock);
        return rc;
    }
    else
        return -1;
}


int send_upnp_device_response(struct sockaddr_in request_addr)
{
    SLOGI("send_upnp_device_response\n ");
    char buf[1024*2];
    int tot_len = 0;

    memset( buf, 0, sizeof( buf ) );

    tot_len += sprintf( buf, "HTTP/1.1 200 OK\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "CACHE-CONTROL: max-age=100\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "EXT:\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "LOCATION: http://%s:%d/description.xml\r\n", server_ip, SERVER_PORT );
    tot_len += sprintf( buf + strlen( buf ), "SERVER: Linux/3.14.0 UPnP/1.0 IpBridge/1.15.0\r\n");
    tot_len += sprintf( buf + strlen( buf ), "hue-bridgeid: 543530FFFEBD6AC7\r\n");
    tot_len += sprintf( buf + strlen( buf ), "ST: uuid:2f402f80-da50-11e1-9b23-543530bd6ac7\r\n");
    tot_len += sprintf( buf + strlen( buf ), "USN: uuid:2f402f80-da50-11e1-9b23-543530bd6ac7\r\n\r\n" );
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock > 0)
    {
        int rc = sendto( sock , buf, tot_len, 0 , (const struct sockaddr *)&request_addr, sizeof(request_addr));
        close(sock);
        return rc;
    }
    else
        return -1;
}

int send_upnp_device_response1(struct sockaddr_in request_addr)
{
    SLOGI("send_upnp_device_response1\n ");
    char buf[1024*2];
    int tot_len = 0;

    memset( buf, 0, sizeof( buf ) );

    tot_len += sprintf( buf, "HTTP/1.1 200 OK\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "CACHE-CONTROL: max-age=100\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "EXT:\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "LOCATION: http://%s:%d/description.xml\r\n", server_ip, SERVER_PORT );
    tot_len += sprintf( buf + strlen( buf ), "SERVER: Linux/3.14.0 UPnP/1.0 IpBridge/1.15.0\r\n");
    tot_len += sprintf( buf + strlen( buf ), "hue-bridgeid: 543530FFFEBD6AC7\r\n");
    tot_len += sprintf( buf + strlen( buf ), "ST: urn:schemas-upnp-org:device:basic:1\r\n");
    tot_len += sprintf( buf + strlen( buf ), "USN: uuid:2f402f80-da50-11e1-9b23-543530bd6ac7\r\n\r\n" );
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if(sock > 0)
    {
        int rc = sendto( sock , buf, tot_len, 0 , (const struct sockaddr *)&request_addr, sizeof(request_addr));
        close(sock);
        return rc;
    }
    else
        return -1;
}

int send_upnp_xml_response(int sock)
{
    SLOGI("send xml response\n");
    char buf[1024*6];
    int tot_len = 0;
    memset( buf, 0, sizeof( buf ) );

    int content_len = 0;
    char buf_content[1024*3];
    memset( buf_content, 0, sizeof( buf_content ) );

    content_len += sprintf( buf_content , "<?xml version=\"1.0\"?>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<root xmlns=\"urn:schemas-upnp-org:device-1-0\">\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<specVersion>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<major>1</major>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<minor>0</minor>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "</specVersion>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<URLBase>http://%s:%d/</URLBase>\n", server_ip, SERVER_PORT );
    content_len += sprintf( buf_content + strlen( buf_content ), "<device>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<deviceType>urn:schemas-upnp-org:device:Basic:1</deviceType>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<friendlyName>Amazon-Echo-HA-Bridge (%s)</friendlyName>\n", server_ip );
    content_len += sprintf( buf_content + strlen( buf_content ), "<manufacturer>Royal Philips Electronics</manufacturer>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<manufacturerURL>http://www.armzilla..com</manufacturerURL>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<modelDescription>Hue Emulator for Amazon Echo bridge</modelDescription>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<modelName>Philips hue bridge 2012</modelName>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<modelNumber>929000226503</modelNumber>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<modelURL>http://www.armzilla.com/amazon-echo-ha-bridge</modelURL>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<serialNumber>01189998819991197253</serialNumber>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<UDN>uuid:88f6698f-2c83-4393-bd03-cd54a9f8595</UDN>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<serviceList>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<service>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<serviceType>(null)</serviceType>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<serviceId>(null)</serviceId>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<controlURL>(null)</controlURL>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<eventSubURL>(null)</eventSubURL>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<SCPDURL>(null)</SCPDURL>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "</service>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "</serviceList>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<presentationURL>index.html</presentationURL>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<iconList>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<icon>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<mimetype>image/png</mimetype>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<height>48</height>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<width>48</width>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<depth>24</depth>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<url>hue_logo_0.png</url>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "</icon>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<icon>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<mimetype>image/png</mimetype>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<height>120</height>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<width>120</width>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<depth>24</depth>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "<url>hue_logo_3.png</url>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "</icon>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "</iconList>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "</device>\n" );
    content_len += sprintf( buf_content + strlen( buf_content ), "</root>\n" );

    tot_len += sprintf( buf, "HTTP/1.1 200 OK\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Server: Apache-Coyote/1.1\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Access-Control-Allow-Origin: *\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Access-Control-Max-Age: 3600\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "X-Application-Context: application:8080\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Content-Type: application/xml;charset=UTF-8\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Content-Length: %d\r\n", content_len );
    tot_len += sprintf( buf + strlen( buf ), "\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "%s", buf_content );
    upnp_msearch_response--;
    return sendBuffer( sock , buf, tot_len);
    
    //return 0;
}

static int features_callback(void *status, int argc, char **argv, char **azColName)
{
    char *state = (char*) status;
    if(argv[0] && !strcmp(argv[0], ST_ON_OFF))
    {
        if(strlen(state))
        {
            char tmp[32];
            strcpy(tmp, state);
            sprintf(state, "%s,%s", argv[1], tmp);
        }
        else
        {
            sprintf(state, "%s", argv[1]);
        }    
    }
    else if(argv[0] && !strcmp(argv[0], ST_DIM))
    {
        if(strlen(state))
        {
            sprintf(state, "%s,%s", state, argv[1]);
        }
        else
        {
            sprintf(state, "%s", argv[1]);
        }
    }
    return 0;
}

static int listlights_db_callback(void *data, int argc, char **argv, char **azColName){
    int i;
    char device_type[256];
    char friendlyname[256];
    char id[256];
    int on = 0;
    int bri = 0;
    char status[32];

    for(i=0; i<argc; i++)
    {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");

        if((!strcasecmp(azColName[i], ST_FRIENDLY_NAME) || !strcasecmp(azColName[i], ST_GROUP_NAME))
            && argv[i])
        {
            strcpy(friendlyname, argv[i]);
        }

        if(!strcasecmp(azColName[i], ST_ID) && argv[i])
        {
            sprintf(id, "%s%s", g_device_id, argv[i]);
            memset(status, 0x00, sizeof(status));
            searching_database("hue", hue_db, features_callback, status, 
                        "SELECT featureId,register from FEATURES where deviceId='%s'"
                        "AND featureId IN ('%s', '%s')",
                        argv[i], ST_ON_OFF, ST_DIM);
            if(strlen(status))
            {
                char *ch = strtok(status, ",");
                if(ch != NULL)
                {
                    on = atoi(ch);
                }
                ch = strtok(NULL, ",");
                if(ch != NULL)
                {
                    bri = atoi(ch);
                }    
            }
        }
        
        if(!strcasecmp(azColName[i], ST_GROUP_ID) && argv[i])
        {
            sprintf(id, "%s%s", g_device_id, argv[i]);
            strcpy(device_type, ST_GROUP);
        }

        if(!strcasecmp(azColName[i], ST_TYPE) && argv[i])
        {
            strcpy(device_type, argv[i]);
        }
    }
    add(device_type, friendlyname, id, on, bri);
    return 0;
}

int send_getlights_response(int sock) // send list lights
{
    SLOGI("start send getlights response\n");
    pthread_mutex_lock(&hue_light_list);
    delete_all();

#if VENUS_DEVICE
    sprintf(g_network_id, "%s00000", g_device_id);
    sprintf(g_venus_service_id, "%s00001", g_device_id);
    add("venus", "venus network", g_network_id, 0, 0);
    add("venus", "venus service", g_venus_service_id, 1, 0);
#endif
    searching_database("hue", hue_db, listlights_db_callback, NULL, 
                        "SELECT id,SUB_DEVICES.friendlyName,type from SUB_DEVICES "
                        "LEFT JOIN CONTROL_DEVS on owner=udn "
                        "where alexa!='no'");

    searching_database("hue", hue_db, listlights_db_callback, NULL, 
                        "SELECT groupId,groupName from GROUPS");

    pthread_mutex_unlock(&hue_light_list);

    int tot_len = 0;
    json_object *jobj = json_object_new_object();

    pthread_mutex_lock(&hue_light_list);
    struct hue_light *temp = hue_lights;
    while(temp)
    {
        json_object *light_info = json_object_new_object();
        create_light_info(temp->uniqueid, temp->name, temp->on, temp->bri, light_info);
        if(temp->uniqueid && strlen(temp->uniqueid))
        {
            json_object_object_add(jobj, temp->uniqueid, light_info);
        }
        temp = temp->next;
    }
    pthread_mutex_unlock(&hue_light_list);

    const char* buf_content = json_object_to_json_string(jobj);
    size_t content_len = strlen(buf_content);

    char *buf = (char*)malloc(content_len + SIZE_1024B*2);

    tot_len += sprintf( buf, "HTTP/1.1 200 OK\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Server: Apache-Coyote/1.1\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Access-Control-Allow-Origin: *\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Access-Control-Max-Age: 3600\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "X-Application-Context: application:8080\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Content-Type: application/json;charset=UTF-8\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Transfer-Encoding: chunked\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "%X\r\n", content_len );
    tot_len += sprintf( buf + strlen( buf ), "%s", buf_content );
    tot_len += sprintf( buf + strlen( buf ), "\r\n0\r\n\r\n" );

    int ret = sendBuffer( sock , buf, tot_len);
    SLOGI("send_getlights_response sock = %d ret = %d\n", sock, ret);
    free(buf);
    json_object_put(jobj);
    return ret;
    
    //return 0;
}

void convert_hex_to_int(char *devicetype, char *hex, char *result)
{
    int number = (int)strtol(hex, NULL, 16);
    if(!strcmp(devicetype, "zigbee"))
    {
        sprintf(result, "%d", number);
    }
    else if(!strcmp(devicetype, "zwave"))
    {
        sprintf(result, "%d", number+1000000);
    }
    else
    {
        sprintf(result, "%d", number+2000000);
    }
}

void convert_int_to_hex(char *integer, char *result)
{
    int number = (int)strtol(integer, NULL, 10);

    if(number >= 2000000)
    {
        number = number - 2000000;
        sprintf(result, "%05X", number);
    }
    else if(number >= 1000000)
    {
        number = number - 1000000;
        sprintf(result, "%02X", number);
    }
    else
    {
        sprintf(result, "%04X", number);
    }
}

int send_google_getlights_response(int sock) // send list lights
{
    SLOGI("send_google_getlights_response\n");

    int tot_len = 0;
    json_object *jobj = json_object_new_object();

    pthread_mutex_lock(&hue_light_list);
    struct hue_light *temp = hue_lights;
    if(temp)
    {
        while(temp)
        {
            json_object *light_info = json_object_new_object();
            char tmp[SIZE_64B];
            convert_hex_to_int(temp->device_type, temp->uniqueid+strlen(g_device_id), tmp);
            create_light_info(tmp, temp->name, temp->on, temp->bri, light_info);
            json_object_object_add(jobj, tmp, light_info);
            temp = temp->next;
        }
    }
    else
    {
#if VENUS_DEVICE
        sprintf(g_network_id, "%s00000", g_device_id);
        sprintf(g_venus_service_id, "%s00001", g_device_id);
        add("venus", "venus network", g_network_id, 0, 0);
        add("venus", "venus service", g_venus_service_id, 1, 0);
#endif
        searching_database("hue", hue_db, listlights_db_callback, NULL, 
                            "SELECT id,SUB_DEVICES.friendlyName,type from SUB_DEVICES "
                            "LEFT JOIN CONTROL_DEVS on owner=udn "
                            "where alexa!='no'");

        searching_database("hue", hue_db, listlights_db_callback, NULL, 
                            "SELECT groupId,groupName from GROUPS");
    }
    pthread_mutex_unlock(&hue_light_list);

    const char* buf_content = json_object_to_json_string(jobj);
    size_t content_len = strlen(buf_content);

    char *buf = (char*)malloc(content_len + SIZE_1024B*2);

    tot_len += sprintf( buf, "HTTP/1.1 200 OK\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Server: Apache-Coyote/1.1\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Access-Control-Allow-Origin: *\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Access-Control-Max-Age: 3600\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "X-Application-Context: application:8080\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Content-Type: application/json;charset=UTF-8\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Transfer-Encoding: chunked\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "%X\r\n", content_len );
    tot_len += sprintf( buf + strlen( buf ), "%s", buf_content );
    tot_len += sprintf( buf + strlen( buf ), "\r\n0\r\n\r\n" );

// printf("buff = %s\n", buf);
    int ret = sendBuffer( sock , buf, tot_len);
    SLOGI("send_google_getlights_response sock = %d ret = %d\n", sock, ret);
    free(buf);
    json_object_put(jobj);
    return ret;
    
    //return 0;
}

// {
// "state":{"on":false,"bri":254,"hue":15823,"sat":88,"effect":"none","ct":313,"alert":"none","colormode":"ct","reachable":true,"xy":[0.4255,0.3998]},
// ST_TYPE:"Extended color light",
// "name":"test2",
// "modelid":"LCT001",
// "manufacturername":"Philips",
// "uniqueid":"98b54edb-04da-4d7a-9408-5ba770ef8d9d",
// "swversion":"65003148",
// "pointsymbol":{"1":"none","2":"none","3":"none","4":"none","5":"none","6":"none","7":"none","8":"none"}
//         }

void create_light_info(char *uniqueid, char* name, int on, int bri, json_object* light_info)
{
    json_object *state = json_object_new_object();
    json_object_object_add(state, "on", json_object_new_boolean(on));
    json_object_object_add(state, "bri", json_object_new_int(bri));
    json_object_object_add(state, "hue", json_object_new_int(0));
    json_object_object_add(state, "sat", json_object_new_int(0));
    json_object_object_add(state, "effect", json_object_new_string("none"));
    json_object_object_add(state, "ct", json_object_new_int(0));
    json_object_object_add(state, "alert", json_object_new_string("none"));
    json_object_object_add(state, "reachable", json_object_new_boolean(1));

    json_object_object_add(light_info, "state", state);
    json_object_object_add(light_info, ST_TYPE, json_object_new_string("Dimmable light"));
    if(name && strlen(name))
    {
        json_object_object_add(light_info, "name", json_object_new_string(name));
    }
    json_object_object_add(light_info, "modelid", json_object_new_string("LCT001"));
    json_object_object_add(light_info, "manufacturername", json_object_new_string("Philips"));
    if(uniqueid && strlen(uniqueid))
    {
        json_object_object_add(light_info, "uniqueid", json_object_new_string(uniqueid));
    }
    json_object_object_add(light_info, "swversion", json_object_new_string("65003148"));
}

int send_getall_response(int sock) // send list lights
{
    SLOGI("start send_getall_response\n");
    int tot_len = 0;

    json_object *jobj = json_object_new_object();
    json_object *lights = json_object_new_object();

    pthread_mutex_lock(&hue_light_list);
    struct hue_light *temp = hue_lights;
    while(temp)
    {
        json_object *light_info = json_object_new_object();
        create_light_info(temp->uniqueid, temp->name, temp->on, temp->bri, light_info);

        if(temp->uniqueid && strlen(temp->uniqueid))
        {
            json_object_object_add(lights, temp->uniqueid, light_info);
        }

        temp = temp->next;
    }
    pthread_mutex_unlock(&hue_light_list);

    json_object_object_add(jobj, "lights", lights);

    const char* buf_content = json_object_to_json_string(jobj);
    size_t content_len = strlen(buf_content);

    char *buf = (char*)malloc(content_len + SIZE_1024B*2);

    tot_len += sprintf( buf, "HTTP/1.1 200 OK\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Server: Jetty(9.3.z-SNAPSHOT)\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Content-Type: application/json\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Transfer-Encoding: chunked\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "%X\r\n", (unsigned int)content_len);
    tot_len += sprintf( buf + strlen( buf ), "%s", buf_content );
    tot_len += sprintf( buf + strlen( buf ), "\r\n0\r\n\r\n" );

    int ret = sendBuffer(sock, buf, tot_len);
    SLOGI("send_getall_response sock %d ret = %d\n", sock, ret);
    free(buf);
    json_object_put(jobj);
    return ret;
    
    //return 0;
}

int send_set_response(int sock, char *uniqueid, int on, int bri) // send list lights
{
    SLOGI("start send_set_response\n");
    int tot_len = 0;
    char *light = (char*)malloc(strlen(uniqueid)+256);

    json_object *jobj = json_object_new_array();
    json_object *onoff_arr = json_object_new_object();
    json_object *bri_arr = json_object_new_object();
    json_object *onoff_info = json_object_new_object();
    json_object *bri_info = json_object_new_object();

    sprintf(light, "/lights/%s/state/on", uniqueid);
    json_object_object_add(onoff_info, light, json_object_new_boolean(on));
    sprintf(light, "/lights/%s/state/bri", uniqueid);
    json_object_object_add(bri_info, light, json_object_new_int(bri));

    json_object_object_add(onoff_arr, "success", onoff_info);
    json_object_object_add(bri_arr, "success", bri_info);

    json_object_array_add(jobj, onoff_arr);
    json_object_array_add(jobj, bri_arr);

    const char* buf_content = json_object_to_json_string(jobj);
    size_t content_len = strlen(buf_content);
    char *buf = (char*)malloc(content_len+SIZE_1024B*2);

    tot_len += sprintf( buf, "HTTP/1.1 200 OK\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Server: Jetty(9.3.z-SNAPSHOT)\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Content-Type: application/json\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Transfer-Encoding: chunked\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "%X\r\n", (unsigned int)content_len);
    tot_len += sprintf( buf + strlen( buf ), "%s", buf_content );
    tot_len += sprintf( buf + strlen( buf ), "\r\n0\r\n\r\n" );

    int ret = sendBuffer(sock, buf, tot_len);
    SLOGI("send_getall_response sock %d ret = %d\n", sock, ret);
    free(light);
    free(buf);
    json_object_put(jobj);
    return ret;
    
    //return 0;
}

int send_getlight_response(int sock, char *uniqueid, char *name, int on, int bri) // send list lights
{
    SLOGI("start send getlight response\n");
    int tot_len = 0;
    json_object *light_info = json_object_new_object();
    create_light_info(uniqueid, name, on, bri, light_info);

    const char *buf_content = json_object_to_json_string(light_info);
    size_t content_len = strlen(buf_content);
    char *buf = (char*)malloc(content_len+SIZE_1024B*2);

    tot_len += sprintf( buf, "HTTP/1.1 200 OK\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Server: Jetty(9.3.z-SNAPSHOT)\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Content-Type: application/json\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Transfer-Encoding: chunked\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "%X\r\n", (unsigned int)content_len);
    tot_len += sprintf( buf + strlen( buf ), "%s", buf_content );
    tot_len += sprintf( buf + strlen( buf ), "\r\n0\r\n\r\n" );

    int ret = sendBuffer( sock , buf, tot_len);
    SLOGI("send getlight response sock %d ret = %d\n", sock, ret);

    free(buf);
    json_object_put(light_info);

    if(!strcmp(uniqueid, g_venus_service_id) && 0 == on)
    {
        g_turn_off_yourself = 0;
    }

    return ret;
    
    //return 0;
}

int send_getgroups_response(int sock) // send_getgroups_response
{
    SLOGI("start send_getgroups_response\n");

    json_object *jobj = json_object_new_object();
    json_object *actions = json_object_new_object();
    json_object *lights = json_object_new_array();
    
    json_object_object_add(actions, "on", json_object_new_boolean(0));
    json_object_object_add(actions, "bri", json_object_new_int(0));
    json_object_object_add(actions, "hue", json_object_new_int(0));
    json_object_object_add(actions, "effect", json_object_new_string("none"));
    json_object_object_add(actions, "ct", json_object_new_int(0));
    json_object_object_add(actions, "alert", json_object_new_string("none"));
    json_object_object_add(actions, "reachable", json_object_new_boolean(1));

    pthread_mutex_lock(&hue_light_list);
    struct hue_light *temp = hue_lights;
    while(temp)
    {
        if(temp->uniqueid && strlen(temp->uniqueid))
        {
            json_object_array_add(lights,json_object_new_string(temp->uniqueid));
        }
        temp = temp->next;
    }
    pthread_mutex_unlock(&hue_light_list);

    json_object_object_add(jobj, "actions", actions);
    json_object_object_add(jobj, "lights", lights);
    json_object_object_add(jobj, "name", json_object_new_string("TBB"));

    int tot_len = 0;
    const char *buf_content = json_object_to_json_string(jobj);
    size_t content_len = strlen(buf_content);

    char *buf =(char*)malloc(content_len+SIZE_1024B*2);

    tot_len += sprintf( buf, "HTTP/1.1 200 OK\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Server: Jetty(9.3.z-SNAPSHOT)\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Content-Type: application/json\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Transfer-Encoding: chunked\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "%X\r\n", (unsigned int)content_len);
    tot_len += sprintf( buf + strlen( buf ), "%s", buf_content );
    tot_len += sprintf( buf + strlen( buf ), "\r\n0\r\n\r\n" );

    int ret = sendBuffer(sock, buf, tot_len);
    SLOGI("send_getgroups_response sock %d ret = %d\n", sock, ret);

    free(buf);
    json_object_put(jobj);
    return ret;
    
    //return 0;
}

int send_google_post_respone(int sock) // send_google_post_respone
{
    SLOGI("send_google_post_respone\n");
    int tot_len = 0;
    char buf[SIZE_1024B*2];
    tot_len += sprintf( buf, "HTTP/1.1 200 OK\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Server: Jetty(9.3.z-SNAPSHOT)\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Content-Type: application/json\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "Transfer-Encoding: chunked\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "\r\n" );
    tot_len += sprintf( buf + strlen( buf ), "3D\r\n");
    tot_len += sprintf( buf + strlen( buf ), "[{\"success\":{\"username\":\"%s\"}}]", g_google_username);
    tot_len += sprintf( buf + strlen( buf ), "\r\n0\r\n\r\n" );

    int ret = sendBuffer(sock, buf, tot_len);
    SLOGI("sock %d ret = %d Value of errno: %s\n", sock, ret, strerror( errno ));

    return ret;
}

int parse_packet_data(char *buf, char *method, char *username,
                      char *resource, char *lightid, char *type, char *data)
{
    int i,j;
    char *str1, *str2, *token, *subtoken;
    char *saveptr1, *saveptr2;
    char *pch, *eptr;
    for (i = 1, str1 = buf; ; i++, str1 = NULL) 
    {
        if(i<3)
        {
            token = strtok_r(str1, " ", &saveptr1);
        }
        else
        {
            token = strtok_r(str1, "\n", &saveptr1);
        }
        if (token == NULL)
           break;
        //printf("%d: %s\n", i, token);
        if(i==1) 
            strncpy(method, token, strlen(token));
        else if(i==2 && strstr(token,"/api/"))
        {
            for (j = 1, str2 = token+5; ; j++, str2 = NULL) {
                subtoken = strtok_r(str2, "/", &saveptr2);
                if (subtoken == NULL)
                    break;
                if(j==1) strncpy(username, subtoken, strlen(subtoken));
                if(j==2) strncpy(resource, subtoken, strlen(subtoken));
                if(j==3) strncpy(lightid, subtoken, strlen(subtoken));
                if(j==4) strncpy(type, subtoken, strlen(subtoken));
            }
        }
        else if(pch = strchr(token,'{'))
        {
            eptr = strchr ( ( const char* )pch, '}' );
            strncpy(data, pch, eptr-pch+1);
        }
        else if(pch = strstr(token,"setup.xml"))
        {
            strncpy(data, "setup.xml", strlen("setup.xml"));
        }
    }
    
    // SLOGI("\n");
    // SLOGI("method =%s\n", method);
    // SLOGI("username =%s\n", username);
    // SLOGI("resource =%s\n", resource);
    // SLOGI("lightid =%s\n", lightid);
    // SLOGI("type =%s\n", type);
    // SLOGI("data =%s\n", data);
    // SLOGI("\n");

    return 0;
}

int round_(double input)
{
    int a = (int) input;
    double b = input - (double)a;
    if(b > 0.5)
    {
        a = a + 1;
    }
    return a;
}

int send_ubus_command(char *device_type, char* deviceid, char *command, int value, char* subdevid)
{
    int i;
    char st_value[SIZE_32B];

    sprintf(st_value, "%d", value);
    if(!strcasecmp(device_type, "venus"))
    {
        if(!strcmp(deviceid, g_network_id))
        {
            hubus_list *hubus = (hubus_list*)malloc(sizeof(hubus_list));
            memset(hubus, 0x00, sizeof(hubus_list));

            strcpy(hubus->name, ST_NETWORK);
            add_last_list(hubus);

            for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
            {
                if(!strcmp(ST_ALLJOYN, daemon_services[i].name))
                {
                    hubus->data = (void*)daemon_services[i].name;
                    if( daemon_services[i].id != 0)
                    {
                        blob_buf_init(&buff, 0);
                        if(value>0)
                        {
                            blobmsg_add_string(&buff, ST_STATE, ST_OPEN);
                        }
                        else
                        {
                            blobmsg_add_string(&buff, ST_STATE, ST_CLOSE);
                        }

                        hubus->action_time = (unsigned)time(NULL);

                        struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                        hubus->req=req;
                        ubus_invoke_async(ctx, daemon_services[i].id, ST_NETWORK, buff.head, req);
                        req->data_cb = receive_data_cb;
                        req->complete_cb = complete_data_cb;
                        req->priv = (void *) hubus;
                        ubus_complete_request_async(ctx, req);
                        break;
                    }
                }
            }
        }
        else if(!strcmp(deviceid, g_venus_service_id))
        {
            if(value==0)
            {
                VR_(write_option)(NULL,"security.@echo-support[0].enable=0");

                char value_str[SIZE_32B];
                sprintf(value_str, "%d", value);
                json_object *jobj = json_object_new_object();
                json_object_object_add(jobj, ST_METHOD, json_object_new_string(ST_SET_BINARY_R));
                json_object_object_add(jobj, ST_TYPE, json_object_new_string("venus"));
                json_object_object_add(jobj, ST_DEVICE_ID, json_object_new_string(ST_ALEXA));
                json_object_object_add(jobj, ST_VALUE, json_object_new_string(value_str));
                json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));

                sendMsgtoRemoteService((char *)json_object_to_json_string(jobj));
                sendMsgtoLocalService((char*)ST_SEND_NOTIFY, (char*)json_object_to_json_string(jobj));
                json_object_put(jobj);
                return 1;
            }
            else
            {
                return 1; //to do
            }
        }
    }
    else if(!strcasecmp(device_type, "group"))
    {
        SLOGI("Start set group binary %s:%s:%s\n",deviceid,
                command, st_value);
        if(strlen(deviceid) < strlen(g_device_id))
        {
            return 0;
        }

        hubus_list *hubus = (hubus_list*)malloc(sizeof(hubus_list));
        memset(hubus, 0x00, sizeof(hubus_list));

        strcpy(hubus->name, ST_SET_GROUP_BINARY);
        add_last_list(hubus);

        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(ST_ALLJOYN, daemon_services[i].name))
            {
                hubus->data = (void*)daemon_services[i].name;
                if( daemon_services[i].id != 0)
                {
                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, ST_GROUP_ID, deviceid+strlen(g_device_id));
                    blobmsg_add_string(&buff, ST_CMD, command);
                    blobmsg_add_string(&buff, ST_VALUE, st_value);

                    hubus->action_time = (unsigned)time(NULL);

                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    hubus->req=req;
                    ubus_invoke_async(ctx, daemon_services[i].id, ST_SET_GROUP_BINARY, buff.head, req);
                    req->data_cb = receive_data_cb;
                    req->complete_cb = complete_data_cb;
                    req->priv = (void *) hubus;
                    ubus_complete_request_async(ctx, req);
                    break;
                }
            }
        }
    }
    else
    {
        SLOGI("Start set binary %s:%s:%s:%s\n", device_type, deviceid,
                command, st_value);

        if(strlen(deviceid) < strlen(g_device_id))
        {
            return 0;
        }

        hubus_list *hubus = (hubus_list*)malloc(sizeof(hubus_list));
        memset(hubus, 0x00, sizeof(hubus_list));

        strcpy(hubus->name, ST_SET_BINARY);
        add_last_list(hubus);

        for (i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
        {
            if(!strcmp(ST_ALLJOYN, daemon_services[i].name))
            {
                hubus->data = (void*)daemon_services[i].name;
                if( daemon_services[i].id != 0)
                {
                    blob_buf_init(&buff, 0);
                    blobmsg_add_string(&buff, ST_SERVICE, device_type);
                    blobmsg_add_string(&buff, ST_DEVICE_ID, deviceid+ strlen(g_device_id));
                    blobmsg_add_string(&buff, ST_CMD, command);
                    blobmsg_add_string(&buff, ST_VALUE, st_value);

                    if(subdevid && strlen(subdevid)) 
                    {
                        if(!strcmp(device_type, ST_UPNP))
                        {
                            blobmsg_add_string(&buff, ST_SUBDEVID, subdevid+strlen(g_device_id));
                        }
                    }

                    hubus->action_time = (unsigned)time(NULL);

                    struct ubus_request *req = (struct ubus_request *)malloc(sizeof(struct ubus_request));
                    hubus->req=req;
                    ubus_invoke_async(ctx, daemon_services[i].id, ST_SET_BINARY, buff.head, req);
                    req->data_cb = receive_data_cb;
                    req->complete_cb = complete_data_cb;
                    req->priv = (void *) hubus;
                    ubus_complete_request_async(ctx, req);
                    break;
                }
            }
        }
    }

    return 1;
}

void connection_handler (void *socket_desc) {
    //printf("########### start connection_handler ##################\n\n\n");
    int sock = *(int*)socket_desc;
    int read_size;
    char buf [ BUF_SIZE * 4 ];
    bzero(buf,BUF_SIZE *4);

    if(((read_size = read(sock, buf, BUF_SIZE))>0))
    {
        //printf("server received %d bytes: %s\n", read_size, buf);
        char method[16] = {0};
        char username[256] = {0};
        char resource[32] = {0};
        char lightid[256] = {0};
        char type[64] = {0};
        char data[256] = {0};

        parse_packet_data(buf, method, username, resource, lightid, type, data);
        
        if(!strncasecmp(method, "GET", 3))
        {
            if(!strncasecmp(data, "setup.xml", strlen("setup.xml"))) // GET /upnp/huebridge/setup.xml HTTP/1.1
            {
                send_upnp_xml_response(sock);
            }
            else
            {
                if(*resource)
                {
                    if(!strncasecmp(resource, "lights", 6)) 
                    {
                        if(!*lightid) // GET /api/bSA4iPtS6gRPBP7VbVDrtOc23vWnlJfcb552LnVr/lights
                        {
                            if(!strcmp(username, g_google_username))
                            {
                                send_google_getlights_response(sock);
                            }
                            else
                            {
                                send_getlights_response(sock);
                            }
                        }
                        else // GET /api/bSA4iPtS6gRPBP7VbVDrtOc23vWnlJfcb552LnVr/lights/9f2b8296-550f-4a8b-a895-f2d463fbb65e
                        {
                            //display();
                            char search_id[SIZE_128B];
                            if(!strcmp(username, g_google_username))
                            {
                                char hexid[SIZE_64B];
                                convert_int_to_hex(lightid, hexid);
                                sprintf(search_id, "%s%s", g_device_id, hexid);
                            }
                            else
                            {
                                strcpy(search_id, lightid);
                            }
                            pthread_mutex_lock(&hue_light_list);
                            struct hue_light *temp = hue_lights;
                            while(temp)
                            {
                                if(!strcasecmp(search_id, temp->uniqueid))
                                {
                                    send_getlight_response(sock, lightid, temp->name, temp->on, temp->bri);
                                    break;
                                }
                                temp = temp->next;
                            }
                            pthread_mutex_unlock(&hue_light_list);
                        }
                    }
                    else if(!strncasecmp(resource, "groups", 6)) 
                    {
                        send_getgroups_response(sock);
                    }
                }
                else // GET /api/bSA4iPtS6gRPBP7VbVDrtOc23vWnlJfcb552LnVr
                {
                    send_getall_response(sock);
                }
            }
        }
        else if(!strncasecmp(method, "PUT", 3))
        {
            if(*resource && !strncasecmp(resource, "lights", 6))
            {
                if(*lightid && *data)
                {
                    char search_id[SIZE_128B];
                    if(!strcmp(username, g_google_username))
                    {
                        char hexid[SIZE_64B];
                        convert_int_to_hex(lightid, hexid);
                        sprintf(search_id, "%s%s", g_device_id, hexid);
                    }
                    else
                    {
                        strcpy(search_id, lightid);
                    }
                    printf("search_id = %s\n", search_id);
                    pthread_mutex_lock(&hue_light_list);
                    struct hue_light *temp = hue_lights;
                    while(temp)
                    {
                        if(!strcasecmp(search_id, temp->uniqueid))
                        {
                            json_object * jobj = VR_(create_json_object)(data);
                            if(!jobj)
                            {
                                break;
                            }
                            json_object_object_foreach(jobj, key, val) 
                            {
                                if(!strncasecmp(key, "on", 2))
                                {
                                    temp->on = json_object_get_boolean(val);
                                    if(send_ubus_command(temp->device_type, temp->uniqueid, ST_ON_OFF, temp->on, NULL))
                                    {
                                        send_set_response(sock, lightid, temp->on, temp->bri);
                                        if(!strcmp(username, g_google_username) && !strcmp(search_id, g_venus_service_id))
                                        {
                                            g_turn_off_yourself = 0;
                                        }
                                    }
                                }
                                else if(!strncasecmp(key, "bri", 3))
                                {
                                    temp->on = 1;
                                    temp->bri = json_object_get_int(val);
                                    if(send_ubus_command(temp->device_type, temp->uniqueid, ST_DIM, (int)round_((double)temp->bri*100/255), NULL))
                                    {
                                        send_set_response(sock, lightid, temp->on, temp->bri);
                                    }
                                }
                            }
                            json_object_put(jobj);

                            break;
                        }
                        temp = temp->next;
                    }
                    pthread_mutex_unlock(&hue_light_list);
                } 
            }
        }
        else if(!strncasecmp(method, "POST", 4))
        {
            send_google_post_respone(sock);
        }
    }

    close(sock);
    free(socket_desc);
    //printf("########### END connection_handler ##################\n\n\n");
}

void accept_inform_service_off(void *data)
{
    g_inform_service_off_timer = 0;
}

void tcp_listen( void )
{
    int sd;
    struct sockaddr_in serveraddr;
    int reuse = 1;

    socklen_t clientlen;
    int client_sock;
    int *new_sock;
    struct sockaddr_in clientaddr;

    sd = socket( AF_INET, SOCK_STREAM, 0 );
    if( setsockopt( sd, SOL_SOCKET, SO_REUSEADDR, ( char * )&reuse, sizeof( reuse ) ) < 0 ) 
    {
        SLOGW( "SO_REUSEADDR ERROR: %s\n", strerror( errno ) );
    }

    memset( &serveraddr, 0, sizeof( struct sockaddr_in ) );
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = htonl( INADDR_ANY );
    //serveraddr.sin_addr.s_addr = inet_addr(server_ip);
    serveraddr.sin_port = htons( SERVER_PORT );

    if( bind( sd, ( const struct sockaddr* )&serveraddr, sizeof( struct sockaddr_in ) ) != 0 ) 
    {
        SLOGE( "BIND FAILED: %s\n", strerror( errno ) );
        close( sd );
        return ;
    }

    if (listen(sd, 2048) < 0)
    {
        SLOGE( "ERROR on listen: %s\n", strerror( errno ) );
        close( sd );
        return ;
    }
    SLOGI( "Listening for TCP messages...\n\r" );
    clientlen = sizeof(clientaddr);
    
    while (1) {
        /* 
         * accept: wait for a connection request 
         */
        client_sock = accept(sd, (struct sockaddr *) &clientaddr, &clientlen);
        if (client_sock < 0) 
            perror("ERROR on accept");

        if(!g_turn_off_yourself)
        {
            if(!g_inform_service_off_timer)
            {
                VR_(execute_system)("ubus send hue '{\"state\":\"turn_off\"}' &");
                g_inform_service_off_timer = timerStart(accept_inform_service_off, 
                                                        NULL, 
                                                        10000, TIMER_ONETIME);
            }
            close(client_sock);
        }
        else
        {
            new_sock = (int *)malloc(sizeof(int));
            *new_sock = client_sock;

            pthread_t thread_id;
            pthread_create( &thread_id , NULL , (void*)&connection_handler , (void*) new_sock);
            pthread_detach(thread_id);
        }
    }
    close( sd );
}

void ssdp_listen()
{
    int sd;
    int ok = 0;
    int rlen = 0;
    char buf [ BUF_SIZE * 4 ];
    char search_target [ BUF_SIZE ];

    struct sockaddr_in client;
    socklen_t client_len = sizeof(client);

    sd = init_ssdp_listen();
    if( sd > 0 ) {
    //printf("opened ssdp port listener ok %d\n\r", sd);
        SLOGI( "Listening for SSDP messages...\n\r" );
    } else {
        SLOGW( "could not open ssdp listener %d\n\r", sd );
        return ;
    }

    fcntl( sd, F_SETFL, O_NONBLOCK );
    /* main loop waiting for data */
    while( 1 ) 
    {
        if(data_waiting( sd, 200 ))
        {
            rlen = 0;
            memset( buf, 0, sizeof( buf ) );
            while( ( ok = recvfrom( sd, buf + rlen, sizeof( buf ) - rlen, 0 , (struct sockaddr *) &client, &client_len) ) > 0 ) 
            {
                rlen += ok;
                if( rlen == sizeof( buf ) ) 
                {
                    break;
                }
            }
            if ( rlen ) 
            {
                ok = parse_search_target( buf, search_target, sizeof( search_target ) );
                if(ok > 0 && 
                    (!strncasecmp(search_target, "urn:schemas-upnp-org:device:basic:1", 
                        strlen("urn:schemas-upnp-org:device:basic:1"))
                    ||
                    !strncasecmp(search_target, "ssdp:all", 
                        strlen("ssdp:all"))
                        )
                    )
                {
                    SLOGI("%s\n", search_target);
                    SLOGI("%d\n", client.sin_family);
                    SLOGI("port = %d\n", client.sin_port);
                    SLOGI("address = %d.%d.%d.%d\n",
                      (int)(client.sin_addr.s_addr&0xFF),
                      (int)((client.sin_addr.s_addr&0xFF00)>>8),
                      (int)((client.sin_addr.s_addr&0xFF0000)>>16),
                      (int)((client.sin_addr.s_addr&0xFF000000)>>24));
                    /////////// get mac from arp //////////////////
                    char *Echo_addr = VR_(read_option)(NULL,"security.@echo-support[0].address");
                    if(!Echo_addr)
                    {
                        char cmd[256], buff[128];
                        memset(buff, 0x00, sizeof(buff));
                        sprintf(cmd, "cat proc/net/arp | grep -m 1 \"%d.%d.%d.%d\" | sed -e 's/  */ /g' | cut -d' ' -f4", 
                                    (int)(client.sin_addr.s_addr&0xFF),
                                    (int)((client.sin_addr.s_addr&0xFF00)>>8),
                                    (int)((client.sin_addr.s_addr&0xFF0000)>>16),
                                    (int)((client.sin_addr.s_addr&0xFF000000)>>24));
                        FILE *fp;
                        fp = popen(cmd, "r");
                        if(fp)
                        {
                            fgets(buff, 127, fp);
                            printf("address = %s\n", buff);
                            pclose(fp);
                        }

                        char *pos;
                        if ((pos=strchr(buff, '\n')) != NULL)
                             *pos = '\0';
                        printf("strlen(buff) = %d\n", strlen(buff));
                        if(strlen(buff)==17 && !strcmp(buff, "00:00:00:00:00:00"))
                        {
                            memset(buff, 0x00, sizeof(buff));
                            sprintf(cmd, "cat proc/net/arp | grep -m 1 \"%d.%d.%d.%d\" | sed -e 's/  */ /g' | cut -d' ' -f4", 
                                        (int)(client.sin_addr.s_addr&0xFF),
                                        (int)((client.sin_addr.s_addr&0xFF00)>>8),
                                        (int)((client.sin_addr.s_addr&0xFF0000)>>16),
                                        (int)((client.sin_addr.s_addr&0xFF000000)>>24));
                            FILE *fp;
                            fp = popen(cmd, "r");
                            if(fp)
                            {
                                fgets(buff, 127, fp);
                                printf("address = %s\n", buff);
                                pclose(fp);
                            }

                            char *pos;
                            if ((pos=strchr(buff, '\n')) != NULL)
                                 *pos = '\0';
                        }
                        if(strlen(buff)==17 && strcmp(buff, "00:00:00:00:00:00"))
                        {
                            VR_(write_option)(NULL,"security.@echo-support[0].address=%s", buff);
                        }
                    }
                    else
                    {
                        free(Echo_addr);
                    }
#if DEBUG
                    SLOGI( "received UDP packet on MULTICAST port %d\n\r", SSDP_PORT );
                    SLOGI( "====================\n\r%s\n\r====================\n\r", buf );
#endif
                    if(!g_turn_off_yourself)
                    {
                        if(!g_inform_service_off_timer)
                        {
                            VR_(execute_system)("ubus send hue '{\"state\":\"turn_off\"}' &");
                            g_inform_service_off_timer = timerStart(accept_inform_service_off, 
                                                                    NULL, 
                                                                    10000, TIMER_ONETIME);
                        }
                        continue;
                    }
                    else
                    {
                        if(!strcmp(search_target, "ssdp:all"))
                        {
#if SUPPORT_GOOGLE
                            send_upnp_rootdevice(client);
                            send_upnp_device_response(client);
                            send_upnp_device_response1(client);
#endif
                        }
                        else
                        {
#if SUPPORT_ALEXA                            
                            send_upnp_msearch_response(client);
#endif
                        }
                    }
                }
            }
        }
    }
    close( sd );
}

static const struct blobmsg_policy enable_alexa_policy[1] = {
    [0] = { .name = ST_VALUE, .type = BLOBMSG_TYPE_STRING },
};

static int enable_service(struct ubus_context *ctx, struct ubus_object *obj,
              struct ubus_request_data *req, const char *method,
              struct blob_attr *msg)
{
    SLOGI("enable_service\n");
    const char *value = "(unknown)";
    struct blob_attr *tb[1];
    blobmsg_parse(enable_alexa_policy, ARRAY_SIZE(enable_alexa_policy), 
                    tb, blob_data(msg), blob_len(msg));

    json_object *jobj = json_object_new_object();
    json_object_object_add(jobj, ST_TYPE, json_object_new_string("huebridge"));
    json_object_object_add(jobj, ST_METHOD, json_object_new_string("enable_service"));

    if(tb[0])
    {
        value = blobmsg_data(tb[0]);
        printf("value = %s\n", value);
        g_turn_off_yourself = atoi(value);

        VR_(write_option)(NULL,"security.@echo-support[0].enable=%s", value);

        json_object_object_add(jobj, ST_VALUE, json_object_new_string(value));
        json_object_object_add(jobj, ST_STATUS, json_object_new_string(ST_SUCCESSFUL));
    }
    else
    {
        json_object_object_add(jobj, ST_STATUS, json_object_new_string("failed"));
    }

    blob_buf_init(&buff, 0);
    blobmsg_add_string(&buff, "msg", json_object_to_json_string(jobj));
    ubus_send_reply(ctx, req, buff.head);
    json_object_put(jobj);

    return 0;
}

static const struct ubus_method hue_methods[] = {
    UBUS_METHOD("enable_service", enable_service, enable_alexa_policy),
};

static struct ubus_object_type hue_object_type =
    UBUS_OBJECT_TYPE("hue", hue_methods);

static struct ubus_object hue_object = {
    .name = "hue",
    .type = &hue_object_type,
    .methods = hue_methods,
    .n_methods = ARRAY_SIZE(hue_methods),
};

static int init_ubus_service()
{
    const char *ubus_socket = NULL;

    uloop_init();

    ctx = ubus_connect(ubus_socket);
    if (!ctx) {
        fprintf(stderr, "Failed to connect to ubus\n");
        return -1;
    }

    ubus_add_uloop(ctx);

    return 0;
}

static void polling_active_cb(struct uloop_timeout *timeout)
{

    if(upnp_msearch_response > 4)
    {
        system("/etc/start_services.sh start_service hue > /system.log");
    }
    else
    {
        char *Echo_addr = VR_(read_option)(NULL,"security.@echo-support[0].address");
        if(Echo_addr && strlen(Echo_addr))
        {
            char cmd[256], buff[128];
            memset(buff, 0x00, sizeof(buff));

            sprintf(cmd, "cat proc/net/arp | grep -m 1 \"%s\" | sed -e 's/  */ /g' | cut -d' ' -f1", 
                    Echo_addr);
            FILE *fp;
            fp = popen(cmd, "r");
            if(fp)
            {
                fgets(buff, 127, fp);
                pclose(fp);
            }
            char *pos;
            if ((pos=strchr(buff, '\n')) != NULL)
                 *pos = '\0';
            printf("buff = %s\n", buff);
            if(strlen(buff))
            {
                sprintf(cmd, "arping -c1 -Iwlan0 %s", buff);
                //FILE *fp;
                char * line = NULL, *ch;
                size_t len = 0;
                ssize_t read;
                fp=popen(cmd,"r");
                if(fp)
                {
                    while ((read = getline(&line, &len, fp)) != -1)
                    {
                        if(strstr(line, "Received"))
                        {
                            ch = strtok(line, " ");
                            ch = strtok(NULL, " ");
                            printf("ch = %s\n", ch);
                            if(!strcmp(ch, "1"))
                            {
                                printf("FOUND ECHO devices at %s\n", buff);
                            }
                            else
                            {
                                printf("not found ECHO device\n");
                            }
                        }
                        usleep(10000);
                    }
                    if(line)
                        free(line);
                    pclose(fp);
                }
            }
            else
            {
                printf("not found ECHO device\n");
            }
            free(Echo_addr);
        }
    }
    
    uloop_timeout_set(timeout, 60000);
}

static const struct blobmsg_policy service_event_policy[2] = {
    [0] = { .name = "id", .type = BLOBMSG_TYPE_INT32 },
    [1] = { .name = "path", .type = BLOBMSG_TYPE_STRING },
};

static void receive_event(struct ubus_context *ctx, struct ubus_event_handler *ev,
              const char *type, struct blob_attr *msg)
{
    char *str;

    str = blobmsg_format_json(msg, true);
    SLOGI("{ \"%s\": %s }\n", type, str);
    free(str);

    if(!strcmp(type, "ubus.object.add"))
    {
        struct blob_attr *tb[2];
        blobmsg_parse(service_event_policy, ARRAY_SIZE(service_event_policy), tb, blob_data(msg), blob_len(msg));
        uint32_t id;
        int i;
        const char *path = "unknown";

        if(tb[0] && tb[1])
        {
            id = blobmsg_get_u32(tb[0]);
            path = blobmsg_data(tb[1]);

            for(i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
            {
                if(!strcmp(daemon_services[i].name, path))
                {                    
                    daemon_services[i].id = id;
                    SLOGW("Watching object %s with id %08X\n", daemon_services[i].name, daemon_services[i].id);
                    if(daemon_services[i].timer_restart)
                    {
                        timerCancel(daemon_services[i].timer_restart);
                    }
                    daemon_services[i].timer_restart = 0;
                    daemon_services[i].restart_time = (unsigned int)time(NULL);
                }
            }
        }
    }
}

void restart_service(void *data)
{
    if(!data)
    {
        return;
    }
    int i;
    for (i = 0; i< sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(!strcmp((char*)data, daemon_services[i].name))
        {
            daemon_services[i].timer_restart = 0;
        }
    }
}

static struct uloop_timeout polling_echo_device = {
    .cb = polling_active_cb,
};

static void remove_action_timeout_cb(struct uloop_timeout *timeout)
{
    //printf("remove_action_timeout_cb\n");
    hubus_list *tmp = g_hubus_list;
    if(tmp)
    {
        unsigned int time_now = (unsigned)time(NULL);
        // printf("tmp->action_time = %d\n", tmp->action_time);
        if((time_now - tmp->action_time) > 40)
        {
            // printf("ACTION %s TIME %d WAS TIMEOUT\n", tmp->name, tmp->action_time);
            if(tmp->name && strlen(tmp->name))
            {
                int j;
                for (j = 0; j< sizeof(daemon_services)/sizeof(daemon_service); j++)
                {
                    if(!strcmp((char*)tmp->data, daemon_services[j].name))
                    {
                        daemon_services[j].timeout++;
                        if(daemon_services[j].timeout>2)
                        {
                            if(!daemon_services[j].timer_restart)
                            {
                                if((daemon_services[j].restart_time > tmp->action_time) && tmp->action_time != 0)
                                {
                                    daemon_services[j].timeout = 0;
                                    break;
                                }

                                printf("##### ACTION timeout %d, restart service %s #########\n", 
                                     daemon_services[j].timeout, daemon_services[j].name);
                                daemon_services[j].timeout = 0;
                                daemon_services[j].restart_time = (unsigned int)time(NULL);
                                daemon_services[j].timer_restart = timerStart(restart_service, 
                                                                        (void*)daemon_services[j].service_name, 
                                                                        10000, TIMER_ONETIME);
                                char cmd[256];
                                sprintf(cmd, "/etc/start_services.sh start_service %s >> /system.log &", daemon_services[j].service_name);
                                VR_(execute_system)((const char*)cmd);
                                break;
                            }
                            else
                            {
                                daemon_services[j].timeout = 0;
                            }
                        }
                    }
                }
            }
            
            //SLOGI("free tmp\n");
            remove_list(tmp);
            //SLOGI("done free tmp\n");
        }
    }
    else
    {
        //printf("list null\n");
    }
    uloop_timeout_set(timeout, 1000);
}

static struct uloop_timeout remove_action_timeout = {
    .cb = remove_action_timeout_cb,
};

static void hue_ubus_service()
{
    int ret,i;
    ret = ubus_add_object(ctx, &hue_object);
    if (ret)
        fprintf(stderr, "Failed to add object: %s\n", ubus_strerror(ret));

    for(i = 0; i < sizeof(daemon_services)/sizeof(daemon_service); i++)
    {
        if(ubus_lookup_id(ctx, daemon_services[i].name, &daemon_services[i].id))
        {
            SLOGI("Failed to look up %s object\n", daemon_services[i].name);
        }
    }

    memset(&ubus_listener, 0, sizeof(ubus_listener));
    ubus_listener.cb = receive_event;
    ret = ubus_register_event_handler(ctx, &ubus_listener, "ubus.object.add");
    if (ret)
        SLOGE("Failed to register event handler: %s\n", ubus_strerror(ret));

    //uloop_timeout_set(&polling_echo_device, 1000);
    uloop_timeout_set(&remove_action_timeout, 1000);
    uloop_run();
}

static void free_ubus_service()
{
    ubus_free(ctx);
    uloop_done();

    if(buff.buf)
    {
        free(buff.buf);
    }

    if(hue_db)
    {
        sqlite3_close(hue_db);
    }
}

int main(int argc, char const *argv[])
{
    char command[256];
    memset(command, 0x00, sizeof(command));
    sprintf(command, "%s", "ps | grep hue | head -n -3 | cut -c-5 | xargs kill -9 &>/dev/null");
    system(command);

    pthread_mutex_init(&hue_ubus_listMutex, 0);
    pthread_mutex_init(&hue_light_list, 0);

    SLOG_Init(SLOG_STDOUT, NULL);
    set_thread_name( "ssdp listener" );

    char *devid = (char *)VR_(read_option)(NULL,"security.@local-access[0].deviceid");
    if(devid)
    {
        strcpy(g_device_id, devid);
        free(devid);
    }
    else
    {
        char macaddress[SIZE_64B];
        memset(macaddress, 0x00, sizeof(macaddress));
#ifdef TITAN
        strcpy(g_device_id, "11");//Titan
#else 
        strcpy(g_device_id, "10");//Venus
#endif
        int i, k;
        k = strlen(g_device_id);
        VR_(run_command)("cat /sys/class/ieee80211/phy0/macaddress", macaddress, sizeof(macaddress));
        printf("macaddress = %s\n", macaddress);
        for(i=0; i< strlen(macaddress);i++)
        {
            if(macaddress[i] != ':')
            {
                g_device_id[k++] = macaddress[i];
            }
        }
        g_device_id[k] = '\0';
    }

    SLOGI("g_device_id = %s\n", g_device_id);
    sprintf(g_google_username, "7ee73473593b4cdba1%s", g_device_id);
    
#if VENUS_DEVICE
    sprintf(g_network_id, "%s00000", g_device_id);
    sprintf(g_venus_service_id, "%s00001", g_device_id);
    
    add("venus", "venus network", g_network_id, 0, 0);
    add("venus", "venus service", g_venus_service_id, 1, 0);
#endif

    if(open_database(DEVICES_DATABASE, &hue_db) <0)
        return -1;

    searching_database("hue", hue_db, listlights_db_callback, NULL, 
                        "SELECT id,SUB_DEVICES.friendlyName,type from SUB_DEVICES "
                        "LEFT JOIN CONTROL_DEVS on owner=udn "
                        "where alexa!='NO'");

    searching_database("hue", hue_db, listlights_db_callback, NULL, 
                        "SELECT groupId,groupName from GROUPS");
    //display();
    //system("arp -n > /dev/stdout");

    char *enable = VR_(read_option)(NULL,"security.@echo-support[0].enable");
    if(enable)
    {
        g_turn_off_yourself = atoi(enable);
        free(enable);
    }
    else
    {
        uci_do_add("security", "echo-support");
        VR_(write_option)(NULL,"security.@echo-support[0].enable=1");
    }

    timerInit();
    pthread_t handle_tcp_thread;
    pthread_create(&handle_tcp_thread, NULL, (void *)&tcp_listen, NULL);
    pthread_detach(handle_tcp_thread);

    pthread_t handle_ssdp_thread;
    pthread_create(&handle_ssdp_thread, NULL, (void *)&ssdp_listen, NULL);
    pthread_detach(handle_ssdp_thread);

    init_ubus_service();
    hue_ubus_service();
    free_ubus_service();
    return 0;
}