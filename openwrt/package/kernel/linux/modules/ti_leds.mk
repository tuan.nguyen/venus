#
# Copyright (C) 2006-2011 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

LEDS_MENU:=LED modules

define KernelPackage/leds-lp5523
  SUBMENU:=$(LEDS_MENU)
  TITLE:=LP5523 LED support titan/venus
  #DEPENDS:=@TARGET_omap_TITAN
  KCONFIG:=CONFIG_LEDS_LP5523=y CONFIG_LEDS_LP55XX_COMMON=y
  #FILES:=$(LINUX_DIR)/drivers/leds/leds-lp5523.ko $(LINUX_DIR)/drivers/leds/leds-lp55xx-common.ko
  #AUTOLOAD:=$(call AutoLoad,50,leds-lp5523)
endef

define KernelPackage/leds-lp5523/description
 Kernel module LP5523 for titan/venus
endef

$(eval $(call KernelPackage,leds-lp5523))



