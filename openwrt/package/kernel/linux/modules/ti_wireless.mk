WIRELESS_MENU:=Wireless Drivers

define KernelPackage/TI_WL18xx_driver
  SUBMENU:=$(WIRELESS_MENU)
  TITLE:=wireless driver for TI WL18xx module
  DEPENDS:=@TARGET_omap +@DRIVER_11N_SUPPORT +@PACKAGE_TI-WL18xx-firmware
  KCONFIG:=\
       CONFIG_CFG80211 CONFIG_MAC80211 CONFIG_CRYPTO_CCM \
       CONFIG_WLCORE CONFIG_WLCORE_SDIO CONFIG_WL18XX CONFIG_WLCORE_SPI=n  
  FILES:=\
  $(LINUX_DIR)/net/wireless/cfg80211.ko $(LINUX_DIR)/net/mac80211/mac80211.ko \
  $(LINUX_DIR)/drivers/net/wireless/ti/wlcore/wlcore.ko \
  $(LINUX_DIR)/drivers/net/wireless/ti/wlcore/wlcore_sdio.ko \
  $(LINUX_DIR)/drivers/net/wireless/ti/wl18xx/wl18xx.ko \
  $(LINUX_DIR)/crypto/aead.ko $(LINUX_DIR)/crypto/arc4.ko $(LINUX_DIR)/crypto/ccm.ko \
  $(LINUX_DIR)/crypto/ecb.ko $(LINUX_DIR)/crypto/cmac.ko \
  $(LINUX_DIR)/crypto/chainiv.ko $(LINUX_DIR)/crypto/crypto_blkcipher.ko $(LINUX_DIR)/crypto/crypto_wq.ko \
  $(LINUX_DIR)/crypto/ctr.ko $(LINUX_DIR)/crypto/eseqiv.ko $(LINUX_DIR)/crypto/krng.ko \
  $(LINUX_DIR)/crypto/rng.ko $(LINUX_DIR)/crypto/seqiv.ko
  AUTOLOAD:=$(call AutoProbe,ctr ccm ecb cmac krng chainiv wlcore_sdio wl18xx)
endef

define KernelPackage/TI_WL18xx_driver/description
 Internal kernel modules for TI WL18xx driver support
endef

$(eval $(call KernelPackage,TI_WL18xx_driver))

define KernelPackage/TI_bluetooth_driver
  SUBMENU:=$(WIRELESS_MENU)
  TITLE:= bluetooth driver for TI WL18xx module
  DEPENDS:=@TARGET_omap +@PACKAGE_uim +@PACKAGE_TI-Bluetooth-firmware
  KCONFIG:=CONFIG_BT CONFIG_BT_HCIUART CONFIG_BT_HCIUART_H4 CONFIG_BT_WILINK CONFIG_TI_ST=y CONFIG_ST_HCI=y \
          CONFIG_BT_BREDR=y CONFIG_BT_LE=y CONFIG_BT_SELFTEST=n CONFIG_BT_DEBUGFS=y CONFIG_BT_HCIUART_INTEL=n \
          CONFIG_BT_HCIUART_BCM=n CONFIG_RFKILL=y CONFIG_RFKILL_GPIO=n
  FILES:=\
  $(LINUX_DIR)/drivers/bluetooth/btwilink.ko $(LINUX_DIR)/net/bluetooth/bluetooth.ko \
  #$(LINUX_DIR)/net/ieee802154/6lowpan_iphc.ko
endef

define KernelPackage/TI_bluetooth_driver/description
  Internal Kernel modules for TI bluetooth driver support
endef

$(eval $(call KernelPackage,TI_bluetooth_driver))

define KernelPackage/venus_MTD
  SUBMENU:=$(WIRELESS_MENU)
  TITLE:= venus_MTD
  DEPENDS:=@TARGET_omap
  KCONFIG:=CONFIG_SPI_OMAP24XX=y CONFIG_MTD_M25P80=y
endef

define KernelPackage/venus_MTD/description
  venus_MTD
endef

$(eval $(call KernelPackage,venus_MTD))

