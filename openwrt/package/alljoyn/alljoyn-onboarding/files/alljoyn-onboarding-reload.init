#!/bin/sh /etc/rc.common

# Copyright AllSeen Alliance. All rights reserved.
#
#    Permission to use, copy, modify, and/or distribute this software for any
#    purpose with or without fee is hereby granted, provided that the above
#    copyright notice and this permission notice appear in all copies.
#
#    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
#    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
#    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
#    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
#    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
#    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
#    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#

START=58

RADIO_24G=
find_2_4_radio() {
    local cfg=$1

    #list of known wireless protocols
    local validModes="11b 11g 11a 11ng 11na"

    # get the current hwmode
    local hwmode
    config_get hwmode "$cfg" hwmode

    # verify that the hwmode is one of the valid modes
    local hasMode=`echo "$validModes" | grep $hwmode`
    if [ -n "$hasMode" ]; then
        RADIO_24G=$cfg
    fi
}

APVAP_24G=
find_2_4_apvap() {
    local cfg=$1
    local radio=$2

    local mode
    config_get mode "$cfg" mode
    local device
    config_get device "$cfg" device

    [ "$mode" = "ap" -a "$device" = "$radio" ] && \
        APVAP_24G="$cfg"
}

start() {
    config_load alljoyn-onboarding
    local state
    state=$(uci_get alljoyn-onboarding @onboarding[0] state)
    [ "${state}" = "3" ] && return 0

    # ap_ssid length must be limited at 32 chars
    local ap_ssid=$(uci_get security @access-point[0] ssid | cut -c1-32)
    local ap_encryption=$(uci_get security @access-point[0] encryption)
    local ap_key=$(uci_get security @access-point[0] key)
    local ap_channel=$(uci_get security @access-point[0] channel)
    
    local board_name=$(fw_printenv -n board_name | sed -e 's/ //g')
    local board_ser=$(fw_printenv -n serial_number | sed -e 's/ //g')

    local default_ap_ssid="${board_name} ${board_ser#${board_ser%???}}"

    # save the ssid in case it gets overwritten and we need it again
    if [ "$ap_ssid" != "" ]; then
        uci_set alljoyn-onboarding @onboarding[0] apssid "$ap_ssid"
    else
        uci_set alljoyn-onboarding @onboarding[0] apssid "$default_ap_ssid"
    fi

    if [ "$ap_encryption" != "" ]; then
        uci_set alljoyn-onboarding @onboarding[0] apencryption "$ap_encryption"
    fi
    uci_set alljoyn-onboarding @onboarding[0] apkey "$ap_key"
    uci_commit alljoyn-onboarding
    
    config_load wireless

    config_foreach find_2_4_radio wifi-device
    [ -z "${RADIO_24G}" ] && {
        echo "Error: No 2.4GHz radio found"
        return 1
    }

    config_foreach find_2_4_apvap wifi-iface ${RADIO_24G}
    [ -z "${APVAP_24G}" ] && {
        echo "Error: No 2.4GHz AP VAP found"
        return 1
    }

    local current_ap_ssid=`uci_get wireless ${APVAP_24G} ssid`
    # Ok, now we start setting the configuration parameters
    uci_set wireless ${RADIO_24G} disabled 0
    if [ "$ap_ssid" != "" ]; then
        uci_set wireless ${RADIO_24G} channel "$ap_channel"
    else
        uci_set wireless ${RADIO_24G} channel 6
    fi

    if [ "$ap_ssid" != "" ]; then
        uci_set wireless ${APVAP_24G} ssid "$ap_ssid"
    else
        uci_set wireless ${APVAP_24G} ssid "$default_ap_ssid"
    fi
    
    if [ "$ap_encryption" != "" ]; then
        uci_set wireless ${APVAP_24G} encryption "$ap_encryption"
    fi
    uci_set wireless ${APVAP_24G} key "$ap_key"

    uci_commit wireless

    local new_ap_ssid=`uci_get wireless ${APVAP_24G} ssid`
    if [ "$current_ap_ssid" != "$new_ap_ssid" ]; then
        wifi
    fi
    
    # Add other appropriate tasks when device is not fully configured
}
