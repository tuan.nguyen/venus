#!/bin/bash
if [ $# -lt 1 ]; then
   echo "example usage: $0 <logfile>"
   exit 1
fi
YEL='\033[0;33m'
NC='\033[0m' # No Color

LOG_FILE=$1
CHECK_ISSUE=`tac $LOG_FILE | grep -m 1 'failed'`
ISS_PKG_NAME=`tac $LOG_FILE | grep -m 3 '.' | grep -m 1 'package' | cut -d ' ' -f 4`
statement=""
temp=`echo "$CHECK_ISSUE"`

if [ "$temp" != "" ]; then statement=`echo "$ISS_PKG_NAME/compile V=s"`; fi
if [ "$statement" != "" ]; then echo -e "${YEL}make[3] -C $statement${NC}" && make $statement; fi
