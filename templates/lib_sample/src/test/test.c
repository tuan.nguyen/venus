#include <stdio.h>
#include<stdlib.h>

#include "search.h"

int  main()
{
	int i,num, pos;
	node_t *head;
	head = NULL;
	while(1)
	{
		printf("\nList Operations\n");
		printf("===============\n");
		printf("1.Insert\n");
		printf("2.Display\n");
		printf("3.Size\n");
		printf("4.Delete\n");
		printf("5.Search\n");
		printf("6.Exit\n");
		printf("Enter your choice : ");
		if(scanf("%d",&i)<=0){
			printf("Enter only an Integer\n");
			exit(0);
		} else {
			switch(i)
			{
				case 1:      
					printf("Enter the number to insert : ");
					scanf("%d",&num);
					insert(&head, num);
					printf("head pointer:%p\n", head);
					break;
				case 2:     
					if(head == NULL)
					{
						printf("List is Empty\n");
					}
					else
					{
						printf("Element(s) in the list are : ");
					}
					display(head);
					break;
				case 3:     
					printf("Size of the list is %d\n",count(head));
					break;
				case 4:     
					if(head==NULL)
						printf("List is Empty\n");
					else{
						printf("Enter the number to delete : ");
						scanf("%d",&num);
						if(delete(&head, num))
							printf("%d deleted successfully\n",num);
						else
						printf("%d not found in the list\n",num);
					    }
					break;
				case 5:
					printf("Enter item to be searched : ");
 					scanf("%d",&num);
 					pos=search(head, num);
					if(pos >= 0)
						printf("Your item is at node %d",pos);
					 else
						printf("Sorry! item is no in linked list.\n");
					break;
				case 6:     return 0;
				default:    printf("Invalid option\n");
			}
		}
	}
	return 0;
}
