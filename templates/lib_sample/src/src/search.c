#include <stdio.h>

#include "search.h"

int search(struct node *start, int item)
{
	int n= 1, pos = -1;


	if (start->data == item)
		return 1;
	else
		n++;
	
	do 
	{
		start = start->next;
		if(start->data == item)
		{
			pos = n;
			break;
		}	
		else
			n++;
	} while(start->next != NULL);
	
	return pos;
}

