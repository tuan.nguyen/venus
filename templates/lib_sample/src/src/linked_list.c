#include<stdio.h>
#include<stdlib.h>

#include "linked_list.h"
 
void append(node_t **head, int num)
{
	node_t *temp,*right;

	temp = (node_t *)malloc(sizeof(node_t));
    	temp->data = num;
    	right = *head;
    	while(right->next != NULL) right = right->next;
    
	right->next = temp;
    	right = temp;
    	right->next = NULL;
}

void initialize(node_t **head, int num)
{
	node_t *temp;

    	temp = (node_t *)malloc(sizeof(node_t));
    	temp->data = num;
	temp->next = NULL;

  	*head = temp;
}
 
void add(node_t **head, int num )
{
	node_t *temp;

    	temp = (node_t *)malloc(sizeof(node_t));
    	temp->data = num;
    	
	if(*head != NULL)
	{
    		temp->next = *head;
    		*head = temp;
    	}
}

void addafter(node_t **head, int num, int loc)
{
    	int i;
    	node_t *temp,*left,*right;
    	right = *head;
    	for(i = 1;i < loc;i++)
    	{
    		left = right;
    		right = right->next;
    	}
    	
	temp = (node_t *)malloc(sizeof(node_t));
    	temp->data = num;
    	left->next = temp;
    	left = temp;
    	left->next = right;
    	return;
}
 
void insert(node_t **head, int num)
{
   	int c = 0;
    	node_t *temp;
    
	temp = *head;
    	
	if (temp != NULL) 
	{
    		while(temp != NULL)
    		{
        		if(temp->data < num) c++;
        		temp = temp->next;
    		}
    	
		if(c == 0) 
			add(head, num);
    		else if(c < count(*head))
        		addafter(head, num,++c);
    		else
        		append(head, num);
    	}
	else
 	{
		initialize(head, num);
	}
}
 
int delete(node_t **head, int num)
{
	node_t *temp, *prev;

	temp = *head;
	while(temp != NULL)
	{
		if(temp->data == num)
		{
			if(temp == *head)
			{
				*head = temp->next;
				free(temp);
				return 1;
			}
			else
			{
				prev->next = temp->next;
				free(temp);
				return 1;
			}
		}
		else
		{
			prev = temp;
			temp = temp->next;
		}
	}
	return 0;
}
 
void  display(node_t *head)
{
	node_t *n;
	n = head;
	if(n == NULL)
	{
		return;
	}
	while(n != NULL)
	{
		printf("%d ", n->data);
		n = n->next;
	}
	printf("\n");
}
 
int count(node_t *head)
{
	node_t *n;
	int c = 0;

	n = head;
	while(n != NULL)
	{
		n = n->next;
		c++;
	}
	return c;
}
