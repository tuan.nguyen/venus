
typedef struct node
{
	int data;
	struct node *next;
} node_t;

void insert(node_t **,int num);
int delete(node_t **,int num);
void  display(node_t *);
int count(node_t *);

