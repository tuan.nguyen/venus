#!/bin/bash
EXPECTED_ARGS=3
E_BADARGS=65

set -o nounset
set -e

if [ $# -ne $EXPECTED_ARGS ]
then
  echo "Usage: `basename $0` <sd card node> <openwrt_bin_dir> <board:ceres|venus|venus-2|titan>"
  echo "Example usage: `basename $0` /dev/sdb ./openwrt/bin ceres"
  exit $E_BADARGS
fi

MMC_DEV=$1
OPENWRT_BINDIR=$2
BOARD_NAME=$3
if [ "$BOARD_NAME" == "venus-2" ]; then
    BOARD_ROOTFS_NAME="VENUS_2"
else
    BOARD_ROOTFS_NAME=`echo ${BOARD_NAME} | awk '{print toupper($0)}'`
fi
TMP_BOOT_MOUNT=/tmp/mmc_boot_mount
TMP_ROOT_MOUNT=/tmp/mmc_root_mount

if [[ ${MMC_DEV} = /dev/sd? ]]
then
 MMC_BOOT_PART=${MMC_DEV}1
 MMC_ROOT_PART=${MMC_DEV}2
else
 MMC_BOOT_PART=${MMC_DEV}p1
 MMC_ROOT_PART=${MMC_DEV}p2
fi

VERSION=${OPENWRT_BINDIR}/omap/uboot-omap-${BOARD_NAME}/version
MLO_FILE=${OPENWRT_BINDIR}/omap/uboot-omap-${BOARD_NAME}/MLO
UBOOT_FILE=${OPENWRT_BINDIR}/omap/uboot-omap-${BOARD_NAME}/u-boot.img
ROOTFS_FILE=${OPENWRT_BINDIR}/omap/openwrt-omap-${BOARD_ROOTFS_NAME}-rootfs.tar.gz

for f in ${MLO_FILE} ${UBOOT_FILE} ${ROOTFS_FILE}
do
        echo -n "Checking file $f ..."
        if [ ! -f $f ]
        then
                echo "missing"
                exit 1
        else
                echo "ok"
        fi
done

if [ ! -e ${MMC_DEV} ]
then
        echo "Please check your SD card is plugged?"
        exit 1
else
        # make sure it is umounted
	for n in `mount | grep ${MMC_DEV} | cut -d" " -f1`; do sudo umount $n; done
fi

echo "Mount && Populate the BOOT"
if [ ! -d ${TMP_BOOT_MOUNT} ]
then
        mkdir -p ${TMP_BOOT_MOUNT}
        sudo chmod 777 -R ${TMP_BOOT_MOUNT}
        sudo mount ${MMC_BOOT_PART} ${TMP_BOOT_MOUNT}
else
        mount | grep ${TMP_BOOT_MOUNT} && sudo umount ${TMP_BOOT_MOUNT}
        sudo chmod 777 -R ${TMP_BOOT_MOUNT}
        sudo mount ${MMC_BOOT_PART} ${TMP_BOOT_MOUNT}
fi

echo "Mount && Populate the rootfs"
if [ ! -d ${TMP_ROOT_MOUNT} ]
then
        mkdir -p ${TMP_ROOT_MOUNT}
        sudo chmod 777 -R ${TMP_ROOT_MOUNT}
        sudo mount ${MMC_ROOT_PART} ${TMP_ROOT_MOUNT}
else
        mount | grep ${TMP_ROOT_MOUNT} && sudo umount ${TMP_ROOT_MOUNT}
        sudo chmod 777 -R ${TMP_ROOT_MOUNT}
        sudo mount ${MMC_ROOT_PART} ${TMP_ROOT_MOUNT}
fi

sudo rm -fr ${TMP_BOOT_MOUNT}/*

sudo cp ${MLO_FILE} ${TMP_BOOT_MOUNT}/
sudo cp ${UBOOT_FILE} ${TMP_BOOT_MOUNT}/
sudo cp ${VERSION} ${TMP_BOOT_MOUNT}/

sudo rm -fr ${TMP_ROOT_MOUNT}/*

sudo tar -xzf ${ROOTFS_FILE} -C ${TMP_ROOT_MOUNT}

sudo umount ${MMC_BOOT_PART}
sudo umount ${MMC_ROOT_PART}
