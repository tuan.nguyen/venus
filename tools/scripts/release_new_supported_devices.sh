#!/bin/sh

LATEST_VERSION_FILE_NAME=latest_version
FIRMWARE_FILE_NAME=openwrt-omap-ext4.img.tar.gz
LAST_VERSION_FILE_NAME=version
LAST_VERSION_DIR=last_firmware
USER_NAME=venus_firmware
PASSWD=v3nu5_f!rmw@r3

EXPECTED_ARGS=3
E_BADARGS=65

set -o nounset
set -e

if [ $# -ne $EXPECTED_ARGS ]
then
  echo "Usage: `basename $0` <new release version> <folder contains the firmware file> <relase|develop>"
  echo "Example usage: `basename $0` abc123 ./openwrt/bin/omap develop"
  exit $E_BADARGS
fi

TAG=$1
DIR=$2
VER=$3
echo $VER

if [ "$VER" != "develop" ]
then
	FTP_SITE=ftp.veriksystems.com
else
	FTP_SITE=192.168.1.150
fi

echo "Step 1: get the last version to create the old version folder"
cat <<EOF > /tmp/get_last_version.ftp
open $FTP_SITE 
user $USER_NAME $PASSWD
binary
cd /
cd $LAST_VERSION_DIR
get $LAST_VERSION_FILE_NAME /tmp/$LAST_VERSION_FILE_NAME
EOF

ftp -n < /tmp/get_last_version.ftp
rm /tmp/get_last_version.ftp

OLD_VERSION=`cat /tmp/$LAST_VERSION_FILE_NAME`
OLD_VERSION_DIR=${OLD_VERSION}'_firmware'
echo $OLD_VERSION_DIR
echo "Step 2: change last_version to old version and move latest version to last verion"
cat <<EOF > /tmp/update_last_version.ftp
open $FTP_SITE 
user $USER_NAME $PASSWD
binary
cd /
rename $LAST_VERSION_DIR ${OLD_VERSION_DIR}
get $LATEST_VERSION_FILE_NAME /tmp/$LAST_VERSION_FILE_NAME 
get $FIRMWARE_FILE_NAME /tmp/$FIRMWARE_FILE_NAME
mkdir $LAST_VERSION_DIR
put /tmp/$LAST_VERSION_FILE_NAME $LAST_VERSION_DIR/$LAST_VERSION_FILE_NAME
put /tmp/$FIRMWARE_FILE_NAME $LAST_VERSION_DIR/$FIRMWARE_FILE_NAME
EOF

ftp -n < /tmp/update_last_version.ftp
rm /tmp/update_last_version.ftp

echo "Step 3: put new firmware version to server"
if [ -f /tmp/$LATEST_VERSION_FILE_NAME ]
then
   rm /tmp/$LATEST_VERSION_FILE_NAME 
fi
echo $TAG > /tmp/$LATEST_VERSION_FILE_NAME

if [ ! -f $DIR/$FIRMWARE_FILE_NAME ]
then
	echo "$DIR/$FIRMWARE_FILE_NAME file doesn't exist. Please check !!!!"
	exit 1
fi

cat <<EOF > /tmp/upload_release_version.ftp
open $FTP_SITE 
user $USER_NAME $PASSWD 
binary
cd /
put /tmp/$LATEST_VERSION_FILE_NAME $LATEST_VERSION_FILE_NAME
put $2/$FIRMWARE_FILE_NAME $FIRMWARE_FILE_NAME
EOF

ftp -n < /tmp/upload_release_version.ftp
rm /tmp/upload_release_version.ftp

# Step 4: clean up
rm /tmp/$LATEST_VERSION_FILE_NAME
rm /tmp/$LAST_VERSION_FILE_NAME
rm /tmp/$FIRMWARE_FILE_NAME
