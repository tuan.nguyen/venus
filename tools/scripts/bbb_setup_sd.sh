#!/bin/bash
# As per instructions:
# http://elinux.org/BeagleBoardBeginners
 
if [ $# -lt 1 ]; then
   echo "example usage: $0 /dev/sdb"
   exit 1
fi
 
DRIVE=$1

if [ ! -e ${DRIVE} ]
then
        echo "Please check your SD card is plugged?"
        exit 1
else
        # make sure it is umounted
        mount | grep ${DRIVE} && sudo umount ${DRIVE}?
fi

 
sudo dd if=/dev/zero of=$DRIVE bs=1024 count=1024


if [[ ${DRIVE} != /dev/sd? ]]
then
P_DRIVE=${DRIVE}p
else
P_DRIVE=$DRIVE
fi

cat <<EOF > /tmp/partitions.sfdisk
# partition table of $DRIVE 
unit: sectors

${P_DRIVE}1 : start=    12288, size=    32768, Id= 6, bootable
${P_DRIVE}2 : start=    45056, size=   524288, Id=83
${P_DRIVE}3 : start=   569344, size=   524288, Id=83
${P_DRIVE}4 : start=  1093632, size=  6459392, Id= 5
${P_DRIVE}5 : start=  1095680, size=  5933056, Id=83
${P_DRIVE}6 : start=  7030784, size=   522240, Id=83
EOF
 
sudo sfdisk --force $DRIVE < /tmp/partitions.sfdisk
rm /tmp/partitions.sfdisk

sudo mkfs.vfat -n "boot" ${P_DRIVE}1
sudo mke2fs -j -L "rootfs0" ${P_DRIVE}2
sudo mke2fs -j -L "rootfs1" ${P_DRIVE}3
sudo mke2fs -j -L "rootfs_data" ${P_DRIVE}5
sudo mke2fs -j -L "settings" ${P_DRIVE}6
