#!/bin/bash

diff(){
	awk 'BEGIN{RS=ORS=" "}
		{NR==FNR?a[$0]++:a[$0]--}
		END{for(k in a)if(a[k])print k}' <(echo -n "${!1}") <(echo -n "${!2}")
}

is_file_exists(){
	local f="$1"
	[[ -f "$f" ]] && return 0 || return 1
}

usage(){
	echo "Usage: $0 [ another openwrt/bin folder ]"
	echo "Without parameter, the default folder ceres_app/openwrt/bin will be used."
	exit 1
}

echo
input=$1

if [[ $# -eq 0 ]]
then
	read -p "Do you want me to use default folder ceres_app/openwrt/bin to flash the venus-2 board? [yY]" -n 1 -r
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
		input="venus-2"
	fi
fi

echo "We will flash the image at ceres_app/openwrt/bin to $input board."
echo 

echo "Please do not insert any USB Sticks"\
		"or mount external hdd during the procedure."
echo 

read -p "When the $input board is connected in USB Boot mode press [yY]." -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]]
then
	before=($(ls /dev | grep "sd[a-z]$"))

	if ( ! is_file_exists venus_usb_flasher)
	then
		echo "Please make the project then execute the script!"
		exit 1
	fi

	echo
	echo "Putting the Venus board into flashing mode!"
	echo

	sudo ./venus_usb_flasher
	rc=$?
	if [[ $rc != 0 ]];
	then
		echo "The Venus board cannot be put in USB Flasing mode. Send "\
				"logs to support@veriksystems.com together with the serial output from the"\
				"$input board."
		exit $rc
	fi

	echo -n "Waiting for the $input board to be mounted"
	for i in {1..12}
	do
		echo -n "."
		sleep 1
	done
	echo 

	after=($(ls /dev | grep "sd[a-z]$"))
	bbb=($(diff after[@] before[@]))
	
	if [ -z "$bbb" ];
	then
		echo "The $input board cannot be detected. Either it has not been"\
				" mounted or the g_mass_storage module failed loading. "\
				"Please send the serial log over to support@veriksystems.com for debugging."
		exit 1
	fi
	
	if [ ${#bbb[@]} != "1" ]
	then
		echo "You inserted an USB stick or mounted an external drive. Please "\
			"rerun the script without doing that."
		exit 1
	fi

	read -p "Are you sure the $input board is mounted at /dev/$bbb?[yY]" -n 1 -r
	echo
	
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
		parts=($(ls /dev | grep "$bbb[1,2]"))
		for index in ${!parts[*]}
		do
			sudo umount /dev/${parts[$index]}
		done
	
		echo "Flashing now, be patient. It will take ~5 minutes!"
		echo
	
		sudo ../../scripts/bbb_setup_sd.sh /dev/$bbb
		sudo ../../scripts/bbb_load_sd.sh /dev/$bbb ../../../openwrt/bin $input 
	
		echo
		echo "Please remove power from your board and plug it again."\
			"You will boot in the new OS!"
	fi
fi
